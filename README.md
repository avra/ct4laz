# ct4laz #



[**CodeTyphon**](https://www.pilotlogic.com/sitejoom/index.php/projects/codetyphon-studio) is a fork of [**Lazarus**](https://www.lazarus-ide.org/) which is an open source version of [**Delphi**](https://www.embarcadero.com/products/delphi) IDE using [**FreePascal**](https://www.freepascal.org/) cross platform compiler. CodeTyphon has tons of components but unfortunately they can not be used in Lazarus directly. This is corrected by **ct4laz**. Many ct4laz components can already be downloaded directly from Lazarus using [**Online Package Manager**](https://wiki.freepascal.org/Online_Package_Manager), but some can be found only in this repo. Besides ready to use components and examples for Lazarus, you will also find step by step instructions if you want to do conversion on your own. That is useful if you want to convert some missing component, or if you want to convert some updated CodeTyphon component and don't want to wait for it to show here.



### Instructions

These are step by step instructions how to use [**ct2laz**](https://bitbucket.org/avra/ct2laz/src/master/) convertor and PowerShell scripts to get components and examples ready to be consumed by Lazarus.

1. I will use ***m:\lazarus\\*** directory as a base location in this instruction. Feel free to replace with your own location each time it is mentioned. Create ***ct4laz*** directory inside of it. You should now have ***m:\lazarus\ct4laz\\***.
2. Copy scripting directory from the repo to ***m:\lazarus\ct4laz\\***. This directory holds ***.list* **files with directory and file lists, ***ct4laz.ps1*** PowerShell script doing the file and directory replacements, ***pack.ps1*** script for archives creation, ***files\\*** directory holding file replacements and ***old.working.dir\\*** directory holding directory replacements.
3. Download and run [**ct2laz**](https://bitbucket.org/avra/ct2laz/src/master/) with all default settings except Download location which you set to ***m:\lazarus\ct4laz\scripting\ct2laz.dl\\***. This will download latest CodeTyphon components and examples so be ready that some new or updated ones will need fixing to compile. If you are bad at fixing compilation issues you can always copy working ones from ***pl_components\\*** and ***pl_examples\\*** directories in the repo.
4. Open **PowerShell ISE**, navigate command shell to ***m:\lazarus\ct4laz\scripting\\* **directory and execute script ***.\ct4laz -ct2laz "m:\lazarus\ct4laz\scripting\ct2laz.dl\" -dbg 1***. This should give you ***m:\lazarus\ct4laz\pl_components\\*** and ***m:\lazarus\ct4laz\pl_examples\\*** directories with everything ready to test in Lazarus. If there are any errors try to fix them.
5. If there were no errors, then execute script ***.\pack***, and in ***m:\lazarus\ct4laz\pl_zip\\*** directory you should have lots of pl_*.zip (for OPM and ct4laz repo) and px_*.zip archives (only for ct4laz repo). 
6. Enjoy and have fun!



### Download ###

If for some reason you do not handle git, then full repository can be downloaded manually from [here](https://bitbucket.org/avra/ct4laz/downloads/).



### License ###

ct4laz is released under [Mozilla Public License 2.0 (MPL-2.0)](https://www.mozilla.org/MPL/2.0). Here you have [MPL-2.0 license explained in plain English](https://www.tldrlegal.com/l/mpl-2.0). This license covers only my work for scripts and lists. Each component holds individual license file, but it is your responsibility to search the net for original license info of each component you want to use!



### Author ###

Made by Жељко Аврамовић (user Avra in Lazarus forum).



### Versions ###

* 1.0 First public version.
* 2.0 Second public version.
* 3.0 Third public version.