library TestPlugin01;

{$include RELEASE_SETTINGS.INC}
{.$include DAV_Compiler.inc}
{$include VSTPLUGIN_SWITCHES.INC}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  {$ENDIF}
  Forms
  { you can add units after this },
  TestPluginMain,    // contains DLL entry point function(s)
  MyVSTModule,
  DAV_VSTEffect,
  DAV_VSTBasicModule,
  DAV_VSTModule;


exports
{$IFDEF DARWIN}  {OS X entry points}
  VSTPluginMain name '_main',
  VSTPluginMain name '_main_macho',
  VSTPluginMain name '_VSTPluginMain';
{$ELSE}
  VSTPluginMain name 'main',
  VSTPluginMain name 'main_plugin',
  VSTPluginMain name 'VSTPluginMain';
{$ENDIF}

{$R *.res}

begin
  Application.Initialize;
end.
