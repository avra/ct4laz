unit MyVSTEditor;

{$include RELEASE_SETTINGS.INC}

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

{*******************************************************************************
*
*  File extracted from  'VST Plugin Wizard by Christian Budde & Tobybear'
*
*******************************************************************************}

interface

uses
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  Messages,
  {$ENDIF}
  Classes, Controls, Forms, Dialogs, ComCtrls, StdCtrls, SysUtils, ExtCtrls,
  MyVSTModule;


type
  { TVSTEditorForm }
  TVSTEditorForm = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    TrackBar0: TTrackBar;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    procedure TrackBar0Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
    theModule: TVSTModule1;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;


const
  TrackBar0Max = 100;
  TrackBar1Max = 100;

var
  VSTEditorForm: TVSTEditorForm;


implementation

{$IFDEF FPC}
  {$R *.lfm}
{$ELSE}
  {$R *.lfm}
{$ENDIF}

constructor TVSTEditorForm.Create(AOwner: TComponent);
begin
  inherited;
  TrackBar0.Max:= TrackBar0Max;  // init var manually
  TrackBar1.Max:= TrackBar1Max;  // init var manually
end;

destructor TVSTEditorForm.Destroy;
begin
  inherited;
end;

procedure TVSTEditorForm.TrackBar0Change(Sender: TObject);
begin
  if theModule.Parameter[0] <> (TrackBar0.Max -TrackBar0.Position) then
    begin
      theModule.Parameter[0]:= (TrackBar0.Max -TrackBar0.Position);
      Edit1.Caption:= IntToStr( Round( theModule.Parameter[0]));
    end;
end;

procedure TVSTEditorForm.TrackBar1Change(Sender: TObject);
begin
  if theModule.Parameter[1] <> (TrackBar1.Position) / TrackBar1.Max then
    begin
      theModule.Parameter[1]:= TrackBar1.Position / TrackBar1.Max;
      Edit2.Caption:= IntToStr( Round( TrackBar1.Max * theModule.Parameter[1]));
    end;
end;


end.
