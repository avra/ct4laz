unit MyVSTModule;

{$include RELEASE_SETTINGS.INC}

  {$mode objfpc}{$H+}

interface

uses
  Interfaces,
  JWAwindows,
  LResources,
  Classes, Forms, SysUtils, FileUtil,
  DAV_VSTModule, DAV_Types, DAV_VSTCustomModule, DAV_VSTEffect;


type
  { TVSTModule1 }
  TVSTModule1 = class(TVSTModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleEditOpen(Sender: TObject; var GUI: TForm; {$H-}ParentWindow: THandle);
    procedure DataModuleInitialize(Sender: TObject);
    procedure DataModuleParameterChange( Sender: TObject; const Index: Integer;
                                         var Value: Single);
    procedure DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                 const SampleFrames: Cardinal);
    procedure DataModuleProcess32Replacing( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                            const SampleFrames: Cardinal);
    procedure DataModuleProcess64Replacing( const Inputs, Outputs: TDAVArrayOfDoubleFixedArray;
                                            const SampleFrames: Cardinal);
  private
    //
  public
    //
  end;

var
  VSTModule1: TVSTModule1;


implementation

{$R *.lfm}

uses
  MyVSTEditor;

{ *** TVSTModule1 ************************************************************ }
procedure TVSTModule1.DataModuleCreate(Sender: TObject);
begin
  // Whyever, the Property Editor throws away the following assignments.
  // So I forced them manually here :
  OnProcess            := @DataModuleProcess;
  OnProcess32Replacing := @DataModuleProcess32Replacing;
  OnProcess64Replacing := @DataModuleProcess64Replacing;
end;

procedure TVSTModule1.DataModuleEditOpen( Sender: TObject; var GUI: TForm;
                                          ParentWindow: THandle);
// Use this if you'd like to use an Editor
// But be sure to enable this feature using the pfEditorFlag
begin
  TRY
    GUI := TVSTEditorForm.Create(nil);
    //doesn't work:  GUI := TVSTEditorForm.CreateNew( nil, 0);
  EXCEPT
    Application.MessageBox( 'EXCEPTION on:  GUI := TVSTEditorForm.Create(nil);',
                            'TVST2Module1.V2M_EditOpen', MB_OK);
  end;

  TRY
    // assign the plugin effect to a pointer in the Plugin GUI :
    (GUI as TVSTEditorForm).theModule:= Self;
  EXCEPT
    Application.MessageBox( 'EXCEPTION on:  (GUI As TVSTEditorForm).theModule:= Self;',
                            'TVST2Module1.V2M_EditOpen', MB_OK);
  end;
end;

procedure TVSTModule1.DataModuleInitialize(Sender: TObject);
// Within this Method you may initialize your programs
var
  i : Integer;
begin
  for i:= 0 to (Sender As TVSTModule).NumPrograms -1 do
    begin
      // Volume value :
      (Sender As TVSTModule1).Programs[i].Parameter[0]:= TrackBar0Max;
      // Pan value :
      (Sender As TVSTModule1).Programs[i].Parameter[1]:= 0;
    end;
end;

procedure TVSTModule1.DataModuleParameterChange( Sender: TObject;
                                                 const Index: Integer;
                                                 var Value: Single);
begin
  if EditorForm <> NIL then
    begin
       case index of
         0: (EditorForm as TVSTEditorForm).TrackBar0.Position:= round( TrackBar0Max -Value);
         1: (EditorForm as TVSTEditorForm).TrackBar1.Position:= round( Value * TrackBar1Max);
       end;
    end;
end;

procedure TVSTModule1.DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                         const SampleFrames: Cardinal);
var
  fL : single;
  fR : single;
  j  : integer;
begin
  // Example of a process routine
  fL:= Parameter[0] / TrackBar0Max * sqrt(-((Parameter[1] -1)));
  fR:= Parameter[0] / TrackBar0Max * sqrt( (1 +Parameter[1]));
  for j:= 0 to SampleFrames -1 do
    begin
      Outputs[0]^[j]:= Inputs[0]^[j] * fL;
      Outputs[1]^[j]:= Inputs[1]^[j] * fR;
    end;
end;

procedure TVSTModule1.DataModuleProcess32Replacing( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                                    const SampleFrames: Cardinal);
begin
  DataModuleProcess( Inputs, Outputs, SampleFrames);
end;

procedure TVSTModule1.DataModuleProcess64Replacing( const Inputs, Outputs: TDAVArrayOfDoubleFixedArray;
                                                    const SampleFrames: Cardinal);
var
  fL : single;
  fR : single;
  j  : integer;
begin
  // Example of a process routine
  fL:= Parameter[0] / TrackBar0Max * sqrt(-((Parameter[1] -1)));
  fR:= Parameter[0] / TrackBar0Max * sqrt( (1 +Parameter[1]));
  for j:= 0 to SampleFrames -1 do
    begin
      Outputs[0]^[j]:= Inputs[0]^[j] * fL;
      Outputs[1]^[j]:= Inputs[1]^[j] * fR;
    end;
end;


end.

