unit TestPluginMain;

{$include RELEASE_SETTINGS.INC}

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

{*******************************************************************************
*                      unit TestPluginMain
*
*                      latest changes:  2018-01-12
*
********************************************************************************
*
*  Hint:  $DEFINE DebugLog  slows down the GUI performance,
*         especially the faders in this example !
*
*******************************************************************************}

interface

uses
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  {$ENDIF}
  Forms,
  SysUtils,
  Dialogs,
  Classes,
  DAV_VSTEffect, // OldName: DAEffect,
  DAV_VSTModule, // OldName: DVSTModule,
  MyVSTModule;   // OldName: VSTDataModule;

function VSTPluginMain( AudioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;


implementation

function VSTPluginMain( AudioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;
var
  MyVSTModule : TVSTModule1;
begin
  TRY
    MyVSTModule              := TVSTModule1.Create( Application);
    MyVSTModule.Effect^.User := MyVSTModule;
    Result                   := MyVSTModule.Effect;
  EXCEPT
    Result:= NIL;
  end;
end;


end.

