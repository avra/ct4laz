library VUMeter;

{$include RELEASE_SETTINGS.INC}
{.$include DAV_Compiler.inc}
{$include VSTPLUGIN_SWITCHES.INC}

uses
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  {$ENDIF}
  Forms,
  VUMeterModule,
  DAV_VSTEffect,
  DAV_VSTModule,
  VUMeterGUI;


function main( AudioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;
var
  VSTVUMeterModule : TVSTVUMeterModule;
begin
  try
    VSTVUMeterModule:= TVSTVUMeterModule.Create(Application);
    VSTVUMeterModule.Effect^.User:= VSTVUMeterModule;
    VSTVUMeterModule.AudioMaster:= AudioMaster;
    Result:= VSTVUMeterModule.Effect;
  except
    Result:= nil;
  end;
end;


exports Main name 'main';
exports Main name 'VSTPluginMain';


{$R *.res}

begin
  {$IFDEF FPC}Application.Initialize;{$ENDIF}
end.
