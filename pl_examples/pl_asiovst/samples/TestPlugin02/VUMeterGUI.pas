unit VUMeterGUI;

{$include RELEASE_SETTINGS.INC}

  {$mode objfpc}{$H+}

interface

uses
  Interfaces,
  SysUtils, Classes, Forms, Controls, StdCtrls, ExtCtrls,
  DAV_Common, VUMeterModule, DAV_VSTModule;

type
  TVSTVUMeterGUI = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    vu_l: TShape;
    vu_r: TShape;
    gain_l: TLabel;
    gain_r: TLabel;
    par0: TScrollBar;
    par1: TScrollBar;
    procedure ParameterChange(Sender: TObject);
  private
    { Private declarations }
    Timer : TTimer;
    procedure OnTimer(Sender : TObject);
  public
    { Public declarations }
    theModule: TVSTModule;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;


implementation

  {$R *.lfm}

constructor TVSTVUMeterGUI.Create(AOwner: TComponent);
begin
  inherited;
  Timer:= TTimer.Create( Self);
  Timer.Interval:= 100;
  Timer.OnTimer:= @Self.OnTimer;
  Timer.Enabled:= TRUE;
end;

destructor TVSTVUMeterGUI.Destroy;
begin
  Timer.Free;
  inherited;
end;

procedure TVSTVUMeterGUI.OnTimer(Sender : TObject);
begin
  // adjust the VU bar length :
  vu_L.Width:= round( 300 +3*Amp_to_dB( (theModule As TVSTVUMeterModule).PeakL));
  vu_R.Width:= round( 300 +3*Amp_to_dB( (theModule As TVSTVUMeterModule).PeakR));
end;

procedure TVSTVUMeterGUI.ParameterChange(Sender: TObject);
begin
  with (Sender as TScrollbar)
    do theModule.Parameter[Tag]:= Position;
end;


end.
