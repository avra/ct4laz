program StaticWaveformTest;

{$MODE Delphi}

uses
  Forms, Interfaces,
  StaticWaveformTestMain in 'StaticWaveformTestMain.pas' {FmStaticWaveformTest};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmStaticWaveformTest, FmStaticWaveformTest);
  Application.Run;
end.
