unit SineSynthModule;

interface

uses

  Interfaces,
  SysUtils, Classes, Forms,
  SineSynthVoice, VoiceList,
  DAV_VSTEffect, DAV_Types, DAV_VSTBasicModule, DAV_VSTModule;


type
  { TVSTSSModule }
  TVSTSSModule = class(TVSTModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy( Sender: TObject);
    procedure DataModuleEditOpen( Sender: TObject; var GUI: TForm;
                                  ParentWindow: THandle);
    procedure DataModuleInitialize( Sender: TObject);
    procedure DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                 const SampleFrames: Cardinal);
    procedure DataModuleProcess64Replacing( const Inputs, Outputs: TDAVArrayOfDoubleFixedArray;
                                            const SampleFrames: Cardinal);
    procedure DataModuleProcessMidi( Sender: TObject;
                                     const MidiEvent: TVstMidiEvent);
  public
    Voices : TVoiceList;
  end;


implementation

  {$R *.lfm}

uses
  SineSynthGUI;


procedure TVSTSSModule.DataModuleEditOpen(Sender: TObject; var GUI: TForm;
  ParentWindow: THandle);
begin
  // Do not delete this if you are using the editor
  GUI:= TVSTGUI.Create( Self);
  //GUI:= TVSTGUI.Create( nil);  // this does NOT work, whyever ..
  (GUI As TVSTGUI).theModule:= Self;
end;

procedure TVSTSSModule.DataModuleCreate(Sender: TObject);
begin
  // Whyever, the Property Editor throws away the following assignments.
  // So I forced them manually here :
  OnProcess            := {@}DataModuleProcess;
  OnProcess32Replacing := {@}DataModuleProcess;
  OnProcess64Replacing := {@}DataModuleProcess64Replacing;
end;

procedure TVSTSSModule.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil( Voices);
end;

procedure TVSTSSModule.DataModuleInitialize(Sender: TObject);
begin
  Voices:= TVoiceList.Create( True);
end;

procedure TVSTSSModule.DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                          const SampleFrames: Cardinal);
var
  i, j : Integer;
begin
  FillChar( outputs[0]^[0], sampleframes * SizeOf(Single), 0);
  FillChar( outputs[1]^[0], sampleframes * SizeOf(Single), 0);

  for j := 0 to sampleframes - 1 do
    for i := 0 to Voices.Count - 1
      do outputs[0]^[j] := outputs[0]^[j] + Voices[i].Process;

  for i := 1 to numOutputs - 1
    do Move( outputs[0]^[0], outputs[i]^[0], sampleframes * SizeOf(Single));
end;

procedure TVSTSSModule.DataModuleProcess64Replacing(const Inputs,
  Outputs: TDAVArrayOfDoubleFixedArray; const SampleFrames: Cardinal);
var i, j : Integer;
begin
  FillChar(outputs[0]^[0], sampleframes * SizeOf( Double), 0);
  FillChar(outputs[1]^[0], sampleframes * SizeOf( Double), 0);

  for j := 0 to SampleFrames - 1 do
    for i := 0 to Voices.Count - 1
      do outputs[0]^[j] := outputs[0]^[j] + Voices[i].Process;

  for i := 1 to numOutputs - 1
    do Move( outputs[0]^[0], outputs[i]^[0], sampleframes * SizeOf(Double));
end;

procedure TVSTSSModule.DataModuleProcessMidi(Sender: TObject;
  const MidiEvent: TVstMidiEvent);
var
  Status  : Byte;
  i       : Integer;
  newNote : TSineSynthVoice;
const VeloDiv : Single = 1/128;
begin
  Status:= MidiEvent.midiData[0] and $F0; // channel information is removed

  if (Status=$90) and (MidiEvent.mididata[2]>0) then // "note on" ?
  begin
   if Voices.Count > 7 then Voices.Remove(Voices.Items[0]);
   newNote:=TSineSynthVoice.Create(self);
   with newNote do
    begin
     newNote.MidiKeyNr:=MidiEvent.midiData[1];
     newNote.Velocity:=MidiEvent.midiData[2];
     newNote.NoteOn(Midi2Pitch[MidiKeyNr],Velocity*VeloDiv);
    end;
   Voices.Add(newNote);
  end

  else if ((status=$90) and (MidiEvent.mididata[2]=0)) or (status=$80) then // "note off" ?
  begin
   for i:=0 to Voices.Count-1 do
    begin
     if (Voices.Items[i].MidiKeyNr= MidiEvent.midiData[1]) then
      begin
       Voices.Delete(i);
       Break;
      end;
    end;
  end

  else if ((status=$B0) and (MidiEvent.midiData[1]=$7e)) then
  begin
   // all notes off
   Voices.Clear;
  end;
end;


end.
