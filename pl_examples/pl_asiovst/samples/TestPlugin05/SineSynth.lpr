library SineSynth;

uses
  Interfaces,
  Forms,
  DAV_Types, DAV_VSTEffect, DAV_VSTBasicModule, DAV_VSTModule,
  SineSynthModule,
  SineSynthGUI,
  SineSynthVoice,
  VoiceList;


function main( AudioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;
var
  VSTSSModule : TVSTSSModule;
begin
  try
    VSTSSModule              := TVSTSSModule.Create( Application);
    VSTSSModule.Effect^.user := VSTSSModule;
    VSTSSModule.AudioMaster  := AudioMaster;
    Result                   := VSTSSModule.Effect;
  except
    Result:= nil;
  end;
end;


exports
  Main name 'main',
  Main name 'VSTPluginMain';


{$R *.res}

begin
  Application.Initialize;
end.

