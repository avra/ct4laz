unit OpAmpGUI;

{$include RELEASE_SETTINGS.INC}
{$include VSTPLUGIN_SWITCHES.INC}

interface

uses
  Interfaces,
  SysUtils, Classes, Forms, Controls, StdCtrls,
  DAV_VSTBasicModule, DAV_VSTModule;

type
  TVSTGUI = class(TForm)
    SBGain: TScrollBar;
    LbGain: TLabel;
    procedure SBGainChange(Sender: TObject);
  private
    //
  public
    theModule: TVSTModule;
  end;


implementation

  {$R *.lfm}

procedure TVSTGUI.SBGainChange(Sender: TObject);
begin
  theModule.Parameter[0]:= SBGain.Position * 0.01;
end;


end.
