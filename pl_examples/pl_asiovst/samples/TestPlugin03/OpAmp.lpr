library OpAmp;

{$include RELEASE_SETTINGS.INC}
{.$include DAV_Compiler.inc}
{$include VSTPLUGIN_SWITCHES.INC}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  {$ENDIF}
  Forms,
  DAV_VSTEffect,
  DAV_VSTBasicModule,
  DAV_VSTModule,
  OpAmpModule,
  OpAmpGUI;


function main( AudioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;
begin
  try
    VSTOpAmp              := TVSTOpAmp.Create(Application);
    VSTOpAmp.Effect^.user := VSTOpAmp;
    VSTOpAmp.AudioMaster  := AudioMaster;
    Result                := VSTOpAmp.Effect;
  except
    Result:= nil;
  end;
end;


exports
  Main name 'main',
  Main name 'main_plugin',
  Main name 'VSTPluginMain';


begin
  {$IFDEF FPC}Application.Initialize;{$ENDIF}
end.

