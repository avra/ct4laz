unit OpAmpModule;

{$include RELEASE_SETTINGS.INC}
{$include VSTPLUGIN_SWITCHES.INC}

interface

uses
  Interfaces,
  Classes, Forms, SysUtils,
  FileUtil,
  DAV_Types, DAV_VSTBasicModule, DAV_VSTModule, DAV_VSTCustomModule;

type
  { TVSTOpAmp }
  TVSTOpAmp = class(TVSTModule)
    procedure VST_EditOpen(Sender: TObject; var GUI: TForm);
    procedure VSTModuleInitialize(Sender: TObject);
    procedure VSTModuleParameterChange(Sender: TObject; const Index: Integer; var Value: Single);
    procedure DataModuleProcess(const Inputs,
      Outputs: TDAVArrayOfSingleFixedArray; const SampleFrames: Cardinal);
    procedure DataModuleProcess64Replacing(const Inputs,
      Outputs: TDAVArrayOfDoubleFixedArray; const SampleFrames: Cardinal);
  private
    fGain : Double;
  public
    //
  end;

var
  VSTOpAmp : TVSTOpAmp;


implementation

  {$R *.lfm}

uses
  Math,
  OpAmpGUI;

procedure TVSTOpAmp.VST_EditOpen(Sender: TObject; var GUI: TForm);
// Do not delete this if you are using the editor
begin
  GUI := TVSTGUI.Create(nil);
  (GUI As TVSTGUI).theModule:=Self;
end;

function Tanh2a( x:Single):Single;
var
  a, b : Single;
  function f_Abs( f:Single):Single; overload;
  var i:Integer;
  begin
    i:= Integer( (@f)^) and $7FFFFFFF;
    Result:= Single( (@i)^);
  end;
begin
 a:= f_abs(x);
 b:= 12 +a*(6+a*(3+a));
 Result:= (x*b)/(a*b+24);
end;

procedure TVSTOpAmp.DataModuleProcess(const Inputs,
  Outputs: TDAVArrayOfSingleFixedArray; const SampleFrames: Cardinal);
var
  i, j : Integer;
begin
  for j:=0 to min( numOutputs, numInputs)-1 do
    for i:=0 to sampleframes-1
      do Outputs[j]^[i]:= Tanh2a( fGain * Inputs[j]^[i]);
end;

procedure TVSTOpAmp.DataModuleProcess64Replacing(const Inputs,
  Outputs: TDAVArrayOfDoubleFixedArray; const SampleFrames: Cardinal);
var
  i, j : Integer;
begin
  for j:= 0 to min( numOutputs, numInputs) -1
    do for i:= 0 to sampleframes-1
      do Outputs[j]^[i]:= Tanh2a( fGain * Inputs[j]^[i]);
end;

procedure TVSTOpAmp.VSTModuleInitialize(Sender: TObject);
begin
  Parameter[0]:= 1;
end;

procedure TVSTOpAmp.VSTModuleParameterChange( Sender: TObject;
                                              const Index: Integer;
                                              var Value: Single);
var i : Integer;
begin
  fGain:= 2 * Value;
  if Assigned( fEditorForm) then
    with fEditorForm As TVSTGUI do
      begin
        i:= Round( 100 * Value);
        if SBGain.Position <> i
          then SBGain.Position:= i;
      end;
end;


end.
