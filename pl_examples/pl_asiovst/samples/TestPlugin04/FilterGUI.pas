unit FilterGUI;

{$include RELEASE_SETTINGS.INC}

{$include VSTPLUGIN_SWITCHES.INC}

interface

uses
  Interfaces,
  SysUtils, Classes, Forms, Controls, StdCtrls,
  DAV_VSTBasicModule, DAV_VSTModule;


type
  { TVSTGUI }
  TVSTGUI = class(TForm)
    LbGain1: TLabel;
    SBGain: TScrollBar;
    LbGain: TLabel;
    procedure SBGainChange(Sender: TObject);
  private
    //
  public
    theModule: TVSTModule;
  end;


implementation

  {$R *.lfm}

uses
  FilterModule;

procedure TVSTGUI.SBGainChange(Sender: TObject);
begin
  theModule.Parameter[0] := SBGain.Position;
  LbGain.Caption         := FloatToStr( theModule.Parameter[0]);
  LbGain1.Caption        := FloatToStr( (theModule as TVSTFilter).fCutOffFrequency);
end;


end.
