unit FilterModule;

{$include RELEASE_SETTINGS.INC}
{$include VSTPLUGIN_SWITCHES.INC}

interface

uses
  Interfaces,
  Classes, Forms,
  FilterGUI,
  DAV_Types, DAV_VSTBasicModule, DAV_VSTModule;


type
  { TVSTFilter }
  TVSTFilter = class(TVSTModule)
    procedure DataModuleEditOpen( Sender: TObject; var GUI: TForm;
                                  ParentWindow: THandle);
    procedure DataModuleInitialize(Sender: TObject);
    procedure DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                 const SampleFrames: Cardinal);
    procedure DataModuleProcess64Replacing( const Inputs, Outputs: TDAVArrayOfDoubleFixedArray;
                                            const SampleFrames: Cardinal);
    procedure VSTFilterParameterProperties0ParameterChange( Sender: TObject;
                                                            const Index: Integer;
                                                            var Value: Single);
  private
    { Private declarations }
    fOld             : array [0..1] of array[0..1] of Double;
  public
    { Public declarations }
    fCutOffFrequency : Single;
  end;


// from unit DDSPBase:
function f_Limit( v:Single; l:Single=-1; u:Single=1):Single; overload;
function f_Limit( v:Double; l:Double=-1; u:Double=1):Double; overload;


implementation

  {$R *.lfm}

const kDenorm = 1E-20;


{ ***** Math ***************************************************************** }
  // from unit DDSPBase:

// Limit a value to be l<=v<=u
function f_Limit( v:Single; l:Single=-1; u:Single=1):Single; overload;
begin
  if v < l
    then Result:= l
    else if v > u then Result:= u else Result:= v;
end;

// Limit a value to be l<=v<=u
function f_Limit( v:Double; l:Double=-1; u:Double=1):Double; overload;
begin
  if v < l
    then Result:= l
    else if v > u then Result:= u else Result:= v;
end;


procedure TVSTFilter.DataModuleEditOpen(Sender: TObject; var GUI: TForm;
  ParentWindow: THandle);
begin
  // Do not delete this if you are using the editor
  GUI:= TVSTGUI.Create( nil);
  (GUI As TVSTGUI).theModule:= Self;
end;


////////////////////////////////////////////////////////////////////////////////
// OnInitialize
////////////////////////////////////////////////////////////////////////////////
procedure TVSTFilter.DataModuleInitialize(Sender: TObject);
begin
  fCutOffFrequency := 0.5;
  Parameter[0]     := 1000;
  Parameter[1]     := 1;
end;


////////////////////////////////////////////////////////////////////////////////
// Parameter 0 Changed (Cutoff Frequency)
////////////////////////////////////////////////////////////////////////////////
procedure TVSTFilter.VSTFilterParameterProperties0ParameterChange( Sender: TObject;
                                                                   const Index: Integer;
                                                                   var Value: Single);
begin
  fCutOffFrequency:= Parameter[0] * 0.0001;
end;


////////////////////////////////////////////////////////////////////////////////
// 32 Bit Processing
////////////////////////////////////////////////////////////////////////////////
procedure TVSTFilter.DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                        const SampleFrames: Cardinal);
var
  i   : integer;
  cut : single;
  res : single;
  fb  : single;
begin
  cut := fCutOffFrequency;
  res := 0.1 * Parameter[1];
  fb  := res +(res / (1 - cut * 0.9));

  for i := 0 to sampleFrames - 1 do
  begin
   fOld[0,0] := fOld[0,0] + cut * (inputs[0,i] - fOld[0,0] + fb * (fOld[0,0] - fOld[1,0])) + kDenorm;
   fOld[0,1] := fOld[0,1] + cut * (inputs[1,i] - fOld[0,1] + fb * (fOld[0,1] - fOld[1,1])) + kDenorm;
   fOld[1,0] := fOld[1,0] + cut * (fOld[0,0] - fOld[1,0]);
   fOld[1,1] := fOld[1,1] + cut * (fOld[0,1] - fOld[1,1]);
   outputs[0]^[i] := f_limit( fOld[1,0]);
   outputs[1]^[i] := f_limit( fOld[1,1]);
  end;
end;


////////////////////////////////////////////////////////////////////////////////
// 64 Bit Processing
////////////////////////////////////////////////////////////////////////////////
procedure TVSTFilter.DataModuleProcess64Replacing( const Inputs, Outputs: TDAVArrayOfDoubleFixedArray;
                                                   const SampleFrames: Cardinal);
var
  i   : integer;
  cut : Double;
  res : Double;
  fb  : Double;
begin
  cut := Parameter[0] * 0.8;
  res := Parameter[1];
  fb := res + res / (1 - cut * 0.9);
  for i := 0 to sampleFrames - 1 do
    begin
      fOld[0,0] := fOld[0,0] + cut * (inputs[0,i] - fOld[0,0] + fb * (fOld[0,0] - fOld[1,0])) + kDenorm;
      fOld[0,1] := fOld[0,1] + cut * (inputs[1,i] - fOld[0,1] + fb * (fOld[0,1] - fOld[1,1])) + kDenorm;
      fOld[1,0] := fOld[1,0] + cut * (fOld[0,0] - fOld[1,0]);
      fOld[1,1] := fOld[1,1] + cut * (fOld[0,1] - fOld[1,1]);
      outputs[0]^[i] := f_limit( fOld[1,0]);
      outputs[1]^[i] := f_limit( fOld[1,1]);
    end;
end;


end.
