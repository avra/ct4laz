library NoGUIFilter;   // because the original project had no GUI ..

{$include RELEASE_SETTINGS.INC}
{.$include DAV_Compiler.inc}
{$include VSTPLUGIN_SWITCHES.INC}

uses
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  {$ENDIF}
  Forms,
  DAV_VSTBasicModule, DAV_VSTModule, DAV_VSTCustomModule, DAV_VSTEffect,
  FilterModule;


function main( AudioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;
var
  VSTModule1 : TVSTFilter;
begin
  try
    VSTModule1              := TVSTFilter.Create(Application);
    VSTModule1.Effect^.user := VSTModule1;
    VSTModule1.AudioMaster  := AudioMaster;
    Result                  := VSTModule1.Effect;
  except
    Result := nil;
  end;
end;


exports
  Main name 'main',
  Main name 'VSTPluginMain';


begin
  {$IFDEF FPC}Application.Initialize;{$ENDIF}
end.
