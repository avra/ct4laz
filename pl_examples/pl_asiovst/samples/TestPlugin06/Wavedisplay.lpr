library Wavedisplay;

{$include RELEASE_SETTINGS.INC}
{$include DAV_Compiler.inc}
{$include VSTPLUGIN_SWITCHES.INC}


{ Avoid Exception:
  ----------------
unit DAV_GuiStaticWaveform, line 463:  catch exception in "MaxSample := FWaveData[Channel][i];"
}


uses
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  {$ENDIF}
  Forms,
  DAV_Types, DAV_Common, DAV_VSTModule, DAV_VSTCustomModule, DAV_VSTEffect,
  WavedisplayModule in 'WavedisplayModule.pas' {WavedisplayModule: TVSTModule},
  WavedisplayGUI in 'WavedisplayGUI.pas' {WavedisplayGUI};

  
function main(audioMaster: TAudioMasterCallbackFunc): PVSTEffect; cdecl; export;
var
  WavedisplayModule: TWavedisplayModule;
begin
  try
    WavedisplayModule := TWavedisplayModule.Create(Application);
    WavedisplayModule.Effect^.user := WavedisplayModule;
    WavedisplayModule.AudioMaster := audioMaster;
    Result := WavedisplayModule.Effect;
  except
    Result := nil;
  end;
end;


exports 
  Main name 'main',
  Main name 'VSTPluginMain';

  
begin
  {$IFDEF FPC}Application.Initialize;{$ENDIF}
end.
