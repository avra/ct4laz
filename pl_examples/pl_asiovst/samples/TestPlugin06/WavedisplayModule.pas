unit WavedisplayModule;

{$include RELEASE_SETTINGS.INC}
{$include DAV_Compiler.inc}
{$include VSTPLUGIN_SWITCHES.INC}

interface

uses 
  {$IFDEF FPC}
  Interfaces,
  {$ELSE}
  Windows,
  Messages,
  {$ENDIF}
  Classes, Forms, SysUtils,
  DAV_Types, DAV_VSTModule, DAV_VSTCustomModule;


type
  { TWavedisplayModule }
  TWavedisplayModule = class(TVSTModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleEditOpen(Sender: TObject; var GUI: TForm;
      ParentWindow: Cardinal);
    procedure DataModuleProcess(const Inputs,
      Outputs: TDAVArrayOfSingleFixedArray; const SampleFrames: Cardinal);
  private
    //
  public
    //
  end;


implementation

{$IFnDEF FPC}
  {$R *.dfm}
{$ELSE}
  {$R *.lfm}
{$ENDIF}

uses
  WavedisplayGUI;

procedure TWavedisplayModule.DataModuleCreate(Sender: TObject);
begin
  // Whyever, the Property Editor throws away the following assignments often ..
  // So I forced them manually here :
  OnProcess            := {@}DataModuleProcess;
  OnProcess32Replacing := {@}DataModuleProcess;
end;

procedure TWavedisplayModule.DataModuleEditOpen(Sender: TObject; var GUI: TForm;
  ParentWindow: Cardinal);
begin
  GUI:= TWavedisplayGUI.Create(Self);
end;

procedure TWavedisplayModule.DataModuleProcess( const Inputs, Outputs: TDAVArrayOfSingleFixedArray;
                                                const SampleFrames: Cardinal);
var i: Integer;
begin
  if Assigned(Editorform) then
    begin
      (EditorForm as TWavedisplayGUI).Display.ProcessBufferIndirect( TDAVArrayOfSingleDynArray(Inputs), 2, SampleFrames);
      //  Error: identifier idents no member "ProcessBufferIndirect":
      //(EditorForm as TWavedisplayGUI).LevelMeter.ProcessBufferIndirect( TDAVArrayOfSingleDynArray(Inputs), 2, SampleFrames);
    end;

  for i := 0 to 1
    do move( inputs[i,0], outputs[i,0], SampleFrames * sizeOf( Single));
end;


end.
