{
 CodeTyphon Project
 https://www.pilotlogic.com
 Demo2 for
 APE (Actionscript Physics Engine) 2D Physics Engine Library
}

unit apedemo1mw;


interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls,
  uApeEngine, uGroup, uRectangleParticle, uCircleParticle, uGDIRender,
  uComposite, uVector, uWheelParticle,
  uSpringConstraint, ucar;

type

  { TForm1 }

  TForm1 = class(TForm)
    btn1: TButton;
    Button1: TButton;
    Button2: TButton;
    btn2: TButton;
    btn3: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Timer1: TTimer;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    ape : ApeEngine;
    rR : GDIRenderer;
    g,gr : Group;
    Procedure BuildCapsule(gr : Group; x, y : Double);
    Procedure BuildBox(gr : Group; x, y, w, h : Double);
  end; 

var
  Form1: TForm1; 

implementation 

{$R *.lfm}

{ TForm1 }

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  ape.Step;
  Repaint;
end;

//==============================================================

procedure TForm1.FormCreate(Sender: TObject);

var r : REctangleParticle;
    c : CircleParticle;

begin
  ape := ApeEngine.Create;
  ape.Init(0.25);
  ape.AddForce(Vector.Create(0,9.4));

  rR := GDIRenderer.Create(Canvas);

  g:=Group.Create(True);
  gr:=Group.Create(true);

  //r:=RectangleParticle.Create(160,100,100,100,0,False,1,0.3,0.2);
  //r.SetRenderer(rR);
  //g.AddParticle(r);


  r:=RectangleParticle.Create(35,450,50,170,0,True,1,0,0);
  r.SetRenderer(rR);
  g.AddParticle(r);

  //Floor.
  r:=RectangleParticle.Create(-35,600,1000,40,0,True,1,0,0);
  r.SetRenderer(rR);
  g.AddParticle(r);

  r:=RectangleParticle.Create(700,500,500,40,-35,True,1,0,0);
  r.SetRenderer(rR);
  g.AddParticle(r);


  //c:=CircleParticle.Create(90,-10,22);
  //c.SetRenderer(rR);
  //g.AddParticle(c);
  //c:=CircleParticle.Create(83,-100,12,false,1,0.6,0.3);
  //c.SetRenderer(rR);
  //g.AddParticle(c);

  //BuildCapsule(gr,200,30);
  //BuildCapsule(gr,300,10);
  //BuildCapsule(gr,400,100);
  //BuildCapsule(gr,400,-300);


  ape.AddGroup(g);
  ape.AddGroup(gr);

  gr.AddCollidable(g);

  DoubleBuffered:=true;

end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  Canvas.Rectangle(0,0,Width,Height);
  ape.Paint;
end;



procedure TForm1.btn2Click(Sender: TObject);
var c : CircleParticle;
begin
  c:=CircleParticle.Create(150+Random(500),-10,10+Random(40));
  c.SetRenderer(rR);
  g.AddParticle(c);
end;

procedure TForm1.btn1Click(Sender: TObject);
var gg : Group;
    i : Integer;
begin
  gg:= Group.Create;
  BuildCapsule(gg,200,30);
  //new paral interact with all.
  for i:=0 to ape.Groups.Count-1 do
  begin
    gg.AddCollidable(ape.Groups[i]);
  end;
  ape.AddGroup(gg);
end;

procedure TForm1.btn3Click(Sender: TObject);
var c : group;
    i : integer;
begin
  c:= Group.Create;
  BuildBox(c,150+Random(500),-100,50,50);

  //new box interact with all.
  for i:=0 to ape.Groups.Count-1 do
  begin
    c.AddCollidable(ape.Groups[i]);
  end;
  ape.AddGroup(c);
end;

procedure TForm1.Button1Click(Sender: TObject);
var c : CircleParticle;
begin
  c:=CircleParticle.Create(200,-10,22,false);
  c.SetRenderer(rR);
  g.AddParticle(c);
end;

procedure TForm1.Button2Click(Sender: TObject);
var a : car;
    i : integer;
begin
  a:= Car.Create(rR,ape);
  //Car interract each other.
  for i:=0 to ape.Groups.Count-1 do
  begin
    a.AddCollidable(ape.Groups[i]);
  end;
  ape.AddGroup(a);

  a.Speed:=0.03;
end;




//================================================================


//================================================================

procedure TForm1.BuildCapsule(gr: Group; x, y: Double);
var capsuleP1, capsuleP2, capsuleP3 : CircleParticle;
    capsule : SpringConstraint;
    w : WheelParticle;
begin
  capsuleP1 := CircleParticle.Create(x,y,14,false,1.3,0.4);
  capsuleP1.SetRenderer(Rr);
	gr.addParticle(capsuleP1);

	capsuleP2 := CircleParticle.Create(x+100,y+25,14,false,1.3,0.4);
  capsuleP2.SetRenderer(Rr);
	gr.addParticle(capsuleP2);

	capsuleP3 := CircleParticle.Create(x+50,y-100,14,false,1.3,0.4);
  capsuleP3.SetRenderer(Rr);
	gr.addParticle(capsuleP3);

	capsule := SpringConstraint.Create(capsuleP1, capsuleP2, 1, true,24);
  capsule.SetRenderer(Rr);
	gr.AddConstraint(capsule);

	capsule := SpringConstraint.Create(capsuleP1, capsuleP3, 1, True,24);
  capsule.SetRenderer(Rr);
	gr.AddConstraint(capsule);
	capsule := SpringConstraint.Create(capsuleP2, capsuleP3, 1, True,24);
  capsule.SetRenderer(Rr);
	gr.AddConstraint(capsule);
end;

procedure TForm1.BuildBox(gr: Group; x, y, w, h: Double);
var c1,c2,c3,c4 : CircleParticle;
    s : SpringConstraint;
begin
  c1 := CircleParticle.Create(x,y,4);
  c1.SetRenderer(Rr);
	gr.addParticle(c1);
  c2 := CircleParticle.Create(x+w,y,4);
  c2.SetRenderer(Rr);
	gr.addParticle(c2);
  c3 := CircleParticle.Create(x+w,y+h,4);
  c3.SetRenderer(Rr);
	gr.addParticle(c3);
  c4 := CircleParticle.Create(x,y+h,4);
  c4.SetRenderer(Rr);
	gr.addParticle(c4);

	s:= SpringConstraint.Create(c1, c2, 1, True,4,1,True);
  s.SetRenderer(Rr);
	gr.AddConstraint(s);
	s:= SpringConstraint.Create(c2, c3, 1, True,4,1,True);
  s.SetRenderer(Rr);
	gr.AddConstraint(s);
	s:= SpringConstraint.Create(c3, c4, 1, True,4,1,True);
  s.SetRenderer(Rr);
	gr.AddConstraint(s);
	s:= SpringConstraint.Create(c4, c1, 1, True,4,1,True);
  s.SetRenderer(Rr);
	gr.AddConstraint(s);
	s:= SpringConstraint.Create(c1, c3, 1, True,4,1,True);
  s.SetRenderer(Rr);
	gr.AddConstraint(s);
	s:= SpringConstraint.Create(c4, c2, 1, True,4,1,True);
  s.SetRenderer(Rr);
	gr.AddConstraint(s);

end;

end.

