
{
 CodeTyphon Project
 https://www.pilotlogic.com
 Demo2 for
 APE (Actionscript Physics Engine) 2D Physics Engine Library
}

unit apedemo2mw;

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  uApeEngine, uVector, uGDIRender, ucar, uBridge, uSurfaces, uCapsule,
  uSwingDoor, uRotator, ExtCtrls, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormPaint(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    Ape : ApeEngine;
    R : GDIRenderer;
    aSurface : Surfaces;
    aBridge : Bridge;
    aCar : Car;
    aCapsule : Capsule;
    aSwingDoor : SwingDoor;
    aRotator : Rotator;
  end; 

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  Ape := ApeEngine.Create;
  R := GDIRenderer.Create(Form1.Canvas);
  // Initialize the engine. The argument here is the time step value.
	// Higher values scale the forces in the sim, making it appear to run
	// faster or slower. Lower values result in more accurate simulations.
  Ape.Init(1/4);

	// gravity -- particles of varying masses are affected the same
  Ape.AddMasslessForce(Vector.Create(0,3));

  aSurface := Surfaces.Create(R,Ape);
	Ape.addGroup(aSurface);
	aBridge := Bridge.Create(R,Ape);
	Ape.addGroup(aBridge);

	acapsule := Capsule.Create(R,Ape);
	APe.addGroup(acapsule);

	arotator := Rotator.Create(R,Ape);
	APE.addGroup(arotator);

	aswingDoor := SwingDoor.create(R,Ape);
	APE.addGroup(aswingDoor);

	aCar := Car.Create(R,Ape);
	Ape.addGroup(aCar);

  aCar.AddCollidable(aSurface);
  aCar.AddCollidable(aBridge);
  aCar.AddCollidable(aCapsule);
  aCar.AddCollidable(aSwingDoor);
  aCar.AddCollidable(aRotator);

  aCapsule.AddCollidable(aSurface);
  aCapsule.AddCollidable(aBridge);
  aCapsule.AddCollidable(aSwingDoor);
  aCapsule.AddCollidable(aRotator);

  DoubleBuffered := true;

end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: char);
begin
  if (Key = 'a') or (Key = 'A') then
    aCar.Speed:=aCar.Speed - 0.2;
  if (Key = 'd') or (Key = 'D') then
    aCar.Speed:=aCar.Speed + 0.2;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  aCar.speed := 0;
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  Ape.Paint;
end;

//=======================================================

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Ape.Step;
  aRotator.RotateByRadian(0.02);
  Repaint;
end;

end.

