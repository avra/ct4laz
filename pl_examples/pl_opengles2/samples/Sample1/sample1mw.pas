unit sample1mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls, 
  ctutils,
  ctGLES2, ctopengles2panel;

type

  { TForm1 }

  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private 
    fGLPanel: TOpenGLES2Panel;
  public
    procedure glPaint(Sender: TObject);
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}

{ TForm1 }

const VertexArray:array[0..11] of single=(0,-1,0,1,
                                          1,1,0,1,
                                          -1,1,0,1);

      ShaderPrecode='';

      VertexShaderSource:string=ShaderPrecode+'attribute vec4 position;'#10+
                                              'varying mediump vec2 pos;'#10+
                                              'void main(){'#10+
                                              ' gl_Position=position;'#10+
                                              ' pos=position.xy;'#10+
                                              '}';

      FragmentShaderSource:string=ShaderPrecode+'varying mediump vec2 pos;'#10+
                                                'uniform mediump float phase;'#10+
                                                'void main(){'#10+
                                                ' gl_FragColor=vec4(1.0,1.0,1.0,1.0)*sin(((pos.x*pos.x)+(pos.y*pos.y))*40.0+phase);'#10+
                                                '}';

var

 vertices: array[0..8] of glfixed;
 triangle: array[0..8] of glfloat;
 colors: array[0..11]  of glfloat;
 ui32Vbo: gluint = 0;
 CloseApp:boolean;
 Running:boolean;
 ReturnCode,CanvasWidth,CanvasHeight:integer;
 VertexShader,FragmentShader,ShaderProgram:TGLuint;
 PhaseLocation:TGLint;
 CurrentTime:int64;


 function GetTime:int64;
 var NowTime:TDateTime;
     Year,Month,Day,hour,min,sec,msec:word;
 begin
  NowTime:=Now;
  DecodeDate(NowTime,Year,Month,Day);
  DecodeTime(NowTime,hour,min,sec,msec);
  result:=(((((((((((Year*365)+Month)*31)+Day)*24)+hour)*60)+min)*60)+sec)*1000)+msec;
 end;

procedure PrintShaderInfoLog(Shader:TGLUint;ShaderType:string);
var len,Success:TGLint;
    Buffer:pchar;
begin
 glGetShaderiv(Shader,GL_COMPILE_STATUS,@Success);
 if Success<>GL_TRUE then begin
  glGetShaderiv(Shader,GL_INFO_LOG_LENGTH,@len);
  if len>0 then begin
   getmem(Buffer,len+1);
   glGetShaderInfoLog(Shader,len,nil,Buffer);
   writeln(ShaderType,': ',Buffer);
   freemem(Buffer);
   Running:=false;
   ReturnCode:=1;
  end;
 end;
end;

function CreateShader(ShaderType:TGLenum;Source:pchar):TGLuint;
begin
 result:=glCreateShader(ShaderType);
 glShaderSource(result,1,@Source,nil);
 glCompileShader(result);
 if ShaderType=GL_VERTEX_SHADER then begin
  PrintShaderInfoLog(result,'Vertex shader');
 end else begin
  PrintShaderInfoLog(result,'Fragment shader');
 end;
end;

procedure Init;
begin
 ShaderProgram:=glCreateProgram();

 VertexShader:=CreateShader(GL_VERTEX_SHADER,pchar(VertexShaderSource));
 FragmentShader:=CreateShader(GL_FRAGMENT_SHADER,pchar(FragmentShaderSource));

 glAttachShader(ShaderProgram,VertexShader);
 glAttachShader(ShaderProgram,FragmentShader);

 glLinkProgram(ShaderProgram);

 glUseProgram(ShaderProgram);

 PhaseLocation:=glGetUniformLocation(ShaderProgram,'phase');
 if PhaseLocation<0 then begin
  writeln('Error: Cannot get phase shader uniform location');
  Running:=false;
  ReturnCode:=1;
 end;
end;

procedure Done;
begin
 glDeleteProgram(ShaderProgram);
 glDeleteShader(FragmentShader);
 glDeleteShader(VertexShader);
end;

procedure Render;
begin
 glViewPort(0,0,CanvasWidth,CanvasHeight);
 glClearColor(0,1,0,1);
 glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

 glUniform1f(PhaseLocation,(GetTime mod 2000)*0.001*pi);

 glVertexAttribPointer(0,4,GL_FLOAT,false,0,@VertexArray);
 glEnableVertexAttribArray(0);
 glDrawArrays(GL_TRIANGLE_STRIP,0,3);

end;

//=================================================================

procedure TForm1.FormShow(Sender: TObject);
 var ss1,ss2:string;
begin

 CanvasWidth:=self.Width;
 CanvasHeight:=self.Height;
 Running:=true;
 ReturnCode:=0;


 ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
 ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLES2Panel.Create(self);
    fGLPanel.libEGL:=ss1;
    fGLPanel.libOpenGLES:=ss2;
  end;

  if Not fGLPanel.InitOpenGLES then exit;
//---------------------------------------------

  if Running then
  begin
  Init;
  CurrentTime:=GetTime;
 end;

end;

procedure TForm1.glPaint(Sender: TObject);
begin
   CurrentTime:=GetTime;
   Render;
   fGLPanel.SwapBuffers;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
 glPaint(nil);
end;


end.

