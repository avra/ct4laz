unit openclTestFPCmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ctypes, cl;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    ButProbe: TButton;
    ComboPlatform: TComboBox;
    ComboDevType: TComboBox;
    LabText2: TLabel;
    LabText1: TLabel;
    LabPlatform: TLabel;
    LabelDevType: TLabel;
    LabKernel: TLabel;
    LabResult: TLabel;
    LabTotal: TLabel;
    Memo1: TMemo;
    Panel1: TPanel;
    RBBillion: TRadioButton;
    RBTrillion: TRadioButton;
    procedure ButProbeClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
// CodeTyphon: Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}


type
  TPlatformNamesArr = array of string;


Procedure WriteLNX(Const aStr:String);
begin
    Form1.Memo1.Lines.Add(aStr);
end;

function getPlatformsStrArray(): TPlatformNamesArr;
var
  err: integer; // error code returned from api calls
  platformids: pcl_platform_id;
  platforms: cl_uint;
  i: integer;
  buf: array[0..99999] of char;
  bufwritten: csize_t;
begin

  writelnx(' ');
  writelnx('--- GetPlatformsStrArray ---------------------------------');
  
  err := clGetPlatformIDs(0, nil, @platforms);
  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Cannot get number of platforms!');
    exit;
  end;
  getmem(platformids, platforms * sizeof(cl_platform_id));
  err := clGetPlatformIDs(platforms, platformids, nil);
  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Cannot get platforms!');
    freemem(platformids);
    exit;
  end;

  SetLength(Result, platforms);
  writelnx(IntToStr(platforms)+ ' platform(s) found');
  for i := 0 to platforms - 1 do
  begin
    err := clGetPlatformInfo(platformids[i], CL_PLATFORM_NAME, sizeof(buf), @buf, bufwritten);
    Result[i] := buf;
  end;
  freemem(platformids);
end;

procedure printDevicesInfo();
const
  platform_str_info: array[1..5] of record
      id: dword;
      Name: PChar
    end
  =
    (
    (id: CL_PLATFORM_PROFILE; Name: 'PROFILE'),
    (id: CL_PLATFORM_VERSION; Name: 'VERSION'),
    (id: CL_PLATFORM_NAME; Name: 'NAME'),
    (id: CL_PLATFORM_VENDOR; Name: 'VENDOR'),
    (id: CL_PLATFORM_EXTENSIONS; Name: 'EXTENSIONS')
    );

  device_str_info: array[1..5] of record
      id: dword;
      Name: PChar
    end
  =
    (
    (id: CL_DEVICE_NAME; Name: 'DEVICE NAME'),
    (id: CL_DEVICE_VENDOR; Name: 'DEVICE VENDOR'),
    (id: CL_DEVICE_VERSION; Name: 'DEVICE VERSION'),
    (id: CL_DEVICE_PROFILE; Name: 'DEVICE PROFILE'),
    (id: CL_DEVICE_EXTENSIONS; Name: 'DEVICE EXTENSIONS')
    );

  device_word_info: array[1..8] of record
      id: dword;
      Name: PChar
    end
  =
    (
    (id: CL_DEVICE_TYPE_INFO; Name: 'DEVICE TYPE'),
    (id: CL_DEVICE_MAX_WORK_GROUP_SIZE; Name: 'DEVICE MAX WORK GROUP SIZE'),
    (id: CL_DEVICE_MAX_COMPUTE_UNITS; Name: 'DEVICE MAX COMPUTE UNITS'),
    (id: CL_DEVICE_IMAGE3D_MAX_WIDTH; Name: 'DEVICE IMAGE3D MAX WIDTH'),
    (id: CL_DEVICE_IMAGE3D_MAX_HEIGHT; Name: 'DEVICE IMAGE3D MAX HEIGHT'),
    (id: CL_DEVICE_GLOBAL_MEM_SIZE; Name: 'DEVICE GLOBAL MEM SIZE'),
    (id: CL_DEVICE_LOCAL_MEM_SIZE; Name: 'DEVICE LOCAL MEM SIZE'),
    (id: CL_DEVICE_COMPILER_AVAILABLE; Name: 'DEVICE COMPILER AVAILABLE')
    );

var
  err: integer; // error code returned from api calls
  platformids: pcl_platform_id;
  platforms: cl_uint;
  devices: cl_uint;
  deviceids: pcl_device_id;
  i, j, k: integer;
  buf: array[0..99999] of char;
  bufwritten: csize_t;
begin
  writelnx(' ');
  writelnx('--- PrintDevicesInfo ---------------------------------');
  
  err := clGetPlatformIDs(0, nil, @platforms);
  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Cannot get number of platforms!');
    exit;
  end;
  getmem(platformids, platforms * sizeof(cl_platform_id));
  err := clGetPlatformIDs(platforms, platformids, nil);
  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Cannot get platforms!');
    freemem(platformids);
    exit;
  end;
  writelnx(IntToStr(platforms)+ ' platform(s) found');
  for i := 0 to platforms - 1 do
  begin
    writelnx('Platform info: '+IntToStr(i)+ ' ----------------------------------------------');
    for k := low(device_str_info) to high(device_str_info) do
    begin
      err := clGetPlatformInfo(platformids[i], platform_str_info[k].id,
        sizeof(buf), @buf, bufwritten);
      writelnx(platform_str_info[k].Name+ ': ' +buf);
    end;

    err := clGetDeviceIDs(platformids[i], CL_DEVICE_TYPE_ALL, 0, nil, @devices);
    if (err <> CL_SUCCESS) then
    begin
      writelnx('ERROR: Cannot get number of devices for platform.');
      break;
    end;
    writelnx(IntToStr(devices)+ ' device(s) found');
    getmem(deviceids, devices * sizeof(cl_device_id));
    err := clGetDeviceIDs(platformids[i], CL_DEVICE_TYPE_ALL, devices, deviceids, nil);
    for j := 0 to devices - 1 do
    begin
      writelnx('Device info: '+ IntToStr(j)+ ' ------------');
      for k := low(device_str_info) to high(device_str_info) do
      begin
        err := clGetDeviceInfo(deviceids[j], device_str_info[k].id,
          sizeof(buf), @buf, bufwritten);
        writelnx(device_str_info[k].Name+ ': '+ buf);
      end;

      for k := low(device_word_info) to high(device_word_info) do
      begin
        err := clGetDeviceInfo(deviceids[j], device_word_info[k].id,
          sizeof(buf), @buf, bufwritten);
        writelnx(device_word_info[k].Name+ ': '+IntToStr( pdword(@buf)^));
      end;
    end;
  end;
  freemem(platformids);
end;

procedure testOpenCL(filename: string; selectedPlatformID: integer; gpu: cl_device_type);

const
  // Use a static data size for simplicity
  DATA_SIZE = 12800;

  compilerOptins: PChar = '-cl-opt-disable';

var
  KernelSource, errorlog: PChar;

  errorlogstr: string[255];
  loglen: csize_t;

type
  single2 = record
    x: single;
    y: single;
  end;

type
  TMyData = single2;

var
  err: integer; // error code returned from api calls
  Data: array [0..DATA_SIZE - 1] of TMyData; // original data set given to device
  results: array [0..DATA_SIZE - 1] of TMyData; // results returned from device
  correct: longword; // number of correct results returned

  global: csize_t; // global domain size for our calculation
  local: csize_t; // local domain size for our calculation

  device_id: cl_device_id;      // compute device id
  context: cl_context;        // compute context
  commands: cl_command_queue;  // compute command queue
  prog: cl_program;        // compute program
  kernel: cl_kernel;         // compute kernel

  input: cl_mem; // device memory used for the input array
  output: cl_mem; // device memory used for the output array

  i: integer;
  Count: integer;

  platformids: Pcl_platform_id;
  num_platforms: cl_uint;

  loadedOpenClCode: TStrings;

  startTime, finishTime, totalTimeSeconds: double;
begin
  // Fill our data set with random float values
  Count := DATA_SIZE;

  for i := 0 to Count - 1 do
    Data[i].x := random;

  loadedOpenClCode := TStringList.Create();
  loadedOpenClCode.LoadFromFile(filename);

  KernelSource := loadedOpenClCode.GetText;

  err := clGetPlatformIDs(0, nil, @num_platforms);
  writelnx('Number of platforms: '+ IntToStr(num_platforms));
  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Cannot get number of platforms!');
    exit;
  end;

  getmem(platformids, num_platforms * sizeof(cl_platform_id));

  err := clGetPlatformIDs(num_platforms, platformids, nil);

  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Failed to get platforms IDs');
    exit;
  end;

  device_id := nil;
  err := clGetDeviceIDs(platformids[selectedPlatformID], gpu, 1, @device_id, nil);
  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Failed to create a device group:'+IntToStr( err));
    exit;
  end
  else
    writelnx('clGetDeviceIDs OK!');

  // Create a compute context
  context := clCreateContext(nil, 1, @device_id, nil, nil, err);

  if context = nil then
  begin
    writelnx('Error: Failed to create a compute context:'+ IntToStr(err));
    exit;
  end
  else
    writelnx('clCreateContext OK!');

  // Create a command commands
  commands := clCreateCommandQueue(context, device_id, 0, err);
  if commands = nil then
  begin
    writelnx('Error: Failed to create a command commands:'+ IntToStr(err));
    exit;
  end
  else
    writelnx('clCreateCommandQueue OK!');

  // Create the compute program from the source buffer
  prog := clCreateProgramWithSource(context, 1, PPChar(@KernelSource), nil, err);
  if prog = nil then
  begin
    writelnx(KernelSource);
    writelnx('Error: Failed to create compute program:'+ IntToStr(err));
    exit;
  end
  else
    writelnx('clCreateProgramWithSource OK!');

  // Build the program executable
  err := clBuildProgram(prog, 0, nil, compilerOptins, nil, nil);

  if (err <> CL_SUCCESS) then
  begin
    errorlog := @errorlogstr[1];
    loglen := 255;
    clGetProgramBuildInfo(prog, device_id, CL_PROGRAM_BUILD_LOG, 255, errorlog, loglen);
    writelnx('Error: Failed to build program executable:'+ IntToStr(err));
    writelnx(errorlog);
    exit;
  end
  else
    writelnx('clBuildProgram OK!');

  // Create the compute kernel in the program we wish to run
  kernel := clCreateKernel(prog, 'minimize_error', err);
  if (kernel = nil) or (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Failed to create compute kernel!');
    exit;
  end
  else
    writelnx('clCreateKernel OK!');

  // Create the input and output arrays in device memory for our calculation
  input := clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(TMyData) *
    Count, nil, err);
  writelnx('clCreateBuffer - input buffer:'+ IntToStr(err)+ ' Size:'+ IntToStr(sizeof(TMyData) * Count)+ ' bytes');
  output := clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(TMyData) *
    Count, nil, err);
  writelnx('clCreateBuffer - output buffer:'+ IntToStr(err)+ ' Size:'+ IntToStr(sizeof(TMyData) * Count)+ ' bytes');
  if (input = nil) or (output = nil) then
  begin
    writelnx('Error: Failed to allocate device memory!');
    exit;
  end
  else
    writelnx('clCreateBuffer OK!');

  // Write our data set into the input array in device memory
  err := clEnqueueWriteBuffer(commands, input, CL_TRUE, 0, sizeof(TMyData) *
    Count, @Data, 0, nil, nil);

  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Failed to write to source array:'+IntToStr(err));
    exit;
  end
  else
    writelnx('clEnqueueWriteBuffer OK!');

  err := 0;
  err := clSetKernelArg(kernel, 0, sizeof(cl_mem), @input);
  
  err := err or clSetKernelArg(kernel, 1, sizeof(cl_mem), @output);
 
  err := err or clSetKernelArg(kernel, 2, sizeof(longword), @Count);
 

  if (err <> CL_SUCCESS) then
  begin
    writelnx('Error: Failed to set kernel arguments:'+ IntToStr(err));
    exit;
  end
  else
    writelnx('Kernel arguments OK!');

  startTime := now();

  global := Count;

  // runs the kernel 80 times
  for i := 1 to 80 do
  begin
    // Execute the kernel over the entire range of our 1d input data set
    // let the API to decide the work group size (local is nil)
    err := clEnqueueNDRangeKernel(commands, kernel, 1, nil, @global, nil, 0, nil, nil);
  end;

  if (err <> 0) then
  begin
    if (err = CL_INVALID_WORK_GROUP_SIZE) then
      writelnx('ERROR: Invalid work group size.');
    writelnx('Error: Failed to execute kernel. Error:'+ IntToStr(err));
  end
  else
    writelnx('clEnqueueNDRangeKernel OK! local: '+ IntToStr(local)+ ' global: '+ IntToStr(global)+ ' count:'+ IntToStr(Count));

  // Wait for the command commands to get serviced before reading back results
  err := clFinish(commands);

  if err = 0 then
    writelnx('clFinish OK!')
  else
    writelnx('Error at clFinish:'+ IntToStr(err));

  if err = CL_INVALID_COMMAND_QUEUE then
    writelnx('ERROR while running OpenCL code.');

  // Read back the results from the device to verify the output
  err := clEnqueueReadBuffer(commands, output, CL_TRUE, 0, sizeof(TMyData) *
    Count, @results, 0, nil, nil);
  if (err <> CL_SUCCESS) then
  begin
    if (err = CL_OUT_OF_RESOURCES) then
      writelnx('ERROR: Out of computing resources - probably out of memory.');
    writelnx('Error: Failed to read output array! '+ IntToStr(err));
  end
  else
  begin
    writelnx('clEnqueueReadBuffer OK!');
    finishTime := now();

    totalTimeSeconds := (finishTime - startTime) * 24 * 60 * 60;

    writelnx('Total run time:'+ FloatToStr(totalTimeSeconds)+ ' seconds.');
    Form1.LabResult.Caption := FloatToStr(round(totalTimeSeconds * 100) / 100) + ' seconds.';

    // Validate our results
    correct := 0;
    for i := 0 to 29 do
    begin
      writelnx('Result at pos i:'+ IntToStr(i)+ ' Value: '+ FloatToStr(results[i].x)+ ' Error:'+ FloatToStr(results[i].y));
    end;
  end;

  // Shutdown and cleanup
  clReleaseMemObject(input);
  clReleaseMemObject(output);
  clReleaseProgram(prog);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(commands);
  clReleaseContext(context);
end;

//=================== TForm1 =========================================

procedure TForm1.Button1Click(Sender: TObject);
var
  DeviceType: cl_device_type;
begin
  writelnx(' ');
  writelnx('--- Start Test ---------------------------------');
  
  Button1.Enabled := false;
  LabResult.Caption := 'RUNNING - Please Wait';
  Application.ProcessMessages();
  if ComboDevType.ItemIndex < 4 then
    DeviceType := 1 shl ComboDevType.ItemIndex
  else
    DeviceType := CL_DEVICE_TYPE_ALL;

  if RBBillion.Checked then
    testOpenCL(pathMedia+'test_billion.cl', ComboPlatform.ItemIndex, DeviceType)
  else
    testOpenCL(pathMedia+'test_trillion.cl', ComboPlatform.ItemIndex, DeviceType);

  Button1.Enabled := true;
  
  writelnx('--- Finish Test --------------------------------');
end;

procedure TForm1.ButProbeClick(Sender: TObject);
var
  Platforms: TPlatformNamesArr;
  i: integer;
begin    
  writelnx(' ');
  writelnx('--- Start Get OpenCL Informations ---------------');
  printDevicesInfo();
  ComboPlatform.Items.Clear();
  Platforms := getPlatformsStrArray();
  if Length(Platforms) > 0 then
  begin
    for i := low(Platforms) to high(Platforms) do
    begin
      ComboPlatform.Items.Add(Platforms[i]);
    end;
    ComboPlatform.ItemIndex := 0;

    ComboPlatform.Enabled := True;
    ComboDevType.Enabled := True;
    Button1.Enabled := True;
  end;    
  writelnx('--- Finish Get OpenCL Informations ---------------');
end;


end.
