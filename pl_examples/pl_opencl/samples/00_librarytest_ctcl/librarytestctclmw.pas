unit librarytestctclmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ctutils,
  ExtCtrls, ComCtrls,ctcl, ctcl_rpt;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    PageControl1: TPageControl;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure Button1Click(Sender: TObject);
  private
    procedure TestLibFunctions;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

Function PathLibrary:string;
begin
{$IFDEF Windows}
  Result := ctGetRuntimesCPUOSDir(true);
{$ELSE}
  Result := '';
{$ENDIF}
end;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

 Memo1.Clear;
 Memo2.Clear;

  if Not OpenCL_Initialize then
  begin
   Memo1.Lines.add('ERROR: '+LIBNAME_OPENCL+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(LIBNAME_OPENCL+' Loaded OK');
  end;

  OpenCL_InitializeAdvance;
  TestLibFunctions;

  //...........................

  Label1.Caption:='"BASE" OpenCL Ver: '+OpenCLLib_VersionStr;
end;

procedure TForm1.TestLibFunctions;
begin
 Memo1.Lines.add('--------- Commands -----------');
 OpenCL_WriteCommandsReport(Memo1.Lines,RadioGroup1.ItemIndex);

 Memo2.Lines.add('--------- Extensions ---------');
 OpenCL_WriteExtensionsReport(Memo2.Lines,RadioGroup1.ItemIndex);
end;

end.

