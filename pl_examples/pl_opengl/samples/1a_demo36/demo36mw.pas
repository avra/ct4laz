{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo36mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


const

   // Image Constants
  IMAGE_WIDTH = 64;
  IMAGE_HEIGHT = 64;


  // Lighting and material

  mat_diffuse : array [0..3] of GLfloat = ( 0.7, 0.7, 0.7, 1.0 );
  mat_specular : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
  mat_shininess : GLfloat = 100.0 ;

var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

 SphereQuadric : PGLUquadricObj;     // Quadric for our sphere

 // Control Points for Nurbs Surface
  ctlpoints : array [0..3] of array [0..3] of array[0..2] of GLfloat;

  // Nurbs
  theNurb : PGLUnurbsObj;
  knots : array [0..7] of GLfloat= (0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0);
  // counter clockwise
  edgePt : array [0..4] of array [0..1] of GLfloat = (
        (0.0, 0.0), (1.0, 0.0),
        (1.0, 1.0), (0.0, 1.0),
        (0.0, 0.0)
        );
  // clockwise
  curvePt : array [0..3] of array [0..1] of GLfloat = (
        (0.25, 0.5), (0.25, 0.75), (0.75, 0.75), (0.75, 0.5)
        );
  curveKnots : array [0..7] of GLfloat= (0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0);
  // clockwise
  pwlPt : array [0..2] of array [0..1] of GLfloat = (
        (0.75, 0.5), (0.5, 0.25), (0.25, 0.5)
        );

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


procedure InitSurface();
var u, v : GLint;
begin
    for u := 0 to 3 do
    begin
        for v := 0 to 3 do
        begin
            ctlpoints[u][v][0] := 2.0*(u - 1.5);
            ctlpoints[u][v][1] := 2.0*(v - 1.5);

          if ( ((u = 1) or (u = 2)) and ((v = 1) or (v = 2))) then
              ctlpoints[u][v][2] := 3.0
          else
              ctlpoints[u][v][2] := -3.0;
       end;
    end;
end;



{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin


    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
    glPushMatrix();
        glRotatef(330.0, 1.0,0.0,0.0);
        glScalef (0.5, 0.5, 0.5);

        gluBeginSurface(theNurb);
          gluNurbsSurface(theNurb,
              8, @knots,
              8, @knots,
              4 * 3,
              3,
              @ctlpoints[0][0][0],
              4, 4,
              GL_MAP2_VERTEX_3);
          gluBeginTrim (theNurb);
              gluPwlCurve(theNurb, 5, @edgePt[0][0], 2,
                      GLU_MAP1_TRIM_2);
          gluEndTrim (theNurb);
          gluBeginTrim (theNurb);
              gluNurbsCurve(theNurb, 8, @curveKnots, 2,
                      @curvePt[0][0], 4, GLU_MAP1_TRIM_2);
              gluPwlCurve (theNurb, 3, @pwlPt[0][0], 2,
                      GLU_MAP1_TRIM_2);
          gluEndTrim (theNurb);
        gluEndSurface(theNurb);

    glPopMatrix();


  glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;

{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;

  //========================================

  glClearColor (0.0, 0.0, 0.0, 1.0);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, @mat_diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR, @mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, @mat_shininess);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_AUTO_NORMAL);
  glEnable(GL_NORMALIZE);
  InitSurface();

  theNurb := gluNewNurbsRenderer();
  gluNurbsProperty(theNurb, GLU_SAMPLING_TOLERANCE, 25.0);
  gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, GLU_FILL);

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

    glViewport(0, 0, Width, Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective (45.0, Width/Height, 3.0, 8.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef (0.0, 0.0, -5.0);

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

