{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit pickingmw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
    procedure DoOnMouseClick(X, Y : Integer);

    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OpenGLPanel1OnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  end;

var
  Form1: TForm1; 

implementation 

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

type TCoord = Record
       X, Y, Z : glFLoat;
     end;
     TClr = Record
       R, G, B : glFloat;
     end;

var
  ElapsedTime, xdelta,xspeed : longint;

  // Textures
  ResetButtonTex : glUint;

  // User vaiables
  Sphere : PGLUquadric;
  SpherePos : Array[1..20] of TCoord;     // Each sphere coordinate
  SphereClr : Array[1..20] of TClr;       // each sphere color
  SphereActive : Array[1..20] of Boolean; // stores if sphere is active
  Selection : Boolean;
  SelectedSphere : Integer;


  Procedure drawBackgroundEffect;
  const P180 = Pi/180;
  Var S: single;
      I: integer;
      R,G,B: single;
  begin
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLE_STRIP);
      For I := 0 to 72 do
      begin
        S := 0.5+Sin(ElapsedTime/465)*0.2+Cos(ElapsedTime/657)*0.1;
        R := 0.5+Sin(Elapsedtime/100+I/1.6)*0.5;
        G := 0.5+Sin(Elapsedtime/200+720+I/1.6)*0.5;
        B := 0.5+Sin(Elapsedtime/100+720+I/1.6)*0.5;
        glColor3f(R,G,B);
        glVertex3f(Sin(P180*I*5)*S, Cos(P180*I*5)*S, Sin(P180*(I*10+Elapsedtime/500))*(0.5+sin(ElapsedTime/300)*0.3));
        glColor3f(B,G,R);
        glVertex3f(Sin(P180*I*5)*S*2, Cos(P180*I*5)*S*2, 1+Sin(P180*(I*(20)+ElapsedTime/500))*(0.5+sin(ElapsedTime/800)*0.4+cos(Elapsedtime/450)*0.4));
      end;
    glEnd;
  end;


  {------------------------------------------------------------------}
  {  Function to draw the actual scene                               }
  {------------------------------------------------------------------}
procedure glDraw();
 var I : Integer;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                       // Reset The View

  glTranslatef(0.0,0.0,-18);

  // Draw the Reset BUtton
  glPushMatrix();
    if Selection then
      glLoadName(99);
    glTranslatef(10, -7.6, -4);
    glEnable(GL_TEXTURE_2D);
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.3); glVertex3f(-1.0, -0.4,  1.0);
      glTexCoord2f(1.0, 0.3); glVertex3f( 1.0, -0.4,  1.0);
      glTexCoord2f(1.0, 0.7); glVertex3f( 1.0,  0.4,  1.0);
      glTexCoord2f(0.0, 0.7); glVertex3f(-1.0,  0.4,  1.0);
    glEnd;
  glPopMatrix();

  // Rotate the scene
  glRotatef(ElapsedTime/50, 1, 0, 0);
  glRotatef(ElapsedTime/40, 0, 1, 0);

  // Draw the spheres
  glDisable(GL_TEXTURE_2D);
  for I :=1 to 20 do
  begin
    if SphereActive[I] then
    begin
      if Selection then
        glLoadName(I);
      glPushMatrix();
      glColor3f(SphereClr[I].R, SphereClr[I].G, SphereClr[I].B);
      glTranslatef(SpherePos[I].X, SpherePos[I].Y, SpherePos[I].Z);
      gluSphere(Sphere,0.3,12,12);
      glPopMatrix();
    end;
  end;
end;


  {------------------------------------------------------------------}
  {  Initialise OpenGL                                               }
  {------------------------------------------------------------------}
procedure glInit();
 var I : Integer;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  glEnable(GL_TEXTURE_2D);
  LoadTexture(pathMedia+'button.bmp', ResetButtonTex,false);
  glBindTexture(GL_TEXTURE_2D, ResetButtonTex);

  Sphere := gluNewQuadric();
  gluQuadricNormals(Sphere, GLU_SMOOTH);   // Create Smooth Normals

  Selection :=FALSE;
  Randomize;
  for I :=1 to 20 do
  begin
    SpherePos[I].X :=Random(100)/10 -5;    // Range = -5.0 to 5.0
    SpherePos[I].Y :=Random(100)/10 -5;
    SpherePos[I].Z :=Random(100)/10 -5;
    SphereClr[I].R :=Random(70)/100 +0.3;   // Range = 0.3 to 1.0
    SphereClr[I].G :=Random(70)/100 +0.3;
    SphereClr[I].B :=Random(70)/100 +0.3;
    SphereActive[I] :=TRUE;
  end;


  xspeed:=10;
  xdelta:=1;
end;

procedure TForm1.DoOnMouseClick(X, Y : Integer);
  var selectBuff : Array[0..23] of glUint;
      viewport : TVector4i;
      hits, SelectedName : glUint;
      I : Integer;
begin
  // Select buffer parameters
  glGetIntegerv(GL_VIEWPORT, @viewport);   // Viewport = [0, 0, width, height]
  glSelectBuffer(23, @selectBuff);

  // Enter to selection mode
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
    glRenderMode(GL_SELECT);
    Selection :=TRUE;

    // Clear Select Buffer;
    glInitNames();
    glPushName(0);
    glLoadIdentity();

    // setup a viewing volume.  (x, y, width, height, viewport)
    // NOTE : y has -27 to account to caption bar
    gluPickMatrix(x, viewport[3]-y-27, 1, 1, viewport);      // Set-up pick matrix
    gluPerspective(45.0, viewport[2]/viewport[3], 0.0, 100.0);  // Do the perspective calculations. Last value = max clipping depth
    glMatrixMode(GL_MODELVIEW);

    // Render all scene and fill selection buffer
    glDraw();

    // Get hits and go back to normal rendering
    hits := glRenderMode(GL_RENDER);

    // Get the element in the selection buffer if there was a hit.
    // SelectionBuffer[0] = Number of elements that got hit (ie. no elements under cursor)
    // SelectionBuffer[1] and [2] = minZ and MaxZ
    // SelectionBuffer[3..n] = number of elements that got hit.
    if (hits > 0) then
    begin
      SelectedName := selectBuff[3];
      if SelectedName = 99 then
        for I :=1 to 20 do
          SphereActive[I] :=TRUE
      else
        SphereActive[SelectedName] :=FALSE;
    end;

    glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  Selection :=FALSE;
  glMatrixMode(GL_MODELVIEW);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;

    OnResize   := OpenGLPanel1Resize;
    OnMouseDown:= OpenGLPanel1OnMouseDown;

    AutoResizeViewport:=true;
  end;
end;

procedure TForm1.OpenGLPanel1OnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin  
  DoOnMouseClick(X,Y-ToolBar1.Height);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  plTimer1.Enabled:=True;    
  glInit;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if OpenGLPanel1.Height <= 0 then OpenGLPanel1.Height:=1;

  glViewport(0, 0, OpenGLPanel1.Width, OpenGLPanel1.Height);    // Set the viewport for the OpenGL window
  glMatrixMode(GL_PROJECTION);                                  // Change Matrix Mode to Projection
  glLoadIdentity();                                             // Reset View
  gluPerspective(45.0, OpenGLPanel1.Width/OpenGLPanel1.Height, 1.0, 100.0);  // Do the perspective calculations. Last value = max clipping depth

  glMatrixMode(GL_MODELVIEW);                                  // Return to the modelview matrix
  glLoadIdentity();                                            // Reset View
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  xspeed:=vcSpeed.Position;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  if ElapsedTime<10 then xdelta:=1;
  if ElapsedTime>10000000 then xdelta:=-1;

   ElapsedTime:=ElapsedTime+xdelta*xspeed;

  //..........................
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

end.

