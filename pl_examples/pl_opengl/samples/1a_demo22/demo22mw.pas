{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo22mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glClear (GL_COLOR_BUFFER_BIT);    // Clear the colour and depth buffer
  glLoadIdentity();                 // Load a "clean" model matrix on the stack

  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Set The polygon mode to use lines to define the front and back of the polygon
                                             // If you arent going to draw primitives any other way, it is a good idea to add this in the glInit procedure
                                             // instead of making recursive calls to set the state to the same thing
  glBegin(GL_POLYGON);              // Start drawing the Polygon
      glEdgeFlag(TRUE);          // Set Edgeflag to true, so we get a line from this vertex to the next
      glVertex3f(-12.0,-12.0,0.0);  // V0
      glEdgeFlag(FALSE);         // Set Edgeflag to FALSE, so we no not get a line from this vertex to the next
      glVertex3f(12.0,-12.0,0.0);   // V1
      glEdgeFlag(TRUE);          // Set Edgeflag to true, so we get a line from this vertex to the next
      glVertex3f(0.0,12.0,0.0);     // V2
  glEnd();
  // Flush the OpenGL Buffer
  glFlush();                        // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================


  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glColor3f(1.0, 1.0, 1.0);         // Set the Polygon colour to white

  glShadeModel(GL_FLAT);            // Use flat shading

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  // This is one way to set the perspective when a window get resized.
  // But this is not the only way. Another way will be handled in a later tutorial

  glViewport(0, 0, Width, Height);                    // Set the viewport for the OpenGL window
  glMatrixMode(GL_PROJECTION);                        // Change Matrix Mode to Projection
  glLoadIdentity();                                   // Reset View ( Load Thye default projection matrix onto the stack)
  if (Width <= Height) then
      glOrtho (-50.0, 50.0, -50.0*Height/Width,
          50.0*Height/Width, -1.0, 1.0)               // Set up orthographic view
  else
      glOrtho (-50.0*Width/Height,
          50.0*Width/Height, -50.0, 50.0, -1.0, 1.0); // Set up orthographic view
  glMatrixMode(GL_MODELVIEW);                         // Return to the modelview matrix
  glLoadIdentity();                                  // Reset View ( Load the default Modelview matrix onto the stack)

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

