{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo4mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

 var
  rtri: GLfloat = 0;
  rquad: GLfloat = 0;

procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, double(width) / height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glTranslatef(-1.5,0.0,-6.0);                         
  glRotatef(rtri,0.0,1.0,0.0);                         
  glBegin(GL_TRIANGLES);                               
    glColor3f(1.0,0.0,0.0);                            
    glVertex3f( 0.0, 1.0, 0.0);                       
    glColor3f(0.0,1.0,0.0);                           
    glVertex3f(-1.0,-1.0, 0.0);                        
    glColor3f(0.0,0.0,1.0);                            
    glVertex3f( 1.0,-1.0, 0.0);                        
  glEnd();                                              
  glLoadIdentity();                                    
  glTranslatef(1.5,0.0,-6.0);                         
  glRotatef(rquad,1.0,0.0,0.0);                        
  glColor3f(0.5,0.5,1.0);                              
  glBegin(GL_QUADS);                                   
    glVertex3f(-1.0, 1.0, 0.0);                        
    glVertex3f( 1.0, 1.0, 0.0);                        
    glVertex3f( 1.0,-1.0, 0.0);                         
    glVertex3f(-1.0,-1.0, 0.0);                        
  glEnd();                                             
  rtri := rtri + 0.9;                                  
  rquad := rquad - 0.9;
  //==========================================================================================
  //OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=@OpenGLPanel1Paint;
    OnResize:=@OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(@OnAppIdle);
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
  OpenGLPanel1.SwapBuffers;
end;

end.

