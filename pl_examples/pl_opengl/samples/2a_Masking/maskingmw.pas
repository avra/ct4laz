{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit maskingmw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, OpenGL_Textures;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

Const MAX_SEGMENTS = 64;
      MAX_DIVS = 16;

var
  ElapsedTime, xdelta,xspeed : integer;             // Elapsed time between frames
  // Textures
  rock, mask: GluInt;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var I,J : Integer;
    Angle : glFloat;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer

  glLoadIdentity();                                       // Reset The View

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  // Hier kommt der OpenGL Code rein

  glLoadIdentity;
  glTranslatef(0,0,-5);

  //Blending aktivieren, Hintergrund belassen zum Pfaden Alpha-Wert nehmen
  glDisable(GL_BLEND);

  glColor3f(1,1,1);
  glBindTexture(GL_TEXTURE_2D,rock);
  glBegin(gl_Quads);
      glTexCoord2f(0,0);
      glVertex3f(-1,-1,0);
      glTexCoord2f(1,0);
      glVertex3f(1,-1,0);
      glTexCoord2f(1,1);
      glvertex3f(1,1,0);
      glTexCoord2f(0,1);
      glvertex3f(-1,1,0);
  glEnd;

  glEnable(GL_BLEND);
  glBlendFunc(GL_ZERO,GL_SRC_COLOR);
  glBindTexture(GL_TEXTURE_2D,mask);
  glBegin(gl_Quads);
    glTexCoord2f(0,0);
    glVertex3f(-1,-1,0);
    glTexCoord2f(1,0);
    glVertex3f(1,-1,0);
    glTexCoord2f(1,1);
    glvertex3f(1,1,0);
    glTexCoord2f(0,1);
    glvertex3f(-1,1,0);
  glEnd;

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
Var I : Integer;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations
  //...................................................

  glEnable(GL_TEXTURE_2D);
  glShadeModel(GL_SMOOTH);
  glClearColor(0.0, 0.0, 0.0, 0.5);
  glClearDepth(1.0);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  LoadTexture(pathMedia+'rock.jpg',rock, false);
  LoadTexture(pathMedia+'ct.jpg',mask, false);
  //.................................................

  xspeed:=10;
  xdelta:=1;
end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end; 

  Application.AddOnIdleHandler(OnAppIdle);

end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;  

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
  OpenGLPanel1.SwapBuffers;
end;

end.

