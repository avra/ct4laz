{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit winguimw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit,ssgui;

type

  { TForm1 }

  TForm1 = class(TForm)
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;     
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OpenGLPanel1OnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure OpenGLPanel1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure OpenGLPanel1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  end;

var
  Form1: TForm1; 

implementation 

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}


var
  ElapsedTime, xdelta,xspeed : longint;



  Procedure drawBackgroundEffect;
  const P180 = Pi/180;
  Var S: single;
      I: integer;
      R,G,B: single;
  begin
    glDisable(GL_TEXTURE_2D);
    glBegin(GL_TRIANGLE_STRIP);
      For I := 0 to 72 do
      begin
        S := 0.5+Sin(ElapsedTime/465)*0.2+Cos(ElapsedTime/657)*0.1;
        R := 0.5+Sin(Elapsedtime/100+I/1.6)*0.5;
        G := 0.5+Sin(Elapsedtime/200+720+I/1.6)*0.5;
        B := 0.5+Sin(Elapsedtime/100+720+I/1.6)*0.5;
        glColor3f(R,G,B);
        glVertex3f(Sin(P180*I*5)*S, Cos(P180*I*5)*S, Sin(P180*(I*10+Elapsedtime/500))*(0.5+sin(ElapsedTime/300)*0.3));
        glColor3f(B,G,R);
        glVertex3f(Sin(P180*I*5)*S*2, Cos(P180*I*5)*S*2, 1+Sin(P180*(I*(20)+ElapsedTime/500))*(0.5+sin(ElapsedTime/800)*0.4+cos(Elapsedtime/450)*0.4));
      end;
    glEnd;
  end;


  {------------------------------------------------------------------}
  {  Function to draw the actual scene                               }
  {------------------------------------------------------------------}
  procedure glDraw();
  begin
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
    glLoadIdentity();                                       // Reset The View

    glTranslatef(0, 0, -4);
    glRotatef(ElapsedTime/40, 1, 1, 1);
    drawBackgroundEffect;

    RenderGUI;
  end;


  {------------------------------------------------------------------}
  {  Initialise OpenGL                                               }
  {------------------------------------------------------------------}
  procedure glInit();
  begin
    glClearColor(0.0, 0.0, 0.0, 0.0); 	     // Black Background
    glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
    glClearDepth(1.0);                       // Depth Buffer Setup
    glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
    glDepthFunc(GL_LESS);		     // The Type Of Depth Test To Do

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

    glEnable(GL_TEXTURE_2D);                     // Enable Texture Mapping

    InitGUI;

  xspeed:=10;
  xdelta:=1;
end;

procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    Cursor:=crNone;

    OnPaint   := OpenGLPanel1Paint;
    OnResize  := OpenGLPanel1Resize;
    OnMouseDown:= OpenGLPanel1OnMouseDown;
    OnMouseUp  := OpenGLPanel1MouseUp;
    OnMouseMove:= OpenGLPanel1MouseMove;

    AutoResizeViewport:=true;
  end;
end;

procedure TForm1.OpenGLPanel1OnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin  
  ssgui.Mouse.Button :=1;
  ssgui.MouseDown;

end;   
procedure TForm1.OpenGLPanel1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin   
  ssgui.Mouse.Button :=0;
  ssgui.MouseUp;

end;
procedure TForm1.OpenGLPanel1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  ssgui.Mouse.X := X;
  ssgui.Mouse.Y := ssgui.Window.Height - Y {- ssgui.Window.CaptionBarHeight};
  if Mouse.Button = 1 then ssgui.MouseDown;
end;

procedure TForm1.FormShow(Sender: TObject);
begin            
  ssgui.Window.Width :=OpenGLPanel1.Width;
  ssgui.Window.Height:=OpenGLPanel1.Height;
  ssgui.Window.CaptionBarHeight:=26;

  plTimer1.Enabled:=True;    
  glInit;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if OpenGLPanel1.Height <= 0 then OpenGLPanel1.Height:=1;

  glViewport(0, 0, OpenGLPanel1.Width, OpenGLPanel1.Height);    // Set the viewport for the OpenGL window
  glMatrixMode(GL_PROJECTION);                                  // Change Matrix Mode to Projection
  glLoadIdentity();                                             // Reset View
  gluPerspective(45.0, OpenGLPanel1.Width/OpenGLPanel1.Height, 1.0, 100.0);  // Do the perspective calculations. Last value = max clipping depth

  glMatrixMode(GL_MODELVIEW);                                  // Return to the modelview matrix
  glLoadIdentity();                                            // Reset View
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  xspeed:=vcSpeed.Position;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  if ElapsedTime<10 then xdelta:=1;
  if ElapsedTime>10000000 then xdelta:=-1;

   ElapsedTime:=ElapsedTime+xdelta*xspeed;

  //..........................
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

end.

