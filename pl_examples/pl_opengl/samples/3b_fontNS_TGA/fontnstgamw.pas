{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

// Description : This project renders non standard "fonts" / text in OpenGL.

unit fontnstgamw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation    

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

var
  // Textures
  texFont : glUint;
  texBackground : glUint;

  // User vaiables
  xSpeed, ySpeed : glFloat;
  xAngle, yAngle : glFloat;
  g_TextScroller : GLfloat;
  ElapsedTime : Integer;             // Elapsed time between frames

{------------------------------------------------------------------}
{  Renders the text to a row of polygons and keeps the origin      }
{  in the centre of the string                                     }
{------------------------------------------------------------------}
procedure glImgWrite(strText : string);
var I, intAsciiCode : integer;
    imgcharWidth : GLfloat;
    imgcharPosX : GLfloat;
begin

  imgcharWidth := 1.0/66;
  strText := UpperCase(strText);

  for I := 1 to length(strText) do
  begin

    if ord(strText[I]) > 31 then //only handle 66 chars
    begin
      intAsciiCode := ord(strText[I]) - 32;
      imgcharPosX := length(strText)/2*0.08-length(strText)*0.08 + (i-1) * 0.08; // Find the character position from the origin [0.0 , 0.0 , 0.0]  to center the text
      glBegin(GL_QUADS);

        glTexCoord2f(imgcharWidth*intAsciiCode, 0.0);
        glVertex3f(-0.04+imgcharPosX, -0.04,  0.0);

        glTexCoord2f(imgcharWidth*intAsciiCode+imgcharWidth, 0.0);
        glVertex3f( 0.04+imgcharPosX, -0.04,  0.0);

        glTexCoord2f(imgcharWidth*intAsciiCode+imgcharWidth, 1.0);
        glVertex3f( 0.04+imgcharPosX,  0.04,  0.0);

        glTexCoord2f(imgcharWidth*intAsciiCode, 1.0);
        glVertex3f(-0.04+imgcharPosX,  0.04,  0.0);
      glEnd;
    end;
  end;
end;
{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                       // Reset The View

  // Draw Rotating Galaxy
  glPushMatrix;
  glTranslatef(0.0,0.0,-12);
    glBindTexture(GL_TEXTURE_2D, texBackground);  // Bind the Texture to the object
    glRotatef(ElapsedTime/100, 0, 0, 1);
    glColor3f(1.0,1.0,1.0);
    glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-7.6, -7.6, 0);
        glTexCoord2f(1.0, 0.0); glVertex3f( 7.6, -7.6, 0);
        glTexCoord2f(1.0, 1.0); glVertex3f( 7.6,  7.6, 0);
        glTexCoord2f(0.0, 1.0); glVertex3f(-7.6,  7.6, 0);
    glEnd();
  glPopMatrix;

  // Draw Text Strings
  glPushMatrix;
    glTranslatef(0.0,0.0+g_TextScroller,-3);

    glBindTexture(GL_TEXTURE_2D, texFont);  // Bind the Texture to the object
    // Line One
    glImgWrite('this is some test text.');

    // Line Two
    glTranslatef(0.0,-0.1,0.0);
    glImgWrite('here we have a second line of text.');

    // Rolling Text
    glTranslatef(0.0,-0.5,0.0);
    glRotatef(-(ElapsedTime/10), 1, 0, 0);
    glImgWrite('lets see of this text can rotate.');

    xAngle :=xAngle + xSpeed;
    yAngle :=yAngle + ySpeed;


  glPopMatrix;

  // Text Scrolling from the top, downwards
  g_TextScroller := g_TextScroller + 0.002;

  if g_TextScroller > 1.5 then
  begin
    g_TextScroller := -1.5;
  end;
  glPushMatrix;
    glTranslatef(0.0,2.0-g_TextScroller,-4.1);
    glImgWrite('text scrolling from the top.');
  glPopMatrix;

  // Growing Text
  glPushMatrix;
    glTranslatef(0.0,0.0,8.0 * g_TextScroller);
    glImgWrite('CodeTyphon');
  glPopMatrix;
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin
  //glClearColor(0.102, 0.102, 0.4, 0.0); 	   //  Background color
  glClearColor(0.0, 0.0, 0.0, 0.0);
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do
  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER, 0.4);

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  glEnable(GL_TEXTURE_2D);                     // Enable Texture Mapping

  LoadTexture(pathMedia+'fontlines2.tga', texFont, FALSE);    // Load the Texture from TGA File
 // LoadTexture(pathMedia+'fontlines2.gif', texFont, FALSE);    // Load the Texture from GIF File
 // LoadTexture(pathMedia+'fontlines2.png', texFont, FALSE);    // Load the Texture from PNG File

  LoadTexture(pathMedia+'galaxy.jpg', texBackground, FALSE);    // Load the Texture

  xSpeed :=0.1;   // start with some movement
  ySpeed :=0.1;
  g_TextScroller := -1.5;
end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
   plTimer1.Enabled:=True;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  yspeed:=vcSpeed.Position/100;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  //// ElapsedTime := round(deltaTime);
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

end.

