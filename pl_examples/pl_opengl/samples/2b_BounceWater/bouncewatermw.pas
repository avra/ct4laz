{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit BounceWatermw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,LCLIntf,
  OpenGLPanel, ctGL, ctGLU,
  OpenGL_Textures;

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    cube_rotationx: GLFloat;
    cube_rotationy: GLFloat;
    cube_rotationz: GLFloat;
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

const
   GridSize = 63;

type TGLCoord = Record
       X, Y, Z : glFloat;
     end;
     TBall = record
      BallY, Angle : glFloat;
      XSize, YSize : glFloat;
      AngleModifier : glFloat;
      RippleX,RippleY : integer;
     end;


var
  stafsLoaded :boolean = false;
  ElapsedTime,DemoStart, LastTime : GLfloat;
  // Textures
  FloorTex   : GLuint;
  BallTex    : GLuint;
  ReflectMirrorTex : GLuint;

  // User vaiables
  MyQuadratic : pgluQuadricObj;

  //ball variables
  Balls : Array [0..5] of TBall;

  //water variables
  sViscosity : glFloat;
  sPosition : Array[0..GridSize, 0..GridSize] of glFloat;
  sVelocity : Array[0..GridSize, 0..GridSize] of glFloat;

  Vertex : Array[0..GridSize, 0..GridSize] of TglCoord;
  Normals:array [0..GridSize, 0..GridSize] of TglCoord;

{------------------------------------------------------------------}
{  Function to convert int to string. (No sysutils = smaller EXE)  }
{------------------------------------------------------------------}
function IntToStr(Num : Integer) : String;  // using SysUtils increase file size by 100K
begin
  Str(Num, result);
end;

{------------------------------------------------------------------}
{  Function to create a ripple at the sPosition I,J                 }
{------------------------------------------------------------------}
procedure CreateRipple(I,J : integer);
begin
  sVelocity[I, J] :=1000;
end;

procedure DrawWater;
var I, J : Integer;
    VectLength : glFloat;
begin
  // Calculate new sVelocity
  For I :=2 to GridSize-2 do
    For J :=2 to GridSize-2 do
      sVelocity[I, J] := sVelocity[I, J] + (sPosition[I, J] -
              (4*(sPosition[I-1,J] + sPosition[I+1,J] + sPosition[I,J-1] + sPosition[I,J+1]) +  // left, right, above, below
              sPosition[I-1,J-1] + sPosition[I+1,J-1] + sPosition[I-1,J+1] + sPosition[I+1,J+1])/25) / 7;  // diagonally across

  // Calculate the new ripple sPositions
  For I:=2 to GridSize-2 do
    For J:=2 to GridSize-2 do
    Begin
      sPosition[I, J] := sPosition[I, J] - sVelocity[I,J];
      sVelocity[I, J] := sVelocity[I, J] * sViscosity;
    End;

  // Calculate the new vertex coordinates
  For I :=0 to GridSize do
    For J :=0 to GridSize do
    begin
      Vertex[I, J].X :=(I - GridSize/2)/GridSize*5;
      Vertex[I, J].Y :=(sPosition[I, J] / 1024)/GridSize*3;
      Vertex[I, J].Z :=(J - GridSize/2)/GridSize*5;
    end;

  // Calculate the new vertex normals.
  // Do this by using the points to each side to get the right angle
  For I :=0 to GridSize do
  begin
    For J :=0 to GridSize do
    begin
      If (I > 0) and (J > 0) and (I < GridSize) and (J < GridSize) then
      begin
        with Normals[I, J] do
        begin
          X := sPosition[I+1, J] - sPosition[I-1,J];
          Y := -2048;
          Z := sPosition[I, J+1] - sPosition[I, J-1];

          VectLength :=sqrt(x*x + y*y + z*z);
          if VectLength <> 0 then
          begin
            X :=X/VectLength;
            Y :=Y/VectLength;
            Z :=Z/VectLength;
          end;
        end;
      end
      else
      begin
        Normals[I, J].X :=0;
        Normals[I, J].Y :=1;
        Normals[I, J].Z :=0;
      end;
    end;
  end;

  // Draw the water texture
  glBindTexture(GL_TEXTURE_2D, FloorTex);

  For J :=0 to GridSize-1 do
  begin
    glColor3f(J/100,J/100,J/100);
    glBegin(GL_QUAD_STRIP);
      for I :=0 to GridSize do
      begin
        glNormal3fv(@Normals[I, J+1]);
//        glTexCoord2f(I/GridSize, (J+1)/GridSize);
        glVertex3fv(@Vertex[I, J+1]);
        glNormal3fv(@Normals[I, J]);
//        glTexCoord2f(I/GridSize, J/GridSize);
        glVertex3fv(@Vertex[I, J]);
      end;
    glEnd;
  end;
end;

{------------------------------------------------------------------}
{  Function to draw a ball                                         }
{------------------------------------------------------------------}
procedure DrawBall(Xsize, Ysize,xpos,zpos,BallY : GLfloat);
begin
  glPushMatrix();

  glFrontFace(GL_CCW);
  //-----  Draw the Main Ball  -----//
  // Draw the ball using standard textures
  glTranslatef(0.0, 0.0 , 0.0);

  glScalef(Xsize, Ysize, Xsize);     // squash the ball into shape


  glDisable(GL_BLEND);

  glTranslatef(xpos, 0.1+BallY , zpos);

  // Draw top sphere

  glColor3f(1.0,1.0,1.0);
  glBindTexture(GL_TEXTURE_2D, BallTex);
  gluSphere(MyQuadratic, 0.3, 32, 32);
  glPopMatrix();

  glPushMatrix();

    //-----  Draw the Reflection of the Ball  -----//
  // Draw the ball using standard textures

  glScalef(Xsize, Ysize, Xsize);     // squash the ball into shape
  glColor3f(0.4,0.4,0.4);
  glTranslatef(xpos, -0.1-BallY ,zpos);
  glRotatef(45,1.0,1.0,1.0);
  // Draw a sphere for the reflection
  glEnable(GL_BLEND);

  glDepthFunc(GL_LEQUAL);
  glBindTexture(GL_TEXTURE_2D, ReflectMirrorTex);
  gluSphere(MyQuadratic, 0.3, 24, 24);

  glEnable(GL_TEXTURE_GEN_S);     // Enable spherical
  glEnable(GL_TEXTURE_GEN_T);     // Environment Mapping

  glPopMatrix();
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw;
var I : integer;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                       // Reset The View

  //----- Set Ball sPositions -----//
  for I := 0 to high(Balls) do
  begin
    Balls[I].Angle :=ElapsedTime/60 * Balls[I].AngleModifier;
  end;


  glTranslatef(-0.25,0.0,-4.6);
  glRotatef(50, 1.0, 0.0, 0.0);

  //----- Calculate the ball sPositions -----//
  for I := 0 to high(Balls) do
  begin
     Balls[I].BallY :=abs(1.6*sin(Balls[I].Angle/24));
     if Balls[I].BallY < 0.11 then
     begin
        CreateRipple(Balls[I].RippleX,Balls[I].RippleY);
        Balls[I].YSize :=0.5/(1+(0.11-Balls[I].BallY));
     end
     else
     begin
        Balls[I].Ysize :=0.5;
     end;
     Balls[I].XSize :=0.5;
  end;

  //----- Draw the balls -----//
  DrawBall(Balls[0].Xsize*1.3, Balls[0].Ysize*1.3, 0.0, 0.0,Balls[0].BallY);
  DrawBall(Balls[1].Xsize    , Balls[1].Ysize    , 0.7, 0.7,Balls[1].BallY);
  DrawBall(Balls[2].Xsize*1.1, Balls[2].Ysize*1.1, 1.6,-0.7,Balls[2].BallY);
  DrawBall(Balls[3].Xsize*1.5, Balls[3].Ysize*1.5,-1.0,-1.0,Balls[3].BallY);
  DrawBall(Balls[4].Xsize*2  , Balls[4].Ysize*2  ,-1.0, 1.0,Balls[4].BallY);
  DrawBall(Balls[5].Xsize*1.9, Balls[5].Ysize*1.9, 1.7, 0.9,Balls[5].BallY);

  //-----  Draw the Floor  -----//
  glFrontFace(GL_CW);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glDepthFunc(GL_LESS);

  DrawWater;

  //-----  Draw pond walls  -----//
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
  glBegin(GL_QUADS);

    //left wall
    glColor3f(0.5608, 0.5725, 0.5804);glVertex3f(-2.5,0.0,-2.5);
    glColor3f(1.0,1.0,1.0);glVertex3f(-2.5,0.0,2.5);
    glColor3f(1.0,1.0,1.0);glVertex3f(-2.5,2.0,2.5);
    glColor3f(0.5608, 0.5725, 0.5804);glVertex3f(-2.5,2.0,-2.5);


    //right wall
    glColor3f(1.0,1.0,1.0);glVertex3f(2.5,0.0,2.5);
    glColor3f(0.5608, 0.5725, 0.5804);glVertex3f(2.5,0.0,-2.5);
    glColor3f(0.5608, 0.5725, 0.5804);glVertex3f(2.5,2.0,-2.5);
    glColor3f(1.0,1.0,1.0);glVertex3f(2.5,2.0,2.5);
  glEnd;
  glEnable(GL_TEXTURE_2D);
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit;
var I, J : Integer;
begin
  glClearColor(0.5608, 0.5725, 0.5804, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  //=======================================
  if stafsLoaded then exit;

  MyQuadratic := gluNewQuadric;	               // Create A Pointer To The Quadric Object (Return 0 If No Memory) (NEW)
  gluQuadricNormals(MyQuadratic, GLU_SMOOTH);	       // Create Smooth Normals (NEW)
  gluQuadricTexture(MyQuadratic, TRUE);	       // Create Texture Coords (NEW)

  glEnable(GL_CULL_FACE);
  glEnable(GL_TEXTURE_2D);          // Enable Texture Mapping
  glTexGenf(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
  glTexGenf(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);

  //Load textures
  LoadTexture(pathMedia+'floor.bmp', FloorTex,false);
  LoadTexture(pathMedia+'reflection.bmp', BallTex,false);
  LoadTexture(pathMedia+'reflectionmirror.bmp', ReflectMirrorTex,false);
  stafsLoaded:=true;
end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  LastTime :=ElapsedTime;
  ElapsedTime :=LCLIntf.GetTickCount64 - DemoStart;     // Calculate Elapsed Time
  ElapsedTime :=(LastTime + ElapsedTime)/2; // Average it out for smoother movement

  //=========================================================
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, width / height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //==========================================================
  glInit;
  glDraw;
  //==========================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);

end;

procedure TForm1.FormShow(Sender: TObject);
var I, J : Integer;
begin
  DemoStart := LCLIntf.GetTickCount64;
  //...................
  stafsLoaded:=false;
  sViscosity :=0.96;
  For I :=0 to GridSize do
  begin
    For J :=0 to GridSize do
    begin
      sPosition[I, J] :=0;
      sVelocity[I, J] :=0;
    end;
  end;

  Randomize;

  for I := 0 to high(Balls) do
  begin
    Balls[I].AngleModifier := random + 0.2;
  end;

  //init ripple sPositions
  Balls[0].RippleX := 32;
  Balls[0].RippleY := 31;
  Balls[1].RippleX := 36;
  Balls[1].RippleY := 37;
  Balls[2].RippleX := 42;
  Balls[2].RippleY := 27;
  Balls[3].RippleX := 23;
  Balls[3].RippleY := 24;
  Balls[4].RippleX := 20;
  Balls[4].RippleY := 45;
  Balls[5].RippleX := 51;
  Balls[5].RippleY := 44;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
  OpenGLPanel1.SwapBuffers;
end;

end.

