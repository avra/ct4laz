{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit fireworksmw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

Const

  EXPLOSION_SIZE = 0.8;


type
     TParticle = Record
       X, Y, Z : glFloat;            //  X, Y and Z coordinates
       dX, dY, dZ : glFloat;         // amount of change in X, Y and Z direction
       R, G, B : glFloat;            // color of the particle
     end;
     TFirework = Record
       Particle : Array[0..127] of TParticle;     // particles in the explosion
       Trail : Array[0..15] of TParticle;         // particles in the tail
       StartTime : Integer;                       // when the firework was set off
       Duration : Integer;                        // how long it should last
       Style : Integer;                           // picture it should create
       X, Y : glFloat;                            // X and Y coordinates
       dX, dY : glFloat;                          // change in X and Y coordinates
     end;

var
  ElapsedTime, xdelta,xspeed : longint;

  // Textures
  ParticleTex : glUint;

  // User vaiables
  Fireworks : Integer;               // number of active fireworks
  Firework : Array[0..9] of TFirework;



{------------------------------------------------------------------}
{  setups up the new firework variables                            }
{------------------------------------------------------------------}
procedure SetupFirework(N : Integer);
var I : Integer;
    Clr : Integer;
begin
  Randomize;

  // select a firework color. 0=red,  1=green,  2=blue
  Clr :=Random(3);

  Firework[N].StartTime :=ElapsedTime;
  FireWork[N].Duration :=random(1000)+3000;
  Firework[N].X :=random(20) - 10;
  Firework[N].Y :=-20;
  Firework[N].dX :=(random*2-1)/80;
  Firework[N].dY :=(random + 1.5)/80;
  Firework[N].Style :=Random(10);             // if style = 0, 1 then its a ring.  20% chance

  // exploding particles
  for I :=0 to 127 do
    with Firework[N].Particle[I] do
    begin
      if Firework[N].Style < 2 then
        R := (random/6 +0.4)/10*EXPLOSION_SIZE
      else
        R := (random/10 -0.05)*EXPLOSION_SIZE;
      dX :=R*cos(I/10);
      dY :=R*sin(I/10);
      dZ :=R*cos(I/4);
      X :=dX;
      Y :=dY;
      Z :=dZ;
      if Clr = 0 then R :=random/3 + 0.7 else R :=random/3 + 0.4;
      if Clr = 1 then G :=random/3 + 0.7 else G :=random/3 + 0.4;
      if Clr = 2 then B :=random/3 + 0.7 else B :=random/3 + 0.4;
    end;

  // tail particles
  for I :=0 to 15 do
    with Firework[N].Trail[I] do
    begin
      X :=0;
      Y :=0;
      Z :=50 + random(50);             // will act as duration for particle
      dX :=-0.7*Firework[N].dX;
      dY :=-random/20-0.001;
      if Clr = 0 then R :=random/3 + 0.8 else R :=random/3 + 0.4;
      if Clr = 1 then G :=random/3 + 0.8 else G :=random/3 + 0.4;
      if Clr = 2 then B :=random/3 + 0.8 else B :=random/3 + 0.4;
    end;
end;


{------------------------------------------------------------------}
{  draws the firework tail as it goes up                           }
{------------------------------------------------------------------}
procedure FireworkTail(const N : Integer);
var I : Integer;
begin
  glTranslatef(Firework[N].X, Firework[N].Y, 0);
  // the main part of the rocket
  glColor3f(1, 1, 1);    // here Z is used as the alpha value
  glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 0);
    glTexCoord2f(1.0, 0.0); glVertex3f( 1.0, -1.0, 0);
    glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0, 0);
    glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0, 0);
  glEnd();

  // the tail of the rocket
  glBegin(GL_QUADS);
    for I :=0 to 15 do
    with Firework[N].Trail[I] do
    begin
      glColor4f(R, G, B, Z/100);    // here Z is used as the alpha value
      glTexCoord2f(0.0, 0.0); glVertex3f(X-1.0, Y-1.0, 0);
      glTexCoord2f(1.0, 0.0); glVertex3f(X+1.0, Y-1.0, 0);
      glTexCoord2f(1.0, 1.0); glVertex3f(X+1.0, Y+1.0, 0);
      glTexCoord2f(0.0, 1.0); glVertex3f(X-1.0, Y+1.0, 0);
      X :=X+dX;
      Y :=Y+dY;
      Z :=Z-1;
      if Z = 0 then
      begin
        X := 0;
        Y := 0;
        Z :=50 + random(50);
      end;
    end;
  glEnd();
end;


{------------------------------------------------------------------}
{  draws the exploding firework                                    }
{------------------------------------------------------------------}
procedure FireworkExplode(const N : Integer);
var I : Integer;
    SlowDown : glFloat;
begin
  glTranslatef(Firework[N].X, Firework[N].Y, 0);
  glBegin(GL_QUADS);
    for I :=0 to 127 do
      with Firework[N].Particle[I] do
      begin
        glColor4f(R, G, B, 1.8-(ElapsedTime-Firework[N].StartTime-1000)/1200);
        glTexCoord2f(0.0, 0.0); glVertex3f(X-1.0, Y-1.0, Z);
        glTexCoord2f(1.0, 0.0); glVertex3f(X+1.0, Y-1.0, Z);
        glTexCoord2f(1.0, 1.0); glVertex3f(X+1.0, Y+1.0, Z);
        glTexCoord2f(0.0, 1.0); glVertex3f(X-1.0, Y+1.0, Z);

        // Calculate the new coords based on the change, dX, in a direction
        // and slow the change down as time goes by.
        slowdown :=((ElapsedTime-Firework[N].StartTime-1000) - Sqr(1000+Firework[N].StartTime-ElapsedTime)/5000)/6;
        X :=dX * slowdown;
        Y :=dY * slowdown;
        Z :=dZ * slowdown;
      end;
  glEnd();
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var N : Integer;
    Slowdown : glFLoat;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                       // Reset The View

  glTranslatef(0.0,0.0,-50);

  for N :=0 to Fireworks-1 do
  begin
    glPushMatrix();
      // Display the firework
      if (ElapsedTime-Firework[N].StartTime < 1000) then
        FireworkTail(N)
      else
        FireworkExplode(N);

      // create a new firework
      if ElapsedTime - Firework[N].StartTime - FireWork[N].Duration >=0 then
        SetupFirework(N);

      // keep moving and slowing down firework
      Slowdown :=Sqr(Firework[N].StartTime-ElapsedTime);
      Firework[N].X :=Firework[N].dX * ((ElapsedTime-Firework[N].StartTime) - Slowdown/7000);
      Firework[N].Y :=Firework[N].dY * ((ElapsedTime-Firework[N].StartTime) - Slowdown/4500) - 20;
    glPopMatrix;
  end;
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
var I : Integer;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);
  glEnable(GL_BLEND);

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  glEnable(GL_TEXTURE_2D);                     // Enable Texture Mapping
  LoadTexture(pathMedia+'spark.bmp', ParticleTex,false);    // Load the Texture
  glBindTexture(GL_TEXTURE_2D, ParticleTex);  // Bind the Texture to the object

  randomize;
  Fireworks :=3;
  for I :=0 to Fireworks-1 do
  begin
    SetupFirework(I);
    Firework[i].StartTime :=-1000*I;
  end;


  xspeed:=10;
  xdelta:=1;
end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
   
  glInit;
  glDraw;
  //==========================================================================================
  //OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
  plTimer1.Enabled:=True;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  xspeed:=vcSpeed.Position;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  if ElapsedTime<10 then xdelta:=1;
  if ElapsedTime>10000000 then xdelta:=-1;

   ElapsedTime:=ElapsedTime+xdelta*xspeed;

  //..........................
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

end.

