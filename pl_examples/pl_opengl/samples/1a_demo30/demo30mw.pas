{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo30mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs, math,
  OpenGLPanel, jitter, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;

const

  // Jitter constants
  ACSIZE = 8;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

  SphereQuadric : PGLUquadricObj;     // Quadric for our sphere
  // Lighting and materials
  ambient : array [0..3] of GLfloat = (  0.0, 0.0, 0.0, 1.0 );
  diffuse : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
  position : array [0..3] of GLfloat = (  0.0, 3.0, 3.0, 0.0 );

  lmodel_ambient : array [0..3] of GLfloat = ( 0.2, 0.2, 0.2, 1.0);
  local_view : GLfloat  =  0.0 ;


{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Used for scene jittering and as a replacement for gluPerspective}
{------------------------------------------------------------------}
procedure accFrustum(dleft, dright, dbottom, dtop, dnear, dfar, dpixdx, dpixdy, deyedx, deyedy, dfocus : GLdouble );
var
  xwsize, ywsize :GLdouble;
  dx, dy : GLdouble;
  viewport : array [0..3] of GLint;
begin

    glGetIntegerv(GL_VIEWPORT, @viewport);                         // Get the current viewport

    xwsize := dright - dleft;
    ywsize := dtop - dbottom;
    dx := -(dpixdx * xwsize/viewport[2] + deyedx * dnear/dfocus);
    dy := -(dpixdy * ywsize/viewport[3] + deyedy * dnear/dfocus);

    glMatrixMode(GL_PROJECTION);                                   // Switch the the Projection Matrix
    glLoadIdentity();                                              // Load A new matix onto the stack
    glFrustum (dleft + dx, dright + dx, dbottom + dy, dtop + dy, dnear, dfar);  // Set the frustrum
    glMatrixMode(GL_MODELVIEW);                                    // Switch to the Modelview matrix
    glLoadIdentity();                                              // Load A new matix onto the stack
    glTranslatef (-deyedx, -deyedy, 0.0);                          // Translate the scene according to the eye coordinates
end;

{------------------------------------------------------------------}
{  Used for scene jittering and as a replacement for gluPerspective}
{------------------------------------------------------------------}
procedure accPerspective(dfovy, daspect, dnear, dfar, dpixdx, dpixdy, deyedx, deyedy, dfocus : GLdouble);
var
  fov2, left, right, bottom, top : GLdouble;
begin

    fov2 := ((dfovy*PI) / 180.0) / 2.0;

    top := dnear / (ArcCos(fov2) / ArcSin(fov2));
    bottom := -top;
    right := top * daspect;
    left := -right;

    accFrustum (left, right, bottom, top, dnear, dfar,
        dpixdx, dpixdy, deyedx, deyedy, dfocus);        // Setup the accumilation frustrum
end;

{------------------------------------------------------------------}
{  Render a spere with the supplied material properties            }
{------------------------------------------------------------------}
procedure renderSphere( x, y, z,
                    ambr, ambg, ambb,
                    difr, difg, difb,
                    specr, specg, specb, shine :GLfloat);
var mat: array [0..3] of single;
begin

    glPushMatrix();                                                  // Save the current matrix
      glTranslatef (x, y, z);                                        // Move the scene
      mat[0] := ambr; mat[1] := ambg; mat[2] := ambb; mat[3] := 1.0;
      glMaterialfv (GL_FRONT, GL_AMBIENT, @mat);                     // Set the Ambient material property
      mat[0] := difr; mat[1] := difg; mat[2] := difb;
      glMaterialfv (GL_FRONT, GL_DIFFUSE, @mat);                     // Set the Diffuse material property
      mat[0] := specr; mat[1] := specg; mat[2] := specb;
      glMaterialfv (GL_FRONT, GL_SPECULAR, @mat);                    // Set the Specular material property
      glMaterialf (GL_FRONT, GL_SHININESS, shine*128.0);             // Set the Shininess material property
      gluSphere(SphereQuadric,0.5,32,32);                            // Draw the Sphere
    glPopMatrix();                                                   // Restore the last saved matrix
end;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
{*  glDraw() draws 5 Spheres into the accumulation buffer
 *  several times; each time with a jittered perspective.
 *  The focal point is at z = 5.0, so the gold spher will
 *  stay in focus.  The amount of jitter is adjusted by the
 *  magnitude of the accPerspective() jitter; in this example, 0.33.
 *  In this example, the Spheres are drawn 8 times.  See jitter.h
 *}
procedure glDraw();
var
  viewport : array [0..3] of GLint;
  jitter : GLint;
begin

    glGetIntegerv (GL_VIEWPORT, @viewport);                               // Get the vurrent view area
    glClear(GL_ACCUM_BUFFER_BIT);                                         // Clear the Accunulation buffer

    for jitter := 0 to ACSIZE do
    begin
      glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);                // Clear the Colour and Depth buffer
      accPerspective (45.0,
        viewport[2]/viewport[3],
        1.0, 15.0, 0.0, 0.0,
        0.33*j8[jitter].x, 0.33*j8[jitter].y, 5.0);                       // Set the accumulation perspective
      //	ruby, gold, silver, emerald, and cyan teapots
      renderSphere (-1.1, -0.5, -4.5, 0.1745, 0.01175, 0.01175,
          0.61424, 0.04136, 0.04136, 0.727811, 0.626959, 0.626959, 0.6);
      renderSphere (-0.5, -0.5, -5.0, 0.24725, 0.1995, 0.0745,
          0.75164, 0.60648, 0.22648, 0.628281, 0.555802, 0.366065, 0.4);
      renderSphere (0.2, -0.5, -5.5, 0.19225, 0.19225, 0.19225,
          0.50754, 0.50754, 0.50754, 0.508273, 0.508273, 0.508273, 0.4);
      renderSphere (1.0, -0.5, -6.0, 0.0215, 0.1745, 0.0215,
          0.07568, 0.61424, 0.07568, 0.633, 0.727811, 0.633, 0.6);
      renderSphere (1.8, -0.5, -6.5, 0.0, 0.1, 0.06, 0.0, 0.50980392,
          0.50980392, 0.50196078, 0.50196078, 0.50196078, 0.25);
      glAccum (GL_ACCUM, 0.125);                                          // Add to the accumulation buffer
    end;

    glAccum (GL_RETURN, 1.0);                                             // Render the accumulation buffer to the scene


    glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;



{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================


    glEnable(GL_DEPTH_TEST);                                   // Enable Depth Testing
    glDepthFunc(GL_LESS);                                      // Set the Depth Function to Less

    glLightfv(GL_LIGHT0, GL_AMBIENT, @ambient);                // Set Light 0's Ambient Property
    glLightfv(GL_LIGHT0, GL_DIFFUSE, @diffuse);                // Set Light 0's Diffuse Property
    glLightfv(GL_LIGHT0, GL_POSITION, @position);              // Set Light 0's Position

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, @lmodel_ambient);   // Set the Lighting Model's Ambient value
    glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, @local_view);  // Use Local Lighting(Slower)

    glFrontFace (GL_CW);                                       // Set the winding of our polygons to Clock-wise
    glEnable(GL_LIGHTING);                                     // Enable Lighting
    glEnable(GL_LIGHT0);                                       // enable Light 0
    glEnable(GL_AUTO_NORMAL);                                  // Auto generate Normals
    glEnable(GL_NORMALIZE);                                    // Normalize if we scale

    glMatrixMode (GL_MODELVIEW);                               // Switch to Modelview matrix
    glLoadIdentity ();                                         // Load a new Modelview Matrix

    glClearColor(0.0, 0.0, 0.0, 0.0);                          // Set the clear colour for the colour buffer
    glClearAccum(0.0, 0.0, 0.0, 0.0);                          // Set the clear colour for the accumulation buffer

  SphereQuadric := gluNewQuadric;		               // Create A Pointer To The Quadric Object (Return 0 If No Memory) (NEW)
  gluQuadricNormals(SphereQuadric, GLU_SMOOTH);	               // Create Smooth Normals (NEW)

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================


    glViewport(0, 0, Width, Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (Width <= Height) then
	    glOrtho (-2.25, 2.25, -2.25*Width/Height, 2.25*Height/Width, -10.0, 10.0)
    else
	    glOrtho (-2.25*Width/Height, 2.25*Width/Height, -2.25, 2.25, -10.0, 10.0);
    glMatrixMode(GL_MODELVIEW);

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

