{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo15mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}


type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;

const
  // User Constants

  // Definition of a complete font
   rasters :array[0..94] of array[0..12] of GLubyte = (
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00),
    ($00, $00, $18, $18, $00, $00, $18, $18, $18, $18, $18, $18, $18),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $36, $36, $36, $36),
    ($00, $00, $00, $66, $66, $ff, $66, $66, $ff, $66, $66, $00, $00),
    ($00, $00, $18, $7e, $ff, $1b, $1f, $7e, $f8, $d8, $ff, $7e, $18),
    ($00, $00, $0e, $1b, $db, $6e, $30, $18, $0c, $76, $db, $d8, $70),
    ($00, $00, $7f, $c6, $cf, $d8, $70, $70, $d8, $cc, $cc, $6c, $38),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $18, $1c, $0c, $0e),
    ($00, $00, $0c, $18, $30, $30, $30, $30, $30, $30, $30, $18, $0c),
    ($00, $00, $30, $18, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $18, $30),
    ($00, $00, $00, $00, $99, $5a, $3c, $ff, $3c, $5a, $99, $00, $00),
    ($00, $00, $00, $18, $18, $18, $ff, $ff, $18, $18, $18, $00, $00),
    ($00, $00, $30, $18, $1c, $1c, $00, $00, $00, $00, $00, $00, $00),
    ($00, $00, $00, $00, $00, $00, $ff, $ff, $00, $00, $00, $00, $00),
    ($00, $00, $00, $38, $38, $00, $00, $00, $00, $00, $00, $00, $00),
    ($00, $60, $60, $30, $30, $18, $18, $0c, $0c, $06, $06, $03, $03),
    ($00, $00, $3c, $66, $c3, $e3, $f3, $db, $cf, $c7, $c3, $66, $3c),
    ($00, $00, $7e, $18, $18, $18, $18, $18, $18, $18, $78, $38, $18),
    ($00, $00, $ff, $c0, $c0, $60, $30, $18, $0c, $06, $03, $e7, $7e),
    ($00, $00, $7e, $e7, $03, $03, $07, $7e, $07, $03, $03, $e7, $7e),
    ($00, $00, $0c, $0c, $0c, $0c, $0c, $ff, $cc, $6c, $3c, $1c, $0c),
    ($00, $00, $7e, $e7, $03, $03, $07, $fe, $c0, $c0, $c0, $c0, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $c7, $fe, $c0, $c0, $c0, $e7, $7e),
    ($00, $00, $30, $30, $30, $30, $18, $0c, $06, $03, $03, $03, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $e7, $7e, $e7, $c3, $c3, $e7, $7e),
    ($00, $00, $7e, $e7, $03, $03, $03, $7f, $e7, $c3, $c3, $e7, $7e),
    ($00, $00, $00, $38, $38, $00, $00, $38, $38, $00, $00, $00, $00),
    ($00, $00, $30, $18, $1c, $1c, $00, $00, $1c, $1c, $00, $00, $00),
    ($00, $00, $06, $0c, $18, $30, $60, $c0, $60, $30, $18, $0c, $06),
    ($00, $00, $00, $00, $ff, $ff, $00, $ff, $ff, $00, $00, $00, $00),
    ($00, $00, $60, $30, $18, $0c, $06, $03, $06, $0c, $18, $30, $60),
    ($00, $00, $18, $00, $00, $18, $18, $0c, $06, $03, $c3, $c3, $7e),
    ($00, $00, $3f, $60, $cf, $db, $d3, $dd, $c3, $7e, $00, $00, $00),
    ($00, $00, $c3, $c3, $c3, $c3, $ff, $c3, $c3, $c3, $66, $3c, $18),
    ($00, $00, $fe, $c7, $c3, $c3, $c7, $fe, $c7, $c3, $c3, $c7, $fe),
    ($00, $00, $7e, $e7, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $e7, $7e),
    ($00, $00, $fc, $ce, $c7, $c3, $c3, $c3, $c3, $c3, $c7, $ce, $fc),
    ($00, $00, $ff, $c0, $c0, $c0, $c0, $fc, $c0, $c0, $c0, $c0, $ff),
    ($00, $00, $c0, $c0, $c0, $c0, $c0, $c0, $fc, $c0, $c0, $c0, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $cf, $c0, $c0, $c0, $c0, $e7, $7e),
    ($00, $00, $c3, $c3, $c3, $c3, $c3, $ff, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $7e, $18, $18, $18, $18, $18, $18, $18, $18, $18, $7e),
    ($00, $00, $7c, $ee, $c6, $06, $06, $06, $06, $06, $06, $06, $06),
    ($00, $00, $c3, $c6, $cc, $d8, $f0, $e0, $f0, $d8, $cc, $c6, $c3),
    ($00, $00, $ff, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $c0),
    ($00, $00, $c3, $c3, $c3, $c3, $c3, $c3, $db, $ff, $ff, $e7, $c3),
    ($00, $00, $c7, $c7, $cf, $cf, $df, $db, $fb, $f3, $f3, $e3, $e3),
    ($00, $00, $7e, $e7, $c3, $c3, $c3, $c3, $c3, $c3, $c3, $e7, $7e),
    ($00, $00, $c0, $c0, $c0, $c0, $c0, $fe, $c7, $c3, $c3, $c7, $fe),
    ($00, $00, $3f, $6e, $df, $db, $c3, $c3, $c3, $c3, $c3, $66, $3c),
    ($00, $00, $c3, $c6, $cc, $d8, $f0, $fe, $c7, $c3, $c3, $c7, $fe),
    ($00, $00, $7e, $e7, $03, $03, $07, $7e, $e0, $c0, $c0, $e7, $7e),
    ($00, $00, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $c3, $c3, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $18, $3c, $3c, $66, $66, $c3, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $c3, $e7, $ff, $ff, $db, $db, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $c3, $66, $66, $3c, $3c, $18, $3c, $3c, $66, $66, $c3),
    ($00, $00, $18, $18, $18, $18, $18, $18, $3c, $3c, $66, $66, $c3),
    ($00, $00, $ff, $c0, $c0, $60, $30, $7e, $0c, $06, $03, $03, $ff),
    ($00, $00, $3c, $30, $30, $30, $30, $30, $30, $30, $30, $30, $3c),
    ($00, $03, $03, $06, $06, $0c, $0c, $18, $18, $30, $30, $60, $60),
    ($00, $00, $3c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $3c),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $c3, $66, $3c, $18),
    ($ff, $ff, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $18, $38, $30, $70),
    ($00, $00, $7f, $c3, $c3, $7f, $03, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $fe, $c3, $c3, $c3, $c3, $fe, $c0, $c0, $c0, $c0, $c0),
    ($00, $00, $7e, $c3, $c0, $c0, $c0, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $7f, $c3, $c3, $c3, $c3, $7f, $03, $03, $03, $03, $03),
    ($00, $00, $7f, $c0, $c0, $fe, $c3, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $30, $30, $30, $30, $30, $fc, $30, $30, $30, $33, $1e),
    ($7e, $c3, $03, $03, $7f, $c3, $c3, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $c3, $c3, $c3, $c3, $c3, $c3, $fe, $c0, $c0, $c0, $c0),
    ($00, $00, $18, $18, $18, $18, $18, $18, $18, $00, $00, $18, $00),
    ($38, $6c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $00, $00, $0c, $00),
    ($00, $00, $c6, $cc, $f8, $f0, $d8, $cc, $c6, $c0, $c0, $c0, $c0),
    ($00, $00, $7e, $18, $18, $18, $18, $18, $18, $18, $18, $18, $78),
    ($00, $00, $db, $db, $db, $db, $db, $db, $fe, $00, $00, $00, $00),
    ($00, $00, $c6, $c6, $c6, $c6, $c6, $c6, $fc, $00, $00, $00, $00),
    ($00, $00, $7c, $c6, $c6, $c6, $c6, $c6, $7c, $00, $00, $00, $00),
    ($c0, $c0, $c0, $fe, $c3, $c3, $c3, $c3, $fe, $00, $00, $00, $00),
    ($03, $03, $03, $7f, $c3, $c3, $c3, $c3, $7f, $00, $00, $00, $00),
    ($00, $00, $c0, $c0, $c0, $c0, $c0, $e0, $fe, $00, $00, $00, $00),
    ($00, $00, $fe, $03, $03, $7e, $c0, $c0, $7f, $00, $00, $00, $00),
    ($00, $00, $1c, $36, $30, $30, $30, $30, $fc, $30, $30, $30, $00),
    ($00, $00, $7e, $c6, $c6, $c6, $c6, $c6, $c6, $00, $00, $00, $00),
    ($00, $00, $18, $3c, $3c, $66, $66, $c3, $c3, $00, $00, $00, $00),
    ($00, $00, $c3, $e7, $ff, $db, $c3, $c3, $c3, $00, $00, $00, $00),
    ($00, $00, $c3, $66, $3c, $18, $3c, $66, $c3, $00, $00, $00, $00),
    ($c0, $60, $60, $30, $18, $3c, $66, $66, $c3, $00, $00, $00, $00),
    ($00, $00, $ff, $60, $30, $18, $0c, $06, $ff, $00, $00, $00, $00),
    ($00, $00, $0f, $18, $18, $18, $38, $f0, $38, $18, $18, $18, $0f),
    ($18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18),
    ($00, $00, $f0, $18, $18, $18, $1c, $0f, $1c, $18, $18, $18, $f0),
    ($00, $00, $00, $00, $00, $00, $06, $8f, $f1, $60, $00, $00, $00)

);


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

  // Font
  fontOffset : GLuint;                // Offset for the Display list

  white : array[0..2] of GLfloat = ( 1.0, 1.0, 1.0 );

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Create the display lists for our raster font                    }
{------------------------------------------------------------------}
procedure MakeRasterFont;
var i : GLuint;
begin
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);      // set the bit alignment
    fontOffset := glGenLists (128);             // generate 128 display lists
    for i := 32 to 126 do
    begin
        glNewList(i+fontOffset, GL_COMPILE);    // Create but dont execute a list
            glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, @rasters[i-32]); // Add a character to the current Display list
        glEndList();                            // End the display list creation
    end;
end;


{------------------------------------------------------------------}
{  Draw a string of characters to the scene                        }
{------------------------------------------------------------------}
procedure PrintString(s : string);
begin
  glPushAttrib (GL_LIST_BIT);
    glListBase(fontOffset);
    glCallLists(length(s), GL_UNSIGNED_BYTE,  PChar(s));
  glPopAttrib();
end;



{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i, j : GLint;
    teststring : string;
    tempI : GLfloat;
begin

    teststring := '';
    glClear(GL_COLOR_BUFFER_BIT);                   // Clear the colour buffer
    glColor3fv(@white);                             // Set the font colour to white
    i := 0;                                         // Initialize the variable for our loop
    while (i < 127) do
    begin
      tempI := i/32.0;
      glRasterPos2f(100.0, 400.0 - 18.0*tempI);     // Set the raster position
        for j := 0 to 31 do                         // Create the string
            teststring := teststring + char(i+j);

        teststring := teststring + char(0);
        printString(teststring);                    // Print the string to the scene
        teststring := '';                           // Re-init the string variable
        i := i + 32;                                // Incrememt by 32 characters
    end;
    glRasterPos2i(100, 300);                        // Set the position for the text to be rendered
    printString('The quick brown fox jumps');       // Print the string to the scene
    glRasterPos2i(100, 282);                        // Set the position for the text to be rendered
    printString('over a lazy dog.');                // Print the string to the scene

  // Flush the OpenGL Buffer
  glFlush();                        // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;

{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //===========================================================


  glShadeModel (GL_FLAT);                           // Do not shade the model
  makeRasterFont();

  //===========================================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  if (Height = 0) then Height := 1;   // prevent divide by zero exception

    glViewport(0, 0, Width, Height);    // Set the viewport for the OpenGL window
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho (0.0, Width, 0.0, Height, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

