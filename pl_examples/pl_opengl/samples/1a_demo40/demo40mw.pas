{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo40mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs, math,
  OpenGLPanel, jitter, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;

const
  // User Constants

    // Icosahedron constants
    X = 0.525731112119133606;
    Z = 0.850650808352039932;

    vdata : array [0..11] of array [0..2] of GLfloat = (
     (-X, 0.0, Z), (X, 0.0, Z), (-X, 0.0, -Z), (X, 0.0, -Z),
     (0.0, Z, X), (0.0, Z, -X), (0.0, -Z, X), (0.0, -Z, -X),
     (Z, X, 0.0), (-Z, X, 0.0), (Z, -X, 0.0), (-Z, -X, 0.0)
      );

    tindices : array [0..19] of array [0..2] of GLint = (
     (0,4,1), (0,9,4), (9,5,4), (4,5,8), (4,8,1),
     (8,10,1), (8,3,10),(5,3,8), (5,2,3), (2,7,3),
     (7,10,3), (7,6,10), (7,11,6), (11,0,6), (0,1,6),
     (6,1,10), (9,0,11), (9,11,2), (9,2,5), (7,2,11)
     );


    // Lighting and materials
    mat_ambient : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
    mat_specular : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
    light_position : array [0..3] of GLfloat = (0.0, 0.0, 10.0, 1.0 );
    lm_ambient : array [0..3] of GLfloat = ( 0.2, 0.2, 0.2, 1.0 );

    torus_diffuse : array [0..3] of GLfloat = ( 0.7, 0.7, 0.0, 1.0 );
    cube_diffuse : array [0..3] of GLfloat = ( 0.0, 0.7, 0.7, 1.0 );
    sphere_diffuse : array [0..3] of GLfloat = (0.7, 0.0, 0.7, 1.0 );
    octa_diffuse : array [0..3] of GLfloat = ( 0.7, 0.4, 0.4, 1.0 );

    // Jitter constants
    ACSIZE = 8;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

  TorusDL : GLuint;                  // For torus DL
  CubeDL : GLuint;                   // For Cube DL
  IcosDL : GLuint;                   // For Icosahedron
  SphereQuadric : PGLUquadricObj;    // Quadric for our sphere



{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Used for scene jittering and as a replacement for gluPerspective}
{------------------------------------------------------------------}
procedure accFrustum(dleft, dright, dbottom, dtop, dnear, dfar, dpixdx, dpixdy, deyedx, deyedy, dfocus : GLdouble );
var
  xwsize, ywsize :GLdouble;
  dx, dy : GLdouble;
  viewport : array [0..3] of GLint;
begin

    glGetIntegerv(GL_VIEWPORT, @viewport);                         // Get the current viewport

    xwsize := dright - dleft;
    ywsize := dtop - dbottom;
    dx := -(dpixdx * xwsize/viewport[2] + deyedx * dnear/dfocus);
    dy := -(dpixdy * ywsize/viewport[3] + deyedy * dnear/dfocus);

    glMatrixMode(GL_PROJECTION);                                   // Switch the the Projection Matrix
    glLoadIdentity();                                              // Load A new matix onto the stack
    glFrustum (dleft + dx, dright + dx, dbottom + dy, dtop + dy, dnear, dfar);  // Set the frustrum
    glMatrixMode(GL_MODELVIEW);                                    // Switch to the Modelview matrix
    glLoadIdentity();                                              // Load A new matix onto the stack
    glTranslatef (-deyedx, -deyedy, 0.0);                          // Translate the scene according to the eye coordinates
end;

{------------------------------------------------------------------}
{  Used for scene jittering and as a replacement for gluPerspective}
{------------------------------------------------------------------}
procedure accPerspective(dfovy, daspect, dnear, dfar, dpixdx, dpixdy, deyedx, deyedy, dfocus : GLdouble);
var
  fov2, left, right, bottom, top : GLdouble;
begin

    fov2 := ((dfovy*PI) / 180.0) / 2.0;

    top := dnear / (ArcCos(fov2) / ArcSin(fov2));
    bottom := -top;
    right := top * daspect;
    left := -right;

    accFrustum (left, right, bottom, top, dnear, dfar,
        dpixdx, dpixdy, deyedx, deyedy, dfocus);        // Setup the accumilation frustrum
end;

{------------------------------------------------------------------}
{  Substitute for auxWireBox (Draws a box with quads)              }
{------------------------------------------------------------------}
procedure DrawBox(Height, Width, Depth : GLfloat);
var HalfHeight, HalfWidth, HalfDepth : GLfloat;
begin

  HalfHeight := Height/2;
  HalfWidth := Width/2;
  HalfDepth := Depth/2;

  glBegin(GL_QUADS);
    // Front Face
    glNormal3f( 0.0, 0.0, 1.0);
    glVertex3f(-HalfWidth, -HalfHeight,  HalfDepth);
    glVertex3f( HalfWidth, -HalfHeight,  HalfDepth);
    glVertex3f( HalfWidth,  HalfHeight,  HalfDepth);
    glVertex3f(-HalfWidth,  HalfHeight,  HalfDepth);
    // Back Face
    glNormal3f( 0.0, 0.0,-1.0);
    glVertex3f(-HalfWidth, -HalfHeight, -HalfDepth);
    glVertex3f(-HalfWidth,  HalfHeight, -HalfDepth);
    glVertex3f( HalfWidth,  HalfHeight, -HalfDepth);
    glVertex3f( HalfWidth, -HalfHeight, -HalfDepth);
    // Top Face
    glNormal3f( 0.0, 1.0, 0.0);
    glVertex3f(-HalfWidth,  HalfHeight, -HalfDepth);
    glVertex3f(-HalfWidth,  HalfHeight,  HalfDepth);
    glVertex3f( HalfWidth,  HalfHeight,  HalfDepth);
    glVertex3f( HalfWidth,  HalfHeight, -HalfDepth);
    // Bottom Face
    glNormal3f( 0.0,-1.0, 0.0);
    glVertex3f(-HalfWidth, -HalfHeight, -HalfDepth);
    glVertex3f( HalfWidth, -HalfHeight, -HalfDepth);
    glVertex3f( HalfWidth, -HalfHeight,  HalfDepth);
    glVertex3f(-HalfWidth, -HalfHeight,  HalfDepth);
    // Right face
    glNormal3f( 1.0, 0.0, 0.0);
    glVertex3f( HalfWidth, -HalfHeight, -HalfDepth);
    glVertex3f( HalfWidth,  HalfHeight, -HalfDepth);
    glVertex3f( HalfWidth,  HalfHeight,  HalfDepth);
    glVertex3f( HalfWidth, -HalfHeight,  HalfDepth);
    // Left Face
    glNormal3f(-1.0, 0.0, 0.0);
    glVertex3f(-HalfWidth, -HalfHeight, -HalfDepth);
    glVertex3f(-HalfWidth, -HalfHeight,  HalfDepth);
    glVertex3f(-HalfWidth,  HalfHeight,  HalfDepth);
    glVertex3f(-HalfWidth,  HalfHeight, -HalfDepth);
  glEnd();
end;

{------------------------------------------------------------------}
{  Create a torus be giving inner, outer radius and detail level   }
{------------------------------------------------------------------}
procedure CreateTorus(TubeRadius, Radius : GLfloat; Sides, Rings : Integer);
var I, J : Integer;
    theta, phi, theta1 : GLfloat;
    cosTheta, sinTheta : GLfloat;
    cosTheta1, sinTheta1 : GLfloat;
    ringDelta, sideDelta : GLfloat;
    cosPhi, sinPhi, dist : GLfloat;
begin
  sideDelta := 2.0 * Pi / Sides;
  ringDelta := 2.0 * Pi / rings;
  theta := 0.0;
  cosTheta := 1.0;
  sinTheta := 0.0;

  TorusDL :=glGenLists(1);
  glNewList(TorusDL, GL_COMPILE);
    for i := rings - 1 downto 0 do
    begin
      theta1 := theta + ringDelta;
      cosTheta1 := cos(theta1);
      sinTheta1 := sin(theta1);
      glBegin(GL_QUAD_STRIP);
        phi := 0.0;
        for j := Sides downto 0 do
        begin
          phi := phi + sideDelta;
          cosPhi := cos(phi);
          sinPhi := sin(phi);
          dist := Radius + (TubeRadius * cosPhi);

          glNormal3f(cosTheta1 * cosPhi, -sinTheta1 * cosPhi, sinPhi);
          glVertex3f(cosTheta1 * dist, -sinTheta1 * dist, TubeRadius * sinPhi);

          glNormal3f(cosTheta * cosPhi, -sinTheta * cosPhi, sinPhi);
          glVertex3f(cosTheta * dist, -sinTheta * dist, TubeRadius * sinPhi);
        end;
      glEnd();
      theta := theta1;
      cosTheta := cosTheta1;
      sinTheta := sinTheta1;
    end;
  glEndList();
end;

{------------------------------------------------------------------}
{  Create an Icosahedron in a Display list                         }
{------------------------------------------------------------------}
procedure CreateIcosahedron();
var i :GLint;
begin
  IcosDL :=glGenLists(1);
  glNewList(IcosDL, GL_COMPILE);
    for i := 0 to 19 do
    begin
        glBegin(GL_TRIANGLES);
          glNormal3fv(@vdata[tindices[i][0]][0]);
          glVertex3fv(@vdata[tindices[i][0]][0]);
          glNormal3fv(@vdata[tindices[i][1]][0]);
          glVertex3fv(@vdata[tindices[i][1]][0]);
          glNormal3fv(@vdata[tindices[i][2]][0]);
          glVertex3fv(@vdata[tindices[i][2]][0]);
      glEnd();
    end;
  glEndList;
end;

{------------------------------------------------------------------}
{  Draw all the objects in the scene                               }
{------------------------------------------------------------------}
procedure DrawObjects();
begin
  glPushMatrix ();                                           // Load a new matrix onto the stack
    glTranslatef (0.0, 0.0, -5.0);                           // Maove the scene back 5 units
    glRotatef (30.0, 1.0, 0.0, 0.0);                         // Rotate the scene 30 degrees on the x-axis

    glPushMatrix ();                                         // Load a new matrix onto the stack
      glTranslatef (-0.80, 0.35, 0.0);                       // Move the scene
      glRotatef (100.0, 1.0, 0.0, 0.0);                      // Rotate 100 degrees on the x-axis
      glMaterialfv(GL_FRONT, GL_DIFFUSE, @torus_diffuse);    // Set the Torus material
      glCallList(TorusDL);                                   // Draw the Torus
    glPopMatrix ();                                          // Restore the last saved matrix

   glPushMatrix ();                                          // Load a new matrix onto the stack
      glTranslatef (-0.75, -0.50, 0.0);                      // Move the scene
      glRotatef (45.0, 0.0, 0.0, 1.0);                       // Rotate 45 degrees on the z-axis
      glRotatef (45.0, 1.0, 0.0, 0.0);                       // Rotate 45 degrees on the x-axis
      glMaterialfv(GL_FRONT, GL_DIFFUSE, @cube_diffuse);     // Set the Cube material
      glCallList(CubeDL);                                    // Draw the cube
    glPopMatrix ();                                          // Restore the last saved matrix

    glPushMatrix ();                                         // Load a new matrix onto the stack
      glTranslatef (0.75, 0.60, 0.0);                        // Move the scene
      glRotatef (30.0, 1.0, 0.0, 0.0);                       // Rotate 30 degrees on the x-axis
      glMaterialfv(GL_FRONT, GL_DIFFUSE, @sphere_diffuse);   // Set the Sphere material
      gluSphere(SphereQuadric,1.0,32,32);                    // Draw the sphere
    glPopMatrix ();                                          // Restore the last saved matrix

    glPushMatrix ();                                         // Load a new matrix onto the stack
      glTranslatef (0.70, -0.90, 0.25);                      // Move the scene
      glMaterialfv(GL_FRONT, GL_DIFFUSE, @octa_diffuse);     // Set the Icosahedron material
      glCallList(IcosDL);                                    // Draw the Icosahedron
    glPopMatrix ();                                          // Restore the last saved matrix

  glPopMatrix ();                                            // Restore the matrix we saved initially
end;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var
  viewport : array [0..3] of GLint;
  jitter : GLint;
begin


    glGetIntegerv (GL_VIEWPORT, @viewport);                  // Get the current Viewport

    glClear(GL_ACCUM_BUFFER_BIT);                            // Clear the Accumulation Buffer
    for jitter := 0 to ACSIZE -1 do
    begin
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT); // Clear the Colour and Depth Buffer
        accPerspective (50.0,
            viewport[2]/viewport[3],
            1.0, 15.0, j8[jitter].x, j8[jitter].y,
            0.0, 0.0, 1.0);
        DrawObjects ();                                      // Set the accumulation Perspective
        glAccum(GL_ACCUM, 1.0/ACSIZE);                       // Accumilate the scene
    end;
    glAccum (GL_RETURN, 1.0);                                // Return the accumulated scene to the Colour buffer

    glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================

  glMaterialfv(GL_FRONT, GL_AMBIENT, @mat_ambient);     // Set the Ambient Material
  glMaterialfv(GL_FRONT, GL_SPECULAR, @mat_specular);   // Set the Specular Material
  glMaterialf(GL_FRONT, GL_SHININESS, 50.0);            // Set the Material Shininess
  glLightfv(GL_LIGHT0, GL_POSITION, @light_position);   // Set the Light Position
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, @lm_ambient);  // Set the Light Ambient Value

  glEnable(GL_LIGHTING);                                // Enable Lighting
  glEnable(GL_LIGHT0);                                  // Enable Light 0
  glDepthFunc(GL_LEQUAL);                               // Set the Depth Funtion To Linear Equal
  glEnable(GL_DEPTH_TEST);                              // Enable Depth Testing (Hidden Surface Removal)
  glShadeModel (GL_FLAT);                               // Use Flat Shading
  glClearColor(0.0, 0.0, 0.0, 0.0);                     // Initialize the Colour buffer to black
  glClearAccum(0.0, 0.0, 0.0, 0.0);                     // Initialize the Accumulation buffer to black

  CreateTorus( 0.275, 0.85, 10, 20);                    // Create a Torus

  CubeDL := glGenLists(1);                              // Create a Cube
  glNewList(CubeDL, GL_COMPILE);
    DrawBox(1.5,1.5,1.5);
  glEndList;

  SphereQuadric := gluNewQuadric();		                 // Create A Pointer To The Quadric Object (Return 0 If No Memory) (NEW)
  gluQuadricNormals(SphereQuadric, GLU_SMOOTH);	       // Create Smooth Normals (NEW)

  CreateIcosahedron();                                 // Create an Icosahedron

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

   glViewport(0, 0, Width, Height);        // Set the Viewport to the size of the sceen

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

