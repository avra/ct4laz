{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit Metaballsmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,LCLIntf,
  OpenGLPanel, ctGL, ctGLU,
  LookUpTable;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    cube_rotationx: GLFloat;
    cube_rotationy: GLFloat;
    cube_rotationz: GLFloat;
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation 

{$R *.lfm}

type
  TGLCoord = Record
    X, Y, Z : glFLoat;
  end;
  TMetaBall = Record
    Radius : glFloat;
    X, Y, Z : glFLoat;
  end;
  TGridPoint = record
    P : TGLCoord;
    Value : glFLoat;  // Result of the metaball equations at this point
  end;
  PGridPoint = ^TGridPoint;
  TGridCube = record
    GridPoint : Array [0..7] of PGridPoint; // Points to 8 grid points (cube)
  end;

var
  FPSCount : GLfloat = 0;
  ElapsedTime,DemoStart, LastTime : GLfloat;
  // Textures
  MyTextureTex : glUint;

  // User vaiables
  Wireframe     : Boolean;
  SmoothShading : Boolean;
  GridSize      : Integer;
  TessTriangles : Integer;           // Number of triangles by metaball tesselation.
  MetaBall : Array[1..3] of TMetaBall;
  Grid  : Array[0..50, 0..50, 0..50] of TGridPoint;  // for this demo set max gridsize = 50
  Cubes : Array[0..49, 0..49, 0..49] of TGridCube;

{------------------------------------------------------------------}
{  Function to convert int to string. (No sysutils = smaller EXE)  }
{------------------------------------------------------------------}
function IntToStr(Num : Integer) : String;  // using SysUtils increase file size by 100K
begin
  Str(Num, result);
end;


procedure SetColor(const V : TGLCoord);
var C : glFloat;
begin
  with V do
    C := sqrt(x*x + y*y +z*z);
  glColor3f(C, C, C+0.1);    // add a hint of blue
end;


procedure InitGrid;
var cx, cy, cz : Integer;
begin
  // Create the grid positions
  for cx := 0 to GridSize do
    for cy := 0 to GridSize do
      for cz := 0 to GridSize do
      begin
        Grid[cx, cy, cz].P.X := 2*cx/GridSize -1;   // grid from -1 to 1
        Grid[cx, cy, cz].P.Y := 2*cy/GridSize -1;   // grid from -1 to 1
        Grid[cx, cy, cz].P.Z := 1-2*cz/GridSize;    // grid from -1 to 1
      end;

  // Create the cubes. Each cube points to 8 grid points
  for cx := 0 to GridSize-1 do
    for cy := 0 to GridSize-1 do
      for cz := 0 to GridSize-1 do
      begin
        Cubes[cx,cy,cz].GridPoint[0] := @Grid[cx,   cy,   cz  ];
        Cubes[cx,cy,cz].GridPoint[1] := @Grid[cx+1, cy,   cz  ];
        Cubes[cx,cy,cz].GridPoint[2] := @Grid[cx+1, cy,   cz+1];
        Cubes[cx,cy,cz].GridPoint[3] := @Grid[cx,   cy,   cz+1];
        Cubes[cx,cy,cz].GridPoint[4] := @Grid[cx,   cy+1, cz  ];
        Cubes[cx,cy,cz].GridPoint[5] := @Grid[cx+1, cy+1, cz  ];
        Cubes[cx,cy,cz].GridPoint[6] := @Grid[cx+1, cy+1, cz+1];
        Cubes[cx,cy,cz].GridPoint[7] := @Grid[cx,   cy+1, cz+1];
      end;
end;


{----------------------------------------------------------}
{  Interpolate the position where an metaballs intersects  }
{  the line betweenthe two coordicates, C1 and C2          }
{----------------------------------------------------------}
function Interpolate(const C1, C2 : TGLCoord; Val1, Val2 : glFloat) : TGLCoord;
var mu : glFLoat;
begin
  if Abs(Val1) = 1 then
    Result := C1
  else
  if Abs(Val2) = 1 then
    Result := C2
  else
  if Val1 = Val2 then
    Result := C1
  else
  begin
    mu := (1 - Val1) / (Val2 - Val1);
    Result.x := C1.x + mu * (C2.x - C1.x);
    Result.y := C1.y + mu * (C2.y - C1.y);
    Result.z := C1.z + mu * (C2.z - C1.z);
  end;
end;

{------------------------------------------------------------}
{  Calculate the triangles required to draw a Cube.          }
{  Draws the triangles that makes up a Cube                  }
{------------------------------------------------------------}
procedure CreateCubeTriangles(const GridCube : TGridCube);
var I : Integer;
    C : glFloat;
    CubeIndex: Integer;
    VertList : Array[0..11] of TGLCoord;
begin
  // Determine the index into the edge table which tells
  // us which vertices are inside/outside the metaballs
  CubeIndex := 0;
  if GridCube.GridPoint[0]^.Value < 1 then CubeIndex := CubeIndex or 1;
  if GridCube.GridPoint[1]^.Value < 1 then CubeIndex := CubeIndex or 2;
  if GridCube.GridPoint[2]^.Value < 1 then CubeIndex := CubeIndex or 4;
  if GridCube.GridPoint[3]^.Value < 1 then CubeIndex := CubeIndex or 8;
  if GridCube.GridPoint[4]^.Value < 1 then CubeIndex := CubeIndex or 16;
  if GridCube.GridPoint[5]^.Value < 1 then CubeIndex := CubeIndex or 32;
  if GridCube.GridPoint[6]^.Value < 1 then CubeIndex := CubeIndex or 64;
  if GridCube.GridPoint[7]^.Value < 1 then CubeIndex := CubeIndex or 128;

  // Check if the cube is entirely in/out of the surface
  if edgeTable[CubeIndex] = 0 then
    Exit;

  // Find the vertices where the surface intersects the cube.
  with GridCube do
  begin
    if (edgeTable[CubeIndex] and 1) <> 0 then
      VertList[0] := Interpolate(GridPoint[0]^.P, GridPoint[1]^.P, GridPoint[0]^.Value, GridPoint[1]^.Value);
    if (edgeTable[CubeIndex] and 2) <> 0 then
      VertList[1] := Interpolate(GridPoint[1]^.P, GridPoint[2]^.P, GridPoint[1]^.Value, GridPoint[2]^.Value);
    if (edgeTable[CubeIndex] and 4) <> 0 then
      VertList[2] := Interpolate(GridPoint[2]^.P, GridPoint[3]^.P, GridPoint[2]^.Value, GridPoint[3]^.Value);
    if (edgeTable[CubeIndex] and 8) <> 0 then
      VertList[3] := Interpolate(GridPoint[3]^.P, GridPoint[0]^.P, GridPoint[3]^.Value, GridPoint[0]^.Value);
    if (edgeTable[CubeIndex] and 16) <> 0 then
      VertList[4] := Interpolate(GridPoint[4]^.P, GridPoint[5]^.P, GridPoint[4]^.Value, GridPoint[5]^.Value);
    if (edgeTable[CubeIndex] and 32) <> 0 then
      VertList[5] := Interpolate(GridPoint[5]^.P, GridPoint[6]^.P, GridPoint[5]^.Value, GridPoint[6]^.Value);
    if (edgeTable[CubeIndex] and 64) <> 0 then
      VertList[6] := Interpolate(GridPoint[6]^.P, GridPoint[7]^.P, GridPoint[6]^.Value, GridPoint[7]^.Value);
    if (edgeTable[CubeIndex] and 128) <> 0 then
      VertList[7] := Interpolate(GridPoint[7]^.P, GridPoint[4]^.P, GridPoint[7]^.Value, GridPoint[4]^.Value);
    if (edgeTable[CubeIndex] and 256) <> 0 then
      VertList[8] := Interpolate(GridPoint[0]^.P, GridPoint[4]^.P, GridPoint[0]^.Value, GridPoint[4]^.Value);
    if (edgeTable[CubeIndex] and 512) <> 0 then
      VertList[9] := Interpolate(GridPoint[1]^.P, GridPoint[5]^.P, GridPoint[1]^.Value, GridPoint[5]^.Value);
    if (edgeTable[CubeIndex] and 1024) <> 0 then
      VertList[10] := Interpolate(GridPoint[2]^.P, GridPoint[6]^.P, GridPoint[2]^.Value, GridPoint[6]^.Value);
    if (edgeTable[CubeIndex] and 2048) <> 0 then
      VertList[11] := Interpolate(GridPoint[3]^.P, GridPoint[7]^.P, GridPoint[3]^.Value, GridPoint[7]^.Value);
  end;

  // Draw the triangles for this cube
  I := 0;
  while TriangleTable[CubeIndex, i] <> -1 do
  begin
    SetColor(VertList[TriangleTable[CubeIndex][i]]);
    glVertex3fv(@VertList[TriangleTable[CubeIndex][i]]);

    SetColor(VertList[TriangleTable[CubeIndex][i+1]]);
    glVertex3fv(@VertList[TriangleTable[CubeIndex][i+1]]);

    if SmoothShading then
      SetColor(VertList[TriangleTable[CubeIndex][i+2]]);
    glVertex3fv(@VertList[TriangleTable[CubeIndex][i+2]]);

    Inc(TessTriangles);
    Inc(i, 3);
  end;
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  glDisable(GL_TEXTURE_2D);                     // Enable Texture Mapping

  // initialise the metaball size and positions
  MetaBall[1].Radius :=0.3;
  MetaBall[1].X :=0.5;
  MetaBall[1].Y :=0;
  MetaBall[1].Z :=0;

  MetaBall[2].Radius :=0.22;
  MetaBall[2].X :=0;
  MetaBall[2].Y :=0;
  MetaBall[2].Z :=0;

  MetaBall[3].Radius :=0.25;
  MetaBall[3].X :=0;
  MetaBall[3].Y :=0;
  MetaBall[3].Z :=0;

  SmoothShading :=TRUE;
  WireFrame :=FALSE;
  GridSize  :=31;
  InitGrid;
  DemoStart := LCLIntf.GetTickCount64;
end;

procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
var cx, cy, cz : Integer;
    I : Integer;
begin
  if Sender=nil then ;


  //=======================================================
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, double(width) / height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //=======================================================

  LastTime :=ElapsedTime;
  ElapsedTime :=LCLIntf.GetTickCount64 - DemoStart;     // Calculate Elapsed Time
  ElapsedTime :=(LastTime + ElapsedTime)/2; // Average it out for smoother movement

  //.......................
  glTranslatef(0.0,0.0,-2.5);
  glRotatef(ElapsedTime/30, 0, 1, 0);
  glRotatef(ElapsedTime/20, 1, 0, 0);

  MetaBall[2].X :=0.4*sin(ElapsedTime/400) - 0.25*cos(ElapsedTime/600);
  MetaBall[2].Y :=0.45*cos(ElapsedTime/400) - 0.2*cos(ElapsedTime/600);
  MetaBall[3].X :=-0.4*cos(ElapsedTime/500) - 0.2*sin(ElapsedTime/600);
  MetaBall[3].Z :=0.4*sin(ElapsedTime/500) - 0.2*sin(ElapsedTime/400);

  TessTriangles := 0;
  For cx := 0 to GridSize do
    For cy := 0 to GridSize do
      For cz := 0 to GridSize do
        with Grid[cx, cy, cz] do
        begin
          Value :=0;
          for I :=1 to 3 do  // go through all meta balls
          begin
            with Metaball[I] do
              Value := Value + Radius*Radius /((P.x-x)*(P.x-x) + (P.y-y)*(P.y-y) + (P.z-z)*(P.z-z));
          end;
        end;

  // Draw the metaballs by drawing the triangle in each cube in the grid
  glBegin(GL_TRIANGLES);
    For cx := 0 to GridSize-1 do
      for cy := 0 to GridSize-1 do
        for cz := 0 to GridSize-1 do
          CreateCubeTriangles(Cubes[cx, cy, cz]);
  glEnd;

  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=@OpenGLPanel1Paint;
    OnResize:=@OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(@OnAppIdle);
  glInit;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
  OpenGLPanel1.SwapBuffers;
end;

end.

