{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit shaders1mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, OpenGLPanel, GL, GLU, OpenGL_Textures,
  TplTimerUnit,
  ctGL,
  OpenGL_Shaders;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject); 
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\6f_shaders1\shaders\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../6f_shaders1/shaders/';
{$ENDIF}


var

  OpenGLInitialized : Boolean;
  OpenGL3Initialized : Boolean;       // OpenGL 3.x initialized

  //shaders
  glslsimplevert: TGLSLShader;
  glslsimplefrag: TGLSLShader;
  glslsimpleprog: TGLSLProgram;

  //VAO VBO
  vaoID: array [0..1] of GLuint;
  vboID: array [0..1] of GLuint;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

  //use simple shader program
  glslsimpleprog.Enable;

  glBindVertexArray(vaoID[0]);		// select first VAO
	glDrawArrays(GL_TRIANGLES, 0, 3);	// draw first object

	glBindVertexArray(vaoID[1]);		// select second VAO
	glVertexAttrib3f(1, 1.0, 0.0, 0.0); // set constant color attribute
	glDrawArrays(GL_TRIANGLES, 0, 3);	// draw second object

	glBindVertexArray(0);

  glslsimpleprog.Disable; //back to fixed pipeline
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
var
  ln: integer;
  x: integer;
  y: integer;
  theta: GLFloat;
  phi: GLFloat;
  r: GLFloat;
  vert: array[0..8] of GLFloat;   //vertex array #1
  vert2: array[0..8] of GLFloat;  //Vertex array #2
  col: array[0..8] of GLFloat;    //Color array #1
begin

  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations
  //load simple glsl shader
  glslsimpleprog := TGLSLProgram.Create();
  glslsimplevert := TGLSLShader.Create(pathMedia+'minimal.vert');
  glslsimplefrag := TGLSLShader.Create(pathMedia+'minimal.frag', GL_FRAGMENT_SHADER_ARB);
  glslsimpleprog.Attach(glslsimplevert);
  glslsimpleprog.Attach(glslsimplefrag);

  glslsimpleprog.BindAttribLocation('in_Position',0);
  glslsimpleprog.BindAttribLocation('in_Color',1);

  glslsimpleprog.Link;

  // Create VAO's en VBO's and fill them with data

  // First simple object
  vert[0] :=-0.3; vert[1] := 0.5; vert[2] :=-1.0;
	vert[3] :=-0.8; vert[4] :=-0.5; vert[5] :=-1.0;
	vert[6] := 0.2; vert[7] :=-0.5; vert[8] :=-1.0;

	col[0] := 1.0; col[1] := 0.0; col[2] := 0.0;
	col[3] := 0.0; col[4] := 1.0; col[5] := 0.0;
	col[6] := 0.0; col[7] := 0.0; col[8] := 1.0;

	// Second simple object
	vert2[0] :=-0.2; vert2[1] := 0.5; vert2[2] :=-1.0;
	vert2[3] := 0.3; vert2[4] :=-0.5; vert2[5] :=-1.0;
	vert2[6] := 0.8; vert2[7] := 0.5; vert2[8] :=-1.0;

  // Two VAO allocation's
  glGenVertexArrays(2,@vaoID);

  // First VAO setup
  glBindVertexArray(vaoID[0]);

  glGenBuffers(2, @vboID);

  glBindBuffer(GL_ARRAY_BUFFER, vboID[0]);
  glBufferData(GL_ARRAY_BUFFER,9*sizeof(GLFloat), @vert, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, 9*sizeof(GLFloat), @col, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(1);

	// Second VAO setup
	glBindVertexArray(vaoID[1]);

	glGenBuffers(1, @vboID);

	glBindBuffer(GL_ARRAY_BUFFER, vboID[0]);
	glBufferData(GL_ARRAY_BUFFER, 9*sizeof(GLfloat), @vert2, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
  //.................................................

end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
  OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;

    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;  

  Application.AddOnIdleHandler(OnAppIdle);

end;

procedure TForm1.FormShow(Sender: TObject);
begin
  //============================== Initialaze ctGL

 OpenGL_Initialize;           // init opengl
 OpenGLPanel1.MakeCurrent();     // create/atrived OpenGLPanel1....
 OpenGL_InitializeAdvance;

 //====================================================
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end; 

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
 // OpenGLPanel1.SwapBuffers;
end;


end.

