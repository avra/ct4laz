{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo6mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;

// User Constants
const
  mat_specular : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
  mat_shininess : GLfloat = 50.0;
  light_position : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 0.0 );

var
 initOK:boolean=false;              //Build scene objects only one time
  // Quadric for sphere ( Dont worry too much about this now we'll handle this later)
 SphereQuadratic : pgluQuadricObj;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw;
begin

  glTranslatef(0.0,0.0,-10.0);          // Move the scene back 10 units so we can see the triangle properly

  glColor3f(1.0,0.0,0.0);               // Set our current rendering colour to red

  gluSphere(SphereQuadratic, 1.0, 32, 32); // Render the sphere to the scene

  // Flush the OpenGL Buffer
  glFlush();                            // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;

{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit;
begin
  if initOK=true then exit;

  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glShadeModel(GL_SMOOTH);          // Use Smooth shading ( This is the Default so we dont actually have to set it)

  // Generate a sphere using the OpenGL utility Library
  SphereQuadratic := gluNewQuadric;		       // Create A Pointer To The Quadric Object (Return 0 If No Memory) (NEW)
  gluQuadricNormals(SphereQuadratic, GLU_SMOOTH);	       // Create Smooth Normals (NEW)

  glMaterialfv(GL_FRONT, GL_SPECULAR, @mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, @mat_shininess);
  glLightfv(GL_LIGHT0, GL_POSITION, @light_position);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);

  initOK:=true;
end;

procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
 initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
  OpenGLPanel1.SwapBuffers;
end;

end.

