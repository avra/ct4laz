{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo32mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

 SphereQuadric : PGLUquadricObj;     // Quadric for our sphere

 // Control Point for Bezier Surface
  ctrlpoints : array [0..3] of array [0..3] of array[0..2] of GLfloat = (
    ((-1.5, -1.5, 4.0), (-0.5, -1.5, 2.0),
     (0.5, -1.5, -1.0), (1.5, -1.5, 2.0)),
    ((-1.5, -0.5, 1.0), (-0.5, -0.5, 3.0),
      (0.5, -0.5, 0.0), (1.5, -0.5, -1.0)),
    ((-1.5, 0.5, 4.0), (-0.5, 0.5, 0.0),
      (0.5, 0.5, 3.0), (1.5, 0.5, 4.0)),
    ((-1.5, 1.5, -2.0), (-0.5, 1.5, -2.0),
      (0.5, 1.5, 0.0), (1.5, 1.5, -1.0))
  );

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i, j : GLint;
begin
    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);     // Clear the Colour and Depth buffer
    glColor3f(1.0, 1.0, 1.0);                                // Set the primitive Colour
    glPushMatrix();                                          // Push a new Matrix onto the stack
    glRotatef(85.0, 1.0, 1.0, 1.0);                          // Rotate the scene 85 degrees on all axis
    for j := 0 to 8 do                                       // Draw Vertical and Horizontal Lines
    begin
        glBegin(GL_LINE_STRIP);                              // Start Drawing A Line strip
            for i := 0 to 29 do                              // Draw Vertical Lines
                glEvalCoord2f(i/29.0, j/8.0);                // Evaluate the given coordinates u and v
        glEnd();                                             // Stop Drawing a line strip
        glBegin(GL_LINE_STRIP);                              // Start Drawing a line strip
            for i := 0 to 29 do                              // Draw Horizontal Lines
                glEvalCoord2f(j/8.0, i/29.0);                // Evaluate the given coordinates u and v
        glEnd();                                             // Stop Drawing a line strip
    end;
    glPopMatrix();                                           // Restore the Previous Matrix

    glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;



{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================

  glClearColor (0.0, 0.0, 0.0, 1.0);                    // Set the Background colour
  glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4,
     0, 1, 12, 4, @ctrlpoints[0][0][0]);                // Set the control points for the Bezier Surface
  glEnable(GL_MAP2_VERTEX_3);                           // Enable Vertex Mapping
  glEnable(GL_DEPTH_TEST);                              // Enable Depth Testing
  glShadeModel(GL_FLAT);                                // Do Not shade the model

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

  glViewport(0, 0, Width, Height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (Width <= Height) then
      glOrtho(-5.0, 5.0, -5.0*Height/Width,5.0*Height/Width, -5.0, 5.0)
  else
      glOrtho(-5.0*Width/Height, 5.0*Width/Height, -5.0, 5.0, -5.0, 5.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

