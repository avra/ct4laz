{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo33mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


const
  // Lighting

  ambient : array [0..3] of GLfloat = ( 0.2, 0.2, 0.2, 1.0 );
  position : array [0..3] of GLfloat = ( 0.0, 0.0, 2.0, 1.0 );
  mat_diffuse : array [0..3] of GLfloat = ( 0.6, 0.6, 0.6, 1.0 );
  mat_specular : array [0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
  mat_shininess : GLfloat =  50.0 ;

var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

 SphereQuadric : PGLUquadricObj;     // Quadric for our sphere

 // Control Points for Bezier Surface
  ctrlpoints : array [0..3] of array [0..3] of array[0..2] of GLfloat = (
    ((-1.5, -1.5, 4.0), (-0.5, -1.5, 2.0),
     (0.5, -1.5, -1.0), (1.5, -1.5, 2.0)),
    ((-1.5, -0.5, 1.0), (-0.5, -0.5, 3.0),
      (0.5, -0.5, 0.0), (1.5, -0.5, -1.0)),
    ((-1.5, 0.5, 4.0), (-0.5, 0.5, 0.0),
      (0.5, 0.5, 3.0), (1.5, 0.5, 4.0)),
    ((-1.5, 1.5, -2.0), (-0.5, 1.5, -2.0),
      (0.5, 1.5, 0.0), (1.5, 1.5, -1.0))
  );

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Initialise Lighting and Material                                }
{------------------------------------------------------------------}
procedure InitLights();
begin                                                            // Enable Lighting
    glEnable(GL_LIGHTING);                                       // Enable Light 0
    glEnable(GL_LIGHT0);

    glLightfv(GL_LIGHT0, GL_AMBIENT, @ambient);                  // The the Ambient value for the light
    glLightfv(GL_LIGHT0, GL_POSITION, @position);                // Set the Position of the light
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, @mat_diffuse);   // Set the Diffuse Material
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, @mat_specular); // Set the Specular Material
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS, @mat_shininess);// Set the Shininess of the Material
end;



{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i, j : GLint;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);         // Clear the Colour and Depth Buffer
  glPushMatrix();                                              // Copy the current matrix and add it to the stack
      glRotatef(85.0, 1.0, 1.0, 1.0);                          // Rotate the scene
      glEvalMesh2(GL_FILL, 0, 8, 0, 8);                        // Draw A Filled Bezier Surface
  glPopMatrix();                                               // Revert back to the saved matrix

  glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================
  glClearColor (0.0, 0.0, 0.0, 1.0);                          // Set the background colour to black
  glEnable(GL_DEPTH_TEST);                                    // Enable Depth testing
  glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4,                       // Set the control points for the bezier surface
      0, 1, 12, 4, @ctrlpoints[0][0][0]);
  glEnable(GL_MAP2_VERTEX_3);                                 // Enable Vertex gereration
  glEnable(GL_AUTO_NORMAL);                                   // Generate Normals Automatically
  glMapGrid2f(8, 0.0, 1.0, 8, 0.0, 1.0);                      // Set the grid for the Bezier surface
  InitLights();                                               // Setup Lights and Materials


   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

  glViewport(0, 0, Width, Height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (Width <= Height) then
      glOrtho(-5.0, 5.0, -5.0*Height/Width,5.0*Height/Width, -5.0, 5.0)
  else
      glOrtho(-5.0*Width/Height, 5.0*Width/Height, -5.0, 5.0, -5.0, 5.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

