unit librarytestctglumw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ctutils,
  ExtCtrls, ctGLU;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    procedure Button1Click(Sender: TObject);
  private
    procedure TestLibFunctions;
    function  DGLWriteFunReport(aOutStrings:TStrings): Boolean;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

Function PathLibrary:string;
begin
{$IFDEF Windows}
  Result := ctGetRuntimesCPUOSDir(true);
{$ELSE}
  Result := '';
{$ENDIF}
end;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
 Memo1.Clear;
  if Not OpenGLULib_Initialize then
  begin
   Memo1.Lines.add('ERROR: '+GLU_Lib+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(GLU_Lib+' Loaded OK');
  end;

  TestLibFunctions;
end;

procedure TForm1.TestLibFunctions;
begin

 Memo1.Lines.add('--------------------');
 DGLWriteFunReport(Memo1.Lines);
end;


function TForm1.DGLWriteFunReport(aOutStrings:TStrings): Boolean;     
//................................................
procedure _writetostrings(const isok:boolean;const astr:string);
begin
  case RadioGroup1.ItemIndex of
   0:begin
      Memo1.Lines.add(astr);
      exit;
   end;
  1: begin
     if isok=false then
       Memo1.Lines.add(astr); 
     exit;
   end;

   2:begin
     if isok=True then
       Memo1.Lines.add(astr);
     exit;
     end;

  end;
end;
//.................................................
 begin
   Result:=true;
   if aOutStrings=nil then exit;

   aOutStrings.BeginUpdate;
  //---- Load  Procs ---------------------

    if gluErrorString=nil then _writetostrings(False,'gluErrorString is NIL ???') else _writetostrings(True,'gluErrorString Is OK');
    if gluGetString=nil then _writetostrings(False,'gluGetString is NIL ???') else _writetostrings(True,'gluGetString Is OK');
    if gluOrtho2D=nil then _writetostrings(False,'gluOrtho2D is NIL ???') else _writetostrings(True,'gluOrtho2D Is OK');
    if gluPerspective=nil then _writetostrings(False,'gluPerspective is NIL ???') else _writetostrings(True,'gluPerspective Is OK');
    if gluPickMatrix=nil then _writetostrings(False,'gluPickMatrix is NIL ???') else _writetostrings(True,'gluPickMatrix Is OK');
    if gluLookAt=nil then _writetostrings(False,'gluLookAt is NIL ???') else _writetostrings(True,'gluLookAt Is OK');
    if gluProject=nil then _writetostrings(False,'gluProject is NIL ???') else _writetostrings(True,'gluProject Is OK');
    if gluUnProject=nil then _writetostrings(False, 'gluUnProject is NIL ???') else _writetostrings(True,'gluUnProject Is OK');
    if gluBuild1DMipmaps =nil then _writetostrings(False,'gluBuild1DMipmaps is NIL ???') else _writetostrings(True,'gluBuild1DMipmaps Is OK');
    if gluBuild2DMipmaps=nil then _writetostrings(False,'gluBuild2DMipmaps is NIL ???') else _writetostrings(True,'gluBuild2DMipmaps Is OK');
    if gluNewQuadric =nil then _writetostrings(False,'gluNewQuadric is NIL ???') else _writetostrings(True,'gluNewQuadric Is OK');
    if gluDeleteQuadric=nil then _writetostrings(False,'gluDeleteQuadric is NIL ???') else _writetostrings(True,'gluDeleteQuadric Is OK');
    if gluQuadricNormals=nil then _writetostrings(False,'gluQuadricNormals is NIL ???') else _writetostrings(True,'gluQuadricNormals Is OK');
    if gluQuadricTexture =nil then _writetostrings(False,'gluQuadricTexture is NIL ???') else _writetostrings(True,'gluQuadricTexture Is OK');
    if gluQuadricOrientation =nil then _writetostrings(False,'gluQuadricOrientation is NIL ???') else _writetostrings(True,'gluQuadricOrientation Is OK');
    if gluQuadricDrawStyle=nil then _writetostrings(False,'gluQuadricDrawStyle is NIL ???') else _writetostrings(True,'gluQuadricDrawStyle Is OK');
    if gluCylinder=nil then _writetostrings(False,'gluCylinder is NIL ???') else _writetostrings(True,'gluCylinder Is OK');
    if gluDisk=nil then _writetostrings(False,'gluDisk is NIL ???') else _writetostrings(True,'gluDisk Is OK');
    if gluPartialDisk=nil then _writetostrings(False,'gluPartialDisk is NIL ???') else _writetostrings(True,'gluPartialDisk Is OK');
    if gluSphere=nil then _writetostrings(False,'gluSphere is NIL ???') else _writetostrings(True,'gluSphere Is OK');
    if gluQuadricCallback=nil then _writetostrings(False,'gluQuadricCallback is NIL ???') else _writetostrings(True,'gluQuadricCallback Is OK');
    if gluNewTess=nil then _writetostrings(False,'gluNewTess is NIL ???') else _writetostrings(True,'gluNewTess Is OK');
    if gluDeleteTess=nil then _writetostrings(False,'gluDeleteTess is NIL ???') else _writetostrings(True,'gluDeleteTess Is OK');
    if gluTessBeginPolygon=nil then _writetostrings(False,'gluTessBeginPolygon is NIL ???') else _writetostrings(True,'gluTessBeginPolygon Is OK');
    if gluTessBeginContour=nil then _writetostrings(False,'gluTessBeginContour is NIL ???') else _writetostrings(True,'gluTessBeginContour Is OK');
    if gluTessVertex=nil then _writetostrings(False,'gluTessVertex is NIL ???') else _writetostrings(True,'gluTessVertex Is OK');
    if gluTessEndContour=nil then _writetostrings(False,'gluTessEndContour is NIL ???') else _writetostrings(True,'gluTessEndContour Is OK');
    if gluTessEndPolygon=nil then _writetostrings(False,'gluTessEndPolygon is NIL ???') else _writetostrings(True,'gluTessEndPolygon Is OK');
    if gluTessProperty=nil then _writetostrings(False,'gluTessProperty is NIL ???') else _writetostrings(True,'gluTessProperty Is OK');
    if gluTessNormal=nil then _writetostrings(False,'gluTessNormal is NIL ???') else _writetostrings(True,'gluTessNormal Is OK');
    if gluTessCallback=nil then _writetostrings(False,'gluTessCallback is NIL ???') else _writetostrings(True,'gluTessCallback Is OK');
    if gluGetTessProperty=nil then _writetostrings(False,'gluGetTessProperty is NIL ???') else _writetostrings(True,'gluGetTessProperty Is OK');
    if gluNewNurbsRenderer=nil then _writetostrings(False,'gluNewNurbsRenderer is NIL ???') else _writetostrings(True,'gluNewNurbsRenderer Is OK');
    if gluDeleteNurbsRenderer=nil then _writetostrings(False,'gluDeleteNurbsRenderer is NIL ???') else _writetostrings(True,'gluDeleteNurbsRenderer Is OK');
    if gluBeginSurface=nil then _writetostrings(False,'gluBeginSurface is NIL ???') else _writetostrings(True,'gluBeginSurface Is OK');
    if gluBeginCurve=nil then _writetostrings(False,'gluBeginCurve is NIL ???') else _writetostrings(True,'gluBeginCurve Is OK');
    if gluEndCurve=nil then _writetostrings(False,'gluEndCurve is NIL ???') else _writetostrings(True,'gluEndCurve Is OK');
    if gluEndSurface=nil then _writetostrings(False,'gluEndSurface is NIL ???') else _writetostrings(True,'gluEndSurface Is OK');
    if gluBeginTrim=nil then _writetostrings(False,'gluBeginTrim is NIL ???') else _writetostrings(True,'gluBeginTrim Is OK');
    if gluEndTrim=nil then _writetostrings(False,'gluEndTrim is NIL ???') else _writetostrings(True,'gluEndTrim Is OK');
    if gluPwlCurve=nil then _writetostrings(False,'gluPwlCurve is NIL ???') else _writetostrings(True,'gluPwlCurve Is OK');
    if gluNurbsCurve=nil then _writetostrings(False,'gluNurbsCurve is NIL ???') else _writetostrings(True,'gluNurbsCurve Is OK');
    if gluNurbsSurface=nil then _writetostrings(False,'gluNurbsSurface is NIL ???') else _writetostrings(True,'gluNurbsSurface Is OK');
    if gluLoadSamplingMatrices=nil then _writetostrings(False,'gluLoadSamplingMatrices is NIL ???') else _writetostrings(True,'gluLoadSamplingMatrices Is OK');
    if gluNurbsProperty=nil then _writetostrings(False,'gluNurbsProperty is NIL ???') else _writetostrings(True,'gluNurbsProperty Is OK');
    if gluGetNurbsProperty=nil then _writetostrings(False,'gluGetNurbsProperty is NIL ???') else _writetostrings(True,'gluGetNurbsProperty Is OK');
    if gluNurbsCallback=nil then _writetostrings(False,'gluNurbsCallback is NIL ???') else _writetostrings(True,'gluNurbsCallback Is OK');
    if gluBeginPolygon=nil then _writetostrings(False,'gluBeginPolygon is NIL ???') else _writetostrings(True,'gluBeginPolygon Is OK');
    if gluNextContour=nil then _writetostrings(False,'gluNextContour is NIL ???') else _writetostrings(True,'gluNextContour Is OK');
    if gluEndPolygon=nil then _writetostrings(False,'gluEndPolygon is NIL ???') else _writetostrings(True,'gluEndPolygon Is OK');
    if gluBuild1DMipmapLevels=nil then _writetostrings(False,'gluBuild1DMipmapLevels is NIL ???') else _writetostrings(True,'gluBuild1DMipmapLevels Is OK');
    if gluBuild2DMipmapLevels=nil then _writetostrings(False,'gluBuild2DMipmapLevels is NIL ???') else _writetostrings(True,'gluBuild2DMipmapLevels Is OK');
    if gluBuild3DMipmapLevels=nil then _writetostrings(False,'gluBuild3DMipmapLevels is NIL ???') else _writetostrings(True,'gluBuild3DMipmapLevels Is OK');
    if gluBuild3DMipmaps=nil then _writetostrings(False,'gluBuild3DMipmaps is NIL ???') else _writetostrings(True,'gluBuild3DMipmaps Is OK');
    if gluCheckExtension =nil then _writetostrings(False,'gluCheckExtension is NIL ???') else _writetostrings(True,'gluCheckExtension Is OK');
    if gluNurbsCallbackData=nil then _writetostrings(False,'gluNurbsCallbackData is NIL ???') else _writetostrings(True,'gluNurbsCallbackData Is OK');
    if gluNurbsCallbackDataEXT=nil then _writetostrings(False,'gluNurbsCallbackDataEXT is NIL ???') else _writetostrings(True,'gluNurbsCallbackDataEXT Is OK');
    if gluUnProject4=nil then _writetostrings(False,'gluUnProject4 is NIL ???') else _writetostrings(True,'gluUnProject4 Is OK');
    if gluQuadricErrorProc=nil then _writetostrings(False,'gluQuadricErrorProc is NIL ???') else _writetostrings(True,'gluQuadricErrorProc Is OK');
    if gluTessBeginProc=nil then _writetostrings(False,'gluTessBeginProc is NIL ???') else _writetostrings(True,'gluTessBeginProc Is OK');
    if gluTessEdgeFlagProc=nil then _writetostrings(False,'gluTessEdgeFlagProc is NIL ???') else _writetostrings(True,'gluTessEdgeFlagProc Is OK');
    if gluTessVertexProc=nil then _writetostrings(False,'gluTessVertexProc is NIL ???') else _writetostrings(True,'gluTessVertexProc Is OK');
    if gluTessEndProc =nil then _writetostrings(False,'gluTessEndProc is NIL ???') else _writetostrings(True,'gluTessEndProc Is OK');
    if gluTessErrorProc=nil then _writetostrings(False,'gluTessErrorProc is NIL ???') else _writetostrings(True,'gluTessErrorProc Is OK');
    if gluTessCombineProc=nil then _writetostrings(False,'gluTessCombineProc is NIL ???') else _writetostrings(True,'gluTessCombineProc Is OK');
    if gluTessBeginDataProc=nil then _writetostrings(False,'gluTessBeginDataProc is NIL ???') else _writetostrings(True,'gluTessBeginDataProc Is OK');
    if gluTessEdgeFlagDataProc=nil then _writetostrings(False,'gluTessEdgeFlagDataProc is NIL ???') else _writetostrings(True,'gluTessEdgeFlagDataProc Is OK');
    if gluTessVertexDataProc=nil then _writetostrings(False,'gluTessVertexDataProc is NIL ???') else _writetostrings(True,'gluTessVertexDataProc Is OK');
    if gluTessEndDataProc=nil then _writetostrings(False,'gluTessEndDataProc is NIL ???') else _writetostrings(True,'gluTessEndDataProc Is OK');
    if gluTessErrorDataProc=nil then _writetostrings(False,'gluTessErrorDataProc is NIL ???') else _writetostrings(True,'gluTessErrorDataProc Is OK');
    if gluTessCombineDataProc=nil then _writetostrings(False,'gluTessCombineDataProc is NIL ???') else _writetostrings(True,'gluTessCombineDataProc Is OK');
    if gluNurbsErrorProc=nil then _writetostrings(False,'gluNurbsErrorProc is NIL ???') else _writetostrings(True,'gluNurbsErrorProc Is OK');
    if gluScaleImage=nil then _writetostrings(False,'gluScaleImage is NIL ???') else _writetostrings(True,'gluScaleImage Is OK');


   aOutStrings.EndUpdate;

end;


end.

