{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo13mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

const

 // User Constants
  X = 0.525731112119133606;
  Z = 0.850650808352039932;

  vdata : array [0..11] of array [0..2] of GLfloat = (
   (-X, 0.0, Z), (X, 0.0, Z), (-X, 0.0, -Z), (X, 0.0, -Z),
   (0.0, Z, X), (0.0, Z, -X), (0.0, -Z, X), (0.0, -Z, -X),
   (Z, X, 0.0), (-Z, X, 0.0), (Z, -X, 0.0), (-Z, -X, 0.0)
    );

  tindices : array [0..19] of array [0..2] of GLint = (
   (0,4,1), (0,9,4), (9,5,4), (4,5,8), (4,8,1),
   (8,10,1), (8,3,10),(5,3,8), (5,2,3), (2,7,3),
   (7,10,3), (7,6,10), (7,11,6), (11,0,6), (0,1,6),
   (6,1,10), (9,0,11), (9,11,2), (9,2,5), (7,2,11)
   );

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by
 DrawShaded : boolean = true;

  light_ambient : array[0..3] of GLfloat = ( 0.3, 0.3, 0.3, 1.0 );
  light_diffuse : array[0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
  light_specular : array[0..3] of GLfloat = ( 1.0, 1.0, 1.0, 1.0 );
  light_position : array[0..3] of GLfloat = ( 5.0, 40.0, -4.0, 2.0 );

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;

{------------------------------------------------------------------}
{  Draw one triangle                                               }
{------------------------------------------------------------------}
procedure DrawTriangle(v1, v2, v3 : array of GLfloat);
begin
   glBegin(GL_POLYGON);
      glNormal3fv(@v1); glVertex3fv(@v1);
      glNormal3fv(@v2); glVertex3fv(@v2);
      glNormal3fv(@v3); glVertex3fv(@v3);
   glEnd();
end;


procedure Normalize(out v : array of GLFloat);
var d : GLfloat;
begin
  d := sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
   if (d = 0.0) then
   begin
      // Error
      exit;
   end;
   v[0] := v[0] / d;
   v[1] := v[1] / d;
   v[2] := v[2] / d;

end;

procedure NormCrossProd(v1, v2 : array of GLfloat;out vout : array of GLFLoat);
begin
   vout[0] := v1[1]*v2[2] - v1[2]*v2[1];
   vout[1] := v1[2]*v2[0] - v1[0]*v2[2];
   vout[2] := v1[0]*v2[1] - v1[1]*v2[0];
   Normalize(vout);
end;


procedure Subdivide(v1, v2, v3 : array of GLfloat);
var
  v12 : array[0..2] of GLfloat;
  v23 : array[0..2] of GLfloat;
  v31 : array[0..2] of GLfloat;
  i : GLint;
begin

   for i := 0 to 2 do
   begin
      v12[i] := v1[i]+ v2[i];
      v23[i] := v2[i]+ v3[i];
      v31[i] := v3[i]+ v1[i];
   end;
   normalize(v12);
   normalize(v23);
   normalize(v31);
   DrawTriangle(v1, v12, v31);
   DrawTriangle(v2, v23, v12);
   DrawTriangle(v3, v31, v23);
   DrawTriangle(v12, v23, v31);

end;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i : GLint;
begin
  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glClear (GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear the colour and depth buffer
  glLoadIdentity();                 // Load a "clean" model matrix on the stack

  glTranslatef(0.0,0.0,-10.0);
  SpinDisplay();
  glRotatef(spin,-1.0,-1.0,-1.0);      // Rotate the Icosahedron so that we can visualise it properly
  glColor3f(1.0,1.0,1.0);

  for i := 19 downto 0 do
  begin
     Subdivide(vdata[tindices[i][0]],
            vdata[tindices[i][1]],
            vdata[tindices[i][2]]);
  end;
  // Flush the OpenGL Buffer
  glFlush();                        // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //===========================================================

  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glColor3f(1.0, 1.0, 1.0);         // Set the Polygon colour to white
  glEnable(GL_DEPTH_TEST);          // Enable Depth testing
  glDepthFunc(GL_LEQUAL);
  glShadeModel(GL_SMOOTH);            // Use smooth shading
  glHint(GL_SHADE_MODEL,GL_NICEST);   // Set the smooth shading to the best we can have

  // Enable Lighting ( This will be explained in a later chapter)
  glLightfv(GL_LIGHT0, GL_AMBIENT, @light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, @light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR,@light_specular);
  glLightfv(GL_LIGHT0, GL_POSITION,@light_position);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);

  //===========================================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

