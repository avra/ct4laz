{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo21mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

const

 // User Constants

 fly : array [0..127] of GLubyte = (
        $00, $00, $00, $00, $00, $00, $00, $00,
        $03, $80, $01, $C0, $06, $C0, $03, $60,
        $04, $60, $06, $20, $04, $30, $0C, $20,
        $04, $18, $18, $20, $04, $0C, $30, $20,
        $04, $06, $60, $20, $44, $03, $C0, $22,
        $44, $01, $80, $22, $44, $01, $80, $22,
        $44, $01, $80, $22, $44, $01, $80, $22,
        $44, $01, $80, $22, $44, $01, $80, $22,
        $66, $01, $80, $66, $33, $01, $80, $CC,
        $19, $81, $81, $98, $0C, $C1, $83, $30,
        $07, $e1, $87, $e0, $03, $3f, $fc, $c0,
        $03, $31, $8c, $c0, $03, $33, $cc, $c0,
        $06, $64, $26, $60, $0c, $cc, $33, $30,
        $18, $cc, $33, $18, $10, $c4, $23, $08,
        $10, $63, $C6, $08, $10, $30, $0c, $08,
        $10, $18, $18, $08, $10, $00, $00, $08);

    halftone : array [0..127] of GLubyte = (
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55,
        $AA, $AA, $AA, $AA, $55, $55, $55, $55);

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;



{------------------------------------------------------------------}
{  Draw one line from [x1, y1] to [x2,y2]                          }
{------------------------------------------------------------------}
procedure DrawOneLine(x1,y1,x2,y2 : GLfloat);
begin
 glBegin(GL_LINES);
   glVertex2f (x1, y1);
   glVertex2f (x2, y2);
 glEnd();
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i : GLint;
begin
  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glClear (GL_COLOR_BUFFER_BIT);     // Clear the colour and depth buffer
  glLoadIdentity();                 // Load a "clean" model matrix on the stack

  glColor3f (1.0, 1.0, 1.0);       // Set the fill colour of the polygon to white
  glRectf (1.0, 1.0, 10.0, 10.0);  // Draw a rectangle to the scene
                                   // A rectangle is defined as Left, Bottom, Top, Right
  // NB: This is not the best way of doing line stippling.
  // Instead of this use a Display list
  glEnable (GL_POLYGON_STIPPLE);   // Enable line stipple
  glPolygonStipple (@fly);         // Load the stipple we defined in our "fly" constant array
  glRectf (10.0, 1.0, 20.0, 10.0); // Draw a rectangle to the scene

  glPolygonStipple (@halftone);    // Load the stipple we defined in our "Halftone" constant array
  glRectf (20.0, 1.0, 30.0, 10.0); // Draw a rectangle to the scene
  glDisable (GL_POLYGON_STIPPLE);  // Disable line stipple so we dont raw any other primitives with the last loaded stipple

  // Flush the OpenGL Buffer
  glFlush();                        // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================


  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glColor3f(1.0, 1.0, 1.0);         // Set the Polygon colour to white

  glShadeModel(GL_FLAT);            // Use flat shading

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  // This is one way to set the perspective when a window get resized.
  // But this is not the only way. Another way will be handled in a later tutorial

  glViewport(0, 0, Width, Height);                    // Set the viewport for the OpenGL window
  glMatrixMode(GL_PROJECTION);                        // Change Matrix Mode to Projection
  glLoadIdentity();                                   // Reset View ( Load Thye default projection matrix onto the stack)
  if (Width <= Height) then
      glOrtho (-50.0, 50.0, -50.0*Height/Width,
          50.0*Height/Width, -1.0, 1.0)               // Set up orthographic view
  else
      glOrtho (-50.0*Width/Height,
          50.0*Width/Height, -50.0, 50.0, -1.0, 1.0); // Set up orthographic view
  glMatrixMode(GL_MODELVIEW);                         // Return to the modelview matrix
  glLoadIdentity();                                  // Reset View ( Load the default Modelview matrix onto the stack)

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

