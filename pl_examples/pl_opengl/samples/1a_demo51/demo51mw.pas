{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo51mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);   
    procedure OpenGLPanel1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private

  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


const
  // User Constants

  // Definition of a complete font
   rasters :array[0..94] of array[0..12] of GLubyte = (
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00),
    ($00, $00, $18, $18, $00, $00, $18, $18, $18, $18, $18, $18, $18),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $36, $36, $36, $36),
    ($00, $00, $00, $66, $66, $ff, $66, $66, $ff, $66, $66, $00, $00),
    ($00, $00, $18, $7e, $ff, $1b, $1f, $7e, $f8, $d8, $ff, $7e, $18),
    ($00, $00, $0e, $1b, $db, $6e, $30, $18, $0c, $76, $db, $d8, $70),
    ($00, $00, $7f, $c6, $cf, $d8, $70, $70, $d8, $cc, $cc, $6c, $38),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $18, $1c, $0c, $0e),
    ($00, $00, $0c, $18, $30, $30, $30, $30, $30, $30, $30, $18, $0c),
    ($00, $00, $30, $18, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $18, $30),
    ($00, $00, $00, $00, $99, $5a, $3c, $ff, $3c, $5a, $99, $00, $00),
    ($00, $00, $00, $18, $18, $18, $ff, $ff, $18, $18, $18, $00, $00),
    ($00, $00, $30, $18, $1c, $1c, $00, $00, $00, $00, $00, $00, $00),
    ($00, $00, $00, $00, $00, $00, $ff, $ff, $00, $00, $00, $00, $00),
    ($00, $00, $00, $38, $38, $00, $00, $00, $00, $00, $00, $00, $00),
    ($00, $60, $60, $30, $30, $18, $18, $0c, $0c, $06, $06, $03, $03),
    ($00, $00, $3c, $66, $c3, $e3, $f3, $db, $cf, $c7, $c3, $66, $3c),
    ($00, $00, $7e, $18, $18, $18, $18, $18, $18, $18, $78, $38, $18),
    ($00, $00, $ff, $c0, $c0, $60, $30, $18, $0c, $06, $03, $e7, $7e),
    ($00, $00, $7e, $e7, $03, $03, $07, $7e, $07, $03, $03, $e7, $7e),
    ($00, $00, $0c, $0c, $0c, $0c, $0c, $ff, $cc, $6c, $3c, $1c, $0c),
    ($00, $00, $7e, $e7, $03, $03, $07, $fe, $c0, $c0, $c0, $c0, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $c7, $fe, $c0, $c0, $c0, $e7, $7e),
    ($00, $00, $30, $30, $30, $30, $18, $0c, $06, $03, $03, $03, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $e7, $7e, $e7, $c3, $c3, $e7, $7e),
    ($00, $00, $7e, $e7, $03, $03, $03, $7f, $e7, $c3, $c3, $e7, $7e),
    ($00, $00, $00, $38, $38, $00, $00, $38, $38, $00, $00, $00, $00),
    ($00, $00, $30, $18, $1c, $1c, $00, $00, $1c, $1c, $00, $00, $00),
    ($00, $00, $06, $0c, $18, $30, $60, $c0, $60, $30, $18, $0c, $06),
    ($00, $00, $00, $00, $ff, $ff, $00, $ff, $ff, $00, $00, $00, $00),
    ($00, $00, $60, $30, $18, $0c, $06, $03, $06, $0c, $18, $30, $60),
    ($00, $00, $18, $00, $00, $18, $18, $0c, $06, $03, $c3, $c3, $7e),
    ($00, $00, $3f, $60, $cf, $db, $d3, $dd, $c3, $7e, $00, $00, $00),
    ($00, $00, $c3, $c3, $c3, $c3, $ff, $c3, $c3, $c3, $66, $3c, $18),
    ($00, $00, $fe, $c7, $c3, $c3, $c7, $fe, $c7, $c3, $c3, $c7, $fe),
    ($00, $00, $7e, $e7, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $e7, $7e),
    ($00, $00, $fc, $ce, $c7, $c3, $c3, $c3, $c3, $c3, $c7, $ce, $fc),
    ($00, $00, $ff, $c0, $c0, $c0, $c0, $fc, $c0, $c0, $c0, $c0, $ff),
    ($00, $00, $c0, $c0, $c0, $c0, $c0, $c0, $fc, $c0, $c0, $c0, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $cf, $c0, $c0, $c0, $c0, $e7, $7e),
    ($00, $00, $c3, $c3, $c3, $c3, $c3, $ff, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $7e, $18, $18, $18, $18, $18, $18, $18, $18, $18, $7e),
    ($00, $00, $7c, $ee, $c6, $06, $06, $06, $06, $06, $06, $06, $06),
    ($00, $00, $c3, $c6, $cc, $d8, $f0, $e0, $f0, $d8, $cc, $c6, $c3),
    ($00, $00, $ff, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $c0, $c0),
    ($00, $00, $c3, $c3, $c3, $c3, $c3, $c3, $db, $ff, $ff, $e7, $c3),
    ($00, $00, $c7, $c7, $cf, $cf, $df, $db, $fb, $f3, $f3, $e3, $e3),
    ($00, $00, $7e, $e7, $c3, $c3, $c3, $c3, $c3, $c3, $c3, $e7, $7e),
    ($00, $00, $c0, $c0, $c0, $c0, $c0, $fe, $c7, $c3, $c3, $c7, $fe),
    ($00, $00, $3f, $6e, $df, $db, $c3, $c3, $c3, $c3, $c3, $66, $3c),
    ($00, $00, $c3, $c6, $cc, $d8, $f0, $fe, $c7, $c3, $c3, $c7, $fe),
    ($00, $00, $7e, $e7, $03, $03, $07, $7e, $e0, $c0, $c0, $e7, $7e),
    ($00, $00, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $ff),
    ($00, $00, $7e, $e7, $c3, $c3, $c3, $c3, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $18, $3c, $3c, $66, $66, $c3, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $c3, $e7, $ff, $ff, $db, $db, $c3, $c3, $c3, $c3, $c3),
    ($00, $00, $c3, $66, $66, $3c, $3c, $18, $3c, $3c, $66, $66, $c3),
    ($00, $00, $18, $18, $18, $18, $18, $18, $3c, $3c, $66, $66, $c3),
    ($00, $00, $ff, $c0, $c0, $60, $30, $7e, $0c, $06, $03, $03, $ff),
    ($00, $00, $3c, $30, $30, $30, $30, $30, $30, $30, $30, $30, $3c),
    ($00, $03, $03, $06, $06, $0c, $0c, $18, $18, $30, $30, $60, $60),
    ($00, $00, $3c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $3c),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $c3, $66, $3c, $18),
    ($ff, $ff, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00, $00),
    ($00, $00, $00, $00, $00, $00, $00, $00, $00, $18, $38, $30, $70),
    ($00, $00, $7f, $c3, $c3, $7f, $03, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $fe, $c3, $c3, $c3, $c3, $fe, $c0, $c0, $c0, $c0, $c0),
    ($00, $00, $7e, $c3, $c0, $c0, $c0, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $7f, $c3, $c3, $c3, $c3, $7f, $03, $03, $03, $03, $03),
    ($00, $00, $7f, $c0, $c0, $fe, $c3, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $30, $30, $30, $30, $30, $fc, $30, $30, $30, $33, $1e),
    ($7e, $c3, $03, $03, $7f, $c3, $c3, $c3, $7e, $00, $00, $00, $00),
    ($00, $00, $c3, $c3, $c3, $c3, $c3, $c3, $fe, $c0, $c0, $c0, $c0),
    ($00, $00, $18, $18, $18, $18, $18, $18, $18, $00, $00, $18, $00),
    ($38, $6c, $0c, $0c, $0c, $0c, $0c, $0c, $0c, $00, $00, $0c, $00),
    ($00, $00, $c6, $cc, $f8, $f0, $d8, $cc, $c6, $c0, $c0, $c0, $c0),
    ($00, $00, $7e, $18, $18, $18, $18, $18, $18, $18, $18, $18, $78),
    ($00, $00, $db, $db, $db, $db, $db, $db, $fe, $00, $00, $00, $00),
    ($00, $00, $c6, $c6, $c6, $c6, $c6, $c6, $fc, $00, $00, $00, $00),
    ($00, $00, $7c, $c6, $c6, $c6, $c6, $c6, $7c, $00, $00, $00, $00),
    ($c0, $c0, $c0, $fe, $c3, $c3, $c3, $c3, $fe, $00, $00, $00, $00),
    ($03, $03, $03, $7f, $c3, $c3, $c3, $c3, $7f, $00, $00, $00, $00),
    ($00, $00, $c0, $c0, $c0, $c0, $c0, $e0, $fe, $00, $00, $00, $00),
    ($00, $00, $fe, $03, $03, $7e, $c0, $c0, $7f, $00, $00, $00, $00),
    ($00, $00, $1c, $36, $30, $30, $30, $30, $fc, $30, $30, $30, $00),
    ($00, $00, $7e, $c6, $c6, $c6, $c6, $c6, $c6, $00, $00, $00, $00),
    ($00, $00, $18, $3c, $3c, $66, $66, $c3, $c3, $00, $00, $00, $00),
    ($00, $00, $c3, $e7, $ff, $db, $c3, $c3, $c3, $00, $00, $00, $00),
    ($00, $00, $c3, $66, $3c, $18, $3c, $66, $c3, $00, $00, $00, $00),
    ($c0, $60, $60, $30, $18, $3c, $66, $66, $c3, $00, $00, $00, $00),
    ($00, $00, $ff, $60, $30, $18, $0c, $06, $ff, $00, $00, $00, $00),
    ($00, $00, $0f, $18, $18, $18, $38, $f0, $38, $18, $18, $18, $0f),
    ($18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18, $18),
    ($00, $00, $f0, $18, $18, $18, $1c, $0f, $1c, $18, $18, $18, $f0),
    ($00, $00, $00, $00, $00, $00, $06, $8f, $f1, $60, $00, $00, $00)

);


  BUFSIZE = 512;                     // Size of the selection buffer

var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

  // Mouse
  MouseButton : Integer = -1;         // mouse button down
  Xcoord, Ycoord : Integer;   // Mouse Coordinates

 // Font
  fontOffset : GLuint;                // Offset for the Display list

  Output : array of string;           // Output string array for our text

  board : array [0..2] of array [0..2] of GLint;     // Amount of color for each square

  GWidth, GHeight : integer;          // Clobal Width and height of our window

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Create the display lists for our raster font                    }
{------------------------------------------------------------------}
procedure MakeRasterFont;
var i : GLuint;
begin
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);      // set the bit alignment
    fontOffset := glGenLists (128);             // generate 128 display lists
    for i := 32 to 126 do
    begin
        glNewList(i+fontOffset, GL_COMPILE);    // Create but dont execute a list
            glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, @rasters[i-32]); // Add a character to the current Display list
        glEndList();                            // End the display list creation
    end;
end;

{------------------------------------------------------------------}
{  Draw a single Triangle                                          }
{------------------------------------------------------------------}
procedure DrawTriangle( x1, y1, x2, y2, x3, y3, z : GLfloat);
begin
    glBegin(GL_TRIANGLES);
    glVertex3f(x1, y1, z);
    glVertex3f(x2, y2, z);
    glVertex3f(x3, y3, z);
    glEnd();
end;

{------------------------------------------------------------------}
{  Draw our view volume (Frustrum)                                 }
{------------------------------------------------------------------}
procedure DrawViewVolume( x1, x2, y1, y2, z1, z2 : GLfloat);
begin
    glColor3f (1.0, 1.0, 1.0);
    glBegin (GL_LINE_LOOP);
        glVertex3f (x1, y1, -z1);
        glVertex3f (x2, y1, -z1);
        glVertex3f (x2, y2, -z1);
        glVertex3f (x1, y2, -z1);
    glEnd ();

    glBegin (GL_LINE_LOOP);
        glVertex3f (x1, y1, -z2);
        glVertex3f (x2, y1, -z2);
        glVertex3f (x2, y2, -z2);
        glVertex3f (x1, y2, -z2);
    glEnd ();

    glBegin (GL_LINES);   //  4 lines
        glVertex3f (x1, y1, -z1);
        glVertex3f (x1, y1, -z2);
        glVertex3f (x1, y2, -z1);
        glVertex3f (x1, y2, -z2);
        glVertex3f (x2, y1, -z1);
        glVertex3f (x2, y1, -z2);
        glVertex3f (x2, y2, -z1);
        glVertex3f (x2, y2, -z2);
    glEnd ();
end;

{------------------------------------------------------------------}
{  Handle window resize                                            }
{------------------------------------------------------------------}
procedure glResizeWnd(Width, Height : Integer);
begin

    GWidth := Width;
    GHeight := Height;
    glViewport(0, 0, Width, Height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective (45.0, Width/Height, 3.0, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
end;

{------------------------------------------------------------------}
{  Draw Text on the screen                                         }
{------------------------------------------------------------------}
procedure glDrawText(SText : string; xPos,yPos : integer);
const
   RectOfs = 0;
begin
  glPushAttrib(GL_DEPTH_TEST);                                          // Save the current Depth test settings (Used for blending )
    glDisable(GL_DEPTH_TEST);                                           // Turn off depth testing (otherwise we get no FPS)
    glMatrixMode(GL_PROJECTION);                                        // Switch to the projection matrix
    glPushMatrix();                                                     // Save current projection matrix
      glLoadIdentity();

      glOrtho(0,GWidth, 0, GHeight, -1, 1);                             // Change the projection matrix using an orthographic projection
      glMatrixMode(GL_MODELVIEW);                                       // Return to the modelview matrix
      glPushMatrix();                                                   // Save the current modelview matrix
        glLoadIdentity();

        glColor3f(1.0,1.0,1.0);
        glRasterPos2i(xPos + RectOfs, GHeight - (yPos + RectOfs + 12)); // Position the Text
        glPushAttrib(GL_LIST_BIT);                                      // Save's the current base list
          glListBase(fontOffset);                                       // Set the base list to our character list
          glCallLists(length(SText), GL_UNSIGNED_BYTE,  PChar(SText));  // Display the text
        glPopAttrib();                                                  // Restore the old base list


        glMatrixMode(GL_PROJECTION);                                    // Switch to projection matrix
      glPopMatrix();                                                    // Restore the old projection matrix
      glMatrixMode(GL_MODELVIEW);                                       // Return to modelview matrix
    glPopMatrix();                                                      // Restore old modelview matrix
    glEnable(GL_DEPTH_TEST);
  glPopAttrib();                                                        // Restore depth testing;

  glResizeWnd(GWidth,GHeight);
end;

{------------------------------------------------------------------}
{  Process hits witin our view volume                              }
{------------------------------------------------------------------}
procedure ProcessHits(hits : GLint; buffer : array of GLuint);
var i, j : Cardinal;
    names : GLuint;
    ptr : PGLuint;
begin
    SetLength(Output,1);
    Output[High(Output)] := 'Hits = ' + IntToStr(hits);  // Number of hits in
    ptr := @GLuint(buffer[0]);
    for i := 0 to hits - 1 do // For each hit
    begin
        names := ptr^;                     // Number of named objects for this hit
        SetLength(Output,Length(Output)+1);
        Output[High(Output)] := '  Number of names for hit '  + IntToStr(i+1) + ' = ' + IntToStr(names); Inc(ptr);
        SetLength(Output,Length(Output)+1);
        Output[High(Output)] := '  z1 is ' + FloatToStr(Round(ptr^/$7fffffff)); Inc(ptr); // Depth one
        SetLength(Output,Length(Output)+1);
        Output[High(Output)] := '  z2 is ' + FloatToStr(Round(ptr^/$7fffffff)); Inc(ptr); // Depth two
        SetLength(Output,Length(Output)+1);
        Output[High(Output)] := '  the name is ';
        for j := 0 to names - 1 do      // for each name
        begin
          Output[High(Output)] := Output[High(Output)] + IntToStr(ptr^); Inc(ptr); // Print each objects name
        end;
    end;

end;


{------------------------------------------------------------------}
{  Select objects and assign names to them                         }
{------------------------------------------------------------------}
procedure SelectObjects();
var
  selectBuf : array [0..BUFSIZE-1] of GLuint;
  viewport : array [0..3] of GLint;
  hits : GLint;
begin

    glSelectBuffer (BUFSIZE, @selectBuf);          // Set selection buffer
    glRenderMode(GL_SELECT);                       // Set render mode to select

    glInitNames();                                 // Initialise Names
    glPushName(0);                                 // Add name 0

    glPushMatrix();                                // Copy and Push a new Matrix Onto the stack
        glMatrixMode (GL_PROJECTION);              // Switch to Projection Matrix
        glLoadIdentity ();                         // Load a clean Matrix onto the stack
        glOrtho (0.0, 5.0, 0.0, 5.0, 0.0, 10.0);   // Set orthographic view
        glMatrixMode (GL_MODELVIEW);               // Switch to Model View Matrix
        glLoadIdentity ();                         // Load a clean Matrix onto the stack
        glLoadName(1);                             // Add name 1
        drawTriangle (2.0, 2.0, 3.0, 2.0, 2.5, 3.0, -5.0); // Draw a triangle
        glLoadName(2);                             // Add name 0
        drawTriangle (2.0, 7.0, 3.0, 7.0, 2.5, 8.0, -5.0); // Draw a triangle
        glLoadName(3);                             // Add name 0
        drawTriangle (2.0, 2.0, 3.0, 2.0, 2.5, 3.0, 0.0);  // Draw a triangle
        drawTriangle (2.0, 2.0, 3.0, 2.0, 2.5, 3.0, -10.0);// Draw a triangle
    glPopMatrix();                                 // Restore the previously save matrix
    glFlush();                                     // Force render

    hits := glRenderMode (GL_RENDER);              // Set Render mode Back to normal and return hits within our view volume
    ProcessHits(hits, selectBuf);                  // Process all hits in the selection buffer
end;



{------------------------------------------------------------------}
{  Draw the text and scene                                         }
{------------------------------------------------------------------}
procedure DrawScene();
var i : GLint;
begin
  for i := 0 to High(Output) do
  begin
    glDrawText(Output[i], 0, (50)+(14*i));                   // Draw One Line of text
  end;

  gluLookAt(7.5, 7.5, 12.5, 2.5, 2.5, -5.0, 0.0, 1.0, 0.0);  // Look at scene
  glColor3f(0.0, 1.0, 0.0);                                  // Green triangle
  DrawTriangle(2.0, 2.0, 3.0, 2.0, 2.5, 3.0, -5.0);          // Draw Triangle
  glColor3f(1.0, 0.0, 0.0);                                  // Red triangle
  DrawTriangle(2.0, 7.0, 3.0, 7.0, 2.5, 8.0, -5.0);          // Draw Triangle
  glColor3f(1.0, 1.0, 0.0);                                  // Yellow triangles
  DrawTriangle(2.0, 2.0, 3.0, 2.0, 2.5, 3.0, 0.0);           // Draw Triangle
  DrawTriangle(2.0, 2.0, 3.0, 2.0, 2.5, 3.0, -10.0);         // Draw Triangle
  DrawViewVolume(0.0, 5.0, 0.0, 5.0, 0.0, 10.0);             // Draw View Volume


end;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClearColor(0.0, 0.0, 0.0, 0.0);                        // Set the background colour to black
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);     // Clear the colour and depth buffer
  glLoadIdentity();                                        // Load a new matrix onto the stack
  DrawScene();                                             // Draw the scene
  SelectObjects();                                         // Select objects in the view volume

  glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;

  //========================================

  glDepthFunc(GL_LEQUAL);                           // Set the depth function
  glEnable(GL_DEPTH_TEST);                          // Enable Depth testing
  glShadeModel(GL_FLAT);                            // Dont shade the model

  MakeRasterFont();                                 // Initialize the raster font

   //========================================
  initOK:=true;
end;

{------------------------------------------------------------------}
{  Processes all the mouse clicks                                  }
{------------------------------------------------------------------}
procedure ProcessMouse;
begin
  case MouseButton of
  1: // Left Mouse Button
    begin
      MouseButton := 0; // Cancel our mouse click (To use this procedure as a mouse down event remove this line)
    end;
  2: // Right Mouse Button
    begin
      MouseButton := 0; // Cancel our mouse click (To use this procedure as a mouse down event remove this line)
    end;
  3: // Middle Mouse Button
    begin
      MouseButton := 0; // Cancel our mouse click (To use this procedure as a mouse down event remove this line)
    end;
  end;
end;

procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

    glResizeWnd(Width, Height);

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    onMouseDown:=OpenGLPanel1MouseDown;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OpenGLPanel1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); 
begin
  Xcoord := X;
  Ycoord := Y;
  MouseButton:=0;

  case Button of
    mbLeft  : MouseButton:=1;
    mbRight : MouseButton:=2;
    mbMiddle: MouseButton:=3;
  end;

  ProcessMouse;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

