{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo34mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


const

  // Image Constants
  IMAGE_WIDTH = 64;
  IMAGE_HEIGHT = 64;

var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

 SphereQuadric : PGLUquadricObj;     // Quadric for our sphere


  // Control Point for Bezier Surface
  ctrlpoints : array [0..3] of array [0..3] of array[0..2] of GLfloat = (
    ((-1.5, -1.5, 4.0), (-0.5, -1.5, 2.0),
     (0.5, -1.5, -1.0), (1.5, -1.5, 2.0)),
    ((-1.5, -0.5, 1.0), (-0.5, -0.5, 3.0),
      (0.5, -0.5, 0.0), (1.5, -0.5, -1.0)),
    ((-1.5, 0.5, 4.0), (-0.5, 0.5, 0.0),
      (0.5, 0.5, 3.0), (1.5, 0.5, 4.0)),
    ((-1.5, 1.5, -2.0), (-0.5, 1.5, -2.0),
      (0.5, 1.5, 0.0), (1.5, 1.5, -1.0))
  );

  // Control Points for texture
  texpts : array [0..1] of array [0..1] of array[0..1] of GLfloat = (
    ((0.0, 0.0), (0.0, 1.0)),
    ((1.0, 0.0), (1.0, 1.0))
    );

  // Texture Data
  image : array [0..3*IMAGE_WIDTH*IMAGE_HEIGHT] of GLubyte;


{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Create an image to use for our bezier surface                   }
{------------------------------------------------------------------}
procedure LoadImage();
var i, j : GLint;
    ti, tj : single;
begin
    for i := 0 to IMAGE_WIDTH -1 do
    begin
        ti := 2.0*3.14159265*i/IMAGE_WIDTH;
        for j := 0 to IMAGE_HEIGHT -1 do
        begin
            tj := 2.0*3.14159265*j/IMAGE_HEIGHT;
            image[3*(IMAGE_HEIGHT*i+j)] := Round(127*(1.0+sin(ti)));
            image[3*(IMAGE_HEIGHT*i+j)+1] := Round(127*(1.0+cos(2*tj)));
            image[3*(IMAGE_HEIGHT*i+j)+2] := Round(127*(1.0+cos(ti+tj)));
        end;
    end;
end;



{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT); // Clear the colour and depth buffer
  glLoadIdentity();                                    // Load a new Matrix onto the stack
  glRotatef(85.0, 1.0, 1.0, 1.0);                      // Rotate the scene
  glPushMatrix();                                      // Copy the current matrix and push it onto the stack
      glRotatef(85.0, 1.0, 1.0, 1.0);                  // Rotate the scene
      glEvalMesh2(GL_FILL, 0, 8, 0, 8);                // Fill the Mesh
  glPopMatrix();                                       // Resture the last saved matrix
  glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;

{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;

  //========================================
  glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4,
      0, 1, 12, 4, @ctrlpoints[0][0][0]);                    // Add the control points for the Mesh
  glMap2f(GL_MAP2_TEXTURE_COORD_2, 0, 1, 2, 2,
      0, 1, 4, 2, @texpts[0][0][0]);                         // Add the Texture COntrol Points for the mesh
  glEnable(GL_MAP2_TEXTURE_COORD_2);                         // Enable Auto Texture U V Generation
  glEnable(GL_MAP2_VERTEX_3);                                // Enable Auto Vertex Generation
  glMapGrid2f(20, 0.0, 1.0, 20, 0.0, 1.0);                   // Create a 20x20 grid
  LoadImage();                                               // Load the Image
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);  // Set the Texturing to Decal
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                   GL_REPEAT);                               // Repeat on the Texture on S
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                   GL_REPEAT);                               // Repeat on the Texture on T
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                   GL_NEAREST);                              // Set the Magnification factor to use the nearest pixels
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                   GL_NEAREST);                              // Set the Minification factor to use the nearest pixels
  glTexImage2D(GL_TEXTURE_2D, 0, 3, IMAGE_WIDTH, IMAGE_HEIGHT,
                   0, GL_RGB, GL_UNSIGNED_BYTE, @image);     // Load the texture into Video Memory
  glEnable(GL_TEXTURE_2D);                                   // Enable Texuring
  glEnable(GL_DEPTH_TEST);                                   // Enable Depth testing
  glEnable(GL_NORMALIZE);                                    // Auto Normalize
  glShadeModel (GL_FLAT);                                    // Do Not shade the model



   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

  glViewport(0, 0, Width, Height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (Width <= Height) then
      glOrtho(-4.0, 4.0, -4.0*Height/Width, 4.0*Height/Width, -4.0, 4.0)
  else
      glOrtho(-4.0*Width/Height, 4.0*Width/Height, -4.0, 4.0, -4.0, 4.0);

  glMatrixMode(GL_MODELVIEW);

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

