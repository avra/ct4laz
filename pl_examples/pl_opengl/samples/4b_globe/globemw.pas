{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit globemw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    vcDayTime: TCheckBox;
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
    procedure vcDayTimeClick(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

var
  // Textures
  DayTime : Boolean;
  LoadingTex : GLuint;
  DayTex     : GLuint;
  NightTex   : GLuint;

  // User vaiables
  SphereDL : glUint;  // Sphere Display List
  xrot   : glFloat;   // X Rotation
  yrot   : glFloat;   // Y Rotation
  zPos   : glFloat;   // Z Position
  xspeed : glFloat;   // X Rotation Speed
  yspeed : glFloat;   // Y Rotation Speed


{------------------------------------------------------------------}
{  Function to Create a sphere                                     }
{------------------------------------------------------------------}
procedure CreateSphere(CX, CY, CZ, Radius : glFloat; N : Integer);  // N = precision
var I, J : Integer;
    theta1,theta2,theta3 : glFloat;
    X, Y, Z, px, py, pz : glFloat;
begin
  SphereDL :=glGenLists(1);
  glNewList(SphereDL, GL_COMPILE);

    if Radius < 0 then Radius :=-Radius;
    if n < 0 then n := -n;
    if (n < 4) OR (Radius <= 0) then
    begin
      glBegin(GL_POINTS);
        glVertex3f(CX, CY, CZ);
      glEnd();
      exit;
    end;

    for J :=0 to N DIV 2 -1 do
    begin
      theta1 := J*2*PI/N - PI/2;
      theta2 := (J+1)*2*PI/n - PI/2;
      glBegin(GL_QUAD_STRIP);
        For I :=0 to N do
        begin
          theta3 := i*2*PI/N;
          x := cos(theta2) * cos(theta3);
          y := sin(theta2);
          z := cos(theta2) * sin(theta3);
          px := CX + Radius*x;
          py := CY + Radius*y;
          pz := CZ + Radius*z;

  //        glNormal3f(X, Y, Z);
          glTexCoord2f(1-I/n, 2*(J+1)/n);
          glVertex3f(px,py,pz);

          X := cos(theta1) * cos(theta3);
          Y := sin(theta1);
          Z := cos(theta1) * sin(theta3);
          px := CX + Radius*X;
          py := CY + Radius*Y;
          pz := CZ + Radius*Z;

  //        glNormal3f(X, Y, Z);
          glTexCoord2f(1-i/n, 2*j/n);
          glVertex3f(px,py,pz);
        end;
      glEnd();
    end;
  glEndList();
end;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                       // Reset The View

  glTranslatef(0.0,0.0,zPos);
  glRotatef(15, 1.0, 0.0, 0.0);
  glRotatef(xrot, 1.0, 0.0, 0.0);
  glRotatef(yrot, 0.0, 1.0, 0.0);

  if Daytime then
    glBindTexture(GL_TEXTURE_2D, DayTex)
  else
    glBindTexture(GL_TEXTURE_2D, NightTex);

  glCallList(SphereDL);

  xrot := xrot + xspeed;
  yrot := yrot + yspeed;

end;

{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  yspeed :=0.5;
  DayTime :=TRUE;

  glEnable(GL_TEXTURE_2D);          // Enable Texture Mapping
  LoadTexture(pathMedia+'loadingtextures.jpg', LoadingTex, FALSE);
  LoadTexture(pathMedia+'earth.jpg', DayTex, FALSE);
  LoadTexture(pathMedia+'night.jpg', NightTex, FALSE);
  CreateSphere(0, 0, 0, 10, 48);
  zPos :=-32.0;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
  plTimer1.Enabled:=True;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  yspeed:=vcSpeed.Position/100;
end;

procedure TForm1.vcDayTimeClick(Sender: TObject);
begin
  DayTime :=vcDayTime.Checked;
end;

end.

