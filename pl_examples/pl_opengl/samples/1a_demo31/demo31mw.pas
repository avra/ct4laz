{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo31mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

 SphereQuadric : PGLUquadricObj;     // Quadric for our sphere

  // Control Point for Bezier Curve
  ctrlpoints : array [0..3] of array[0..2] of GLfloat = (
    (-4.0, -4.0, 0.0),
    (-2.0, 4.0, 0.0),
    ( 2.0, -4.0, 0.0),
    ( 4.0, 4.0, 0.0)
    );

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i : GLint;
begin

    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);        // Clear the Depth And Colour Buffers
    glColor3f(1.0, 1.0, 1.0);                                   // Set the Primitive Colour to white
    glBegin(GL_LINE_STRIP);                                     // Start Drawing a Line Strip
        for i := 0 to 29 do
            glEvalCoord1f(i/29);                                // glEvalCoord1f() is just like using glVertex1f()
    glEnd();                                                    // Stop Drawing a Line Strip
    // The following code displays the control points as dots.
    glPointSize(5.0);                                           // Set the Poinjt Size to 5.0
    glColor3f(1.0, 1.0, 0.0);                                   // Set the primitive colour to Yellow
    glBegin(GL_POINTS);                                         // Start Drawing Points
        for i := 0 to 4 do
            glVertex3fv(@ctrlpoints[i][0]);                     // Draw a Point
    glEnd();                                                    // Stop Drawing Points


    glFlush();              // ( Force the buffer to draw or send a network packet of commands in a networked system)
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================
                                                          // Set the colour that we will use to clear the colour buffer
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4,@ctrlpoints[0][0]);  // Add our control points for the bezier curves
  glEnable(GL_MAP1_VERTEX_3);                                   // Enable the one-dimensional evaluator for two-dimensional vertices.
  glShadeModel(GL_FLAT);                                        // Do not shade the primitives

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================

  glViewport(0, 0, Width, Height);                              // Set our viewable area
  glMatrixMode(GL_PROJECTION);                                  // Switch to the projection matrix
  glLoadIdentity();                                             // Load a new Matrix onto the stack
  if (Width <= Height) then
      glOrtho(-5.0, 5.0, -5.0*Height/Width,5.0*Height/Width, -5.0, 5.0)
  else
      glOrtho(-5.0*Width/Height, 5.0*Width/Height, -5.0, 5.0, -5.0, 5.0);
  glMatrixMode(GL_MODELVIEW);                                   // Switch to Model View Matrix
  glLoadIdentity();                                             // Load a new Model View Matrix onto the stack

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

