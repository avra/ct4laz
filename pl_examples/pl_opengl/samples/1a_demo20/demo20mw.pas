{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo20mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


{------------------------------------------------------------------}
{  Draw one line from [x1, y1] to [x2,y2]                          }
{------------------------------------------------------------------}
procedure DrawOneLine(x1,y1,x2,y2 : GLfloat);
begin
 glBegin(GL_LINES);
   glVertex2f (x1, y1);
   glVertex2f (x2, y2);
 glEnd();
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i : GLint;
begin
  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glClear(GL_COLOR_BUFFER_BIT OR GL_DEPTH_BUFFER_BIT);     // Clear the colour and depth buffer
  glLoadIdentity();                 // Load a "clean" model matrix on the stack

//  draw all lines in white
    glColor3f (1.0, 1.0, 1.0);


//  in 1st row, 3 lines, each with a different stipple
    glEnable (GL_LINE_STIPPLE);
    glLineStipple (1, $0101);   //  dotted
    drawOneLine (-15.0, 5.0, -5.0, 5.0);
    glLineStipple (1, $00FF);   //  dashed
    drawOneLine (-5.0, 5.0, 5.0, 5.0);
    glLineStipple (1, $1C47);   //  dash/dot/dash
    drawOneLine (0.0, 5.0, 10.0, 5.0);

//  in 2nd row, 3 wide lines, each with different stipple
    glLineWidth (5.0);
    glLineStipple (1, $0101);
    drawOneLine (-15.0, 0.0, -5.0, 0.0);
    glLineStipple (1, $00FF);
    drawOneLine (-5.0, 0.0, 5.0, 0.0);
    glLineStipple (1, $1C47);
    drawOneLine (0.0, 0.0, 10.0, 0.0);
    glLineWidth (1.0);

//  in 3rd row, 6 lines, with dash/dot/dash stipple,
//  as part of a single connected line strip
    glLineStipple (1, $1C47);
    glBegin (GL_LINE_STRIP);
    for i := 0 to 6 do
      glVertex2f (-15.0 + (i * 5.0), -5.0);
    glEnd();

//  in 4th row, 6 independent lines,   */
//  with dash/dot/dash stipple         */
    for i := 0 to 5 do
      drawOneLine (-15.0 + ( i * 5.0),
       -10.0, -5 + ((i+1) * 5.0), -10.0);


//  in 5th row, 1 line, with dash/dot/dash stipple
//  and repeat factor of 5
    glLineStipple (5, $1C47);
    drawOneLine (-15.0, -15.0, 15.0, -15.0);

  // Flush the OpenGL Buffer
  glFlush();                        // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //========================================

  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glColor3f(1.0, 1.0, 1.0);         // Set the Polygon colour to white

  glShadeModel(GL_FLAT);            // Use flat shading
  glEnable(GL_DEPTH_TEST);          // Enable testing the debth of any pixel on the scene.
                                    // Used for Hidden Surface Removal

  glPointSize(2.0);                 // Sets the size of all points that will be drawn in the scene
                                    // The parameter must be greater that 0.0 and its default is 1.0

  glLineWidth(4.0);                 // Set the width of all lines drawn in the scene
                                    // The paramter for this must be greater than 0.0 and has a befault of 1.0

   //========================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  // This is one way to set the perspective when a window get resized.
  // But this is not the only way. Another way will be handled in a later tutorial

  glViewport(0, 0, Width, Height);                    // Set the viewport for the OpenGL window
  glMatrixMode(GL_PROJECTION);                        // Change Matrix Mode to Projection
  glLoadIdentity();                                   // Reset View ( Load Thye default projection matrix onto the stack)
  if (Width <= Height) then
      glOrtho (-50.0, 50.0, -50.0*Height/Width,
          50.0*Height/Width, -1.0, 1.0)               // Set up orthographic view
  else
      glOrtho (-50.0*Width/Height,
          50.0*Width/Height, -50.0, 50.0, -1.0, 1.0); // Set up orthographic view
  glMatrixMode(GL_MODELVIEW);                         // Return to the modelview matrix
  glLoadIdentity();                                  // Reset View ( Load the default Modelview matrix onto the stack)

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

