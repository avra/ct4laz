{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo11mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}

const

 // User Constants

  X = 0.525731112119133606;
  Z = 0.850650808352039932;

  vdata : array [0..11] of array [0..2] of GLfloat = (
   (-X, 0.0, Z), (X, 0.0, Z), (-X, 0.0, -Z), (X, 0.0, -Z),
   (0.0, Z, X), (0.0, Z, -X), (0.0, -Z, X), (0.0, -Z, -X),
   (Z, X, 0.0), (-Z, X, 0.0), (Z, -X, 0.0), (-Z, -X, 0.0)
    );

  tindices : array [0..19] of array [0..2] of GLint = (
   (0,4,1), (0,9,4), (9,5,4), (4,5,8), (4,8,1),
   (8,10,1), (8,3,10),(5,3,8), (5,2,3), (2,7,3),
   (7,10,3), (7,6,10), (7,11,6), (11,0,6), (0,1,6),
   (6,1,10), (9,0,11), (9,11,2), (9,2,5), (7,2,11)
   );

type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;

{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var i : GLint;
begin
  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glClear (GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear the colour and depth buffer
  glLoadIdentity();                 // Load a "clean" model matrix on the stack

  glTranslatef(0.0,0.0,-10.0);
  SpinDisplay();
  glRotatef(spin,1.0,1.0,1.0);      // Rotate the Icosahedron so that we can visualise it properly
  for i := 0 to 19 do
  begin
   glColor3f(i/19,i/19,i/19);       // Generate a unique colour for each triangle
   glBegin(GL_TRIANGLES);
      glVertex3fv(@vdata[tindices[i][0]][0]);
      glVertex3fv(@vdata[tindices[i][1]][0]);
      glVertex3fv(@vdata[tindices[i][2]][0]);
   glEnd();
  end;
  // Flush the OpenGL Buffer
  glFlush();                        // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;

  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glColor3f(1.0, 1.0, 1.0);         // Set the Polygon colour to white
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_FLAT);            // Use flat shading

  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

