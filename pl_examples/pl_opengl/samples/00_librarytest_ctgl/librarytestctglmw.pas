unit librarytestctglmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ctutils,
  ExtCtrls, ComCtrls,ctgl, OpenGLPanel, ctgl_rpt;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    PageControl1: TPageControl;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    fGLPanel: TOpenGLPanel;
    procedure TestLibFunctions;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

Function PathLibrary:string;
begin
{$IFDEF Windows}
  Result := ctGetRuntimesCPUOSDir(true);
{$ELSE}
  Result := '';
{$ENDIF}
end;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
 if fGLPanel<> nil then exit;

 Memo1.Clear;
 Memo2.Clear;

  if Not OpenGL_Initialize then
  begin
   Memo1.Lines.add('ERROR: '+LIBNAME_OPENGL+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(LIBNAME_OPENGL+' Loaded OK');
  end;

  TestLibFunctions;

  //...........................

  Label1.Caption:='"BASE" OpenGL Ver: '+OpenGLLib_VersionStr;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
 Button1.Enabled:=false;

 Memo1.Clear;   
 Memo2.Clear;

  if Not OpenGL_Initialize then
  begin
   Memo1.Lines.add('ERROR: '+LIBNAME_OPENGL+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(LIBNAME_OPENGL+' Loaded OK');
  end;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLPanel.Create(self);
    fGLPanel.Parent:=panel1;
    fGLPanel.SetBounds(440,8,72,56);
    fGLPanel.Visible:=true;
  end;

  fGLPanel.MakeCurrent;

  OpenGL_InitializeAdvance;

  TestLibFunctions;

  //...........................
  Label1.Caption:='"FULL" OpenGL Ver: '+OpenGLLib_VersionStr;
end;

procedure TForm1.TestLibFunctions;
begin
 Memo1.Lines.add('--------- Commands -----------');
 OpenGL_WriteCommandsReport(Memo1.Lines,RadioGroup1.ItemIndex);

 Memo2.Lines.add('--------- Extensions ---------');
 OpenGL_WriteExtensionsReport(Memo2.Lines,RadioGroup1.ItemIndex);
end;

end.

