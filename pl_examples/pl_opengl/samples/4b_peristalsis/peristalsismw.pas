{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit peristalsismw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation 

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

Const MAX_SEGMENTS = 64;
      MAX_DIVS = 16;

var
  ElapsedTime, xdelta,xspeed : integer;             // Elapsed time between frames
  // Textures
  TubeTex : GLuint;
  BackTex : GLUint;

  // User vaiables
  twist  : Array[0..MAX_SEGMENTS] of glFloat;
  radius : Array[0..MAX_SEGMENTS] of glFloat;
  xpos   : Array[0..MAX_SEGMENTS] of glFloat;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var I,J : Integer;
    Angle : glFloat;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer

  glLoadIdentity();                                       // Reset The View
  Angle :=ElapsedTime/300;

  //--- Background effect ---//
  glPushMatrix();
    glTranslatef(0, 0, -5);
    glRotatef(25*Angle,1.0,0.0,0.0);
    glRotatef(45*Angle,0.0,1.0,0.0);

    glScalef(5, 5, 5);
    glBindTexture(GL_TEXTURE_2D, BackTex);
    glBegin(GL_QUADS);
      // Front Face
      glNormal3f( 0.0, 0.0, 1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0,  1.0);
      glTexCoord2f(1.0, 0.0); glVertex3f( 1.0, -1.0,  1.0);
      glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0,  1.0);
      glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0,  1.0);
      // Back Face
      glNormal3f( 0.0, 0.0,-1.0);
      glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 1.0); glVertex3f(-1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.0); glVertex3f( 1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f( 1.0, -1.0, -1.0);
      // Top Face
      glNormal3f( 0.0, 1.0, 0.0);
      glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(-1.0,  1.0,  1.0);
      glTexCoord2f(1.0, 0.0); glVertex3f( 1.0,  1.0,  1.0);
      glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0, -1.0);
      // Bottom Face
      glNormal3f( 0.0,-1.0, 0.0);
      glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, -1.0, -1.0);
      glTexCoord2f(0.0, 1.0); glVertex3f( 1.0, -1.0, -1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f( 1.0, -1.0,  1.0);
      glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0,  1.0);
      // Right face
      glNormal3f( 1.0, 0.0, 0.0);
      glTexCoord2f(1.0, 0.0); glVertex3f( 1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 1.0); glVertex3f( 1.0,  1.0, -1.0);
      glTexCoord2f(0.0, 1.0); glVertex3f( 1.0,  1.0,  1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f( 1.0, -1.0,  1.0);
      // Left Face
      glNormal3f(-1.0, 0.0, 0.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, -1.0);
      glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0,  1.0);
      glTexCoord2f(1.0, 1.0); glVertex3f(-1.0,  1.0,  1.0);
      glTexCoord2f(0.0, 1.0); glVertex3f(-1.0,  1.0, -1.0);
    glEnd();
  glPopMatrix();

  glBindTexture(GL_TEXTURE_2D, TubeTex);
  glTranslatef(0.0, 0.0, -4.6);
  glRotatef( Angle*4, 0, 0, 1 );
  glTranslatef(0,-2.75,0);


  //--- DrawCylinder ---//
  glBegin(GL_QUADS);
    for I :=0 to MAX_SEGMENTS do
    begin
      For J :=0 to MAX_DIVS do
      begin
        //  i,j
        glTexCoord2f(twist[i] + j*4/MAX_DIVS, i*5.5/MAX_SEGMENTS);
        glVertex3f(xpos[i]+ 0.7*radius[i]*cos(j*PI*2.0/MAX_DIVS), i*5.5/MAX_SEGMENTS, 0.7*radius[i]*sin(j*PI*2.0/MAX_DIVS));

        //  i+1, j
        glTexCoord2f(twist[i+1] + j*4/MAX_DIVS, (i+1)*5.5/MAX_SEGMENTS);
        glVertex3f(xpos[i+1]+ 0.7*radius[i+1]*cos(j*PI*2.0/MAX_DIVS), (i+1)*5.5/MAX_SEGMENTS, 0.7*radius[i+1]*sin(j*PI*2.0/MAX_DIVS));

        //  i+1, j+1
        glTexCoord2f(twist[i+1] + (j+1)*4/MAX_DIVS, (i+1)*5.5/MAX_SEGMENTS);
        glVertex3f(xpos[i+1]+ 0.7*radius[i+1]*cos((j+1)*PI*2.0/MAX_DIVS), (i+1)*5.5/MAX_SEGMENTS, 0.7*radius[i+1]*sin((j+1)*PI*2.0/MAX_DIVS));

        //  i, j+1
        glTexCoord2f(twist[i] + (j+1)*4/MAX_DIVS, i*5.5/MAX_SEGMENTS);
        glVertex3f(xpos[i]+ 0.7*radius[i]*cos((j+1)*PI*2.0/MAX_DIVS), i*5.5/MAX_SEGMENTS, 0.7*radius[i]*sin((j+1)*PI*2.0/MAX_DIVS));
      end
    end;
  glEnd();

  // Recalculate values
  for I :=0 to MAX_SEGMENTS-1 do
  begin
    twist[i] := 0.35*sin(Angle*0.78+i/12.0) + 0.35*sin(Angle*-1.23+i/18.0);;
    radius[i]:= 0.30*sin(Angle+i/8.0) +0.80 + 0.15*sin(Angle*-0.8+i/3.0);
    xpos[i]  := 0.25*sin(Angle*1.23+i/5.0)  + 0.30*sin(Angle*0.9+i/6.0);
  end;
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
Var I : Integer;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations

  glEnable(GL_TEXTURE_2D);                     // Enable Texture Mapping
  LoadTexture(pathMedia+'tube.bmp', TubeTex, false);
  LoadTexture(pathMedia+'background2.bmp', BackTex, false);

  For I :=0 to MAX_SEGMENTS do
  begin
    twist[I] :=0;
    radius[I] :=1;
  end;

  xspeed:=10;
  xdelta:=1;
end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
   plTimer1.Enabled:=True;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  xspeed:=vcSpeed.Position;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  if ElapsedTime<10 then xdelta:=1;
  if ElapsedTime>10000 then xdelta:=-1;

   ElapsedTime:=ElapsedTime+xdelta*xspeed;

  //..........................
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

end.

