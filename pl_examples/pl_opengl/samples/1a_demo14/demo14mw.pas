{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo14mw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  OpenGLPanel, ctGL, ctGLU;

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

{$R *.lfm}


type
  TCharCoord = packed record
    x, y : GLfloat;
    coordtype : GLint;
  end;

const
  // User Constants

  FONT_PT = 1;
  FONT_STROKE = 2;
  FONT_END = 3;

  Adata : array [0..7] of TCharCoord = (
    ( x: 0.0; y: 0.0; coordtype:FONT_PT),
    ( x: 0.0; y: 9.0; coordtype:FONT_PT),
    ( x: 1.0; y:10.0; coordtype:FONT_PT),
    ( x: 4.0; y:10.0; coordtype:FONT_PT),
    ( x: 5.0; y: 9.0; coordtype:FONT_PT),
    ( x: 5.0; y: 0.0; coordtype:FONT_STROKE),
    ( x: 0.0; y: 5.0; coordtype:FONT_PT),
    ( x: 5.0; y: 5.0; coordtype:FONT_END)
    );

  Edata : array [0..5] of TCharCoord = (
    ( x: 5.0; y: 0.0; coordtype:FONT_PT),
    ( x: 0.0; y: 0.0; coordtype:FONT_PT),
    ( x: 0.0; y:10.0; coordtype:FONT_PT),
    ( x: 5.0; y:10.0; coordtype:FONT_STROKE),
    ( x: 0.0; y: 5.0; coordtype:FONT_PT),
    ( x: 4.0; y: 5.0; coordtype:FONT_END)
    );


  Pdata : array [0..6] of TCharCoord = (
    ( x: 0.0; y: 0.0; coordtype:FONT_PT),
    ( x: 0.0; y:10.0; coordtype:FONT_PT),
    ( x: 4.0; y:10.0; coordtype:FONT_PT),
    ( x: 5.0; y: 9.0; coordtype:FONT_PT),
    ( x: 5.0; y: 6.0; coordtype:FONT_PT),
    ( x: 4.0; y: 5.0; coordtype:FONT_PT),
    ( x: 0.0; y: 5.0; coordtype:FONT_END)
    );

  Rdata : array [0..8] of TCharCoord = (
    ( x: 0.0; y: 0.0; coordtype:FONT_PT),
    ( x: 0.0; y:10.0; coordtype:FONT_PT),
    ( x: 4.0; y:10.0; coordtype:FONT_PT),
    ( x: 5.0; y: 9.0; coordtype:FONT_PT),
    ( x: 5.0; y: 6.0; coordtype:FONT_PT),
    ( x: 4.0; y: 5.0; coordtype:FONT_PT),
    ( x: 0.0; y: 5.0; coordtype:FONT_STROKE),
    ( x: 3.0; y: 5.0; coordtype:FONT_PT),
    ( x: 5.0; y: 0.0; coordtype:FONT_END)
    );
  Sdata : array [0..11] of TCharCoord = (
    ( x: 0.0; y: 1.0; coordtype:FONT_PT),
    ( x: 1.0; y: 0.0; coordtype:FONT_PT),
    ( x: 4.0; y: 0.0; coordtype:FONT_PT),
    ( x: 5.0; y: 1.0; coordtype:FONT_PT),
    ( x: 5.0; y: 4.0; coordtype:FONT_PT),
    ( x: 4.0; y: 5.0; coordtype:FONT_PT),
    ( x: 1.0; y: 5.0; coordtype:FONT_PT),
    ( x: 0.0; y: 6.0; coordtype:FONT_PT),
    ( x: 0.0; y: 9.0; coordtype:FONT_PT),
    ( x: 1.0; y:10.0; coordtype:FONT_PT),
    ( x: 4.0; y:10.0; coordtype:FONT_PT),
    ( x: 5.0; y: 9.0; coordtype:FONT_END)
    );


var
 initOK:boolean=false;              //Build scene objects only one time
 spin : GLfloat = 0.0;              // Var used for the amount to rotate an object by

  base : GLuint;

  test1 : PChar = 'A SPARE SERAPE APPEARS AS';
  test2 : PChar = 'APES PREPARE RARE PEPPERS';

{------------------------------------------------------------------}
{  Calculate the next rotation value an assign it to spin          }
{------------------------------------------------------------------}
procedure SpinDisplay;
begin
    spin := spin + 2.0;
    if spin > 360.0 then
        spin := spin - 360.0;
end;


procedure DrawLetter(Letter : array of  TCharCoord);
var i : GLint;
begin
    glBegin(GL_LINE_STRIP);
    for i := low(Letter) to high(Letter) do
    begin
      case Letter[i].coordtype of
      FONT_PT: glVertex2f(Letter[i].x,Letter[i].y);
      FONT_STROKE:
        begin
              glVertex2f(Letter[i].x,Letter[i].y);
              glEnd();
              glBegin(GL_LINE_STRIP);
        end;
      FONT_END:
        begin
              glVertex2f(Letter[i].x,Letter[i].y);
              glEnd();
              glTranslatef(6.0, 0.0, 0.0);
        end;
      end;
    end;
end;

procedure printStrokedString(s : PChar);
var len : GLsizei;
begin
    len := Length(string(s));
    glCallLists(len, GL_BYTE, PGLbyte(s));
end;



{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
begin
  glClearColor(0.0, 0.0, 0.0, 1.0);     // Black Background
  glClear (GL_COLOR_BUFFER_BIT);        // Clear the colour buffer
  glLoadIdentity();                     // Load a "clean" model matrix on the stack
  glTranslatef(-55.0,-10.0,-100.0);     // Move the scene back 10 units so we can see the arm properly

  glColor3f(1.0,0.0,0.0);               // Set our current rendering colour to red

  glPushMatrix();
    glScalef(0.5, 0.5, 0.5);
      glTranslatef(10.0, 30.0, 0.0);
      printStrokedString(test1);
    glPopMatrix();
    glPushMatrix();
      glScalef(0.5, 0.5, 0.5);
    glTranslatef(10.0, 13.0, 0.0);
    printStrokedString(test2);
  glPopMatrix();

  // Flush the OpenGL Buffer
  glFlush();                            // ( Force the buffer to draw or send a network packet of commands in a networked system)

end;

{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
begin       
  if initOK=true then exit;
  //===========================================================

  glClearColor(0.0, 0.0, 0.0, 1.0); // Black Background
  glShadeModel(GL_FLAT);            // Use Flat shading

  base := glGenLists (128);

    glListBase(base);

    glNewList(base+ord('A'), GL_COMPILE);
      DrawLetter(Adata);
    glEndList();
    glNewList(base+ord('E'), GL_COMPILE);
      DrawLetter(Edata);
    glEndList();
    glNewList(base+ord('P'), GL_COMPILE);
      DrawLetter(Pdata);
    glEndList();
    glNewList(base+ord('R'), GL_COMPILE);
      DrawLetter(Rdata);
    glEndList();
    glNewList(base+ord('S'), GL_COMPILE);
      DrawLetter(Sdata);
    glEndList();
    glNewList(base+ord(' '), GL_COMPILE);
      glTranslatef(8.0, 0.0, 0.0);
    glEndList();


  // NB: In most cases you will create all Display Lists Here,
  // since creating them in the Render Loop
  // will defeat the point of using Display Lists. (No Performance Gain)

  //===========================================================
  initOK:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  if (Height = 0) then Height := 1;   // prevent divide by zero exception

  glViewport(0, 0, Width, Height);    // Set the viewport for the OpenGL window
  glMatrixMode(GL_PROJECTION);        // Change Matrix Mode to Projection
  glLoadIdentity();                   // Reset View
  gluPerspective(45.0, Width/Height, 1.0, 200.0);  // Do the perspective calculations. Last value = max clipping depth

  glMatrixMode(GL_MODELVIEW);         // Return to the modelview matrix
  glLoadIdentity();

  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  initOK:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Invalidate;
  OpenGLPanel1.SwapBuffers;
end;

end.

