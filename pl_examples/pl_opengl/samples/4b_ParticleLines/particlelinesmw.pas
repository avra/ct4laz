{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit ParticleLinesmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,LCLIntf,
  OpenGLPanel, ctGL, ctGLU,
  OpenGL_Textures;

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

type

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    cube_rotationx: GLFloat;
    cube_rotationy: GLFloat;
    cube_rotationz: GLFloat;
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

const
  GRID_WIDTH = 30;
  GRID_HEIGHT = 9;
  TAIL_LENGTH = 6;
  MAX_PARTICLES = 500;


type TVertex = Record
       X, Y, Z : glInt;
     end;
     TParticle = Record
       NextVertex : Integer;         // vertex its traveling to
       DestValue  : Integer;         // speed*elapsedtime value when it gets there.
       Visited : Array[1..TAIL_LENGTH] of Integer;  // last 5 visited vertices
       Speed   : Single;             // speed of the particle
       Step    : glFloat;
     end;


var
  stafsLoaded :boolean = false;
  ElapsedTime,DemoStart, LastTime : GLfloat;

 // Textures
  particleTex : glUint;
  backgroundTex : glUint;

  // User variables
  Particles : Integer = 150;
  Grid     : Array[0..GRID_WIDTH*GRID_WIDTH*GRID_HEIGHT] of TVertex;  // grid of 100x100x7
  Particle : Array[1..MAX_PARTICLES] of TParticle;
  ShowBackground : Boolean = TRUE;

{------------------------------------------------------------------}
{  Function to convert int to string.                              }
{------------------------------------------------------------------}
function IntToStr(Num : Integer) : String;  // using SysUtils increase file size by 100K
begin
  Str(Num, result);
end;


//--- Check if the particle has reached its destination. If it has, select a new destination
procedure UpdateParticles;
var I, P : Integer;
begin
  for P :=1 to PARTICLES do
  with Particle[P] do
  begin
    if ElapsedTime >= DestValue then      // Check if the particle has reached its destination
    begin
      for I :=TAIL_LENGTH downto 2 do     // store current location in visited list and thift the other up
        Visited[I] :=Visited[I-1];
      Visited[1] :=NextVertex;

      // select a new random direction
      repeat
        I :=Random(6);
        case I of
          0 : if Visited[1] MOD GRID_WIDTH > 0 then NextVertex := Visited[1] - 1;                 // left
          1 : if Visited[1] MOD GRID_WIDTH < GRID_WIDTH-1 then NextVertex := Visited[1] + 1;      // right
          2 : if Visited[1] MOD (GRID_WIDTH*GRID_WIDTH) > GRID_WIDTH then NextVertex := Visited[1] - GRID_WIDTH;      // back
          3 : if Visited[1] MOD (GRID_WIDTH*GRID_WIDTH) < GRID_WIDTH*GRID_WIDTH-GRID_WIDTH then NextVertex := Visited[1] + GRID_WIDTH;   // forward
          4 : if Visited[1] DIV (GRID_WIDTH*GRID_WIDTH) > 0 then NextVertex := Visited[1] - GRID_WIDTH*GRID_WIDTH;              // down
          5 : if Visited[1] DIV (GRID_WIDTH*GRID_WIDTH) < GRID_HEIGHT-1 then NextVertex := Visited[1] + GRID_WIDTH*GRID_WIDTH;  // up
        end;
        if NextVertex <> Visited[1] then    // not equal current vertex
          if NextVertex <> Visited[2] then  // not equal previous vertex
            I :=-1;
      until I = -1;

      // calculate when the next endpoint will be reached
      DestValue :=Round(((ElapsedTime / (Round(10/Speed)))*10+10)/Speed);
    end;
  end;
end;


//--- Draws the tail of the particle. Color fades away. Fast particles are red
procedure drawParticleTail;
var I, P : Integer;
    C : glFloat;
begin
  for P :=1 to PARTICLES do
  with Particle[P] do
  begin

    // Draw the path of the particle and fade the color
    Step := 1-(DestValue - ElapsedTime)/10*Speed;
    glBegin(GL_LINE_STRIP);
      glColor3f(1, 1, 1);
      glVertex3f((Grid[NextVertex].X-Grid[Visited[1]].X)*Step + Grid[Visited[1]].X,
                 (Grid[NextVertex].Y-Grid[Visited[1]].Y)*Step + Grid[Visited[1]].Y,
                 (Grid[NextVertex].Z-Grid[Visited[1]].Z)*Step + Grid[Visited[1]].Z);
      for I :=1 to TAIL_LENGTH-1 do
      begin
        C :=(TAIL_LENGTH-I)/TAIL_LENGTH - Step/TAIL_LENGTH;
        if Speed < 0.05 then
          glColor3f(C, C, 0.5+C)
        else
          glColor3f(0.5+C, C, C);    // faster particles are red
        glVertex3iv(@Grid[Visited[I]]);
      end;
      glColor3f(0, 0, 0.1);
      glVertex3f((Grid[Visited[TAIL_LENGTH-1]].X-Grid[Visited[TAIL_LENGTH]].X)*Step + Grid[Visited[TAIL_LENGTH]].X,
                 (Grid[Visited[TAIL_LENGTH-1]].Y-Grid[Visited[TAIL_LENGTH]].Y)*Step + Grid[Visited[TAIL_LENGTH]].Y,
                 (Grid[Visited[TAIL_LENGTH-1]].Z-Grid[Visited[TAIL_LENGTH]].Z)*Step + Grid[Visited[TAIL_LENGTH]].Z);
    glEnd();
  end;
end;


//--- Draws the head of the particle. Fast particles are red
procedure drawParticleHead();
var P : Integer;
    X, Y, Z : glFloat;
begin
  glBindTexture(GL_TEXTURE_2D, particleTex);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  for P :=1 to Particles do
  with Particle[P] do
  begin
    X :=(Grid[NextVertex].X-Grid[Visited[1]].X)*Step + Grid[Visited[1]].X;
    Y :=(Grid[NextVertex].Y-Grid[Visited[1]].Y)*Step + Grid[Visited[1]].Y;
    Z :=(Grid[NextVertex].Z-Grid[Visited[1]].Z)*Step + Grid[Visited[1]].Z;
    if Particle[P].Speed < 0.05 then
      glColor3f(1, 1, 1)
    else
      glColor3f(1, 0.7, 0.7);     // faster particles are red
    glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0); glVertex3f(X-0.5, Y-0.5, Z);
      glTexCoord2f(1.0, 0.0); glVertex3f(X+0.5, Y-0.5, Z);
      glTexCoord2f(1.0, 1.0); glVertex3f(X+0.5, Y+0.5, Z);
      glTexCoord2f(0.0, 1.0); glVertex3f(X-0.5, Y+0.5, Z);
    glEnd();
  end;
  glDisable(GL_BLEND);
  glDisable(GL_TEXTURE_2D);
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                       // Reset The View

  if ShowBackground then
  begin
    glPushMatrix();
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, backgroundTex);
      glTranslatef(0.0,0.0,-2.8);
      glRotatef(ElapsedTime/500, 0, 0, 1);
      glColor3f(1, 1, 1);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -0.9, 0);
        glTexCoord2f(1.0, 0.0); glVertex3f(+1.0, -0.9, 0);
        glTexCoord2f(1.0, 1.0); glVertex3f(+1.0, +0.9, 0);
        glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, +0.9, 0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    glPopMatrix;
  end;

  glTranslatef(0.0,0.0,-31);
  glRotatef(20, 1, 0, 0);
  glRotatef(ElapsedTime/300, 0, 1, 0);

  // Check if the particle has reached its destination. If so, select a new destination
  updateParticles;

  // Draw the particle tail
  drawParticleTail;

  // Draw the actual particles
  drawParticleHead;
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit;
var I, J : Integer;
begin
  glClearColor(0.1, 0.1, 0.4, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do
  glBlendFunc(GL_ONE, GL_ONE);

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);    //Realy Nice perspective calculations
  glEnable(GL_TEXTURE_2D);                              // Enable Texture Mapping

  //======================================
  if stafsLoaded then exit;

  LoadTexture(pathMedia+'particle1.bmp', particleTex,false);       // Load the Texture
  LoadTexture(pathMedia+'background1.bmp', backgroundTex,false);   // Load the Texture
  glBindTexture(GL_TEXTURE_2D, particleTex);            // Bind the Texture to the object

  stafsLoaded:=true;
end;


procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  LastTime :=ElapsedTime;
  ElapsedTime :=LCLIntf.GetTickCount64 - DemoStart;     // Calculate Elapsed Time
  ElapsedTime :=(LastTime + ElapsedTime)/2; // Average it out for smoother movement

  glInit;

  //=========================================================
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, double(width) / height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //==========================================================

  glDraw;
  //==========================================================
  OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=@OpenGLPanel1Paint;
    OnResize:=@OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(@OnAppIdle);

end;

procedure TForm1.FormShow(Sender: TObject);
var I, J : Integer;
begin
  DemoStart := LCLIntf.GetTickCount64;
  //......................................
  stafsLoaded:=false;
  Randomize;
  // Initialise all particles
  for I :=1 to MAX_PARTICLES do
  begin
    Particle[I].Visited[1] :=random(GRID_WIDTH*GRID_WIDTH*GRID_HEIGHT);
    for J :=2 to TAIL_LENGTH do
      Particle[I].Visited[J] :=Particle[I].Visited[1];
    Particle[I].NextVertex :=Particle[I].Visited[1];
    Particle[I].Speed :=0.01 + Round(Random(5))/100;
    Particle[I].DestValue :=0;
  end;

  // initialise grid
  for I :=0 to GRID_WIDTH * GRID_WIDTH * GRID_HEIGHT-1 do
  begin
    Grid[I].X :=  I MOD GRID_WIDTH - (GRID_WIDTH DIV 2);
    Grid[I].Z := (I DIV GRID_WIDTH) MOD GRID_WIDTH - (GRID_WIDTH DIV 2);
    Grid[I].Y := (I DIV (GRID_WIDTH*GRID_WIDTH)) MOD GRID_HEIGHT - (GRID_HEIGHT DIV 2);
  end;

end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1.Repaint;
 // OpenGLPanel1.SwapBuffers;
end;

end.

