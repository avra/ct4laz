{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit movinglightsmw;

interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, OpenGL_Textures,
  TplTimerUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    plTimer1: TTimer;
    ToolBar1: TToolBar;
    vcSpeed: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure plTimer1Progress(Sender: TObject);
    procedure vcSpeedChange(Sender: TObject);
  private
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

const
// Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../xmedia/';
{$ENDIF}

Const MAX_SEGMENTS = 64;
      MAX_DIVS = 16;

var
  ElapsedTime, xdelta,xspeed : integer;             // Elapsed time between frames
  x           : single;
  // Textures
  rock, light: GluInt;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw();
var I,J : Integer;
    Angle : glFloat;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer

  glLoadIdentity();                                       // Reset The View

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);

  glClearColor(0,0,0,0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  // Hier kommt der OpenGL Code rein

  if x<-1000000000 then
    x:= 0 else
    x:= x-xspeed/800;

  glLoadIdentity;
  glTranslatef(0,0,-5);


  //Blending aktivieren, Hintergrund belassen zum Pfaden Alpha-Wert nehmen
  glDisable(GL_BLEND);

  // Zeichnen der Bodentextur
  glColor3f(1,1,1);
  glBindTexture(GL_TEXTURE_2D, rock);
  glBegin(gl_Quads);
      glTexCoord2f(0,0);
      glVertex3f(-1,-1,0);
      glTexCoord2f(1,0);
      glVertex3f(1,-1,0);
      glTexCoord2f(1,1);
      glvertex3f(1,1,0);
      glTexCoord2f(0,1);
      glvertex3f(-1,1,0);
  glEnd;

  // Zeichnen der Lichter
  glEnable(GL_BLEND);
  glColor3f(1,0.7,0.7); //red

  glBlendFunc(GL_ONE,GL_ONE);
  glBindTexture(GL_TEXTURE_2D, light);
  glBegin(gl_Quads);
      glTexCoord2f(0,0);
      glVertex3f(-1,-1,0);
      glTexCoord2f(1,0);
      glVertex3f(1,-1,0);
      glTexCoord2f(1,1);
      glvertex3f(1,1,0);
      glTexCoord2f(0,1);
      glvertex3f(-1,1,0);
  glEnd;

  glMatrixMode(GL_TEXTURE);
  glLoadIdentity;
  glTranslatef(-x,0,0);
  glMatrixMode(GL_MODELVIEW);

  glColor3f(0.2,0.2,1);
  glBegin(gl_Quads);
      glTexCoord2f(0,0);
      glVertex3f(-1,-1,0);
      glTexCoord2f(1,0);
      glVertex3f(1,-1,0);
      glTexCoord2f(1,1);
      glvertex3f(1,1,0);
      glTexCoord2f(0,1);
      glvertex3f(-1,1,0);
  glEnd;

  glMatrixMode(GL_TEXTURE);
  glLoadIdentity;
  glTranslatef(0,x,0);
  glMatrixMode(GL_MODELVIEW);

  glColor3f(0.5,1,0.5);
  glBegin(gl_Quads);
      glTexCoord2f(0,0);
      glVertex3f(-1,-1,0);
      glTexCoord2f(1,0);
      glVertex3f(1,-1,0);
      glTexCoord2f(1,1);
      glvertex3f(1,1,0);
      glTexCoord2f(0,1);
      glvertex3f(-1,1,0);
  glEnd;

  glColor3f(1,1,1);

  glMatrixMode(GL_TEXTURE);
  glLoadIdentity;
  glMatrixMode(GL_MODELVIEW);

end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit();
Var I : Integer;
begin
  glClearColor(0.0, 0.0, 0.0, 0.0); 	   // Black Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		           // The Type Of Depth Test To Do

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations
  //...................................................

  glEnable(GL_TEXTURE_2D);	       // Aktiviert Texture Mapping
  glShadeModel(GL_SMOOTH);	       // Aktiviert weiches Shading
  glClearColor(0.0, 0.0, 0.0, 0.5);    // Bildschirm lφschen (schwarz)
  glClearDepth(1.0);		       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);	       // Aktiviert Depth Testing
  glDepthFunc(GL_LEQUAL);	       // Bestimmt den Typ des Depth Testing
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
                                       // Qualitativ bessere Koordinaten Interpolation
  LoadTexture(pathMedia+'rock.jpg',rock, false);
  LoadTexture(pathMedia+'light3.jpg',light, false);
  //.................................................
  x:=0;
  xspeed:=10;
  xdelta:=1;
end;



procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin
  if Sender=nil then ;

  //===============================================================================================
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //=========================================================
  glInit;
  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;

end;

procedure TForm1.FormShow(Sender: TObject);
begin
   plTimer1.Enabled:=True;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.vcSpeedChange(Sender: TObject);
begin
  xspeed:=vcSpeed.Position;
end;

procedure TForm1.plTimer1Progress(Sender: TObject);
begin
  if ElapsedTime<10 then xdelta:=1;
  if ElapsedTime>10000 then xdelta:=-1;

   ElapsedTime:=ElapsedTime+xdelta*xspeed;

  //..........................
  glDraw;
  OpenGLPanel1.SwapBuffers;
end;

end.

