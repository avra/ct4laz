//------------------------------------------------------------------------
//
// Author      : Maarten "McCLaw" Kronberger
// Email       : sulacomcclaw@hotmail.com
// Website     : http://www.sulaco.co.za
// Date        : 1 April 2003
// Version     : 1.0
// Description : Skeletal Character animation using Keyframe interpolation and 
//               Milkshape 3D ASCII files
//
//------------------------------------------------------------------------
unit Model;

interface
uses
  ctGL,
  Matrix,
  OpenGL_Textures,fvars,
  SysUtils;

const
  MS_MAX_NAME = 32;
  MS_MAX_PATH = 256;

type
  TVec = class
  public
    x,y,z : single;
    w     : single;	// 4th homogenous coordinate
    u,v   : single;	// texture position
    bone : integer;	// index into Model bones array

    { Transform All 4 coorditates }
	  procedure	transform( m : TMatrix );

    { Transform first 3 Coordinates }
    procedure	transform3( m : TMatrix );
  end;
type PVec = ^TVec;

type
    TTri = class
    public
      v : array [0..2] of integer;	// indices into vertices[]
	    n : array [0..2] of integer;	// indices into normals[], normal per vertex
    end;
type PTri = ^TTri;

type
  TNormal = class
  public
  	x,y,z : single;
  end;
type PNormal = ^TNormal;

type
  TShape = class
	public
    num_vertices : integer;
		vertices : array of TVec;

		num_triangles : integer;
		triangles : array of TTri;

		num_normals : integer;
		normals : array of TNormal;

		{	Constructor. }
		constructor create();

    { Destructor }
    destructor Destroy; override;

    { Load an Milkshape 3D Ascii File Section }
		function loadFromMs3dAsciiSegment(var ifile : TextFile) : boolean;

    { Render Shape }
		procedure render();
  end;
type PShape = ^TShape;

type
  TMaterial = class
  public
    canMultiTexture, isMultiTexture : boolean;

    { Constructor. }
	  constructor create(icanMultiTexture, iisMultiTexture : Boolean);

    { Load an Milkshape 3D Ascii File Section }
	  function loadFromMs3dAsciiSegment(var ifile : TextFile ) : boolean;

    { Activate Materials And Textures}
	  procedure activate();

    { Reload Textures}
	  procedure reloadTexture();

  private
	  Name : string;
  	Ambient : array [0..3] of single;
  	Diffuse : array [0..3] of single;
  	Specular : array [0..3] of single;
  	Emissive : array [0..3] of single;
  	Shininess : single;
  	Transparency : single;
  	DiffuseTexture : string;
  	AlphaTexture : string;
  	textexture : GLuint;
    MultiTexture : array [0..31] of GLUint;
    CurrentTextureIndex : integer;
  end;
type PMaterial = ^TMaterial;
type
  TKeyFrame = class
	public
		Time : single;
		Value : array [0..2] of single;
  end;
type PKeyFrame = ^TKeyFrame;

type
  TBone = class
	public
    Name : string;
		ParentName : string;
		Parent : ^TBone;						// pointer to parent bone (or NULL)
		startPosition : array [0..2] of single;
    startRotation : array [0..2] of single;
		m_relative : TMatrix;				// fixed transformation matrix relative to parent
		m_final : TMatrix;				  // absolute in accordance to animation
		NumPositionKeys : integer;
		PositionKeyFrames : array of TKeyFrame;
		NumRotationKeys : integer;
		RotationKeyFrames : array of TKeyFrame;

    { Constructor }
    constructor Create();

    { Destructor }
    destructor Destroy; override;

    { Load an Milkshape 3D Ascii File Section }
		function loadFromMs3dAsciiSegment(var ifile : TextFile ) : boolean;

    { Render Bones }
		procedure render();

    { Not Implemented }
		procedure fixup();

    { Advance Animation to Current time }
		procedure advanceTo( CurrentTime : single );

    { Initialize Bone System }
		procedure initialize();
  end;
type PBone = ^TBone;

type
  TModel = class
	public

    MaxTime : single;		// length of animation (= Frames)
		CurrentTime : single;	// advancement within animation

		num_shapes : integer;
		shapes : array of TShape;
		material_indices : array of integer;

		num_materials : integer;
		materials : array of TMaterial;

		num_bones : integer;
		bones :  array of TBone;

    canMultiTexture, isMultiTexture, isSphereMap : Boolean;
    { Constructor. }
		constructor Create(icanMultiTexture, iisMultiTexture : Boolean);

    { Destructor }
    destructor Destroy; override;

    { Load a Milkshape 3D Ascii File }
		function loadFromMs3dAsciiFile( filename : string ) : boolean;

    { Reload all Textures }
		procedure	reloadTextures();

    { Render Model}
		procedure	render();

    { Render Bones }
		procedure	renderBones();

    { Link Child And Parent Bones }
		function linkBones() : boolean;

    { Initialize All The Bones }
		procedure	initializeBones();

    { Advance Animation To DeltaTime }
		procedure	advanceAnimation( deltaTime : single );

    { Attach The Mesh To The Bones }
		procedure	attachSkin();


  end;

implementation


{ TVec }

{---------------------------------------------------------------------}
{  Transform All 4 coorditates                                        }
{---------------------------------------------------------------------}
procedure TVec.transform(m: TMatrix);
var matrix : array [0..15] of single;
    vector : array [0..3] of single;
begin

  m.getMatrix(matrix);

	vector[0] := x*matrix[0]+y*matrix[4]+z*matrix[8]+matrix[12];
	vector[1] := x*matrix[1]+y*matrix[5]+z*matrix[9]+matrix[13];
	vector[2] := x*matrix[2]+y*matrix[6]+z*matrix[10]+matrix[14];
	vector[3] := x*matrix[3]+y*matrix[7]+z*matrix[11]+matrix[15];

  x := vector[0];
	y := vector[1];
	z := vector[2];
	w := vector[3];

end;

{---------------------------------------------------------------------}
{  Transform first 3 Coordinates                                      }
{---------------------------------------------------------------------}
procedure TVec.transform3(m: TMatrix);
var matrix : array [0..15] of single;
    vec : array [0..2] of single;
begin
	m.getMatrix(matrix);

	vec[0] := x*matrix[0]+y*matrix[4]+z*matrix[8];
	vec[1] := x*matrix[1]+y*matrix[5]+z*matrix[9];
	vec[2] := x*matrix[2]+y*matrix[6]+z*matrix[10];

  x := vec[0];
  y := vec[1];
  z := vec[2];

end;

{ TShape }

{---------------------------------------------------------------------}
{  Constructor.                                                       }
{---------------------------------------------------------------------}
constructor TShape.create;
begin
  num_vertices := 0;
	vertices := Nil;
	num_triangles := 0;
	triangles := Nil;
	num_normals := 0;
	normals := Nil;
end;

{---------------------------------------------------------------------}
{  Destructor                                                         }
{---------------------------------------------------------------------}
destructor TShape.Destroy;
var
  I: Integer;
begin
  for I := (num_vertices - 1) downto 0 do
    if Assigned(vertices[I]) then
      vertices[I].Free;

  for I := (num_triangles - 1) downto 0 do
    if Assigned(triangles[I]) then
      triangles[I].Free;

  for I := (num_normals - 1) downto 0 do
    if Assigned(normals[I]) then
      normals[I].Free;

  inherited Destroy;
end;

{---------------------------------------------------------------------}
{  Render Shape                                                       }
{---------------------------------------------------------------------}
procedure TShape.render;
var i,j : integer;
	  tri : TTri;
	  vec : TVec;
	  N   : TNormal;
begin
//	glEnable(GL_LIGHTING);
	glBegin(GL_TRIANGLES);
    for i := 0 to num_triangles - 1	do// for each triangle
    begin
      tri := triangles[i];				// get next triangle to triangle

      for j := 0 to 2 do	// 3 vertices of the triangle
      begin
        N := normals[tri.n[j]];
        glNormal3f(N.x, N.y, N.z);	// set normal vector  (object space)

        vec := vertices[tri.v[j]];		// pointer to vertex
        // Give OpenGL the current terrain texture coordinate for our height map
	      glMultiTexCoord2fARB(GL_TEXTURE0_ARB, vec.u, vec.v);

	      // Give OpenGL the current detail texture coordinate for our height map
        glMultiTexCoord2fARB(GL_TEXTURE1_ARB, vec.u, vec.v);
        //glTexCoord2f (vec.u, vec.v);			// texture coordinate

        glVertex3f( vec.x, vec.y, vec.z );	// 3d coordinate (object space)
      end;
    end;
	glEnd();
end;


{---------------------------------------------------------------------}
{  Load an Milkshape 3D Ascii File Section                            }
{---------------------------------------------------------------------}
function TShape.loadFromMs3dAsciiSegment(var ifile: TextFile): boolean;
var nFlags, nIndex, j : integer;
begin
  //
  // vertices
  //
  Readln(ifile,num_vertices);
  if eof(ifile) then
  begin
    result := false;
    exit;
  end;
  SetLength(vertices,num_vertices);
  if (num_vertices > 0)then
  begin

    for j := 0 to num_vertices - 1 do
    begin
      //init
      vertices[j] := TVec.Create;

      Readln(ifile,nFlags,vertices[j].x, vertices[j].y, vertices[j].z,vertices[j].u, vertices[j].v,vertices[j].bone);

      if eof(ifile) then
      begin
        result := false;
        exit;
      end;

      // adjust the y direction of the texture coordinate
      vertices[j].v := 1.0 - vertices[j].v;
    end;
  end;
  
  //
  // normals
  //
  Readln(ifile,num_normals);
  if eof(ifile) then
  begin
    result := false;
    exit;
  end;


  SetLength(normals,num_normals);
  if num_normals > 0 then
  begin
    for j := 0 to num_normals - 1 do
    begin

      //init
      normals[j] := TNormal.Create;

      Readln(ifile,normals[j].x, normals[j].y, normals[j].z);
      if eof(ifile) then
      begin
        result := false;
        exit;
      end;

    end;
  end;

  //
  // triangles
  //
  Readln(ifile,num_triangles);

  if eof(ifile) then
  begin
    result := false;
    exit;
  end;

  SetLength (triangles,num_triangles);

  for j := 0 to num_triangles - 1 do
  begin

    triangles[j] := TTri.Create;

    ReadLn(ifile,nFlags,triangles[j].v[0], triangles[j].v[1], triangles[j].v[2],triangles[j].n[0], triangles[j].n[1], triangles[j].n[2],nIndex);
    if eof(ifile) then
    begin
      result := false;
      exit;
    end;

    assert(triangles[j].v[0] >= 0);
    assert(triangles[j].v[0] < num_vertices);
    assert(triangles[j].v[1] >= 0);
    assert(triangles[j].v[1] < num_vertices);
    assert(triangles[j].v[2] >= 0);
    assert(triangles[j].v[2] < num_vertices);
  end;

  result := true;
end;

{ TMaterial }

{---------------------------------------------------------------------}
{  Activate Materials And Textures                                    }
{---------------------------------------------------------------------}
procedure TMaterial.activate;
begin
  glMaterialfv( GL_FRONT, GL_AMBIENT, @Ambient );
	glMaterialfv( GL_FRONT, GL_DIFFUSE, @Diffuse );
	glMaterialfv( GL_FRONT, GL_SPECULAR, @Specular );
	glMaterialfv( GL_FRONT, GL_EMISSION, @Emissive );
	glMaterialf( GL_FRONT, GL_SHININESS, Shininess );

	if ( textexture > 0 ) then
	begin
    if(canMultiTexture) then
    begin
      glActiveTextureARB(GL_TEXTURE0_ARB);
      glBindTexture( GL_TEXTURE_2D, textexture );
      glEnable( GL_TEXTURE_2D );
      if(isMultiTexture) then
      begin
        glActiveTextureARB(GL_TEXTURE1_ARB);
        glBindTexture( GL_TEXTURE_2D,MultiTexture[CurrentTextureIndex]);
        glEnable( GL_TEXTURE_2D );
        CurrentTextureIndex := CurrentTextureIndex + 1;
        if(CurrentTextureIndex > 31) then
          CurrentTextureIndex := 0;
      end;
    end
    else
    begin
      glBindTexture( GL_TEXTURE_2D, textexture );
		  glEnable( GL_TEXTURE_2D );
    end;
	end
	else
		glDisable( GL_TEXTURE_2D );
end;

{---------------------------------------------------------------------}
{  Constructor.                                                       }
{---------------------------------------------------------------------}
constructor TMaterial.Create(icanMultiTexture, iisMultiTexture : Boolean);
begin
  { TODO : Stefanos Fix this 9999 }

  // Only load these if we want to multitexture
  {
  canMultiTexture := icanMultiTexture;
  isMultiTexture := iisMultiTexture;

  if(canMultiTexture and isMultiTexture) then
  begin
    LoadTexture('Caust00.bmp',MultiTexture[0],false);
    LoadTexture('Caust01.bmp',MultiTexture[1],false);
    LoadTexture('Caust02.bmp',MultiTexture[2],false);
    LoadTexture('Caust03.bmp',MultiTexture[3],false);
    LoadTexture('Caust04.bmp',MultiTexture[4],false);
    LoadTexture('Caust05.bmp',MultiTexture[5],false);
    LoadTexture('Caust06.bmp',MultiTexture[6],false);
    LoadTexture('Caust07.bmp',MultiTexture[7],false);
    LoadTexture('Caust08.bmp',MultiTexture[8],false);
    LoadTexture('Caust09.bmp',MultiTexture[9],false);
    LoadTexture('Caust10.bmp',MultiTexture[10],false);
    LoadTexture('Caust11.bmp',MultiTexture[11],false);
    LoadTexture('Caust12.bmp',MultiTexture[12],false);
    LoadTexture('Caust13.bmp',MultiTexture[13],false);
    LoadTexture('Caust14.bmp',MultiTexture[14],false);
    LoadTexture('Caust15.bmp',MultiTexture[15],false);
    LoadTexture('Caust16.bmp',MultiTexture[16],false);
    LoadTexture('Caust17.bmp',MultiTexture[17],false);
    LoadTexture('Caust18.bmp',MultiTexture[18],false);
    LoadTexture('Caust19.bmp',MultiTexture[19],false);
    LoadTexture('Caust20.bmp',MultiTexture[20],false);
    LoadTexture('Caust21.bmp',MultiTexture[21],false);
    LoadTexture('Caust22.bmp',MultiTexture[22],false);
    LoadTexture('Caust23.bmp',MultiTexture[23],false);
    LoadTexture('Caust24.bmp',MultiTexture[24],false);
    LoadTexture('Caust25.bmp',MultiTexture[25],false);
    LoadTexture('Caust26.bmp',MultiTexture[26],false);
    LoadTexture('Caust27.bmp',MultiTexture[27],false);
    LoadTexture('Caust28.bmp',MultiTexture[28],false);
    LoadTexture('Caust29.bmp',MultiTexture[29],false);
    LoadTexture('Caust30.bmp',MultiTexture[30],false);
    LoadTexture('Caust31.bmp',MultiTexture[31],false);
    CurrentTextureIndex := 0;
  end;

   }

end;

{---------------------------------------------------------------------}
{  Load an Milkshape 3D Ascii File Section                            }
{---------------------------------------------------------------------}
function TMaterial.loadFromMs3dAsciiSegment(var ifile: TextFile): boolean;
var szLine : string;
begin
    // name
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,szLine);
    Name := StringReplace(szLine,'"','',[rfReplaceAll]);

    // ambient
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Ambient[0], Ambient[1], Ambient[2], Ambient[3]);

    // diffuse
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Diffuse[0], Diffuse[1], Diffuse[2], Diffuse[3]);

    // specular
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Specular[0], Specular[1], Specular[2], Specular[3]);

    // emissive
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Emissive[0], Emissive[1], Emissive[2], Emissive[3]);


    // shininess
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Shininess);

    // transparency
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Transparency);

    // diffuse texture
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

	  DiffuseTexture := '';
    Readln(ifile,DiffuseTexture);

    DiffuseTexture := StringReplace(DiffuseTexture,'"','',[rfReplaceAll]);

    // alpha texture
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    AlphaTexture := '';
    Readln(ifile,AlphaTexture);

    AlphaTexture := StringReplace(AlphaTexture,'"','',[rfReplaceAll]);

	  reloadTexture();

	  result := true;
end;

{---------------------------------------------------------------------}
{  Reload Textures                                                    }
{---------------------------------------------------------------------}
procedure TMaterial.reloadTexture;
begin
  if( length(DiffuseTexture) > 0 ) then
    if( FileExists(DiffuseTexture)) then
      LoadTexture(DiffuseTexture, textexture,false)
    else
      LoadTexture(pathMedia+ DiffuseTexture, textexture,false)
	else
		textexture := 0;
end;

{ TBone }

{---------------------------------------------------------------------}
{  Constructor                                                        }
{---------------------------------------------------------------------}
constructor TBone.create;
begin
  m_relative := TMatrix.create;				// fixed transformation matrix relative to parent
  m_final := TMatrix.create;				// absolute in accordance to animation
end;

{---------------------------------------------------------------------}
{  Destructor                                                         }
{---------------------------------------------------------------------}
destructor TBone.Destroy;
var
  i: Integer;
begin
  for i := (NumRotationKeys -1) downto 0 do
    if Assigned(RotationKeyFrames[i]) then
      RotationKeyFrames[i].Free;

  for i := (NumPositionKeys - 1) downto 0 do
    if Assigned(PositionKeyFrames[i]) then
      PositionKeyFrames[i].Free;

  if Assigned(m_relative) then
    FreeAndNil(m_relative);
  if Assigned(m_final) then
    FreeAndNil(m_final);

  inherited Destroy;
end;


{---------------------------------------------------------------------}
{  Advance Animation to Current time                                  }
{---------------------------------------------------------------------}
procedure TBone.advanceTo(CurrentTime: single);
var
  i : integer;
  deltaTime : single;
  fraction : single;
  Position : array [0..2] of single;
  Rotation : array [0..2] of single;
  m_rel , m_frame : TMatrix;
  tempm : array [0..15] of single;
begin
  //
  // Position
  //

  // Find appropriate position key frame
  i := 0;
  while ( (i < NumPositionKeys-1) and (PositionKeyFrames[i].Time < CurrentTime) ) do
    i := i + 1;

  assert(i < NumPositionKeys);


  if( i > 0 ) then
  begin
    // Interpolate between 2 key frames

    // time between the 2 key frames
    deltaTime := PositionKeyFrames[i].Time - PositionKeyFrames[i-1].Time;

    assert( deltaTime > 0 );

    // relative position of interpolation point to the keyframes [0..1]
    fraction := (CurrentTime - PositionKeyFrames[i-1].Time) / deltaTime;

    assert( fraction > 0 );
    //assert( fraction < 1.0 );
    if(fraction > 1.0) then
       fraction := 1.0;
    Position[0] := PositionKeyFrames[i-1].Value[0] + fraction * (PositionKeyFrames[i].Value[0] - PositionKeyFrames[i-1].Value[0]);
    Position[1] := PositionKeyFrames[i-1].Value[1] + fraction * (PositionKeyFrames[i].Value[1] - PositionKeyFrames[i-1].Value[1]);
    Position[2] := PositionKeyFrames[i-1].Value[2] + fraction * (PositionKeyFrames[i].Value[2] - PositionKeyFrames[i-1].Value[2]);
  end
  else
  begin
    Position[0] := PositionKeyFrames[i].Value[0];
    Position[1] := PositionKeyFrames[i].Value[1];
    Position[2] := PositionKeyFrames[i].Value[2];
  end;


  //
  // Rotation
  //

  // Find appropriate rotation key frame
  i := 0;
  while( (i < NumRotationKeys-1) and (RotationKeyFrames[i].Time < CurrentTime) ) do
          i := i + 1;

  assert(i < NumRotationKeys);

  if( i > 0 ) then
  begin
          // Interpolate between 2 key frames

          // time between the 2 key frames
          deltaTime := RotationKeyFrames[i].Time - RotationKeyFrames[i-1].Time;
          assert( deltaTime > 0 );

          // relative position of interpolation point to the keyframes [0..1]
          fraction := (CurrentTime - RotationKeyFrames[i-1].Time) / deltaTime;
          assert( fraction > 0 );
          //assert( fraction < 1.0 );
          if( fraction > 1.0) then
            fraction := 1.0;
          Rotation[0] := RotationKeyFrames[i-1].Value[0] + fraction * (RotationKeyFrames[i].Value[0] - RotationKeyFrames[i-1].Value[0]);
          Rotation[1] := RotationKeyFrames[i-1].Value[1] + fraction * (RotationKeyFrames[i].Value[1] - RotationKeyFrames[i-1].Value[1]);
          Rotation[2] := RotationKeyFrames[i-1].Value[2] + fraction * (RotationKeyFrames[i].Value[2] - RotationKeyFrames[i-1].Value[2]);
  end
  else
  begin
    Rotation[0] := RotationKeyFrames[i].Value[0];
    Rotation[1] := RotationKeyFrames[i].Value[1];
    Rotation[2] := RotationKeyFrames[i].Value[2];
  end;

  // Now we know the position and rotation for this animation frame.
  // Let's calculate the transformation matrix (m_final) for this bone...

  m_rel := TMatrix.create;
  m_frame := TMatrix.create;

  // Create a transformation matrix from the position and rotation of this
  // joint in the rest position
  m_rel.setRotationRadians( startRotation );
  m_rel.setTranslation( startPosition );

  // Create a transformation matrix from the position and rotation
  // m_frame: additional transformation for this frame of the animation
  m_frame.setRotationRadians( Rotation );
  m_frame.setTranslation( Position );

  // Add the animation state to the rest position
  m_rel.postMultiply( m_frame );

  //
  if ( Parent = nil ) then					// this is the root node
  begin
    m_rel.getMatrix(tempm);
    m_final.setMatrixValues(tempm);	// m_final := m_rel
  end
  else									// not the root node
  begin
    // m_final := parent's m_final * m_rel (matrix concatenation)
    Parent.m_final.getMatrix(tempm);
    m_final.setMatrixValues(tempm);
    m_final.postMultiply( m_rel );
  end;

  { >> Scotts Added Stuff }
  FreeAndNil(m_frame);
  FreeAndNil(m_rel);
end;

{---------------------------------------------------------------------}
{  Not Implemented                                                    }
{---------------------------------------------------------------------}
procedure TBone.fixup;
begin
  {TODO : Look for the code.Is this inherent in c++?}
end;

{---------------------------------------------------------------------}
{  Initialize Bone System                                             }
{---------------------------------------------------------------------}
procedure TBone.initialize;
var m_rel : TMatrix;
    tempm : array [0..15] of single;
begin
  m_rel := TMatrix.create;
  // Create a transformation matrix from the position and rotation

  m_rel.setRotationRadians( startRotation );
  m_rel.setTranslation( startPosition );

  // Each bone's final matrix is its relative matrix concatenated onto its
  // parent's final matrix (which in turn is ....)
  //
  if ( Parent = nil ) then
  begin
    m_rel.getMatrix(tempm);
    m_final.setMatrixValues(tempm);
  end
  else
  begin
    Parent.m_final.getMatrix(tempm);
    m_final.setMatrixValues(tempm);
    m_final.postMultiply( m_rel );
  end;

  { >> Scotts Added Stuff }
  FreeAndNil(m_rel);
end;

{---------------------------------------------------------------------}
{  Render Bones                                                       }
{---------------------------------------------------------------------}
procedure TBone.render;
var vector , parentvector : TVec;
begin
  vector := TVec.Create;
  parentvector := TVec.Create;

  vector.x := 0;
  vector.y := 0;
  vector.z := 0;
  vector.w := 1;
  vector.transform( m_final );

  if( Parent <> nil ) then
  begin
    parentvector.x := 0;
    parentvector.y := 0;
    parentvector.z := 0;
    parentvector.w := 1;
    parentvector.transform( Parent.m_final );
  end;

  glDisable( GL_TEXTURE_2D );

  // render bone as a line
  glLineWidth(1.0);
  glColor3f(1.0, 0, 0);
  glBegin(GL_LINES);
  glVertex3f( vector.x, vector.y, vector.z );
  if( Parent <> nil ) then
    glVertex3f( parentvector.x, parentvector.y, parentvector.z )
  else
    glVertex3f( vector.x, vector.y, vector.z );

  glEnd();

  // render bone-ends as fat points
  glPointSize(2.0);
  glColor3f(1.0, 0, 1.0);
  glBegin(GL_POINTS);
  glVertex3f( vector.x, vector.y, vector.z );
  if( Parent <> nil ) then
    glVertex3f( parentvector.x, parentvector.y, parentvector.z );
    
  glEnd();
  glColor3f(1.0, 1.0, 1.0);

  { >> Scotts Added Stuff }
  FreeAndNil(vector);
  FreeAndNil(parentvector);
end;

{---------------------------------------------------------------------}
{  Load an Milkshape 3D Ascii File Section                            }
{---------------------------------------------------------------------}
function TBone.loadFromMs3dAsciiSegment(var ifile: TextFile): boolean;
var szLine : string;
    j ,nFlags : integer;
begin
    // name
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,Name);

    Name := StringReplace(Name,'"','',[rfReplaceAll]) ;

    // parent name
     if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    ParentName := '';
    Readln(ifile,ParentName);

    ParentName := StringReplace(ParentName,'"','',[rfReplaceAll]) ;

    // Start Position
	  if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;
    ReadLn(ifile,nFlags,
                 startPosition[0], startPosition[1], startPosition[2],
                 startRotation[0], startRotation[1], startRotation[2]);


    // position key count
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;
    Readln(ifile,NumPositionKeys);


	  SetLength(PositionKeyFrames,NumPositionKeys);

    for j := 0 to NumPositionKeys - 1 do
    begin

      PositionKeyFrames[j] := TKeyFrame.Create;

		  if (eof(ifile)) then
      begin
		    result := false;
        exit;
      end;

      Readln(ifile , PositionKeyFrames[j].Time,
			PositionKeyFrames[j].Value[0],
			PositionKeyFrames[j].Value[1],
			PositionKeyFrames[j].Value[2] );

    end;

    // rotation key count
    if (eof(ifile)) then
    begin
		  result := false;
      exit;
    end;

    Readln(ifile,NumRotationKeys);

	  SetLength(RotationKeyFrames,NumRotationKeys);


    for j := 0 to NumRotationKeys -1 do
    begin

		  RotationKeyFrames[j] :=  TKeyFrame.Create;

      if (eof(ifile)) then
      begin
		    result := false;
        exit;
      end;

        Readln(ifile , RotationKeyFrames[j].Time,
        RotationKeyFrames[j].Value[0],
			  RotationKeyFrames[j].Value[1],
			  RotationKeyFrames[j].Value[2] );

    end;
	
	result := true;
end;

{ TModel }

{---------------------------------------------------------------------}
{  Advance Animation To DeltaTime                                     }
{---------------------------------------------------------------------}
procedure TModel.advanceAnimation(deltaTime: single);
var i : integer;
begin
	CurrentTime := CurrentTime + deltaTime;
	if( CurrentTime > MaxTime ) then	// this is a looped animation!
		CurrentTime := 1.0;		// N.B. time starts at 1, not at 0!

  for i := 0 to num_bones -1 do
	  bones[i].advanceTo( CurrentTime );

end;

{---------------------------------------------------------------------}
{  Attach The Mesh To The Bones                                       }
{---------------------------------------------------------------------}
procedure TModel.attachSkin;
var i, j : integer;
	bone : integer;
	matrix : TMatrix;
	v : array [0..2] of single;
begin
	// Make vertex vector relative to the bone it's attached to

	for i := 0 to num_shapes - 1 do	// for all shapes
	begin
		for j := 0 to shapes[i].num_vertices - 1 do	// for all vertices
		begin
			bone := shapes[i].vertices[j].bone;

			if( bone <> -1 ) then	// if vertex attached to a bone
			begin
				matrix := bones[bone].m_final;

				// make relative to bone position and orientation
				// modifies vertex x,y,z

				v[0] := shapes[i].vertices[j].x;
				v[1] := shapes[i].vertices[j].y;
				v[2] := shapes[i].vertices[j].z;
				matrix.inverseTranslateVect( v );
				matrix.inverseRotateVect( v );
				shapes[i].vertices[j].x := v[0];
				shapes[i].vertices[j].y := v[1];
				shapes[i].vertices[j].z := v[2];
			end;

		end;
	end;
end;

{---------------------------------------------------------------------}
{  Constructor.                                                       }
{---------------------------------------------------------------------}
constructor TModel.Create(icanMultiTexture , iisMultiTexture : Boolean);
begin
  num_shapes := 0;
  shapes := nil;
  num_materials := 0;
  materials := nil;

  canMultiTexture := icanMultiTexture;
  isMultiTexture := iisMultiTexture;
  {$IFDEF Windows}
  if (icanMultiTexture and isMultiTexture) then
  begin
    glMultiTexCoord2fARB := wglGetProcAddress('glMultiTexCoord2fARB');
    glActiveTextureARB := wglGetProcAddress('glActiveTextureARB');
  end;
  {$ENDIF}
end;
{---------------------------------------------------------------------}
{  Destructor                                                         }
{---------------------------------------------------------------------}
destructor TModel.Destroy;
var
  i: Integer;
begin
  for i := (num_bones -1) downto 0 do
    if Assigned(bones[i]) then
      bones[i].Free;

  for i := (num_materials - 1) downto 0 do
    if Assigned(materials[i]) then
      materials[i].Free;

  for i := (num_shapes - 1) downto 0 do
    if Assigned(shapes[i]) then
      shapes[i].Free;

  inherited Destroy;
end;

{---------------------------------------------------------------------}
{  Initialize All The Bones                                           }
{---------------------------------------------------------------------}


procedure TModel.initializeBones;
var i : integer;
begin
  for i := 0 to num_bones-1 do
			bones[i].initialize;
end;

{---------------------------------------------------------------------}
{  Link Child And Parent Bones                                        }
{---------------------------------------------------------------------}
function TModel.linkBones: boolean;
var i, j : integer;
begin
  // The relationship between child and parent bones are defined
	// by each child bone providing the name of its parent bone.
	// This function builds corresponding pointers for faster lookup.


	// Link children to parent
  for i := 0 to num_bones-1 do
	begin

		bones[i].Parent := nil;

		if( length(bones[i].ParentName) >  0 ) then	// does bone have parent?
		begin
			for j := 0 to num_bones -1 do // search for parent
			begin
				if( bones[j].Name = bones[i].ParentName) then	// j is parent of i
				begin
					bones[i].Parent := @bones[j];
					break;
				end;
			end;
			if ( bones[i].Parent = nil) then // Unable to find parent bone
      begin
        result := false;
        exit;
      end;
    end;
  end;
	result := true;
end;

{---------------------------------------------------------------------}
{  Load a Milkshape 3D Ascii File                                     }
{---------------------------------------------------------------------}
function TModel.loadFromMs3dAsciiFile(filename: string): boolean;
var StartTime : single;
    bError :boolean;
    szLine , szName , strTemp : string;
    nFrame, nFlags, nIndex, i : integer;
    fModel :  TextFile;
begin
  bError := false;

  AssignFile(fModel,filename);
  Reset(fModel);

	CurrentTime := 0;

  while ((not eof(fModel)) and (not bError)) do
  begin
    Readln(fmodel,szLine);
    if ( copy(szLine,0,2) = '//') then
      continue;

    if szLine = '' then
       continue;

    if (pos('Frames:',szLine) = 1) then
    begin
      nFrame := StrToInt(copy(szLine,8,length(szLine)));
			MaxTime := 1.0 * nFrame;		// length of animation
      continue;
    end;

    if (pos('Frame:',szLine) = 1) then
    begin
      nFrame := StrToInt(copy(szLine,7,length(szLine)));
  		StartTime := 1.0 * nFrame;
      continue;
    end;

    if (pos('Meshes: ',szLine) = 1) then
    begin
      num_shapes := StrToInt(StringReplace(szLine,'Meshes: ','',[rfReplaceAll]));
      SetLength(shapes,num_shapes);
      SetLength(material_indices, num_shapes);
      for i := 0 to num_shapes - 1 do
      begin
        shapes[i] := TShape.create;
        material_indices[num_shapes-1] := 0;
      end;
      
      for i := 0 to num_shapes - 1 do
      begin

          // mesh: name, flags, material index
          Readln(fmodel,szName);
          if (eof(fmodel)) then
          begin
              bError := true;
              break;
          end;

          //remove "
          szName := StringReplace(szName,'"','',[rfReplaceAll]);

          //get nFlags
          strTemp := szName;
          strTemp := copy(strTemp,pos(' ',strTemp)+1,length(strTemp));
          nFlags := StrToInt(copy(strTemp,0,pos(' ',strTemp)-1));
          //get nIndex
          nIndex := StrToInt(copy(strTemp,pos(' ',strTemp)+1,length(strTemp)));

				  material_indices[i] := nIndex;

				  if( not shapes[i].loadFromMs3dAsciiSegment(fModel) ) then
          begin
              bError := true;
              break;
          end;
			end;
      continue;
		end;

    //
    // materials
    //
    if (pos('Materials: ',szLine) = 1) then
    begin


      num_materials := StrToInt(StringReplace(szLine,'Materials: ','',[rfReplaceAll]));

      SetLength(materials,num_materials);

      // Init materials
      for i := 0 to num_materials -1 do
        materials[i] := TMaterial.create(canMultiTexture,isMultiTexture);

      for i := 0 to num_materials -1 do
      begin

 				if( not materials[i].loadFromMs3dAsciiSegment(fModel) ) then
        begin
            bError := true;
            break;
        end;
			end;
      continue;
    end;

		//
    // bones
    //
    if (pos('Bones: ',szLine) = 1) then
    begin


        num_bones := StrToInt(StringReplace(szLine,'Bones: ','',[rfReplaceAll]));

        SetLength(bones,num_bones);

        // Initialize Bones
        for i := 0 to num_bones -1 do
          bones[i] := TBone.create;

        for i := 0 to num_bones -1 do
        begin

            if( not bones[i].loadFromMs3dAsciiSegment(fModel) ) then
            begin
                bError := true;
                break;
            end;
        end;
        continue;
    end;
  end;

	CloseFile(fModel);

	if( not linkBones() ) then
  begin
    result := false;
    exit;
  end;
	initializeBones();
	attachSkin();
	advanceAnimation( StartTime );
	result := true;
end;

{---------------------------------------------------------------------}
{  Reload all Textures                                                }
{---------------------------------------------------------------------}
procedure TModel.reloadTextures;
var i : integer;
begin
	for i := 0 to num_materials - 1 do	// for each shape
		materials[i].reloadTexture();

end;

{---------------------------------------------------------------------}
{  Render Model                                                       }
{---------------------------------------------------------------------}
procedure TModel.render;
var k,i,j,materialIndex : integer;
    tri : TTri;
    N : TNormal;
    vec : TVec;
    bone : TBone;
    v : array [0..2] of single;
    matrix : TMatrix;
begin
  for k := 0 to num_shapes - 1 do	// for each shape
  begin
    materialIndex := material_indices[k];
    if ( materialIndex >= 0 ) then
      materials[materialIndex].activate()
    else
    begin
      // Material properties?
      glDisable( GL_TEXTURE_2D );
    end;




    glBegin(GL_TRIANGLES);
    for i := 0 to shapes[k].num_triangles - 1 do	// for each triangle
    begin
      tri := shapes[k].triangles[i];				// pointer to triangle

      for j := 0 to 2 do	// 3 vertices of the triangle
      begin
        N := shapes[k].normals[tri.n[j]];
        glNormal3f(N.x, N.y, N.z);	// set normal vector  (object space)

        vec := shapes[k].vertices[tri.v[j]];		// pointer to vertex
        if (canMultiTexture) then
        begin
          // Give OpenGL the current terrain texture coordinate
	        glMultiTexCoord2fARB(GL_TEXTURE0_ARB, vec.u, vec.v);

          if(isMultiTexture) then
          begin
            // Give OpenGL the current detail texture coordinate
            glMultiTexCoord2fARB(GL_TEXTURE1_ARB, vec.u, vec.v);
            //glTexCoord2f (vec.u, vec.v);			// texture coordinate
          end;
        end
        else
        begin
          // Just do the Boring Old Texture Mapping.
          glTexCoord2f (vec.u, vec.v);			// texture coordinate
          // Give OpenGL the current terrain texture coordinate

	        //glMultiTexCoord2fARB(GL_TEXTURE0_ARB, vec.u, vec.v);
        end;
        
        if( vec.bone = -1 ) then
        begin
          glVertex3f( vec.x, vec.y, vec.z );
        end
        else
        begin
          bone := bones[vec.bone];
          matrix := bone.m_final;

          v[0] := vec.x;
          v[1] := vec.y;
          v[2] := vec.z;
          matrix.rotateVect( v );
          matrix.translateVect( v );
          glVertex3fv( @v );
        end;
      end;
    end;
    glEnd();
  end;
end;

{---------------------------------------------------------------------}
{  Render Bones                                                       }
{---------------------------------------------------------------------}
procedure TModel.renderBones;
var i : integer;
begin
	for i := 0 to num_bones - 1 do	// for each bone
	  bones[i].render();

end;

end.
