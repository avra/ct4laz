unit Boid;

interface

uses
  Graphics,
  Math,
  Classes,
  GL,
  Animate,
  SysUtils,
  Contnrs;

type
  TBoid = class;
  PBoid = ^TBoid;
  TBoidGene = class;
  T2DCoord = class
  public
    X, Y : single;
    constructor Create( iX, iY : single);
  end;
  TColor = class
    public
    r, g, b : single;
    constructor Create(red, green, blue : single);
  end;
  TFlock = class
    private
    { FIELDS }

      numberOfMembers : integer;	                              // Number of critters on each team.
      numberOfTeams : integer;                                  // Number of teams.
      members : TObjectList;                                     // The critters.
      meanHeading, meanSpeed, meanX, meanY : double;        // Flock averages.
      height, width : double;                               // The world.  (0,0) is in the upper left corner.

      procedure checkBounds( boid : TBoid);
      procedure addToMomentum( b : TBoid);



      function FindAngle( FSourceX, FSourceY, FDestX, FDestY  : single) : single;

    public

      Skins : array of TAnimate;

      { CONSTRUCTOR }
      constructor Create(size, teams : integer; h, w : double; skin : string; icanMultitexture, iisMultiTexture : boolean);

      { DESTRUCTOR }
      destructor Destroy; override;

      { METHODS }

      procedure moveMembers;

      { UTILITY METHODS }

      function calculateMeanX : double;
      function calculateMeanY : double;

      //procedure initializeFlock;
      procedure initializeDuel( gene, challenger : TBoidGene);
      procedure add( b : TBoid);
      procedure addBoid(direction,
                        magnitude,
                        xPos,
                        yPos,
                        maxSpeed,
                        minSpeed,
                        angle,
                        accel,
                        sensor,
                        death,
                        deathC,
                        collision,
                        attackC,
                        retreatC: double;
                        iteamIndex: integer;
                        iTeamColorR,
                        iTeamColorG,
                        iTeamColorB : single);

      procedure remove( b : TBoid);
      function elements : TList;
      procedure Render(ShowLines, ShowEnemyLines : boolean);
      { GET AND SET METHODS }



      function getNumberOfMembers : integer;
      procedure setNumberOfMembers( n : integer);


      function getNumberOfTeams : integer;
      procedure setNumberOfTeams( teams : integer);

      function getMeanHeading : double;
      procedure setMeanHeading( heading : double); // Priv?

      function getMeanSpeed : double;
      procedure setMeanSpeed( speed : double); // Priv?

      function getMeanX : double;
      procedure setMeanX( x : double); // Priv?

      function getMeanY : double;
      procedure setMeanY( y : double); // Priv?

      function getHeight : double;
      procedure setHeight( h : double);

      function getWidth : double;
      procedure setWidth( w : double);


  end;
  PFlock = ^TFlock;

  TBoidGene = class
  private

    { FIELDS }
    numberOfTrials : double;
    trialsWon :double;
  
    heading,
    speed,
    x,
    y,
    maximumSpeed,
    minimumSpeed,
    turnAngle,
    acceleration,
    sensorRange,
    collisionRange,
    deathRange,
    deathConstant,
    attackConstant,
    retreatConstant : double;
    color : TColor;
    flock : TFlock;
  public

    teamIndex : integer;

    { CONSTRUCTORS }
    constructor Create( direction,
                 magnitude,
                 xPos,
                 yPos,
                 maxSpeed,
                 minSpeed,
                 angle,
                 accel,
                 sensor,
                 death,
                 deathC,
                 collision,
                 attackC,
                 retreatC : double;
                 c : TColor;
                 gaggle : TFlock;
                 iteamIndex : integer);

    procedure setHeading( direction : double);
    procedure setSpeed( magnitude : double);
    procedure setX( xPos : double);
    procedure setY( yPos : double);
    procedure setMaximumSpeed( s : double);
    procedure setMinimumSpeed( s : double);
    procedure setColor( c : TColor);
    procedure setTurnAngle( a : double);
    procedure setAcceleration( a : double);
    procedure setSensorRange( sensor : double);
    procedure setDeathRange( death : double);
    procedure setDeathConstant( c : double);
    procedure setAttackConstant( c : double);
    procedure setRetreatConstant( c : double);
    procedure setCollisionRange( collision : double);
    procedure setFlock( gaggle : TFlock);
    procedure setNumberOfTrials( n : double);
    procedure setTrialsWon( t : double);

    { FUNCTIONS }

    function reproduceWith( o : TBoidGene) : TBoidGene;

    function getNumberOfTrials : double;
    function getTrialsWon : double;
    function getFitness : double;
    function getAcceleration: double;
    function getAttackConstant: double;
    function getCollisionRange: double;
    function getColor: TColor;
    function getDeathConstant: double;
    function getDeathRange: double;
    function getFlock: TFlock;
    function getHeading: double;
    function getMaximumSpeed: double;
    function getMinimumSpeed: double;
    function getRetreatConstant: double;
    function getSensorRange: double;
    function getSpeed: double;
    function getTurnAngle: double;
    function getX: double;
    function getY: double;

  end;

  TBoid = class
  private
   { FIELDS }

    heading,
    speed,
    x, y,
    maximumSpeed,
    minimumSpeed,
    turnAngle,
    acceleration,
    sensorRange,
    collisionRange,
    deathRange,
    deathRangeNumber,
    deathConstant,
    attackConstant,
    retreatConstant : double;
    friendsHeading, friendsSpeed, friendsX, friendsY, friendsNumber : double;
    enemiesHeading, enemiesSpeed, enemiesX, enemiesY, enemiesNumber : double;
    color : TColor;
    flock : TFlock;
    nearestFriend : TBoid; // Set only if within the collision range.
    nearestEnemy : TBoid;
  public

    teamIndex : integer;
    { CONSTRUCTORS }
    constructor Create (direction, magnitude, xPos, yPos, maxSpeed, minSpeed,
                            angle, accel, sensor, death, deathC, attackC, retreatC,
                            collision : double;
                            Color : TColor ;
                            gaggle : TFlock); overload;
                            
    constructor Create (b : TBoidGene); overload;

    { DESTRUCTOR }
    destructor Destroy; override;

    { PROCEDURES }
    procedure move;
    procedure slowDown;
    procedure speedUp;
    procedure turnLeft;
    procedure turnRight;
    procedure senseFlock(f : TFlock);
    procedure decide;
    procedure avoidCollision;
    procedure attackEnemies;
    procedure hangOutWithFriends;
    procedure retreat;
    procedure turnAway(xPos, yPos: double);
    procedure turnToward(xPos, yPos: double);

    procedure setHeading( direction : double);
    procedure setSpeed( magnitude : double);
    procedure setX( xPos : double);
    procedure setY( yPos : double);
    procedure setMaximumSpeed( s : double);
    procedure setMinimumSpeed( s : double);
    procedure setColor( c : TColor);
    procedure setTurnAngle( a : double);
    procedure setAcceleration( a : double);
    procedure setSensorRange( sensor : double);
    procedure setDeathRange( death : double);
    procedure setDeathConstant( c : double);
    procedure setAttackConstant( c : double);
    procedure setRetreatConstant( c : double);
    procedure setDeathRangeNumber( number : double);
    procedure setCollisionRange( collision : double);
    procedure setFlock( gaggle : TFlock);
    procedure setFriendsHeading( direction : double);
    procedure setFriendsSpeed( speed : double);
    procedure setFriendsX( xPos : double);
    procedure setFriendsY( yPos : double);
    procedure setFriendsNumber( number : double);
    procedure setEnemiesHeading( direction : double);
    procedure setEnemiesSpeed( speed : double);
    procedure setEnemiesX( xPos : double);
    procedure setEnemiesY(yPos : double );
    procedure setEnemiesNumber(number : double );
    procedure setNearestFriend(b : TBoid );
    procedure setNearestEnemy(b : TBoid );

    { FUNCTIONS }
    function isInRange(b : TBoid; range : double) : boolean;
    function isDead: boolean;
    function isFriend(b: TBoid): boolean;

    function relativeAngle(xPos, yPos : double) : double;

    function getAcceleration: double;
    function getAttackConstant: double;
    function getCollisionRange: double;
    function getColor: TColor;
    function getDeathConstant: double;
    function getDeathRange: double;
    function getDeathRangeNumber: double;
    function getEnemiesHeading: double;
    function getEnemiesNumber: double;
    function getEnemiesSpeed: double;
    function getEnemiesX: double;
    function getEnemiesY: double;
    function getFlock: TFlock;
    function getFriendsHeading: double;
    function getFriendsNumber: double;
    function getFriendsSpeed: double;
    function getFriendsX: double;
    function getFriendsY: double;
    function getHeading: double;
    function getMaximumSpeed: double;
    function getMinimumSpeed: double;
    function getNearestEnemy: TBoid;
    function getNearestFriend: TBoid;
    function getRetreatConstant: double;
    function getSensorRange: double;
    function getSpeed: double;
    function getTurnAngle: double;
    function getX: double;
    function getY: double;

    function reportStatistics : string; 
end;

implementation

{ METHODS }
{ TBoid }

{------------------------------------------------------------------}
{  Create a Boid                                                   }
{------------------------------------------------------------------}

constructor TBoid.Create(direction, magnitude, xPos, yPos, maxSpeed,
  minSpeed, angle, accel, sensor, death, deathC, attackC, retreatC,
  collision: double; Color: TColor; gaggle: TFlock);
begin
    setMaximumSpeed(maxSpeed);
    setMinimumSpeed(minSpeed);
    setHeading(direction);
    setSpeed(magnitude);
    setX(xPos);
    setY(yPos);
    setColor(Color);
    setTurnAngle(angle);
    setAcceleration(accel);
    setSensorRange(sensor);
    setDeathRange(death);
    setDeathConstant(deathC);
    setAttackConstant(attackC);
    setRetreatConstant(retreatC);
    setCollisionRange(collision);
    setFlock(gaggle);
    setNearestFriend(nil);
    setNearestEnemy(nil);

    teamIndex := 0;
end;

{------------------------------------------------------------------}
{  Create a Boid as a sibling of another Boid                      }
{------------------------------------------------------------------}
constructor TBoid.Create( b : TBoidGene );
begin

  setMaximumSpeed(b.getMaximumSpeed());
  setMinimumSpeed(b.getMinimumSpeed());
  setHeading(b.getHeading());
  setSpeed(b.getSpeed());
  setX(b.getX());
  setY(b.getY());
  setColor(b.getColor());
  setTurnAngle(b.getTurnAngle());
  setAcceleration(b.getAcceleration());
  setSensorRange(b.getSensorRange());
  setDeathRange(b.getDeathRange());
  setDeathConstant(b.getDeathConstant());
  setAttackConstant(b.getAttackConstant());
  setRetreatConstant(b.getRetreatConstant());
  setCollisionRange(b.getCollisionRange());
  setFlock(b.getFlock());
  setNearestFriend(nil);
  setNearestEnemy(nil);
  teamIndex := b.teamIndex;
end;

{------------------------------------------------------------------}
{  Destroy the Boid                                                }
{------------------------------------------------------------------}
destructor TBoid.Destroy;
begin
  // This is NOT the best place to release the colour, but at this point
  // the other location instances of the color no longer exist, and this
  // is the only remaining reference to the object, so if it is still here
  // we need to clean it up.
  if Assigned(Color) then
    Color.Free;

  inherited;
end;

{------------------------------------------------------------------}
{  Move the Boid                                                   }
{------------------------------------------------------------------}
procedure TBoid.move;
begin
  setX(getX() + getSpeed()*Cos(getHeading()));
  setY(getY() + getSpeed()*Sin(getHeading()));
end;

{------------------------------------------------------------------}
{ Slow Down (Never less than min speed)                            }
{------------------------------------------------------------------}
procedure TBoid.slowDown;
begin
    setSpeed(getSpeed() - getAcceleration());
end;

{------------------------------------------------------------------}
{  Speed Up (Never more than Max Speed)                            }
{------------------------------------------------------------------}
procedure TBoid.speedUp;
begin
    setSpeed(getSpeed() + getAcceleration());
end;

{------------------------------------------------------------------}
{  Turn Left                                                       }
{------------------------------------------------------------------}
procedure TBoid.turnLeft;
begin
    setHeading(getHeading() + getTurnAngle());
end;

{------------------------------------------------------------------}
{  Turn Right                                                      }
{------------------------------------------------------------------}
procedure TBoid.turnRight;
begin
    setHeading(getHeading() - getTurnAngle());
end;

{------------------------------------------------------------------}
{  Is this Boid in range of a target Boid?                         }
{------------------------------------------------------------------}
function TBoid.isInRange(b : TBoid; range : double) : boolean;
begin
    Result := (sqrt((b.getX()-getX())*(b.getX()-getX()) + (b.getY()-getY())*(b.getY()-getY())) < range);
end;

{------------------------------------------------------------------}
{  Check the whole flock for friends and enemies close by          }
{------------------------------------------------------------------}
procedure TBoid.senseFlock(f : TFlock);
var b : TBoid;
    friends,
    totalFriendsX,
    totalFriendsY,
    totalFriendsSpeed,
    totalFriendsHeading,
    enemies,
    totalEnemiesX,
    totalEnemiesY,
    totalEnemiesSpeed,
    totalEnemiesHeading,
    distanceToFriend,
    distanceToEnemy : double;
    i : integer;
    e : TList;
begin
    e := f.elements();
    friends := 0.0;
    totalFriendsX := 0.0;
    totalFriendsY := 0.0;
    totalFriendsSpeed := 0.0;
    totalFriendsHeading := 0.0;
    enemies := 0.0;
    totalEnemiesX := 0.0;
    totalEnemiesY := 0.0;
    totalEnemiesSpeed := 0.0;
    totalEnemiesHeading := 0.0;
    setNearestFriend(nil);
    setNearestEnemy(nil);
    setDeathRangeNumber(0.0);
    distanceToFriend := getCollisionRange();
    distanceToEnemy := getSensorRange();

    for i := 0 to e.Count - 1 do
    begin
        b := TBoid(e.Items[i]);
        if (b <> self) then
        begin
            if (isFriend(b)) then
            begin
                if (isInRange(b, distanceToFriend)) then
                begin
                    setNearestFriend(b);
                    distanceToFriend :=
                        sqrt((b.getX()-getX())*(b.getX()-getX()) +
                                   (b.getY()-getY())*(b.getY()-getY()));
                end;
                {if(distanceToFriend < getCollisionRange) then // Not correct
                begin
                   turnAway(b.getX(),b.getY());
                end;}
                if (isInRange(b, getSensorRange())) then
                begin
                    friends := friends + 1.0;
                    totalFriendsX := totalFriendsX + b.getX();
                    totalFriendsY := totalFriendsY + b.getY();
                    totalFriendsSpeed := totalFriendsSpeed + b.getSpeed();
                    totalFriendsHeading := totalFriendsHeading + b.getHeading();
                end;
            end
            else
            begin // Is enemy
                if (isInRange(b, distanceToEnemy)) then
                begin
                    setNearestEnemy(b);
                    distanceToEnemy :=
                        sqrt((b.getX()-getX())*(b.getX()-getX()) +
                                   (b.getY()-getY())*(b.getY()-getY()));
                end;
                {if(b.getCollisionRange() < distanceToEnemy) then // Kinda defeats the point of hunting, but I cant think of another way to stop collisions
                begin
                  slowDown();
                end;}
                if (isInRange(b, getDeathRange())) then
                begin
                    setDeathRangeNumber(getDeathRangeNumber() + 1.0);
                end;
                if (isInRange(b, getSensorRange())) then
                begin
                    enemies := enemies + 1.0;
                    totalEnemiesX := totalEnemiesX + b.getX();
                    totalEnemiesY := totalEnemiesY + b.getY();
                    totalEnemiesSpeed := totalEnemiesSpeed + b.getSpeed();
                    totalEnemiesHeading := totalEnemiesHeading + b.getHeading();
                end;
            end;
        end;
        if (friends = 0.0) then
        begin
            setFriendsNumber(0.0);
            setFriendsHeading(getHeading());
            setFriendsSpeed(getSpeed());
            setFriendsX(getX());
            setFriendsY(getY());
        end
        else
        begin
            setFriendsNumber(friends);
            setFriendsHeading(totalFriendsHeading/friends);
            setFriendsSpeed(totalFriendsSpeed/friends);
            setFriendsX(totalFriendsX/friends);
            setFriendsY(totalFriendsY/friends);
        end;

        if (enemies = 0.0) then
        begin
            setEnemiesNumber(0.0);
            setEnemiesHeading(getHeading());
            setEnemiesSpeed(getSpeed());
            setEnemiesX(getX());
            setEnemiesY(getY());
        end
        else
        begin
            setEnemiesNumber(enemies);
            setEnemiesHeading(totalEnemiesHeading/enemies);
            setEnemiesSpeed(totalEnemiesSpeed/enemies);
            setEnemiesX(totalEnemiesX/enemies);
            setEnemiesY(totalEnemiesY/enemies);
        end;
    end;
end;

{-------------------------------------------------------------------}
{This is where the boid decides what to do based on what it senses. }
{-------------------------------------------------------------------}

procedure TBoid.decide;
begin
    if (getEnemiesNumber() <> 0.0) then
    begin
      if ((getFriendsNumber()+1)/getEnemiesNumber() > getAttackConstant()) then
      begin
          avoidCollision();
          attackEnemies();

      end
      else if ((getFriendsNumber()+1)/getEnemiesNumber() < getRetreatConstant()) then
      begin
          retreat();
      end;
    end
    else if (getNearestFriend() <> nil) then
    begin
        avoidCollision();
    end
    else
    begin
        hangOutWithFriends();
    end;
end;

{-------------------------------------------------------------------}
{  Try not to collide with others                                   }
{-------------------------------------------------------------------}
procedure TBoid.avoidCollision;
var n : TBoid;
  angle : double;
begin
  n := getNearestEnemy();
  if (n <> nil) then
  begin
      turnAway(n.getX(),n.getY());
      angle := relativeAngle(n.getX(),n.getY());
      if (Cos(n.getHeading() - self.getHeading()) > 0) then  // going the same direction
      begin
          if (cos(angle - getHeading()) > 0) then            // this boid is behind the other
          begin
              // Added to avoid collision Not working Properly Yet!!!!
              if(sqrt((n.getX()-getX())*(n.getX()-getX()) + (n.getY()-getY())*(n.getY()-getY())) > n.collisionRange) then
              begin
                n.speedUp;
                slowDown;
                turnAway(n.getX(),n.getY());
              end;
              slowDown();
          end
          else
          begin                                                   // this boid is in front of the other
              speedUp();
          end;
      end
      else
      begin                                                   // going opposite directions
          if (Cos(angle - getHeading()) > 0) then            // head-on collision course
          begin
              slowDown();
          end;
      end;
  end;

  n := getNearestFriend();
  if (n <> nil) then
  begin
      turnAway(n.getX(),n.getY());
      angle := relativeAngle(n.getX(),n.getY());
      if (Cos(n.getHeading() - self.getHeading()) > 0) then  // going the same direction
      begin
          if (cos(angle - getHeading()) > 0) then            // this boid is behind the other
          begin
              // Added to avoid collision Not working Properly yet!!!!
              if(sqrt((n.getX()-getX())*(n.getX()-getX()) + (n.getY()-getY())*(n.getY()-getY())) > n.collisionRange) then
              begin
                //n.speedUp;
                slowDown;
                //turnAway(n.getX(),n.getY());
              end;
              slowDown();
          end
          else
          begin                                                   // this boid is in front of the other
              speedUp();
          end;
      end
      else
      begin                                                   // going opposite directions
          if (cos(angle - getHeading()) > 0) then            // head-on collision course
          begin
              slowDown();
          end;
      end;
  end;

end;

{-------------------------------------------------------------------}
{  Stay with Friendly Boids                                         }
{-------------------------------------------------------------------}
procedure TBoid.hangOutWithFriends;
begin
    if (getFriendsNumber() > 0.0) then
    begin
        turnToward(getFriendsX(),getFriendsY());
        if (getFriendsSpeed() > getSpeed()) then
        begin
            speedUp();
        end
        else if (getFriendsSpeed() < getSpeed()) then
        begin
            slowDown();
        end;
    end
    else
    begin
        slowDown();
        if (random < 0.5) then
        begin
            turnLeft();
        end
        else
        begin
            turnRight();
        end;
    end;
end;

{-------------------------------------------------------------------}
{  Atack enemy boids to drive them away from the flock/school       }
{-------------------------------------------------------------------}
procedure TBoid.attackEnemies;
begin
    turnToward(getEnemiesX(),getEnemiesY());
    if (getEnemiesSpeed() > getSpeed()) then
    begin
        speedUp();
    end
    else if (getEnemiesSpeed() < getSpeed()) then
    begin
        slowDown();
    end;
end;

{-------------------------------------------------------------------}
{  Retreat from a fight                                             }
{-------------------------------------------------------------------}
procedure TBoid.retreat;
begin
    turnAway(getEnemiesX(),getEnemiesY());
    speedUp();
end;

{-------------------------------------------------------------------}
{  Get the relative angle from a Line                               }
{-------------------------------------------------------------------}
function TBoid.relativeAngle(xPos, yPos : double) : double;
var dx, dy, r, angle : double;
begin
    dx := xPos - getX();
    dy := yPos - getY();
    r := sqrt(dx*dx + dy*dy);
    angle := arccos(dx/r);
    if (dy < 0) then
    begin
        angle := 2.0*PI - angle;
    end;
    Result := angle;
end;

{-------------------------------------------------------------------}
{  Tunt away from a Specified Position                              }
{-------------------------------------------------------------------}
procedure TBoid.turnAway(xPos, yPos : double);
var angle : double;
begin
    if ((xPos = getX()) and (yPos = getY())) then
    begin
        exit;
    end;
    angle := relativeAngle(xPos,yPos);
    if (abs(angle-getHeading()) < PI)  then
    begin
        if (angle - getHeading() > 0) then
        begin
            turnRight();
        end
        else
        begin
            turnLeft();
        end;
    end
    else
    begin
        if (angle - getHeading() > 0) then
        begin
            turnLeft();
        end
        else
        begin
            turnRight();
        end;
    end;
end;

{-------------------------------------------------------------------}
{  Turn Towards a Specified Position                                }
{-------------------------------------------------------------------}
procedure TBoid.turnToward(xPos, yPos : double);
var angle : double;
begin
    //Do seperation calculation here.
    if ((xPos = getX()) and (yPos = getY())) then
    begin
        turnAway(xPos,yPos);
        exit;
    end;
    angle := relativeAngle(xPos,yPos);
    if (abs(angle - getHeading()) < PI) then
    begin
        if (angle - getHeading() > 0) then
        begin
            turnLeft();
        end
        else
        begin
            turnRight();
        end;
    end
    else
    begin
        if (angle - getHeading() > 0) then
        begin
            turnRight();
        end
        else
        begin
            turnLeft();
        end;
    end;
end;

{-------------------------------------------------------------------}
{  Examine boid colour to see if it is a friend                     }
{-------------------------------------------------------------------}
function TBoid.isFriend(b : TBoid ) : boolean;
var PotentialFriendColor : TColor;
    SelfColor : TColor;
begin
    PotentialFriendColor := b.getColor;
    SelfColor := getColor;
    if (PotentialFriendColor.r = SelfColor.r) and (PotentialFriendColor.g = SelfColor.g) and (PotentialFriendColor.b = SelfColor.b) then
      Result := true
    else
      Result := false;
end;

{-------------------------------------------------------------------}
{  Is the current boid dead (Not yet fully Functional)              }
{-------------------------------------------------------------------}
function TBoid.isDead : boolean;
begin
    Result := (random < (1.0 - exp(-getDeathConstant()*getDeathRangeNumber())));
    {*
     * If death constant is 0.231049, then there is a 50% chance
     * of dying when confronted with three enemies.
     *}
end;

{  GET AND SET METHODS }

function TBoid.getHeading : double;
begin
  Result := heading;
end;
procedure TBoid.setHeading( direction : double);
begin
    heading := direction;
  if (heading > 2.0*PI) then
  begin
    heading := heading - 2.0*PI;
  end
  else
  begin
    if (heading < 0) then
    begin
        heading := 2.0*PI + heading;
    end;
  end;
end;

function TBoid.getSpeed : double;
begin
  Result := speed;
end;
procedure TBoid.setSpeed( magnitude : double);
begin
    speed := abs(magnitude);
    if (speed > getMaximumSpeed()) then
    begin
        speed := getMaximumSpeed();
    end;
    if (speed < getMinimumSpeed()) then
    begin
        speed := getMinimumSpeed();
    end;
end;

function TBoid.getX : double;
begin
  Result := x;
end;
procedure TBoid.setX( xPos : double);
begin
  x := xPos;
end;

function TBoid.getY : double;
begin
  Result := y;
end;
procedure TBoid.setY( yPos : double);
begin
  y := yPos;
end;

function TBoid.getMaximumSpeed : double;
begin
  Result := maximumSpeed;
end;
procedure TBoid.setMaximumSpeed( s : double);
begin
  maximumSpeed := abs(s);
end;

function TBoid.getMinimumSpeed : double;
begin
  Result := minimumSpeed;
end;
procedure TBoid.setMinimumSpeed( s : double);
begin
  minimumSpeed := abs(s);
end;

function TBoid.getColor : TColor;
begin
  Result := color;
end;
procedure TBoid.setColor( c : TColor);
begin
  color := c;
end;

function TBoid.getTurnAngle : double;
begin
  Result := turnAngle;
end;
procedure TBoid.setTurnAngle( a : double);
begin
    while (a>2.0*PI) do
    begin
        a := a - 2.0*PI;
    end;
    while (a<0) do
    begin
        a := 2.0*PI + a;
    end;
    turnAngle := a;
end;

function TBoid.getAcceleration : double;
begin
  Result := acceleration;
end;
procedure TBoid.setAcceleration( a : double);
begin
  acceleration := abs(a);
end;

function TBoid.getSensorRange : double;
begin
  Result := sensorRange;
end;
procedure TBoid.setSensorRange( sensor : double);
begin
  sensorRange := abs(sensor);
end;

function TBoid.getDeathRange : double;
begin
  Result := deathRange;
end;
procedure TBoid.setDeathRange( death : double);
begin
  deathRange := abs(death);
end;

function TBoid.getDeathConstant : double;
begin
  Result := deathConstant;
end;
procedure TBoid.setDeathConstant( c : double);
begin
  deathConstant := abs(c);
end;

function TBoid.getAttackConstant : double;
begin
  Result := attackConstant;
end;
procedure TBoid.setAttackConstant( c : double);
begin
  attackConstant := abs(c);
end;

function TBoid.getRetreatConstant : double;
begin
  Result := retreatConstant;
end;
procedure TBoid.setRetreatConstant( c : double);
begin
  retreatConstant := abs(c);
end;

function TBoid.getDeathRangeNumber : double;
begin
  Result := deathRangeNumber;
end;
procedure TBoid.setDeathRangeNumber( number : double);
begin
  deathRangeNumber := number;
end;

function TBoid.getCollisionRange : double;
begin
  Result := collisionRange;
end;
procedure TBoid.setCollisionRange( collision : double);
begin
  collisionRange := abs(collision);
end;

function TBoid.getFlock : TFlock;
begin
  Result := flock;
end;
procedure TBoid.setFlock( gaggle : TFlock);
begin
  flock := gaggle;
end;

function TBoid.getFriendsHeading : double;
begin
  Result := friendsHeading;
end;
procedure TBoid.setFriendsHeading( direction : double);
begin
  friendsHeading := direction;
end;

function TBoid.getFriendsSpeed : double;
begin
  Result := friendsSpeed;
end;
procedure TBoid.setFriendsSpeed( speed : double);
begin
  friendsSpeed := speed;
end;

function TBoid.getFriendsX : double;
begin
  Result := friendsX;
end;
procedure TBoid.setFriendsX( xPos : double);
begin
  friendsX := xPos;
end;

function TBoid.getFriendsY : double;
begin
  Result := friendsY;
end;
procedure TBoid.setFriendsY( yPos : double);
begin
  friendsY := yPos;
end;

function TBoid.getFriendsNumber : double;
begin
  Result := friendsNumber;
end;
procedure TBoid.setFriendsNumber( number : double);
begin
  friendsNumber := number;
end;

function TBoid.getEnemiesHeading : double;
begin
  Result := enemiesHeading;
end;
procedure TBoid.setEnemiesHeading( direction : double);
begin
  enemiesHeading := direction;
end;

function TBoid.getEnemiesSpeed : double;
begin
  Result := enemiesSpeed;
end;
procedure TBoid.setEnemiesSpeed( speed : double);
begin
  enemiesSpeed := speed;
end;

function TBoid.getEnemiesX : double;
begin
  Result := enemiesX;
end;
procedure TBoid.setEnemiesX( xPos : double);
begin
  enemiesX := xPos;
end;

function TBoid.getEnemiesY : double;
begin
  Result := enemiesY;
end;
procedure TBoid.setEnemiesY(yPos : double );
begin
  enemiesY := yPos;
end;

function TBoid.getEnemiesNumber : double;
begin
  Result := enemiesNumber;
end;
procedure TBoid.setEnemiesNumber(number : double );
begin
  enemiesNumber := number;
end;

function TBoid.getNearestFriend : TBoid;
begin
  Result := nearestFriend;
end;
procedure TBoid.setNearestFriend(b : TBoid );
begin
  nearestFriend := b;
end;

function TBoid.getNearestEnemy : TBoid;
begin
  Result := nearestEnemy;
end;
procedure TBoid.setNearestEnemy(b : TBoid );
begin
  nearestEnemy := b;
end;

{-------------------------------------------------------------------}
{  Function to report all the curent attributes of this boid        }
{-------------------------------------------------------------------}
function TBoid.reportStatistics: string;
var stats : string;
begin
  stats := stats + 'Position : x - ' + FloatToStr(Round(x)) + ' y - '  + FloatToStr(Round(y));
  stats := stats + ' Heading: ' + FloatToStr(getHeading);
  Result := stats;
end;

{ TBoidGene }
{-------------------------------------------------------------------}
{  Basic data class (Gene Memory) of a boid,                        }
{  Will pass this on to any Children of the parent boid             }
{-------------------------------------------------------------------}
constructor TBoidGene.Create(direction, magnitude, xPos, yPos, maxSpeed,
  minSpeed, angle, accel, sensor, death, deathC, collision, attackC,
  retreatC: double; c: TColor; gaggle: TFlock; iteamIndex : integer);
begin
  setMaximumSpeed(maxSpeed);
      setMinimumSpeed(minSpeed);
      setHeading(direction);
      setSpeed(magnitude);
      setX(xPos);
      setY(yPos);
      setColor(c);
      setTurnAngle(angle);
      setAcceleration(accel);
      setSensorRange(sensor);
      setDeathRange(death);
      setDeathConstant(deathC);
      setAttackConstant(attackC);
      setRetreatConstant(attackC);
      setCollisionRange(collision);
      setFlock(gaggle);

      setNumberOfTrials(0.0);
      setTrialsWon(0.0);

      self.teamIndex := iteamIndex
end;

function TBoidGene.getAcceleration: double;
begin
  Result := acceleration;
end;

function TBoidGene.getAttackConstant: double;
begin
  Result := attackConstant;
end;

function TBoidGene.getCollisionRange: double;
begin
  Result := collisionRange;
end;

function TBoidGene.getColor: TColor;
begin
  Result := color;
end;

function TBoidGene.getDeathConstant: double;
begin
  Result := deathConstant;
end;

function TBoidGene.getDeathRange: double;
begin
  Result := deathRange;
end;

function TBoidGene.getFitness: double;
begin
  Result := getTrialsWon()/getNumberOfTrials();
end;

function TBoidGene.getFlock: TFlock;
begin
  Result := flock;
end;


function TBoidGene.getHeading: double;
begin
  Result := heading;
end;

function TBoidGene.getMaximumSpeed: double;
begin
  Result := maximumSpeed;
end;

function TBoidGene.getMinimumSpeed: double;
begin
  Result := minimumSpeed;
end;

function TBoidGene.getNumberOfTrials: double;
begin
  Result := numberOfTrials;
end;

function TBoidGene.getRetreatConstant: double;
begin
  Result := retreatConstant;
end;

function TBoidGene.getSensorRange: double;
begin
  Result := sensorRange;
end;

function TBoidGene.getSpeed: double;
begin
  Result := speed;
end;

function TBoidGene.getTrialsWon: double;
begin
  Result := trialsWon;
end;

function TBoidGene.getTurnAngle: double;
begin
  Result := turnAngle;
end;

function TBoidGene.getX: double;
begin
  Result := x;
end;

function TBoidGene.getY: double;
begin
  Result := y;
end;

{-------------------------------------------------------------------}
{  Reproduction takes place                                         }
{-------------------------------------------------------------------}
function TBoidGene.reproduceWith(o : TBoidGene): TBoidGene;
var direction, magnitude, xPos, yPos, maxSpeed, minSpeed, turnRate,
    accel, sRange, cRange, attackC, retreatC : double;
begin

     if (random > 0.5) then
     begin
         direction := o.getHeading();
     end
     else
     begin
         direction := getHeading();
     end;
     if (random > 0.5) then
     begin
         magnitude := o.getSpeed();
     end
     else
     begin
         magnitude := getSpeed();
     end;
     if (random > 0.5) then
     begin
         xPos := o.getX();
     end
     else
     begin
         xPos := getX();
     end;
     if (random > 0.5) then
     begin
         yPos := o.getY();
     end
     else
     begin
         yPos := getY();
     end;
     if (random > 0.5) then
     begin
         maxSpeed := o.getMaximumSpeed();
     end
     else
     begin
         maxSpeed := getMaximumSpeed();
     end;
     if (random > 0.5) then
     begin
         minSpeed := o.getMinimumSpeed();
     end
     else
     begin
         minSpeed := getMinimumSpeed();
     end;
     if (minSpeed > maxSpeed) then
     begin
         maxSpeed := minSpeed;
     end;
     if (random > 0.5) then
     begin
         turnRate := o.getTurnAngle();
     end
     else
     begin
         turnRate := getTurnAngle();
     end;
     if (random > 0.5) then
     begin
         accel := o.getAcceleration();
     end
     else
     begin
         accel := getAcceleration();
     end;
     if (random > 0.5) then
     begin
         sRange := o.getSensorRange();
     end
     else
     begin
         sRange := getSensorRange();
     end;
     if (random > 0.5) then
     begin
         cRange := o.getCollisionRange();
     end
     else
     begin
         cRange := getCollisionRange();
     end;
     if (random > 0.5) then
     begin
         attackC := o.getAttackConstant();
     end
     else
     begin
         attackC := getAttackConstant();
     end;
     if (random > 0.5) then
     begin
         retreatC := o.getRetreatConstant();
     end
     else
     begin
         retreatC := getRetreatConstant();
     end;
     Result := TBoidGene.Create(
                     direction,
                     magnitude,
                     xPos,
                     yPos,
                     maxSpeed,
                     minSpeed,
                     turnRate,
                     accel,
                     sRange,
                     getDeathRange(),
                     getDeathConstant(),
                     cRange,
                     attackC,
                     retreatC,
                     TColor.Create(0.0,0.0,1.0),
                     getFlock(),
                     teamIndex);

end;

procedure TBoidGene.setAcceleration(a: double);
begin
  acceleration := a;
end;

procedure TBoidGene.setAttackConstant(c: double);
begin
  attackConstant := c;
end;

procedure TBoidGene.setCollisionRange(collision: double);
begin
  collisionRange := collision;
end;

procedure TBoidGene.setColor(c: TColor);
begin
  color := c;
end;

procedure TBoidGene.setDeathConstant(c: double);
begin
  deathConstant := c;
end;

procedure TBoidGene.setDeathRange(death: double);
begin
  deathRange := death;
end;

procedure TBoidGene.setFlock(gaggle: TFlock);
begin
  flock := gaggle;
end;


procedure TBoidGene.setHeading(direction: double);
begin
  heading := direction;
end;

procedure TBoidGene.setMaximumSpeed(s: double);
begin
  maximumSpeed := s;
end;

procedure TBoidGene.setMinimumSpeed(s: double);
begin
  minimumSpeed := s;
end;


procedure TBoidGene.setNumberOfTrials(n: double);
begin
  numberOfTrials := n;
end;

procedure TBoidGene.setRetreatConstant(c: double);
begin
  retreatConstant := c;
end;

procedure TBoidGene.setSensorRange(sensor: double);
begin
  sensorRange := sensor;
end;

procedure TBoidGene.setSpeed(magnitude: double);
begin
  speed := magnitude;
end;

procedure TBoidGene.setTrialsWon(t: double);
begin
  trialsWon := t;
end;

procedure TBoidGene.setTurnAngle(a: double);
begin
  turnAngle := a;
end;

procedure TBoidGene.setX(xPos: double);
begin
  x := xPos;
end;

procedure TBoidGene.setY(yPos: double);
begin
  y := yPos;
end;

{ TFlock }
{-------------------------------------------------------------------}
{  Holding Flock for all boids in our aquarium                      }
{-------------------------------------------------------------------}
procedure TFlock.add(b: TBoid);
begin
  members.Add(b);
end;

procedure TFlock.addToMomentum(b: TBoid);
var x, y, r, angle : double;
begin
  x := getMeanSpeed()*cos(getMeanHeading()) + b.getSpeed()*cos(b.getHeading());
  y := getMeanSpeed()*sin(getMeanHeading()) + b.getSpeed()*sin(b.getHeading());
  r := sqrt(x*x + y*y);
  angle := arccos(x/r);
  if (y < 0) then
  begin
      angle := 2.0*PI - angle;
  end;
  setMeanSpeed(r);
  setMeanHeading(angle);

end;

function TFlock.calculateMeanX: double;
var e : TList;
    sum : double;
    i : integer;
begin
  e := elements();
  sum := 0;
  for i := 0 to e.Count - 1 do
  begin
    sum := sum + (TBoid(e.Items[i])).getX();
  end;
  Result := sum/getNumberOfMembers;

end;

function TFlock.calculateMeanY: double;
var e : TList;
    sum : double;
    i : integer;
begin
  e := elements();
  sum := 0;
  for i := 0 to e.Count - 1 do
  begin
    sum := sum + (TBoid(e.Items[i])).getY();
  end;
  Result := sum/getNumberOfMembers;
end;

{-------------------------------------------------------------------}
{  Wrap around when Boid goes out of bounds                         }
{-------------------------------------------------------------------}
procedure TFlock.checkBounds(boid: TBoid);
begin
  if (boid.getX() > width) then
  begin
      boid.setX(boid.getX() - width);
  end;
  if (boid.getX() < 0) then
  begin
      boid.setX(boid.getX() + width);
  end;
  if (boid.getY() > height) then
  begin
      boid.setY(boid.getY() - height);
  end;
  if (boid.getY() < 0) then
  begin
      boid.setY(boid.getY() + height);
  end;
end;

{-------------------------------------------------------------------}
{  Create the flock                                                 }
{-------------------------------------------------------------------}
constructor TFlock.Create(size, teams: integer; h, w: double; skin : string; icanMultitexture, iisMultiTexture : boolean);
var i : integer;
begin
  randomize;

  members := TObjectList.Create;
  members.OwnsObjects := True;

  setNumberOfMembers(size);
  setNumberOfTeams(teams);
  setHeight(h);
  setWidth(w);

  SetLength(Skins,teams);
  for i := 0 to teams-1 do
    Skins[i] := TAnimate.create(skin + IntToStr(i) + '.txt',24,icanMultitexture, iisMultiTexture);
end;

function TFlock.elements: TList;
begin
  Result := members;
end;


function TFlock.getHeight: double;
begin
  Result := height;
end;

function TFlock.getMeanHeading: double;
begin
  Result := meanHeading;
end;

function TFlock.getMeanSpeed: double;
begin
  Result := meanSpeed;
end;

function TFlock.getMeanX: double;
begin
  Result := meanX;
end;

function TFlock.getMeanY: double;
begin
  Result := meanY;
end;

function TFlock.getNumberOfMembers: integer;
begin
   Result := members.Count -1;
end;

function TFlock.getNumberOfTeams: integer;
begin
  Result := numberOfTeams;
end;

function TFlock.getWidth: double;
begin
  Result := width;
end;

procedure TFlock.initializeDuel(gene, challenger: TBoidGene);
var i : integer;
begin
  members := TObjectList.Create;
  for i := 0 to getNumberOfMembers -1 do
  begin
      add(TBoid.Create(gene));
  end;
  for i := 0 to getNumberOfMembers -1 do
  begin
      add(TBoid.Create(challenger));
  end;
  setMeanX(calculateMeanX());
  setMeanY(calculateMeanY());
end;

{-------------------------------------------------------------------}
{  Calculate next movement for all boids                            }
{-------------------------------------------------------------------}
procedure TFlock.moveMembers;
var e : TList;
    boid : TBoid;
    colorSeen : TColor;
    i : integer;
begin
  e := elements();
  colorSeen := nil;
  setMeanHeading(0.0);
  setMeanSpeed(0.0);
  setMeanX(0.0);
  setMeanY(0.0);
  for i := 0 to e.Count -1 do
  begin
      boid := TBoid(e.Items[i]);
      if (getNumberOfTeams() > 1) then
      begin
          if (colorSeen = nil) then
          begin
              colorSeen := boid.getColor;
          end;
      end;
      boid.senseFlock(self);
      boid.decide();
      boid.move();
      checkBounds(boid);
      addToMomentum(boid);
      setMeanX(getMeanX() + boid.getX());
      setMeanY(getMeanY() + boid.getY());
  end;
  setMeanX(getMeanX()/getNumberOfMembers());
  setMeanY(getMeanY()/getNumberOfMembers());
end;

function TFlock.FindAngle( FSourceX, FSourceY, FDestX, FDestY  : single) : single;
var
  XDiff, YDiff: Single;
  fpAngle: Single;
  FSourcePoint , FDestPoint : T2DCoord;
begin
  FSourcePoint := T2DCoord.Create(FSourceX, FSourceY);
  FDestPoint   := T2DCoord.Create(FDestX, FDestY);
  fpAngle := 0;
  { If we have two points here, then we can work out the angle }
  if (FSourcePoint.X <> -1) AND (FSourcePoint.Y <> -1) AND (FDestPoint.X <> -1) AND (FDestPoint.Y <> -1) then
  begin
      { Basic Trig:
                    B
                   /|         Angle ?  =  ArcTan( Y / X )
                 /  |
               /    |         Given any two points we can use the rule of
             /      |Y        opposite-over-adjacent to try to determine the
           /`       |         unknown angle.
         /   ?`    _|
       /_______`__|_|         However, all of this maths is based on
      A      X                right-angled triangles, which obviously can only
                              account for a 90 degree segment at any point in
                              time, so we need to try to calculate which
                              quadrant of the full circle the angle represents.

      _______180________
     |        |        |      So basing the quadrants around the co-ordinate
     |  -x    |  +x    |      system I have chosen in this particular case we
     |  -y    |  -y    |      find ourselves with the following grid.
  270|________|________|90
     |        |        |      As such we need to react to the locations the user
     |  -x    |  +x    |      has clicked in to be the basis of our maths
     |  +y    |  +y    |      calculations.  Working against this information,
     |________|________|      we can then calculate a correct angle.
              0                                                    }


      { All angles derived are is based from the X axis, and as such we need to
        do the maths to determine the actual angle }

      if (FDestPoint.X > FSourcePoint.X) AND (FDestPoint.Y > FSourcePoint.Y) then
      begin
        { 0 - 90 degree quadrant }
        XDiff := FDestPoint.X - FSourcePoint.X;
        YDiff := FDestPoint.Y - FSourcePoint.Y;

        { In this case the actual angle is:

          90 degrees minus the angle we calculate }
        fpAngle := 90 - RadToDeg(ArcTan(YDiff / XDiff));
      end else if (FDestPoint.X > FSourcePoint.X) AND (FDestPoint.Y < FSourcePoint.Y) then
      begin
        { 90 - 180 degree quadrant }
        XDiff := FDestPoint.X - FSourcePoint.X;
        YDiff := FSourcePoint.Y - FDestPoint.Y;

        { In this case the actual angle is:

          90 degrees plus the angle we calculate }
        fpAngle := 90 + RadToDeg(ArcTan(YDiff / XDiff));
      end else if (FDestPoint.X < FSourcePoint.X) AND (FDestPoint.Y < FSourcePoint.Y) then
      begin
        { 180 - 270 degree quadrant }
        XDiff := FSourcePoint.X - FDestPoint.X;
        YDiff := FSourcePoint.Y - FDestPoint.Y;

        { In this case the actual angle is:

          270 degrees minus the angle we calculate }
        fpAngle := 270 - RadToDeg(ArcTan(YDiff / XDiff));
      end else
      begin
        { 270 - 360/0 degree quadrant }
        XDiff := FSourcePoint.X - FDestPoint.X;
        YDiff := FDestPoint.Y - FSourcePoint.Y;

        { In this case the actual angle is:

          270 degrees plus the angle we calculate }
        fpAngle := 270 + RadToDeg(ArcTan(YDiff / XDiff));
      end;

      FreeAndNil(FSourcePoint);
      FreeAndNil(FDestPoint);

  end;
  Result := fpAngle;
end;

{-------------------------------------------------------------------}
{  Render the flock                                                 }
{-------------------------------------------------------------------}
procedure TFlock.Render(ShowLines, ShowEnemyLines : boolean);
var i : integer;
    e :TList;
    tempBoid : TBoid;
    xPoints : array [0..2] of single;
    yPoints : array [0..2] of single;
    rotangle : glfloat;
    LocalX, LocalY,PositionColor : single;
begin
  e := elements();
  for i := 0 to e.Count - 1 do
  begin
    tempBoid := TBoid(e.Items[i]);
    glColor3f(tempBoid.color.r,tempBoid.color.g,tempBoid.color.b);

    xPoints[0] := (tempBoid.getX()+20.0*cos(tempBoid.getHeading()))/100;
    yPoints[0] := (tempBoid.getY()+20.0*sin(tempBoid.getHeading()))/100;
    xPoints[1] := (tempBoid.getX()+5.0*cos(tempBoid.getHeading() + (2.0/3.0)*PI))/100;
    yPoints[1] := (tempBoid.getY()+5.0*sin(tempBoid.getHeading() + (2.0/3.0)*PI))/100;
    xPoints[2] := (tempBoid.getX()+5.0*cos(tempBoid.getHeading() - (2.0/3.0)*PI))/100;
    yPoints[2] := (tempBoid.getY()+5.0*sin(tempBoid.getHeading() - (2.0/3.0)*PI))/100;


    LocalX := tempBoid.getX()/100;
    LocalY := tempBoid.getY()/100;

    glPushMatrix;
      glTranslatef(LocalX,LocalY,0.0);
      glScalef(0.005,0.005,0.005);

      rotangle := -FindAngle(LocalX, LocalY, xPoints[0], yPoints[0]);

      glRotatef(rotangle ,0.0,0.0,1.0) ;
      PositionColor := 1.0-LocalY/10;
      glColor3f(PositionColor,PositionColor,PositionColor);

      Skins[tempBoid.teamIndex].render;
    glPopMatrix;


    if ShowLines then
    begin
      glBegin(GL_LINES);
        glDisable(GL_TEXTURE_2D);
        glColor3f(1.0,1.0,1.0);
        glVertex3f(tempBoid.x/100,tempBoid.y/100,0.0);
        glVertex3f(tempBoid.friendsX/100,tempBoid.friendsY/100,0.0);
      glEnd;
    end;
    if ShowEnemyLines then
    begin
      glBegin(GL_LINES);
        glColor3f(0.0,0.0,0.0);
        glVertex3f(tempBoid.x/100,tempBoid.y/100,0.0);
        glVertex3f(tempBoid.enemiesX/100,tempBoid.enemiesY/100,0.0);
      glEnd;
    end;
  end;
end;

procedure TFlock.remove(b: TBoid);
begin
  members.Remove(b)
end;

procedure TFlock.setHeight(h: double);
begin
  height := abs(h);
end;

procedure TFlock.setMeanHeading(heading: double);
begin
  meanHeading := heading;
end;

procedure TFlock.setMeanSpeed(speed: double);
begin
  meanSpeed := speed;
end;

procedure TFlock.setMeanX(x: double);
begin
  meanX := x;
end;

procedure TFlock.setMeanY(y: double);
begin
  meanY := y;
end;

procedure TFlock.setNumberOfMembers(n: integer);
begin
  numberOfMembers := abs(n);
end;

procedure TFlock.setNumberOfTeams(teams: integer);
begin
  numberOfTeams := abs(teams);
end;

procedure TFlock.setWidth(w: double);
begin
  width := abs(w);
end;

{---------------------------------------------------------------------}
{  Destructor                                                         }
{---------------------------------------------------------------------}
destructor TFlock.Destroy;
var i : integer;
  ani: TAnimate;
begin
  FreeAndNil(members);

  for i := numberOfTeams - 1 downto 0 do
  begin
    ani := Skins[i];
    ani.Free;
    Skins[i] := Nil;
  end;

  inherited Destroy;
end;

procedure TFlock.addBoid(direction, magnitude, xPos, yPos, maxSpeed,
  minSpeed, angle, accel, sensor, death, deathC, collision, attackC,
  retreatC: double; iteamIndex: integer; iTeamColorR, iTeamColorG, iTeamColorB : single);
var teamColor : TColor;
    gene : TBoidGene;
begin
  teamColor := TColor.Create(iTeamColorR, iTeamColorG, iTeamColorB);

    gene := TBoidGene.Create(
                     2*random*PI,                   // heading
                     random(1),                     // speed
                     random*width,                  // x position
                     random*height,                 // y position
                     maxSpeed,                      // maximum speed
                     minSpeed,                      // minimum speed
                     angle,                         // turning rate
                     accel,                         // acceleration
                     sensor,                        // sensor range
                     death,                         // death range
                     deathC,                        // death constant
                     collision,                     // collision range
                     attackC,                       // attack constant
                     retreatC,                      // retreat constant
                     teamColor,                     // color
                     self,                          // This Boid
                     iteamIndex);                   // flock

    add(TBoid.Create(gene));

    FreeAndNil(gene);
end;

{ TColor }

constructor TColor.Create(red, green, blue: single);
begin
  r := red;
  g := green;
  b := blue;
end;

{ T2DCoord }

constructor T2DCoord.Create(iX, iY: single);
begin
  X := iX;
  Y := iY;
end;

end.


