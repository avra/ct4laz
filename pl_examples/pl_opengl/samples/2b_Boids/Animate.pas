//------------------------------------------------------------------------
//
// Author      : Maarten "McCLaw" Kronberger
// Email       : sulacomcclaw@hotmail.com
// Website     : http://www.sulaco.co.za
// Date        : 1 April 2003
// Version     : 1.0
// Description : Skeletal Character animation using Keyframe interpolation and 
//               Milkshape 3D ASCII files
//
//------------------------------------------------------------------------
unit Animate;

interface

uses
  ctGL,
  Model,
  //timer,
  epiktimer,
  Dialogs;

type
  TAnimate = class
  public
    AnimationFile : string;
    FramesPerSecond : double;
    Model : TModel;
    angle_speed : double;
    dist_speed : double;
    show_bones : boolean;
    mouse_x, mouse_y , mouse_b : integer;
    t : TEpikTimer;
    time : single;
    elapsedTime : single;
    canMultiTexture , isMultiTexture : Boolean;

    { Condtructor }
    constructor Create(iAnimationFile : string; iFramesPerSecond : double; icanMultiTexture , iisMultiTexture : Boolean);
    { Destructor }
    destructor Destroy; override;

    { Render Te Model }
    procedure display(model : TModel);

    { Render Model and Calculate FPS }
    procedure render();

    { Set OpenGL Projection }
    procedure set_projection();
  end;
implementation

uses
  SysUtils;

{ TAnimate }

{---------------------------------------------------------------------}
{  Condtructor                                                        }
{---------------------------------------------------------------------}
constructor TAnimate.Create(iAnimationFile: string; iFramesPerSecond: double; icanMultiTexture, iisMultiTexture : Boolean);
begin


  angle_speed := 0.5;
  dist_speed := 0.5;
  show_bones := true;
  AnimationFile := iAnimationFile;
  FramesPerSecond := iFramesPerSecond;

  canMultiTexture := icanMultiTexture;
  isMultiTexture := iisMultiTexture;
  model := TModel.create(canMultiTexture,isMultiTexture);

	if( not model.loadFromMs3dAsciiFile( AnimationFile ) ) then
		ShowMessage( 'Couldn''t load the model' );

  t := TEpikTimer.create(nil);
end;

{---------------------------------------------------------------------}
{  Destructor                                                         }
{---------------------------------------------------------------------}
destructor TAnimate.Destroy;
begin
  FreeAndNil(model);
  FreeAndNil(t);

  inherited Destroy;
end;

{---------------------------------------------------------------------}
{  Set OpenGL Projection                                              }
{---------------------------------------------------------------------}
procedure TAnimate.set_projection;
begin
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	glFrustum (-1.0, 1.0, -1.0, 1.0, 1.0, 1500.0);
end;

{---------------------------------------------------------------------}
{  Render Te Model                                                    }
{---------------------------------------------------------------------}
procedure TAnimate.display(model: TModel);
begin
	model.render();				// render model
end;

{---------------------------------------------------------------------}
{  Render Model and Calculate FPS                                     }
{---------------------------------------------------------------------}
procedure TAnimate.render();
begin
  glPushMatrix;
  glRotatef(90,1.0,0.0,0.0);
  display(model);
  glPopMatrix;
  // advance the animation in accordance to
	// elapsed time since last call
  elapsedTime := t.Elapsed() - time;
  time := time + elapsedTime;
  model.advanceAnimation( elapsedTime * FramesPerSecond);

end;

end.
