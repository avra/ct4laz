{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit boidsmw;


interface

uses
  Classes, SysUtils, LCLProc, LResources, Forms, Controls, Graphics, Dialogs,LCLIntf,
  OpenGLPanel, ctGL, ctGLU,
  OpenGL_Textures,
  fvars, Boid, Animate;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
  public
    cube_rotationx: GLFloat;
    cube_rotationy: GLFloat;
    cube_rotationz: GLFloat;
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation  

{$R *.lfm}

var
  stafsLoaded :boolean = false;
  ElapsedTime,DemoStart, LastTime : GLfloat;
   // User variables
  HighlightIndex : integer;
  LPressed : boolean;
  KPressed : boolean;
  isSetupMultiTexture : boolean = True;
  isHighPolyCount : boolean = true;
  TexBG : GLuint;

  // Flocking variables
  Flock : TFlock;

  //Fog Variables
  fogColor : array [0..3] of GLfloat;

  //Terrain
  Terrain : TAnimate;

{------------------------------------------------------------------}
{  Function to convert int to string. (No sysutils = smaller EXE)  }
{------------------------------------------------------------------}
function IntToStr(Num : Integer) : String;  // using SysUtils increase file size by 100K
begin
  Str(Num, result);
end;


{------------------------------------------------------------------}
{  Function to draw the actual scene                               }
{------------------------------------------------------------------}
procedure glDraw;
begin
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);    // Clear The Screen And The Depth Buffer
  glClearColor(0.3,0.3,1.0,0.0);
  glLoadIdentity();                                       // Reset The View

  glTranslatef(-4.5,-2.0,-5);

   // Render Flock
   glPushMatrix;
    glRotatef(-45,1.0,0.0,0.0);
    Flock.moveMembers;
    Flock.Render(LPressed,KPressed);
   glPopMatrix;

   // Draw Background
  // if(MultiTexture) then
  // begin
     glActiveTextureARB(GL_TEXTURE0_ARB);
 //  end;

   glPushMatrix;
    glTranslatef(4.5,4.0,-8.0);
    glBindTexture(GL_TEXTURE_2D,TexBG);
    glColor3f(1.0,1.0,1.0);
    glBegin(GL_QUADS);
      glTexCoord2f(0.0, 0.0);  glVertex3f( 8.0,-1.0, 0.0);
      glTexCoord2f(0.0, 1.0);  glVertex3f( 8.0, 4.0, 0.0);
      glTexCoord2f(1.0, 1.0);  glVertex3f(-8.0, 4.0, 0.0);
      glTexCoord2f(1.0, 0.0);  glVertex3f(-8.0,-1.0, 0.0);
    glEnd;
   glPopMatrix;

   glDisable(GL_TEXTURE_2D);

   glPushMatrix;
    glTranslatef(6.5,-2.0,-1.5);
    glRotatef(-70,1.0,0.0,0.0);

    glScalef(0.05,0.05,0.05);
    glColor3f(1.0,1.0,1.0);
 // Render Terrain
   Terrain.render;
   glPopMatrix;

   glDisable(GL_TEXTURE_2D);
   glDisable(GL_TEXTURE_2D);
end;


{------------------------------------------------------------------}
{  Initialise OpenGL                                               }
{------------------------------------------------------------------}
procedure glInit;
var ColRed, ColGreen, ColBlue : single;
    i : integer;
    XPos, YPos : double;
    SkinString : string;
begin
  glClearColor(0.3,0.3,1.0,0.0); 	         // Blue Background
  glShadeModel(GL_SMOOTH);                 // Enables Smooth Color Shading
  glClearDepth(1.0);                       // Depth Buffer Setup
  glEnable(GL_DEPTH_TEST);                 // Enable Depth Buffer
  glDepthFunc(GL_LESS);		                 // The Type Of Depth Test To Do

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GREATER,0.4);

  // Add some fog
  glEnable(GL_FOG);

  fogColor[0] := 0.0;
  fogColor[1] := 0.0;
  fogColor[2] := 0.8;
  fogColor[3] := 0.0;

  glFogi (GL_FOG_MODE, GL_EXP2);
  glFogfv (GL_FOG_COLOR, @fogColor);
  glFogf (GL_FOG_DENSITY, 0.04);
  glHint (GL_FOG_HINT,GL_NICEST );

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);   //Realy Nice perspective calculations


//================================================================

if stafsLoaded then exit;


   LoadTexture(pathMedia+'reefbackgr.bmp', TexBG, FALSE);    // Load the Texture

  // Use High Polygon Models?
  if(isHighPolyCount) then
    SkinString := pathMedia+'fish'   else
    SkinString := pathMedia+'lpfish';

  Flock := TFlock.Create(8 ,4 ,1000 ,1000,SkinString,isSetupMultiTexture,false);

  // Add Sharks
  for i := 0 to 3 do
  begin
    Flock.addBoid(2*random*PI,                    // heading
                  random(1),                      // speed
                  random*1000,                    // x position
                  random*1000,                    // y position
                  0.8,                            // maximum speed
                  0.5,                            // minimum speed
                  0.01,                           // turning rate
                  0.05,                           // acceleration
                  60.0,                           // sensor range
                  10.0,                           // death range
                  0.23109,                        // death constant
                  50.0,                           // collision range
                  1.0,                            // attack constant
                  1.0,                            // retreat constant
                  0,                              // Team Index
                  0.99,                           // Red
                  0.99,                           // Green
                  0.99);                          // Blue
  end;


  // Add Small Fish To Flock
  ColRed := ((random-0.1)/2)+0.5;
  ColGreen := ((random-0.1)/2)+0.5;
  ColBlue := ((random-0.1)/2)+0.5;
  XPos := random*1000;
  YPos := random*1000;
  for i := 0 to 20 do
  begin
    Flock.addBoid(2*random*PI,                   // heading
                  random(1),                     // speed
                  XPos,                          // x position
                  YPos,                          // y position
                  1.5,                           // maximum speed
                  0.6,                           // minimum speed
                  0.02,                          // turning rate
                  0.05,                          // acceleration
                  60.0,                          // sensor range
                  10.0,                          // death range
                  0.23109,                       // death constant
                  25.0,                          // collision range
                  1.0,                           // attack constant
                  1.0,                           // retreat constant
                  1,                             // Team Index
                  ColRed,                        // Red
                  ColGreen,                      // Green
                  ColBlue);                      // Blue
  end;

  // Add Middle Sized Fish To Flock
  ColRed := ((random-0.1)/2)+0.5;
  ColGreen := ((random-0.1)/2)+0.5;
  ColBlue := ((random-0.1)/2)+0.5;

  for i := 0 to 10 do
  begin
    Flock.addBoid(2*random*PI,                   // heading
                  random(1),                     // speed
                  random*1000,                   // x position
                  random*1000,                   // y position
                  1.0,                           // maximum speed
                  0.5,                           // minimum speed
                  0.02,                          // turning rate
                  0.05,                          // acceleration
                  60.0,                          // sensor range
                  10.0,                          // death range
                  0.23109,                       // death constant
                  40.0,                          // collision range
                  1.0,                           // attack constant
                  1.0,                           // retreat constant
                  2,                             // Team Index
                  ColRed,                        // Red
                  ColGreen,                      // Green
                  ColBlue);                      // Blue
  end;

  // Add Big Fish To Flock
  ColRed := ((random-0.1)/2)+0.5;
  ColGreen := ((random-0.1)/2)+0.5;
  ColBlue := ((random-0.1)/2)+0.5;

  for i := 0 to 10 do
  begin
    Flock.addBoid(2*random*PI,                    // heading
                  random(1),                      // speed
                  random*1000,                    // x position
                  random*1000,                    // y position
                  0.8,                            // maximum speed
                  0.3,                            // minimum speed
                  0.01,                           // turning rate
                  0.05,                           // acceleration
                  60.0,                           // sensor range
                  10.0,                           // death range
                  0.23109,                        // death constant
                  35.0,                           // collision range
                  1.0,                            // attack constant
                  1.0,                            // retreat constant
                  3,                              // Team Index
                  ColRed,                         // Red
                  ColGreen,                       // Green
                  ColBlue);                       // Blue
  end;

  Flock.setMeanX(Flock.calculateMeanX());
  Flock.setMeanY(Flock.calculateMeanY());
  // Load Terrain
  HighlightIndex := 0;
  Terrain := TAnimate.Create(pathMedia+'terrain.txt',24,isSetupMultiTexture,isSetupMultiTexture);

stafsLoaded:=true;
end;

procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
var cx, cy, cz : Integer;
    I : Integer;
begin
  if Sender=nil then ;

  glInit;
  //=======================================================
 { glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, width/height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();  }
  //=========================================================
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, width/ height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  //==========================================================

  glDraw;
  //==========================================================================================
 // OpenGLPanel1.SwapBuffers;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if Sender=nil then ;

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;
  
  Application.AddOnIdleHandler(OnAppIdle);

end;

procedure TForm1.FormShow(Sender: TObject);
begin
   DemoStart:=LCLIntf.GetTickCount64;
   stafsLoaded:=false;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  if Sender=nil then ;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;

  OpenGLPanel1Paint(nil);
  OpenGLPanel1.SwapBuffers;
end;

end.

