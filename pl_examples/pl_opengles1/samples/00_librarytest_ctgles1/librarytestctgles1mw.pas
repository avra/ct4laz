unit librarytestctgles1mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ComCtrls, ctutils,
  ctopengles1panel,
  ctGLES1, ctGLES1_rpt;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Memo1: TMemo;
    Memo2: TMemo;
    PageControl1: TPageControl;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    fGLPanel: TOpenGLES1Panel;
    procedure TestLibFunctions;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
 var ss1,ss2:String;
begin
 if fGLPanel<> nil then exit;

  ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
  ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

 Memo1.Clear;
 Memo2.Clear;

  if Not OpenGLES_Initialize(ss1,ss2) then
  begin
   Memo1.Lines.add('ERROR: '+ss1+' NOT Loaded');
   Memo1.Lines.add('ERROR: '+ss2+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(ss1+' Loaded OK');
   Memo1.Lines.add(ss2+' Loaded OK');
  end;

  TestLibFunctions;

  //...........................

  Label1.Caption:='"BASE" OpenEGL  Ver: '+OpenEGLLib_VersionStr;
  Label2.Caption:='"BASE" OpenGLES Ver: '+OpenGLESLib_VersionStr;
end;

procedure TForm1.Button2Click(Sender: TObject);   
 var ss1,ss2:String;
begin
 Button1.Enabled:=false;

 ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
 ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

 Memo1.Clear;   
 Memo2.Clear;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLES1Panel.Create(self);
    fGLPanel.libEGL:=ss1;
    fGLPanel.libOpenGLES:=ss2;
  end;

  if Not fGLPanel.InitOpenGLES then
  begin
   Memo1.Lines.add('ERROR: '+ss1+' NOT Loaded');
   Memo1.Lines.add('ERROR: '+ss2+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(ss1+' Loaded OK');
   Memo1.Lines.add(ss2+' Loaded OK');
  end;

  TestLibFunctions;

  //...........................

  Label1.Caption:='"FULL" OpenEGL  Ver: '+OpenEGLLib_VersionStr;
  Label2.Caption:='"FULL" OpenGLES Ver: '+OpenGLESLib_VersionStr;

end;

procedure TForm1.TestLibFunctions;
begin
 Memo1.Lines.add('--------- Commands -----------');
 OpenGLES_WriteCommandsReport(Memo1.Lines,RadioGroup1.ItemIndex);

 Memo2.Lines.add('--------- Extensions ---------');
 OpenGLES_WriteExtensionsReport(Memo2.Lines,RadioGroup1.ItemIndex);
end;

end.

