unit sample1mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ctutils,
  ctGLES1, ctopengles1panel;

type

  { TForm1 }

  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    fGLPanel: TOpenGLES1Panel;
  public
    procedure glPaint(Sender: TObject);
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}



 const
   square:array[0..11] of glfloat=(0.25, 0.25, 0.0,
                                   0.75, 0.25, 0.0,
 	                           0.25, 0.75, 0.0,
  	                           0.75, 0.75, 0.0);


procedure TForm1.FormShow(Sender: TObject);
 var ss1,ss2:string;
begin

 ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
 ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLES1Panel.Create(self);
    fGLPanel.libEGL:=ss1;
    fGLPanel.libOpenGLES:=ss2;
  end;

  if Not fGLPanel.InitOpenGLES then exit;

   glClearColor (0.0, 0.0, 0.0, 0.0);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrthof(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
   glVertexPointer(3, GL_FLOAT, 0, @square);
   glEnableClientState(GL_VERTEX_ARRAY);
end;

procedure TForm1.glPaint(Sender: TObject);

begin

//=============================================
   glClear(GL_COLOR_BUFFER_BIT);
   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
   glFlush();
//=============================================

  fGLPanel.SwapBuffers;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  glPaint(nil);
end;


end.

