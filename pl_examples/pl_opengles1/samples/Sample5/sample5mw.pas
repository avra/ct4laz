unit sample5mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls, 
  ctutils,
  ctGLES1, ctopengles1panel;

type

  { TForm1 }

  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private  
    fGLPanel: TOpenGLES1Panel;
  public
    procedure glPaint(Sender: TObject);
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}

 const
   triangle:array[0..8] of glfloat=(
	 0.5,-0.5, 0.0,
	 0.0, 0.5, 0.0,
	-0.5,-0.5, 0.0);

    colors :array[0..11] of glfloat=(
	1.0, 0.0, 0.0, 1.0,
	0.0, 1.0, 0.0, 1.0,
	0.0, 0.0, 1.0, 1.0);

procedure TForm1.FormShow(Sender: TObject);
 var ss1,ss2:string;
begin

 ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
 ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLES1Panel.Create(self);
    fGLPanel.libEGL:=ss1;
    fGLPanel.libOpenGLES:=ss2;
  end;

  if Not fGLPanel.InitOpenGLES then exit;
//---------------------------------------------

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClearDepthf(1.0);

  glVertexPointer(3, GL_FLOAT, 0, @triangle);
  glColorPointer(4, GL_FLOAT, 0, @colors);
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
end;

procedure TForm1.glPaint(Sender: TObject);
begin

//=============================================
  glClear (GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  glDrawArrays(GL_TRIANGLES, 0, 3);

  glPushMatrix();

		glTranslatef(-0.2, 0.0, -1.0);

		glDrawArrays(GL_TRIANGLES, 0, 3);

		glRotatef(45.0, 0.0, 0.0, 1.0);

		glDrawArrays(GL_TRIANGLES, 0, 3);

  glPopMatrix();

  glPushMatrix();

		glTranslatef(0.5, 0.0, 0.0);

		glScalef(0.5, 0.5, 0.5);
		glDrawArrays(GL_TRIANGLES, 0, 3);

  glPopMatrix();

  glFlush();
//=============================================

  fGLPanel.SwapBuffers;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  glPaint(nil);
end;


end.

