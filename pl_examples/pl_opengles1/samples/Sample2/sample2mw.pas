unit sample2mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,   
  ctutils,
  ctGLES1, ctopengles1panel;

type

  { TForm1 }

  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private    
    fGLPanel: TOpenGLES1Panel;
  public
    procedure glPaint(Sender: TObject);
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}

{ TForm1 }
 var
 vertices: array[0..8] of glfixed;
 triangle: array[0..8] of glfloat;
 colors: array[0..11]  of glfloat;


function Int2Str(Number : glfixed) : String;
var
  Minus : Boolean;
begin
   Result := '';
   if Number = 0 then
      Result := '0';
   Minus := Number < 0;
   if Minus then
      Number := -Number;
   while Number < 0 do
   begin
      Result := Char((Number mod 10) + glfixed('0')) + Result;
      Number := Number div 10;
   end;
   if Minus then
      Result := '-' + Result;
end;

function f2vt(value: single): integer;
var
 temp: integer;
begin
        temp := integer(trunc(single(value) * single(65535)));
        result := integer(temp);
end;


procedure TForm1.FormShow(Sender: TObject);
 var ss1,ss2:string;
begin

 ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
 ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLES1Panel.Create(self);
    fGLPanel.libEGL:=ss1;
    fGLPanel.libOpenGLES:=ss2;
  end;

  if Not fGLPanel.InitOpenGLES then exit;
//---------------------------------------------
        vertices[0]:=f2vt(-0.4);  //x
	vertices[1]:=f2vt(-0.4);   //y
	vertices[2]:=0;   //z

	vertices[3]:=f2vt(0.4);  //x
	vertices[4]:=f2vt(-0.4);   //y
	vertices[5]:=0;   //z

	vertices[6]:=0;  //x
	vertices[7]:=f2vt(0.4);   //y
	vertices[8]:=0;   //z

	triangle[0] := 0.25;
	triangle[1] := 0.25;
	triangle[2] := 0.0;
	triangle[3] := 0.75;
	triangle[4] := 0.25;
	triangle[5] := 0.0;
	triangle[6] := 0.25;
	triangle[7] := 0.75;
	triangle[8] := 0.0;

	colors[0] := 1.0;
	colors[1] := 0.0;
	colors[2] := 0.0;
	colors[3] := 1.0;

	colors[4] := 0.0;
	colors[5] := 1.0;
	colors[6] := 0.0;
	colors[7] := 1.0;

	colors[8] := 0.0;
	colors[9] := 0.0;
	colors[10] := 1.0;
	colors[11] := 1.0;


        glClearColor(0.3, 0.5, 0.5, 1.0); // clear blue
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glShadeModel(GL_SMOOTH);
end;

procedure TForm1.glPaint(Sender: TObject);
begin

//=============================================

   glClear(GL_COLOR_BUFFER_BIT);

   glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, 0, @triangle);

   glEnableClientState(GL_COLOR_ARRAY);
   glColorPointer(4, GL_FLOAT, 0, @colors);

  // glTranslatef(0.0,0.0,0.0);
   glRotatef(1.0,0.0,0.0,0.1);

   glDrawArrays(GL_TRIANGLES, 0, 3);

   glDisableClientState(GL_COLOR_ARRAY);
   glDisableClientState(GL_VERTEX_ARRAY);
//=============================================

  fGLPanel.SwapBuffers;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  glPaint(nil);
end;


end.

