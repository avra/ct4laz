unit sample6mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls, 
  ctutils,
  ctGLES1, ctopengles1panel;

type

  { TForm1 }

  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private      
    fGLPanel: TOpenGLES1Panel;
  public

    procedure glPaint(Sender: TObject);
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}

 const
   box:array[0..71] of glfloat=(
	// FRONT
	-0.5, -0.5,  0.5,
	 0.5, -0.5,  0.5,
	-0.5,  0.5,  0.5,
	 0.5,  0.5,  0.5,
	// BACK
	-0.5, -0.5, -0.5,
	-0.5,  0.5, -0.5,
	 0.5, -0.5, -0.5,
	 0.5,  0.5, -0.5,
	// LEFT
	-0.5, -0.5,  0.5,
	-0.5,  0.5,  0.5,
	-0.5, -0.5, -0.5,
	-0.5,  0.5, -0.5,
	// RIGHT
	 0.5, -0.5, -0.5,
	 0.5,  0.5, -0.5,
	 0.5, -0.5,  0.5,
	 0.5,  0.5,  0.5,
	// TOP
	-0.5,  0.5,  0.5,
	 0.5,  0.5,  0.5,
        -0.5,  0.5, -0.5,
	 0.5,  0.5, -0.5,
	// BOTTOM
	-0.5, -0.5,  0.5,
	-0.5, -0.5, -0.5,
	 0.5, -0.5,  0.5,
	 0.5, -0.5, -0.5);



var
 xrot,yrot:glfloat;

procedure TForm1.FormShow(Sender: TObject);
 var ss1,ss2:string;
begin

 ss1:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_EGL;
 ss2:= {$IFDEF Windows}ctGetRuntimesCPUOSDir(true)+'opengles1'+ctGetDirTrail+{$ENDIF}LIBNAME_OPENGLES;

  if fGLPanel=Nil then
  begin
    fGLPanel:=TOpenGLES1Panel.Create(self);
    fGLPanel.libEGL:=ss1;
    fGLPanel.libOpenGLES:=ss2;
  end;

  if Not fGLPanel.InitOpenGLES then exit;
//---------------------------------------------


 glEnable(GL_DEPTH_TEST);
 glDepthFunc(GL_LEQUAL);

 glClearColor(0.0, 0.0, 0.0, 0.0);
 glClearDepthf(1.0);

 glVertexPointer(3, GL_FLOAT, 0, @box);
 glEnableClientState(GL_VERTEX_ARRAY);

 glShadeModel(GL_FLAT);

end;

procedure TForm1.glPaint(Sender: TObject);
begin
 xrot :=xrot+ 1.0;
 if xrot>1000000 then xrot:=0;

 yrot :=xrot+ 1.0;
 if yrot>1000000 then yrot:=0;
//=============================================
   glClear (GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
   glLoadIdentity();

   glScalef(0.6,0.6,0.6);

   glRotatef(xrot, 1.0, 0.0, 0.0);
   glRotatef(yrot, 0.0, 1.0, 0.0);

   glColor4f(1.0, 0.0, 0.0, 1.0);
   glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
   glDrawArrays(GL_TRIANGLE_STRIP, 4, 4);

   glColor4f(0.0, 1.0, 0.0, 1.0);
   glDrawArrays(GL_TRIANGLE_STRIP, 8, 4);
   glDrawArrays(GL_TRIANGLE_STRIP, 12, 4);

   glColor4f(0.0, 0.0, 1.0, 1.0);
   glDrawArrays(GL_TRIANGLE_STRIP, 16, 4);
   glDrawArrays(GL_TRIANGLE_STRIP, 20, 4);

   glFlush ();
//=============================================

  fGLPanel.SwapBuffers;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  glPaint(nil);
end;


end.

