program ImgBrowser;

{$MODE Delphi}

uses
  Forms, Interfaces,
  ImgBrowsermw in 'Main.pas';

begin
  Application.Initialize;
  Application.Title := 'VCL Image Browser';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
