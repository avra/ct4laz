program LCLImager;


{$mode objfpc}{$H+}

uses
  Interfaces, // this includes the LCL widgetset
  Forms, LCLImagermw, AboutUnit, lclimagerutils;


begin
  Application.Title := 'LCL Imager';
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.Run;
end.

