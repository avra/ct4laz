{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}
{$APPTYPE GUI}
{$I ..\DirectXapp.inc}

program HDRPipeline;

uses
  Windows,
  SysUtils,
  Interfaces, // this includes the LCL widgetset
  Direct3D9,
  DirectXUT,
  HDRPipelineUnit,
  HDREnumeration,
  Luminance,
  HDRScene,
  PostProcess;


//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Entry point to the program. Initializes everything, and goes into a
//       message-processing loop. Idle time is used to render the scene.
//-----------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------

{$R *.res}

begin
  CreateCustomDXUTobjects;

  // Set the callback functions. These functions allow DXUT to notify
  // the application about device changes, user input, and windows messages.  The
  // callbacks are optional so you need only set callbacks for events you're interested
  // in. However, if you don't handle the device reset/lost callbacks then the sample
  // framework won't be able to reset your device since the application must first
  // release all device resources before resetting.  Likewise, if you don't handle the
  // device created/destroyed callbacks then DXUT won't be able to
  // recreate your device resources.
  DXUTSetCallbackDeviceCreated(OnCreateDevice);
  DXUTSetCallbackDeviceReset(OnResetDevice);
  DXUTSetCallbackDeviceLost(OnLostDevice);
  DXUTSetCallbackDeviceDestroyed(OnDestroyDevice);
  DXUTSetCallbackMsgProc(MsgProc);
  DXUTSetCallbackFrameRender(OnFrameRender);
  DXUTSetCallbackFrameMove(OnFrameMove);

  InitApp;

  // Initialize DXUT and create the desired Win32 window and Direct3D device for the application
  DXUTInit(True, True, True); // Parse the command line, handle the default hotkeys, and show msgboxes
  DXUTSetCursorSettings(True, True); // Show the cursor and clip it when in full screen
  DXUTCreateWindow('CodeTyphon-HDR Pipeline Demonstration');
  DXUTCreateDevice(D3DADAPTER_DEFAULT, True, 640, 480, IsDeviceAcceptable, ModifyDeviceSettings);

  // Start the render loop
  g_dwLastFPSCheck := GetTickCount64;
  DXUTMainLoop;

  ExitCode:= DXUTGetExitCode;

  DXUTFreeState;
  DestroyCustomDXUTobjects;
end.


