{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}
{$APPTYPE GUI}
{$I ..\DirectXapp.inc}

program EmptyProject;

uses
  Windows,
  SysUtils,
  Interfaces, // this includes the LCL widgetset
  Direct3D9,
  DirectXUT,
  EmptyUnit;

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------

//{$IFDEF WINDOWS}{$R EmptyProject.rc}{$ENDIF}

{$R *.res}

begin
  // Set the callback functions
  DXUTSetCallbackDeviceCreated(OnCreateDevice);
  DXUTSetCallbackDeviceReset(OnResetDevice);
  DXUTSetCallbackDeviceLost(OnLostDevice);
  DXUTSetCallbackDeviceDestroyed(OnDestroyDevice);
  DXUTSetCallbackMsgProc(MsgProc);
  DXUTSetCallbackFrameRender(OnFrameRender);
  DXUTSetCallbackFrameMove(OnFrameMove);

  // TODO: Perform any application-level initialization here

  // Initialize DXUT and create the desired Win32 window and Direct3D device for the application
  DXUTInit(True, True, True); // Parse the command line, handle the default hotkeys, and show msgboxes
  DXUTSetCursorSettings(True, True); // Show the cursor and clip it when in full screen
  DXUTCreateWindow('CodeTyphon-EmptyProject');
  DXUTCreateDevice(D3DADAPTER_DEFAULT, true, 640, 480, IsDeviceAcceptable, ModifyDeviceSettings);

  // Start the render loop
  DXUTMainLoop;

  // TODO: Perform any application-level cleanup here

  ExitCode:= DXUTGetExitCode;

  DXUTFreeState;
end.

