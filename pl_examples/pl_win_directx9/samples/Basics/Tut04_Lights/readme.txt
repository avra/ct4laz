Desc: Rendering 3D geometry is much more interesting when dynamic lighting
           is added to the scene. To use lighting in D3D, you must create one or
           lights, setup a material, and make sure your geometry contains surface
           normals. Lights may have a position, a color, and be of a certain type
           such as directional (light comes from one direction), point (light
           comes from a specific x,y,z coordinate and radiates in all directions)
           or spotlight. Materials describe the surface of your geometry,
           specifically, how it gets lit (diffuse color, ambient color, etc.).
          Surface normals are part of a vertex, and are needed for the D3D's
          internal lighting calculations.   
