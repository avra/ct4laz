Now that we know how to create a device and render some 2D vertices,
this tutorial goes the next step and renders 3D geometry. To deal with
3D geometry we need to introduce the use of 4x4 matrices to transform
the geometry with translations, rotations, scaling, and setting up our
camera.

Geometry is defined in model space. We can move it (translation),
rotate it (rotation), or stretch it (scaling) using a world transform.
The geometry is then said to be in world space. Next, we need to
position the camera, or eye point, somewhere to look at the geometry.
Another transform, via the view matrix, is used, to position and
rotate our view. With the geometry then in view space, our last
transform is the projection transform, which "projects" the 3D scene
into our 2D viewport.

Note that in this tutorial, we are introducing the use of D3DX, which
is a set of helper utilities for D3D. In this case, we are using some
of D3DX's useful matrix initialization functions. To use D3DX, simply
include <d3dx9.h> and link with d3dx9.lib.  
