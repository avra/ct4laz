In this tutorial, we are rendering some vertices. 
This introduces the concept of the vertex buffer, a Direct3D object used to store
vertices. Vertices can be defined any way we want by defining a
custom structure and a custom FVF (flexible vertex format). In this
tutorial, we are using vertices that are transformed (meaning they
are already in 2D window coordinates) and lit (meaning we are not
 using Direct3D lighting, but are supplying our own colors).
