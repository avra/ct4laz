{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

{$APPTYPE GUI}
{$I ..\..\DirectXapp.inc}

program GetDXVer;

uses
  Windows,
  SysUtils,
  Interfaces, // this includes the LCL widgetset
  GetDXVerUnit in 'GetDXVerUnit.pas';

//-----------------------------------------------------------------------------
// Desc: Entry point to the program. Initializes everything, and pops
//       up a message box with the results of the GetDXVersion call
//-----------------------------------------------------------------------------
var
  hr: HRESULT;
  strResult: String;
  dwDirectXVersion: DWORD = 0;
  strDirectXVersion: String;
  CPUstr:string;

//{$IFDEF WINDOWS}{$R GetDXVer.rc}{$ENDIF}

{$R *.res}

begin
  hr := GetDXVersion(dwDirectXVersion, strDirectXVersion);
  if SUCCEEDED(hr) then
  begin
    if (dwDirectXVersion > 0)
    then strResult := Format('DirectX %s installed', [strDirectXVersion])
    else strResult := 'DirectX not installed';
  end else
    strResult := 'Unknown version of DirectX installed';

   CPUstr:='32bits';
  {$ifdef CPU64}
   CPUstr:='64bits';
  {$endif}

  MessageBox(0, PChar(strResult), PChar('DirectX '+CPUstr+' Version:'), MB_OK or MB_ICONINFORMATION);
end.

