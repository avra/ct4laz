// Desc: This is the second tutorial for using the XACT API. This tutorial
//       differs from the first tutorial by loading a streaming XACT wave bank
//       and playing background music that is streamed from this wave bank.
//       It also shows how to do zero-latency streaming. 
