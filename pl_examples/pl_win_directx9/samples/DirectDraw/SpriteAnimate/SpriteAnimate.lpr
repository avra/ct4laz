program SpriteAnimate;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Sprite Animate Example                   }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : SpriteAnimate.h,                        }
{                          SpriteAnimate.cpp                       }
{ The original Pascal code is : SpriteAnimate_vcl.pas              }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   SpriteAnimate demonstrates a simple technique to animate       }
{   DirectDraw surfaces.                                           }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   For details on how to setup a full-screen DirectDraw app, see  }
{   the FullScreenMode sample.                                     }
{                                                                  }
{   One simple method to animate sprites in DirectDraw is author a }
{   single bitmap file to contain many frames of animation.  The   }
{   program then stores the current frame indicator in each        }
{   sprite's state.  From this current frame indicator, it can     }
{   progmatically derive a src rect that encompasses only a        }
{   single frame of animation in the off-screen plain surface.     }
{   The rect then is blited from the off-screen plain surface to   }
{   the back buffer.                                               }
{                                                                  }
{   InitDirectDraw() in the sample shows how to build an cached    }
{   array of these source rects. DisplayFrame() then access this   }
{   array based on each sprite's current frame.                    }
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  SpriteAnimatemw in 'MainForm.pas' {FormMain},
  DDUtil in '..\..\Common\DDUtil.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm( TFormMain, FormMain );
  Application.Run;
end.
