unit SpriteAnimatemw;

{$MODE Delphi}

interface

uses
  Windows,
  Messages,
  SysUtils,
  Forms,
  Classes,
  ExtCtrls,
  DirectDraw,
  DDUtil;

const
  SCREEN_WIDTH = 640;
  SCREEN_HEIGHT = 480;
  SCREEN_BPP = 16;

  SPRITE_DIAMETER = 48;
  NUM_SPRITES = 25;
  NUM_FRAMES = 30;
  NUM_RAND = 100;

  HELPTEXT : PChar = 'Press Escape to quit.';

type
  PSpriteRecord = ^TSpriteRecord;
  TSpriteRecord = packed record
    fRotationSpeed : Single;
    fRotationTick : Single;
    lFrame : LongInt;
    bClockwise : Boolean;
    fPosX : Single;
    fPosY : Single;
    fVelX : Single;
    fVelY : Single;
  end;

  TFormMain = class( TForm )
    Timer1 : TTimer;
    procedure FormCreate( Sender : TObject );
    procedure FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
    procedure FormActivate( Sender : TObject );
    procedure FormDeactivate( Sender : TObject );
    procedure FormClose( Sender : TObject; var Action : TCloseAction );
    procedure FormShow( Sender : TObject );
    procedure Timer1Timer( Sender : TObject );
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FLastTick : DWORD;
    FActive : Boolean;
    FDisplay : TDisplay;
    FAnimationSurface : TSurface;
    FTextSurface : TSurface;
    FSprite : array[ 0..NUM_SPRITES - 1 ] of TSpriteRecord;
    FRctFrame : array[ 0..NUM_FRAMES - 1 ] of TRect;
    FRandTable : array[ 0..NUM_RAND - 1 ] of LongInt;
    FRandIndex : DWORD;
    procedure FormOnIdle( Sender : TObject; var Done : Boolean );
    procedure FormSetCursor( var aMsg : TMessage ); message WM_SETCURSOR;
    procedure ErrorOut( hRet : HRESULT; FuncName : string );
    procedure FreeDirectDraw;
    function GetNextRand : LongInt;
    procedure UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
    function DisplayFrame : HRESULT;
    function RestoreSurfaces : HRESULT;
    function ProcessNextFrame : HRESULT;
    function InitDirectDraw : HRESULT;
  end;

var
  FormMain : TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}

procedure TFormMain.ErrorOut( hRet : HRESULT; FuncName : string );
var
  ErrorMessage : string;
begin
  ErrorMessage := FuncName + ': ' + #13 + DDErrorString( hRet );
  MessageBox( Handle, PChar( ErrorMessage ), PChar( Caption ), MB_ICONERROR or MB_OK );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.Create
   Desc: Initialize the window
-----------------------------------------------------------------------------}

procedure TFormMain.FormCreate( Sender : TObject );
begin
  // Maximize the window
  Left := 0;
  Top := 0;
  Width := GetSystemMetrics( SM_CXSCREEN );
  Height := GetSystemMetrics( SM_CYSCREEN );
  Application.OnIdle := FormOnIdle;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormClose
   Desc: Cleanup
-----------------------------------------------------------------------------}

procedure TFormMain.FormClose( Sender : TObject; var Action : TCloseAction );
begin
  FreeDirectDraw;
  Application.Minimize;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormKeyUp
   Desc: React to keystrokes
-----------------------------------------------------------------------------}

procedure TFormMain.FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
begin

end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormActivate
   Desc: Toggle active-flag
-----------------------------------------------------------------------------}

procedure TFormMain.FormActivate( Sender : TObject );
begin
  FActive := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormDeactivate
   Desc: Toggle active-flag
----------------------------------------------------------------------------}

procedure TFormMain.FormDeactivate( Sender : TObject );
begin
  FActive := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormSetCursor
   Desc: Turn off cursor
-----------------------------------------------------------------------------}

procedure TFormMain.FormSetCursor( var aMsg : TMessage );
begin
  SetCursor( 0 );
  aMsg.Result := 1;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormOnIdle
   Desc: Update frame and flip pages
-----------------------------------------------------------------------------}

procedure TFormMain.FormOnIdle( Sender : TObject; var Done : Boolean );
var
  hr : HRESULT;
begin
  if FActive then
  begin
    hr := ProcessNextFrame;
    if hr <> DD_OK then
    begin
      ErrorOut( hr, 'ProcessNextFrame' );
      Close;
    end;
  end;
  Done := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Initialize Direct Draw
-----------------------------------------------------------------------------}

procedure TFormMain.FormShow( Sender : TObject );
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if hr <> DD_OK then
  begin
    ErrorOut( hr, 'ProcessNextFrame' );
    Application.Terminate;
  end;
end;

{-----------------------------------------------------------------------------
   Name: DisplayFrame()
   Desc: Blts a the sprites to the back buffer, then it blts or flips the
         back buffer onto the primary buffer.
-----------------------------------------------------------------------------}

function TFormMain.DisplayFrame : HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
begin
  if FDisplay = nil then
  begin
    result := DD_OK;
    exit;
  end;

  // Fill the back buffer with black, ignoring errors until the flip
  FDisplay.Clear( 0 );

  // Blt the help text on the backbuffer, ignoring errors until the flip
  FDisplay.Blt( 10, 10, FTextSurface, nil );

  // Blt all the sprites onto the back buffer using color keying,
  // ignoring errors until the flip. Note that all of these sprites
  // use the same DirectDraw surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
    FDisplay.Blt( Round( FSprite[ iSprite ].fPosX ), Round( FSprite[ iSprite ].fPosY ), FAnimationSurface, @FRctFrame[ FSprite[ iSprite ].lFrame ] );

  // Flip or blt the back buffer onto the primary buffer
  hr := FDisplay.Flip;
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
  Name: FreeDirectDraw
  Desc: Release all the DirectDraw objects
-----------------------------------------------------------------------------}

procedure TFormMain.FreeDirectDraw;
begin
  FAnimationSurface.Free;
  FAnimationSurface := nil;
  FTextSurface.Free;
  FTextSurface := nil;
  FDisplay.Free;
  FDisplay := nil;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDraw()
   Desc: Create the DirectDraw object, and init the surfaces
-----------------------------------------------------------------------------}

function TFormMain.InitDirectDraw : HRESULT;
var
  hr : HRESULT;
  iSprite, iFrame, iRand : integer;
begin
  ZeroMemory( @FSprite, SizeOf( TSpriteRecord ) * NUM_SPRITES );

  // Initialize all the surfaces we need
  FDisplay := TDisplay.Create;

  hr := FDisplay.CreateFullScreenDisplay( Handle, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP );
  if ( hr <> DD_OK ) then
  begin
    ErrorOut( hr, 'CreateFullScreenDisplay' );
    result := hr;
    exit;
  end;

  // Create a surface, and draw a bitmap resource on it.
  hr := FDisplay.CreateSurfaceFromBitmap( FAnimationSurface, PAnsiChar( '..\xmedia\animate.bmp' ) {MAKEINTRESOURCE( IDB_DIRECTX )}, SPRITE_DIAMETER * 5, SPRITE_DIAMETER * 6 );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromBitmap', hr );
    ErrorOut( hr, 'CreateSurfaceFromBitmap' );
    result := hr;
    exit;
  end;

  // Create a surface, and draw text to it.
  hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromText', hr );
    ErrorOut( hr, 'CreateSurfaceFromText' );
    result := hr;
    exit;
  end;

  // Set the color key for the logo sprite to black
  hr := FAnimationSurface.SetColorKey( 0 );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'SetColorKey', hr )
    ErrorOut( hr, 'SetColorKey' );
    result := hr;
    exit;
  end;

  // Init all the sprites.  All of these sprites look the same,
  // using the g_pDDSLogo surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
  begin
    // Set the sprite's position and velocity
    FSprite[ iSprite ].fPosX := random( SCREEN_WIDTH );
    FSprite[ iSprite ].fPosY := random( SCREEN_HEIGHT );

    FSprite[ iSprite ].fVelX := 500.0 * random / RAND_MAX - 250.0;
    FSprite[ iSprite ].fVelY := 500.0 * random / RAND_MAX - 250.0;

    FSprite[ iSprite ].lFrame := random( NUM_FRAMES ) mod NUM_FRAMES;
    FSprite[ iSprite ].fRotationTick := 0.0;
    FSprite[ iSprite ].fRotationSpeed := ( random( 50 + 25 ) mod 50 + 25 ) / 1000.0;
    if ( random( 2 ) mod 2 = 1 ) then
      FSprite[ iSprite ].bClockwise := False
    else
      FSprite[ iSprite ].bClockwise := True;
  end;

  // Precompute the source rects for g_pDDSAnimateSheet.  The source rects
  // are used during the blt of the dds to the backbuffer.
  for iFrame := 0 to NUM_FRAMES - 1 do
  begin
    FRctFrame[ iFrame ].top := ( iFrame div 5 ) * SPRITE_DIAMETER;
    FRctFrame[ iFrame ].left := ( iFrame mod 5 ) * SPRITE_DIAMETER;

    FRctFrame[ iFrame ].bottom := FRctFrame[ iFrame ].top + SPRITE_DIAMETER;
    FRctFrame[ iFrame ].right := FRctFrame[ iFrame ].left + SPRITE_DIAMETER;
  end;

  // Init a array of random values.  This array is used to create the
  // 'flocking' effect the seen in the sample.
  FRandIndex := 0;
  for iRand := 0 to NUM_RAND - 1 do
    FRandTable[ iRand ] := Random( NUM_RAND );

  FLastTick := GetTickCount64;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: ProcessNextFrame()
   Desc: Move the sprites, blt them to the back buffer, then
         flip or blt the back buffer to the primary buffer
-----------------------------------------------------------------------------}

function TFormMain.ProcessNextFrame : HRESULT;
var
  hr : HRESULT;
  dwCurrTick : DWord;
  dwTickDiff : DWord;
  iSprite : integer;
begin
  // Figure how much time has passed since the last time
  dwCurrTick := GetTickCount64;
  dwTickDiff := dwCurrTick - FLastTick;

  // Don't update if no time has passed
  if ( dwTickDiff = 0 ) then
  begin
    result := DD_OK;
    exit
  end;

  FLastTick := dwCurrTick;

  // Move the sprites according to how much time has passed
  for iSprite := 0 to NUM_SPRITES - 1 do
    UpdateSprite( FSprite[ iSprite ], dwTickDiff / 1000.0 );

  // Display the sprites on the screen
  hr := DisplayFrame;
  if ( hr <> DD_OK ) then
  begin
    if ( hr <> DDERR_SURFACELOST ) then
    begin
      FreeDirectDraw;
      //DXTRACE_ERR( 'DisplayFrame', hr );
      result := hr;
      exit;
    end;

    // The surfaces were lost so restore them
    RestoreSurfaces;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: RestoreSurfaces()
   Desc: Restore all the surfaces, and redraw the sprite surfaces.
-----------------------------------------------------------------------------}

function TFormMain.RestoreSurfaces : HRESULT;
var
  hr : HRESULT;
begin
  hr := FDisplay.GetDirectDraw.RestoreAllSurfaces;
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("RestoreAllSurfaces"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  hr := FTextSurface.DrawText( 0, HELPTEXT, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("DrawText"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  //hr := FAnimationSurface.DrawBitmap( MakeIntResource( IDB_DIRECTX ), SPRITE_DIAMETER, SPRITE_DIAMETER );
  hr := FAnimationSurface.DrawBitmap( PChar( '..\xmedia\animate.bmp' ), SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("DrawBitmap"), hr );
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: UpdateSprite()
   Desc: Move the sprite around and make it bounce based on how much time
         has passed
-----------------------------------------------------------------------------}

procedure TFormMain.UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
begin
    // Update the sprite position
  pSprite.fPosX := pSprite.fPosX + pSprite.fVelX * fTimeDelta;
  pSprite.fPosY := pSprite.fPosY + pSprite.fVelY * fTimeDelta;


  pSprite.fRotationTick := pSprite.fRotationTick + fTimeDelta;
  if ( pSprite.fRotationTick > pSprite.fRotationSpeed ) then
  begin
    // If it is, then either change the frame clockwise or counter-clockwise
    if ( pSprite.bClockwise ) then
    begin
      inc( pSprite.lFrame );
      pSprite.lFrame := pSprite.lFrame mod NUM_FRAMES;
    end
    else
    begin
      dec( pSprite.lFrame );
      if ( pSprite.lFrame < 0 ) then
        pSprite.lFrame := NUM_FRAMES - 1;
    end;

    pSprite.fRotationTick := 0;
  end;

  // Using the next element from the random arry,
  // randomize the velocity of the sprite
  if ( GetNextRand mod 100 < 2 ) then
  begin
    pSprite.fVelX := 500.0 * GetNextRand / RAND_MAX - 250.0;
    pSprite.fVelY := 500.0 * GetNextRand / RAND_MAX - 250.0;
  end;

  // Using the next element from the random arry,
  // randomize the rotational speed of the sprite
  if ( GetNextRand mod 100 < 5 ) then
    pSprite.fRotationSpeed := ( GetNextRand mod 50 + 5 ) / 1000.0;

  // Clip the position, and bounce if it hits the edge
  if ( pSprite.fPosX < 0.0 ) then
  begin
    pSprite.fPosX := 0;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosX >= SCREEN_WIDTH - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosX := SCREEN_WIDTH - 1 - SPRITE_DIAMETER;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosY < 0 ) then
  begin
    pSprite.fPosY := 0;
    pSprite.fVelY := -pSprite.fVelY;
  end;

  if ( pSprite.fPosY > SCREEN_HEIGHT - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosY := SCREEN_HEIGHT - 1 - SPRITE_DIAMETER;
    pSprite.fVelY := -pSprite.fVelY;
  end;
end;

{-----------------------------------------------------------------------------
   Name: GetNextRand()
   Desc: Gets the next element from the circular array of random values
-----------------------------------------------------------------------------}

function TFormMain.GetNextRand : LongInt;
var
  lRand : LongInt;
begin
  inc( FRandIndex );
  lRand := FRandTable[ FRandIndex ];
  FRandIndex := FRandIndex mod NUM_RAND;

  result := lRand;
end;

procedure TFormMain.Timer1Timer( Sender : TObject );
var
  iRand : Integer;
begin
  FRandIndex := 0;
  for iRand := 0 to NUM_RAND - 1 do
    FRandTable[ iRand ] := random( NUM_RAND );
end;

procedure TFormMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ( Key = VK_F12 ) or ( Key = VK_ESCAPE ) then
  begin
    Key := 0;
    Close;
  end;
end;

end.
