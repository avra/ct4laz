program FullScreenMode;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Full Screen Mode Example               }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : FullScreenMode.h,                     }
{                          FullScreenMode.cpp                    }
{ The original Pascal code is : FullScreenMode_vcl.pas           }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   FullScreenMode demonstrates the tasks required to initialize   }
{   and run a full-screen DirectDraw application.                  }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   The basic tasks to author a simple full-screen DirectDraw      }
{   application are as follows:                                    }
{                                                                  }
{   Initialize DirectDraw:                                         }
{    1. Register a window class and create a window.               }
{    2. Call DirectDrawCreateEx to create a DirectDraw object      }
{    3. Call SetCooperativeLevel to set the DirectDraw cooperative }
{       level to exclusive and full-screen.                        }
{    4. Call SetDisplayMode to set the display mode, for example   }
{       640x480x8.                                                 }
{    5. Call CreateSurface to create a flipable primary surface    }
{       with 1 back buffer.                                        }
{    6. Call GetAttachedSurface to obtain a pointer to the back    }
{       buffer.                                                    }
{    7. If the display mode was set to palettized color, a palette }
{       is needs to be created.  This sample displays a single     }
{       bitmap so it can read the bitmap palette info to read and  }
{       create a DirectDraw palette. After a palette               }
{       is created, call SetPalette to set the palette for the     }
{       primary surface.                                           }
{    8. Create an off-screen plain DirectDraw surface, and load    }
{       media content into it.                                     }
{       For example, this sample calls                             }
{       DDUtil_CreateSurfaceFromBitmap() to do just                }
{       this.                                                      }
{                                                                  }
{   When the app is idle, and it is not hidden or minimized then   }
{   render the next frame as follows:                              }
{    1. If movement or animation is involved in the app, then      }
{       calculate how much time has passed since the last time the }
{       frame was displayed.                                       }
{    2. Move or animate the app state based on how much time has   }
{       passed.                                                    }
{    3. Draw the current state into the backbuffer.                }
{    4. Call Flip to flip the contents of the backbuffer into the  }
{       primary surface.                                           }
{                                                                  }
{  If the user alt-tabs away from the app, then the DirectDraw     }
{  surface may be lost                                             }
{  (resulting in a DirectDraw call returning DDERR_SURFACELOST),   }
{  then handle it by:                                              }
{    1. Call RestoreAllSurfaces to have DirectDraw restore all the }
{       surfaces.                                                  }
{    2. Restoring a surface doesn't reload any content that        }
{       existed in the surface                                     }
{       prior to it being lost. So you must now redraw the         }
{       graphics the surfaces once held. For example, this sample  }
{       handles this by calling DDUtil_ReDrawBitmapOnDDS()         }
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  FullScreenModemw in 'MainForm.pas' {FormMain},
  DDUtil in '..\..\Common\DDUtil.pas';

begin
  Application.Initialize;
  Application.Title := 'Full Screen Mode Example';
  Application.CreateForm( TFormMain, FormMain );
  Application.Run;
end.
