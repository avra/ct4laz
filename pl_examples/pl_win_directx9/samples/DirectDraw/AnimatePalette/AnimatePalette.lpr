program AnimatePalette;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Animate Palette Example                  }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : AnimatePalette.h, AnimatePalette.cpp    }
{ The original Pascal code is : AnimatePalette_api.pas             }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   This sample demonstrates how to do DirectDraw palette          }
{   animatation when in full-screen mode.                          }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   For details on how to setup a full-screen DirectDraw app, see  }
{   the FullScreenMode  sample.                                    }
{                                                                  }
{   To animate the palette on a palettized DirectDraw surface, call}
{   IDirectDrawPalette::GetEntries to retrieve the palette colors. }
{   Then every frame (or as often as desired) alter this array as  }
{   needed, then set the new palette by first calling...           }
{   IDirectDraw::WaitForVerticalBlank( DDWAITVB_BLOCKBEGIN, NULL ) }
{   to synchronize the palette change to a vertical blank, then    }
{   call IDirectDrawPalette::SetEntries.                           }
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  AnimatePalettemw in 'MainForm.pas' {FormMain},
  DDUtil in '..\..\Common\DDUtil.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm( TFormMain, FormMain );
  Application.Run;
end.
