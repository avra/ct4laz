program RefreshRate;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Overlay Animate Example                  }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : RefreshRate.h,                          }
{                          RefreshRate.cpp                         }
{ The original Pascal code is : RefreshRate_vcl.pas                }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{    This example demonstrates basic usage of the IDirectDraw7::StartRefreshRate
    and IDirectDraw7::EvaluateMode methods. Together, these methods allow an
    application to explore what display modes and refresh rates the monitor
    connected to this display device is able to display, though a manual
    user-controlled process. The application will present the UI that asks
    the user if the current mode under test is being displayed correctly by
    the monitor.

    Applications should use these methods when they are interested in using
    higher refresh rates.    }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   The basic idea is that DirectDraw will setup a list of modes to be tested
    (based on the list the app passed in), and then sequentially test them
    under application control. The app starts the test process, and then
    calls IDirectDraw7::EvaluateMode continuously. DirectDraw will take care
    of settings the modes. All the app has to do is SetCooperativeLevel
    beforehand, and then handle surface loss and drawing the UI that asks the
    user if they can see the current mode under test. DirectDraw returns
    enough information from IDirectDraw7::EvalulateMode to allow the app to
    know when to do these things, and when to stop testing. The app can pass
    a flag to IDirectDraw7::EvaluateMode if the user happened to say they
    could see the mode corretly, which will cause DirectDraw to mark the mode
    as good and move on. DirectDraw may also decide that time as run out and
    give up on a certain mode.

    DirectDraw uses information at its disposal from any automated means to
    make the testing process as short as possible, and applications only need
    to test modes they are interested in.}
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}
uses
  Forms, Interfaces,
  RefreshRatemw in 'MainForm.pas' {FormMain};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Refresh Rate Example';
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
