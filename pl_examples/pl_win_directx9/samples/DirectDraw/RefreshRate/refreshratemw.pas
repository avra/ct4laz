unit RefreshRatemw;

{$MODE Delphi}

interface

uses
  Windows,
  Classes,
  StdCtrls,
  SysUtils,
  Controls,
  Forms,
  DirectDraw;

const
  MAX_DEVICES = 32;
  MAX_MODESIZES = 256;

type
  TDDrawDeviceInfo = packed record
    FGuid : TGUID;
    strDescription : string;
    strDriverName : string;
    dwModeCount : DWord;
    aModeSize : array[ 0..MAX_MODESIZES - 1 ] of TSize;
  end;

  TFormMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ButtonExit: TButton;
    DeviceListBox: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    TestModesListBox: TListBox;
    DisplayModesListBox: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ButtonExitClick(Sender: TObject);
  private
    FDD : IDIRECTDRAW;

    procedure ErrorOut(hRet : HRESULT; FuncName : string);
    procedure InitDialog;
    function UpdateModesListBox( DeviceIndex : integer ) : HResult;
  public
     //
    FDeviceInfo : array[ 0..MAX_DEVICES - 1 ] of TDDrawDeviceInfo;
    FDeviceCount : DWord;
  end;

function EnumDisplayModesCallback(const pddsd : TDDSurfaceDesc2; pContext: Pointer): HResult; stdcall;
function EnumModesCallback(const pddsd: TDDSurfaceDesc; pContext : Pointer) : HResult; stdcall;
function DDEnumCallbackEx(pGUID: PGUID; strDescription: PAnsiChar; strDriverName: PAnsiChar; pContext: Pointer; hm: HMONITOR): LongBool; stdcall;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}
procedure TFormMain.ErrorOut(hRet : HRESULT; FuncName : string);
var
  OutString : string;
begin
  OutString := FuncName + ': ' + #13 + DDErrorString(hRet);
  MessageBox(0, PChar(OutString), PChar(Caption), MB_OK or MB_ICONSTOP);
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormCreate
   Desc: Init variables
-----------------------------------------------------------------------------}
procedure TFormMain.FormCreate(Sender: TObject);
begin
  FDD := nil;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Start enumeration
-----------------------------------------------------------------------------}
procedure TFormMain.FormShow(Sender: TObject);
var
  hr : HRESULT;
begin
  hr := DirectDrawEnumerateEx( @DDEnumCallbackEx, Self, DDENUM_ATTACHEDSECONDARYDEVICES or
                                DDENUM_DETACHEDSECONDARYDEVICES or DDENUM_NONDISPLAYDEVICES);
  if hr <> DD_OK then
  begin
    ErrorOut(hr, 'DirectDrawEnumerateEx');
    Close;
  end;

  InitDialog;
end;

{-----------------------------------------------------------------------------
   Name: EnumDisplayModesCallback()
   Desc: For each mode enumerated, it adds it to the "All Modes" listbox.
-----------------------------------------------------------------------------}
function EnumDisplayModesCallback(const pddsd : TDDSurfaceDesc2; pContext: Pointer): HResult; stdcall;
var
  List : TListBox absolute pContext;
begin
  List.Items.Add( Format( '%dx%dx%d - %d Hz',[
                              pddsd.dwWidth,
                              pddsd.dwHeight,
                              pddsd.ddpfPixelFormat.dwRGBBitCount,
                              pddsd.dwRefreshRate] ) );

  result := DDENUMRET_OK;
end;

{-----------------------------------------------------------------------------
   Name: EnumModesCallback()
   Desc: Enumerates the available modes for the device from which
         EnumDisplayModes() was called.  It records the unique mode sizes in
         the g_aDevices[g_dwDeviceCount].aModeSize array
-----------------------------------------------------------------------------}
function EnumModesCallback(const pddsd: TDDSurfaceDesc; pContext : Pointer) : HResult; stdcall;
var
  i : DWORD;
  dwModeSizeX : DWORD;
  dwModeSizeY : DWORD;
  dwModeCount : DWORD ;
begin
  // For each mode, look through all previously found modes
  // to see if this mode has already been added to the list
  dwModeCount := FormMain.FDeviceInfo[ FormMain.FDeviceCount ].dwModeCount;
  for i := 0 to dwModeCount - 1 do
  begin
    dwModeSizeX := FormMain.FDeviceInfo[ FormMain.FDeviceCount ].aModeSize[i].cx;
    dwModeSizeY := FormMain.FDeviceInfo[ FormMain.FDeviceCount ].aModeSize[i].cy;

    if ( ( dwModeSizeX = pddsd.dwWidth ) and ( dwModeSizeY = pddsd.dwHeight ) ) then
    begin
      // If this mode has been added, then stop looking
      break;
    end;
  end;

  // If this mode was not in g_aDevices[g_dwDeviceCount].aModeSize[]
  // then added it.
  if( FormMain.FDeviceInfo[ FormMain.FDeviceCount ].dwModeCount = i  ) then
  begin
    FormMain.FDeviceInfo[ FormMain.FDeviceCount ].aModeSize[i].cx := pddsd.dwWidth;
    FormMain.FDeviceInfo[ FormMain.FDeviceCount ].aModeSize[i].cy := pddsd.dwHeight;

    // Increase the number of modes found for this device
    inc( FormMain.FDeviceInfo[ FormMain.FDeviceCount ].dwModeCount );
  end;

  result := DDENUMRET_OK;
end;

{-----------------------------------------------------------------------------
   Name: DDEnumCallbackEx()
   Desc: This callback gets the information for each device enumerated
-----------------------------------------------------------------------------}
function DDEnumCallbackEx(pGUID: PGUID; strDescription: PAnsiChar; strDriverName: PAnsiChar; pContext: Pointer; hm: HMONITOR): LongBool; stdcall;
var
  pDD : IDIRECTDRAW7;
  hr : HRESULT;
begin
  pDD := nil;

  // Create a DirectDraw object using the enumerated GUID
  hr := DirectDrawCreateEx( pGUID, pDD, IID_IDirectDraw7, nil );
  if( hr <> DD_OK ) then
  begin
    result := False;
    exit;
  end
  else
  begin
    if pGUID <> nil then
    begin
      // Add it to the global storage structure
      FormMain.FDeviceInfo[ FormMain.FDeviceCount ].FGuid := pGUID^;
    end
    else
    begin
      // Clear the guid from the global storage structure
      FillChar( FormMain.FDeviceInfo[ FormMain.FDeviceCount ].FGuid, SizeOf(TGUID), 0 );
    end;

    // Copy the description of the driver into the structure
    FormMain.FDeviceInfo[ FormMain.FDeviceCount ].strDescription := strDescription;
    FormMain.FDeviceInfo[ FormMain.FDeviceCount ].strDriverName :=  strDriverName;

    // Retrive the modes this device can support
    FormMain.FDeviceInfo[ FormMain.FDeviceCount ].dwModeCount := 1;
    hr := pDD.EnumDisplayModes( 0, nil, nil, @EnumModesCallback );
    if( hr <> DD_OK ) then
    begin
      result := False;
      exit;
    end;

    // Increase the counter for the number of devices found
    inc( FormMain.FDeviceCount );

    // Finished with the DirectDraw object, so release it
    pDD := nil;
  end;
  result := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.ButtonExitClick
   Desc: Close form
-----------------------------------------------------------------------------}
procedure TFormMain.ButtonExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFormMain.InitDialog;
var
  i : integer;
  hr : HRESULT;
begin
  // Set the icon for this dialog.
  //PostMessage( this, WM_SETICON, ICON_BIG,   LPARAM( h_Icon ) );  // Set big icon
  //PostMessage( this, WM_SETICON, ICON_SMALL, LPARAM( h_Icon ) );  // Set small icon

  // Show all available DirectDraw devices in the listbox
  for i := 0 to  FDeviceCount - 1 do
  begin
    DeviceListBox.Items.AddObject( FDeviceInfo[i].strDescription, TObject( i ) );
  end;

  // Select the first device by default
  DeviceListBox.Selected[0] := True;

  hr := UpdateModesListBox( 0 );
  if( hr <> S_OK ) then
  begin
    MessageBox( Handle, 'Error enumerating DirectDraw modes.' + #13#10 + 'Sample will now exit.', 'DirectDraw Refresh Rete Example', MB_OK or MB_ICONERROR );
    Application.Terminate;
  end;

  SetFocus;
end;

function TFormMain.UpdateModesListBox( DeviceIndex: integer ): HResult;
var
  pDD : IDIRECTDRAW7;
  hr : HRESULT;
  i : DWord;
begin
  pDD := nil;

  TestModesListBox.Items.Clear;

  // Update the "modes to test" list box based on the display device selected
  for i := 0 to FDeviceInfo[ DeviceIndex ].dwModeCount - 1 do
  begin
    // Make a string based on the this mode's size and
    // Add it to the list box
    TestModesListBox.Items.AddObject(
    Format( '%d x %d',[ FDeviceInfo[ DeviceIndex ].aModeSize[ i ].cx, FDeviceInfo[ DeviceIndex ].aModeSize[ i ].cy ] )
    , TObject( i ) );
  end;

  // Create a DirectDraw device based using the selected device guid
  hr := DirectDrawCreateEx( @FDeviceInfo[ DeviceIndex ].FGuid, pDD, IID_IDirectDraw7, nil );
  if( hr = DD_OK ) then
  begin
    DisplayModesListBox.Items.Clear;
    // Enumerate and display all supported modes along
    // with supported bit depth, and refresh rates
    // in the "All Modes" listbox
    hr := pDD.EnumDisplayModes( DDEDM_REFRESHRATES, nil, DisplayModesListBox, @EnumDisplayModesCallback );

    // Release this device
    pDD := nil;
  end;

  result := hr;
end;

end.
