unit FullScreenDialogmw;

{$MODE Delphi}

interface

uses
  windows,
  Messages,
  SysUtils,
  Forms,
  Classes,
  ExtCtrls,
  DirectDraw, showdialog,
  DDUtil, Controls;

const
  SCREEN_WIDTH = 640;
  SCREEN_HEIGHT = 480;
  SCREEN_BPP = 8;

  SPRITE_DIAMETER = 48;
  NUM_SPRITES = 25;

  HELPTEXT : PChar = 'Press Escape to quit.  Press F1 to bring up the dialog.';

type
  PSpriteRecord = ^TSpriteRecord;
  TSpriteRecord = packed record
    fPosX : single;
    fPosY : single;
    fVelX : single;
    fVelY : single;
  end;

  { TFormMain }

  TFormMain = class( TForm )
    procedure FormCreate( Sender : TObject );
    procedure FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
    procedure FormActivate( Sender : TObject );
    procedure FormDeactivate( Sender : TObject );
    procedure FormClose( Sender : TObject; var Action : TCloseAction );
    procedure FormShow( Sender : TObject );
  private
    FLastTick : DWORD;
    FActive : Boolean;
    FDisplay : TDisplay;
    FLogoSurface : TSurface;
    FTextSurface : TSurface;
    FSprite : array[ 0..NUM_SPRITES - 1 ] of TSpriteRecord;
    procedure FormOnIdle( Sender : TObject; var Done : Boolean );
    procedure ErrorOut( hRet : HRESULT; FuncName : string );
    procedure FreeDirectDraw;
    procedure UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
    function RestoreSurfaces : HRESULT;
    function ProcessNextFrame : HRESULT;
    function InitDirectDraw : HRESULT;
  public
    function DisplayFrame : HRESULT;
  end;

var
  FormMain : TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}

procedure TFormMain.ErrorOut( hRet : HRESULT; FuncName : string );
var
  ErrorMessage : string;
begin
  ErrorMessage := FuncName + ': ' + #13 + DDErrorString( hRet );
  MessageBox( Handle, PChar( ErrorMessage ), PChar( Caption ), MB_ICONERROR or MB_OK );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.Create
   Desc: Initialize the window
-----------------------------------------------------------------------------}

procedure TFormMain.FormCreate( Sender : TObject );
begin
  // Maximize the window
  Left := 0;
  Top := 0;
  Width := GetSystemMetrics( SM_CXSCREEN );
  Height := GetSystemMetrics( SM_CYSCREEN );
  Application.OnIdle := FormOnIdle;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormClose
   Desc: Cleanup
-----------------------------------------------------------------------------}

procedure TFormMain.FormClose( Sender : TObject; var Action : TCloseAction );
begin
  FreeDirectDraw;
  Application.Minimize;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormKeyUp
   Desc: React to keystrokes
-----------------------------------------------------------------------------}

procedure TFormMain.FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
begin
  case Key of
    VK_F12, VK_ESCAPE :
      begin
        Key := 0;
        Close;
      end;
    VK_F1 :
      begin
      //  MessageBeep(DWORD(-1));
        SampleDlg.Show;
        Key := 0;
      end;

  end;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormActivate
   Desc: Toggle active-flag
-----------------------------------------------------------------------------}

procedure TFormMain.FormActivate( Sender : TObject );
begin
  FActive := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormDeactivate
   Desc: Toggle active-flag
----------------------------------------------------------------------------}

procedure TFormMain.FormDeactivate( Sender : TObject );
begin
//  FActive := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormOnIdle
   Desc: Update frame and flip pages
-----------------------------------------------------------------------------}

procedure TFormMain.FormOnIdle( Sender : TObject; var Done : Boolean );
var
  hr : HRESULT;
begin
  if FActive then
  begin
    hr := ProcessNextFrame;
    if hr <> DD_OK then
    begin
      ErrorOut( hr, 'ProcessNextFrame' );
      Close;
    end;
  end;
  Done := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Initialize Direct Draw
-----------------------------------------------------------------------------}

procedure TFormMain.FormShow( Sender : TObject );
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if hr <> DD_OK then
  begin
    ErrorOut( hr, 'ProcessNextFrame' );
    Application.Terminate;
  end;
end;

{-----------------------------------------------------------------------------
   Name: DisplayFrame()
   Desc: Blts a the sprites to the back buffer, then it blts or flips the
         back buffer onto the primary buffer.
-----------------------------------------------------------------------------}

function TFormMain.DisplayFrame : HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
  pddsBackBuffer, pddsFrontBuffer : IDIRECTDRAWSURFACE7;
begin
  // Fill the back buffer with black, ignoring errors until the flip
  FDisplay.Clear( 0 );

  // Blt the help text on the backbuffer, ignoring errors until the flip
  FDisplay.Blt( 10, 10, FTextSurface, nil );

  // Blt all the sprites onto the back buffer using color keying,
  // ignoring errors until the flip. Note that all of these sprites
  // use the same DirectDraw surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
    FDisplay.Blt( Round( FSprite[ iSprite ].fPosX ), Round( FSprite[ iSprite ].fPosY ), FLogoSurface, nil );


  // Updating the primary buffer with a blt
  pddsFrontBuffer := FDisplay.GetFrontBuffer;
  pddsBackBuffer := FDisplay.GetBackBuffer;
  hr := pddsFrontBuffer.Blt( nil, pddsBackBuffer, nil, DDBLT_WAIT, nil );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;
  
  pddsFrontBuffer := nil;
  pddsBackBuffer := nil;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
  Name: FreeDirectDraw
  Desc: Release all the DirectDraw objects
-----------------------------------------------------------------------------}

procedure TFormMain.FreeDirectDraw;
begin
  FLogoSurface.Free;
  FLogoSurface := nil;
  FTextSurface.Free;
  FTextSurface := nil;
  FDisplay.Free;
  FDisplay := nil;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDraw()
   Desc: Create the DirectDraw object, and init the surfaces
-----------------------------------------------------------------------------}

function TFormMain.InitDirectDraw : HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
  ddcaps : TDDCAPS;
begin
  fillbyte( FSprite, SizeOf( TSpriteRecord ) * NUM_SPRITES,0 );

  // Initialize all the surfaces we need
  FDisplay := TDisplay.Create;

  hr := FDisplay.CreateFullScreenDisplay( Handle, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP );
  if ( hr <> DD_OK ) then
  begin
    ErrorOut( hr, 'CreateFullScreenDisplay' );
    result := hr;
    exit;
  end;

  // Check if the device supports DDCAPS2_CANRENDERWINDOWED.
  // If it does, then it supports GDI writing directly to the primary surface
  // Otherwise, to do GDI displaying on these cards, you you must create a
  // bitmap of the GDI window and BitBlt the bitmap to the backbuffer
  // then flip as normal. However, this sample does not show this.
  FillChar( ddcaps, SizeOf( ddcaps ), 0 );
  ddcaps.dwSize := SizeOf( ddcaps );
  FDisplay.GetDirectDraw.GetCaps( @ddcaps, nil );
  if ( ( ddcaps.dwCaps2 and DDCAPS2_CANRENDERWINDOWED ) = 0 ) then
  begin
    MessageBox( Handle, 'This display card can not render GDI.', PChar( Caption ), MB_ICONERROR or MB_OK );
   // result := E_FAIL;
    exit;
  end;

  // Create a clipper so DirectDraw will not blt over the GDI dialog
  hr := FDisplay.InitClipper;
  if ( hr <> DD_OK ) then
  begin
    ErrorOut( hr, 'Failed on call to g_pDisplay.InitClipper.' );
    result := hr;
    exit;
  end;

  // Create a surface, and draw a bitmap resource on it.
  hr := FDisplay.CreateSurfaceFromBitmap( FLogoSurface, PAnsiChar( '..\xmedia\directx.bmp' ) {MAKEINTRESOURCE( IDB_DIRECTX )}, SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromBitmap', hr );
    ErrorOut( hr, 'CreateSurfaceFromBitmap' );
    result := hr;
    exit;
  end;

  // Create a surface, and draw text to it.
  hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromText', hr );
    ErrorOut( hr, 'CreateSurfaceFromText' );
    result := hr;
    exit;
  end;

  // Set the color key for the logo sprite to black
  hr := FLogoSurface.SetColorKey( 0 );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'SetColorKey', hr )
    ErrorOut( hr, 'SetColorKey' );
    result := hr;
    exit;
  end;

  // Init all the sprites.  All of these sprites look the same,
  // using the FDDSLogo surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
  begin
    // Set the sprite's position and velocity
    FSprite[ iSprite ].fPosX := random( SCREEN_WIDTH );
    FSprite[ iSprite ].fPosY := random( SCREEN_HEIGHT );

    FSprite[ iSprite ].fVelX := 500.0 * random / RAND_MAX - 250.0;
    FSprite[ iSprite ].fVelY := 500.0 * random / RAND_MAX - 250.0;
  end;

  FDisplay.GetDirectDraw.FlipToGDISurface;
  SampleDlg.Show;
  FLastTick := GetTickCount64;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: ProcessNextFrame()
   Desc: Move the sprites, blt them to the back buffer, then
         flip or blt the back buffer to the primary buffer
-----------------------------------------------------------------------------}

function TFormMain.ProcessNextFrame : HRESULT;
var
  hr : HRESULT;
  dwCurrTick : DWord;
  dwTickDiff : DWord;
  iSprite : integer;
begin
  // Figure how much time has passed since the last time
  dwCurrTick := GetTickCount64;
  dwTickDiff := dwCurrTick - FLastTick;

  // Don't update if no time has passed
  if ( dwTickDiff = 0 ) then
  begin
    result := DD_OK;
    exit
  end;

  FLastTick := dwCurrTick;

  // Move the sprites according to how much time has passed
  for iSprite := 0 to NUM_SPRITES - 1 do
    UpdateSprite( FSprite[ iSprite ], dwTickDiff / 1000.0 );

  // Display the sprites on the screen
  hr := DisplayFrame;
  if ( hr <> DD_OK ) then
  begin
    if ( hr <> DDERR_SURFACELOST ) then
    begin
      FreeDirectDraw;
      //DXTRACE_ERR( 'DisplayFrame', hr );
      result := hr;
      exit;
    end;

    // The surfaces were lost so restore them
    RestoreSurfaces;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: RestoreSurfaces()
   Desc: Restore all the surfaces, and redraw the sprite surfaces.
-----------------------------------------------------------------------------}

function TFormMain.RestoreSurfaces : HRESULT;
var
  hr : HRESULT;
begin
  hr := FDisplay.GetDirectDraw.RestoreAllSurfaces;
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("RestoreAllSurfaces"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  hr := FLogoSurface.DrawBitmap( PChar( '..\xmedia\directx.bmp' ), SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("DrawBitmap"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  hr := FTextSurface.DrawText( 0, HELPTEXT, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: UpdateSprite()
   Desc: Move the sprite around and make it bounce based on how much time
         has passed
-----------------------------------------------------------------------------}

procedure TFormMain.UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
begin
// Update the sprite position
  pSprite.fPosX := pSprite.fPosX + pSprite.fVelX * fTimeDelta;
  pSprite.fPosY := pSprite.fPosY + pSprite.fVelY * fTimeDelta;

  // Clip the position, and bounce if it hits the edge
  if ( pSprite.fPosX < 0.0 ) then
  begin
    pSprite.fPosX := 0;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosX >= SCREEN_WIDTH - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosX := SCREEN_WIDTH - 1 - SPRITE_DIAMETER;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosY < 0 ) then
  begin
    pSprite.fPosY := 0;
    pSprite.fVelY := -pSprite.fVelY;
  end;

  if ( pSprite.fPosY > SCREEN_HEIGHT - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosY := SCREEN_HEIGHT - 1 - SPRITE_DIAMETER;
    pSprite.fVelY := -pSprite.fVelY;
  end;
end;


end.
