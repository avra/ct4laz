program FullScreenDialog;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Full Screen Dialog Example               }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : FullScreenDialog.h,                     }
{                          FullScreenDialog.cpp                    }
{ The original Pascal code is : FullScreenDialog_vcl.pas           }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   FullScreenDialog demonstrates how to display a GDI dialog      }
{   while using DirectDraw in full-screen exclusive mode.          }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   For details on how to setup a full-screen DirectDraw app, see  }
{   the FullScreenMode sample.                                     }
{                                                                  }
{   If the display device supports DDCAPS2_CANRENDERWINDOWED then  }
{   to make GDI write to a DirectDraw surface, call                }
{   IDirectDraw::FlipToGDISurface, then create a clipper object    }
{   which GDI uses when drawing.  To display a dialog, then simply }
{   create and show it as normal.                                  }
{                                                                  }
{   If the display device does not support                         }
{   DDCAPS2_CANRENDERWINDOWED (some secondary                      }
{   graphics cards are like this) then the card does not support   }
{   FlipToGDISurface.                                              }
{   So it is necessary instead to have GDI make a bitmap of the    }
{   dialog to be drawn, then blt this bitmap to the back buffer.   }
{   However, this sample does not show how to do this technique,   }
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  FullScreenDialogmw,
  DDUtil,
  showdialog;

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TSampleDlg, SampleDlg);
  Application.Run;
end.
