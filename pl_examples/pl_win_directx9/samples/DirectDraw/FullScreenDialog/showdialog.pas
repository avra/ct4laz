unit showdialog;

{$MODE Delphi}

interface

uses
  windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  ExtCtrls,
  Buttons;

type
  TSampleDlg = class( TForm )
    RadioGroup1 : TRadioGroup;
    ComboBox1 : TComboBox;
    Edit1 : TEdit;
    Label1 : TLabel;
    BitBtn1 : TBitBtn;
    BitBtn2 : TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SampleDlg : TSampleDlg;

implementation

uses FullScreenDialogmw;

{$R *.lfm}

procedure TSampleDlg.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

end.
