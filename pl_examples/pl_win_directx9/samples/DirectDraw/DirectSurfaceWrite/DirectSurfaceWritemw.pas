unit DirectSurfaceWritemw;

{$MODE Delphi}

interface

uses
  Windows,
  Messages,
  SysUtils,
  Forms,
  Classes,
  ExtCtrls,
  DirectDraw,
  DDUtil;

const
  SCREEN_WIDTH = 640;
  SCREEN_HEIGHT = 480;

  SCREEN_BPP = 16;

  SPRITE_DIAMETER = 250;
  NUM_SPRITES = 5;

  HELPTEXT : PChar = 'Press Escape to quit.';

type
  PSpriteRecord = ^TSpriteRecord;
  TSpriteRecord = packed record
    fPosX : single;
    fPosY : single;
    fVelX : single;
    fVelY : single;
  end;

  TFormMain = class( TForm )
    procedure FormCreate( Sender : TObject );
    procedure FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
    procedure FormActivate( Sender : TObject );
    procedure FormDeactivate( Sender : TObject );
    procedure FormClose( Sender : TObject; var Action : TCloseAction );
    procedure FormShow( Sender : TObject );
  private
    FLastTick : DWORD;
    FActive : Boolean;
    FDisplay : TDisplay;
    FSpriteSurface : TSurface;
    FTextSurface : TSurface;
    FSprite : array[ 0..NUM_SPRITES - 1 ] of TSpriteRecord;
    procedure FormOnIdle( Sender : TObject; var Done : Boolean );
    procedure FormSetCursor( var aMsg : TMessage ); message WM_SETCURSOR;
    procedure ErrorOut( hRet : HRESULT; FuncName : string );
    procedure FreeDirectDraw;
    procedure UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
    function DisplayFrame : HRESULT;
    function RestoreSurfaces : HRESULT;
    function ProcessNextFrame : HRESULT;
    function InitDirectDraw : HRESULT;
    function DrawSprite : HRESULT;
  end;

var
  FormMain : TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}

procedure TFormMain.ErrorOut( hRet : HRESULT; FuncName : string );
var
  ErrorMessage : string;
begin
  ErrorMessage := FuncName + ': ' + #13 + DDErrorString( hRet );
  MessageBox( Handle, PChar( ErrorMessage ), PChar( Caption ), MB_ICONERROR or MB_OK );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.Create
   Desc: Initialize the window
-----------------------------------------------------------------------------}

procedure TFormMain.FormCreate( Sender : TObject );
begin
  Randomize;
  // Maximize the window
  Left := 0;
  Top := 0;
  Width := GetSystemMetrics( SM_CXSCREEN );
  Height := GetSystemMetrics( SM_CYSCREEN );
  Application.OnIdle := FormOnIdle;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormClose
   Desc: Cleanup
-----------------------------------------------------------------------------}

procedure TFormMain.FormClose( Sender : TObject; var Action : TCloseAction );
begin
  FreeDirectDraw;
  Application.Minimize;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormKeyUp
   Desc: React to keystrokes
-----------------------------------------------------------------------------}

procedure TFormMain.FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
begin
  if ( Key = VK_F12 ) or ( Key = VK_ESCAPE ) then
  begin
    Key := 0;
    Close;
  end;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormActivate
   Desc: Toggle active-flag
-----------------------------------------------------------------------------}

procedure TFormMain.FormActivate( Sender : TObject );
begin
  FActive := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormDeactivate
   Desc: Toggle active-flag
----------------------------------------------------------------------------}

procedure TFormMain.FormDeactivate( Sender : TObject );
begin
  FActive := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormSetCursor
   Desc: Turn off cursor
-----------------------------------------------------------------------------}

procedure TFormMain.FormSetCursor( var aMsg : TMessage );
begin
  SetCursor( 0 );
  aMsg.Result := 1;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormOnIdle
   Desc: Update frame and flip pages
-----------------------------------------------------------------------------}

procedure TFormMain.FormOnIdle( Sender : TObject; var Done : Boolean );
var
  hr : HRESULT;
begin
  if FActive then
  begin
    hr := ProcessNextFrame;
    if hr <> DD_OK then
    begin
      ErrorOut( hr, 'ProcessNextFrame' );
      Close;
    end;
  end;
  Done := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Initialize Direct Draw
-----------------------------------------------------------------------------}

procedure TFormMain.FormShow( Sender : TObject );
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if hr <> DD_OK then
  begin
    ErrorOut( hr, 'ProcessNextFrame' );
    Application.Terminate;
  end;
end;

{-----------------------------------------------------------------------------
   Name: DisplayFrame()
   Desc: Blts a the sprites to the back buffer, then it blts or flips the
         back buffer onto the primary buffer.
-----------------------------------------------------------------------------}

function TFormMain.DisplayFrame : HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
begin
  if FDisplay = nil then
  begin
    result := DD_OK;
    exit;
  end;

  // Fill the back buffer with black, ignoring errors until the flip
  FDisplay.Clear( 0 );

  // Blt the help text on the backbuffer, ignoring errors until the flip
  FDisplay.Blt( 10, 10, FTextSurface, nil );

  // Blt all the sprites onto the back buffer using color keying,
  // ignoring errors until the flip. Note that all of these sprites
  // use the same DirectDraw surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
    FDisplay.Blt( Round( FSprite[ iSprite ].fPosX ), Round( FSprite[ iSprite ].fPosY ), FSpriteSurface, nil );


  // Flip or blt the back buffer onto the primary buffer
  hr := FDisplay.Flip;
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
  Name: FreeDirectDraw
  Desc: Release all the DirectDraw objects
-----------------------------------------------------------------------------}

procedure TFormMain.FreeDirectDraw;
begin
  FSpriteSurface.Free;
  FSpriteSurface := nil;
  FTextSurface.Free;
  FTextSurface := nil;
  FDisplay.Free;
  FDisplay := nil;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDraw()
   Desc: Create the DirectDraw object, and init the surfaces
-----------------------------------------------------------------------------}

function TFormMain.InitDirectDraw : HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
begin
  FillChar( FSprite, SizeOf( TSpriteRecord ) * NUM_SPRITES, 0 );

  // Initialize all the surfaces we need
  FDisplay := TDisplay.Create;

  hr := FDisplay.CreateFullScreenDisplay(Handle, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP);
  if ( hr <> DD_OK ) then
  begin
    ErrorOut(hr, 'This display card does not support 640x480x8.');
    Result := hr;
    exit;
  end;

  // Create a surface, and draw a bitmap resource on it.
  hr := FDisplay.CreateSurface( FSpriteSurface, SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurface', hr )
    result := hr;
    exit
  end;

  hr := DrawSprite;
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'DrawSprite', hr )
    result := hr;
    exit
  end;

  // Create a surface, and draw text to it.
  hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromText', hr );
    ErrorOut( hr, 'CreateSurfaceFromText' );
    result := hr;
    exit;
  end;

  // Init all the sprites.  All of these sprites look the same,
  // using the FDDSLogo surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
  begin
    // Set the sprite's position and velocity
    FSprite[ iSprite ].fPosX := random( SCREEN_WIDTH );
    FSprite[ iSprite ].fPosY := random( SCREEN_HEIGHT );

    FSprite[ iSprite ].fVelX := 500.0 * random {/ RAND_MAX} - 250.0;
    FSprite[ iSprite ].fVelY := 500.0 * random {/ RAND_MAX} - 250.0;
  end;

  FLastTick := GetTickCount64;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: ProcessNextFrame()
   Desc: Move the sprites, blt them to the back buffer, then
         flip or blt the back buffer to the primary buffer
-----------------------------------------------------------------------------}

function TFormMain.ProcessNextFrame : HRESULT;
var
  hr : HRESULT;
  dwCurrTick : DWord;
  dwTickDiff : DWord;
  iSprite : integer;
begin
  // Figure how much time has passed since the last time
  dwCurrTick := GetTickCount64;
  dwTickDiff := dwCurrTick - FLastTick;

  // Don't update if no time has passed
  if ( dwTickDiff = 0 ) then
  begin
    result := DD_OK;
    exit
  end;

  FLastTick := dwCurrTick;

  // Move the sprites according to how much time has passed
  for iSprite := 0 to NUM_SPRITES - 1 do
    UpdateSprite( FSprite[ iSprite ], dwTickDiff / 1000.0 );

  // Display the sprites on the screen
  hr := DisplayFrame;
  if ( hr <> DD_OK ) then
  begin
    if ( hr <> DDERR_SURFACELOST ) then
    begin
      FreeDirectDraw;
      //DXTRACE_ERR( 'DisplayFrame', hr );
      result := hr;
      exit;
    end;

    // The surfaces were lost so restore them
    RestoreSurfaces;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: RestoreSurfaces()
   Desc: Restore all the surfaces, and redraw the sprite surfaces.
-----------------------------------------------------------------------------}

function TFormMain.RestoreSurfaces : HRESULT;
var
  hr : HRESULT;
begin
  hr := FDisplay.GetDirectDraw.RestoreAllSurfaces;
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("RestoreAllSurfaces"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  hr := FTextSurface.DrawText( 0, HELPTEXT, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("DrawText"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  hr := DrawSprite;
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: UpdateSprite()
   Desc: Move the sprite around and make it bounce based on how much time
         has passed
-----------------------------------------------------------------------------}

procedure TFormMain.UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
begin
// Update the sprite position
  pSprite.fPosX := pSprite.fPosX + pSprite.fVelX * fTimeDelta;
  pSprite.fPosY := pSprite.fPosY + pSprite.fVelY * fTimeDelta;

  // Clip the position, and bounce if it hits the edge
  if ( pSprite.fPosX < 0.0 ) then
  begin
    pSprite.fPosX := 0;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosX >= SCREEN_WIDTH - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosX := SCREEN_WIDTH - 1 - SPRITE_DIAMETER;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosY < 0 ) then
  begin
    pSprite.fPosY := 0;
    pSprite.fVelY := -pSprite.fVelY;
  end;

  if ( pSprite.fPosY > SCREEN_HEIGHT - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosY := SCREEN_HEIGHT - 1 - SPRITE_DIAMETER;
    pSprite.fVelY := -pSprite.fVelY;
  end;
end;

function TFormMain.DrawSprite : HRESULT;
var
  ddsd : TDDSURFACEDESC2;
  hr : HRESULT;
  dwShift, dwBits, dwRed, dwGreen, dwBlue : DWORD;
  fPercentX, fPercentY, fPercentXY : Single;
  pDDS : IDIRECTDRAWSURFACE7;
  pDDSColor : PWORD;
  iY, iX : DWord;
begin
  pDDS := FSpriteSurface.GetDDrawSurface;

  FillChar( ddsd, SizeOf( ddsd ), 0 ); //ZeroMemory( @ddsd,sizeof(ddsd) );
  ddsd.dwSize := SizeOf( ddsd );

  // Lock the surface to directly write to the surface memory
  Result:= pDDS.Lock( nil, ddsd, DDLOCK_WAIT, 0 );
  if Result=0 then Exit;

  // Get a pointer into the memory starting at ddsd.lpSurface.  Cast this pointer to a
  // 16-bit WORD since we are in 16 bit color, so each pixel has 16 bits of color information.
  pDDSColor := PWord(ddsd.lpSurface);

  for iY := 0 to ddsd.dwHeight - 1 do
  begin
    for iX := 0 to ddsd.dwWidth - 1 do
    begin
      // Figure out the red component as a function of the Y location of the pixel
      FSpriteSurface.GetBitMaskInfo(ddsd.ddpfPixelFormat.dwRBitMask, dwShift, dwBits);
      fPercentY := abs( ddsd.dwHeight div 2 - iY ) / ( ( ddsd.dwHeight div 2 ) + 0.25 );
      if ( fPercentY > 1.00 ) then
        fPercentY := 1.00;
      dwRed := Trunc((ddsd.ddpfPixelFormat.dwRBitMask shr dwShift) * fPercentY) shl dwShift;

      // Figure out the green component as a function of the X location of the pixel
      FSpriteSurface.GetBitMaskInfo( ddsd.ddpfPixelFormat.dwGBitMask, dwShift, dwBits );
      fPercentX := abs( ddsd.dwWidth div 2 - iX ) / ( ( ddsd.dwWidth div 2 ) + 0.25 );
      if ( fPercentX > 1.00 ) then
        fPercentX := 1.00;
      dwGreen := Round( (ddsd.ddpfPixelFormat.dwGBitMask shr dwShift) * fPercentX) shl dwShift;

      // Figure out the blue component as a function of the X and Y location of the pixel
      FSpriteSurface.GetBitMaskInfo( ddsd.ddpfPixelFormat.dwBBitMask, dwShift, dwBits );
      fPercentX := abs( ddsd.dwWidth / 2 - iX ) / ( ddsd.dwWidth / 4 );
      fPercentX := 1.0 - fPercentX * fPercentX;
      fPercentY := abs( ddsd.dwHeight / 2 - iY ) / ( ddsd.dwHeight / 4 );
      fPercentY := 1.0 - fPercentY * fPercentY;
      fPercentXY := fPercentX + fPercentY;
      if ( fPercentXY > 1.0 ) then
        fPercentXY := 1.0;
      if ( fPercentXY < 0.0 ) then
        fPercentXY := 0.0;
      dwBlue := Trunc((ddsd.ddpfPixelFormat.dwBBitMask shr dwShift) * fPercentXY ) shl dwShift;

      // Make the dwDDSColor by combining all the color components
      pDDSColor^ := Word( dwRed or dwGreen or dwBlue );

      // Advance the surface pointer by 16 bits (one pixel)
      Inc(pDDSColor);
    end;

    // Multiply ddsd.lPitch by iY to figure out offset needed to access
    // the next scan line on the surface.
    pDDSColor := PWord(LongWord(ddsd.lpSurface) + (iY + 1)*LongWord(ddsd.lPitch));
  end;

  // Unlock the surface
  pDDS.Unlock( nil );

  result := DD_OK;
end;

end.
