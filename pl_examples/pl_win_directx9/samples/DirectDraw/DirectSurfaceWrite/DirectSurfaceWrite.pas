program DirectSurfaceWrite;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Direct Draw Enumeration Example          }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : DirectSurfaceWrite.h,                   }
{                          DirectSurfaceWrite.cpp                  }
{ The original Pascal code is : DirectSurfaceWrite_vcl.pas         }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   This sample demonstrates how to animate sprites using          }
{   DirectDraw.  The samples runs in full-screen mode.  Pressing   }
{   any key will exit the sample.                                  }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   For details on how to setup a full-screen DirectDraw app, see  }
{   the FullScreenMode sample.                                     }
{                                                                  }
{   To write directly on a DirectDraw surface first call           }
{   IDirectDrawSurface::Lock to obtain a pointer directly into the }
{   memory of the DirectDraw surface.  While the surface is locked,}
{   the surface can not be blted or flipped onto other surfaces.   }
{   The surface's pixel format will tell you the data format the   }
{   surface stores pixels in.  Be sure to advance the surface      }
{   pointer by the surface pitch instead of the surface            }
{   width, since the surface may be wider than its width.          }
{   After the drawing is finished call IDirectDrawSurface::Unlock  }
{   to allow the surface to blt to other surfaces.  See            }
             {   DrawSprite() in this sample for an example of how this is done.}
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  DirectSurfaceWritemw,
  DDUtil;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm( TFormMain, FormMain );
  Application.Run;
end.
