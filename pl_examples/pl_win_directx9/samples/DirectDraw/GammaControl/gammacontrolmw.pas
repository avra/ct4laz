unit GammaControlmw;

{$MODE Delphi}

interface

uses
  Windows,
  Messages,
  SysUtils,
  Forms,
  Classes,
  ExtCtrls,
  Controls,
  DirectDraw,
  DDUtil;

const
  SCREEN_WIDTH = 640;
  SCREEN_HEIGHT = 480;
  SCREEN_BPP = 32;

  NUM_COLOR_BARS = 63;

  HELPTEXT : PChar = 'Adjust the gamma level by clicking either mouse button. Press Escape to quit.';
  GAMMATEXT : PChar = 'Displaying with linear gamma ramp between 0 and %0.5d';

type
  TFormMain = class( TForm )
    procedure FormCreate( Sender : TObject );
    procedure FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
    procedure FormActivate( Sender : TObject );
    procedure FormDeactivate( Sender : TObject );
    procedure FormClose( Sender : TObject; var Action : TCloseAction );
    procedure FormShow( Sender : TObject );
    procedure FormMouseDown( Sender : TObject; Button : TMouseButton;
      Shift : TShiftState; X, Y : Integer );
  private
    FActive : Boolean;
    FDisplay : TDisplay;
    FGammaTextSurface : TSurface;
    FTextSurface : TSurface;
    FGammaControl : IDIRECTDRAWGAMMACONTROL;
    FGammaRamp : LongInt;
    procedure FormOnIdle( Sender : TObject; var Done : Boolean );
    procedure FormSetCursor( var aMsg : TMessage ); message WM_SETCURSOR;
    procedure ErrorOut( hRet : HRESULT; FuncName : string );
    procedure FreeDirectDraw;
    function UpdateGammaRamp : HRESULT;
    function DisplayFrame : HRESULT;
    function RestoreSurfaces : HRESULT;
    function ProcessNextFrame : HRESULT;
    function InitDirectDraw : HRESULT;
    function DrawGammaText : HResult;
    function HasGammaSupport : Boolean;
  end;

var
  FormMain : TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}

procedure TFormMain.ErrorOut( hRet : HRESULT; FuncName : string );
var
  ErrorMessage : string;
begin
  ErrorMessage := FuncName + ': ' + #13 + DDErrorString( hRet );
  MessageBox( Handle, PChar( ErrorMessage ), PChar( Caption ), MB_ICONERROR or MB_OK );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.Create
   Desc: Initialize the window
-----------------------------------------------------------------------------}

procedure TFormMain.FormCreate( Sender : TObject );
begin
  // Maximize the window
  Left := 0;
  Top := 0;
  Width := GetSystemMetrics( SM_CXSCREEN );
  Height := GetSystemMetrics( SM_CYSCREEN );
  FGammaControl := nil;
  FGammaRamp := 256;
  Application.OnIdle := FormOnIdle;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormClose
   Desc: Cleanup
-----------------------------------------------------------------------------}

procedure TFormMain.FormClose( Sender : TObject; var Action : TCloseAction );
begin
  FreeDirectDraw;
  Application.Minimize;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormKeyUp
   Desc: React to keystrokes
-----------------------------------------------------------------------------}

procedure TFormMain.FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
begin
  if ( Key = VK_F12 ) or ( Key = VK_ESCAPE ) then
  begin
    Key := 0;
    Close;
  end;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormActivate
   Desc: Toggle active-flag
-----------------------------------------------------------------------------}

procedure TFormMain.FormActivate( Sender : TObject );
begin
  FActive := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormDeactivate
   Desc: Toggle active-flag
----------------------------------------------------------------------------}

procedure TFormMain.FormDeactivate( Sender : TObject );
begin
  FActive := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormSetCursor
   Desc: Turn off cursor
-----------------------------------------------------------------------------}

procedure TFormMain.FormSetCursor( var aMsg : TMessage );
begin
  SetCursor( 0 );
  aMsg.Result := 1;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormOnIdle
   Desc: Update frame and flip pages
-----------------------------------------------------------------------------}

procedure TFormMain.FormOnIdle( Sender : TObject; var Done : Boolean );
var
  hr : HRESULT;
begin
  if FActive then
  begin
    hr := ProcessNextFrame;
    if hr <> DD_OK then
    begin
      ErrorOut( hr, 'ProcessNextFrame' );
      Close;
    end;
  end;
  Done := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Initialize Direct Draw
-----------------------------------------------------------------------------}

procedure TFormMain.FormShow( Sender : TObject );
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if hr <> DD_OK then
  begin
    ErrorOut( hr, 'InitDirectDraw' );
    Application.Terminate;
  end;

  if not HasGammaSupport then
  begin
    FDisplay := nil;

    MessageBox( Handle, 'The primary surface does not support gamma control. ' +
      #13#10 + 'The sample will now exit. ', PChar( Caption ),
      MB_ICONERROR or MB_OK );
    Application.Terminate;
  end;

  hr := FDisplay.GetFrontBuffer.QueryInterface( IID_IDirectDrawGammaControl, FGammaControl );
  if ( hr <> DD_OK ) then
  begin
    FDisplay := nil;

    MessageBox( Handle, 'The primary surface does not support IDirectDrawGammaControl. ' +
      #13#10 + 'The sample will now exit. ', PChar( Caption ),
      MB_ICONERROR or MB_OK );
    Application.Terminate;
  end;
end;

{-----------------------------------------------------------------------------
   Name: DisplayFrame()
   Desc: Blts a the sprites to the back buffer, then it blts or flips the
         back buffer onto the primary buffer.
-----------------------------------------------------------------------------}

function TFormMain.DisplayFrame : HRESULT;
var
  hr : HRESULT;
  ddsd : TDDSURFACEDESC2;
  ddbltfx : TDDBLTFX;
  rctDest : TRECT;
  dwRShift, dwGShift, dwBShift : DWORD;
  dwRColorLevels, dwGColorLevels, dwBColorLevels : DWORD;
  dwBits : DWORD;
  dwColor : DWORD;
  pFrontBuffer : IDIRECTDRAWSURFACE7;
  pBackBuffer : IDIRECTDRAWSURFACE7;
  i : integer;
begin
  if FDisplay = nil then
  begin
    result := DD_OK;
    exit;
  end;

  pFrontBuffer := FDisplay.GetFrontBuffer;
  pBackBuffer := FDisplay.GetBackBuffer;

  FillChar( ddsd, SizeOf( ddsd ), 0 );
  ddsd.dwSize := SizeOf( ddsd );
  hr := pFrontBuffer.GetSurfaceDesc( ddsd );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  // Fill the back buffer with black, ignoring errors until the flip
  FDisplay.Clear(0);

  FTextSurface.GetBitMaskInfo(ddsd.ddpfPixelFormat.dwRBitMask, dwRShift, dwBits);
  FTextSurface.GetBitMaskInfo(ddsd.ddpfPixelFormat.dwGBitMask, dwGShift, dwBits);
  FTextSurface.GetBitMaskInfo(ddsd.ddpfPixelFormat.dwBBitMask, dwBShift, dwBits);

  dwRColorLevels := ddsd.ddpfPixelFormat.dwRBitMask shr dwRShift;
  dwGColorLevels := ddsd.ddpfPixelFormat.dwGBitMask shr dwGShift;
  dwBColorLevels := ddsd.ddpfPixelFormat.dwBBitMask shr dwBShift;

  FillChar( ddbltfx, SizeOf( ddbltfx ), 0 );
  ddbltfx.dwSize := sizeof( ddbltfx );

  for i := 0 to NUM_COLOR_BARS do
  begin
    rctDest.left := Round( SCREEN_WIDTH / ( NUM_COLOR_BARS + 1 ) * ( i + 0 ) );
    rctDest.right := Round( SCREEN_WIDTH / ( NUM_COLOR_BARS + 1 ) * ( i + 1 ) );

    // Figure out the color for the red color bar
    dwColor := Round( dwRColorLevels * i / NUM_COLOR_BARS );
    ddbltfx.dwFillColor := dwColor shl dwRShift;

    rctDest.top := SCREEN_HEIGHT div 3 * 0;
    rctDest.bottom := SCREEN_HEIGHT div 3 * 1;
    pBackBuffer.Blt( @rctDest, nil, nil, DDBLT_COLORFILL or DDBLT_WAIT, @ddbltfx );

    // Figure out the color for the green color bar
    dwColor := Round( dwGColorLevels * i / NUM_COLOR_BARS );
    ddbltfx.dwFillColor := dwColor shl dwGShift;

    rctDest.top := SCREEN_HEIGHT div 3 * 1;
    rctDest.bottom := SCREEN_HEIGHT div 3 * 2;
    pBackBuffer.Blt( @rctDest, nil, nil, DDBLT_COLORFILL or DDBLT_WAIT, @ddbltfx );

    // Figure out the color for the blue color bar
    dwColor := Round( dwBColorLevels * i / NUM_COLOR_BARS );
    ddbltfx.dwFillColor := dwColor shl dwBShift;

    rctDest.top := SCREEN_HEIGHT div 3 * 2;
    rctDest.bottom := SCREEN_HEIGHT div 3 * 3;
    pBackBuffer.Blt( @rctDest, nil, nil, DDBLT_COLORFILL or DDBLT_WAIT, @ddbltfx );
  end;

  // Blt the help text on the backbuffer, ignoring errors until the flip
  FDisplay.Blt( 10, 10, FTextSurface, nil );

  // Blt the Gamma text on the backbuffer, ignoring errors until the flip
  FDisplay.Blt( 10, 30, FGammaTextSurface, nil );

  // Flip or blt the back buffer onto the primary buffer
  hr := FDisplay.Flip;
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
  Name: FreeDirectDraw
  Desc: Release all the DirectDraw objects
-----------------------------------------------------------------------------}

procedure TFormMain.FreeDirectDraw;
begin
  FGammaTextSurface.Free;
  FGammaTextSurface := nil;
  FTextSurface.Free;
  FTextSurface := nil;
  FDisplay.Free;
  FDisplay := nil;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDraw()
   Desc: Create the DirectDraw object, and init the surfaces
-----------------------------------------------------------------------------}

function TFormMain.InitDirectDraw : HRESULT;
var
  hr : HRESULT;
  strGammaText : string;
begin
  // Initialize all the surfaces we need
  FDisplay := TDisplay.Create;

  hr := FDisplay.CreateFullScreenDisplay( Handle, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP );
  if ( hr <> DD_OK ) then
  begin
    ErrorOut( hr, 'CreateFullScreenDisplay' );
    result := hr;
    exit;
  end;

  // Create a surface, and draw text to it.
  hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromText', hr );
    ErrorOut( hr, 'CreateSurfaceFromText' );
    result := hr;
    exit;
  end;

  strGammaText := Format( GAMMATEXT, [ FGammaRamp * 256 ] );
  // Create a surface, and draw text to it.
  hr := FDisplay.CreateSurfaceFromText( FGammaTextSurface, 0, strGammaText, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromText', hr );
    ErrorOut( hr, 'CreateSurfaceFromText' );
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: ProcessNextFrame()
   Desc: Move the sprites, blt them to the back buffer, then
         flip or blt the back buffer to the primary buffer
-----------------------------------------------------------------------------}

function TFormMain.ProcessNextFrame : HRESULT;
var
  hr : HRESULT;
begin
  // Display the sprites on the screen
  hr := DisplayFrame;
  if ( hr <> DD_OK ) then
  begin
    if ( hr <> DDERR_SURFACELOST ) then
    begin
      FreeDirectDraw;
      //DXTRACE_ERR( 'DisplayFrame', hr );
      result := hr;
      exit;
    end;

    // The surfaces were lost so restore them
    RestoreSurfaces;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: RestoreSurfaces()
   Desc: Restore all the surfaces, and redraw the sprite surfaces.
-----------------------------------------------------------------------------}

function TFormMain.RestoreSurfaces : HRESULT;
var
  hr : HRESULT;
begin
  hr := FDisplay.GetDirectDraw.RestoreAllSurfaces;
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("RestoreAllSurfaces"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  hr := FTextSurface.DrawText( 0, HELPTEXT, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("DrawText"), hr );
    result := hr;
    exit;
  end;

  DrawGammaText;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: UpdateGammaRamp()
   Desc:
-----------------------------------------------------------------------------}

function TFormMain.UpdateGammaRamp : HRESULT;
var
  hr : HRESULT;
  ddgr : TDDGAMMARAMP;
  dwGamma : WORD;
  iColor : Integer;
begin
  FillChar( ddgr, SizeOf( ddgr ), 0 );

  hr := FGammaControl.GetGammaRamp( 0, ddgr );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  dwGamma := 0;

  for iColor := 0 to 256 - 1 do
  begin
    ddgr.red[ iColor ] := dwGamma;
    ddgr.green[ iColor ] := dwGamma;
    ddgr.blue[ iColor ] := dwGamma;

    dwGamma := dwGamma + FGammaRamp;
  end;

  hr := FGammaControl.SetGammaRamp( 0, ddgr );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  hr := DrawGammaText;
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: DrawGammaText()
   Desc:
-----------------------------------------------------------------------------}

function TFormMain.DrawGammaText : HResult;
var
  hr : HRESULT;
  strGammaText : string;
begin
  strGammaText := Format( GAMMATEXT, [ FGammaRamp * 256 ] );
  hr := FGammaTextSurface.DrawText( 0, strGammaText, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: HasGammaSupport()
   Desc: Returns TRUE if the device supports gamma control, FALSE otherwise
-----------------------------------------------------------------------------}

function TFormMain.HasGammaSupport : Boolean;
var
  ddcaps : TDDCAPS;
begin
  // Get driver capabilities to determine gamma support.
  FillChar( ddcaps, SizeOf( ddcaps ), 0 );
  ddcaps.dwSize := SizeOf( ddcaps );

  FDisplay.GetDirectDraw.GetCaps( @ddcaps, nil );

  // Does the driver support gamma?
  // The DirectDraw emulation layer does not support overlays
  // so gamma related APIs will fail without hardware support.
  if ( ( ddcaps.dwCaps2 and DDCAPS2_PRIMARYGAMMA ) <> 0 ) then
    result := True
  else
    result := False;
end;

procedure TFormMain.FormMouseDown( Sender : TObject; Button : TMouseButton; Shift : TShiftState; X, Y : Integer );
begin
  case Button of
    mbLeft :
      begin
        FGammaRamp := FGammaRamp - 8;
        if ( FGammaRamp < 0 ) then
          FGammaRamp := 0;

        if ( UpdateGammaRamp <> DD_OK ) then
        begin
          FDisplay := nil;

          MessageBox( Handle, 'Failed setting the new gamma level. ' + #13#10 +
            'The sample will now exit.', PChar( Caption ), MB_ICONERROR or MB_OK );

          PostQuitMessage( 0 );
        end;
      end;

    mbRight :
      begin
        FGammaRamp := FGammaRamp + 8;
        if ( FGammaRamp > 256 ) then
          FGammaRamp := 256;

        if ( UpdateGammaRamp <> DD_OK ) then
        begin
          FDisplay := nil;

          MessageBox( Handle, 'Failed setting the new gamma level. ' + #13#10 +
            'The sample will now exit.', PChar( Caption ), MB_ICONERROR or MB_OK );

          PostQuitMessage( 0 );
        end;
      end;
  end;
end;

end.
