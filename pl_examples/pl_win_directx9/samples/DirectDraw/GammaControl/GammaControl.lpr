program GammaControl;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Gamma Control Example                    }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : GammaControl.h,                         }
{                          GammaControl.cpp                        }
{ The original Pascal code is : GammaControl_vcl.pas               }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   GammaControl demonstrates how to use IDirectDrawGammaControl to}
{   adjust how bright the system displays the DirectDraw surface.  }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   For details on how to setup a full-screen DirectDraw app, see  }
{   the FullScreenMode sample.                                     }
{                                                                  }
{   To adjust the gamma, first check to see if the device supports }
{   it.  Call IDirectDraw::GetCaps to check if DDCAPS2_PRIMARYGAMMA}
{   is set.  If it is, then you can set the gamma ramp by calling  }
{   IDirectDrawGammaControl::SetGammaRamp.                         }
{                                                                  }
{   For simplicity this sample creates a gamma ramp which is linear}
{   for all color components.  The ramp runs from 0 at index 0     }
{   linearly up to a user defined number at index 255 for each of  }
{   the color components.  However, the ramp does not need to be   }
{   linear.                                                        }
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  DDUtil,
  GammaControlmw;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm( TFormMain, FormMain );
  Application.Run;
end.
