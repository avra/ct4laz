unit SwitchScreenModemw;

{$MODE Delphi}

interface

uses
  Windows,
  Messages,
  SysUtils,
  Forms,
  Classes,
  ExtCtrls,
  DirectDraw,
  DDUtil,
  Menus;

const
  SCREEN_WIDTH = 640;
  SCREEN_HEIGHT = 480;
  SCREEN_BPP = 8;

  SPRITE_DIAMETER = 48;
  NUM_SPRITES = 25;

  WINDOWED_HELPTEXT = 'Press Escape to quit.  Press Alt-Enter to switch to Full-Screen mode.';
  FULLSCREEN_HELPTEXT = 'Press Escape to quit.  Press Alt-Enter to switch to Windowed mode.';

type
  PSpriteRecord = ^TSpriteRecord;
  TSpriteRecord = packed record
    fPosX : single;
    fPosY : single;
    fVelX : single;
    fVelY : single;
  end;

  TFormMain = class( TForm )
    MainMenu1 : TMainMenu;
    File1 : TMenuItem;
    ToggleFullScreen2 : TMenuItem;
    Exit1 : TMenuItem;
    procedure FormCreate( Sender : TObject );
    procedure FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
    procedure FormActivate( Sender : TObject );
    procedure FormDeactivate( Sender : TObject );
    procedure FormClose( Sender : TObject; var Action : TCloseAction );
    procedure FormShow( Sender : TObject );
    procedure FormPaint( Sender : TObject );
    procedure ToggleFullScreen2Click( Sender : TObject );
    procedure Exit1Click( Sender : TObject );
    procedure FormCanResize( Sender : TObject; var NewWidth,
      NewHeight : Integer; var Resize : Boolean );
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FLastTick : DWORD;
    FActive : Boolean;
    FWindowed : Boolean;
    FDisplay : TDisplay;
    FLogoSurface : TSurface;
    FTextSurface : TSurface;
    FRctWindow : TRect;
    FSprite : array[ 0..NUM_SPRITES - 1 ] of TSpriteRecord;
    procedure FormOnIdle( Sender : TObject; var Done : Boolean );
    procedure WMSetCursor( var aMsg : TMessage ); message WM_SETCURSOR;
    procedure WMQueryNewPalette( var aMsg : TMessage ); message WM_QUERYNEWPALETTE;
    procedure WMGetMinMaxInfo( var aMsg : TMessage ); message WM_GETMINMAXINFO;
    procedure WMMove( var aMsg : TMessage ); message WM_MOVE;
    procedure WMSize( var aMsg : TMessage ); message WM_SIZE;
    procedure ErrorOut( hRet : HRESULT; FuncName : string );
    procedure FreeDirectDraw;
    procedure UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
    function DisplayFrame : HRESULT;
    function RestoreSurfaces : HRESULT;
    function ProcessNextFrame : HRESULT;
    function InitDirectDraw : HRESULT;
    function InitDirectDrawMode( bWindowed : Boolean ) : HRESULT;
  end;

var
  FormMain : TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}

procedure TFormMain.ErrorOut( hRet : HRESULT; FuncName : string );
var
  ErrorMessage : string;
begin
  ErrorMessage := FuncName + ': ' + #13 + DDErrorString( hRet );
  MessageBox( Handle, PChar( ErrorMessage ), PChar( Caption ), MB_ICONERROR or MB_OK );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.Create
   Desc: Initialize the window
-----------------------------------------------------------------------------}

procedure TFormMain.FormCreate( Sender : TObject );
begin
  // Maximize the window
  Left := 0;
  Top := 0;
  Width := GetSystemMetrics( SM_CXSCREEN );
  Height := GetSystemMetrics( SM_CYSCREEN );
  FWindowed := true;
  Application.OnIdle := FormOnIdle;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormClose
   Desc: Cleanup
-----------------------------------------------------------------------------}

procedure TFormMain.FormClose( Sender : TObject; var Action : TCloseAction );
begin
  FreeDirectDraw;
  Application.Minimize;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormKeyUp
   Desc: React to keystrokes
-----------------------------------------------------------------------------}

procedure TFormMain.FormKeyUp( Sender : TObject; var Key : Word; Shift : TShiftState );
begin

end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormActivate
   Desc: Toggle active-flag
-----------------------------------------------------------------------------}

procedure TFormMain.FormActivate( Sender : TObject );
begin
  FActive := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormDeactivate
   Desc: Toggle active-flag
----------------------------------------------------------------------------}

procedure TFormMain.FormDeactivate( Sender : TObject );
begin
  FActive := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormOnIdle
   Desc: Update frame and flip pages
-----------------------------------------------------------------------------}

procedure TFormMain.FormOnIdle( Sender : TObject; var Done : Boolean );
var
  hr : HRESULT;
begin
  if FActive then
  begin
    hr := ProcessNextFrame;
    if hr <> DD_OK then
    begin
      ErrorOut( hr, 'ProcessNextFrame' );
      Close;
    end;
  end;
  Done := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Initialize Direct Draw
-----------------------------------------------------------------------------}

procedure TFormMain.FormShow( Sender : TObject );
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if hr <> DD_OK then
  begin
    ErrorOut( hr, 'InitDirectDraw' );
    Application.Terminate;
  end;
  // Save the window size/pos for switching modes
  GetWindowRect( Handle, FRctWindow );
end;

{-----------------------------------------------------------------------------
   Name: DisplayFrame()
   Desc: Blts a the sprites to the back buffer, then it blts or flips the
         back buffer onto the primary buffer.
-----------------------------------------------------------------------------}

function TFormMain.DisplayFrame : HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
begin
  // Fill the back buffer with black, ignoring errors until the flip
  FDisplay.Clear( 0 );

  // Blt the help text on the backbuffer, ignoring errors until the flip
  FDisplay.Blt( 10, 10, FTextSurface, nil );

  // Blt all the sprites onto the back buffer using color keying,
  // ignoring errors until the flip. Note that all of these sprites
  // use the same DirectDraw surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
    FDisplay.Blt( Round( FSprite[ iSprite ].fPosX ), Round( FSprite[ iSprite ].fPosY ), FLogoSurface, nil );


  // Flip or blt the back buffer onto the primary buffer
  hr := FDisplay.Flip;
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
  Name: FreeDirectDraw
  Desc: Release all the DirectDraw objects
-----------------------------------------------------------------------------}

procedure TFormMain.FreeDirectDraw;
begin
  FLogoSurface.Free;
  FLogoSurface := nil;
  FTextSurface.Free;
  FTextSurface := nil;
  FDisplay.Free;
  FDisplay := nil;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDraw()
   Desc: Create the DirectDraw object, and init the surfaces
-----------------------------------------------------------------------------}

function TFormMain.InitDirectDraw : HRESULT;
var
  hr : HRESULT;
begin
  ZeroMemory( @FSprite, SizeOf( TSpriteRecord ) * NUM_SPRITES );

  random( GetTickCount64 );
  // Initialize all the surfaces we need
  hr := InitDirectDrawMode( FWindowed );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  FLastTick := GetTickCount64;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: ProcessNextFrame()
   Desc: Move the sprites, blt them to the back buffer, then
         flip or blt the back buffer to the primary buffer
-----------------------------------------------------------------------------}

function TFormMain.ProcessNextFrame : HRESULT;
var
  hr : HRESULT;
  dwCurrTick : DWord;
  dwTickDiff : DWord;
  iSprite : integer;
begin
  // Figure how much time has passed since the last time
  dwCurrTick := GetTickCount64;
  dwTickDiff := dwCurrTick - FLastTick;

  // Don't update if no time has passed
  if ( dwTickDiff = 0 ) then
  begin
    result := DD_OK;
    exit
  end;

  FLastTick := dwCurrTick;

  // Move the sprites according to how much time has passed
  for iSprite := 0 to NUM_SPRITES - 1 do
    UpdateSprite( FSprite[ iSprite ], dwTickDiff / 1000.0 );

  // Display the sprites on the screen
  hr := DisplayFrame;
  if ( hr <> DD_OK ) then
  begin
    if ( hr <> DDERR_SURFACELOST ) then
    begin
      FreeDirectDraw;
      //DXTRACE_ERR( 'DisplayFrame', hr );
      result := hr;
      exit;
    end;

    // The surfaces were lost so restore them
    RestoreSurfaces;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: RestoreSurfaces()
   Desc: Restore all the surfaces, and redraw the sprite surfaces.
-----------------------------------------------------------------------------}

function TFormMain.RestoreSurfaces : HRESULT;
var
  hr : HRESULT;
  FDDPal : IDIRECTDRAWPALETTE;
begin
  FDDPal := nil;

  hr := FDisplay.GetDirectDraw.RestoreAllSurfaces;
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("RestoreAllSurfaces"), hr );
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  // We need to release and re-load, and set the palette again to
  hr := FDisplay.CreatePaletteFromBitmap( FDDPal, PAnsiChar( '..\xmedia\directx.bmp' ) {MakeIntResource( IDB_DIRECTX ) } );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("CreatePaletteFromBitmap"), hr );
    result := hr;
    exit;
  end;

  FDisplay.SetPalette( FDDPal );

  FDDPal := nil;

  // No need to re-create the surface, just re-draw it.
  //hr := FLogoSurface.DrawBitmap( MakeIntResource( IDB_DIRECTX ), SPRITE_DIAMETER, SPRITE_DIAMETER );
  hr := FLogoSurface.DrawBitmap( PChar( '..\xmedia\directx.bmp' ), SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( TEXT("DrawBitmap"), hr );
    result := hr;
    exit;
  end;

  if ( FWindowed ) then
  begin
    // No need to re-create the surface, just re-draw it.
    hr := FTextSurface.DrawText( 0, WINDOWED_HELPTEXT, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
    if ( hr <> DD_OK ) then
    begin
      result := hr;
      exit;
    end;
  end
  else
  begin
    // No need to re-create the surface, just re-draw it.
    hr := FTextSurface.DrawText( 0, FULLSCREEN_HELPTEXT, 0, 0, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
    if ( hr <> DD_OK ) then
    begin
      result := hr;
      exit;
    end;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: UpdateSprite()
   Desc: Move the sprite around and make it bounce based on how much time
         has passed
-----------------------------------------------------------------------------}

procedure TFormMain.UpdateSprite( var pSprite : TSpriteRecord; fTimeDelta : Single );
begin
// Update the sprite position
  pSprite.fPosX := pSprite.fPosX + pSprite.fVelX * fTimeDelta;
  pSprite.fPosY := pSprite.fPosY + pSprite.fVelY * fTimeDelta;

  // Clip the position, and bounce if it hits the edge
  if ( pSprite.fPosX < 0.0 ) then
  begin
    pSprite.fPosX := 0;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosX >= SCREEN_WIDTH - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosX := SCREEN_WIDTH - 1 - SPRITE_DIAMETER;
    pSprite.fVelX := -pSprite.fVelX;
  end;

  if ( pSprite.fPosY < 0 ) then
  begin
    pSprite.fPosY := 0;
    pSprite.fVelY := -pSprite.fVelY;
  end;

  if ( pSprite.fPosY > SCREEN_HEIGHT - SPRITE_DIAMETER ) then
  begin
    pSprite.fPosY := SCREEN_HEIGHT - 1 - SPRITE_DIAMETER;
    pSprite.fVelY := -pSprite.fVelY;
  end;
end;

procedure TFormMain.WMGETMINMAXINFO( var aMsg : TMessage );
var
  pMinMax : TMinMaxInfo;
  dwFrameWidth, dwFrameHeight, dwMenuHeight, dwCaptionHeight : DWORD;
begin
  // Don't allow resizing in windowed mode.
  // Fix the size of the window to 640x480 (client size)
  pMinMax := pMinMaxinfo( aMsg.lParam )^;

  dwFrameWidth := GetSystemMetrics( SM_CXSIZEFRAME );
  dwFrameHeight := GetSystemMetrics( SM_CYSIZEFRAME );
  dwMenuHeight := GetSystemMetrics( SM_CYMENU );
  dwCaptionHeight := GetSystemMetrics( SM_CYCAPTION );

  pMinMax.ptMinTrackSize.x := SCREEN_WIDTH + dwFrameWidth * 2;
  pMinMax.ptMinTrackSize.y := SCREEN_HEIGHT + dwFrameHeight * 2 + dwMenuHeight + dwCaptionHeight;

  pMinMax.ptMaxTrackSize.x := pMinMax.ptMinTrackSize.x;
  pMinMax.ptMaxTrackSize.y := pMinMax.ptMinTrackSize.y;
  aMsg.Result := 0;
end;

{procedure TFormMain.WMPaint(var aMsg: TMessage);
begin
  if( FDisplay <> nil ) then
  begin
    // Display the new position of the sprite
    if( DisplayFrame = DDERR_SURFACELOST ) then
    begin
      // If the surfaces were lost, then restore and try again
      RestoreSurfaces;
      DisplayFrame;
    end;
  end;
end;   }

procedure TFormMain.WMQueryNewPalette( var aMsg : TMessage );
var
  pDDPal : IDIRECTDRAWPALETTE;
begin
  if ( FDisplay <> nil ) and ( FDisplay.GetFrontBuffer <> nil ) then
  begin
    // If we are in windowed mode with a desktop resolution in 8 bit
    // color, then the palette we created during init has changed
    // since then.  So get the palette back from the primary
    // DirectDraw surface, and set it again so that DirectDraw
    // realises the palette, then release it again.
    pDDPal := nil;
    FDisplay.GetFrontBuffer.GetPalette( pDDPal );
    FDisplay.GetFrontBuffer.SetPalette( pDDPal );
    pDDPal := nil;
  end;
end;

procedure TFormMain.WMMove( var aMsg : TMessage );
begin
  // Retrieve the window position after a move.
  if ( FDisplay <> nil ) then
    FDisplay.UpdateBounds;
  aMsg.Result := 0;
end;

procedure TFormMain.WMSize( var aMsg : TMessage );
begin
  // Check to see if we are losing our window...
  if ( aMsg.wParam = SIZE_MAXHIDE ) or ( aMsg.wParam = SIZE_MINIMIZED ) then
    FActive := False
  else
    FActive := True;

  if ( FDisplay <> nil ) then
    FDisplay.UpdateBounds;
end;

procedure TFormMain.FormPaint( Sender : TObject );
begin
  if ( FDisplay <> nil ) then
  begin
    // Display the new position of the sprite
    if ( DisplayFrame = DDERR_SURFACELOST ) then
    begin
      // If the surfaces were lost, then restore and try again
      RestoreSurfaces;
      DisplayFrame;
    end;
  end;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDrawMode()
   Desc: Called when the user wants to toggle between full-screen and windowed
         to create all the needed DDraw surfaces and set the coop level
-----------------------------------------------------------------------------}

function TFormMain.InitDirectDrawMode( bWindowed : Boolean ) : HRESULT;
var
  hr : HRESULT;
  pDDPal : IDIRECTDRAWPALETTE;
  iSprite : integer;
  dwStyle : DWORD;
begin
  pDDpal := nil;

  // Release all existing surfaces
  FreeDirectDraw;

  // The back buffer and primary surfaces need to be created differently
  // depending on if we are in full-screen or windowed mode
  FDisplay := TDisplay.Create;

  if ( bWindowed ) then
  begin
    hr := FDisplay.CreateWindowedDisplay( Handle, SCREEN_WIDTH, SCREEN_HEIGHT );
    if ( hr <> DD_OK ) then
    begin
      result := hr;
      exit;
    end;

    // Add the system menu to the window's style
    dwStyle := GetWindowLongPtr( Handle, GWL_STYLE );
    dwStyle := dwStyle or WS_SYSMENU;
    SetWindowLongPtr( Handle, GWL_STYLE, dwStyle );

      // Show the menu in windowed mode

    GetWindowLongPtr( Handle, GWLP_HINSTANCE );

    Menu := MainMenu1;

  end
  else
  begin
    hr := FDisplay.CreateFullScreenDisplay( Handle, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP );
    if ( hr <> DD_OK ) then
    begin
      MessageBox( Handle, 'This display card does not support 640x480x8.', 'DirectDraw Sample', MB_ICONERROR or MB_OK );
      result := hr;
      exit
    end;

    // Disable the menu in full-screen since we are
    // using a palette and a menu would look bad
    Menu := nil; //SetMenu( h_Wnd, 0 );

    // Remove the system menu from the window's style
    dwStyle := GetWindowLongPtr( Handle, GWL_STYLE );
    dwStyle := dwStyle and not WS_SYSMENU;
    SetWindowLongPtr( Handle, GWL_STYLE, dwStyle );
  end;

  // We need to release and re-load, and set the palette again to
  // redraw the bitmap on the surface.  Otherwise, GDI will not
  // draw the bitmap on the surface with the right palette
  hr := FDisplay.CreatePaletteFromBitmap( pDDPal, PAnsiChar( '..\xmedia\directx.bmp' ) {MakeIntResource( IDB_DIRECTX ) } );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit
  end;

  FDisplay.SetPalette( pDDPal );

  pDDPal := nil;

  if ( FWindowed ) then
  begin
    // Create a surface, and draw text to it.
    hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, WINDOWED_HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
    if ( hr <> DD_OK ) then
    begin
      result := hr;
      exit;
    end;
  end
  else
  begin
    // Create a surface, and draw text to it.
    hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, FULLSCREEN_HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
    if ( hr <> DD_OK ) then
    begin
      result := hr;
      exit;
    end;
  end;


  // Create a surface, and draw a bitmap resource on it.  The surface must
  // be newly created every time the screen mode is switched since it
  // uses the pixel format of the primary surface
  hr := FDisplay.CreateSurfaceFromBitmap( FLogoSurface, PAnsiChar( '..\xmedia\directx.bmp' ) {MakeIntResource( IDB_DIRECTX )}, SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  // Set the color key for the logo sprite to black
  hr := FLogoSurface.SetColorKey( 0 );
  if ( hr <> DD_OK ) then
  begin
    result := hr;
    exit;
  end;

  // Init all the sprites.  All of these sprites look the same,
  // using the FDDSLogo surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
  begin
    // Set the sprite's position and velocity
    FSprite[ iSprite ].fPosX := random( SCREEN_WIDTH );
    FSprite[ iSprite ].fPosY := random( SCREEN_HEIGHT );

    FSprite[ iSprite ].fVelX := 500.0 * random / RAND_MAX - 250.0;
    FSprite[ iSprite ].fVelY := 500.0 * random / RAND_MAX - 250.0;
  end;

  result := DD_OK;
end;

procedure TFormMain.ToggleFullScreen2Click( Sender : TObject );
var
  hr : HResult;
begin
  // Toggle the fullscreen/window mode
  if ( FWindowed ) then
    GetWindowRect( Handle, FRctWindow );

  FWindowed := not FWindowed;

  if FWindowed then
  begin
    FormMain.Menu := MainMenu1;
  end
  else
  begin
    FormMain.Menu := nil;
  end;

  hr := InitDirectDrawMode( FWindowed );
  if ( hr <> DD_OK ) then
  begin
    FreeDirectDraw;
    MessageBox( Handle, 'InitDirectDrawMode failed.' + #13#10 + 'The sample will now exit. ', 'DirectDraw Sample', MB_ICONERROR or MB_OK );
    Close;
  end;
end;

procedure TFormMain.Exit1Click( Sender : TObject );
begin
  Close;
end;

procedure TFormMain.WMSetCursor( var aMsg : TMessage );
begin
  if ( not FWindowed ) then
  begin
    SetCursor( 0 );
    aMsg.Result := 1;
  end
  else
    SetCursor( 1 );
end;

procedure TFormMain.FormCanResize( Sender : TObject; var NewWidth,
  NewHeight : Integer; var Resize : Boolean );
begin
  Resize := false;
end;

procedure TFormMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE :
    begin
      Key := 0;
      Close;
    end;

    VK_RETURN :
    begin
      if ( Shift = [ ssAlt ] ) then
      begin
        Key := 0;
        ToggleFullScreen2Click( Sender );
      end;
    end;
  end;
end;

end.
