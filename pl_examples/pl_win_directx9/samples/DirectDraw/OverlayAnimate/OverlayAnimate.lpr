program OverlayAnimate;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Overlay Animate Example                  }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : OverlayAnimate.h,                       }
{                          OverlayAnimate.cpp                      }
{ The original Pascal code is : OverlayAnimate_vcl.pas             }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   OverlayAnimate demonstrates how to use DirectDraw overlays.    }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   For details on how to setup a windowed mode DirectDraw app, see}
{   the WindowedMode sample.                                       }
{                                                                  }
{   To use overlays in general do the following steps in addition  }
{   to those needed to                                             }
{  author a windowed mode DirectDraw app:
    1. Check to see if hardware supports overlays - check IDirectDraw::GetCaps 
       for DDCAPS_OVERLAY.
    2. Size the window to meet the hardware overlay size restrictions.
    3. Create an overlay surface (create it with 1 backbuffer if needed), and 
       set its pixel format to a desired format that is supported by the device.  
    4. Set the dest color key on the overlay to the background color of the window. 
       Be sure to choose a color for the background of the window that Windows typically
       does not use otherwise, the overlay will be drawn on top of overlapping windows.
    5. Call UpdateOverlay to display or hide the overlay on the desktop.  WM_PAINT is 
       a good place for this.
    6. When WM_SIZE or WM_MOVE is sent, then update the src and dest rects and 
       check to make sure that they are within the hardware limits.
       
  To animate the overlay, instead of drawing to the off-screen plain back buffer as 
  in windowed mode case, just draw to the the overlay's backbuffer then flip it.}
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  OverlayAnimatemw in 'MainForm.pas' {FormMain},
  DDUtil in '..\..\Common\DDUtil.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end.
