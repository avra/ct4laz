unit OverlayAnimatemw;

{$MODE Delphi}

interface

uses
  Windows,
  Messages,
  SysUtils,
  Forms,
  Classes,
  ExtCtrls,
  DirectDraw,
  DDUtil, Menus;

const
  NAME: PChar = 'OverlayAnimate';
  TITLE: PChar = 'DirectDraw Overlay Animation Example';
  IDI_MAIN_ICON = 101;
  IDR_MENU = 102;
  IDR_MAIN_ACCEL = 103;
  IDB_ANIMATE_SHEET = 107;
  IDM_EXIT = 1001;

  SCREEN_WIDTH = 128;
  SCREEN_HEIGHT = 128;
  SCREEN_BPP = 16;
  SPRITE_DIAMETER = 48;
  NUM_FRAMES = 30;

  HELPTEXT = 'Press Escape to quit.';
  
type
  PSpriteRecord = ^TSpriteRecord;
  TSpriteRecord = packed record
    fPosX : single;
    fPosY : single;
    fVelX : single;
    fVelY : single;
  end;
  
  TFormMain = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
  private
    FActive        : Boolean;
    FpDD: IDIRECTDRAW7;
    FpDDSPrimary: IDIRECTDRAWSURFACE7;
    FpDDSOverlay: IDIRECTDRAWSURFACE7;
    FpDDSOverlayBack: IDIRECTDRAWSURFACE7;
    FpDDSAnimationSheet: IDIRECTDRAWSURFACE7;
    FOverlayFX: TDDOVERLAYFX;
    FdwOverlayFlags: DWORD;
    FdwFrame: DWORD;
    Fddcaps: TDDCAPS;
    FrcSrc: TRect;
    FrcDst: TRect;
    FdwXRatio: DWORD;
    FdwYRatio: DWORD;

    // This will be used as the color key, so try to make it something
    // that doesn't appear in the source image.
    FdwBackgroundColor: TColorRef;
    procedure FormOnIdle(Sender: TObject; var Done: Boolean);
    //procedure WMPaint(var aMsg : TMessage); message WM_PAINT;
    procedure WMQueryNewPalette(var aMsg : TMessage); message WM_QUERYNEWPALETTE;
    //procedure WMGetMinMaxInfo(var aMsg : TMessage); message WM_GETMINMAXINFO;
    procedure WMMove(var aMsg : TMessage); message WM_MOVE;
    procedure WMSize(var aMsg : TWMSize); message WM_SIZE;
    procedure WMDisplayChange(var aMsg : TMessage); message WM_DISPLAYCHANGE;
    procedure ErrorOut(hRet : HRESULT; FuncName : string);
    procedure FreeDirectDraw;
    function DisplayFrame : HRESULT;
    function RestoreSurfaces : HRESULT;
    function ProcessNextFrame : HRESULT;
    function InitDirectDraw : HRESULT;
    function HasOverlaySupport: Boolean;
    procedure AdjustSizeForHardwareLimits;
    function CreateDirectDrawSurfaces : HResult;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}
procedure TFormMain.ErrorOut(hRet : HRESULT; FuncName : string);
var
  ErrorMessage : string;
begin
  ErrorMessage := FuncName + ': ' + #13 + DDErrorString(hRet);
  MessageBox( Handle, PChar(ErrorMessage), PChar(Caption), MB_ICONERROR or MB_OK );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.Create
   Desc: Initialize the window
-----------------------------------------------------------------------------}
procedure TFormMain.FormCreate(Sender: TObject);
begin
  // Maximize the window
  Left   := 0;
  Top    := 0;
  Width  := SCREEN_WIDTH;
  Height := SCREEN_HEIGHT;
  FpDD:= nil;
  FpDDSPrimary := nil;
  FpDDSOverlay := nil;
  FpDDSOverlayBack := nil;
  FpDDSAnimationSheet := nil;
  FdwOverlayFlags := 0;
  FdwFrame := 0;
  FActive := False;
  Application.OnIdle := FormOnIdle;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormClose
   Desc: Cleanup
-----------------------------------------------------------------------------}
procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeDirectDraw;
  Application.Minimize;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormKeyUp
   Desc: React to keystrokes
-----------------------------------------------------------------------------}
procedure TFormMain.FormKeyUp(Sender: TObject; var Key : Word; Shift: TShiftState);
begin
  if (Key = VK_F12) or (Key = VK_ESCAPE) then
  begin
    Key := 0;
    Close;
  end;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormActivate
   Desc: Toggle active-flag
-----------------------------------------------------------------------------}
procedure TFormMain.FormActivate(Sender: TObject);
begin
  FActive := True;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormDeactivate
   Desc: Toggle active-flag
----------------------------------------------------------------------------}
procedure TFormMain.FormDeactivate(Sender: TObject);
begin
  FActive := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormOnIdle
   Desc: Update frame and flip pages
-----------------------------------------------------------------------------}
procedure TFormMain.FormOnIdle(Sender: TObject; var Done: Boolean);
var
  hr : HRESULT;
begin
  if FActive then
  begin
    hr := ProcessNextFrame;
    if hr  <> DD_OK then
    begin
      FpDD.SetCooperativeLevel(0, DDSCL_NORMAL);
      ErrorOut( hr,  'ProcessNextFrame' );
      Close;
    end;
  end;
  Done := False;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Initialize Direct Draw
-----------------------------------------------------------------------------}
procedure TFormMain.FormShow(Sender: TObject);
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if (hr <> DD_OK) then
  begin
    MessageBox(Handle, 'DirectDraw init failed. ' + #13#10 +
      'The sample will now exit. ', 'DirectDraw Sample', MB_ICONERROR or MB_OK);
    exit;
  end;

  if (not HasOverlaySupport) then
  begin
    MessageBox(Handle, 'This DirectDraw device does not support overlays. ' +
      #13#10 + 'The sample will now exit. ', 'DirectDraw Sample', MB_ICONERROR
      or
      MB_OK);
    exit;
  end;

  // Create the DirectDraw surfaces needed
  hr := CreateDirectDrawSurfaces;
  if (hr <> DD_OK) then
  begin
    MessageBox(Handle,
      'Failed to create surfaces.  This DirectDraw device may not support overlays. ' + #13#10
      + 'The sample will now exit. ', 'DirectDraw Sample', MB_ICONERROR or
      MB_OK);
    exit;
  end;
end;

{-----------------------------------------------------------------------------
   Name: DisplayFrame()
   Desc: Blts a the sprites to the back buffer, then it blts or flips the
         back buffer onto the primary buffer.
-----------------------------------------------------------------------------}
function TFormMain.DisplayFrame : HRESULT;
var
  hr: HRESULT;
  rcSrc: TRect;
begin
  rcSrc.left := (FdwFrame mod 5) * SPRITE_DIAMETER;
  rcSrc.top := (FdwFrame div 5) * SPRITE_DIAMETER;
  rcSrc.right := rcSrc.left + SPRITE_DIAMETER;
  rcSrc.bottom := rcSrc.top + SPRITE_DIAMETER;

  FpDDSOverlayBack.Blt(nil, FpDDSAnimationSheet, @rcSrc, DDBLT_WAIT, nil);

  hr := FpDDSOverlay.Flip(nil, DDFLIP_WAIT);
  if (hr <> DD_OK) then
  begin
    result := hr;
    exit;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
  Name: FreeDirectDraw
  Desc: Release all the DirectDraw objects
-----------------------------------------------------------------------------}
procedure TFormMain.FreeDirectDraw;
begin
  FpDD:= nil;
  FpDDSPrimary := nil;
  FpDDSOverlayBack := nil;
  FpDDSOverlay := nil;
  FpDDSAnimationSheet := nil;
end;

{-----------------------------------------------------------------------------
   Name: InitDirectDraw()
   Desc: Create the DirectDraw object, and init the surfaces
-----------------------------------------------------------------------------}
function TFormMain.InitDirectDraw : HRESULT;
var
  hr: HRESULT;
  ddsd: TDDSURFACEDESC2;
begin
  // Create the main DirectDraw object
  hr := DirectDrawCreateEx(nil, FpDD, IID_IDirectDraw7, nil);
  if (hr <> DD_OK) then
  begin
    MessageBox( Handle, 'Failed initializing DirectDraw.', TITLE, MB_ICONERROR or
      MB_OK);
    result := hr;
    exit;
  end;

  // Request normal cooperative level to put us in windowed mode
  hr := FpDD.SetCooperativeLevel(Handle, DDSCL_NORMAL);
  if (hr <> DD_OK) then
  begin
    MessageBox(Handle, 'Failed to Set Cooperative Level.', TITLE, MB_ICONERROR or
      MB_OK);
    result := hr;
    exit;
  end;

  // Get driver capabilities to determine Overlay support.
  fillbyte(Fddcaps, sizeof(Fddcaps),0);
  Fddcaps.dwSize := sizeof(Fddcaps);

  hr := FpDD.GetCaps(@Fddcaps, nil);
  if (hr <> DD_OK) then
  begin
    MessageBox(Handle, 'Failed to GetCaps.', TITLE, MB_ICONERROR or MB_OK);
    result := hr;
    exit;
  end;

  // Create the primary surface, which in windowed mode is the desktop.
  fillbyte(ddsd, sizeof(ddsd),0);
  ddsd.dwSize := sizeof(ddsd);
  ddsd.dwFlags := DDSD_CAPS;
  ddsd.ddsCaps.dwCaps := DDSCAPS_PRIMARYSURFACE;

  hr := FpDD.CreateSurface(ddsd, FpDDSPrimary, nil);
  if (hr <> DD_OK) then
  begin
    MessageBox(Handle, 'Failed to GetCaps.', TITLE, MB_ICONERROR or MB_OK);
    result := hr;
    exit;
  end;
  
  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: ProcessNextFrame()
   Desc: Move the sprites, blt them to the back buffer, then
         flip or blt the back buffer to the primary buffer
-----------------------------------------------------------------------------}
function TFormMain.ProcessNextFrame : HRESULT;
const
  s_dwFrameSkip: integer = 0;
var
  hr: HRESULT;
begin
  //s_dwFrameSkip := 0;

  // Only advance the frame animation number every 5th frame
  inc(s_dwFrameSkip);
  s_dwFrameSkip := s_dwFrameSkip mod 5;

  if (s_dwFrameSkip = 0) then
  begin
    inc(FdwFrame);
    FdwFrame := FdwFrame mod NUM_FRAMES;
  end;

  // Check the cooperative level before rendering
  hr := FpDD.TestCooperativeLevel;
  if (hr <> DD_OK) then
  begin
    case hr of

      DDERR_EXCLUSIVEMODEALREADYSET:
        begin
          // Do nothing because some other app has exclusive mode
          Sleep(10);
          result := DD_OK;
          exit;
        end;

      DDERR_WRONGMODE:
        begin
          // The display mode changed on us. Update the
          // DirectDraw surfaces accordingly
          result := CreateDirectDrawSurfaces;
          exit;
        end;
    end;
    result := hr;
    exit;
  end;

  // Display the sprites on the screen
  hr := DisplayFrame;
  if (hr <> DD_OK) then
  begin
    if (hr <> DDERR_SURFACELOST) then
    begin
      result := hr;
      exit;
    end;

    // The surfaces were lost so restore them
    RestoreSurfaces;
  end;

  result := DD_OK;
end;

{-----------------------------------------------------------------------------
   Name: RestoreSurfaces()
   Desc: Restore all the surfaces, and redraw the sprite surfaces.
-----------------------------------------------------------------------------}
function TFormMain.RestoreSurfaces: HRESULT;
var
  hr: HRESULT;
  animateSurface: TSurface;
begin
  hr := FpDD.RestoreAllSurfaces;
  if (hr <> DD_OK) then
  begin
    result := hr;
    exit;
  end;

  // No need to re-create the surface, just re-draw it.
  animateSurface := TSurface.Create;
  try
    animateSurface.CreateSurface(FpDDSAnimationSheet);
    hr := animateSurface.DrawBitmap('../xmedia/animate.bmp'{MAKEINTRESOURCE(IDB_ANIMATE_SHEET)},
      SPRITE_DIAMETER * 5, SPRITE_DIAMETER * 6);
    if (hr <> DD_OK) then
    begin
      result := hr;
      exit;
    end;
  finally
    animateSurface.Free;
  end;

  result := DD_OK;
end;

{procedure TFormMain.WMGETMINMAXINFO(var aMsg: TMessage);
var
  pMinMax : TMinMaxInfo;
  dwFrameWidth, dwFrameHeight, dwMenuHeight, dwCaptionHeight : DWORD;
begin
  // Don't allow resizing in windowed mode.
  // Fix the size of the window to 640x480 (client size)
  pMinMax := pMinMaxinfo( aMsg.lParam )^;

  dwFrameWidth    := GetSystemMetrics( SM_CXSIZEFRAME );
  dwFrameHeight   := GetSystemMetrics( SM_CYSIZEFRAME );
  dwMenuHeight    := GetSystemMetrics( SM_CYMENU );
  dwCaptionHeight := GetSystemMetrics( SM_CYCAPTION );

  pMinMax.ptMinTrackSize.x := SCREEN_WIDTH  + dwFrameWidth * 2;
  pMinMax.ptMinTrackSize.y := SCREEN_HEIGHT + dwFrameHeight * 2 + dwMenuHeight + dwCaptionHeight;

  pMinMax.ptMaxTrackSize.x := pMinMax.ptMinTrackSize.x;
  pMinMax.ptMaxTrackSize.y := pMinMax.ptMinTrackSize.y;
  aMsg.Result := 0;
end;}

{procedure TFormMain.WMPaint(var aMsg: TMessage);
begin
  if( FDisplay <> nil ) then
  begin
    // Display the new position of the sprite
    if( DisplayFrame = DDERR_SURFACELOST ) then
    begin
      // If the surfaces were lost, then restore and try again
      RestoreSurfaces;
      DisplayFrame;
    end;
  end;
end;   }

procedure TFormMain.WMQueryNewPalette(var aMsg: TMessage);
var
  pDDPal : IDIRECTDRAWPALETTE;
begin
  if (FpDDSPrimary <> nil) then
  begin
    // If we are in windowed mode with a desktop resolution in 8 bit
    // color, then the palette we created during init has changed
    // since then.  So get the palette back from the primary
    // DirectDraw surface, and set it again so that DirectDraw
    // realises the palette, then release it again.
    pDDPal := nil;
    FpDDSPrimary.GetPalette(pDDPal);
    FpDDSPrimary.SetPalette(pDDPal);
    pDDPal := nil;
  end;
end;

procedure TFormMain.WMMove(var aMsg: TMessage);
var
  p: TPOINT;
begin
  // Make sure we're not moving to be minimized - because otherwise
  // our ratio varialbes (FdwXRatio and FdwYRatio) will end up
  // being 0, and once we hit CheckBoundries it divides by 0.
  if (not IsIconic(Handle)) then
  begin
    p.x := 0;
    p.y := 0; // Translation point for the window's client region

    FrcSrc.left := 0;
    FrcSrc.right := SCREEN_WIDTH;
    FrcSrc.top := 0;
    FrcSrc.bottom := SCREEN_HEIGHT;

    Windows.GetClientRect(Handle, FrcDst);

    FdwXRatio := (FrcDst.right - FrcDst.left) * 1000 div
      (FrcSrc.right - FrcSrc.left);
    FdwYRatio := (FrcDst.bottom - FrcDst.top) * 1000 div
      (FrcSrc.bottom - FrcSrc.top);

    Windows.ClientToScreen(Handle, p);
    FrcDst.left := p.x;
    FrcDst.top := p.y;
    FrcDst.bottom := FrcDst.bottom + p.y;
    FrcDst.right := FrcDst.right + p.x;
    if (FpDD <> nil) then
      AdjustSizeForHardwareLimits;
  end
  else
  begin
    // Else, hide the overlay... just in case we can't do
    // destination color keying, this will pull the overlay
    // off of the screen for the user.
    if (FpDDSOverlay <> nil) and (FpDDSPrimary <> nil) then
      FpDDSOverlay.UpdateOverlay(nil, FpDDSPrimary, nil,
        DDOVER_HIDE, nil);
  end;

  // Check to make sure our window exists before we tell it to
  // repaint. This will fail the first time (while the window is
  // being created).
  if (Handle <> 0) then
  begin
    InvalidateRect(Handle, nil, FALSE);
    UpdateWindow(Handle);
  end;

  aMsg.Result := 0;
end;

procedure TFormMain.WMSize(var aMsg: TWMSize);
var
  p: TPOINT;
begin
  // Another check for the minimization action.  This check is
  // quicker though...
  // Check to see if we are losing our window...
  if (SIZE_MAXHIDE = aMsg.SizeType) or (SIZE_MINIMIZED = aMsg.SizeType) then
    FActive := FALSE
  else 
    FActive := TRUE;

  if (FActive) then
  begin
    p.x := 0;
    p.y := 0; // Translation point for the window's client region

    Windows.GetClientRect(Handle, FrcDst);
    Windows.ClientToScreen(Handle, p);

    FrcDst.left := p.x;
    FrcDst.top := p.y;
    FrcDst.bottom := FrcDst.bottom + p.y;
    FrcDst.right := FrcDst.right + p.x;

    FrcSrc.left := 0;
    FrcSrc.right := SCREEN_WIDTH;
    FrcSrc.top := 0;
    FrcSrc.bottom := SCREEN_HEIGHT;

    // Here we multiply by 1000 to preserve 3 decimal places in the
    // division opperation (we picked 1000 to be on the same order
    // of magnitude as the stretch factor for easier comparisons)
    FdwXRatio := (FrcDst.right - FrcDst.left) * 1000 div (FrcSrc.right - FrcSrc.left);

    FdwYRatio := (FrcDst.bottom - FrcDst.top) * 1000 div (FrcSrc.bottom - FrcSrc.top);

    AdjustSizeForHardwareLimits;
  end;
  AMsg.Result := 0;
end;

procedure TFormMain.FormPaint(Sender: TObject);
begin
  // Update the screen if we need to refresh. This case occurs
  // when in windowed mode and the window is behind others.
  // The app will not be active, but it will be visible.
  if (FpDDSPrimary <> nil) then
  begin
    // UpdateOverlay is how we put the overlay on the screen.
    if (FrcDst.top = FrcDst.bottom) then
    begin
      FpDDSOverlay.UpdateOverlay(nil, FpDDSPrimary, nil, DDOVER_HIDE, nil);
    end
    else
    begin
      FpDDSOverlay.UpdateOverlay(@FrcSrc, FpDDSPrimary, @FrcDst, FdwOverlayFlags, @FOverlayFX);
    end;
  end;
end;

procedure TFormMain.Exit1Click(Sender: TObject);
begin
  Close;
end;

//-----------------------------------------------------------------------------
// Name: AdjustSizeForHardwareLimits()
// Desc: Checks and corrects all boundries for alignment and stretching
//-----------------------------------------------------------------------------
procedure TFormMain.AdjustSizeForHardwareLimits;
begin
  // Setup effects structure
  // Make sure the coordinates fulfill the stretching requirements.  Often
  // the hardware will require a certain ammount of stretching to do
  // overlays. This stretch factor is held in dwMinOverlayStretch as the
  // stretch factor multiplied by 1000 (to keep an accuracy of 3 decimal
  // places).
  if ( (Fddcaps.dwCaps and DDCAPS_OVERLAYSTRETCH) and
     ( Fddcaps.dwMinOverlayStretch ) and
     Ord( FdwXRatio < Fddcaps.dwMinOverlayStretch ) <> 0 ) then
  begin
    // Window is too small
    FrcDst.right := 2 * GetSystemMetrics(SM_CXSIZEFRAME) + FrcDst.left +
                    (SCREEN_WIDTH * (Fddcaps.dwMinOverlayStretch + 1)) div 1000;
  end;

  if ((Fddcaps.dwCaps and DDCAPS_OVERLAYSTRETCH) and
     (Fddcaps.dwMaxOverlayStretch) and
     Ord( FdwXRatio < Fddcaps.dwMaxOverlayStretch ) <> 0 ) then
  begin
    // Window is too large
    FrcDst.right := 2 * GetSystemMetrics(SM_CXSIZEFRAME) + FrcDst.left +
      (SCREEN_HEIGHT
      * (Fddcaps.dwMaxOverlayStretch + 999)) div 1000;
  end;

  // Recalculate the ratio's for the upcoming calculations
  FdwXRatio := (FrcDst.right - FrcDst.left) * 1000 div
                (FrcSrc.right - FrcSrc.left);
  FdwYRatio := (FrcDst.bottom - FrcDst.top) * 1000 div
                (FrcSrc.bottom -  FrcSrc.top);

  // Check to make sure we're within the screen's boundries, if not then fix
  // the problem by adjusting the source rectangle which we draw from.
  if (FrcDst.left < 0) then
  begin
    FrcSrc.left := -FrcDst.left * 1000 div FdwXRatio;
    FrcDst.left := 0;
  end;

  if (FrcDst.right > GetSystemMetrics(SM_CXSCREEN)) then
  begin
    FrcSrc.right := SCREEN_WIDTH - ((FrcDst.right -
      GetSystemMetrics(SM_CXSCREEN)) *
      1000 div FdwXRatio);
    FrcDst.right := GetSystemMetrics(SM_CXSCREEN);
  end;

  if (FrcDst.bottom > GetSystemMetrics(SM_CYSCREEN)) then
  begin
    FrcSrc.bottom := SCREEN_HEIGHT - ((FrcDst.bottom -
      GetSystemMetrics(SM_CYSCREEN))
      * 1000 div FdwYRatio);
    FrcDst.bottom := GetSystemMetrics(SM_CYSCREEN);
  end;

  if (FrcDst.top < 0) then
  begin
    FrcSrc.top := -FrcDst.top * 1000 div FdwYRatio;
    FrcDst.top := 0;
  end;

  // Make sure the coordinates fulfill the alignment requirements
  // these expressions (x & -y) just do alignment by dropping low order bits...
  // so to round up, we add first, then truncate.
  if ((Fddcaps.dwCaps and DDCAPS_ALIGNBOUNDARYSRC) and
    (Fddcaps.dwAlignBoundarySrc) <> 0 )  then
  begin
    FrcSrc.left := (FrcSrc.left + Fddcaps.dwAlignBoundarySrc div 2) and
                    -(Fddcaps.dwAlignBoundarySrc);
  end;

  if ((Fddcaps.dwCaps and DDCAPS_ALIGNSIZESRC) and
    (Fddcaps.dwAlignSizeSrc) <> 0 )  then
  begin
    FrcSrc.right := (FrcSrc.left + (FrcSrc.right - FrcSrc.left +
                     Fddcaps.dwAlignSizeSrc div 2) ) and -(Fddcaps.dwAlignSizeSrc);
  end;

  if ((Fddcaps.dwCaps and DDCAPS_ALIGNBOUNDARYDEST) and
    (Fddcaps.dwAlignBoundaryDest) <> 0 )  then
  begin
    FrcDst.left := (FrcDst.left + Fddcaps.dwAlignBoundaryDest div 2) and
                    -(Fddcaps.dwAlignBoundaryDest);
  end;

  if ((Fddcaps.dwCaps and DDCAPS_ALIGNSIZEDEST) and
    (Fddcaps.dwAlignSizeDest) <> 0 )  then
  begin
    FrcDst.right := FrcDst.left + (FrcDst.right - FrcDst.left) and
                     -(Fddcaps.dwAlignSizeDest);
  end;
end;

//-----------------------------------------------------------------------------
// Name: HasOverlaySupport()
// Desc: Returns TRUE if the device supports overlays, FALSE otherwise
//-----------------------------------------------------------------------------
function TFormMain.HasOverlaySupport: Boolean;
begin
  // Get driver capabilities to determine overlay support.
  ZeroMemory(@Fddcaps, sizeof(Fddcaps));
  Fddcaps.dwSize := sizeof(Fddcaps);
  FpDD.GetCaps(@Fddcaps, nil);

  // Does the driver support overlays in the current mode?
  // The DirectDraw emulation layer does not support overlays
  // so overlay related APIs will fail without hardware support.
  if (Fddcaps.dwCaps and DDCAPS_OVERLAY) <> 0 then
  begin
    // Make sure it supports stretching (scaling)
    if (Fddcaps.dwCaps and DDCAPS_OVERLAYSTRETCH) <> 0 then
      result := TRUE
    else
      result := FALSE;
  end
  else
  begin
    result := FALSE;
  end;
end;

function TFormMain.CreateDirectDrawSurfaces : HResult;
var
  ddsd: TDDSURFACEDESC2;
  ddpfOverlayFormat: TDDPIXELFORMAT;
  ddscaps: TDDSCAPS2;
  hr: HRESULT;
  pClipper: IDIRECTDRAWCLIPPER;
  // Using a color key will clip the overlay
  // when the mouse or other windows go on top of us.
  dwDDSColor: DWORD;

  // The color key can be any color, but a near black (not exactly) allows
  // the cursor to move around on the window without showing off the
  // color key, and also clips windows with exactly black text.
  frontSurface: TSurface;
  animateSurface: TSurface;
begin
  // Release any previous surfaces
  FpDDSOverlay := nil;

  // Set the overlay format to 16 bit RGB 5:6:5
  ZeroMemory(@ddpfOverlayFormat, sizeof(ddpfOverlayFormat));
  ddpfOverlayFormat.dwSize := sizeof(ddpfOverlayFormat);
  ddpfOverlayFormat.dwFlags := DDPF_RGB;
  ddpfOverlayFormat.dwRGBBitCount := 16;
  ddpfOverlayFormat.dwRBitMask := $F800;
  ddpfOverlayFormat.dwGBitMask := $07E0;
  ddpfOverlayFormat.dwBBitMask := $001F;

  // Setup the overlay surface's attributes in the surface descriptor
  ZeroMemory(@ddsd, sizeof(ddsd));
  ddsd.dwSize := sizeof(ddsd);
  ddsd.dwFlags := DDSD_CAPS or DDSD_HEIGHT or DDSD_WIDTH or
    DDSD_BACKBUFFERCOUNT or DDSD_PIXELFORMAT;
  ddsd.ddsCaps.dwCaps := DDSCAPS_OVERLAY or DDSCAPS_FLIP or
    DDSCAPS_COMPLEX or DDSCAPS_VIDEOMEMORY;
  ddsd.dwBackBufferCount := 1;
  ddsd.dwWidth := SCREEN_WIDTH;
  ddsd.dwHeight := SCREEN_HEIGHT;
  ddsd.ddpfPixelFormat := ddpfOverlayFormat; // Use 16 bit RGB 5:6:5 pixel format

  // Attempt to create the surface with theses settings
  hr := FpDD.CreateSurface(ddsd, FpDDSOverlay, nil);
  if (hr <> DD_OK) then
  begin
    result := hr;
    exit;
  end;

  ZeroMemory(@ddscaps, sizeof(ddscaps));
  ddscaps.dwCaps := DDSCAPS_BACKBUFFER;
  hr := FpDDSOverlay.GetAttachedSurface(ddscaps, FpDDSOverlayBack);
  if (hr <> DD_OK) then
  begin
    result := hr;
    exit;
  end;

  // Setup effects structure
  ZeroMemory(@FOverlayFX, sizeof(FOverlayFX));
  FOverlayFX.dwSize := sizeof(FOverlayFX);

  // Setup overlay flags.
  FdwOverlayFlags := DDOVER_SHOW;

  // Check for destination color keying capability
  if (Fddcaps.dwCKeyCaps and DDCKEYCAPS_DESTOVERLAY) <> 0 then
  begin
    frontSurface := TSurface.Create;
    try
      frontSurface.CreateSurface(FpDDSPrimary);
      dwDDSColor := frontSurface.ConvertGDIColor(FdwBackgroundColor);
    finally
      frontSurface.Free;
    end;
    FOverlayFX.dckDestColorkey.dwColorSpaceLowValue := dwDDSColor;
    FOverlayFX.dckDestColorkey.dwColorSpaceHighValue := dwDDSColor;
    FdwOverlayFlags := FdwOverlayFlags or DDOVER_DDFX or
      DDOVER_KEYDESTOVERRIDE;
  end
  else
  begin
    pClipper := nil;

    // If not, we'll setup a clipper for the window.  This will fix the
    // problem on a few video cards - but the ones that don't shouldn't
    // care.
    hr := FpDD.CreateClipper(0, pClipper, nil);
    if (hr <> DD_OK) then
    begin
      result := hr;
      exit;
    end;

    hr := pClipper.SetHWnd(0, Handle);
    if (hr <> DD_OK) then
    begin
      result := hr;
      exit;
    end;

    hr := FpDDSPrimary.SetClipper(pClipper);
    if (hr <> DD_OK) then
    begin
      result := hr;
      exit;
    end;

    pClipper := nil;
  end;

  ZeroMemory(@ddsd, sizeof(ddsd));
  ddsd.dwSize := sizeof(ddsd);
  ddsd.dwFlags := DDSD_CAPS or DDSD_HEIGHT or DDSD_WIDTH or DDSD_PIXELFORMAT;
  ddsd.ddsCaps.dwCaps := DDSCAPS_OFFSCREENPLAIN;
  ddsd.dwWidth := SPRITE_DIAMETER * 5;
  ddsd.dwHeight := SPRITE_DIAMETER * 6;
  ddsd.ddpfPixelFormat := ddpfOverlayFormat; // Use 16 bit RGB 5:6:5 pixel format

  // Attempt to create the surface with theses settings
  hr := FpDD.CreateSurface(ddsd, FpDDSAnimationSheet, nil);
  if (hr <> DD_OK) then
  begin
    result := hr;
    exit;
  end;

  animateSurface := TSurface.Create;
  try
    animateSurface.CreateSurface(FpDDSAnimationSheet);
    hr := animateSurface.DrawBitmap('../xmedia/animate.bmp'{MAKEINTRESOURCE(IDB_ANIMATE_SHEET)},
      SPRITE_DIAMETER * 5, SPRITE_DIAMETER * 6);
    if (hr <> DD_OK) then
    begin
      result := hr;
      exit;
    end;
  finally
    animateSurface.Free;
  end;

  result := DD_OK;
end;

procedure TFormMain.WMDisplayChange(var aMsg: TMessage);
begin
  // This not only checks for overlay support in the new video mode -
  // but gets the new caps for the new display settings.  That way we
  // have more accurate info about min/max stretch factors, color
  // keying
  if (not HasOverlaySupport) then
  begin
    MessageBox(Handle, 'You have changed your adapter settings such ' +
      ' that you no longer support this overlay.', 'Overlay Error', MB_OK);
    PostMessage(Handle, WM_CLOSE, 0, 0);
  end;
  aMsg.result := 0;
end;

end.
