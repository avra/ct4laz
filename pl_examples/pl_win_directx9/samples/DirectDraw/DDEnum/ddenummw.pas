unit DDEnummw;

{$MODE Delphi}

interface

uses
  windows,
  Classes,
  StdCtrls,
  SysUtils,
  Controls,
  Forms,
  DirectDraw;

const
  MAX_DEVICES = 16;

type
  TDeviceInfo = packed record
    DeviceInfo : TDDDeviceIdentifier2;
    DeviceInfoHost : TDDDeviceIdentifier2;
  end;

type
  TFormMain = class( TForm )
    LabelShow : TLabel;
    RadioButtonDevice : TRadioButton;
    RadioButtonHost : TRadioButton;
    LabelDescription : TLabel;
    LabelDescriptionOut : TLabel;
    LabelGUID : TLabel;
    LabelGUIDOut : TLabel;
    GroupBoxDriver : TGroupBox;
    LabelVersion : TLabel;
    LabelVersionOut : TLabel;
    LabelFileName : TLabel;
    LabelFileNameOut : TLabel;
    GroupBoxDevice : TGroupBox;
    LabelVendorID : TLabel;
    LabelVendorIDOut : TLabel;
    LabelDeviceID : TLabel;
    LabelDeviceIDOut : TLabel;
    LabelSubSysID : TLabel;
    LabelSubSysIDOut : TLabel;
    LabelRevision : TLabel;
    LabelRevisionOut : TLabel;
    GroupBoxLevel : TGroupBox;
    LabelLevel : TLabel;
    LabelLevelOut : TLabel;
    ButtonPrev : TButton;
    ButtonClose : TButton;
    ButtonNext : TButton;
    procedure FormCreate( Sender : TObject );
    procedure ButtonPrevClick( Sender : TObject );
    procedure ButtonCloseClick( Sender : TObject );
    procedure FormShow( Sender : TObject );
    procedure ButtonNextClick( Sender : TObject );
    procedure RadioButtonDeviceClick( Sender : TObject );
    procedure RadioButtonHostClick( Sender : TObject );
  private
    FDeviceInfo : array[ 0..MAX_DEVICES - 1 ] of TDeviceInfo;
    FMaxDevices : Integer;
    FCurrent : Integer;
    FHost : Integer;
    procedure SetInfoDlgText;
    procedure ErrorOut( hRet : HRESULT; FuncName : string );
  public
    function FormEnumCallback( lpGUID : PGUID; lpDriverDescription : PAnsiChar; lpDriverName : PAnsiChar; Monitor : HMonitor ) : Integer;
  end;

function DDEnumCallbackEx( lpGUID : PGUID; lpDriverDescription : PAnsiChar; lpDriverName : PAnsiChar; lpContext : Pointer; Monitor : HMonitor ) : BOOL; stdcall;

var
  FormMain : TFormMain;

implementation

{$R *.lfm}

{-----------------------------------------------------------------------------
   Name: TFormMain.SetInfoDlgText
   Desc: Update dialog
-----------------------------------------------------------------------------}

procedure TFormMain.SetInfoDlgText;
var
  OutString : string;
  di : TDDDeviceIdentifier2;
  Ver : ULarge_Integer;
begin
  if FHost = DDGDI_GETHOSTIDENTIFIER then
  begin
    RadioButtonDevice.Checked := True;
    di := FDeviceInfo[ FCurrent ].DeviceInfo;
  end
  else
  begin
    RadioButtonHost.Checked := True;
    di := FDeviceInfo[ FCurrent ].DeviceInfoHost;
  end;

  OutString := Format( 'Device information for device %d of %d', [ FCurrent + 1, FMaxDevices ] );
  RadioButtonDevice.Caption := OutString;

  // Device ID stuff
  OutString := Format( '%8.8x', [ di.dwVendorId ] );
  LabelVendorIDOut.Caption := OutString;
  OutString := Format( '%8.8x', [ di.dwDeviceId ] );
  LabelDeviceIDOut.Caption := OutString;
  OutString := Format( '%8.8x', [ di.dwSubSysId ] );
  LabelSubSysIDOut.Caption := OutString;
  OutString := Format( '%8.8x', [ di.dwRevision ] );
  LabelRevisionOut.Caption := OutString;

  // Driver version
  Ver := ULarge_Integer( di.liDriverVersion );
  OutString := Format( '%d.%2.2d.%2.2d.%4.4d', [
    HIWORD( Ver.HighPart ),
      LOWORD( Ver.HighPart ),
      HIWORD( Ver.LowPart ),
      LOWORD( Ver.LowPart ) ] );
  LabelVersionOut.Caption := OutString;

  //Device description and HAL filename
  LabelDescriptionOut.Caption := string( di.szDescription );
  LabelFileNameOut.Caption := string( di.szDriver );

  //Unique driver/device identifier:
  with di.guidDeviceIdentifier do
  begin
    OutString := Format( '{%8.8x-%4.4x-%4.4x-%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x}',
      [ D1, D2, D3, D4[ 0 ], D4[ 1 ], D4[ 2 ], D4[ 3 ], D4[ 4 ], D4[ 5 ],
      D4[ 6 ], D4[ 7 ] ] );
    LabelGUIDOut.Caption := OutString;
  end;

  //WHQL Level
  OutString := Format( '%8.8x', [ di.dwWHQLLevel ] );
  LabelLevelOut.Caption := OutString;

  // Change the state and style of the Prev and Next buttons if needed
  if FCurrent <= 0 then
  begin
    ButtonClose.SetFocus;
    ButtonPrev.Enabled := False
  end
  else
  begin
    ButtonPrev.Enabled := True;
  end;

  if FCurrent >= FMaxDevices - 1 then
  begin
    ButtonClose.SetFocus;
    ButtonNext.Enabled := False
  end
  else
  begin
    ButtonNext.Enabled := True;
  end;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.ErrorOut
   Desc: Format and display errors
-----------------------------------------------------------------------------}

procedure TFormMain.ErrorOut( hRet : HRESULT; FuncName : string );
var
  OutString : string;
begin
  OutString := FuncName + ': ' + #13 + DDErrorString( hRet );
  MessageBox( 0, PChar( OutString ), PChar( Caption ), MB_OK or MB_ICONSTOP );
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormCreate
   Desc: Init variables
-----------------------------------------------------------------------------}

procedure TFormMain.FormCreate( Sender : TObject );
begin
  FCurrent := 0;
  FHost := DDGDI_GETHOSTIDENTIFIER;
  FMaxDevices := 0;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.FormShow
   Desc: Start enumeration
-----------------------------------------------------------------------------}

procedure TFormMain.FormShow( Sender : TObject );
var
  hRet : HRESULT;
begin
  hRet := DirectDrawEnumerateEx( DDEnumCallbackEx, Self, DDENUM_ATTACHEDSECONDARYDEVICES or
    DDENUM_DETACHEDSECONDARYDEVICES or DDENUM_NONDISPLAYDEVICES );
  if hRet <> DD_OK then
  begin
    ErrorOut( hRet, 'DirectDrawEnumerateEx' );
    Close;
  end;
  SetInfoDlgText;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.ButtonCloseClick
   Desc: Close form
-----------------------------------------------------------------------------}

procedure TFormMain.ButtonCloseClick( Sender : TObject );
begin
  Close;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.ButtonPrevClick
   Desc: Display previous device
-----------------------------------------------------------------------------}

procedure TFormMain.ButtonPrevClick( Sender : TObject );
begin
  if FCurrent > 0 then
  begin
    FCurrent := FCurrent - 1;
  end;
  SetInfoDlgText;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.ButtonNextClick
   Desc: Display next device
-----------------------------------------------------------------------------}

procedure TFormMain.ButtonNextClick( Sender : TObject );
begin
  if FCurrent < FMaxDevices then
  begin
    FCurrent := FCurrent - 1;
  end;
  SetInfoDlgText;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.RadioButtonDeviceClick
   Desc: Switch device
-----------------------------------------------------------------------------}

procedure TFormMain.RadioButtonDeviceClick( Sender : TObject );
begin
  FHost := DDGDI_GETHOSTIDENTIFIER;
  SetInfoDlgText;
end;

{-----------------------------------------------------------------------------
   Name: TFormMain.RadioButtonHostClick
   Desc: Switch device
-----------------------------------------------------------------------------}

procedure TFormMain.RadioButtonHostClick( Sender : TObject );
begin
  FHost := 0;
  SetInfoDlgText;
end;

{-----------------------------------------------------------------------------
  Name: TFormMain.FormEnumCallback
  Desc: Enumerate devices
-----------------------------------------------------------------------------}

function TFormMain.FormEnumCallback( lpGUID : PGUID; lpDriverDescription : PAnsiChar; lpDriverName : PAnsiChar; Monitor : HMonitor ) : Integer;
var
  pDD : IDirectDraw;
  pDD7 : IDirectDraw7;
  hRet : HRESULT;
begin
  // Create the main DirectDraw object
  hRet := DirectDrawCreate( lpGUID, pDD, nil );
  if hRet <> DD_OK then
  begin
    ErrorOut( hRet, 'DirectDrawCreate' );
    Result := DDENUMRET_CANCEL;
    Exit;
  end;

  // Fetch DirectDraw7 interface
  hRet := pDD.QueryInterface( IDirectDraw7, pDD7 );
  if hRet <> DD_OK then
  begin
    ErrorOut( hRet, 'QueryInterface' );
    Result := DDENUMRET_CANCEL;
    Exit;
  end;

  // Get the device information and save it
  pDD7.GetDeviceIdentifier( FDeviceInfo[ FMaxDevices ].DeviceInfo, 0 );
  pDD7.GetDeviceIdentifier( FDeviceInfo[ FMaxDevices ].DeviceInfoHost, DDGDI_GETHOSTIDENTIFIER );

  // Bump to the next open slot or finish the callbacks if full
  if FMaxDevices < MAX_DEVICES then
  begin
    inc( FMaxDevices );
  end
  else
  begin
    Result := DDENUMRET_CANCEL;
    Exit;
  end;
  Result := DDENUMRET_OK;
end;

{-----------------------------------------------------------------------------
   Name: DDEnumCallbackEx
   Desc: Enumerate devices, calls TFormMain.FormEnumCallback
-----------------------------------------------------------------------------}

function DDEnumCallbackEx( lpGUID : PGUID; lpDriverDescription : PAnsiChar; lpDriverName : PAnsiChar; lpContext : Pointer; Monitor : HMonitor ) : BOOL;
var
  EnumForm : TFormMain;
begin
  EnumForm := TFormMain( lpContext );
  Result := LongBool( EnumForm.FormEnumCallback( lpGUID, lpDriverDescription, lpDriverName, Monitor ) );
end;

end.
