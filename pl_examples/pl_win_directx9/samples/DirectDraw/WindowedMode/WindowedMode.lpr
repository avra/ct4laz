program WindowedMode;

{$MODE Delphi}

{******************************************************************}
{                                                                  }
{       Borland Delphi DirectX 8 SDK Examples                      }
{       Conversion of the Windowed Mode Example                    }
{                                                                  }
{ Portions created by Microsoft are                                }
{ Copyright (C) 1995-2000 Microsoft Corporation.                   }
{ All Rights Reserved.                                             }
{                                                                  }
{ The original files are : WindowedMode.h,                         }
{                          WindowedMode.cpp                        }
{ The original Pascal code is : WindowedMode_vcl.pas               }
{ The initial developer of the Pascal code is : Dominique Louis    }
{ ( Dominique@SavageSoftware.com.au )                              }
{                                                                  }
{ Portions created by Dominique Louis are                          }
{ Copyright (C) 2000 - 2001 Dominique Louis.                       }
{                                                                  }
{ Contributor(s)                                                   }
{ --------------                                                   }
{ <Contributer Name> ( contributer@sp.sw )                         }
{                                                                  }
{ Obtained through:                                                }
{ Joint Endeavour of Delphi Innovators ( Project JEDI )            }
{                                                                  }
{ You may retrieve the latest version of this file at the Project  }
{ JEDI home page, located at http://delphi-jedi.org                }
{                                                                  }
{ The contents of this file are used with permission, subject to   }
{ the Mozilla Public License Version 1.1 (the "License"); you may  }
{ not use this file except in compliance with the License. You may }
{ obtain a copy of the License at                                  }
{ http://www.mozilla.org/NPL/NPL-1_1Final.html                     }
{                                                                  }
{ Software distributed under the License is distributed on an      }
{ "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or   }
{ implied. See the License for the specific language governing     }
{ rights and limitations under the License.                        }
{                                                                  }
{ Description                                                      }
{ -----------                                                      }
{   WindowedMode demonstrates the tasks required to initialize     }
{   and run a windowed DirectDraw application.                     }
{                                                                  }
{ Requires                                                         }
{ --------                                                         }
{   Eric Unger and Tim Baumgarten's conversions of the DirectX     }
{   headers. They are available from...                            }
{   http://delphi-jedi.org/DelphiGraphics/ .                       }
{                                                                  }
{                                                                  }
{ Programming Notes                                                }
{ -----------------                                                }
{   The basic tasks to author a simple windowed DirectDraw         }
{   application are as follows:                                    }
{                                                                  }
{   Initialize DirectDraw:                                         }
{     1. Register a window class and create a window.              }
{     2. Call DirectDrawCreateEx to create a DirectDraw object     }
{     3. Call SetCooperativeLevel to set the DirectDraw cooperative}
{        level to normal.                                          }
{     4. Call CreateSurface to obtain a pointer to the primary     }
{        surface.  In windowed mode, the primary surface is also   }
{        the desktop window.  So the pixel format for the primary  }
{        surface is based on the user's selection of display       }
{        resolution.                                               }
{     5. Create a back-buffer.  In windowed mode, this is should   }
{        be an off-screen plain buffer. The back buffer is         }
{        typically the same size as the window's client rect, but  }
{        could be created with any size.                           }
{     6. Test to see if the display mode is in palettized color    }
{        (8-bit or less).                                          }
{        If it is then a palette needs to be created. This sample  }
{        displays a single bitmap so it can read the bitmap        }
{        palette info to read and create a DirectDraw              }
{        palette.  After a palette is created, call SetPalette to  }
{        set the palette for the primary surface.                  }
{     7. Call CreateClipper and SetClipper to create a clipper     }
{        object, and attach it                                     }
{        to the primary surface.  This will keep DirectDraw from   }
{        blting on top of any windows which happen to overlap.     }
{     8. Create an off-screen plain DirectDraw surface, and load   }
{        media content into it. For example, this sample calls     }
{        DDUtil_CreateSurfaceFromBitmap() to do just this.         }
{                                                                  }
{   When the app is idle, and it is not hidden or minimized then   }
{   render the next frame by:                                      }
{     1. If movement or animation is involved in the app, then     }
{        calculate how much time has passed since the last time    }
{        the frame was displayed.                                  }
{     2. Move or animate the app state based on how much time has  }
{        passed.                                                   }
{     3. Draw the current state into the off-screen plain          }
{        backbuffer.                                               }
{     4. Call Blt to blt the contents of the off-screen plain      }
{        backbuffer into the primary surface.                      }
{                                                                  }
{   If the display resolution changes or an exclusive mode         }
{   DirectDraw app is run, then                                    }
{   the DirectDraw surface may be lost ( resulting in a DirectDraw }
{   call returning DDERR_SURFACELOST ), then handle it by:         }
{     1. Call RestoreAllSurfaces to have DirectDraw restore all    }
{        the surfaces.                                             }
{     2. Restoring a surface doesn't reload any content that       }
{        existed in the surface prior to it being lost. So you     }
{        must now redraw the graphics the surfaces once held. For  }
{        example, this sample handles this by calling              }
{        DDUtil_ReDrawBitmapOnDDS()                                }
{                                                                  }
{   In windowed mode, handle the following windows messages:       }
{     1. WM_PAINT: This is sent when all or a part of the window   }
{        is needs to be redrawn.  The app may not be active at the }
{        time this is called, so if this is not handled then the   }
{        window will appear blank.  To avoid this, make a call to  }
{        draw the next frame here.                                 }
{     2. WM_QUERYNEWPALETTE: This is sent when in a 8-bit desktop  }
{        and the another window set a new palette, but now this    }
{        window has control so it needs to reset its palette.  The }
{        easy way to make this happen in DirectDraw is just to     }
{        call SetPalette.  This will force DirectDraw to realize   }
{        the DirectDrawPalette attached to it.                     }
{     3. WM_MOVE: This is sent when the window is moving.  Record  }
{        the new window position here since the blt to the primary }
{        surface needs to know the window position.                }
{                                                                  }
{ Revision History                                                 }
{ ----------------                                                 }
{   2000-12-01 - DL  Initial translation.                          }
{                                                                  }
{                                                                  }
{******************************************************************}

uses
  Forms, Interfaces,
  WindowedModemw in 'MainForm.pas' {FormMain},
  DDUtil in '..\..\Common\DDUtil.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm( TFormMain, FormMain );
  Application.Run;
end.
