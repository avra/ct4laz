unit mddemo2mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  ExtCtrls, SynEditTypes, cmdrunner;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ComboBox1: TComboBox;
    ImageList1: TImageList;
    Panel1: TPanel;
    SaveDialog1: TSaveDialog;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    vbtClear: TToolButton;
    vbtClose: TToolButton;
    vbtCopy: TToolButton;
    vbtFindDown: TToolButton;
    vbtFindUp: TToolButton;
    vbtFontDown: TToolButton;
    vbtFontUp: TToolButton;
    vbtSave: TToolButton;
    vSelRunCommandBoxMethod: TComboBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure vbtClearClick(Sender: TObject);
    procedure vbtCloseClick(Sender: TObject);
    procedure vbtCopyClick(Sender: TObject);
    procedure vbtFindDownClick(Sender: TObject);
    procedure vbtFindUpClick(Sender: TObject);
    procedure vbtFontDownClick(Sender: TObject);
    procedure vbtFontUpClick(Sender: TObject);
    procedure vbtSaveClick(Sender: TObject);
  private
    FSynEditRunner:TCmdSynEditRunner;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  FSynEditRunner:=TCmdSynEditRunner.Create(self);
  FSynEditRunner.Parent:=self;
  FSynEditRunner.Align:=alClient;

  vSelRunCommandBoxMethod.Items.Assign(FSynEditRunner.Runner.CmdRunMethods);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  FSynEditRunner.Runner.CmdRunMethod:=vSelRunCommandBoxMethod.ItemIndex;
  FSynEditRunner.Runner.API_RunFileInCommandBox('Timed.exe','','5','',false);
end;

procedure TForm1.Button2Click(Sender: TObject);
 var
   sse,ssd:string;
begin
  FSynEditRunner.Runner.CmdRunMethod:=vSelRunCommandBoxMethod.ItemIndex;

{$IFDEF WINDOWS}
   ssd:='..\xmedia\';
   sse:='xxw_test1.bat';
{$ELSE}
   ssd:='../xmedia/';
   sse:='xxu_test1.sh';
{$ENDIF}
  FSynEditRunner.Runner.API_RunFileInCommandBox(ssd+sse,ssd,'','',True);
end;

//===================================================

procedure TForm1.vbtClearClick(Sender: TObject);
begin
  FSynEditRunner.Clear;
end;

procedure TForm1.vbtCloseClick(Sender: TObject);
begin
  close;
end;

procedure TForm1.vbtCopyClick(Sender: TObject);
 begin
   if FSynEditRunner.selstart=FSynEditRunner.SelEnd then
     begin
     FSynEditRunner.SelectAll;
     FSynEditRunner.CopyToClipboard;
     FSynEditRunner.SelEnd:=FSynEditRunner.selstart;
     end else
     FSynEditRunner.CopyToClipboard;
 end;

procedure TForm1.vbtFindDownClick(Sender: TObject);
begin  
  FSynEditRunner.SearchReplaceEx(ComboBox1.Text,ComboBox1.Text,[],FSynEditRunner.CaretXY);
end;

procedure TForm1.vbtFindUpClick(Sender: TObject);
begin   
  FSynEditRunner.SearchReplaceEx(ComboBox1.Text,ComboBox1.Text,[ssoBackwards],FSynEditRunner.CaretXY);
end;

procedure TForm1.vbtFontDownClick(Sender: TObject);
begin
   if FSynEditRunner.Font.Size=2 then exit;
   FSynEditRunner.Font.Size:=FSynEditRunner.Font.Size-1;
end;

procedure TForm1.vbtFontUpClick(Sender: TObject);
begin 
   if FSynEditRunner.Font.Size=24 then exit;
   FSynEditRunner.Font.Size:=FSynEditRunner.Font.Size+1;
end;

procedure TForm1.vbtSaveClick(Sender: TObject);
begin 
 if SaveDialog1.Execute then
  begin
    FSynEditRunner.Lines.SaveToFile(SaveDialog1.FileName);
  end;

end;

end.

