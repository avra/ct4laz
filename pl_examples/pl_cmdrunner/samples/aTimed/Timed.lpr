
program Timed;

{$MODE Delphi}

{
  Application that runs for the number of seconds specified as the first
  parameter on the command line. If no parameter is specified the application
  runs for 5 seconds.

  A full stop is written to standard output approx every 1/10th second followed
  by "Done" when it completes.

  Program exit code is always 0.

  Usage:
    Timed [time-to-run]
  Eg: to run the application for 6 seconds use
    Timed 6
}

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  TimeToRun: Integer; // time program is to run for in ms
  StartTick: Integer; // tick count when program starts
  TickNow: Integer;   // tick count during each program loops

{$R *.res}

begin
  TimeToRun := 1000 * StrToIntDef(ParamStr(1), 5);
  ExitCode := 0;
  WriteLn('TIMED: Running for ', TimeToRun div 1000, ' seconds');
  WriteLn(' ');
  StartTick := GetTickCount64;
  repeat
    TickNow := GetTickCount64;
    Sleep(100);
    Write('.');
    Writeln('//');
  until TickNow - StartTick >= TimeToRun;
  WriteLn;
  WriteLn('Done');
end.

