unit OBJLoader;

interface

uses ctGL, Windows, SysUtils {,Textures};

type
  TOBJFarbe = record
    R, G, B: glFloat;
  end;
  TOBJVertex = record
    X, Y, Z: glFLoat;
  end;
  TTexCoord = record
    U, V: glFloat;
  end;

  TMaterial = record
    Name: string;
    Ambient, Diffuse, Specular: TOBJFarbe;
    Shininess: glFloat;
    Texture: glUint;
  end;

  TFace = record
    Count: INTEGER;
    vIndex, tIndex, nIndex: array of INTEGER; {Vektoren, Texturen, Normalen}
  end;

  TGroup = record
    Name: string;
    Faces: INTEGER;
    Face: array of TFace;
    mIndex: INTEGER; {Zeiger aufs Material}
  end;

  TOBJModell = class
  private
    Name, MaterialFile: string;
    Vertices, Normals, TexCoords, Groups, Materials: INTEGER;
    function HoleToken(var EingabeStr: string; const Teiler: CHAR): string;
    procedure InitModell;
    function HoleXYZ(S: string): TOBJVertex;
    function HoleTexUV(S: string): TTexCoord;
    procedure LeseVektoren(S: string);
    procedure LeseFaces(V: string);
    procedure HoleMaterialName(S: string);
    procedure ErzeugeMaterial(S: string);
    procedure HoleMaterial(S: string);
    procedure HoleShininess(S: string);
    procedure HoleTextur(S: string);
    procedure LadeMaterialen(S: string);
    procedure BerechneMinMax;
  public
    Scale: SINGLE;
    Vertex, Normal: array of TOBJVertex;
    TexCoord: array of TTexCoord;
    Group: array of TGroup;
    Material: array of TMaterial;
    constructor Create;
    destructor Destroy; override;
    function LoadModel(Dateiname: string): boolean;
    procedure DrawModel;
  end;

implementation

procedure glBindTexture(target: GLenum; texture: GLuint); stdcall; external
  opengl32;

constructor TOBJModell.Create;
begin
  inherited Create;
  InitModell;
end;

destructor TOBJModell.Destroy;
begin
  InitModell;
  inherited Destroy;
end;

function TOBJModell.HoleToken(var EingabeStr: string; const Teiler: CHAR):
  string;
var
  I: INTEGER;
begin
  I := 1;
  while (I <= LENGTH(EingabeStr)) and not (EingabeStr[I] = Teiler) do
    INC(I);
  RESULT := COPY(EingabeStr, 1, I - 1);
  DELETE(EingabeStr, 1, I);
end;

procedure TOBJModell.InitModell;
begin
  Name := '';
  MaterialFile := '';
  Vertices := 0;
  Normals := 0;
  TexCoords := 0;
  Groups := 0;
  Materials := 0;
  SETLENGTH(Vertex, 0);
  SETLENGTH(Normal, 0);
  SETLENGTH(TexCoord, 0);
  SETLENGTH(Group, 0);
  SETLENGTH(Material, 0);
end;

function TOBJModell.HoleXYZ(S: string): TOBJVertex;
var
  C: TOBJVertex;
begin
  S := TRIM(COPY(S, 3, LENGTH(S)));
  S := STRINGREPLACE(S, '.', DecimalSeparator, [rfReplaceAll]);
  C.X := STRTOFLOAT(HoleToken(S, ' '));
  C.Y := STRTOFLOAT(HoleToken(S, ' '));
  C.Z := STRTOFLOAT(HoleToken(S, ' '));
  RESULT := C;
end;

function TOBJModell.HoleTexUV(S: string): TTexCoord;
var
  T: TTexCoord;
begin
  S := TRIM(COPY(S, 3, LENGTH(S)));
  S := STRINGREPLACE(S, '.', DecimalSeparator, [rfReplaceAll]);
  T.U := STRTOFLOAT(HoleToken(S, ' '));
  T.V := STRTOFLOAT(HoleToken(S, ' '));
  RESULT := T;
end;

procedure TOBJModell.LeseVektoren(S: string);
var
  C: TOBJVertex;
  T: TTexCoord;
begin
  case S[2] of
    ' ':
      begin
        C := HoleXYZ(S);
        INC(Vertices);
        SETLENGTH(Vertex, Vertices + 1);
        Vertex[Vertices] := C;
      end;
    'N':
      begin
        C := HoleXYZ(S);
        INC(Normals);
        SETLENGTH(Normal, Normals + 1);
        Normal[Normals] := C;
      end;
    'T':
      begin
        T := HoleTexUV(S);
        INC(TexCoords);
        SETLENGTH(TexCoord, TexCoords + 1);
        TexCoord[TexCoords] := T;
      end;
  end;
end;

procedure TOBJModell.LeseFaces(V: string);
var
  P: INTEGER;
  F: TFace;
  S, A, L: string;
begin
  P := POS(' ', V);
  S := TRIM(COPY(V, P + 1, LENGTH(V)));

  INC(Group[Groups].Faces);
  SETLENGTH(Group[Groups].Face, Group[Groups].Faces + 1);

  F.Count := 0;
  while LENGTH(S) > 0 do
  begin
    A := HoleToken(S, ' ');

    P := POS('/', A);
    if P > 0 then
    begin
      INC(F.Count);
      SETLENGTH(F.vIndex, F.Count);
      SETLENGTH(F.tIndex, F.Count);
      SETLENGTH(F.nIndex, F.Count);

      L := HoleToken(A, '/');
      if L = '' then
        L := '0';
      try
        F.vIndex[F.Count - 1] := STRTOINT(L);
      except
        F.vIndex[F.Count - 1] := 0;
      end;

      L := HoleToken(A, '/');
      if L = '' then
        L := '0';
      try
        F.tIndex[F.Count - 1] := STRTOINT(L);
      except
        F.tIndex[F.Count - 1] := 0;
      end;

      L := HoleToken(A, '/');
      if L = '' then
        L := '0';
      try
        F.nIndex[F.Count - 1] := STRTOINT(L);
      except
        F.nIndex[F.Count - 1] := 0;
      end;
    end
    else
    begin
      INC(F.Count);
      SETLENGTH(F.vIndex, F.Count);
      F.vIndex[F.Count - 1] := STRTOINT(A);
    end;
  end;
  Group[Groups].Face[Group[Groups].Faces] := F;
end;

procedure TOBJModell.HoleMaterialName(S: string);
var
  I: INTEGER;
begin
  S := TRIM(S);
  if COPY(S, 1, 6) <> 'USEMTL' then
    EXIT;
  DELETE(S, 1, POS(' ', S));
  for I := 1 to Materials do
    if Material[I].Name = S then
      Group[Groups].mIndex := I;
end;

procedure TOBJModell.ErzeugeMaterial(S: string);
begin
  if COPY(S, 1, 6) <> 'NEWMTL' then
    EXIT;
  INC(Materials);
  SETLENGTH(Material, Materials + 1);
  S := TRIM(COPY(S, 7, LENGTH(S)));
  FILLCHAR(Material[Materials].Ambient, 0, Sizeof(Material[Materials].Ambient));
  FILLCHAR(Material[Materials].Diffuse, 0, Sizeof(Material[Materials].Diffuse));
  FILLCHAR(Material[Materials].Specular, 0,
    Sizeof(Material[Materials].Specular));
  Material[Materials].Shininess := 60;
  Material[Materials].Texture := 0;
  Material[Materials].Name := S;
end;

procedure TOBJModell.HoleMaterial(S: string);
var
  C: TOBJFarbe;
  Ch: CHAR;
begin
  Ch := S[2];
  S := TRIM(COPY(S, 3, LENGTH(S)));
  S := STRINGREPLACE(S, '.', DecimalSeparator, [rfReplaceAll]);

  C.R := STRTOFLOAT(HoleToken(S, ' '));
  C.G := STRTOFLOAT(HoleToken(S, ' '));
  C.B := STRTOFLOAT(HoleToken(S, ' '));

  case CH of
    'A': Material[Materials].Ambient := C;
    'D': Material[Materials].Diffuse := C;
    'S': Material[Materials].Specular := C;
  end;
end;

procedure TOBJModell.HoleShininess(S: string);
begin
  S := TRIM(COPY(S, 3, LENGTH(S)));
  S := STRINGREPLACE(S, '.', DecimalSeparator, [rfReplaceAll]);
  Material[Materials].Shininess := STRTOFLOAT(S);
end;

procedure TOBJModell.HoleTextur(S: string);
begin
  S := TRIM(COPY(S, 3, LENGTH(S)));
  S := STRINGREPLACE(S, '/', '\', [rfReplaceAll]);
  // LadeTextur(s,Material[Materials].Texture);
end;

procedure TOBJModell.LadeMaterialen(S: string);
var
  Dateiname: string;
  F: file;
  C: CHAR;
begin
  S := TRIM(S);
  if COPY(S, 1, 6) <> 'MTLLIB' then
    EXIT;
  DELETE(S, 1, 6);
  Dateiname := TRIM(S);
  if FileExists(Dateiname) then
  begin
    ASSIGNFILE(F, Dateiname);
    RESET(F, 1);
    while not EOF(F) do
    begin
      C := #0;
      S := '';
      while (C <> #10) and not EOF(F) do
      begin
        BLOCKREAD(F, C, SIZEOF(CHAR));
        if not (C in [#10, #13]) then
        begin
          S := S + C;
        end;
      end;
      S := UPPERCASE(TRIM(S));
      if (S <> '') and (S[1] <> '#') then
      begin
        case S[1] of
          'N':
            begin
              if S[2] = 'S' then
                HoleShininess(S);
              if S[2] = 'E' then
                ErzeugeMaterial(S);
            end;
          'K': HoleMaterial(S);
          'M': HoleTextur(S);
        end;
      end;
    end;
    CLOSEFILE(F);
  end; // ELSE MessageBox(0,PChar('Konnte Material Eigenschaften Datei "'+Dateiname+'" finden oder �ffnen!'),PCHAR('OBJ Loader'),MB_OK);
end;

procedure TOBJModell.BerechneMinMax;
var
  MinC, MaxC: TOBJVertex;
  I: INTEGER;
begin
  FILLCHAR(MinC, SIZEOF(TOBJVertex), 0);
  FILLCHAR(MaxC, SIZEOF(TOBJVertex), 0);
  for I := 1 to Vertices do
  begin
    if Vertex[I].X < MinC.X then
    begin
      MinC.X := Vertex[I].X;
    end
    else if Vertex[I].X > MaxC.X then
    begin
      MaxC.X := Vertex[I].X;
    end;
    if Vertex[I].Y < MinC.Y then
    begin
      MinC.Y := Vertex[I].Y;
    end
    else if Vertex[I].Y > MaxC.Y then
    begin
      MaxC.Y := Vertex[I].Y;
    end;
    if Vertex[I].Z < MinC.Z then
    begin
      MinC.Z := Vertex[I].Z;
    end
    else if Vertex[I].Z > MaxC.Z then
    begin
      MaxC.Z := Vertex[I].Z;
    end;
  end;
  MaxC.X := MaxC.X - MinC.X;
  MaxC.Y := MaxC.Y - MinC.Y;
  MaxC.Z := MaxC.Z - MinC.Z;
  if MaxC.X > MaxC.Y then
    Scale := MaxC.X
  else
    Scale := MaxC.Y;
  if Scale > MaxC.Z then
    Scale := MaxC.Z;
end;

function TOBJModell.LoadModel(Dateiname: string): boolean;
var
  F: file;
  S, S2: string;
  P: INTEGER;
  C: CHAR;
begin
  RESULT := false;
  InitModell;

  P := POS('.', Dateiname) - 1;
  if P < 1 then
    P := LENGTH(Dateiname);
  Name := COPY(Dateiname, 1, P);

  if FileExists(Dateiname) then
  begin
    ASSIGNFILE(F, Dateiname);
    RESET(F, 1);
    while not EOF(F) do
    begin
      C := #0;
      S := '';
      while (C <> #10) and not EOF(F) do
      begin
        BLOCKREAD(F, C, SIZEOF(CHAR));
        if not (C in [#10, #13]) then
        begin
          S := S + C;
        end;
      end;
      //READLN(F,S);
      S := UPPERCASE(TRIM(S));
      if (S <> '') and (S[1] <> '#') then
      begin
        case S[1] of
          'G':
            begin
              INC(Groups);
              SETLENGTH(Group, Groups + 1);
              S2 := TRIM(COPY(S, 2, LENGTH(S)));
              Group[Groups].Name := S2;
            end;
          'V': LeseVektoren(S);
          'F': LeseFaces(S);
          'U': HoleMaterialName(S);
          'M': LadeMaterialen(S);
        end;
      end;
    end;
    CLOSEFILE(F);
    BerechneMinMax;
    RESULT := TRUE;
  end; // ELSE MessageBox(0,PChar('Konnte 3D Modell Datei "'+Dateiname+'" finden oder �ffnen!'),PCHAR('OBJ Loader'),MB_OK);
end;

procedure TOBJModell.DrawModel;
var
  I, J, K: INTEGER;
begin
  for I := 1 to Groups do
  begin
    if Group[I].mIndex < LENGTH(Material) then
    begin
      glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,
        @Material[Group[I].mIndex].Diffuse);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,
        @Material[Group[I].mIndex].Specular);
      glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,
        @Material[Group[I].mIndex].Ambient);
      glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS,
        @Material[Group[I].mIndex].Shininess);
      if Material[Group[I].mIndex].Texture <> 0 then
      begin
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, Material[Group[I].mIndex].Texture);
      end
      else
      begin
        glDisable(GL_TEXTURE_2D);
      end;
    end
    else
    begin
      glDisable(GL_TEXTURE_2D);
    end;
    for J := 1 to Group[I].Faces do
    begin
      with Group[I].Face[J] do
      begin
        case Count of
          3: glBegin(GL_TRIANGLES);
          4: glBegin(GL_QUADS);
        else
          glBegin(GL_POLYGON);
        end;
        for K := 0 to Count - 1 do
        begin
          if Normals > 0 then
            glNormal3fv(@Normal[nIndex[K]]);
          if TexCoords > 0 then
            glTexCoord2fv(@TexCoord[tIndex[K]]);
          glVertex3fv(@Vertex[vIndex[K]]);
        end;
        glEnd();
      end;
    end;
  end;
end;

end.
