UNIT Model3DS;

INTERFACE

{$IFDEF OpenGL}
USES OpenGL;
{$ENDIF}

TYPE T3DSRGB=RECORD
      r,g,b:SINGLE;
     END;

     P3DSMaterial=^T3DSMaterial;
     T3DSMaterial=RECORD
      MatName,MapName:STRING[255];
      AMBIENT,DIFFUSE,SPECULAR:T3DSRGB;
      TRANSPARENZ,SHININESS,SHADING:SINGLE;
      Visibility,MapX,MapY:WORD;
      MapData:POINTER;
      Faces:ARRAY OF LONGWORD;
      Verts:ARRAY OF LONGWORD;
      CountFaces,CountVerts:INTEGER;
     END;

     T3DSSubVertex=RECORD
      X,Y,Z:SINGLE;
     END;

     T3DSSubTexVertex=RECORD
      U,V:SINGLE;
     END;

     T3DSFace=RECORD
      Z:INTEGER;
      L1,L2,L3:WORD;
      ABVis,BCVis,CAVis,UWrap,VWrap:BOOLEAN;
      FaceNormal,FaceTempNormal:T3DSSubVertex;
      MatName:STRING;
      Material:P3DSMaterial;
     END;

     T3DSVertex=RECORD
      Vector,NormalVector:T3DSSubVertex;
      TextureUV:T3DSSubTexVertex;
     END;

     T3DSChunk=RECORD
      ID:WORD;
      Next:LONGWORD;
     END;

     T3DSLoader=CLASS
      PRIVATE
       FDrawListID:LONGWORD;
       PROCEDURE CalculateMinMax;
{$IFDEF OpenGL}
       PROCEDURE DrawCreateList(Lighting:BOOLEAN);
{$ENDIF}
      PUBLIC
       MinC,MaxC,MaxXC:T3DSSubVertex;
       DoCenterise:BOOLEAN;
       MaterialArray:ARRAY OF T3DSMaterial;
       FaceArray:ARRAY OF T3DSFace;
       VertexArray:ARRAY OF T3DSVertex;
       CountMaterial,CountFaces,CountVertices:WORD;
       ObjectsToLoad:ARRAY OF STRING;
       Scale:SINGLE;
       Version:BYTE;
       CONSTRUCTOR Create;
       DESTRUCTOR Destroy; OVERRIDE;
       FUNCTION Load(FileName:STRING;ScaleFactor:SINGLE=1):BOOLEAN;
{$IFDEF OpenGL}
       PROCEDURE Draw(Lighting:BOOLEAN);
       PROCEDURE Render;
{$ENDIF}
     END;

IMPLEMENTATION

FUNCTION MTRIM(S:STRING):STRING;
VAR StartPos,Laenge:INTEGER;
BEGIN
 Laenge:=LENGTH(S);
 IF Laenge>0 THEN BEGIN
  WHILE (Laenge>0) AND (S[Laenge] IN [#0..#32]) DO DEC(Laenge);
  StartPos:=1;
  WHILE (StartPos<=Laenge) AND (S[StartPos] IN [#0..#32]) DO INC(StartPos);
  RESULT:=COPY(S,StartPos,Laenge-StartPos+1);
 END ELSE RESULT:='';
END;

FUNCTION MUPPERCASE(S:STRING):STRING;
VAR I,L:INTEGER;
BEGIN
 RESULT:='';
 L:=LENGTH(S);
 I:=1;
 WHILE I<=L DO BEGIN
  IF S[I] IN ['a'..'Z'] THEN BEGIN
   RESULT:=RESULT+CHAR(BYTE(S[I])-32);
  END ELSE BEGIN
   RESULT:=RESULT+S[I];
  END;
  INC(I);
 END;
END;

CONSTRUCTOR T3DSLoader.Create;
BEGIN
 INHERITED Create;
 SETLENGTH(ObjectsToLoad,1);
 SETLENGTH(MaterialArray,0);
 SETLENGTH(FaceArray,0);
 SETLENGTH(VertexArray,0);
 CountMaterial:=0;
 CountFaces:=0;
 CountVertices:=0;
 ObjectsToLoad[0]:='*';
 FDrawListID:=0;
 DoCenterise:=FALSE;
END;

DESTRUCTOR T3DSLoader.Destroy;
BEGIN
{$IFDEF OpenGL}
 IF FDrawListID>0 THEN glDeleteLists(FDrawListID,0);
{$ENDIF}
 SETLENGTH(ObjectsToLoad,0);
 SETLENGTH(MaterialArray,0);
 SETLENGTH(FaceArray,0);
 SETLENGTH(VertexArray,0);
 INHERITED Destroy;
END;

PROCEDURE T3DSLoader.CalculateMinMax;
VAR I:INTEGER;
BEGIN
 FILLCHAR(MinC,SIZEOF(T3DSSubVertex),0);
 FILLCHAR(MaxC,SIZEOF(T3DSSubVertex),0);
 MinC.X:=16777216;
 MinC.Y:=16777216;
 MinC.Z:=16777216;
 MaxC.X:=-16777216;
 MaxC.Y:=-16777216;
 MaxC.Z:=-16777216;
 FOR I:=0 TO CountVertices-1 DO BEGIN
  IF VertexArray[I].Vector.X<MinC.X THEN BEGIN
   MinC.X:=VertexArray[I].Vector.X;
  END ELSE IF VertexArray[I].Vector.X>MaxC.X THEN BEGIN
   MaxC.X:=VertexArray[I].Vector.X;
  END;
  IF VertexArray[I].Vector.Y<MinC.Y THEN BEGIN
   MinC.Y:=VertexArray[I].Vector.Y;
  END ELSE IF VertexArray[I].Vector.Y>MaxC.Y THEN BEGIN
   MaxC.Y:=VertexArray[I].Vector.Y;
  END;
  IF VertexArray[I].Vector.Z<MinC.Z THEN BEGIN
   MinC.Z:=VertexArray[I].Vector.Z;
  END ELSE IF VertexArray[I].Vector.Z>MaxC.Z THEN BEGIN
   MaxC.Z:=VertexArray[I].Vector.Z;
  END;
 END;
 MaxXC.X:=MaxC.X-MinC.X;
 MaxXC.Y:=MaxC.Y-MinC.Y;
 MaxXC.Z:=MaxC.Z-MinC.Z;
 IF MaxXC.X>MaxXC.Y THEN Scale:=MaxXC.X ELSE Scale:=MaxXC.Y;
 IF Scale>MaxXC.Z THEN Scale:=MaxXC.Z;
END;

FUNCTION T3DSLoader.Load(FileName:STRING;ScaleFactor:SINGLE=1):BOOLEAN;
VAR TheFile:FILE;
    LastCountVertices,LastCountMapping:WORD;
    Error,NewColor:BOOLEAN;
    GRGBA:T3DSRGB;

 FUNCTION ReadChunk:T3DSChunk;
 VAR AChunk:T3DSChunk;
 BEGIN
  {$I-}BLOCKREAD(TheFile,AChunk.ID,2);{$I+}IF IOResult<>0 THEN Error:=TRUE;
  {$I-}BLOCKREAD(TheFile,AChunk.Next,4);{$I+}IF IOResult<>0 THEN Error:=TRUE;
  ReadChunk:=AChunk;
 END;

 FUNCTION ReadByte:Byte;
 VAR B:BYTE;
 BEGIN
  {$I-}BLOCKREAD(TheFile,B,1);{$I+}IF IOResult<>0 THEN Error:=TRUE;
  ReadByte:=B;
 END;

 FUNCTION ReadWord:WORD;
 VAR W:WORD;
 BEGIN
  {$I-}BLOCKREAD(TheFile,W,2);{$I+}IF IOResult<>0 THEN Error:=TRUE;
  ReadWord:=W;
 END;

 FUNCTION ReadSingle:SINGLE;
 VAR S:SINGLE;
 BEGIN
  {$I-}BLOCKREAD(TheFile,S,SIZEOF(SINGLE));{$I+}IF IOResult<>0 THEN Error:=TRUE;
  ReadSingle:=S;
 END;

 FUNCTION ReadShort:SMALLINT;
 VAR S:SMALLINT;
 BEGIN
  {$I-}BLOCKREAD(TheFile,S,2);{$I+}IF IOResult<>0 THEN Error:=TRUE;
  ReadShort:=S;
 END;

 FUNCTION ParseAsciiString:STRING;
 VAR S:STRING;
     C:CHAR;
 BEGIN
  S:='';
  C:=#1;
  WHILE (C<>#0) AND NOT Error DO BEGIN
   {$I-}BLOCKREAD(TheFile,C,1);{$I+}
   IF IOResult<>0 THEN Error:=TRUE;
   IF (C<>#0) AND NOT Error THEN S:=S+C;
  END;
  ParseAsciiString:=S;
 END;

 PROCEDURE ParseVertexList;
  VAR CountVerticesInternal:WORD;
     I:INTEGER;
     X,Y,Z:SINGLE;
 BEGIN
  CountVerticesInternal:=ReadWord;
  FOR I:=1 TO CountVerticesInternal DO BEGIN
   X:=ReadSingle;
   Y:=ReadSingle;
   Z:=ReadSingle;
   IF Error THEN BREAK;
   IF CountVertices>=LENGTH(VertexArray) THEN SETLENGTH(VertexArray,CountVertices+1);
   VertexArray[CountVertices].Vector.X:=X/ScaleFactor;
   VertexArray[CountVertices].Vector.Y:=Z/ScaleFactor;
   VertexArray[CountVertices].Vector.Z:=(-Y)/ScaleFactor;
   VertexArray[CountVertices].TextureUV.U:=0;
   VertexArray[CountVertices].TextureUV.V:=0;
   INC(CountVertices);
  END;
 END;

 PROCEDURE ParseMappingList;
 VAR CountVerticesInternal:WORD;
     I,J:INTEGER;
     U,V:SINGLE;
     Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  CountVerticesInternal:=ReadWord;
  FOR I:=1 TO CountVerticesInternal DO BEGIN
   U:=ReadSingle;
   V:=ReadSingle;
   IF Error THEN BREAK;
   J:=I+LastCountMapping-1;
   IF J>=LENGTH(VertexArray) THEN SETLENGTH(VertexArray,J+1);
   VertexArray[J].TextureUV.U:=U;
   VertexArray[J].TextureUV.V:=V;
  END;
  OldPos:=FILEPOS(TheFile);
  Chunk.ID:=$4155;
  WHILE (Chunk.ID>=$4150) AND (Chunk.ID<=$415F) AND NOT Error DO BEGIN
   Chunk:=ReadChunk;
   IF Chunk.ID=$4150 THEN;
  END;
  IF NOT Error THEN BEGIN
   Seek(TheFile,OldPos+Chunk.Next);
  END ELSE BEGIN
   Error:=FALSE;
  END;
  LastCountMapping:=CountVerticesInternal;
 END;

 PROCEDURE ParseFaceMappingList(A:WORD); FORWARD;

 PROCEDURE ParseFaceList;
 VAR I,CountFacesIntern,P1,P2,P3,Flags,Ab:LONGWORD;
     Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  CountFacesIntern:=ReadWord;
  Ab:=CountFaces;
  FOR I:=1 TO CountFacesIntern DO BEGIN
   P1:=ReadWord;
   P2:=ReadWord;
   P3:=ReadWord;
   Flags:=ReadWord;
   IF Error THEN BREAK;
   SETLENGTH(FaceArray,CountFaces+1);
   FaceArray[CountFaces].L1:=P1+LastCountVertices;
   FaceArray[CountFaces].L2:=P2+LastCountVertices;
   FaceArray[CountFaces].L3:=P3+LastCountVertices;
   FaceArray[CountFaces].ABVis:=(Flags AND $0004)<>0;
   FaceArray[CountFaces].BCVis:=(Flags AND $0002)<>0;
   FaceArray[CountFaces].CAVis:=(Flags AND $0001)<>0;
   FaceArray[CountFaces].UWrap:=(Flags AND $0008)<>0;
   FaceArray[CountFaces].VWrap:=(Flags AND $0010)<>0;
   FaceArray[CountFaces].MatName:='';
   FaceArray[CountFaces].Material:=NIL;
   INC(CountFaces);
  END;
  OldPos:=FILEPOS(TheFile);
  Chunk.ID:=$4135;
  WHILE (Chunk.ID>=$4130) AND (Chunk.ID<=$413F) AND NOT Error DO BEGIN
   Chunk:=ReadChunk;
   IF Chunk.ID=$4130 THEN ParseFaceMappingList(Ab);
  END;
  IF NOT Error THEN BEGIN
   Seek(TheFile,OldPos+Chunk.Next);
  END ELSE BEGIN
   Error:=FALSE;
  END;
  NewColor:=FALSE;
 END;

 PROCEDURE ParseFaceMappingList(A:WORD);
 VAR I,W,CountFaceMappingIntern:WORD;
     BufMatName:STRING;
 BEGIN
  BufMatName:=MUPPERCASE(ParseAsciiString);
  CountFaceMappingIntern:=ReadWord;
  FOR I:=1 TO CountFaceMappingIntern DO BEGIN
   W:=ReadWord+A;
   IF W<=CountFaces THEN FaceArray[W].MatName:=BufMatName;
  END;
 END;

 PROCEDURE ParseRGBByte(VAR RGBA:T3DSRGB);
 BEGIN
  RGBA.r:=ReadByte/255;
  RGBA.g:=ReadByte/255;
  RGBA.b:=ReadByte/255;
  NewColor:=TRUE;
 END;

 PROCEDURE ParseRGBFloat(VAR RGBA:T3DSRGB);
 BEGIN
  RGBA.r:=ReadSingle;
  RGBA.g:=ReadSingle;
  RGBA.b:=ReadSingle;
  NewColor:=TRUE;
 END;

 PROCEDURE ParseLight;
 VAR X,Y,Z:SINGLE;
 BEGIN
  X:=ReadSingle;
  Y:=ReadSingle;
  Z:=ReadSingle;
  IF X=0 THEN;
  IF Y=0 THEN;
  IF Z=0 THEN;
 END;

 PROCEDURE ParseCamera;
 VAR X,Y,Z,ZielX,ZielY,ZielZ,Bank,Lens:SINGLE;
 BEGIN
  X:=ReadSingle;
  Y:=ReadSingle;
  Z:=ReadSingle;
  ZielX:=ReadSingle;
  ZielY:=ReadSingle;
  ZielZ:=ReadSingle;
  Bank:=ReadSingle;
  Lens:=ReadSingle;
  IF X=0 THEN;
  IF Y=0 THEN;
  IF Z=0 THEN;
  IF ZielX=0 THEN;
  IF ZielY=0 THEN;
  IF ZielZ=0 THEN;
  IF Bank=0 THEN;
  IF Lens=0 THEN;
 END;

 PROCEDURE ParseMeshColor;
 BEGIN
  ReadByte;
  NewColor:=TRUE;
 END;

 PROCEDURE ParseMaterialSubColorChunk(VAR RGBA:T3DSRGB);
 VAR Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  Chunk.ID:=$0015;
  WHILE ((Chunk.ID>=$0010) AND (Chunk.ID<=$001F)) AND NOT Error DO BEGIN
   OldPos:=FILEPOS(TheFile);
   Chunk:=ReadChunk;
   IF Error THEN Chunk.ID:=0;
   CASE Chunk.ID OF
    $0010:ParseRGBFloat(RGBA);
    $0011:ParseRGBByte(RGBA);
{   $0013:ParseRGBByte(RGBA);
    $0014:ParseRGBFloat(RGBA); }
    ELSE BEGIN
    END;
   END;
   IF NOT Error THEN Seek(TheFile,OldPos+Chunk.Next);
  END;
 END;

 PROCEDURE ParseMaterialSubPercentChunk(VAR S:SINGLE);
 VAR Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  Chunk.ID:=$0035;
  WHILE ((Chunk.ID>=$0030) AND (Chunk.ID<=$003F)) AND NOT Error DO BEGIN
   OldPos:=FILEPOS(TheFile);
   Chunk:=ReadChunk;
   IF Error THEN Chunk.ID:=0;
   CASE Chunk.ID OF
    $0030:S:=ReadShort/100;
    $0031:S:=ReadSingle;
    ELSE BEGIN
    END;
   END;
   IF NOT Error THEN Seek(TheFile,OldPos+Chunk.Next);
  END;
 END;

 PROCEDURE ParseMaterialChunk;
 VAR Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  SETLENGTH(MaterialArray,CountMaterial+1);
  FILLCHAR(MaterialArray[CountMaterial],SIZEOF(T3DSMaterial),#0);
  SETLENGTH(MaterialArray[CountMaterial].Faces,0);
  WITH MaterialArray[CountMaterial] DO BEGIN
   MatName:='';
   MapName:='';
   Shininess:=60;
   CountFaces:=0;
  END;
  Chunk.ID:=$A000;
  WHILE ((Chunk.ID>=$A000) AND (Chunk.ID<=$AFFF)) AND NOT Error DO BEGIN
   OldPos:=FILEPOS(TheFile);
   Chunk:=ReadChunk;
   IF Error THEN Chunk.ID:=0;
   WITH MaterialArray[CountMaterial] DO CASE Chunk.ID OF
    $A000:MatName:=MUPPERCASE(ParseAsciiString);
    $A010:ParseMaterialSubColorChunk(AMBIENT);
    $A020:ParseMaterialSubColorChunk(DIFFUSE);
    $A030:ParseMaterialSubColorChunk(SPECULAR);
    $A040:ParseMaterialSubPercentChunk(SHININESS);
    $A050:ParseMaterialSubPercentChunk(TRANSPARENZ);
    $A100:SHADING:=ReadShort/32768;
    $A200:;
    $A300:MapName:=MUPPERCASE(ParseAsciiString);
    ELSE BEGIN
    END;
   END;
   IF NOT Error THEN Seek(TheFile,OldPos+Chunk.Next);
  END;
{ WITH MaterialArray[CountMaterial] DO BEGIN
   WITH AMBIENT DO WRITELN(r:5:5,g:5:5,b:5:5);
   WITH DIFFUSE DO WRITELN(r:5:5,g:5:5,b:5:5);
   WITH SPECULAR DO WRITELN(r:5:5,g:5:5,b:5:5);
  END;}
  INC(CountMaterial);
 END;

 PROCEDURE ParseTriPolygonObjectChunk;
 VAR Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  LastCountVertices:=CountVertices;
  Chunk.ID:=$4150;
  WHILE ((Chunk.ID>=$4100) AND (Chunk.ID<=$41FF)) AND NOT Error DO BEGIN
   OldPos:=FILEPOS(TheFile);
   Chunk:=ReadChunk;
   IF Error THEN Chunk.ID:=0;
   CASE Chunk.ID OF
    $4110:ParseVertexList;
    $4120:ParseFaceList;
    $4140:ParseMappingList;
    $4165:ParseMeshColor;
    ELSE BEGIN
    END;
   END;
   IF NOT Error THEN Seek(TheFile,OldPos+Chunk.Next);
  END;
 END;

 PROCEDURE ParseObjectMeshChunk;
 VAR ObjectName:STRING;
     Chunk:T3DSChunk;
     OldPos:LONGWORD;
     I:WORD;
     Done,Load:BOOLEAN;
 BEGIN
  ObjectName:=MUPPERCASE(ParseAsciiString);
  I:=0;
  Done:=FALSE;
  Load:=FALSE;
  WHILE (I<LENGTH(ObjectsToLoad)) AND NOT Done DO BEGIN
   IF (ObjectsToLoad[I]='*') OR (ObjectsToLoad[I]=ObjectName) THEN BEGIN
    Done:=TRUE;
    Load:=TRUE;
   END;
  END;
  IF Load THEN BEGIN
   Chunk.ID:=$4100;
   WHILE ((Chunk.ID>=$4000) AND (Chunk.ID<=$5000)) AND NOT Error DO BEGIN
    OldPos:=FILEPOS(TheFile);
    Chunk:=ReadChunk;
    IF Error THEN Chunk.ID:=0;
    CASE Chunk.ID OF
     $4100:ParseTriPolygonObjectChunk;
     $4600:ParseLight;
     $4700:ParseCamera;
     ELSE BEGIN
     END;
    END;
    IF NOT Error THEN Seek(TheFile,OldPos+Chunk.Next);
   END;
  END;
 END;

 PROCEDURE ParsePrimaryChunk;
 VAR Chunk:T3DSChunk;
     OldPos:LONGWORD;
 BEGIN
  Chunk.ID:=$FF;
  WHILE ((Chunk.ID<>0) AND (Chunk.ID<>$B000)) AND NOT Error DO BEGIN
   OldPos:=FILEPOS(TheFile);
   Chunk:=ReadChunk;
   IF Error THEN Chunk.ID:=0;
   CASE Chunk.ID OF
    $4000:ParseObjectMeshChunk;
    $AFFF:ParseMaterialChunk;
    ELSE BEGIN
    END;
   END;
   IF NOT Error THEN Seek(TheFile,OldPos+Chunk.Next);
  END;
 END;

 FUNCTION VectorAdd(V1,V2:T3DSSubVertex):T3DSSubVertex;
 BEGIN
  RESULT.X:=V1.X+V2.X;
  RESULT.Y:=V1.Y+V2.Y;
  RESULT.Z:=V1.Z+V2.Z;
 END;

 FUNCTION VectorSub(V1,V2:T3DSSubVertex):T3DSSubVertex;
 BEGIN
  RESULT.X:=V1.X-V2.X;
  RESULT.Y:=V1.Y-V2.Y;
  RESULT.Z:=V1.Z-V2.Z;
 END;

 FUNCTION VectorDiv(V1:T3DSSubVertex;D:DOUBLE):T3DSSubVertex;
 BEGIN
  RESULT.X:=V1.X/D;
  RESULT.Y:=V1.Y/D;
  RESULT.Z:=V1.Z/D;
 END;

 FUNCTION VectorCrossProduct(V1,V2:T3DSSubVertex):T3DSSubVertex;
 VAR Temp:T3DSSubVertex;
 BEGIN
  Temp.X:=V1.Y*V2.Z-V1.Z*V2.Y;
  Temp.Y:=V1.Z*V2.X-V1.X*V2.Z;
  Temp.Z:=V1.X*V2.Y-V1.Y*V2.X;
  RESULT:=Temp;
 END;

 FUNCTION VectorLength(V:T3DSSubVertex):SINGLE;
 BEGIN
  RESULT:=SQRT(SQR(V.X)+SQR(V.Y)+SQR(V.Z));
 END;

 FUNCTION VectorNorm(V:T3DSSubVertex):T3DSSubVertex;
 VAR Temp:T3DSSubVertex;
     L:SINGLE;
 BEGIN
  L:=VectorLength(V);
  IF L=0 THEN L:=1;
  Temp.X:=V.X/L;
  Temp.Y:=V.Y/L;
  Temp.Z:=V.Z/L;
  RESULT:=Temp;
 END;

 PROCEDURE FixValues;
 VAR I,J,K,Count:INTEGER;
     V1,V2,V3,VA,VB,VN,Normal:T3DSSubVertex;
 BEGIN
  // Objekt zentieren
  IF DoCenterise THEN BEGIN
   // MinMax Vektoren suchen
   CalculateMinMax;
   FOR I:=0 TO CountVertices-1 DO BEGIN
    VertexArray[I].Vector.X:=VertexArray[I].Vector.X-MinC.X-((MaxC.X-MinC.X)*0.5);
    VertexArray[I].Vector.Y:=VertexArray[I].Vector.Y-MinC.Y-((MaxC.Y-MinC.Y)*0.5);
    VertexArray[I].Vector.Z:=VertexArray[I].Vector.Z-MinC.Z-((MaxC.Z-MinC.Z)*0.5);
   END;
  END;

  // MinMax Vektoren erneut suchen
  CalculateMinMax;
  FOR I:=0 TO CountFaces-1 DO BEGIN
   FOR J:=0 TO CountMaterial-1 DO BEGIN
    IF FaceArray[I].MatName=MaterialArray[J].MatName THEN BEGIN
     FaceArray[I].Material:=@MaterialArray[J];
    END;
   END;
   WITH FaceArray[I] DO BEGIN
    V1:=VertexArray[L1].Vector;
    V2:=VertexArray[L2].Vector;
    V3:=VertexArray[L3].Vector;
    VA:=VectorSub(V1,V3);
    VB:=VectorSub(V3,V2);
    VN:=VectorCrossProduct(VA,VB);
    FaceTempNormal:=VN;
    VN:=VectorNorm(VN);
    FaceNormal:=VN;
   END;
  END;
  FOR I:=0 TO CountVertices-1 DO BEGIN
   Normal.X:=0;
   Normal.Y:=0;
   Normal.Z:=0;
   Count:=0;
   FOR J:=0 TO CountFaces-1 DO BEGIN
    IF (FaceArray[J].L1=I) OR (FaceArray[J].L2=I) OR (FaceArray[J].L3=I) THEN BEGIN
     Normal:=VectorAdd(Normal,FaceArray[J].FaceTempNormal);
     INC(Count);
    END;
   END;
   IF Count=0 THEN Count:=1;            
   VertexArray[I].NormalVector:=VectorNorm(VectorDiv(Normal,-Count));
  END;

  FOR I:=0 TO CountMaterial-1 DO BEGIN
   Count:=0;
   FOR J:=0 TO CountFaces-1 DO BEGIN
    IF FaceArray[J].Material=@MaterialArray[I] THEN BEGIN
     INC(Count);
    END;
   END;

   MaterialArray[I].CountFaces:=Count;
   MaterialArray[I].CountVerts:=Count*3;
   SETLENGTH(MaterialArray[I].Faces,MaterialArray[I].CountFaces);
   SETLENGTH(MaterialArray[I].Verts,MaterialArray[I].CountVerts);

   K:=0;
   FOR J:=0 TO CountFaces-1 DO BEGIN
    IF FaceArray[J].Material=@MaterialArray[I] THEN BEGIN
     MaterialArray[I].Faces[K]:=J;
     WITH FaceArray[J] DO BEGIN
      MaterialArray[I].Verts[(K*3)]:=L1;
      MaterialArray[I].Verts[(K*3)+1]:=L2;
      MaterialArray[I].Verts[(K*3)+2]:=L3;
     END;
     INC(K);
    END;
   END;
  END;
 END;

 PROCEDURE InitValues;
 VAR I:WORD;
 BEGIN
  GRGBA.r:=1;
  GRGBA.g:=1;
  GRGBA.b:=1;
  NewColor:=TRUE;
  CountMaterial:=0;
  CountVertices:=0;
  CountFaces:=0;
  LastCountMapping:=0;
  FOR I:=0 TO LENGTH(ObjectsToLoad)-1 DO BEGIN
   ObjectsToLoad[I]:=MUPPERCASE(ObjectsToLoad[I]);
  END;
  SETLENGTH(MaterialArray,CountMaterial+1);
  FILLCHAR(MaterialArray[CountMaterial],SIZEOF(T3DSMaterial),#0);
  SETLENGTH(MaterialArray[CountMaterial].Faces,0);
  WITH MaterialArray[CountMaterial] DO BEGIN
   MatName:='';
   MapName:='';
   Shininess:=60;
   CountFaces:=0;
  END;
  INC(CountMaterial);
 END;

VAR Chunk:T3DSChunk;
    OldPos:LONGWORD;
BEGIN
 Error:=FALSE;
 IF POS('.', FileName)=0 THEN BEGIN
  FileName:=FileName+'.3DS';
 END;

 ASSIGNfILE(TheFile,FileName);
 {$I-}RESET(TheFile,1);{$I+}
 IF IOResult<>0 THEN BEGIN
  Error:=TRUE;
  RESULT:=NOT Error;
  EXIT;
 END;

 InitValues;

 SEEK(TheFile,0);
 Chunk:=ReadChunk;
 IF (Chunk.ID=$4D4D) OR (IOResult=0) THEN BEGIN
  WHILE NOT (EOF(TheFile) OR Error) DO BEGIN
   OldPos:=FILEPOS(TheFile);
   Chunk:=ReadChunk;
   IF Error THEN Chunk.ID:=0;
   CASE Chunk.ID OF
    $3D3D:ParsePrimaryChunk;
   END;
   SEEK(TheFile,OldPos+Chunk.Next);
  END;
  FixValues;
 END ELSE BEGIN
  Error:=TRUE;
 END;
 CLOSEFILE(TheFile);
{$IFDEF OpenGL}
 DrawCreateList(TRUE);
{$ENDIF} 
 RESULT:=NOT Error;
END;

{$IFDEF OpenGL}
PROCEDURE T3DSLoader.Draw(Lighting:BOOLEAN);
VAR I:INTEGER;
    Mat:P3DSMaterial;
    CR,CG,CB:SINGLE;
    Blenden:BOOLEAN;
BEGIN
 glColor3f(1,1,1);
 CR:=1;
 CG:=1;
 CB:=1;

 glBlendFunc(GL_ONE_MINUS_SRC_ALPHA,GL_SRC_ALPHA);

 glEnableClientState(GL_VERTEX_ARRAY);
 glEnableClientState(GL_NORMAL_ARRAY);
 glEnableClientState(GL_TEXTURE_COORD_ARRAY);
 glDisableClientState(GL_COLOR_ARRAY);

 glNormalPointer(GL_FLOAT,SIZEOF(T3DSVertex),@VertexArray[0].NormalVector);
 glVertexPointer(3,GL_FLOAT,SIZEOF(T3DSVertex),@VertexArray[0].Vector);
 glTexCoordPointer(2,GL_FLOAT,SIZEOF(T3DSVertex),@VertexArray[0].TextureUV);

 Blenden:=TRUE;

 glDisable(GL_TEXTURE_2D);
 FOR I:=0 TO CountMaterial-1 DO BEGIN
  Mat:=@MaterialArray[I];
  IF Mat<>NIL THEN BEGIN
   IF Blenden THEN BegiN
    glDisable(gl_BLEND);
    Blenden:=FALSE;
   END;
   IF Lighting THEN BEGIN
    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,@Mat^.Diffuse);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,@Mat^.Specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,@Mat^.Ambient);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,@Mat^.Shininess);
   END ELSE WITH Mat^.Diffuse DO IF (R<>CR) OR (G<>CG) OR (B<>CB) THEN BEGIN
    glColor3f(R,G,B);
    CR:=R;
    CG:=G;
    CB:=B;
   END;
   IF Mat^.TRANSPARENZ>0 THEN BEGIN
    glEnable(gl_Blend);
    Blenden:=TRUE;
   END;
   glDrawElements(GL_TRIANGLES,MaterialArray[I].CountVerts,GL_UNSIGNED_INT,@MaterialArray[I].Verts[0]);
  END;
 END;
END;

PROCEDURE T3DSLoader.DrawCreateList(Lighting:BOOLEAN);
BEGIN
 FDrawListID:=glGenLists(1);
 glNewList(FDrawListID,GL_COMPILE);
 Draw(Lighting);
 glEndList;
END;

PROCEDURE T3DSLoader.Render;
BEGIN
 glCallList(FDrawListID);
END;
{$ENDIF}

END.
