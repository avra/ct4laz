{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

program convmesh;
{$APPTYPE CONSOLE}

uses
  SysUtils,
  Model3DS,
  OBJLoader;

function Max(A, B: SINGLE): SINGLE;
begin
  if A < B then
  begin
    RESULT := B;
  end
  else
  begin
    RESULT := A;
  end;
end;

function SoftTRUNC(FloatValue: SINGLE): INTEGER;
type
  PLONGWORD = ^LONGWORD;
const
  MaskMantissa = (1 shl 23) - 1;
var
  Exponent, Mantissa, Sig, SigExtra, Signed, IsDenormalized: LONGWORD;
  Value, Shift: INTEGER;
begin
  Exponent := (PLONGWORD(@FloatValue)^ and $7FFFFFFF) shr 23;
  Mantissa := PLONGWORD(@FloatValue)^ and MaskMantissa;
  Shift := Exponent - $96;
  Sig := Mantissa or $00800000;
  SigExtra := Sig shl (Shift and 31);
  IsDenormalized := 0 - ORD(0 <= Shift);
  Value := (((-ORD(Exponent >= $7E)) and (Sig shr (32 - Shift))) and not
    IsDenormalized) or
    (SigExtra and IsDenormalized);
  Signed := 0 - ORD((PLONGWORD(@FloatValue)^ and $80000000) <> 0);
  RESULT := (((0 - Value) and Signed) or (Value and not Signed)) and (0 - ORD($9E
    > Exponent));
end;

type
  TVector = record
    X, Y, Z: SINGLE;
  end;

  TFace = array[0..2] of INTEGER;

var
  ModelFile3DS: T3DSLoader;
  ModelFileOBJ: TOBJModell;
  I, J, K: INTEGER;
  F: file;
  T: TEXT;
  W: LONGWORD;
  B, LB: BYTE;
  SI8: SHORTINT;
  SFX, SFY, SFZ: SINGLE;
  Vertices: array of TVector;
  Faces: array of TFace;

procedure WriteDiffByte(var S);
var
  B, NLB: BYTE;
begin
  NLB := BYTE(POINTER(@S)^);
  B := NLB - LB;
  BLOCKWRITE(F, B, SIZEOF(BYTE));
  LB := NLB;
end;

//{$IFDEF WINDOWS}{$R convmesh.rc}{$ENDIF}

{$R *.res}

begin
  Vertices := nil;
  Faces := nil;

  if FileExists(PARAMSTR(1) + '.obj') then
  begin
    ModelFileOBJ := TOBJModell.Create;
    ModelFileOBJ.LoadModel(PARAMSTR(1) + '.obj');
    SETLENGTH(Vertices, LENGTH(ModelFileOBJ.Vertex));
    for I := 0 to LENGTH(ModelFileOBJ.Vertex) - 1 do
    begin
      Vertices[I].X := ModelFileOBJ.Vertex[I].X;
      Vertices[I].Y := ModelFileOBJ.Vertex[I].Y;
      Vertices[I].Z := ModelFileOBJ.Vertex[I].Z;
    end;
    for I := 0 to LENGTH(ModelFileOBJ.Group) - 1 do
    begin
      for J := 0 to LENGTH(ModelFileOBJ.Group[I].Face) - 1 do
      begin
        if LENGTH(ModelFileOBJ.Group[I].Face[J].vIndex) = 3 then
        begin
          K := LENGTH(Faces);
          SETLENGTH(Faces, K + 1);
          Faces[K, 0] := ModelFileOBJ.Group[I].Face[J].vIndex[0];
          Faces[K, 1] := ModelFileOBJ.Group[I].Face[J].vIndex[1];
          Faces[K, 2] := ModelFileOBJ.Group[I].Face[J].vIndex[2];
        end;
      end;
    end;
    ModelFileOBJ.Destroy;
  end
  else
  begin
    ModelFile3DS := T3DSLoader.Create;
    ModelFile3DS.Load(PARAMSTR(1) + '.3ds');
    SETLENGTH(Vertices, LENGTH(ModelFile3DS.VertexArray));
    for I := 0 to LENGTH(ModelFile3DS.VertexArray) - 1 do
    begin
      Vertices[I].X := ModelFile3DS.VertexArray[I].Vector.X;
      Vertices[I].Y := ModelFile3DS.VertexArray[I].Vector.Y;
      Vertices[I].Z := ModelFile3DS.VertexArray[I].Vector.Z;
    end;
    SETLENGTH(Faces, LENGTH(ModelFile3DS.FaceArray));
    for I := 0 to LENGTH(ModelFile3DS.FaceArray) - 1 do
    begin
      Faces[I, 0] := ModelFile3DS.FaceArray[I].L1;
      Faces[I, 1] := ModelFile3DS.FaceArray[I].L2;
      Faces[I, 2] := ModelFile3DS.FaceArray[I].L3;
    end;
    ModelFile3DS.Destroy;
  end;

  ASSIGNFILE(F, PARAMSTR(1) + '.pmf');
  REWRITE(F, 1);

  BLOCKWRITE(F, 'PMF0', 4);

  W := LENGTH(Faces);
  BLOCKWRITE(F, W, SIZEOF(LONGWORD));

  W := LENGTH(Vertices);
  BLOCKWRITE(F, W, SIZEOF(LONGWORD));

  for I := 0 to LENGTH(Faces) - 1 do
  begin
    W := Faces[I, 0];
    BLOCKWRITE(F, W, SIZEOF(LONGWORD));
    W := Faces[I, 1];
    BLOCKWRITE(F, W, SIZEOF(LONGWORD));
    W := Faces[I, 2];
    BLOCKWRITE(F, W, SIZEOF(LONGWORD));
  end;

  for I := 0 to LENGTH(Vertices) - 1 do
  begin
    BLOCKWRITE(F, Vertices[I].X, SIZEOF(SINGLE));
    BLOCKWRITE(F, Vertices[I].Y, SIZEOF(SINGLE));
    BLOCKWRITE(F, Vertices[I].Z, SIZEOF(SINGLE));
  end;

  CLOSEFILE(F);

  SETLENGTH(Vertices, 0);
  SETLENGTH(Faces, 0);

  //READLN;
end.


