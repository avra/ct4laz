program pmf2pas;
var
  Src: file;
  Dst: TEXT;
  B, C: BYTE;
  S: string;

//{$IFDEF WINDOWS}{$R pmf2pas.rc}{$ENDIF}

{$R *.res}

begin
  ASSIGNFILE(Src, 'sandbox.pmf');
{$I-}RESET(Src, 1);
{$I+}
  if IOResult = 0 then
  begin
    ASSIGNFILE(Dst, 'sandboxfile.pas');
{$I-}REWRITE(Dst);
{$I+}
    if IOResult = 0 then
    begin
      WRITELN(Dst, 'UNIT sandboxfile;');
      WRITELN(Dst, 'INTERFACE');
      WRITELN(Dst, 'CONST sandboxSize=', FILESIZE(Src), ';');
      WRITE(Dst, '      sandboxData:ARRAY[1..sandboxSize] OF BYTE=(');
      C := 0;
      while not EOF(Src) do
      begin
        BLOCKREAD(Src, B, 1);
        STR(B, S);
        if FILESIZE(Src) <> FILEPOS(Src) then
          S := S + ',';
        C := C + LENGTH(S);
        WRITE(Dst, S);
        if C > 40 then
        begin
          if FILESIZE(Src) <> FILEPOS(Src) then
          begin
            WRITELN(Dst);
            WRITE(Dst, '                                             ');
          end;
          C := 0;
        end;
      end;
      WRITELN(Dst, ');');
      WRITELN(Dst, 'IMPLEMENTATION');
      WRITELN(Dst, 'END.');
      CLOSEFILE(Dst);
    end;
    CLOSEFILE(Src);
  end;
end.


