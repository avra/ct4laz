{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit demo2mw;

interface

uses
  Classes, SysUtils,Messages,lMessages, Types,LCLType,
  LCLProc, LResources, Forms, Controls, Graphics, Dialogs,
  LCLIntf, ExtCtrls, StdCtrls,
  OpenGLPanel, ctGL, ctGLU, PAPPE;

type


  TForm1 = class(TForm)
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
    procedure OpenGLPanel1Paint(Sender: TObject);
    procedure OpenGLPanel1Resize(Sender: TObject);
    procedure OnAppIdle(Sender: TObject; var Done: Boolean);
  private
    procedure xxFormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure xxFormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure xxFormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure xxFormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  public
    OpenGLPanel1: TOpenGLPanel;
  end;

var
  Form1: TForm1; 

implementation

const
 ScreenWidthHalf:integer = 5500;
 ScreenHeightHalf:integer = 768;
 TwoPI: single = 2*PI;
 MaxObjects=256;

var
 ScreenWidth:integer = 640;
 ScreenHeight:integer = 480;
 mx,my:integer;
 Now: longword;

 MousePos: Types.TPoint;
 Physics: TPhysics;

 Object0: TPhysicsObject;
 Object0RigidBody: TPhysicsRigidBody;
 Objects:array[0..MaxObjects-1] of TPhysicsObject;
 ObjectRigidBodies:array[0..MaxObjects-1] of TPhysicsRigidBody;

const
 GlobalAmbient: array[0..3] of GLFLOAT = (0.025,0.025,0.025,1);
 Licht0Pos: array[0..3] of GLFLOAT = (0,160,160,1);
 Licht0Ambient: array[0..3] of GLFLOAT = (0.015,0.015,0.015,1);
 Licht0Diffuse: array[0..3] of GLFLOAT = (0.8,0.8,0.8,1);
 Licht0Specular: array[0..3] of GLFLOAT = (0.025,0.025,0.025,1);
 LModellAmbient: array[0..3] of GLFLOAT = (0.025,0.025,0.025,1);
 MaterialDiffuse: GLFLOAT = 0.0;
 MaterialSpecular: GLFLOAT = 0.0;
 MaterialAmbient: GLFLOAT = 0.0;
 MaterialShininess: GLFLOAT = 0.0;

const Dir:TPhysicsVector3=(x:0;y:0;z:1);

var viewv:TPhysicsVector3;
    yy:single;
    OldPosition,OldView,OldUpVector:TPhysicsVector3;
    xPosition,View,UpVector:TPhysicsVector3;
    Joint:TPhysicsJoint;

//======================================================================================
 procedure DrawObjectMesh(var AObjectMesh: TPhysicsObjectMesh); register;
 var
  I: integer;
  N: TPhysicsVector3;
 begin
  glBegin(GL_TRIANGLES);
  for I:=0 to AObjectMesh.NumMeshs-1 do begin
   DrawObjectMesh(AObjectMesh.Meshs^[i]^);
  end;
  for I:=0 to AObjectMesh.NumTriangles-1 do begin
   N:=Vector3Norm(Vector3Cross(Vector3Sub(AObjectMesh.Triangles^[I].Vertices[1],AObjectMesh.Triangles^[I].Vertices[0]),Vector3Sub(AObjectMesh.Triangles^[I].Vertices[2],AObjectMesh.Triangles^[I].Vertices[0])));
   glNormal3fv(@N);

   glVertex3fv(@AObjectMesh.Triangles^[I].Vertices[0]);
   glVertex3fv(@AObjectMesh.Triangles^[I].Vertices[1]);
   glVertex3fv(@AObjectMesh.Triangles^[I].Vertices[2]);
  end;
  glEnd;
 end;

 procedure DrawObject(var AObject: TPhysicsObject); register;
 var
  I: integer;
  N: TPhysicsVector3;
 begin
  glPushMatrix;
  glEnable(GL_LIGHTING);
  glMultMatrixf(@AObject.Transform);
  for I:=0 to AObject.NumMeshs-1 do begin
   DrawObjectMesh(AObject.Meshs^[i]^);
  end;
  glPopMatrix;
 end;
 procedure Draw(TimeStep: single); register;
 var
  P,pv,pp,pn: TPhysicsVector3;
  m,m1: TPhysicsMatrix4x4;
  k,a:single;
  i:integer;
 begin
  glClearColor(0,0,0,0);
  glMatrixMode(GL_PROJECTION);
  glClear(GL_COLOR_BUFFER_BIT or GL_STENCIL_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  glFrustum(-0.01,0.01,-0.0075,0.0075,0.01,1000.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glCullFace(0);
  gldisable(GL_BLEND);
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,@LModellAmbient);
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT,@GlobalAmbient);
  glLightfv(GL_LIGHT0,GL_POSITION,@Licht0Pos);
  glLightfv(GL_LIGHT0,GL_AMBIENT,@Licht0Ambient);
  glLightfv(GL_LIGHT0,GL_DIFFUSE,@Licht0Diffuse);
  glLightfv(GL_LIGHT0,GL_SPECULAR,@Licht0Specular);
  glPushMatrix;

  glEnable(GL_LIGHTING);

  glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,@MaterialDiffuse);
  glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,@MaterialSpecular);
  glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,@MaterialAmbient);
  glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,@MaterialShininess);

  glShadeModel(GL_SMOOTH);

  glDepthFunc(GL_LEQUAL);
  glCullFace(1);

  m:=Matrix4x4LookAt(xPosition,View,UpVector);

  glLoadMatrixf(@m);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE,GL_ONE);

  glColor4f(1,1,1,1);
  DrawObject(Object0);

  glDisable(GL_LIGHTING);
  glColor4f(1,1,0,1);
  glDepthFunc(GL_LEQUAL);
  glLineWidth(2);

  glLineWidth(1);
  glEnable(GL_DEPTH_TEST);

  glDisable(GL_BLEND);
  glEnable(GL_LIGHTING);

  for i:=0 to length(Objects)-1 do begin
   glColor4f(1,0,0,1);
   DrawObject(Objects[i]);
  end;

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glCullFace(1);

  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);

  for i:=0 to length(Objects)-1 do begin
   PhysicsObjectUpdate(Objects[i],TimeStep);
  end;
  PhysicsObjectUpdate(Object0,TimeStep);

  PhysicsUpdate(Physics,TimeStep);

  glEnable(GL_DEPTH_TEST);

  glPopMatrix;

  glMatrixMode(GL_PROJECTION);
  glClear(GL_STENCIL_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  glFrustum(-0.01,0.01,-0.0075,0.0075,0.01,1000.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);
  glColor4f(1,1,1,1);
  glPointSize(2);
  glLineWidth(2);
  glTranslatef(0,0,-4);
  glBegin(GL_LINES);
  glVertex3f(-0.25,0,0);
  glVertex3f(0.25,0,0);
  glVertex3f(0,-0.25,0);
  glVertex3f(0,0.25,0);
  glEnd;
  glBegin(GL_LINE_STRIP);
  for i:=0 to 16 do begin
   glVertex3f(cos(i*pi/8)*0.125,sin(i*pi/8)*0.125,0);
  end;
  glEnd;
  glBegin(GL_LINE_STRIP);
  for i:=0 to 16 do begin
   glVertex3f(cos(i*pi/8)*0.06125*0.5,sin(i*pi/8)*0.06125*0.5,0);
  end;
  glEnd;
  glPointSize(1);
  glLineWidth(1);
  glEnable(GL_DEPTH_TEST);
 end;

var Collide:TPhysicsCollide;

function CheckCamera(d:single;const xPosition,View:TPhysicsVector3):boolean;
begin
 PhysicsCollidePoint(Collide,Vector3Add(xPosition,Vector3ScalarMul(Vector3Sub(View,xPosition),d)),0.5);
 result:=Collide.NumContacts=0;
end;

PROCEDURE MoveCamera(Geschwindigkeit:glFloat);
VAR V,np:TPhysicsVector3;
BEGIN
 V.X:=View.X-xPosition.X;
 V.Y:=View.Y-xPosition.Y;
 V.Z:=View.Z-xPosition.Z;
 np.X:=xPosition.X+V.X*Geschwindigkeit;
 np.Y:=xPosition.Y+V.Y*Geschwindigkeit;
 np.Z:=xPosition.Z+V.Z*Geschwindigkeit;
 if CheckCamera(1,xPosition,np) then begin
  xPosition:=np;
  View.X:=View.X+V.X*Geschwindigkeit;
  View.Y:=View.Y+V.Y*Geschwindigkeit;
  View.Z:=View.Z+V.Z*Geschwindigkeit;
 end;
END;

PROCEDURE MoveCameraSidewards(VAR xPosition,View:TPhysicsVector3;Geschwindigkeit:glFloat);
VAR Cross,ViewVector,np:TPhysicsVector3;
BEGIN
 Cross.X:=0;
 Cross.Y:=0;
 Cross.Z:=0;
 ViewVector.X:=View.X-xPosition.X;
 ViewVector.Y:=View.Y-xPosition.Y;
 ViewVector.Z:=View.Z-xPosition.Z;
 Cross.X:=(UpVector.Y*ViewVector.Z)-(UpVector.Z*ViewVector.Y);
 Cross.Y:=(UpVector.Z*ViewVector.X)-(UpVector.X*ViewVector.Z);
 Cross.Z:=(UpVector.X*ViewVector.Y)-(UpVector.Y*ViewVector.X);
 np.X:=xPosition.X+Cross.X*Geschwindigkeit;
 np.Y:=xPosition.Y;
 np.Z:=xPosition.Z+Cross.Z*Geschwindigkeit;
 if CheckCamera(1,xPosition,np) then begin
  xPosition:=np;
  View.X:=View.X+Cross.X*Geschwindigkeit;
  View.Z:=View.Z+Cross.Z*Geschwindigkeit;
 end;
END;

PROCEDURE RotateCamera(VAR xPosition:TPhysicsVector3;CONST X,Y,Z:glFloat);
VAR vVector:TPhysicsVector3;
BEGIN
 vVector.X:=View.X-xPosition.X;
 vVector.Y:=View.Y-xPosition.Y;
 vVector.Z:=View.Z-xPosition.Z;
 IF X<>0 THEN BEGIN
  View.Z:=xPosition.Z+SIN(X)*vVector.Y+COS(X)*vVector.Z;
  View.Y:=xPosition.Y+COS(X)*vVector.Y-SIN(X)*vVector.Z;
 END;
 IF Y<>0 THEN BEGIN
  View.Z:=xPosition.Z+SIN(Y)*vVector.X+COS(Y)*vVector.Z;
  View.X:=xPosition.X+COS(Y)*vVector.X-SIN(Y)*vVector.Z;
 END;
 IF Z<>0 THEN BEGIN
  View.X:=xPosition.X+SIN(Z)*vVector.Y+COS(Z)*vVector.X;
  View.Y:=xPosition.Y+COS(Z)*vVector.Y-SIN(Z)*vVector.X;
 END;
END;

VAR oldmousePos:TPoint;

PROCEDURE MoveCameraByMouse;
VAR mousePos:TPoint;
    middleX,middleY:INTEGER;
    deltaX,rotateX:glFloat;
    deltaY,rotateY:glFloat;
BEGIN

 middleX:=ScreenWidth  SHR 1;
 middleY:=ScreenHeight SHR 1;

 mousePos.X:=mx;
 mousePos.y:=my;

IF (mousePos.X=middleX) AND (mousePos.Y=middleY) THEN EXIT;

//IF (mousePos.X=oldmousePos.X) and (mousePos.y=oldmousePos.y) THEN EXIT;

 oldmousePos:=mousePos;

 rotateY:=(middleX-mousePos.X)/20000;
 deltaY:=(middleY-mousePos.Y)/40000;

 View.Y:=View.Y+deltaY*5;

 IF View.Y-xPosition.Y>10 THEN View.Y:=xPosition.Y+10;
 IF View.Y-xPosition.Y<(-10) THEN View.Y:=xPosition.Y-10;

 RotateCamera(xPosition,0,-rotateY,0);

END;

procedure SaveOldCamera;
begin
 OldPosition:=xPosition;
 OldView:=View;
 OldUpVector:=UpVector;
end;

procedure RestoreOldCamera;
begin
 xPosition:=OldPosition;
 View:=OldView;
 UpVector:=OldUpVector;
end;

var hDC:longword;
    MSG:TMsg;
    StartTime,LastTime,FST,FET:longword;
    i,j,k,kk,r,Frames,DX,DY:integer;
    TimeStep,FPS,Angle:single;
    v,v2,Gravitation:TPhysicsVector3;
    x,y,z,sx,ti:single;
    m:TPhysicsMatrix4x4;

const cwChop:word=$F7B;

procedure Fire;
var Point,Normal:TPhysicsVector3;
    o:PPhysicsObject;
begin
 o:=PhysicsRayIntersection(Physics,xPosition,Vector3Norm(Vector3Sub(View,xPosition)),Point,Normal);
 if assigned(o) and assigned(o^.RigidBody) then begin
  PhysicsRigidBodyAddImpulse(o^.RigidBody^,Point,Vector3ScalarMul(Vector3Neg(Normal),abs(Vector3Dot(Normal,Vector3Sub(View,xPosition))*10000)));
 end;
end;

procedure TestCamera;
var m:TPhysicsVector3;
    i:integer;
begin

 PhysicsCollidePoint(Collide,Vector3Add(xPosition,Vector3ScalarMul(Vector3Sub(View,xPosition),1)),8);
 m:=Vector3Origin;
 for i:=0 to Collide.NumContacts-1 do begin
  m:=Vector3Add(m,Vector3ScalarMul(Collide.Contacts^[i].Normal,Collide.Contacts^[i].Depth/Collide.NumContacts));
 end;
 xPosition:=Vector3Add(xPosition,m);
 View:=Vector3Add(View,m);
end;

const randseed:integer=$12345678;

function random(i:integer):integer;
begin
 randseed:=(randseed*$08088405)+1;
 result:=abs(randseed shr 12) mod i;
end;

procedure InitScreen;
begin

 ScreenWidth:=Form1.OpenGLPanel1.Width;
 ScreenHeight:=Form1.OpenGLPanel1.Height;
 ScreenWidthHalf:=ScreenWidth DIV 2;
 ScreenHeightHalf:=ScreenHeight DIV 2;
end;

procedure InitPhysics;
begin
 InitScreen;
  PhysicsInit(Physics);
 PhysicsInstance:=@Physics;
 PhysicsInstance^.SweepAndPruneWorkMode:=sapwmAXISAUTO;

 PhysicsInstance^.VelocityMax:=2400;
 PhysicsInstance^.AngularVelocityMax:=pi*8;

 PhysicsCollideInit(Collide);

 PhysicsObjectInit(Object0,BodyMesh);

 PhysicsObjectAddMesh(Object0);
 PhysicsObjectMeshCreateBox(Object0.Meshs^[0]^,-140,-140,-140);
 PhysicsObjectFinish(Object0);


 xPosition:=vector3(0,55,0);
 View:=vector3(0,55,1);
 UpVector:=vector3(0,1,0);

 sx:=0;
 x:=0;
 y:=0;
 j:=0;
 kk:=10;
 k:=kk;
 r:=0;
 for i:=0 to length(Objects)-1 do begin
  PhysicsObjectInit(Objects[i],BodySphere);
  PhysicsObjectAddMesh(Objects[i]);
  PhysicsObjectMeshCreateSphere(Objects[i].Meshs^[0]^,8,12);
  PhysicsObjectFinish(Objects[i]);
  y:=0;
  x:=(random(14)-7)*8;
  y:=(random(14)-7)*6;
  z:=(random(14)-7)*8;
  PhysicsObjectSetVector(Objects[i],Vector3(x,y,z));
  PhysicsRigidBodyInit(ObjectRigidBodies[i],@Objects[i],kk,0.5,0.8);
  x:=x+1;
  inc(j);
  if j>k then begin
   inc(r);
   j:=0;
   dec(k);
   x:=0;
   y:=y+1;
   sx:=sx+0.5;
  end;
 end;
 k:=10;

 Gravitation:=Vector3(0,-9.81*4,0);
 Physics.Gravitation:=Gravitation;
 Physics.VelocityMax:=100;
 yy:=8;
 StartTime:=GetTickCount64;
 LastTime:=0;

 Now:=GetTickCount64-StartTime;
 FST:=Now;
 FET:=FST;
 Frames:=0;
 FPS:=60;
 ti:=0;
end;

procedure RunPhysics;
begin
   Now:=GetTickCount64-StartTime;
  inc(Frames);
  if Frames=10 then begin
   inc(Frames);
   FET:=FST;
   FST:=Now;
   if (FST-FET)<>0 then begin
    FPS:=(Frames*1000)/(FST-FET);
   end;
   Frames:=0;
  end;
  TimeStep:=1/FPS;
  MoveCameraByMouse;
  Draw(TimeStep);

end;

Procedure ClosePhysics;
begin
 PhysicsObjectDone(Object0);

 for i:=0 to length(Objects)-1 do
 begin
  PhysicsObjectDone(Objects[i]);
  PhysicsRigidBodyDone(ObjectRigidBodies[i]);
 end;

 PhysicsCollideDone(Collide);
 PhysicsDone(Physics);
end;



//====================== OpenGLPanel1 Mouse Events ===================================

procedure TForm1.xxFormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  MoveCamera(Physics.TimeStep*WheelDelta);
end;

procedure TForm1.xxFormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
BEGIN
 mx:=x;
 my:=y;
END;

procedure TForm1.xxFormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
   begin
    Fire;
    exit;
   end;

  if ssRight in Shift then
   begin
    Physics.Gravitation:=Vector3Add(Gravitation,Vector3ScalarMul(Vector3Norm(Vector3Sub(View,xPosition)),1000));
    for i:=0 to length(Objects)-1 do begin
     ObjectRigidBodies[i].Frozen:=false;
     ObjectRigidBodies[i].FrozenTime:=0;
    end;
   end;
end;

procedure TForm1.xxFormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   if ssRight in Shift then
   begin
    Physics.Gravitation:=Gravitation;
    for i:=0 to length(Objects)-1 do begin
     ObjectRigidBodies[i].Frozen:=false;
     ObjectRigidBodies[i].FrozenTime:=0;
    end;
   end;
end;


//========================= Form1 Events/ functions =========================================
procedure TForm1.OpenGLPanel1Paint(Sender: TObject);
begin

  //-------------------------------------------------------------------------------
  glClearColor(0.0, 0.0, 0.0, 0.0);                      // Set the Background colour of or scene
  glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT);   // Clear the colour buffer
  glEnable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  gluPerspective(45.0, width / height, 0.1, 100.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();                                      // Load a "clean" model matrix on the stack
  //-------------------------------------------
   RunPhysics;
  //-------------------------------------------------------------------------------
   OpenGLPanel1.SwapBuffers;

end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  ClosePhysics;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

  OpenGLPanel1:=TOpenGLPanel.Create(Self);
  with OpenGLPanel1 do begin
    Name:='OpenGLPanel1';
    Align:=alClient;
    Parent:=Self;
    OnPaint:=OpenGLPanel1Paint;
    OnResize:=OpenGLPanel1Resize;
    AutoResizeViewport:=true;
  end;

  OpenGLPanel1.OnMouseMove:=xxFormMouseMove;
  OpenGLPanel1.OnMouseDown:=xxFormMouseDown;
  OpenGLPanel1.OnMouseUp:= xxFormMouseUp;
  OpenGLPanel1.OnMouseWheel:=xxFormMouseWheel;

  Application.AddOnIdleHandler(OnAppIdle);
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState );
begin

case key of

  VK_SPACE   :Fire;
  VK_UP      :MoveCamera(Physics.TimeStep*50);
  VK_DOWN    :MoveCamera(-Physics.TimeStep*50);
  VK_LEFT    :MoveCameraSidewards(xPosition,View,Physics.TimeStep*50);
  VK_RIGHT   :MoveCameraSidewards(xPosition,View,-Physics.TimeStep*50);
  VK_ESCAPE  :begin
               close;
              end;
end;
end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: char);
begin

  if (Key='F') then begin Fire; exit; end;
  if (Key='W') then begin MoveCamera(Physics.TimeStep*50); exit; end;
  if (Key='S') then begin MoveCamera(-Physics.TimeStep*50); exit; end;
  if (Key='A') then begin MoveCameraSidewards(xPosition,View,Physics.TimeStep*50); exit; end;
  if (Key='D') then begin MoveCameraSidewards(xPosition,View,-Physics.TimeStep*50); exit; end;

  if (Key='f') then begin Fire; exit; end;
  if (Key='w') then begin MoveCamera(Physics.TimeStep*50); exit; end;
  if (Key='s') then begin MoveCamera(-Physics.TimeStep*50); exit; end;
  if (Key='a') then begin MoveCameraSidewards(xPosition,View,Physics.TimeStep*50); exit; end;
  if (Key='d') then begin MoveCameraSidewards(xPosition,View,-Physics.TimeStep*50); exit; end;

  if Key='1' then
  begin
    Gravitation:=Vector3(0,-9.81*4,0);
    Physics.Gravitation:=Gravitation;
    for i:=0 to length(Objects)-1 do begin
     ObjectRigidBodies[i].Frozen:=false;
     ObjectRigidBodies[i].FrozenTime:=0;
    end;
     exit;
   end;

   if Key='2' then
   begin
    Gravitation:=Vector3(0,0,0);
    Physics.Gravitation:=Gravitation;
    for i:=0 to length(Objects)-1 do begin
     ObjectRigidBodies[i].Frozen:=false;
     ObjectRigidBodies[i].FrozenTime:=0;
    end;
    exit;
   end;

   if Key='3' then
   begin
     Gravitation:=Vector3(0,9.81*4,0);
    Physics.Gravitation:=Gravitation;
    for i:=0 to length(Objects)-1 do begin
     ObjectRigidBodies[i].Frozen:=false;
     ObjectRigidBodies[i].FrozenTime:=0;
    end;
     exit;
   end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  InitPhysics;
  mx:=0;
  my:=0;
end;

procedure TForm1.OpenGLPanel1Resize(Sender: TObject);
begin
  InitScreen;
  if OpenGLPanel1.Height <= 0 then exit;
end;

procedure TForm1.OnAppIdle(Sender: TObject; var Done: Boolean);
begin
  Done:=false;
  OpenGLPanel1.Invalidate;
end;

//===============================================================================================
initialization
  {$I demo2mw.lrs}

end.

