
{***************************************************************
  CodeTyphon studio
  Adaptation 
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit MP3Playermw;

interface

uses
  LCLIntf, LCLType, LMessages, Messages,
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, DSPack;

type
  TForm2 = class(TForm)
    OpenDlgSong: TOpenDialog;
    tmTrackbar: TTimer;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    pnlDisplay: TPanel;
    btnPlay: TSpeedButton;
    btnPause: TSpeedButton;
    btnStop: TSpeedButton;
    btnPrevious: TSpeedButton;
    btnNext: TSpeedButton;
    btnOpen: TSpeedButton;
    Bevel1: TBevel;
    sbcTrackbar: TScrollBar;
    sbcVolume: TScrollBar;
    lbPlayList: TListBox;
    btnAdd: TSpeedButton;
    btnRemove: TSpeedButton;
    cbOrder: TComboBox;
    Bevel2: TBevel;
    btnPlSave: TSpeedButton;
    btnPlOpen: TSpeedButton;
    sbcFreq: TScrollBar;
    lbFiles: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnPlayClick(Sender: TObject);
    procedure btnPauseClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure sbcTrackbarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: integer);
    procedure tmTrackbarTimer(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnRemoveClick(Sender: TObject);
    procedure lbPlayListDblClick(Sender: TObject);
    procedure lbPlayListKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
  private
    procedure AddFile(FileName: string);
    procedure PlayItem(Item: integer);
    procedure RemoveSelected;

  public
  end;

var
  Form2: TForm2;
  Tracking: boolean;
  FilterGraph1: TFilterGraph;

implementation

{$R *.lfm}

procedure TForm2.AddFile(FileName: string);
begin
  lbPlayList.Items.Add(ExtractFileName(FileName));
  lbFiles.Items.Add(FileName);

  if lbPlayList.ItemIndex = -1 then
    lbPlayList.ItemIndex := lbPlayList.Items.Count - 1;
end;

procedure TForm2.PlayItem(Item: integer);
begin
  if Item < 0 then
    exit;

  tmTrackbar.Enabled := False;

  case FilterGraph1.State of
    gsPlaying, gsStopped, gsUninitialized:
    begin
      FilterGraph1.Stop;
      FilterGraph1.ClearGraph;
      FilterGraph1.Active := True;
      FilterGraph1.RenderFile(lbFiles.Items.Strings[Item]);
      FilterGraph1.Position := 0;

      pnlDisplay.Caption := lbPlayList.Items.Strings[Item];
      sbcTrackbar.Min := 0;
      sbcTrackbar.Max := FilterGraph1.Duration;
      sbcTrackbar.Position := 0;
    end;

    gsPaused:
    begin

    end;

  end;


  FilterGraph1.Play;
  tmTrackbar.Enabled := True;
  ;
end;

procedure TForm2.RemoveSelected;
var
  OldIndex: integer;
begin
  OldIndex := lbPlaylist.ItemIndex;
  lbPlaylist.Items.Delete(OldIndex);
  lbFiles.Items.Delete(OldIndex);

  if OldIndex > lbPlaylist.Items.Count - 1 then
    OldIndex := lbPlaylist.Items.Count - 1;

  lbPlaylist.ItemIndex := OldIndex;
end;

procedure TForm2.btnAddClick(Sender: TObject);
begin
  if OpenDlgSong.Execute = False then  exit;

  AddFile(OpenDlgSong.FileName);
end;

procedure TForm2.btnOpenClick(Sender: TObject);
begin
  if OpenDlgSong.Execute = False then
    exit;

  AddFile(OpenDlgSong.FileName);
  lbPlaylist.ItemIndex := lbPlaylist.Items.Count - 1;

  PlayItem(lbPlaylist.ItemIndex);
end;

procedure TForm2.btnPauseClick(Sender: TObject);
begin
  FilterGraph1.Pause;
end;

procedure TForm2.btnPlayClick(Sender: TObject);
begin
  PlayItem(lbPlaylist.ItemIndex);
end;

procedure TForm2.btnRemoveClick(Sender: TObject);
begin
  RemoveSelected;
end;

procedure TForm2.btnStopClick(Sender: TObject);
begin
  FilterGraph1.Stop;
  FilterGraph1.Position := 0;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  FilterGraph1 := TFilterGraph.Create(nil);
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  FilterGraph1.Free;
end;

procedure TForm2.lbPlayListDblClick(Sender: TObject);
begin
  PlayItem(lbPlaylist.ItemIndex);
end;

procedure TForm2.lbPlayListKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
    RemoveSelected;
end;

procedure TForm2.sbcTrackbarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: integer);
begin
  if ScrollCode = scEndScroll then
  begin
    FilterGraph1.Position := ScrollPos;
    Tracking := False;
  end
  else
    Tracking := True;
end;

procedure TForm2.tmTrackbarTimer(Sender: TObject);
begin
  if Tracking = False then
    sbcTrackbar.Position := FilterGraph1.Position;
end;

end.
