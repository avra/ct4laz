program DVDPlay2;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  dvdplaymain2 ,
  ColorControl ;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormDVDPlayer, FormDVDPlayer);
  Application.CreateForm(TColorControlForm, ColorControlForm);
  Application.Run;
end.
