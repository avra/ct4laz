program DVDPlay;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  DVDPlaymain;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormDVDPlayer, FormDVDPlayer);
  Application.Run;
end.
