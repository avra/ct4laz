unit SelectURL;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFormSelectURL = class(TForm)
    btOK: TButton;
    btCancel: TButton;
    URL: TEdit;
  private
    { Dιclarations privιes }
  public
    { Dιclarations publiques }
  end;

var
  FormSelectURL: TFormSelectURL;

implementation

{$R *.lfm}

end.
