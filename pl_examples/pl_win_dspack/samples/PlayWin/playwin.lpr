program playwin;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  playwinmain,
  SelectURL;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormPlayWin, FormPlayWin);
  Application.Run;
end.
