program PlayCap;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  PlayCapmain in 'main.pas' {VideoForm},
  Allocator in '..\Allocator\Allocator.pas',
  PlaneScene in '..\Allocator\PlaneScene.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TVideoForm, VideoForm);
  Application.Run;
end.
