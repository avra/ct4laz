program PlayFile;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  PlayFilemain in 'main.pas' {MainForm},
  PlaneScene in '..\Allocator\PlaneScene.pas',
  Allocator in '..\Allocator\Allocator.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
