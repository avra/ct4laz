program VMRMixer;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  VMRMixermain;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormVMRMixer, FormVMRMixer);
  Application.Run;
end.
