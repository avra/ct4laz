{***************************************************
 Titan Scripter Import unit
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com
 This file is part of CodeTyphon Studio
***********************************************************************}

unit PasDoc_TitanScripterGen;

interface
 uses
  PasDoc_Utils,
  PasDoc_Gen,
  PasDoc_Items,
  PasDoc_Languages,
  PasDoc_StringVector,
  PasDoc_Types,
  Classes,
  PasDoc_ObjectVector,
  PasDoc_TitanScripterFileStore,
  PasDoc_StringPairVector;

const
 cntsIMPORTERVERSION='5.41';

type

TTitanScriptDocGenerator = class(TDocGenerator)
    private
       fRegFunctions:TStrings;

       fsConstants:TStrings;
       fsTypes:TStrings;
       fsFuncsProcs:TStrings;
       fsVariables:TStrings;
       fsCIOs:TStrings;
       fsCIOsWappers:TStrings;
       fsClassOf:TStrings;

       fsFixUnitsList:TStrings;
       fsMissUnitsList:TStrings;
    protected
       function CodeString(const s: string): string; override;
       function ConvertString(const s: string): string; override;
       function ConvertChar(c: char): string; override;
       procedure WriteExternalCore(const ExternalItem: TExternalItem; const Id: TTranslationID); override;
       function FormatSection(HL: integer; const Anchor: string;const Caption: string): string; override;
       function FormatAnchor(const Anchor: string): string;override;
       function FormatList(ListData: TListData): string; override;
       function FormatTable(Table: TTableData): string; override;
       Function CheckForBAD(sl:TStrings;Const Val:String):boolean;
       Procedure FindFixUnits;

       function  Fix_EmptyStrings(const aStr:string):string;
       function  Fix_LongStrings(const aStr:string):string;

       //.................................................
       Function  GetType_Type(itm:TPasItem):TItemType;
       Function  Get_StrTo_RegType(const aStr:string):string;
       //.................................................
       procedure Find_CIO_Class(cl:TPasCio);
       procedure Find_CIO_RECORD(cl:TPasCio; const IsPacked:Boolean=false);
       procedure Find_Type_ClassOf(itm:TPasItem);
       procedure Find_Type_Array(itm:TPasItem);
       procedure Find_Type_DynArray(itm:TPasItem);
       //.................................................
       Procedure FindUnit_Constants(U: TPasUnit);
       Procedure FindUnit_FuncsProcs(U: TPasUnit);
       procedure FindUnit_Variables(U: TPasUnit);
       procedure FindUnit_Types(U: TPasUnit);
       procedure FindUnit_CIOs(U: TPasUnit);
       //...........................................
       procedure WriteUnitHeader(U: TPasUnit);
       procedure WriteUnitUses(U: TPasUnit);
       procedure WriteUnitRegProcStart(U: TPasUnit);

       procedure WriteUnit_Constants;
       procedure WriteUnit_FuncsProcs;
       procedure WriteUnit_Variables;
       procedure WriteUnit_Types;
       procedure WriteUnit_CIOs;
       procedure WriteUnit_ClassOf;

       procedure WriteUnitinItialization(U: TPasUnit);

       procedure WriteUnit(const HL: integer; const U: TPasUnit);override;
       procedure WriteUnits(const HL: integer);
       procedure WriteTotalRegistrationUnit;
       procedure WriteExtraUnit;
    public
      constructor Create(AOwner: TComponent); override;
      destructor Destroy; override;
      function GetFileExtension: string; override;
      function GetFilePrefix: string;
      procedure WriteDocumentation; override;
    published

    end;

  implementation
    uses SysUtils,Variants;

  constructor TTitanScriptDocGenerator.Create(AOwner: TComponent);
  begin
    inherited;
    fRegFunctions:=TStringList.Create;
    fsConstants:=TStringList.Create;
    fsTypes:=TStringList.Create;
    fsFuncsProcs:=TStringList.Create;
    fsVariables:=TStringList.Create;
    fsCIOs:=TStringList.Create;
    fsCIOsWappers:=TStringList.Create;
    fsClassOf:=TStringList.Create;
    //....
    fsFixUnitsList:=TStringList.Create;
    fsMissUnitsList:=TStringList.Create;

  end;

  destructor TTitanScriptDocGenerator.Destroy;
  begin
    fsConstants.Free;
    fsTypes.Free;
    fsFuncsProcs.Free;
    fsVariables.Free;
    fsCIOs.Free;
    fsCIOsWappers.Free;
    fRegFunctions.Free;
    fsClassOf.Free;
    //....
    fsFixUnitsList.Free;
    fsMissUnitsList.Free;
    inherited;
  end;

  function TTitanScriptDocGenerator.GetFileExtension: string;
  begin
    Result := '.pas';
  end;

  function TTitanScriptDocGenerator.GetFilePrefix: string;
  begin
    Result := '_imp';
  end;

  function TTitanScriptDocGenerator.CodeString(const s: string): string;
   begin
   end;
  function TTitanScriptDocGenerator.ConvertString(const s: string): string;
   begin
   end;
  function TTitanScriptDocGenerator.ConvertChar(c: char): string;
   begin
   end;
  procedure TTitanScriptDocGenerator.WriteExternalCore(const ExternalItem: TExternalItem; const Id: TTranslationID);
   begin
   end;
  function TTitanScriptDocGenerator.FormatSection(HL: integer; const Anchor: string;const Caption: string): string;
   begin
   end;
  function TTitanScriptDocGenerator.FormatAnchor(const Anchor: string): string;
   begin
   end;
  function TTitanScriptDocGenerator.FormatList(ListData: TListData): string;
   begin
   end;
  function TTitanScriptDocGenerator.FormatTable(Table: TTableData): string;
   begin
   end;



  //===============================================================
  Function TTitanScriptDocGenerator.CheckForBAD(sl:TStrings;Const Val:String):boolean;
    var x:integer;
   begin
     result:=false;
     if sl=nil then exit;
     for x := 0 to sl.Count - 1 do
      if SameText(sl[x],Val) then begin result:=true; exit; end;
   end;

  function  TTitanScriptDocGenerator.Fix_LongStrings(const aStr:string):string;
  var iss:string;
      l,iv,ivs:Integer;

      Procedure _FindToLeft;
         var i:integer;
        begin

         for I:=iv downto 1 do
            if iss[i]=';' then
              begin
               ivs:=i;
               exit;
              end;
        end;

    begin
      result:=aStr;
      iss:=result;
      l:=System.Length(iss);

      if L<MAXPSTRINGLength then Exit;

      iv:=MAXPSTRINGLength;
      ivs:=0;
      _FindToLeft;
      if ivs=0 then exit;
      system.Insert('''+'+LineEnding+'          ''',iss,ivs+1);
      result:=iss;
    end;


  function  TTitanScriptDocGenerator.Fix_EmptyStrings(const aStr:string):string;
    var iss,sst:string;
        iv,iv2,ivs,l:Integer;
       //...............................
       Procedure _FindToLeft;
         var i:integer;
        begin
         for I:=iv downto 1 do
            if iss[i]='=' then
              begin
               ivs:=i;
               exit;
              end;
        end;
        //...............................
        Procedure _FindToRight;
         var i:integer;
        begin
         for I:=iv to system.Length(iss) do
            if (iss[i]=';') or(iss[i]=')') then
              begin
               iv2:=i;
               exit;
              end;
        end;
       //...............................
   begin
     result:=aStr;
     iss:=result;

     iv:=-1;
     repeat
     iv:=Pos('''''',iss);
     ivs:=iv;
     if iv>0 then
       begin
         _FindToLeft;
         System.Delete(iss,ivs,iv-ivs+2);
       end;
     until iv<=0;

     iv:=-1;
     repeat
     iv:=Pos('''',iss);
     ivs:=iv;
     iv2:=iv;
     if iv>0 then
       begin
         _FindToLeft;
         _FindToRight;
         l:=System.Length(iss);
         System.Delete(iss,ivs,iv2-ivs);
       end;
     until iv<=0;

     result:=iss;
   end;

  Procedure TTitanScriptDocGenerator.FindFixUnits;
    var i,ix:Integer;
        iU: TPasUnit;
        b:boolean;
      //...........................................
      Procedure _AddToMissUnits(aname:string);
       begin
        if fsMissUnitsList.IndexOf(aname)=-1 then
          fsMissUnitsList.Add(aname);
       end;

      function _MoveUnit(const aname:string):boolean;
         var i1,i2,i2t,ixx,ixx2:integer;
             ss1,ss2:string;
             Ux: TPasUnit;
       begin
         result:=false;
         ss1:=aname;

         for I1:=0 to Units.Count - 1 do
         begin

         Ux:=Units.UnitAt[I1];
         for i2 := 0 to Ux.UsesUnits.Count  - 1 do
          begin
          ss2:=Ux.UsesUnits.Strings[i2];

          if SameText(ss1,ss2) then
           begin
            ixx:=fsFixUnitsList.IndexOf(ss1);
            ixx2:=fsFixUnitsList.IndexOf(Ux.Name);
              if  (ixx+1)>ixx2 then
               if (ixx+1)<fsFixUnitsList.Count then
                begin
                  fsFixUnitsList.Move(ixx2,ixx+1);
                  result:=true;
                end;
           end;
         end;
        end;
       end;

      procedure  _CheckUnit(U: TPasUnit);
        var i2,i2t,ix:integer;
            ss1:string;
       begin

         for I2 := 0 to U.UsesUnits.Count  - 1 do
          begin
            ss1:=U.UsesUnits.Strings[i2];
            _AddToMissUnits(ss1);
          end;

         ss1:=U.Name;

          repeat
           b:=_MoveUnit(ss1);
          until b=false;


       end;
      //...........................................
   begin
      fsFixUnitsList.Clear;
      fsMissUnitsList.Clear;

      //add all Units to FixUnits
      for i:=0 to Units.Count - 1 do
         begin
          iU:=Units.UnitAt[i];
          if iU=nil then Continue;
          fsFixUnitsList.Add(iU.name);
         end;
      //Check unit
      for i:=0 to Units.Count - 1 do
         begin
          iU:=Units.UnitAt[i];
          if iU=nil then Continue;
          _CheckUnit(iU);
         end;
      //clear Miss Units
      for i:=0 to Units.Count - 1 do
         begin
          iU:=Units.UnitAt[i];
          if iU=nil then Continue;
          ix:=fsMissUnitsList.IndexOf(iU.name);
          if ix>-1 then fsMissUnitsList.Delete(ix);
         end;

   end;

  procedure TTitanScriptDocGenerator.WriteExtraUnit;
    begin
      if VarFilesStore.ExtraUnitName='' then exit;
      CreateStream(VarFilesStore.ExtraUnitName+GetFileExtension);
      WriteDirectline(VarFilesStore.ExtraUnit.Text);
      CloseStream;
    end;

  procedure TTitanScriptDocGenerator.WriteTotalRegistrationUnit;
    var i:Integer;
        fi:string;

        function _fix(a:string):string;
         begin
           result:=ChangeFileExt(a,'');
         end;

   begin
       FindFixUnits;

       CreateStream(VarFilesStore.Title+GetFileExtension);
       WriteDirectline('{***************************************************************************');
       WriteDirectline('   Titan Scripter TOTAL Registration file ');
       WriteDirectline('   Generated by TitanScript UnitImporter '+cntsIMPORTERVERSION);
       WriteDirectline('   Create at : '+DateToStr(date)+' '+TimeToStr(Now));
       WriteDirectline(' ***************************************************************************}');
       WriteDirectline('');

       WriteDirectline('Unit '+VarFilesStore.Title+';');
       WriteDirectline(' Interface');
       WriteDirectline(' Uses');
       WriteDirectline( '//..... Used Units .........................');

       if NOT (VarFilesStore.ExtraUnitName='') then
         WriteDirectline('  '+VarFilesStore.ExtraUnitName+',  //Extra unit');

       for i :=0 to fsFixUnitsList.Count - 1 do
        begin
           WriteDirectline('  '+fsFixUnitsList[i]+',');
        end;

        WriteDirectline('');
        WriteDirectline(' //.... Import Units .......................');

       if fsFixUnitsList.Count>1 then
       for i :=0 to fsFixUnitsList.Count - 2 do
        begin
           WriteDirectline('  '+fsFixUnitsList[i]+GetFilePrefix+',');
        end;

        if fsFixUnitsList.Count>0 then
        WriteDirectline('  '+fsFixUnitsList[fsFixUnitsList.Count - 1]+GetFilePrefix+';');

        WriteDirectline('');
        WriteDirectline('Implementation');
        WriteDirectline('end.');

        WriteDirectline('{***********************************************************************');
        WriteDirectline('');
        WriteDirectline(' //.............. $$$_MissUnits ..........................');
        for i :=0 to fsMissUnitsList.Count - 1 do
        begin
           WriteDirectline(fsMissUnitsList[i]);
        end;
        WriteDirectline('//.............. $$$_BadConsts ..........................');
        WriteDirectline(IntToStr(VarFilesStore.BadConsts.Count));
        for i :=0 to VarFilesStore.BadConsts.Count - 1 do
        begin
           WriteDirectline(VarFilesStore.BadConsts[i]);
        end;

        WriteDirectline('//.............. $$$_BadTypes ..........................');
        WriteDirectline(IntToStr(VarFilesStore.BadTypes.Count));
        for i :=0 to VarFilesStore.BadTypes.Count - 1 do
        begin
           WriteDirectline(VarFilesStore.BadTypes[i]);
        end;

        WriteDirectline('//.............. $$$_BadVars ..........................');
        WriteDirectline(IntToStr(VarFilesStore.BadVars.Count));
        for i :=0 to VarFilesStore.BadVars.Count - 1 do
        begin
           WriteDirectline(VarFilesStore.BadVars[i]);
        end;

        WriteDirectline('//.............. $$$_BadCIOs ..........................');
        WriteDirectline(IntToStr(VarFilesStore.BadCIOS.Count));
        for i :=0 to VarFilesStore.BadCIOS.Count - 1 do
        begin
           WriteDirectline(VarFilesStore.BadCIOS[i]);
        end;

        WriteDirectline('//.............. $$$_BadFunProcs ..........................');
        WriteDirectline(IntToStr(VarFilesStore.BadFunProcs.Count));
        for i :=0 to VarFilesStore.BadFunProcs.Count - 1 do
        begin
           WriteDirectline(VarFilesStore.BadFunProcs[i]);
        end;

        WriteDirectline(' //.............. $$$_FilesUsed ..........................');
        WriteDirectline(IntToStr(VarFilesStore.FileStore.Count));
        for i :=0 to VarFilesStore.FileStore.Count - 1 do
        begin
           WriteDirectline(VarFilesStore.FileStore[i]);
        end;
        WriteDirectline('');
        WriteDirectline(' ***********************************************************************}');

      CloseStream;
   end;

  //==============================================================
  procedure TTitanScriptDocGenerator.WriteUnitHeader(U: TPasUnit);
   begin
      if U=nil then exit;

      WriteDirectline('{***************************************************************************');
      WriteDirectline('   Titan Scripter Unit Regisration file');
      WriteDirectline('   Generated by TitanScript UnitImporter '+cntsIMPORTERVERSION);
      WriteDirectline('   Build from : '+U.Name+'.pas');
      WriteDirectline('   at : '+DateToStr(date)+' '+TimeToStr(Now));
      WriteDirectline(' ***************************************************************************}');
      WriteDirectline('');
      WriteDirectline('Unit ' + U.Name+GetFilePrefix + ';');
      WriteDirectline('Interface');
   end;

  procedure TTitanScriptDocGenerator.WriteUnitUses(U: TPasUnit);
   var i:Integer;
   begin
      if U=nil then exit;

      WriteDirectline('Uses');

      for I := 0 to U.UsesUnits.Count  - 1 do
        if  sametext('windows',U.UsesUnits.Strings[i]) then
         begin  // must check for "windows" init
          WriteDirectline(' //'+U.UsesUnits.Strings[i]+',   //you must test this');
          WriteDirectline(' '+'LCLIntf, LCLType, LMessages, // you must test this with above line');
         end else
         WriteDirectline(' '+U.UsesUnits.Strings[i]+',');

       WriteDirectline(' '+U.Name+',');
       WriteDirectline(' '+'base_engine, titanscripter;'); // must units for script import functions
       WriteDirectline('');
       WriteDirectline('  procedure RegisterTotalFor_'+U.Name+';');
       WriteDirectline('');
       WriteDirectline('Implementation');
       WriteDirectline('  var H,G:Integer;');
       WriteDirectline('      GoR,GoW:TTitanMethodDefinition;');
   end;

  //===========================================================================
  Function TTitanScriptDocGenerator.Get_StrTo_RegType(const aStr:string):string;
    var ss:string;
   begin
    result:='';
    if aStr='' then exit;
    ss:=UpperCase(astr);
    if SameText(ss,'LONGINT')     then begin result:= '_typeINTEGER'; exit; end;
    if SameText(ss,'INTEGER')     then begin result:= '_typeINTEGER'; exit; end;
    if SameText(ss,'DOUBLE')      then begin result:= '_typeDOUBLE'; exit; end;
    if SameText(ss,'SINGLE')      then begin result:= '_typeSINGLE'; exit; end;
    if SameText(ss,'EXTENDED')    then begin result:= '_typeEXTENDED'; exit; end;
    if SameText(ss,'INT64')       then begin result:= '_typeINT64'; exit; end;
    if SameText(ss,'CARDINAL')    then begin result:= '_typeCARDINAL'; exit; end;
    if SameText(ss,'CURRENCY')    then begin result:= '_typeCURRENCY'; exit; end;
    if SameText(ss,'SMALLINT')    then begin result:= '_typeSMALLINT'; exit; end;
    if SameText(ss,'SHORTINT')    then begin result:= '_typeSHORTINT'; exit; end;
    if SameText(ss,'LONGWORD')    then begin result:= '_typeLONGWORD'; exit; end;
    if SameText(ss,'BOOLEAN')     then begin result:= '_typeBOOLEAN'; exit; end;
    if SameText(ss,'BYTE')        then begin result:= '_typeBYTE'; exit; end;
    if SameText(ss,'CHAR')        then begin result:= '_typeCHAR'; exit; end;
    if SameText(ss,'STRING')      then begin result:= '_typeSTRING'; exit; end;
    if SameText(ss,'WORD')        then begin result:= '_typeWORD'; exit; end;
    if SameText(ss,'SHORTSTRING') then begin result:= '_typeSHORTSTRING'; exit; end;
    if SameText(ss,'WIDECHAR')    then begin result:= '_typeWIDECHAR'; exit; end;
    if SameText(ss,'WIDESTRING')  then begin result:= '_typeWIDESTRING'; exit; end;
    if SameText(ss,'WORDBOOL')    then begin result:= '_typeWORDBOOL'; exit; end;
    if SameText(ss,'LONGBOOL')    then begin result:= '_typeLONGBOOL'; exit; end;
    if SameText(ss,'BYTEBOOL')    then begin result:= '_typeBYTEBOOL'; exit; end;
    if SameText(ss,'PCHAR')       then begin result:= '_typePCHAR'; exit; end;
    if SameText(ss,'PVOID')       then begin result:= '_typePVOID'; exit; end;
    if SameText(ss,'PWIDECHAR')   then begin result:= '_typePWIDECHAR'; exit; end;
    if SameText(ss,'POINTER')     then begin result:= '_typePOINTER'; exit; end;
    if SameText(ss,'DATE')        then begin result:= '_typeDATE'; exit; end;
    if SameText(ss,'DATETIME')    then begin result:= '_typeDATETIME'; exit; end;
   end;

  Function TTitanScriptDocGenerator.GetType_Type(itm:TPasItem):TItemType;
    var ss:string;
        l:Integer;
   begin
      result:=itNone;

      if itm=nil then Exit;

      if itm is TPasEnum then begin result:=itEmul; exit; end;

      ss:=UpperCase(itm.FullDeclaration);

      l:=system.Length(itm.Name);
      System.Delete(ss,1,l);      //Delete Item Name


      if (Pos('POINTER',ss)>0) then begin  result:=itPointer; exit; end;
      if (Pos('^',ss)>0)       then begin  result:=itPointerOf; exit; end;

      if (Pos('ARRAY',ss)>0) and (Pos('[',ss)>0)and (Pos('..',ss)>0) and (Pos(']',ss)>0) and (Pos('OF',ss)>0) then begin  result:=itArray; exit; end;
      if (Pos('ARRAY',ss)>0) and (Pos('OF',ss)>0) then begin  result:=itDynArray; exit; end;


      if (Pos('TGUID',ss)>0)   then begin  result:=itGUID; exit; end;

      if (Pos('CLASS',ss)>0) and (Pos('OF',ss)>0) then begin result:=itClassOf; exit; end;

      if (Pos('OBJECT',ss)>0) and (Pos('OF',ss)>0) then begin  result:=itEvent; exit; end;

      if (Pos('SET',ss)>0) and (Pos('OF',ss)>0) then begin  result:=itSetOf; exit; end;

      if (Pos('[',ss)>0) and (Pos(']',ss)>0) then begin  result:=itSetOfNoType; exit; end;

      if (Pos('FUNCTION',ss)>0)  then begin  result:=itFunction; exit; end;
      if (Pos('PROCEDURE',ss)>0) then begin  result:=itProcedure; exit; end;

      if (Pos('=',ss)>0) and (Pos('(',ss)>0) and (Pos(',',ss)>0)and (Pos(')',ss)>0) then begin  result:=itRecordDef; exit; end;
      if (Pos('..',ss)>0) then begin  result:=itSimpleRange; exit; end;
   end;

  //============================ Constants =================================================
  Procedure TTitanScriptDocGenerator.FindUnit_Constants(U: TPasUnit);
  var i:integer;
        itm:TPasItem;
        it:TItemType;
        ss:string;
   begin
     if U.Constants.Count<1 then Exit;

     for I :=0 to U.Constants.Count - 1 do
       begin

         itm:=TPasItem(U.Constants.Items[i]);
         if itm=nil then Continue;

         //........................................................................................
            ss:=UpperCase(itm.Name);
            if Pos('_',ss)=1 then Continue;
            if Pos('DBCOLUMN_',ss)>0 then Continue;    // DO NOT Inmport all DBCOLUMN_*** Consts at OleDB.pas
         //........................................................................................

         if CheckForBAD(VarFilesStore.BadConsts,itm.Name) then Continue; //Check For Bad  Constant

         it:=GetType_Type(itm);

          case it of
             itSetOfNoType:    fsConstants.Add(' //itSetOfNoType  =============== Constant ============= '+itm.name);
             itEmul:           fsConstants.Add(' //itEmul      =============== Constant ============= '+itm.name);
             itClassOf:        fsConstants.Add(' //itClassOf   =============== Constant ============= '+itm.name);
             itSetOf:          fsConstants.Add(' //itSetOf     =============== Constant ============= '+itm.name);
             itPointer:        fsConstants.Add(' //itPointer   =============== Constant ============= '+itm.name);
             itPointerOf:      fsConstants.Add(' //itPointerOf =============== Constant ============= '+itm.name);
             itArray:          fsConstants.Add(' //itArray     =============== Constant ============= '+itm.name);
             itDynArray:       fsConstants.Add(' //itDynArray  =============== Constant ============= '+itm.name);
             itEvent:          fsConstants.Add(' //itEvent     =============== Constant ============= '+itm.name);
             itRecord:         fsConstants.Add(' //itRecord    =============== Constant ============= '+itm.name);
             itFunction:       fsConstants.Add(' //itFunction  =============== Constant ============= '+itm.name);
             itProcedure:      fsConstants.Add(' //itProcedure =============== Constant ============= '+itm.name);
             itGUID     :      fsConstants.Add(' //itGUID      =============== Constant ============= '+itm.name);
             itRecordDef:      fsConstants.Add(' //itRecordDef =============== Constant ============= '+itm.name);
             itSimpleRange:    fsConstants.Add(' //itSimpleRange =============== Constant ============= '+itm.name);
           else
             fsConstants.Add('  RegisterConstantSX('''+itm.name+''','+itm.name+');');
           end;


       end;
   end;


  procedure TTitanScriptDocGenerator.WriteUnit_Constants;
    var i,f,iStart,iEnd,itotal:integer;

        Procedure _GWrite(const ifrom,ito,id:integer);
          var z:integer;
              ss:string;
         begin
           ss:='_RegConstants'+IntToStr(id);
           WriteDirectline(' ');
           WriteDirectline('procedure '+ss+';');
           WriteDirectline(' Begin');
           for z := ifrom to ito-1 do WriteDirectline(fsConstants[z]);
           WriteDirectline(' End;');
           fRegFunctions.Add(ss+';');
         end;
   begin
     if fsConstants.Count=0 then Exit;

     if fsConstants.Count<MAXCONSTSINSECTION then
       begin
         _GWrite(0,fsConstants.Count,0);
       end else
       begin
         itotal:=fsConstants.Count;
         iStart:=0;
         iEnd:=MAXCONSTSINSECTION;
         i:=0;
         repeat
            _GWrite(iStart,iEnd,i);

            iStart:=iEnd;
            iEnd:=iStart+MAXCONSTSINSECTION;
            if iEnd>itotal then iEnd:=itotal;

            inc(i);
         until (iStart>itotal) or (iEnd-iStart<1) ;

       end;
   end;

  //========================= Types ============================================
  procedure TTitanScriptDocGenerator.Find_Type_DynArray(itm:TPasItem);
    var ss,ssu,st:string;
        i1,i2,r1,r2:integer;
        isok,IsPacked:boolean;
   begin
      if itm=nil then Exit;
      isOk:=true;

      ss:=itm.FullDeclaration;
      i1:=Pos('=',ss);
      if i1>0 then system.delete(ss,1,i1+1);
      ss:=Trim(ss);

      ssu:=ss;
      ss:=UpperCase(ss);
      st:='';
      i1:=Pos('OF',ss);
      i2:=Pos(';',ss);
      if i1>0 then St:=system.copy(ssu,i1+2,i2-i1-2);

      st:=Trim(st);

     if st='' then isOk:=false;

      if isOk=false then
       begin
        fsTypes.Add(' //itDynArray    =============== Type ============= '+itm.name);
       end else
       begin
         fsTypes.Add('  RegisterDynArrayTypeSX('''+itm.Name+''','''+st+''');');
       end;
   end;

  procedure TTitanScriptDocGenerator.Find_Type_Array(itm:TPasItem);
    var ss,s1,s2,st:string;
        i1,i2,r1,r2:integer;
        isok,IsPacked:boolean;
   begin
      if itm=nil then Exit;
      isOk:=true;
      ss:=UpperCase(itm.FullDeclaration);

      //...........find if is packed ......................
      // IsPacked:=Pos('PACKED',ss)>0;
      //...........find string of type of array............
      st:='';
      i1:=Pos('OF',ss);
      i2:=Pos(';',ss);
      st:=system.Copy(itm.FullDeclaration,i1+2,i2-i1-2);
      st:=Trim(st);
     // st:=Get_StrTo_RegType(st);
      //...........find range ............
      s1:='';
      s2:='';

      i1:=Pos('[',ss);
      i2:=Pos('..',ss);
      if (i1>0) and (i2>0) then s1:=system.Copy(itm.FullDeclaration,i1+1,i2-i1-1);

      i1:=Pos('..',ss);
      i2:=Pos(']',ss);
      if (i1>0) and (i2>0) then s2:=system.Copy(itm.FullDeclaration,i1+2,i2-i1-2);

     if (s1<>'') and (s2<>'')then
      isOk:=true else
      isOk:=false;

     if st='' then isOk:=false;

      if isOk=false then
       begin
        fsTypes.Add(' //itArray    =============== Type ============= '+itm.name);
       end else
       begin
         //if IsPacked then ss:='true' else ss:='false';
         fsTypes.Add('  RegisterArrayTypeSX('''+itm.Name+''','''+st+''','+S1+','+S2+');');
       end;

   end;

  procedure TTitanScriptDocGenerator.FindUnit_Types(U: TPasUnit);
    var i:integer;
        it:TItemType;
        itm:TPasItem;
        ss:string;

        Function _GetTypeAlias(const aStr:string):string;
          var i1:integer;
              ss1,ss2:string;
         begin
           result:='';
           ss1:=aStr;
           i1:=Pos('=',ss1);
           if i1=0 then exit;
           system.Delete(ss1,1,i1);

           ss2:=UpperCase(ss1);
           i1:=Pos('TYPE',ss2);
           if i1>0 then system.Delete(ss1,1,i1+3);

            i1:=Pos('.',ss1);
            if i1>0 then system.Delete(ss1,1,i1);

            i1:=Pos(';',ss1);
            if i1>0 then system.Delete(ss1,i1,1);
           result:=trim(ss1);
         end;
   begin

     if U.Types.Count<1 then Exit;

     for I :=0 to U.Types.Count - 1 do
       begin

            itm:=TPasItem(U.Types.Items[i]);
            if itm=nil then Continue;
            //........................................................................................
            ss:=UpperCase(itm.Name);
            if Pos('_',ss)=1 then Continue;
            if Pos('TWM',ss)>0 then Continue;    // DO NOT Inmport all TWM**** types at Messages.pas
            //........................................................................................

            if CheckForBAD(VarFilesStore.BadTypes,itm.Name) then Continue;  //Check For Bad  Type

            it:=GetType_Type(itm);

           case it of
             //itClassOf: Find_Type_ClassOf(itm);
             itArray:Find_Type_Array(itm);
             itDynArray:Find_Type_DynArray(itm);
             itSetOf:          fsTypes.Add('  RegisterRTTITypeSX(TypeInfo('+itm.name+'));') ;
             itEvent:          fsTypes.Add('  RegisterRTTITypeSX(TypeInfo('+itm.name+'));') ;
             itSimpleRange:    fsTypes.Add('  RegisterRTTITypeSX(TypeInfo('+itm.name+'));') ;
             itEmul:           fsTypes.Add('  RegisterRTTITypeSX(TypeInfo('+itm.name+'));') ;

             itClassOf:        fsTypes.Add(' //itClassOf =============== Type ============= '+itm.name);
             itFunction:       fsTypes.Add(' //itFunction =============== Type ============= '+itm.name);
             itProcedure:      fsTypes.Add(' //itProcedure =============== Type ============= '+itm.name);
             itSetOfNoType:    fsTypes.Add(' //itSetOfNoType =============== Type ============= '+itm.name);
             itPointer:        fsTypes.Add(' //itPointer   =============== Type ============= '+itm.name);
             itPointerOf:      fsTypes.Add(' //itPointerOf =============== Type ============= '+itm.name);
             itRecord:         fsTypes.Add(' //itRecord    =============== Type ============= '+itm.name);
             itGUID:           fsTypes.Add(' //itGUID     =============== Type ============= '+itm.name);
           else

             ss:=_GetTypeAlias(itm.FullDeclaration);

             if Not (ss='') then
              if Not SameText(itm.name,ss) then
               fsTypes.Add('  RegisterTypeAliasSX('''+itm.name+''','''+ss+''');') ;
           end;

           //.............................
       end;

   end;


  procedure TTitanScriptDocGenerator.WriteUnit_Types;
    var ss:string;
   begin

     if fsTypes.Count=0 then exit;

     ss:='_RegTypes';
     WriteDirectline(' ');
     WriteDirectline('procedure '+ss+';');
     WriteDirectline(' Begin');
     WriteDirectline(fsTypes.Text);
     WriteDirectline(' End;');

     fRegFunctions.Add(ss+';');
   end;

  //==================================== Variables =======================================
  procedure TTitanScriptDocGenerator.FindUnit_Variables(U: TPasUnit);
    var i,z,l:integer;
        itm:TPasItem;
        ss:string;

       Function _FindVarType(const str:string):String;
         var i1,i2:integer;
             ss1:String;
        begin
           result:='';
           ss1:=str;

           i1:=Pos(':',ss1);
           if i1=0 then exit;
           System.Delete(ss1,1,i1);

           i1:=Pos('=',ss1);     //Delete Varaible Value (ss:integer=1)
           if i1>0 then
            begin
              i2:=System.Length(ss1);
              System.Delete(ss1,i1,i2-i1);
            end;

           i1:=Pos(';',ss1);
           if i1>0 then system.Delete(ss1,i1,1);
           result:=trim(ss1);

        end;

   begin
     if U.Variables.Count=0 then Exit;

     for I :=0 to U.Variables.Count - 1 do
       begin

         itm:=TPasItem(U.Variables.Items[i]);
         if itm=nil then Continue;
         if CheckForBAD(VarFilesStore.BadVars,itm.Name) then Continue;  //Check For Bad Var

         //.................................................
         ss:=UpperCase(itm.FullDeclaration);
         if Pos('_',ss)=1 then Continue;
         if Pos('FUNCTION',ss)>0 then Continue;
         if Pos('PROCEDURE',ss)>0 then Continue;
         //.................................................

         ss:=_FindVarType(itm.FullDeclaration);
         ss:=UpperCase(ss);
         fsVariables.Add('  RegisterVariableSX('''+itm.name+''','''+ss+''',@'+itm.name+');');

       end;
   end;

  procedure TTitanScriptDocGenerator.WriteUnit_Variables;
    var i,f,iStart,iEnd,itotal:integer;

        Procedure _GWrite(const ifrom,ito,id:integer);
          var z:integer;
              ss:string;
         begin
           ss:='_RegVariables'+IntToStr(id);
           WriteDirectline(' ');
           WriteDirectline('procedure '+ss+';');
           WriteDirectline(' Begin');
           for z := ifrom to ito-1 do WriteDirectline(fsVariables[z]);
           WriteDirectline(' End;');
           fRegFunctions.Add(ss+';');
         end;
   begin
     if fsVariables.Count=0 then Exit;

     if fsVariables.Count<MAXCONSTSINSECTION then
       begin
         _GWrite(0,fsVariables.Count,0);
       end else
       begin
         itotal:=fsVariables.Count;
         iStart:=0;
         iEnd:=MAXCONSTSINSECTION;
         i:=0;
         repeat
            _GWrite(iStart,iEnd,i);

            iStart:=iEnd;
            iEnd:=iStart+MAXCONSTSINSECTION;
            if iEnd>itotal then iEnd:=itotal;

            inc(i);
         until (iStart>itotal) or (iEnd-iStart<1) ;

       end;
   end;

  //================================== FuncsProcs =============================

  procedure TTitanScriptDocGenerator.FindUnit_FuncsProcs(U: TPasUnit);
    var i:integer;
        itm:TPasItem;
        ss:string;
   begin
     if U.FuncsProcs.Count=0 then Exit;

     for I :=0 to U.FuncsProcs.Count - 1 do
       begin

         itm:=TPasItem(U.FuncsProcs.Items[i]);
         if itm=nil then Continue;
         if CheckForBAD(VarFilesStore.BadFunProcs,itm.Name) then Continue;  //Check For Bad FuncsProcs

         ss:=UpperCase(itm.Name);
         if (Pos('_',ss)=1) then Continue;
         if SameText('REGISTER', ss) then Continue;
         if SameText('REGISTERCLASSE', ss) then Continue;
         if SameText('REGISTERCLASSES', ss) then Continue;
         if SameText('REGISTERCOMPONENT', ss) then Continue;
         if SameText('REGISTERCOMPONENTS', ss) then Continue;

         ss:=Fix_EmptyStrings(itm.FullDeclaration);
         ss:=Fix_LongStrings(ss);

         fsFuncsProcs.Add('  RegisterRoutineSX('''+ss+''',@'+itm.name+');');
       end;

   end;

  procedure TTitanScriptDocGenerator.WriteUnit_FuncsProcs;
    var i,f,iStart,iEnd,itotal:integer;

        Procedure _GWrite(const ifrom,ito,id:integer);
          var z:integer;
              ss:string;
         begin
           ss:='_RegFuncsProcs'+IntToStr(id);
           WriteDirectline(' ');
           WriteDirectline('procedure '+ss+';');
           WriteDirectline(' Begin');
           for z := ifrom to ito-1 do WriteDirectline(fsFuncsProcs[z]);
           WriteDirectline(' End;');
           fRegFunctions.Add(ss+';');
         end;
   begin
     if fsFuncsProcs.Count=0 then Exit;

     if fsFuncsProcs.Count<MAXCONSTSINSECTION then
       begin
         _GWrite(0,fsFuncsProcs.Count,0);
       end else
       begin
         itotal:=fsFuncsProcs.Count;
         iStart:=0;
         iEnd:=MAXCONSTSINSECTION;
         i:=0;
         repeat
            _GWrite(iStart,iEnd,i);

            iStart:=iEnd;
            iEnd:=iStart+MAXCONSTSINSECTION;
            if iEnd>itotal then iEnd:=itotal;

            inc(i);
         until (iStart>itotal) or (iEnd-iStart<1) ;

       end;
   end;

  //================================ClassOf===================================================
  procedure TTitanScriptDocGenerator.Find_Type_ClassOf(itm:TPasItem);
    var z,l:Integer;
        st:string;
   begin

     if itm=nil then Exit;

     st:=UpperCase(itm.FullDeclaration);
     z:=Pos('OF',st);
     if z=0 then exit;

     st:=itm.FullDeclaration;
     l:=System.Length(st);
     System.Delete(st,1,z+2);

     l:=System.Length(st);
     System.Delete(st,l,1);
     st:=Trim(st);

     if st='' then
        fsClassOf.Add('   // BADClassOf =============== ClassOf ============= '+itm.name) else
        fsClassOf.Add('   RegisterClassReferenceTypeSX('''+itm.name+''','''+st+''');');
   end;

  procedure TTitanScriptDocGenerator.WriteUnit_ClassOf;
    var ss:string;
   begin

     if fsClassOf.Count=0 then Exit;

     ss:='_RegClassOf';
     WriteDirectline(' ');
     WriteDirectline('procedure '+ss+';');
     WriteDirectline(' Begin');
     WriteDirectline(fsClassOf.Text);
     WriteDirectline(' End;');
   end;

  //=================================== Class ======================================
  procedure TTitanScriptDocGenerator.Find_CIO_Class(cl:TPasCio);
    var i:integer;
        tm:TPasMethod;
        tp:TPasProperty;
        ss,ss1:string;
        ooW,ooR:Boolean;
        sooW,sooR:string;
        iTypeStr:string;
        iProcName:string;
        iIndexFullDec:string;
        iIndexDec:string;
        hasProOrFunct:boolean;
        //.............................................................
        procedure _GetIndexType(const str:string);
         var z,l:integer;
        begin
           iTypeStr:=str;
           z:=Pos(':',iTypeStr);

            if z>0 then
             begin
               System.Delete(iTypeStr,1,z);
               l:=System.Length(iTypeStr);
               System.Delete(iTypeStr,l,1);
             end;

           if iTypeStr='' then iTypeStr:='Integer';
        end;

        procedure _GetIndexProc(const str:string);
         var z,l:integer;
        begin
           iProcName:=str;
           l:=System.Length(iProcName);
           System.Delete(iProcName,l,1);
        end;

        procedure _GetIndexFullDec(xp:TPasProperty);
          var x,x2,l:integer;
              ssx:string;
         begin
           iIndexFullDec:=xp.IndexDecl;
           System.Delete(iIndexFullDec,1,1);  //delete '['

           l:=System.Length(iIndexFullDec);
           System.Delete(iIndexFullDec,l,1);  //delete ']'

           //.....................................................
           // Insert and " " for constIndex error
           ssx:=UpperCase(iIndexFullDec);
           x2:=Pos('CONST',ssx);
           if x2>0 then
            begin
              x:=Pos('CONST ',ssx);
              if x<1 then Insert(' ',iIndexFullDec,x2+5);
            end;
         end;

         procedure _GetIndexDec(xp:TPasProperty); // ====
          var x,l,z1,z2:integer;
              ssx:string;
         begin
           iIndexDec:=xp.IndexDecl;
           ssx:=UpperCase(xp.IndexDecl);
           x:=Pos('CONST',ssx);
           if x>0 then System.Delete(iIndexDec,x,5); // ct9999

           z1:=Pos(':',iIndexDec);
           z2:=Pos(',',iIndexDec);
           if z1>z2 then
             begin
              l:=System.Length(iIndexDec);
              System.Delete(iIndexDec,z1,l-z1);
             end;

         end;

         Function _RemoveVitrual(const aStr:string):string;
           var z1,z2:integer;
               ssx:string;
          begin
            result:=aStr;
            if result='' then exit;
            ssx:=UpperCase(aStr);
            z1:=Pos('VIRTUAL',ssx);
            z2:=System.Length(ssx);
            if z1>0 then system.Delete(result,z1,z2-z1+1);
            result:=Trim(result);
          end;
       //...................................................................
   begin

     if cl=nil then Exit;

     ss:=UpperCase(cl.Name);
     if Pos('HELPER',ss)>0 then Exit;

     fsCIOs.Add('  G:=RegisterClassTypeSX('+cl.name+');');    //Write Class Import code

     hasProOrFunct:=false;
     //------------------------ Write Methods ---------------------------
     if cl.Methods.Count>0 then
       for I := 0 to cl.Methods.Count - 1 do
        begin
          tm:=TPasMethod(cl.Methods.Items[i]);
          if tm.Visibility<>viPublic then Continue;

          ss:=Fix_EmptyStrings(tm.FullDeclaration);
          ss:=Fix_LongStrings(ss);

          fsCIOs.Add('   RegisterMethodSX(G,'''+ss+''',@'+cl.name+'.'+tm.name+');');
          hasProOrFunct:=true;
        end;

     //--------------------- Class Write Properties ---------------------------------

      if cl.Properties.Count>0 then
       for I := 0 to cl.Properties.Count - 1 do
        begin
          tp:=TPasProperty(cl.Properties.Items[i]);
          if tp.Visibility<>viPublic then Continue;           //Import only Public Properies



          if not (tp.IndexDecl='') then
           begin  //--------------------- Index Properties --------------------------------------------

            _GetIndexProc(tp.Name);
            _GetIndexFullDec(tp);
            _GetIndexDec(tp);
            _GetIndexType(tp.IndexDecl);

            ss:=UpperCase(tp.FullDeclaration);
            ooW:=(Pos('WRITE',ss)>0);
            ooR:=(Pos('READ',ss)>0);

            if (ooW=false) and (ooR=false) then Continue;               //Properties With no read AND write NOT inport

            //... build link functions ....................
            if ooW or ooR then fsCIOsWappers.Add('');

            if ooR then
             begin
              fsCIOsWappers.Add('function '+cl.Name+'__Get'+iProcName+'('+iIndexFullDec+'):'+tp.Proptype+';');
              fsCIOsWappers.Add(' begin result:='+cl.Name+'(_Self).'+tp.Name+iIndexDec+'; end;');
             end;
            if ooW then
             begin
              fsCIOsWappers.Add('procedure '+cl.Name+'__Set'+iProcName+'('+iIndexFullDec+';Val:'+tp.Proptype+');');
              fsCIOsWappers.Add(' begin '+cl.Name+'(_Self).'+tp.Name+iIndexDec+':=val; end;');
             end;

            //... build reg functions ....................
            if ooR then
             begin
              ss:='Function __Get'+iProcName+'('+iIndexFullDec+'):'+tp.Proptype+';';
              fsCIOs.Add('    GoR:=RegisterMethodSXF(G,'''+ss+''',@'+cl.name+'__Get'+iProcName+');');
             end;
            if ooW then
             begin
              ss:='Procedure __Set'+iProcName+'('+iIndexFullDec+'; Val:'+tp.Proptype+');';
              fsCIOs.Add('    GoW:=RegisterMethodSXF(G,'''+ss+''',@'+cl.name+'__Set'+iProcName+');');
             end;

             ss:='';
             sooR:='Nil';
             sooW:='Nil';
             if ooR then begin  sooR:='GoR'; end;
             if ooW then begin  sooW:='GoW'; end;
             ss:='Property '+tp.Name+'['+iIndexFullDec+']:'+tp.Proptype+ss;

             if tp.NoDefault=false then  ss:=ss+'; Default;';

             fsCIOs.Add('   RegisterPropertySXi(G,'+sooR+','+sooW+','''+ss+''');');

             hasProOrFunct:=true;
           end else //-------------------- No Index Properties -----------------------------------------------
           begin

            ss:=UpperCase(tp.FullDeclaration);
            ooW:=(Pos('WRITE',ss)>0);
            ooR:=(Pos('READ',ss)>0);

           if (ooW=false) and (ooR=false) then Continue;               //Properties With no read AND write NOT inport


            //... build link functions ....................
            if ooW or ooR then fsCIOsWappers.Add('');

            if ooR then
             begin
              fsCIOsWappers.Add('function '+cl.Name+'_Get'+tp.Name+':'+tp.Proptype+';');
              fsCIOsWappers.Add(' begin result:='+cl.Name+'(_Self).'+tp.Name+'; end;');
             end;
            if ooW then
             begin
              fsCIOsWappers.Add('procedure '+cl.Name+'_Set'+tp.Name+'(Val:'+tp.Proptype+');');
              fsCIOsWappers.Add(' begin '+cl.Name+'(_Self).'+tp.Name+':=val; end;');
             end;

            //... build reg functions ....................
            if ooR then
             begin
              ss:='function _Get'+tp.Name+':'+tp.Proptype+';';
              fsCIOs.Add('    GoR:=RegisterMethodSXF(G,'''+ss+''',@'+cl.name+'_Get'+tp.Name+');');
             end;
            if ooW then
             begin
              ss:='procedure _Set'+tp.Name+'(Val:'+tp.Proptype+');';
              fsCIOs.Add('    GoW:=RegisterMethodSXF(G,'''+ss+''',@'+cl.name+'_Set'+tp.Name+');');
             end;

             //ss:='';
             sooR:='Nil';
             sooW:='Nil';
             if ooR then Begin sooR:='GoR'; end;
             if ooW then Begin sooW:='GoW';end;


            fsCIOs.Add('   RegisterPropertySX(G,'+sooR+','+sooW+','''+tp.Name+''','''+tp.Proptype+''');');

             hasProOrFunct:=true;
           end;
        end;


      if hasProOrFunct then fsCIOs.Add(' ');
   end;

  //======================================= RECORDS ===========================================
  procedure TTitanScriptDocGenerator.Find_CIO_RECORD(cl:TPasCio; const IsPacked:Boolean=false);
    var i:Integer;
        itm:TPasItem;
        ss,sIDStr,LastsIDStr:string;
        isBAD:Boolean;

        procedure _GetFieldTypeIDStr;
          var xs:string;
              i1,i2:integer;
         begin

           if itm=nil then exit;
           sIDStr:='';
           xs:=itm.FullDeclaration;

           i1:=Pos(':',xs);
           i2:=system.Length(xs);
           if (i1>0) and (i2>0) and (i2>i1) then
            begin
              sIDStr:=system.Copy(xs,i1+1,i2-i1);

              i1:=Pos(';',sIDStr);
              if i1>0 then system.Delete(sIDStr,i1,1);
              sIDStr:=trim(sIDStr);
            end;

         end;
   begin

     if cl=nil then Exit;

    // if Not SameText(cl.Name,'TRect') then exit;   // S.O.S. PROGRAMMING TEST


     ss:=UpperCase(cl.Name);
     if Pos('TWM',ss)>0 then Exit;    // DO NOT Inmport all TWM.... Records at Messages.pas

     //....... Check For Array Field of Record ..............
     isBAD:=false;
     if cl.Fields.Count>0 then
      for I := 0 to cl.Fields.Count - 1 do
       begin
         itm:=TPasItem(cl.Fields.Items[i]);
         ss:=UpperCase(itm.FullDeclaration);
         if (Pos('ARRAY',ss)>0) and (Pos('OF',ss)>0) then
           begin
             isBAD:=true;
           end;
       end;

     if isBAD=true then
       begin
         fsCIOs.Add('  //BAD Record =============== Has Array Field ============ '+cl.name);
         Exit;
       end;


       fsCIOs.Add('  G:=RegisterRecordTypeSX('''+cl.name+''');');

       LastsIDStr:='';
      //................ Fields ...............
      if cl.Fields.Count>0 then
      begin
       for I := 0 to cl.Fields.Count - 1 do
        begin
          itm:=TPasItem(cl.Fields.Items[i]);

          if itm.FullDeclaration='' then Continue;

          _GetFieldTypeIDStr;

         // if LastsIDStr='' then
         // fsCIOs.Add('   RegisterRecordFieldSX(G,'''+itm.name+''','''+sIDStr+''',0);') else
          fsCIOs.Add('   RegisterRecordFieldSX(G,'''+itm.name+''','''+sIDStr+''');');

         LastsIDStr:=sIDStr;
       end;
       fsCIOs.Add('');
      end;

   end;

  //========================================= Main CIOs function ===============================================
  procedure TTitanScriptDocGenerator.FindUnit_CIOs(U: TPasUnit);
    var i:integer;
        itm:TPasCio;
   begin
     if U.CIOs.Count<1 then Exit;

     for I :=0 to U.CIOs.Count - 1 do
       begin

         itm:=TPasCio(U.CIOs.Items[i]);
         if itm=nil then Continue;
         if CheckForBAD(VarFilesStore.BadCIOS,itm.Name) then Continue;  //Check For Bad CIO

         case itm.MyType of
               CIO_CLASS:Find_CIO_Class(itm);
               CIO_RECORD:Find_CIO_RECORD(itm,false);
               CIO_PACKEDRECORD:Find_CIO_RECORD(itm,true);
               CIO_OBJECT:Find_CIO_Class(itm);

               //..........................................
              // CIO_SPINTERFACE:  fsCIOs.Add(' // CIO_SPINTERFACE  =============== CIO =============== '+itm.Name);
               CIO_INTERFACE:    fsCIOs.Add(' // CIO_INTERFACE    =============== CIO =============== '+itm.Name);


         end;

       end;

   end;
  {
  procedure TTitanScriptDocGenerator.WriteUnit_CIOs;
    var ss:string;
   begin
     if fsCIOs.Count=0 then exit;

     ss:='_RegCIOs';
     WriteDirectline(' ');
     WriteDirectline('procedure '+ss+';');
     WriteDirectline(' Begin');
     WriteDirectline(fsCIOs.Text);
     WriteDirectline(' End;');

     fRegFunctions.Add(ss+';');

     if fsClassOf.Count>0 then  fRegFunctions.Add('_RegClassOf;');
   end;
   }

   procedure TTitanScriptDocGenerator.WriteUnit_CIOs;
     var i,f,iStart,iEnd,itotal:integer;
       //......................................
       Procedure _GWrite(const ifrom,ito,id:integer);
          var z:integer;
              ss:string;
         begin
           ss:='_RegCIOs'+IntToStr(id);
           WriteDirectline(' ');
           WriteDirectline('procedure '+ss+';');
           WriteDirectline(' Begin');
           for z := ifrom to ito-1 do WriteDirectline(fsCIOs[z]);
           WriteDirectline(' End;');
           fRegFunctions.Add(ss+';');
         end;
       //......................................
   begin
     if fsCIOs.Count=0 then exit;

     if fsCIOs.Count<MAXCIOSDECLERATION then
       begin
         _GWrite(0,fsCIOs.Count,0);
       end else
       begin
         itotal:=fsCIOs.Count;
         iStart:=0;
         iEnd:=MAXCIOSDECLERATION;
         i:=0;
         repeat
            _GWrite(iStart,iEnd,i);

            iStart:=iEnd;
            iEnd:=iStart+MAXCIOSDECLERATION;
            if iEnd>itotal then iEnd:=itotal;

            inc(i);
         until (iStart>itotal) or (iEnd-iStart<1) ;

       end;
     if fsClassOf.Count>0 then  fRegFunctions.Add('_RegClassOf;');
   end;

  //...............................................................................
  procedure TTitanScriptDocGenerator.WriteUnitRegProcStart(U: TPasUnit);
    var i:Integer;
   begin
     WriteDirectline(' ');
     WriteDirectline('procedure RegisterTotalFor_'+U.Name+';');
     WriteDirectline(' Begin');
    // WriteDirectline('  H:=RegisterNamespace(0, '''+U.Name+''');');
     WriteDirectline('  H:=-1;');

     if fRegFunctions.Count=0 then exit;
     for I :=0 to fRegFunctions.Count - 1 do
      WriteDirectline('  '+fRegFunctions.Strings[i]);
   end;

  procedure TTitanScriptDocGenerator.WriteUnitInitialization(U: TPasUnit);
   begin
     WriteDirectline(' End;');
     WriteDirectline('Initialization');
     WriteDirectline('  RegisterTotalFor_'+U.Name+';');
     WriteDirectline('End.');
   end;

  procedure TTitanScriptDocGenerator.WriteUnit(const HL: integer; const U: TPasUnit);
   begin
     fRegFunctions.Clear;
     fsConstants.Clear;
     fsTypes.Clear;
     fsFuncsProcs.Clear;
     fsVariables.Clear;
     fsCIOs.Clear;
     fsCIOsWappers.Clear;
     fRegFunctions.Clear;
     fsClassOf.Clear;

     if U=nil then exit;
     CreateStream(U.Name+GetFilePrefix+GetFileExtension);
       WriteUnitHeader(U);
       WriteUnitUses(U);

       FindUnit_Constants(U);
       FindUnit_Types(U);
       FindUnit_CIOs(U);
       FindUnit_FuncsProcs(U);
       FindUnit_Variables(U);
       //.......................................
       WriteDirectline(fsCIOsWappers.Text);
       WriteUnit_Constants;
       WriteUnit_Types;
       WriteUnit_CIOs;
       WriteUnit_ClassOf;
       WriteUnit_FuncsProcs;
       WriteUnit_Variables;

       WriteUnitRegProcStart(U);
       WriteUnitInitialization(U);
     CloseStream;
   end;

  procedure TTitanScriptDocGenerator.WriteUnits(const HL: integer);
    var  i: Integer;
  begin
    if ObjectVectorIsNilOrEmpty(Units) then Exit;

    WriteExtraUnit;
    WriteTotalRegistrationUnit;

    for i:=0 to Units.Count - 1 do
         begin
          WriteUnit(HL,Units.UnitAt[i]);
         end;
  end;

  procedure TTitanScriptDocGenerator.WriteDocumentation;
  begin
   // inherited;

   WriteUnits(0);
  end;

  end.

