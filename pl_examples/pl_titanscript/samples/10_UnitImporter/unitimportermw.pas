unit UnitImportermw;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  lazfileutils,
  Dialogs, ComCtrls, Menus, ActnList, ToolWin, ImgList,ModuleForProgram,
  PasDoc_TitanScripterFileStore, ExtCtrls, Buttons, StdCtrls, FileUtil,UnitBuild,
  fConsole,PasDoc_Items,fStringsForm,FDlgPreview;

type

  { TMainWin }

  TMainWin = class(TForm)
    MainMenu1: TMainMenu;
    ActionList1: TActionList;
    ToolBar1: TToolBar;
    ActionClose: TAction;
    File1: TMenuItem;
    ActionClose1: TMenuItem;
    N1: TMenuItem;
    ToolButton1: TToolButton;
    ActionUnitAdd: TAction;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    TreeView1: TTreeView;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ActionProjectOpen: TAction;
    ActionProjectSave: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ActionProjectNew: TAction;
    ToolButton7: TToolButton;
    cProjectName: TEdit;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    cOutDirectory: TEdit;
    SpeedButton1: TSpeedButton;
    Build: TAction;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ActionDirAdd: TAction;
    ToolButton10: TToolButton;
    ActionUnitDelete: TAction;
    ActionDirDelete: TAction;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    vPROTECTED: TCheckBox;
    vPRIVATE: TCheckBox;
    vPUBLIC: TCheckBox;
    vPUBLISHED: TCheckBox;
    vAUTOMATED: TCheckBox;
    cOutputGraphVizUses: TCheckBox;
    cOutputGraphVizClassHierarchy: TCheckBox;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    TabSheet2: TTabSheet;
    StaticText6: TStaticText;
    cTitle: TEdit;
    cNumericFilenames: TCheckBox;
    cWriteUsesClause: TCheckBox;
    SpeedButton2: TSpeedButton;
    NewProject1: TMenuItem;
    OpenProject1: TMenuItem;
    SaveProject1: TMenuItem;
    N2: TMenuItem;
    AddUnit1: TMenuItem;
    DeleteselectedUnit1: TMenuItem;
    AddDirectory1: TMenuItem;
    DeleteselectedDirectory1: TMenuItem;
    N3: TMenuItem;
    Build1: TMenuItem;
    ActionOpenStringsDlg: TAction;
    OpenStringsDialog1: TMenuItem;
    ToolButton13: TToolButton;
    ActionViewFile: TAction;
    ToolButton15: TToolButton;
    ActionClearTempDir: TAction;
    Actions1: TMenuItem;
    ClearTempDirectory1: TMenuItem;
    ActionMoveChm: TAction;
    ClearTempDirectoryandMovehelpfileChm1: TMenuItem;
    ActionViewHelpFile: TAction;
    ViewHelpFile1: TMenuItem;
    cFullLink: TCheckBox;
    TabSheet3: TTabSheet;
    cCommentMarkers: TMemo;
    Panel1: TPanel;
    cStarStyleOnly: TCheckBox;
    cMarkerOptional: TCheckBox;
    StaticText8: TStaticText;
    StaticText9: TStaticText;
    StaticText10: TStaticText;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    ToolButton17: TToolButton;
    StatusBar1: TStatusBar;
    ActionCopyAllFilesToGLSceneTemp: TAction;
    N4: TMenuItem;
    CopyFilestoGLScenetemp1: TMenuItem;
    vIMPLICIT: TCheckBox;
    Panel3: TPanel;
    ToolButton19: TToolButton;
    cLinkLook: TComboBox;
    Memo1: TMemo;
    cAutoAbstract: TCheckBox;
    Memo2: TMemo;
    Label1: TLabel;
    cImplicitVisibility: TComboBox;
    Memo3: TMemo;
    Label2: TLabel;
    TabSheet4: TTabSheet;
    cAutoLink: TCheckBox;
    Memo4: TMemo;
    vSTRICTPRIVATE: TCheckBox;
    vSTRICTPROTECTED: TCheckBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    vvNonrecordfields: TCheckBox;
    vvVariables: TCheckBox;
    vvCIOs: TCheckBox;
    vvConstants: TCheckBox;
    vvTypes: TCheckBox;
    vvFuncsProcs: TCheckBox;
    vvRecordfields: TCheckBox;
    vvUsesclauses: TCheckBox;
    Memo5: TMemo;
    vvMethods: TCheckBox;
    vvProperties: TCheckBox;
    PanelForOptions: TPanel;
    PanelTop: TPanel;
    SpeedButton6: TSpeedButton;
    TabSheet5: TTabSheet;
    cUseTipueSearch: TCheckBox;
    Panel4: TPanel;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    cHeader: TMemo;
    cFooter: TMemo;
    cCSS: TMemo;
    cNoGeneratorInfo: TCheckBox;
    ActionProjectSaveAs: TAction;
    Saveas1: TMenuItem;
    TabSheet9: TTabSheet;
    cDirectives: TMemo;
    TabSheet10: TTabSheet;
    PageControl3: TPageControl;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    cBadConsts: TMemo;
    cBadTypes: TMemo;
    cBadVars: TMemo;
    cBadCIOs: TMemo;
    TabSheet15: TTabSheet;
    cBadFunProcs: TMemo;
    TabSheet16: TTabSheet;
    cExtraUnit: TMemo;
    ToolBar2: TToolBar;
    ToolButton20: TToolButton;
    cExtraUnitName: TEdit;
    ToolButton21: TToolButton;
    procedure ActionCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionUnitAddExecute(Sender: TObject);
    procedure ActionProjectOpenExecute(Sender: TObject);
    procedure ActionProjectSaveExecute(Sender: TObject);
    procedure ActionProjectNewExecute(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cProjectNameChange(Sender: TObject);
    procedure BuildExecute(Sender: TObject);
    procedure ActionDirAddExecute(Sender: TObject);
    procedure ActionUnitDeleteExecute(Sender: TObject);
    procedure ActionDirDeleteExecute(Sender: TObject);
    procedure cHelpFileClick(Sender: TObject);
    procedure cTitleChange(Sender: TObject);
    procedure ActionOpenStringsDlgExecute(Sender: TObject);
    procedure ActionViewFileExecute(Sender: TObject);
    procedure TreeView1DblClick(Sender: TObject);
    procedure ActionClearTempDirExecute(Sender: TObject);
    procedure ActionCopyAllFilesToGLSceneTempExecute(Sender: TObject);
    procedure ActionProjectSaveAsExecute(Sender: TObject);
  private
    fProjectNode:TTreeNode;
    fFilesNode:TTreeNode;
    FDirNode:TTreeNode;
    fIsUpdating:boolean;
    Procedure BuildTreeView;
    Procedure DataToForm;
    Procedure FormToData;
  public
    Procedure CheckActions;
  end;

var
  MainWin: TMainWin;


implementation

uses TypInfo,PasDoc_Gen,PasDoc_Types,PasDoc_SortSettings;

{$R *.lfm}

//-------------------------------------------------- 9999
const
// CodeTyphon: Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = 'C:\codetyphon\CodeOcean\pl_TitanScript\samples\ImportProjects\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '/usr/local/codetyphon/pl_TitanScript/samples/ImportProjects/';
{$ENDIF}
//--------------------------------------------------

Procedure TMainWin.CheckActions;
  var b:Boolean;
 begin

      b:=( (VarFilesStore.ProjectBuildType=pbtTitanScript) );

      b:=Not b;
      
      ActionViewHelpFile.Enabled:=b;
      ActionMoveChm.Enabled:=b;

      TabSheet3.TabVisible:=b;
      TabSheet5.TabVisible:=b;

      TabSheet10.TabVisible:=not b;
      TabSheet16.TabVisible:=not b;
 end;

Procedure TMainWin.BuildTreeView;
 var i:integer;
     SubNode:TTreeNode;
 begin
  TreeView1.Items.Clear;

  TreeView1.Items.BeginUpdate;

  fProjectNode:=TreeView1.Items.AddChild(nil,VarFilesStore.ProjectName);
  fProjectNode.ImageIndex:=5;
  fProjectNode.SelectedIndex:=5;


  fFilesNode:=TreeView1.Items.AddChild(fProjectNode,'Units Files ('+inttostr(VarFilesStore.FileStore.Count)+')');
   fFilesNode.ImageIndex:=15;
   fFilesNode.SelectedIndex:=15;

  FDirNode:=TreeView1.Items.AddChild(fProjectNode,'Directories ('+inttostr(VarFilesStore.Directories.Count)+')');
   FDirNode.ImageIndex:=15;
   FDirNode.SelectedIndex:=15;

  for i:=0 to VarFilesStore.FileStore.Count-1 do
    begin
     SubNode:=TreeView1.Items.AddChild(fFilesNode,VarFilesStore.FileStore[i]);  // 8888
     SubNode.ImageIndex:=7;
     SubNode.SelectedIndex:=7;
    end;

   for i:=0 to VarFilesStore.Directories.Count-1 do
    begin
     SubNode:=TreeView1.Items.AddChild(FDirNode,VarFilesStore.Directories[i]);
     SubNode.ImageIndex:=16;
     SubNode.SelectedIndex:=16;
    end;

  TreeView1.Items.EndUpdate;

  fProjectNode.Expanded:=true;
 end;

Procedure TMainWin.DataToForm;
 begin
  fIsUpdating:=true;

   cProjectName.Text:=VarFilesStore.ProjectName;
   cOutDirectory.Text:=VarFilesStore.OutDirectory;

   //=============== ClassMembers ========
   vPRIVATE.Checked:=  (viPRIVATE in VarFilesStore.ClassMembers);
   vSTRICTPRIVATE.Checked:=  (viSTRICTPRIVATE in VarFilesStore.ClassMembers);
   vPROTECTED.Checked:=(viPROTECTED in VarFilesStore.ClassMembers);
   vSTRICTPROTECTED.Checked:=(viSTRICTPROTECTED in VarFilesStore.ClassMembers);
   vPUBLIC.Checked:=   (viPUBLIC in VarFilesStore.ClassMembers);
   vPUBLISHED.Checked:=(viPUBLISHED in VarFilesStore.ClassMembers);
   vAUTOMATED.Checked:=(viAUTOMATED in VarFilesStore.ClassMembers);
   vIMPLICIT.Checked:=(viIMPLICIT  in VarFilesStore.ClassMembers);
   //============== SortSettings =========
   vvCIOs.Checked:=  (ssCIOs in VarFilesStore.SortSettings);
   vvConstants.Checked:=  (ssConstants in VarFilesStore.SortSettings);
   vvFuncsProcs.Checked:=(ssFuncsProcs in VarFilesStore.SortSettings);
   vvTypes.Checked:=(ssTypes in VarFilesStore.SortSettings);
   vvVariables.Checked:=   (ssVariables in VarFilesStore.SortSettings);
   vvUsesclauses.Checked:=(ssUsesClauses in VarFilesStore.SortSettings);
   vvRecordfields.Checked:=(ssRecordFields in VarFilesStore.SortSettings);
   vvNonrecordfields.Checked:=(ssNonRecordFields  in VarFilesStore.SortSettings);
   vvMethods.Checked:=(ssMethods in VarFilesStore.SortSettings);
   vvProperties.Checked:=(ssProperties  in VarFilesStore.SortSettings);

   cOutputGraphVizUses.Checked:=VarFilesStore.OutputGraphVizUses;
   cOutputGraphVizClassHierarchy.Checked:=VarFilesStore.OutputGraphVizClassHierarchy;

   cTitle.Text:=VarFilesStore.Title;

   cNumericFilenames.Checked:=VarFilesStore.NumericFilenames;
   cWriteUsesClause.Checked:=VarFilesStore.WriteUsesClause;
   cFullLink.Checked:=VarFilesStore.FullLink;

   cCommentMarkers.Lines.Text:=VarFilesStore.CommentMarkers;
  // cStarStyleOnly.Checked:=VarFilesStore.StarStyleOnly;
   cMarkerOptional.Checked:=VarFilesStore.MarkerOptional;
   cLinkLook.ItemIndex:= Ord(VarFilesStore.LinkLook);
   cAutoAbstract.Checked:=VarFilesStore.AutoAbstract;
   cImplicitVisibility.ItemIndex:= Ord(VarFilesStore.ImplicitVisibility);
   cAutoLink.Checked:=VarFilesStore.AutoLink;
  // cProjectBuildType.ItemIndex:= Ord(VarFilesStore.ProjectBuildType);
   cUseTipueSearch.Checked:=VarFilesStore.UseTipueSearch;

   cHeader.Lines.Text:=VarFilesStore.Header;
   cFooter.Lines.Text:=VarFilesStore.Footer;
   cCSS.Lines.Text:=VarFilesStore.CSS;
   cNoGeneratorInfo.Checked:=VarFilesStore.NoGeneratorInfo;

   cDirectives.Lines.Assign(VarFilesStore.Directives);

   cBadConsts.Lines.Assign(VarFilesStore.BadConsts);
   cBadTypes.Lines.Assign(VarFilesStore.BadTypes);
   cBadVars.Lines.Assign(VarFilesStore.BadVars);
   cBadCIOs.Lines.Assign(VarFilesStore.BadCIOs);
   cBadFunProcs.Lines.Assign(VarFilesStore.BadFunProcs);

   cExtraUnit.Lines.Assign(VarFilesStore.ExtraUnit);
   cExtraUnitName.Text:=VarFilesStore.ExtraUnitName;

  fIsUpdating:=false;
  
  CheckActions;
 end;

Procedure TMainWin.FormToData;
 begin
 if fIsUpdating then exit;
  fProjectNode.Text:=cProjectName.Text;
  VarFilesStore.ProjectName:=cProjectName.Text;
  VarFilesStore.OutDirectory:=cOutDirectory.Text;

  //==========================================
  VarFilesStore.ClassMembers:=[];
   if vPRIVATE.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viPRIVATE];
   if vSTRICTPRIVATE.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viSTRICTPRIVATE];
   if vPROTECTED.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viPROTECTED];
   if vSTRICTPROTECTED.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viSTRICTPROTECTED];
   if vPUBLIC.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viPUBLIC];
   if vPUBLISHED.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viPUBLISHED];
   if vAUTOMATED.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viAUTOMATED];
   if vIMPLICIT.Checked then VarFilesStore.ClassMembers:=VarFilesStore.ClassMembers+[viIMPLICIT];
   //==========================================
  VarFilesStore.SortSettings:=[];
   if vvCIOs.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssCIOs];
   if vvConstants.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssConstants];
   if vvFuncsProcs.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssFuncsProcs];
   if vvTypes.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssTypes];
   if vvVariables.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssVariables];
   if vvUsesclauses.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssUsesClauses];
   if vvRecordfields.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssRecordFields];
   if vvNonrecordfields.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssNonRecordFields];
   if vvMethods.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssMethods];
   if vvProperties.Checked then VarFilesStore.SortSettings:=VarFilesStore.SortSettings+[ssProperties];


   VarFilesStore.OutputGraphVizUses:=cOutputGraphVizUses.Checked;
   VarFilesStore.OutputGraphVizClassHierarchy:=cOutputGraphVizClassHierarchy.Checked;
   VarFilesStore.NumericFilenames:=cNumericFilenames.Checked;


  VarFilesStore.Title:=cTitle.Text;
  VarFilesStore.WriteUsesClause:=cWriteUsesClause.Checked;
  VarFilesStore.FullLink:=cFullLink.Checked;
  VarFilesStore.CommentMarkers:=cCommentMarkers.Lines.Text;
 // VarFilesStore.StarStyleOnly:=cStarStyleOnly.Checked;
  VarFilesStore.MarkerOptional:=cMarkerOptional.Checked;
  VarFilesStore.LinkLook:=TLinkLook(cLinkLook.ItemIndex);
  VarFilesStore.AutoAbstract:=cAutoAbstract.Checked;
  VarFilesStore.ImplicitVisibility:=TImplicitVisibility(cImplicitVisibility.ItemIndex);
  VarFilesStore.AutoLink:=cAutoLink.Checked;
  VarFilesStore.ProjectBuildType:=pbtTitanScript;
  VarFilesStore.UseTipueSearch:=cUseTipueSearch.Checked;

  VarFilesStore.Header:=cHeader.Lines.Text;
  VarFilesStore.Footer:=cFooter.Lines.Text;
  VarFilesStore.CSS:=cCSS.Lines.Text;
  VarFilesStore.NoGeneratorInfo:=cNoGeneratorInfo.Checked;
  VarFilesStore.Directives.Assign(cDirectives.Lines);

  VarFilesStore.BadConsts.Assign(cBadConsts.Lines);
  VarFilesStore.BadTypes.Assign(cBadTypes.Lines);
  VarFilesStore.BadVars.Assign(cBadVars.Lines);
  VarFilesStore.BadCIOs.Assign(cBadCIOs.Lines);
  VarFilesStore.BadFunProcs.Assign(cBadFunProcs.Lines);

  VarFilesStore.ExtraUnit.Assign(cExtraUnit.Lines);
  VarFilesStore.ExtraUnitName:=cExtraUnitName.Text;

  CheckActions;  
 end;

procedure TMainWin.FormCreate(Sender: TObject);
var i:integer;
     ptd: PTypeData;
 begin

 //-------- TLinkLook ------------------------
  ptd := GetTypeData(typeinfo(TLinkLook));
   for i:= ptd^.MinValue to ptd^.MaxValue do
     cLinkLook.Items.Add( GetEnumName(typeinfo(TLinkLook),i));
 //-------- TImplicitVisibility ------------------------
  ptd := GetTypeData(typeinfo(TImplicitVisibility));
   for i:= ptd^.MinValue to ptd^.MaxValue do
     cImplicitVisibility.Items.Add( GetEnumName(typeinfo(TImplicitVisibility),i));

 VarFilesStore:=TImpStore.Create(self);
end;

procedure TMainWin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 VarFilesStore.Free;
end;

procedure TMainWin.FormShow(Sender: TObject);
begin
  TheProjectFile:='';
  StatusBar1.SimpleText:='No Project File';
  VarFilesStore.ProjectBuildType:=pbtTitanScript;
  CheckActions;
  BuildTreeView;
  DataToForm;
end;

//==================== Actions ===================================
procedure TMainWin.ActionCloseExecute(Sender: TObject);
begin
 Close;
end;

procedure TMainWin.ActionUnitAddExecute(Sender: TObject);
 var i:integer;
begin

if ProgramModule.OpenDialogForPascal.Execute then // 8888
   begin

    for i:=0 to ProgramModule.OpenDialogForPascal.Files.Count-1 do
       VarFilesStore.FileStore.Add(ProgramModule.OpenDialogForPascal.Files[i]);

    BuildTreeView;
    fFilesNode.Expanded:=true;
   end;
end;

procedure TMainWin.ActionProjectOpenExecute(Sender: TObject);
begin

  if ProgramModule.OpenDialogForProject.FileName='' then
     ProgramModule.OpenDialogForProject.InitialDir:=SetDirSeparators(pathMedia);  // 9999

  ProgramModule.OpenDialogForProject.Filename:=TheProjectFile;

  if ProgramModule.OpenDialogForProject.FileName='' then
       ProgramModule.OpenDialogForProject.InitialDir:=pathMedia ;

  if ProgramModule.OpenDialogForProject.Execute then
   begin
    VarFilesStore.Clear;
    VarFilesStore.LoadFromFile(ProgramModule.OpenDialogForProject.Filename);
    DataToForm;
    BuildTreeView;
    fFilesNode.Expanded:=true;
    TheProjectFile:=ProgramModule.OpenDialogForProject.Filename;
    StatusBar1.SimpleText:=TheProjectFile;
   end;

end;

procedure TMainWin.ActionProjectSaveAsExecute(Sender: TObject);
begin
 FormToData;

  ProgramModule.SaveDialogForProject.Filename:=TheProjectFile;
  if ProgramModule.SaveDialogForProject.FileName='' then
        ProgramModule.SaveDialogForProject.InitialDir:=pathMedia;

  if ProgramModule.SaveDialogForProject.Execute then
   begin
    VarFilesStore.SaveToFile(ProgramModule.SaveDialogForProject.Filename);
    TheProjectFile:=ProgramModule.SaveDialogForProject.Filename;
    StatusBar1.SimpleText:=TheProjectFile;
   end;   

end;

procedure TMainWin.ActionProjectSaveExecute(Sender: TObject);
begin
 FormToData;

 if TheProjectFile='' then
   begin
      ActionProjectSaveAs.Execute;
      exit;
   end;

 VarFilesStore.SaveToFile(TheProjectFile);
end;

procedure TMainWin.ActionProjectNewExecute(Sender: TObject);
begin
 VarFilesStore.Clear;
 DataToForm;
 BuildTreeView;
end;

procedure TMainWin.SpeedButton1Click(Sender: TObject);
begin
with ProgramModule.PBFolderDialog1 do
 begin
  ProgramModule.PBFolderDialog1.FileName:=VarFilesStore.OutDirectory;
  if Execute then
   begin
    VarFilesStore.OutDirectory:=FileName;
    datatoform;
   end;
 end;
end;

procedure TMainWin.cProjectNameChange(Sender: TObject);
begin
  FormtoData;
end;

procedure TMainWin.BuildExecute(Sender: TObject);  
begin
 FormtoData;
 if VarFilesStore.FileStore.Count=0 then
   begin
    ShowMessage('Error:Project is empty Sir ???.');
    exit;
   end;
  Console.Execute;

 xxBuildAll(Console.Memo1.Lines);

end;

procedure TMainWin.ActionDirAddExecute(Sender: TObject);
begin
 with ProgramModule.PBFolderDialog1 do
 begin
  if Execute then
   begin
    VarFilesStore.Directories.Add(filename);
    BuildTreeView;
   end;
 end;
end;

procedure TMainWin.ActionUnitDeleteExecute(Sender: TObject);
 var i:integer;
begin
  if treeview1.Selected=nil then exit;
  if treeview1.Selected.Parent<>fFilesNode then exit;

  i:=treeview1.Selected.Index;
  VarFilesStore.FileStore.Delete(treeview1.Selected.Index);
  BuildTreeView;

  i:=i-1;
  if i<=0 then i:=0;

  if VarFilesStore.FileStore.Count=0 then
    treeview1.Selected:=fFilesNode else
    treeview1.Selected:=fFilesNode.Items[i];
end;

procedure TMainWin.ActionDirDeleteExecute(Sender: TObject);
 var i:integer;
begin
  if treeview1.Selected=nil then exit;
  if treeview1.Selected.Parent<>FDirNode then exit;

  i:=treeview1.Selected.Index;
  VarFilesStore.Directories.Delete(treeview1.Selected.Index);
  BuildTreeView;

  i:=i-1;
  if i<=0 then i:=0;

  if i=0 then
    treeview1.Selected:=FDirNode else
    treeview1.Selected:=FDirNode.Items[i];
end;

procedure TMainWin.cHelpFileClick(Sender: TObject);
begin
 formtodata;
end;

procedure TMainWin.cTitleChange(Sender: TObject);
begin
  VarFilesStore.Title:=cTitle.Text;
  datatoform;
end;

procedure TMainWin.ActionOpenStringsDlgExecute(Sender: TObject);  // 8888
begin
  //VarFilesStore.FileStore.WriteToStrings(StringsForm.Memo1.Lines);
  StringsForm.Memo1.Lines. Assign(VarFilesStore.FileStore);
  StringsForm.ShowModal;

  case StringsForm.ModalResult of
    mrAll: begin
             VarFilesStore.FileStore.Assign(StringsForm.Memo1.Lines);
             BuildTreeView;
           end;
  end;
  
end;

procedure TMainWin.ActionViewFileExecute(Sender: TObject);
 var ss1,ss2:string;
begin

  if TreeView1.Selected=nil then exit;
   if TreeView1.Selected.Parent=fFilesNode then
     begin
       ss1:=VarFilesStore.FileStore[TreeView1.Selected.Index];
       ss2:='';

       if (VarFilesStore.ProjectBuildType=pbtTitanScript) then
        begin
         ss2:=ExtractfileName(ss1);
         ss2:=ChangeFileExt(ss2,'');
         ss2:=VarFilesStore.OutDirectory+'\'+ss2+'_Imp'+'.pas';
         DlgPreview.Execute(ss1,ss2);
        end else
        begin
         DlgPreview.Execute(ss1,ss2);
        end;
     end;

end;

procedure TMainWin.TreeView1DblClick(Sender: TObject);
begin
  ActionViewFile.Execute;
end;

procedure TMainWin.ActionClearTempDirExecute(Sender: TObject);
begin
 if (VarFilesStore.OutDirectory='') or
     Sametext(VarFilesStore.OutDirectory,extractfilepath(application.ExeName)) then
    begin
      showMessage('Error:No temp Directory Sir ???');
      exit;
    end;

  xxClearDirectory(VarFilesStore.OutDirectory);
  //RemoveDirUTF8(VarFilesStore.OutDirectory);
  //CreateDirUTF8(VarFilesStore.OutDirectory);

  ShowMessage('************** All OK  Sir (Clear) *******************');
end;


procedure TMainWin.ActionCopyAllFilesToGLSceneTempExecute(Sender: TObject);
 var i:integer;
     s:string;
begin

 s:='D:\Projects\PascalHelpMaker\FixedGLSceneAPIUnits\';

   for i:=0 to VarFilesStore.FileStore.Count-1 do
    begin
      if pos('GLScene\',VarFilesStore.FileStore[i])>0 then
       if FileExistsUTF8(VarFilesStore.FileStore[i]) { *Converted from FileExists*  } then
        begin
         DlgPreview.CodeFix.Strings.LoadFromFile(VarFilesStore.FileStore[i]);
         DlgPreview.CodeFix.FixStrings;
         DlgPreview.CodeFix.Strings.SaveToFile(s+ExtractFilename(VarFilesStore.FileStore[i]));
         VarFilesStore.FileStore[i]:=s+ExtractFilename(VarFilesStore.FileStore[i]);
       end;

    end;
 CopyFile(pchar('D:\Projects\SternasVCL\GLScene\Source\GLScene.inc'),
          pchar(s+'GLScene.inc'),
          true);

 BuildTreeView;
 TheProjectFile:='Cosmos4DTemp.PHM';
 treeview1.Selected:=fFilesNode;
end;

end.
