{***************************************************
 Titan Scripter Import unit
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com
 This file is part of CodeTyphon Studio
***********************************************************************}

unit PasDoc_TitanScripterFileStore;

interface

uses Classes, SysUtils, Forms,
  PasDoc_Items, PasDoc_Gen, PasDoc_Types, PasDoc_SortSettings;

const
  MAXCONSTSINSECTION = 254;
  MAXCALLSINSECTION = 5000;
  MAXPSTRINGLength = 200;
  MAXCIOSDECLERATION = 254;

type


  TItemType = (itNone, itEmul, itSetOf, itSetOfNoType, itPointer, itPointerOf, itClassOf,
    itArray, itDynArray, itEvent, itRecord, itFunction, itProcedure, itGUID,
    itRecordDef, itSimpleRange);

  TProjectBuildType = (pbtChmHelp, pbtHtmlHelp, pbtHtml, pbtLatex, pbtTitanScript);

  TImpStore = class(TComponent)
  private
    FFileStore: TStrings;
    FOutDirectory: string;
    FProjectName: string;
    FDirectories: TStrings;
    FDirectives: TStrings;
    fProjectBuildType: TProjectBuildType;
    FNoGeneratorInfo: boolean;
    FClassMembers: TVisibilities;
    FGraphVizClasses: boolean;
    FGraphVizUses: boolean;
    fTitle: string;
    FNumericFilenames: boolean;
    FWriteUses: boolean;
    FChmInstallDir: string;
    FFullLink: boolean;
    FCommentMarkers: string;
    fStarOnly: boolean;
    fMarkerOptional: boolean;
    FLinkLook: TLinkLook;
    fAutoAbstract: boolean;
    FImplicitVisibility: TImplicitVisibility;
    FAutoLink: boolean;
    FShowVisibilities: TVisibilities;
    FSortSettings: TSortSettings;
    FUseTipueSearch: boolean;
    FHeader: string;
    FFooter: string;
    FCSS: string;
    //.........Bad strings ..............
    FBadConsts: TStrings;
    FBadTypes: TStrings;
    FBadVars: TStrings;
    FBadCIOS: TStrings;
    FBadFunProcs: TStrings;
    //....................
    fExtraUnitName: string;
    fExtraUnit: TStrings;

  protected
    procedure SetDirectories(val: TStrings);
    procedure SetDirectives(val: TStrings);
    procedure SetBadConsts(val: TStrings);
    procedure SetBadTypes(val: TStrings);
    procedure SetBadVars(val: TStrings);
    procedure SetBadCIOS(val: TStrings);
    procedure SetBadFunProcs(val: TStrings);
    procedure SetExtraUnit(val: TStrings);
    procedure SetTITLE(const val: string);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    procedure SaveToStream(aStream: TStream);
    procedure LoadFromStream(aStream: TStream);
    procedure SaveToFile(const fileName: string);
    procedure LoadFromFile(const fileName: string);
  published
    property FileStore: TStrings read fFileStore write FFileStore;
    property OutDirectory: string read fOutDirectory write FOutDirectory;
    property ProjectName: string read fProjectName write FProjectName;
    property Title: string read FTitle write SetTITLE;
    property Directories: TStrings read FDirectories write SetDirectories;
    property Directives: TStrings read FDirectives write SetDirectives;
    property ProjectBuildType: TProjectBuildType read FProjectBuildType write FProjectBuildType;
    property NoGeneratorInfo: boolean read FNoGeneratorInfo write FNoGeneratorInfo;
    property ClassMembers: TVisibilities read FClassMembers write FClassMembers;
    property OutputGraphVizUses: boolean read FGraphVizUses write FGraphVizUses;
    property OutputGraphVizClassHierarchy: boolean read FGraphVizClasses write FGraphVizClasses;
    property WriteUsesClause: boolean read FWriteUses write FWriteUses;
    //....
    property ChmInstallDir: string read FChmInstallDir write FChmInstallDir;
    property FullLink: boolean read FFullLink write FFullLink;
    property CommentMarkers: string read FCommentMarkers write FCommentMarkers;
    property StarOnly: boolean read FStarOnly write FStarOnly;
    property MarkerOptional: boolean read FMarkerOptional write FMarkerOptional;
    property LinkLook: TLinkLook read FLinkLook write FLinkLook;
    property AutoAbstract: boolean read FAutoAbstract write FAutoAbstract;
    property ImplicitVisibility: TImplicitVisibility read FImplicitVisibility write FImplicitVisibility;
    property AutoLink: boolean read FAutoLink write FAutoLink;
    property ShowVisibilities: TVisibilities read FShowVisibilities write FShowVisibilities;
    property SortSettings: TSortSettings read FSortSettings write FSortSettings;
    //For html generators
    property NumericFilenames: boolean read FNumericFilenames write FNumericFilenames;
    property UseTipueSearch: boolean read FUseTipueSearch write FUseTipueSearch;
    property Header: string read FHeader write FHeader;
    property Footer: string read FFooter write FFooter;
    property CSS: string read FCSS write FCSS;

    // For TitanScripter2
    property BadConsts: TStrings read FBadConsts write SetBadConsts;
    property BadTypes: TStrings read FBadTypes write SetBadTypes;
    property BadVars: TStrings read FBadVars write SetBadVars;
    property BadCIOS: TStrings read FBadCIOS write SetBadCIOS;
    property BadFunProcs: TStrings read FBadFunProcs write SetBadFunProcs;
    property ExtraUnitName: string read FExtraUnitName write FExtraUnitName;
    property ExtraUnit: TStrings read FExtraUnit write SetExtraUnit;
  end;


var
  VarFilesStore: TImpStore;
  TheProjectFile: string;

implementation

//---------------------- TImpStore -------------------------
constructor TImpStore.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fFileStore := TStringList.Create;
  FDirectories := TStringList.Create;
  FDirectives := TStringList.Create;
  FBadConsts := TStringList.Create;
  FBadTypes := TStringList.Create;
  FBadVars := TStringList.Create;
  FBadCIOS := TStringList.Create;
  FBadFunProcs := TStringList.Create;
  fExtraUnit := TStringList.Create;

  Clear;
end;

destructor TImpStore.Destroy;
begin
  FBadConsts.Free;
  FBadTypes.Free;
  FBadVars.Free;
  FBadCIOS.Free;
  FBadFunProcs.Free;
  fFileStore.Free;
  FDirectories.Free;
  FDirectives.Free;
  fExtraUnit.Free;
  inherited Destroy;
end;

procedure TImpStore.Clear;
begin
  fFileStore.Clear;
  FDirectories.Clear;
  fDirectives.Clear;
  FBadConsts.Clear;
  FBadTypes.Clear;
  FBadVars.Clear;
  FBadCIOS.Clear;
  FBadFunProcs.Clear;
  fExtraUnit.Clear;

  fExtraUnitName := '';
  FProjectName := 'NoName';
  fTitle := 'No Name';
  OutDirectory := ExtractFilePath(application.ExeName);
  FClassMembers := [viPublic, viPublished, viAutomated];
  FGraphVizClasses := True;
  FGraphVizUses := True;
  FNumericFilenames := False;
  FWriteUses := True;
  FChmInstallDir := '';
  FFullLink := True;
  FCommentMarkers := '';
  fStarOnly := False;
  fMarkerOptional := False;
  FLinkLook := llDefault;
  FAutoAbstract := True;
  FImplicitVisibility := ivPublic;
  FAutoLink := False;
  FShowVisibilities := [viPublic, viPublished, viAutomated];
  FSortSettings := AllSortSettings;
  fProjectBuildType := pbtChmHelp;
  fUseTipueSearch := False;
  FHeader := '';
  FFooter := '';
  FCSS := '';
end;

procedure TImpStore.SetTITLE(const val: string);
begin
  fTITLE := StringReplace(val, ' ', '', [rfReplaceAll]);
end;

procedure TImpStore.SetDirectories(val: TStrings);
begin
  if val = nil then
  begin
    FDirectories.Clear;
    exit;
  end;
  FDirectories.Assign(val);
end;

procedure TImpStore.SetDirectives(val: TStrings);
begin
  if val = nil then
  begin
    FDirectives.Clear;
    exit;
  end;
  FDirectives.Assign(val);
end;

procedure TImpStore.SetBadConsts(val: TStrings);
begin
  if val = nil then
  begin
    FBadConsts.Clear;
    exit;
  end;
  FBadConsts.Assign(val);
end;

procedure TImpStore.SetBadTypes(val: TStrings);
begin
  if val = nil then
  begin
    FBadTypes.Clear;
    exit;
  end;
  FBadTypes.Assign(val);
end;

procedure TImpStore.SetBadVars(val: TStrings);
begin
  if val = nil then
  begin
    FBadVars.Clear;
    exit;
  end;
  FBadVars.Assign(val);
end;

procedure TImpStore.SetBadCIOS(val: TStrings);
begin
  if val = nil then
  begin
    FBadCIOS.Clear;
    exit;
  end;
  FBadCIOS.Assign(val);
end;

procedure TImpStore.SetBadFunProcs(val: TStrings);
begin
  if val = nil then
  begin
    FBadFunProcs.Clear;
    exit;
  end;
  FBadFunProcs.Assign(val);
end;

procedure TImpStore.SetExtraUnit(val: TStrings);
begin
  if val = nil then
  begin
    FExtraUnit.Clear;
    exit;
  end;
  FExtraUnit.Assign(val);
end;

procedure TImpStore.SaveToStream(aStream: TStream);
begin
  astream.WriteComponent(self);
end;

procedure TImpStore.LoadFromStream(aStream: TStream);
begin
  astream.ReadComponent(self);
end;

procedure TImpStore.SaveToFile(const fileName: string);
var
  fs: TStream;
begin
  fs := TFileStream.Create(fileName, fmCreate);
  try
    SaveToStream(fs);
  finally
    fs.Free;
  end;
end;

procedure TImpStore.LoadFromFile(const fileName: string);
var
  fs: TStream;
begin

  fs := TFileStream.Create(fileName, fmOpenRead + fmShareDenyWrite);
  try
    LoadFromStream(fs);
  finally
    fs.Free;
  end;
end;

end.

