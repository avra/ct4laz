unit UnitBuild;

interface
uses
  LCLIntf, LCLType, LMessages,Classes,SysUtils,forms, dialogs, FileUtil ,
  lazfileutils,
  PasDoc_Base,
  PasDoc_Languages,
  PasDoc_HierarchyTree,
  PasDoc_Gen,
  PasDoc_GenHtml,
  PasDoc_GenHtmlHelp,
  PasDoc_GenLatex,
  PasDoc_TitanScripterGen,
  PasDoc_Items,
  PasDoc_SortSettings,
  PasDoc_OptionParser,
  PasDoc_Types,
  PasDoc_Utils,
  PasDoc_TitanScripterFileStore ;

var
  GPasDoc: TPasDoc;
  GOptionParser: TOptionParser;
  GOption_Verbosity: TIntegerOption;
  GOption_Define: TStringOptionList;
  GOption_Help: TBoolOption;
  GOption_IncludePaths: TPathListOption;
  GOption_Descriptions,
  GOption_ConditionalFile,
  GOption_SourceList,
  GOption_AbbrevFiles: TStringOptionList;
  GOption_ContentFile,
  GOption_Footer,
  GOption_Header,
  GOption_Name,
  GOption_Title,
  GOption_OutputPath,
  GOption_Language,
  GOption_ASPELL,
  GOption_IgnoreWords: TStringOption;
  GOption_StarOnly,
  GOption_Generator,
  GOption_NumericFilenames,
  GOption_WriteUsesList,
  GOption_WriteGVUses,
  GOption_WriteGVClasses: TBoolOption;
  GOption_VisibleMembers: TSetOption;
  GOption_CommentMarker: TStringOptionList;
  GOption_MarkerOptional: TBoolOption;
  GOption_CacheDir: TStringOption;
  GOption_FullLink: TBoolOption;

  //..............

Procedure xxBuildAll(OutString:TStrings);
Procedure xxClearDirectory(Const adir:string; liveChc:boolean=true);

var Vstring:Tstrings;

implementation

Procedure xWriteLn(const astring:string='');
 begin
   if Vstring=nil then exit;
   Vstring.Add(astring);
 end;


Procedure xxClearDirectory(Const adir:string; liveChc:boolean=true);
 var ss:tstrings;
     sr: TSearchRec;
     i,FileAttrs:integer;
 begin
 if DirectoryExistsUTF8(adir) =false then exit;
 try
   ss:=Tstringlist.Create;
   FileAttrs := faAnyFile;
    if FindFirstUTF8(adir+'*.*',FileAttrs,sr) =0 then
    begin
      repeat
       if (SameText(sr.Name,'.')=false) and
          (SameText(sr.Name,'..')=false) then
            if liveChc then
             begin
              if (SameText(sr.Name,VarFilesStore.ProjectName+'.chm')=false) and
                 (SameText(sr.Name,VarFilesStore.ProjectName+'.log')=false) then
                 ss.Add(adir+sr.Name);

             end else
             begin
                ss.Add(adir+sr.Name);
             end;

      until FindNextUTF8(sr)  <> 0;
      FindCloseUTF8(sr);

      for i:=0 to ss.Count-1 do
       if FileExistsUTF8(ss.Strings[i])  then
        begin
         DeleteFileUTF8(ss.Strings[i]);
        end;
     // showMessage('Detete total: '+inttostr(ss.Count-1)+' Files');
    end;
 finally
   ss.Free;
 end;
end;

//.................................................

procedure CreateOptions; 
var
  l: TLanguageID;
begin
  GOptionParser := TOptionParser.Create;

  GOption_Help := TBoolOption.Create('?', 'help');
  GOption_Help.Explanation := 'show this help';
  GOptionParser.AddOption(GOption_Help);

  GOption_Verbosity := TIntegerOption.Create('v', 'verbosity');
  GOption_Verbosity.Value := DEFAULT_VERBOSITY_LEVEL;
  GOption_Verbosity.Explanation := 'set log verbosity (0-5) ['+IntToStr(DEFAULT_VERBOSITY_LEVEL)+']';
  GOptionParser.AddOption(GOption_Verbosity);

  GOption_Define := TStringOptionList.Create('D', 'define');
  GOption_Define.Explanation := 'define conditional';
  GOptionParser.AddOption(GOption_Define);

  GOption_Descriptions := TStringOptionList.Create('R', 'description');
  GOption_Descriptions.Explanation := 'read description from this file';
  GOptionParser.AddOption(GOption_Descriptions);

  GOption_ConditionalFile := TStringOptionList.Create('d', 'conditionals');
  GOption_ConditionalFile.Explanation := 'read conditionals from this file';
  GOptionParser.AddOption(GOption_ConditionalFile);


  GOption_IncludePaths := TPathListOption.Create('I', 'include');
  GOption_IncludePaths.Explanation := 'includes search path';
  GOptionParser.AddOption(GOption_IncludePaths);

  GOption_SourceList := TStringOptionList.Create('S', 'source');
  GOption_SourceList.Explanation := 'read source filenames from file';
  GOptionParser.AddOption(GOption_SourceList);

  GOption_ContentFile := TStringOption.Create('C', 'content');
  GOption_ContentFile.Explanation := 'Read Contents for HtmlHelp from file';
  GOptionParser.AddOption(GOption_ContentFile);

  GOption_Footer := TStringOption.Create('F', 'footer');
  GOption_Footer.Explanation := 'include file as footer';
  GOptionParser.AddOption(GOption_Footer);

  GOption_Header := TStringOption.Create('H', 'header');
  GOption_Header.Explanation := 'include file as header';
  GOptionParser.AddOption(GOption_Header);

  GOption_Name := TStringOption.Create('N', 'name');
  GOption_Name.Explanation := 'Name for documentation';
  GOptionParser.AddOption(GOption_Name);

  GOption_Title := TStringOption.Create('T', 'title');
  GOption_Title.Explanation := 'Documentation title';
  GOptionParser.AddOption(GOption_Title);


  GOption_OutputPath := TStringOption.Create('E', 'output');
  GOption_OutputPath.Explanation := 'output path';
  GOptionParser.AddOption(GOption_OutputPath);

  GOption_Generator := TBoolOption.Create('X', 'exclude-generator');
  GOption_Generator.Explanation := 'exclude generator information';
  GOptionParser.AddOption(GOption_Generator);

  GOption_Language := TStringOption.Create('L', 'language');
  GOption_Language.Explanation := 'Output language. Valid languages are: ' + LineEnding;
  for l := Low(l) to High(l) do
    GOption_Language.Explanation := GOption_Language.Explanation + '  ' +
      LanguageDescriptor(l)^.Syntax + ': ' + LanguageDescriptor(l)^.Name + LineEnding;
  GOptionParser.AddOption(GOption_Language);


  GOption_StarOnly := TBoolOption.Create(#0, 'staronly');
  GOption_StarOnly.Explanation := 'Parse only {**, (*** and //** style comments';
  GOptionParser.AddOption(GOption_StarOnly);


  GOption_CommentMarker := TStringOptionList.Create(#0, 'marker');
  GOption_CommentMarker.Explanation := 'Parse only {<marker>, (*<marker> and //<marker> comments. Overrides the staronly option, which is a shortcut for ''--marker=**''';
  GOptionParser.AddOption(GOption_CommentMarker);


  GOption_NumericFilenames := TBoolOption.Create(#0, 'numericfilenames');
  GOption_NumericFilenames.Explanation := 'Causes the html generator to create numeric filenames';
  GOptionParser.AddOption(GOption_NumericFilenames);

  GOption_VisibleMembers := TSetOption.Create('M','visible-members');
  GOption_VisibleMembers.Explanation := 'Include / Exclude class Members by visiblity';
  GOption_VisibleMembers.PossibleValues := 'private,protected,public,published,automated';
  GOption_VisibleMembers.Values := 'protected,public,published,automated';
  GOptionParser.AddOption(GOption_VisibleMembers);

  GOption_WriteUsesList := TBoolOption.Create(#0, 'write-uses-list');
  GOption_WriteUsesList.Explanation := 'put uses list into output';
  GOptionParser.AddOption(GOption_WriteUsesList);

  GOption_WriteGVUses := TBoolOption.Create(#0, 'graphviz-uses');
  GOption_WriteGVUses.Explanation := 'write a GVUses.gviz file that can be used for the `dot` program from GraphViz to generate a unit dependency graph';
  GOptionParser.AddOption(GOption_WriteGVUses);

  GOption_WriteGVClasses := TBoolOption.Create(#0, 'graphviz-classes');
  GOption_WriteGVClasses.Explanation := 'write a GVClasses.gviz file that can be used for the `dot` program from GraphViz to generate a class hierarchy graph';
  GOptionParser.AddOption(GOption_WriteGVClasses);

  GOption_AbbrevFiles := TStringOptionList.Create(#0, 'abbreviations');
  GOption_AbbrevFiles.Explanation := 'abbreviation file, format is "[name]  value", value is trimmed, lines that do not start with ''['' (or whitespace before that) are ignored';
  GOptionParser.AddOption(GOption_AbbrevFiles);

  GOption_ASPELL := TStringOption.Create(#0, 'aspell');
  GOption_ASPELL.Explanation := 'enable aspell, giving language as parameter, currently only done in HTML output';
  GOptionParser.AddOption(GOption_ASPELL);

  GOption_IgnoreWords := TStringOption.Create(#0, 'ignore-words');
  GOption_IgnoreWords.Explanation := 'When spell-checking, ignore the words in that file. The file should contain one word on every line, no comments allowed';
  GOptionParser.AddOption(GOption_IgnoreWords);
end;

procedure PrintUsage;
begin
  xWriteLn('Usage: ' + ExtractFileName(ParamStr(0)) + ' [options] [files]');
  xWriteLn('Valid options are: ');
  GOptionParser.WriteExplanations;
end;

type
  EInvalidCommandLine = class(Exception);

  function GetLanguageFromStr(S: string): TLanguageID;
  begin
    if not LanguageFromStr(S, Result) then
      raise EInvalidCommandLine.CreateFmt('Unknown language code "%s"', [S]);
  end;


function ParseCommandLine: boolean;
var
  i: Integer;
  lng: TLanguageID;
begin
  Result := false;
  GOptionParser.ParseOptions;
  if GOption_Help.TurnedOn then
   begin
    PrintUsage;
    exit;
  end;


  Case VarFilesStore.ProjectBuildType of 

    pbtTitanScript : begin
                      GPasDoc.Generator:=TTitanScriptDocGenerator.Create(GPasDoc);
                    end;

  End;

  //GPasDoc.HtmlHelpContentsFileName := GOption_ContentFile.Value;     
  GPasDoc.Directives.Assign(GOption_Define.Values);

  for i := 0 to GOption_ConditionalFile.Values.Count - 1 do
   begin
     GPasDoc.Directives.LoadFromTextFileAdd(GOption_ConditionalFile.Values[i]);
   end;

  for i:=0 to VarFilesStore.Directives.Count-1 do      //***********Load Directives************
    GPasDoc.Directives.Add(VarFilesStore.Directives[i]);

  GOption_OutputPath.Value:=VarFilesStore.OutDirectory; //***********************
  GPasDoc.Generator.DestinationDirectory := GOption_OutputPath.Value;


  for i:=0 to VarFilesStore.FileStore.Count-1 do  //******************Load Files*****
    GOption_IncludePaths.Values.Add(ExtractFilePath(VarFilesStore.FileStore[i])); // getdir

  for i:=0 to VarFilesStore.Directories.Count-1 do //********************Load Directories***
    GOption_IncludePaths.Values.Add(VarFilesStore.Directories[i]);


  GPasDoc.IncludeDirectories.Assign(GOption_IncludePaths.Values);


  if GOption_Language.WasSpecified then
    GPasDoc.Generator.Language := GetLanguageFromStr(GOption_Language.Value);
   {
  GOption_Language.Value := lowercase(GOption_Language.Value);
  for lng := Low(LANGUAGE_ARRAY) to High(LANGUAGE_ARRAY) do begin
    if LowerCase(LANGUAGE_ARRAY[lng].Syntax) = GOption_Language.Value then
      begin
      GPasDoc.Generator.Language := lng;
      break;
    end;
  end;
         }

  GOption_Name.Value:=VarFilesStore.ProjectName; //***********************
  GPasDoc.ProjectName := GOption_Name.Value;

  GPasDoc.DescriptionFileNames.Assign(GOption_Descriptions.Values);

  for i := 0 to GOption_SourceList.Values.Count - 1 do begin
    GPasDoc.AddSourceFileNamesFromFile(GOption_SourceList.Values[i],false);
  end;

  GPasDoc.Title := VarFilesStore.Title;
  GPasDoc.Verbosity := GOption_Verbosity.Value;

  GOption_CommentMarker := TStringOptionList.Create(#0, 'marker');
  GOption_CommentMarker.Explanation := 'Parse only {<marker>, (*<marker> and //<marker> comments. Overrides the staronly option, which is a shortcut for ''--marker=**''';
  GOptionParser.AddOption(GOption_CommentMarker);

  GOption_MarkerOptional := TBoolOption.Create(#0, 'marker-optional');
  GOption_MarkerOptional.Explanation := 'do not require the markers given in --marker but remove them from the comment if they exist.';
  GOptionParser.AddOption(GOption_MarkerOptional);

 //========================================================================
  for i:=0 to VarFilesStore.FileStore.Count-1 do
    GOptionParser.LeftList.Add(VarFilesStore.FileStore[i]);

  GPasDoc.AddSourceFileNames(GOptionParser.LeftList);

  //GPasDoc.ClassMembers :=VarFilesStore.ClassMembers;

  GPasDoc.Generator.OutputGraphVizUses:=VarFilesStore.OutputGraphVizUses;
  GPasDoc.Generator.OutputGraphVizClassHierarchy:=VarFilesStore.OutputGraphVizClassHierarchy;

  for i := 0 to GOption_AbbrevFiles.Values.Count-1 do begin
    GPasDoc.Generator.ParseAbbreviationsFile(GOption_AbbrevFiles.Values[i]);
  end;
  GPasDoc.Generator.CheckSpelling := GOption_ASPELL.WasSpecified;
  GPasDoc.Generator.AspellLanguage := GOption_ASPELL.Value;

  //=============================================================
  if GPasDoc.Generator is TGenericHTMLDocGenerator then   //Only Html
    begin
      if VarFilesStore.Header<>'' then
         TGenericHTMLDocGenerator(GPasDoc.Generator).Header:=VarFilesStore.Header;
      if VarFilesStore.Footer<>'' then
         TGenericHTMLDocGenerator(GPasDoc.Generator).Footer:=VarFilesStore.Footer;
      if VarFilesStore.CSS<>'' then
          TGenericHTMLDocGenerator(GPasDoc.Generator).CSS:=VarFilesStore.CSS;
    end;

  GPasDoc.StarOnly:=VarFilesStore.StarOnly;
  GPasDoc.CommentMarkers.Text:=VarFilesStore.CommentMarkers;
  GPasDoc.MarkerOptional:=VarFilesStore.MarkerOptional;
  GPasDoc.Generator.LinkLook:=VarFilesStore.LinkLook;
  GPasDoc.ImplicitVisibility:=VarFilesStore.ImplicitVisibility;
  GPasDoc.Generator.AutoAbstract:=VarFilesStore.AutoAbstract;
  GPasDoc.AutoLink :=VarFilesStore.AutoLink;
  GPasDoc.ShowVisibilities:=VarFilesStore.ShowVisibilities;
  GPasDoc.SortSettings := VarFilesStore.SortSettings;
  GPasDoc.Generator.ExcludeGenerator:=VarFilesStore.NoGeneratorInfo;


  //===============================================================
  Result := True;
end;

//----------------------------------------------------------------------------
//Call back for mmssegs
procedure WriteWarning(const Dummy: Pointer; const MessageType: TPasDocMessageType;
                       const AMessage: String; const AVerbosity: Cardinal);
begin
  case MessageType of
    pmtInformation: xWriteLn('Info['+ Floattostr(AVerbosity)+ ']:    '+ AMessage);
    pmtWarning: xWriteLn('Warning['+ Floattostr(AVerbosity)+ ']: '+ AMessage);
    pmtError: xWriteLn('Error['+ Floattostr(AVerbosity)+ ']:   '+ AMessage);
  else
    xWriteLn(AMessage);
  end;
  
  Application.ProcessMessages;
end;

Procedure xxBuildAll(OutString:TStrings);
begin
  if VarFilesStore=nil then exit;
  
  xxClearDirectory(VarFilesStore.OutDirectory+'\',false);
  Vstring:=OutString;
  Vstring.Clear;
  xWriteLn('****************************************************************************');
  xWriteLn('This Beta ... for CodeTyphon');//PASDOC_FULL_INFO);
  xWriteLn('Documentation generator for Pascal source');
  xWriteLn;
  xWriteLn('This is free software; see the source for copying conditions.  There is NO');
  xWriteLn('warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  xWriteLn;
  CreateOptions;
  try
    GPasDoc := TPasDoc.Create(nil);
    try
      GPasDoc.OnWarning := TPasDocMessageEvent(MakeMethod(nil,@WriteWarning));
      GPasDoc.OnMessage := TPasDocMessageEvent(MakeMethod(nil,@WriteWarning));

      if ParseCommandLine then
      begin
        GPasDoc.Execute;
        xWriteLn('******************************* END Building ******************************************');
      end;
    finally
      GPasDoc.Free;
    end;
  except
    on e: EPasDoc do
      with e do
        xWriteLn('Fatal Error: '+ Message);
  end;
  GOptionParser.Free;    
end;


end.
