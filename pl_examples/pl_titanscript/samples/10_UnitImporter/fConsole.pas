unit fConsole;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ModuleForProgram, StdCtrls, ComCtrls, ToolWin;

type
  TConsole = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    Memo1: TMemo;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure Execute;
  end;

var
  Console: TConsole;

implementation

{$R *.lfm}

Procedure TConsole.Execute;
 begin
  Memo1.Clear;
  if not visible then show;
 end;

procedure TConsole.ToolButton1Click(Sender: TObject);
begin
 close;
end;

end.
