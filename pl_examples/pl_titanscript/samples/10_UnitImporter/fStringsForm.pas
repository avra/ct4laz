unit fStringsForm;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TStringsForm = class(TForm)
    Memo1: TMemo;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StringsForm: TStringsForm;

implementation

{$R *.lfm}

procedure TStringsForm.Button1Click(Sender: TObject);
begin
 memo1.SelectAll;
 Memo1.CopyToClipboard;
end;

procedure TStringsForm.Button2Click(Sender: TObject);
begin
 memo1.Clear;
 memo1.PasteFromClipboard;
end;

procedure TStringsForm.Button3Click(Sender: TObject);
begin
 memo1.Clear;
end;

end.
