program UnitImporter;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  UnitImportermw,
  fConsole,
  FDlgPreview,
  fStringsForm,
  ModuleForProgram,
  UnitBuild,
  UnitCodeFix,
  PasDoc_TitanScripterGen;

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Pascal Help Maker';
  Application.CreateForm(TMainWin, MainWin);
  Application.CreateForm(TConsole, Console);
  Application.CreateForm(TDlgPreview, DlgPreview);
  Application.CreateForm(TStringsForm, StringsForm);
  Application.CreateForm(TProgramModule, ProgramModule);
  Application.Run;
end.
