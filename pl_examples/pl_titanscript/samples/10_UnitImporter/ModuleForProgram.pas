unit ModuleForProgram;

{$MODE Delphi}

interface

uses
  SysUtils, Classes, ImgList, Controls, Dialogs,
  PasDoc_Base,PasDoc_Gen, PasDoc_GenHtml;

type
  TProgramModule = class(TDataModule)
    GenImageList: TImageList;
    OpenDialogForPascal: TOpenDialog;
    OpenDialogForProject: TOpenDialog;
    SaveDialogForProject: TSaveDialog;
    PBFolderDialog1: TSelectDirectoryDialog;
  private
  public
  end;

var
  ProgramModule: TProgramModule;

implementation

{$R *.lfm}
uses
  LCLIntf, LCLType, LMessages,
  Registry,
  PasDoc_Tokenizer,
  PasDoc_HierarchyTree;

end.
