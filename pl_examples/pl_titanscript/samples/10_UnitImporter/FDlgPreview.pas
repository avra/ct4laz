unit FDlgPreview;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  lazfileutils,
  Dialogs, SynEdit, SynMemo, ComCtrls, SynEditHighlighter,ModuleForProgram,
  SynHighlighterPas, ToolWin, ActnList, ImgList, ExtCtrls,
  SynEditMiscClasses, UnitCodeFix, StdCtrls, FileUtil;

type

  { TDlgPreview }

  TDlgPreview = class(TForm)
    ActionList1: TActionList;
    ActionClose: TAction;
    ActionFileSaveAsImp: TAction;
    SynPasSyn2: TSynPasSyn;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    Memo1: TSynMemo;
    SaveDialogForPascal: TSaveDialog;
    ActionFileSave: TAction;
    ActionFixCode: TAction;
    Splitter1: TSplitter;
    Memo2: TSynMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    ToolBar2: TToolBar;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    CheckBox2: TCheckBox;
    ToolBar3: TToolBar;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CheckBox1: TCheckBox;
    procedure ActionFileSaveAsImpExecute(Sender: TObject);
    procedure ActionCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ActionFileSaveExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionFixCodeExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    fInfilename1:string;
    fInfilename2:string;
    fCodeFix:TCodeFix;
    Procedure SetInfilename1(const Val:string);
    Procedure SetInfilename2(const Val:string);
  public
    Procedure Execute(const filename1,filename2 :string);
    Property Infilename1:string read fInfilename1 write SetInfilename1;
    Property Infilename2:string read fInfilename2 write SetInfilename2;
    Property CodeFix:TCodeFix read fCodeFix;
  end;

var
  DlgPreview: TDlgPreview;

implementation

{$R *.lfm}

Procedure TDlgPreview.Execute(const filename1,filename2 :string);
 begin
 if CheckBox1.Checked then
  if Memo1.Lines.Text<>'' then
   if Memo1.Modified=true then ActionFileSave.Execute;

  Infilename1:=filename1;
  Infilename2:=filename2;

 // fCodeFix.Editor:=Memo1;
  if visible=false then show;
 end;

Procedure TDlgPreview.SetInfilename1(const Val:string);
 begin
   Memo1.Lines.Clear;
   if Val='' then exit;


  if FileExistsUTF8(val) { *Converted from FileExists*  } then
   begin
    Memo1.Lines.LoadFromFile(Val);
    fInfilename1:=val;
    caption:=val;
   end;
 end;

Procedure TDlgPreview.SetInfilename2(const Val:string);
  var b:boolean;
 begin
  b:=false;
  Memo2.Lines.Clear;
  if Val='' then exit;

  if FileExistsUTF8(val) { *Converted from FileExists*  }  then
   begin
    Memo2.Lines.LoadFromFile(Val);
    fInfilename2:=val;
    //caption:=val;
    b:=true;
   end;

  Panel2.Visible:=b;
  Splitter1.Visible:=b; 
 end;

procedure TDlgPreview.ActionFileSaveAsImpExecute(Sender: TObject);
begin
 SaveDialogForPascal.FileName:=fInfilename1;
   if SaveDialogForPascal.Execute then
   begin
    Memo1.Lines.SaveToFile(SaveDialogForPascal.FileName);
    fInfilename1:=SaveDialogForPascal.FileName;
    Memo1.Modified:=false;
   end;
end;

procedure TDlgPreview.ActionCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TDlgPreview.FormShow(Sender: TObject);
begin
  SetFocusedControl(Memo1)
end;

procedure TDlgPreview.ActionFileSaveExecute(Sender: TObject);
begin
Memo1.Lines.SaveToFile(fInfilename1);
Memo1.Modified:=false;
end;

procedure TDlgPreview.FormCreate(Sender: TObject);
begin
 fCodeFix:=TCodeFix.Create;
end;

procedure TDlgPreview.FormDestroy(Sender: TObject);
begin
 fCodeFix.Free;
end;

procedure TDlgPreview.ActionFixCodeExecute(Sender: TObject);
begin
 fCodeFix.FixEditor;
end;

procedure TDlgPreview.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if CheckBox1.Checked then
   if Memo1.Lines.Text<>'' then
    if Memo1.Modified=true then ActionFileSave.Execute;
end;

end.
