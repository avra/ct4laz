unit UnitCodeFix;

{$MODE Delphi}

interface
uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SynEdit, SynExportHTML, {SynExportRTF,}
  ImgList, {SynCompletionProposal,} SynEditExport, SynEditHighlighter,
  {SynEditPrint,} SynHighlighterPas,SynEditTypes,StrUtils;

type
  TCodeFix = class(TPersistent)
  private
  protected
   FEditor:TSynEdit;
   FStrings:TstringList;
  public
    constructor Create;
    destructor Destroy; override;

    Procedure xFindReplaceCase(const inText,OutText:string);
    Procedure xClearLineCase(const inText:string);
    Procedure xDeleteLineCase(const inText:string);
    Procedure FixEditor;
    Procedure FixStrings;
  published
    Property Editor:TSynEdit read FEditor write FEditor;
    Property Strings:TstringList read FStrings;
 end;

//function BufferCoord(AX, AY: Integer): TBufferCoord;

implementation
  {
function BufferCoord(AX, AY: Integer): TBufferCoord;
begin
  Result.Char:=AX;
  result.Line:=AY;;
end;
    }
Procedure TCodeFix.FixStrings;
 begin
  if FStrings.Text='' then exit;

   xDeleteLineCase('<b>History');
   xDeleteLineCase('Historique');
   xDeleteLineCase('This unit is part of the GLScene');
   xDeleteLineCase('<li>');
   //===========================================
   xClearLineCase('Private Declarations');
   xClearLineCase('Protected Declarations');
   xClearLineCase('Public Declarations');
   xClearLineCase('Published Declarations');
   //============================================
   xFindReplaceCase('{:','{ ');
   xFindReplaceCase('//:','// ');
   xFindReplaceCase('<p>','   ');
   xFindReplaceCase('<ul>','   ');
   xFindReplaceCase('</ul> ','   ');
   xFindReplaceCase('<br>','    ');
   xFindReplaceCase('<b>','   ');
   xFindReplaceCase('</ul>','   ');
   xFindReplaceCase('</ul></font>','            ');
   xFindReplaceCase('</font>','       ');
   xFindReplaceCase('http://glscene.org','https://www.pilotlogic.com');
 end;

Procedure  TCodeFix.FixEditor;
 begin
   fstrings.Text:=FEditor.Text;
   FixStrings;
   FEditor.Text:=fstrings.Text;
   FEditor.Modified:=true;
 end;

Procedure  TCodeFix.xFindReplaceCase(const inText,OutText:string);
 var i:integer;
     sl1,sl2:string;
 begin
   for i:=0 to fstrings.Count-1 do
    begin
      sl1:=fstrings[i];
      sl2:=AnsiReplaceStr(sl1,intext,Outtext);
      fstrings[i]:=sl2;
    end;
 end;

Procedure  TCodeFix.xClearLineCase(const inText:string);
 var i:integer;
     sl1:string;
 begin
   for i:=0 to fstrings.Count-1 do
    begin
      sl1:=fstrings[i];
      if AnsiContainsStr(sl1,intext) then
          fstrings[i]:='';
    end;
 end;

Procedure  TCodeFix.xDeleteLineCase(const inText:string);
 var i:integer;
     sl1:string;
     e:boolean;
 begin
   i:=-1;
   e:=false;

   repeat
      i:=i+1;
      sl1:=fstrings[i];
      if AnsiContainsStr(sl1,intext) then
        if AnsiContainsStr(sl1,'}') then
            fstrings[i]:='}' else
            begin
            fstrings.Delete(i);
            i:=i-1;
            end;
     e:=(i=fstrings.Count-1);
    until e=true;

 end;


constructor TCodeFix.Create;
 begin
  inherited;
  FStrings:=TstringList.Create;
 end;
destructor TCodeFix.Destroy;
 begin
  FStrings.Free;
  inherited Destroy;
 end;


end.
