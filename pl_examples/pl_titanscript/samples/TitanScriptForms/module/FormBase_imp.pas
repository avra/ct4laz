{****************************************************************}
{*                                                              *}
{*             Zeljko Cvijanovic - Teslic                       *}
{*              E-mail: cvzeljko@gmail.com                      *}
{*                    www.zeljus.com                            *}
{*                        2015                                  *}
{****************************************************************}
unit FormBase_imp;

{$mode objfpc}{$H+}


interface

uses
  Classes,
  SysUtils,
  fFormBase,
  Forms,
  LCLType,
  Controls,
  base_engine, titanscripter;

procedure RegisterTotalFor_FormBase;

implementation
var H, G:Integer;
    GoR,GoW: TTitanMethodDefinition;


function TFormBase_GetCaption: TCaption;
  begin result:=TFormBase(_Self).Caption; end;
procedure TFormBase_SetCaption(Value: TCaption);
  begin TFormBase(_Self).Caption:= Value; end;



procedure _RegCIOs0;
Begin
   G:=RegisterClassTypeSX(TFormBase);
   RegisterMethodSX(G,'constructor Create(AOwner: TComponent); override;', @TFormBase.Create);
   RegisterMethodSX(G,'destructor Destroy; override;', @TFormBase.Destroy);
   RegisterMethodSX(G,'function ShowModal: Integer; virtual;', @TFormBase.ShowModal);

   //Caption
   GoR:=RegisterMethodSXF(G,'function _GetCaption: String;',@TFormBase_GetCaption);
   GoW:=RegisterMethodSXF(G,'procedure _SetCaption(Value: String);', @TFormBase_SetCaption);
   RegisterPropertySX(G, GoR, GoW,'Caption','TCaption');

end;



procedure RegisterTotalFor_FormBase;
begin
    H:=-1;
 // _RegConstants0;
 // _RegTypes;
  _RegCIOs0;
 // _RegFuncsProcs0;
 // _RegVariables0;

end;

Initialization
  RegisterTotalFor_FormBase;

end.

