{****************************************************************}
{*                                                              *}
{*             Zeljko Cvijanovic - Teslic                       *}
{*              E-mail: cvzeljko@gmail.com                      *}
{*                    www.zeljus.com                            *}
{*                        2015                                  *}
{****************************************************************}
unit fFormBase;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqlite3conn, sqldb, IBConnection,  FileUtil, Forms,
  Controls, Graphics, Dialogs,  DbCtrls, DBGrids, ExtCtrls, ComCtrls;

type

  { TFormBase }

  TFormBase = class(TForm)
    IBConnection: TIBConnection;
    ImageList: TImageList;
    PageControl: TPageControl;
    ScrollBox1: TScrollBox;
    SQLQuery: TSQLQuery;
    SQLTransaction: TSQLTransaction;
    StatusBar1: TStatusBar;
    Forma: TTabSheet;
    Tabela: TTabSheet;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }

  published
   // property DataBase: String read FDataBase;
  end;

var
  FormBase: TFormBase;

implementation

{$R *.lfm}

{ TFormBase }



procedure TFormBase.ToolButton1Click(Sender: TObject);
begin
  Close;
end;


procedure TFormBase.FormCreate(Sender: TObject);
begin


end;




end.

