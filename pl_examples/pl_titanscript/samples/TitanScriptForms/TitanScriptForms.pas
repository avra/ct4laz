{****************************************************************}
{*                                                              *}
{*             Zeljko Cvijanovic - Teslic                       *}
{*              E-mail: cvzeljko@gmail.com                      *}
{*                    www.zeljus.com                            *}
{*                        2015                                  *}
{****************************************************************}
program TitanScriptForms;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, pl_titanscript, TitanScriptFormsmw //, modules
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

