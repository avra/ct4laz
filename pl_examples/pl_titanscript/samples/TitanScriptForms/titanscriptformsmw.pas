{****************************************************************}
{*                                                              *}
{*             Zeljko Cvijanovic - Teslic                       *}
{*              E-mail: cvzeljko@gmail.com                      *}
{*                    www.zeljus.com                            *}
{*                        2015                                  *}
{****************************************************************}
unit TitanScriptFormsmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Sqlite3DS, sqlite3conn, sqldb, FileUtil, SynEdit,
  SynHighlighterPas, TitanScripterBaseEngine, TitanScripter,
  Titan_PascalLanguage, Forms, Controls, Graphics, Dialogs, StdCtrls,
    //....Registration Units...........

  allregsystem,  modules,

  //....Registration Units...........
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Memo1: TMemo;
    Script: TBaseScriptEngine;
    Button1: TButton;
    Panel1: TPanel;
    SynEdit1: TSynEdit;
    SynPasSyn1: TSynPasSyn;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
    procedure DoOnError(Sender : TObject; Const aErrorType:integer);
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  Memo1.Clear;
  Script.Clear;   //must clear on new stript
  Script.OnError:=@DoOnError;
  Script.ScriptText:=SynEdit1.Text;
  Script.Run;
end;



procedure TForm1.DoOnError(Sender: TObject; const aErrorType: integer);
var ss:string;
begin
   if aErrorType=0 then exit;
   ss:='ERROR : '+ Script.ErrorDescription+
       ' At pos '+ Script.ErrorModuleName+
       //' -> '+ inttostr(BaseScriptEngine1.ErrorLine)+
       ' : '+  inttostr(Script.ErrorPos)+
       ' : '+  inttostr(Script.ErrorTextPos);

   Memo1.Lines.Add(ss);
end;



end.

