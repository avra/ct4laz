unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  TitanScripterBaseEngine;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
     src: TBaseScriptEngine;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormShow(Sender: TObject);
begin

   src:= TBaseScriptEngine.Create(self);

end;

end.

