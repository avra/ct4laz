unit Simpleidemw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, Menus, ActnList, ComCtrls, TitanScripterBaseEngine,
  //....Registration Units...........

  allregsystem,

  //....Registration Units...........

  SynEdit, SynHighlighterPas;

type

  { TForm1 }

  TForm1 = class(TForm)
    actExit: TAction;
    actStop: TAction;
    actRun: TAction;
    actSaveAs: TAction;
    actSave: TAction;
    actOpen: TAction;
    ActionList1: TActionList;
    BaseScriptEngine1: TBaseScriptEngine;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    Memo1: TMemo;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    OpenDialog1: TOpenDialog;
    Panel1: TPanel;
    SaveDialog1: TSaveDialog;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    SynEdit1: TSynEdit;
    SynPasSyn1: TSynPasSyn;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    procedure actExitExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
  private
    fFilename:string;
    procedure DoOnError(Sender : TObject; Const aErrorType:integer);
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}

//-------------------------------------------------- 9999
const
// CodeTyphon: Different platforms store resource files on different locations
{$IFDEF Windows}
  pathMedia = '..\testscripts\';
{$ENDIF}
{$IFDEF UNIX}
  pathMedia = '../testscripts/';
{$ENDIF}
//--------------------------------------------------

procedure TForm1.actExitExecute(Sender: TObject);
begin
  close;
end;

procedure TForm1.actOpenExecute(Sender: TObject);
begin
   if fFilename='' then
    OpenDialog1.InitialDir:=pathMedia else
    OpenDialog1.FileName:=fFilename;

  if OpenDialog1.Execute then
   begin
    SynEdit1.Lines.LoadFromFile(OpenDialog1.FileName);
    fFilename:=OpenDialog1.FileName;
    StatusBar1.SimpleText:=fFilename;
    BaseScriptEngine1.Clear;   //must clear on new stript
   end;
end;

procedure TForm1.actSaveAsExecute(Sender: TObject);
begin
  if fFilename='' then
   SaveDialog1.InitialDir:=pathMedia;
   SaveDialog1.FileName:=OpenDialog1.FileName;
   if SaveDialog1.Execute then
   begin
    SynEdit1.Lines.SaveToFile(SaveDialog1.FileName);
    fFilename:=OpenDialog1.FileName;
    StatusBar1.SimpleText:=fFilename;
   end;
end;

procedure TForm1.actSaveExecute(Sender: TObject);
begin
   if fFilename='' then
   actSaveAsExecute(nil) else
   SynEdit1.Lines.SaveToFile(fFileName);
   StatusBar1.SimpleText:=fFilename;
end;

procedure TForm1.actRunExecute(Sender: TObject);
begin
   Memo1.Clear;
   BaseScriptEngine1.Clear;   //must clear on new stript
   BaseScriptEngine1.OnError:=@DoOnError;
   BaseScriptEngine1.ScriptText:=SynEdit1.Text;
   BaseScriptEngine1.Run;
end;

procedure TForm1.actStopExecute(Sender: TObject);
begin
   BaseScriptEngine1.Stop;
end;

procedure TForm1.DoOnError(Sender : TObject; Const aErrorType:integer);
 var ss:string;
 begin
    if aErrorType=0 then exit;
    ss:='ERROR : '+BaseScriptEngine1.ErrorDescription+
        ' At pos '+BaseScriptEngine1.ErrorModuleName+
        //' -> '+ inttostr(BaseScriptEngine1.ErrorLine)+
        ' : '+  inttostr(BaseScriptEngine1.ErrorPos)+
        ' : '+  inttostr(BaseScriptEngine1.ErrorTextPos);

    Memo1.Lines.Add(ss);

 end;

end.

