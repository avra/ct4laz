{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_graphics;

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, mscoreengine, msrtti_classes, Graphics;

type
  TmscrRTTILibrary_Graphics = class(TComponent);

implementation
type
  THackGraphic = class(TGraphic)
  end;

  TLibrary_Graphics = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterConsts(AScript: TmscrScript);
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
    procedure GetColorProc(const Name: string);
  public
    constructor Create(AScript: TmscrScript); override;
  end;


constructor TLibrary_Graphics.Create(AScript: TmscrScript);
begin
    inherited Create(AScript);

    RegisterConsts(AScript);
    RegisterTypes(AScript);
    RegisterClasses(AScript);
end;

function TLibrary_Graphics.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
    Result := 0;

    if aInstance<>NIL then
      Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;

procedure TLibrary_Graphics.RegisterConsts(AScript: TmscrScript);
begin
  with AScript do
  begin  
    GetColorValues(GetColorProc);
  end;
end;

procedure TLibrary_Graphics.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_EnumSet('TFontStyles', 'fsBold, fsItalic, fsStrikeout, fsUnderline');
    Register_Enum('TFontPitch', 'fpDefault, fpVariable, fpFixed');
    Register_Enum('TPenStyle', 'psSolid, psDash, psDot, psDashDot, psDashDotDot, psClear, psInsideFrame');
    Register_Enum('TPenMode', 'pmBlack, pmWhite, pmNop, pmNot, pmCopy, pmNotCopy, pmMergePenNot, ' +
                              'pmMaskPenNot, pmMergeNotPen, pmMaskNotPen, pmMerge, pmNotMerge, pmMask, pmNotMask, pmXor, pmNotXor');
    Register_Enum('TBrushStyle', 'bsSolid, bsClear, bsHorizontal, bsVertical, ' + 'bsFDiagonal, bsBDiagonal, bsCross, bsDiagCross');
  end;
end;

procedure TLibrary_Graphics.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin

    with Register_Class(TFont, 'TPersistent') do
      Register_Constructor('constructor Create', ClassCallMethod);

    with Register_Class(TPen, 'TPersistent') do
      Register_Constructor('constructor Create', ClassCallMethod);

    with Register_Class(TBrush, 'TPersistent') do
      Register_Constructor('constructor Create', ClassCallMethod);

    with Register_Class(TCanvas, 'TPersistent') do
    begin
      Register_Constructor('constructor Create', ClassCallMethod);
      Register_Method('procedure Draw(X, Y: Integer; Graphic: TGraphic)', ClassCallMethod);
      Register_Method('procedure Ellipse(X1, Y1, X2, Y2: Integer)', ClassCallMethod);
      Register_Method('procedure LineTo(X, Y: Integer)', ClassCallMethod);
      Register_Method('procedure MoveTo(X, Y: Integer)', ClassCallMethod);
      Register_Method('procedure Rectangle(X1, Y1, X2, Y2: Integer)', ClassCallMethod);
      Register_Method('procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: Integer)', ClassCallMethod);
      Register_Method('procedure StretchDraw(X1, Y1, X2, Y2: Integer; Graphic: TGraphic)', ClassCallMethod);
      Register_Method('function TextHeight(const Text: string): Integer', ClassCallMethod);
      Register_Method('procedure TextOut(X, Y: Integer; const Text: string)', ClassCallMethod);
      Register_Method('function TextWidth(const Text: string): Integer', ClassCallMethod);
      Register_IndexProperty('Pixels', 'Integer, Integer', 'TColor', ClassCallMethod);
    end;

    with Register_Class(TGraphic, 'TPersistent') do
    begin
      Register_Constructor('constructor Create', ClassCallMethod);
      Register_Method('procedure LoadFromFile(const Filename: string)', ClassCallMethod);
      Register_Method('procedure SaveToFile(const Filename: string)', ClassCallMethod);
      Register_Property('Height', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('Width', 'Integer', ClassGetProp, ClassSetProp);
    end;

    with Register_Class(TPicture, 'TPersistent') do
    begin
      Register_Method('procedure LoadFromFile(const Filename: string)', ClassCallMethod);
      Register_Method('procedure SaveToFile(const Filename: string)', ClassCallMethod);
      Register_Property('Height', 'Integer', ClassGetProp, nil);
      Register_Property('Width', 'Integer', ClassGetProp, nil);
    end;

    with Register_Class(TBitmap, 'TGraphic') do
      Register_Property('Canvas', 'TCanvas', ClassGetProp);
  end;
end;

function TLibrary_Graphics.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  _Canvas: TCanvas;
begin
  Result := 0;

  if aClassType = TFont then begin
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(TFont(aInstance).Create); exit; end;
  exit; end;

  if aClassType = TPen then begin
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(TPen(aInstance).Create); exit; end;
  exit; end;

  if aClassType = TBrush then begin
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(TBrush(aInstance).Create); exit; end;
  exit; end;

  if aClassType = TCanvas then begin
    _Canvas := TCanvas(aInstance);

    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(TCanvas(aInstance).Create); exit; end;
    if aMethodName = 'DRAW' then begin
      _Canvas.Draw(aCallVar.Params[0], aCallVar.Params[1], TGraphic(fmscrInteger(aCallVar.Params[2]))); exit; end;
    if aMethodName = 'ELLIPSE' then begin
      _Canvas.Ellipse(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2], aCallVar.Params[3]); exit; end;
    if aMethodName = 'LINETO' then begin
      _Canvas.LineTo(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'MOVETO' then begin
      _Canvas.MoveTo(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'RECTANGLE' then begin
      _Canvas.Rectangle(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2], aCallVar.Params[3]); exit; end;
    if aMethodName = 'ROUNDRECT' then begin
      _Canvas.RoundRect(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2], aCallVar.Params[3], aCallVar.Params[4], aCallVar.Params[5]); exit; end;
    if aMethodName = 'STRETCHDRAW' then begin
      _Canvas.StretchDraw(Rect(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2], aCallVar.Params[3]), TGraphic(fmscrInteger(aCallVar.Params[4]))); exit; end;
    if aMethodName = 'TEXTHEIGHT' then begin
      Result := _Canvas.TextHeight(aCallVar.Params[0]); exit; end;
    if aMethodName = 'TEXTOUT' then begin
      _Canvas.TextOut(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'TEXTWIDTH' then begin
      Result := _Canvas.TextWidth(aCallVar.Params[0]); exit; end;
    if aMethodName = 'PIXELS.GET' then begin
      Result := _Canvas.Pixels[aCallVar.Params[0], aCallVar.Params[1]]; exit; end;
    if aMethodName = 'PIXELS.SET' then begin
      _Canvas.Pixels[aCallVar.Params[0], aCallVar.Params[1]] := aCallVar.Params[2]; exit; end;
  exit; end;

  if aClassType = TGraphic then begin
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(THackGraphic(aInstance).Create); exit; end;
    if aMethodName = 'LOADFROMFILE' then begin
      TGraphic(aInstance).LoadFromFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'SAVETOFILE' then begin
      TGraphic(aInstance).SaveToFile(aCallVar.Params[0]); exit; end;
  exit; end;

  if aClassType = TPicture then begin
    if aMethodName = 'LOADFROMFILE' then begin
      TPicture(aInstance).LoadFromFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'SAVETOFILE' then begin
      TPicture(aInstance).SaveToFile(aCallVar.Params[0]); exit; end;
  exit; end;

end;

function TLibrary_Graphics.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
begin
  Result := 0;

  if aClassType = TGraphic then begin
    if aPropName = 'HEIGHT' then begin
      Result := TGraphic(aInstance).Height; exit; end;
    if aPropName = 'WIDTH' then begin
      Result := TGraphic(aInstance).Width; exit; end;
  exit; end;

  if aClassType = TPicture then begin
    if aPropName = 'HEIGHT' then begin
      Result := TPicture(aInstance).Height; exit; end;
    if aPropName = 'WIDTH' then begin
      Result := TPicture(aInstance).Width; exit; end;
  exit; end;

  if aClassType = TBitmap then begin
    if aPropName = 'CANVAS' then begin
      Result := fmscrInteger(TBitmap(aInstance).Canvas); exit; end;
  exit; end;

end;

procedure TLibrary_Graphics.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
begin
  if aClassType = TGraphic then begin
    if aPropName = 'HEIGHT' then begin
      TGraphic(aInstance).Height := aValue; exit; end;
    if aPropName = 'WIDTH' then begin
      TGraphic(aInstance).Width := aValue; exit; end;
  exit; end;
end;

procedure TLibrary_Graphics.GetColorProc(const Name: string);
var
  c: integer;
begin
  IdentToColor(Name, c);
  Script.Register_Const(Name, 'Integer', c);
end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_Graphics);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_Graphics);

end.
