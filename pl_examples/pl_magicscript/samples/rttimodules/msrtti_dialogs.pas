{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_dialogs;

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, mscoreengine, msrtti_classes, Dialogs;

type
  TmscrRTTILibrary_Dialogs = class(TComponent);

implementation
type
  THackDialog = class(TCommonDialog);

  TLibrary_Dialogs = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterProcs(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ProcCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
  public
    constructor Create(AScript: TmscrScript); override;
  end;

type
  TWordSet = set of 0..15;
  PWordSet = ^TWordSet;

constructor TLibrary_Dialogs.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);

  RegisterTypes(AScript);
  RegisterProcs(AScript);
  RegisterClasses(AScript);
end;

function TLibrary_Dialogs.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
  if aInstance=NIL then
   begin
    Result:=ProcCallMethod(aInstance, aClassType, aMethodName, aCallVar);
    if Result <> 0 then Exit;
   end;
  if aInstance<>NIL then
    Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;


procedure TLibrary_Dialogs.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_EnumSet('TOpenOptions', 'ofReadOnly, ofOverwritePrompt, ofHideReadOnly,' +
                                     'ofNoChangeDir, ofShowHelp, ofNoValidate, ofAllowMultiSelect,' +
                                     'ofExtensionDifferent, ofPathMustExist, ofFileMustExist, ofCreatePrompt,' +
                                     'ofShareAware, ofNoReadOnlyReturn, ofNoTestFileCreate, ofNoNetworkButton,' +
                                     'ofNoLongNames, ofOldStyleDialog, ofNoDereferenceLinks, ofEnableIncludeNotify,' +
                                     'ofEnableSizing');
    Register_Enum('TFileEditStyle', 'fsEdit, fsComboBox');
    Register_EnumSet('TColorDialogOptions', 'cdFullOpen, cdPreventFullOpen, cdShowHelp, cdSolidColor, cdAnyColor');
    Register_EnumSet('TFontDialogOptions', 'fdAnsiOnly, fdTrueTypeOnly, fdEffects,' +
                     'fdFixedPitchOnly, fdForceFontExist, fdNoFaceSel, fdNoOEMFonts,' +
                     'fdNoSimulations, fdNoSizeSel, fdNoStyleSel,  fdNoVectorFonts,' +
                     'fdShowHelp, fdWysiwyg, fdLimitSize, fdScalableOnly, fdApplyButton');
    Register_Enum('TFontDialogDevice', 'fdScreen, fdPrinter, fdBoth');
    Register_Enum('TPrintRange', 'prAllPages, prSelection, prPageNums');
    Register_EnumSet('TPrintDialogOptions', 'poPrintToFile, poPageNums, poSelection, poWarning, poHelp, poDisablePrintToFile');
    Register_Enum('TMsgDlgType', 'mtWarning, mtError, mtInformation, mtConfirmation, mtCustom');
    Register_EnumSet('TMsgDlgButtons', 'mbYes, mbNo, mbOK, mbCancel, mbAbort, mbRetry, mbIgnore, mbAll, mbNoToAll, mbYesToAll, mbHelp');
  end;
end;

procedure TLibrary_Dialogs.RegisterProcs(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_Method('function MessageDlg(Msg: string; DlgType: TMsgDlgType; Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer', ProcCallMethod, 'catOther');
    Register_Method('function InputBox(ACaption, APrompt, ADefault: string): string', ProcCallMethod, 'catOther');
    Register_Method('function InputQuery(ACaption, APrompt: string; var Value: string): Boolean', ProcCallMethod, 'catOther');
  end;
end;

function TLibrary_Dialogs.ProcCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  s: string;
  b: TMsgDlgButtons;
begin
  Result := 0;

  if aMethodName = 'INPUTBOX' then begin
    Result := InputBox(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]);
  exit; end;

  if aMethodName = 'INPUTQUERY' then begin
    s := aCallVar.Params[2];
    Result := InputQuery(aCallVar.Params[0], aCallVar.Params[1], s);
    aCallVar.Params[2] := s;
  exit; end;

  if aMethodName = 'MESSAGEDLG' then begin
    word(PWordSet(@b)^) := aCallVar.Params[2];  //<<<=== ct9999 NOT of mode objfpc ==================
    Result := MessageDlg(aCallVar.Params[0], aCallVar.Params[1], b, aCallVar.Params[3]);
  exit; end;
end;

procedure TLibrary_Dialogs.RegisterClasses(AScript: TmscrScript);
var
  dlg: string;
begin

  with AScript do
  begin

    dlg := 'TCommonDialog';
    with Register_Class(TCommonDialog, 'TComponent') do
      Register_Method('function Execute: Boolean', ClassCallMethod);

    Register_Class(TOpenDialog, dlg);
    Register_Class(TSaveDialog, dlg);
    Register_Class(TColorDialog, dlg);
    Register_Class(TFontDialog, dlg);

  end;
end;

function TLibrary_Dialogs.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;

  if aClassType = TCommonDialog then begin
    if aMethodName = 'EXECUTE' then begin
      Result := THackDialog(aInstance).Execute; exit; end;
  exit; end;

end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_Dialogs);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_Dialogs);

end.
