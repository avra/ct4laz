{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_forms;

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, mscoreengine, msrtti_classes,
  msrtti_graphics, LCLType, Buttons, Controls, Forms, StdCtrls;

type
  TmscrRTTILibrary_Forms = class(TComponent);

implementation
type
  TLibrary_Forms = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterConsts(AScript: TmscrScript);
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    procedure RegisterVariables(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
  public
    constructor Create(AScript: TmscrScript); override;
  end;


constructor TLibrary_Forms.Create(AScript: TmscrScript);
begin
    inherited Create(AScript);

    RegisterConsts(AScript);
    RegisterTypes(AScript);
    RegisterClasses(AScript);
    RegisterVariables(AScript);
end;

function TLibrary_Forms.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
    Result := 0;

    if aInstance<>NIL then
      Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;


procedure TLibrary_Forms.RegisterConsts(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_Const('mrNone', 'Integer', mrNone);
    Register_Const('mrOk', 'Integer', mrOk);
    Register_Const('mrCancel', 'Integer', mrCancel);
    Register_Const('mrAbort', 'Integer', mrAbort);
    Register_Const('mrRetry', 'Integer', mrRetry);
    Register_Const('mrIgnore', 'Integer', mrIgnore);
    Register_Const('mrYes', 'Integer', mrYes);
    Register_Const('mrNo', 'Integer', mrNo);
    Register_Const('mrAll', 'Integer', mrAll);
    Register_Const('mrNoToAll', 'Integer', mrNoToAll);
    Register_Const('mrYesToAll', 'Integer', mrYesToAll);

    Register_Const('crDefault', 'Integer', crDefault);
    Register_Const('crNone', 'Integer', crNone);
    Register_Const('crArrow', 'Integer', crArrow);
    Register_Const('crCross', 'Integer', crCross);
    Register_Const('crIBeam', 'Integer', crIBeam);
    Register_Const('crSize', 'Integer', crSize);
    Register_Const('crSizeNESW', 'Integer', crSizeNESW);
    Register_Const('crSizeNS', 'Integer', crSizeNS);
    Register_Const('crSizeNWSE', 'Integer', crSizeNWSE);
    Register_Const('crSizeWE', 'Integer', crSizeWE);
    Register_Const('crUpArrow', 'Integer', crUpArrow);
    Register_Const('crHourGlass', 'Integer', crHourGlass);
    Register_Const('crDrag', 'Integer', crDrag);
    Register_Const('crNoDrop', 'Integer', crNoDrop);
    Register_Const('crHSplit', 'Integer', crHSplit);
    Register_Const('crVSplit', 'Integer', crVSplit);
    Register_Const('crMultiDrag', 'Integer', crMultiDrag);
    Register_Const('crSQLWait', 'Integer', crSQLWait);
    Register_Const('crNo', 'Integer', crNo);
    Register_Const('crAppStart', 'Integer', crAppStart);
    Register_Const('crHelp', 'Integer', crHelp);
    Register_Const('crHandPoint', 'Integer', crHandPoint);
    Register_Const('crSizeAll', 'Integer', crSizeAll);
    Register_Const('bsNone', 'Integer', bsNone);
    Register_Const('bsSingle', 'Integer', bsSingle);
    Register_Const('bsSizeable', 'Integer', bsSizeable);
    Register_Const('bsDialog', 'Integer', bsDialog);
    Register_Const('bsToolWindow', 'Integer', bsToolWindow);
    Register_Const('bsSizeToolWin', 'Integer', bsSizeToolWin);


    Register_Const('VK_RBUTTON', 'Integer', VK_RBUTTON);
    Register_Const('VK_CANCEL', 'Integer', VK_CANCEL);
    Register_Const('VK_MBUTTON', 'Integer', VK_MBUTTON);
    Register_Const('VK_BACK', 'Integer', VK_BACK);//Backspace key
    Register_Const('VK_TAB', 'Integer', VK_TAB);//Tab key
    Register_Const('VK_RETURN', 'Integer', VK_RETURN);//Enter key
    Register_Const('VK_SHIFT', 'Integer', VK_SHIFT);//Shift key
    Register_Const('VK_CONTROL', 'Integer', VK_CONTROL);//Ctrl key
    Register_Const('VK_MENU', 'Integer', VK_MENU);//Alt key
    Register_Const('VK_PAUSE', 'Integer', VK_PAUSE);//Pause key
    Register_Const('VK_CAPITAL', 'Integer', VK_CAPITAL);//Caps Lock key
    Register_Const('VK_ESCAPE', 'Integer', VK_ESCAPE);//Esc key
    Register_Const('VK_SPACE', 'Integer', VK_SPACE);//Space bar
    Register_Const('VK_PRIOR', 'Integer', VK_PRIOR);//Page Up key
    Register_Const('VK_NEXT', 'Integer', VK_NEXT);// Page Down key
    Register_Const('VK_END', 'Integer', VK_END);// End key
    Register_Const('VK_HOME', 'Integer', VK_HOME);// Home key
    Register_Const('VK_LEFT', 'Integer', VK_LEFT);// Left Arrow key
    Register_Const('VK_UP', 'Integer', VK_UP);// Up Arrow key
    Register_Const('VK_RIGHT', 'Integer', VK_RIGHT);// Right Arrow key
    Register_Const('VK_DOWN', 'Integer', VK_DOWN);// Down Arrow key
    Register_Const('VK_INSERT', 'Integer', VK_INSERT);// Insert key
    Register_Const('VK_DELETE', 'Integer', VK_DELETE);// Delete key
    Register_Const('VK_HELP', 'Integer', VK_HELP);// Help key
    Register_Const('VK_LWIN', 'Integer', VK_LWIN);// Left Windows key (Microsoft keyboard)
    Register_Const('VK_RWIN', 'Integer', VK_RWIN);// Right Windows key (Microsoft keyboard)
    Register_Const('VK_APPS', 'Integer', VK_APPS);// Applications key (Microsoft keyboard)
    Register_Const('VK_NUMPAD0', 'Integer', VK_NUMPAD0);// 0 key (numeric keypad)
    Register_Const('VK_NUMPAD1', 'Integer', VK_NUMPAD1);// 1 key (numeric keypad)
    Register_Const('VK_NUMPAD2', 'Integer', VK_NUMPAD2);// 2 key (numeric keypad)
    Register_Const('VK_NUMPAD3', 'Integer', VK_NUMPAD3);// 3 key (numeric keypad)
    Register_Const('VK_NUMPAD4', 'Integer', VK_NUMPAD4);// 4 key (numeric keypad)
    Register_Const('VK_NUMPAD5', 'Integer', VK_NUMPAD5);// 5 key (numeric keypad)
    Register_Const('VK_NUMPAD6', 'Integer', VK_NUMPAD6);// 6 key (numeric keypad)
    Register_Const('VK_NUMPAD7', 'Integer', VK_NUMPAD7);// 7 key (numeric keypad)
    Register_Const('VK_NUMPAD8', 'Integer', VK_NUMPAD8);// 8 key (numeric keypad)
    Register_Const('VK_NUMPAD9', 'Integer', VK_NUMPAD9);// 9 key (numeric keypad)
    Register_Const('VK_MULTIPLY', 'Integer', VK_MULTIPLY);// Multiply key (numeric keypad)
    Register_Const('VK_ADD', 'Integer', VK_ADD);// Add key (numeric keypad)
    Register_Const('VK_SEPARATOR', 'Integer', VK_SEPARATOR);// Separator key (numeric keypad)
    Register_Const('VK_SUBTRACT', 'Integer', VK_SUBTRACT);// Subtract key (numeric keypad)
    Register_Const('VK_DECIMAL', 'Integer', VK_DECIMAL);// Decimal key (numeric keypad)
    Register_Const('VK_DIVIDE', 'Integer', VK_DIVIDE);// Divide key (numeric keypad)
    Register_Const('VK_F1', 'Integer', VK_F1);// F1 key
    Register_Const('VK_F2', 'Integer', VK_F2);// F2 key
    Register_Const('VK_F3', 'Integer', VK_F3);// F3 key
    Register_Const('VK_F4', 'Integer', VK_F4);// F4 key
    Register_Const('VK_F5', 'Integer', VK_F5);// F5 key
    Register_Const('VK_F6', 'Integer', VK_F6);// F6 key
    Register_Const('VK_F7', 'Integer', VK_F7);// F7 key
    Register_Const('VK_F8', 'Integer', VK_F8);// F8 key
    Register_Const('VK_F9', 'Integer', VK_F9);// F9 key
    Register_Const('VK_F10', 'Integer', VK_F10);// F10 key
    Register_Const('VK_F11', 'Integer', VK_F11);// F11 key
    Register_Const('VK_F12', 'Integer', VK_F12);// F12 key
    Register_Const('VK_NUMLOCK', 'Integer', VK_NUMLOCK);// Num Lock key
    Register_Const('VK_SCROLL', 'Integer', VK_SCROLL);// Scroll Lock key

    Register_Const('crDefault', 'Integer', crDefault);
    Register_Const('crNone', 'Integer', crNone);
    Register_Const('crArrow', 'Integer', crArrow);
    Register_Const('crCross', 'Integer', crCross);
    Register_Const('crIBeam', 'Integer', crIBeam);
    Register_Const('crSize', 'Integer', crSize);
    Register_Const('crSizeNESW', 'Integer', crSizeNESW);
    Register_Const('crSizeNS', 'Integer', crSizeNS);
    Register_Const('crSizeNWSE', 'Integer', crSizeNWSE);
    Register_Const('crSizeWE', 'Integer', crSizeWE);
    Register_Const('crUpArrow', 'Integer', crUpArrow);
    Register_Const('crHourGlass', 'Integer', crHourGlass);
    Register_Const('crDrag', 'Integer', crDrag);
    Register_Const('crNoDrop', 'Integer', crNoDrop);
    Register_Const('crHSplit', 'Integer', crHSplit);
    Register_Const('crVSplit', 'Integer', crVSplit);
    Register_Const('crMultiDrag', 'Integer', crMultiDrag);
    Register_Const('crSQLWait', 'Integer', crSQLWait);
    Register_Const('crNo', 'Integer', crNo);
    Register_Const('crAppStart', 'Integer', crAppStart);
    Register_Const('crHelp', 'Integer', crHelp);
    Register_Const('crHandPoint', 'Integer', crHandPoint);
    Register_Const('crSizeAll', 'Integer', crSizeAll); 
    Register_Const('taLeftJustify', 'Integer', taLeftJustify);
    Register_Const('taRightJustify', 'Integer', taRightJustify);
    Register_Const('taCenter', 'Integer', taCenter);

  end;
end;

procedure TLibrary_Forms.RegisterTypes(AScript: TmscrScript);
begin
 with AScript do
 begin
    Register_Type('TFormBorderStyle', msvtInt);
    Register_Type('TBorderStyle', msvtInt);
    Register_Type('TAlignment', msvtInt);
    Register_Type('TLeftRight', msvtInt);

    Register_EnumSet('TShiftState', 'ssShift, ssAlt, ssCtrl, ssLeft, ssRight, ssMiddle, ssDouble');
    Register_Enum('TAlign', 'alNone, alTop, alBottom, alLeft, alRight, alClient');
    Register_Enum('TMouseButton', 'mbLeft, mbRight, mbMiddle');
    Register_EnumSet('TAnchors', 'akLeft, akTop, akRight, akBottom');
    Register_Enum('TBevelCut', 'bvNone, bvLowered, bvRaised, bvSpace');
    Register_Enum('TTextLayout', 'tlTop, tlCenter, tlBottom');
    Register_Enum('TEditCharCase', 'ecNormal, ecUpperCase, ecLowerCase');
    Register_Enum('TScrollStyle', 'ssNone, ssHorizontal, ssVertical, ssBoth');
    Register_Enum('TComboBoxStyle', 'csDropDown, csSimple, csDropDownList, csOwnerDrawFixed, csOwnerDrawVariable');
    Register_Enum('TCheckBoxState', 'cbUnchecked, cbChecked, cbGrayed');
    Register_Enum('TListBoxStyle', 'lbStandard, lbOwnerDrawFixed, lbOwnerDrawVariable');
    Register_Enum('TWindowState', 'wsNormal, wsMinimized, wsMaximized');
    Register_Enum('TFormStyle', 'fsNormal, fsMDIChild, fsMDIForm, fsStayOnTop');
    Register_EnumSet('TBorderIcons', 'biSystemMenu, biMinimize, biMaximize, biHelp');
    Register_Enum('TPosition', 'poDesigned, poDefault, poDefaultPosOnly, poDefaultSizeOnly, poScreenCenter, poDesktopCenter');
    Register_Enum('TCloseAction', 'caNone, caHide, caFree, caMinimize');
 end;
end;

procedure TLibrary_Forms.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin

    with Register_Class(TControl, 'TComponent') do
    begin
      Register_Property('Parent', 'TWinControl', ClassGetProp, ClassSetProp);
      Register_Method('procedure Hide', ClassCallMethod);
      Register_Method('procedure Show', ClassCallMethod);
      Register_Method('procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer)', ClassCallMethod);
      Register_Event('OnCanResize', TmscrCanResizeEvent);
      Register_Event('OnClick', TmscrNotifyEvent);
      Register_Event('OnDblClick', TmscrNotifyEvent);
      Register_Event('OnMouseDown', TmscrMouseEvent);
      Register_Event('OnMouseMove', TmscrMouseMoveEvent);
      Register_Event('OnMouseUp', TmscrMouseEvent);
      Register_Event('OnResize', TmscrNotifyEvent);
    end;
    with Register_Class(TWinControl, 'TControl') do
    begin
      Register_Method('procedure SetFocus', ClassCallMethod);
      Register_Method('procedure Invalidate', ClassCallMethod);
      Register_Event('OnEnter', TmscrNotifyEvent);
      Register_Event('OnExit', TmscrNotifyEvent);
      Register_Event('OnKeyDown', TmscrKeyEvent);
      Register_Event('OnKeyPress', TmscrKeyPressEvent);
      Register_Event('OnKeyUp', TmscrKeyEvent);
    end;
    Register_Class(TCustomControl, 'TWinControl');
    Register_Class(TGraphicControl, 'TControl');
    Register_Class(TGroupBox, 'TWinControl');
    Register_Class(TLabel, 'TControl');
    Register_Class(TEdit, 'TWinControl');
    with Register_Class(TMemo, 'TWinControl') do
    begin
      Register_Property('Text', 'String', ClassGetProp, ClassSetProp);
    end;
    with Register_Class(TCustomComboBox, 'TWinControl') do
    begin
      Register_Property('DroppedDown', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('ItemIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Event('OnChange', TmscrNotifyEvent);
      Register_Event('OnDropDown', TmscrNotifyEvent);
      Register_Event('OnCloseUp', TmscrNotifyEvent);
    end;
    Register_Class(TComboBox, 'TCustomComboBox');
    Register_Class(TButton, 'TWinControl');
    Register_Class(TCheckBox, 'TWinControl');
    Register_Class(TRadioButton, 'TWinControl');
    with Register_Class(TCustomListBox, 'TWinControl') do
    begin
      Register_Property('ItemIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('SelCount', 'Integer', ClassGetProp, nil);
      Register_IndexProperty('Selected', 'Integer', 'Boolean', ClassCallMethod);
    end;
    Register_Class(TListBox, 'TCustomListBox');
    Register_Class(TControlScrollBar, 'TPersistent');
    Register_Class(TScrollingWinControl, 'TWinControl');
    Register_Class(TScrollBox, 'TScrollingWinControl');
    with Register_Class(TCustomForm, 'TScrollingWinControl') do
    begin
      Register_Method('procedure Close', ClassCallMethod);
      Register_Method('procedure Hide', ClassCallMethod);
      Register_Method('procedure Show', ClassCallMethod);
      Register_Method('function ShowModal: Integer', ClassCallMethod);
      Register_Event('OnActivate', TmscrNotifyEvent);
      Register_Event('OnClose', TmscrCloseEvent);
      Register_Event('OnCloseQuery', TmscrCloseQueryEvent);
      Register_Event('OnCreate', TmscrNotifyEvent);
      Register_Event('OnDestroy', TmscrNotifyEvent);
      Register_Event('OnDeactivate', TmscrNotifyEvent);
      Register_Event('OnHide', TmscrNotifyEvent);
      Register_Event('OnPaint', TmscrNotifyEvent);
      Register_Event('OnShow', TmscrNotifyEvent);
      Register_Property('Canvas', 'TCanvas', ClassGetProp, nil);
      Register_Property('ModalResult', 'Integer', ClassGetProp, ClassSetProp);
    end;
    Register_Class(TForm, 'TCustomForm');
    Register_Class(TDataModule, 'TComponent');

    with Register_Class(TApplication, 'TComponent') do
    begin
      Register_Method('procedure Minimize', ClassCallMethod);
      Register_Method('procedure HandleMessage', ClassCallMethod);
      Register_Method('procedure ProcessMessages', ClassCallMethod);
      Register_Method('procedure Restore', ClassCallMethod);
      Register_Property('ExeName', 'String', ClassGetProp, nil);
      Register_Property('Title', 'String', ClassGetProp, ClassSetProp);
      Register_Property('MainForm', 'TForm', ClassGetProp, nil);
    end;

    with Register_Class(TScreen, 'TComponent') do
    begin
      Register_Property('Height', 'Integer', ClassGetProp, nil);
      Register_Property('Width', 'Integer', ClassGetProp, nil);
      Register_Property('PixelsPerInch', 'integer', ClassGetProp, nil);
      Register_Property('MonitorCount', 'Integer', ClassGetProp, nil);
    end;

  end;
end;

function TLibrary_Forms.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  Form: TCustomForm;
begin
  Result := 0;

  if aClassType = TControl then begin
    if aMethodName = 'HIDE' then begin
      TControl(aInstance).Hide; exit; end;
    if aMethodName = 'SHOW' then begin
      TControl(aInstance).Show; exit; end;
    if aMethodName = 'SETBOUNDS' then begin
      TControl(aInstance).SetBounds(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2], aCallVar.Params[3]); exit; end;
  exit; end;

  if aClassType = TWinControl then begin
    if aMethodName = 'SETFOCUS' then begin
      TWinControl(aInstance).SetFocus; exit; end;
    if aMethodName = 'INVALIDATE' then begin
      TWinControl(aInstance).Invalidate; exit; end;
  exit; end;

  if aClassType = TCustomListBox then begin
    if aMethodName = 'SELECTED.GET' then begin
      Result := TCustomListBox(aInstance).Selected[aCallVar.Params[0]]; exit; end;
    if aMethodName = 'SELECTED.SET' then begin
      TCustomListBox(aInstance).Selected[aCallVar.Params[0]] := aCallVar.Params[1]; exit; end;
  exit; end;

  if aClassType = TCustomForm then begin
    Form := TCustomForm(aInstance);
    if aMethodName = 'CLOSE' then begin
      Form.Close; exit; end;
    if aMethodName = 'HIDE' then begin
      Form.Hide; exit; end;
    if aMethodName = 'SHOW' then begin
      Form.Show; exit; end;
    if aMethodName = 'SHOWMODAL' then begin
      Result := Form.ShowModal; exit; end;
  exit; end;

  if aClassType = TApplication then begin
    if aMethodName = 'MINIMIZE' then begin
      TApplication(aInstance).Minimize; exit; end;
    if aMethodName = 'HANDLEMESSAGE' then begin
      TApplication(aInstance).HandleMessage; exit; end;
    if aMethodName = 'PROCESSMESSAGES' then begin
      TApplication(aInstance).ProcessMessages; exit; end;
    if aMethodName = 'RESTORE' then begin
      TApplication(aInstance).Restore; exit; end;
  exit; end;
end;

function TLibrary_Forms.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
begin
  Result := 0;
  if aClassType = TControl then begin
    if aPropName = 'PARENT' then begin
      Result := fmscrInteger(TControl(aInstance).Parent); exit; end;
  exit; end;

  if aClassType = TMemo then begin
    if aPropName = 'TEXT' then begin
      Result := TMemo(aInstance).Text; exit; end;
  exit; end;

  if aClassType = TCustomComboBox then begin
    if aPropName = 'DROPPEDDOWN' then begin
      Result := TCustomComboBox(aInstance).DroppedDown; exit; end;
    if aPropName = 'ITEMINDEX' then begin
      Result := TCustomComboBox(aInstance).ItemIndex; exit; end;
  exit; end;

  if aClassType = TCustomListBox then begin
    if aPropName = 'SELCOUNT' then begin
      Result := TCustomListBox(aInstance).SelCount; exit; end;
    if aPropName = 'ITEMINDEX' then begin
      Result := TCustomListBox(aInstance).ItemIndex; exit; end;
  exit; end;

  if aClassType = TCustomForm then begin
    if aPropName = 'MODALRESULT' then begin
      Result := TCustomForm(aInstance).ModalResult; exit; end;
    if aPropName = 'CANVAS' then begin
      Result := fmscrInteger(TCustomForm(aInstance).Canvas); exit; end;
  exit; end;

  if aClassType = TApplication then begin
    if aPropName = 'EXENAME' then begin
      Result := TApplication(aInstance).ExeName; exit; end;
    if aPropName = 'TITLE' then begin
      Result := TApplication(aInstance).Title; exit; end;
    if aPropName = 'MAINFORM' then begin
      Result := fmscrInteger(TApplication(aInstance).MainForm); exit; end;
  exit; end;

  if aClassType = TScreen then begin
    if aPropName = 'HEIGHT' then begin
      Result := TScreen(aInstance).Height; exit; end;
    if aPropName = 'WIDTH' then begin
      Result := TScreen(aInstance).Width; exit; end;
    if aPropName = 'PIXELSPERINCH' then begin
      Result := TScreen(aInstance).PixelsPerInch; exit; end;
    if aPropName = 'MONITORCOUNT' then begin
      Result := TScreen(aInstance).MonitorCount; exit; end;
  exit; end;
end;

procedure TLibrary_Forms.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
begin

  if aClassType = TControl then begin
    if aPropName = 'PARENT' then begin
      TControl(aInstance).Parent := TWinControl(fmscrInteger(aValue)); exit; end;
  exit; end;

  if aClassType = TMemo then begin
    if aPropName = 'TEXT' then begin
      TMemo(aInstance).Text:=aValue; exit; end;
  exit; end;

  if aClassType = TCustomComboBox then begin
    if aPropName = 'DROPPEDDOWN' then begin
      TCustomComboBox(aInstance).DroppedDown := aValue; exit; end;
    if aPropName = 'ITEMINDEX' then begin
      TCustomComboBox(aInstance).ItemIndex := aValue; exit; end;
  exit; end;

  if aClassType = TCustomListBox then begin
    if aPropName = 'ITEMINDEX' then begin
      TCustomListBox(aInstance).ItemIndex := aValue; exit; end;
  exit; end;

  if aClassType = TCustomForm then begin
    if aPropName = 'MODALRESULT' then begin
      TCustomForm(aInstance).ModalResult := aValue; exit; end;
  exit; end;

  if aClassType = TApplication then begin
    if aPropName = 'TITLE' then begin
      TApplication(aInstance).Title:= aValue; exit; end;
  exit; end;

end;

procedure TLibrary_Forms.RegisterVariables(AScript: TmscrScript);
begin
  with AScript do
  begin
     Register_Object('Application',Application);
     Register_Object('Screen',Screen);
  end;
end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_Forms);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_Forms);

end.
