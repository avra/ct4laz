{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_IniFiles;

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, mscoreengine, IniFiles;

type
  TmscrRTTILibrary_IniFiles = class(TComponent);

implementation
type
  TLibrary_IniFiles = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;

    procedure SaveIniFileToStream(oIniFile: TCustomIniFile; oStream: TStream);
    procedure LoadIniFileFromStream(oIniFile: TCustomIniFile; oStream: TStream);
    procedure WriteTStrings(oIniFile: TCustomIniFile; const Section: string; Values: TStrings; IsClear: boolean = True);
    procedure ReadTStrings(oIniFile: TCustomIniFile; const Section: string; Values: TStrings; IsClear: boolean = True);
  public
    constructor Create(AScript: TmscrScript); override;
  end;


constructor TLibrary_IniFiles.Create(AScript: TmscrScript);
begin
    inherited Create(AScript);

    RegisterClasses(AScript);
end;

function TLibrary_IniFiles.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
    Result := 0;
    if aInstance<>NIL then
      Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;

procedure TLibrary_IniFiles.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin
    with Register_Class(TCustomIniFile, 'TObject') do
    begin
      Register_Constructor('constructor Create(const FileName: String)', ClassCallMethod);
      Register_Method('function ReadInteger(const Section, Ident: String; Default: LongInt): LongInt', ClassCallMethod);
      Register_Method('procedure WriteInteger(const Section, Ident: String; Value: LongInt)', ClassCallMethod);
      Register_Method('function ReadBool(const Section, Ident: String; Default: Boolean): Boolean', ClassCallMethod);
      Register_Method('procedure WriteBool(const Section, Ident: String; Value: Boolean)', ClassCallMethod);
      Register_Method('function ReadDate(const Section, Name: String; Default: TDateTime): TDateTime', ClassCallMethod);
      Register_Method('procedure WriteDate(const Section, Name: String; Value: TDateTime)', ClassCallMethod);
      Register_Method('function ReadDateTime(const Section, Name: String; Default: TDateTime): TDateTime', ClassCallMethod);
      Register_Method('procedure WriteDateTime(const Section, Name: String; Value: TDateTime)', ClassCallMethod);
      Register_Method('function ReadFloat(const Section, Name: String; Default: Double): Double', ClassCallMethod);
      Register_Method('procedure WriteFloat(const Section, Name: String; Value: Double)', ClassCallMethod);
      Register_Method('function ReadTime(const Section, Name: String; Default: TDateTime): TDateTime', ClassCallMethod);
      Register_Method('procedure WriteTime(const Section, Name: String; Value: TDateTime);', ClassCallMethod);
      Register_Method('function ReadBinaryStream(const Section, Name: String; Value: TStream): Integer', ClassCallMethod);
      Register_Method('procedure WriteBinaryStream(const Section, Name: String; Value: TStream)', ClassCallMethod);
      Register_Method('function SectionExists(const Section: String): Boolean', ClassCallMethod);
      Register_Method('function ValueExists(const Section, Ident: String): Boolean', ClassCallMethod);
      Register_Method('procedure WriteTStrings(const Section :String; Value :TStrings; IsClear :Boolean = True)', ClassCallMethod);
      Register_Method('function ReadTStrings(const Section :String; Value :TStrings; IsClear :Boolean = True): String;', ClassCallMethod);
      Register_Property('FileName', 'String', ClassGetProp);
    end;

    with Register_Class(TMemIniFile, 'TCustomIniFile') do
    begin
      Register_Constructor('constructor Create(const FileName: String)', ClassCallMethod);
      Register_Method('procedure WriteString(const Section, Ident, Value: String)', ClassCallMethod);
      Register_Method('function ReadString(const Section, Ident, Default: String): String;', ClassCallMethod);
      Register_Method('procedure ReadSectionValuesEx(const Section: String; Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure DeleteKey(const Section, Ident: String)', ClassCallMethod);
      Register_Method('procedure ReadSection(const Section: String; Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure ReadSections(Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure ReadSectionValues(const Section: String; Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure EraseSection(const Section: String)', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure GetStrings(List: TStrings)', ClassCallMethod);
      Register_Method('procedure SetStrings(List: TStrings)', ClassCallMethod);
      Register_Method('procedure SaveIniFileToStream(oStream: TStream)', ClassCallMethod);
      Register_Method('procedure LoadIniFileFromStream(oStream: TStream)', ClassCallMethod);
    end;

    with Register_Class(TIniFile, 'TCustomIniFile') do
    begin
      Register_Method('procedure WriteString(const Section, Ident, Value: String)', ClassCallMethod);
      Register_Method('function ReadString(const Section, Ident, Default: String): String;', ClassCallMethod);
      Register_Method('procedure ReadSectionValuesEx(const Section: String; Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure DeleteKey(const Section, Ident: String)', ClassCallMethod);
      Register_Method('procedure ReadSection(const Section: String; Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure ReadSections(Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure ReadSectionValues(const Section: String; Strings: TStrings)', ClassCallMethod);
      Register_Method('procedure EraseSection(const Section: String)', ClassCallMethod);
      Register_Method('procedure SaveIniFileToStream(oStream: TStream)', ClassCallMethod);
      Register_Method('procedure LoadIniFileFromStream(oStream: TStream)', ClassCallMethod);
    end;

  end;

end;

{$HINTS OFF}
function TLibrary_IniFiles.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  oCustomIniFile: TCustomIniFile;
  oMemIniFile: TMemIniFile;
  oIniFile: TIniFile;
  oList: TStrings;
  nCou: integer;

begin

  Result := 0;

  if aClassType = TCustomIniFile then begin
    oCustomIniFile := TCustomIniFile(aInstance);
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(oCustomIniFile.Create(aCallVar.Params[0])); exit; end;
    if aMethodName = 'WRITEINTEGER' then begin
      oCustomIniFile.WriteInteger(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READINTEGER' then begin
      Result := oCustomIniFile.ReadInteger(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'WRITEBOOL' then begin
      oCustomIniFile.WriteBool(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READBOOL' then begin
      Result := oCustomIniFile.ReadBool(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'WRITEDATE' then begin
      oCustomIniFile.WriteDate(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READDATE' then begin
      Result := oCustomIniFile.ReadDate(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'WRITEDATETIME' then begin
      oCustomIniFile.WriteDateTime(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READDATETIME' then begin
      Result := oCustomIniFile.ReadDateTime(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'WRITEFLOAT' then begin
      oCustomIniFile.WriteFloat(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READFLOAT' then begin
      Result := oCustomIniFile.ReadFloat(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'WRITETIME' then begin
      oCustomIniFile.WriteTime(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READTIME' then begin
      Result := oCustomIniFile.ReadTime(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'WRITEBINARYSTREAM' then begin
      oCustomIniFile.WriteBinaryStream(aCallVar.Params[0], aCallVar.Params[1], TStream(fmscrInteger(aCallVar.Params[2]))); exit; end;
    if aMethodName = 'READBINARYSTREAM' then begin
      Result := oCustomIniFile.ReadBinaryStream(aCallVar.Params[0], aCallVar.Params[1], TStream(fmscrInteger(aCallVar.Params[2]))); exit; end;
    if aMethodName = 'SECTIONEXISTS' then begin
      Result := oCustomIniFile.SectionExists(aCallVar.Params[0]); exit; end;
    if aMethodName = 'VALUEEXISTS' then begin
      Result := oCustomIniFile.ValueExists(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'WRITETSTRINGS' then begin
      WriteTStrings(oCustomIniFile, aCallVar.Params[0], TStrings(fmscrInteger(aCallVar.Params[1])), aCallVar.Params[2]); exit; end;
    if aMethodName = 'READTSTRINGS' then begin
      ReadTStrings(oCustomIniFile, aCallVar.Params[0], TStrings(fmscrInteger(aCallVar.Params[1])), aCallVar.Params[2]); exit; end;
  exit; end;

  if aClassType = TMemIniFile then begin
    oMemIniFile := TMemIniFile(aInstance);
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(oMemIniFile.Create(aCallVar.Params[0], False)); exit; end;
    if aMethodName = 'WRITESTRING' then begin
      oMemIniFile.WriteString(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READSTRING' then begin
      Result := oMemIniFile.ReadString(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'DELETEKEY' then begin
      oMemIniFile.DeleteKey(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'READSECTION' then begin
      oMemIniFile.ReadSection(aCallVar.Params[0], TStrings(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'READSECTIONS' then begin
      oMemIniFile.ReadSections(TStrings(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'READSECTIONVALUES' then begin
      oMemIniFile.ReadSectionValues(aCallVar.Params[0], TStrings(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'ERASESECTION' then begin
      oMemIniFile.EraseSection(aCallVar.Params[0]); exit; end;
    if aMethodName = 'READSECTIONVALUESEX' then begin
       oList := TStringList.Create;
       try
         oMemIniFile.ReadSectionValues(aCallVar.Params[0], oList);
         TStrings(fmscrInteger(aCallVar.Params[1])).Clear;
         for nCou := 0 to oList.Count - 1 do
           TStrings(fmscrInteger(aCallVar.Params[1])).Add(oList.Values[oList.Names[nCou]]);
       finally
         oList.Free;
       end;
    exit; end;
    if aMethodName = 'CLEAR' then begin
      oMemIniFile.Clear; exit; end;
    if aMethodName = 'GETSTRINGS' then begin
      oMemIniFile.GetStrings(TStrings(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'SETSTRINGS' then begin
      oMemIniFile.SetStrings(TStrings(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'SAVEINIFILETOSTREAM' then begin
      SaveIniFileToStream(oMemIniFile, TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'LOADINIFILEFROMSTREAM' then begin
      LoadIniFileFromStream(oMemIniFile, TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
  exit; end;

  if aClassType = TIniFile then begin
    oIniFile := TIniFile(aInstance);
    if aMethodName = 'WRITESTRING' then begin
      oIniFile.WriteString(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'READSTRING' then begin
      Result := oIniFile.ReadString(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
    if aMethodName = 'DELETEKEY' then begin
      oIniFile.DeleteKey(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'READSECTION' then begin
      oIniFile.ReadSection(aCallVar.Params[0], TStrings(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'READSECTIONS' then begin
      oIniFile.ReadSections(TStrings(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'READSECTIONVALUES' then begin
      oIniFile.ReadSectionValues(aCallVar.Params[0], TStrings(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'ERASESECTION' then begin
      oIniFile.EraseSection(aCallVar.Params[0]); exit; end;
    if aMethodName = 'READSECTIONVALUESEX' then begin
       oList := TStringList.Create;
       try
         oIniFile.ReadSectionValues(aCallVar.Params[0], oList);
         TStrings(fmscrInteger(aCallVar.Params[1])).Clear;
         for nCou := 0 to oList.Count - 1 do
           TStrings(fmscrInteger(aCallVar.Params[1])).Add(oList.Values[oList.Names[nCou]]);
       finally
         oList.Free;
       end;
      exit; end;
    if aMethodName = 'SAVEINIFILETOSTREAM' then begin
      SaveIniFileToStream(oIniFile, TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'LOADINIFILEFROMSTREAM' then begin
      LoadIniFileFromStream(oIniFile, TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
  exit; end;

end;

{$HINTS ON}

function TLibrary_IniFiles.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
begin
  Result := 0;

  if aClassType = TCustomIniFile then begin
    if aPropName = 'FILENAME' then begin
      Result := TIniFile(aInstance).FileName; exit; end;
  exit; end;
end;

procedure TLibrary_IniFiles.SaveIniFileToStream(oIniFile: TCustomIniFile; oStream: TStream);
var
  oStrings: TStrings;

begin

  if (not Assigned(oIniFile)) or (not Assigned(oStream)) then
    Exit;

  if not ((oIniFile is TIniFile) or (oIniFile is TMemIniFile)) then
    Exit;

  oStrings := TStringList.Create;
  try

    if oIniFile is TIniFile then
      oStrings.LoadFromFile(oIniFile.FileName)
    else
    if oIniFile is TMemIniFile then
      TMemIniFile(oIniFile).GetStrings(oStrings);

    oStrings.SaveToStream(oStream);

  finally
    oStrings.Free;
  end;

end;

procedure TLibrary_IniFiles.LoadIniFileFromStream(oIniFile: TCustomIniFile; oStream: TStream);
 var oStrings: TStrings;
begin
  if (not Assigned(oIniFile)) or (not Assigned(oStream)) then Exit;
  if not ((oIniFile is TIniFile) or (oIniFile is TMemIniFile)) then Exit;

  oStrings := TStringList.Create;
  try
    oStrings.LoadFromStream(oStream);

    if oIniFile is TIniFile then
      oStrings.SaveToFile(oIniFile.FileName)
    else
    if oIniFile is TMemIniFile then
      TMemIniFile(oIniFile).SetStrings(oStrings);

  finally
    oStrings.Free;
  end;
end;

procedure TLibrary_IniFiles.WriteTStrings(oIniFile: TCustomIniFile; const Section: string; Values: TStrings; IsClear: boolean = True);
 var  nCou: integer;
begin
  if IsClear then
    oIniFile.EraseSection(Section);

  for nCou := 0 to Values.Count - 1 do
    oIniFile.WriteString(Section, 'Items' + IntToStr(nCou), Values[nCou]);

  oIniFile.WriteInteger(Section, 'Count', Values.Count);
end;

procedure TLibrary_IniFiles.ReadTStrings(oIniFile: TCustomIniFile; const Section: string; Values: TStrings; IsClear: boolean = True);
 var nCou, nCount: integer;
begin
  nCount := oIniFile.ReadInteger(Section, 'Count', 0);

  if IsClear then
    Values.Clear;

  for nCou := 0 to nCount - 1 do
    Values.Add(oIniFile.ReadString(Section, 'Items' + IntToStr(nCou), ''));
end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_IniFiles);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_IniFiles);

end.
