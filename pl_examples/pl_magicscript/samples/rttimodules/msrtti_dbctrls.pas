{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_dbctrls;  

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, mscoreengine,
  msrtti_forms, msrtti_db,
  DB, DBCtrls, DBGrids;

type
  TmscrRTTILibrary_DBCtrls = class(TComponent);

implementation
type

  TLibrary_DBctrls = class(TmscrRTTILibrary)
  private
    function CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
  public
    constructor Create(AScript: TmscrScript); override;
  end;

constructor TLibrary_DBctrls.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);

  RegisterTypes(AScript);
  RegisterClasses(AScript);
end;

function TLibrary_DBctrls.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
  if aInstance<>NIL then
    Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;
             
procedure TLibrary_DBctrls.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_EnumSet('TButtonSet', 'nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh');
    Register_Enum('TColumnButtonStyle', 'cbsAuto, cbsEllipsis, cbsNone');
    Register_EnumSet('TDBGridOptions', 'dgEditing, dgAlwaysShowEditor, dgTitles,' +
                                       'dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect,' +
                                       'dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect');
  end;
end;

procedure TLibrary_DBctrls.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin

    Register_Class(TDBEdit, 'TWinControl');
    Register_Class(TDBText, 'TGraphicControl');

    with Register_Class(TDBCheckBox, 'TWinControl') do
      Register_Property('Checked', 'Boolean', ClassGetProp, nil);

    with Register_Class(TDBComboBox, 'TCustomComboBox') do
      Register_Property('Text', 'String', ClassGetProp, nil);

    Register_Class(TDBListBox, 'TCustomListBox');

    with Register_Class(TDBRadioGroup, 'TWinControl') do
    begin
      Register_Property('ItemIndex', 'Integer', ClassGetProp, nil);
      Register_Property('Value', 'String', ClassGetProp, nil);
    end;

    Register_Class(TDBMemo, 'TWinControl');
    Register_Class(TDBImage, 'TCustomControl');
    Register_Class(TDBNavigator, 'TWinControl');
    Register_Class(TColumnTitle, 'TPersistent');
    Register_Class(TColumn, 'TPersistent');

    with Register_Class(TDBGridColumns, 'TCollection') do
    begin
      Register_Method('function Add: TColumn', ClassCallMethod);
      Register_Method('procedure RebuildColumns', ClassCallMethod);
      Register_Method('procedure RestoreDefaults', ClassCallMethod);
      Register_DefaultProperty('Items', 'Integer', 'TColumn', ClassCallMethod, True);
    end;

    Register_Class(TDBGrid, 'TWinControl');

  end;
end;

function TLibrary_DBctrls.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;

  if aClassType = TDBGridColumns then begin
    if aMethodName = 'ADD' then begin
      Result := fmscrInteger(TDBGridColumns(aInstance).Add); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TDBGridColumns(aInstance).Items[aCallVar.Params[0]]); exit; end;
  exit; end;
end;

function TLibrary_DBctrls.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
begin
  Result := 0;

  if aClassType = TDBCheckBox then begin
    if aPropName = 'CHECKED' then begin
      Result := TDBCheckBox(aInstance).Checked; exit; end;
  exit; end;

  if aClassType = TDBComboBox then begin
    if aPropName = 'TEXT' then begin
      Result := TDBComboBox(aInstance).Text; exit; end;
  exit; end;

  if aClassType = TDBRadioGroup then begin
    if aPropName = 'ITEMINDEX' then begin
      Result := TDBRadioGroup(aInstance).ItemIndex; exit; end;
    if aPropName = 'VALUE' then begin
      Result := TDBRadioGroup(aInstance).Value; exit; end;
  exit; end;

end;

procedure TLibrary_DBctrls.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
begin
  // Nothing yet....
end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_DBctrls);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_DBctrls);

end.
