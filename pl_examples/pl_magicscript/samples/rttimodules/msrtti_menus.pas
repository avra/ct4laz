{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_menus;

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, Menus, mscoreengine, ImgList, Types, Variants;

type
  TmscrRTTILibrary_Menus = class(TComponent);

implementation
type
  TLibrary_Menus = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
  public
    constructor Create(AScript: TmscrScript); override;
  end;


constructor TLibrary_Menus.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);

  RegisterTypes(AScript);
  RegisterClasses(AScript);
end;

function TLibrary_Menus.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
  if aInstance<>NIL then
    Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;

procedure TLibrary_Menus.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_Type('TPopupAlignment', msvtInt);
  end;
end;

procedure TLibrary_Menus.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin

    Register_Class(TCustomImageList, 'TComponent');

    with Register_Class(TMenuItem, 'TComponent') do
    begin
      Register_Method('procedure Add(Item: TMenuItem)', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure Delete(Index: Integer)', ClassCallMethod);
      Register_Method('procedure Insert(Index: Integer; Item: TMenuItem)', ClassCallMethod);
      Register_Method('procedure Remove(Item: TMenuItem)', ClassCallMethod);
      Register_Method('function GetParentMenu: TMenu', ClassCallMethod);
      Register_Event('OnClick', TmscrNotifyEvent);
      Register_Property('Count', 'Integer', ClassGetProp);
      Register_DefaultProperty('Items', 'Integer', 'TMenuItem', ClassCallMethod, True);
    end;

    with Register_Class(TMenu, 'TComponent') do
      Register_IndexProperty('Items', 'Integer', 'TMenuItem', ClassCallMethod, True);

    with Register_Class(TMainMenu, 'TMenu') do
     begin

     end;

    with Register_Class(TPopupMenu, 'TMenu') do
    begin
      Register_Event('OnPopup', TmscrNotifyEvent);
      Register_Method('procedure Popup(X, Y: Extended)', ClassCallMethod);
      Register_Property('PopupComponent', 'TComponent', ClassGetProp, ClassSetProp);
      Register_Property('Images', 'TCustomImageList', ClassGetProp, ClassSetProp);
    end;

  end;
end;

function TLibrary_Menus.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  oMenuItem: TMenuItem;
begin
  Result := 0;

  if aClassType = TMenuItem then begin
    oMenuItem := TMenuItem(aInstance);

    if aMethodName = 'ADD' then begin
      oMenuItem.Add(TMenuItem(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'CLEAR' then begin
      oMenuItem.Clear; exit; end;
    if aMethodName = 'DELETE' then begin
      oMenuItem.Delete(aCallVar.Params[0]); exit; end;
    if aMethodName = 'INSERT' then begin
      oMenuItem.Insert(aCallVar.Params[0], TMenuItem(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'REMOVE' then begin
      oMenuItem.Remove(TMenuItem(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(oMenuItem.Items[aCallVar.Params[0]]); exit; end;
    if aMethodName = 'GETPARENTMENU' then begin
      Result := fmscrInteger(oMenuItem.GetParentMenu()); exit; end;
  exit; end;

  if aClassType = TMenu then begin
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TMenu(aInstance).Items[aCallVar.Params[0]]); exit; end;
  exit; end;

  if aClassType = TPopupMenu then begin
    if aMethodName = 'POPUP' then begin
      TPopupMenu(aInstance).Popup(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
  exit; end;

end;

function TLibrary_Menus.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
begin
  Result := 0;

  if aClassType = TMenuItem then begin
    if aPropName = 'COUNT' then begin
      Result := TMenuItem(aInstance).Count; exit; end;
  exit; end;

  if aClassType = TPopupMenu then begin
    if aPropName = 'POPUPCOMPONENT' then begin
      Result := fmscrInteger(TPopupMenu(aInstance).PopupComponent); exit; end;
    if aPropName = 'IMAGES' then begin
      Result := fmscrInteger(TPopupMenu(aInstance).Images); exit; end;
  exit; end;

end;

procedure TLibrary_Menus.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
begin
  if aClassType = TPopupMenu then begin
    if aPropName = 'IMAGES' then begin
      TPopupMenu(aInstance).Images := TCustomImageList(fmscrInteger(aValue)); exit; end;
    if aPropName = 'POPUPCOMPONENT' then begin
      TPopupMenu(aInstance).PopupComponent := TComponent(fmscrInteger(aValue)); exit; end;
  exit; end;
end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_Menus);


finalization
  MSCR_RTTILibraries.Remove(TLibrary_Menus);

end.








