{***************************************************************************
               Copyright (c) PilotLogic Software House
                       All rights reserved
 
   MagicScript Import File for Library GLScene
   Build from : GLMaterial.pas file
   Generated by MagicScript LibraryBuilder 7.0
   at : 6/10/2019 20:35:02
   This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
 ***************************************************************************}
 
Unit msrtti_GLMaterial;
 
{$MODE DELPHI}{$H+}
 
Interface
Uses
 Classes,
 GLApplicationFileIO,
 GLBaseClasses,
 GLColor,
 GLContext,
 GLCoordinates,
 GLCrossPlatform,
 GLGraphics,
 GLPersistentClasses,
 GLRenderContextInfo,
 GLSLog,
 GLState,
 GLStrings,
 GLTexture,
 GLTextureFormat,
 GLUtils,
 GLVectorGeometry,
 GLVectorTypes,
 Graphics,
 OpenGLTokens,
 SysUtils,
 Types,
 XOpenGL,
 GLMaterial,
 mscoreengine;

Type
 TmscrRTTILibrary_GLMaterial=class(TComponent);

Implementation

Type
 TLibrary_GLMaterial=class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: string): variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: string; Value: variant);
  public
    constructor Create(AScript: TmscrScript); override;
  end;
 
constructor TLibrary_GLMaterial.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);
  
  RegisterTypes(AScript);
  RegisterClasses(AScript);
end;
 
function TLibrary_GLMaterial.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
  if aInstance<>NIL then
    Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;
 
procedure TLibrary_GLMaterial.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
     Register_Enum('TFaceCulling','fcBufferDefaultfcCullfcNoCull');
     Register_Enum('TGLBlendingMode','bmOpaquebmTransparency, bmAdditive, bmAlphaTest50, bmAlphaTest100, bmModulatebmCustom');
     Register_Type('TGLLibMaterialName',msvtString);
     Register_Enum('TGLShaderFailedInitAction','fiaSilentDisablefiaRaiseStandardException, fiaRaiseHandledExceptionfiaReRaiseException');
     Register_Enum('TGLShaderStyle','ssHighLevelssLowLevelssReplace');
     Register_Enum('TMaterialOption','moIgnoreFogmoNoLighting');
     Register_EnumSet('TMaterialOptions','moIgnoreFogmoNoLighting');

  end;
end;
 
procedure TLibrary_GLMaterial.RegisterClasses(AScript: TmscrScript);
begin
  with AScript do
  begin

    with Register_Class(TGLAbstractLibMaterial,'TCollectionItem') do
    begin
    end;
    with Register_Class(TGLAbstractLibMaterials,'TOwnedCollection') do
    begin
    end;
    with Register_Class(TGLAbstractMaterialLibrary,'TGLCadenceAbleComponent') do
    begin
    end;
    with Register_Class(TGLBlendingParameters,'TGLUpdateAbleObject') do
    begin
    end;
    with Register_Class(TGLDepthProperties,'TGLUpdateAbleObject') do
    begin
    end;
    with Register_Class(TGLFaceProperties,'TGLUpdateAbleObject') do
    begin
    end;
    with Register_Class(TGLLibMaterial,'TGLAbstractLibMaterial') do
    begin
    end;
    with Register_Class(TGLLibMaterials,'TGLAbstractLibMaterials') do
    begin
    end;
    with Register_Class(TGLMaterial,'TGLUpdateAbleObject') do
    begin
    end;
    with Register_Class(TGLMaterialLibrary,'TGLAbstractMaterialLibrary') do
    begin
    end;
    with Register_Class(TGLShader,'TGLUpdateAbleComponent') do
    begin
    end;

  end;
end;
 
function TLibrary_GLMaterial.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
 
  if aClassType=TGLAbstractLibMaterial then begin
   exit; end;
 
  if aClassType=TGLAbstractLibMaterials then begin
   exit; end;
 
  if aClassType=TGLAbstractMaterialLibrary then begin
   exit; end;
 
  if aClassType=TGLBlendingParameters then begin
   exit; end;
 
  if aClassType=TGLDepthProperties then begin
   exit; end;
 
  if aClassType=TGLFaceProperties then begin
   exit; end;
 
  if aClassType=TGLLibMaterial then begin
   exit; end;
 
  if aClassType=TGLLibMaterials then begin
   exit; end;
 
  if aClassType=TGLMaterial then begin
   exit; end;
 
  if aClassType=TGLMaterialLibrary then begin
   exit; end;
 
  if aClassType=TGLShader then begin
   exit; end;
 

end;
 
function TLibrary_GLMaterial.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: string): variant;
begin
  Result := 0;
 
  if aClassType=TGLAbstractLibMaterial then begin
  exit; end;
 
  if aClassType=TGLBlendingParameters then begin
  exit; end;
 
  if aClassType=TGLDepthProperties then begin
  exit; end;
 
  if aClassType=TGLFaceProperties then begin
  exit; end;
 
  if aClassType=TGLLibMaterial then begin
  exit; end;
 
  if aClassType=TGLLibMaterials then begin
  exit; end;
 
  if aClassType=TGLMaterial then begin
  exit; end;
 
  if aClassType=TGLMaterialLibrary then begin
  exit; end;
 
  if aClassType=TGLShader then begin
  exit; end;
 

end;
 
procedure TLibrary_GLMaterial.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: string; Value: variant);
begin
 
  if aClassType=TGLAbstractLibMaterial then begin
  exit; end;
 
  if aClassType=TGLBlendingParameters then begin
  exit; end;
 
  if aClassType=TGLDepthProperties then begin
  exit; end;
 
  if aClassType=TGLFaceProperties then begin
  exit; end;
 
  if aClassType=TGLLibMaterial then begin
  exit; end;
 
  if aClassType=TGLLibMaterials then begin
  exit; end;
 
  if aClassType=TGLMaterial then begin
  exit; end;
 
  if aClassType=TGLMaterialLibrary then begin
  exit; end;
 
  if aClassType=TGLShader then begin
  exit; end;
 

end;
 
 
//===============================================
Initialization
  MSCR_RTTILibraries.Add(TLibrary_GLMaterial);
Finalization
  MSCR_RTTILibraries.Remove(TLibrary_GLMaterial);
End.
