{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_db;  

{$MODE DELPHI}{$H+}

interface

uses
  SysUtils, Classes, mscoreengine, msrtti_classes, DB;

type
  TmscrRTTILibrary_DB = class(TComponent);

  TmscrDatasetNotifyEvent = class(TmscrCustomEvent)
  public
    procedure DoEvent(Dataset: TDataset);
    function  GetMethod: Pointer; override;
  end;

  TmscrFilterRecordEvent = class(TmscrCustomEvent)
  public
    procedure DoEvent(DataSet: TDataSet; var Accept: boolean);
    function  GetMethod: Pointer; override;
  end;

  TmscrFieldGetTextEvent = class(TmscrCustomEvent)
  public
    procedure DoEvent(Sender: TField; var Text: string; DisplayText: boolean);
    function  GetMethod: Pointer; override;
  end;

implementation

procedure TmscrDatasetNotifyEvent.DoEvent(Dataset: TDataset);
begin
  CallHandler([Dataset]);
end;

function TmscrDatasetNotifyEvent.GetMethod: Pointer;
begin
  Result := @TmscrDatasetNotifyEvent.DoEvent;
end;

procedure TmscrFilterRecordEvent.DoEvent(DataSet: TDataSet; var Accept: boolean);
begin
  CallHandler([DataSet, Accept]);
  Accept := Handler.Params[1].Value;
end;

function TmscrFilterRecordEvent.GetMethod: Pointer;
begin
  Result := @TmscrFilterRecordEvent.DoEvent;
end;

procedure TmscrFieldGetTextEvent.DoEvent(Sender: TField; var Text: string; DisplayText: boolean);
begin
  CallHandler([Sender, Text, DisplayText]);
  Text := Handler.Params[1].Value;
end;

function TmscrFieldGetTextEvent.GetMethod: Pointer;
begin
  Result := @TmscrFieldGetTextEvent.DoEvent;
end;

//==========================

type
  TLibrary_DB = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
  public
    constructor Create(AScript: TmscrScript); override;
  end;


constructor TLibrary_DB.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);

  RegisterTypes(AScript);
  RegisterClasses(AScript);
end;

function TLibrary_DB.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
  if aInstance<>NIL then
    Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;

procedure TLibrary_DB.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_Enum('TFieldType', 'ftUnknown, ftString, ftSmallint, ftInteger, ftWord,' +
                  'ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime,' +
                  'ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,' +
                  'ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,' +
                  'ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,' +
                  'ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp, ftFMTBcd');

    Register_Enum('TBlobStreamMode', 'bmRead, bmWrite, bmReadWrite');
    Register_EnumSet('TLocateOptions', 'loCaseInsensitive, loPartialKey');
    Register_EnumSet('TFilterOptions', 'foCaseInsensitive, foNoPartialCompare');
    Register_Enum('TParamType', 'ptUnknown, ptInput, ptOutput, ptInputOutput, ptResult');
  end;
end;

procedure TLibrary_DB.RegisterClasses(AScript: TmscrScript);
begin
  inherited Create(AScript);
  with AScript do
  begin

    with Register_Class(TField, 'TComponent') do
    begin
      Register_Property('AsBoolean', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('AsCurrency', 'Currency', ClassGetProp, ClassSetProp);
      Register_Property('AsDateTime', 'TDateTime', ClassGetProp, ClassSetProp);
      Register_Property('AsFloat', 'Double', ClassGetProp, ClassSetProp);
      Register_Property('AsInteger', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('AsString', 'String', ClassGetProp, ClassSetProp);
      Register_Property('AsVariant', 'Variant', ClassGetProp, ClassSetProp);
      Register_Property('DataType', 'TFieldType', ClassGetProp, nil);
      Register_Property('DisplayName', 'String', ClassGetProp, nil);
      Register_Property('DisplayText', 'String', ClassGetProp, nil);
      Register_Property('IsNull', 'Boolean', ClassGetProp, nil);
      Register_Property('Size', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('Value', 'Variant', ClassGetProp, ClassSetProp);
      Register_Property('OldValue', 'Variant', ClassGetProp, nil);
      Register_Event('OnGetText', TmscrFieldGetTextEvent);
    end;

    with Register_Class(TFields, 'TObject') do
      Register_DefaultProperty('Fields', 'Integer', 'TField', ClassCallMethod, True);

    Register_Class(TStringField, 'TField');
    Register_Class(TNumericField, 'TField');
    Register_Class(TIntegerField, 'TNumericField');
    Register_Class(TSmallIntField, 'TIntegerField');
    Register_Class(TWordField, 'TIntegerField');
    Register_Class(TAutoIncField, 'TIntegerField');
    Register_Class(TFloatField, 'TNumericField');
    Register_Class(TCurrencyField, 'TFloatField');
    Register_Class(TBooleanField, 'TField');
    Register_Class(TDateTimeField, 'TField');
    Register_Class(TDateField, 'TDateTimeField');
    Register_Class(TTimeField, 'TDateTimeField');
    Register_Class(TBinaryField, 'TField');
    Register_Class(TBytesField, 'TBinaryField');
    Register_Class(TVarBytesField, 'TBinaryField');
    Register_Class(TBCDField, 'TNumericField');

    with Register_Class(TBlobField, 'TField') do
    begin
      Register_Method('procedure LoadFromFile(const FileName: String)', ClassCallMethod);
      Register_Method('procedure LoadFromStream(Stream: TStream)', ClassCallMethod);
      Register_Method('procedure SaveToFile(const FileName: String)', ClassCallMethod);
      Register_Method('procedure SaveToStream(Stream: TStream)', ClassCallMethod);
    end;

    Register_Class(TMemoField, 'TBlobField');
    Register_Class(TGraphicField, 'TBlobField');
    Register_Class(TFieldDef, 'TPersistent');

    with Register_Class(TFieldDefs, 'TObject') do
    begin
      Register_Method('function AddFieldDef: TFieldDef', ClassCallMethod);
      Register_Method('function Find(const Name: string): TFieldDef', ClassCallMethod);
      Register_Method('procedure Add(const Name: string; DataType: TFieldType; Size: Word; Required: Boolean)', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure Update', ClassCallMethod);
      Register_DefaultProperty('Items', 'Integer', 'TFieldDef', ClassCallMethod, True);
    end;

    Register_Class(TDataSource, 'TComponent');
    Register_Type('TBookmark', msvtVariant);

    with Register_Class(TDataSet, 'TComponent') do
    begin
      Register_Method('procedure Open', ClassCallMethod);
      Register_Method('procedure Close', ClassCallMethod);
      Register_Method('procedure First', ClassCallMethod);
      Register_Method('procedure Last', ClassCallMethod);
      Register_Method('procedure Next', ClassCallMethod);
      Register_Method('procedure Prior', ClassCallMethod);
      Register_Method('procedure Cancel', ClassCallMethod);
      Register_Method('procedure Delete', ClassCallMethod);
      Register_Method('procedure Post', ClassCallMethod);
      Register_Method('procedure Append', ClassCallMethod);
      Register_Method('procedure Insert', ClassCallMethod);
      Register_Method('procedure Edit', ClassCallMethod);

      Register_Method('function FieldByName(const FieldName: string): TField', ClassCallMethod);
      Register_Method('procedure GetFieldNames(List: TStrings)', ClassCallMethod);
      Register_Method('function FindFirst: Boolean', ClassCallMethod);
      Register_Method('function FindLast: Boolean', ClassCallMethod);
      Register_Method('function FindNext: Boolean', ClassCallMethod);
      Register_Method('function FindPrior: Boolean', ClassCallMethod);
      Register_Method('procedure FreeBookmark(Bookmark: TBookmark)', ClassCallMethod);
      Register_Method('function GetBookmark: TBookmark', ClassCallMethod);
      Register_Method('procedure GotoBookmark(Bookmark: TBookmark)', ClassCallMethod);
      Register_Method('function Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean', ClassCallMethod);
      Register_Method('function IsEmpty: Boolean', ClassCallMethod);
      Register_Method('procedure EnableControls', ClassCallMethod);
      Register_Method('procedure DisableControls', ClassCallMethod);

      Register_Property('Bof', 'Boolean', ClassGetProp, nil);
      Register_Property('Eof', 'Boolean', ClassGetProp, nil);
      Register_Property('FieldCount', 'Integer', ClassGetProp, nil);
      Register_Property('FieldDefs', 'TFieldDefs', ClassGetProp, nil);
      Register_Property('Fields', 'TFields', ClassGetProp, nil);
      Register_Property('Filter', 'string', ClassGetProp, ClassSetProp);
      Register_Property('Filtered', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('FilterOptions', 'TFilterOptions', ClassGetProp, ClassSetProp);
      Register_Property('Active', 'Boolean', ClassGetProp, ClassSetProp);

      Register_Event('BeforeOpen', TmscrDatasetNotifyEvent);
      Register_Event('AfterOpen', TmscrDatasetNotifyEvent);
      Register_Event('BeforeClose', TmscrDatasetNotifyEvent);
      Register_Event('AfterClose', TmscrDatasetNotifyEvent);
      Register_Event('BeforeInsert', TmscrDatasetNotifyEvent);
      Register_Event('AfterInsert', TmscrDatasetNotifyEvent);
      Register_Event('BeforeEdit', TmscrDatasetNotifyEvent);
      Register_Event('AfterEdit', TmscrDatasetNotifyEvent);
      Register_Event('BeforePost', TmscrDatasetNotifyEvent);
      Register_Event('AfterPost', TmscrDatasetNotifyEvent);
      Register_Event('BeforeCancel', TmscrDatasetNotifyEvent);
      Register_Event('AfterCancel', TmscrDatasetNotifyEvent);
      Register_Event('BeforeDelete', TmscrDatasetNotifyEvent);
      Register_Event('AfterDelete', TmscrDatasetNotifyEvent);
      Register_Event('BeforeScroll', TmscrDatasetNotifyEvent);
      Register_Event('AfterScroll', TmscrDatasetNotifyEvent);
      Register_Event('OnCalcFields', TmscrDatasetNotifyEvent);
      Register_Event('OnFilterRecord', TmscrFilterRecordEvent);
      Register_Event('OnNewRecord', TmscrDatasetNotifyEvent);
    end;

    with Register_Class(TParam, 'TPersistent') do
    begin
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Property('AsBoolean', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('AsCurrency', 'Currency', ClassGetProp, ClassSetProp);
      Register_Property('AsDateTime', 'TDateTime', ClassGetProp, ClassSetProp);
      Register_Property('AsFloat', 'Double', ClassGetProp, ClassSetProp);
      Register_Property('AsInteger', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('AsDate', 'TDate', ClassGetProp, ClassSetProp);
      Register_Property('AsTime', 'TTime', ClassGetProp, ClassSetProp);
      Register_Property('AsString', 'String', ClassGetProp, ClassSetProp);
      Register_Property('Bound', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('IsNull', 'Boolean', ClassGetProp, nil);
      Register_Property('Text', 'String', ClassGetProp, ClassSetProp);
    end;

    with Register_Class(TParams, 'TPersistent') do
    begin
      Register_Method('function ParamByName(const Value: string): TParam', ClassCallMethod);
      Register_Method('function FindParam(const Value: string): TParam', ClassCallMethod);
      Register_DefaultProperty('Items', 'Integer', 'TParam', ClassCallMethod, True);
    end;

  end;
end;

function TLibrary_DB.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  _TDataSet: TDataSet;

  function IntToLocateOptions(i: integer): TLocateOptions;
  begin
    Result := [];
    if (i and 1) <> 0 then
      Result := Result + [loCaseInsensitive];
    if (i and 2) <> 0 then
      Result := Result + [loPartialKey];
  end;

begin
  Result := 0;

  if aClassType = TFields then begin
    if aMethodName = 'FIELDS.GET'then begin
      Result := fmscrInteger(TFields(aInstance)[aCallVar.Params[0]]); exit; end;
  exit; end;
  
  if aClassType = TFieldDefs then begin
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TFieldDefs(aInstance)[aCallVar.Params[0]]); exit; end;
    if aMethodName = 'ADD' then begin
      TFieldDefs(aInstance).Add(aCallVar.Params[0], TFieldType(aCallVar.Params[1]), aCallVar.Params[2], aCallVar.Params[3]); exit; end;
    if aMethodName = 'ADDFIELDDEF' then begin
      Result := fmscrInteger(TFieldDefs(aInstance).AddFieldDef); exit; end;
    if aMethodName = 'CLEAR' then begin
      TFieldDefs(aInstance).Clear; exit; end;
    if aMethodName = 'FIND' then begin
      Result := fmscrInteger(TFieldDefs(aInstance).Find(aCallVar.Params[0])); exit; end;
    if aMethodName = 'UPDATE' then begin
      TFieldDefs(aInstance).Update; exit; end;
  exit; end;
  
  if aClassType = TBlobField then begin
    if aMethodName = 'LOADFROMFILE' then begin
      TBlobField(aInstance).LoadFromFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'LOADFROMSTREAM' then begin
      TBlobField(aInstance).LoadFromStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'SAVETOFILE' then begin
      TBlobField(aInstance).SaveToFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'SAVETOSTREAM' then begin
      TBlobField(aInstance).SaveToStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
  exit; end;
  
  if aClassType = TDataSet then begin
    _TDataSet := TDataSet(aInstance);
    if aMethodName = 'OPEN' then begin
      _TDataSet.Open; exit; end;
    if aMethodName = 'CLOSE' then begin
      _TDataSet.Close; exit; end;
    if aMethodName = 'FIRST' then begin
      _TDataSet.First; exit; end;
    if aMethodName = 'LAST' then begin
      _TDataSet.Last; exit; end;
    if aMethodName = 'NEXT' then begin
      _TDataSet.Next; exit; end;
    if aMethodName = 'PRIOR' then begin
      _TDataSet.Prior; exit; end;
    if aMethodName = 'CANCEL' then begin
      _TDataSet.Cancel; exit; end;
    if aMethodName = 'DELETE' then begin
      _TDataSet.Delete; exit; end;
    if aMethodName = 'POST' then begin
      _TDataSet.Post; exit; end;
    if aMethodName = 'APPEND' then begin
      _TDataSet.Append; exit; end;
    if aMethodName = 'INSERT' then begin
      _TDataSet.Insert; exit; end;
    if aMethodName = 'EDIT' then begin
      _TDataSet.Edit; exit; end;
    if aMethodName = 'FIELDBYNAME' then begin
      Result := fmscrInteger(_TDataSet.FieldByName(aCallVar.Params[0])); exit; end;
    if aMethodName = 'GETFIELDNAMES' then begin
      _TDataSet.GetFieldNames(TStrings(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'FINDFIRST' then begin
      Result := _TDataSet.FindFirst; exit; end;
    if aMethodName = 'FINDLAST' then begin
      Result := _TDataSet.FindLast; exit; end;
    if aMethodName = 'FINDNEXT' then begin
      Result := _TDataSet.FindNext; exit; end;
    if aMethodName = 'FINDPRIOR' then begin
      Result := _TDataSet.FindPrior; exit; end;
    if aMethodName = 'FREEBOOKMARK' then begin
      _TDataSet.FreeBookmark(TBookMark(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'GETBOOKMARK' then begin
      Result := fmscrInteger(_TDataSet.GetBookmark); exit; end;
    if aMethodName = 'GOTOBOOKMARK' then begin
      _TDataSet.GotoBookmark(TBookMark(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'LOCATE' then begin
      Result := _TDataSet.Locate(aCallVar.Params[0], aCallVar.Params[1], IntToLocateOptions(aCallVar.Params[2])); exit; end;
    if aMethodName = 'ISEMPTY' then begin
      Result := _TDataSet.IsEmpty; exit; end;
    if aMethodName = 'ENABLECONTROLS' then begin
      _TDataSet.EnableControls; exit; end;
    if aMethodName = 'DISABLECONTROLS' then begin
      _TDataSet.DisableControls; exit; end;
  exit; end;
  
  if aClassType = TParam then begin
    if aMethodName = 'CLEAR' then begin
      TParam(aInstance).Clear; exit; end;
  exit; end;
  
  if aClassType = TParams then begin
    if aMethodName = 'PARAMBYNAME' then begin
      Result := fmscrInteger(TParams(aInstance).ParamByName(aCallVar.Params[0])); exit; end;
    if aMethodName = 'FINDPARAM' then begin
      Result := fmscrInteger(TParams(aInstance).FindParam(aCallVar.Params[0])); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TParams(aInstance)[aCallVar.Params[0]]); exit; end;
  exit; end;
end;

function TLibrary_DB.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
var
  _TField: TField;
  _TParam: TParam;
  _TDataSet: TDataSet;

  function FilterOptionsToInt(f: TFilterOptions): integer;
  begin
    Result := 0;
    if foCaseInsensitive in f then
      Result := Result or 1;
    if foNoPartialCompare in f then
      Result := Result or 2;
  end;

begin
  Result := 0;

  if aClassType = TField then begin
    _TField := TField(aInstance);
    if aPropName = 'ASBOOLEAN' then begin
      Result := _TField.AsBoolean; exit; end;
    if aPropName = 'ASCURRENCY' then begin
      Result := _TField.AsCurrency; exit; end;
    if aPropName = 'ASDATETIME' then begin
      Result := _TField.AsDateTime; exit; end;
    if aPropName = 'ASFLOAT' then begin
      Result := _TField.AsFloat; exit; end;
    if aPropName = 'ASINTEGER' then begin
      Result := _TField.AsInteger; exit; end;
    if aPropName = 'ASSTRING' then begin
      Result := _TField.AsString; exit; end;
    if aPropName = 'ASVARIANT' then begin
      Result := _TField.AsVariant; exit; end;
    if aPropName = 'DATATYPE' then begin
      Result := _TField.DataType; exit; end;
    if aPropName = 'DISPLAYNAME' then begin
      Result := _TField.DisplayName; exit; end;
    if aPropName = 'DISPLAYTEXT' then begin
      Result := _TField.DisplayText; exit; end;
    if aPropName = 'ISNULL' then begin
      Result := _TField.IsNull; exit; end;
    if aPropName = 'SIZE' then begin
      Result := _TField.Size; exit; end;
    if aPropName = 'VALUE' then begin
      Result := _TField.Value; exit; end;
    if aPropName = 'OLDVALUE' then begin
      Result := _TField.OldValue; exit; end;
  exit; end;
  
  if aClassType = TDataSet then begin
    _TDataSet := TDataSet(aInstance);
    if aPropName = 'BOF' then begin
      Result := _TDataSet.Bof; exit; end;
    if aPropName = 'EOF' then begin
      Result := _TDataSet.EOF; exit; end;
    if aPropName = 'FIELDCOUNT' then begin
      Result := _TDataSet.FieldCount; exit; end;
    if aPropName = 'FIELDDEFS' then begin
      Result := fmscrInteger(_TDataSet.FieldDefs); exit; end;
    if aPropName = 'FIELDS' then begin
      Result := fmscrInteger(_TDataSet.Fields); exit; end;
    if aPropName = 'FILTER' then begin
      Result := _TDataSet.Filter; exit; end;
    if aPropName = 'FILTERED' then begin
      Result := _TDataSet.Filtered; exit; end;
    if aPropName = 'FILTEROPTIONS' then begin
      Result := FilterOptionsToInt(_TDataSet.FilterOptions); exit; end;
    if aPropName = 'ACTIVE' then begin
      Result := _TDataSet.Active; exit; end;
  exit; end;
  
  if aClassType = TParam then begin
    _TParam := TParam(aInstance);
    if aPropName = 'BOUND' then begin
      Result := _TParam.Bound; exit; end;
    if aPropName = 'ISNULL' then begin
      Result := _TParam.IsNull; exit; end;
    if aPropName = 'TEXT' then begin
      Result := _TParam.Text; exit; end;
    if aPropName = 'ASBOOLEAN' then begin
      Result := _TParam.AsBoolean; exit; end;
    if aPropName = 'ASCURRENCY' then begin
      Result := _TParam.AsCurrency; exit; end;
    if aPropName = 'ASDATETIME' then begin
      Result := _TParam.AsDateTime; exit; end;
    if aPropName = 'ASFLOAT' then begin
      Result := _TParam.AsFloat; exit; end;
    if aPropName = 'ASINTEGER' then begin
      Result := _TParam.AsInteger; exit; end;
    if aPropName = 'ASDATE' then begin
      Result := _TParam.AsDate; exit; end;
    if aPropName = 'ASTIME' then begin
      Result := _TParam.AsTime; exit; end;
    if aPropName = 'ASSTRING' then begin
      Result := _TParam.AsString; exit; end;
  exit; end;
end;

procedure TLibrary_DB.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
var
  _TField: TField;
  _TParam: TParam;
  _TDataSet: TDataSet;

  function IntToFilterOptions(i: integer): TFilterOptions;
  begin
    Result := [];
    if (i and 1) <> 0 then
      Result := Result + [foCaseInsensitive];
    if (i and 2) <> 0 then
      Result := Result + [foNoPartialCompare];
  end;

begin
  if aClassType = TField then begin
    _TField := TField(aInstance);
    if aPropName = 'ASBOOLEAN' then begin
      _TField.AsBoolean := aValue; exit; end;
    if aPropName = 'ASCURRENCY' then begin
      _TField.AsCurrency := aValue; exit; end;
    if aPropName = 'ASDATETIME' then begin
      _TField.AsDateTime := aValue; exit; end;
    if aPropName = 'ASFLOAT' then begin
      _TField.AsFloat := aValue; exit; end;
    if aPropName = 'ASINTEGER' then begin
      _TField.AsInteger := aValue; exit; end;
    if aPropName = 'ASSTRING' then begin
      _TField.AsString := aValue; exit; end;
    if aPropName = 'ASVARIANT' then begin
      _TField.AsVariant := aValue; exit; end;
    if aPropName = 'VALUE' then begin
      _TField.Value := aValue; exit; end;
    if aPropName = 'SIZE' then begin
      _TField.Size := aValue; exit; end;
  exit; end;
  
  if aClassType = TDataSet then
  begin
    _TDataSet := TDataSet(aInstance);
    if aPropName = 'FILTER' then begin
      _TDataSet.Filter := aValue; exit; end;
    if aPropName = 'FILTERED' then begin
      _TDataSet.Filtered := aValue; exit; end;
    if aPropName = 'FILTEROPTIONS' then begin
      _TDataSet.FilterOptions := IntToFilterOptions(aValue); exit; end;
    if aPropName = 'ACTIVE' then begin
      _TDataSet.Active := aValue; exit; end;
  exit; end;
  
  if aClassType = TParam then
  begin
    _TParam := TParam(aInstance);
    if aPropName = 'ASBOOLEAN' then begin
      _TParam.AsBoolean := aValue; exit; end;
    if aPropName = 'ASCURRENCY' then begin
      _TParam.AsCurrency := aValue; exit; end;
    if aPropName = 'ASDATETIME' then begin
      _TParam.AsDateTime := aValue; exit; end;
    if aPropName = 'ASFLOAT' then begin
      _TParam.AsFloat := aValue; exit; end;
    if aPropName = 'ASINTEGER' then begin
      _TParam.AsInteger := aValue; exit; end;
    if aPropName = 'ASDATE' then begin
      _TParam.AsDate := aValue; exit; end;
    if aPropName = 'ASTIME' then begin
      _TParam.AsTime := aValue; exit; end;
    if aPropName = 'ASSTRING' then begin
      _TParam.AsString := aValue; exit; end;
    if aPropName = 'BOUND' then begin
      _TParam.Bound := aValue; exit; end;
    if aPropName = 'TEXT' then begin
      _TParam.Text := aValue; exit; end;
  exit; end;

end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_DB);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_DB);

end.
