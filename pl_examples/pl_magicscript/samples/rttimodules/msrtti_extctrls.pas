{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_extctrls;

{$MODE DELPHI}{$H+}

interface


uses SysUtils, Classes, mscoreengine, msrtti_forms,
  ExtCtrls, Buttons, CheckLst, ComCtrls;

type
  TmscrRTTILibrary_ExtCtrls = class(TComponent);

implementation
type
  TLibrary_Extctrls = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant; 
    procedure RegisterTypes(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
  public
    constructor Create(AScript: TmscrScript); override;
  end;


constructor TLibrary_Extctrls.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);

  RegisterTypes(AScript);
  RegisterClasses(AScript);
end;

function TLibrary_Extctrls.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;
  if aInstance<>NIL then
    Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;


procedure TLibrary_Extctrls.RegisterTypes(AScript: TmscrScript);
begin
  with AScript do
  begin
    Register_Enum('TShapeType', 'stRectangle, stSquare, stRoundRect, stRoundSquare, stEllipse, stCircle');
    Register_Enum('TBevelStyle', 'bsLowered, bsRaised');
    Register_Enum('TBevelShape', 'bsBox, bsFrame, bsTopLine, bsBottomLine, bsLeftLine, bsRightLine, bsSpacer');
    Register_Enum('TResizeStyle', 'rsNone, rsLine, rsUpdate, rsPattern');
    Register_Enum('TButtonLayout', 'blGlyphLeft, blGlyphRight, blGlyphTop, blGlyphBottom');
    Register_Enum('TButtonState', 'bsUp, bsDisabled, bsDown, bsExclusive');
    Register_Enum('TButtonStyle', 'bsAutoDetect, bsWin31, bsNew');
    Register_Enum('TBitBtnKind', 'bkCustom, bkOK, bkCancel, bkHelp, bkYes, bkNo, bkClose, bkAbort, bkRetry, bkIgnore, bkAll');
    Register_Type('TNumGlyphs', msvtInt);
    Register_Enum('TTabPosition', 'tpTop, tpBottom, tpLeft, tpRight');
    Register_Enum('TTabStyle', 'tsTabs, tsButtons, tsFlatButtons');
    Register_Enum('TStatusPanelStyle', 'psText, psOwnerDraw');
    Register_Enum('TStatusPanelBevel', 'pbNone, pbLowered, pbRaised');
    Register_Enum('TSortType', 'stNone, stData, stText, stBoth');
    Register_Enum('TTrackBarOrientation', 'trHorizontal, trVertical');
    Register_Enum('TTickMark', 'tmBottomRight, tmTopLeft, tmBoth');
    Register_Enum('TTickStyle', 'tsNone, tsAuto, tsManual');
    Register_Enum('TProgressBarOrientation', 'pbHorizontal, pbVertical');
    Register_Enum('TIconArrangement', 'iaTop, iaLeft');
    Register_Enum('TListArrangement', 'arAlignBottom, arAlignLeft, arAlignRight, arAlignTop, arDefault, arSnapToGrid');
    Register_Enum('TViewStyle', 'vsIcon, vsSmallIcon, vsList, vsReport');
    Register_Enum('TToolButtonStyle', 'tbsButton, tbsCheck, tbsDropDown, tbsSeparator, tbsDivider');
    Register_Enum('TDateTimeKind', 'dtkDate, dtkTime');
    Register_Enum('TDTDateMode', 'dmComboBox, dmUpDown');
    Register_Enum('TDTDateFormat', 'dfShort, dfLong');
    Register_Enum('TDTCalAlignment', 'dtaLeft, dtaRight');
    Register_Enum('TCalDayOfWeek', 'dowMonday, dowTuesday, dowWednesday, dowThursday, dowFriday, dowSaturday, dowSunday, dowLocaleDefault');
  end;
end;

procedure TLibrary_Extctrls.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin
    Register_Class(TShape, 'TGraphicControl');

    with Register_Class(TPaintBox, 'TGraphicControl') do
      Register_Event('OnPaint', TmscrNotifyEvent);

    Register_Class(TImage, 'TGraphicControl');
    Register_Class(TBevel, 'TGraphicControl');

    with Register_Class(TTimer, 'TComponent') do
      Register_Event('OnTimer', TmscrNotifyEvent);

    Register_Class(TPanel, 'TCustomControl');
    Register_Class(TSplitter, 'TGraphicControl');
    Register_Class(TBitBtn, 'TButton');
    Register_Class(TSpeedButton, 'TGraphicControl');

    with Register_Class(TCheckListBox, 'TCustomListBox') do
      Register_IndexProperty('Checked', 'Integer', 'Boolean', ClassCallMethod);

    Register_Class(TTabControl, 'TWinControl');

    with Register_Class(TTabSheet, 'TWinControl') do
      Register_Property('PageControl', 'TPageControl', ClassGetProp, ClassSetProp);

    with Register_Class(TPageControl, 'TWinControl') do
    begin
      Register_Method('procedure SelectNextPage(GoForward: Boolean)', ClassCallMethod);
      Register_Property('PageCount', 'Integer', ClassGetProp, nil);
      Register_IndexProperty('Pages', 'Integer', 'TTabSheet', ClassCallMethod, True);
    end;

    Register_Class(TStatusPanel, 'TPersistent');

    with Register_Class(TStatusPanels, 'TPersistent') do
    begin
      Register_Method('function Add: TStatusPanel', ClassCallMethod);
      Register_IndexProperty('Items', 'Integer', 'TStatusPanel', ClassCallMethod, True);
    end;

    Register_Class(TStatusBar, 'TWinControl');

    with Register_Class(TTreeNode, 'TPersistent') do
    begin
      Register_Method('procedure Delete', ClassCallMethod);
      Register_Method('function EditText: Boolean', ClassCallMethod);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_Property('Data', 'Pointer', ClassGetProp, ClassSetProp);
      Register_Property('ImageIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('SelectedIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('StateIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('Text', 'String', ClassGetProp, ClassSetProp);
    end;

    with Register_Class(TTreeNodes, 'TPersistent') do
    begin
      Register_Method('function Add(Node: TTreeNode; const S: string): TTreeNode', ClassCallMethod);
      Register_Method('function AddChild(Node: TTreeNode; const S: string): TTreeNode', ClassCallMethod);
      Register_Method('procedure BeginUpdate', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure Delete(Node: TTreeNode)', ClassCallMethod);
      Register_Method('procedure EndUpdate', ClassCallMethod);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_DefaultProperty('Item', 'Integer', 'TTreeNode', ClassCallMethod, True);
    end;

    with Register_Class(TTreeView, 'TWinControl') do
    begin
      Register_Method('procedure FullCollapse', ClassCallMethod);
      Register_Method('procedure FullExpand', ClassCallMethod);
      Register_Property('Items', 'TTreeNodes', ClassGetProp, nil);
      Register_Property('Selected', 'TTreeNode', ClassGetProp, ClassSetProp);
      Register_Property('TopItem', 'TTreeNode', ClassGetProp, ClassSetProp);
    end;

    Register_Class(TTrackBar, 'TWinControl');
    Register_Class(TProgressBar, 'TWinControl');
    Register_Class(TListColumn, 'TPersistent');

    with Register_Class(TListColumns, 'TPersistent') do
    begin
      Register_Method('function Add: TListColumn', ClassCallMethod);
      Register_DefaultProperty('Items', 'Integer', 'TListColumn', ClassCallMethod, True);
    end;

    with Register_Class(TListItem, 'TPersistent') do
    begin
      Register_Method('procedure Delete', ClassCallMethod);
      Register_Method('function EditCaption: Boolean', ClassCallMethod);
      Register_Property('Caption', 'String', ClassGetProp, ClassSetProp);
      Register_Property('Checked', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('Data', 'Pointer', ClassGetProp, ClassSetProp);
      Register_Property('ImageIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('Selected', 'Boolean', ClassGetProp, ClassSetProp);
      Register_Property('StateIndex', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('SubItems', 'TStrings', ClassGetProp, ClassSetProp);
    end;

    with Register_Class(TListItems, 'TPersistent') do
    begin
      Register_Method('function Add: TListItem', ClassCallMethod);
      Register_Method('procedure BeginUpdate', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure Delete(Index: Integer)', ClassCallMethod);
      Register_Method('procedure EndUpdate', ClassCallMethod);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_DefaultProperty('Item', 'Integer', 'TListItem', ClassCallMethod, True);
    end;

    Register_Class(TListView, 'TWinControl');
    Register_Class(TToolButton, 'TGraphicControl');
    Register_Class(TToolBar, 'TWinControl');
  end;
end;

function TLibrary_Extctrls.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  Result := 0;

  if aClassType = TCheckListBox then begin
    if aMethodName = 'CHECKED.GET' then begin
      Result := TCheckListBox(aInstance).Checked[aCallVar.Params[0]]; exit; end;
    if aMethodName = 'CHECKED.SET' then begin
      TCheckListBox(aInstance).Checked[aCallVar.Params[0]] := aCallVar.Params[1]; exit; end;
  exit; end;

  if aClassType = TPageControl then begin
    if aMethodName = 'SELECTNEXTPAGE' then begin
      TPageControl(aInstance).SelectNextPage(aCallVar.Params[0]); exit; end;
    if aMethodName = 'PAGES.GET' then begin
      Result := fmscrInteger(TPageControl(aInstance).Pages[aCallVar.Params[0]]); exit; end;
  exit; end;

  if aClassType = TStatusPanels then begin
    if aMethodName = 'ADD' then begin
      Result := fmscrInteger(TStatusPanels(aInstance).Add); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TStatusPanels(aInstance).Items[aCallVar.Params[0]]); exit; end;
  exit; end;

  if aClassType = TTreeNode then begin
    if aMethodName = 'DELETE' then begin
      TTreeNode(aInstance).Delete; exit; end;
    if aMethodName = 'EDITTEXT' then begin
      Result := TTreeNode(aInstance).EditText; exit; end;
  exit; end;

  if aClassType = TTreeNodes then begin
    if aMethodName = 'ADD' then begin
      Result := fmscrInteger(TTreeNodes(aInstance).Add(TTreeNode(fmscrInteger(aCallVar.Params[0])), aCallVar.Params[1])); exit; end;
    if aMethodName = 'ADDCHILD' then begin
      Result := fmscrInteger(TTreeNodes(aInstance).AddChild(TTreeNode(fmscrInteger(aCallVar.Params[0])), aCallVar.Params[1])); exit; end;
    if aMethodName = 'BEGINUPDATE' then begin
      TTreeNodes(aInstance).BeginUpdate; exit; end;
    if aMethodName = 'CLEAR' then begin
      TTreeNodes(aInstance).Clear; exit; end;
    if aMethodName = 'DELETE' then begin
      TTreeNodes(aInstance).Delete(TTreeNode(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'ENDUPDATE' then begin
      TTreeNodes(aInstance).EndUpdate; exit; end;
    if aMethodName = 'ITEM.GET' then begin
      Result := fmscrInteger(TTreeNodes(aInstance).Item[aCallVar.Params[0]]); exit; end;
  exit; end;

  if aClassType = TTreeView then begin
    if aMethodName = 'FULLCOLLAPSE' then begin
      TTreeView(aInstance).FullCollapse; exit; end;
    if aMethodName = 'FULLEXPAND' then begin
      TTreeView(aInstance).FullExpand; exit; end;
  exit; end;

  if aClassType = TListColumns then begin
    if aMethodName = 'ADD' then begin
      Result := fmscrInteger(TListColumns(aInstance).Add); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TListColumns(aInstance).Items[aCallVar.Params[0]]);; exit; end;
  exit; end;

  if aClassType = TListItem then begin
    if aMethodName = 'DELETE' then begin
      TListItem(aInstance).Delete; exit; end;
  exit; end;

  if aClassType = TListItems then begin
    if aMethodName = 'ADD' then begin
      Result := fmscrInteger(TListItems(aInstance).Add); exit; end;
    if aMethodName = 'CLEAR' then begin
      TListItems(aInstance).Clear; exit; end;
    if aMethodName = 'DELETE' then begin
      TListItems(aInstance).Delete(aCallVar.Params[0]); exit; end;
    if aMethodName = 'ITEM.GET' then begin
      Result := fmscrInteger(TListItems(aInstance).Item[aCallVar.Params[0]]); exit; end;
  exit; end;

end;

function TLibrary_Extctrls.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
begin
  Result := 0;

  if aClassType = TPageControl then begin
    if aPropName = 'PAGECOUNT' then begin
      Result := TPageControl(aInstance).PageCount; exit; end;
  exit; end;

  if aClassType = TTabSheet then begin
    if aPropName = 'PAGECONTROL' then begin
      Result := fmscrInteger(TTabSheet(aInstance).PageControl); exit; end;
  exit; end;

  if aClassType = TTreeNode then begin
    if aPropName = 'COUNT' then begin
      Result := TTreeNode(aInstance).Count; exit; end;
    if aPropName = 'DATA' then begin
      Result := fmscrInteger(TTreeNode(aInstance).Data); exit; end;
    if aPropName = 'IMAGEINDEX' then begin
      Result := TTreeNode(aInstance).ImageIndex; exit; end;
    if aPropName = 'SELECTEDINDEX' then begin
      Result := TTreeNode(aInstance).SelectedIndex; exit; end;
    if aPropName = 'STATEINDEX' then begin
      Result := TTreeNode(aInstance).StateIndex; exit; end;
    if aPropName = 'TEXT' then begin
      Result := TTreeNode(aInstance).Text; exit; end;
  exit; end;

  if aClassType = TTreeNodes then begin
    if aPropName = 'COUNT' then begin
      Result := TTreeNodes(aInstance).Count; exit; end;
  exit; end;

  if aClassType = TTreeView then begin
    if aPropName = 'ITEMS' then begin
      Result := fmscrInteger(TTreeView(aInstance).Items); exit; end;
    if aPropName = 'SELECTED' then begin
      Result := fmscrInteger(TTreeView(aInstance).Selected); exit; end;
    if aPropName = 'TOPITEM' then begin
      Result := fmscrInteger(TTreeView(aInstance).TopItem); exit; end;
  exit; end;

  if aClassType = TListItem then begin
    if aPropName = 'CAPTION' then begin
      Result := TListItem(aInstance).Caption; exit; end;
    if aPropName = 'CHECKED' then begin
      Result := TListItem(aInstance).Checked; exit; end;
    if aPropName = 'DATA' then begin
      Result := fmscrInteger(TListItem(aInstance).Data); exit; end;
    if aPropName = 'IMAGEINDEX' then begin
      Result := TListItem(aInstance).ImageIndex; exit; end;
    if aPropName = 'SELECTED' then begin
      Result := TListItem(aInstance).Selected; exit; end;
    if aPropName = 'SUBITEMS' then begin
      Result := fmscrInteger(TListItem(aInstance).SubItems); exit; end;
  exit; end;

  if aClassType = TListItems then begin
    if aPropName = 'COUNT' then begin
      Result := TListItems(aInstance).Count; exit; end;
  exit; end;
end;

procedure TLibrary_Extctrls.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
begin
  if aClassType = TTabSheet then begin
    if aPropName = 'PAGECONTROL' then begin
      TTabSheet(aInstance).PageControl := TPageControl(fmscrInteger(aValue)); exit; end;
  exit; end;

  if aClassType = TTreeNode then begin
    if aPropName = 'DATA' then begin
      TTreeNode(aInstance).Data := Pointer(fmscrInteger(aValue)); exit; end;
    if aPropName = 'IMAGEINDEX' then begin
      TTreeNode(aInstance).ImageIndex := fmscrInteger(aValue); exit; end;
    if aPropName = 'SELECTEDINDEX' then begin
      TTreeNode(aInstance).SelectedIndex := aValue; exit; end;
    if aPropName = 'TEXT' then begin
      TTreeNode(aInstance).Text := aValue; exit; end;
  exit; end;

  if aClassType = TTreeView then begin
    if aPropName = 'SELECTED' then begin
      TTreeView(aInstance).Selected := TTreeNode(fmscrInteger(aValue)); exit; end;
    if aPropName = 'TOPITEM' then begin
      TTreeView(aInstance).TopItem := TTreeNode(fmscrInteger(aValue)); exit; end;
  exit; end;

  if aClassType = TListItem then begin
    if aPropName = 'CAPTION' then begin
      TListItem(aInstance).Caption := aValue; exit; end;
    if aPropName = 'CHECKED' then begin
      TListItem(aInstance).Checked := aValue; exit; end;
    if aPropName = 'DATA' then begin
      TListItem(aInstance).Data := Pointer(fmscrInteger(aValue)); exit; end;
    if aPropName = 'IMAGEINDEX' then begin
      TListItem(aInstance).ImageIndex := integer(aValue); exit; end;
    if aPropName = 'SELECTED' then begin
      TListItem(aInstance).Selected := aValue; exit; end;
    if aPropName = 'SUBITEMS' then begin
      TListItem(aInstance).SubItems := TStrings(fmscrInteger(aValue)); exit; end;
  exit; end;
end;


//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_Extctrls);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_Extctrls);

end.
