{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msrtti_classes;

{$MODE DELPHI}{$H+}

interface

uses SysUtils, Classes, TypInfo, mscoreengine, msbaseobjects;

type
  TmscrRTTILibrary_Classes = class(TComponent);

implementation
type
  TLibrary_Classes = class(TmscrRTTILibrary)
  private
    function  CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    procedure RegisterConsts(AScript: TmscrScript); 
    procedure RegisterRecords(AScript: TmscrScript);
    procedure RegisterClasses(AScript: TmscrScript);       
    function  ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
    function  ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant;
    procedure ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);

    function  RecGetProp(aInstance: Pointer; aTypeInfo: PTypeInfo;  const aPropName: String): variant;
    procedure RecSetProp(aInstance: Pointer; aTypeInfo: PTypeInfo; const aPropName: String; aValue: Variant);
  public
    constructor Create(AScript: TmscrScript); override;
  end;

//...................

constructor TLibrary_Classes.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);
  RegisterConsts(AScript);
  RegisterRecords(AScript);
  RegisterClasses(AScript);
end;

function TLibrary_Classes.CallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
   Result := 0;
   if aInstance<>NIL then
      Result:=ClassCallMethod(aInstance, aClassType, aMethodName, aCallVar);
end;

procedure TLibrary_Classes.RegisterConsts(AScript: TmscrScript);
begin
  with AScript do
  begin

    Register_Const('fmCreate', 'Integer', fmCreate);
    Register_Const('fmOpenRead', 'Integer', fmOpenRead);
    Register_Const('fmOpenWrite', 'Integer', fmOpenWrite);
    Register_Const('fmOpenReadWrite', 'Integer', fmOpenReadWrite);
    Register_Const('fmShareExclusive', 'Integer', fmShareExclusive);
    Register_Const('fmShareDenyWrite', 'Integer', fmShareDenyWrite);
    Register_Const('fmShareDenyNone', 'Integer', fmShareDenyNone);
    Register_Const('soFromBeginning', 'Integer', soFromBeginning);
    Register_Const('soFromCurrent', 'Integer', soFromCurrent);
    Register_Const('soFromEnd', 'Integer', soFromEnd);
    Register_Enum('TDuplicates', 'dupIgnore, dupAccept, dupError');
    Register_Enum('TPrinterOrientation', 'poPortrait, poLandscape');

  end;
end;

procedure TLibrary_Classes.RegisterRecords(AScript: TmscrScript);
begin

  with AScript do
  begin
  
   with Register_Record('TPoint',TypeInfo(TPoint)) do
    begin
      Register_Element('X', 'LongInt', RecGetProp, RecSetProp);
      Register_Element('Y', 'LongInt', RecGetProp, RecSetProp);
    end;

   with Register_Record('TRect',TypeInfo(TRect)) do
    begin
      Register_Element('Left',   'LongInt', RecGetProp, RecSetProp);
      Register_Element('Top',    'LongInt', RecGetProp, RecSetProp);
      Register_Element('Right',  'LongInt', RecGetProp, RecSetProp);
      Register_Element('Bottom', 'LongInt', RecGetProp, RecSetProp);
    end;

  end;
end;

function  TLibrary_Classes.RecGetProp(aInstance: Pointer; aTypeInfo: PTypeInfo;  const aPropName: String): variant;
begin
  Result := 0;

  if aTypeInfo=TypeInfo(TPoint) then begin
    if aPropName = 'X' then begin
      Result:=TPoint(aInstance^).X; exit; end;
    if aPropName = 'Y' then begin
      Result:=TPoint(aInstance^).Y; exit; end;
  exit; end;

   if aTypeInfo=TypeInfo(TRect) then begin
    if aPropName = 'LEFT' then begin
      Result:=TRect(aInstance^).Left; exit; end;
    if aPropName = 'TOP' then begin
      Result:=TRect(aInstance^).Top;  exit; end;
    if aPropName = 'RIGHT' then begin
      Result:=TRect(aInstance^).Right;  exit; end;
    if aPropName = 'BOTTOM' then begin
      Result:=TRect(aInstance^).Bottom;  exit; end;
  exit; end;

end;

procedure TLibrary_Classes.RecSetProp(aInstance: Pointer; aTypeInfo: PTypeInfo; const aPropName: String; aValue: Variant);
begin
  if aTypeInfo=TypeInfo(TPoint) then begin
    if aPropName = 'X' then begin
      TPoint(aInstance^).X:=aValue; exit; end;
    if aPropName = 'Y' then begin
      TPoint(aInstance^).Y:=aValue; exit; end;
  exit; end;

  if aTypeInfo=TypeInfo(TRect) then begin
    if aPropName = 'LEFT' then begin
       TRect(aInstance^).Left :=aValue; exit; end;
    if aPropName = 'TOP' then begin
       TRect(aInstance^).Top :=aValue; exit; end;
    if aPropName = 'RIGHT' then begin
       TRect(aInstance^).Right :=aValue; exit; end;
    if aPropName = 'BOTTOM' then begin
       TRect(aInstance^).Bottom :=aValue; exit; end;
  exit; end;

end;

//=======================================

procedure TLibrary_Classes.RegisterClasses(AScript: TmscrScript);
begin

  with AScript do
  begin

    with Register_Class(TObject, '') do
    begin
      Register_Constructor('constructor Create', ClassCallMethod);
      Register_Method('procedure Free', ClassCallMethod);
      Register_Method('function ClassName: String', ClassCallMethod);
    end;
    with Register_Class(TPersistent, 'TObject') do
      Register_Method('procedure Assign(Source: TPersistent)', ClassCallMethod);

    with Register_Class(TCollectionItem, 'TPersistent') do
    begin
      Register_Property('ID', 'Integer', ClassGetProp, NIL);
      Register_Property('Index', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('DisplayName', 'String', ClassGetProp, ClassSetProp);
    end;

    with Register_Class(TCollection, 'TPersistent') do
    begin
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_DefaultProperty('Items', 'Integer', 'TCollectionItem', ClassCallMethod, True);
    end;
    with Register_Class(TList, 'TObject') do
    begin
      Register_Method('function Add(Item: TObject): Integer', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure Delete(Index: Integer)', ClassCallMethod);
      Register_Method('function IndexOf(Item: TObject): Integer', ClassCallMethod);
      Register_Method('procedure Insert(Index: Integer; Item: TObject)', ClassCallMethod);
      Register_Method('function Remove(Item: TObject): Integer', ClassCallMethod);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_DefaultProperty('Items', 'Integer', 'TObject', ClassCallMethod);
    end;
    with Register_Class(TStrings, 'TPersistent') do
    begin
      Register_Constructor('constructor Create', ClassCallMethod);
      Register_Method('function Add(const S: string): Integer', ClassCallMethod);
      Register_Method('function AddObject(const S: string; AObject: TObject): Integer', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure Delete(Index: Integer)', ClassCallMethod);
      Register_Method('function IndexOf(const S: string): Integer', ClassCallMethod);
      Register_Method('function IndexOfName(const Name: string): Integer', ClassCallMethod);
      Register_Method('function IndexOfObject(AObject: TObject): Integer', ClassCallMethod);
      Register_Method('procedure Insert(Index: Integer; const S: string)', ClassCallMethod);
      Register_Method('procedure InsertObject(Index: Integer; const S: string; AObject: TObject)', ClassCallMethod);
      Register_Method('procedure LoadFromFile(const FileName: string)', ClassCallMethod);
      Register_Method('procedure LoadFromStream(Stream: TStream)', ClassCallMethod);
      Register_Method('procedure SaveToFile(const FileName: string)', ClassCallMethod);
      Register_Method('procedure Move(CurIndex, NewIndex: Integer)', ClassCallMethod);
      Register_Method('procedure SaveToStream(Stream: TStream)', ClassCallMethod);

      Register_Property('CommaText', 'string', ClassGetProp, ClassSetProp);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_IndexProperty('Names', 'Integer', 'string', ClassCallMethod, True);
      Register_IndexProperty('Objects', 'Integer', 'TObject', ClassCallMethod);
      Register_IndexProperty('Values', 'String', 'string', ClassCallMethod);
      Register_DefaultProperty('Strings', 'Integer', 'string', ClassCallMethod);
      Register_Property('Text', 'string', ClassGetProp, ClassSetProp);
    end;
    with Register_Class(TStringList, 'TStrings') do
    begin
      Register_Method('function Find(s: String; var Index: Integer): Boolean', ClassCallMethod);
      Register_Method('procedure Sort', ClassCallMethod);
      Register_Property('Duplicates', 'TDuplicates', ClassGetProp, ClassSetProp);
      Register_Property('Sorted', 'Boolean', ClassGetProp, ClassSetProp);
    end;
    with Register_Class(TStream, 'TObject') do
    begin
      Register_Method('function Read(var Buffer: string; Count: Longint): Longint', ClassCallMethod);
      Register_Method('function Write(Buffer: string; Count: Longint): Longint', ClassCallMethod);
      Register_Method('function Seek(Offset: Longint; Origin: Word): Longint', ClassCallMethod);
      Register_Method('function CopyFrom(Source: TStream; Count: Longint): Longint', ClassCallMethod);
      Register_Property('Position', 'Longint', ClassGetProp, ClassSetProp);
      Register_Property('Size', 'Longint', ClassGetProp, nil);
    end;
    with Register_Class(TFileStream, 'TStream') do
      Register_Constructor('constructor Create(Filename: String; Mode: Word)', ClassCallMethod);
    with Register_Class(TMemoryStream, 'TStream') do
    begin
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure LoadFromStream(Stream: TStream)', ClassCallMethod);
      Register_Method('procedure LoadFromFile(Filename: String)', ClassCallMethod);
      Register_Method('procedure SaveToStream(Stream: TStream)', ClassCallMethod);
      Register_Method('procedure SaveToFile(Filename: String)', ClassCallMethod);
    end;
    with Register_Class(TComponent, 'TPersistent') do
    begin
      Register_Constructor('constructor Create(AOwner: TComponent)', ClassCallMethod);
      Register_Property('Owner', 'TComponent', ClassGetProp, nil);
    end;

    with Register_Class(TmscrDATAItem, 'TObject') do
    begin
      Register_Constructor('constructor Create', ClassCallMethod);
      Register_Method('procedure AddItem(Item: TmscrDATAItem)', ClassCallMethod);
      Register_Method('procedure Clear', ClassCallMethod);
      Register_Method('procedure InsertItem(Index: Integer; Item: TmscrDATAItem)', ClassCallMethod);
      Register_Method('function Add: TmscrDATAItem', ClassCallMethod);
      Register_Method('function Find(const Name: String): Integer', ClassCallMethod);
      Register_Method('function FindItem(const Name: String): TmscrDATAItem', ClassCallMethod);
      Register_Method('function Root: TmscrDATAItem', ClassCallMethod);
      Register_Property('Data', 'Integer', ClassGetProp, ClassSetProp);
      Register_Property('Count', 'Integer', ClassGetProp, nil);
      Register_DefaultProperty('Items', 'Integer', 'TmscrDATAItem', ClassCallMethod, True);
      Register_IndexProperty('Prop', 'String', 'String', ClassCallMethod);
      Register_Property('Name', 'String', ClassGetProp, ClassSetProp);
      Register_Property('Parent', 'TmscrDATAItem', ClassGetProp, nil);
      Register_Property('Text', 'String', ClassGetProp, ClassSetProp);
    end;
    with Register_Class(TmscrDATADocument, 'TObject') do
    begin
      Register_Constructor('constructor Create', ClassCallMethod);
      Register_Method('procedure SaveToStream(Stream: TStream)', ClassCallMethod);
      Register_Method('procedure LoadFromStream(Stream: TStream)', ClassCallMethod);
      Register_Method('procedure SaveToFile(const FileName: String)', ClassCallMethod);
      Register_Method('procedure LoadFromFile(const FileName: String)', ClassCallMethod);
      Register_Property('Root', 'TmscrDATAItem', ClassGetProp, nil);
    end;

  end;
end;

function TLibrary_Classes.ClassCallMethod(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  i: integer;
  s: string;
  _TList: TList;
  _TStrings: TStrings;
  _TStream: TStream;
  _TMemoryStream: TMemoryStream;
  _TfsXMLItem: TmscrDATAItem;
  _TfsXMLDocument: TmscrDATADocument;
begin
  Result := 0;

  if aClassType = TObject then begin
    if aMethodName = 'CREATE' then begin
      if aInstance is TList then
        Result := fmscrInteger(TList.Create) else
        Result := fmscrInteger(aInstance.Create);
    exit; end;

  end;
  if aClassType = TObject then begin
    if aMethodName = 'CREATE' then begin
      if aInstance is TList then
        Result := fmscrInteger(TList.Create) else
        Result := fmscrInteger(aInstance.Create);
    exit; end;
    if aMethodName = 'FREE' then begin
      aInstance.Free; exit; end;
    if aMethodName = 'CLASSNAME' then begin
      Result := aInstance.ClassName; exit; end;
  exit; end;
  
  if aClassType = TPersistent then begin
    if aMethodName = 'ASSIGN' then begin
      TPersistent(aInstance).Assign(TPersistent(fmscrInteger(aCallVar.Params[0]))); exit; end;
  exit; end;
  
  if aClassType = TCollection then begin
    if aMethodName = 'CLEAR' then begin
      TCollection(aInstance).Clear; exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(TCollection(aInstance).Items[aCallVar.Params[0]]); exit; end;
  exit; end;

  if aClassType = TList then begin
    _TList := TList(aInstance);
    if aMethodName = 'ADD' then begin
      _TList.Add(Pointer(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'CLEAR' then begin
      _TList.Clear; exit; end;
    if aMethodName = 'DELETE' then begin
      _TList.Delete(aCallVar.Params[0]); exit; end;
    if aMethodName = 'INDEXOF' then begin
      Result := _TList.IndexOf(Pointer(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'INSERT' then begin
      _TList.Insert(aCallVar.Params[0], Pointer(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'REMOVE' then begin
      _TList.Remove(Pointer(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(_TList.Items[aCallVar.Params[0]]); exit; end;
    if aMethodName = 'ITEMS.SET' then begin
      _TList.Items[aCallVar.Params[0]] := Pointer(fmscrInteger(aCallVar.Params[1])); exit; end;
  exit; end;
   //...................
  if aClassType = TStrings then begin
    _TStrings := TStrings(aInstance);
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(_TStrings.Create); exit; end;
    if aMethodName = 'ADD' then begin
      Result := _TStrings.Add(aCallVar.Params[0]); exit; end;
    if aMethodName = 'AddObject' then begin
      Result := _TStrings.AddObject(aCallVar.Params[0], TObject(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'CLEAR' then begin
      _TStrings.Clear; exit; end;
    if aMethodName = 'DELETE' then begin
      _TStrings.Delete(aCallVar.Params[0]); exit; end;
    if aMethodName = 'INDEXOF' then begin
      Result := _TStrings.IndexOf(aCallVar.Params[0]); exit; end;
    if aMethodName = 'INDEXOFNAME' then begin
      Result := _TStrings.IndexOfName(aCallVar.Params[0]); exit; end;
    if aMethodName = 'INDEXOFOBJECT' then begin
      Result := _TStrings.IndexOfObject(TObject(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'INSERT' then begin
      _TStrings.Insert(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'INSERTOBJECT' then begin
      _TStrings.InsertObject(aCallVar.Params[0], aCallVar.Params[1], TObject(fmscrInteger(aCallVar.Params[2]))); exit; end;
    if aMethodName = 'LOADFROMFILE' then begin
      _TStrings.LoadFromFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'LOADFROMSTREAM' then begin
      _TStrings.LoadFromStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'MOVE' then begin
      _TStrings.Move(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
    if aMethodName = 'SAVETOFILE' then begin
      _TStrings.SaveToFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'SAVETOSTREAM' then begin
      _TStrings.SaveToStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'NAMES.GET' then begin
      Result := _TStrings.Names[aCallVar.Params[0]]; exit; end;
    if aMethodName = 'OBJECTS.GET' then begin
      Result := fmscrInteger(_TStrings.Objects[aCallVar.Params[0]]); exit; end;
    if aMethodName = 'OBJECTS.SET' then begin
      _TStrings.Objects[aCallVar.Params[0]] := TObject(fmscrInteger(aCallVar.Params[1])); exit; end;
    if aMethodName = 'VALUES.GET' then begin
      Result := _TStrings.Values[aCallVar.Params[0]]; exit; end;
    if aMethodName = 'VALUES.SET' then begin
      _TStrings.Values[aCallVar.Params[0]] := aCallVar.Params[1]; exit; end;
    if aMethodName = 'STRINGS.GET' then begin
      Result := _TStrings.Strings[aCallVar.Params[0]]; exit; end;
    if aMethodName = 'STRINGS.SET' then begin
      _TStrings.Strings[aCallVar.Params[0]] := aCallVar.Params[1]; exit; end;
  exit; end;

  if aClassType = TStringList then begin
    if aMethodName = 'FIND' then begin
      Result := TStringList(aInstance).Find(aCallVar.Params[0], i);
      aCallVar.Params[1] := i; exit; end;
    if aMethodName = 'SORT' then begin
      TStringList(aInstance).Sort; exit; end;
  exit; end;
  
  if aClassType = TStream then begin
    _TStream := TStream(aInstance);
    if aMethodName = 'READ' then begin
      SetLength(s, integer(aCallVar.Params[1]));
      Result := _TStream.Read(s[1], aCallVar.Params[1]);
      SetLength(s, integer(Result));
      aCallVar.Params[0] := s;
    exit; end;
    if aMethodName = 'WRITE' then begin
      s := aCallVar.Params[0];
      Result := _TStream.Write(s[1], aCallVar.Params[1]);
    exit; end;
    if aMethodName = 'SEEK' then begin
      Result := _TStream.Seek(int64(aCallVar.Params[0]), word(aCallVar.Params[1]));exit; end;
    if aMethodName = 'COPYFROM' then begin
      Result := _TStream.CopyFrom(TStream(fmscrInteger(aCallVar.Params[0])), aCallVar.Params[1]); exit; end;
  exit; end;
  
  if aClassType = TFileStream then begin
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(TFileStream(aInstance).Create(aCallVar.Params[0], aCallVar.Params[1])); exit; end;
  exit; end;
  
  if aClassType = TMemoryStream then begin
    _TMemoryStream := TMemoryStream(aInstance);
    if aMethodName = 'CLEAR' then begin
      _TMemoryStream.Clear; exit; end;
    if aMethodName = 'LOADFROMSTREAM' then begin
      _TMemoryStream.LoadFromStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'LOADFROMFILE' then begin
      _TMemoryStream.LoadFromFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'SAVETOSTREAM' then begin
      _TMemoryStream.SaveToStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'SAVETOFILE' then begin
      _TMemoryStream.SaveToFile(aCallVar.Params[0]); exit; end;
  exit; end;
  
  if aClassType = TComponent then begin
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(TComponent(aInstance).Create(TComponent(fmscrInteger(aCallVar.Params[0])))); exit; end;
  exit; end;
  
  if aClassType = TmscrDATAItem then begin
    _TfsXMLItem := TmscrDATAItem(aInstance);
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(_TfsXMLItem.Create); exit; end;
    if aMethodName = 'ADDITEM' then begin
      _TfsXMLItem.AddItem(TmscrDATAItem(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'CLEAR' then begin
      _TfsXMLItem.Clear; exit; end;
    if aMethodName = 'INSERTITEM' then begin
      _TfsXMLItem.InsertItem(aCallVar.Params[0], TmscrDATAItem(fmscrInteger(aCallVar.Params[1]))); exit; end;
    if aMethodName = 'ADD' then begin
      Result := fmscrInteger(_TfsXMLItem.Add); exit; end;
    if aMethodName = 'FIND' then begin
      Result := _TfsXMLItem.Find(aCallVar.Params[0]); exit; end;
    if aMethodName = 'FINDITEM' then begin
      Result := fmscrInteger(_TfsXMLItem.FindItem(aCallVar.Params[0])); exit; end;
    if aMethodName = 'PROP.GET' then begin
      Result := _TfsXMLItem.Prop[aCallVar.Params[0]]; exit; end;
    if aMethodName = 'PROP.SET' then begin
      _TfsXMLItem.Prop[aCallVar.Params[0]] := aCallVar.Params[1] ; exit; end;
    if aMethodName = 'ROOT' then begin
      Result := fmscrInteger(_TfsXMLItem.Root); exit; end;
    if aMethodName = 'ROOT' then begin
      Result := fmscrInteger(_TfsXMLItem.Root); exit; end;
    if aMethodName = 'ITEMS.GET' then begin
      Result := fmscrInteger(_TfsXMLItem[aCallVar.Params[0]]); exit; end;
  exit; end;
  
  if aClassType = TmscrDATADocument then begin
    _TfsXMLDocument := TmscrDATADocument(aInstance);
    if aMethodName = 'CREATE' then begin
      Result := fmscrInteger(_TfsXMLDocument.Create); exit; end;
    if aMethodName = 'SAVETOSTREAM' then begin
      _TfsXMLDocument.SaveToStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'LOADFROMSTREAM' then begin
      _TfsXMLDocument.LoadFromStream(TStream(fmscrInteger(aCallVar.Params[0]))); exit; end;
    if aMethodName = 'SAVETOFILE' then begin
      _TfsXMLDocument.SaveToFile(aCallVar.Params[0]); exit; end;
    if aMethodName = 'LOADFROMFILE' then begin
      _TfsXMLDocument.LoadFromFile(aCallVar.Params[0]); exit; end;
  exit; end;

end;

function TLibrary_Classes.ClassGetProp(aInstance: TObject; aClassType: TClass; const aPropName: string; aCallVar: TmscrEngProperty): variant;
begin
  Result := 0;

  if aClassType = TCollection then begin
    if aPropName = 'COUNT' then begin
      Result := TCollection(aInstance).Count; exit; end;
  exit; end;

  if aClassType = TList then begin
    if aPropName = 'COUNT' then begin
      Result := TList(aInstance).Count; exit; end;
  exit; end;

  if aClassType = TStrings then begin
    if aPropName = 'COMMATEXT' then begin
      Result := TStrings(aInstance).CommaText; exit; end;
    if aPropName = 'COUNT' then begin
      Result := TStrings(aInstance).Count; exit; end;
    if aPropName = 'TEXT' then begin
      Result := TStrings(aInstance).Text; exit; end;
  exit; end;
  
  if aClassType = TStringList then begin
    if aPropName = 'DUPLICATES' then begin
      Result := TStringList(aInstance).Duplicates; exit; end;
    if aPropName = 'SORTED' then begin
      Result := TStringList(aInstance).Sorted; exit; end;
  exit; end;

  if aClassType = TStream then begin
    if aPropName = 'POSITION' then begin
      Result := TStream(aInstance).Position; exit; end;
    if aPropName = 'SIZE' then begin
      Result := TStream(aInstance).Size; exit; end;
  exit; end;

  if aClassType = TComponent then begin
    if aPropName = 'OWNER' then begin
      Result := fmscrInteger(TComponent(aInstance).Owner); exit; end;
  exit; end;

  if aClassType = TmscrDATAItem then begin
    if aPropName = 'DATA' then begin
      Result := fmscrInteger(TmscrDATAItem(aInstance).Data); exit; end;
    if aPropName = 'COUNT' then begin
      Result := TmscrDATAItem(aInstance).Count; exit; end;
    if aPropName = 'NAME' then begin
      Result := TmscrDATAItem(aInstance).Name; exit; end;
    if aPropName = 'PARENT' then begin
      Result := fmscrInteger(TmscrDATAItem(aInstance).Parent); exit; end;
    if aPropName = 'TEXT' then begin
      Result := TmscrDATAItem(aInstance).Text; exit; end;
  exit; end;

  if aClassType = TmscrDATADocument then begin
    if aPropName = 'ROOT' then begin
      Result := fmscrInteger(TmscrDATADocument(aInstance).Root); exit; end;
  exit; end;
  
  if aClassType = TCollectionItem then begin
    if aPropName = 'ID' then begin
      Result := TCollectionItem(aInstance).ID; exit; end;
    if aPropName = 'INDEX' then begin
      Result := TCollectionItem(aInstance).Index; exit; end;
    if aPropName = 'DISPLAYNAME' then begin
      Result := TCollectionItem(aInstance).DisplayName; exit; end;
  exit; end;

end;

procedure TLibrary_Classes.ClassSetProp(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty);
begin

  if aClassType = TStrings then begin
    if aPropName = 'COMMATEXT' then begin
      TStrings(aInstance).CommaText := aValue; exit; end;
    if aPropName = 'TEXT' then begin
      TStrings(aInstance).Text := aValue; exit; end;
  exit; end;
  
  if aClassType = TStringList then begin
    if aPropName = 'DUPLICATES' then begin
      TStringList(aInstance).Duplicates := aValue; exit; end;
    if aPropName = 'SORTED' then begin
      TStringList(aInstance).Sorted := aValue; exit; end;
  exit; end;
  
  if aClassType = TStream then  begin
    if aPropName = 'POSITION' then begin
      TStream(aInstance).Position := aValue; exit; end;
  exit; end;
  
  if aClassType = TmscrDATAItem then begin
    if aPropName = 'DATA' then begin
      TmscrDATAItem(aInstance).Data := Pointer(fmscrInteger(aValue)); exit; end;
    if aPropName = 'NAME' then begin
      TmscrDATAItem(aInstance).Name := aValue; exit; end;
    if aPropName = 'TEXT' then begin
      TmscrDATAItem(aInstance).Text := aValue; exit; end;
  exit; end;
  
  if aClassType = TCollectionItem then begin
    if aPropName = 'INDEX' then begin
      TCollectionItem(aInstance).Index:= aValue; exit; end;
    if aPropName = 'DISPLAYNAME' then begin
      TCollectionItem(aInstance).DisplayName:=aValue; exit; end;
  exit; end;

end;

//==========================================

initialization
  MSCR_RTTILibraries.Add(TLibrary_Classes);

finalization
  MSCR_RTTILibraries.Remove(TLibrary_Classes);

end.
