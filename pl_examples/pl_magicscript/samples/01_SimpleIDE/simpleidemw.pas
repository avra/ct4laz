unit Simpleidemw;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf,
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ExtCtrls, Menus, ActnList, ComCtrls,
  Unit2,
  mscoreengine,
  //....Registration Units...........

  msrtti_forms,
  msrtti_graphics,
  msrtti_classes,
  msrtti_extctrls,
  msrtti_inifiles,
  msrtti_menus,
  msrtti_dialogs,
  msrtti_db,
  msrtti_dbctrls,

//  ms_allreg_graphics32,
//  ms_allreg_GLScene,

  //....Registration Units...........

  SynEdit, SynHighlighterPas, msLibrariestree, msScriptHighlighter,
  msScriptEditor;

type

  { TForm1 }

  TForm1 = class(TForm)
    actExit: TAction;
    acEvaluate: TAction;
    actNew: TAction;
    actStop: TAction;
    actRun: TAction;
    actSaveAs: TAction;
    actSave: TAction;
    actOpen: TAction;
    ActionList1: TActionList;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    N2: TMenuItem;
    N1: TMenuItem;
    mscrPascalScript1: TmscrPascalScript;
    mscrRTTILibrariesTree1: TmscrRTTILibrariesTree;
    mscrScriptEditor1: TmscrScriptEditor;
    mscrScriptTerminal1: TmscrScriptTerminal;
    OpenDialog1: TOpenDialog;
    PanelClient: TPanel;
    PanelBottom: TPanel;
    PanelRight: TPanel;
    SaveDialog1: TSaveDialog;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    mscrScriptHighlighter1: TmscrScriptHighlighter;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton10: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    RunBtn: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    procedure acEvaluateExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actNewExecute(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actRunExecute(Sender: TObject);
    procedure actSaveAsExecute(Sender: TObject);
    procedure actSaveExecute(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    fFilename:string; 
    FRunning: Boolean;  
    FStopped: Boolean;
    Procedure WriteCaption;
  public
    procedure DoOnPascalScript1RunLine(Sender: TmscrScript; const aUnitName, aSourcePos: String);
  end; 

var
  Form1: TForm1; 

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathscripts = '..\scripts\';
{$ELSE}
  pathscripts = '../scripts/';
{$ENDIF}

const
  {$if defined(MSWINDOWS)}
   cnCodeTyphonFont= 'Courier New';
   cnCodeTyphonFontHeight= 8;
  {$elseif defined(NETBSD)}
       cnCodeTyphonFont= 'Monospace';
       cnCodeTyphonFontHeight=8;
  {$elseif defined(DARWIN)}
     {$IFDEF LCLCocoa}
       cnCodeTyphonFont= 'Courier New';
       cnCodeTyphonFontHeight=8;
     {$ELSE}
       cnCodeTyphonFont= 'Courier New';
       cnCodeTyphonFontHeight=8;
     {$ENDIF}
  {$ELSE}
       cnCodeTyphonFont= 'Courier New';
       cnCodeTyphonFontHeight=8;
  {$ENDIF}

cnCaption='MagicScript Simple IDE';

  
function MSecsBetween(AStopTime, AStartTime: TDateTime): Int64;
var
  StartMs, StopMs: Comp;
begin
  StopMs := TimeStampToMSecs(DateTimeToTimeStamp(AStopTime));
  StartMs := TimeStampToMSecs(DateTimeToTimeStamp(AStartTime));
  Result := Trunc(StopMs - StartMs);
end;

//---------------------------------

procedure TForm1.FormCreate(Sender: TObject);
begin
  MSCR_GlobalScripter.Register_Form(Form1);
  VarMSCR_ConcoleOut:=mscrScriptTerminal1;

  FRunning:= False;
  FStopped:= False;

{$IFDEF WINDOWS}
   mscrScriptEditor1.Font.Name:=cnCodeTyphonFont;
   mscrScriptEditor1.Font.size:=cnCodeTyphonFontHeight+1;
{$ELSE}
   mscrScriptEditor1.Font.Name:=cnCodeTyphonFont;
   mscrScriptEditor1.Font.size:=cnCodeTyphonFontHeight;
{$ENDIF}
end;


Procedure TForm1.WriteCaption;  
begin
  Caption:=cnCaption+'  ('+ExtractFileName(fFilename)+')';
end;

procedure TForm1.actExitExecute(Sender: TObject);
begin
  close;
end;

procedure TForm1.actNewExecute(Sender: TObject);
begin
  if FRunning then Exit;
  mscrScriptEditor1.Clear;
  mscrScriptEditor1.Lines.Add('PROGRAM Untitled;');
  mscrScriptEditor1.Lines.Add(' ');      
  mscrScriptEditor1.Lines.Add('BEGIN //************* Main Program start');
  mscrScriptEditor1.Lines.Add(' ');
  mscrScriptEditor1.Lines.Add(' ');
  mscrScriptEditor1.Lines.Add('END.  //************* Main Program End');
  fFilename:='';
  WriteCaption;
end;

procedure TForm1.actOpenExecute(Sender: TObject);
begin
  if OpenDialog1.InitialDir='' then
   OpenDialog1.InitialDir :=  pathscripts;

  if OpenDialog1.Execute then
   begin                            
    fFilename:=OpenDialog1.FileName;
    mscrScriptEditor1.Lines.LoadFromFile(fFilename);
    WriteCaption;
   end;
end;
 
procedure TForm1.actSaveExecute(Sender: TObject);
begin
   if fFilename='' then
    begin
     actSaveAsExecute(nil);
    end else
    begin
     mscrScriptEditor1.Lines.SaveToFile(fFileName);
    end;
end;

procedure TForm1.actSaveAsExecute(Sender: TObject);
begin
  if fFilename='' then SaveDialog1.InitialDir:=pathscripts;

  SaveDialog1.FileName:=ExtractFilename(OpenDialog1.FileName);

  if SaveDialog1.Execute then
   begin
    fFilename:=SaveDialog1.FileName;
    mscrScriptEditor1.Lines.SaveToFile(fFilename);
    WriteCaption;
   end;
end;


//======================================

procedure TForm1.actRunExecute(Sender: TObject);
var
  StartTime: TDateTime;
  p: TPoint;
begin
  mscrScriptTerminal1.Clear;

  if FRunning then
  begin
    if (Sender = RunBtn) or (Sender = actRun) then
      mscrPascalScript1.OnRunLine := nil;
    FStopped := False;
    Exit;
  end;

  mscrPascalScript1.Clear;
  mscrPascalScript1.Lines := mscrScriptEditor1.Lines;
  mscrPascalScript1.Parent := MSCR_GlobalScripter;

  if not mscrPascalScript1.Compile then
  begin
    mscrScriptEditor1.SetFocus;
    p := MSCR_PosToPoint(mscrPascalScript1.ErrorPos);

    mscrScriptEditor1.CaretXY:=p;

    if mscrPascalScript1.ErrorUnit = '' then
      StatusBar1.SimpleText := mscrPascalScript1.ErrorMsg else
      StatusBar1.SimpleText := mscrPascalScript1.ErrorUnit + ': ' + mscrPascalScript1.ErrorMsg;
    Exit;
  end else
  begin
    StatusBar1.SimpleText := 'Compiled OK, Running...';
  end;

  StartTime := Now;
  Application.ProcessMessages;

  if (Sender = RunBtn) or (Sender = actRun) then
    mscrPascalScript1.OnRunLine := nil else
    mscrPascalScript1.OnRunLine := @DoOnPascalScript1RunLine;

  FRunning := True;
  try
    mscrPascalScript1.Execute;
  finally
    FRunning := False;
    StatusBar1.SimpleText := 'Exception in the program';
  end;

  StatusBar1.SimpleText := 'Executed in ' + IntToStr(MSecsBetween(Now, StartTime)) + ' ms';

end;

procedure TForm1.actStopExecute(Sender: TObject);
begin
  if FRunning=False then Exit;
  if mscrPascalScript1=nil then Exit;

  mscrPascalScript1.Terminate;
end;

procedure TForm1.acEvaluateExecute(Sender: TObject);
begin
  Form2.ShowModal;
end;

//===================================================

procedure TForm1.DoOnPascalScript1RunLine(Sender: TmscrScript; const aUnitName, aSourcePos: String);
var
  p: TPoint;
begin
  EnableWindow(Handle, True);
  SetFocus;

  p := MSCR_PosToPoint(aSourcePos);
  mscrScriptEditor1.CaretXY:=p;

  FStopped := True;
  while FStopped do
    Application.ProcessMessages;
end;

end.

