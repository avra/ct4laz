unit drawpicturemw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLCanvas, Forms, Controls, Graphics, Dialogs;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenGLCanvas1: TOpenGLCanvas;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure OpenGLCanvas1Paint(Sender: TObject);
  private
    FPicture1, FPicture2: TPicture;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

{ TForm1 }
 

procedure TForm1.FormCreate(Sender: TObject);
begin
  FPicture1 := TPicture.Create;
  FPicture2 := TPicture.Create;

  FPicture1.LoadFromFile(ExpandFileName(pathMedia + '1.png'));
  FPicture2.LoadFromFile(ExpandFileName(pathMedia + '2.png'));
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  FPicture1.Free;
  FPicture2.Free;
end;

procedure TForm1.OpenGLCanvas1Paint(Sender: TObject);
var
  ARect: TRect;
begin
  ARect:=OpenGLCanvas1.BoundsRect;
  OpenGLCanvas1.Canvas.StretchDraw(ARect, FPicture2.Bitmap);

  ARect:=Rect(50,50,200,180);
  OpenGLCanvas1.Canvas.StretchDraw(ARect, FPicture1.Bitmap);
end;

end.

