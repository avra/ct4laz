unit drawtextdemomw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLCanvas, Forms, Controls, Graphics, Dialogs;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenGLCanvas1: TOpenGLCanvas;
    procedure OpenGLCanvas1Paint(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.OpenGLCanvas1Paint(Sender: TObject);
begin

  OpenGLCanvas1.Canvas.Font.Color:=clblack; 
  OpenGLCanvas1.Canvas.Font.Bold:= false;
  OpenGLCanvas1.Canvas.Font.Height := 14;
  OpenGLCanvas1.Canvas.TextOut(20,50, 'Simple text Black Color');

  OpenGLCanvas1.Canvas.Font.Color:=clred;
  OpenGLCanvas1.Canvas.Font.Bold:= true;   
  OpenGLCanvas1.Canvas.Font.Height := 20;
  OpenGLCanvas1.Canvas.TextOut(20,100, 'Simple test Red-Bold Color');
  
  OpenGLCanvas1.Canvas.Font.Color:=clBlue;
  OpenGLCanvas1.Canvas.Font.Height := 45;
  OpenGLCanvas1.Canvas.TextOut(20,200, 'Simple test BIG Blue');
end;

end.
