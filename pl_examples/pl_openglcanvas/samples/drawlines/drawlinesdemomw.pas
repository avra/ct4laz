unit DrawLinesdemomw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLCanvas, Forms, Controls, Graphics, Dialogs;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenGLCanvas1: TOpenGLCanvas;
    procedure OpenGLCanvas1Paint(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.OpenGLCanvas1Paint(Sender: TObject);
var
  LIndex: Integer;
  ARect: TRect;
begin
  ARect:=OpenGLCanvas1.BoundsRect;
  RandSeed := 0;
  OpenGLCanvas1.Canvas.MoveTo((ARect.Left + ARect.Right) div 2, (ARect.Top + ARect.Bottom) div 2);

  for LIndex := 1 to 10 do
  begin
    OpenGLCanvas1.Canvas.LineTo(ARect.Left + Random(ARect.Right  - ARect.Left),
                                ARect.Top  + Random(ARect.Bottom - ARect.Top));
  end;
  //.........
  OpenGLCanvas1.Canvas.Pen.Color:=clred;
  OpenGLCanvas1.Canvas.Pen.Width:=3;
  for LIndex := 1 to 10 do
  begin
    OpenGLCanvas1.Canvas.LineTo(ARect.Left + Random(ARect.Right  - ARect.Left),
                                ARect.Top  + Random(ARect.Bottom - ARect.Top));
  end;

  //.........
  OpenGLCanvas1.Canvas.Pen.Color:=clblue;
  OpenGLCanvas1.Canvas.Pen.Width:=6;
  for LIndex := 1 to 10 do
  begin
    OpenGLCanvas1.Canvas.LineTo(ARect.Left + Random(ARect.Right  - ARect.Left),
                                ARect.Top  + Random(ARect.Bottom - ARect.Top));
  end;
end;

end.

