unit drawpolylinemw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLCanvas, Forms, Controls, Graphics, Dialogs;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenGLCanvas1: TOpenGLCanvas;
    procedure OpenGLCanvas1Paint(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.OpenGLCanvas1Paint(Sender: TObject);
 var ARect: TRect;

  //.......
  function P(X, Y: Single): TPoint;
  begin
    Result := Point(Round(ARect.Left + (ARect.Right  - ARect.Left) * X),
                    Round(ARect.Top  + (ARect.Bottom - ARect.Top)  * Y));
  end;
  //.......
begin


  ARect:=Rect(20,20,200,200);

  OpenGLCanvas1.Canvas.Pen.Color := clblue;
  OpenGLCanvas1.Canvas.Pen.Width := 2;
  OpenGLCanvas1.Canvas.Polyline([P(0.5, 0),
                                P(0, 0.5),
                                P(0.5, 1),
                                P(1, 0.5),
                                P(0.5, 0)]);

  //-----------
  ARect:=Rect(150,150,350,350);

  OpenGLCanvas1.Canvas.Pen.Color := clRed;
  OpenGLCanvas1.Canvas.Pen.Width := 3;
  OpenGLCanvas1.Canvas.Polyline([P(0.5, 0), P(0, 0.5), P(0.5, 1), P(1, 0.5), P(0.5, 0),
                                P(0.5, 0.5), P(0.3, 0.5), P(0.5, 0.7), P(0.7, 0.5), P(0.5, 0.5)]);


end;

end.

