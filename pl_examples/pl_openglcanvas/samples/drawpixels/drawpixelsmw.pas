unit drawpixelsmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLCanvas, Forms, Controls, Graphics, Dialogs;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenGLCanvas1: TOpenGLCanvas;
    procedure OpenGLCanvas1Paint(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.OpenGLCanvas1Paint(Sender: TObject);
var
  LIndex: Integer;
  ARect: TRect;
begin
  ARect:=OpenGLCanvas1.BoundsRect;
  RandSeed := 0;


  for LIndex := 1 to 200 do
  begin
    OpenGLCanvas1.Canvas.Pixels[ARect.Left + Random(ARect.Right - ARect.Left),
                                ARect.Top  + Random(ARect.Bottom - ARect.Top)] := clblack;
  end;
  //.........
  for LIndex := 1 to 200 do
  begin
    OpenGLCanvas1.Canvas.Pixels[ARect.Left + Random(ARect.Right - ARect.Left),
                                ARect.Top  + Random(ARect.Bottom - ARect.Top)] := clred;
  end;
  //.........

  for LIndex := 1 to 200 do
  begin
    OpenGLCanvas1.Canvas.Pixels[ARect.Left + Random(ARect.Right - ARect.Left),
                                ARect.Top  + Random(ARect.Bottom - ARect.Top)] := clblue;
  end;

end;

end.

