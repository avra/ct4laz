unit drawrectanglesmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, OpenGLCanvas, Forms, Controls, Graphics, Dialogs,GraphUtil;

type

  { TForm1 }

  TForm1 = class(TForm)
    OpenGLCanvas1: TOpenGLCanvas;
    procedure OpenGLCanvas1Paint(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.OpenGLCanvas1Paint(Sender: TObject);
 var ARect: TRect;
begin
  ARect:=Rect(10,10,100,100);
  OpenGLCanvas1.Canvas.Brush.Color := clPurple;
  OpenGLCanvas1.Canvas.Pen.Color := clRed;
  OpenGLCanvas1.Canvas.Pen.Width := 2;
  OpenGLCanvas1.Canvas.Rectangle(ARect);
  //----

  ARect:=Rect(120,120,200,200);
  OpenGLCanvas1.Canvas.Brush.Color := clSkyBlue;
  OpenGLCanvas1.Canvas.Pen.Color := clBlue;
  OpenGLCanvas1.Canvas.Pen.Width := 3;
  OpenGLCanvas1.Canvas.Rectangle(ARect);

end;

end.

