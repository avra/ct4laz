unit zoomingmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, GIS_Earth, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Spin, Types;

type

  { TForm1 }

  TForm1 = class(TForm)
    btZoomDefault: TButton;
    btZoomMIN: TButton;
    btZoomMAX: TButton;
    btZoomPlus: TButton;
    btZoomMinus: TButton;
    Earth1: TEarth;
    cbZoomFactor: TFloatSpinEdit;
    Label1: TLabel;
    Panel1: TPanel;
    procedure btZoomDefaultClick(Sender: TObject);
    procedure btZoomMAXClick(Sender: TObject);
    procedure btZoomMINClick(Sender: TObject);
    procedure btZoomMinusClick(Sender: TObject);
    procedure btZoomPlusClick(Sender: TObject);
    procedure cbZoomFactorChange(Sender: TObject);
    procedure Earth1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }
  
procedure TForm1.FormShow(Sender: TObject);
begin
 // cbZoomFactor.Value:=Earth1.ZoomFactor;
end;

procedure TForm1.btZoomDefaultClick(Sender: TObject);
begin
 Earth1.SetZoomFactorToDefault;
 cbZoomFactor.Value:=Earth1.ZoomFactor;
 Earth1.ZoomToDefault;
end;

procedure TForm1.btZoomMAXClick(Sender: TObject);
begin
 Earth1.ZoomToMax;
end;

procedure TForm1.btZoomMINClick(Sender: TObject);
begin
 Earth1.ZoomToMin;
end;

procedure TForm1.btZoomMinusClick(Sender: TObject);
begin
 Earth1.ZoomOut;
end;

procedure TForm1.btZoomPlusClick(Sender: TObject);
begin
 Earth1.ZoomIn;
end;

procedure TForm1.cbZoomFactorChange(Sender: TObject);
begin
 Earth1.ZoomFactor:= cbZoomFactor.Value;
end;

procedure TForm1.Earth1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if WheelDelta>0 then
   Earth1.ZoomIn else
   Earth1.ZoomOut;
end;

end.

