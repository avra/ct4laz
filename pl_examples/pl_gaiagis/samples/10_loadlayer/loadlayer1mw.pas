unit loadlayer1mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Spin, ExtDlgs, Types,
  GIS_Classes,
  GIS_Earth;

type

  { TForm1 }

  TForm1 = class(TForm)
    btZoomDefault: TButton;
    btZoomMIN: TButton;
    btZoomMAX: TButton;
    btZoomPlus: TButton;
    btZoomMinus: TButton;
    btLoadSurfaceImage: TButton;
    btClearSurfaceImage: TButton;
    Button1: TButton;
    Earth1: TEarth;
    cbZoomFactor: TFloatSpinEdit;
    Label1: TLabel;
    OpenPictureDialog1: TOpenPictureDialog;
    Panel1: TPanel;
    procedure btLoadSurfaceImageClick(Sender: TObject);
    procedure btZoomDefaultClick(Sender: TObject);
    procedure btZoomMAXClick(Sender: TObject);
    procedure btZoomMINClick(Sender: TObject);
    procedure btZoomMinusClick(Sender: TObject);
    procedure btZoomPlusClick(Sender: TObject);
    procedure btClearSurfaceImageClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure cbZoomFactorChange(Sender: TObject);
    procedure Earth1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia   = '..\xmedia\';
  pathMediaSI = '..\xmedia\surfaceimages\';
{$ELSE}
  pathMedia   = '../xmedia/';
  pathMediaSI = '../xmedia/surfaceimages/';
{$ENDIF}

{ TForm1 }
  
procedure TForm1.FormShow(Sender: TObject);
begin
 // cbZoomFactor.Value:=Earth1.ZoomFactor;
end;

procedure TForm1.btZoomDefaultClick(Sender: TObject);
begin
 Earth1.SetZoomFactorToDefault;
 cbZoomFactor.Value:=Earth1.ZoomFactor;
 Earth1.ZoomToDefault;
end;

procedure TForm1.btZoomMAXClick(Sender: TObject);
begin
 Earth1.ZoomToMax;
end;

procedure TForm1.btZoomMINClick(Sender: TObject);
begin
 Earth1.ZoomToMin;
end;

procedure TForm1.btZoomMinusClick(Sender: TObject);
begin
 Earth1.ZoomOut;
end;

procedure TForm1.btZoomPlusClick(Sender: TObject);
begin
 Earth1.ZoomIn;
end;

procedure TForm1.cbZoomFactorChange(Sender: TObject);
begin
 Earth1.ZoomFactor:= cbZoomFactor.Value;
end;

procedure TForm1.Earth1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin   
  if WheelDelta>0 then
   Earth1.ZoomIn else
   Earth1.ZoomOut;

end;

procedure TForm1.btLoadSurfaceImageClick(Sender: TObject);
begin
  if OpenPictureDialog1.FileName='' then
       OpenPictureDialog1.InitialDir:=SetDirSeparators(pathMediaSI);

  if OpenPictureDialog1.Execute then
       Earth1.SurfaceTextureName:=OpenPictureDialog1.Filename;
end;   

procedure TForm1.btClearSurfaceImageClick(Sender: TObject);
begin
  Earth1.SurfaceTextureName:='';
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
 Earth1.LayerNewFromAnyFile(pathMedia+'country.shp')
end;


end.

