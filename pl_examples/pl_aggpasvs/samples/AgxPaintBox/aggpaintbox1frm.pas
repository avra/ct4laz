unit aggpaintbox1frm;

{$mode objfpc}{$H+}

{$IFDEF WINCE}
   {$r hiresaware.res}
{$ENDIF}
interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, agx_lclpaintbox, agg_lclcontrols;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    CheckBox1: TCheckBox;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    Agg: TAgxPaintBox;
    tex:TAggLabel;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
   Agg:=TAgxPaintBox.Create(self);
   Agg.Parent:=self;
   Agg.SetBounds(0,0,self.Width,450);

        {
   tex:=TAggLabel.Create(self);
   tex.Parent:=self;
   tex.SetBounds(10,10,100,100);
   tex.Text:='ffff sdfsdf';
           }
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Button1Click(nil);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin

 //...................................
 agg.aggengine.ClearAll(255,255,255);
 agg.aggengine.FillColorSet(agg.aggengine.ColorToColorAgx(clred));
 agg.aggengine.LineColorSet(agg.aggengine.ColorToColorAgx(clBlack));

 //............ Ellipse ...................
 agg.aggengine.Ellipse(50,50,200,Random(agg.aggengine.Width));

 agg.aggengine.FillColorSet(Random(255),255,0,127);
 agg.aggengine.Ellipse(0,0,Random(agg.aggengine.Width),Random(agg.aggengine.Height));

 agg.aggengine.FillColorSet(20,30,Random(255),100);
 agg.aggengine.Ellipse(0,0,Random(agg.aggengine.Width),Random(agg.aggengine.Height));

 //............ Text ......................
  agg.aggengine.FillColorSet(255,255,255);
  agg.aggengine.NoLine;
  agg.aggengine.Font('arial',24);
  agg.aggengine.Text(100,Random(100),'Text Demo 1');
  agg.aggengine.Font('arial',60,true);

  agg.aggengine.TextAngleSet(0);
  agg.aggengine.FillColorSet(Random(255),0,Random(255),120);
  agg.aggengine.LineWidthSet(0.5);
  agg.aggengine.LineColorSet(agg.aggengine.ColorToColorAgx(clblue));
  agg.aggengine.FontHeightSet(45);
  agg.aggengine.Text(100,120,'Text Demo 2');

  agg.aggengine.TextAngleSet(Random(359));
  agg.aggengine.FillColorSet(Random(255),Random(255),Random(255),255);
  agg.aggengine.LineWidthSet(0.8);
  agg.aggengine.LineColorSet(agg.aggengine.ColorToColorAgx(clred));
  agg.aggengine.FontHeightSet(60);
  agg.aggengine.Text(round(agg.aggengine.Width/2),round(agg.aggengine.Height/2),'AGG Library Demo');


  agg.Invalidate;

end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  Timer1.Enabled:=CheckBox1.Checked;
end;

end.

