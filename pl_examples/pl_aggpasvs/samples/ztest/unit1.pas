unit Unit1;

{$mode objfpc}{$H+}

{$IFDEF WINCE}
   {$r hiresaware.res}
{$ENDIF}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics,
  Dialogs, StdCtrls,agg_lclpaintbox,agg_lclcontrols;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    Agg: TAggpaintbox;
    tex:TAggLabel;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
   Agg:=TAggpaintbox.Create(self);
   Agg.Parent:=self;
   Agg.SetBounds(220,100,250,450);

   tex:=TAggLabel.Create(self);
   tex.Parent:=self;
   tex.SetBounds(10,10,100,100);
   tex.Text:='ffff sdfsdf';

end;

procedure TForm1.Button1Click(Sender: TObject);
begin

   Agg.AggEngine.clearAll(100,100,100);

   Agg.AggEngine.fillColor(200,0,100);
   Agg.AggEngine.lineColor(200,0,100);
   Agg.AggEngine.arc(10,10,20,20,0,20);
   Agg.Invalidate;

end;

end.

