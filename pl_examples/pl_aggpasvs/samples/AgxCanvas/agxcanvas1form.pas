{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}
 
unit AgxCanvas1form;

{$mode objfpc}{$H+}

{$IFDEF WINCE}
   {$r hiresaware.res}
{$ENDIF}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs
  ,StdCtrls, ExtCtrls,agx_canvas;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Image1: TImage;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
   // fbitmap:Tbitmap;
    fagg:TAgxCanvas;
    fex:boolean;

  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation   

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin

 //...................................
 fagg.ClearAll(255,255,255);
 fagg.FillColorSet(fagg.ColorToColorAgx(clred));
 fagg.LineColorSet(fagg.ColorToColorAgx(clBlack));

 //............ Ellipse ...................
 fagg.Ellipse(50,50,200,200);

 fagg.FillColorSet(Random(255),255,0,127);
 fagg.Ellipse(0,0,Random(fagg.Width),Random(fagg.Height));

 fagg.FillColorSet(20,30,Random(255),100);
 fagg.Ellipse(0,0,Random(fagg.Width),Random(fagg.Height));

 //............ Text ......................
  fagg.FillColorSet(255,255,255);
  fagg.NoLine;

  fagg.Font('arial',30);
  fagg.Text(100,Random(100),'Text Demo 1');


  fagg.Font('arial',100,true);
  fagg.TextAngleSet(0);
  fagg.FillColorSet(Random(255),0,Random(255),120);
  fagg.LineWidthSet(0.5);
  fagg.LineColorSet(fagg.ColorToColorAgx(clblue));
  fagg.Text(100,120,'Text Demo 2');

  fagg.FillColorSet(Random(255),Random(255),Random(255),255);
  fagg.LineWidthSet(0.8);
  fagg.LineColorSet(fagg.ColorToColorAgx(clred));

  fagg.TextAngleSet(0);
  fagg.FontHeightSet(10);
  fagg.Text(100,100,'AGG Library Demo line1');

  fagg.FontHeightSet(15);
  fagg.Text(100,150,'AGG Library Demo line2');

  fagg.TextAngleSet(Random(80));
  fagg.FontHeightSet(20);
  fagg.Text(round(fagg.Width/2),round(fagg.Height/2),'AGG Library Demo');

 fagg.Invalidate;

 //..................................
 //Now Draw the preview image as a Small Image (Picture In Picture)

 fagg.LineWidthSet(0.8);
 fagg.FillColorSet(fagg.ColorToColorAgx(clblack));
 fagg.Rectangle(0,0,102,102);
 fagg.TransformImage(Image1.Picture.Bitmap,2,2,100,100);

 fagg.Invalidate;  // final update
end;

procedure TForm1.FormShow(Sender: TObject);
begin
 Image1.Picture.Bitmap.PixelFormat:=pf32bit;
 Image1.Picture.Bitmap.SetSize(Image1.Width,Image1.Height);
 fAgg:=TAgxCanvas.create;
 fagg.Attach(Image1.Picture.Bitmap);
end;

procedure TForm1.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  fAgg.free ;
end;

end.

