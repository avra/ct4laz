unit drawtoformmw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
  ctutils,
  sdl2lib;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
  private

  public
    procedure xSDL2_Init;
    procedure xSDL2_Draw;
    procedure xSDL2_Close;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}


const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

Function PathLibrary:string;
begin
{$IFDEF Windows}
  Result := ctGetRuntimesCPUOSDir(true);
{$ELSE}
  Result := '';
{$ENDIF}
end;



var
i : Integer;
sdlWindow1 : PSDL_Window;
sdlRenderer : PSDL_Renderer;
sdlTexture1 : PSDL_Texture;
sdlRect1 : PSDL_Rect;
sdlPoint1 : PSDL_Point;
Surface1:PSDL_Surface;

//----------------------------------------------

{ TForm1 }

procedure TForm1.xSDL2_Init;
begin
// load SDL2 library
  if Not SDL2LIB_Initialize(PathLibrary+SDL_LibName) then exit;
//-----------------------------------------------------------------
end;

procedure TForm1.xSDL2_Draw;
begin
  Caption:='Draw to Form Demo Running...';

  //initilization of video subsystem
  if SDL_Init( SDL_INIT_EVERYTHING ) < 0 then Exit;

  //sdlWindow1 := SDL_CreateWindow( 'Window 1', 50, 50, 500, 500, SDL_WINDOW_SHOWN );
  sdlWindow1 := SDL_CreateWindowFrom(Pointer(Form1.Handle));
  if sdlWindow1 = nil then Exit;

  sdlRenderer := SDL_CreateRenderer( sdlWindow1, -1, 0 );
  if sdlRenderer = nil then Exit;

  Surface1 := SDL_LoadBMP( pathMedia+ 'sdl2.bmp' );
  sdlTexture1:=SDL_CreateTextureFromSurface(sdlRenderer,Surface1);

  if sdlTexture1 = nil then Exit;

  //change colours
  for i := 0 to 255 do
  begin
    SDL_RenderClear( sdlRenderer );
    SDL_SetTextureColorMod( sdlTexture1, 255-i, 255-i, i );
    SDL_RenderCopy( sdlRenderer, sdlTexture1, nil, nil );
    SDL_RenderPresent (sdlRenderer);
    SDL_Delay( 20 );
  end;
  SDL_SetTextureColorMod( sdlTexture1, 255, 255, 255 );
  application.ProcessMessages;

{
  //fade out
  SDL_SetRenderDrawColor( sdlRenderer, 0, 255, 255, 255 );
  SDL_SetTextureBlendMode( sdlTexture1, SDL_BLENDMODE_BLEND );
  for i := 0 to 255 do
  begin
    SDL_RenderClear( sdlRenderer );
    SDL_SetTextureAlphaMod( sdlTexture1, 255-i );
    SDL_RenderCopy( sdlRenderer, sdlTexture1, nil, nil );
    SDL_RenderPresent (sdlRenderer);
    SDL_Delay( 20 );
  end;
  SDL_SetTextureAlphaMod( sdlTexture1, 255 );
  application.ProcessMessages;

  //change clip rectangle and change colours
  new( sdlRect1 );
  sdlRect1^.x := 100; sdlRect1^.y := 100; sdlRect1^.w := 190; sdlRect1^.h := 190;
  SDL_RenderSetClipRect( sdlRenderer, sdlRect1 );
  for i := 0 to 255 do
  begin
    SDL_SetTextureColorMod( sdlTexture1, 255-i, 255-i, i );
    SDL_RenderCopy( sdlRenderer, sdlTexture1, nil, nil );
    SDL_RenderPresent ( sdlRenderer );
    SDL_Delay( 20 );
  end;
  SDL_SetTextureColorMod( sdlTexture1, 255, 255, 255 );
  SDL_RenderSetClipRect( sdlRenderer, nil );
  application.ProcessMessages;

  //change viewport and change colours
  sdlRect1^.x := 10; sdlRect1^.y := 10; sdlRect1^.w := 190; sdlRect1^.h := 190;
  SDL_RenderSetViewport( sdlRenderer, sdlRect1 );
  for i := 0 to 255 do
  begin
    SDL_SetTextureColorMod( sdlTexture1, 255-i, 255-i, i );
    SDL_RenderCopy( sdlRenderer, sdlTexture1, nil, nil );
    SDL_RenderDrawRect( sdlRenderer, sdlRect1 );
    SDL_RenderPresent (sdlRenderer);
    SDL_Delay( 20 );
  end;
  SDL_RenderSetViewport( sdlRenderer, nil );
  application.ProcessMessages;

  //setting logical size resolution
  SDL_RenderClear( sdlRenderer );
  SDL_RenderSetLogicalSize( sdlRenderer, 640, 480 );
  for i := 0 to 255 do
  begin
    SDL_SetTextureColorMod( sdlTexture1, 255-i, 255-i, i );
    SDL_RenderCopy( sdlRenderer, sdlTexture1, nil, nil );
    SDL_RenderPresent (sdlRenderer);
    SDL_Delay( 20 );
  end;
  SDL_RenderSetLogicalSize( sdlRenderer, 500, 500 );
  SDL_SetTextureColorMod( sdlTexture1, 255, 255, 255 );
  application.ProcessMessages;

  //rotate
  new( sdlPoint1 ); sdlPoint1^.x := 250; sdlPoint1^.y := 250;
  for i := 0 to 359 do
  begin
    SDL_RenderClear( sdlRenderer );
    SDL_RenderCopyEx( sdlRenderer, sdlTexture1, nil, nil, i, sdlPoint1, SDL_FLIP_NONE );
    SDL_RenderPresent( sdlRenderer );
    SDL_Delay( 5 );
  end;
  application.ProcessMessages;

  //scale
  for i := 500 downto 1 do
  begin
    SDL_RenderClear( sdlRenderer );
    sdlRect1^.x := 0; sdlRect1^.y := 0; sdlRect1^.w := i; sdlRect1^.h := i;
    SDL_RenderCopy( sdlRenderer, sdlTexture1, nil, sdlRect1 );
    SDL_RenderPresent( sdlRenderer );
    SDL_Delay( 20 );
  end;
}

  Caption:='Draw to Form Demo, Finished'
end;

procedure TForm1.xSDL2_Close;
begin
  dispose( sdlPoint1 );
  dispose( sdlRect1 );
  SDL_DestroyTexture( sdlTexture1 );
  SDL_DestroyRenderer( sdlRenderer );
  SDL_DestroyWindow ( sdlWindow1 );

  //shutting down video subsystem
  SDL_Quit;

end;

//===========================================================

procedure TForm1.FormActivate(Sender: TObject);
begin 
   xSDL2_Init;
   xSDL2_Draw;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin 
   xSDL2_Close;
end;


end.

