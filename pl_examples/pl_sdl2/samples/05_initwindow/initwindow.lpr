program initwindow;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,ctutils,
  sdl2lib;

const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

Function PathLibrary:string;
begin
  Result := ctGetRuntimesCPUOSDir(true);
end;

var
i : integer;
sdlWindow1 : PSDL_Window;

begin
// load SDL2 library
  if Not SDL2LIB_Initialize(PathLibrary+SDL_LibName) then exit;
//-----------------------------------------------------------------

  //initilization of video subsystem
  if SDL_Init( SDL_INIT_VIDEO ) < 0 then Exit;


  sdlWindow1 := SDL_CreateWindow( 'Window 1', 50, 50, 500, 500, SDL_WINDOW_SHOWN );
  if sdlWindow1 = nil then Exit;

  SDL_Delay( 3000 );

  SDL_DestroyWindow ( sdlWindow1 );

  //shutting down video subsystem    }
  SDL_Quit;

end.

