unit librarytestsdl2mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ctutils,
  ExtCtrls,sdl2lib;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    RadioGroup1: TRadioGroup;
    procedure Button1Click(Sender: TObject);
  private
    procedure TestLibFunctions;
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia = '..\xmedia\';
{$ELSE}
  pathMedia = '../xmedia/';
{$ENDIF}

Function PathLibrary:string;
begin
{$IFDEF Windows}
  Result := ctGetRuntimesCPUOSDir(true);
{$ELSE}
  Result := '';
{$ENDIF}
end;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
 Memo1.Clear;
  if Not SDL2LIB_Initialize(PathLibrary+SDL_LibName) then
  begin
   Memo1.Lines.add('ERROR: '+SDL_LibName+' NOT Loaded');
   exit;
  end else
  begin
   Memo1.Lines.add(SDL_LibName+' Loaded OK');
  end;

  TestLibFunctions;
end;

procedure TForm1.TestLibFunctions;
//................................................
procedure _writetostrings(const isok:boolean;const astr:string);
begin
  case RadioGroup1.ItemIndex of
   0:begin
      Memo1.Lines.add(astr);
      exit;
   end;
  1: begin
     if isok=false then
       Memo1.Lines.add(astr);
     exit;
   end;

   2:begin
     if isok=True then
       Memo1.Lines.add(astr);
     exit;
     end;

  end;
end;
//.................................................
begin
 Memo1.Lines.BeginUpdate;

 Memo1.Lines.add('--------------------');


  if SDL_GetVersion=nil then _writetostrings(false,'SDL_GetVersion'+' is NIL ???') else  _writetostrings(true,'SDL_GetVersion'+' Is OK');
  if SDL_GetRevision=nil then _writetostrings(false,'SDL_GetRevision'+' is NIL ???') else  _writetostrings(true,'SDL_GetRevision'+' Is OK');
  if SDL_GetRevisionNumber=nil then _writetostrings(false,'SDL_GetRevisionNumber'+' is NIL ???') else  _writetostrings(true,'SDL_GetRevisionNumber'+' Is OK');

  if SDL_SetError=nil then _writetostrings(false,'SDL_SetError'+' is NIL ???') else  _writetostrings(true,'SDL_SetError'+' Is OK');
  if SDL_GetError=nil then _writetostrings(false,'SDL_GetError'+' is NIL ???') else  _writetostrings(true,'SDL_GetError'+' Is OK');
  if SDL_ClearError=nil then _writetostrings(false,'SDL_ClearError'+' is NIL ???') else  _writetostrings(true,'SDL_ClearError'+' Is OK');
  if SDL_Error=nil then _writetostrings(false,'SDL_Error'+' is NIL ???') else  _writetostrings(true,'SDL_Error'+' Is OK');
  if SDL_GetPlatform=nil then _writetostrings(false,'SDL_GetPlatform'+' is NIL ???') else  _writetostrings(true,'SDL_GetPlatform'+' Is OK');

  if SDL_CreateThread=nil then _writetostrings(false,'SDL_CreateThread'+' is NIL ???') else  _writetostrings(true,'SDL_CreateThread'+' Is OK');

  if SDL_GetThreadName=nil then _writetostrings(false,'SDL_GetThreadName'+' is NIL ???') else  _writetostrings(true,'SDL_GetThreadName'+' Is OK');
  if SDL_ThreadID=nil then _writetostrings(false,'SDL_ThreadID'+' is NIL ???') else  _writetostrings(true,'SDL_ThreadID'+' Is OK');
  if SDL_GetThreadID=nil then _writetostrings(false,'SDL_GetThreadID'+' is NIL ???') else  _writetostrings(true,'SDL_GetThreadID'+' Is OK');
  if SDL_SetThreadPriority=nil then _writetostrings(false,'SDL_SetThreadPriority'+' is NIL ???') else  _writetostrings(true,'SDL_SetThreadPriority'+' Is OK');
  if SDL_WaitThread=nil then _writetostrings(false,'SDL_WaitThread'+' is NIL ???') else  _writetostrings(true,'SDL_WaitThread'+' Is OK');
  if SDL_DetachThread=nil then _writetostrings(false,'SDL_DetachThread'+' is NIL ???') else  _writetostrings(true,'SDL_DetachThread'+' Is OK');
  if SDL_TLSCreate=nil then _writetostrings(false,'SDL_TLSCreate'+' is NIL ???') else  _writetostrings(true,'SDL_TLSCreate'+' Is OK');
  if SDL_TLSGet=nil then _writetostrings(false,'SDL_TLSGet'+' is NIL ???') else  _writetostrings(true,'SDL_TLSGet'+' Is OK');
  if SDL_TLSSet=nil then _writetostrings(false,'SDL_TLSSet'+' is NIL ???') else  _writetostrings(true,'SDL_TLSSet'+' Is OK');

  if SDL_CreateMutex=nil then _writetostrings(false,'SDL_CreateMutex'+' is NIL ???') else  _writetostrings(true,'SDL_CreateMutex'+' Is OK');
  if SDL_LockMutex=nil then _writetostrings(false,'SDL_LockMutex'+' is NIL ???') else  _writetostrings(true,'SDL_LockMutex'+' Is OK');
  if SDL_TryLockMutex=nil then _writetostrings(false,'SDL_TryLockMutex'+' is NIL ???') else  _writetostrings(true,'SDL_TryLockMutex'+' Is OK');
  if SDL_UnlockMutex=nil then _writetostrings(false,'SDL_UnlockMutex'+' is NIL ???') else  _writetostrings(true,'SDL_UnlockMutex'+' Is OK');
  if SDL_DestroyMutex=nil then _writetostrings(false,'SDL_DestroyMutex'+' is NIL ???') else  _writetostrings(true,'SDL_DestroyMutex'+' Is OK');
  if SDL_CreateSemaphore=nil then _writetostrings(false,'SDL_CreateSemaphore'+' is NIL ???') else  _writetostrings(true,'SDL_CreateSemaphore'+' Is OK');
  if SDL_DestroySemaphore=nil then _writetostrings(false,'SDL_DestroySemaphore'+' is NIL ???') else  _writetostrings(true,'SDL_DestroySemaphore'+' Is OK');
  if SDL_SemWait=nil then _writetostrings(false,'SDL_SemWait'+' is NIL ???') else  _writetostrings(true,'SDL_SemWait'+' Is OK');
  if SDL_SemTryWait=nil then _writetostrings(false,'SDL_SemTryWait'+' is NIL ???') else  _writetostrings(true,'SDL_SemTryWait'+' Is OK');
  if SDL_SemWaitTimeout=nil then _writetostrings(false,'SDL_SemWaitTimeout'+' is NIL ???') else  _writetostrings(true,'SDL_SemWaitTimeout'+' Is OK');
  if SDL_SemPost=nil then _writetostrings(false,'SDL_SemPost'+' is NIL ???') else  _writetostrings(true,'SDL_SemPost'+' Is OK');
  if SDL_SemValue=nil then _writetostrings(false,'SDL_SemValue'+' is NIL ???') else  _writetostrings(true,'SDL_SemValue'+' Is OK');
  if SDL_CreateCond=nil then _writetostrings(false,'SDL_CreateCond'+' is NIL ???') else  _writetostrings(true,'SDL_CreateCond'+' Is OK');
  if SDL_DestroyCond=nil then _writetostrings(false,'SDL_DestroyCond'+' is NIL ???') else  _writetostrings(true,'SDL_DestroyCond'+' Is OK');
  if SDL_CondSignal=nil then _writetostrings(false,'SDL_CondSignal'+' is NIL ???') else  _writetostrings(true,'SDL_CondSignal'+' Is OK');
  if SDL_CondBroadcast=nil then _writetostrings(false,'SDL_CondBroadcast'+' is NIL ???') else  _writetostrings(true,'SDL_CondBroadcast'+' Is OK');
  if SDL_CondWait=nil then _writetostrings(false,'SDL_CondWait'+' is NIL ???') else  _writetostrings(true,'SDL_CondWait'+' Is OK');
  if SDL_CondWaitTimeout=nil then _writetostrings(false,'SDL_CondWaitTimeout'+' is NIL ???') else  _writetostrings(true,'SDL_CondWaitTimeout'+' Is OK');

  if SDL_GetTicks=nil then _writetostrings(false,'SDL_GetTicks'+' is NIL ???') else  _writetostrings(true,'SDL_GetTicks'+' Is OK');
  if SDL_GetPerformanceCounter=nil then _writetostrings(false,'SDL_GetPerformanceCounter'+' is NIL ???') else  _writetostrings(true,'SDL_GetPerformanceCounter'+' Is OK');
  if SDL_GetPerformanceFrequency=nil then _writetostrings(false,'SDL_GetPerformanceFrequency'+' is NIL ???') else  _writetostrings(true,'SDL_GetPerformanceFrequency'+' Is OK');
  if SDL_Delay=nil then _writetostrings(false,'SDL_Delay'+' is NIL ???') else  _writetostrings(true,'SDL_Delay'+' Is OK');
  if SDL_AddTimer=nil then _writetostrings(false,'SDL_AddTimer'+' is NIL ???') else  _writetostrings(true,'SDL_AddTimer'+' Is OK');
  if SDL_RemoveTimer=nil then _writetostrings(false,'SDL_RemoveTimer'+' is NIL ???') else  _writetostrings(true,'SDL_RemoveTimer'+' Is OK');

  if SDL_GetPixelFormatName=nil then _writetostrings(false,'SDL_GetPixelFormatName'+' is NIL ???') else  _writetostrings(true,'SDL_GetPixelFormatName'+' Is OK');
  if SDL_PixelFormatEnumToMasks=nil then _writetostrings(false,'SDL_PixelFormatEnumToMasks'+' is NIL ???') else  _writetostrings(true,'SDL_PixelFormatEnumToMasks'+' Is OK');
  if SDL_MasksToPixelFormatEnum=nil then _writetostrings(false,'SDL_MasksToPixelFormatEnum'+' is NIL ???') else  _writetostrings(true,'SDL_MasksToPixelFormatEnum'+' Is OK');
  if SDL_AllocFormat=nil then _writetostrings(false,'SDL_AllocFormat'+' is NIL ???') else  _writetostrings(true,'SDL_AllocFormat'+' Is OK');
  if SDL_FreeFormat=nil then _writetostrings(false,'SDL_FreeFormat'+' is NIL ???') else  _writetostrings(true,'SDL_FreeFormat'+' Is OK');
  if SDL_AllocPalette=nil then _writetostrings(false,'SDL_AllocPalette'+' is NIL ???') else  _writetostrings(true,'SDL_AllocPalette'+' Is OK');
  if SDL_SetPixelFormatPalette=nil then _writetostrings(false,'SDL_SetPixelFormatPalette'+' is NIL ???') else  _writetostrings(true,'SDL_SetPixelFormatPalette'+' Is OK');
  if SDL_SetPaletteColors=nil then _writetostrings(false,'SDL_SetPaletteColors'+' is NIL ???') else  _writetostrings(true,'SDL_SetPaletteColors'+' Is OK');
  if SDL_FreePalette=nil then _writetostrings(false,'SDL_FreePalette'+' is NIL ???') else  _writetostrings(true,'SDL_FreePalette'+' Is OK');
  if SDL_MapRGB=nil then _writetostrings(false,'SDL_MapRGB'+' is NIL ???') else  _writetostrings(true,'SDL_MapRGB'+' Is OK');
  if SDL_MapRGBA=nil then _writetostrings(false,'SDL_MapRGBA'+' is NIL ???') else  _writetostrings(true,'SDL_MapRGBA'+' Is OK');
  if SDL_GetRGB=nil then _writetostrings(false,'SDL_GetRGB'+' is NIL ???') else  _writetostrings(true,'SDL_GetRGB'+' Is OK');
  if SDL_GetRGBA=nil then _writetostrings(false,'SDL_GetRGBA'+' is NIL ???') else  _writetostrings(true,'SDL_GetRGBA'+' Is OK');
  if SDL_CalculateGammaRamp=nil then _writetostrings(false,'SDL_CalculateGammaRamp'+' is NIL ???') else  _writetostrings(true,'SDL_CalculateGammaRamp'+' Is OK');

  if SDL_HasIntersection=nil then _writetostrings(false,'SDL_HasIntersection'+' is NIL ???') else  _writetostrings(true,'SDL_HasIntersection'+' Is OK');
  if SDL_IntersectRect=nil then _writetostrings(false,'SDL_IntersectRect'+' is NIL ???') else  _writetostrings(true,'SDL_IntersectRect'+' Is OK');
  if SDL_UnionRect=nil then _writetostrings(false,'SDL_UnionRect'+' is NIL ???') else  _writetostrings(true,'SDL_UnionRect'+' Is OK');
  if SDL_EnclosePoints=nil then _writetostrings(false,'SDL_EnclosePoints'+' is NIL ???') else  _writetostrings(true,'SDL_EnclosePoints'+' Is OK');
  if SDL_IntersectRectAndLine=nil then _writetostrings(false,'SDL_IntersectRectAndLine'+' is NIL ???') else  _writetostrings(true,'SDL_IntersectRectAndLine'+' Is OK');

  if SDL_RWFromFile=nil then _writetostrings(false,'SDL_RWFromFile'+' is NIL ???') else  _writetostrings(true,'SDL_RWFromFile'+' Is OK');
  if SDL_RWFromFP=nil then _writetostrings(false,'SDL_RWFromFP'+' is NIL ???') else  _writetostrings(true,'SDL_RWFromFP'+' Is OK');
  if SDL_RWFromMem=nil then _writetostrings(false,'SDL_RWFromMem'+' is NIL ???') else  _writetostrings(true,'SDL_RWFromMem'+' Is OK');
  if SDL_RWFromConstMem=nil then _writetostrings(false,'SDL_RWFromConstMem'+' is NIL ???') else  _writetostrings(true,'SDL_RWFromConstMem'+' Is OK');
  if SDL_AllocRW=nil then _writetostrings(false,'SDL_AllocRW'+' is NIL ???') else  _writetostrings(true,'SDL_AllocRW'+' Is OK');
  if SDL_FreeRW=nil then _writetostrings(false,'SDL_FreeRW'+' is NIL ???') else  _writetostrings(true,'SDL_FreeRW'+' Is OK');
  if SDL_ReadU8=nil then _writetostrings(false,'SDL_ReadU8'+' is NIL ???') else  _writetostrings(true,'SDL_ReadU8'+' Is OK');
  if SDL_ReadLE16=nil then _writetostrings(false,'SDL_ReadLE16'+' is NIL ???') else  _writetostrings(true,'SDL_ReadLE16'+' Is OK');
  if SDL_ReadBE16=nil then _writetostrings(false,'SDL_ReadBE16'+' is NIL ???') else  _writetostrings(true,'SDL_ReadBE16'+' Is OK');
  if SDL_ReadLE32=nil then _writetostrings(false,'SDL_ReadLE32'+' is NIL ???') else  _writetostrings(true,'SDL_ReadLE32'+' Is OK');
  if SDL_ReadBE32=nil then _writetostrings(false,'SDL_ReadBE32'+' is NIL ???') else  _writetostrings(true,'SDL_ReadBE32'+' Is OK');
  if SDL_ReadLE64=nil then _writetostrings(false,'SDL_ReadLE64'+' is NIL ???') else  _writetostrings(true,'SDL_ReadLE64'+' Is OK');
  if SDL_ReadBE64=nil then _writetostrings(false,'SDL_ReadBE64'+' is NIL ???') else  _writetostrings(true,'SDL_ReadBE64'+' Is OK');
  if SDL_WriteU8=nil then _writetostrings(false,'SDL_WriteU8'+' is NIL ???') else  _writetostrings(true,'SDL_WriteU8'+' Is OK');
  if SDL_WriteLE16=nil then _writetostrings(false,'SDL_WriteLE16'+' is NIL ???') else  _writetostrings(true,'SDL_WriteLE16'+' Is OK');
  if SDL_WriteBE16=nil then _writetostrings(false,'SDL_WriteBE16'+' is NIL ???') else  _writetostrings(true,'SDL_WriteBE16'+' Is OK');
  if SDL_WriteLE32=nil then _writetostrings(false,'SDL_WriteLE32'+' is NIL ???') else  _writetostrings(true,'SDL_WriteLE32'+' Is OK');
  if SDL_WriteBE32=nil then _writetostrings(false,'SDL_WriteBE32'+' is NIL ???') else  _writetostrings(true,'SDL_WriteBE32'+' Is OK');
  if SDL_WriteLE64=nil then _writetostrings(false,'SDL_WriteLE64'+' is NIL ???') else  _writetostrings(true,'SDL_WriteLE64'+' Is OK');
  if SDL_WriteBE64=nil then _writetostrings(false,'SDL_WriteBE64'+' is NIL ???') else  _writetostrings(true,'SDL_WriteBE64'+' Is OK');

  if SDL_GetNumAudioDrivers=nil then _writetostrings(false,'SDL_GetNumAudioDrivers'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumAudioDrivers'+' Is OK');
  if SDL_GetAudioDriver=nil then _writetostrings(false,'SDL_GetAudioDriver'+' is NIL ???') else  _writetostrings(true,'SDL_GetAudioDriver'+' Is OK');
  if SDL_AudioInit=nil then _writetostrings(false,'SDL_AudioInit'+' is NIL ???') else  _writetostrings(true,'SDL_AudioInit'+' Is OK');
  if SDL_AudioQuit=nil then _writetostrings(false,'SDL_AudioQuit'+' is NIL ???') else  _writetostrings(true,'SDL_AudioQuit'+' Is OK');
  if SDL_GetCurrentAudioDriver=nil then _writetostrings(false,'SDL_GetCurrentAudioDriver'+' is NIL ???') else  _writetostrings(true,'SDL_GetCurrentAudioDriver'+' Is OK');
  if SDL_OpenAudio=nil then _writetostrings(false,'SDL_OpenAudio'+' is NIL ???') else  _writetostrings(true,'SDL_OpenAudio'+' Is OK');
  if SDL_GetNumAudioDevices=nil then _writetostrings(false,'SDL_GetNumAudioDevices'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumAudioDevices'+' Is OK');
  if SDL_GetAudioDeviceName=nil then _writetostrings(false,'SDL_GetAudioDeviceName'+' is NIL ???') else  _writetostrings(true,'SDL_GetAudioDeviceName'+' Is OK');
  if SDL_OpenAudioDevice=nil then _writetostrings(false,'SDL_OpenAudioDevice'+' is NIL ???') else  _writetostrings(true,'SDL_OpenAudioDevice'+' Is OK');
  if SDL_GetAudioStatus=nil then _writetostrings(false,'SDL_GetAudioStatus'+' is NIL ???') else  _writetostrings(true,'SDL_GetAudioStatus'+' Is OK');
  if SDL_GetAudioDeviceStatus=nil then _writetostrings(false,'SDL_GetAudioDeviceStatus'+' is NIL ???') else  _writetostrings(true,'SDL_GetAudioDeviceStatus'+' Is OK');
  if SDL_PauseAudio=nil then _writetostrings(false,'SDL_PauseAudio'+' is NIL ???') else  _writetostrings(true,'SDL_PauseAudio'+' Is OK');
  if SDL_PauseAudioDevice=nil then _writetostrings(false,'SDL_PauseAudioDevice'+' is NIL ???') else  _writetostrings(true,'SDL_PauseAudioDevice'+' Is OK');
  if SDL_LoadWAV_RW=nil then _writetostrings(false,'SDL_LoadWAV_RW'+' is NIL ???') else  _writetostrings(true,'SDL_LoadWAV_RW'+' Is OK');
  if SDL_FreeWAV=nil then _writetostrings(false,'SDL_FreeWAV'+' is NIL ???') else  _writetostrings(true,'SDL_FreeWAV'+' Is OK');
  if SDL_BuildAudioCVT=nil then _writetostrings(false,'SDL_BuildAudioCVT'+' is NIL ???') else  _writetostrings(true,'SDL_BuildAudioCVT'+' Is OK');
  if SDL_ConvertAudio=nil then _writetostrings(false,'SDL_ConvertAudio'+' is NIL ???') else  _writetostrings(true,'SDL_ConvertAudio'+' Is OK');
  if SDL_MixAudio=nil then _writetostrings(false,'SDL_MixAudio'+' is NIL ???') else  _writetostrings(true,'SDL_MixAudio'+' Is OK');
  if SDL_MixAudioFormat=nil then _writetostrings(false,'SDL_MixAudioFormat'+' is NIL ???') else  _writetostrings(true,'SDL_MixAudioFormat'+' Is OK');
  if SDL_QueueAudio=nil then _writetostrings(false,'SDL_QueueAudio'+' is NIL ???') else  _writetostrings(true,'SDL_QueueAudio'+' Is OK');
  if SDL_DequeueAudio=nil then _writetostrings(false,'SDL_DequeueAudio'+' is NIL ???') else  _writetostrings(true,'SDL_DequeueAudio'+' Is OK');
  if SDL_GetQueuedAudioSize=nil then _writetostrings(false,'SDL_GetQueuedAudioSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetQueuedAudioSize'+' Is OK');
  if SDL_ClearQueuedAudio=nil then _writetostrings(false,'SDL_ClearQueuedAudio'+' is NIL ???') else  _writetostrings(true,'SDL_ClearQueuedAudio'+' Is OK');
  if SDL_LockAudio=nil then _writetostrings(false,'SDL_LockAudio'+' is NIL ???') else  _writetostrings(true,'SDL_LockAudio'+' Is OK');
  if SDL_LockAudioDevice=nil then _writetostrings(false,'SDL_LockAudioDevice'+' is NIL ???') else  _writetostrings(true,'SDL_LockAudioDevice'+' Is OK');
  if SDL_UnlockAudio=nil then _writetostrings(false,'SDL_UnlockAudio'+' is NIL ???') else  _writetostrings(true,'SDL_UnlockAudio'+' Is OK');
  if SDL_UnlockAudioDevice=nil then _writetostrings(false,'SDL_UnlockAudioDevice'+' is NIL ???') else  _writetostrings(true,'SDL_UnlockAudioDevice'+' Is OK');
  if SDL_CloseAudio=nil then _writetostrings(false,'SDL_CloseAudio'+' is NIL ???') else  _writetostrings(true,'SDL_CloseAudio'+' Is OK');
  if SDL_CloseAudioDevice=nil then _writetostrings(false,'SDL_CloseAudioDevice'+' is NIL ???') else  _writetostrings(true,'SDL_CloseAudioDevice'+' Is OK');

  if SDL_CreateRGBSurface=nil then _writetostrings(false,'SDL_CreateRGBSurface'+' is NIL ???') else  _writetostrings(true,'SDL_CreateRGBSurface'+' Is OK');
  if SDL_CreateRGBSurfaceWithFormat=nil then _writetostrings(false,'SDL_CreateRGBSurfaceWithFormat'+' is NIL ???') else  _writetostrings(true,'SDL_CreateRGBSurfaceWithFormat'+' Is OK');
  if SDL_CreateRGBSurfaceFrom=nil then _writetostrings(false,'SDL_CreateRGBSurfaceFrom'+' is NIL ???') else  _writetostrings(true,'SDL_CreateRGBSurfaceFrom'+' Is OK');
  if SDL_CreateRGBSurfaceWithFormatFrom=nil then _writetostrings(false,'SDL_CreateRGBSurfaceWithFormatFrom'+' is NIL ???') else  _writetostrings(true,'SDL_CreateRGBSurfaceWithFormatFrom'+' Is OK');
  if SDL_FreeSurface=nil then _writetostrings(false,'SDL_FreeSurface'+' is NIL ???') else  _writetostrings(true,'SDL_FreeSurface'+' Is OK');
  if SDL_SetSurfacePalette=nil then _writetostrings(false,'SDL_SetSurfacePalette'+' is NIL ???') else  _writetostrings(true,'SDL_SetSurfacePalette'+' Is OK');
  if SDL_LockSurface=nil then _writetostrings(false,'SDL_LockSurface'+' is NIL ???') else  _writetostrings(true,'SDL_LockSurface'+' Is OK');
  if SDL_UnlockSurface=nil then _writetostrings(false,'SDL_UnlockSurface'+' is NIL ???') else  _writetostrings(true,'SDL_UnlockSurface'+' Is OK');
  if SDL_LoadBMP_RW=nil then _writetostrings(false,'SDL_LoadBMP_RW'+' is NIL ???') else  _writetostrings(true,'SDL_LoadBMP_RW'+' Is OK');
  if SDL_SaveBMP_RW=nil then _writetostrings(false,'SDL_SaveBMP_RW'+' is NIL ???') else  _writetostrings(true,'SDL_SaveBMP_RW'+' Is OK');
  if SDL_SetSurfaceRLE=nil then _writetostrings(false,'SDL_SetSurfaceRLE'+' is NIL ???') else  _writetostrings(true,'SDL_SetSurfaceRLE'+' Is OK');
  if SDL_SetColorKey=nil then _writetostrings(false,'SDL_SetColorKey'+' is NIL ???') else  _writetostrings(true,'SDL_SetColorKey'+' Is OK');
  if SDL_GetColorKey=nil then _writetostrings(false,'SDL_GetColorKey'+' is NIL ???') else  _writetostrings(true,'SDL_GetColorKey'+' Is OK');
  if SDL_SetSurfaceColorMod=nil then _writetostrings(false,'SDL_SetSurfaceColorMod'+' is NIL ???') else  _writetostrings(true,'SDL_SetSurfaceColorMod'+' Is OK');
  if SDL_GetSurfaceColorMod=nil then _writetostrings(false,'SDL_GetSurfaceColorMod'+' is NIL ???') else  _writetostrings(true,'SDL_GetSurfaceColorMod'+' Is OK');
  if SDL_SetSurfaceAlphaMod=nil then _writetostrings(false,'SDL_SetSurfaceAlphaMod'+' is NIL ???') else  _writetostrings(true,'SDL_SetSurfaceAlphaMod'+' Is OK');
  if SDL_GetSurfaceAlphaMod=nil then _writetostrings(false,'SDL_GetSurfaceAlphaMod'+' is NIL ???') else  _writetostrings(true,'SDL_GetSurfaceAlphaMod'+' Is OK');
  if SDL_SetSurfaceBlendMode=nil then _writetostrings(false,'SDL_SetSurfaceBlendMode'+' is NIL ???') else  _writetostrings(true,'SDL_SetSurfaceBlendMode'+' Is OK');
  if SDL_GetSurfaceBlendMode=nil then _writetostrings(false,'SDL_GetSurfaceBlendMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetSurfaceBlendMode'+' Is OK');
  if SDL_SetClipRect=nil then _writetostrings(false,'SDL_SetClipRect'+' is NIL ???') else  _writetostrings(true,'SDL_SetClipRect'+' Is OK');
  if SDL_GetClipRect=nil then _writetostrings(false,'SDL_GetClipRect'+' is NIL ???') else  _writetostrings(true,'SDL_GetClipRect'+' Is OK');
  if SDL_ConvertSurface=nil then _writetostrings(false,'SDL_ConvertSurface'+' is NIL ???') else  _writetostrings(true,'SDL_ConvertSurface'+' Is OK');
  if SDL_ConvertSurfaceFormat=nil then _writetostrings(false,'SDL_ConvertSurfaceFormat'+' is NIL ???') else  _writetostrings(true,'SDL_ConvertSurfaceFormat'+' Is OK');
  if SDL_ConvertPixels=nil then _writetostrings(false,'SDL_ConvertPixels'+' is NIL ???') else  _writetostrings(true,'SDL_ConvertPixels'+' Is OK');
  if SDL_FillRect=nil then _writetostrings(false,'SDL_FillRect'+' is NIL ???') else  _writetostrings(true,'SDL_FillRect'+' Is OK');
  if SDL_UpperBlit=nil then _writetostrings(false,'SDL_UpperBlit'+' is NIL ???') else  _writetostrings(true,'SDL_UpperBlit'+' Is OK');
  if SDL_LowerBlit=nil then _writetostrings(false,'SDL_LowerBlit'+' is NIL ???') else  _writetostrings(true,'SDL_LowerBlit'+' Is OK');
  if SDL_SoftStretch=nil then _writetostrings(false,'SDL_SoftStretch'+' is NIL ???') else  _writetostrings(true,'SDL_SoftStretch'+' Is OK');
  if SDL_UpperBlitScaled=nil then _writetostrings(false,'SDL_UpperBlitScaled'+' is NIL ???') else  _writetostrings(true,'SDL_UpperBlitScaled'+' Is OK');
  if SDL_LowerBlitScaled=nil then _writetostrings(false,'SDL_LowerBlitScaled'+' is NIL ???') else  _writetostrings(true,'SDL_LowerBlitScaled'+' Is OK');

  if SDL_GetShapedWindowMode=nil then _writetostrings(false,'SDL_GetShapedWindowMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetShapedWindowMode'+' Is OK');
  if SDL_SetWindowShape=nil then _writetostrings(false,'SDL_SetWindowShape'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowShape'+' Is OK');
  if SDL_CreateShapedWindow=nil then _writetostrings(false,'SDL_CreateShapedWindow'+' is NIL ???') else  _writetostrings(true,'SDL_CreateShapedWindow'+' Is OK');
  if SDL_IsShapedWindow=nil then _writetostrings(false,'SDL_IsShapedWindow'+' is NIL ???') else  _writetostrings(true,'SDL_IsShapedWindow'+' Is OK');

  if SDL_GetNumVideoDrivers=nil then _writetostrings(false,'SDL_GetNumVideoDrivers'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumVideoDrivers'+' Is OK');
  if SDL_GetVideoDriver=nil then _writetostrings(false,'SDL_GetVideoDriver'+' is NIL ???') else  _writetostrings(true,'SDL_GetVideoDriver'+' Is OK');
  if SDL_VideoInit=nil then _writetostrings(false,'SDL_VideoInit'+' is NIL ???') else  _writetostrings(true,'SDL_VideoInit'+' Is OK');
  if SDL_VideoQuit=nil then _writetostrings(false,'SDL_VideoQuit'+' is NIL ???') else  _writetostrings(true,'SDL_VideoQuit'+' Is OK');
  if SDL_GetCurrentVideoDriver=nil then _writetostrings(false,'SDL_GetCurrentVideoDriver'+' is NIL ???') else  _writetostrings(true,'SDL_GetCurrentVideoDriver'+' Is OK');
  if SDL_GetNumVideoDisplays=nil then _writetostrings(false,'SDL_GetNumVideoDisplays'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumVideoDisplays'+' Is OK');
  if SDL_GetDisplayName=nil then _writetostrings(false,'SDL_GetDisplayName'+' is NIL ???') else  _writetostrings(true,'SDL_GetDisplayName'+' Is OK');
  if SDL_GetDisplayBounds=nil then _writetostrings(false,'SDL_GetDisplayBounds'+' is NIL ???') else  _writetostrings(true,'SDL_GetDisplayBounds'+' Is OK');
  if SDL_GetDisplayDPI=nil then _writetostrings(false,'SDL_GetDisplayDPI'+' is NIL ???') else  _writetostrings(true,'SDL_GetDisplayDPI'+' Is OK');
  if SDL_GetDisplayUsableBounds=nil then _writetostrings(false,'SDL_GetDisplayUsableBounds'+' is NIL ???') else  _writetostrings(true,'SDL_GetDisplayUsableBounds'+' Is OK');
  if SDL_GetNumDisplayModes=nil then _writetostrings(false,'SDL_GetNumDisplayModes'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumDisplayModes'+' Is OK');
  if SDL_GetDisplayMode=nil then _writetostrings(false,'SDL_GetDisplayMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetDisplayMode'+' Is OK');
  if SDL_GetDesktopDisplayMode=nil then _writetostrings(false,'SDL_GetDesktopDisplayMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetDesktopDisplayMode'+' Is OK');
  if SDL_GetCurrentDisplayMode=nil then _writetostrings(false,'SDL_GetCurrentDisplayMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetCurrentDisplayMode'+' Is OK');
  if SDL_GetClosestDisplayMode=nil then _writetostrings(false,'SDL_GetClosestDisplayMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetClosestDisplayMode'+' Is OK');
  if SDL_GetWindowDisplayIndex=nil then _writetostrings(false,'SDL_GetWindowDisplayIndex'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowDisplayIndex'+' Is OK');
  if SDL_SetWindowDisplayMode=nil then _writetostrings(false,'SDL_SetWindowDisplayMode'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowDisplayMode'+' Is OK');
  if SDL_GetWindowDisplayMode=nil then _writetostrings(false,'SDL_GetWindowDisplayMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowDisplayMode'+' Is OK');
  if SDL_GetWindowPixelFormat=nil then _writetostrings(false,'SDL_GetWindowPixelFormat'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowPixelFormat'+' Is OK');
  if SDL_CreateWindow=nil then _writetostrings(false,'SDL_CreateWindow'+' is NIL ???') else  _writetostrings(true,'SDL_CreateWindow'+' Is OK');
  if SDL_CreateWindowFrom=nil then _writetostrings(false,'SDL_CreateWindowFrom'+' is NIL ???') else  _writetostrings(true,'SDL_CreateWindowFrom'+' Is OK');
  if SDL_GetWindowID=nil then _writetostrings(false,'SDL_GetWindowID'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowID'+' Is OK');
  if SDL_GetWindowFromID=nil then _writetostrings(false,'SDL_GetWindowFromID'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowFromID'+' Is OK');
  if SDL_GetWindowFlags=nil then _writetostrings(false,'SDL_GetWindowFlags'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowFlags'+' Is OK');
  if SDL_SetWindowTitle=nil then _writetostrings(false,'SDL_SetWindowTitle'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowTitle'+' Is OK');
  if SDL_GetWindowTitle=nil then _writetostrings(false,'SDL_GetWindowTitle'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowTitle'+' Is OK');
  if SDL_SetWindowIcon=nil then _writetostrings(false,'SDL_SetWindowIcon'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowIcon'+' Is OK');
  if SDL_SetWindowData=nil then _writetostrings(false,'SDL_SetWindowData'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowData'+' Is OK');
  if SDL_GetWindowData=nil then _writetostrings(false,'SDL_GetWindowData'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowData'+' Is OK');
  if SDL_SetWindowPosition=nil then _writetostrings(false,'SDL_SetWindowPosition'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowPosition'+' Is OK');
  if SDL_GetWindowPosition=nil then _writetostrings(false,'SDL_GetWindowPosition'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowPosition'+' Is OK');
  if SDL_SetWindowSize=nil then _writetostrings(false,'SDL_SetWindowSize'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowSize'+' Is OK');
  if SDL_GetWindowSize=nil then _writetostrings(false,'SDL_GetWindowSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowSize'+' Is OK');
  if SDL_GetWindowBordersSize=nil then _writetostrings(false,'SDL_GetWindowBordersSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowBordersSize'+' Is OK');
  if SDL_SetWindowMinimumSize=nil then _writetostrings(false,'SDL_SetWindowMinimumSize'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowMinimumSize'+' Is OK');
  if SDL_GetWindowMinimumSize=nil then _writetostrings(false,'SDL_GetWindowMinimumSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowMinimumSize'+' Is OK');
  if SDL_SetWindowMaximumSize=nil then _writetostrings(false,'SDL_SetWindowMaximumSize'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowMaximumSize'+' Is OK');
  if SDL_GetWindowMaximumSize=nil then _writetostrings(false,'SDL_GetWindowMaximumSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowMaximumSize'+' Is OK');
  if SDL_SetWindowBordered=nil then _writetostrings(false,'SDL_SetWindowBordered'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowBordered'+' Is OK');
  if SDL_SetWindowResizable=nil then _writetostrings(false,'SDL_SetWindowResizable'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowResizable'+' Is OK');
  if SDL_ShowWindow=nil then _writetostrings(false,'SDL_ShowWindow'+' is NIL ???') else  _writetostrings(true,'SDL_ShowWindow'+' Is OK');
  if SDL_HideWindow=nil then _writetostrings(false,'SDL_HideWindow'+' is NIL ???') else  _writetostrings(true,'SDL_HideWindow'+' Is OK');
  if SDL_RaiseWindow=nil then _writetostrings(false,'SDL_RaiseWindow'+' is NIL ???') else  _writetostrings(true,'SDL_RaiseWindow'+' Is OK');
  if SDL_MaximizeWindow=nil then _writetostrings(false,'SDL_MaximizeWindow'+' is NIL ???') else  _writetostrings(true,'SDL_MaximizeWindow'+' Is OK');
  if SDL_MinimizeWindow=nil then _writetostrings(false,'SDL_MinimizeWindow'+' is NIL ???') else  _writetostrings(true,'SDL_MinimizeWindow'+' Is OK');
  if SDL_RestoreWindow=nil then _writetostrings(false,'SDL_RestoreWindow'+' is NIL ???') else  _writetostrings(true,'SDL_RestoreWindow'+' Is OK');
  if SDL_SetWindowFullscreen=nil then _writetostrings(false,'SDL_SetWindowFullscreen'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowFullscreen'+' Is OK');
  if SDL_GetWindowSurface=nil then _writetostrings(false,'SDL_GetWindowSurface'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowSurface'+' Is OK');
  if SDL_UpdateWindowSurface=nil then _writetostrings(false,'SDL_UpdateWindowSurface'+' is NIL ???') else  _writetostrings(true,'SDL_UpdateWindowSurface'+' Is OK');
  if SDL_UpdateWindowSurfaceRects=nil then _writetostrings(false,'SDL_UpdateWindowSurfaceRects'+' is NIL ???') else  _writetostrings(true,'SDL_UpdateWindowSurfaceRects'+' Is OK');
  if SDL_SetWindowGrab=nil then _writetostrings(false,'SDL_SetWindowGrab'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowGrab'+' Is OK');
  if SDL_GetWindowGrab=nil then _writetostrings(false,'SDL_GetWindowGrab'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowGrab'+' Is OK');
  if SDL_GetGrabbedWindow=nil then _writetostrings(false,'SDL_GetGrabbedWindow'+' is NIL ???') else  _writetostrings(true,'SDL_GetGrabbedWindow'+' Is OK');
  if SDL_SetWindowBrightness=nil then _writetostrings(false,'SDL_SetWindowBrightness'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowBrightness'+' Is OK');
  if SDL_GetWindowBrightness=nil then _writetostrings(false,'SDL_GetWindowBrightness'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowBrightness'+' Is OK');
  if SDL_SetWindowOpacity=nil then _writetostrings(false,'SDL_SetWindowOpacity'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowOpacity'+' Is OK');
  if SDL_GetWindowOpacity=nil then _writetostrings(false,'SDL_GetWindowOpacity'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowOpacity'+' Is OK');
  if SDL_SetWindowModalFor=nil then _writetostrings(false,'SDL_SetWindowModalFor'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowModalFor'+' Is OK');
  if SDL_SetWindowInputFocus=nil then _writetostrings(false,'SDL_SetWindowInputFocus'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowInputFocus'+' Is OK');
  if SDL_SetWindowGammaRamp=nil then _writetostrings(false,'SDL_SetWindowGammaRamp'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowGammaRamp'+' Is OK');
  if SDL_GetWindowGammaRamp=nil then _writetostrings(false,'SDL_GetWindowGammaRamp'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowGammaRamp'+' Is OK');
  if SDL_SetWindowHitTest=nil then _writetostrings(false,'SDL_SetWindowHitTest'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowHitTest'+' Is OK');
  if SDL_DestroyWindow=nil then _writetostrings(false,'SDL_DestroyWindow'+' is NIL ???') else  _writetostrings(true,'SDL_DestroyWindow'+' Is OK');
  if SDL_IsScreenSaverEnabled=nil then _writetostrings(false,'SDL_IsScreenSaverEnabled'+' is NIL ???') else  _writetostrings(true,'SDL_IsScreenSaverEnabled'+' Is OK');
  if SDL_EnableScreenSaver=nil then _writetostrings(false,'SDL_EnableScreenSaver'+' is NIL ???') else  _writetostrings(true,'SDL_EnableScreenSaver'+' Is OK');
  if SDL_DisableScreenSaver=nil then _writetostrings(false,'SDL_DisableScreenSaver'+' is NIL ???') else  _writetostrings(true,'SDL_DisableScreenSaver'+' Is OK');

  if SDL_GL_LoadLibrary=nil then _writetostrings(false,'SDL_GL_LoadLibrary'+' is NIL ???') else  _writetostrings(true,'SDL_GL_LoadLibrary'+' Is OK');
  if SDL_GL_GetProcAddress=nil then _writetostrings(false,'SDL_GL_GetProcAddress'+' is NIL ???') else  _writetostrings(true,'SDL_GL_GetProcAddress'+' Is OK');
  if SDL_GL_UnloadLibrary=nil then _writetostrings(false,'SDL_GL_UnloadLibrary'+' is NIL ???') else  _writetostrings(true,'SDL_GL_UnloadLibrary'+' Is OK');
  if SDL_GL_ExtensionSupported=nil then _writetostrings(false,'SDL_GL_ExtensionSupported'+' is NIL ???') else  _writetostrings(true,'SDL_GL_ExtensionSupported'+' Is OK');
  if SDL_GL_ResetAttributes=nil then _writetostrings(false,'SDL_GL_ResetAttributes'+' is NIL ???') else  _writetostrings(true,'SDL_GL_ResetAttributes'+' Is OK');
  if SDL_GL_SetAttribute=nil then _writetostrings(false,'SDL_GL_SetAttribute'+' is NIL ???') else  _writetostrings(true,'SDL_GL_SetAttribute'+' Is OK');
  if SDL_GL_GetAttribute=nil then _writetostrings(false,'SDL_GL_GetAttribute'+' is NIL ???') else  _writetostrings(true,'SDL_GL_GetAttribute'+' Is OK');
  if SDL_GL_CreateContext=nil then _writetostrings(false,'SDL_GL_CreateContext'+' is NIL ???') else  _writetostrings(true,'SDL_GL_CreateContext'+' Is OK');
  if SDL_GL_MakeCurrent=nil then _writetostrings(false,'SDL_GL_MakeCurrent'+' is NIL ???') else  _writetostrings(true,'SDL_GL_MakeCurrent'+' Is OK');
  if SDL_GL_GetCurrentWindow=nil then _writetostrings(false,'SDL_GL_GetCurrentWindow'+' is NIL ???') else  _writetostrings(true,'SDL_GL_GetCurrentWindow:'+' Is OK');
  if SDL_GL_GetCurrentContext=nil then _writetostrings(false,'SDL_GL_GetCurrentContext'+' is NIL ???') else  _writetostrings(true,'SDL_GL_GetCurrentContext'+' Is OK');
  if SDL_GL_GetDrawableSize=nil then _writetostrings(false,'SDL_GL_GetDrawableSize'+' is NIL ???') else  _writetostrings(true,'SDL_GL_GetDrawableSize'+' Is OK');
  if SDL_GL_SetSwapInterval=nil then _writetostrings(false,'SDL_GL_SetSwapInterval'+' is NIL ???') else  _writetostrings(true,'SDL_GL_SetSwapInterval'+' Is OK');
  if SDL_GL_GetSwapInterval=nil then _writetostrings(false,'SDL_GL_GetSwapInterval'+' is NIL ???') else  _writetostrings(true,'SDL_GL_GetSwapInterval'+' Is OK');
  if SDL_GL_SwapWindow=nil then _writetostrings(false,'SDL_GL_SwapWindow'+' is NIL ???') else  _writetostrings(true,'SDL_GL_SwapWindow'+' Is OK');
  if SDL_GL_DeleteContext=nil then _writetostrings(false,'SDL_GL_DeleteContext'+' is NIL ???') else  _writetostrings(true,'SDL_GL_DeleteContext'+' Is OK');

  if SDL_SetHintWithPriority=nil then _writetostrings(false,'SDL_SetHintWithPriority'+' is NIL ???') else  _writetostrings(true,'SDL_SetHintWithPriority'+' Is OK');
  if SDL_SetHint=nil then _writetostrings(false,'SDL_SetHint'+' is NIL ???') else  _writetostrings(true,'SDL_SetHint'+' Is OK');
  if SDL_GetHint=nil then _writetostrings(false,'SDL_GetHint'+' is NIL ???') else  _writetostrings(true,'SDL_GetHint'+' Is OK');
  if SDL_GetHintBoolean=nil then _writetostrings(false,'SDL_GetHintBoolean'+' is NIL ???') else  _writetostrings(true,'SDL_GetHintBoolean'+' Is OK');
  if SDL_AddHintCallback=nil then _writetostrings(false,'SDL_AddHintCallback'+' is NIL ???') else  _writetostrings(true,'SDL_AddHintCallback'+' Is OK');
  if SDL_DelHintCallback=nil then _writetostrings(false,'SDL_DelHintCallback'+' is NIL ???') else  _writetostrings(true,'SDL_DelHintCallback'+' Is OK');
  if SDL_ClearHints=nil then _writetostrings(false,'SDL_ClearHints'+' is NIL ???') else  _writetostrings(true,'SDL_ClearHints'+' Is OK');
  if SDL_LoadObject=nil then _writetostrings(false,'SDL_LoadObject'+' is NIL ???') else  _writetostrings(true,'SDL_LoadObject'+' Is OK');
  if SDL_LoadFunction=nil then _writetostrings(false,'SDL_LoadFunction'+' is NIL ???') else  _writetostrings(true,'SDL_LoadFunction'+' Is OK');
  if SDL_UnloadObject=nil then _writetostrings(false,'SDL_UnloadObject'+' is NIL ???') else  _writetostrings(true,'SDL_UnloadObject'+' Is OK');

  if SDL_ShowMessageBox=nil then _writetostrings(false,'SDL_ShowMessageBox'+' is NIL ???') else  _writetostrings(true,'SDL_ShowMessageBox'+' Is OK');
  if SDL_ShowSimpleMessageBox=nil then _writetostrings(false,'SDL_ShowSimpleMessageBox'+' is NIL ???') else  _writetostrings(true,'SDL_ShowSimpleMessageBox'+' Is OK');

  if SDL_GetNumRenderDrivers=nil then _writetostrings(false,'SDL_GetNumRenderDrivers'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumRenderDrivers'+' Is OK');
  if SDL_GetRenderDriverInfo=nil then _writetostrings(false,'SDL_GetRenderDriverInfo'+' is NIL ???') else  _writetostrings(true,'SDL_GetRenderDriverInfo'+' Is OK');
  if SDL_CreateWindowAndRenderer=nil then _writetostrings(false,'SDL_CreateWindowAndRenderer'+' is NIL ???') else  _writetostrings(true,'SDL_CreateWindowAndRenderer'+' Is OK');
  if SDL_CreateRenderer=nil then _writetostrings(false,'SDL_CreateRenderer'+' is NIL ???') else  _writetostrings(true,'SDL_CreateRenderer'+' Is OK');
  if SDL_CreateSoftwareRenderer=nil then _writetostrings(false,'SDL_CreateSoftwareRenderer'+' is NIL ???') else  _writetostrings(true,'SDL_CreateSoftwareRenderer'+' Is OK');
  if SDL_GetRenderer=nil then _writetostrings(false,'SDL_GetRenderer'+' is NIL ???') else  _writetostrings(true,'SDL_GetRenderer'+' Is OK');
  if SDL_GetRendererInfo=nil then _writetostrings(false,'SDL_GetRendererInfo'+' is NIL ???') else  _writetostrings(true,'SDL_GetRendererInfo'+' Is OK');
  if SDL_GetRendererOutputSize=nil then _writetostrings(false,'SDL_GetRendererOutputSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetRendererOutputSize'+' Is OK');
  if SDL_CreateTexture=nil then _writetostrings(false,'SDL_CreateTexture'+' is NIL ???') else  _writetostrings(true,'SDL_CreateTexture'+' Is OK');
  if SDL_CreateTextureFromSurface=nil then _writetostrings(false,'SDL_CreateTextureFromSurface'+' is NIL ???') else  _writetostrings(true,'SDL_CreateTextureFromSurface'+' Is OK');
  if SDL_QueryTexture=nil then _writetostrings(false,'SDL_QueryTexture'+' is NIL ???') else  _writetostrings(true,'SDL_QueryTexture'+' Is OK');
  if SDL_SetTextureColorMod=nil then _writetostrings(false,'SDL_SetTextureColorMod'+' is NIL ???') else  _writetostrings(true,'SDL_SetTextureColorMod'+' Is OK');
  if SDL_GetTextureColorMod=nil then _writetostrings(false,'SDL_GetTextureColorMod'+' is NIL ???') else  _writetostrings(true,'SDL_GetTextureColorMod'+' Is OK');
  if SDL_SetTextureAlphaMod=nil then _writetostrings(false,'SDL_SetTextureAlphaMod'+' is NIL ???') else  _writetostrings(true,'SDL_SetTextureAlphaMod'+' Is OK');
  if SDL_GetTextureAlphaMod=nil then _writetostrings(false,'SDL_GetTextureAlphaMod'+' is NIL ???') else  _writetostrings(true,'SDL_GetTextureAlphaMod'+' Is OK');
  if SDL_SetTextureBlendMode=nil then _writetostrings(false,'SDL_SetTextureBlendMode'+' is NIL ???') else  _writetostrings(true,'SDL_SetTextureBlendMode'+' Is OK');
  if SDL_GetTextureBlendMode=nil then _writetostrings(false,'SDL_GetTextureBlendMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetTextureBlendMode'+' Is OK');
  if SDL_UpdateTexture=nil then _writetostrings(false,'SDL_UpdateTexture'+' is NIL ???') else  _writetostrings(true,'SDL_UpdateTexture'+' Is OK');
  if SDL_LockTexture=nil then _writetostrings(false,'SDL_LockTexture'+' is NIL ???') else  _writetostrings(true,'SDL_LockTexture'+' Is OK');
  if SDL_UnlockTexture=nil then _writetostrings(false,'SDL_UnlockTexture'+' is NIL ???') else  _writetostrings(true,'SDL_UnlockTexture'+' Is OK');
  if SDL_RenderTargetSupported=nil then _writetostrings(false,'SDL_RenderTargetSupported'+' is NIL ???') else  _writetostrings(true,'SDL_RenderTargetSupported'+' Is OK');
  if SDL_SetRenderTarget=nil then _writetostrings(false,'SDL_SetRenderTarget'+' is NIL ???') else  _writetostrings(true,'SDL_SetRenderTarget'+' Is OK');
  if SDL_GetRenderTarget=nil then _writetostrings(false,'SDL_GetRenderTarget'+' is NIL ???') else  _writetostrings(true,'SDL_GetRenderTarget'+' Is OK');
  if SDL_RenderSetLogicalSize=nil then _writetostrings(false,'SDL_RenderSetLogicalSize'+' is NIL ???') else  _writetostrings(true,'SDL_RenderSetLogicalSize'+' Is OK');
  if SDL_RenderGetLogicalSize=nil then _writetostrings(false,'SDL_RenderGetLogicalSize'+' is NIL ???') else  _writetostrings(true,'SDL_RenderGetLogicalSize'+' Is OK');
  if SDL_RenderSetViewport=nil then _writetostrings(false,'SDL_RenderSetViewport'+' is NIL ???') else  _writetostrings(true,'SDL_RenderSetViewport'+' Is OK');
  if SDL_RenderGetViewport=nil then _writetostrings(false,'SDL_RenderGetViewport'+' is NIL ???') else  _writetostrings(true,'SDL_RenderGetViewport'+' Is OK');
  if SDL_RenderSetClipRect=nil then _writetostrings(false,'SDL_RenderSetClipRect'+' is NIL ???') else  _writetostrings(true,'SDL_RenderSetClipRect'+' Is OK');
  if SDL_RenderGetClipRect=nil then _writetostrings(false,'SDL_RenderGetClipRect'+' is NIL ???') else  _writetostrings(true,'SDL_RenderGetClipRect'+' Is OK');
  if SDL_RenderIsClipEnabled=nil then _writetostrings(false,'SDL_RenderIsClipEnabled'+' is NIL ???') else  _writetostrings(true,'SDL_RenderIsClipEnabled'+' Is OK');
  if SDL_RenderSetScale=nil then _writetostrings(false,'SDL_RenderSetScale'+' is NIL ???') else  _writetostrings(true,'SDL_RenderSetScale'+' Is OK');
  if SDL_RenderGetScale=nil then _writetostrings(false,'SDL_RenderGetScale'+' is NIL ???') else  _writetostrings(true,'SDL_RenderGetScale'+' Is OK');
  if SDL_SetRenderDrawColor=nil then _writetostrings(false,'SDL_SetRenderDrawColor'+' is NIL ???') else  _writetostrings(true,'SDL_SetRenderDrawColor'+' Is OK');
  if SDL_GetRenderDrawColor=nil then _writetostrings(false,'SDL_GetRenderDrawColor'+' is NIL ???') else  _writetostrings(true,'SDL_GetRenderDrawColor'+' Is OK');
  if SDL_SetRenderDrawBlendMode=nil then _writetostrings(false,'SDL_SetRenderDrawBlendMode'+' is NIL ???') else  _writetostrings(true,'SDL_SetRenderDrawBlendMode'+' Is OK');
  if SDL_GetRenderDrawBlendMode=nil then _writetostrings(false,'SDL_GetRenderDrawBlendMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetRenderDrawBlendMode'+' Is OK');
  if SDL_RenderClear=nil then _writetostrings(false,'SDL_RenderClear'+' is NIL ???') else  _writetostrings(true,'SDL_RenderClear'+' Is OK');
  if SDL_RenderDrawPoint=nil then _writetostrings(false,'SDL_RenderDrawPoint'+' is NIL ???') else  _writetostrings(true,'SDL_RenderDrawPoint'+' Is OK');
  if SDL_RenderDrawPoints=nil then _writetostrings(false,'SDL_RenderDrawPoints'+' is NIL ???') else  _writetostrings(true,'SDL_RenderDrawPoints'+' Is OK');
  if SDL_RenderDrawLine=nil then _writetostrings(false,'SDL_RenderDrawLine'+' is NIL ???') else  _writetostrings(true,'SDL_RenderDrawLine'+' Is OK');
  if SDL_RenderDrawLines=nil then _writetostrings(false,'SDL_RenderDrawLines'+' is NIL ???') else  _writetostrings(true,'SDL_RenderDrawLines'+' Is OK');
  if SDL_RenderDrawRect=nil then _writetostrings(false,'SDL_RenderDrawRect'+' is NIL ???') else  _writetostrings(true,'SDL_RenderDrawRect'+' Is OK');
  if SDL_RenderDrawRects=nil then _writetostrings(false,'SDL_RenderDrawRects'+' is NIL ???') else  _writetostrings(true,'SDL_RenderDrawRects'+' Is OK');
  if SDL_RenderFillRect=nil then _writetostrings(false,'SDL_RenderFillRect'+' is NIL ???') else  _writetostrings(true,'SDL_RenderFillRect'+' Is OK');
  if SDL_RenderFillRects=nil then _writetostrings(false,'SDL_RenderFillRects'+' is NIL ???') else  _writetostrings(true,'SDL_RenderFillRects'+' Is OK');
  if SDL_RenderCopy=nil then _writetostrings(false,'SDL_RenderCopy'+' is NIL ???') else  _writetostrings(true,'SDL_RenderCopy'+' Is OK');
  if SDL_RenderCopyEx=nil then _writetostrings(false,'SDL_RenderCopyEx'+' is NIL ???') else  _writetostrings(true,'SDL_RenderCopyEx'+' Is OK');
  if SDL_RenderReadPixels=nil then _writetostrings(false,'SDL_RenderReadPixels'+' is NIL ???') else  _writetostrings(true,'SDL_RenderReadPixels'+' Is OK');
  if SDL_RenderPresent=nil then _writetostrings(false,'SDL_RenderPresent'+' is NIL ???') else  _writetostrings(true,'SDL_RenderPresent'+' Is OK');
  if SDL_DestroyTexture=nil then _writetostrings(false,'SDL_DestroyTexture'+' is NIL ???') else  _writetostrings(true,'SDL_DestroyTexture'+' Is OK');
  if SDL_DestroyRenderer=nil then _writetostrings(false,'SDL_DestroyRenderer'+' is NIL ???') else  _writetostrings(true,'SDL_DestroyRenderer'+' Is OK');
  if SDL_GL_BindTexture=nil then _writetostrings(false,'SDL_GL_BindTexture'+' is NIL ???') else  _writetostrings(true,'SDL_GL_BindTexture'+' Is OK');
  if SDL_GL_UnbindTexture=nil then _writetostrings(false,'SDL_GL_UnbindTexture'+' is NIL ???') else  _writetostrings(true,'SDL_GL_UnbindTexture'+' Is OK');
  if SDL_UpdateYUVTexture=nil then _writetostrings(false,'SDL_UpdateYUVTexture'+' is NIL ???') else  _writetostrings(true,'SDL_UpdateYUVTexture'+' Is OK');

  if SDL_GetKeyboardFocus=nil then _writetostrings(false,'SDL_GetKeyboardFocus'+' is NIL ???') else  _writetostrings(true,'SDL_GetKeyboardFocus'+' Is OK');
  if SDL_GetKeyboardState=nil then _writetostrings(false,'SDL_GetKeyboardState'+' is NIL ???') else  _writetostrings(true,'SDL_GetKeyboardState'+' Is OK');
  if SDL_GetModState=nil then _writetostrings(false,'SDL_GetModState'+' is NIL ???') else  _writetostrings(true,'SDL_GetModState'+' Is OK');
  if SDL_SetModState=nil then _writetostrings(false,'SDL_SetModState'+' is NIL ???') else  _writetostrings(true,'SDL_SetModState'+' Is OK');
  if SDL_GetKeyFromScancode=nil then _writetostrings(false,'SDL_GetKeyFromScancode'+' is NIL ???') else  _writetostrings(true,'SDL_GetKeyFromScancode'+' Is OK');
  if SDL_GetScancodeFromKey=nil then _writetostrings(false,'SDL_GetScancodeFromKey'+' is NIL ???') else  _writetostrings(true,'SDL_GetScancodeFromKey'+' Is OK');
  if SDL_GetScancodeName=nil then _writetostrings(false,'SDL_GetScancodeName'+' is NIL ???') else  _writetostrings(true,'SDL_GetScancodeName'+' Is OK');
  if SDL_GetScancodeFromName=nil then _writetostrings(false,'SDL_GetScancodeFromName'+' is NIL ???') else  _writetostrings(true,'SDL_GetScancodeFromName'+' Is OK');
  if SDL_GetKeyName=nil then _writetostrings(false,'SDL_GetKeyName'+' is NIL ???') else  _writetostrings(true,'SDL_GetKeyName'+' Is OK');
  if SDL_GetKeyFromName=nil then _writetostrings(false,'SDL_GetKeyFromName'+' is NIL ???') else  _writetostrings(true,'SDL_GetKeyFromName'+' Is OK');
  if SDL_StartTextInput=nil then _writetostrings(false,'SDL_StartTextInput'+' is NIL ???') else  _writetostrings(true,'SDL_StartTextInput'+' Is OK');
  if SDL_IsTextInputActive=nil then _writetostrings(false,'SDL_IsTextInputActive'+' is NIL ???') else  _writetostrings(true,'SDL_IsTextInputActive'+' Is OK');
  if SDL_StopTextInput=nil then _writetostrings(false,'SDL_StopTextInput'+' is NIL ???') else  _writetostrings(true,'SDL_StopTextInput'+' Is OK');
  if SDL_SetTextInputRect=nil then _writetostrings(false,'SDL_SetTextInputRect'+' is NIL ???') else  _writetostrings(true,'SDL_SetTextInputRect'+' Is OK');
  if SDL_HasScreenKeyboardSupport=nil then _writetostrings(false,'SDL_HasScreenKeyboardSupport'+' is NIL ???') else  _writetostrings(true,'SDL_HasScreenKeyboardSupport'+' Is OK');
  if SDL_IsScreenKeyboardShown=nil then _writetostrings(false,'SDL_IsScreenKeyboardShown'+' is NIL ???') else  _writetostrings(true,'SDL_IsScreenKeyboardShown'+' Is OK');

  if SDL_GetMouseFocus=nil then _writetostrings(false,'SDL_GetMouseFocus'+' is NIL ???') else  _writetostrings(true,'SDL_GetMouseFocus'+' Is OK');
  if SDL_GetMouseState=nil then _writetostrings(false,'SDL_GetMouseState'+' is NIL ???') else  _writetostrings(true,'SDL_GetMouseState'+' Is OK');
  if SDL_GetGlobalMouseState=nil then _writetostrings(false,'SDL_GetGlobalMouseState'+' is NIL ???') else  _writetostrings(true,'SDL_GetGlobalMouseState'+' Is OK');
  if SDL_GetRelativeMouseState=nil then _writetostrings(false,'SDL_GetRelativeMouseState'+' is NIL ???') else  _writetostrings(true,'SDL_GetRelativeMouseState'+' Is OK');
  if SDL_WarpMouseInWindow=nil then _writetostrings(false,'SDL_WarpMouseInWindow'+' is NIL ???') else  _writetostrings(true,'SDL_WarpMouseInWindow'+' Is OK');
  if SDL_WarpMouseGlobal=nil then _writetostrings(false,'SDL_WarpMouseGlobal'+' is NIL ???') else  _writetostrings(true,'SDL_WarpMouseGlobal'+' Is OK');
  if SDL_SetRelativeMouseMode=nil then _writetostrings(false,'SDL_SetRelativeMouseMode'+' is NIL ???') else  _writetostrings(true,'SDL_SetRelativeMouseMode'+' Is OK');
  if SDL_CaptureMouse=nil then _writetostrings(false,'SDL_CaptureMouse'+' is NIL ???') else  _writetostrings(true,'SDL_CaptureMouse'+' Is OK');
  if SDL_GetRelativeMouseMode=nil then _writetostrings(false,'SDL_GetRelativeMouseMode'+' is NIL ???') else  _writetostrings(true,'SDL_GetRelativeMouseMode'+' Is OK');
  if SDL_CreateCursor=nil then _writetostrings(false,'SDL_CreateCursor'+' is NIL ???') else  _writetostrings(true,'SDL_CreateCursor'+' Is OK');
  if SDL_CreateColorCursor=nil then _writetostrings(false,'SDL_CreateColorCursor'+' is NIL ???') else  _writetostrings(true,'SDL_CreateColorCursor'+' Is OK');
  if SDL_CreateSystemCursor=nil then _writetostrings(false,'SDL_CreateSystemCursor'+' is NIL ???') else  _writetostrings(true,'SDL_CreateSystemCursor'+' Is OK');
  if SDL_SetCursor=nil then _writetostrings(false,'SDL_SetCursor'+' is NIL ???') else  _writetostrings(true,'SDL_SetCursor'+' Is OK');
  if SDL_GetCursor=nil then _writetostrings(false,'SDL_GetCursor'+' is NIL ???') else  _writetostrings(true,'SDL_GetCursor'+' Is OK');
  if SDL_GetDefaultCursor=nil then _writetostrings(false,'SDL_GetDefaultCursor'+' is NIL ???') else  _writetostrings(true,'SDL_GetDefaultCursor'+' Is OK');
  if SDL_FreeCursor=nil then _writetostrings(false,'SDL_FreeCursor'+' is NIL ???') else  _writetostrings(true,'SDL_FreeCursor'+' Is OK');
  if SDL_ShowCursor=nil then _writetostrings(false,'SDL_ShowCursor'+' is NIL ???') else  _writetostrings(true,'SDL_ShowCursor'+' Is OK');

  if SDL_NumJoysticks=nil then _writetostrings(false,'SDL_NumJoysticks'+' is NIL ???') else  _writetostrings(true,'SDL_NumJoysticks'+' Is OK');
  if SDL_JoystickNameForIndex=nil then _writetostrings(false,'SDL_JoystickNameForIndex'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickNameForIndex'+' Is OK');
  if SDL_JoystickOpen=nil then _writetostrings(false,'SDL_JoystickOpen'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickOpen'+' Is OK');
  if SDL_JoystickFromInstanceID=nil then _writetostrings(false,'SDL_JoystickFromInstanceID'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickFromInstanceID'+' Is OK');
  if SDL_JoystickName=nil then _writetostrings(false,'SDL_JoystickName'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickName'+' Is OK');
  if SDL_JoystickGetDeviceGUID=nil then _writetostrings(false,'SDL_JoystickGetDeviceGUID'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetDeviceGUID'+' Is OK');
  if SDL_JoystickGetGUID=nil then _writetostrings(false,'SDL_JoystickGetGUID'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetGUID'+' Is OK');
  if SDL_JoystickGetGUIDString=nil then _writetostrings(false,'SDL_JoystickGetGUIDString'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetGUIDString'+' Is OK');
  if SDL_JoystickGetGUIDFromString=nil then _writetostrings(false,'SDL_JoystickGetGUIDFromString'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetGUIDFromString'+' Is OK');
  if SDL_JoystickGetAttached=nil then _writetostrings(false,'SDL_JoystickGetAttached'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetAttached'+' Is OK');
  if SDL_JoystickInstanceID=nil then _writetostrings(false,'SDL_JoystickInstanceID'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickInstanceID'+' Is OK');
  if SDL_JoystickNumAxes=nil then _writetostrings(false,'SDL_JoystickNumAxes'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickNumAxes'+' Is OK');
  if SDL_JoystickNumBalls=nil then _writetostrings(false,'SDL_JoystickNumBalls'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickNumBalls'+' Is OK');
  if SDL_JoystickNumHats=nil then _writetostrings(false,'SDL_JoystickNumHats'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickNumHats'+' Is OK');
  if SDL_JoystickNumButtons=nil then _writetostrings(false,'SDL_JoystickNumButtons'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickNumButtons'+' Is OK');
  if SDL_JoystickUpdate=nil then _writetostrings(false,'SDL_JoystickUpdate'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickUpdate'+' Is OK');
  if SDL_JoystickEventState=nil then _writetostrings(false,'SDL_JoystickEventState'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickEventState'+' Is OK');
  if SDL_JoystickGetAxis=nil then _writetostrings(false,'SDL_JoystickGetAxis'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetAxis'+' Is OK');
  if SDL_JoystickGetHat=nil then _writetostrings(false,'SDL_JoystickGetHat'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetHat'+' Is OK');
  if SDL_JoystickGetBall=nil then _writetostrings(false,'SDL_JoystickGetBall'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetBall'+' Is OK');
  if SDL_JoystickGetButton=nil then _writetostrings(false,'SDL_JoystickGetButton'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickGetButton'+' Is OK');
  if SDL_JoystickClose=nil then _writetostrings(false,'SDL_JoystickClose'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickClose'+' Is OK');
  if SDL_JoystickCurrentPowerLevel=nil then _writetostrings(false,'SDL_JoystickCurrentPowerLevel'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickCurrentPowerLevel'+' Is OK');

  if SDL_GameControllerAddMapping=nil then _writetostrings(false,'SDL_GameControllerAddMapping'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerAddMapping'+' Is OK');
  if SDL_GameControllerAddMappingsFromRW=nil then _writetostrings(false,'SDL_GameControllerAddMappingsFromRW'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerAddMappingsFromRW'+' Is OK');
  if SDL_GameControllerMappingForGUID=nil then _writetostrings(false,'SDL_GameControllerMappingForGUID'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerMappingForGUID'+' Is OK');
  if SDL_GameControllerMapping=nil then _writetostrings(false,'SDL_GameControllerMapping'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerMapping'+' Is OK');
  if SDL_IsGameController=nil then _writetostrings(false,'SDL_IsGameController'+' is NIL ???') else  _writetostrings(true,'SDL_IsGameController'+' Is OK');
  if SDL_GameControllerNameForIndex=nil then _writetostrings(false,'SDL_GameControllerNameForIndex'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerNameForIndex'+' Is OK');
  if SDL_GameControllerOpen=nil then _writetostrings(false,'SDL_GameControllerOpen'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerOpen'+' Is OK');
  if SDL_GameControllerFromInstanceID=nil then _writetostrings(false,'SDL_GameControllerFromInstanceID'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerFromInstanceID'+' Is OK');
  if SDL_GameControllerName=nil then _writetostrings(false,'SDL_GameControllerName'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerName'+' Is OK');
  if SDL_GameControllerGetAttached=nil then _writetostrings(false,'SDL_GameControllerGetAttached'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetAttached'+' Is OK');
  if SDL_GameControllerGetJoystick=nil then _writetostrings(false,'SDL_GameControllerGetJoystick'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetJoystick'+' Is OK');
  if SDL_GameControllerEventState=nil then _writetostrings(false,'SDL_GameControllerEventState'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerEventState'+' Is OK');
  if SDL_GameControllerUpdate=nil then _writetostrings(false,'SDL_GameControllerUpdate'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerUpdate'+' Is OK');
  if SDL_GameControllerGetAxisFromString=nil then _writetostrings(false,'SDL_GameControllerGetAxisFromString'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetAxisFromString'+' Is OK');
  if SDL_GameControllerGetStringForAxis=nil then _writetostrings(false,'SDL_GameControllerGetStringForAxis'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetStringForAxis'+' Is OK');
  if SDL_GameControllerGetBindForAxis=nil then _writetostrings(false,'SDL_GameControllerGetBindForAxis'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetBindForAxis'+' Is OK');
  if SDL_GameControllerGetAxis=nil then _writetostrings(false,'SDL_GameControllerGetAxis'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetAxis'+' Is OK');
  if SDL_GameControllerGetButtonFromString=nil then _writetostrings(false,'SDL_GameControllerGetButtonFromString'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetButtonFromString'+' Is OK');
  if SDL_GameControllerGetStringForButton=nil then _writetostrings(false,'SDL_GameControllerGetStringForButton'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetStringForButton'+' Is OK');
  if SDL_GameControllerGetBindForButton=nil then _writetostrings(false,'SDL_GameControllerGetBindForButton'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetBindForButton'+' Is OK');
  if SDL_GameControllerGetButton=nil then _writetostrings(false,'SDL_GameControllerGetButton'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerGetButton'+' Is OK');
  if SDL_GameControllerClose=nil then _writetostrings(false,'SDL_GameControllerClose'+' is NIL ???') else  _writetostrings(true,'SDL_GameControllerClose'+' Is OK');


  if SDL_NumHaptics=nil then _writetostrings(false,'SDL_NumHaptics'+' is NIL ???') else  _writetostrings(true,'SDL_NumHaptics'+' Is OK');
  if SDL_HapticName=nil then _writetostrings(false,'SDL_HapticName'+' is NIL ???') else  _writetostrings(true,'SDL_HapticName'+' Is OK');
  if SDL_HapticOpen=nil then _writetostrings(false,'SDL_HapticOpen'+' is NIL ???') else  _writetostrings(true,'SDL_HapticOpen'+' Is OK');
  if SDL_HapticOpened=nil then _writetostrings(false,'SDL_HapticOpened'+' is NIL ???') else  _writetostrings(true,'SDL_HapticOpened'+' Is OK');
  if SDL_HapticIndex=nil then _writetostrings(false,'SDL_HapticIndex'+' is NIL ???') else  _writetostrings(true,'SDL_HapticIndex'+' Is OK');
  if SDL_MouseIsHaptic=nil then _writetostrings(false,'SDL_MouseIsHaptic'+' is NIL ???') else  _writetostrings(true,'SDL_MouseIsHaptic'+' Is OK');
  if SDL_HapticOpenFromMouse=nil then _writetostrings(false,'SDL_HapticOpenFromMouse'+' is NIL ???') else  _writetostrings(true,'SDL_HapticOpenFromMouse'+' Is OK');
  if SDL_JoystickIsHaptic=nil then _writetostrings(false,'SDL_JoystickIsHaptic'+' is NIL ???') else  _writetostrings(true,'SDL_JoystickIsHaptic'+' Is OK');
  if SDL_HapticOpenFromJoystick=nil then _writetostrings(false,'SDL_HapticOpenFromJoystick'+' is NIL ???') else  _writetostrings(true,'SDL_HapticOpenFromJoystick'+' Is OK');
  if SDL_HapticClose=nil then _writetostrings(false,'SDL_HapticClose'+' is NIL ???') else  _writetostrings(true,'SDL_HapticClose'+' Is OK');
  if SDL_HapticNumEffects=nil then _writetostrings(false,'SDL_HapticNumEffects'+' is NIL ???') else  _writetostrings(true,'SDL_HapticNumEffects'+' Is OK');
  if SDL_HapticNumEffectsPlaying=nil then _writetostrings(false,'SDL_HapticNumEffectsPlaying'+' is NIL ???') else  _writetostrings(true,'SDL_HapticNumEffectsPlaying'+' Is OK');
  if SDL_HapticQuery=nil then _writetostrings(false,'SDL_HapticQuery'+' is NIL ???') else  _writetostrings(true,'SDL_HapticQuery'+' Is OK');
  if SDL_HapticNumAxes=nil then _writetostrings(false,'SDL_HapticNumAxes'+' is NIL ???') else  _writetostrings(true,'SDL_HapticNumAxes'+' Is OK');
  if SDL_HapticEffectSupported=nil then _writetostrings(false,'SDL_HapticEffectSupported'+' is NIL ???') else  _writetostrings(true,'SDL_HapticEffectSupported'+' Is OK');
  if SDL_HapticNewEffect=nil then _writetostrings(false,'SDL_HapticNewEffect'+' is NIL ???') else  _writetostrings(true,'SDL_HapticNewEffect'+' Is OK');
  if SDL_HapticUpdateEffect=nil then _writetostrings(false,'SDL_HapticUpdateEffect'+' is NIL ???') else  _writetostrings(true,'SDL_HapticUpdateEffect'+' Is OK');
  if SDL_HapticRunEffect=nil then _writetostrings(false,'SDL_HapticRunEffect'+' is NIL ???') else  _writetostrings(true,'SDL_HapticRunEffect'+' Is OK');
  if SDL_HapticStopEffect=nil then _writetostrings(false,'SDL_HapticStopEffect'+' is NIL ???') else  _writetostrings(true,'SDL_HapticStopEffect'+' Is OK');
  if SDL_HapticDestroyEffect=nil then _writetostrings(false,'SDL_HapticDestroyEffect'+' is NIL ???') else  _writetostrings(true,'SDL_HapticDestroyEffect'+' Is OK');
  if SDL_HapticGetEffectStatus=nil then _writetostrings(false,'SDL_HapticGetEffectStatus'+' is NIL ???') else  _writetostrings(true,'SDL_HapticGetEffectStatus'+' Is OK');
  if SDL_HapticSetGain=nil then _writetostrings(false,'SDL_HapticSetGain'+' is NIL ???') else  _writetostrings(true,'SDL_HapticSetGain'+' Is OK');
  if SDL_HapticSetAutocenter=nil then _writetostrings(false,'SDL_HapticSetAutocenter'+' is NIL ???') else  _writetostrings(true,'SDL_HapticSetAutocenter'+' Is OK');
  if SDL_HapticPause=nil then _writetostrings(false,'SDL_HapticPause'+' is NIL ???') else  _writetostrings(true,'SDL_HapticPause'+' Is OK');
  if SDL_HapticUnpause=nil then _writetostrings(false,'SDL_HapticUnpause'+' is NIL ???') else  _writetostrings(true,'SDL_HapticUnpause'+' Is OK');
  if SDL_HapticStopAll=nil then _writetostrings(false,'SDL_HapticStopAll'+' is NIL ???') else  _writetostrings(true,'SDL_HapticStopAll'+' Is OK');
  if SDL_HapticRumbleSupported=nil then _writetostrings(false,'SDL_HapticRumbleSupported'+' is NIL ???') else  _writetostrings(true,'SDL_HapticRumbleSupported'+' Is OK');
  if SDL_HapticRumbleInit=nil then _writetostrings(false,'SDL_HapticRumbleInit'+' is NIL ???') else  _writetostrings(true,'SDL_HapticRumbleInit'+' Is OK');
  if SDL_HapticRumblePlay=nil then _writetostrings(false,'SDL_HapticRumblePlay'+' is NIL ???') else  _writetostrings(true,'SDL_HapticRumblePlay'+' Is OK');
  if SDL_HapticRumbleStop=nil then _writetostrings(false,'SDL_HapticRumbleStop'+' is NIL ???') else  _writetostrings(true,'SDL_HapticRumbleStop'+' Is OK');

  if SDL_GetNumTouchDevices=nil then _writetostrings(false,'SDL_GetNumTouchDevices'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumTouchDevices'+' Is OK');
  if SDL_GetTouchDevice=nil then _writetostrings(false,'SDL_GetTouchDevice'+' is NIL ???') else  _writetostrings(true,'SDL_GetTouchDevice'+' Is OK');
  if SDL_GetNumTouchFingers=nil then _writetostrings(false,'SDL_GetNumTouchFingers'+' is NIL ???') else  _writetostrings(true,'SDL_GetNumTouchFingers'+' Is OK');
  if SDL_GetTouchFinger=nil then _writetostrings(false,'SDL_GetTouchFinger'+' is NIL ???') else  _writetostrings(true,'SDL_GetTouchFinger'+' Is OK');

  if SDL_RecordGesture=nil then _writetostrings(false,'SDL_RecordGesture'+' is NIL ???') else  _writetostrings(true,'SDL_RecordGesture'+' Is OK');
  if SDL_SaveAllDollarTemplates=nil then _writetostrings(false,'SDL_SaveAllDollarTemplates'+' is NIL ???') else  _writetostrings(true,'SDL_SaveAllDollarTemplates'+' Is OK');
  if SDL_SaveDollarTemplate=nil then _writetostrings(false,'SDL_SaveDollarTemplate'+' is NIL ???') else  _writetostrings(true,'SDL_SaveDollarTemplate'+' Is OK');
  if SDL_LoadDollarTemplates=nil then _writetostrings(false,'SDL_LoadDollarTemplates'+' is NIL ???') else  _writetostrings(true,'SDL_LoadDollarTemplates'+' Is OK');

  if SDL_GetWindowWMInfo=nil then _writetostrings(false,'SDL_GetWindowWMInfo'+' is NIL ???') else  _writetostrings(true,'SDL_GetWindowWMInfo'+' Is OK');

  if SDL_PumpEvents=nil then _writetostrings(false,'SDL_PumpEvents'+' is NIL ???') else  _writetostrings(true,'SDL_PumpEvents'+' Is OK');
  if SDL_PeepEvents=nil then _writetostrings(false,'SDL_PeepEvents'+' is NIL ???') else  _writetostrings(true,'SDL_PeepEvents'+' Is OK');
  if SDL_HasEvent=nil then _writetostrings(false,'SDL_HasEvent'+' is NIL ???') else  _writetostrings(true,'SDL_HasEvent'+' Is OK');
  if SDL_HasEvents=nil then _writetostrings(false,'SDL_HasEvents'+' is NIL ???') else  _writetostrings(true,'SDL_HasEvents'+' Is OK');
  if SDL_FlushEvent=nil then _writetostrings(false,'SDL_FlushEvent'+' is NIL ???') else  _writetostrings(true,'SDL_FlushEvent'+' Is OK');
  if SDL_FlushEvents=nil then _writetostrings(false,'SDL_FlushEvents'+' is NIL ???') else  _writetostrings(true,'SDL_FlushEvents'+' Is OK');
  if SDL_PollEvent=nil then _writetostrings(false,'SDL_PollEvent'+' is NIL ???') else  _writetostrings(true,'SDL_PollEvent'+' Is OK');
  if SDL_WaitEvent=nil then _writetostrings(false,'SDL_WaitEvent'+' is NIL ???') else  _writetostrings(true,'SDL_WaitEvent'+' Is OK');
  if SDL_WaitEventTimeout=nil then _writetostrings(false,'SDL_WaitEventTimeout'+' is NIL ???') else  _writetostrings(true,'SDL_WaitEventTimeout'+' Is OK');
  if SDL_PushEvent=nil then _writetostrings(false,'SDL_PushEvent'+' is NIL ???') else  _writetostrings(true,'SDL_PushEvent'+' Is OK');
  if SDL_SetEventFilter=nil then _writetostrings(false,'SDL_SetEventFilter'+' is NIL ???') else  _writetostrings(true,'SDL_SetEventFilter'+' Is OK');
  if SDL_GetEventFilter=nil then _writetostrings(false,'SDL_GetEventFilter'+' is NIL ???') else  _writetostrings(true,'SDL_GetEventFilter'+' Is OK');
  if SDL_AddEventWatch=nil then _writetostrings(false,'SDL_AddEventWatch'+' is NIL ???') else  _writetostrings(true,'SDL_AddEventWatch'+' Is OK');
  if SDL_DelEventWatch=nil then _writetostrings(false,'SDL_DelEventWatch'+' is NIL ???') else  _writetostrings(true,'SDL_DelEventWatch'+' Is OK');
  if SDL_FilterEvents=nil then _writetostrings(false,'SDL_FilterEvents'+' is NIL ???') else  _writetostrings(true,'SDL_FilterEvents'+' Is OK');
  if SDL_EventState=nil then _writetostrings(false,'SDL_EventState'+' is NIL ???') else  _writetostrings(true,'SDL_EventState'+' Is OK');
  if SDL_RegisterEvents=nil then _writetostrings(false,'SDL_RegisterEvents'+' is NIL ???') else  _writetostrings(true,'SDL_RegisterEvents'+' Is OK');
  if SDL_SetClipboardText=nil then _writetostrings(false,'SDL_SetClipboardText'+' is NIL ???') else  _writetostrings(true,'SDL_SetClipboardText'+' Is OK');
  if SDL_GetClipboardText=nil then _writetostrings(false,'SDL_GetClipboardText'+' is NIL ???') else  _writetostrings(true,'SDL_GetClipboardText'+' Is OK');
  if SDL_HasClipboardText=nil then _writetostrings(false,'SDL_HasClipboardText'+' is NIL ???') else  _writetostrings(true,'SDL_HasClipboardText'+' Is OK');
  if SDL_GetCPUCount=nil then _writetostrings(false,'SDL_GetCPUCount'+' is NIL ???') else  _writetostrings(true,'SDL_GetCPUCount'+' Is OK');
  if SDL_GetCPUCacheLineSize=nil then _writetostrings(false,'SDL_GetCPUCacheLineSize'+' is NIL ???') else  _writetostrings(true,'SDL_GetCPUCacheLineSize'+' Is OK');
  if SDL_HasRDTSC=nil then _writetostrings(false,'SDL_HasRDTSC'+' is NIL ???') else  _writetostrings(true,'SDL_HasRDTSC'+' Is OK');
  if SDL_HasAltiVec=nil then _writetostrings(false,'SDL_HasAltiVec'+' is NIL ???') else  _writetostrings(true,'SDL_HasAltiVec'+' Is OK');
  if SDL_HasMMX=nil then _writetostrings(false,'SDL_HasMMX'+' is NIL ???') else  _writetostrings(true,'SDL_HasMMX'+' Is OK');
  if SDL_Has3DNow=nil then _writetostrings(false,'SDL_Has3DNow'+' is NIL ???') else  _writetostrings(true,'SDL_Has3DNow'+' Is OK');
  if SDL_HasSSE=nil then _writetostrings(false,'SDL_HasSSE'+' is NIL ???') else  _writetostrings(true,'SDL_HasSSE'+' Is OK');
  if SDL_HasSSE2=nil then _writetostrings(false,'SDL_HasSSE2'+' is NIL ???') else  _writetostrings(true,'SDL_HasSSE2'+' Is OK');
  if SDL_HasSSE3=nil then _writetostrings(false,'SDL_HasSSE3'+' is NIL ???') else  _writetostrings(true,'SDL_HasSSE3'+' Is OK');
  if SDL_HasSSE41=nil then _writetostrings(false,'SDL_HasSSE41'+' is NIL ???') else  _writetostrings(true,'SDL_HasSSE41'+' Is OK');
  if SDL_HasSSE42=nil then _writetostrings(false,'SDL_HasSSE42'+' is NIL ???') else  _writetostrings(true,'SDL_HasSSE42'+' Is OK');
  if SDL_HasAVX=nil then _writetostrings(false,'SDL_HasAVX'+' is NIL ???') else  _writetostrings(true,'SDL_HasAVX'+' Is OK');
  if SDL_HasAVX2=nil then _writetostrings(false,'SDL_HasAVX2'+' is NIL ???') else  _writetostrings(true,'SDL_HasAVX2'+' Is OK');
  if SDL_GetSystemRAM=nil then _writetostrings(false,'SDL_GetSystemRAM'+' is NIL ???') else  _writetostrings(true,'SDL_GetSystemRAM'+' Is OK');
  if SDL_GetBasePath=nil then _writetostrings(false,'SDL_GetBasePath'+' is NIL ???') else  _writetostrings(true,'SDL_GetBasePath'+' Is OK');
  if SDL_GetPrefPath=nil then _writetostrings(false,'SDL_GetPrefPath'+' is NIL ???') else  _writetostrings(true,'SDL_GetPrefPath'+' Is OK');
  if SDL_LogSetAllPriority=nil then _writetostrings(false,'SDL_LogSetAllPriority'+' is NIL ???') else  _writetostrings(true,'SDL_LogSetAllPriority'+' Is OK');
  if SDL_LogSetPriority=nil then _writetostrings(false,'SDL_LogSetPriority'+' is NIL ???') else  _writetostrings(true,'SDL_LogSetPriority'+' Is OK');
  if SDL_LogGetPriority=nil then _writetostrings(false,'SDL_LogGetPriority'+' is NIL ???') else  _writetostrings(true,'SDL_LogGetPriority'+' Is OK');
  if SDL_LogResetPriorities=nil then _writetostrings(false,'SDL_LogResetPriorities'+' is NIL ???') else  _writetostrings(true,'SDL_LogResetPriorities'+' Is OK');
  if SDL_Log=nil then _writetostrings(false,'SDL_Log'+' is NIL ???') else  _writetostrings(true,'SDL_Log'+' Is OK');
  if SDL_LogVerbose=nil then _writetostrings(false,'SDL_LogVerbose'+' is NIL ???') else  _writetostrings(true,'SDL_LogVerbose'+' Is OK');
  if SDL_LogDebug=nil then _writetostrings(false,'SDL_LogDebug'+' is NIL ???') else  _writetostrings(true,'SDL_LogDebug'+' Is OK');
  if SDL_LogInfo=nil then _writetostrings(false,'SDL_LogInfo'+' is NIL ???') else  _writetostrings(true,'SDL_LogInfo'+' Is OK');
  if SDL_LogWarn=nil then _writetostrings(false,'SDL_LogWarn'+' is NIL ???') else  _writetostrings(true,'SDL_LogWarn'+' Is OK');
  if SDL_LogError=nil then _writetostrings(false,'SDL_LogError'+' is NIL ???') else  _writetostrings(true,'SDL_LogError'+' Is OK');
  if SDL_LogCritical=nil then _writetostrings(false,'SDL_LogCritical'+' is NIL ???') else  _writetostrings(true,'SDL_LogCritical'+' Is OK');
  if SDL_LogMessage=nil then _writetostrings(false,'SDL_LogMessage'+' is NIL ???') else  _writetostrings(true,'SDL_LogMessage'+' Is OK');
  if SDL_LogMessageV=nil then _writetostrings(false,'SDL_LogMessageV'+' is NIL ???') else  _writetostrings(true,'SDL_LogMessageV'+' Is OK');
  if SDL_LogGetOutputFunction=nil then _writetostrings(false,'SDL_LogGetOutputFunction'+' is NIL ???') else  _writetostrings(true,'SDL_LogGetOutputFunction'+' Is OK');
  if SDL_LogSetOutputFunction=nil then _writetostrings(false,'SDL_LogSetOutputFunction'+' is NIL ???') else  _writetostrings(true,'SDL_LogSetOutputFunction'+' Is OK');


{$IFDEF WINDOWS}
  if SDL_SetWindowsMessageHook=nil then _writetostrings(false,'SDL_SetWindowsMessageHook'+' is NIL ???') else  _writetostrings(true,'SDL_SetWindowsMessageHook'+' Is OK');
  if SDL_Direct3D9GetAdapterIndex=nil then _writetostrings(false,'SDL_Direct3D9GetAdapterIndex'+' is NIL ???') else  _writetostrings(true,'SDL_Direct3D9GetAdapterIndex'+' Is OK');
  if SDL_RenderGetD3D9Device=nil then _writetostrings(false,'SDL_RenderGetD3D9Device'+' is NIL ???') else  _writetostrings(true,'SDL_RenderGetD3D9Device'+' Is OK');
  if SDL_DXGIGetOutputInfo=nil then _writetostrings(false,'SDL_DXGIGetOutputInfo'+' is NIL ???') else  _writetostrings(true,'SDL_DXGIGetOutputInf'+' Is OK');
{$IFEND}
{$IFDEF WINCE}
 if SDL_WinRTGetFSPathUNICODE=nil then _writetostrings(false,'SDL_WinRTGetFSPathUNICODE'+' is NIL ???') else  _writetostrings(true,'SDL_WinRTGetFSPathUNICODE'+' Is OK');
 if SDL_WinRTGetFSPathUTF8=nil then _writetostrings(false,'SDL_WinRTGetFSPathUTF8'+' is NIL ???') else  _writetostrings(true,'SDL_WinRTGetFSPathUTF8'+' Is OK');
{$ENDIF}


 if SDL_Init=nil then _writetostrings(false,'SDL_Init'+' is NIL ???') else  _writetostrings(true,'SDL_Init'+' Is OK');
 if SDL_InitSubSystem=nil then _writetostrings(false,'SDL_InitSubSystem'+' is NIL ???') else  _writetostrings(true,'SDL_InitSubSystem'+' Is OK');
 if SDL_QuitSubSystem=nil then _writetostrings(false,'SDL_QuitSubSystem'+' is NIL ???') else  _writetostrings(true,'SDL_QuitSubSystem'+' Is OK');
 if SDL_WasInit=nil then _writetostrings(false,'SDL_WasInit'+' is NIL ???') else  _writetostrings(true,'SDL_WasInit'+' Is OK');
 if SDL_Quit=nil then _writetostrings(false,'SDL_Quit'+' is NIL ???') else  _writetostrings(true,'SDL_Quit'+' Is OK');


 Memo1.Lines.EndUpdate;
end;

end.

