unit demoObj2mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, TplShapeObjects;

type

  { TForm1 }

  TForm1 = class(TForm)
    plEllipse1: TplEllipse;
    plSolidArrow1: TplSolidArrow;
    plStar1: TplStar;
    plText1: TplText;
    plTextBezier1: TplTextBezier;
    procedure FormCreate(Sender: TObject);
    procedure plText1Click(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin

end;

procedure TForm1.plText1Click(Sender: TObject);
begin

end;

end.

