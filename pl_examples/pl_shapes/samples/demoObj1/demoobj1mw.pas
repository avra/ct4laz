unit demoobj1mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, TplShapeObjects;

type

  { TForm1 }

  TForm1 = class(TForm)
    plArc1: TplArc;
    plDrawPicture1: TplDrawPicture;
    plLine1: TplLine;
    plRectangle1: TplRectangle;
    plSolidBezier1: TplSolidBezier;
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

end.

