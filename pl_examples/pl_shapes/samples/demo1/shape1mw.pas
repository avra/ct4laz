{***************************************************************
  This Application is part of CodeTyphon Studio
  by PiloLogic Software House (https://www.pilotlogic.com/)
****************************************************************}

unit shape1mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  TplShapesUnit, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    plCircleShape1: TplCircleShape;
    plSquareShape1: TplSquareShape;
    plStarShape1: TplStarShape;
    Timer1: TTimer;
    procedure plSquareShape1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure plStarShape1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

{ TForm1 }

procedure TForm1.plSquareShape1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  plSquareShape1.Brush.Color:=RGBToColor(random(255),random(255),random(255));
end;

procedure TForm1.plStarShape1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  plStarShape1.Brush.Color:=RGBToColor(random(255),random(255),random(255));
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  plCircleShape1.Brush.Color:=RGBToColor(random(255),random(255),random(255));
end;

initialization
  {$I shape1mw.lrs}

end.

