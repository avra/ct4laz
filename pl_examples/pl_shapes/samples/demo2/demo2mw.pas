unit demo2mw;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  Spin, ComCtrls, StdCtrls, TplShapesUnit;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    plBubbleShape1: TplBubbleShape;
    plHexagonShape1: TplHexagonShape;
    plOctagonShape1: TplOctagonShape;
    plParallelogramShape1: TplParallelogramShape;
    plPentagonShape1: TplPentagonShape;
    plRectangleShape1: TplRectangleShape;
    plStarShape1: TplStarShape;
    plTrapezoidShape1: TplTrapezoidShape;
    plTriangleShape1: TplTriangleShape;
    TrackBar1: TTrackBar;
    procedure TrackBar1Change(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation


procedure TForm1.TrackBar1Change(Sender: TObject);
begin
    plBubbleShape1.Angle:=TrackBar1.Position;
    plHexagonShape1.Angle:=TrackBar1.Position;
    plOctagonShape1.Angle:=TrackBar1.Position;
    plParallelogramShape1.Angle:=TrackBar1.Position;
    plPentagonShape1.Angle:=TrackBar1.Position;
    plTrapezoidShape1.Angle:=TrackBar1.Position;
    plTriangleShape1.Angle:=TrackBar1.Position;
    plStarShape1.Angle:=TrackBar1.Position;
    plRectangleShape1.Angle:=TrackBar1.Position;
end;

initialization
  {$I demo2mw.lrs}

end.

