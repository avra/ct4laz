{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 http://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

unit titan_runtimeapirtti;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, LCLType, LMessages,
  Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Clipbrd, TypInfo, StdCtrls, Buttons;

const
  sssProReadOnly = '(Is ReadOnly)';
  sssInherited = '-->';

type
  EVMTError = class(Exception);

  TDynamicIndexList = array[0..MaxInt div 16] of word;
  PDynamicIndexList = ^TDynamicIndexList;
  TDynamicAddressList = array[0..MaxInt div 16] of Pointer;
  PDynamicAddressList = ^TDynamicAddressList;

  TMethodEntry = packed record
    EntrySize: word;
    Address: Pointer;
    Name: ShortString;
  end;
  PMethodEntry = ^TMethodEntry;

  TMethodTable = packed record
    Count: word;
    FirstEntry: TMethodEntry;
  end;
  PMethodTable = ^TMethodTable;

  TFieldEntry = packed record
    OffSet: integer;
    IDX: word;
    Name: ShortString;
  end;
  PFieldEntry = ^TFieldEntry;

  TFieldClassTable = packed record
    Count: smallint;
    Classes: array[0..8191] of ^TPersistentClass;
  end;
  PFieldClassTable = ^TFieldClassTable;

  TFieldTable = packed record
    EntryCount: word;
    FieldClassTable: PFieldClassTable;
    FirstEntry: TFieldEntry;
  end;
  PFieldTable = ^TFieldTable;

  TRecordField = record
    TypeInfo: PPTypeInfo;
    Offset: integer;
  end;

  TRecordFields = array[0..30000] of TRecordField;
  PRecordFields = ^TRecordFields;

  TRecordTypeData = packed record
    Size: integer;
    FieldCount: integer;
    Fields: TRecordFields;
  end;
  PRecordTypeData = ^TRecordTypeData;

  PArrayTypeData = ^TArrayTypeData;

  TArrayTypeData = packed record
    ArraySize: integer;
    ItemSize: integer;
    ItemType: ^PTypeInfo;
  end;

  PDynArrayTypeData = ^TDynArrayTypeData;

  TDynArrayTypeData = packed record
    elSize: longint;
    elType: ^PTypeInfo;
    varType: integer;
  end;

  PParamRecord = ^TParamRecord;

  TParamRecord = record
    Flags: TParamFlags;
    ParamName: ShortString;
    TypeName: ShortString;
  end;

  PPropData = ^TPropData;

  ETypeInfoError = class(Exception);

function MakeComplTextBold(const InText: string): string;
function MakeComplTextComment(const InText: string): string;
function MakeComplTextCommentRed(const InText: string): string;
function MakeComplImage(const InText: string): string;
function BuildCompletionStr(const Header, ProcName: string): string;
procedure xAddStr(var BaseString: string; const AddString: string);
function xPReadShortStr(var p: pAnsichar): string;
procedure ShowRttiDetail(pti: PTypeInfo);
procedure ShowRTTI(pti: PTypeInfo; sList: TStrings);
procedure ShowMethod(pti: PTypeInfo; sList: TStrings);
procedure ShowClass(pti: PTypeInfo; sList: TStrings);
procedure ShowOrdinal(pti: PTypeInfo; sList: TStrings);
procedure ListEnum(pti: PTypeInfo; sList: TStrings; ShowIndex: boolean);
function GetPropValAsString(Obj: TObject; PropInfo: PPropInfo): string;
function SetToString(Value: cardinal; pti: PTypeInfo): string;
function GetPropertyType(Obj: TObject; const PropName: string): TTypeKind; overload;
function GetPropertyType(aclass: TClass; const PropName: string): TTypeKind; overload;
function GetPropertyTypeAsString(Obj: TObject; const PropName: string): string; overload;
function GetPropertyTypeAsString(aClass: TClass; const PropName: string): string; overload;
procedure GetEnumList(Obj: TObject; const PropName: string; SList: TStrings);
function GetEnumListStr(_PTypeInfo: PTypeInfo): string;
function IsBitOn(Value: integer; Bit: byte): boolean;
function GetValueFromPropertyName(Component: TObject; const PropertyName: string): string;
procedure SetValueByPropertyName(Component: TObject; const PropertyName, PropertyValue: string);
procedure Assign(Source, Target: TObject; Recursive: boolean);
procedure GetMethodList(FromClass: TClass; MethodList: TStrings);
procedure GetPublishedProperties(AClass: TClass; Result: TStrings);
procedure GetPublishedPropertiesEx(AClass: TClass; Result: TStrings);

function RTTI_GetEventString(aclass: TClass; const EventName: string): string; overload;
function RTTI_GetEventString(aclass: TClass; const EventName: string; var IsFunction: boolean): string; overload;

function RTTI_GetMethodString(aclass: TClass; const MethodName: string;
  TypeData: PTypeData; OnlyTypes, AddOwnerName,
  NotEventName, ForCompletion, NotProcedure: boolean): string;

function RTTI_GetVirtualMethod(AClass: TClass; const Index: integer): Pointer;
function RTTI_GetMethodEntry(MethodTable: PMethodTable; Index: integer): PMethodEntry;
function RTTI_GetPropData(TypeData: PTypeData): PPropData;
procedure RTTI_NextPropInfo(var PropInfo: PPropInfo);
function RTTI_GetSizeName(Size: integer): string;
function RTTI_GetTypeInfoName(TypeInfo: PTypeInfo): string;
function RTTI_GetTypeInfoSize(TypeInfo: PTypeInfo): integer;
function RTTI_GetVarSize(TypeInfo: PTypeInfo): integer;
function RTTI_GetTypeDef(TypeInfo: PTypeInfo): string;
function RTTI_GetMethodTypeParameters(TypeInfo: PTypeInfo): string; overload;
function RTTI_GetMethodTypeParameters(Typedata: PTypeData): string; overload;
function RTTI_GetMethodTypeResult(TypeInfo: PTypeInfo): string; overload;
function RTTI_GetMethodTypeResult(Typedata: PTypeData): string; overload;

implementation

const
  SMemoryWriteError = 'Error writing VMT memory (%s).';

type
  PLongint = ^longint;
  PPointer = ^Pointer;

  TvmtDynamicTable = packed record
    Count: word;
  end;

  TParamData = record
    Flags: TParamFlags;
    ParamName: ShortString;
    TypeName: ShortString;
  end;
  PParamData = ^TParamData;

//-------------------------------------------------

function IsCorrectAddress(Address: Pointer): boolean;
var
  R: record
    W1, W2: word;
  end;
begin
  Move(Address, R, SizeOf(Pointer));
  Result := (R.W2 <> 0);
end;

function MakeComplTextBold(const InText: string): string;
begin
  Result := InText;
  if InText <> '' then
    Result := ' \style{+B}' + InText + '\style{-B}';
end;

function MakeComplTextComment(const InText: string): string;
begin
  Result := InText;
  if InText <> '' then
    Result := '\color{clGray}\style{+N}' + '-> ' + InText + '\style{-N}\color{0}';
end;

function MakeComplTextCommentRed(const InText: string): string;
begin
  Result := InText;
  if InText <> '' then
    Result := '\color{clRed}\style{+N}' + InText + '\style{-N}\color{0}';
end;

function MakeComplImage(const InText: string): string;
begin
  Result := InText;

  if sametext(InText, 'Property ') then
  begin
    Result := '\image{0}\hspace{5}' + InText;
    exit;
  end;
  if sametext(InText, 'Event ') then
  begin
    Result := '\image{1}\hspace{5}' + InText;
    exit;
  end;
  if sametext(InText, 'Procedure ') then
  begin
    Result := '\image{2}\hspace{5}' + InText;
    exit;
  end;
  if sametext(InText, 'Function ') then
  begin
    Result := '\image{3}\hspace{5}' + InText;
    exit;
  end;
  if sametext(InText, 'CONSTRUCTOR ') then
  begin
    Result := '\image{4}\hspace{5}' + InText;
    exit;
  end;
  if sametext(InText, 'DESTRUCTOR ') then
  begin
    Result := '\image{4}\hspace{5}' + InText;
    exit;
  end;

  Result := '\image{5}\hspace{5}' + InText;
end;

function BuildCompletionStr(const Header, ProcName: string): string;
var
  ss, sspn, sst: string;
  i1, l: integer;
  b: boolean;
begin
  Result := Header;
  ss := UpperCase(Header);
  sspn := UpperCase(ProcName);
  if ss = '' then
    exit;
  sst := '';
  b := False;
  i1 := System.Pos('PROPERTY', ss);
  if i1 > 0 then
  begin
    sst := '\image{0}\hspace{5}' + Header;
    b := True;
  end;
  i1 := System.Pos('EVENT', ss);
  if i1 > 0 then
  begin
    sst := '\image{1}\hspace{5}' + Header;
    b := True;
  end;
  i1 := System.Pos('PROCEDURE', ss);
  if i1 > 0 then
  begin
    sst := '\image{2}\hspace{5}' + Header;
    b := True;
  end;
  i1 := System.Pos('FUNCTION', ss);
  if i1 > 0 then
  begin
    sst := '\image{3}\hspace{5}' + Header;
    b := True;
  end;
  i1 := System.Pos('CONSTRUCTOR', ss);
  if i1 > 0 then
  begin
    sst := '\image{4}\hspace{5}' + Header;
    b := True;
  end;
  i1 := System.Pos('DESTRUCTOR', ss);
  if i1 > 0 then
  begin
    sst := '\image{4}\hspace{5}' + Header;
    b := True;
  end;

  if b = True then
    Result := sst;

  if sspn = '' then
    exit;
  sst := UpperCase(Result);

  i1 := System.Pos(sspn, sst);
  if i1 > 0 then
  begin
    l := System.Length(sspn);
    system.Insert(' \style{+B}', Result, i1);
    system.Insert(' \style{-B}', Result, i1 + l + 11);
  end;

  ss := UpperCase(sssProReadOnly);
  sst := UpperCase(Result);

  i1 := System.Pos(ss, sst);
  if i1 > 0 then
  begin
    l := System.Length(ss);
    system.Insert('\color{clRed}\style{+N}    ', Result, i1);
    sst := UpperCase(Result);
    i1 := System.Pos(ss, sst);
    system.Insert('\style{-N}\color{0}', Result, i1 + l);
  end;

  ss := UpperCase(sssInherited);
  sst := UpperCase(Result);

  i1 := System.Pos(ss, sst); //must be at last
  if i1 > 0 then
  begin
    system.Insert('\color{clGray}\style{+N}        ', Result, i1);
    Result := Result + '\style{-N}\color{0}';
  end;
end;


procedure xAddStr(var BaseString: string; const AddString: string);
begin
  BaseString := BaseString + AddString;
end;

function xPReadShortStr(var p: pAnsichar): string;
var
  l: integer;
begin
  l := byte(p^);
  SetLength(Result, l);
  Inc(p);
  Move(p^, Result[1], l);
  Inc(p, l);
end;

function RTTI_GetEventString(aclass: TClass; const EventName: string): string;
var
  aPropInfo: PPropInfo;
  aTypeData: PTypeData;
begin
  Result := '';
  if (aclass = nil) or (EventName = '') then
    exit;

  aPropInfo := GetPropInfo(aclass, EventName, tkAny);
  if aPropInfo <> nil then
    if aPropInfo^.PropType^.Kind in tkMethods then
    begin
      aTypeData := GetTypeData(aPropInfo^.PropType);
      Result := RTTI_GetMethodString(aclass, EventName, aTypeData, False, False, False, False, False);
    end;
end;

function RTTI_GetEventString(aclass: TClass; const EventName: string; var IsFunction: boolean): string;
var
  aPropInfo: PPropInfo;
  aTypeData: PTypeData;
begin
  Result := '';
  isfunction := False;
  if (aclass = nil) or (EventName = '') then
    exit;

  aPropInfo := GetPropInfo(aclass, EventName, tkAny);
  if aPropInfo <> nil then
    if aPropInfo^.PropType^.Kind in tkMethods then
    begin
      aTypeData := GetTypeData(aPropInfo^.PropType);
      IsFunction := (aTypeData^.MethodKind in [mkFunction]);
      Result := RTTI_GetMethodString(aclass, EventName, aTypeData, False, False, False, False, True);
    end;
end;

function RTTI_GetMethodString(aclass: TClass; const MethodName: string;
  TypeData: PTypeData; OnlyTypes, AddOwnerName,
  NotEventName, ForCompletion, NotProcedure: boolean): string;
var
  P: PAnsiChar;
  i: integer;
  IsFunction: boolean;
  ParamName: string;
  OldTypeName: string;
  TypeName: string;
  OldFlags: TParamFlags;
  FLags: TParamFlags;
  List: TStringList;

  //...........................
  function GetParPrefix(const A: TParamFlags): string;
  begin
    if pfVar in A then
      Result := 'var ' //don't resource
    else if pfConst in A then
      Result := 'const ' //don't resource
    else if pfOut in A then
      Result := 'out ' //don't resource
    else
      Result := '';
  end;
  //...........................
  function BuildClassName(BaseName: string): string;
  begin
    Result := 'T' + BaseName;
  end;
  //...........................
  function InsParName(const PrevPar, NewPar: string): string;
  begin
    Result := PrevPar;
    Insert(', ' + NewPar, Result, Pos(':', Result));
  end;
  //...........................
  procedure _add(const s: string);
  begin
    xAddStr(Result, s);
  end;
  //...........................
begin
  Result := '';

  if TypeData = nil then
    exit;

  IsFunction := (TypeData^.MethodKind in [mkFunction]);

  if not OnlyTypes then
  begin

    if NotProcedure = False then
      if IsFunction then
        Result := 'Function ' //don't resource
      else
        Result := 'Procedure '; //don't resource


    if ForCompletion then
      Result := MakeComplImage(Result);


    if AddOwnerName and (aclass <> nil) and (aclass.ClassName <> '') then
      _add(BuildClassName(aclass.ClassName + '.'));

    if NotEventName = False then
      if ForCompletion then
        _add(MakeComplTextBold(MethodName))
      else
        _add(MethodName);

    if TypeData^.ParamCount > 0 then
      _add('(');
  end;

  P := @TypeData^.ParamList;
  List := TStringList.Create;

  try
    for i := 1 to TypeData^.ParamCount do
    begin

      //  Flags := TParamFlags(p^); //??????????????? ct9999 ???????????????????????

      Inc(p);
      ParamName := xPReadShortStr(p);

      if OnlyTypes then
        ParamName := '#';

      TypeName := xPReadShortStr(p);

      if pfArray in Flags then
        Insert('Array of ', TypeName, 1); //don't resource

      if (i > 1) and (OldFlags = Flags) and (CompareText(TypeName, oldTypeName) = 0) then
        List[List.Count - 1] := InsParName(List[List.Count - 1], ParamName)
      else
        List.Add(GetParPrefix(Flags) + ParamName + ': ' + TypeName);

      OldFlags := Flags;
      OldTypeName := TypeName;
    end;

    for i := 0 to List.Count - 1 do
    begin
      _add(List[i]);
      if i < List.Count - 1 then
        _add('; ');
    end;
  finally
    List.Free;
  end;

  if not OnlyTypes then
  begin

    if TypeData^.ParamCount > 0 then
      _add(')');
    if IsFunction then
      _add(': ' + xPReadShortStr(p));

    _add(';');
  end;
end;

procedure ShowMethod(pti: PTypeInfo; sList: TStrings);
var
  ptd: PTypeData;
  pParam: PParamData;
  nParam: integer;
  Line: string;
  pTypeString, pReturnString: ^ShortString;
begin

  if pti^.Kind <> tkMethod then
    raise Exception.Create('Invalid type information');

  ptd := GetTypeData(pti);

  sList.Add('Type Name: ' + pti^.Name);
  sList.Add('Type Kind: ' + GetEnumName(TypeInfo(TTypeKind), integer(pti^.Kind)));

  sList.Add('Method Kind: ' + GetEnumName(TypeInfo(TMethodKind), integer(ptd^.MethodKind)));
  sList.Add('Number of parameter: ' + IntToStr(ptd^.ParamCount));

  pParam := PParamData(@(ptd^.ParamList));
  nParam := 1;

  while nParam <= ptd^.ParamCount do
  begin

    Line := 'Param ' + IntToStr(nParam) + ' > ';

    if pfVar in pParam^.Flags then
      Line := Line + 'var ';
    if pfConst in pParam^.Flags then
      Line := Line + 'const ';
    if pfOut in pParam^.Flags then
      Line := Line + 'out ';

    Line := Line + pParam^.ParamName + ': ';

    if pfArray in pParam^.Flags then
      Line := Line + ' array of ';

    pTypeString := Pointer(integer(pParam) + sizeof(TParamFlags) + Length(pParam^.ParamName) + 1);

    Line := Line + pTypeString^;

    sList.Add(Line);

    pParam := PParamData(integer(pParam) + sizeof(TParamFlags) + Length(pParam^.ParamName) + 1 + Length(pTypeString^) + 1);

    Inc(nParam);
  end;

  if ptd^.MethodKind = mkFunction then
  begin

    pReturnString := Pointer(pParam);
    sList.Add('Returns > ' + pReturnString^);
  end;
end;

procedure ShowClass(pti: PTypeInfo; sList: TStrings);
var
  ptd: PTypeData;
  ppi: PPropInfo;
  pProps: PPropList;
  nProps, I: integer;
  ParentClass: TClass;
begin

  if pti^.Kind <> tkClass then
    raise Exception.Create('Invalid type information');

  ptd := GetTypeData(pti);

  sList.Add('Type Name: ' + pti^.Name);
  sList.Add('Type Kind: ' + GetEnumName(TypeInfo(TTypeKind), integer(pti^.Kind)));

  sList.Add('Size: ' + IntToStr(ptd^.ClassType.InstanceSize) + ' bytes');
  sList.Add('Defined in: ' + ptd^.UnitName + '.pas');

  ParentClass := ptd^.ClassType.ClassParent;
  if ParentClass <> nil then
  begin
    sList.Add('');
    sList.Add('=== Parent classes ===');
    while ParentClass <> nil do
    begin
      sList.Add(ParentClass.ClassName);
      ParentClass := ParentClass.ClassParent;
    end;
  end;

  nProps := ptd^.PropCount;
  if nProps > 0 then
  begin

    sList.Add('');
    sList.Add('=== Properties (' + IntToStr(nProps) + ') ===');

    GetMem(pProps, sizeof(PPropInfo) * nProps);
    try
      GetPropInfos(pti, pProps);

      for I := 0 to nProps - 1 do
      begin
        ppi := pProps^[I];
        sList.Add(ppi^.Name + ': ' + ppi^.PropType^.Name);
      end;
    finally
      FreeMem(pProps, sizeof(PPropInfo) * nProps);
    end;
  end;
end;

procedure ShowOrdinal(pti: PTypeInfo; sList: TStrings);
var
  ptd: PTypeData;
begin
  if not (pti^.Kind in [tkInteger, tkChar, tkEnumeration, tkSet, tkWChar]) then
    raise Exception.Create('Invalid type information');

  ptd := GetTypeData(pti);

  sList.Add('Type Name: ' + pti^.Name);
  sList.Add('Type Kind: ' + GetEnumName(TypeInfo(TTypeKind), integer(pti^.Kind)));

  sList.Add('Implement: ' + GetEnumName(TypeInfo(TOrdType), integer(ptd^.OrdType)));

  if pti^.Kind <> tkSet then
  begin
    sList.Add('Min Value: ' + IntToStr(ptd^.MinValue));
    sList.Add('Max Value: ' + IntToStr(ptd^.MaxValue));
  end;

  if pti^.Kind = tkEnumeration then
  begin
    sList.Add('Base Type: ' + (ptd^.BaseType)^.Name);
    sList.Add('');
    sList.Add('Values...');
    ListEnum(pti, sList, True);
  end;

  if pti^.Kind = tkSet then
  begin
    sList.Add('');
    sList.Add('Set base type information...');
    ShowOrdinal(ptd^.CompType, sList);
  end;
end;

procedure ListEnum(pti: PTypeInfo; sList: TStrings; ShowIndex: boolean);
var
  I: integer;
begin
  with GetTypeData(pti)^ do
    for I := MinValue to MaxValue do
      if ShowIndex then
        sList.Add('  ' + IntToStr(I) + '. ' + GetEnumName(pti, I))
      else
        sList.Add(GetEnumName(pti, I));
end;

procedure ShowRTTI(pti: PTypeInfo; sList: TStrings);
begin
  case pti^.Kind of
    tkInteger, tkChar, tkEnumeration, tkSet, tkWChar:
      ShowOrdinal(pti, sList);
    tkMethod:
      ShowMethod(pti, sList);
    tkClass:
      Showclass(pti, sList);
    tkString, tkLString:
    begin
      sList.Add('Type Name: ' + pti^.Name);
      sList.Add('Type Kind: ' + GetEnumName(TypeInfo(TTypeKind), integer(pti^.Kind)));
    end
    else
      sList.Add('Undefined type information');
  end;
end;

procedure ShowRttiDetail(pti: PTypeInfo);
var
  Form: TForm;
begin
  Form := TForm.Create(Application);
  try
    Form.Width := 250;
    Form.Height := 1000;

    Form.Left := Screen.Width div 2 - 125;
    Form.Top := Screen.Height div 2 - 150;
    Form.Caption := 'RTTI Details for ' + pti^.Name;
    Form.BorderStyle := bsDialog;
    with TMemo.Create(Form) do
    begin
      Parent := Form;
      Width := Form.ClientWidth;
      Height := Form.ClientHeight - 35;
      ReadOnly := True;
      Color := clBtnFace;
      ShowRTTI(pti, Lines);
    end;
    with TBitBtn.Create(Form) do
    begin
      Parent := Form;
      Left := Form.ClientWidth div 3;
      Width := Form.ClientWidth div 3;
      Top := Form.ClientHeight - 32;
      Height := 30;
      Kind := bkOK;
    end;
    Form.ShowModal;
  finally
    Form.Free;
  end;
end;

function GetOwnerForm(comp: TComponent): TComponent;
begin
  while not (comp is TForm) and not (comp is TDataModule) do
    comp := comp.Owner;
  Result := comp;
end;

function IsBitOn(Value: integer; Bit: byte): boolean;
begin
  Result := (Value and (1 shl Bit)) <> 0;
end;

function SetToString(Value: cardinal; pti: PTypeInfo): string;
var
  Res: string;
  BaseType: PTypeInfo;
  I: integer;
  Found: boolean;
begin
  Found := False;

  Res := '[';

  BaseType := GetTypeData(pti)^.CompType;

  for I := GetTypeData(BaseType)^.MinValue
    to GetTypeData(BaseType)^.MaxValue do
    if IsBitOn(Value, I) then
    begin

      Res := Res + GetEnumName(BaseType, I) + ', ';
      Found := True;
    end;
  if Found then
    Res := Copy(Res, 1, Length(Res) - 2);

  Result := Res + ']';
end;


function GetPropValAsString(Obj: TObject; PropInfo: PPropInfo): string;
var
  Pt: Pointer;
  word: cardinal;
begin
  case PropInfo^.PropType^.Kind of

    tkUnknown:
      Result := 'Unknown type';

    tkChar:
    begin
      word := GetOrdProp(Obj, PropInfo);
      if word > 32 then
        Result := char(word)
      else
        Result := '#' + IntToStr(word);
    end;

    tkWChar:
    begin
      word := GetOrdProp(Obj, PropInfo);
      if word > 32 then
        Result := widechar(word)
      else
        Result := '#' + IntToStr(word);
    end;


    tkInteger:
      if PropInfo^.PropType^.Name = 'TColor' then
        Result := ColorToString(GetOrdProp(Obj, PropInfo))
      else if PropInfo^.PropType^.Name = 'TCursor' then
        Result := CursorToString(GetOrdProp(Obj, PropInfo))
      else
        Result := Format('%d', [GetOrdProp(Obj, PropInfo)]);

    tkEnumeration:
      Result := GetEnumName(PropInfo^.PropType, GetOrdProp(Obj, PropInfo));

    tkFloat:
      Result := FloatToStr(GetFloatProp(Obj, PropInfo));

    tkString, tkLString:
      Result := GetStrProp(Obj, PropInfo);

    tkSet:
      Result := SetToString(GetOrdProp(Obj, PropInfo), PropInfo^.PropType);

    tkClass:
    begin
      Pt := Pointer(GetOrdProp(Obj, PropInfo));
      if Pt = nil then
        Result := '(None)'
      else
        Result := '(' + TPersistent(Pt).ClassName + ')';
    end;

    tkMethod:
    begin
      Pt := GetMethodProp(Obj, PropInfo).Code;
      if Pt <> nil then
        Result := GetOwnerForm(Obj as TComponent).MethodName(Pt)
      else
        Result := '';
    end;

    tkVariant:
      Result := GetVariantProp(Obj, PropInfo);

    tkArray, tkRecord, tkInterface:
      Result := 'Unsupported type';

    else
      Result := 'Undefined type';
  end;
end;

function GetPropertyType(Obj: TObject; const PropName: string): TTypeKind;
var
  PropInfo: PPropInfo;
  C: TComponentClass;
begin
  Result := tkUnknown;
  c := TComponentClass(GetClass(Obj.ClassName));
  PropInfo := GetPropInfo(C.ClassInfo, PropName);
  if Propinfo <> nil then
    Result := PropInfo^.PropType^.Kind;
end;

function GetPropertyType(aclass: TClass; const PropName: string): TTypeKind;
var
  PropInfo: PPropInfo;
begin
  Result := tkUnknown;
  PropInfo := GetPropInfo(aclass.ClassInfo, PropName);
  if Propinfo <> nil then
    Result := PropInfo^.PropType^.Kind;
end;

function GetPropertyTypeAsString(Obj: TObject; const PropName: string): string;
var
  rt: TTypeKind;
  PropInfo: PPropInfo;
  C: TComponentClass;
begin
  Result := '';
  rt := tkUnknown;
  c := TComponentClass(GetClass(Obj.ClassName));
  PropInfo := GetPropInfo(C.ClassInfo, PropName);
  if propinfo = nil then
    exit;
  Result := PropInfo^.PropType^.Name;
end;

function GetPropertyTypeAsString(aclass: TClass; const PropName: string): string;
var
  rt: TTypeKind;
  PropInfo: PPropInfo;
begin
  Result := '';
  rt := tkUnknown;
  PropInfo := GetPropInfo(aclass.ClassInfo, PropName);
  if propinfo = nil then
    exit;
  Result := PropInfo^.PropType^.Name;
end;

procedure GetEnumList(Obj: TObject; const PropName: string; SList: TStrings);
var
  PropInfo: PPropInfo;
  C: TComponentClass;
begin
  c := TComponentClass(GetClass(Obj.ClassName));
  PropInfo := GetPropInfo(C.ClassInfo, PropName);
  if propinfo <> nil then
    ListEnum(PropInfo^.PropType, SList, False);
end;

function GetEnumListStr(_PTypeInfo: PTypeInfo): string;
var
  J: integer;
  ptd: PTypeData;
begin
  Result := '';
  ptd := GetTypeData(_PTypeInfo);
  for J := ptd^.MinValue to ptd^.MaxValue do
  begin
    if Length(Result) > 0 then
      Result := Result + ', ';
    Result := Result + GetEnumName(_PTypeInfo, J);
  end;
end;

function GetValueFromPropertyName(Component: TObject; const PropertyName: string): string;
var
  PropInfo: PPropInfo;
  TypeInf, PropTypeInf: PTypeInfo;
  TypeData: PTypeData;
  I: integer;
  AName, PropName: string;
  PropList: PPropList;
  NumProps: word;
  PropObject: TObject;
begin

  TypeInf := Component.ClassInfo;
  AName := TypeInf^.Name;
  TypeData := GetTypeData(TypeInf);
  NumProps := TypeData^.PropCount;

  Result := '';
  GetMem(PropList, NumProps * SizeOf(Pointer));
  try

    GetPropInfos(TypeInf, PropList);

    for I := 0 to NumProps - 1 do
    begin
      PropName := PropList^[I]^.Name;
      PropTypeInf := PropList^[I]^.PropType;
      PropInfo := PropList^[I];

      if PropTypeInf^.Kind = tkClass then
      begin
        PropObject := GetObjectProp(Component, PropInfo);
        Result := GetValueFromPropertyName(PropObject, PropertyName);
      end
      else
      if CompareText(PropName, PropertyName) = 0 then
      begin
        Result := GetPropValue(Component, PropName, True);
        Break;
      end;

      if Result <> '' then
        Exit;
    end;
  finally
    FreeMem(PropList);
  end;
end;

procedure SetValueByPropertyName(Component: TObject; const PropertyName, PropertyValue: string);
var
  PropInfo: PPropInfo;
  TypeInf, PropTypeInf: PTypeInfo;
  TypeData: PTypeData;
  I: integer;
  AName, PropName: string;
  PropList: PPropList;
  NumProps: word;
  PropObject: TObject;
begin

  TypeInf := Component.ClassInfo;
  AName := TypeInf^.Name;
  TypeData := GetTypeData(TypeInf);
  NumProps := TypeData^.PropCount;

  GetMem(PropList, NumProps * SizeOf(Pointer));
  try

    GetPropInfos(TypeInf, PropList);

    for I := 0 to NumProps - 1 do
    begin
      PropName := PropList^[I]^.Name;
      PropTypeInf := PropList^[I]^.PropType;
      PropInfo := PropList^[I];

      if PropTypeInf^.Kind = tkClass then
      begin
        PropObject := GetObjectProp(Component, PropInfo);
        SetValueByPropertyName(PropObject, PropertyName, PropertyValue);
      end
      else
      if CompareText(PropName, PropertyName) = 0 then
      begin
        SetPropValue(Component, PropName, PropertyValue);
        Break;
      end;
    end;
  finally
    FreeMem(PropList);
  end;
end;

procedure Assign(Source, Target: TObject; Recursive: boolean);
var
  PropTypeInf: PTypeInfo;
  I, Index: integer;
  PropName: string;
  Source_PropList, Target_PropList: PPropList;
  Source_NumProps, Target_NumProps: word;
  Source_PropObject, Target_PropObject: TObject;

  function FindProperty(const PropName: string; PropList: PPropList; NumProps: word): integer;
  var
    I: integer;
  begin
    Result := -1;
    for I := 0 to NumProps - 1 do
      if CompareStr(PropList^[I]^.Name, PropName) = 0 then
      begin
        Result := I;
        Break;
      end;
  end;

begin
  if not Assigned(Source) or not Assigned(Target) then
    Exit;


  Source_NumProps := GetTypeData(Source.ClassInfo)^.PropCount;
  Target_NumProps := GetTypeData(Target.ClassInfo)^.PropCount;

  GetMem(Source_PropList, Source_NumProps * SizeOf(Pointer));
  GetMem(Target_PropList, Target_NumProps * SizeOf(Pointer));
  try

    GetPropInfos(Source.ClassInfo, Source_PropList);
    GetPropInfos(Target.ClassInfo, Target_PropList);

    for I := 0 to Source_NumProps - 1 do
    begin
      PropName := Source_PropList^[I]^.Name;

      Index := FindProperty(PropName, Target_PropList, Target_NumProps);
      if Index = -1 then
        Continue;

      if Source_PropList^[I]^.PropType^.Kind <> Target_PropList^[I]^.PropType^.Kind then
        Continue;

      PropTypeInf := Source_PropList^[I]^.PropType;

      if PropTypeInf^.Kind = tkClass then
      begin
        if Recursive then
        begin
          Source_PropObject := GetObjectProp(Source, Source.ClassInfo);
          Target_PropObject := GetObjectProp(Target, Target.ClassInfo);
          Assign(Source_PropObject, Target_PropObject, Recursive);
        end;
      end
      else
        SetPropValue(Target, PropName, GetPropValue(Source, PropName));
    end;
  finally
    FreeMem(Source_PropList);
    FreeMem(Target_PropList);
  end;
end;


procedure GetPublishedPropertiesEx(AClass: TClass; Result: TStrings);
var
  pti: PTypeInfo;
  ptd: PTypeData;
  Loop, nProps: integer;
  pProps: PPropList;
  ppi: PPropInfo;
  S: string;
begin
  pti := AClass.ClassInfo;
  if pti = nil then
    Exit;
  ptd := GetTypeData(pti);
  nProps := ptd^.PropCount;
  if nProps > 0 then
  begin
    GetMem(pProps, SizeOf(PPropInfo) * nProps);
    GetPropInfos(pti, pProps);
  end
  else
    pProps := nil;
  for Loop := 0 to nProps - 1 do
  begin
    ppi := pProps^[Loop];
    S := ppi^.Name;
    Result.Add(S + ':' + ppi^.PropType^.Name);
  end;
  if pProps <> nil then
    FreeMem(pProps, SizeOf(PPropInfo) * nProps);
end;

procedure GetPublishedProperties(AClass: TClass; Result: TStrings);
var
  pti: PTypeInfo;
  ptd: PTypeData;
  Loop, nProps: integer;
  pProps: PPropList;
  ppi: PPropInfo;
  S: string;
begin
  pti := AClass.ClassInfo;
  if pti = nil then
    Exit;
  ptd := GetTypeData(pti);
  nProps := ptd^.PropCount;
  if nProps > 0 then
  begin
    GetMem(pProps, SizeOf(PPropInfo) * nProps);
    GetPropInfos(pti, pProps);
  end
  else
    pProps := nil;
  for Loop := 0 to nProps - 1 do
  begin
    ppi := pProps^[Loop];
    S := ppi^.Name;
    Result.Add(UpperCase(S));
  end;
  if pProps <> nil then
    FreeMem(pProps, SizeOf(PPropInfo) * nProps);
end;

procedure GetMethodList(FromClass: TClass; MethodList: TStrings);
type
  PPointer = ^Pointer;
  PMethodRec = ^TMethodRec;

  TMethodRec = packed record
    wSize: word;
    pCode: Pointer;
    sName: ShortString;
  end;
var
  MethodTable: PChar;
  MethodRec: PMethodRec;
  wCount: word;
  nMethod: integer;
begin
  MethodList.Clear;

  while (FromClass <> nil) do
  begin
    MethodTable := PChar(Pointer(PChar(FromClass) + vmtMethodTable)^);
    if (MethodTable <> nil) then
    begin
      Move(MethodTable^, wCount, 2);
      MethodRec := PMethodRec(MethodTable + 2);
      for nMethod := 0 to wCount - 1 do
      begin
        MethodList.AddObject(MethodRec^.sName, TObject(MethodRec^.pCode));
        MethodRec := PMethodRec(PChar(MethodRec) + MethodRec^.wSize);
      end;
    end;
    FromClass := FromClass.ClassParent;
  end;
end;

function RTTI_GetVirtualMethod(AClass: TClass; const Index: integer): Pointer;
begin
  Result := PPointer(integer(AClass) + Index * SizeOf(Pointer))^;
end;

function RTTI_GetMethodEntry(MethodTable: PMethodTable; Index: integer): PMethodEntry;
begin
  Result := Pointer(cardinal(MethodTable) + 2);
  for Index := Index downto 1 do
    Inc(PtrInt(Result), Result^.EntrySize);
end;

function RTTI_GetPropData(TypeData: PTypeData): PPropData;
begin
  Result := Pointer(integer(TypeData) + SizeOf(TClass) + SizeOf(PPTypeInfo) + SizeOf(smallint) + Length(TypeData^.UnitName) + 1);
end;

procedure RTTI_NextPropInfo(var PropInfo: PPropInfo);
begin
  PropInfo := Pointer(integer(PropInfo) + SizeOf(TPropInfo) - 255 + Length(PropInfo^.Name));
end;

type
  PWord = ^word;
  PInteger = ^integer;

function RTTI_GetSizeName(Size: integer): string;
const
  TypeName: array[1..4] of string = ('shortint', 'smallint', '', 'integer');
var
  ArraySize: integer;
begin
  if Size <= 0 then
    raise ETypeInfoError.Create('Negative Size');
  if (Size = 1) or (Size = 2) or (Size = 4) then
    Result := TypeName[Size]
  else
  begin
    ArraySize := 4;
    while Size mod ArraySize <> 0 do
      ArraySize := ArraySize div 2;
    Result := Format('array[0..%d] of %s', [Size div ArraySize - 1, TypeName[ArraySize]]);
  end;
end;

function RTTI_GetTypeInfoName(TypeInfo: PTypeInfo): string;
begin
  Result := TypeInfo^.Name;
  if Result[1] = '.' then
    Result := RTTI_GetTypeDef(TypeInfo);
end;

function RTTI_GetTypeDef(TypeInfo: PTypeInfo): string;
var
  TypeData: PTypeData;
  DynArrayTypeData: PDynArrayTypeData absolute TypeData;
  ArrayTypeData: PArrayTypeData absolute TypeData;
  RecordTypeData: PRecordTypeData absolute TypeData;
  ParamRecord: PParamRecord;
  BaseTypeData: PTypeData;
  I, J: integer;
  X: PShortString;
const
  OrdTypeName: array[TOrdType] of string = ('SByte', 'UByte', 'SWord,', 'UWord', 'SLong', 'ULong' {$if fpc_fullversion >= 30200}, 'otSQWord', 'otUQWord' {$endif});
  FloatTypeName: array [TFloatType] of string = ('Single', 'Double', 'Extended', 'Comp', 'Currency');
  ProcName: array[TMethodKind] of string = ('Procedure', 'Function', 'Constructor', 'Destructor',
    'Class Procedure', 'Class Function', 'Class Constructor', 'Operator Overload',
    'Safe Procedure');

  SInvalidArraySize = 'Array size (%d) of typeinfo at %p should be a multiple of the item size (%d)';
begin
  Result := '';
  if TypeInfo = nil then
    exit;
  typeData := GetTypeData(TypeInfo);
  case TypeInfo^.Kind of
    tkLString: Result := 'string';
    tkWString: Result := 'WideString';
    tkVariant: Result := 'Variant';
    tkInteger, tkInt64:
    begin
      if (TypeData^.OrdType = otSLong) and (Low(integer) = TypeData^.MinValue) then
        Result := Format('Low(Integer)..%d', [TypeData^.MaxValue])
      else
        Result := Format('%d..%d', [TypeData^.MinValue, TypeData^.MaxValue]);
    end;
    tkChar, tkWChar: Result := Format('#%d..#%d', [TypeData^.MinValue, TypeData^.MaxValue]);
    tkEnumeration:

      if TypeData^.BaseType = TypeInfo then
      begin

        X := @TypeData^.NameList;
        Result := '(';
        for I := TypeData^.MinValue to TypeData^.MaxValue - 1 do
        begin
          Result := Result + X^ + ', ';
          Inc(PtrInt(X), Length(X^) + 1);
        end;
        Result := Result + X^ + ')';

      end
      else
      begin

        if TypeData^.BaseType^.Kind = tkEnumeration then
        begin
          BaseTypeData := GetTypeData(TypeData^.BaseType);
          X := @BaseTypeData^.NameList;
          for I := BaseTypeData^.MinValue to BaseTypeData^.MaxValue do
          begin
            if TypeData^.MinValue = I then
              Result := Result + X^ + ' .. ';
            if TypeData^.MaxValue = I then
            begin
              Result := Result + X^;
              Break;
            end;
            Inc(PtrInt(X), Length(X^) + 1);
          end;
        end
        else
          raise ETypeInfoError.Create('Unsupported type');

      end;
    tkSet: Result := 'set of ' + RTTI_GetTypeInfoName(TypeData^.CompType);
    tkFloat: Result := FloatTypeName[TypeData^.FloatType];
    tkString: Result := Format('string[%d]', [TypeData^.MaxLength]);
    tkMethod:
    begin
      Result := ProcName[TypeData^.MethodKind];
      Result := Result + '(';
      ParamRecord := @TypeData^.ParamList;
      for I := 0 to TypeData^.ParamCount - 1 do
      begin
        X := Pointer(integer(@ParamRecord^.ParamName) + Length(ParamRecord^.ParamName) + 1);
        if pfVar in ParamRecord^.Flags then
          Result := Result + 'var ';
        if pfConst in ParamRecord^.Flags then
          Result := Result + 'const ';
        if pfOut in ParamRecord^.Flags then
          Result := Result + 'out ';
        Result := Result + ParamRecord^.ParamName + ': ';
        if pfArray in ParamRecord^.Flags then
          Result := Result + 'array of ';
        Result := Result + X^;
        if I < TypeData^.ParamCount - 1 then
          Result := Result + '; ';
        ParamRecord := PParamRecord(integer(ParamRecord) + SizeOf(TParamFlags) + (Length(ParamRecord^.Paramname) + 1) +
          (Length(X^) + 1));
      end;
      Result := Result + ')';
      if TypeData^.MethodKind in [mkFunction, mkClassFunction] then
        Result := Result + ': ' + PShortString(ParamRecord)^;
      Result := Result + ' of object';
    end;
    tkDynArray:
    begin
      if DynArrayTypeData^.elType = nil then
      begin
        if DynArrayTypeData^.elSize mod 4 = 0 then
          Result := 'array of ' + RTTI_GetSizeName(DynArrayTypeData^.elSize)
        else
          Result := 'packed array of ' + RTTI_GetSizeName(DynArrayTypeData^.elSize);
      end
      else
        Result := Format('array of %s', [RTTI_GetTypeInfoName(DynArrayTypeData^.elType^)]);
    end;
    tkRecord:
    begin
      Result := 'packed record ';
      I := 0;
      for J := 0 to RecordTypeData^.FieldCount - 1 do
      begin
        if I <> RecordTypeData^.Fields[J].Offset then
          Result := Result + Format('f%d: %s; ', [I, RTTI_GetSizeName(RecordTypeData^.Fields[J].Offset - I)]);
        I := RecordTypeData^.Fields[J].Offset;
        Result := Result + Format('f%d: %s; ', [I, RTTI_GetTypeInfoName(RecordTypeData^.Fields[J].TypeInfo^)]);
        I := I + RTTI_GetVarSize(RecordTypeData^.Fields[J].TypeInfo^);
      end;
      if I <> RecordTypeData^.Size then
        Result := Result + Format('f%d: %s; ', [I, RTTI_GetSizeName(RecordTypeData^.Size - I)]);
      Result := Result + 'end';
    end;
    tkInterface:
      Result := 'interface(' + TypeData^.IntfParent^.Name + ') [''' + GUIDToString(TypeData^.Guid) + '''] end';
    tkArray:
    begin
      I := RTTI_GetVarSize(ArrayTypeData^.ItemType^);
      if ArrayTypeData^.ArraySize mod I <> 0 then
        raise ETypeInfoError.CreateFmt(SInvalidArraySize, [ArrayTypeData^.ArraySize, Pointer(TypeInfo), I]);
      Result := Format('array[0..%d] of %s', [ArrayTypeData^.ArraySize div I - 1, RTTI_GetTypeInfoName(ArrayTypeData^.ItemType^)]);
    end;
    else
      raise ETypeInfoError.Create('type Info defenition not yet supported');
  end;
end;

function RTTI_GetTypeInfoSize(TypeInfo: PTypeInfo): integer;
var
  TypeData: PTypeData;
  PropInfo: PPropInfo;
  J: PChar;
  I: integer;
begin
  Result := SizeOf(TTypeKind) + Length(TypeInfo^.Name) + 1;
  TypeData := GetTypeData(TypeInfo);
  case TypeInfo^.Kind of
    tkUnknown, tkLString, tkWString, tkVariant: ;
    tkInteger, tkChar, tkWChar: Result := Result + SizeOf(TOrdType) + 2 * SizeOf(longint);
    tkEnumeration:
    begin
      Result := Result + SizeOf(TOrdType) + 2 * SizeOf(longint) + SizeOf(Pointer);
      if TypeData^.BaseType = TypeInfo then
      begin
        J := @TypeData^.NameList;
        for I := 0 to abs(cardinal(TypeData^.MinValue) - cardinal(TypeData^.MaxValue)) do
        begin
          Result := Result + byte(J[0]) + 1;
          J := J + byte(J[0]) + 1;
        end;
      end;
    end;
    tkSet: Result := Result + SizeOf(Pointer);
    tkFloat: Result := Result + SizeOf(TFloatType);
    tkString: Result := Result + SizeOf(byte);
    tkClass:
    begin
      Result := Result + SizeOf(TClass) + SizeOf(Pointer) + SizeOf(smallint) + Length(TypeData^.UnitName) + 1 + SizeOf(word);
      PropInfo := PPropInfo(integer(TypeData) + SizeOf(TClass) + SizeOf(Pointer) + SizeOf(smallint) +
        Length(TypeData^.UnitName) + 1 + SizeOf(word));
      for I := 0 to PWord(integer(PropInfo) - SizeOf(word))^ - 1 do
      begin
        Result := Result + 4 * SizeOf(Pointer) + SizeOf(integer) + SizeOf(longint) + SizeOf(smallint) + Length(PropInfo^.Name) + 1;
        PropInfo := PPropInfo(integer(PropInfo) + 4 * SizeOf(Pointer) + SizeOf(integer) + SizeOf(longint) +
          SizeOf(smallint) + Length(PropInfo^.Name) + 1);
      end;
    end;
    tkMethod:
    begin
      J := PChar(TypeData) + SizeOf(TMethodKind) + SizeOf(byte);
      for I := 0 to TypeData^.ParamCount - 1 do
      begin
        J := J + SizeOf(TParamFlags);
        J := J + byte(J[0]) + 1;
        J := J + byte(J[0]) + 1;
      end;
      if TypeData^.MethodKind in [mkFunction, mkClassFunction] then
        J := J + byte(J[0]) + 1;
      Result := Result + J - PChar(TypeData);
    end;
    tkInterface:
    begin
      Result := Result + SizeOf(Pointer) + SizeOf(TIntfFlagsBase) + SizeOf(TGUID) + Length(TypeData^.IntfUnit) + 1 + SizeOf(word);
      PropInfo := PPropInfo(integer(TypeData) + SizeOf(Pointer) + SizeOf(TIntfFlagsBase) + SizeOf(TGUID) +
        Length(TypeData^.IntfUnit) + 1 + SizeOf(word));
      for I := 0 to PWord(integer(PropInfo) - SizeOf(word))^ - 1 do
      begin
        Result := Result + 4 * SizeOf(Pointer) + SizeOf(integer) + SizeOf(longint) + SizeOf(smallint) + Length(PropInfo^.Name) + 1;
        PropInfo := PPropInfo(integer(PropInfo) + 4 * SizeOf(Pointer) + SizeOf(integer) + SizeOf(longint) +
          SizeOf(smallint) + Length(PropInfo^.Name) + 1);
      end;
    end;
    tkInt64: Result := Result + 2 * SizeOf(int64);
    tkDynArray: Result := Result + SizeOf(TDynArrayTypeData);
    tkRecord:
    begin
      Result := Result + 2 * SizeOf(integer) + PInteger(integer(TypeData) + SizeOf(integer))^ * SizeOf(TRecordField);
    end;
    tkArray: Result := Result + 2 * SizeOf(integer) + SizeOf(Pointer);
    else
      raise ETypeInfoError.Create('Unkwno Type Info Size');
  end;
end;

function RTTI_GetVarSize(TypeInfo: PTypeInfo): integer;
const
  OrdTypeSize: array[TOrdType] of integer = (1, 1, 2, 2, 4, 4 {$if fpc_fullversion >= 30200}, 8, 8 {$endif});
  FloatTypeSize: array[TFloatType] of integer = (4, 8, 10, 8, 8);
begin
  case TypeInfo^.Kind of
    tkLString, tkWString, tkString, tkClass, tkInterface, tkDynArray:
      Result := 4;
    tkInteger, tkChar, tkEnumeration, tkSet, tkWChar:
      Result := OrdTypeSize[GetTypeData(TypeInfo)^.OrdType];
    tkFloat:
      Result := FloatTypeSize[GetTypeData(TypeInfo)^.FloatType];
    tkMethod, tkInt64: Result := 8;
    tkRecord: Result := PRecordTypeData(GetTypeData(TypeInfo))^.Size;
    tkArray: Result := PArrayTypeData(GetTypeData(TypeInfo))^.ArraySize;
    tkVariant: Result := SizeOf(TVarData);
    else
      raise ETypeInfoError.Create('Unknown Variant type');
  end;
end;

function RTTI_GetMethodTypeParameters(TypeInfo: PTypeInfo): string;
begin
  Result := '';
  if TypeInfo = nil then
    exit;
  Result := RTTI_GetMethodTypeParameters(GetTypeData(TypeInfo));
end;

function RTTI_GetMethodTypeParameters(TypeData: PTypeData): string;
var
  ParamRecord: PParamRecord;
  I: integer;
  X: PShortString;
begin
  Result := '';
  if TypeData = nil then
    exit;
  ParamRecord := @TypeData^.ParamList;
  for I := 0 to TypeData^.ParamCount - 1 do
  begin
    X := Pointer(integer(@ParamRecord^.ParamName) + Length(ParamRecord^.ParamName) + 1);

    if pfVar in ParamRecord^.Flags then
      Result := Result + 'var ';
    if pfConst in ParamRecord^.Flags then
      Result := Result + 'const ';
    if pfOut in ParamRecord^.Flags then
      Result := Result + 'out ';

    Result := Result + ParamRecord^.ParamName + ': ';

    if pfArray in ParamRecord^.Flags then
      Result := Result + 'array of ';
    Result := Result + X^;

    if I < TypeData^.ParamCount - 1 then
      Result := Result + '; ';

    ParamRecord := PParamRecord(integer(ParamRecord) + SizeOf(TParamFlags) + (Length(ParamRecord^.Paramname) + 1) + (Length(X^) + 1));
  end;
end;

function RTTI_GetMethodTypeResult(TypeInfo: PTypeInfo): string;
begin
  Result := '';
  if TypeInfo = nil then
    exit;
  Result := RTTI_GetMethodTypeResult(GetTypeData(TypeInfo));
end;

function RTTI_GetMethodTypeResult(Typedata: PTypeData): string;
var
  I: integer;
  J: PChar;
begin
  Result := '';
  if TypeData = nil then
    exit;

  if TypeData^.MethodKind in [mkFunction, mkClassFunction] then
  begin
    J := PChar(TypeData) + SizeOf(TMethodKind) + SizeOf(byte);
    for I := 0 to TypeData^.ParamCount - 1 do
    begin
      J := J + SizeOf(TParamFlags);
      J := J + byte(J[0]) + 1;
      J := J + byte(J[0]) + 1;
    end;
    Result := J + 1;
  end;
end;


end.
