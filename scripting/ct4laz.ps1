#############################################################################
# copy2ct4laz v1.0 - Windows Powershell script by Zeljko Avramovic (c) 2019 #
# copy only certain component and example directories from ct2laz to ct4laz #
#############################################################################
# in case of this error: �Execution of scripts is disabled on this system�  #
# you need to execute "Set-ExecutionPolicy RemoteSigned" from powershell    #
#############################################################################

# default parameters:
param  # example usage:
(      # .\ct4laz -ct2laz "c:\other\src\dir\" -ct4laz "c:\other\dest\dir\" -dbg 1
  [string]  $ct2laz     = "..\scripting\ct2laz.dl\", # or absolute path: "m:\Lazarus\ct4laz\scripting\ct2laz.dl\",
  [string]  $ct4laz     = "..\",                     # or absolute path: "m:\Lazarus\ct4laz\" to get to get same effect (this is the place where components and examples dirs will be stored
  [string]  $oldct2laz  = "old.working.dir\",
  [string]  $dirlist    = "dir.list",
  [string]  $oldlist    = "old.list",
  [string]  $removelist = "remove.list",
  [string]  $filelist   = "file.list",
  [boolean] $dbg        = $false                     # use parameter 0 for false and 1 for true
)

$StartTime = Get-Date

# use Log instead of Write-Host
set-alias Log Write-Host   

# change powershell console output colors:
$COLOR_INFO             = "DarkGray"
$COLOR_EXTRA            = "Magenta"
$COLOR_ERROR            = "Red"

# exit if preconditions are not met:
if (-not (Test-Path $ct2laz))
  { Log "ERROR: Source directory does not exist (parameter ct2laz: $ct2laz)" -ForegroundColor $COLOR_ERROR ; Return}
if (-not (Test-Path $dirlist))
  { Log "ERROR: File does not exist (parameter dirlist: $dirlist)"           -ForegroundColor $COLOR_ERROR ; Return}
if (-not (Test-Path $oldlist))
  { Log "ERROR: File does not exist (parameter oldlist: $oldlist)"           -ForegroundColor $COLOR_ERROR ; Return}
if (-not (Test-Path $removelist))
  { Log "ERROR: File does not exist (parameter removelist: $removelist)"     -ForegroundColor $COLOR_ERROR ; Return}
if (-not (Test-Path $filelist))
  { Log "ERROR: File does not exist (parameter filelist: $filelist)"         -ForegroundColor $COLOR_ERROR ; Return}

# previous CT version dir for packages not compatible with official Lazarus:
$ct2laz_old             = $oldct2laz

# read directories to be copied from dirlist file:    
$dirs                   = Get-Content $dirlist      # "pl_aggpas", "pl_aggpasvs", "pl_ape", "pl_asiovst"...

# read directories to be copied from oldlist file:
$dirs_old               = Get-Content $oldlist      # "components\pl_cindy", "examples\pl_cindy", "components\pl_graphics32", "examples\pl_graphics32"...

# read files/dirs that will be deleted after copy is finished from removelist file:
$remove                 = Get-Content $removelist   # "makefile*", "backup", "*.exe"...

# read files that will be copied from filelist file:
$files                  = Get-Content $filelist     # files\ctutils.pas, examples\pl_opengl\samples\00_librarytest_ctgl ...

if ($dbg)
{
  Log "Parameter ct2laz:   " $ct2laz
  Log "Parameter ct4laz:   " $ct4laz
  Log "Parameter oldct2laz:" $oldct2laz
  Log "Debug content of" $dirlist "file:    " $dirs
  Log "Debug content of" $oldlist "file:    " $dirs_old
  Log "Debug content of" $removelist "file: " $remove
  Log
}

# CT archives default source sub-directories:
$ct2laz_examples        = "codeocean\"
$ct2laz_components      = "typhon\components\"

# target sub-directories:
$ct4laz_examples        = "pl_examples\"
$ct4laz_components      = "pl_components\"

# destination conponents and examples directories:
$dest_examples          = $ct4laz + $ct4laz_examples
$dest_components        = $ct4laz + $ct4laz_components

# source and destination for previous version CT directories:
$src_old                = $ct2laz_old
$dest_old               = $ct4laz

# directory type enumerator:
Add-Type -TypeDefinition @"
  public enum TDirType
  {
    dtComponent,
    dtExamples,
    dtUnknown
  }
"@

###########################
function GetDirType
###########################
{
  param([parameter(Mandatory=$true)] [ValidateNotNullOrEmpty()] [String] $Dir)

  if (($Dir.Contains($ct2laz_components)) -or ($Dir.Contains($ct4laz_components)))
    { Return [TDirType]::dtComponent }

  if (($Dir.Contains($ct2laz_examples)) -or ($Dir.Contains($ct4laz_examples)))
    { Return [TDirType]::dtExamples }

  Return [TDirType]::dtUnknown
}


###########################
function CopyDir
###########################
{ 
  param ([String] $DirName, $SrcPath, $DestPath)
  if (-not (($DirName.Contains("#")) -or (($DirName.Trim() -eq ""))))
  {
    $DirNameOnly = $DirName.Replace($ct2laz_examples,"")       # remove examples dir prefix from string   (current CT version)
    $DirNameOnly = $DirNameOnly.Replace($ct2laz_components,"") # remove components dir prefix from string (current CT version)
    $DirNameOnly = $DirNameOnly.Replace($ct4laz_examples,"")   # remove examples dir prefix from string   (previous CT version)
    $DirNameOnly = $DirNameOnly.Replace($ct4laz_components,"") # remove components dir prefix from string (previous CT version)

    [TDirType] $DirType = GetDirType $DirName # is this a component dir, examples dir, or unknown dir?

    if ($dbg)
    {
      Log "DirName:    " $DirName  -ForegroundColor $COLOR_INFO -NoNewline
      switch ($DirType)
      {
        dtComponent { Log " [component]"  -ForegroundColor $COLOR_EXTRA ; break } 
        dtExamples  { Log " [example]"    -ForegroundColor $COLOR_EXTRA ; break } 
        default     { Log " [unknown!!!]" -ForegroundColor $COLOR_ERROR ; break } 
      }
      Log "DirNameOnly:" $DirNameOnly -ForegroundColor $COLOR_INFO
      Log "SrcPath:    " $SrcPath     -ForegroundColor $COLOR_INFO
      Log "DestPath:   " $DestPath    -ForegroundColor $COLOR_INFO
    }

    $src = $SrcPath + $DirName

    switch ($DirType)
    {
      dtComponent { $dest = $DestPath + $ct4laz_components + $DirNameOnly ; break } 
      dtExamples  { $dest = $DestPath + $ct4laz_examples   + $DirNameOnly ; break } 
      default     { Log "ERROR: Directory type not recognized for $DirName" -ForegroundColor $COLOR_ERROR ; Log ; return } # exit func
    }
    
    if (Test-Path $src)
    {
      if (Test-Path $dest)
        { Remove-Item $dest -Recurse }
      Log "CopyFrom:   " $src  -ForegroundColor $COLOR_INFO
      Log "CopyTo:     " $dest -ForegroundColor $COLOR_INFO
      Copy-Item $src -Recurse $dest -Force
      Log
    }
    else
    {
      Log "Source directory does not exist:" $src -ForegroundColor $COLOR_ERROR
      Log
    }  
  }
}


###########################
function CopyFromFreshCT
###########################
{
  Log ; Log ; Log "Copy directories from latest version CT folder:" ; Log
  foreach ($element in $dirs) 
  { # copy directories from latest version CT folder
    if (-not (($element.Contains("#")) -or (($element.Trim() -eq ""))))
      { CopyDir $element $ct2laz $ct4laz }
  }
}


###########################
function CopyFromOldCT
###########################
{
  Log ; Log "Copy directories from previous version CT folder:" ; Log
  foreach ($element in $dirs_old) 
  { # copy directories from previous version CT folder
    if (-not (($element.Contains("#")) -or (($element.Trim() -eq ""))))
      { CopyDir $element $ct2laz_old $ct4laz }
  }
}


###########################
function CopyFiles
###########################
{
  Log ; Log "Copy files from files directory:" ; Log
  foreach ($element in $files) 
  { # copy files from files directory
    if (-not (($element.Contains("#")) -or (($element.Trim() -eq ""))))
    {
      $split        = $element.Split(",")
      $CopyFileName = $split[0].Trim()
      $CopyFromFile = "files\" + $CopyFileName
      $CopyToDir    = $ct4laz + $split[1].Trim()

      if ($dbg)
      {
        Log "Element:     " $element      -ForegroundColor $COLOR_INFO
        Log "CopyFileName:" $CopyFileName -ForegroundColor $COLOR_INFO
        Log "CopyToDir:   " $CopyToDir    -ForegroundColor $COLOR_INFO
      }
      Log "CopyFromFile:" $CopyFromFile -ForegroundColor $COLOR_INFO

      if (-not (Test-Path $CopyFromFile))
        { Log "ERROR: Source file does not exist $CopyFromFile"        -ForegroundColor $COLOR_ERROR ; Continue }

      if (-not (Test-Path $CopyToDIr))
        { Log "ERROR: Destination directory does not exist $CopyToDir" -ForegroundColor $COLOR_ERROR ; Continue }

      Copy-Item -Path $CopyFromFile -Destination $CopyToDir
      Log
     }
  }
}


###########################
function DeleteNotNeeded
###########################
{
  Log; Log ; Log "Deleting not needed files/dirs in '$dest_components' and '$dest_examples':" ; Log
  foreach ($element in $remove) 
  { # delete all files and directories from remove list
    if (-not (($element.Contains("#")) -or (($element.Trim() -eq ""))))
    {
      $split           = $element.Split(",")
      $IncludeFileMask = $split[0].Trim()
      # Log $split.Count
      if ($split.Count -gt 1)
        { $ExcludeFilter   = $split[1].Trim() }
      else
        { $ExcludeFilter   = "" }
      Log "'$IncludeFileMask'" -NoNewline  -ForegroundColor $COLOR_INFO   # strange quotes but works 'Makefile*', '*.exe'
      if ($ExcludeFilter -ne "") # exclude filter is not empty string
        { Log " [-'$ExcludeFilter']" -NoNewline  -ForegroundColor $COLOR_INFO }  # strange quotes but works 'Makefile*', '*.exe [-pl_0_libs]' 
      if (-not ($element -contains $remove[$remove.length-1]))    # not last element?
        { Log ", " -NoNewLine }
      else
        { Log }
      Get-ChildItem $dest_components -include $IncludeFileMask -filter $ExcludeFilter -recurse | Remove-Item -recurse 
      Get-ChildItem $dest_examples   -include $IncludeFileMask -filter $ExcludeFilter -recurse | Remove-Item -recurse   # backup dir can only be deleted with last -recurse switch
    }  
  }
}


###########################
# main
###########################

CopyFromFreshCT
CopyFromOldCT
CopyFiles
DeleteNotNeeded

$EndTime          = Get-Date
$PassedTime       = $EndTime - $StartTime
$PassedSeconds    = [math]::Round($PassedTime.TotalSeconds, 0)      # $PassedSeconds = [math]::Round($PassedTime.Ticks / 10000000, 0)
$PassedSecondsStr = ([string]$PassedSeconds).PadLeft(6)
Log "      ___________________________________________________"
Log "     /                                                   \"
Log "    <   Script time: $PassedSecondsStr seconds.   Job finished !!!   >"
Log "     \___________________________________________________/"
