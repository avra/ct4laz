unit Simplemw;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dwsComp, Menus, ExtCtrls, StdCtrls, dwsVCLGUIFunctions, dwsExprs,
  dwsCompiler;

type

  { TMainForm }

  TMainForm = class(TForm)
    MESourceCode: TMemo;
    Splitter1: TSplitter;
    MainMenu: TMainMenu;
    MEResult: TMemo;
    MILoad: TMenuItem;
    MISave: TMenuItem;
    MIRun: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure MILoadClick(Sender: TObject);
    procedure MISaveClick(Sender: TObject);
    procedure MIRunClick(Sender: TObject);
  private

    FFileName : String;

    DelphiWebScript: TDelphiWebScript;
    dwsGUIFunctions: TdwsGUIFunctions;
  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

const
{$IFDEF Windows}
  pathMedia = '..\stripts4test\';
{$ELSE}
  pathMedia = '../stripts4test/';
{$ENDIF}



procedure TMainForm.FormCreate(Sender: TObject);
begin
   DelphiWebScript:=TDelphiWebScript.Create(self);
   dwsGUIFunctions:=TdwsGUIFunctions.Create(self);
end;

procedure TMainForm.MILoadClick(Sender: TObject);
begin
   OpenDialog.FileName:=FFileName;

   if OpenDialog.FileName='' then  OpenDialog.InitialDir:=ExpandFileName(SetDirSeparators(pathMedia));

   if OpenDialog.Execute then begin
      FFileName:=OpenDialog.FileName;
      MESourceCode.Lines.LoadFromFile(FFileName);
      Caption:='Simple - '+FFileName;
   end;
end;


procedure TMainForm.MISaveClick(Sender: TObject);
begin
   SaveDialog.FileName:=FFileName;

   if SaveDialog.FileName='' then  SaveDialog.InitialDir:=ExpandFileName(SetDirSeparators(pathMedia));

   if SaveDialog.Execute then begin
      FFileName:=SaveDialog.FileName;
      MESourceCode.Lines.SaveToFile(FFileName);
      Caption:='Simple - '+FFileName;
   end;
end;

procedure TMainForm.MIRunClick(Sender: TObject);
var
   prog : IdwsProgram;
   exec : IdwsProgramExecution;
begin
   prog:=DelphiWebScript.Compile(MESourceCode.Lines.Text);

   if prog.Msgs.Count>0 then
      MEResult.Lines.Text:=prog.Msgs.AsInfo
   else begin
      MEResult.Clear;
      try
         exec:=prog.Execute;
         MEResult.Lines.Text:=exec.Result.ToString;
      except
         on E: Exception do begin
            MEResult.Lines.Text:=E.ClassName+': '+E.Message;
         end;
      end;
   end;
end;

end.
