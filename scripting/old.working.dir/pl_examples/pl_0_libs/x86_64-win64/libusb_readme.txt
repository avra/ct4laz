
libusb ver 1.0.23 for Win64
MinGW64 version
Release at 29/08/2019

--------------------------------
https://libusb.info/
https://github.com/libusb/libusb

o Additional information:
  - The libusb 1.0 API documentation can be accessed at:
    http://api.libusb.info
  - For some libusb samples (including source), please have a look in examples/
  - For additional information on the libusb 1.0 Windows backend please visit:
    http://windows.libusb.info
  - The MinGW and MS generated DLLs are fully interchangeable, provided that you
    use the import libs provided or generate one from the .def also provided.
  - If you find any issue, please visit http://libusb.info/ and check the
    Support section