{    
    This file is part of CodeTyphon Studio (http://www.pilotlogic.com/)    

    See the file COPYING.FPC, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    
    ct9999
 **********************************************************************}

{$MODE objfpc}
{$H+}

unit ctutils;

interface

uses classes, sysutils {, dialogs}; // dialogs are not possible in console applications

const
 cnCTVer     = 670;
 cnCTVerStr  = '6.70';

 cnCTWindir  = 'C:\codetyphon';
 cnCTUnixdir = '/usr/local/codetyphon';


function ctGetDirTrail:String;

function ctGetHostCPU:String;
function ctGetHostOS:String;
function ctGetHostCPUOS:String;

function ctGetCTDir(const usetrail: boolean = false): string;
function ctGetRuntimesDir(const usetrail: boolean = false): string;
function ctGetRuntimesCPUOSDir(const usetrail: boolean = false): string;
function ctGetSettingsDir(const usetrail: boolean = false): string;
function ctGetbinSettingsDir(const usetrail: boolean = false): string;
function ctGetCodeOceanDir(const usetrail: boolean = false): string;
function ctGetCodeOceanLibrariesDir(const usetrail: boolean = false): string;

function ctGetDocFactoryDir(const usetrail: boolean = false): string;
function ctGetDocFactoryBuildDir(const usetrail: boolean = false): string;
function ctGetDocFactoryProjectsDir(const usetrail: boolean = false): string;
function ctGetDocFactoryXMLDir(const usetrail: boolean = false): string;    
function ctGetDocFactoryXCTDir(const usetrail: boolean = false): string;

function ctGetCTScriptsDir(const usetrail: boolean = false): string;

function ctGetCTHelpDir(const usetrail: boolean = false): string;
function ctGetCTHelpCHMDir(const usetrail: boolean = false): string;
function ctGetCTHelpHTMLDir(const usetrail: boolean = false): string;
function ctGetCTHelpPDFDir(const usetrail: boolean = false): string;

function ctGetFPCSrcDir(const usetrail: boolean = false): string;
function ctGetFPCRootDir(const usetrail: boolean = false): string;
function ctGetFPCBitsDir(const usetrail: boolean = false): string;
function ctGetFPCUnitsDir(const usetrail: boolean = false): string;
function ctGetFPCUnitsCPUOSDir(const usetrail: boolean = false): string;
function ctGetFPCBinDir(const usetrail: boolean = false): string;
function ctGetFPCBinCPUOSDir(const usetrail: boolean = false): string;
function ctGetFPCfpmDir(const usetrail: boolean = false): string;
function ctGetFPCfpmCPUOSDir(const usetrail: boolean = false): string;
function ctGetFPCEXEDir(const usetrail: boolean = false): string;
function ctGetFPCcfgDir(const usetrail: boolean = false): string;

function ctGetTyphonDir(const usetrail: boolean = false): string;
function ctGetTyphonBinDir(const usetrail: boolean = false): string;
function ctGetTyphonEXEDir(const usetrail: boolean = false): string;
function ctGetTyphonFilesDir(const usetrail: boolean = false): string;
function ctGetTyphonDocsDir(const usetrail:boolean=False):String;
function ctGetTyphonPkgLinksDir(const usetrail:boolean=False):String;
function ctGetTyphonToolsSrcDir(const usetrail:boolean=False):String;
function ctGetTyphonToolsBinDir(const usetrail:boolean=False):String;
function ctGetTyphonComponentsDir(const usetrail: boolean = false): string;

// ct4laz
function GetCodeOceanDirName: string;
function GetComponentsDirName: string;
function GetOriginalDirCT: string;
function ExistsOriginalDirCT: boolean;


implementation


///////////////
// ct4laz start
//

function GetOriginalDirCT: string;
begin
  {$IFDEF WINDOWS}
    Result := cnCTWindir;// + 'xx'; // fake CT existance (when needed for testing)
  {$ELSE}
    Result := cnCTUnixdir;
  {$ENDIF}
end;

function ExistsOriginalDirCT: boolean;
begin
  //if DirectoryExists(GetOriginalDirCT) then // ct installed
  //  Result := true
  //else
  //  Result := false;                        // ct not installed
  Result := false; // it is much better now to ignore ct even when it is installed
end;

function GetCodeOceanDirName: string;
begin
  if ExistsOriginalDirCT then
    Result := 'CodeOcean'    // ct
  else
    Result := 'pl_examples'; // ct4laz
end;

function GetComponentsDirName: string;
begin
  if ExistsOriginalDirCT then
    Result := 'typhon' + DirectorySeparator + 'components' // ct
  else
    Result := 'pl_components';                             // ct4laz
end;

//
// ct4laz end
/////////////


//-------------- CodeTyphon ------------------

function ctGetCTDir(const usetrail: boolean = false): string; // changed by avra
const
  CT4LAZ   = 'ct4laz';
  EXAMPLES = 'pl_examples';
  OPM      = 'onlinepackagemanager' + DirectorySeparator + 'packages';
var
  s: string;
  i: integer;
begin
  Result := GetOriginalDirCT; // at first asume that user has CT installed

  // if is commented because we do not care for CT location any more:
  // if not DirectoryExists(Result) then // if CT is not installed
  begin // no CT installed so try to guess ct4laz dir (this will work only if original ct4laz directory structure is not changed)
    s := ExtractFilePath(ParamStr(0)); // c:\prg\lazarus\fixesall\config_lazarus\ct4laz\examples\pl_opengl\samples\xbin
    i := Pos(CT4LAZ, s);
    if i > 0 then
      Result := LeftStr(s, i + Length(CT4LAZ) - 1) // 'ct4laz' found in executable path, so use it to extract ct4laz path
    else
    begin // try another way to guess ct4laz directory
      i := Pos(OPM, s); // let's test if package was installed via OPM
      if i > 0 then
        Result := LeftStr(s, i + Length(OPM) - 1) // 'onlinepackagemanager/packages' found in executable path, so use it to extract ct4laz path (actually ct4laz path does not exist in OPM, so use 'onlinepackagemanager/packages' as if it was ct4laz path
      else
      begin // installed in some unknown way, so let's test if 'pl_examples' is still somewhere in the path
        i := Pos(DirectorySeparator + EXAMPLES + DirectorySeparator, s);
        if i > 0 then
          Result := LeftStr(s, i - 1) // 'pl_examples' found in executable path, so use it to extract ct4laz path
      end;
    end;
    //ShowMessage(IntToStr(i) + ' ' + Result);
  end;

  if usetrail then Result := Result + DirectorySeparator;
end;

function ctGetCodeOceanDir(const usetrail: boolean = false): string; // changed by avra
begin
  Result := ctGetCTDir(true) + GetCodeOceanDirName;
  if usetrail then Result := Result + ctGetDirTrail;
end;

//.... DocFactory ....
function ctGetDocFactoryDir(const usetrail: boolean = false): string; 
begin
  result := ctGetCTDir(true) + 'DocFactory';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetDocFactoryBuildDir(const usetrail: boolean = false): string;
begin
  result := ctGetDocFactoryDir(true) + 'build';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetDocFactoryProjectsDir(const usetrail: boolean = false): string;
begin
  result := ctGetDocFactoryDir(true) + 'projects';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetDocFactoryXMLDir(const usetrail: boolean = false): string; 
begin
  result := ctGetDocFactoryDir(true) + 'xml';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetDocFactoryXCTDir(const usetrail: boolean = false): string;
begin
  result := ctGetDocFactoryDir(true) + 'xct';
  if usetrail then Result := Result + ctGetDirTrail;
end;

//.... Runtimes ....
function ctGetRuntimesDir(const usetrail: boolean = false): string; // changed by avra
begin
  Result := ctGetCodeOceanDir(true) + 'pl_0_libs'; // changed from 'binRuntimes'
  if not DirectoryExists(Result) then
    Result := '';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetRuntimesCPUOSDir(const usetrail: boolean = false): string; // changed by avra
begin
  Result := ctGetRuntimesDir(true);
  if (Result = '') or (Result = ctGetDirTrail) then
  begin
    Result := '';
    Exit;
  end;
  Result := Result + LowerCase({$i %FPCTARGETCPU%} + '-' + {$i %FPCTARGETOS%});
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetCodeOceanLibrariesDir(const usetrail: boolean = false): string;
begin
  Result := ctGetRuntimesCPUOSDir(usetrail);
end;

function ctGetSettingsDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(true) + 'Settings';
  if usetrail then Result := Result + ctGetDirTrail;
end;   

function ctGetbinSettingsDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(true) + 'binSettings';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetCTScriptsDir(const usetrail: boolean = false): string;
begin
  {$IFDEF WINDOWS}
  Result := ctGetCTDir(true) + 'ScriptsWin';
  {$ELSE}
  Result := ctGetCTDir(true) + 'ScriptsLin';
  {$ENDIF}
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetCTHelpDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(true) + 'help';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetCTHelpCHMDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTHelpDir(true) + 'chm';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetCTHelpHTMLDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTHelpDir(true) + 'html';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetCTHelpPDFDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTHelpDir(true) + 'pdf';
  if usetrail then Result := Result + ctGetDirTrail;
end;

//-------------- FPC --------------------------

function ctGetFPCSrcDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(True) + 'fpcsrc';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCRootDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(True) + 'fpc';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCBitsDir(const usetrail: boolean = false): string;
begin
  {$IFDEF CPU64}
    Result := ctGetFPCRootDir(True) + 'fpc64';
  {$ELSE}
    Result := ctGetFPCRootDir(True) + 'fpc32';
  {$ENDIF}

  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCBinDir(const usetrail: boolean = false): string;
begin
  Result := ctGetFPCBitsDir(True) + 'bin';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCBinCPUOSDir(const usetrail: boolean = false): string;
begin
 Result := ctGetFPCBinDir(True) + ctGetHostCPUOS;
 if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCUnitsDir(const usetrail: boolean = false): string;
begin
  Result := ctGetFPCBitsDir(True) + 'units';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCUnitsCPUOSDir(const usetrail:boolean=False):String;
begin
  Result := ctGetFPCUnitsDir(true) + ctGetHostCPUOS;
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCfpmDir(const usetrail: boolean = false): string;
begin
  Result := ctGetFPCBitsDir(True) + 'fpmkinst';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCfpmCPUOSDir(const usetrail: boolean = false): string;
begin
  Result := ctGetFPCfpmDir(True) + ctGetHostCPUOS;
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCEXEDir(const usetrail: boolean = false): string;
begin
  Result := ctGetFPCBinDir(true) + ctGetHostCPUOS;
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetFPCcfgDir(const usetrail: boolean = false): string;  
begin
  Result := ctGetFPCEXEDir(usetrail);
end; 

//-------------- Typhon IDE ---------------------

function ctGetTyphonDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(True) + 'typhon';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetTyphonBinDir(const usetrail: boolean = false): string;
begin
  {$IFDEF CPU64}
    Result := ctGetTyphonDir(True) + 'bin64';
  {$ELSE}
    Result := ctGetTyphonDir(True) + 'bin32';
  {$ENDIF}
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetTyphonEXEDir(const usetrail: boolean = false): string; 
begin
  Result := ctGetTyphonBinDir(usetrail);
end; 

function ctGetTyphonFilesDir(const usetrail: boolean = false): string;
begin
  Result := ctGetTyphonDir(True) + 'files';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetTyphonDocsDir(const usetrail : boolean = false): string;
begin
  Result := ctGetTyphonDir(True) + 'docs';
  if usetrail then Result := Result + ctGetDirTrail;
end; 

function ctGetTyphonPkgLinksDir(const usetrail: boolean = false): string;
begin
  Result := ctGetTyphonDir(True) + 'pkglinks';
  if usetrail then Result := Result + ctGetDirTrail;
end; 

function ctGetTyphonToolsSrcDir(const usetrail: boolean = false): string;
begin
  Result := ctGetTyphonDir(True) + 'tools';
  if usetrail then Result := Result + ctGetDirTrail;
end;

function ctGetTyphonToolsBinDir(const usetrail: boolean = false): string;
begin
  Result := ctGetTyphonBinDir(usetrail);
end;

function ctGetTyphonComponentsDir(const usetrail: boolean = false): string;
begin
  Result := ctGetCTDir(true) + GetComponentsDirName;
  if usetrail then Result := Result + ctGetDirTrail;
end;

//-------------- GENERAL ---------------------


function ctGetDirTrail:String;
begin
  Result := DirectorySeparator;
end;

function ctGetHostCPU:String;
begin
  result := '';

{$ifdef CPUi386}
  result := 'i386';
{$endif}

{$ifdef CPUx86_64}
  result := 'x86_64';
{$endif}

{$ifdef CPUARM}
  result := 'arm';
{$endif}

{$ifdef CPUaarch64}
  result := 'aarch64';
{$endif}    

{$ifdef CPUsparc}
  result := 'sparc';
{$endif}     

{$ifdef CPUsparc64}
  result := 'sparc64';
{$endif}     

{$ifdef CPUpowerpc}
  result := 'powerpc';
{$endif}     

{$ifdef CPUpowerpc64}
  result := 'powerpc64';
{$endif}     

{$ifdef CPUmips}
  result := 'mips';
{$endif}

{$ifdef CPUmips64}
  result := 'mips64';
{$endif}

end;

function ctGetHostOS:String; 
begin
  result := '';
{$ifdef Windows}
  {$ifdef CPU32}
    result := 'win32';
  {$endif}
  
  {$ifdef CPU64}
    result := 'win64';
  {$endif}
{$ELSE}
  {$ifdef linux}
    result := 'linux';
  {$endif}
  
  {$ifdef freebsd}
    result := 'freebsd';
  {$endif}
  
  {$ifdef openbsd}
    result := 'openbsd';
  {$endif}
  
  {$ifdef netbsd}
    result := 'netbsd';
  {$endif}
  
  {$ifdef dragonfly}
    result := 'dragonfly';
  {$endif}
  
  {$ifdef darwin}
    result := 'darwin';
  {$endif}
  
  {$IF DEFINED(solaris) or DEFINED(sunos)}
    result := 'solaris';
  {$endif}
{$endif}
end;

function ctGetHostCPUOS: string;
begin
  result := ctGetHostCPU + '-' + ctGetHostOS;
end;

end.
