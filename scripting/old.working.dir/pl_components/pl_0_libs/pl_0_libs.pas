{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_0_libs;

{$warn 5023 off : no warning about unused units}
interface

uses
  ctutils, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_0_libs', @Register);
end.
