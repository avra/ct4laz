{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_win_directx12;

{$warn 5023 off : no warning about unused units}
interface

uses
  DX12.D2D1, DX12.D2D1_3, DX12.D2D1EffectAuthor_1, DX12.D2D1Effects2, 
  DX12.D2D1SVG, DX12.D3D10, DX12.D3D10_1, DX12.D3D11, DX12.D3D11_1, 
  DX12.D3D11_2, DX12.D3D11_3, DX12.D3D11_4, DX12.D3D11On12, DX12.D3D12, 
  DX12.D3D12SDKLayers, DX12.D3D12Shader, DX12.D3D12Video, DX12.D3DCommon, 
  DX12.D3DCompiler, DX12.D3DCSX, DX12.D3DX10, DX12.D3DX11, DX12.D3DX11Effect, 
  DX12.DCommon, DX12.DocumentTarget, DX12.DWrite, DX12.DWrite3, DX12.DXGI, 
  DX12.DXGI1_2, DX12.DXGI1_3, DX12.DXGI1_4, DX12.DXGI1_5, DX12.DXGI1_6, 
  DX12.DXGIDebug, DX12.DXGIMessages, DX12.DXProgrammableCapture, DX12.OCIdl, 
  DX12.WinCodec, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_win_directx12', @Register);
end.
