
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit SBaseKernelMessages;

interface

uses LCLIntf, LCLType, Controls, Classes,forms;

const
  MsgInfo=0;
  MsgError=1;
  MsgTimeEvent=2;
type

 _TMessageToIDEEvent = procedure (Sender : TObject;Text:string;aType:integer) of object; //Connect 
 _TEngineStatusEvent = procedure (Sender : TObject; const Status:integer) of object;

 Procedure SendMessageToIDE(Sender : TObject;Text:string;aType:integer=0);

var _xxOnMessageToIDEEvent:_TMessageToIDEEvent;  //CommonKernelDebuggerDlg.TCommonKernelDebugger.Execute
    _xxEngineStatusEvent:_TEngineStatusEvent;

    _xxMainForm:TForm;   //This link to Main Form of Project/Player..etc

implementation

Procedure SendMessageToIDE(Sender : TObject;Text:string;aType:integer=0);
 begin
    if Assigned(_xxOnMessageToIDEEvent) then _xxOnMessageToIDEEvent(Sender,text,aType);
 end;

 {===== _xxEngineStatusEvent TBaseTimeEventsEngine SentMessageToKernel Codes...=============
 -1 Change CurrentTime
  0 Enable=false  ->For TimeEvent Engine
  1 Enable=true;  ->For TimeEvent Engine
  3 Total Change SceneEngine  ->For TimeEvent Engine
  5 Change FRunMode     ->For TimeEvent Engine
  15 Change FVMaxTime  ->For TimeEvent Engine
  16 Change FVMaxTime   ->For TimeEvent Engine }

end.
 
