
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit ExtraTGSysUtils;

interface

uses
  LCLIntf, LCLType,
  SysUtils, Classes, GIS_SysUtils;

type

  TObjPointLL = class(TObject)
  private
    FPointLL: TPointLL;
    function GetLL: TPointLL;
    procedure PutLL(LL: TPointLL);
    function GetDD: TPointDD;
    procedure PutDD(DD: TPointDD);
    function Get3D: TPoint3D;
  public
    property AsDD: TPointDD read GetDD write PutDD;
    property AsLL: TPointLL read GetLL write PutLL;
    property As3D: TPoint3D read Get3D;
  end;

  { this point Hold a naumber of Tobjpoint
    used from Tools}
  TSPointsStore = class(TList)
  private
    FOwnsObjects: boolean;
    FOnPaint: TNotifyEvent;
    FActive: boolean;
    function GetCenter: TPointLL;
    procedure SetCenter(NewCenter: TPointLL);
    function GetHeightFlag: boolean;
    procedure SetHeightFlag(Value: boolean);
  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
    function GetItem(Index: integer): TObjPointLL;
    procedure SetItem(Index: integer; aTObjPointLL: TObjPointLL);
  public
    Flags: TPointStoreFlagsSet;
    constructor Create; overload;
    constructor Create(AOwnsObjects: boolean); overload;
    procedure Paint;
    function Add(aTObjPointLL: TObjPointLL): integer;
    procedure Insert(Index: integer; aTObjPointLL: TObjPointLL);
    function AddLL(aTPointLL: TPointLL): integer;
    procedure InsertLL(Index: integer; aTPointLL: TPointLL);
    function Remove(aTObjPointLL: TObjPointLL): integer;
    function IndexOf(aTObjPointLL: TObjPointLL): integer;
    function FindInstanceOf(AClass: TClass; AExact: boolean = True; AStartAt: integer = 0): integer;
    procedure CopyToTPointStore(aPointStore: TPointStore);
    function CloneToTPointStore: TPointStore;
    procedure CloneFromTPointStore(aTPointStore: TPointStore);
    function PointOnPolyline(iLong, iLat, iTolerance: integer): boolean;
    function PointInPolygon(iLong, iLat: integer): boolean;
    procedure MoveCenter(dx, dy, dz: double);
    function PointStoreMER: TMER;
    property OwnsObjects: boolean read FOwnsObjects write FOwnsObjects;
    property Items[Index: integer]: TObjPointLL read GetItem write SetItem; default;
    property Center: TPointLL read GetCenter write SetCenter;
    property StoreHeight: boolean read GetHeightFlag write SetHeightFlag;
    property Active: boolean read fActive write fActive;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
  end;

implementation

//===================== TObjPointLL ==============================================================

function TObjPointLL.GetLL: TPointLL;
begin
  Result := fPointLL;
end;

procedure TObjPointLL.PutLL(LL: TPointLL);
begin
  fPointLL := LL;
end;

function TObjPointLL.GetDD: TPointDD;
begin
  Result := PointDDH(fPointLL.iLongX * (1 / GU_DEGREE), fPointLL.iLatY * (1 / GU_DEGREE), fPointLL.iHeightZ * (1 / GU_DEGREE));
end;

procedure TObjPointLL.PutDD(DD: TPointDD);
begin
  AsLL := PointLLH(DecimalToEarthUnits(DD.LongX), DecimalToEarthUnits(DD.LatY), DecimalToEarthUnits(DD.HeightZ));
end;

function TObjPointLL.Get3D: TPoint3D;
begin
  Result := Point3D(fPointLL.iLongX, fPointLL.iLatY, fPointLL.iHeightZ);
end;

//========================== TsPointsStore ============================================

//Return True if the PointStore store Height Data
function TsPointsStore.GetHeightFlag: boolean;
begin
  Result := (psHeight in Flags);
end;


//Param Value Indicates if the PointStore should store Height data
procedure TsPointsStore.SetHeightFlag(Value: boolean);
begin
  if Value <> (psHeight in Flags) then
  begin
    if Value then
      Include(Flags, psHeight)
    else
      Exclude(Flags, psHeight);
  end;
end;

procedure TSPointsStore.CopyToTPointStore(aPointStore: TPointStore);
var
  chec: integer;
begin
  if aPointStore = nil then
    exit;
  aPointStore.Clear;
  for chec := 0 to Count - 1 do
    aPointStore.Add(items[chec].asll);
end;

function TSPointsStore.CloneToTPointStore: TPointStore;
var
  chec: integer;
  OutObject: TPointStore;
begin
  OutObject := TPointStore.Create;
  for chec := 0 to Count - 1 do
  begin
    OutObject.Add(items[chec].asll);
  end;
  Result := OutObject;
end;

procedure TSPointsStore.CloneFromTPointStore(aTPointStore: TPointStore);
var
  chec: integer;
begin
  if aTPointStore = nil then
    exit;
  Clear;
  for chec := 0 to aTPointStore.Count - 1 do
    addll(aTPointStore.asll[chec]);

end;

{ PointOnPolyline
  @Param iLong Longitude of point to test.
  @Param iLat Latitude of point to test.
  @Param iTolerance distance away from the line the point is alowed to be to hit.
  @Return True if point is on a line in the object within the tolerance distance.
}

function TSPointsStore.PointOnPolyline(iLong, iLat, iTolerance: integer): boolean;
var
  ptLast, ptNew: TPointLL;
  idx: integer;
begin
  if Count > 0 then
  begin
    Result := True;
    ptLast := items[0].AsLL;
    for idx := 1 to Count - 1 do
    begin
      ptNew := items[idx].AsLL;
      if PointOnLine(iLong, iLat, ptLast.iLongX, ptLast.iLatY, ptNew.iLongX, ptNew.iLatY, iTolerance) then
        Exit;
      ptLast := ptNew;
    end;
  end;
  Result := False;
end;

{PointInPolygon
  @Param iLong Longitude of point to test.
  @Param iLat Latitude of point to test.
  @Return True if point is inside the object.
}

function TSPointsStore.PointInPolygon(iLong, iLat: integer): boolean;
var
  iZone, idx: integer;
  iX1, iX2, iY1, iY2: double;
  iPtr: TPointLL;
begin
  Result := False;

  if iLong >= 0 then
    iZone := 2
  else
    iZone := 1;

  iX2 := 0;
  iY2 := 0;
  for idx := Count - 1 downto 0 do
  begin
    iPtr := items[idx].AsLL;
    iX2 := iPtr.iLongX;
    iY2 := iPtr.iLatY;

    if (iZone = 1) and (iX2 >= 0) then
      continue;
    if (iZone = 2) and (iX2 <= 0) then
      continue;
    Break;
  end;

  idx := 0;
  while idx < Count do
  begin
    iPtr := items[idx].AsLL;
    iX1 := iPtr.iLongX;
    iY1 := iPtr.iLatY;

    Inc(idx);

    if iZone <> 3 then
    begin
      if (iZone = 1) and (iX1 > 0) then
        continue;
      if (iZone = 2) and (iX1 < 0) then
        continue;
    end;

    if ((((iY1 <= iLat) and (iLat < iY2)) or ((iY2 <= iLat) and (iLat < iY1))) and (iLong < MulDivFloat(
      (iX2 - iX1), (iLat - iY1), (iY2 - iY1)) + iX1)) then
      Result := not Result;

    iX2 := iX1;
    iY2 := iY1;
  end;
end;

{ Centroid
Calculates the Centroid of the points in the PointStore.
  @Return Point representing the averaged centroid of the object.
}
function TSPointsStore.GetCenter: TPointLL;
var
  idx, iCount: integer;
  eSumX, eSumY, eSumZ: extended;
begin
  eSumX := 0;
  eSumY := 0;
  eSumZ := 0;
  iCount := Count;
  for idx := 0 to iCount - 1 do
    with items[idx].AsLL do
    begin
      eSumX := eSumX + iLongX;
      eSumY := eSumY + iLatY;
      eSumZ := eSumZ + iHeightZ;
    end;

  if iCount = 0 then
    iCount := 1;

  Result := PointLLH(Round(eSumX / iCount), Round(eSumY / iCount), Round(eSumZ / iCount));
end;

procedure TSPointsStore.SetCenter(NewCenter: TPointLL);
var
  check: TPointLL;
  dx, dy, dz: double;
begin
  check := Center;
  dx := NewCenter.iLongX - check.iLongX;
  dy := NewCenter.iLatY - check.iLatY;
  dz := NewCenter.iHeightZ - check.iHeightZ;
  MoveCenter(dx, dy, dz);
end;

{** Moves all the points in the PointStore.
  @Param prLL New Position for Centroid.
}

procedure TSPointsStore.MoveCenter(dx, dy, dz: double);
var
  idx: integer;
  p3: TPointLL;
begin

  if psHeight in Flags then
  begin
    for idx := 0 to Count - 1 do
    begin
      p3 := items[idx].AsLL;
      P3.iLongX := P3.iLongX + dx;
      P3.iLatY := P3.iLatY + dy;
      P3.iHeightZ := P3.iHeightZ + dz;
    end;
  end
  else
    for idx := 0 to Count - 1 do
    begin
      p3 := items[idx].AsLL;
      P3.iLongX := P3.iLongX + dx;
      P3.iLatY := P3.iLatY + dy;
    end;

end;

function TSPointsStore.PointStoreMER: TMER;
var
  iMaxX, iMinX, iMaxY, iMinY, iMinPlus, iMinMinus: double;
  idx, iCross180Count: integer;
  iLongX, iLatY: double;
  iPtr: TPointLL;
  iLastZone: integer;

  procedure Cross180(iLong: double);
  var
    iZone: integer;
  begin
    iZone := Round(iLong / GU_90_DEGREE);
    if ((iZone < 0) and (iLastZone > 0)) or ((iZone > 0) and (iLastZone < 0)) then
      Inc(iCross180Count);
    iLastZone := iZone;
  end;

begin
  if Count = 0 then
  begin
    Result := MER(0, 0, 0, 0);
    Exit;
  end;

  iMinX := GMaxDouble;
  iMaxX := -GMaxDouble;
  iMinY := GMaxDouble;
  iMaxY := -GMaxDouble;
  iMinPlus := GMaxDouble;
  iMinMinus := -GMaxDouble;

  Cross180(items[Count - 1].asll.iLongX); { initialise Cross180 }
  iCross180Count := 0;

  for idx := 0 to Count - 1 do
  begin
    iPtr := items[idx].AsLL;
    iLongX := iPtr.iLongX;
    iLatY := iPtr.iLatY;

    Cross180(iLongX);

    if (iLongX >= 0) and (iLongX < iMinPlus) then
      iMinPlus := iLongX;
    if (iLongX < 0) and (iLongX > iMinMinus) then
      iMinMinus := iLongX;

    if iLongX < iMinX then
      iMinX := iLongX;
    if iLongX > iMaxX then
      iMaxX := iLongX;

    if iLatY < iMinY then
      iMinY := iLatY;
    if iLatY > iMaxY then
      iMaxY := iLatY;
  end;

  if (iCross180Count mod 2) <> 0 then
    Result := MER(-GU_180_DEGREE + 1, iMinY, GU_360_DEGREE - 2, iMaxY - iMinY)
  else if iCross180Count > 0 then
    Result := MER(iMinPlus, iMinY, (GU_180_DEGREE - iMinPlus) + (iMinMinus + GU_180_DEGREE), iMaxY - iMinY)
  else
    Result := MER(iMinX, iMinY, iMaxX - iMinX, iMaxY - iMinY);
end;

function TSPointsStore.AddLL(aTPointLL: TPointLL): integer;
var
  New: TObjPointLL;
begin
  New := TObjPointLL.Create;
  new.asll := aTPointLL;
  Result := inherited Add(new);
end;

procedure TSPointsStore.InsertLL(Index: integer; aTPointLL: TPointLL);
var
  New: TObjPointLL;
begin
  New := TObjPointLL.Create;
  new.asll := aTPointLL;
  inherited Insert(Index, New);
end;

function TSPointsStore.Add(aTObjPointLL: TObjPointLL): integer;
begin
  Result := inherited Add(aTObjPointLL);

end;

procedure TSPointsStore.Insert(Index: integer; aTObjPointLL: TObjPointLL);
begin
  inherited Insert(Index, aTObjPointLL);
end;

constructor TSPointsStore.Create;
begin
  inherited Create;
  FOwnsObjects := True;
end;

constructor TSPointsStore.Create(AOwnsObjects: boolean);
begin
  inherited Create;
  FOwnsObjects := AOwnsObjects;
  fActive := False;
end;

function TSPointsStore.FindInstanceOf(AClass: TClass; AExact: boolean; AStartAt: integer): integer;
var
  I: integer;
begin
  Result := -1;
  for I := AStartAt to Count - 1 do
    if (AExact and (Items[I].ClassType = AClass)) or (not AExact and Items[I].InheritsFrom(AClass)) then
    begin
      Result := I;
      break;
    end;
end;

function TSPointsStore.GetItem(Index: integer): TObjPointLL;
begin
  Result := TObjPointLL(inherited Items[Index]);
end;

function TSPointsStore.IndexOf(aTObjPointLL: TObjPointLL): integer;
begin
  Result := inherited IndexOf(aTObjPointLL);
end;

procedure TSPointsStore.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if OwnsObjects then
    if Action = lnDeleted then
      TObject(Ptr).Free;
  inherited Notify(Ptr, Action);
end;

function TSPointsStore.Remove(aTObjPointLL: TObjPointLL): integer;
begin
  Result := inherited Remove(aTObjPointLL);
end;

procedure TSPointsStore.SetItem(Index: integer; aTObjPointLL: TObjPointLL);
begin
  inherited Items[Index] := aTObjPointLL;
end;

procedure TSPointsStore.Paint;
begin
  if Assigned(fOnPaint) then
    fOnPaint(Self);
end;

end.
