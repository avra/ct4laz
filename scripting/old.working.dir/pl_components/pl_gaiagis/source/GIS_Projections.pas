
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_Projections;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  Classes, Graphics, SysUtils,
  GIS_SysUtils, GIS_Classes, GIS_XML, Math;

type

 {---------------------------- TProjectionModel -------------------------------------}
 //
TProjectionModel = class(TCustomProjectionModel)
  public
    constructor Create(Parent : TEarthProjection); override; 
    procedure LoadEnvironment( Element : TGXML_Element ); override;
    function  SaveEnvironment : TGXML_Element; override;
    function  PointStoreToXY(Points : TPointStore; iStart : integer; bClosed : Boolean) : Integer; override;
    function  LLToXY(iLong, iLat:Double; iIndex: Integer): Boolean; override;
    function  XYListVisible(iCount : Integer; var ClipCode : Byte) : Boolean; override;
    function  VisibleMER(const MER : TMER; var State : TEarthObjectStateSet) : Boolean; override;
    function  ExtentsLL : TGRectFloat; override;
    procedure PaintSurface; override;
  end;

  {---------------------------- TSphericalPrj ---------------------------------}
  //
  TSphericalPrj = class(TProjectionModel)
  private
    FTransform : TGMatrix; { Rotation and Scale matrix }
    FInvTransform : TGMatrix; { Inverse Rotation and Scale matrix }
    FEarthRadius, FEarthRadiusSquared : Extended;
  public
    constructor Create(Parent : TEarthProjection); override; 
    procedure PropertyChanged( ProjectionProperty : TProjectionProperty ); override;
    function  PointStoreToXY(Points:TPointStore; iStart:integer; bClosed:Boolean):Integer; override;
    procedure Assign( AModel : TPersistent ); override;
    function  PointLLToXY(const ptLL : TPointLL; iIndex : Integer) : Boolean; override;
    function  XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
    function  ExtentsLL : TGRectFloat; override;
    procedure PaintSurface; override;
  end;

  {---------------------------- TCartesianPrj ---------------------------------}
  //
  TCartesianPrj = class(TProjectionModel)
  private
    LastXrotation : Extended;
    LastYrotation : Extended;
  public
    constructor Create(Parent : TEarthProjection); override;  
    procedure PropertyChanged( ProjectionProperty : TProjectionProperty ); override;  
    function  PointLLToXY(const ptLL : TPointLL; iIndex : Integer) : Boolean; override;
    function  XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
    procedure PaintSurface; override;
  end;

  {---------------------------- TCartesianPrj ---------------------------------}
  //
  TFlatCartesianPrj = class(TCartesianPrj)
  private
    ZSin, ZCos: Extended;
  public
    procedure PropertyChanged( ProjectionProperty : TProjectionProperty ); override;
    function PointLLToXY(const ptLL : TPointLL; iIndex : Integer) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end;

  {---------------------------- TMercatorPrj ----------------------------------}
  //
  TMercatorPrj = class(TCartesianPrj)
  public
    function PointLLToXY(const ptLL : TPointLL; iIndex : Integer) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end;


implementation
 //======================== TProjectionModel =========================================
constructor TProjectionModel.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );

  Projection := Parent;
  Projection.GraticuleLongSteps := 72;
  Projection.GraticuleLatSteps := 144;
end;

function TProjectionModel.SaveEnvironment : TGXML_Element;
begin
  Result := TGXML_Element.Create;
  Result.AddAttribute( 'Class', ClassName );
end;

procedure TProjectionModel.LoadEnvironment( Element : TGXML_Element );
begin
  // Does nothing
end;

function TProjectionModel.XYListVisible(iCount : Integer; var ClipCode : Byte) : Boolean;
var
  idx:integer;
  iLeft, iTop, iRight, iBottom : Double;
  A, B, C, D, TL, BR : Byte;
  iWidth, iHeight : integer;
begin
  Result := False;
  iWidth := Projection.Earth.EarthCanvas.CanvasWidth;
  iHeight := Projection.Earth.EarthCanvas.CanvasHeight;

  if iCount > 1 then
  begin
    iLeft := GMaxDouble;
    iTop :=  GMaxDouble;
    iRight := -GMaxDouble;
    iBottom := -GMaxDouble;
    for idx := 0 to iCount - 1 do
      with Projection.Earth.EarthCanvas.gaPoints^[idx] do
      begin
        if X < iLeft then iLeft := X;
        if X > iRight then iRight := X;
        if Y < iTop then iTop := Y;
        if Y > iBottom then iBottom := Y;
      end;

    A := (Ord(iTop < 0) shl 2) or (Ord(iTop > iHeight) shl 3);
    B := Ord(iLeft < 0) or (Ord(iLeft > iWidth) shl 1);

    C := (Ord(iBottom < 0) shl 2) or (Ord(iBottom > iHeight) shl 3);
    D := Ord(iRight < 0) or (Ord(iRight > iWidth) shl 1);

    TL := A or B;
    BR := C or D;

    ClipCode := (TL shl 4) or BR;

    if (TL and BR) <> 0 then
      Result := not ((A = 8) or (B = 2) or (C = 4) or (D = 1)) else
      Result := True;
  end;
end;
 

function TProjectionModel.VisibleMER(const MER : TMER; var State : TEarthObjectStateSet) : Boolean;
var
  idx :Double;
  iFace,iCount: integer;
  ClipCode : Byte;

  function TestRect(iLeft, iTop, iRight, iBottom : double) : Boolean;
  begin
    Result := False;
    iFace := 0;

    Inc(iFace, Ord(LLToXY(iLeft, iTop, 0)));
    Inc(iFace, Ord(LLToXY(iLeft, iBottom, 1)));
    Inc(iFace, Ord(LLToXY(iRight, iBottom, 2)));
    Inc(iFace, Ord(LLToXY(iRight, iTop, 3)));
    // add in points around the edge of the rectangle if dealing with a large object
    iCount := 4;
    idx := iLeft + GU_DEGREE * 5;
    while idx < iRight do
    begin
      Inc(iFace, Ord(LLToXY(idx, iTop, iCount)));
      Inc(iCount);
      Inc(iFace, Ord(LLToXY(idx, iBottom, iCount)));
      Inc(iCount);
      idx := idx + GU_DEGREE * 5;
    end;

    idx := iTop + GU_DEGREE * 5;
    while idx < iBottom do
    begin
      Inc(iFace, Ord(LLToXY(iLeft, idx, iCount)));
      Inc(iCount);
      Inc(iFace, Ord(LLToXY(iRight, idx, iCount)));
      Inc(iCount);
      idx := idx + GU_DEGREE * 5;
    end;

    if iFace > 0 then
    begin
       Result := XYListVisible(iCount, ClipCode);
     //  Result := ParentGlobe.Renderer.XYListVisible(ParentGlobe.Renderer.Points, iCount);

      if Result then
      begin
{        if ParentGlobe.Projector.RenderBackFace then
        begin
          if iFace <> iCount then
            Include(State, rsFace);
          if iFace > 0 then
            Include(State, rsBackFace);
        end
        else}
        begin
          if iFace <> iCount then Include(State, osBackFace);
          if iFace > 0 then       Include(State, osFace);
        end;
      end;
    end;
  end;
begin
  Exclude( State, osFace );
  Exclude( State, osBackFace );

  with MER do
  try
    if iWidthX >= GU_360_DEGREE then
    begin
      Result := True;
      Exit;
    end;

    if ( iWidthX > GU_180_DEGREE ) or ( iLongX + iWidthX > GU_180_DEGREE ) then
    begin
      Result := TestRect(iLongX, iLatY, GU_180_DEGREE, iLatY + iHeightY) or
                TestRect(-GU_180_DEGREE, iLatY, -GU_180_DEGREE + iWidthX - (GU_180_DEGREE - iLongX), iLatY + iHeightY);
    end
    else
      Result := TestRect(iLongX, iLatY, iLongX + iWidthX, iLatY + iHeightY);
  except
    Result := true;
  end;
end;



function TProjectionModel.PointStoreToXY( Points : TPointStore; iStart : integer ; bClosed : Boolean) : Integer;
var
  idx : Integer;
begin
  Result := iStart;
  for idx := 0 to Points.Count - 1 do
    if PointLLToXY( Points.AsLL[idx], Result ) then
      Inc( Result );
end;

function TProjectionModel.LLToXY(iLong, iLat:Double; iIndex: Integer): Boolean;
var ptLL : TPointLL;
begin
  ptLL.iLongX := iLong;
  ptLL.iLatY := iLat;
  ptLL.iHeightZ := 0;
  Result := PointLLToXY( ptLL, iIndex );
end;

procedure TProjectionModel.PaintSurface;
var
  idx : integer;
  CM : Extended;
begin
  with Projection do
  begin
    CM := CentralMeridian;
    CentralMeridian := 0;
    with ExtentsLL do
    begin
      for idx := 0 to 35 do  LLToXY( Left + idx * GU_10_DEGREE, Top, idx );
      for idx := 0 to 17 do  LLToXY( Right - 1, Top + idx * GU_10_DEGREE, 36 + idx );
      for idx := 0 to 35 do  LLToXY( Right - 1 - idx * GU_10_DEGREE, Bottom, 54 + idx );
      for idx := 0 to 17 do  LLToXY( Left, Bottom - idx * GU_10_DEGREE, 90 + idx );
    end;
    CentralMeridian := CM;

    Earth.EarthCanvas.DrawClippedPoly( 108, [osClosed,osClipLeft,osClipTop,osClipBottom,osClipRight], 1 );
  end;
end;

function TProjectionModel.ExtentsLL: TGRectFloat;
begin
   Result := GRectFloat( -GU_180_DEGREE, -GU_90_DEGREE, GU_180_DEGREE, GU_90_DEGREE )
end;


//========================= TSphericalPrj ======================================
constructor TSphericalPrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );

  Projection.Flags := [pfContinuous,pfTextureMap];

  Projection.GraticuleLongSteps := 36;
  Projection.GraticuleLatSteps := 72;
end;
procedure TSphericalPrj.Assign( AModel : TPersistent );
begin
  inherited Assign( AModel );

  if AModel is TSphericalPrj then
  begin
    FTransform := TSphericalPrj( AModel ).FTransform;
    FInvTransform := TSphericalPrj( AModel ).FInvTransform;
    FEarthRadius := TSphericalPrj( AModel ).FEarthRadius;
    FEarthRadiusSquared := TSphericalPrj( AModel ).FEarthRadiusSquared;
  end;
end;

function TSphericalPrj.ExtentsLL: TGRectFloat;
begin
  Result := GRectFloat( -EARTHRADIUS ,-EARTHRADIUS ,EARTHRADIUS ,EARTHRADIUS )
end;

procedure TSphericalPrj.PropertyChanged( ProjectionProperty : TProjectionProperty );
begin
  with Projection do
  begin
    case ProjectionProperty of
      ppXRotation : XRotation := Min( Max( XRotation, -90 ), 90 );
      ppYRotation : YRotation := Mod180Float( YRotation );
      ppZRotation : ZRotation := Mod180Float( ZRotation );
      ppScaleFactor :
      begin
        FEarthRadius := EARTHRADIUS * ScaleFactor;
        FEarthRadiusSquared := FEarthRadius * FEarthRadius;
      end;
      else
        Exit;
    end;
    QuatToMatrix( EulerToQuat( XRotation, YRotation, ZRotation ), FTransform );
    TransposeMatrix( FTransform, FInvTransform );

    ScaleMatrix( FTransform, ScaleFactor );
    ScaleMatrix( FInvTransform, 1 / ScaleFactor );
  end;
end;

function TSphericalPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  eTmpX, eTmpY, eTmpZ : Extended;
  eX, eY, eZ : Extended;
begin
  { XY to World }
  eTmpX := iX - Projection.XOrigin;
  eTmpY := iY - Projection.YOrigin;
  eTmpZ := FEarthRadiusSquared - eTmpX * eTmpX - eTmpY * eTmpY;

  { check to see if the coords are outside the Earth }
  Result := eTmpZ >= 0;

  if Result then
  begin
    { World To LL }
    eTmpZ := -Sqrt( eTmpZ );

    eX := ( FInvTransform[0] * eTmpX ) + ( FInvTransform[1] * eTmpY ) + ( FInvTransform[2] * eTmpZ );
    eY := ( FInvTransform[3] * eTmpX ) + ( FInvTransform[4] * eTmpY ) + ( FInvTransform[5] * eTmpZ );
    eZ := ( FInvTransform[6] * eTmpX ) + ( FInvTransform[7] * eTmpY ) + ( FInvTransform[8] * eTmpZ );

    with Projection.Earth.EarthCanvas.gaPoints^[iIndex] do
    begin
      X := ( ArcTan2( eX, eZ ) * GU_FROMRADIANS );
      Y := ( ArcSin( eY * ( 1 / EARTHRADIUS ) ) * GU_FROMRADIANS );
    end;
  end;
end;

function TSphericalPrj.PointStoreToXY( Points : TPointStore; iStart : integer;  bClosed : Boolean ) : Integer;
var
  idx : Integer;
  eLastX, eLastY, iXorg, iYorg : Extended;
  Array3D : DynArray;
  Ptr3D : PTPoint3D;
  iX, iY, iZ : double;
  bFlag, bBackFace : Boolean;
  eTmpX, eTmpY, eTmp : Extended;
begin
  Result := 0;

  bBackFace := Projection.RenderBackFace;
  iXorg := Projection.XOrigin;
  iYorg := Projection.YOrigin;
  eLastX := GMaxDouble;
  eLastY := GMaxDouble;

  // project 3D world data to XY screen
  Array3D := Points.Point3DArray( goCache3Dpoints in Projection.Earth.EarthOptions );
  for idx := 0 to Array3D.Count - 1 do
  begin
    Ptr3D:=DynArrayPtr( Array3D, idx);
    iX := Ptr3D^.iWorldX;
    iY := Ptr3D^.iWorldY;
    iZ := Ptr3D^.iWorldZ;

    // convert to XY
    eTmpX := FTransform[0] * iX + FTransform[1] * iY + FTransform[2] * iZ;
    eTmpY := FTransform[3] * iX + FTransform[4] * iY + FTransform[5] * iZ;
    bFlag := FTransform[6] * iX + FTransform[7] * iY + FTransform[8] * iZ <= 0.0;

    if bFlag = bBackFace then
    begin
      if not bClosed then
        Continue
      else
      begin // adjust point to lie on the horizon
        eTmp := FEarthRadius / Hypot( eTmpX, eTmpY );
        eTmpX := eTmpX * eTmp;
        eTmpY := eTmpY * eTmp;
      end;
    end;

    if ( Abs( eLastX - eTmpX ) > 1 ) or ( Abs( eLastY - eTmpY ) > 1 ) then
    begin
      eLastX := eTmpX;
      eLastY := eTmpY;
      Projection.Earth.EarthCanvas.gaPoints^[Result] := PointDouble( iXorg + eTmpX , iYorg + eTmpY);
      Inc( Result );
    end;
  end;

  DynArrayFree( Array3D );  // dispose of Array 3D
end;

function TSphericalPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  eTmpX, eTmpY : Extended;
  bFlag : Boolean;
begin
  with Point3D( ptLL.iLongX, ptLL.iLatY, ptLL.iHeightZ ) do
  begin
    { convert to XY }
    eTmpX := FTransform[0] * iWorldX + FTransform[1] * iWorldY + FTransform[2] * iWorldZ;
    eTmpY := FTransform[3] * iWorldX + FTransform[4] * iWorldY + FTransform[5] * iWorldZ;
    bFlag := ( FTransform[6] * iWorldX + FTransform[7] * iWorldY + FTransform[8] * iWorldZ ) <= 0.0;

    if ( not bFlag ) and ( PtLL.iHeightZ > 0 ) then  bFlag := Hypot( eTmpX, eTmpY ) > FEarthRadius;

    Result := bFlag <> Projection.RenderBackFace;

    Projection.Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble( Projection.XOrigin+eTmpX, Projection.YOrigin +eTmpY);
  end;
end;

procedure TSphericalPrj.PaintSurface;
type
  TPoint3f = record
    x, y, z : Extended;
  end;
const
  SEGMENTS = 90;
  v : array[0..23] of TPoint3f = (
    ( x : 0.99999999; y : 0.0; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : 0.99999999 ),
    ( x : 0.0; y : 0.99999999; z : 0.0 ),

    ( x : 0.99999999; y : 0.0; z : 0.0 ),
    ( x : 0.0; y : 0.99999999; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : - 0.99999999 ),

    ( x : 0.0; y : - 0.99999999; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : 0.99999999 ),
    ( x : 0.99999999; y : 0.0; z : 0.0 ),

    ( x : 0.0; y : - 0.99999999; z : 0.0 ),
    ( x : 0.99999999; y : 0.0; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : - 0.99999999 ),

    ( x : 0.0; y : 0.99999999; z : 0.0 ),
    ( x : - 0.99999999; y : 0.0; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : - 0.99999999 ),

    ( x : 0.0; y : 0.99999999; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : 0.99999999 ),
    ( x : - 0.99999999; y : 0.0; z : 0.0 ),

    ( x : - 0.99999999; y : 0.0; z : 0.0 ),
    ( x : 0.0; y : - 0.99999999; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : - 0.99999999 ),

    ( x : - 0.99999999; y : 0.0; z : 0.0 ),
    ( x : 0.0; y : 0.0; z : 0.99999999 ),
    ( x : 0.0; y : - 0.99999999; z : 0.0 ) );
var
  idx, iLevel : Integer;
  eLastX, eLastY, eX, eY : Extended;
  eCosAngle, eSinAngle : Extended;
  WorldTri : TTriangleLL;
  bNegativeHemisphere : Boolean;
  iCount : integer;

 {---------------------------------------------------------------------------}
  procedure DivideTriangle( const A, B, C : TPoint3f; const iLevel : integer );
  var
    v1, v2, v3 : TPoint3f;

  {--------------------------------------}
    function AddVectors( const v1, v2 : TPoint3f ) : TPoint3f;
    var
      d : Extended;
    begin
      with Result do
      begin
        x := v1.x + v2.x;
        y := v1.y + v2.y;
        z := v1.z + v2.z;
    { Normalise Result vector }
        d := sqrt( x * x + y * y + z * z );
        if d > 0 then
        begin
          x := x / d;
          y := y / d;
          z := z / d;
        end;
      end;
    end;

  {------------------------------------------------------}
    function ToPolar( const v : TPoint3f ) : TPointLL;
    var  iLong : Extended;
    begin
   {Convert to Polar coords taking hemisphere into account }
      iLong := ( ArcTan2( v.x, v.z ) * GU_FROMRADIANS ) - 1;
      if bNegativeHemisphere then iLong := -abs( iLong );
      Result := PointLL( iLong, ( ArcSin( v.y ) * GU_FROMRADIANS ));
    end;

  begin
    if iLevel > 0 then
    begin
      iCount := 0;
      Inc( iCount, Ord( PointLLToXY( ToPolar( A ), 0 ) ) );
      Inc( iCount, Ord( PointLLToXY( ToPolar( B ), 1 ) ) );
      Inc( iCount, Ord( PointLLToXY( ToPolar( C ), 2 ) ) );

      if iCount > 0 then
      begin
        v1 := AddVectors( A, B );
        v2 := AddVectors( A, C );
        v3 := AddVectors( B, C );

        DivideTriangle( A, v2, v1, iLevel - 1 );
        DivideTriangle( B, v1, v3, iLevel - 1 );
        DivideTriangle( C, v3, v2, iLevel - 1 );
        DivideTriangle( v1, v2, v3, iLevel - 1 );
      end;
      Exit;
    end;
    WorldTri[0] := ToPolar( A );
    WorldTri[1] := ToPolar( B );
    WorldTri[2] := ToPolar( C );
    with Projection.Earth do
      EarthCanvas.RenderTextureTriangle( WorldTri, BackgroundTextureMap );
  end;

begin
  with Projection do
    if Earth.BackgroundTextureMap.IsTextureOK and ( pfTexturemap in Flags ) then
    begin
    { Adjust sub division level to suit scalefactor - performance improvement }
      iLevel := 3;
      if ScaleFactor > 0.0000002 then Inc( iLevel );
      if ScaleFactor > 0.0000002 then Inc( iLevel );

      bNegativeHemisphere := False;
      for idx := 0 to 3 do
        DivideTriangle( v[idx * 3], v[idx * 3 + 1], v[idx * 3 + 2], iLevel );

    { Switch to western hemisphere }
      bNegativeHemisphere := True;
      for idx := 4 to 7 do
        DivideTriangle( v[idx * 3], v[idx * 3 + 1], v[idx * 3 + 2], ilevel );
    end
    else
    begin
      eY := 0;
      eX := FEarthRadius;

      Projection.Earth.EarthCanvas.gaPoints^[0] := PointDouble( Projection.XOrigin + eX , Projection.YOrigin +eY);
                                      SinCos( ( GU_360_DEGREE / SEGMENTS ) * GU_TORADIANS, eSinAngle, eCosAngle );

      for idx := 1 to SEGMENTS - 1 do
      begin
        eLastX := eX;
        eLastY := eY;
        eX := eLastX * eCosAngle - eLastY * eSinAngle;
        eY := eLastX * eSinAngle + eLastY * eCosAngle;
        Projection.Earth.EarthCanvas.gaPoints^[idx] := PointDouble( Projection.XOrigin + eX, Projection.YOrigin +eY);
      end;
      Earth.EarthCanvas.DrawClippedPoly( SEGMENTS,[osClosed,osClipLeft,osClipTop,osClipBottom,osClipRight], 1 );
    end;
end;
 //================================== TCartesianPrj =========================================
constructor TCartesianPrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );

  { Allow texture mapping for this projection }
  Projection.Flags := [pfTextureMap];
end;

procedure TCartesianPrj.PropertyChanged( ProjectionProperty : TProjectionProperty );
begin
  with Projection do
    case ProjectionProperty of
      ppXRotation :
        begin
          case Projection.XAxisChanges of
          xaCentralParallel :
            if ( CentralParallel > -GU_90_DEGREE ) and ( CentralParallel < GU_90_DEGREE ) then
              CentralParallel := Mod180Float(( XRotation - LastXRotation ) + CentralParallel );
          xaYOrigin :
            begin
              LLToXY( ( YRotation * GU_DEGREE ), -(( XRotation - LastXRotation ) * GU_DEGREE ), 0 );
              YOrigin := (Earth.EarthCanvas.gaPoints^[0].Y);
//              Projection.Earth.UpdateScrollBars;
            end;
          end;
          LastXRotation := XRotation;
        end;
      ppYRotation :
        begin
          case Projection.YAxisChanges of
          yaCentralMeridian :
                 CentralMeridian := Mod180Float(( YRotation - LastYRotation ) + CentralMeridian );
          yaXOrigin :
            begin
              LLToXY( -(( YRotation - LastYRotation ) * GU_DEGREE), -( XRotation * GU_DEGREE ), 0 );
              XOrigin := (Earth.EarthCanvas.gaPoints^[0].X);
//              Projection.Earth.UpdateScrollBars;
            end;
          end;
          LastYRotation := YRotation;
        end;
    end;
end;


function TCartesianPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  iLong, iLat : Double;
  eRecipSF : Extended;
begin
  Result := False;
  { XY to World }
  with Projection do
  try
    eRecipSF := 1 / ScaleFactor;
    iLong := Mod180Float( ( ( iX - XOrigin ) * eRecipSF + CentralMeridian * GU_DEGREE ));
    iLat := -( ( iY - YOrigin ) * eRecipSF );

 //check to see if the coords are outside the Earth
    with ExtentsLL do
      Result := ( iLong >= Left ) and ( iLong <= Right ) and ( iLat >= Top ) and ( iLat <= Bottom );

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble( iLong, iLat );
  except
  end;
end;

function TCartesianPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
begin
 // check to see if the coords are outside the Earth
  with Projection, ptLL do
  begin
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
      XOrigin + ( Mod180Float( iLongX - ( CentralMeridian * GU_DEGREE )) * ScaleFactor ),
      YOrigin - iLatY * ScaleFactor );
  end;
end;

procedure TCartesianPrj.PaintSurface;
begin
  with Projection do
    if Earth.BackgroundTextureMap.IsTextureOK and ( pfTextureMap in Flags ) then
      Earth.EarthCanvas.RenderTextureMap( Earth.BackgroundTextureMap )
    else
      inherited PaintSurface;
end;

//============================  TMercatorPrj ==========================================
function TMercatorPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  iLong, iLat : Extended;
  eY : Extended;
begin
  { XY to World }
  with Projection do
  begin
    iLong := Mod180Float( ( ( iX - XOrigin ) / ScaleFactor + CentralMeridian * GU_DEGREE ));

    eY := ( iY - YOrigin ) / ScaleFactor;
    iLat := ( ( 2.0 * arctan( Exp( -eY * GU_TORADIANS ) ) - HalfPI ) * GU_FROMRADIANS );
  end;

  { check to see if the coords are outside the Earth }
  with Projection.ExtentsLL do
    Result := ( iLong >= Left ) and ( iLong <= Right ) and ( iLat >= Top ) and ( iLat <= Bottom );

  if Result then
    Projection.Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble( iLong, iLat );
end;

function TMercatorPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  eY, eSLat, eCLat : Extended;
  iLatY : Double;
begin
  iLatY := ptLL.iLatY;
  with Projection do
  begin
    with ExtentsLL, ptLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );


    if iLatY >= GU_90_DEGREE then  iLatY :=  GU_90_DEGREE - 1;
    if iLatY <= -GU_90_DEGREE then iLatY := -GU_90_DEGREE + 1;

    SinCos( ( iLatY * GU_TORADIANS ) * 0.5 + QuarterPI, eSLat, eCLat );

    eY := Ln( eSLat / eCLat ) * GU_FROMRADIANS;

    Projection.Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
      XOrigin + ( Mod180Float( ptLL.iLongX - ( CentralMeridian * GU_DEGREE)) * ScaleFactor ),
      YOrigin - ( eY * ScaleFactor ));
  end;
end;

//============================= TFlatCartesianPrj =============================================

function TFlatCartesianPrj.PointLLToXY(const ptLL: TPointLL; iIndex: Integer): Boolean;
var Lon, Lat: Extended;
begin
  with Projection, ptLL do
  begin
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    Lon := Mod180Float( iLongX - ( CentralMeridian * GU_DEGREE ));
    Lat := iLatY;

    Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
      XOrigin + (( ZCos * Lon - ZSin * Lat) * ScaleFactor),
      YOrigin - (( ZSin * Lon + ZCos * Lat) * ScaleFactor));
  end;
end;

procedure TFlatCartesianPrj.PropertyChanged(ProjectionProperty: TProjectionProperty);
begin
  inherited;

  with Projection do
    SinCos(ZRotation * DEG_TORADIANS, ZSin, ZCos);
end;

function TFlatCartesianPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  iLong, iLat : Extended;
  eX, eY, eRecipSF : Extended;
begin
  Result := False;
  { XY to World }
  with Projection do
  try
    eRecipSF := 1 / ScaleFactor;

    eX := (iX - XOrigin) * eRecipSF;
    eY := -((iY - YOrigin) * eRecipSF);

    iLong := Mod180Float(((( ZCos * eX + ZSin * eY) + CentralMeridian * GU_DEGREE )));
    iLat := ( -ZSin * eX + ZCos * eY);

    { check to see if the coords are outside the Earth }
    with ExtentsLL do
      Result := ( iLong >= Left ) and ( iLong <= Right ) and ( iLat >= Top ) and ( iLat <= Bottom );

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble( iLong, iLat );
  except
  end;
end;

initialization
  RegisterClasses( [TCartesianPrj, TMercatorPrj, TSphericalPrj,TFlatCartesianPrj] );
end.
