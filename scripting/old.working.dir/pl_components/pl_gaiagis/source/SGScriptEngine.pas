
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit SGScriptEngine;



interface
uses SysUtils, Classes,{SBaseScriptEngine,}Dialogs,GIS_Classes,
    SGISProject,SGISEngine;

type

TGScriptEngine = class(TBaseScriptEngine)
  private
    FProject:TGProject;
    FGISEngine:TGISEngine;
  protected
   Procedure CommonRegisterVars;override;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   
   Function  IsScriptFileFormat(Filename:string):boolean;override;
   Property  GISEngine:TGISEngine read FGISEngine write FGISEngine;
   Property  Project:TGProject read FProject write FProject;
  published

  end;

const    
GScriptUnitFileFilter = 'GaiaCAD Scenario file (*.Gsr)|*.Gsr|Any file (*.*)|*.*';
GScriptUnitFileExt='Gsr';


implementation
  uses Forms,Clipbrd{,CommonWinFunctions};
//========================== TScriptEngine ====================
constructor TGScriptEngine.Create(AOwner: TComponent);
  begin
   inherited Create(AOwner);
   FGISEngine:=nil;
  end;

destructor TGScriptEngine.Destroy;
begin
    FGISEngine:=nil;
    inherited Destroy;
end;

Function  TGScriptEngine.IsScriptFileFormat(Filename:string):boolean;
 var sss:string;
 begin
  result:=false;
  sss:=ExtractFileExt(Filename);
  if (SameText(sss,'.'+GScriptUnitFileExt))  then
   result:=true;
 end;

//------------------------RegisterVars------------------------------------
Procedure TGScriptEngine. CommonRegisterVars;

//................................................
  procedure AddObjectToScript2(Instance : TObject; const Name : string; Global : boolean);
    begin
    AddObjectToScript(Instance,Name,Global);
   
    if Instance is TComponent then
     if SameText(Name,TComponent(Instance).Name)=false then
       AddObjectToScript(Instance,TComponent(Instance).Name,Global);
   end;

 //...............................................
var i:integer;
 begin
  inherited CommonRegisterVars;

try
  //................... Common Vars ....................
  AddObjectToScript(Application,'Application',true);
  AddObjectToScript(Screen,'Screen',true);
  AddObjectToScript(Clipboard,'Clipboard',true);
  AddObjectToScript(OpSystem,'OpSystem',true);

  AddObjectToScript2(self,'Scripter',false);
  //.............. GaiaCAD Vars ........................
  
  AddObjectToScript2(FProject,'Project',false);
  AddObjectToScript2(FGISEngine,'GISEngine',false);

  if Assigned(FGISEngine.Earth ) then
    AddObjectToScript2(FGISEngine.Earth,'Earth',false);

  if Assigned(FGISEngine.ViewsLibrary ) then
    AddObjectToScript2(FGISEngine.ViewsLibrary,'ViewsLibrary',false);

  if Assigned(FGISEngine.LayersLibrary ) then
    AddObjectToScript2(FGISEngine.LayersLibrary,'LayersLibrary',false);

  if Assigned(FGISEngine.FilesLibrary ) then
    AddObjectToScript2(FGISEngine.FilesLibrary,'FilesLibrary',false);


 if Assigned(FGISEngine.Earth ) then
 if Assigned(FGISEngine.Earth.Layers ) then
    AddObjectToScript2(FGISEngine.Earth.Layers,'Layers',false);

 //........ for extra Managers ......................
   if FGISEngine.VclsLibraryStore<>nil then
     for i:=0 to FGISEngine.VclsLibraryStore.ComponentCount-1 do
         AddObjectToScript(FGISEngine.VclsLibraryStore.Components[i],
                           FGISEngine.VclsLibraryStore.Components[i].name,False );

 finally
 end;

 end;


Initialization
 RegisterClass(TGScriptEngine);

end.
