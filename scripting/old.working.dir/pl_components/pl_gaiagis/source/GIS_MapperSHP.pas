
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_MapperSHP;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  SysUtils, Classes, GIS_DBFReader, GIS_EarthBase, GIS_SysUtils, GIS_EarthObjects,
  GIS_Classes, GIS_XML, GIS_MMStream, GIS_EarthStreams, FileUtil, lazfileutils;

const
  TG_SHP_READERVERSION = $0103;

type
  TSHPItem = record
    iOffset : integer;
    iDBFRecordID : integer;
  end;
  TSHPItemPtr = ^TSHPItem;

  {---------------------------- TEarthSHPReader -------------------------------}
  TEarthSHPReader = class(TEarthFileReader)
  private
    FLongXDivisor : Double;
    FLatYDivisor : Double;
    FLongXShift : Double;
    FLatYShift : Double;
    FTitleColumn : integer;
    FDataItems : DynArray;
    procedure SetTitleColumn( Value : integer );
  protected
    DBF : TEarthDBFReader;
    SHPStream : TStream;

    function InternalOpen : Boolean; override;
    function InternalClose : Boolean; override;

    function CorrectX( X: Extended ): Extended;
    function CorrectY( Y: Extended ): Extended;

    function ReadBigEndianInt : integer;
    function ReadLittleEndianInt : integer;

    function MapDataHeader( aStream : TStream ) : Boolean; override;
    function MapDataObject( aStream : TStream; iIndex : integer ) : Boolean; override;

    function ReadPolyObject( bPolygon : Boolean ) : TGeoDataObject;
    function ReadMultiPointObject : TGeoDataObject;
  public
    constructor Create(aLayer:TEarthLayer); override;
    procedure LoadEnvironment( Element : TGXML_Element ); override;
    function SaveEnvironment : TGXML_Element; override;
    procedure SaveMetaData; override;
    procedure LoadMetaData; override;

    function LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject; override;
  published
    property LongXDivisor : Double read FLongXDivisor write FLongXDivisor;
    property LatYDivisor : Double read FLatYDivisor write FLatYDivisor;
    property LongXShift : Double read FLongXShift write FLongXShift;
    property LatYShift : Double read FLatYShift write FLatYShift;
    property TitleColumn : integer read FTitleColumn write SetTitleColumn;
  end;

implementation

Uses GIS_Resource,DlgDbFieldSelector;

{-------------------------------------------------------------------------}
procedure SwapWord( iLength : integer; wordP : PChar );
var
	idx : integer;
	cTmp : char;
begin
	for idx := 0 to iLength div 2 - 1 do
	begin
		cTmp := wordP[idx];
		wordP[idx] := wordP[iLength - idx - 1];
		wordP[iLength - idx - 1] := cTmp;
	end;
end;

{-------------------------------------------------------------------------}
constructor TEarthSHPReader.Create(aLayer:TEarthLayer);
begin
  inherited;
  FTitleColumn := -1;
  LongXDivisor := 1;
  LatYDivisor := 1;
  LongXShift := 0;
  LatYShift := 0;
end;

{-------------------------------------------------------------------------}
procedure TEarthSHPReader.SetTitleColumn( Value : integer );
begin
  if Value <> FTitleColumn then
  begin
    FTitleColumn := Value;
    UnloadObjects;
  end;
end;

{-------------------------------------------------------------------------}
function TEarthSHPReader.CorrectX( X: Extended ): Extended;
begin
  Result := LongXShift + X / LongXDivisor;
end; // CorrectX

{-------------------------------------------------------------------------}
function TEarthSHPReader.CorrectY( Y: Extended ): Extended;
begin
  Result := LatYShift + Y / LatYDivisor;
end; // CorrectY

{-------------------------------------------------------------------------}
function TEarthSHPReader.ReadBigEndianInt : integer;
begin
	SHPStream.Read( Result, SizeOf( integer ));
	SwapWord( 4, PChar( @Result ));
end;

{-------------------------------------------------------------------------}
function TEarthSHPReader.ReadLittleEndianInt : integer;
begin
	SHPStream.Read( Result, SizeOf( integer ));
end;

{-------------------------------------------------------------------------}
function TEarthSHPReader.ReadPolyObject( bPolygon : Boolean ) : TGeoDataObject;
var
	idx, iParts, iChain : integer;
	eTmp, eX, eY : double;
	iPoints : integer;
  offsets : DynArray;
begin
  Result := TGeoDataObject.Create( Self );

	for idx := 0 to 3 do
		SHPStream.Read( eTmp, SizeOf( double ));	{ read the box for the object }

	iParts := ReadLittleEndianInt;	{ Number of parts }
	iPoints := ReadLittleEndianInt;	{ Number of points }

  offsets := DynArrayCreate( SizeOf( integer ), iParts + 1 );
  DynArraySetAsInteger( Offsets, iParts, iPoints );

  // Read all the offsets
  for idx := 0 to iParts - 1 do
    DynArraySetAsInteger( Offsets, idx, ReadLittleEndianInt );

  Result.PointsGroups.Count := iParts;
	for idx := 0 to iParts - 1  do
    Result.PointsGroups[idx].Count := DynArrayAsInteger( Offsets, idx+1 ) - DynArrayAsInteger( Offsets, idx );

  iChain := 0;
	for idx := 0 to iPoints - 1 do
	begin
    if idx = DynArrayAsInteger( Offsets, iChain + 1) then
      Inc( iChain );
		SHPStream.Read( eX, SizeOf( double ));	{ X }
		SHPStream.Read( eY, SizeOf( double ));	{ Y }
    Result.PointsGroups[iChain].AsLL[idx - DynArrayAsInteger( Offsets, iChain )] :=
      DecimalToPointLL( CorrectX( eX ), CorrectY( eY ));
  end;

  Result.Closed := bPolygon;

  DynArrayFree( offsets );
end;

{-------------------------------------------------------------------------}
function TEarthSHPReader.ReadMultiPointObject : TGeoDataObject;
var
	idx, iPoints : integer;
	eTmp, eX, eY : double;
begin
	for idx := 0 to 3 do
		SHPStream.Read( eTmp, SizeOf( double ));	{ read the box for the object }
	iPoints := ReadLittleEndianInt;	{ Number of points }

  Result := TGeoDataObject.Create( Self );

	for idx := 1 to iPoints do
	begin
		SHPStream.Read( eX, SizeOf( Double ));	{ X }
		SHPStream.Read( eY, SizeOf( Double ));	{ Y }
    Result.PointsGroups[0].Add( DecimalToPointLL( CorrectX( eX ), CorrectY( eY )));
	end;
end;

{------------------------------------------------------------------------------
 TEarthSHPReader.LoadObject
------------------------------------------------------------------------------}
function TEarthSHPReader.LoadObject( iIndex : integer; bNewObject : Boolean ) : TEarthObject;
var
  eX,eY : Double;
  NewObject : TGeoDataObject;
  SHPItemPtr : TSHPItemPtr;
begin
  Result := nil;
  SHPItemPtr := TSHPItemPtr( DynArrayPtr( FDataItems, iIndex));
  with SHPItemPtr^ do
  begin
    SHPStream.Position := iOffset;

    if DBF <> nil then
      DBF.RecordID := iDBFRecordID;

		ReadBigEndianInt;	{ record number }
    ReadBigEndianInt; { content length }

		case ReadLittleEndianInt of	{ ShapeType }
		1 :	{ Point }
			begin
				SHPStream.Read( eX, SizeOf( double ));	{ X }
				SHPStream.Read( eY, SizeOf( double ));	{ Y }
				NewObject := TGeoDataObject.Create( Self );
				NewObject.Centroid := DecimaltoPointLL( CorrectX( eX ), CorrectY( eY ));
			end;
		8 : { MultiPoint }
			NewObject := ReadMultiPointObject;
		3 :	{ Arc }
			NewObject := ReadPolyObject( False );
		5 :	{ Polygon }
			NewObject := ReadPolyObject( True );
		else
			Exit;
		end;

    if DBF <> nil then
    begin
      if TitleColumn = -1 then
        NewObject.Title := rsNewObject
      else
        NewObject.Title := DBF.AsString[TitleColumn];

      NewObject.ID := iDBFRecordID;
    end;   

	end;

  Result := NewObject;
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.SaveMetaData
------------------------------------------------------------------------------}
procedure TEarthSHPReader.SaveMetaData;
var
  idx : integer;
  Writer : TEarthStreamWriter;
  sMetaDataName : TFilename;
begin
  sMetaDataName := MetaDataFilename( Filename );
  if sMetaDataName = '' then
    Exit;

  if not FileExists( sMetaDataName ) then
  begin
    Writer := TEarthStreamWriter.Create( TMemoryStream.Create );
    try
      Writer.WriteWord( TG_SHP_READERVERSION );

      WriteMetaData( Writer );

      Writer.WriteDouble( LongXDivisor );
      Writer.WriteDouble( LatYDivisor );
      Writer.WriteDouble( LongXShift );
      Writer.WriteDouble( LatYShift );
      Writer.WriteInteger( TitleColumn );

      Writer.WriteInteger( FDataItems.Count ); // save the count of items

      // save the class data.
      for idx := 0 to FDataItems.Count - 1 do
      begin
        Writer.WriteInteger( TSHPItemPtr( DynArrayPtr( FDataItems, idx))^.iOffset );
        Writer.WriteInteger( TSHPItemPtr( DynArrayPtr( FDataItems, idx))^.iDBFRecordID );
      end;

//      Writer.WriteShortString( ExtractFilename( Filename ));
      TMemoryStream( Writer.DataStream ).SaveToFile( sMetaDataName );
    finally
      Writer.Free;
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.LoadMetaData
------------------------------------------------------------------------------}
procedure TEarthSHPReader.LoadMetaData;
var
  idx, iCount : Integer;
  Reader : TEarthStreamReader;
  sMetaDataName : TFilename;
begin
  sMetaDataName := MetaDataFilename( Filename );
  if not FileExists( sMetaDataName ) then
    Exit;

  // Check the metadata file is older than the data file
  if FileAge( sMetaDataName ) < FileAge( Filename ) then
    Exit;

  Reader := TEarthStreamReader.Create( TMemoryStream.Create );
  try
    TMemoryStream( Reader.DataStream ).LoadFromFile( sMetaDataName );

    if Reader.ReadWord <> TG_SHP_READERVERSION then
    begin
      DeleteFile( sMetaDataName );
      Exit;
    end;

    ReadMetaData( Reader );

    LongXDivisor := Reader.ReadDouble;
    LatYDivisor := Reader.ReadDouble;
    LongXShift := Reader.ReadDouble;
    LatYShift := Reader.ReadDouble;

    idx := Reader.ReadInteger;  // Get the titlecolumn from the metafile
    if TitleColumn = -1 then // only assign it if not already specified
      FTitleColumn := idx;

    DynArrayFree( FDataItems );  // clear the data item list

    // get the number of items for this class
    iCount := Reader.ReadInteger;

    // set the length of the Reader Item array
    FDataItems := DynArrayCreate( SizeOf( TSHPItem ), iCount );

    // load in the Reader data
    for idx := 0 to iCount - 1 do
    begin
      TSHPItemPtr( DynArrayPtr( FDataItems, idx))^.iOffset := Reader.ReadInteger;
      TSHPItemPtr( DynArrayPtr( FDataItems, idx))^.iDBFRecordID := Reader.ReadInteger;

    end;
  finally
    Reader.Free;
  end;
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.InternalOpen
------------------------------------------------------------------------------}
function TEarthSHPReader.InternalOpen : Boolean;
 var Dlg:TGISDbFieldSelector;
     DBFFile:string;
begin
  // Create the data stream
  Result := FileExists( Filename );
  if Result then
  begin
    Name := ExtractFilename( Filename );

    LoadMetaData;

    SHPStream := TGFileMapStream.Create( Filename );

    DBFFile:=ChangeFileExt( Filename, '.DBF' );

    if FileExists(DBFFile) then
     begin
      DBF := TEarthDBFReader.Create(DBFFile);

      Dlg:=TGISDbFieldSelector.Create(nil);
      Dlg.FileName:='SHP Layer file: '+Name+'   DataBase file: '+ExtractFilename(DBFFile);

      if Dlg.Execute(DBF) then
        TitleColumn:=dlg.FieldNum;
      Dlg.Free;
     end;

    if FDataItems = nil then
    begin
      MapDataFile( SHPStream );

      if TEarthBase(Earth).SaveMetaDataLocation <> smdDiscard then
        SaveMetaData;
    end;

    Result := inherited InternalOpen;
  end;
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.InternalClose
------------------------------------------------------------------------------}
function TEarthSHPReader.InternalClose;
begin
  Count := 0;
  DynArrayFree( FDataItems ); // clear the data item list

  DBF.Free;

  SHPStream.Free;
  SHPStream := nil;

  Result := inherited InternalClose;
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.SaveEnvironment
------------------------------------------------------------------------------}
{**
  @Result Element containing the SHPReader environment.
}
function TEarthSHPReader.SaveEnvironment : TGXML_Element;
begin
  Result := inherited SaveEnvironment;

  Result.AddFloatAttribute( 'LongXDivisor', LongXDivisor);
  Result.AddFloatAttribute( 'LatYDivisor', LatYDivisor);
  Result.AddFloatAttribute( 'LongXShift', LongXShift);
  Result.AddFloatAttribute( 'LatYShift', LatYShift);
  Result.AddIntAttribute( 'TitleColumn', TitleColumn );
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.LoadEnvironment
------------------------------------------------------------------------------}
{**
  @Param Element containing the SHPReader environment to load.
}
procedure TEarthSHPReader.LoadEnvironment( Element : TGXML_Element );
begin
  if Element <> nil then
    with Element do
    begin
      LongXDivisor := FloatAttributeByName( 'LongXDivisor', LongXDivisor);
      LatYDivisor := FloatAttributeByName( 'LatYDivisor', LatYDivisor);
      LongXShift := FloatAttributeByName( 'LongXShift', LongXShift);
      LatYShift := FloatAttributeByName( 'LatYShift', LatYShift);
      TitleColumn := IntAttributeByName( 'TitleColumn', TitleColumn );
    end;

  inherited LoadEnvironment( Element );
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.MapDataHeader
------------------------------------------------------------------------------}
{**
  @Param aStream The data stream to read from.
  @Result True if the Header was correctly read
}
function TEarthSHPReader.MapDataHeader(aStream: TStream): Boolean;
var
	idx : integer;
  eTmp : Double;
begin
  aStream.Position := 0;
	if ReadBigEndianInt <> 9994 then
    raise EEarthException.CreateFmt( 'Invalid SHP file ', [Filename] );

  FDataItems := DynArrayCreate( SizeOf( TSHPItem ), 0 );

  for idx := 0 to 4 do
    ReadLittleEndianInt;

  ReadBigEndianInt; // Shape file size

  if ReadLittleEndianInt <> 1000 then	{ Version }
    raise EEarthException.CreateFmt( 'Invalid SHP file version ', [Filename] );

  ReadLittleEndianInt;	{ Shape File Type }

  for idx := 0 to 3 do  // read the bounding box
    aStream.Read( eTmp, SizeOf( Double ));

  for idx := 0 to 7 do
    ReadLittleEndianInt;
  Result := True;
end;

{------------------------------------------------------------------------------
  TEarthSHPReader.MapDataObject
------------------------------------------------------------------------------}
{**
  @Param aStream The data stream to read from.
  @Param iIndex The Index of the object to Read.
  @Result True if the Object was correctly read.
}
function TEarthSHPReader.MapDataObject(aStream: TStream;
  iIndex: integer): Boolean;
var
  iPos : integer;
begin
  iPos := aStream.Position;
    Result := iPos < aStream.Size;

  if Result then
  begin
    if iIndex >= FDataItems.Count then
      DynArraySetLength(FDataItems, iIndex + 128);

    with TSHPItemPtr( DynArrayPtr( FDataItems, iIndex))^ do
    begin
      iOffset := iPos;
      if DBF <> nil then
        iDBFRecordID := iIndex + 1;
    end;

//    ReadBigEndianInt;	{ record number }
//    aStream.Position := aStream.Position + ( 2 + ReadBigEndianInt ) * 2;	{ record length }
//    if DBF <> nil then
//      DBF.Next;
  end;
end;

initialization
  RegisterClasses( [TEarthSHPReader] );
end.
