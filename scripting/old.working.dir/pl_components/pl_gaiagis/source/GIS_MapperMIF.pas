
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_MapperMIF;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  Forms, Graphics, Classes, SysUtils, GIS_EarthBase, GIS_SysUtils,
  GIS_EarthObjects, GIS_Presenters, GIS_TextReader, GIS_Classes, GIS_XML,
  GIS_EarthStreams, GR32, FileUtil, lazfileutils;

const
  TG_MIF_READERVERSION = $0103;

  DefaultPenColor = clBlack32;
  DefaultPenStyle = psSolid;
  DefaultPenWidth = 1;
  DefaultPenUnit = euPixel;
  DefaultBrushColor = clBlack32;
  DefaultBrushStyle = gbsSolid;
  DefaultFontName = 'Arial';
  DefaultFontSize = 9;
  DefaultFontFillColor = clBlack32;
  DefaultFontOutlineColor = clWhite32;
  DefaultFontUnit = euPixel;
  DefaultFontAngle = 0;
  DefaultFontStyle = [];

type
  TMIFMIDItem = record
    iMIDOffset : integer;
    iMIFOffset : integer;
  end;
  TMIFItemPtr = ^TMIFMIDItem;

  {---------------------------- TEarthMIFReader -------------------------------}
  TEarthMIFReader = class(TEarthFileReader)
  private
    FMIFReader : TEarthTextReader;
    FMIDReader : TEarthTextReader;
    FDataItems : DynArray;
    FTitleColumn : integer;
    FMIFCoordSys : string;
    FMIDDelimiter : string;
    FMIDColumnNames : TStringList;
  protected
    iMIFVersion : integer;
    CurrentMIFLine : string;
    CurrentMIDLine : string;

    aPen : TEarthPen;
    aBrush : TEarthBrush;
    aFont : TEarthFont;
    aTitleFont : TEarthFont;

    function InternalOpen : Boolean; override;
    function InternalClose : Boolean; override;

    function MIF2DelphiBrush(iStyle : Integer) : TEarthBrushStyle;
    function MIF2DelphiPen(iStyle : Integer) : TPenStyle;
    function MIF2DelphiColor(iColor : Integer) : Integer;
    function ReadLLPointFromLine : TPointLL;
    function ReadLLPoint(iLongPos, iLatPos : integer) : TPointLL;
    function ReadPenObject : Boolean;
    function ReadBrushObject : Boolean;
    function ReadFontObject : Boolean;
    function ReadLineObject(bNewObject : Boolean) : TGeoDataObject;
    function ReadPLineObject(bNewObject : Boolean) : TGeoDataObject;
    function ReadPointObject(bNewObject : Boolean) : TGeoDataObject;
    function ReadRegionObject(bNewObject : Boolean) : TGeoDataObject;
    function ReadTextObject(bNewObject : Boolean) : TGeoDataObject;

    function ReadMIFLine : Boolean;
    function ReadMIDLine : Boolean;

    function MapDataHeader(aStream : TStream) : Boolean; override;
    function MapDataObject(aStream : TStream; iIndex : integer) : Boolean; override;

    function GetMIFWord(iIndex : integer) : string;
    function GetMIDWord(iIndex : integer) : string;
    function ReplaceStr(const sOrigin, sToReplace, sReplacer : string) : string;
    function StrSplitToList(const sToSplit, sSeparator : string;
                            var tsStrList : TStrings) : Integer;
  public
    procedure LoadEnvironment(Element : TGXML_Element); override;
    function  SaveEnvironment : TGXML_Element; override;
    procedure SaveMetaData; override;
    procedure LoadMetaData; override;

    function LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject; override;
  published
    property MIFCoordSys : string read FMIFCoordSys write FMIFCoordSys;
    property MIDDelimiter : string read FMIDDelimiter write FMIDDelimiter;
    property MIDColumnNames : TStringList read FMIDColumnNames write FMIDColumnNames;
    property TitleColumn : integer read FTitleColumn write FTitleColumn;
  end;

implementation

uses GIS_Resource;

{------------------------------------------------------------------------------
  TEarthMIFReader.GetMIFWord
------------------------------------------------------------------------------}
function TEarthMIFReader.GetMIFWord(iIndex : integer) : string;
var
  iLen : integer;
  P, S : PChar;
  bSpace : Boolean;
begin
  iLen := Length(CurrentMIFLine);

  if iLen = 0 then
  begin
    Result := '';
    Exit;
  end;
  P := Pointer(@CurrentMIFLine[1]);

  bSpace := True;
  repeat
    while (iLen > 0) and (bSpace = (P^ in [#9, ' '])) do
    begin
      Inc(P);
      Dec(iLen);
    end;
    bSpace := not bSpace;
    if not bSpace then
      Dec(iIndex);
  until (iLen = 0) or (iIndex < 0);

  S := P;
  while (iLen > 0) and not (S^ in [#9, ' ']) do
  begin
    Inc(S);
    Dec(iLen);
  end;

  SetString(Result, P, S - P);
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.GetMIDWord
------------------------------------------------------------------------------}
function TEarthMIFReader.GetMIDWord(iIndex : integer) : string;
begin
  //ToDo: extract the right bit of the line using MIDDelimiter
  Result := CurrentMIDLine;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.StrSplitToList
------------------------------------------------------------------------------}
function TEarthMIFReader.StrSplitToList(const sToSplit, sSeparator : string;
  var tsStrList : TStrings) : Integer;
var
  iCurPos, iNoStrings : Integer;
  sTmpRet, sTmpStr : string;
begin
  if tsStrList = nil then
    tsStrList := TStringList.Create;

  sTmpRet := Copy(sToSplit, 1, Length(sToSplit));
  iCurPos := Pos(sSeparator, sToSplit);
  tsStrList.Clear;
  iNoStrings := 0;
  if iCurPos > 0 then
  begin
    while iCurPos > 0 do
    begin
      sTmpStr := Copy(sTmpRet, 0, iCurPos - 1);
      tsStrList.Add(sTmpStr);
      Inc(iNoStrings, 1);
      sTmpRet := Copy(sTmpRet, iCurPos + Length(sSeparator), Length(sTmpRet));
      iCurPos := Pos(sSeparator, sTmpRet);
    end;
    if Length(sTmpRet) > 0 then
    begin
      tsStrList.Add(sTmpRet);
      Inc(iNoStrings, 1);
    end;
  end
  else
  begin
    tsStrList.Add(sTmpRet);
    Inc(iNoStrings, 1);
  end;
  Result := iNoStrings;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReplaceStr
------------------------------------------------------------------------------}
function TEarthMIFReader.ReplaceStr(const sOrigin, sToReplace, sReplacer : string) : string;
{ ex: sNewString := ReplaceStr('Hej hopp', ' ', '_');
  results in sNewString := 'Hej_hopp');}
var
  sToReturn : string;
  iCurPos : Integer;
  sTmpRet, sTmpStr : string;
begin
  iCurPos := Pos(sToReplace, sOrigin);
  if iCurPos > 0 then
  begin
    sTmpRet := Copy(sOrigin, 1, Length(sOrigin));
    sToReturn := '';
    while iCurPos > 0 do
    begin
      sTmpStr := Copy(sTmpRet, 1, iCurPos - 1);
      sToReturn := sToReturn + sTmpStr + sReplacer;
      sTmpRet := Copy(sTmpRet, iCurPos + Length(sToReplace), Length(sTmpRet));
      iCurPos := Pos(sToReplace, sTmpRet);
    end;
    sToReturn := sToReturn + sTmpRet;
    Result := sToReturn;
  end
  else
    Result := sOrigin;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.MIF2DelphiBrush
------------------------------------------------------------------------------}
function TEarthMIFReader.MIF2DelphiBrush(iStyle : Integer) : TEarthBrushStyle;
const
  gBrushStyle : array[1..68] of TEarthBrushStyle = (
    gbsClear, gbsSolid, gbsHorizontal, gbsVertical, gbsBDiagonal, gbsFDiagonal, gbsCross, gbsDiagCross,
    gbsPattern1, gbsPattern32, gbsPattern26, gbsPattern29, gbsPattern33, gbsPattern1, gbsPattern35, gbsHorizontal,
    gbsPattern3, gbsPattern4, gbsPattern5, gbsPattern6, gbsVertical, gbsPattern8, gbsPattern9, gbsPattern10,
    gbsPattern11, gbsFDiagonal, gbsPattern13, gbsPattern14, gbsPattern15, gbsPattern16, gbsBDiagonal, gbsPattern18,
    gbsPattern19, gbsPattern20, gbsPattern21, gbsCross, gbsPattern23, gbsPattern24, gbsPattern25, gbsPattern26,
    gbsPattern27, gbsPattern28, gbsPattern29, gbsPattern30, gbsPattern31, gbsPattern32, gbsPattern33, gbsPattern34,
    gbsPattern35, gbsPattern36, gbsPattern37, gbsPattern38, gbsPattern39, gbsPattern40, gbsPattern41, gbsPattern42,
    gbsPattern43, gbsPattern44, gbsPattern45, gbsPattern46, gbsPattern47, gbsPattern48, gbsPattern49, gbsPattern50,
    gbsPattern46, gbsPattern45, gbsPattern44, gbsPattern50
    );
begin
  if iStyle > High(gBrushStyle) then
    Result := gbsSolid
  else
    Result := gBrushStyle[iStyle];
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.MIF2DelphiPen
------------------------------------------------------------------------------}
function TEarthMIFReader.MIF2DelphiPen(iStyle : Integer) : TPenStyle;
begin
  case iStyle of
    1 : Result := psClear;
    3..9 : Result := psDash;
    10..13 : Result := psDot;
    14..16 : Result := psDashDot;
    20..25 : Result := psDashDotDot;
    71..77 : Result := psInsideFrame;
  else
    Result := psSolid;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.MIF2DelphiColor
------------------------------------------------------------------------------}
function TEarthMIFReader.MIF2DelphiColor(iColor : Integer) : Integer;
var
  sTmp : string;
begin
  sTmp := IntToHex(iColor, 6);
  Result := StrToInt('$' + Copy(sTmp, 5, 2) + Copy(sTmp, 3, 2) + Copy(sTmp, 1, 2));
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadMIFLine
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadMIFLine : Boolean;
begin
  repeat
    FMIFReader.ReadLn(CurrentMIFLine);
    Result := Length(CurrentMIFLine) > 0;
  until FMIFReader.EOT or Result;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadMIDLine
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadMIDLine : Boolean;
begin
  Result := (FMIDReader <> nil) and not FMIDReader.EOT;
  if Result then
    FMIDReader.ReadLn(CurrentMIDLine);
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadLLPoint
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadLLPoint(iLongPos, iLatPos : integer) : TPointLL;
var
  Long, Lat : Double;
begin
  Long := StrToExtended(GetMIFWord(iLongPos));
  Lat := StrToExtended(GetMIFWord(iLatPos));

  {ToDo:  if Assigned( OnProjectCoords ) then
      OnProjectCoords( Self, Long, Lat );}

  Result := DecimalToPointLL(Long, Lat);
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadLLPointFromLine
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadLLPointFromLine : TPointLL;
begin
  ReadMIFLine;
  Result := ReadLLPoint(0, 1);
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadPointObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadPointObject(bNewObject : Boolean) : TGeoDataObject;
var
  ptLL : TPointLL;
  aPresenter : TPointPresenter;
begin
  ptLL := ReadLLPoint(1, 2);

  Result := TGeoDataObject.Create(Self);
  Result.Centroid := ptLL;
  Result.Title := GetMIDWord(TitleColumn);

  if bNewObject then
  begin
    ReadMIFLine;
    Result.PresenterID := TPointPresenter.FindMatch(Presenters,
      aPen, aBrush, aFont, nil, ppCircle, euPixel, 6, '');

    if Result.PresenterID = 0 then
    begin
      aPresenter := TPointPresenter.Create(Presenters, 0);
      aPresenter.PointPen.Assign(aPen);
      aPresenter.PointBrush.Assign(aBrush);
      aPresenter.PointFont.Assign(aFont);
      aPresenter.TitleFont.Assign(aTitleFont);
      Result.PresenterID := aPresenter.PresenterID;
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadPenObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadPenObject : Boolean;
var
  sTmp : string;
  slPen : TStrings;
begin
  Result := CompareText(GetMIFWord(0), 'Pen') = 0;

  if not Result then
    Exit;

  sTmp := GetMIFWord(1);
  slPen := TStringList.Create;
  StrSplitToList(Copy(sTmp, 2, Length(sTmp) - 2), ',', slPen);

  try
    aPen.Define(
      MIF2DelphiColor(StrToInt(slPen[2])),
      MIF2DelphiPen(StrToInt(slPen[1])),
      StrToIntDef(slPen[0], 1), euPixel);
  finally
    slPen.Free;
  end;

  ReadMIFLine;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadBrushObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadBrushObject : Boolean;
var
  sTmp : string;
  slBrush : TStrings;
begin
  Result := CompareText(GetMIFWord(0), 'Brush') = 0;

  if not Result then
    Exit;

  sTmp := GetMIFWord(1);
  slBrush := TStringList.Create;
  StrSplitToList(Copy(sTmp, 2, Length(sTmp) - 2), ',', slBrush);
  with aBrush do
  try
    BrushStyle := MIF2DelphiBrush(StrToInt(slBrush[0]));
    BrushColor := MIF2DelphiColor(StrToInt(slBrush[1]));
    if (slBrush.Count > 2 ) then
      BrushBkColor := MIF2DelphiColor(StrToInt(slBrush[2]));
  finally
    slBrush.Free;
  end;

  ReadMIFLine;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadFontObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadFontObject : Boolean;
var
  sTmp : string;
  slFont : TStrings;
begin
  Result := CompareText(GetMIFWord(0), 'Font') = 0;

  if not Result then
    Exit;

  slFont := TStringList.Create;
  sTmp := Trim(CurrentMIFLine);
  StrSplitToList(Copy(sTmp, 7, Length(sTmp) - 7), ',', slFont);

  try
    with Earth.EarthCanvas do
    begin
      aTitleFont.FontName := ReplaceStr(slFont[0], '"', '');
      aTitleFont.FontStyle := [];
      aTitleFont.FontSize := StrToInt(slFont[2]);
      aTitleFont.FontFillColor := MIF2DelphiColor(StrToInt(slFont[3]));
      aTitleFont.FontAngle := 0;
    end;

    while ReadMIFLine do
    begin
      if CompareText(slFont[0], 'Angle') = 0 then
      begin
        aTitleFont.FontAngle := Round(StrToExtended(slFont[1])) * 10;
        continue;
      end;
      if CompareText(slFont[0], 'Spacing') = 0 then
        continue;
      if CompareText(slFont[0], 'justify') = 0 then
        continue;
      if CompareText(slFont[0], 'Label') = 0 then
        continue;
      break;
    end;
  finally
    slFont.free;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadRegionObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadRegionObject(bNewObject : Boolean) : TGeoDataObject;
var
  idx, iParts, iPoints : integer;
  aPresenter : TPolygonPresenter;
begin
  Result := TGeoDataObject.Create(Self);
  Result.Closed := True;

  Result.PointsGroups.Count := StrToInt(GetMIFWord(1)); { get number of parts to region }
  for iParts := 0 to Result.PointsGroups.Count - 1 do
  begin
    ReadMIFLine;
    iPoints := StrToInt(GetMIFWord(0)); { get number of points in this region }
    for idx := 1 to iPoints do
      Result.PointsGroups[iParts].Add(ReadLLPointFromLine);
  end;

  if ReadMIDLine then
    Result.Title := GetMIDWord(TitleColumn)
  else
    Result.Title := rsNewPolygon;

  if bNewObject then
  begin
    { read attributes }
    ReadMIFLine;
    while True do
      if not ReadPenObject then
        if not ReadBrushObject then
          if CompareText(GetMIFWord(0), 'Center') <> 0 then
            break
          else
            if not ReadMIFLine then
              break;

    Result.PresenterID := TPolygonPresenter.FindMatch(Presenters, aPen, aBrush, nil);

    if Result.PresenterID = 0 then
    begin
      aPresenter := TPolygonPresenter.Create(Presenters, 0);
      aPresenter.PolyPen.Assign(aPen);
      aPresenter.PolyBrush.Assign(aBrush);
      Result.PresenterID :=aPresenter.PresenterID;
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadPLineObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadPLineObject(bNewObject : Boolean) : TGeoDataObject;
var
  idx, iParts, iPoints : integer;
  sTmp : string;
  aPresenter : TPolylinePresenter;
  MultipleParts : Boolean;
begin
  Result := TGeoDataObject.Create(Self);

  MultipleParts := False;
  iPoints := 0;
  Result.PointsGroups.Count := 1;
  case iMIFVersion of
    300 :
      begin
        sTmp := GetMIFWord(1);
        if sTmp <> '' then
        begin
          if CompareText(sTmp, 'Multiple') = 0 then
          begin
            Result.PointsGroups.Count := StrToInt(GetMIFWord(2)); { get number of parts to region }
            //            ReadMIFLine;
            //            iPoints := StrToInt(GetMIFWord(0)); { get number of points in this region }
            MultipleParts := True; { region has multiple parts }
          end
          else
            iPoints := StrToInt(GetMIFWord(1)); { get number of points in this region }
        end
        else
        begin
          ReadMIFLine;
          iPoints := StrToInt(GetMIFWord(0)); { get number of points in this region }
        end;
      end;
    1 :
      iPoints := StrToInt(GetMIFWord(1)); { get number of points in this region }
  else
    Result.PointsGroups.Count := StrToInt(GetMIFWord(1)); { get number of parts to region }
    MultipleParts := True; { region has multiple parts }
    //    ReadMIFLine;
    //    iPoints := StrToInt(GetMIFWord(0)); { get number of points in this region }
  end;

  for iParts := 0 to Result.PointsGroups.Count - 1 do
  begin
    if MultipleParts then
    begin
      ReadMIFLine;
      iPoints := StrToInt(GetMIFWord(0)); { get number of points in this region }
    end;

    for idx := 1 to iPoints do
      Result.PointsGroups[iParts].Add(ReadLLPointFromLine);
  end;

  if ReadMIDLine then
    Result.Title := GetMIDWord(TitleColumn)
  else
    Result.Title := rsNewPolyline;

  if bNewObject then
  begin
    { read attributes }
    ReadMIFLine;
    while True do
      if not ReadPenObject then
        if CompareText(GetMIFWord(0), 'Smooth') <> 0 then
          break
        else
          if not ReadMIFLine then
            break;

    aBrush.Define(DefaultBrushColor, DefaultBrushStyle);

    Result.PresenterID := TPolylinePresenter.FindMatch(Presenters, aPen, aBrush, nil);

    if Result.PresenterID = 0 then
    begin
      aPresenter := TPolylinePresenter.Create(Presenters, 0);
      aPresenter.PolyPen.Assign(aPen);
      aPresenter.PolyBrush.Assign(aBrush);
      Result.PresenterID := aPresenter.PresenterID
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadLineObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadLineObject(bNewObject : Boolean) : TGeoDataObject;
var
  aPresenter : TPolylinePresenter;
begin
  Result := TGeoDataObject.Create(Self);

  Result.PointsGroups[0].Add(ReadLLPoint(1, 2));
  Result.PointsGroups[0].Add(ReadLLPoint(3, 4));

  if ReadMIDLine then
    Result.Title := GetMIDWord(TitleColumn)
  else
    Result.Title := rsNewPolyline;

  if bNewObject then
  begin
    ReadMIFLine;
    ReadPenObject;

    Result.PresenterID := TPolylinePresenter.FindMatch(Presenters, aPen, aBrush, nil);

    if Result.PresenterID = 0 then
    begin
      aPresenter := TPolylinePresenter.Create(Presenters, 0);
      aPresenter.PolyPen.Assign(aPen);
      aPresenter.PolyBrush.Assign(aBrush);
      Result.PresenterID := aPresenter.PresenterID;
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.ReadTextObject
------------------------------------------------------------------------------}
function TEarthMIFReader.ReadTextObject(bNewObject : Boolean) : TGeoDataObject;
var
  sText : string;
  ptLL1, ptLL2 : TPointLL;
  aPresenter : TPointPresenter;
begin
  ReadMIFLine;
  sText := CurrentMIFLine;
  sText := ReplaceStr(ReplaceStr(Trim(sText), '"', ''), '\n', ' ');

  ReadMIFLine;
  ptLL1 := ReadLLPoint(0, 1);
  ptLL2 := ReadLLPoint(0, 1);

  Result := TGeoDataObject.Create(Self);

  with Result do
  begin
    Centroid := ptLL1;
    Title := sText;

    if bNewObject then
    begin
      ReadMIFLine;
      ReadFontObject;

      with Earth.EarthCanvas do
      begin
        aTitleFont.FontSizeUnits := euMeter;
        with ptLL2 do
          aTitleFont.FontSize := -Abs(ptLL1.iLatY - ptLL2.iLatY);

        aPen.Define(DefaultPenColor, DefaultPenStyle, DefaultPenWidth, DefaultPenUnit);
        aBrush.Define(DefaultBrushColor, DefaultBrushStyle);
        aFont.Define(DefaultFontName, DefaultFontFillColor,DefaultFontOutlineColor,
                      DefaultFontSize, DefaultFontUnit, DefaultFontAngle, DefaultFontStyle);

        Result.PresenterID := TPointPresenter.FindMatch(Presenters,
          aPen, aBrush, aFont, aTitleFont, ppCircle, euPixel, 6, '');

        if Result.PresenterID = 0 then
        begin
          aPresenter := TPointPresenter.Create(Presenters, 0);
          aPresenter.PointPen.Assign(aPen);
          aPresenter.PointBrush.Assign(aBrush);
          aPresenter.PointFont.Assign(aFont);
          aPresenter.TitleFont.Assign(aTitleFont);

          Result.PresenterID := aPresenter.PresenterID
        end;
      end;
    end;
  end;
end;

{------------------------------------------------------------------------------
 TEarthMIFReader.LoadObject
------------------------------------------------------------------------------}
function TEarthMIFReader.LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject;
var
  sTmp : string;
begin
  Result := nil;

  with TMIFItemPtr(DynArrayPtr(FDataItems, iIndex))^ do
  begin
    FMIFReader.Position := iMIFOffset;

    if FMIDReader <> nil then
      FMIDReader.Position := iMIDOffset;

    ReadMIFLine;

    sTmp := lowercase(GetMIFWord(0)); // Check the current line

    if sTmp = 'point' then
      Result := ReadPointObject(bNewObject)
    else
      if sTmp = 'region' then
        Result := ReadRegionObject(bNewObject)
      else
        if sTmp = 'line' then
          Result := ReadLineObject(bNewObject)
        else
          if sTmp = 'pline' then
            Result := ReadPlineObject(bNewObject)
          else
            if sTmp = 'text' then
              Result := ReadTextObject(bNewObject);
  end;
end;

{------------------------------------------------------------------------------
 TEarthMIFReader.SaveMetaData
------------------------------------------------------------------------------}
procedure TEarthMIFReader.SaveMetaData;
var
  idx : integer;
  Writer : TEarthStreamWriter;
  sMetaDataName : TFilename;
begin
  sMetaDataName := MetaDataFilename(Filename);
  if sMetaDataName = '' then
    Exit;

  if not FileExists(sMetaDataName) then
  begin
    Writer := TEarthStreamWriter.Create(TMemoryStream.Create);
    try
      Writer.WriteWord(TG_MIF_READERVERSION);

      WriteMetaData(Writer);

      Writer.WriteInteger(iMIFVersion); // save Version of MIF file
      Writer.WriteInteger(TitleColumn);
      Writer.WriteShortString(MIDDelimiter);
      Writer.WriteShortString(FMIFCoordSys);

      // Save the MID Column names
      Writer.WriteInteger(MIDColumnNames.Count); // save the count of Column Names
      for idx := 0 to MIDColumnNames.Count - 1 do
        Writer.WriteShortString(MIDColumnNames[idx]);

      Writer.WriteInteger(FDataItems.Count); // save the count of items

      // save the class data.
      for idx := 0 to FDataItems.Count - 1 do
      begin
        Writer.WriteInteger(TMIFItemPtr(DynArrayPtr(FDataItems, idx))^.iMIFOffset);
        Writer.WriteInteger(TMIFItemPtr(DynArrayPtr(FDataItems, idx))^.iMIDOffset);
      end;

      //      Writer.WriteShortString( ExtractFilename( Filename ));
      TMemoryStream(Writer.DataStream).SaveToFile(sMetaDataName);
    finally
      Writer.Free;
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.LoadMetaData
------------------------------------------------------------------------------}
procedure TEarthMIFReader.LoadMetaData;
var
  idx, iCount : Integer;
  Reader : TEarthStreamReader;
  sMetaDataName : TFilename;
begin
  sMetaDataName := MetaDataFilename(Filename);
  if not FileExists(sMetaDataName) then
    Exit;

  // Check the metadata file is older than the data file
  if FileAge(sMetaDataName) < FileAge(Filename) then
    Exit;

  Reader := TEarthStreamReader.Create(TMemoryStream.Create);
  try
    TMemoryStream(Reader.DataStream).LoadFromFile(sMetaDataName);

    if Reader.ReadWord <> TG_MIF_READERVERSION then
    begin
      DeleteFile(sMetaDataName);
      Exit;
    end;

    ReadMetaData(Reader);

    DynArrayFree(FDataItems); // clear the data item list

    iMIFVersion := Reader.ReadInteger; // get Version of MIF file
    TitleColumn := Reader.ReadInteger;
    MIDDelimiter := Reader.ReadShortString;
    MIFCoordSys := Reader.ReadShortString;

    if MIDColumnNames = nil then
      MIDColumnNames := TStringList.Create;
    MIDColumnNames.Clear;

    // get the number of ColumnNames
    iCount := Reader.ReadInteger;
    for idx := 0 to iCount - 1 do
      MIDColumnNames.Add(Reader.ReadShortString);

    // get the number of items for this class
    iCount := Reader.ReadInteger;

    // set the length of the Reader Item array
    FDataItems := DynArrayCreate(SizeOf(TMIFMIDItem), iCount);

    // load in the Reader data
    for idx := 0 to iCount - 1 do
    begin
      TMIFItemPtr(DynArrayPtr(FDataItems, idx))^.iMIFOffset := Reader.ReadInteger;
      TMIFItemPtr(DynArrayPtr(FDataItems, idx))^.iMIDOffset := Reader.ReadInteger;
    end;
  finally
    Reader.Free;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.InternalOpen
------------------------------------------------------------------------------}
function TEarthMIFReader.InternalOpen : Boolean;
begin
  Result := FileExists(Filename);
  if Result then
  begin
    aPen := TEarthPen.Create(self);
    aBrush := TEarthBrush.Create(self);
    aFont := TEarthFont.Create(self);
    aTitleFont := TEarthFont.Create(self);

    if FMIDColumnNames = nil then
      FMIDColumnNames := TStringList.Create;

    Name := ExtractFilename(Filename);

    LoadMetaData;

    // Create the MIF data stream
    FMIFReader := TEarthTextReader.Create(Filename);

    if FileExists(ChangeFileExt(Filename, '.MID')) then
      FMIDReader := TEarthTextReader.Create(ChangeFileExt(Filename, '.MID'));

    if FDataItems = nil then
    begin
      MapDataFile(FMIFReader.DataStream);
      //      MapMIFFile;
      if TEarthBase(Earth).SaveMetaDataLocation <> smdDiscard then
        SaveMetaData;
    end;

    Result := inherited InternalOpen;
  end;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.InternalClose
------------------------------------------------------------------------------}
function TEarthMIFReader.InternalClose : Boolean;
begin
  SaveMetaData;

  Count := 0;
  DynArrayFree(FDataItems); // clear the data item list

  FMIDColumnNames.Free;

  FMIFReader.Free;
  FMIFReader := nil;
  FMIDReader.Free;
  FMIDReader := nil;

  aPen.Free;
  aBrush.Free;
  aFont.Free;
  aTitleFont.Free;

  Result := inherited InternalClose;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.SaveEnvironment
------------------------------------------------------------------------------}
{**
  @Result Element containing the MIFReader environment.
}
function TEarthMIFReader.SaveEnvironment : TGXML_Element;
begin
  Result := inherited SaveEnvironment;

  Result.AddAttribute('MIFCoordSys', MIFCoordSys);
  Result.AddAttribute('MIDDelimiter', MIDDelimiter);
  Result.AddIntAttribute('TitleColumn', TitleColumn);
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.LoadEnvironment
------------------------------------------------------------------------------}
{**
  @Param Element containing the MIFReader environment to load.
}
procedure TEarthMIFReader.LoadEnvironment(Element : TGXML_Element);
begin
  inherited LoadEnvironment(Element);

  if Element <> nil then
    with Element do
    begin
      MIFCoordSys := AttributeByName('MIFCoordSys', MIFCoordSys);
      MIDDelimiter := AttributeByName('MIDDelimiter', MIDDelimiter);
      TitleColumn := IntAttributeByName('TitleColumn', 0);
      Active := True;
    end;
end;

function TEarthMIFReader.MapDataHeader(aStream : TStream) : Boolean;
var
  sTmp : string;
  bColumns : Boolean;
begin
  MIDDelimiter := #9;

  FMIFReader.TextBufferSize := 16 * 1024;
//  FMIFReader.TextBufferSize := 64;

  bColumns := False;
  while ReadMIFLine do
  begin
    sTmp := GetMIFWord(0);

    if CompareText(sTmp, 'Data') = 0 then
      Break;

    if CompareText(sTmp, 'Version') = 0 then
      iMIFVersion := StrToInt(GetMIFWord(1));

    if CompareText(sTmp, 'Delimiter') = 0 then
      MIDDelimiter := GetMIFWord(1)[2];

    if CompareText(sTmp, 'CoordSys') = 0 then
      MIFCoordSys := CurrentMIFLine;

    if bColumns then
      MIDColumnNames.Add(sTmp + '=' + GetMIFWord(1));

    if CompareText(sTmp, 'Columns') = 0 then
      bColumns := True;
  end;

  // Check the Projection Model used.
  CurrentMIFLine := MIFCoordSys;
  if not ((CompareText(GetMIFWord(1), 'Earth') = 0)
    and (CompareText(GetMIFWord(2), 'Projection') = 0)
    and (CompareText(GetMIFWord(3), '1,') = 0)) then
    raise EEarthException.Create(rsMIFImportError);

  FDataItems := DynArrayCreate(Sizeof(TMIFMIDItem), 0);

  Result := True;
end;

{------------------------------------------------------------------------------
  TEarthMIFReader.MapDataObject
------------------------------------------------------------------------------}
{**
  @Param aStream The data stream to read from.
  @Param iIndex The Index of the object to Read.
  @Result True if the Object was correctly read.
}
function TEarthMIFReader.MapDataObject(aStream : TStream;
  iIndex : integer) : Boolean;
var
  sTmp : string;
begin
  Result := not FMIFReader.EOT;
  while Result do
  begin
    sTmp := lowercase(GetMIFWord(0)); // Check the current line

    if (sTmp = 'point') or (sTmp = 'region') or (sTmp = 'line')
      or (sTmp = 'pline') or (sTmp = 'text') then
    begin
      if iIndex >= FDataItems.Count then
        DynArraySetLength(FDataItems, iIndex + 64);

      with TMIFItemPtr(DynArrayPtr(FDataItems, iIndex))^ do
      begin
        iMIFOffset := FMIFReader.LineStartPosition;
        if FMidReader <> nil then
          iMIDOffset := FMIDReader.Position;
        ReadMIDLine;
      end;
      Break;
    end;
    ReadMIFLine;
    Result := not FMIFReader.EOT;
  end;
end;

initialization
  RegisterClasses([TEarthMIFReader]);
end.

