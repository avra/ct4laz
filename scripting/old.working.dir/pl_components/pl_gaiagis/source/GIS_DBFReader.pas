
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_DBFReader;



interface

uses
  SysUtils, Classes;

type
  EDBFException = class(Exception);

  TDBFField = record
    FName : array [0..10] of char;
    FType : char;
    FData : PChar;
    FLen  : Byte;
    FCount : Byte;
    FReserved : array [0..13] of char;
  end;
  TDBFFieldArray = array [0..255] of TDBFField;
  PTDBFFieldArray = ^TDBFFieldArray;

  TEarthDBFReader = class( TObject )
  private
    iRecordID : integer;
    iHeaderLen : SmallInt;
    iRecordLen : SmallInt;
    DBFstream : TStream;
    Data : PChar;
    function GetFieldName( iField : integer ) : string;
    function GetFieldType( iField : integer ) : Char;
    function GetFieldLength( iField : integer ) : integer;
    function GetString( iField : integer ) : string;
    procedure SetID( iRecord : integer );
  public
    iRecordCount : integer;
    iFieldCount : integer;
    Fields : PTDBFFieldArray;

    constructor Create( const DBFfilename : TFilename );
    destructor Destroy; override;
    function First : Boolean;
    function Next : Boolean;

    property FieldName[iField : integer] : string read GetFieldName;
    property FieldType[iField : integer] : char read GetFieldType;
    property FieldLength[iField : integer] : integer read GetFieldLength;

    property AsString[iField : integer] : string read GetString;
    property RecordID : integer read iRecordID write SetID;
  end;


implementation

{-------------------------------------------------------------------------}
constructor TEarthDBFReader.Create( const DBFfilename : TFilename );
var
  cTmp : Byte;
  idx, iTmp : integer;
begin
  DBFstream := TFileStream.Create( DBFfilename, fmOpenRead or fmShareDenyNone );
  DBFstream.Position := 0;
  DBFstream.Read( cTmp, 1 );
  if ( cTmp <> $03 ) and ( cTmp <> $83 ) then
  begin
    DBFStream.Free;
    Raise EDBFException.Create( 'Unsupported DBase III version in Stream' );
  end;

  for idx := 1 to 3 do
    DBFstream.Read( cTmp, 1 );

  DBFstream.Read( iRecordCount, SizeOf( integer ));
  DBFstream.Read( iHeaderLen, SizeOf( SmallInt ));
  DBFstream.Read( iRecordLen, SizeOf( SmallInt ));
  GetMem( Data, iRecordLen );

  for idx := 1 to 20 do
    DBFstream.Read( cTmp, 1 );

  GetMem( Fields, iHeaderLen - 32 );
  DBFstream.Read( Fields^, iHeaderLen - 32 );

  iFieldCount := 0;
  iTmp := 1;
  repeat
    Fields^[iFieldCount].FData := Data + iTmp;
    Inc( iTmp, Fields^[iFieldCount].FLen );
    Inc( iFieldCount );
  until Ord( Fields^[iFieldCount].FName[0] ) in [$0, $0D];

  First;
end;

{-------------------------------------------------------------------------}
destructor TEarthDBFReader.Destroy;
begin
  if Data <> nil then
    FreeMem( Data, iRecordLen );
  if Fields <> nil then
    FreeMem( Fields, iHeaderLen - 32 );
  DBFstream.Free;
  inherited Destroy;
end;

{-------------------------------------------------------------------------}
function TEarthDBFReader.First : Boolean;
begin
  iRecordID := 1;
  Result := DBFstream <> nil;
  if Result then
    DBFstream.Position := iHeaderLen;
end;

{-------------------------------------------------------------------------}
function TEarthDBFReader.Next : Boolean;
begin
  Result := False;
  if DBFstream <> nil then
  begin
    repeat
      Inc( iRecordID );
      if DBFstream.Read( Data^, iRecordLen ) <> iRecordLen then
        Exit;
    until Data[0] <> '*';
    Result := True;
  end;
end;

{-------------------------------------------------------------------------}
function TEarthDBFReader.GetFieldName( iField : integer ) : string;
begin
  Result := StrPas( Fields^[iField].FName );
end;

{-------------------------------------------------------------------------}
function TEarthDBFReader.GetFieldType( iField : integer ) : Char;
begin
  Result := Fields^[iField].FType;
end;

{-------------------------------------------------------------------------}
function TEarthDBFReader.GetFieldLength( iField : integer ) : integer;
begin
  Result := Fields^[iField].FLen;
end;

{-------------------------------------------------------------------------}
function TEarthDBFReader.GetString( iField : integer ) : string;
begin
  if iField < iFieldCount then
    Result := Trim( Copy( StrPas( Fields^[iField].FData ), 1, Fields^[iField].FLen ))
  else
    Result := '';
end;

{-------------------------------------------------------------------------}
procedure TEarthDBFReader.SetID( iRecord : integer );
begin
  if iRecord > 0 then
  begin
    Dec( iRecord );
    DBFstream.Position := iHeaderLen + iRecordLen * iRecord;
    DBFstream.Read( Data^, iRecordLen );
    iRecordID := iRecord;
  end;
end;

end.
