
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_Presenters;

interface

uses
  LCLIntf, LCLType, LMessages,
  Classes, Graphics, SysUtils, types, Math,
  GIS_EarthBase, GIS_Classes, GIS_EarthObjects, GIS_SysUtils,
  GIS_XML, GIS_Resource, GIS_EarthStreams,
  GR32, FileUtil, lazfileutils, LazUtf8;

type

{---------------------------- Point Presenter type --------------------------}
//
TPointPresenterType = (ppNone, ppFontSymbol, ppImage,
                       ppDot, ppCircle, ppSquare, ppDiamond,
                       ppTriangleUp, ppTriangleDown, ppTriangleLeft, ppTriangleRight);

TUserRenderObjectEvent = procedure(Sender:TCustomEarth; EarthObject:TEarthObject; State:TEarthObjectStateSet) of object;
TUserDrawShapeEvent = procedure(Sender:TObject) of object;

TCustomDrawPresenter = class(TEarthPresenter)
 protected
    eSin, eCos: extended;
    FDrawCenterXY: TPointDouble;
    FDrawOffectX: Double;
    FDrawOffectY: Double;
    FTmpString: string;
    bSelected, bShapeRendered: boolean;
    //...
    FShapeUnit: TEarthUnitTypes;
    FShapeSizeX: Double;
    FShapeSizeY: Double;
    FMaxPixelsSizeX: Double;
    FMaxPixelsSizeY: Double;
    FTransAngle:Double;
    FTransScaleX:Double;
    FTransScaleY:Double;
    FTransSkewX:Double;
    FTransSkewY:Double;
    procedure SetShapeUnit(Const Value: TEarthUnitTypes);
    procedure SetShapeSize(Const Value: Double);
    procedure SetShapeSizeX(Const Value: Double);
    procedure SetShapeSizeY(Const Value: Double);
    procedure SetMaxPixelsSize(Const Value: Double);
    procedure SetMaxPixelsSizeX(Const Value: Double);
    procedure SetMaxPixelsSizeY(Const Value: Double);
    procedure SetTransAngle(Const Value:Double);
    procedure SetTransScaleX(Const Value:Double);
    procedure SetTransScaleY(Const Value:Double);
    procedure SetTransSkewX(Const Value:Double);
    procedure SetTransSkewY(Const Value:Double);
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    destructor Destroy; override;
    procedure  Assign(Source: TPersistent); override;
    class function PrintableClassName : string; override;
    procedure  LoadEnvironment(Element: TGXML_Element); override;
    function   SaveEnvironment: TGXML_Element; override;
    procedure  WriteProperties(Writer: TEarthStreamWriter); override;
    procedure  ReadProperties(Reader: TEarthStreamReader); override;
    procedure  RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
    procedure  RenderShape(const ptLL: TPointLL); virtual;
    procedure  DrawShape; virtual;
    function   LLInObject( Earth : TCustomEarth; ptLL : TPointLL; Geod : TEarthObject; iTolerance : Double ) : Boolean; override;

    property  DrawCenterXY: TPointDouble read FDrawCenterXY;
    property  DrawOffectX: Double read FDrawOffectX;
    property  DrawOffectY: Double read FDrawOffectY;
    property  ShapeUnit: TEarthUnitTypes read FShapeUnit write SetShapeUnit;
    property  ShapeSize: Double read FShapeSizeX write SetShapeSize; //For One size objects
    property  ShapeSizeX: Double read FShapeSizeX write SetShapeSizeX;
    property  ShapeSizeY: Double read FShapeSizeY write SetShapeSizeY;
    property  MaxPixelsSize: Double read FMaxPixelsSizeX write SetMaxPixelsSize; //For One size objects
    property  MaxPixelsSizeX: Double read FMaxPixelsSizeX write SetMaxPixelsSizeX;
    property  MaxPixelsSizeY: Double read FMaxPixelsSizeY write SetMaxPixelsSizeY;
    property  TransAngle:Double read FTransAngle write SetTransAngle;
    Property  TransScaleX:Double read FTransScaleX write SetTransScaleX;
    Property  TransScaleY:Double read FTransScaleY write SetTransScaleY;
    Property  TransSkewX:Double  read FTransSkewX write SetTransSkewX;
    Property  TransSkewY:Double  read FTransSkewY write SetTransSkewY;
  end;

TCustomDrawBrushPresenter = class(TCustomDrawPresenter)
  protected
    FileVersion:integer; //this is then file version when read properties
    FShapePen: TEarthPen;
    FShapeBrush: TEarthBrush;
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    destructor Destroy; override;
    procedure  Assign(Source: TPersistent); override;
    procedure  LoadEnvironment(Element: TGXML_Element); override;
    function   SaveEnvironment: TGXML_Element; override;
    procedure  WriteProperties(Writer: TEarthStreamWriter); override;
    procedure  ReadProperties(Reader: TEarthStreamReader); override;
    procedure  RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;

    property   ShapePen: TEarthPen read FShapePen write FShapePen;
    property   ShapeBrush: TEarthBrush read FShapeBrush write FShapeBrush;
  end;


TUserDrawBrushPresenter = class(TCustomDrawBrushPresenter)
  protected
   fOnUserRenderObject:TUserRenderObjectEvent;
   fOnUserDrawShape:TUserDrawShapeEvent;
  public
    procedure RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
    procedure DrawShape; override;
  published
    Property OnUserRenderObject:TUserRenderObjectEvent read fOnUserRenderObject write fOnUserRenderObject;
    Property OnUserDrawShape:TUserDrawShapeEvent read fOnUserDrawShape write fOnUserDrawShape;
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
  end;

 //TUserRenderObjectEvent
  
  {---------------------------- TPointPresenter -------------------------------} 
  //
TPointPresenter = class(TCustomDrawPresenter)
  private
    FPointPen : TEarthPen;
    FPointBrush : TEarthBrush;
    FPointFont : TEarthFont;
    FiSymbolIndex : SmallInt;
    FPointType : TPointPresenterType;
    FImage : TPicture;
    FsImageName : string;
    procedure SetImageName(const sName : string);
    procedure SetPointType(Const Val : TPointPresenterType);
    procedure SetSymbolIndex(Const Val : SmallInt);
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    destructor Destroy; override;
    Function   GetFullImageName:String; virtual;
    class function FindMatch( Presenters : TEarthPresenterStore;
                             APen : TEarthPen; ABrush : TEarthBrush; AFont : TEarthFont; ATitleFont : TEarthFont;
                             APointType : TPointPresenterType; APointUnit : TEarthUnitTypes; APointSize : integer;
                             const AImageName : string) : integer;

    procedure Assign(Source : TPersistent); override;
    class function PrintableClassName : string; override;
    procedure RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
    procedure DrawShape; override;
    procedure LoadEnvironment( Element : TGXML_Element ); override;
    function  SaveEnvironment : TGXML_Element; override;
    procedure WriteProperties( Writer : TEarthStreamWriter ); override;
    procedure ReadProperties( Reader : TEarthStreamReader ); override;

  published
    property PointType : TPointPresenterType read FPointType write SetPointType;
    property SymbolIndex : SmallInt read FiSymbolIndex write SetSymbolIndex;
    property ImageName : string read FsImageName write SetImageName;
    property PointPen : TEarthPen read FPointPen write FPointPen;
    property PointBrush : TEarthBrush read FPointBrush write FPointBrush;
    property PointFont : TEarthFont read FPointFont write FPointFont;
    property ShapeUnit;
    property ShapeSize;
    property MaxPixelsSize;
  end;


{---------------------------- TBasePolyPresenter --------------------------------}
  //
TBasePolyPresenter = class(TEarthPresenter)
  private
    FPolyPen : TEarthPen;
    FPolyBrush : TEarthBrush;
  protected
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    destructor Destroy; override;
    class function FindMatch( Presenters : TEarthPresenterStore;APen : TEarthPen;
                              ABrush : TEarthBrush; ATitleFont : TEarthFont) : integer;
    procedure Assign(Source : TPersistent); override;
    class function PrintableClassName : string; override;
    procedure RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
    procedure LoadEnvironment( Element : TGXML_Element ); override;
    function  SaveEnvironment : TGXML_Element; override;
    procedure WriteProperties( Writer : TEarthStreamWriter ); override;
    procedure ReadProperties( Reader : TEarthStreamReader ); override;
    
    property PolyPen : TEarthPen read FPolyPen write FPolyPen;
    property PolyBrush : TEarthBrush read FPolyBrush write FPolyBrush;
  published

  end;


{---------------------------- TPolyPresenter --------------------------------}
  //
TPolyPresenter = class(TBasePolyPresenter)
  private
  protected
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    class function FindMatch( Presenters : TEarthPresenterStore;
                       APen : TEarthPen; ABrush : TEarthBrush; ATitleFont : TEarthFont) : integer;
    procedure Assign(Source : TPersistent); override;
    class function PrintableClassName : string; override;
  published
    property PolyPen;
    property PolyBrush;
  end;

{---------------------------- TPolygonPresenter -----------------------------}
  //
TPolygonPresenter = class(TBasePolyPresenter)
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    class function PrintableClassName : string; override;
    procedure RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
  published
    property PolyPen;
    property PolyBrush;
  end;


  {---------------------------- TPolylinePresenter ----------------------------} 
  //
TPolylinePresenter = class(TBasePolyPresenter)
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    class function PrintableClassName : string; override;
    procedure RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
  published
    property PolyPen;
  end;


implementation


//========================= TCustomDrawPresenter =========================================================
constructor TCustomDrawPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create(aPresenterStore, iID);
  Name := rsPointPresenterName; 
  FShapeUnit := euPixel;
  FMaxPixelsSizeX:=0;
  FMaxPixelsSizeY:=0;
  FShapeSizeX := 30;
  FShapeSizeY := 30;
  FTransAngle:=0.0;
  FTransScaleX:=1.0;
  FTransScaleY:=1.0;
  FTransSkewX:=0.0;
  FTransSkewY:=0.0;
end;

destructor TCustomDrawPresenter.Destroy;
begin
  inherited Destroy;
end;

procedure TCustomDrawPresenter.SetMaxPixelsSize(Const Value: Double);
begin
  if Value = FMaxPixelsSizeX then exit;
  FMaxPixelsSizeX := Value;
  FMaxPixelsSizeY := Value;
  RedrawObject;
end;

procedure TCustomDrawPresenter.SetMaxPixelsSizeX(Const Value: Double);
begin
  if Value = FMaxPixelsSizeX then exit;
  FMaxPixelsSizeX := Value;
  RedrawObject;
end;

procedure TCustomDrawPresenter.SetMaxPixelsSizeY(Const Value: Double);
begin
  if Value = FMaxPixelsSizeY then exit;
  FMaxPixelsSizeY := Value;
  RedrawObject;
end;

procedure TCustomDrawPresenter.SetShapeSize(Const Value: Double);
begin
  if Value = FShapeSizeX then exit;
  FShapeSizeX := Value;
  FShapeSizeY := Value;
  RedrawObject;
end;

procedure TCustomDrawPresenter.SetTransAngle(Const Value: Double);
 begin
  if Value = FTransAngle then exit;
  FTransAngle:=Value;
  RedrawObject;
 end;

procedure TCustomDrawPresenter.SetTransScaleX(Const Value:Double);
 begin
  if Value = FTransScaleX then exit;
  FTransScaleX:=Value;
  RedrawObject;
 end;

procedure TCustomDrawPresenter.SetTransScaleY(Const Value:Double);
 begin
  if Value = FTransScaleY then exit;
  FTransScaleY:=Value;
  RedrawObject;
 end;

procedure TCustomDrawPresenter.SetTransSkewX(Const Value:Double);
 begin
  if Value = FTransSkewX then exit;
  FTransSkewX:=Value;
  RedrawObject;
 end;

procedure TCustomDrawPresenter.SetTransSkewY(Const Value:Double);
 begin
  if Value = FTransSkewY then exit;
  FTransSkewY:=Value;
  RedrawObject;
 end;

procedure TCustomDrawPresenter.SetShapeSizeX(Const Value: Double);
begin
  if Value = FShapeSizeX then exit;
  FShapeSizeX := Value;
  RedrawObject;
end;

procedure TCustomDrawPresenter.SetShapeSizeY(Const Value: Double);
begin
  if Value = FShapeSizeY then exit;
  FShapeSizeY := Value;
  RedrawObject;
end;

procedure TCustomDrawPresenter.SetShapeUnit(Const Value: TEarthUnitTypes);
begin
  if Value = FShapeUnit then exit;
  FShapeUnit := Value;
  RedrawObject;
end;

class function TCustomDrawPresenter.PrintableClassName : string;
begin
  Result := rsTPointPresenterClassName;
end;

procedure TCustomDrawPresenter.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TCustomDrawPresenter then
  begin
    fShapeUnit := TCustomDrawPresenter(Source).ShapeUnit;
    FShapeSizeX := TCustomDrawPresenter(Source).ShapeSizeX;
    FShapeSizeY := TCustomDrawPresenter(Source).ShapeSizeY;
    FMaxPixelsSizeX := TCustomDrawPresenter(Source).MaxPixelsSizeX;
    FMaxPixelsSizeY := TCustomDrawPresenter(Source).MaxPixelsSizeY;
  end;
end;

function TCustomDrawPresenter.LLInObject(Earth: TCustomEarth; ptLL: TPointLL; Geod: TEarthObject; iTolerance: Double): boolean;
var iTmpX,iTmpY:Double;
    idx,iChain:integer;
begin
 result:=false;
 iTmpX := Earth.EarthCanvas.EarthUnitsFrom(ShapeSizeX, ShapeUnit);
    iTmpY := Earth.EarthCanvas.EarthUnitsFrom(ShapeSizeY, ShapeUnit);
    //...for X
    if MaxPixelsSizeX<>0 then iTmpX:=Min(iTmpX, (MaxPixelsSizeX / Earth.Projection.Scalefactor));
    iTmpX := iTmpX/2;
     //...for Y
    if MaxPixelsSizeY<>0 then iTmpY:=Min(iTmpY, (MaxPixelsSizeY / Earth.Projection.Scalefactor));
    iTmpY := iTmpY/2;

 with Geod as TGeoDataObject do
   begin
    for iChain := 0 to PointsGroups.Count - 1 do
        with PointsGroups[iChain] do
         for idx := 0 to Count - 1 do
          begin
            Result:=PointLLinMER( ptLL, MER( AsLL[idx].iLongX - iTmpX, AsLL[idx].iLatY - iTmpY, 2*iTmpX, 2*iTmpY));
            if Result then exit;
          end;
   end;

 Result:=PointLLinMER( ptLL, MER( Geod.Centroid.iLongX-iTmpX, Geod.Centroid.iLatY - iTmpY, 2*iTmpX, 2*iTmpY));
end;

procedure TCustomDrawPresenter.RenderShape(const ptLL: TPointLL);
begin
  if FIsUpdating then exit; //must set to work the Begin/EndUpdate

  bShapeRendered := True;

  if ParentEarth.Projection.PointLLToXY(ptLL, FDrawCenterXY)then
  begin
    FDrawOffectX := ParentEarth.EarthCanvas.ScaleUnitsToDevice(ShapeSizeX, ShapeUnit);
    FDrawOffectY := ParentEarth.EarthCanvas.ScaleUnitsToDevice(ShapeSizeY, ShapeUnit);
   //... for X
    if MaxPixelsSizeX <> 0 then
     begin
      with ParentEarth.EarthCanvas do // Scale the MaxPixelsSize to the output canvas
       begin
        FDrawOffectX := Min(FDrawOffectX, (MaxPixelsSizeX * CanvasPixelsPerInch / ScreenPixelsPerInch));
       end;
      end;
     FDrawOffectX := FDrawOffectX/2;

   //... for X
   if MaxPixelsSizeY <> 0 then
     begin
      with ParentEarth.EarthCanvas do // Scale the MaxPixelsSize to the output canvas
       begin
        FDrawOffectY := Min(FDrawOffectY, Round(MaxPixelsSizeY * CanvasPixelsPerInch / ScreenPixelsPerInch));
       end;
      end;
    FDrawOffectY := FDrawOffectY/2;
    //..................................................................
    if (FTransAngle<>0) or (FTransScaleX<>1) or (FTransScaleY<>1)
       or (FTransSkewX<>0) or (FTransSkewY<>0) then
      begin
        ParentEarth.EarthCanvas.Translate(-FDrawCenterXY.X,-FDrawCenterXY.Y);
        ParentEarth.EarthCanvas.Rotate(FTransAngle);
        ParentEarth.EarthCanvas.Scale(FTransScaleX,FTransScaleY);
        ParentEarth.EarthCanvas.Skew(FTransSkewX,FTransSkewY);
        ParentEarth.EarthCanvas.Translate(FDrawCenterXY.X,FDrawCenterXY.Y);
      end;

    Drawshape;
    ParentEarth.EarthCanvas.TransformationsReset;
  end;
end;


procedure TCustomDrawPresenter.RenderObject(Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet );
var idx, iChain: integer;iMer:TMER;
begin
  if FIsUpdating then exit; //must set to work the Begin/EndUpdate

  inherited;

  Earth := Earth;
  bSelected := (osSelected in Geod.ObjectState);
  bShapeRendered := False;
  //..... Draw normal ......................................
  with Geod as TGeoDataObject do
    begin
      for iChain := 0 to PointsGroups.Count - 1 do
        with PointsGroups[iChain] do
         for idx := 0 to Count - 1 do
            RenderShape(AsLL[idx]);

      if not bShapeRendered then RenderShape(Centroid);
    end;
  //.......Draw Selected Above normal Darw ........................
  if bSelected=false then exit;
  Earth.EarthCanvas.DefPen.RenderAttribute(Earth.EarthCanvas,true);
  with Geod as TGeoDataObject do
    begin
       for iChain := 0 to PointsGroups.Count - 1 do
        with PointsGroups[iChain] do
         for idx := 0 to Count - 1 do
           Earth.EarthCanvas.RenderBoxLL(AsLL[idx],FDrawOffectX,FDrawOffectY);
    end;
end;

procedure TCustomDrawPresenter.LoadEnvironment(Element: TGXML_Element);
begin
  inherited LoadEnvironment(Element);

  if Element = nil then exit;

  FShapeUnit := StrToUnits(Element.AttributeByName('Unit', UnitsToStr(ShapeUnit)));
  ShapeSize := Element.FloatAttributeByName('SizeX', ShapeSizeX);
  FShapeSizeY := Element.FloatAttributeByName('SizeY', ShapeSizeY);
  FMaxPixelsSizeX := Element.FloatAttributeByName('MaxSizeX', MaxPixelsSizeX);
  FMaxPixelsSizeY := Element.FloatAttributeByName('MaxSizeY', MaxPixelsSizeY);

end;


function TCustomDrawPresenter.SaveEnvironment: TGXML_Element;
begin
  Result := inherited SaveEnvironment;
  Result.AddAttribute('Unit', UnitsToStr(ShapeUnit));
  Result.AddFloatAttribute('SizeX', ShapeSizeX);
  Result.AddFloatAttribute('SizeY', ShapeSizeY);
  Result.AddFloatAttribute('MaxSizeX', MaxPixelsSizeX);
  Result.AddFloatAttribute('MaxSizeY', MaxPixelsSizeY);
end;


procedure TCustomDrawPresenter.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1); //Write Object Stream Version

  Writer.WriteBuffer(FShapeUnit, SizeOf(TEarthUnitTypes));
  Writer.WriteDouble(ShapeSizeX);
  Writer.WriteDouble(ShapeSizeY);
  Writer.WriteDouble(MaxPixelsSizeX);
  Writer.WriteDouble(MaxPixelsSizeY);
end;

procedure TCustomDrawPresenter.ReadProperties(Reader: TEarthStreamReader);
 var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);
  iVer:=Reader.ReadInteger;//Read Object Stream Version

  Reader.ReadBuffer(FShapeUnit, SizeOf(TEarthUnitTypes));
  ShapeSizeX := Reader.ReadDouble;
  ShapeSizeY := Reader.ReadDouble;
  MaxPixelsSizeX := Reader.ReadDouble;
  MaxPixelsSizeY := Reader.ReadDouble;
end;

procedure TCustomDrawPresenter.Drawshape;
begin
  //virtual method
end;

//========================= TCustomDrawBrushPresenter ============================================
constructor TCustomDrawBrushPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create(aPresenterStore, iID);
  FShapePen := TEarthPen.Create(self);
  FShapeBrush := TEarthBrush.Create(self);
  FshapeBrush.BrushColor:=clWhite32;
end;

destructor TCustomDrawBrushPresenter.Destroy;
begin
  FShapePen.Free;
  FShapeBrush.Free;
  inherited Destroy;
end;

procedure TCustomDrawBrushPresenter.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TCustomDrawBrushPresenter then
  begin
    fShapePen.Assign(TCustomDrawBrushPresenter(Source).ShapePen);
    fShapeBrush.Assign(TCustomDrawBrushPresenter(Source).ShapeBrush);

    TransAngle:=TCustomDrawBrushPresenter(Source).TransAngle;
    TransScaleX:=TCustomDrawBrushPresenter(Source).TransScaleX;
    TransScaleY:=TCustomDrawBrushPresenter(Source).TransScaleY;
    TransSkewX:=TCustomDrawBrushPresenter(Source).TransSkewX;
    TransSkewY:=TCustomDrawBrushPresenter(Source).TransSkewY;
  end;
end;

procedure TCustomDrawBrushPresenter.LoadEnvironment(Element: TGXML_Element);
begin
  inherited LoadEnvironment(Element);

  if Element = nil then exit;
  with Element do
   begin
     ShapePen.LoadEnvironment(ElementByName('Pen', 0));
     ShapeBrush.LoadEnvironment(ElementByName('Brush', 0));

     TransAngle := FloatAttributeByName( 'TransAngle', TransAngle);
     TransScaleX := FloatAttributeByName( 'TransScaleX', TransScaleX);
     TransScaleY := FloatAttributeByName( 'TransScaleY', TransScaleY);
     TransSkewX := FloatAttributeByName( 'TransSkewX', TransSkewX);
     TransSkewY := FloatAttributeByName( 'TransSkewY', TransSkewY);
    end;
end;


function TCustomDrawBrushPresenter.SaveEnvironment: TGXML_Element;
begin
  Result := inherited SaveEnvironment;
  Result.AddElement('Pen', ShapePen.SaveEnvironment);
  Result.AddElement('Brush', ShapeBrush.SaveEnvironment);

  Result.AddFloatAttribute( 'TransAngle', TransAngle);
  Result.AddFloatAttribute( 'TransScaleX', TransScaleX);
  Result.AddFloatAttribute( 'TransScaleY', TransScaleY);
  Result.AddFloatAttribute( 'TransSkewX', TransSkewX);
  Result.AddFloatAttribute( 'TransSkewY', TransSkewY);
end;


procedure TCustomDrawBrushPresenter.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1);//Write Object Stream Version

  ShapePen.WriteProperties(Writer);
  ShapeBrush.WriteProperties(Writer);

  Writer.WriteDouble(FTransAngle);
  Writer.WriteDouble(FTransScaleX);
  Writer.WriteDouble(FTransScaleY);
  Writer.WriteDouble(FTransSkewX);
  Writer.WriteDouble(FTransSkewY);
end;

procedure TCustomDrawBrushPresenter.ReadProperties(Reader: TEarthStreamReader);
var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);
  iVer:=Reader.ReadInteger;//File version

  FShapePen.ReadProperties(Reader);
  FShapeBrush.ReadProperties(Reader);

  FTransAngle:=Reader.ReadDouble;
  FTransScaleX:=Reader.ReadDouble;
  FTransScaleY:=Reader.ReadDouble;
  FTransSkewX:=Reader.ReadDouble;
  FTransSkewY:=Reader.ReadDouble;  
end;


procedure TCustomDrawBrushPresenter.RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet);
  begin
   if FIsUpdating then exit; //must set to work the Begin/EndUpdate
   fShapePen.RenderAttribute( Earth.EarthCanvas,false);
   fShapeBrush.RenderAttribute( Earth.EarthCanvas,false);
   inherited RenderObject( Earth,Geod, State);
end;

//================ TUserDrawBrushPresenter ====================================
procedure TUserDrawBrushPresenter.RenderObject(Earth:TCustomEarth; Geod:TEarthObject; State:TEarthObjectStateSet);
 begin
   Earth := Earth;
   bSelected := (osSelected in Geod.ObjectState);
   bShapeRendered := False;
   if Assigned(fOnUserRenderObject) then
      fOnUserRenderObject(Earth, Geod, State) else
      inherited RenderObject( Earth,Geod, State);
 end;

procedure TUserDrawBrushPresenter.DrawShape;
 begin
   if Assigned(fOnUserDrawShape) then fOnUserDrawShape(self);
 end;

//=============== TPointPresenter ========================================================

constructor TPointPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create( aPresenterStore, iID );

  Name := rsPointPresenterName;

  FPointPen := TEarthPen.Create(self);
  FPointBrush := TEarthBrush.Create(self);
  FPointBrush.BrushColor:=clWhite;
  FPointFont := TEarthFont.Create(self);  
  FPointType := ppCircle;
  SetImageName( '' );
end;

destructor TPointPresenter.Destroy;
begin
  if FsImageName <> '' then
    FImage.Free;

  FPointPen.Free;
  FPointBrush.Free;
  FPointFont.Free;

  inherited Destroy;
end;

procedure TPointPresenter.Assign( Source : TPersistent );
begin
  inherited Assign( Source );

  if Source is TPointPresenter then
    with TPointPresenter(Source) do
     begin
     beginUpdate;
     Self.PointType := PointType;
     Self.SymbolIndex := SymbolIndex;
     Self.ShapeSize := ShapeSize;
     Self.MaxPixelsSize := MaxPixelsSize;
     Self.ImageName := ImageName;
     Self.PointPen.Assign( PointPen );
     Self.PointBrush.Assign(PointBrush );
     Self.PointFont.Assign(PointFont );
     endUpdate;
  end;
end;

class function TPointPresenter.FindMatch( Presenters : TEarthPresenterStore;
  APen : TEarthPen;
  ABrush : TEarthBrush; AFont : TEarthFont; ATitleFont : TEarthFont;
  APointType : TPointPresenterType; APointUnit : TEarthUnitTypes;
  APointSize : integer; const AImageName : string ) : integer;
var
  idx : integer;
begin
  for idx := 0 to Presenters.Count - 1 do
    if Presenters[idx] is TPointPresenter then
      with TPointPresenter( Presenters[idx] ) do
      begin
        Result := PresenterID;
        if ( APen <> nil ) and not PointPen.Equals( APen ) then
          continue;
        if ( ABrush <> nil ) and not PointBrush.Equals( ABrush ) then
          continue;
        if ( AFont <> nil ) and not PointFont.Equals( AFont ) then
          continue;
        if ( ATitleFont <> nil ) and not TitleFont.Equals( ATitleFont ) then
          continue;
        if ( PointType = APointType )
          and ( ShapeUnit = APointUnit )
          and ( ShapeSize = APointSize )
          and ( ImageName = AImageName ) then
          Exit;
      end;
  Result := 0;
end;

Function  TPointPresenter.GetFullImageName:String;
 begin
 Result:=FsImageName;
  //...... UnFix Name.............
 if Parent.Parent<>nil then
   begin
    if Parent.Parent is TEarthLayer then
      Result:=UnFixFilePath(FsImageName,TEarthLayer(Parent.Parent).FilePath);

    if Parent.Parent is TEarthObjectStore then
      if TEarthObjectStore(Parent.Parent).Parent <>nil then
       Result:=UnFixFilePath(FsImageName,TEarthObjectStore(Parent.Parent).Parent.FilePath);
   end;
 end;

procedure TPointPresenter.SetImageName( const sName : string );
begin
if sName='' then Exit;

  FsImageName:=sName;
  FsImageName:=GetFullImageName;
  FsImageName :=ParentEarth.ResolveFilename(FsImageName);

  FsImageName := ParentEarth.ResolveFilename( sName );
  if FsImageName <> '' then
  begin
    if FImage = nil then
      FImage := TPicture.Create;
    FImage.LoadFromFile( FsImageName );
  end
  else
  begin
    FImage.Free;
    FImage := nil;
  end;

  //...... Fix Name.............
  if Parent.Parent<>nil then
   begin
    if Parent.Parent is TEarthLayer then
      FsImageName:=FixFilePath(FsImageName,TEarthLayer(Parent.Parent).FilePath);

    if Parent.Parent is TEarthObjectStore then
      if TEarthObjectStore(Parent.Parent).Parent <>nil then
       FsImageName:=FixFilePath(FsImageName,TEarthObjectStore(Parent.Parent).Parent.FilePath);
   end;
  //..............................

 RedrawObject;
end;


procedure TPointPresenter.SetSymbolIndex(Const Val : SmallInt);
 begin
  if Val= FiSymbolIndex then exit;
    FiSymbolIndex := Val;
    RedrawObject;
end;



procedure TPointPresenter.SetPointType(Const Val : TPointPresenterType);
 begin
  if Val=FPointType then exit;
  FPointType:=Val;
  RedrawObject;
 end;


class function TPointPresenter.PrintableClassName : string;
begin
  Result := rsTPointPresenterClassName;
end;

procedure TPointPresenter.Drawshape;
var
   iTmp2 : Double;
   sTmp : string;
 //-----------------------------------
  function SymbolExtent( const Text : string ) : TSize;
  begin   
    Result.cx:=Round(ParentEarth.EarthCanvas.TextWidthGet('W'));
    Result.cy:=Round(ParentEarth.EarthCanvas.TextHeightGet);
  end;
  //-----------------------------------
begin

with ParentEarth.EarthCanvas do
  case PointType of
        ppFontSymbol : { display a font symbol }
          begin
            PointFont.RenderAttribute( ParentEarth.EarthCanvas, bSelected );
            SinCos( ( -PointFont.FontAngle /10 ) * LocalPI / 180, eSin, eCos );
            with SymbolExtent( Chr( FiSymbolIndex ) ) do
            begin
              FDrawCenterXY.X := cx div 2;
              FDrawCenterXY.Y := cy div 2;
            end;
            sTmp := Chr( FiSymbolIndex );
              gDrawTextOut(Round(gaPoints^[0].X - Trunc( FDrawCenterXY.X * eCos - FDrawCenterXY.Y * eSin )),
                           Round(gaPoints^[0].Y - Trunc( FDrawCenterXY.X * eSin + FDrawCenterXY.Y * eCos )), PChar( sTmp ), 1 );
          end;
        ppImage : { display a Bitmap or Icon }
          if FImage <> nil then
          begin
            with FImage do
              iTmp2 := Round( ( FDrawOffectX / Width ) * Height );
             Canvas.StretchDraw(RectD( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - iTmp2,
                                       FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y +iTmp2), FImage.Graphic );
          end;
        ppDot : { display a single pixel }
          gDrawPixel(FDrawCenterXY.X, FDrawCenterXY.Y, PointPen.PenColor );
        ppCircle : { display a circle }
          gDrawEllipse(FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - FDrawOffectX, FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectX );
        ppTriangleUp : { display an upwards pointing triangle }
          begin
            gaPoints^[0] := PointDouble( FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectX );
            gaPoints^[1] := PointDouble( FDrawCenterXY.X, FDrawCenterXY.Y - FDrawOffectX );
            gaPoints^[2] := PointDouble( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y + FDrawOffectX );
            gDrawPolygon(gaPoints, 3 );
          end;
        ppTriangleDown : { display an downwards pointing triangle }
          begin
            gaPoints^[0] := PointDouble( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - FDrawOffectX );
            gaPoints^[1] := PointDouble( FDrawCenterXY.X, FDrawCenterXY.Y + FDrawOffectX );
            gaPoints^[2] := PointDouble( FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y - FDrawOffectX );
            gDrawPolygon(gaPoints, 3 );
          end;
        ppTriangleRight : { display an rightwards pointing triangle }
          begin
            gaPoints^[0] := PointDouble( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - FDrawOffectX );
            gaPoints^[1] := PointDouble( FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y );
            gaPoints^[2] := PointDouble( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y + FDrawOffectX );
            gDrawPolygon(gaPoints, 3 );
          end;
        ppTriangleLeft : { display an leftwards pointing triangle }
          begin
            gaPoints^[0] := PointDouble( FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectX );
            gaPoints^[1] := PointDouble( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y );
            gaPoints^[2] := PointDouble( FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y - FDrawOffectX );
            gDrawPolygon(gaPoints, 3 );
          end;
        ppSquare : { display a square }
          gDrawRectangle(FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - FDrawOffectX, FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectX );
        ppDiamond : { display a diamond }
          begin
            gaPoints^[0] := PointDouble( FDrawCenterXY.X, FDrawCenterXY.Y - FDrawOffectX );
            gaPoints^[1] := PointDouble( FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y );
            gaPoints^[2] := PointDouble( FDrawCenterXY.X, FDrawCenterXY.Y + FDrawOffectX );
            gaPoints^[3] := PointDouble( FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y );
            gDrawPolygon(gaPoints, 4 );
          end;
      end;

end;


procedure TPointPresenter.RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet );
begin
  if FIsUpdating then exit; //must set to work the Begin/EndUpdate
  PointPen.RenderAttribute( Earth.EarthCanvas,false);
  PointBrush.RenderAttribute( Earth.EarthCanvas,false);
  inherited RenderObject( Earth,Geod, State);
end;

procedure TPointPresenter.LoadEnvironment( Element : TGXML_Element );
begin
  inherited LoadEnvironment( Element );

  if Element <> nil then
    with Element do
    begin
      FPointType := TPointPresenterType( IntAttributeByName( 'Type', Ord( PointType )));
      FShapeUnit := StrToUnits( AttributeByName( 'Unit', UnitsToStr( ShapeUnit )));
      ShapeSize := FloatAttributeByName( 'Size', ShapeSize );
      MaxPixelsSize := FloatAttributeByName( 'MaxSize', MaxPixelsSize );
      FiSymbolIndex := IntAttributeByName( 'SymbolIndex', SymbolIndex );
      ImageName := AttributeByName( 'ImageName', ImageName );

      PointPen.LoadEnvironment( ElementByName( 'Pen', 0 ));
      PointBrush.LoadEnvironment( ElementByName( 'Brush', 0 ));
      PointFont.LoadEnvironment( ElementByName( 'Font', 0 ));
    end;
end;

function TPointPresenter.SaveEnvironment : TGXML_Element;
begin
  Result := inherited SaveEnvironment;

  Result.AddIntAttribute( 'Type', Ord( PointType ));
  Result.AddAttribute( 'Unit', UnitsToStr( ShapeUnit ));
  Result.AddFloatAttribute( 'Size', ShapeSize );
  Result.AddFloatAttribute( 'MaxSize', MaxPixelsSize );
  Result.AddIntAttribute( 'SymbolIndex', SymbolIndex );
  Result.AddAttribute( 'ImageName', ExtractRelativePath(
    ExpandFilename( ParentEarth.DataDirectory ), ExpandFilename( ImageName )));

  Result.AddElement( 'Pen', PointPen.SaveEnvironment );
  Result.AddElement( 'Brush', PointBrush.SaveEnvironment );
  Result.AddElement( 'Font', PointFont.SaveEnvironment );
end;

procedure TPointPresenter.WriteProperties( Writer : TEarthStreamWriter );
begin
  if Writer=nil then exit;
  inherited WriteProperties( Writer );
  Writer.WriteInteger(1);   //Write Object Stream Version

  Writer.WriteBuffer( FPointType, SizeOf( FPointType ) );
  Writer.WriteBuffer( FShapeUnit, SizeOf( TEarthUnitTypes ) );
  if MaxPixelsSize > 0 then
  begin
    Writer.WriteDouble( -ShapeSize );
    Writer.WriteDouble( MaxPixelsSize );
  end
  else
    Writer.WriteDouble( ShapeSize );

  Writer.WriteSmallInt( SymbolIndex );
  Writer.WriteShortString( ImageName );

  PointPen.WriteProperties( Writer );
  PointBrush.WriteProperties( Writer );
  PointFont.WriteProperties( Writer );
end;

procedure TPointPresenter.ReadProperties( Reader : TEarthStreamReader );
var sTmp : string;
    iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties( Reader );

  if giFileVersion>=TG_FILEVERSION1002 then
     iVer:= Reader.ReadInteger;            //Read Object Stream Version

  Reader.ReadBuffer( FPointType, SizeOf( FPointType ) );
  Reader.ReadBuffer( FShapeUnit, SizeOf( TEarthUnitTypes ) );
  ShapeSize := Reader.ReadDouble;

  if ShapeSize < 0 then
  begin
    ShapeSize := -ShapeSize;
    MaxPixelsSize := Reader.ReadDouble;
  end;

  fiSymbolIndex := Reader.ReadSmallInt;
  sTmp := Reader.ReadShortString;
  if Length( sTmp ) > 0 then
    fsImageName := sTmp;

  FPointPen.ReadProperties( Reader );
  FPointBrush.ReadProperties( Reader );
  FPointFont.ReadProperties( Reader );
end;

//========================================================================================
//================================ TBasePolyPresenter ====================================
//========================================================================================

constructor TBasePolyPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create( aPresenterStore, iID );

  Name := rsBasePolyPresenterName;

  FPolyPen := TEarthPen.Create(self);
  FPolyBrush := TEarthBrush.Create(self);
end;

destructor TBasePolyPresenter.Destroy;
begin
  FPolyPen.Free;
  FPolyBrush.Free;
  inherited Destroy;
end;

procedure TBasePolyPresenter.Assign( Source : TPersistent );
begin
  inherited Assign( Source );

  if Source.InheritsFrom( TBasePolyPresenter ) then
  begin
    PolyPen.Assign( TBasePolyPresenter( Source ).PolyPen );
    PolyBrush.Assign( TBasePolyPresenter( Source ).PolyBrush );
  end;
end;

class function TBasePolyPresenter.FindMatch( Presenters : TEarthPresenterStore;
  APen : TEarthPen; ABrush : TEarthBrush; ATitleFont : TEarthFont ) : integer;
var
  idx : integer;
begin
  for idx := 0 to Presenters.Count - 1 do
    if Presenters[idx] is TBasePolyPresenter then
      with TBasePolyPresenter( Presenters[idx] ) do
      begin
        Result := PresenterID;
        if ( APen <> nil ) and not PolyPen.Equals( APen ) then
          continue;
        if ( ABrush <> nil ) and not PolyBrush.Equals( ABrush ) then
          continue;
        if ( ATitleFont <> nil ) and not TitleFont.Equals( ATitleFont ) then
          continue;
        Exit;
      end;
  Result := 0;
end;

class function TBasePolyPresenter.PrintableClassName : string;
begin
  Result := rsTBasePolyPresenterClassName;
end;

procedure TBasePolyPresenter.RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet );
 var x1,y1,x2,y2:Double;
begin
inherited;

if Geod is TGeoDataObject then
  begin
    PolyPen.RenderAttribute(Earth.EarthCanvas,false);     //Set Normal Pen
    if Geod.Closed then PolyBrush.RenderAttribute( Earth.EarthCanvas, false);  //Set Normal Brush
    Earth.EarthCanvas.RenderChainStore( TGeoDataObject( Geod ).PointsGroups,State, PolyPen.DrawPenWidth);  //Draw normal
    //.............Fill Patttern ..............................................
    if PolyBrush.DrawFillWithImage then Earth.EarthCanvas.PathFillWithImage32(PolyBrush.BrushFillImage);

     //Draw Selected Above normal Draw
    if osSelected in State then
       begin
         PolyPen.RenderAttribute(Earth.EarthCanvas, true);  //Set Selected Pen
         if Geod.Closed then PolyBrush.RenderAttribute( Earth.EarthCanvas, true );  //Set Selected Brush
         Earth.EarthCanvas.RenderChainStore( TGeoDataObject( Geod ).PointsGroups,State,PolyPen.DrawPenWidth); //Draw Selected
       end;

  end;
end;

procedure TBasePolyPresenter.LoadEnvironment( Element : TGXML_Element );
begin
  inherited LoadEnvironment( Element );

  if Element <> nil then
    with Element do
    begin
      PolyPen.LoadEnvironment( ElementByName( 'Pen', 0 ));
      PolyBrush.LoadEnvironment( ElementByName( 'Brush', 0 ));
    end;
end;

function TBasePolyPresenter.SaveEnvironment : TGXML_Element;
begin
  Result := inherited SaveEnvironment;

  Result.AddElement( 'Pen', PolyPen.SaveEnvironment );
  Result.AddElement( 'Brush', PolyBrush.SaveEnvironment );
end;

procedure TBasePolyPresenter.WriteProperties( Writer : TEarthStreamWriter );
begin
  if Writer=nil then exit;
  inherited WriteProperties( Writer );
  Writer.WriteInteger(1);   //Write Object Stream Version

  PolyPen.WriteProperties( Writer );
  if not( Self is TPolylinePresenter ) then
    PolyBrush.WriteProperties( Writer );
end;

procedure TBasePolyPresenter.ReadProperties( Reader : TEarthStreamReader );
 var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties( Reader );
  if giFileVersion>=TG_FILEVERSION1002 then
     iVer:= Reader.ReadInteger;            //Read Object Stream Version

  FPolyPen.ReadProperties( Reader );
  if not( Self is TPolylinePresenter ) then
    FPolyBrush.ReadProperties( Reader );
end;


//============================ TPolyPresenter =================================

constructor TPolyPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create( aPresenterStore, iID );
  Name := rsPolyPresenterName;
end;
 
class function TPolyPresenter.FindMatch( Presenters : TEarthPresenterStore;
  APen : TEarthPen; ABrush : TEarthBrush; ATitleFont : TEarthFont ) : integer;
var
  idx : integer;
begin
  for idx := 0 to Presenters.Count - 1 do
    if Presenters[idx] is TPolyPresenter then
      with TPolyPresenter( Presenters[idx] ) do
      begin
        Result := PresenterID;
        if ( APen <> nil ) and not PolyPen.Equals( APen ) then
          continue;
        if ( ABrush <> nil ) and not PolyBrush.Equals( ABrush ) then
          continue;
        if ( ATitleFont <> nil ) and not TitleFont.Equals( ATitleFont ) then
          continue;
        Exit;
      end;
  Result := 0;
end;

class function TPolyPresenter.PrintableClassName : string;
begin
  Result := rsTPolyPresenterClassName;
end;

procedure TPolyPresenter.Assign( Source : TPersistent );
begin
  inherited Assign( Source );

  if Source.InheritsFrom( TBasePolyPresenter ) then
  begin
    PolyPen.Assign( TBasePolyPresenter( Source ).PolyPen );
    PolyBrush.Assign( TBasePolyPresenter( Source ).PolyBrush );
  end;
end;   

//============================ TPolygonPresenter ========================
constructor TPolygonPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited;
  Name := rsPolygonPresenterName;
  FPolyBrush.BrushColor:=clsilver;
end;

class function TPolygonPresenter.PrintableClassName : string;
begin
  Result := rsTPolygonPresenterClassName;
end;

procedure TPolygonPresenter.RenderObject( Earth : TCustomEarth;
  Geod : TEarthObject; State : TEarthObjectStateSet);
begin
  Geod.Closed := True;
  Include( State, osClosed );
  inherited RenderObject( Earth, Geod, State );
end;

//============================= TPolylinePresenter ========================
constructor TPolylinePresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited;
  Name := rsPolyline32PresenterName;
end;

class function TPolylinePresenter.PrintableClassName : string;
begin
  Result := rsTPolyline32PresenterClassName;
end;

procedure TPolylinePresenter.RenderObject( Earth : TCustomEarth;
  Geod : TEarthObject; State : TEarthObjectStateSet);
begin
  Geod.Closed := False;
  Exclude( State, osClosed );
  inherited RenderObject( Earth, Geod, State );
end;

//======================================================================================================
initialization
  RegisterClasses( [TEarthPresenter,
                    TCustomDrawPresenter,
                    TCustomDrawBrushPresenter,
                    TUserDrawBrushPresenter,
                    TPointPresenter,
                    TBasePolyPresenter,
                    TPolyPresenter,
                    TPolygonPresenter,
                    TPolylinePresenter
                    ] );
end.
