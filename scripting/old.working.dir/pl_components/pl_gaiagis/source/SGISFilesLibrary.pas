
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit SGISFilesLibrary;




interface

uses
  SysUtils, Classes,SBaseFilesLibrary;

type

{A library for store Eatrh views files}
TEarthViewsLibrary = class(TBaseFilesLibrary)
 end;

TEarthLayersLibrary = class(TBaseFilesLibrary)
 end;

TEarthFilesLibrary = class(TBaseFilesLibrary)
 end;

implementation

initialization
   RegisterClasses([
                   TEarthViewsLibrary,
                   TEarthLayersLibrary,
                   TEarthFilesLibrary
                   ]);

end.
