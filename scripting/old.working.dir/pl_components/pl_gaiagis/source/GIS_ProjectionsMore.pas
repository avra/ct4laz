
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_ProjectionsMore;



interface

uses LCLIntf, LCLType, LMessages,SysUtils, Classes,
     GIS_SysUtils, GIS_EarthBase, GIS_Classes,
     GIS_Projections,math;

type
  {------------------------ TTGnomonicPrjPrj -------------------------}
  TGnomonicPrj = class( TCartesianPrj )
  public
    constructor Create(Parent : TEarthProjection); override;
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end;

  {------------------------ TAzimuthal2EquidistantPrj -------------------------}
  TAzimuthal2EquidistantPrj = class( TCartesianPrj )
  public
    constructor Create(Parent : TEarthProjection); override;
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
    procedure PaintSurface; override;
  end;

  {------------------------ TAzimuthalEquidistantPrj --------------------------}
  TAzimuthalEquidistantPrj = class( TAzimuthal2EquidistantPrj )
  end;

  {---------------------------- TEquidistantPrj -------------------------------}
  TEquidistantPrj = class( TCartesianPrj )
  public
    constructor Create(Parent : TEarthProjection); override;
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
    procedure PaintSurface; override;
  end;

  {---------------------------- TCylEqualAreaPrj ------------------------------}
  TCylEqualAreaPrj = class( TCartesianPrj )
  public
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end;

  {---------------------------- TBehrmannCylEqualAreaPrj ----------------------}
  TBehrmannCylEqualAreaPrj = class( TCylEqualAreaPrj )
  public
    constructor Create(Parent : TEarthProjection); override;
  end;

  {---------------------------- TPetersPrj ------------------------------------}
  TPetersPrj = class( TCylEqualAreaPrj )
  public
    constructor Create(Parent : TEarthProjection); override;
  end;

  {---------------------------- TAlbersEqualAreaConicPrj ----------------------}
  TAlbersEqualAreaConicPrj = class( TCartesianPrj )
  private
    n, Rho0, C : Extended;
  public
    constructor Create(Parent : TEarthProjection); override;

    procedure PropertyChanged( ProjectionProperty : TProjectionProperty ); override;
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end;

  {---------------------------- TBonnePrj -------------------------------------}
  TBonnePrj = class( TCartesianPrj )
  private
    cotSP : Extended;
  public
    constructor Create(Parent : TEarthProjection); override;

    procedure PropertyChanged( ProjectionProperty : TProjectionProperty ); override;
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end;

  {---------------------------- TSinusoidalPrj -------------------------------------}
  TSinusoidalPrj = class( TCartesianPrj )
  public
    function PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean; override;
    function XYToLL(iX, iY:Double; iIndex : Integer) : Boolean; override;
  end; 

implementation

{-------------------------------------------------------------------------
 TAzimuthal2EquidistantPrj.Create
-------------------------------------------------------------------------}
{**
  @Param  ParentEarth Earth to associate the Projection with.
} 
constructor TAzimuthal2EquidistantPrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );

  Projection.Flags := Projection.Flags + [pfContinuous];
  Projection.GraticuleLongSteps := 180;
end;

{-------------------------------------------------------------------------
 TAzimuthal2EquidistantPrj.XYtoLL()
-------------------------------------------------------------------------}
{**
  @Param iX Screen X coordinate to convert.
  @Param iY Screen Y coordinate to convert.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the visible surface of the Earth.
} 
function TAzimuthal2EquidistantPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var x, y,
    c, sinc, cosc,
    sinhlat, coshlat,
    lat2, lon2 : extended;
begin
  with Projection do
  begin
    x := ( iX - XOrigin ) / ScaleFactor * GU_TORADIANS;
    y := -( iY - YOrigin ) / ScaleFactor * GU_TORADIANS;
    c := Hypot( x, y );
    if c <= GEPSILON then begin
      lat2 := 0;
      lon2 := 0;
      Result := True;
    end else begin
      SinCos( c, sinc, cosc );
      SinCos(CentralParallel * DEG_TORADIANS,sinhlat,coshlat);
      lat2 := ArcSin( cosc*sinhlat + y*sinc*coshlat/c );
      lon2 := ArcTan2( x*sinc,(c*coshlat*cosc-y*sinhlat*sinc) );
      { check to see if the coords are outside the Earth }
      Result := ( abs( lon2 ) < LocalPi ) and ( abs( lat2 ) < HalfPi );
    end;
    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(Mod180Float( ( lon2 * GU_FROMRADIANS + CentralMeridian * GU_DEGREE)),
                                             ( lat2 * GU_FROMRADIANS ));
  end;
end;

{-------------------------------------------------------------------------
 TAzimuthal2EquidistantPrj.LLToXY
-------------------------------------------------------------------------}
{**
  @Param ptLL TPointLL object to convert to XY screen Coordinates.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the screen.
} 
function TAzimuthal2EquidistantPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var lat2, lon2, plat, plon, cll, c, sinll, cosll, sinc, cosc, kprime,
      HLat, HLon, sinHLat, sinLat2, cosHLat, cosLat2 : Extended;
begin
  with ptLL do
  begin    { check to see if the coords are outside the Earth }
    with Projection.ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );
    if Result then
      with Projection do
      begin
        Hlat := CentralParallel * DEG_TORADIANS; //CentralParallel must be in degrees (decimal)
        Hlon := CentralMeridian * DEG_TORADIANS; //CentralMeridian must be in degrees (decimal)
        SinCos(HLat, sinHLat, cosHlat);
        lat2 := iLatY  * GU_TORADIANS;
        lon2 := iLongX * GU_TORADIANS;
        SinCos(Lat2, sinLat2, cosLat2);
        SinCos(lon2-HLon,sinll,cosll);
        cll :=  CosLat2 * Cosll;
        cosc := SinHlat * SinLat2 + CosHlat * cll;
        If cosc > 0.9999999999999999999 Then begin
          Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(XOrigin,YOrigin);
          Exit;
        end;
        If cosc < -0.9999999999999999999999 Then begin
          Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(XOrigin,YOrigin);
          Exit;
        end;
        c := arccos(cosc);
        SinCos(c,sinc,cosc);
        kprime := c/sinc;
        plon := kprime*CosLat2*sinll*GU_FROMRADIANS;
        plat := kprime*(CosHLat*SinLat2-SinHLat*cll)*GU_FROMRADIANS;
        Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(XOrigin + ( plon * ScaleFactor ),
                                                YOrigin - ( plat * ScaleFactor ) );
      end;
  end;
end;

{-------------------------------------------------------------------------
 TAzimuthal2EquidistantPrj.PaintSurface
-------------------------------------------------------------------------}
procedure TAzimuthal2EquidistantPrj.PaintSurface;
const
  SEGMENTS = 90;
var
  idx : Integer;
  eLastX, eLastY, eX, eY : Extended;
  eCosAngle, eSinAngle : Extended;
begin
  with Projection do
  begin
    eY := 0;
    eX := EARTHRADIUS * ScaleFactor * LocalPi;

    Earth.EarthCanvas.gaPoints^[0] := PointDouble( ( XOrigin + eX ), ( YOrigin + eY ) );
                                      SinCos( ( GU_360_DEGREE / SEGMENTS ) * GU_TORADIANS, eSinAngle, eCosAngle );

    idx := 1;
    while idx < SEGMENTS do
    begin
      eLastX := eX;
      eLastY := eY;
      eX := eLastX * eCosAngle - eLastY * eSinAngle;
      eY := eLastX * eSinAngle + eLastY * eCosAngle;
      Earth.EarthCanvas.gaPoints^[idx] := PointDouble( ( XOrigin + eX ), ( YOrigin + eY ) );
      Inc( idx );
    end;

    Earth.EarthCanvas.DrawClippedPoly( SEGMENTS, [osClosed,osClipLeft,osClipTop,osClipBottom,osClipRight], 1 );
  end;
end;

{-------------------------------------------------------------------------
 TEquidistantPrj.Create
-------------------------------------------------------------------------}
{**
  @Param  ParentEarth Earth to associate the Projection with.
} 
constructor TEquidistantPrj.Create( Parent : TEarthProjection);
begin
  inherited Create( Parent );

  Projection.Flags := Projection.Flags + [pfContinuous];
end;

{-------------------------------------------------------------------------
 TEquidistantPrj.XYtoLL()
-------------------------------------------------------------------------}
{**
  @Param iX Screen X coordinate to convert.
  @Param iY Screen Y coordinate to convert.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the visible surface of the Earth.
} 
function TEquidistantPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  x, y, D, sinD, cosD : Extended;
  lat, lon : Extended;
begin
  with Projection do
  begin
    x := ( iX - XOrigin ) / ScaleFactor * GU_TORADIANS;
    y := -( iY - YOrigin ) / ScaleFactor * GU_TORADIANS;
    D := Hypot( x, y );
    if D <= GEPSILON then
    begin
      lat := 0;
      lon := 0;
      Result := True;
    end
    else
    begin
      SinCos( D, sinD, cosD );
      lat := ArcSin( ( y * sinD ) / D );
      lon := ArcCos( cosD / Cos( lat ) );

    { check to see if the coords are outside the Earth }
      Result := ( abs( lon ) < LocalPi ) and ( abs( lat ) < HalfPi );
    end;

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
                              Mod180Float( ( lon * GU_FROMRADIANS + CentralMeridian * GU_DEGREE )),
                              ( lat * GU_FROMRADIANS ) );
  end;
end;

{-------------------------------------------------------------------------
 TEquidistantPrj.LLToXY
-------------------------------------------------------------------------}
{**
  @Param ptLL TPointLL object to convert to XY screen Coordinates.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the screen.
} 
function TEquidistantPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  sinlat2, coslat2,
    d, tc, lat2, lon2,
    xcoord, ycoord,
    sinx, cosx : extended;
begin
  with ptLL, Projection do
  begin
  { check to see if the coords are outside the Earth }
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    if Result then
    begin
      lat2 := iLatY * GU_TORADIANS;
      lon2 := Mod180Float( iLongX - Round( CentralMeridian * GU_DEGREE)) * GU_TORADIANS;

      SinCos( lat2, sinlat2, coslat2 );
      d := arccos( coslat2 * cos( lon2 ) );
      SinCos( d, sinx, cosx );
      if Abs( sinx ) < 10E-6 then
        tc := 0.0
      else
      begin
        tc := arccos( sinlat2 / sinx );
        if sin( lon2 ) < 0.0 then
          tc := 2.0 * pi - tc;
      end;
      SinCos( tc, sinx, cosx );
      xcoord := Sinx * d * GU_FROMRADIANS; //-pi .. pi
      ycoord := Cosx * d * GU_FROMRADIANS; //-pi .. pi

      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
                                        XOrigin + ( xcoord * ScaleFactor ),
                                        YOrigin - ( ycoord * ScaleFactor ) );
    end;
  end;
end;

{-------------------------------------------------------------------------
 TEquidistantPrj.PaintSurface
-------------------------------------------------------------------------}
procedure TEquidistantPrj.PaintSurface;
const
  SEGMENTS = 90;
var
  idx : Integer;
  eLastX, eLastY, eX, eY : Extended;
  eCosAngle, eSinAngle : Extended;
begin
  with Projection do
  begin
    eY := 0;
    eX := EARTHRADIUS * ScaleFactor * LocalPi;

    Earth.EarthCanvas.gaPoints^[0] := PointDouble( ( XOrigin + eX ), ( YOrigin + eY ) );
                                          SinCos( ( GU_360_DEGREE / SEGMENTS ) * GU_TORADIANS, eSinAngle, eCosAngle );

    idx := 1;
    while idx < SEGMENTS do
    begin
      eLastX := eX;
      eLastY := eY;
      eX := eLastX * eCosAngle - eLastY * eSinAngle;
      eY := eLastX * eSinAngle + eLastY * eCosAngle;
      Earth.EarthCanvas.gaPoints^[idx] := PointDouble( ( XOrigin + eX ), ( YOrigin + eY ) );
      Inc( idx );
    end;
    Earth.EarthCanvas.DrawClippedPoly( SEGMENTS, [osClosed,osClipLeft,osClipTop,osClipBottom,osClipRight], 1 );
  end;
end;

{-------------------------------------------------------------------------
 TCylEqualAreaPrj.XYtoLL()
-------------------------------------------------------------------------}
{**
  @Param iX Screen X coordinate to convert.
  @Param iY Screen Y coordinate to convert.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the visible surface of the Earth.
} 
function TCylEqualAreaPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  x, y, lat, long, eCos : Extended;
begin
  with Projection do
  begin
    x := ( iX - XOrigin ) / ScaleFactor * GU_TORADIANS;
    y := -( iY - YOrigin ) / ScaleFactor * GU_TORADIANS;

    begin
      eCos := Cos( FirstParallel * DEG_TORADIANS);
      lat := ArcSin( y * eCos );
      Long := ( x / eCos );

    { check to see if the coords are outside the Earth }
      Result := ( abs( long ) < LocalPi ) and ( abs( lat ) < HalfPi );
    end;

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        Mod180Float( ( long * GU_FROMRADIANS + CentralMeridian * GU_DEGREE )),
        ( lat * GU_FROMRADIANS ) );
  end;
end;

{-------------------------------------------------------------------------
 TCylEqualAreaPrj.PointLLToXY
-------------------------------------------------------------------------}
{**
  @Param ptLL TPointLL object to convert to XY screen Coordinates.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the screen.
} 
function TCylEqualAreaPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  lat, long, eCos,
    xcoord, ycoord : extended;
begin
  with ptLL, Projection do
  begin
  { check to see if the coords are outside the Earth }
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    if Result then
    begin
      lat := iLatY * GU_TORADIANS;
      long := Mod180Float( iLongX - Round( CentralMeridian * GU_DEGREE )) * GU_TORADIANS;

      eCos := Cos( FirstParallel * DEG_TORADIANS);
      xcoord := long * eCos * GU_FROMRADIANS;
      ycoord := Sin( lat ) / eCos * GU_FROMRADIANS;

      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        XOrigin + ( xcoord * ScaleFactor ),
        YOrigin - ( ycoord * ScaleFactor ) );
    end;
  end;
end;

{------------------------------------------------------------------------------
 TBehrmannCylEqualAreaPrj.Create
------------------------------------------------------------------------------}
{**
  @Param  ParentEarth Earth to associate the Projection with.
} 
constructor TBehrmannCylEqualAreaPrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );
  Projection.FirstParallel := 30; { For Behrmann Projection }
end;

{------------------------------------------------------------------------------
 TPetersPrj.Create
------------------------------------------------------------------------------}
{**
  @Param  ParentEarth Earth to associate the Projection with.
}
constructor TPetersPrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );
  Projection.FirstParallel := 45; { For Peters Projection }
end;

{------------------------------------------------------------------------------
 TAlbersEqualAreaConicPrj.Create
------------------------------------------------------------------------------}
{**
  @Param  ParentEarth Earth to associate the Projection with.
} 
constructor TAlbersEqualAreaConicPrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );

  Projection.FirstParallel := 45;
  Projection.SecondParallel := 0;
end;

{------------------------------------------------------------------------------
 TAlbersEqualAreaConicPrj.SetProperty
------------------------------------------------------------------------------}
procedure TAlbersEqualAreaConicPrj.PropertyChanged( ProjectionProperty : TProjectionProperty );
var
  eCos, eSin : Extended;
begin
 { if either the first or second parallels changed }
  if ( ProjectionProperty = ppFirstParallel ) or ( ProjectionProperty = ppSecondParallel ) then
    with Projection do
    begin
      SinCos( FirstParallel * DEG_TORADIANS, eSin, eCos );
      n := 0.5 * ( eSin + Sin( SecondParallel * DEG_TORADIANS ) );

      if n = 0 then n := 1;

      C := Sqr( eCos ) + 2 * n * eSin;
      Rho0 := Sqrt( C ) / n;
    end;
end;

{------------------------------------------------------------------------------
 TAlbersEqualAreaConicPrj.PointLLToXY
------------------------------------------------------------------------------}
{**
  @Param ptLL TPointLL object to convert to XY screen Coordinates.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the screen.
} 
function TAlbersEqualAreaConicPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  lat, long, eCos, eSin : Extended;
  xcoord, ycoord : Extended;
  Rho, Theta : Extended;
begin
  with ptLL, Projection do
  begin
  { check to see if the coords are outside the Earth }
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    if Result then
    begin
      lat := iLatY * GU_TORADIANS;
      long := Mod180Float( iLongX - Round( CentralMeridian * GU_DEGREE )) * GU_TORADIANS;

      theta := n * ( long );
      Rho := Sqrt( C - 2 * n * Sin( lat ) ) / n;

      SinCos( Theta, eSin, eCos );

      xcoord := Rho * eSin * GU_FROMRADIANS;
      ycoord := ( Rho0 - Rho * eCos ) * GU_FROMRADIANS;

      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        XOrigin + ( xcoord * ScaleFactor ),
        YOrigin - ( ycoord * ScaleFactor ) );
    end;
  end;
end;

{------------------------------------------------------------------------------
 TAlbersEqualAreaConicPrj.XYToLL
------------------------------------------------------------------------------}
{**
  @Param iX Screen X coordinate to convert.
  @Param iY Screen Y coordinate to convert.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the visible surface of the Earth.
} 
function TAlbersEqualAreaConicPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  x, y, lat, long, Rho, Theta : Extended;
begin
  with Projection do
  begin
    x := ( iX - XOrigin ) / ScaleFactor * GU_TORADIANS;
    y := -( iY - YOrigin ) / ScaleFactor * GU_TORADIANS;

    Rho := Sqrt( x * x + Sqr( Rho0 - y ) );
    Theta := ArcTan( x / ( Rho0 - y ) );

    begin
      lat := ArcSin( ( C - Rho * Rho * n * n ) / ( n + n ) );
      Long := Theta / n;

    { check to see if the coords are outside the Earth }
      Result := ( abs( long ) < LocalPi ) and ( abs( lat ) < HalfPi );
    end;

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        Mod180Float( ( long * GU_FROMRADIANS + CentralMeridian * GU_DEGREE)),( lat * GU_FROMRADIANS ) );
  end;
end;

{------------------------------------------------------------------------------
 TBonnePrj.Create
------------------------------------------------------------------------------}
{**
  @Param  ParentEarth Earth to associate the Projection with.
}
constructor TBonnePrj.Create( Parent : TEarthProjection );
begin
  inherited Create( Parent );

  Projection.FirstParallel := 90;
end;

{------------------------------------------------------------------------------
 TBonnePrj.SetProperty
------------------------------------------------------------------------------}
procedure TBonnePrj.PropertyChanged( ProjectionProperty : TProjectionProperty );
begin
 { if the first parallel changes }
  if ProjectionProperty = ppFirstParallel then
    with Projection do
      if FirstParallel = 0 then
        cotSP := LocalPI
      else
        cotSP := 1 / Tan( FirstParallel * DEG_TORADIANS);
end;

{------------------------------------------------------------------------------
 TBonnePrj.PointLLToXY
------------------------------------------------------------------------------}
{**
  @Param ptLL TPointLL object to convert to XY screen Coordinates.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the screen.
} 
function TBonnePrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  lat, long : Extended;
  xcoord, ycoord : Extended;
  Rho, E : Extended;
begin
  with ptLL, Projection do
  begin
  { check to see if the coords are outside the Earth }
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    if Result then
    try
      lat := iLatY * GU_TORADIANS;
      long := Mod180Float( iLongX - Round( CentralMeridian * GU_DEGREE )) * GU_TORADIANS;

      Rho := ( cotSP + FirstParallel * DEG_TORADIANS - lat );
      E := long * ( Cos( lat ) / Rho );

      xcoord := Rho * Sin( E );
      ycoord := ( cotSP - Rho * Cos( E ) );

      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        XOrigin + ( xcoord * ScaleFactor * GU_FROMRADIANS ),
        YOrigin - ( ycoord * ScaleFactor * GU_FROMRADIANS ) );
    except
    end;
  end;
end;

{------------------------------------------------------------------------------
 TBonnePrj.XYToLL
------------------------------------------------------------------------------}
{**
  @Param iX Screen X coordinate to convert.
  @Param iY Screen Y coordinate to convert.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the visible surface of the Earth.
} 
function TBonnePrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  x, y, lat, long, Rho : Extended;
begin
  with Projection do
  begin
    x := ( iX - XOrigin ) / ScaleFactor * GU_TORADIANS;
    y := -( iY - YOrigin ) / ScaleFactor * GU_TORADIANS;

    Rho := Sign( FirstParallel ) * Sqrt( x * x + Sqr( cotSP - y ) );

    lat := cotSP + FirstParallel * DEG_TORADIANS - Rho;
    Long := ( Rho / Cos( lat ) ) * ArcTan2( x, cotSP - y );

   { check to see if the coords are outside the Earth }
    Result := ( abs( long ) < LocalPi ) and ( abs( lat ) < HalfPi );

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        Mod180Float( ( CentralMeridian + long * GU_FROMRADIANS )),
        ( lat * GU_FROMRADIANS ));
  end;
end;

{------------------------------------------------------------------------------
 TSinusoidalPrj.PointLLToXY
------------------------------------------------------------------------------}
{**
  @Param ptLL TPointLL object to convert to XY screen Coordinates.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the screen.
} 
function TSinusoidalPrj.PointLLToXY( const ptLL : TPointLL; iIndex : Integer ) : Boolean;
var
  xcoord, ycoord : Extended;
begin
  with ptLL,Projection do
  begin
  { check to see if the coords are outside the Earth }
    with ExtentsLL do
      Result := ( iLongX >= Left ) and ( iLongX <= Right ) and ( iLatY >= Top ) and ( iLatY <= Bottom );

    if Result then
    begin
      ycoord := iLatY * GU_TORADIANS;
      xcoord := Mod180Float( iLongX - Round( CentralMeridian * GU_DEGREE )) * GU_TORADIANS * Cos( ycoord );

      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        XOrigin + ( xcoord * GU_FROMRADIANS * ScaleFactor ),
        YOrigin - ( ycoord * GU_FROMRADIANS * ScaleFactor ) );
    end;
  end;
end;

{------------------------------------------------------------------------------
 TSinusoidalPrj.XYToLL
------------------------------------------------------------------------------}
{**
  @Param iX Screen X coordinate to convert.
  @Param iY Screen Y coordinate to convert.
  @Param iIndex Position in the gaPoints global array to store the result.
  @Result True if the Point is on the visible surface of the Earth.
}
function TSinusoidalPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
var
  x, y, lat, long : Extended;
begin
  with Projection do
  begin
    x := ( iX - XOrigin ) / ScaleFactor * GU_TORADIANS;
    y := -( iY - YOrigin ) / ScaleFactor * GU_TORADIANS;

    lat := y;
    Long := x / Cos( lat );

   { check to see if the coords are outside the Earth }
    Result := ( abs( long ) < LocalPi ) and ( abs( lat ) < HalfPi );

    if Result then
      Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
        Mod180Float( ( CentralMeridian + long * GU_FROMRADIANS ) ),
        ( lat * GU_FROMRADIANS ) );
  end;
end;

{------------------------------------------------------------------------------
  TGnomonicPrj.Create
------------------------------------------------------------------------------}
constructor TGnomonicPrj.Create(Parent: TEarthProjection);
begin
  inherited;

  Projection.Flags := Projection.Flags + [pfContinuous];
  Projection.GraticuleLongSteps := 180;
  Projection.GraticuleLatSteps := 180;
end;

{------------------------------------------------------------------------------
  TGnomonicPrj.PointLLToXY
------------------------------------------------------------------------------}
function TGnomonicPrj.PointLLToXY(const ptLL: TPointLL; iIndex: Integer): Boolean;
var
  CosC, k : Extended;
begin
  with ptLL,Projection do
  begin

    cosC := Sin( CentralParallel * DEG_TORADIANS )
      * Sin( iLatY * GU_TORADIANS )
      + Cos( CentralParallel * DEG_TORADIANS )
      * Cos( iLatY * GU_TORADIANS )
      * Cos( iLongX * GU_TORADIANS - ( CentralMeridian * DEG_TORADIANS ));

    Result := CosC > 0;

    if not Result then  Exit;

    k := 1 / CosC;

    Earth.EarthCanvas.gaPoints^[iIndex] := PointDouble(
      XOrigin + ( EARTHRADIUS * GU_TORADIANS * k * Cos( iLatY * GU_TORADIANS )
        * Sin( iLongX * GU_TORADIANS - ( CentralMeridian * DEG_TORADIANS ))
        * GU_FROMRADIANS * ScaleFactor),
      YOrigin + ( EARTHRADIUS * GU_TORADIANS * k * ( Cos( CentralParallel * DEG_TORADIANS )
        * Sin( iLatY * GU_TORADIANS )
        - Sin( CentralParallel * DEG_TORADIANS )
        * Cos( iLatY * GU_TORADIANS )
        * Cos( iLongX * GU_TORADIANS - ( CentralMeridian * DEG_TORADIANS )))
        * GU_FROMRADIANS * ScaleFactor));
  end;
end;

function TGnomonicPrj.XYToLL(iX, iY:Double; iIndex : Integer) : Boolean;
begin
  // ToDO: Not implemented yet
  Result := False;
end;



initialization
  RegisterClasses( [TAlbersEqualAreaConicPrj,
            TAzimuthal2EquidistantPrj,
            TAzimuthalEquidistantPrj,
            TBehrmannCylEqualAreaPrj,
            TBonnePrj,
            TCylEqualAreaPrj,
            TEquidistantPrj,
            TPetersPrj,
            TSinusoidalPrj
            ] );
end.

