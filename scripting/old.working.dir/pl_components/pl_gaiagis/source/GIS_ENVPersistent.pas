
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_ENVPersistent;



interface

uses
  LCLIntf, LCLType, LMessages,
  Forms, Classes, Graphics, Controls, SysUtils, StdCtrls,
  Printers, GIS_SysUtils, GIS_XML;

type
  {---------------------------- TEarthENVPersistent ------------------------------}
  //
TEarthENVPersistent = class(TEarthRoot)
  public
    procedure LoadEnvironment(Element : TGXML_Element); virtual; abstract;
    function  SaveEnvironment : TGXML_Element; virtual; abstract;
    procedure LoadEnvironmentFromFile(const sFileName: TFileName); virtual;
    procedure SaveEnvironmentToFile(const sFileName: TFileName); virtual;
  end;

implementation

{**
  @Param sFileName Filename to load the environment from
}
procedure TEarthENVPersistent.LoadEnvironmentFromFile( const sFileName: TFileName);
var
  Doc : TGXML_Document;
begin
  Doc := TGXML_Document.Create;
  try
    if Doc.LoadFromFile(sFilename) then
      LoadEnvironment(Doc.Document.ElementByName(ClassName, 0));
  finally
    Doc.Free;
  end;
end;


{**
  @Param sFilename Filename to save the environment to.
}
procedure TEarthENVPersistent.SaveEnvironmentToFile( const sFileName: TFileName);
var
  Doc : TGXML_Document;
begin
  Doc := TGXML_Document.Create;
  try
    Doc.Document.AddElement(ClassName, SaveEnvironment);
    Doc.SaveToFile(sFilename);
  finally
    Doc.Free;
  end;
end;

end.
 
