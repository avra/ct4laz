
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_EarthObjectsForMaps;

interface

uses
  LCLIntf, LCLType,
  Classes, Graphics, SysUtils, GIS_EarthBase, GIS_Classes, Dialogs,
  GIS_SysUtils, GIS_EarthObjects, SMap_Sternas, SMap_ADRI,
  SMap_DTED1, SMap_ADRG, SMap_GaiaCAD, FileUtil, lazfileutils, SBaseKernelMessages;

type
//---------- From TCustomMapObject -------------------
TGaiaCADMapObject = class(TCustomMapObject)
  protected
  public
   function  LoadMapFile:boolean;override;
  end;

TSternasMapObject = class(TCustomMapObject)
  protected
  public
   function  LoadMapFile:boolean;override;
  end;

TADRGMapObject = class(TCustomMapObject)
  protected
  public
   function LoadMapFile:boolean;override;
  end;

TADRIMapObject = class(TCustomMapObject)
  protected
  public
   function LoadMapFile:boolean;override;
  end;

TDTED1MapObject = class(TCustomMapObject)
  public
    function LoadMapFile:boolean; override;
  end;

//---------- From TCustomMapStoredObject -------------------
//
TGaiaCADMapStoredObject = class(TCustomMapStoredObject)
  protected
  public
   function  LoadMapFile:boolean;override;
  end;

TSternasMapStoredObject = class(TCustomMapStoredObject)
  protected
  public
   function  LoadMapFile:boolean;override;
  end;

TADRGMapStoredObject = class(TCustomMapStoredObject)
  protected
  public
   function LoadMapFile:boolean;override;
  end;

TADRIMapStoredObject = class(TCustomMapStoredObject)
  protected
  public
   function LoadMapFile:boolean;override;
  end;

TDTED1MapStoredObject = class(TCustomMapStoredObject)
  public
    function LoadMapFile:boolean; override;
  end;



implementation

//=============================== TGaiaCADMapObject ======================================
function  TGaiaCADMapObject.LoadMapFile:boolean;
var
  fInfo: xGenInfoRec;
  fGeo: xGeoRec;
  fPoints: xTestPointsRec;
  Srtm: TMemoryStream;
  x,y:real;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
    FTextureMap.TextureStream.Clear;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    Srtm.Read(fInfo ,sizeof(fInfo));
    if fInfo.FileID=GaiaCADMapIDFile then //check file type
     begin
      Srtm.Read(fGeo ,sizeof(fGeo));
      Srtm.Read(fPoints ,sizeof(fPoints));
      TextureData.TextureStream.LoadFromStream(Srtm);
      x:=fGeo.Start_Hor; //in Second
      y:=fGeo.Start_Ver; //in Second
      Centroid:=PointLL(EarthUnitsFrom(x ,euSecond),EarthUnitsFrom(y ,euSecond));
      EarthUnitsPerXPixel:=round(GU_MINUTE*fGeo.Factor_Hor/60) ;
      EarthUnitsPerYPixel:=round(-GU_MINUTE*fGeo.Factor_Ver/60) ;
      result:=true;
      end else
      begin
        SendMessageToIDE(self,FsMapFilename+' Is Unsuppored GaiaCAD Map File Format',MsgError);
      end;
finally
    Srtm.Free;
end;
end;

//=============================== TSternasMapObject ======================================
function  TSternasMapObject.LoadMapFile:boolean;
var
  fInfo: InfoRec;
  fGeo: GeograficalRec;
  fPoints: CheckpointsRec;
  Srtm: TMemoryStream;
  x,y:real;
  bitm:tbitmap;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
  FTextureMap.TextureStream.Clear;

   //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    bitm:=tbitmap.Create;
    bitm.LoadFromStream(Srtm);
    FTextureMap.TextureStream.Assign(bitm);
    
    Srtm.Position:=Srtm.Size-sizeof(fGeo);
    Srtm.Read(fGeo ,sizeof(fGeo));

    x:=geotoreal(fGeo.Start_Hor);
    y:=geotoreal(fGeo.Start_Ver);
    Centroid:=PointLL(EarthUnitsFrom(x ,euSecond),EarthUnitsFrom(y ,euSecond));
    EarthUnitsPerXPixel:=round(GU_MINUTE*fGeo.Factor_Hor/60) ;
    EarthUnitsPerYPixel:=round(-GU_MINUTE*fGeo.Factor_Ver/60) ;
    result:=true;
finally
  Srtm.Free;
  bitm.Free;
end;
end;

//================== TADRIMapObject ======================================
function TADRIMapObject.LoadMapFile:boolean;
var
  ADRIMap: TMapFileADRI;
  Srtm: TMemoryStream;
  x,y:real;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
    FTextureMap.TextureStream.Clear;
    ADRIMap := TMapFileADRI.Create;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    if ADRIMap.LoadFromStream(Srtm)=true then
     begin
      FTextureMap.TextureStream.Assign(ADRIMap.Image);
      result:=true;
      end else
      begin
        SendMessageToIDE(self,FsMapFilename+' Is Unsuppored ADRI file Format',MsgError);
      end;

   if result=true then
    begin
      y:=ADRIMap.FFrames[0,0].ImageGeo.subframe_nw_lat;
      x:=ADRIMap.FFrames[0,0].ImageGeo.subframe_nw_lon;
      Centroid:=PointLL(EarthUnitsFrom(x ,euDegree),EarthUnitsFrom(y ,euDegree));
      EarthUnitsPerXPixel:=round(GU_MINUTE*ADRIMap.FFrames[0,0].ImageGeo.frame_Y_interval/666.6) ;
      EarthUnitsPerYPixel:=round(GU_MINUTE*ADRIMap.FFrames[0,0].ImageGeo.frame_X_interval/666.6) ;
    end;
finally
 ADRIMap.Free;
 Srtm.Free;
end;
end;

//====================== TADRGMapObject ======================================
function  TADRGMapObject.LoadMapFile:boolean;
var
  ADRGMap: TMapFileADRG;
  Srtm: TMemoryStream;
  x,y:real;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
    FTextureMap.TextureStream.Clear;
    ADRGMap := TMapFileADRG.Create;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    if ADRGMap.LoadFromStream(Srtm)=true then
     begin
      FTextureMap.TextureStream.Assign(ADRGMap.Image);
      result:=true;
      end else
      begin
        SendMessageToIDE(self,FsMapFilename+' Is Unsuppored ADRG file Format',MsgError);
      end;

   if result=true then
    begin
      y:=ADRGMap.FFrames[0,0].ImageGeo.subframe_nw_lat;
      x:=ADRGMap.FFrames[0,0].ImageGeo.subframe_nw_lon;
      Centroid:=PointLL(EarthUnitsFrom(x ,euDegree),EarthUnitsFrom(y ,euDegree));
      EarthUnitsPerXPixel:=round(GU_MINUTE*ADRGMap.FFrames[0,0].ImageGeo.frame_Y_interval/666.6) ;
      EarthUnitsPerYPixel:=round(GU_MINUTE*ADRGMap.FFrames[0,0].ImageGeo.frame_X_interval/666.6) ;
    end;

finally
   Srtm.Free;
   ADRGMap.Free;
end;
end;

//================================ TDTED1MapObject ==============================================
function TDTED1MapObject.LoadMapFile:boolean;
var
  DTED1Map: TMapFileDTED1;
  Srtm: TMemoryStream;
begin
 result:=false;
 if FileExists(FsMapFilename)=false then exit;

 try
    FTextureMap.TextureStream.Clear;
    DTED1Map := TMapFileDTED1.Create;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    if DTED1Map.LoadFromStream(Srtm)=true then
      begin
      FTextureMap.TextureStream.Assign(DTED1Map.Image);
      result:=true;
      end else
      begin
       SendMessageToIDE(self,FsMapFilename+' Is Unsuppored DTED file Format',MsgError);  
      end;

 finally
    Srtm.Free;
    DTED1Map.Free;
  end;
end;



//*************************************************************************
//From TCustomMapStoredObject
//*************************************************************************

//=============================== TGaiaCADMapStoredObject ======================================
function  TGaiaCADMapStoredObject.LoadMapFile:boolean;
var
  fInfo: xGenInfoRec;
  fGeo: xGeoRec;
  fPoints: xTestPointsRec;
  Srtm: TMemoryStream;
  x,y:real;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
    FTextureMap.TextureStream.Clear;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    Srtm.Read(fInfo ,sizeof(fInfo));
    if fInfo.FileID=GaiaCADMapIDFile then //check file type
     begin
      Srtm.Read(fGeo ,sizeof(fGeo));
      Srtm.Read(fPoints ,sizeof(fPoints));
      TextureData.TextureStream.LoadFromStream(Srtm);
      x:=fGeo.Start_Hor; //in Second
      y:=fGeo.Start_Ver; //in Second
      Centroid:=PointLL(EarthUnitsFrom(x ,euSecond),EarthUnitsFrom(y ,euSecond));
      EarthUnitsPerXPixel:=round(GU_MINUTE*fGeo.Factor_Hor/60) ;
      EarthUnitsPerYPixel:=round(-GU_MINUTE*fGeo.Factor_Ver/60) ;
      result:=true;
      end else
      begin
        SendMessageToIDE(self,FsMapFilename+' Is Unsuppored GaiaCAD Map File Format',MsgError);
      end;
finally
    Srtm.Free;
end;
end;

//=============================== TSternasMapStoredObject ======================================
function  TSternasMapStoredObject.LoadMapFile:boolean;
var
  fInfo: InfoRec;
  fGeo: GeograficalRec;
  fPoints: CheckpointsRec;
  Srtm: TMemoryStream;
  x,y:real;
  bitm:tbitmap;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
  FTextureMap.TextureStream.Clear;

   //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    bitm:=tbitmap.Create;
    bitm.LoadFromStream(Srtm);
    FTextureMap.TextureStream.Assign(bitm);
    
    Srtm.Position:=Srtm.Size-sizeof(fGeo);
    Srtm.Read(fGeo ,sizeof(fGeo));

    x:=geotoreal(fGeo.Start_Hor);
    y:=geotoreal(fGeo.Start_Ver);
    Centroid:=PointLL(EarthUnitsFrom(x ,euSecond),EarthUnitsFrom(y ,euSecond));
    EarthUnitsPerXPixel:=round(GU_MINUTE*fGeo.Factor_Hor/60) ;
    EarthUnitsPerYPixel:=round(-GU_MINUTE*fGeo.Factor_Ver/60) ;
    result:=true;
finally
  Srtm.Free;
  bitm.Free;
end;
end;

//================== TADRIMapStoredObject ======================================
function TADRIMapStoredObject.LoadMapFile:boolean;
var
  ADRIMap: TMapFileADRI;
  Srtm: TMemoryStream;
  x,y:real;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
    FTextureMap.TextureStream.Clear;
    ADRIMap := TMapFileADRI.Create;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    if ADRIMap.LoadFromStream(Srtm)=true then
     begin
      FTextureMap.TextureStream.Assign(ADRIMap.Image);
      result:=true;
      end else
      begin
        SendMessageToIDE(self,FsMapFilename+' Is Unsuppored ADRI file Format',MsgError);
      end;

   if result=true then
    begin
      y:=ADRIMap.FFrames[0,0].ImageGeo.subframe_nw_lat;
      x:=ADRIMap.FFrames[0,0].ImageGeo.subframe_nw_lon;
      Centroid:=PointLL(EarthUnitsFrom(x ,euDegree),EarthUnitsFrom(y ,euDegree));
      EarthUnitsPerXPixel:=round(GU_MINUTE*ADRIMap.FFrames[0,0].ImageGeo.frame_Y_interval/666.6) ;
      EarthUnitsPerYPixel:=round(GU_MINUTE*ADRIMap.FFrames[0,0].ImageGeo.frame_X_interval/666.6) ;
    end;
finally
 ADRIMap.Free;
 Srtm.Free;
end;
end;

//====================== TADRGMapStoredObject ======================================
function  TADRGMapStoredObject.LoadMapFile:boolean;
var
  ADRGMap: TMapFileADRG;
  Srtm: TMemoryStream;
  x,y:real;
begin
result:=false;
if FileExists(FsMapFilename)=false then exit;

try
    FTextureMap.TextureStream.Clear;
    ADRGMap := TMapFileADRG.Create;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    if ADRGMap.LoadFromStream(Srtm)=true then
     begin
      FTextureMap.TextureStream.Assign(ADRGMap.Image);
      result:=true;
      end else
      begin
        SendMessageToIDE(self,FsMapFilename+' Is Unsuppored ADRG file Format',MsgError);
      end;

   if result=true then
    begin
      y:=ADRGMap.FFrames[0,0].ImageGeo.subframe_nw_lat;
      x:=ADRGMap.FFrames[0,0].ImageGeo.subframe_nw_lon;
      Centroid:=PointLL(EarthUnitsFrom(x ,euDegree),EarthUnitsFrom(y ,euDegree));
      EarthUnitsPerXPixel:=round(GU_MINUTE*ADRGMap.FFrames[0,0].ImageGeo.frame_Y_interval/666.6) ;
      EarthUnitsPerYPixel:=round(GU_MINUTE*ADRGMap.FFrames[0,0].ImageGeo.frame_X_interval/666.6) ;
    end;

finally
   Srtm.Free;
   ADRGMap.Free;
end;
end;

//================================ TDTED1MapStoredObject ==============================================
function TDTED1MapStoredObject.LoadMapFile:boolean;
var
  DTED1Map: TMapFileDTED1;
  Srtm: TMemoryStream;
begin
 result:=false;
 if FileExists(FsMapFilename)=false then exit;

 try
    FTextureMap.TextureStream.Clear;
    DTED1Map := TMapFileDTED1.Create;

    //load File to dummy stream
    Srtm := TMemoryStream.Create;
    Srtm.LoadFromFile(FsMapFilename);
    Srtm.Position:=0;

    if DTED1Map.LoadFromStream(Srtm)=true then
      begin
      FTextureMap.TextureStream.Assign(DTED1Map.Image);
      result:=true;
      end else
      begin
       SendMessageToIDE(self,FsMapFilename+' Is Unsuppored DTED file Format',MsgError);  
      end;

 finally
    Srtm.Free;
    DTED1Map.Free;
  end;
end;

//****************************************************************************
//****************************************************************************
initialization
  RegisterClasses([TGaiaCADMapObject,
                   TSternasMapObject,
                   TADRIMapObject,
                   TDTED1MapObject,
                   TADRGMapObject,
                   TGaiaCADMapStoredObject,
                   TSternasMapStoredObject,
                   TADRIMapStoredObject,
                   TDTED1MapStoredObject,
                   TADRGMapStoredObject]);

end.
