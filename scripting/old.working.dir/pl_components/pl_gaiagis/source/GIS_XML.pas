
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_XML;



interface

uses
  SysUtils, Classes, FileUtil, lazfileutils;

type
  TGXML_Element = class(TStringList)
  private
    function GetAttributeNames( Index : integer ) : string;
  public
    destructor Destroy; override;
    procedure AddTextElement(const Ident, aText: string);
    procedure AddBinaryElement(const Ident : string; var Buffer; BufSize: Integer);
    procedure AddElement( const Ident : string; Element : TGXML_Element );
    function  ElementByName( const Ident : string; iOccourance : integer ) : TGXML_Element;
    procedure AddAttribute( const Ident, Value : string );
    procedure AddBoolAttribute( const Ident : string; Value : Boolean );
    procedure AddFloatAttribute( const Ident : string; Value : Extended );
    procedure AddIntAttribute( const Ident : string; Value : integer );
    function  AttributeByName( const Ident, DefaultValue : string ) : string;
    function  BoolAttributeByName( const Ident : String; DefaultValue : Boolean ) : Boolean;
    function  FloatAttributeByName( const Ident : String; DefaultValue : Extended ) : Extended;
    function  IntAttributeByName( const Ident : String; DefaultValue : integer ) : integer;
    function  AsBinary( var Buffer; BufSize: Integer) : integer;
    property AttributeNames[Index : integer] : string read GetAttributeNames;
  end;

  TGXML_Document = class
  private
    FDocument : TGXML_Element;
    function  GetXMLString : string;
    procedure SetXMLString( const XML : string );
    procedure SetDocument( doc : TGXML_Element );
  public
    constructor Create;
    destructor Destroy; override;
    function  LoadFromFile(const sFilename: String):boolean;
    function  LoadFromStream(Stream: TStream):boolean;
    procedure SaveToFile(const sFilename: String);
    procedure SaveToStream(Stream: TStream);
    property  AsString : string read GetXMLstring write SetXMLstring;
    property  Document : TGXML_Element read FDocument write SetDocument;
  end;

function StrToXMLText(const value: String; attribute : Boolean): String;
function XMLTextToStr(const value: String): String;
implementation


function StrToXMLText(const value: String; attribute : Boolean): String;
var
  iStart, idx : integer;
begin
  Result := '';
  iStart := 1;
  idx := 1;
  while idx <= Length( value ) do
  begin
    if ( value[idx] in ['&','<','>'] ) or ( attribute and ( value[idx] in ['''','"',#13,#10] )) then
    begin
      if idx - iStart > 0 then
        Result := Result + Copy( value, iStart, idx - iStart );
      iStart := idx + 1;
      case value[idx] of
      '&': Result := Result + '&#38;';
      '<': Result := Result + '&#60;';
      '>': Result := Result + '&#62;';
      '''': Result := Result + '&#39;';
      '"': Result := Result + '&#34;';
      #13: Result := Result + '&#13;';
      #10: Result := Result + '&#10;';
      end;
    end;
    Inc( idx );
  end;

  if idx - iStart > 0 then
    Result := Result + Copy( value, iStart, idx - iStart );

  Result := Trim(Result);
end;


function XMLTextToStr(const value: String): String;
begin
  Result := Trim(Value);
  {Standard XML named entities - added 8 Aug 2001, John R}
  Result := StringReplace(Result, '&lt;',   '<',  [rfReplaceAll]);
  Result := StringReplace(Result, '&gt;',   '>',  [rfReplaceAll]);
  Result := StringReplace(Result, '&apos;', '''', [rfReplaceAll]);
  Result := StringReplace(Result, '&quot;', '"',  [rfReplaceAll]);

  Result := StringReplace(Result, '&#10;', #10, [rfReplaceAll]);
  Result := StringReplace(Result, '&#13;', #13, [rfReplaceAll]);
  Result := StringReplace(Result, '&#39;', '''', [rfReplaceAll]);
  Result := StringReplace(Result, '&#34;', '"', [rfReplaceAll]);
  Result := StringReplace(Result, '&#60;', '<', [rfReplaceAll]);
  Result := StringReplace(Result, '&#62;', '>', [rfReplaceAll]);

  {Do the ampersands last, and do them in such a way that the result of the ampersand
  replacement does not, itself, result in any additional replacements.  E.g.
  '&#38;amp;' becomes '&amp;' in the final result, NOT '&'. }
  Result := StringReplace(Result, '&#38;',  '&amp;', [rfReplaceAll]);
  Result := StringReplace(Result, '&amp;',  '&',     [rfReplaceAll]);
end;

//======================= TGXML_Element ==============================
destructor TGXML_Element.Destroy;
var
  idx : integer;
begin
  for idx := 0 to Count - 1 do
    if Objects[idx] <> Self then
      Objects[idx].Free;

  inherited;
end;

procedure TGXML_Element.AddAttribute(const Ident, Value: string);
begin
  AddObject( Trim( Ident ) + '="' + Trim( Value ) + '"', Self );
end;

procedure TGXML_Element.AddBoolAttribute( const Ident : string; Value : Boolean );
begin
  AddAttribute( Ident, 'FT'[Ord(Value)+1]);
end;

procedure TGXML_Element.AddFloatAttribute( const Ident : string; Value : Extended );
var
  SaveDecimalSeparator : Char;
begin
  SaveDecimalSeparator := DecimalSeparator;
  DecimalSeparator := '.';
  AddAttribute( Ident, FloatToStr( Value ));
  DecimalSeparator := SaveDecimalSeparator;
end;

procedure TGXML_Element.AddIntAttribute( const Ident : string; Value : integer );
begin
  AddAttribute( Ident, IntToStr( Value ));
end;

procedure TGXML_Element.AddTextElement( const Ident, aText: string );
var
  el : TGXML_Element;
begin
  el := TGXML_Element.Create;
  el.Add( Trim( aText ));
  AddElement( Trim( Ident ), el );
end;

procedure TGXML_Element.AddBinaryElement(const Ident: string; var Buffer;
  BufSize: Integer);
var
  el : TGXML_Element;
  sText : string;
  idx : integer;
begin
  SetLength( sText, BufSize * 2 );
  for idx := 0 to BufSize - 1 do
  begin
    sText[idx*2 + 1] := '0123456789ABCDEF'[(Ord(PChar(Buffer)[idx]) shr 4 ) and $0F + 1];
    sText[idx*2 + 2] := '0123456789ABCDEF'[Ord(PChar(Buffer)[idx]) and $0F + 1];
  end;
  el := TGXML_Element.Create;
  el.Add( sText );
  AddElement( Trim( Ident ), el );
end;

function TGXML_Element.AsBinary( var Buffer; BufSize: Integer) : integer;
begin
  Result := 0;
  while Result < BufSize do
  begin

    Inc( Result );
  end
end;

procedure TGXML_Element.AddElement(const Ident: string; Element: TGXML_Element);
begin
  AddObject( Trim( Ident ), Element );
end;

function TGXML_Element.ElementByName( const Ident : string; iOccourance : integer ) : TGXML_Element;
var
  idx : integer;
begin
  for idx := 0 to Count - 1 do
  begin
    Result := TGXML_Element(Objects[idx]);
    if ( Result <> nil ) and ( Result <> Self ) then
      if strings[idx] = Ident then
        if iOccourance = 0 then
          Exit
        else
          Dec( iOccourance );
  end;
  Result := nil;
end;

function TGXML_Element.GetAttributeNames( Index : integer ) : string;
var
  idx : integer;
begin
  for idx := 0 to Count -1 do
    if objects[idx] = self then
      if Index = 0 then
      begin
        Result := Names[idx];
        Exit;
      end;
  Result := '';
end;

function TGXML_Element.AttributeByName( const Ident, DefaultValue : string ) : string;
begin
  Result := Trim( Values[Ident] );
  if Result <> '' then
    Result := Copy( Result, 2, Length(Result) - 2 )
  else
    Result := DefaultValue;
end;

function TGXML_Element.BoolAttributeByName( const Ident : String; DefaultValue : Boolean ) : Boolean;
var
  tmp : string;
begin
  tmp := AttributeByName( Ident, '' );
  if tmp = 'F' then
    Result := False
  else
    if tmp = 'T' then
      Result := True
    else
      Result := DefaultValue;
end;

function TGXML_Element.FloatAttributeByName( const Ident : string; DefaultValue : Extended ) : Extended;
var
  sValue : string;
  SaveDecimalSeparator : Char;
begin
  sValue := AttributeByName( Ident, '' );
  if sValue = '' then
    Result := DefaultValue
  else
  begin
    SaveDecimalSeparator := DecimalSeparator;
    DecimalSeparator := '.';
    Result := StrToFloat( sValue );
    DecimalSeparator := SaveDecimalSeparator;
  end;
end;

function TGXML_Element.IntAttributeByName( const Ident : String; DefaultValue : integer ) : integer;
begin
  Result := StrToInt( AttributeByName( Ident, IntToStr( DefaultValue )));
end;

//================== TGXML_Document ========================================
constructor TGXML_Document.Create;
begin
  inherited;
  FDocument := TGXML_Element.Create;
end;

destructor TGXML_Document.Destroy;
begin
  FDocument.Free;

  inherited;
end;

function TGXML_Document.GetXMLString: string;

  function Attributes( el : TGXML_Element ) : string;
  var
    idx, iCount : integer;
  begin
    Result := ' ';
    iCount := 0;
    for idx := 0 to el.Count - 1 do
      if el.objects[idx] = el then
      begin
        Result := Result + el[idx] + ' ';
        Inc( iCount );
      end;

    if iCount = el.Count then
      Result := Result + '/';
  end;

  function WriteElement( el : TGXML_Element; iLevel : integer ) : string;
  var
    idx : integer;
    sAttr : string;
  begin
    Result := '';
    for idx := 0 to el.count - 1 do
      if el.objects[idx] <> el then
        if el.objects[idx] = nil then
          Result := Result + el[idx] + #13#10
        else
        begin
          sAttr := Attributes( TGXML_Element( el.objects[idx] ));
          Result := Result + StringOfChar( ' ', iLevel ) + '<' + Trim( el[idx] + sAttr ) + '>'#13#10;
          if sAttr[Length(sAttr)] <> '/' then
          begin
            Result := Result + WriteElement( TGXML_Element( el.objects[idx] ), iLevel + 1 );
            Result := Result + StringOfChar( ' ', iLevel ) + '</' + Trim( el[idx] ) + '>'#13#10;
          end;
        end;
  end;
begin
  Result := WriteElement( Document, 0 );
end;

function TGXML_Document.LoadFromFile(const sFilename: String):boolean;
var
  Stream: TFileStream;
begin
  result:=false;
  Stream := nil;
  if FileExistsUTF8(sFileName)   then
  try
    Stream := TFileStream.Create(sFileName, fmOpenRead or fmShareDenyWrite);
    LoadFromStream(Stream);
    result:=true;
  finally
    Stream.Free;
  end;
end;


function TGXML_Document.LoadFromStream(Stream: TStream):boolean;
var
  Count: integer;
  XML: String;
begin
  result:=false;
  if Stream=nil then exit;
  Stream.Position := 0;
  Count := Stream.Size;
  SetLength(XML, Count);
  if Count <> 0 then
    Stream.ReadBuffer(XML[1], Count);
  AsString := XML;
  result:=true;
end;

procedure TGXML_Document.SaveToFile(const sFilename: String);
var
  Stream: TFileStream;
begin
  Stream := TFileStream.Create(sFileName, fmCreate or fmShareDenyNone);
  try
    SaveToStream(Stream);
  finally
    Stream.Free;
  end;
end;


procedure TGXML_Document.SaveToStream(Stream: TStream);
var
  XML: String;
begin
  if Stream=nil then exit;
  XML := AsString;
  Stream.Write(XML[1], Length(XML));
end;

procedure TGXML_Document.SetDocument(doc: TGXML_Element);
begin
  if doc <> FDocument then
  begin
    FDocument.Free;
    FDocument := doc
  end;
end;

procedure TGXML_Document.SetXMLString(const XML: string);
var
  iOffset, iEOS : integer;

  //---------------------------------------------------------------------------
  procedure ParseBody( el : TGXML_Element );
  var
    sBody : string;
  begin
    sBody := '';
    while ( XML[iOffset] <> '<' ) and ( iOffset <= iEOS ) do
    begin
      sBody := sBody + XML[iOffset];
      Inc( iOffset );
    end;
    Inc( iOffset );

    sBody := Trim( sBody );
    if sBody <> '' then
      el.Add( sBody );
  end;

  //---------------------------------------------------------------------------
  procedure ParseAttributes( el : TGXML_Element; sAttr : string );
  var
    iPos : integer;
    sIdent, sValue : string;
  begin
    iPos := Pos( '="', sAttr );
    while iPos > 0 do
    begin
      sIdent := Copy( sAttr, 1, iPos - 1 );
      sAttr := Copy( sAttr, iPos + 2, Length( sAttr ));
      iPos := Pos( '"', sAttr );
      sValue := Copy( sAttr, 1, iPos - 1 );
      sAttr := Copy( sAttr, iPos + 1, Length( sAttr ));

      el.AddAttribute( sIdent, sValue );
      iPos := Pos( '="', sAttr );
    end;
  end;

  //---------------------------------------------------------------------------
  function ParseTagIdent( var sIdent : string ) : Boolean;
  begin
    sIdent := '';
    while ( not ( XML[iOffset] in ['>', ' '])) and ( iOffset <= iEOS ) do
    begin
      sIdent := sIdent + XML[iOffset];
      Inc( iOffset );
    end;
    Result := ( sIdent = '' ) or ( sIdent[1] = '/' );
    if Result then
      Inc( iOffset );
  end;

  //---------------------------------------------------------------------------
  function ParseTagAttributes( el : TGXML_Element ) : Boolean;
  var
    sAttr : string;
  begin
    sAttr := '';
    while ( not ( XML[iOffset] in ['>'] )) and ( iOffset <= iEOS) do
    begin
      sAttr := sAttr + XML[iOffset];
      Inc( iOffset );
    end;

    Result := False;
    sAttr := Trim( sAttr );
    Inc( iOffset );
    if sAttr <> '' then
    begin
      ParseAttributes( el, sAttr );
      Result := sAttr[Length(sAttr)] = '/';
    end;
  end;

  //---------------------------------------------------------------------------
  procedure ParseElement( el : TGXML_Element );
  var
    sIdent : string;
    sub : TGXML_Element;
  begin
    while iOffset <= iEOS do
    begin
      ParseBody( el );
      if ParseTagIdent( sIdent ) then
        Break;

      sub := TGXML_Element.Create;
      el.AddElement( sIdent, sub );

      if not ParseTagAttributes( sub ) then
        ParseElement( sub );
    end;
  end;

begin
  iOffset := 1;
  iEOS := Length( XML );
  FDocument.Free;
  FDocument := TGXML_Element.Create;
  ParseElement(FDocument);
end;

end.
