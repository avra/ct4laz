// Language definition

{$DEFINE ENGLISH}
{.$DEFINE FRENCH}

// Delphi version checking

{$IFDEF VER100} { Delphi 3.0 }
  {$DEFINE D3}
  {$DEFINE D3ABOVE}
{$ENDIF}

{$IFDEF VER110} { C++Builder 3.0 }
  {$DEFINE D3}
  {$DEFINE D3ABOVE}
{$ENDIF}

{$IFDEF VER120} { Delphi 4.0 }
  {$DEFINE D4}
  {$DEFINE D3ABOVE}
  {$DEFINE D4ABOVE}
{$ENDIF}

{$IFDEF VER125} { C++Builder 4.0 }
  {$DEFINE D4}
  {$DEFINE D3ABOVE}
  {$DEFINE D4ABOVE}
{$ENDIF}

{$IFDEF VER185} { Borland Delphi 5.0 }
  {$DEFINE D3ABOVE}
  {$DEFINE D4ABOVE}
  {$DEFINE D5ABOVE}
  {$DEFINE D5}
{$ENDIF}
