
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}



unit GIS_MMStream;



interface

uses LCLIntf, LCLType, LMessages, SysUtils, Classes, GIS_Resource, fileutil;

type

  TGFileMapStream = class(TFileStream)
  private
  protected
  public
    constructor Create(aFilename: string);
  end;

implementation

constructor TGFileMapStream.Create(aFilename: string);
begin
  inherited Create(aFilename,fmOpenRead or fmShareDenyNone);
end;

end.

