
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}



unit SGISProject;



interface
  uses
   Messages, SysUtils, Classes,forms, Controls,ClipBrd,
   SGISEngine,SBaseKernelMessages;

type

TProjectConnectionLine = procedure (Sender : TObject;
                                    const Conn:Integer;
                                    const InInt:integer;
                                    var OutInt:Integer;
                                    var OutStr:string) of object;

TApplicationType=(XGaiaCAD,XPlayer,XGUSS,XSreenSaver,XPresentation,XPlayerNP);

TGProject = class(TComponent)
  private
    fAppType:TApplicationType;
    fGISEngine:TGISEngine;
    faTitle:String;
    fAuthor:String;
    fDescription:String;
    fVersion:integer;
    //======== User Variables =================
    FUserBoolean:Boolean;
    FUserInteger:integer;
    FUserReal:Real;
    FUserString:String;
    FUserData:Pointer;
    //....... User Events ...................
    FOnxUserEvent1:TNotifyEvent;
    FOnxUserEvent2:TNotifyEvent;
    FOnxUserEvent3:TNotifyEvent;
  protected
    Function  GetAppForm:Tform;
    Function  GetBuildWithVersion:String;
  public
    constructor CreateEx(AOwner: TComponent; aGISEngine:TGISEngine;const aAppType:TApplicationType); virtual;
    destructor Destroy; override;
    Procedure  Clear;
    // .......... User Variables .............................
    //
    Property UserBoolean:Boolean  Read FUserBoolean Write FUserBoolean;
    Property UserInteger:integer Read FUserInteger Write FUserInteger;
    Property UserReal:Real Read FUserReal Write FUserReal;
    Property UserString:String  Read FUserString Write FUserString;
    Property UserData:Pointer Read FUserData Write FUserData;
    //Project Application Type   
    Property AppType:TApplicationType read fAppType;
    Property AppForm:Tform read GetAppForm;
    Property MainForm:Tform read GetAppForm;
    Property GISEngine:TGISEngine read fGISEngine;
  published
    Property   aTitle:String read faTitle write faTitle;
    Property   Author:String read fAuthor write fAuthor;
    Property   Description:String read fDescription write fDescription;
    Property   Version:integer read fVersion write fVersion;
    Property   BuildWithVersion:String read GetBuildWithVersion;

    property OnxUserEvent1:TNotifyEvent read FOnxUserEvent1 write FOnxUserEvent1;
    property OnxUserEvent2:TNotifyEvent read FOnxUserEvent2 write FOnxUserEvent2;
    property OnxUserEvent3:TNotifyEvent read FOnxUserEvent3 write FOnxUserEvent3;
  end;

var
 XProjectConnectionLine:TProjectConnectionLine;

implementation


//===================== TGProject =================================================== 

constructor TGProject.CreateEx(AOwner: TComponent; aGISEngine:TGISEngine;const aAppType:TApplicationType);
  begin
    inherited Create(AOwner);
    fAppType:=aAppType;
    fGISEngine:=aGISEngine;
    faTitle:='--None--';
    fAuthor:='--No Name--';
    fDescription:='--No Description--';
    fVersion:=100;
  end;

destructor TGProject.Destroy;
begin  
    inherited Destroy;
end;

Procedure TGProject.Clear;
 begin
    faTitle:='--None--';
    fAuthor:='--No Name--';
    fDescription:='--No Description--';
    fVersion:=100;

    FUserBoolean:=false;
    FUserInteger:=0;
    FUserReal:=0;
    FUserString:='';
    FUserData:=nil;
    FOnxUserEvent1:=nil;
    FOnxUserEvent2:=nil;
    FOnxUserEvent3:=nil;
 end;

Function  TGProject.GetAppForm:Tform;
  var  FF:TComponent;
  begin
   result:=nil;     

  if _xxMainForm<>nil then  //new code from 27-7-2008
  begin
    result:=_xxMainForm;
  end else                  //OLD Code
  begin
    ff:=nil;
    case AppType of
      xGaiaCAD:  ff:=FGISEngine.Earth.Parent;
      xPlayer:   ff:=application.MainForm;
      xPlayerNP: ff:=application.MainForm;
      xGUSS:     ff:=application.MainForm;
    end;

   if ff<>nil then
     if ff is TForm then
       result:=TForm(ff);
   end;

end;

Function  TGProject.GetBuildWithVersion:String;
var dInInt,dOutInt:integer;
       dOutStr:string;
begin
   result:='';
   if Assigned(XProjectConnectionLine) then
     begin
        XProjectConnectionLine(self,100,dInInt,dOutInt,dOutStr);
        result:=dOutStr;
     end;
 end;

initialization
 registerClasses([TGProject]);
end.
