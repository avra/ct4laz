
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_PresenterImage;



interface

uses LCLIntf, LCLType, LMessages, Classes, Graphics, SysUtils,Forms,
  GIS_EarthBase, GIS_Classes, GIS_EarthObjects, GIS_SysUtils, GIS_XML, GIS_Resource,
  GIS_Presenters,{CommonWinFunctions,}
  GIS_EarthStreams,ExtCtrls,GR32, FileUtil, lazfileutils;

type

TLinkImagePresenter = class(TCustomDrawPresenter)
  protected
    FImage: Tbitmap32;
    FsImageName: TFilename;
    FTransparent:boolean;
    FTransparentColor:TColor;
    procedure SetImageName(const sName: TFilename); virtual;
    procedure SetImage(aImage: Tbitmap32);virtual;
    Procedure SetTransparent(const Value:Boolean);
    procedure SetTransparentColor(const Value: TColor);
    procedure OnBitmap32Changed(Sender: TObject);
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    destructor Destroy; override;
    Function  GetFullImageName:String; virtual;
    procedure Drawshape; override;
    procedure Assign(Source: TPersistent); override;
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function  SaveEnvironment: TGXML_Element; override;
    procedure WriteProperties(Writer: TEarthStreamWriter); override;
    procedure ReadProperties(Reader: TEarthStreamReader); override;
    property  Image: Tbitmap32 read FImage write SetImage; 
  published
    property ImageName: TFilename read FsImageName write SetImageName;
    //property Transparent: Boolean read fTransparent write SetTransparent;
   // property TransparentColor: TColor read fTransparentColor  write SetTransparentColor;

    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;

  end;


TImagePresenter = class(TLinkImagePresenter)
  protected
    procedure SetImageName(const sName: TFilename); Override;
  published
    procedure WriteProperties(Writer: TEarthStreamWriter); override;
    procedure ReadProperties(Reader: TEarthStreamReader); override;
    property Image;
  end;



implementation

//===================== TLinkImagePresenter =========================

constructor TLinkImagePresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create(aPresenterStore, iID);
  Name := 'LinkImagePresenter';
  FImage:= Tbitmap32.Create;
  FImage.DrawMode:=dmBlend;
  Fimage.OnChange:=@OnBitmap32Changed;
  FTransparent:=false;
  FTransparentColor:=clwhite;
  SetImageName('');
end;

destructor TLinkImagePresenter.Destroy;
begin
  Fimage.OnChange:=nil;
  FImage.Free;
  inherited Destroy;
end;

procedure TLinkImagePresenter.Drawshape;
begin
 ParentEarth.EarthCanvas.gDrawBitMap32(FImage,
            RectD(FDrawCenterXY.X - FDrawOffectX,
                 FDrawCenterXY.Y - FDrawOffectY,
                 FDrawCenterXY.X + FDrawOffectX,
                 FDrawCenterXY.Y + FDrawOffectY));
end;


procedure TLinkImagePresenter.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TImagePresenter then
  begin
    Image.Assign(TImagePresenter(Source).Image);
    fShapeUnit := TImagePresenter(Source).ShapeUnit;
    fMaxPixelsSizeY := TImagePresenter(Source).MaxPixelsSizeY;
    fMaxPixelsSizeX := TImagePresenter(Source).MaxPixelsSizeX;
    FsImageName := TImagePresenter(Source).ImageName;
    fShapeSizeX := TImagePresenter(Source).ShapeSizeX;
    fShapeSizeY := TImagePresenter(Source).ShapeSizeY;
   // Transparent:= TImagePresenter(Source).Transparent;
   // TransparentColor:= TImagePresenter(Source).TransparentColor; 
    RedrawObject;
  end;
end;

procedure TLinkImagePresenter.LoadEnvironment(Element: TGXML_Element);
begin
  inherited LoadEnvironment(Element);
  if Element <> nil then
    with Element do
    begin
      ImageName := AttributeByName('ImageName', ImageName);
    end;
end;

function TLinkImagePresenter.SaveEnvironment: TGXML_Element;
begin
  Result := inherited SaveEnvironment;
  Result.AddAttribute('ImageName', ExtractRelativePath(ExpandFileNameUTF8(ParentEarth.DataDirectory), 
                                    ExpandFileNameUTF8(ImageName) ));
end;

procedure TLinkImagePresenter.WriteProperties(Writer: TearthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1); //Write Object Stream Version

  Writer.WriteBuffer( FShapeUnit, SizeOf( TEarthUnitTypes ) );
  Writer.WriteDouble( FMaxPixelsSizeX );
  Writer.WriteDouble( FMaxPixelsSizeY );
  Writer.WriteDouble( FShapeSizeX );
  Writer.WriteDouble( FShapeSizeY );
  Writer.WriteBoolean(fTransparent);
  Writer.WriteInteger(fTransparentColor);
  Writer.WriteShortString(ImageName);

end;

procedure TLinkImagePresenter.ReadProperties(Reader: TEarthStreamReader);
var sTmp: string;
    iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);
  iVer:=Reader.ReadInteger;

  Reader.ReadBuffer( FShapeUnit, SizeOf( TEarthUnitTypes ) );
  FMaxPixelsSizeX:=Reader.ReadInteger;
  FMaxPixelsSizeY:=Reader.ReadInteger;
  FShapeSizeX:=Reader.ReadInteger;
  FShapeSizeY:=Reader.ReadInteger;
  fTransparent:=Reader.ReadBoolean;
  fTransparentColor:=Reader.ReadInteger;
  sTmp := Reader.ReadShortString;
  if Length(sTmp) > 0 then ImageName := sTmp;

end;

procedure TLinkImagePresenter.SetImage(aImage: TBitmap32);
begin
  FsImageName := '';
  FImage.Assign(Image);
end;

Function  TLinkImagePresenter.GetFullImageName:String;
 begin
 Result:=FsImageName;
  //...... UnFix Name.............
 if Parent.Parent<>nil then
   begin
    if Parent.Parent is TEarthLayer then
      Result:=UnFixFilePath(FsImageName,TEarthLayer(Parent.Parent).FilePath);

    if Parent.Parent is TEarthObjectStore then
      if TEarthObjectStore(Parent.Parent).Parent <>nil then
       Result:=UnFixFilePath(FsImageName,TEarthObjectStore(Parent.Parent).Parent.FilePath);
   end;
 end;

procedure TLinkImagePresenter.SetImageName(const sName: TFilename);
begin
 if sName = '' then Exit;
 FsImageName:=sName;
 FsImageName:=GetFullImageName;
 FsImageName :=ParentEarth.ResolveFilename(FsImageName);

 if FileExistsUTF8(FsImageName)  =false then  Exit;
 if FsImageName <> '' then
   begin
     Application.ProcessMessages;
     FImage.LoadFromFile(FsImageName);
   end;

 //...... Fix Name.............
 if Parent.Parent<>nil then
   begin
    if Parent.Parent is TEarthLayer then
      FsImageName:=FixFilePath(FsImageName,TEarthLayer(Parent.Parent).FilePath);

    if Parent.Parent is TEarthObjectStore then
      if TEarthObjectStore(Parent.Parent).Parent <>nil then
       FsImageName:=FixFilePath(FsImageName,TEarthObjectStore(Parent.Parent).Parent.FilePath);
   end;
 //...............................

 RedrawObject;
end;


Procedure TLinkImagePresenter.SetTransparent(const Value:Boolean);
begin
 if FTransparent=Value then exit;
  FTransparent:=Value;
  RedrawObject;
end;

procedure TLinkImagePresenter.SetTransparentColor(const Value: TColor);
begin
 if FTransparentColor=Value then exit;

 FTransparentColor:=Value;
 RedrawObject;
end;

procedure TLinkImagePresenter.OnBitmap32Changed(Sender: TObject);
 begin
   RedrawObject;
 end;

//======================== TImagePresenter =================================================
procedure TImagePresenter.SetImageName(const sName: TFilename);
begin
if sName = '' then Exit;
 FsImageName :='';

  if FileExistsUTF8(sName)  =false then  Exit;
  if sName <> '' then FImage.LoadFromFile(sName);

 FsImageName :='';
 RedrawObject;
end;

procedure TImagePresenter.WriteProperties(Writer: TearthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1);   //Write Object Stream Version

  Writer.WriteBirmap32(Fimage);
end;

procedure TImagePresenter.ReadProperties(Reader: TEarthStreamReader);
var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);
  if giFileVersion>=TG_FILEVERSION1002 then
     iVer:= Reader.ReadInteger;            //Read Object Stream Version
  
  Reader.ReadBirmap32(Fimage); 
end;


//****************************************************************
//****************************************************************
initialization
  RegisterClasses( [TLinkImagePresenter, TImagePresenter ] );
end.
