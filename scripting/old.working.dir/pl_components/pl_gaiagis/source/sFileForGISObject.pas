
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}



unit sFileForGISObject;



interface
 uses
  LCLIntf, LCLType, SysUtils,dialogs, Classes,
  GIS_SysUtils,GIS_Classes,GIS_EarthStreams, FileUtil, lazfileutils;
const
  GaiaCADGISObjectFileFilter='GaiaCAD Objects File (*.GOF)|*.GOF';
  GaiaCADGISObjectFileExt='GOF';

Function Is_GISObjectFile(const sFilename : TFilename):boolean;

Function Save_GISObjectToStream(aObj:TEarthObject;astream:Tstream):boolean;
Function Save_GISObjectToFile(aObj:TEarthObject;const aFile:String):boolean;
Function Save_GISObjectsListToStream(aList:TList;astream:Tstream):longint;
Function Save_GISObjectsListToFile(aList:TList;const aFile:String):longint;
Function Save_GISObjectsToStream(aObjectStore:TEarthObjectStore;astream:Tstream):longint;
Function Save_GISObjectsToFile(aObjectStore:TEarthObjectStore;const aFile:String):longint;

Function Load_GISObjectsFromStream(aParent:TEarthObjectStore;astream:Tstream):longint;
Function Load_GISObjectsFromFile(aParent:TEarthObjectStore;const aFile:String):longint;

implementation
 type

 TGISObjectRec=record
        Whatis:string[70];
        FileID:integer;              //Use to intetify the file type
        Version:integer;            //Rec version
        TG_FILEVERSION:integer;
        ObjectsNum:longint;
        FutureString1:String[100];
        FutureString2:String[100];
        FutureBool1:boolean;
        FutureBool2:boolean;
        FutureInt1:longint;
        FutureInt2:longint;
     end;

const
     GISObjectFileCodeId=55963;  //Use to intetify the file type
     GISObjectFileVersion=1;     //Rec version

Procedure Clear_TGISObjectRec(var arec:TGISObjectRec);
 begin
     arec.Whatis:='GaiaCAD GISObject file';
     arec.FileID:=GISObjectFileCodeId;
     arec.Version:=GISObjectFileVersion;
     arec.TG_FILEVERSION:=TG_FILEVERSION;
     arec.ObjectsNum:=0;
     arec.FutureString1:='';
     arec.FutureString2:='';
     arec.FutureBool1:=false;
     arec.FutureBool2:=false;
     arec.FutureInt1:=-9999;
     arec.FutureInt2:=-9999;
 end;
//...............................................
Function Is_GISObjectFile(const sFilename : TFilename):boolean;
 begin
  result:=(SameText(ExtractFileExt(sFilename),'.'+GaiaCADGISObjectFileExt));
 end;

Function  Save_GISObjectToStream(aobj:TEarthObject;astream:Tstream):boolean;
 var rec:TGISObjectRec;
     ObjWS:TEarthStreamWriter;
 begin
  result:=false;

  if astream=nil then exit;
  if aobj=nil then exit;
  Clear_TGISObjectRec(rec);
  rec.ObjectsNum:=1;

  ObjWS:=TEarthStreamWriter.Create(astream);
  try
    ObjWS.WriteBuffer(rec,sizeof(TGISObjectRec));
    ObjWS.WriteShortString( aobj.ClassName );
    aobj.WriteProperties(ObjWS);
    result:=true;
  finally
    ObjWS.Free;
  end;
 end; 

Function  Save_GISObjectToFile(aObj:TEarthObject;const aFile:String):boolean;
var s:TFileStream;
 begin
  result:=false;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=Save_GISObjectToStream(aobj,s);
  finally
   s.Free;
  end;
 end;


 //==========================================================

Function Save_GISObjectsListToStream(aList:TList;astream:Tstream):longint;
 var rec:TGISObjectRec;
     ObjWS:TEarthStreamWriter;
     obj:TEarthObject;
     i:longint;
 begin
  result:=0;

  if astream=nil then exit;
  if aList=nil then exit;
  Clear_TGISObjectRec(rec);
  rec.ObjectsNum:=aList.Count;

  ObjWS:=TEarthStreamWriter.Create(astream);
  try
    ObjWS.WriteBuffer(rec,sizeof(TGISObjectRec));

    for I := 0 to aList.Count - 1 do
      begin
       obj:=nil;
       obj:=TEarthObject(alist.Items[i]);
       ObjWS.WriteShortString(obj.ClassName);
       obj.WriteProperties(ObjWS);
       inc(result);
      end;

  finally
    ObjWS.Free;
  end;
 end;

Function Save_GISObjectsListToFile(aList:TList;const aFile:String):longint;
var s:TFileStream;
 begin
  result:=0;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=Save_GISObjectsListToStream(aList,s);
  finally
   s.Free;
  end;
 end;

Function  Save_GISObjectsToStream(aObjectStore:TEarthObjectStore;astream:Tstream):longint;
 var rec:TGISObjectRec;
     ObjWS:TEarthStreamWriter;
     obj:TEarthObject;
     i:longint;
 begin
  result:=0;

  if astream=nil then exit;
  if aObjectStore=nil then exit;
  Clear_TGISObjectRec(rec);
  rec.ObjectsNum:=aObjectStore.Count;

  ObjWS:=TEarthStreamWriter.Create(astream);
  try
    ObjWS.WriteBuffer(rec,sizeof(TGISObjectRec));

    for I := 0 to aObjectStore.Count - 1 do
      begin
       obj:=nil;
       obj:=aObjectStore.EarthObject[i];
       ObjWS.WriteShortString(obj.ClassName );
       obj.WriteProperties(ObjWS);
       inc(result);
      end;

  finally
    ObjWS.Free;
  end;
 end;


Function  Save_GISObjectsToFile(aObjectStore:TEarthObjectStore;const aFile:String):longint;
var s:TFileStream;
 begin
  result:=0;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=Save_GISObjectsToStream(aObjectStore,s);
  finally
   s.Free;
  end;
 end;

Function  Load_GISObjectsFromStream(aParent:TEarthObjectStore;astream:Tstream):longint;
 var rec:TGISObjectRec;
     obj:TEarthObject;
     cs:TPersistentClass;
     ObjRS:TEarthStreamReader;
     i:longint;
 begin
 result:=0;

  if astream=nil then exit;
  if aParent=nil then exit;
  Clear_TGISObjectRec(rec);

  ObjRS:=TEarthStreamReader.Create(astream);
  try
    Cs:=nil;
    ObjRS.ReadBuffer(rec,sizeof(TGISObjectRec));
    if Rec.FileID=GISObjectFileCodeId then
     begin
      giFileVersion:=Rec.TG_FILEVERSION;

      for I := 0 to Rec.ObjectsNum - 1 do
        begin
         Cs:=GetClass(ObjRS.ReadShortString);    //read class
         if Cs<>nil then
          begin
           obj:=TEarthObjectClass(Cs).Create(aParent);   //Create class
           obj.ReadProperties(ObjRS );
           inc(result);
          end;
       end;
    end;
  finally
    ObjRS.Free;
  end;
 end;


Function  Load_GISObjectsFromFile(aParent:TEarthObjectStore;const aFile:String):longint;
var s:TFileStream;
 begin
  result:=0;
  if afile='' then exit;
  if FileExistsUTF8(afile)  =false then exit;
  try
   s:=TFileStream.Create(afile,fmOpenRead);
   result:=Load_GISObjectsFromStream(aparent,s);
  finally
   s.Free;
  end;
 end;
//...............................................
end.
