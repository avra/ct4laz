
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_EarthObjectData;



interface

uses
  LCLIntf, LCLType, LMessages,
  Forms, Classes, Graphics, Controls, SysUtils, StdCtrls,
  Printers, GIS_SysUtils;

type

 {------- Object Types ------------}
  TEarthObjectFlags = (ofTitle, ofID, ofHeight, of16Bit, ofCompressed,
    ofSingle, ofLongCount, ofZoomMinMax, ofClosed, ofGreatCircle, ofMultiRing,
    ofGUID);
  TEarthObjectFlagSet = set of TEarthObjectFlags;

  {---------------------------- TEarthObjectData ------------------------------}
TEarthObjectData = class(TEarthRoot)
  protected
    FiIndex : integer;
    FPresenterID : integer;
    FObjectMER : TMER;
    FUserObject : TObject;
    FValue : Double;
    procedure SetPresenterID(const Value : integer); virtual;
  public
    ObjectState : TEarthObjectStateSet;
    ObjectFlags : TEarthObjectFlagSet;
    function  GetObjectMER : TMER; virtual;
    function  ObjectInstanceSize : integer; virtual;
    procedure Assign(Source : TPersistent); override;

    property Index : integer read FiIndex write FiIndex;
    property ObjectMER : TMER read GetObjectMER write FObjectMER;
    property UserObject : TObject read FUserObject write FUserObject;
    property PresenterID : integer read FPresenterID write SetPresenterID;
  published
    property Value : Double read FValue write FValue;
  end;
implementation

procedure TEarthObjectData.Assign(Source : TPersistent);
begin
  if Source is TEarthObjectData then
  begin
    ObjectState := TEarthObjectData(Source).ObjectState;
    ObjectFlags := TEarthObjectData(Source).ObjectFlags;
    FUserObject := TEarthObjectData(Source).UserObject;
    FiIndex := TEarthObjectData(Source).Index;
    FObjectMER := TEarthObjectData(Source).ObjectMER;
    FPresenterID := TEarthObjectData(Source).PresenterID;
    FValue := TEarthObjectData(Source).Value;
  end;
end;

function TEarthObjectData.GetObjectMER: TMER;
begin
  Result := FObjectMER;
end;

function TEarthObjectData.ObjectInstanceSize: integer;
begin
  Result := Self.InstanceSize;
end;

procedure TEarthObjectData.SetPresenterID(const Value: integer);
begin
  FPresenterID := Value;
end;


end.
