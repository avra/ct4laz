unit GIS_Earth;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  GIS_EarthBase,
  GIS_EarthObjects,
  GIS_EarthObjectsForMaps,
  GIS_Projections,
  GIS_ProjectionsMore,
  GIS_Presenters,
  GIS_PresentersShapes,
  GIS_PresenterThematic,
  GIS_PresenterImage,
  GIS_MapperSHP,
  GIS_MapperMIF,
  GIS_MapperLYR,
  GIS_EarthObjPointsGenerators,
  GIS_EarthUtils;
type

TEarth = class(TEarthBase)
  private
  public
    function LayerIsValidFile(const sFilename: TFilename): boolean;
    function LayerNewFromAnyFile(const sFilename: TFilename): integer;
  end;

implementation

function TEarth.LayerIsValidFile(const sFilename: TFilename): boolean;
begin
 Result:=GIS_EarthUtils.IsLayerValidFile(sFilename);
end;

function TEarth.LayerNewFromAnyFile(const sFilename: TFilename): integer;
begin
 Result:=GIS_EarthUtils.LayerNewFromAnyFile(self.Layers,sFilename);
end;

end.

