
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit STVclsLibraryStore;



interface

uses LCLIntf, LCLType, LMessages,
  Messages, SysUtils, Classes;

 type
  
 TVclsLibraryStore = class(TComponent)
  private
    FLoadStage:integer;
       //0: Library is ready for load
       //1: library must ReLoad
  protected
    procedure DefineProperties(Filer: TFiler); override;
    Procedure OldClear;
    procedure WriteVcls(astream : TStream);
    procedure ReadVcls(astream : TStream);
    procedure DoReadFindComponentInstance(Reader: TReader; const aName: string; var Instance: Pointer);
  public
    constructor Create(AOwner: TComponent); Override;
    destructor Destroy; override;
    Procedure Clear;
    Procedure VclAdd(aVcl:TComponent);
    Procedure VclDelete(aVcl:TComponent);
    Function  VclFind(Const aVclName:string):TComponent;
    Procedure VclSetName(aVcl:TComponent);
    Property  LoadStage:integer Read FLoadStage;
  end;   

implementation
uses forms;

constructor TVclsLibraryStore.Create(AOwner: TComponent);
 begin
  inherited Create(AOwner);
  FLoadStage:=0;
 end;

destructor TVclsLibraryStore.Destroy;
begin   
  Clear;
  inherited Destroy;
end;


Procedure TVclsLibraryStore.Clear;
 begin
   DestroyComponents;
 end;

Procedure TVclsLibraryStore.OldClear;
var i:integer;
 begin
  for i:=ComponentCount-1 downto 0 do
    begin   
     Components[i].free;
    end;
 end;

Procedure TVclsLibraryStore.VclAdd(aVcl:TComponent);
 begin
    VclSetName(aVcl);
    InsertComponent(aVcl);
 end;

Procedure TVclsLibraryStore.VclDelete(aVcl:TComponent);
 begin
    RemoveComponent(aVcl);
    aVcl.Free;
 end;

Function TVclsLibraryStore.VclFind(Const aVclName:string):TComponent;
  begin
   result:=FindComponent(aVclName);
  end;
{
 Procedure TVclsLibraryStore.VclSetName(aVcl:TComponent);
 var ss1,ss2:string;
     checkObj:TComponent;
     CheckInt:integer;
 begin
 if  aVcl=nil then exit;

 CheckInt:=0;
 ss1:='';
 ss2:='';

 repeat
   inc(CheckInt);
   checkObj:=nil;

   if aVcl.Name='' then
    begin
     ss1:=aVcl.ClassName;
     System.Delete(ss1,1,1);
     aVcl.Name:=ss1;
     ss2:=inttostr(CheckInt);
     checkObj:=VclFind(ss1+ss2);
    end else
    begin
     ss1:=aVcl.Name;

     if CheckInt=1 then
        begin
         checkObj:=VclFind(ss1);
         ss2:='';
        end else
        begin
         ss2:=inttostr(CheckInt);
         checkObj:=VclFind(ss1+ss2);
        end;
    end;

 until Assigned(checkObj)=false;

 //... Check for exist manager fror Cosmos4D...
 ss1:=ss1+ss2;
 if SameText(ss1,'GLWindowsBitmapFont1') or
    SameText(ss1,'XGLGuiLayout1')
    then begin
      CheckInt:=Length(ss1);
      System.Delete(ss1,CheckInt,1);
      ss1:=ss1+'2';
    end;
 //............................................

 aVcl.Name:=ss1;

end;
 }

Procedure TVclsLibraryStore.VclSetName(aVcl:TComponent);
 var StrBase,StrInt:string;
     checkObj:TComponent;
     CheckInt:integer;
     XStop:boolean;
   //...........................................
   Procedure xFindNameToObj(xVcl:TComponent);
    begin
      if XVcl=nil then exit;

      repeat
       checkObj:=nil;
       checkObj:=xVcl.FindComponent(StrBase+StrInt);
       if checkObj<>nil then
         begin
            inc(CheckInt);
            StrInt:=IntToStr(CheckInt);
            XStop:=False;
         end  else
         begin
            XStop:=true;
         end;
       until XStop;
    end;
   //...........................................
 begin
 if  aVcl=nil then exit;

 CheckInt:=0;
 StrBase:='';
 StrInt:='';
 checkObj:=nil;
 XStop:=false;

 if aVcl.Name='' then
    begin
     StrBase:=aVcl.ClassName;
     System.Delete(StrBase,1,1);
     CheckInt:=1;
     StrInt:=IntToStr(CheckInt);
    end else
    begin
     StrBase:=aVcl.Name;
    end;

 //Check to Self
  xFindNameToObj(self);
 // Check to Parent
  if self.Owner<>nil then xFindNameToObj(self.Owner);
 // Check to Parent.Parent
  if self.Owner<>nil then
   if self.Owner.Owner<>nil
     then xFindNameToObj(self.Owner.Owner);


 //At Last
 aVcl.Name:=StrBase+StrInt;
end;


//*************************************************************************
procedure TVclsLibraryStore.DoReadFindComponentInstance(Reader: TReader; const aName: string; var Instance: Pointer);
 var L,i:integer;
     ss:string;
     com:TComponent;
begin
 //find VCL name
 ss:=aname;
 repeat
  L:=Length(ss);
  i:=pos('.',ss);
  if i>0 then  ss:=Copy(ss, i+1, l-i);
 until i<1;

  com:=self.FindComponent(ss);                                        //Scerch to self
  if (com=nil) and (Owner<>nil) then com:=Owner.FindComponent(ss);    //Scerch to Owner
  if com=nil then com:=Application.FindComponent(ss);                 //Scerch to Application

  if com<>nil then Instance:=com;

 // showmessage('DoReadReferenceName xxxxxxxx-->: Ok'+Name);
end;  
//*************************************************************************


procedure TVclsLibraryStore.DefineProperties(Filer: TFiler);
begin
   inherited DefineProperties(Filer);  
   Filer.DefineBinaryProperty('xVcls', @ReadVcls,@WriteVcls, (ComponentCount>0));
end;

procedure TVclsLibraryStore.WriteVcls(AStream: TStream);
var
  LWriter: TWriter;
  i: Integer;
begin

  i:=ComponentCount;
  astream.Write(i,sizeof(Integer));

  LWriter := TWriter.Create(astream, 4096);
  try
     for i:=0 to ComponentCount-1 do
      LWriter.WriteDescendent(Components[i],nil);
  finally
    LWriter.Free;
  end;
end;


procedure TVclsLibraryStore.ReadVcls(AStream: TStream);
var
  LReader: TReader;
  num,i:integer;
  com:TComponent;
begin

 astream.Read(num,sizeof(Integer));

 LReader := TReader.Create(astream, 4096);
    try

    if FLoadStage=0 then
     begin
         Clear;

         for i:=0 to num-1 do
           begin
            com:=lReader.ReadRootComponent(nil);
            self.InsertComponent(com);
           end;

        fLoadStage:=1;
     end else
      begin
      //   LReader.OnFindComponentInstance:=DoReadFindComponentInstance;
         for i:=0 to num-1 do lReader.ReadRootComponent(self.Components[i]);
         fLoadStage:=0;
      end;

 finally
    //  LReader.OnFindComponentInstance:=nil;
      LReader.Free;
 end;

end;

initialization
RegisterClass(TVclsLibraryStore);


end.
