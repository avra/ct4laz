
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit SGISEngine;

interface
uses
  LCLIntf, LCLType,
  SysUtils, Classes,forms, Controls,ClipBrd,
  GIS_EarthBase,SGISFilesLibrary, STVclsLibraryStore,GIS_SysUtils,
  GIS_Classes,TypInfo,SBaseKernelMessages, GIS_XML,GIS_EarthUtils,
  GR32, GR32_Layers, GR32_Image,GR32_LowLevel,
  sFileForGISObject,sFileForGISPresenter,sFileForGISLayer;

type

TGISEngine = class(TComponent)
  private
    FEarth:TEarthBase;
    FViewsLibrary:TEarthViewsLibrary;
    FLayersLibrary:TEarthLayersLibrary;
    FFilesLibrary:TEarthFilesLibrary;
    FVclsLibraryStore:TVclsLibraryStore;
    FGeoUnitsformat:TUnitsformat;
    FHeightUnits:THeightUnitTypes;
    //..... User Variables ............
    FUserInteger:integer;
    FUserBoolean:Boolean;
    FUserString:String;
    FUserData:Pointer;
    //Events From TCustomEarth
    FOnSelectedLayerChange:TNotifyEvent;
    FOnObjectSelectChange : TEarthObjectNotifyEvent;
    FOnPresenterStoreChange:TPresenterStoreNotifyEvent;
    FOnObjectStoreChange:TObjectStoreNotifyEvent ;
    //Events From TEarthBase
    FOnFileNotFound : TEarthFileNotFound;
    FOnReduceCacheMemory : TNotifyEvent;
    FOnPaintGrid : TNotifyEvent;
    FOnPaint : TNotifyEvent;
    FOnProgress : TEarthProgressEvent;
    FOnObjectRender : TEarthObjectRenderEvent;
    FOnObjectCreate : TEarthObjectNotifyEvent;
    FOnObjectFree : TEarthObjectNotifyEvent;
    FOnObjectClick : TEarthObjectNotifyEvent;
    FOnObjectSourceCreate : TEarthSourceNotifyEvent;
    FOnObjectSourceActiveChange : TEarthSourceNotifyEvent;
    FOnObjectSourceFree : TEarthSourceNotifyEvent;
    FOnSaveEnvironment : TEarthEnvironmentEvent;
    FOnLoadEnvironment : TEarthEnvironmentEvent;
    FOnLayersChange:TNotifyEvent;
    FOnDirectoryNotFound : TEarthDirectoryNotFound;
    FOnZoomed : TNotifyEvent;
    FOnRender : TNotifyEvent;
    FOnPanned : TNotifyEvent;
    FOnMouseEnter : TNotifyEvent;
    FOnMouseLeave : TNotifyEvent;
    FOnMouseMove: TMouseMoveEvent;
    FOnMouseDown: TMouseEvent;
    FOnMouseUp: TMouseEvent;
    //....... User Events ...................
    FOnUserEvent1:TNotifyEvent;
    FOnUserEvent2:TNotifyEvent;
    FOnUserEvent3:TNotifyEvent;
  protected
    Procedure SetEarth(aEarth:TEarthBase);
    // Procedures For Events
    procedure DoSelectedLayerChange(Sender: TObject);
    procedure DoObjectSelectChange(Sender :TCustomEarth; EarthObject : TEarthObject);
    procedure DoPresenterStoreChange(Sender :TObject; PresenterStore : TEarthPresenterStore);
    procedure DoObjectStoreChange(Sender :TObject; ObjectStore: TEarthObjectStore);
    procedure DoPanned(Sender: TObject);
    procedure DoRender(Sender: TObject);
    procedure DoZoomed(Sender: TObject);
    procedure DoReduceCacheMemory(Sender: TObject);
    procedure DoPaintGrid(Sender: TObject);
    procedure DoPaint(Sender: TObject);
    procedure DoObjectRender(Sender : TEarthBase; EarthObject : TEarthObject; State : TEarthObjectStateSet);
    procedure DoObjectClick (Sender :TCustomEarth; EarthObject : TEarthObject);
    procedure DoObjectCreate (Sender :TCustomEarth; EarthObject : TEarthObject);
    procedure DoObjectFree(Sender :TCustomEarth; EarthObject : TEarthObject);
    procedure DoObjectSourceCreate(Sender : TEarthBase; ObjectSource : TEarthObjectStore);
    procedure DoObjectSourceActiveChange(Sender : TEarthBase; ObjectSource : TEarthObjectStore);
    procedure DoObjectSourceFree(Sender : TEarthBase; ObjectSource : TEarthObjectStore);
    procedure DoProgress (Sender : TEarthBase; MsgType : TGISProgressMessage;
                                  const MsgText : string; var Abort : Boolean);
    procedure DoSaveEnvironment(Sender : TEarthBase; Document : TGXML_Document);
    procedure DoLoadEnvironment(Sender : TEarthBase; Document : TGXML_Document);
    procedure DoDirectoryNotFound(Sender : TEarthBase; var DirectoryName : string);
    procedure DoFileNotFound (Sender : TEarthBase; var Filename : TFilename; var TryFilename : Boolean);
    procedure DoLayersChange(Sender: TObject);
    procedure DoMouseDown(Sender: TObject; Button: TMouseButton;
                             Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure DoMouseEnter(Sender: TObject);
    procedure DoMouseLeave(Sender: TObject);
    procedure DoMouseMove(Sender: TObject; Shift: TShiftState;X, Y: Integer; Layer: TCustomLayer);
    procedure DoMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    Procedure Clear;
    procedure OnEarthFormKeyPress(var Key: Char);
    procedure OnEarthFormKeyDown(var Key: Word;Shift: TShiftState);
    procedure OnEarthFormMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);

    Function  GetGeoAsTextAtPoint(Const x, y: integer;WriteXY:boolean):string;
    Function  GetAltitudeAsText:string;
    Function  GetProjectionAsText:string;
    Function  GetSpheroidAsText:string;
    Function  GetAngle(const LL1,ll2:TPointLL):extended;

    Function  GetDistance(const LL1,ll2:TPointLL;aUnits:TEarthUnitTypes):extended;
    Function  GetAngleAsText(const LL1,ll2:TPointLL):String;
    Function  GetDistanceAsText(const LL1,ll2:TPointLL;aUnits:TEarthUnitTypes):String;

    Procedure MsgToIDE(Const Text:string; aType:integer=0);
    Procedure UpdateIDE;
    //............Managers Functions .......
    //
    Procedure ManagerAdd(aManager:TComponent);
    Procedure ManagerDelete(aManager:TComponent);
    Function  ManagerFind(Const aManagerName:string):TComponent;
    //............Layers Functions .......
    //
    Function  IsLayerFile(const sFilename : TFilename):boolean;
    Function  LayerSaveToFile( aLayer : TEarthLayer; const sFilename : TFilename ) : Boolean;
    Function  LayerSaveToStream( aLayer : TEarthLayer; aStream:Tstream ) : Boolean;
    Function  LayersSaveToFile( aLayerStore:TEarthLayerStore; const sFilename : TFilename ) :longint;
    Function  LayersSaveToStream( aLayerStore:TEarthLayerStore; aStream:Tstream ) :longint;
    Function  LayersListLoadFromFile(aList:TList;const aFile:string):longint;
    Function  LayersListLoadFromStream(aList:TList;Stream: TStream):longint; 
    Function  LayersLoadFromFile(aLayerStore:TEarthLayerStore;const aFile:string):longint;
    Function  LayersLoadFromStream(aLayerStore:TEarthLayerStore;Stream: TStream):longint;
    //............ Objects Functions ......
    //
    Function IsObjectFile(const sFilename : TFilename):boolean;
    Function ObjectSaveToStream(aObj:TEarthObject;astream:Tstream):boolean;
    Function ObjectSaveToFile(aObj:TEarthObject;const aFile:String):boolean;     
    Function ObjectsSaveToStream(aObjectStore:TEarthObjectStore;astream:Tstream):longint;
    Function ObjectsSaveToFile(aObjectStore:TEarthObjectStore;const aFile:String):longint;
    Function ObjectsListSaveToStream(aList:TList;astream:Tstream):longint;
    Function ObjectsListSaveToFile(aList:TList;const aFile:String):longint;
    Function ObjectsLoadFromStream(aParent:TEarthObjectStore;astream:Tstream):longint;
    Function ObjectsLoadFromFile(aParent:TEarthObjectStore;const aFile:String):longint;
    //............ Presenters Functions ......
    //
    Function IsPresenterFile(const sFilename : TFilename):boolean;
    Function PresenterSaveToStream(aPres:TEarthPresenter;astream:Tstream):boolean;
    Function PresenterSaveToFile(aPres:TEarthPresenter;const aFile:String):boolean;     
    Function PresentersSaveToStream(aParent:TEarthPresenterStore;astream:Tstream):longint;
    Function PresentersSaveToFile(aParent:TEarthPresenterStore;const aFile:String):longint;
    Function PresentersListSaveToStream(aList:TList;astream:Tstream):longint;
    Function PresentersListSaveToFile(aList:TList;const aFile:String):longint;
    Function PresentersLoadFromStream(aParent:TEarthPresenterStore;astream:Tstream):longint;
    Function PresentersLoadFromFile(aParent:TEarthPresenterStore;const aFile:String):longint;
    //..........................................
    //
    Property Earth:TEarthBase read FEarth write SetEarth;
    Property ViewsLibrary:TEarthViewsLibrary read FViewsLibrary;
    Property LayersLibrary:TEarthLayersLibrary read FLayersLibrary;
    Property FilesLibrary:TEarthFilesLibrary  read FFilesLibrary;
    Property VclsLibraryStore:TVclsLibraryStore   read FVclsLibraryStore;

    // .......... User Variables .............................
    //..
    Property UserInteger:integer Read FUserInteger Write FUserInteger;
    Property UserBoolean:Boolean  Read FUserBoolean Write FUserBoolean;
    Property UserString:String  Read FUserString Write FUserString;
    Property UserData:Pointer Read FUserData Write FUserData;    

published
    Property GeoUnitsformat:TUnitsformat Read FGeoUnitsformat Write FGeoUnitsformat;
    Property HeightUnits:THeightUnitTypes Read FHeightUnits Write FHeightUnits;
    //.......... Earth events
    //
    property OnSelectedLayerChange:TNotifyEvent read FOnSelectedLayerChange write FOnSelectedLayerChange;
    property OnObjectSelectChange : TEarthObjectNotifyEvent read FOnObjectSelectChange write FOnObjectSelectChange;
    property OnPresenterStoreChange:TPresenterStoreNotifyEvent read FOnPresenterStoreChange write FOnPresenterStoreChange;
    property OnObjectStoreChange:TObjectStoreNotifyEvent read FOnObjectStoreChange write FOnObjectStoreChange;
    property OnPanned : TNotifyEvent read FOnPanned write FOnPanned;
    property OnRender : TNotifyEvent read FOnRender write FOnRender;
    property OnZoomed : TNotifyEvent read FOnZoomed write FOnZoomed;
    property OnReduceCacheMemory : TNotifyEvent read FOnReduceCacheMemory write FOnReduceCacheMemory;
    property OnPaintGrid : TNotifyEvent read FOnPaintGrid write FOnPaintGrid;
    property OnPaint : TNotifyEvent read FOnPaint write FOnPaint;
    property OnObjectRender : TEarthObjectRenderEvent read FOnObjectRender write FOnObjectRender;
    property OnObjectClick : TEarthObjectNotifyEvent read FOnObjectClick write FOnObjectClick;
    property OnObjectCreate : TEarthObjectNotifyEvent read FOnObjectCreate write FOnObjectCreate;
    property OnObjectFree : TEarthObjectNotifyEvent read FOnObjectFree write FOnObjectFree;
    property OnObjectSourceCreate : TEarthSourceNotifyEvent read FOnObjectSourceCreate write FOnObjectSourceCreate;
    property OnObjectSourceActiveChange : TEarthSourceNotifyEvent read FOnObjectSourceActiveChange write FOnObjectSourceActiveChange;
    property OnObjectSourceFree : TEarthSourceNotifyEvent read FOnObjectSourceFree write FOnObjectSourceFree;
    property OnProgress : TEarthProgressEvent read FOnProgress write FOnProgress;
    property OnSaveEnvironment : TEarthEnvironmentEvent read FOnSaveEnvironment write FOnSaveEnvironment;
    property OnLoadEnvironment : TEarthEnvironmentEvent read FOnLoadEnvironment write FOnLoadEnvironment;
    property OnDirectoryNotFound : TEarthDirectoryNotFound read FOnDirectoryNotFound write FOnDirectoryNotFound;
    property OnFileNotFound : TEarthFileNotFound read FOnFileNotFound write FOnFileNotFound;
    property OnLayersChange:TNotifyEvent read FOnLayersChange write FOnLayersChange;
    property OnMouseEnter: TNotifyEvent read FOnMouseEnter write FOnMouseEnter;
    property OnMouseLeave: TNotifyEvent read FOnMouseLeave write FOnMouseLeave;
    property OnMouseMove: TMouseMoveEvent read FOnMouseMove write FOnMouseMove;
    property OnMouseDown: TMouseEvent read FOnMouseDown write FOnMouseDown;
    property OnMouseUp: TMouseEvent read FOnMouseUp write FOnMouseUp;

    //............ User Events ....................
    //
    property OnUserEvent1:TNotifyEvent read FOnUserEvent1 write FOnUserEvent1;
    property OnUserEvent2:TNotifyEvent read FOnUserEvent2 write FOnUserEvent2;
    property OnUserEvent3:TNotifyEvent read FOnUserEvent3 write FOnUserEvent3;
 end;

var
 XOnUpdateManagersExplorer:TNotifyEvent;
 XOnUpdateIDE:TNotifyEvent;

implementation

//=========================== TGISEngine =========================
constructor TGISEngine.Create(AOwner: TComponent);
  begin

   inherited Create(AOwner);
   
   FGeoUnitsformat:=UF_DDMMSS;
   FHeightUnits:=huEarthUnit;

   FEarth:=Nil;
   FViewsLibrary:=TEarthViewsLibrary.Create(self);
   FViewsLibrary.Name:='ViewsLibrary';

   FLayersLibrary:=TEarthLayersLibrary.Create(self);
   FLayersLibrary.Name:='LayersLibrary';

   FFilesLibrary:=TEarthFilesLibrary.Create(self);
   FFilesLibrary.Name:='FilesLibrary';

   FVclsLibraryStore:=TVclsLibraryStore.Create(self);
   FVclsLibraryStore.Name:='VclsLibraryStore';

   FUserInteger:=0;
   FUserBoolean:=false;
   FUserString:='';
   FUserData:=nil;
  end;

destructor TGISEngine.Destroy;
begin
   FOnUserEvent1:=Nil;
   FOnUserEvent2:=Nil;
   FOnUserEvent3:=Nil;
   FEarth:=Nil;
   FViewsLibrary.Free;
   FLayersLibrary.Free;
   FFilesLibrary.Free;
   FVclsLibraryStore.Free;
   inherited Destroy;
end;

Procedure TGISEngine.Clear;
 begin
   FEarth.Clear;
   FVclsLibraryStore.Clear;
   FViewsLibrary.Clear;
   FLayersLibrary.Clear;
   FFilesLibrary.Clear;  
 end;

Procedure TGISEngine.SetEarth(aEarth:TEarthBase);
  begin
    FEarth:=aEarth;
    if FEarth=nil then exit;

    FEarth.OnSelectedLayerChange:=@DoSelectedLayerChange;
    FEarth.OnObjectSelectChange := @DoObjectSelectChange ;
    FEarth.OnPresenterStoreChange:=@DoPresenterStoreChange;
    FEarth.OnObjectStoreChange:=@DoObjectStoreChange;
    FEarth.OnPanned := @DoPanned ;
    FEarth.OnRender := @DoRender;
    FEarth.OnZoomed := @DoZoomed ;
    FEarth.OnReduceCacheMemory := @DoReduceCacheMemory;
    FEarth.OnPaintGrid :=@DoPaintGrid;
    FEarth.OnPaint :=@DoPaint;
    FEarth.OnObjectRender :=@DoObjectRender;
    FEarth.OnObjectClick :=@DoObjectClick;
    FEarth.OnObjectCreate :=@DoObjectCreate;
    FEarth.OnObjectFree :=@DoObjectFree;
    FEarth.OnObjectSourceCreate :=@DoObjectSourceCreate;
    FEarth.OnObjectSourceActiveChange :=@DoObjectSourceActiveChange;
    FEarth.OnObjectSourceFree :=@DoObjectSourceFree;
    FEarth.OnProgress :=@DoProgress;
    FEarth.OnSaveEnvironment :=@DoSaveEnvironment;
    FEarth.OnLoadEnvironment :=@DoLoadEnvironment;
    FEarth.OnDirectoryNotFound :=@DoDirectoryNotFound;
    FEarth.OnFileNotFound :=@DoFileNotFound;
    FEarth.OnLayersChange:=@DoLayersChange;
    FEarth.OnMouseDown:=@DoMouseDown;
    FEarth.OnMouseEnter:=@DoMouseEnter;
    FEarth.OnMouseLeave:=@DoMouseLeave;
    FEarth.OnMouseMove:=@DoMouseMove;
    FEarth.OnMouseUp:=@DoMouseUp;
  end;
//............ Objects Functions ......
Function TGISEngine.IsObjectFile(const sFilename : TFilename):boolean;
  begin
   Result:=Is_GISObjectFile(sFilename);
 end;

Function TGISEngine.ObjectSaveToStream(aObj:TEarthObject;astream:Tstream):boolean;
  begin
   Result:=Save_GISObjectToStream(aObj,astream);
  end;
  
Function TGISEngine.ObjectSaveToFile(aObj:TEarthObject;const aFile:String):boolean;
  begin
   Result:=Save_GISObjectToFile(aObj,aFile);
  end;     

Function TGISEngine.ObjectsLoadFromStream(aParent:TEarthObjectStore;astream:Tstream):longint;
 begin
   result:=Load_GISObjectsFromStream(aParent,astream);
 end;

Function TGISEngine.ObjectsLoadFromFile(aParent:TEarthObjectStore;const aFile:String):longint;
begin
   result:=Load_GISObjectsFromFile(aParent,aFile);
 end;

Function TGISEngine.ObjectsSaveToStream(aObjectStore:TEarthObjectStore;astream:Tstream):longint;
begin
   result:=Save_GISObjectsToStream(aObjectStore,astream);
 end;

Function TGISEngine.ObjectsSaveToFile(aObjectStore:TEarthObjectStore;const aFile:String):longint;
begin
   result:=Save_GISObjectsToFile(aObjectStore,aFile);
 end;

Function TGISEngine.ObjectsListSaveToStream(aList:TList;astream:Tstream):longint;
begin
   result:=Save_GISObjectsListToStream(aList,astream);
 end;

Function TGISEngine.ObjectsListSaveToFile(aList:TList;const aFile:String):longint;
begin
   result:=Save_GISObjectsListToFile(aList,aFile);
 end;

//............ Presenters Functions .....................
Function TGISEngine.IsPresenterFile(const sFilename : TFilename):boolean;
  begin
   Result:=Is_GISPresenterFile(sFilename);
 end;

Function TGISEngine.PresenterSaveToStream(aPres:TEarthPresenter;astream:Tstream):boolean;
  begin
   Result:=Save_GISPresenterToStream(aPres,astream);
  end;

Function TGISEngine.PresenterSaveToFile(aPres:TEarthPresenter;const aFile:String):boolean;
  begin
   Result:=Save_GISPresenterToFile(aPres,aFile);
  end;  

Function TGISEngine.PresentersLoadFromStream(aParent:TEarthPresenterStore;astream:Tstream):longint;
  begin
   Result:=Load_GISPresentersFromStream(aParent,astream);
  end;

Function TGISEngine.PresentersLoadFromFile(aParent:TEarthPresenterStore;const aFile:String):longint;
  begin
   Result:=Load_GISPresentersFromFile(aParent,aFile);
  end;

Function TGISEngine.PresentersSaveToStream(aParent:TEarthPresenterStore;astream:Tstream):longint;
  begin
   Result:=Save_GISPresentersToStream(aParent,astream);
  end;

Function TGISEngine.PresentersSaveToFile(aParent:TEarthPresenterStore;const aFile:String):longint;
  begin
   Result:=Save_GISPresentersToFile(aParent,aFile);
  end;

Function TGISEngine.PresentersListSaveToStream(aList:TList;astream:Tstream):longint;
  begin
   Result:=Save_GISPresentersListToStream(aList,astream);
  end;

Function TGISEngine.PresentersListSaveToFile(aList:TList;const aFile:String):longint;
  begin
   Result:=Save_GISPresentersListToFile(aList,aFile);
  end;


//.............. Layers Function..........................
Function  TGISEngine.IsLayerFile(const sFilename : TFilename):boolean;
 begin
   Result:=Is_GISLayerFile(sFilename);
 end;  

Function  TGISEngine.LayersLoadFromFile(aLayerStore:TEarthLayerStore;const aFile:string):longint;
  begin
    result:=Load_GISLayersFromFile(aLayerStore,aFile);
 end;

Function  TGISEngine.LayersLoadFromStream(aLayerStore:TEarthLayerStore;Stream: TStream):longint;
 begin
  result:=Load_GISLayersFromStream(aLayerStore,Stream);
 end;

Function  TGISEngine.LayerSaveToFile( aLayer : TEarthLayer; const sFilename : TFilename ) : Boolean;
 begin
   result:=Save_GISLayerTofile(aLayer,sFilename);
 end;

Function  TGISEngine.LayerSaveToStream( aLayer : TEarthLayer; aStream:Tstream ) : Boolean;
 begin
   result:=Save_GISLayerToStream(aLayer,aStream);
 end;

Function  TGISEngine.LayersSaveToFile( aLayerStore:TEarthLayerStore; const sFilename : TFilename ) :longint;
  begin
   result:=Save_GISLayersToFile(aLayerStore,sFilename);
 end;

Function  TGISEngine.LayersSaveToStream( aLayerStore:TEarthLayerStore; aStream:Tstream ) :longint;
  begin
   result:=Save_GISLayersToStream(aLayerStore,aStream);
  end;

Function  TGISEngine.LayersListLoadFromFile(aList:TList;const aFile:string):longint;
  begin
   result:=Save_GISLayersListToFile(aList,aFile);
  end;

Function  TGISEngine.LayersListLoadFromStream(aList:TList;Stream: TStream):longint;
  begin
   result:=Save_GISLayersListToStream(aList,Stream);
  end;
//.................................................................................



procedure TGISEngine.OnEarthFormMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if WheelDelta<0 then
      fEarth.ZoomOut else
      fEarth.ZoomIn;
end;

procedure TGISEngine.OnEarthFormKeyPress(var Key: Char);
begin
 case Key of
 '8':fEarth.RotateUP;
 '2':fEarth.RotateDOWN;
 '6':fEarth.RotateRIGHT;
 '4':fEarth.RotateLEFT;
 '+':fEarth.ZoomIN;
 '-':fEarth.ZoomOut;
 end;
end;

procedure TGISEngine.OnEarthFormKeyDown(var Key: Word;Shift: TShiftState);
begin
 case Key of
   VK_UP:fEarth.RotateUP;
   VK_DOWN:fEarth.RotateDOWN;
   VK_RIGHT:fEarth.RotateRIGHT;
   VK_LEFT:fEarth.RotateLEFT;
   VK_HOME:fEarth.ZoomToMin;
   VK_END:fEarth.ZoomToMax;
 end;
end;

Function TGISEngine.GetGeoAsTextAtPoint(Const x, y: integer;WriteXY:boolean):string;
var
  sTmp: string;
  pt:TPointLL;
  MouseObj: TEarthObject;
begin
  result :='';

  with fEarth do
  begin
    mouseXYToLL(X,Y, pt);

    with pt do
      sTmp := ' Long: '+EarthUnitsToStr(iLongX,GetUnitsformatForLong(fGeoUnitsformat)) +', Lat: '+
              EarthUnitsToStr(iLatY,GetUnitsformatForLat(fGeoUnitsformat));

    MouseObj := Layers.ObjectAtXY(X, Y);
    if MouseObj <> nil then
      sTmp := sTmp + '  "' + MouseObj.Title+'"';

    if WriteXY then
       result := Format(' Mouse Pos: X='+inttostr(x)+' Y='+Inttostr(Y)+'  %s', [sTmp]) else
       result := Format(' %s', [sTmp]);
  end;
end;

Function TGISEngine.GetAltitudeAsText:string;
 begin
   Result:= 'Alt: '+
         FloatToStrF(fEarth.HeightUnitsTo(fEarth.Projection.Altitude,fHeightUnits),
                     ffFixed,12,3)+' '+HUnitsToStr(fHeightUnits);
 end;

Function TGISEngine.GetProjectionAsText:string;
 begin
   Result:='Projection: ' + fEarth.Projection.ProjectionClass;
 end;

Function TGISEngine.GetSpheroidAsText:string;
 begin
   Result:='Spheroid: '+GetEnumName(TypeInfo(TSpheroid),Ord(Earth.Projection.Spheroid));
 end;

Function TGISEngine.GetAngle(const LL1,ll2:TPointLL):extended;
 begin
   Result :=AnglePointToPoint(LL1,LL2);
 end;

Function TGISEngine.GetDistance(const LL1,ll2:TPointLL;aUnits:TEarthUnitTypes):extended;
 begin
   Result:=EarthUnitsTo(EllipsoidDistanceLLToLL(LL1,LL2,fEarth.Projection.Spheroid),aUnits);
 end;

Function TGISEngine.GetAngleAsText(const LL1,ll2:TPointLL):String;
 var ex:extended;
 begin
   ex :=AnglePointToPoint(LL1,LL2);
   Result:=FloatToStrF(ex,ffFixed,10,3)+'�';
 end;

Function TGISEngine.GetDistanceAsText(const LL1,ll2:TPointLL;aUnits:TEarthUnitTypes):String;
var ex:extended;
 begin
   ex :=EarthUnitsTo(EllipsoidDistanceLLToLL(LL1,LL2,fEarth.Projection.Spheroid),aUnits);
   Result:=FloatToStrF(ex,ffFixed,10,3)+' '+UnitsToStr(aUnits);
 end;

//========== Managers Function =======================
Procedure TGISEngine.ManagerAdd(aManager:TComponent);
 begin
  if FVclsLibraryStore=nil then exit;
  if aManager=nil then exit;

  FVclsLibraryStore.VclAdd(aManager);

  SendMessageToIDE(self,'Add New World Manager With name << '+aManager.Name+' >>');
  if Assigned(XOnUpdateManagersExplorer) then XOnUpdateManagersExplorer(Self);
 end;

Procedure TGISEngine.ManagerDelete(aManager:TComponent);
 var ss:string;
 begin
  if FVclsLibraryStore=nil then exit;
  if aManager=nil then exit;
  ss:=aManager.Name;
 // MessageOnBeforeDeleteObject(aManager);    // 7777
  SendMessageToIDE(self,'Delete World Manager With name << '+ss+' >>');

  FVclsLibraryStore.VclDelete(aManager);
  if Assigned(XOnUpdateManagersExplorer) then XOnUpdateManagersExplorer(Self);
 end;

Function TGISEngine.ManagerFind(Const aManagerName:string):TComponent;
 begin
 result:=nil;
  if FVclsLibraryStore=nil then exit;
  if aManagerName='' then exit;
  result:=FVclsLibraryStore.VclFind(aManagerName);
 end;

 //======================= Procedure For Events ==============================
 Procedure TGISEngine.UpdateIDE;
   begin
      if Assigned(xonUpdateIDE) then xonUpdateIDE(self);
   end;
   
Procedure TGISEngine.MsgToIDE(Const Text:string; aType:integer=0);
 begin
   SendMessageToIDE(self,Text,aType);
 end;

 procedure TGISEngine.DoSelectedLayerChange(Sender: TObject);
   begin
      if Assigned(onSelectedLayerChange) then onSelectedLayerChange(sender);
   end;

 procedure TGISEngine.DoObjectSelectChange(Sender :TCustomEarth; EarthObject : TEarthObject);
   begin
      if Assigned(OnObjectSelectChange) then OnObjectSelectChange(Sender,EarthObject);
   end;

 procedure TGISEngine.DoPresenterStoreChange(Sender :TObject; PresenterStore : TEarthPresenterStore);
   begin
      if Assigned(OnPresenterStoreChange) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoObjectStoreChange(Sender :TObject; ObjectStore: TEarthObjectStore);
   begin
      if Assigned(OnObjectStoreChange) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoPanned(Sender: TObject);
   begin
      if Assigned(OnPanned) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoRender(Sender: TObject);
   begin
      if Assigned(OnRender) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoZoomed(Sender: TObject);
   begin
      if Assigned(OnZoomed) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoReduceCacheMemory(Sender: TObject);
   begin
      if Assigned(OnReduceCacheMemory) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoPaintGrid(Sender: TObject);
   begin
      if Assigned(OnPaintGrid) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoPaint(Sender: TObject);
   begin
      if Assigned(OnPaint) then OnMouseLeave(Self);
   end;

 procedure TGISEngine.DoObjectRender(Sender : TEarthBase; EarthObject : TEarthObject; State : TEarthObjectStateSet);
   begin
      if Assigned(OnObjectRender) then OnObjectRender(Sender,EarthObject,State);
   end;

 procedure TGISEngine.DoObjectClick (Sender :TCustomEarth; EarthObject : TEarthObject);
   begin
      if Assigned(OnObjectClick) then OnObjectClick (Sender,EarthObject);
   end;

 procedure TGISEngine.DoObjectCreate (Sender :TCustomEarth; EarthObject : TEarthObject);
   begin
      if Assigned(OnObjectCreate) then OnObjectCreate (Sender ,EarthObject);
   end;

 procedure TGISEngine.DoObjectFree(Sender :TCustomEarth; EarthObject : TEarthObject);
   begin
      if Assigned(OnObjectFree) then OnObjectFree(Sender,EarthObject);
   end;

 procedure TGISEngine.DoObjectSourceCreate(Sender : TEarthBase; ObjectSource : TEarthObjectStore);
   begin
      if Assigned(OnObjectSourceCreate) then OnObjectSourceCreate(Sender,ObjectSource);
   end;

 procedure TGISEngine.DoObjectSourceActiveChange(Sender : TEarthBase; ObjectSource : TEarthObjectStore);
   begin
      if Assigned(OnObjectSourceActiveChange) then OnObjectSourceActiveChange(Sender,ObjectSource);
   end;

 procedure TGISEngine.DoObjectSourceFree(Sender : TEarthBase; ObjectSource : TEarthObjectStore);
   begin
      if Assigned(OnObjectSourceFree) then OnObjectSourceFree(Sender,ObjectSource);
   end;

 procedure TGISEngine.DoProgress(Sender : TEarthBase; MsgType : TGISProgressMessage;
                                  const MsgText : string; var Abort : Boolean);
   begin
      if Assigned(OnProgress) then OnProgress(Sender,MsgType,MsgText,Abort);
   end;

 procedure TGISEngine.DoSaveEnvironment(Sender : TEarthBase; Document : TGXML_Document);
   begin
      if Assigned(OnSaveEnvironment) then OnSaveEnvironment(Sender,Document);
   end;

 procedure TGISEngine.DoLoadEnvironment(Sender : TEarthBase; Document : TGXML_Document);
   begin
      if Assigned(OnLoadEnvironment) then OnLoadEnvironment(Sender,Document);
   end;

 procedure TGISEngine.DoDirectoryNotFound(Sender : TEarthBase; var DirectoryName : string);
   begin
      if Assigned(OnDirectoryNotFound) then OnDirectoryNotFound(Sender,DirectoryName);
   end;

 procedure TGISEngine.DoFileNotFound(Sender : TEarthBase; var Filename : TFilename; var TryFilename : Boolean);
   begin
      if Assigned(OnFileNotFound) then OnFileNotFound(Sender,Filename,TryFilename);
   end;

 procedure TGISEngine.DoLayersChange(Sender: TObject);
   begin
      if Assigned(OnLayersChange) then OnLayersChange(Sender);
   end;

 procedure TGISEngine.DoMouseDown(Sender: TObject; Button: TMouseButton;
                             Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
   begin
      if Assigned(OnMouseDown) then OnMouseDown(Sender,Button,Shift,X, Y);
   end;

 procedure TGISEngine.DoMouseEnter(Sender: TObject);
   begin
      if Assigned(OnMouseEnter) then OnMouseEnter(Sender);
   end;

 procedure TGISEngine.DoMouseLeave(Sender: TObject);
   begin
      if Assigned(OnMouseLeave) then OnMouseLeave(Sender);
   end;

 procedure TGISEngine.DoMouseMove(Sender: TObject; Shift: TShiftState;X, Y: Integer; Layer: TCustomLayer);
   begin
      if Assigned(OnMouseMove) then OnMouseMove(Sender, Shift,X, Y);
   end;

 procedure TGISEngine.DoMouseUp(Sender: TObject; Button: TMouseButton;
                             Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
   begin
      if Assigned(OnMouseUp) then OnMouseUp(Sender, Button,Shift,X, Y);
   end;

end.

