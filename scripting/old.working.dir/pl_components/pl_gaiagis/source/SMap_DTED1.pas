
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


{

 DTED files (Digital Terrain Elevation Data)
 Level 1, aka DTED 1 
  post spacing is 3 arc seconds (~ 100 meters)
  FileExt: *.Dt1
}

unit SMap_DTED1;

 {$MODE DELPHI}

interface
uses LCLIntf, LCLType, Classes, Graphics, SysUtils, Dialogs,
     GIS_EarthBase, GIS_Classes,GIS_EarthObjects, SMap_UniDIB;

const
  DTED1_Size_of_File_header = 18;
  DTED1_Size_of_elevation_section = 10;
  DTED1_Size_of_Elevation_Data = 46;



type
  DTED1_file_header = record            //At begin of file
    attribute_offset: longint;   // 4 bytes
    security_class: char;
    security_code: array[0..2 - 1] of char;
    security_mark: array[0..2 - 1] of char;
    terminator: char;
    pad: array[0..3 - 1] of char;
    number_extended: byte;
    attribute_length: longint;   // 4 bytes
  end {DTED1_file_header};

  DTED1_elevation_section = record
    elevation_length: longint;       //4
    atype: smallint;                 //2
    array_length: longint;          //4
  end {DTED1_elevation_section};


  DTED1_elevation_data = record
    subframe_sw_x: single;
    subframe_sw_y: single;
    subframe_se_x: single;
    subframe_se_y: single;
    subframe_ne_x: single;
    subframe_ne_y: single;
    subframe_nw_x: single;
    subframe_nw_y: single;
    number_rows: smallint;
    number_cols: smallint;
    x_interval: single;
    y_interval: single;
    minimum_elevation: smallint;
    maximum_elevation: smallint;
  end {DTED1_elevation_data};

  DTED1_Frame = record
    isload: boolean;
    Width: integer;
    Height: integer;
    DibPos: integer;
    ElevSectionRec: DTED1_elevation_section;
    ElevDataRec: DTED1_elevation_data;
  end;

  TElevMatrix_DTED1 = array[0..0] of single;//smallint;

 {...........................TMapFileDTED1..........................................}
  TMapFileDTED1 = class(TObject)
  private

  protected
    FDTED1_file_header: DTED1_file_header;
    FFrames: array[0..16] of DTED1_Frame;
    FNumOfFrames: integer;
    
    FElevMatrix: ^TElevMatrix_DTED1;   //store all elevetion data
    FStream: TStream;
    FImage: TBitmap;
    Fpalette: TxLogPalette256;
    FisImageload: boolean;
    FisRecordsLoad: boolean;
    FWidth: integer;
    FHeight: integer;
    function LoadHeader:boolean;

    function FindAndLoadDTED1_FramesRec:boolean;
    procedure FindWidthAndHeight;
    procedure LoadDTED1_FramesToImage;
    procedure FillPalette;
    procedure LoadDTED1_FrameToImage(Uni: TUniDIB; Width, Height, X, Y: integer);
    function  LoadRecords:boolean;
    procedure LoadImage;
  public
    constructor Create;
    procedure Free;
    procedure Clear;
    Function LoadFromStream(aStream: TStream):boolean;
    function GetHeightFromXY(x, y: integer): single;
    property IsImageLoad: boolean read FisImageload default False;
    property IsRecordsLoad: boolean read FisRecordsLoad default False;
    property Image: TBitmap read FImage write FImage;
    property Width: integer read FWidth;
    property Height: integer read FHeight;
  end;


 procedure WriteToTStrings(aTStrings: TStrings);

 var
 FDTED1_file_header: DTED1_file_header;
 FDTED1_Frames: array[0..16] of DTED1_Frame;
 FNumOfDTED1_Frames: integer;

implementation

//.................TMapFileDTED1.......................................

constructor TMapFileDTED1.Create;
begin
  inherited;
  FImage := TBitmap.Create;
  FisImageload := False;
  FisRecordsLoad := False;
  FWidth := 0;
  FHeight := 0;
end;

procedure TMapFileDTED1.Free;
begin
  Fimage.Free;
  inherited;
end;

{-----------------------------------------------------------------------------
   clear variables
------------------------------------------------------------------------------}
Function TMapFileDTED1.LoadFromStream(aStream: TStream):boolean;
begin
 result:=false;
  if astream <> nil then
  begin
    Fstream := aStream;
    Clear;
    //Fstream.Position:=0;
    if LoadRecords=true then
     begin
      LoadImage;
      result:=true;
     end;
  end;
end;

procedure TMapFileDTED1.Clear;
begin
  inherited;
  FisImageload := False;
  FisRecordsLoad := False;
  FWidth := 0;
  FHeight := 0;
  fillchar(FDTED1_file_header, sizeof(FDTED1_file_header), 0);
  fillchar(FDTED1_Frames, sizeof(FDTED1_Frames), 0);
end;

function TMapFileDTED1.GetHeightFromXY(x, y: integer): single;//smallint;
begin
  Result := FElevMatrix[y * fWidth + x];
end;

function TMapFileDTED1.LoadRecords:boolean;
begin
  result:=false;
  Clear;
  FisRecordsload :=false;
  if LoadHeader=true then
     if FindAndLoadDTED1_FramesRec=true then
          begin
          FindWidthAndHeight;
          FisRecordsload := True;
          result:=true;
          end;

end;

procedure TMapFileDTED1.LoadImage;
begin
  if FisRecordsload = True then
  begin
    FillPalette; //Load Color Palettee
    LoadDTED1_FramesToImage;
    FisImageload := True;
  end;
end;

procedure TMapFileDTED1.LoadDTED1_FramesToImage;
var 
  col, X, Y: integer;
  uni: TUniDIB;
begin
  Fimage.PixelFormat := pf8bit;
  fimage.Width := Fwidth;
  fimage.Height := FHeight;
  //........
  Getmem(FElevMatrix, FHeight * Fwidth * 2);
  uni := TUniDIB.Create(Fwidth, FHeight, 8,SBU_NONE);
  uni.SetPalette(FPalette);
  //.................................
  x := 0;
  y := 0;
  try
    for col := 0 to 16 do
    begin
      if fDTED1_Frames[col].isload = True then
      begin
        FStream.Position := fDTED1_Frames[col].DibPos;
        LoadDTED1_FrameToImage(uni,
          fDTED1_Frames[col].Width, fDTED1_Frames[col].Height,
          X, Y);
        x := x + fDTED1_Frames[col].Width;
      end;
    end;
    //..............................
    uni.DIBtoCanvasXY(0,0,fimage.Canvas.Handle);
  finally
    uni.Free;
  end;
end;

procedure TMapFileDTED1.LoadDTED1_FrameToImage(Uni: TUniDIB; Width, Height, X, Y: integer);
type
  TBuf1 = array[0..0] of byte;
  TBuf2 = array[0..0] of smallint;
var
  i: integer;
  PBuf1: ^TBuf1;
  PBuf2: ^TBuf2;
  Pos, Count: longint;
begin
  Count := Width * Height;
  getmem(PBuf1, Count);
  getmem(PBuf2, 2 * Count);        //getmem for smallint
  try  
    FStream.Read(Pbuf2^, 2 * Count); //read count x sizeof (smallint);
    for i := 0 to Count - 1 do
    begin
      Pbuf1[i] := Pbuf2[i];
    end;
    for  i := 0 to Height - 1 do
    begin
      Pos := ((Y + i) * FImage.Width) + x;
      System.Move(Pointer(longint(Pbuf1) + (Width * i))^,
        Pointer(longint(uni.bits) + Pos)^, Width);
      System.Move(Pointer(longint(Pbuf2) + (Width * i * 2))^,
        Pointer(longint(FElevMatrix) + Pos * 2)^, Width * 2);
    end;
  finally
    FreeMem(PBuf1);
    FreeMem(PBuf2);
  end;
end;

procedure TMapFileDTED1.FindWidthAndHeight;
var 
  col, sWidth, sHeight: integer;
begin
  swidth := 0;
  sHeight := 0;

  for col := 0 to 16 do
  begin
    if fDTED1_Frames[col].isload = True then
    begin
      swidth := swidth + fDTED1_Frames[col].Width;
      sHeight := fDTED1_Frames[col].Height;
    end;
  end;
  FHeight := sHeight;
  FWidth := swidth;
end;
 
Function TMapFileDTED1.FindAndLoadDTED1_FramesRec:boolean;
var 
  es: DTED1_elevation_section;
  ed: DTED1_elevation_data;
  col, Count: integer;
  End_of_images: longint;
begin
 result:=true;
  col := 0;
  Count := 1;
  End_of_images := Fstream.Size;
  repeat
    FStream.Read(es, DTED1_Size_of_File_header);
    if es.atype <> 21 then   //This is chec rec type of ADRG or ADRG file type
    begin
       if Count = 1 then result:=false;
      exit;
    end;
    FStream.Read(ed, DTED1_Size_of_File_header);
    if Count > 1 then
    begin
      inc(col);
    end;
    fDTED1_Frames[col].isload := True;
    fDTED1_Frames[col].ElevSectionRec := es;
    fDTED1_Frames[col].ElevDataRec := ed;
    fDTED1_Frames[col].Width := ed.number_cols;
    fDTED1_Frames[col].Height := ed.number_rows;
    fDTED1_Frames[col].DibPos := Fstream.Position;
    inc(Count);
    Fstream.Position := Fstream.Position + 2 * (ed.number_rows * ed.number_cols) + 2;
  until FStream.Position >= End_of_images;
  FnumofDTED1_Frames := Count - 1;
end;
 
function TMapFileDTED1.LoadHeader:boolean;
begin
  result:=false;
  FStream.Read(FDTED1_file_header, DTED1_Size_of_File_header);
 if (FDTED1_file_header.attribute_offset) < (FStream.Size) then 
    Result :=true;

end;

procedure TMapFileDTED1.FillPalette;
var 
  i: byte;
begin
  FPalette.palVersion := $300;
  FPalette.palNumEntries := 256;
  for i := 0 to 255 do
  begin
    FPalette.palEntry[i].peRed := 256 - i;
    FPalette.palEntry[i].peGreen := 256 - i;
    FPalette.palEntry[i].peBlue := 256 - i;
    FPalette.palEntry[i].peFlags := 0;
  end;
end;
  
procedure WriteToTStrings(aTStrings: TStrings);
var    
  col: integer;
begin
  with  aTStrings do
  begin
    aTStrings.Clear;
    add('Report of DTED1 data format File...........................................');
    add('DTED1_file_header rec size:' + IntToStr(sizeof(DTED1_file_header)) +
      ' read from file: ' + IntToStr(DTED1_Size_of_File_header));
    add('DTED1_elevation_section rec size:' + IntToStr(sizeof(DTED1_elevation_section)));
    add('DTED1_elevation_data rec size:' + IntToStr(sizeof(DTED1_elevation_data))
      + ' read from file: ' + IntToStr(DTED1_Size_of_elevation_data));
    add(' ');
    add('The_DTED1_file_header');
    add('         attribute_offset:' + IntToStr(FDTED1_file_header.attribute_offset));
    add('         security_class:' + FDTED1_file_header.security_class);
    add('         security_code:' + FDTED1_file_header.security_code);
    add('         security_mark:' + FDTED1_file_header.security_mark);
    add('         terminator:' + FDTED1_file_header.terminator);
    add('         pad:' + FDTED1_file_header.pad);
    add('         number_extended:' + IntToStr(FDTED1_file_header.number_extended));
    add('         attribute_length:' + IntToStr(FDTED1_file_header.attribute_length));
    add('Total Number Of DTED1_Frames in file: ' + IntToStr(FNumOfDTED1_Frames));
    add('...............Start of DTED1_Frames ............................ ');
    for col := 0 to 6 do
    begin
      if fDTED1_Frames[col].isload = True then
      begin
        add('DTED1_Frame at martix position Col: ' + IntToStr(col));
        add('DIB At file Possition : ' + IntToStr(fDTED1_Frames[col].DibPos));
        add('DTED1_elevation_section');
        add('        elevation_length' + IntToStr(fDTED1_Frames[col].ElevSectionRec.elevation_length));
        add('        atype: Integer' + IntToStr(fDTED1_Frames[col].ElevSectionRec.atype));
        add('        array_length' + IntToStr(fDTED1_Frames[col].ElevSectionRec.array_length));
        add('DTED1_elevation_data');

        add('        subFrame_sw_x :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_sw_x));
        add('        subFrame_sw_y :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_sw_y));
        add('        subFrame_se_x :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_se_x));
        add('        subFrame_se_y :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_se_y));
        add('        subFrame_ne_x :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_ne_x));
        add('        subFrame_ne_y :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_ne_y));
        add('        subFrame_nw_x :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_nw_x));
        add('        subFrame_nw_y :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.subFrame_nw_y));
        add('        number_rows :' + IntToStr(fDTED1_Frames[col].ElevDataRec.number_rows));
        add('        number_cols :' + IntToStr(fDTED1_Frames[col].ElevDataRec.number_cols));
        add('        x_interval :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.x_interval));
        add('        y_interval :' + FloatToStr(fDTED1_Frames[col].ElevDataRec.y_interval));
        add('        minimum_elevation :' +
          IntToStr(fDTED1_Frames[col].ElevDataRec.minimum_elevation));
        add('        maximum_elevation :' +
          IntToStr(fDTED1_Frames[col].ElevDataRec.maximum_elevation));
      end;
    end; //end of write DTED1_Frames loop
    add('...............End of DTED1_Frames ................................');  
  end;
end;
{-----------------------------------------------------------------------}


end.
