
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit sFileForGISLayer;



interface
  uses
  LCLIntf, LCLType, LMessages, Forms,  Classes, SysUtils,
  GIS_SysUtils,GIS_Classes, GIS_EarthStreams, FileUtil, lazfileutils;

const
  GaiaCADGISLayerFileFilter='GaiaCAD Layers File (*.GLF)|*.GLF';
  GaiaCADGISLayerFileExt='GLF';

Function Is_GISLayerFile(const sFilename : TFilename):boolean;

Function Save_GISLayerToStream(aLayer:TEarthLayer;astream:Tstream):boolean;
Function Save_GISLayerToFile(aLayer:TEarthLayer;const aFile:String):boolean;
Function Save_GISLayersListToStream(aList:TList;astream:Tstream):longint;
Function Save_GISLayersListToFile(aList:TList;const aFile:String):longint;
Function Save_GISLayersToStream(aLayerStore:TEarthLayerStore;astream:Tstream):longint;
Function Save_GISLayersToFile(aLayerStore:TEarthLayerStore;const aFile:String):longint;

Function Load_GISLayersFromStream(aLayerStore:TEarthLayerStore;astream:Tstream):longint;
Function Load_GISLayersFromFile(aLayerStore:TEarthLayerStore;const aFile:String):longint;

implementation
 const
  GISLayerFileCodeId=56968;    //Use to intetify the file type
  GISLayerFileVersion=1;       //Rec version
 type
   
TGISLayerRec=Record
    WhatIs:string[70];
    RecId1:LongInt;  //Use of intetify the file type
    RecVer:integer;  //Rec version
    TG_FILEVERSION:integer;
    LayersNum:longint;
    FutureString1:String[100];
    FutureString2:String[100];
    FutureBool1:boolean;
    FutureBool2:boolean;
    FutureInt1:longint;
    FutureInt2:longint;
   end;

Procedure Clear_TGISLayerRec(var arec:TGISLayerRec);
 begin
    arec.WhatIs:='GaiaCAD Layer File';
    arec.RecId1:=GISLayerFileCodeId;
    arec.RecVer:=GISLayerFileVersion;
    arec.TG_FILEVERSION:=TG_FILEVERSION;
    arec.LayersNum:=0;
    arec.FutureString1:='';
    arec.FutureString2:='';
    arec.FutureBool1:=false;
    arec.FutureBool2:=false;
    arec.FutureInt1:=-9999;
    arec.FutureInt2:=-9999;
 end;

 //======================================================================

Function Is_GISLayerFile(const sFilename : TFilename):boolean;
 begin
  result:=(SameText(ExtractFileExt(sFilename),'.'+GaiaCADGISLayerFileExt));
 end;

 Function Save_GISLayerToStream(aLayer:TEarthLayer;astream:Tstream):boolean;
 var Rec:TGISLayerRec;
     strW:TEarthStreamWriter;
 begin
   result:=false;
   if aStream=nil then exit;
   if aLayer=nil then exit;
   Clear_TGISLayerRec(Rec);
   Rec.LayersNum:=1;
   strW:=TEarthStreamWriter.Create(aStream);
   try
     strW.WriteBuffer(Rec,SizeOf(TGISLayerRec));
     strW.WriteShortString(aLayer.ClassName);
     aLayer.WriteProperties(strW);
     result:=True;
   finally
     strW.Free;
   end;
 end;



Function Save_GISLayerToFile(aLayer:TEarthLayer;const aFile:String):boolean;
 var istr:TMemoryStream;
     sss:string;
 begin
   result:=false;
   sss:=aFile;
   if sss='' then exit;
   if aLayer=nil then exit;

   istr:=TMemoryStream.Create;
   try
     Result:=Save_GISLayerToStream(aLayer,istr);
     //.... Check and set File Extension ......
     if (SameText(ExtractFileExt(sss),'.'+GaiaCADGISLayerFileExt)=false) then
         sss:=SysUtils.ChangeFileExt(sss,'.'+GaiaCADGISLayerFileExt);
     //........................................
     istr.SaveToFile(sss);
     result:=True;
   finally
     istr.free;
   end;
 end;

Function Save_GISLayersListToStream(aList:TList;astream:Tstream):longint;
 var Rec:TGISLayerRec;
     strW:TEarthStreamWriter;
     Lay:TEarthLayer;
     i:longint;
 begin
   result:=0;
   if aStream=nil then exit;
   if aList=nil then exit;
   Clear_TGISLayerRec(Rec);
   Rec.LayersNum:=aList.Count;
   strW:=TEarthStreamWriter.Create(aStream);
   try
     strW.WriteBuffer(Rec,SizeOf(TGISLayerRec));
     for I := 0 to aList.Count - 1 do
      begin
       Lay:=nil;
       Lay:=TEarthLayer(alist.Items[i]);
       strW.WriteShortString(Lay.ClassName);
       Lay.WriteProperties(strW);
       inc(result);
      end;
   finally
     strW.Free;
   end;
 end;

Function Save_GISLayersListToFile(aList:TList;const aFile:String):longint;
 var istr:TMemoryStream;
     sss:string;
 begin
   result:=0;
   sss:=aFile;
   if sss='' then exit;
   if aList=nil then exit;

   istr:=TMemoryStream.Create;
   try
     Result:=Save_GISLayersListToStream(aList,istr);
     //.... Check and set File Extension ......
     if (SameText(ExtractFileExt(sss),'.'+GaiaCADGISLayerFileExt)=false) then
         sss:=SysUtils.ChangeFileExt(sss,'.'+GaiaCADGISLayerFileExt);
     //........................................
     istr.SaveToFile(sss);
   finally
     istr.free;
   end;
 end;

Function Save_GISLayersToStream(aLayerStore:TEarthLayerStore;astream:Tstream):longint;
 var Rec:TGISLayerRec;
     strW:TEarthStreamWriter;
     Lay:TEarthLayer;
     i:longint;
 begin
   result:=0;
   if aStream=nil then exit;
   if aLayerStore=nil then exit;
   Clear_TGISLayerRec(Rec);
   Rec.LayersNum:=aLayerStore.Count;
   strW:=TEarthStreamWriter.Create(aStream);
   try
     strW.WriteBuffer(Rec,SizeOf(TGISLayerRec));
     for I := 0 to aLayerStore.Count - 1 do
      begin
       Lay:=nil;
       Lay:=aLayerStore.Layer[i];
       strW.WriteShortString(Lay.ClassName);
       Lay.WriteProperties(strW);
       inc(result);
      end;
   finally
     strW.Free;
   end;
 end;

Function Save_GISLayersToFile(aLayerStore:TEarthLayerStore;const aFile:String):longint;
 var istr:TMemoryStream;
     sss:string;
 begin
   result:=0;
   sss:=aFile;
   if sss='' then exit;
   if aLayerStore=nil then exit;

   istr:=TMemoryStream.Create;
   try
     Result:=Save_GISLayersToStream(aLayerStore,istr);
     //.... Check and set File Extension ......
     if (SameText(ExtractFileExt(sss),'.'+GaiaCADGISLayerFileExt)=false) then
         sss:=SysUtils.ChangeFileExt(sss,'.'+GaiaCADGISLayerFileExt);
     //........................................
     istr.SaveToFile(sss);
   finally
     istr.free;
   end;
 end;


//.........................................
Function Load_GISLayersFromStream(aLayerStore:TEarthLayerStore;astream:Tstream):longint;
var Rec:TGISLayerRec;
    strR:TEarthStreamReader;
    cs:TPersistentClass;
    Ls:TEarthLayer;
    i:longint;
 begin
   result:=0;
   Ls:=nil;
   if aStream=nil then exit;
   if aLayerStore=nil then exit;
   Clear_TGISLayerRec(Rec);

   strR:=TEarthStreamReader.Create(aStream);
   try
     Cs:=nil;
     strR.ReadBuffer(Rec,SizeOf(TGISLayerRec));
     if Rec.RecId1=GISLayerFileCodeId then
      begin
        giFileVersion:=Rec.TG_FILEVERSION;
        for I := 0 to Rec.LayersNum - 1 do
         begin             
          cs:=GetClass(strR.ReadShortString);  //read class
          if cs<>nil then
          begin
            Ls:=TEarthLayerClass(cs).Create(aLayerStore); //Create class
            Ls.ReadProperties(strR);
            inc(result);
          end;

         end;
      end;
   finally
     strR.Free;
   end;
 end;

Function Load_GISLayersFromFile(aLayerStore:TEarthLayerStore;const aFile:String):longint;
 var istr:TMemoryStream;
 begin
   result:=0;
   if aFile='' then exit;
   if FileExistsUTF8(aFile)  =false then exit;
   if aLayerStore=nil then exit;

   istr:=TMemoryStream.Create;
   try
    istr.LoadFromFile(aFile);
    istr.Position:=0;
    Result:=Load_GISLayersFromStream(aLayerStore,istr);
   finally
    istr.free;
   end;
 end;
end.
