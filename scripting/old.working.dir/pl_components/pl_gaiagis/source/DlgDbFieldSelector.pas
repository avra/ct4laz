
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit DlgDbFieldSelector;



interface

uses
  LCLIntf, LCLType, LMessages, Messages,
  SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,GIS_DBFReader, ComCtrls, ToolWin, ImgList,
  ExtCtrls;

type
  TGISDbFieldSelector = class(TForm)
    ListBox1: TListBox;
    ImageList1: TImageList;
    ToolBar1: TToolBar;
    ToolOk: TToolButton;
    ToolCancel: TToolButton;
    Panel1: TPanel;
    StatusBar1: TStatusBar;
    Memo1: TMemo;
    procedure ListBox1Click(Sender: TObject);
    procedure ToolOkClick(Sender: TObject);
    procedure ToolCancelClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
  private
    FDBFReader:TEarthDBFReader;
    fFieldNum:integer;
    FFileName:String;
    Procedure DataToForm;
  public
    Function Execute(aDBFReader:TEarthDBFReader):boolean;
    Property FieldNum:integer read fFieldNum;
    Property FileName:String read fFileName write ffilename;
  end;


implementation

{$R *.lfm}

Function TGISDbFieldSelector.Execute(aDBFReader:TEarthDBFReader):boolean;
  begin
     if aDBFReader= nil then exit;
     FDBFReader:=aDBFReader;
     DataToForm;
     ShowModal;
     result:=(ModalResult=mrok);
  end;

Procedure TGISDbFieldSelector.DataToForm;
var i:integer;
 begin
   ListBox1.Clear;
   fFieldNum:=-1;
   StatusBar1.SimpleText:=FFileName;
   if fDBFReader= nil then exit;


  for i:=0 to fDBFReader.iFieldCount-1 do
    ListBox1.AddItem(fDBFReader.FieldName[i]+'  (FieldType '+fDBFReader.FieldType[i]+')',nil);
 end;
procedure TGISDbFieldSelector.ListBox1Click(Sender: TObject);
begin
  fFieldNum:=ListBox1.ItemIndex;
end;

procedure TGISDbFieldSelector.ToolOkClick(Sender: TObject);
begin
  ModalResult:=mrok;
end;

procedure TGISDbFieldSelector.ToolCancelClick(Sender: TObject);
begin
 ModalResult:=mrCancel;
end;

procedure TGISDbFieldSelector.ListBox1DblClick(Sender: TObject);
begin
 fFieldNum:=ListBox1.ItemIndex;
 ModalResult:=mrok;
end;

end.
