
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_MapperLYR;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  SysUtils, Classes, Forms, GIS_EarthBase, GIS_Presenters, GIS_SysUtils,
  GIS_EarthObjects, GIS_TextReader, GIS_Classes, GIS_XML, GIS_MMStream,
  GIS_EarthStreams, FileUtil, lazfileutils;

const
  TG_LYR_READERVERSION = $0103;

type
  TLYRItem = record
    iObjClass : integer;
    iOffset : integer;
  end;
  TLYRItemPtr = ^TLYRItem;

//---------------------------- TEarthLYRStreamReader -------------------------------
//
TEarthLYRStreamReader = class(TEarthFileReader)
  private
    FDataItems : DynArray;
    FLYRStream : TStream;
    FObjectClasses : TStringList;
    iObjCount, iObjClassIndex : integer;
  protected
    FLayer : TEarthLayer;
    Reader : TEarthStreamReader;
    procedure SetLYRStream( aStream : TStream );
    function InternalOpen : Boolean; override;
    function InternalClose : Boolean; override;
    function MapDataHeader( aStream : TStream ) : Boolean; override;
    function MapDataObject( aStream : TStream; iIndex : integer ) : Boolean; override;
  public
    constructor Create(aLayer:TEarthLayer); override;
    destructor Destroy; override;
    procedure SaveMetaData; override;
    procedure LoadMetaData; override;
    function  LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject; override; 
    property  LYRStream : TStream read FLYRStream write SetLYRStream;
  end;
//---------------------------- TEarthLYRReader -------------------------------
//
TEarthLYRReader = class(TEarthLYRStreamReader)
  protected
    function InternalOpen : Boolean; override;
    function InternalClose : Boolean; override;
  end;

//---------------------------- TEarthLYRWriter -------------------------------
//
TEarthLYRWriter = class( TEarthFileWriter )
  private
    FObjectClasses : TStringList;
    FPresenterClasses : TStringList;
    procedure WriteLYRBody( Writer : TEarthStreamWriter );
    procedure WriteLYRHeader( Writer : TEarthStreamWriter );
    procedure WriteLYRHeaderOnlyUsed( Writer : TEarthStreamWriter );
  public
    constructor Create( aLayer : TEarthLayer ); override;
    destructor Destroy; override;
    function SaveToFile( const sFilename : TFilename ) : Boolean; override;
    function SaveToStream(sStream: TStream ):Boolean;
  end;


implementation

Uses GIS_Resource;   

//=======================================================================================
//========================= TEarthLYRWriter =============================================
//=======================================================================================

constructor TEarthLYRWriter.Create(aLayer : TEarthLayer);
begin
  inherited Create( aLayer );

  FObjectClasses := TStringList.Create;
  FPresenterClasses := TStringList.Create;
end;

destructor TEarthLYRWriter.Destroy;
begin
  FObjectClasses.Free;
  FObjectClasses := nil;

  FPresenterClasses.Free;
  FPresenterClasses := nil;

  inherited Destroy;
end;  

function TEarthLYRWriter.SaveToFile(const sFilename: TFilename): Boolean;
var  DS:TmemoryStream;
begin
  result:=false;
  if sFilename='' then exit;

   DS:=TmemoryStream.Create;
  try

   if SaveToStream(DS) then
     begin
     DS.SaveToFile(sFilename);
     FLayer.FullFileName:=sFilename;
     end;

  finally
    DS.Free;
  end;
end;

function TEarthLYRWriter.SaveToStream(sStream: TStream ):Boolean;
var
  Writer : TEarthStreamWriter;
  DS:TmemoryStream;
  idx, iIndex : integer;
begin
 result:=false;
 if sStream=nil then exit;

  DS:=TmemoryStream.Create;
  Writer := TEarthStreamWriter.Create(DS);

  try
    //Reset Layer.FilePath
    FLayer.FullFileName:='';
    // Build a list of all object classes on the layer
    with FObjectClasses do
      for idx := 0 to FLayer.Objects.Count - 1 do
      begin
        iIndex := IndexOf( FLayer.Objects[idx].Classname );
        if iIndex = -1 then
          iIndex := Add( FLayer.Objects[idx].Classname );

        Objects[iIndex] := TObject( integer( Objects[iIndex] ) + 1 );
      end;

    WriteLYRHeader( Writer );
    WriteLYRBody( Writer );
    DS.Position:=0;
    sStream.CopyFrom(DS,DS.Size);
    Result := True;
  finally
    Writer.Free;
  end;
end;

procedure TEarthLYRWriter.WriteLYRBody( Writer : TEarthStreamWriter );
var
  idx, jdx, iCount : integer;
  sClassName : string;
begin
  for idx := 0 to FObjectClasses.Count - 1 do
  begin
    iCount := integer( FObjectClasses.Objects[idx] );
    sClassName := FObjectClasses[idx];

    if iCount > 0 then
    begin
      Writer.WriteInteger( iCount );

      Writer.WriteShortString( sClassName );
      // Save the class data
      for jdx := 0 to FLayer.Objects.Count - 1 do
        if sClassName = FLayer.Objects[jdx].Classname then
          FLayer.Objects[jdx].WriteProperties( Writer );
    end;
  end;
  Writer.WriteInteger( 0 );  // sentinal at the end of the data
end;

procedure TEarthLYRWriter.WriteLYRHeader( Writer : TEarthStreamWriter );
var
  idx, jdx, iIndex, iCount : integer;
  sClassName : string;
  aPresenterStore : TEarthPresenterStore;
  aPresenter: TEarthPresenter;

begin
  Writer.WriteSmallInt( TG_FILEVERSION ); // write the version of Earth data file
  
  Writer.WriteMER( FLayer.LayerMER );  // write the Layer MER   
  aPresenterStore := FLayer.Objects.Presenters;
  try
    // Build a list of all presenter classes on the layer
    with FPresenterClasses do
      for idx := 0 to aPresenterStore.Count - 1 do
      begin
        iIndex := IndexOf( aPresenterStore[idx].Classname );
        if iIndex = -1 then
          iIndex := Add( aPresenterStore[idx].Classname );

        Objects[iIndex] := TObject( integer( Objects[iIndex] ) + 1 );
      end;

    for idx := 0 to FPresenterClasses.Count - 1 do
    begin
      iCount := integer( FPresenterClasses.Objects[idx] );
      sClassName := FPresenterClasses[idx];

      if iCount > 0 then
      begin
        Writer.WriteInteger( iCount );
        Writer.WriteShortString( sClassName );
        // Save the class data
        for jdx := 0 to aPresenterStore.Count -1 do
          if sClassName = aPresenterStore[jdx].Classname then
            aPresenterStore[jdx].WriteProperties( Writer );
      end;
    end;


  finally

  end;
end;

// SOS this code  delete any NO used Tresenter for layer at save procedure  
procedure TEarthLYRWriter.WriteLYRHeaderOnlyUsed( Writer : TEarthStreamWriter );
var
  idx, jdx, iIndex, iCount : integer;
  sClassName : string;
  aPresenterStore : TEarthPresenterStore;
  aPresenter: TEarthPresenter;
begin
  Writer.WriteSmallInt( TG_FILEVERSION ); // write the version of Earth data file

  Writer.WriteMER( FLayer.LayerMER );  // write the Layer MER
  aPresenterStore := TEarthPresenterStore.Create(self);
  try
    // Build the list of used presenters for the objects in this ObjectStore
    for idx := 0 to FLayer.Objects.Count -1 do
      with FLayer.Objects[idx] do
        if aPresenterStore.ByID( PresenterID, False ) = nil then
        begin
          aPresenter := FLayer.FindPresenter( PresenterID );
          if aPresenter <> nil then aPresenter.Clone(aPresenterStore);

        end;

    // Build a list of all presenter classes on the layer
    with FPresenterClasses do
      for idx := 0 to aPresenterStore.Count - 1 do
      begin
        iIndex := IndexOf( aPresenterStore[idx].Classname );
        if iIndex = -1 then
          iIndex := Add( aPresenterStore[idx].Classname );

        Objects[iIndex] := TObject( integer( Objects[iIndex] ) + 1 );
      end;

    for idx := 0 to FPresenterClasses.Count - 1 do
    begin
      iCount := integer( FPresenterClasses.Objects[idx] );
      sClassName := FPresenterClasses[idx];

      if iCount > 0 then
      begin
        Writer.WriteInteger( iCount );
        Writer.WriteShortString( sClassName );
        // Save the class data
        for jdx := 0 to aPresenterStore.Count -1 do
          if sClassName = aPresenterStore[jdx].Classname then
            aPresenterStore[jdx].WriteProperties( Writer );
      end;
    end;

  finally
    aPresenterStore.Free;
  end;

end;

//============================ TEarthLYRStreamReader ======================================================

function TEarthLYRStreamReader.LoadObject( iIndex : integer; bNewObject : Boolean ) : TEarthObject;
begin
  with TLYRItemPtr( DynArrayPtr( FDataItems, iIndex))^ do
  begin
    FLYRStream.Position := iOffset;

    { Create new instance of class the Presenter }
    Result := TEarthObjectClass( FObjectClasses.Objects[iObjClass] ).Create( Self );
    Result.ReadProperties( Reader );
	end;
end;

procedure TEarthLYRStreamReader.SetLYRStream( aStream : TStream );
begin
  Reader.Free;
  Reader := nil;
  FLYRStream := aStream;
end;

procedure TEarthLYRStreamReader.SaveMetaData;
var
  idx : integer;
  Writer : TEarthStreamWriter;
  sMetaDataName : TFilename;
begin
  sMetaDataName := MetaDataFilename( Filename );
  if sMetaDataName = '' then
    Exit;

  Writer := TEarthStreamWriter.Create( TMemoryStream.Create );
  try
    Writer.WriteWord( TG_LYR_READERVERSION );

    WriteMetaData( Writer );

    Writer.WriteInteger( FObjectClasses.Count ); // save the count of Classes
    // Save the class names
    for idx := 0 to FObjectClasses.Count -1 do
      Writer.WriteShortString( FObjectClasses[idx] );

    Writer.WriteInteger( FDataItems.Count ); // save the count of items
    // save the class data.
    for idx := 0 to FDataItems.Count - 1 do
      with TLYRItemPtr( DynArrayPtr( FDataItems, idx))^ do
      begin
        Writer.WriteShortInt( iObjClass );
        Writer.WriteInteger( iOffset );
      end;

    TMemoryStream(Writer.DataStream).SaveToFile( sMetaDataName );
  finally
    Writer.Free;
  end;
end;

procedure TEarthLYRStreamReader.LoadMetaData;
var
  idx, iCount : Integer;
  sClassName : string;
  Reader : TEarthStreamReader;
  sMetaDataName : TFilename;
begin
  sMetaDataName := MetaDataFilename( Filename );
  if not FileExists( sMetaDataName ) then  Exit;
  // Check if the metadata file is older than the data file
  if FileAge( sMetaDataName ) < FileAge( Filename ) then  Exit;

  Reader := TEarthStreamReader.Create( TMemoryStream.Create );
  try
    TMemoryStream(Reader.DataStream).LoadFromFile( sMetaDataName );

    if Reader.ReadWord <> TG_LYR_READERVERSION then
    begin
     // DeleteFile( sMetaDataName );
      Exit;
    end;

    ReadMetaData( Reader );

    DynArrayFree( FDataItems ); // clear the data item list

    FObjectClasses.Clear;

    // get the number of classes
    iCount := Reader.ReadInteger;
    for idx := 0 to iCount -1 do
    begin
      sClassName := Reader.ReadShortString;
      FObjectClasses.AddObject( sClassName, TObject( FindClass( sClassName )));
    end;

    // get the number of items for this class
    iCount := Reader.ReadInteger;

    // set the length of the Reader Item array
    FDataItems := DynArrayCreate( SizeOf( TLYRItem ), iCount );

    // load in the Reader data
    for idx := 0 to iCount - 1 do
      with TLYRItemPtr( DynArrayPtr( FDataItems, idx))^ do
      begin
        iObjClass := Reader.ReadShortInt;
        iOffset := Reader.ReadInteger;
      end;
  finally
    Reader.Free;
  end;
end;

constructor TEarthLYRStreamReader.Create(aLayer:TEarthLayer);
begin
  inherited;
  FLayer:=aLayer;
  FObjectClasses := TStringList.Create;
end;

destructor TEarthLYRStreamReader.Destroy;
begin
  FObjectClasses.Free;
  FObjectClasses := nil;

  inherited Destroy;
end;

function TEarthLYRStreamReader.InternalOpen : boolean;
begin
  if Assigned( LYRStream ) then
  begin
    LoadMetaData;

    Reader := TEarthStreamReader.Create( LYRStream );

    if FDataItems = nil then
    begin
      MapDataFile( LYRStream );

      if TEarthBase(Earth).SaveMetaDataLocation <> smdDiscard then
        SaveMetaData;
    end;

    Result := inherited InternalOpen;
  end
  else
    Result := false;
end;

function TEarthLYRStreamReader.InternalClose : Boolean;
begin
  Count := 0;
  DynArrayFree( FDataItems ); // clear the data item list
  Presenters.Clear;
  fLYRStream := nil;
  Result := inherited InternalClose;  
end;

function TEarthLYRStreamReader.MapDataHeader( aStream : TStream ): Boolean;
var
  idx : Integer;
  sClassName : ShortString;
  oNew : TEarthPresenter;
begin
  Result := True;
  giFileVersion := Reader.ReadSmallInt; // read the version of Earth data file is the TG_FILEVERSION

  if giFileVersion < TG_FILEVERSION300 then
    raise EEarthException.CreateFmt( rsEDataFileVersionMsg, [giFileVersion, FileName] );

  FDataItems := DynArrayCreate( Sizeof( TLYRItem ), 0 );


  Reader.ReadRect; // read in the bounding rectangle

  iObjCount := Reader.ReadInteger;
  while ( iObjCount > 0 ) and ( FLYRStream.Position < FLYRStream.Size ) do
  begin
    sClassName := Reader.ReadShortString; { Get class name }

    // Stop if we hit the TEarthObject data
    with Earth do
      if FindClass( sClassName ).InheritsFrom( TEarthObject ) then
      begin
        iObjCount := 0;
        FLYRStream.Position := FLYRStream.Position - Length( sClassName ) - 1 - SizeOf( integer );
        Exit;
      end;

    for idx := 0 to iObjCount - 1 do
      with Earth do
      begin
        { Create new instance of class the Presenter }
        oNew := TEarthPresenterClass( FindClass( sClassName )).Create(Presenters, 0);
        oNew.ReadProperties( Reader );
      end;
    iObjCount := Reader.ReadInteger;
  end;
end;

function TEarthLYRStreamReader.MapDataObject( aStream : TStream; iIndex : integer ): Boolean;
var
  sClassName : string;
begin
  if iObjCount = 0 then
  begin
    Result := False;
    iObjCount := Reader.ReadInteger;
    if iObjCount = 0 then  Exit;
    sClassName := Reader.ReadShortString; { Get class name }

    iObjClassIndex := FObjectClasses.IndexOf( sClassName );
    if iObjClassIndex = -1 then
      iObjClassIndex := FObjectClasses.AddObject( sClassName, TObject( FindClass( sClassName )));

    // Allocate the Mapper Space and Slot space for the objects
    DynArraySetLength( FDataItems, Count + iObjCount );
  end;

  with TLYRItemPtr( DynArrayPtr( FDataItems, iIndex ))^ do
  begin
    iOffset := aStream.Position;
    iObjClass := iObjClassIndex;
  end;
  Dec( iObjCount );
  Result := True;
end;

//===========================TEarthLYRReader===================================
function TEarthLYRReader.InternalOpen : Boolean;
begin
  Result := FileExists( Filename );
  if Result then
  begin
    Name := ExtractFilename( Filename );
    LoadMetaData;
    FLYRStream := TMemoryStream.Create;       //By Sternas Stefanos
    TMemoryStream(FLYRStream).LoadFromFile(Filename);  //By Sternas Stefanos
    Reader := TEarthStreamReader.Create( FLYRStream );

    if FDataItems = nil then
    begin
      MapDataFile( FLYRStream );
      if TEarthBase(Earth).SaveMetaDataLocation <> smdDiscard then SaveMetaData;
    end;

    Result := inherited InternalOpen;
  end;
end;

function TEarthLYRReader.InternalClose : Boolean;
begin
  Result := inherited InternalClose;
  if LYRStream<>nil then  LYRStream.Free;
   LYRStream := nil;
end;


initialization
  RegisterClasses( [TEarthLYRStreamReader,
                    TEarthLYRReader,
                    TEarthLYRWriter] );
end.
