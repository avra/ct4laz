
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_Classes;

interface

uses
  LCLIntf, LCLType,
  Forms, Classes, Graphics, Controls, SysUtils, StdCtrls, Printers,
  FileUtil, lazfileutils, contnrs,
  GR32, GR32_Image, agx_canvas,
  GIS_SysUtils, GIS_XML, GIS_ENVPersistent, GIS_EarthObjectData,
  GIS_EarthStreams, GIS_CustomEarthCanvas;

const
  GLOBAL_POINT_PRESENTER = -1;
  GLOBAL_POLY_PRESENTER = -2;

type

  TCustomEarth = class;
  TEarthProjection = class;
  TEarthCanvas = class;
  TEarthLayer = class;
  TEarthLayerStore = class;
  TEarthObject = class;
  TEarthObjectStore = class;
  TEarthPresenterStore = class;

  TEarthFontName = string;
  TProjectionClass = string;

  TGreatCircle = (gcShortest, gcLongest, gcCircle);

  TEarthNotification = (gnLayerCreate, gnLayerFree, gnLayersChanged, gnLayerPropertyChanged,
    gnObjectSourceCreate, gnObjectSourceActiveChange, gnObjectSourceFree,
    gnObjectFree, gnObjectSelectChange,
    gnPresenterPropertyChange, gnPresenterStoreChange);

  TEarthOptions = (goCache3DPoints, goDoubleBuffered, goInterruptable,
    goMultiSelect, goTransparentEarth, goTransparentBackground,
    goConstantScaleOnResize, goFixedCenterXYOnResize);

  TEarthOptionsSet = set of TEarthOptions;

  TTitleAlignment = (taNone, taCenter, taLeft, taRight, taTopLeft, taTop, taTopRight,
    taBottomLeft, taBottom, taBottomRight);

  TObjectSourceNotification = (osnOpen, osnClose, osnAdd, osnDelete,
    osnRedraw, osnSelectedChanged, osnCreate, osnFree);

  TEarthLayerState = (lsValidMER, lsValidSI);
  TEarthLayerStateSet = set of TEarthLayerState;

  TProjectionFlags = (pfContinuous, pfTextureMap);
  TProjectionFlagSet = set of TProjectionFlags;

  TXAxisChanges = (xaYOrigin, xaCentralParallel, xaNoAction);
  TYAxisChanges = (yaXOrigin, yaCentralMeridian, yaNoAction);

  TProjectionProperty = (ppXRotation, ppYRotation, ppZRotation, ppScaleFactor,
    ppXOrigin, ppYOrigin, ppCentralMeridian, ppCentralParallel,
    ppFirstParallel, ppSecondParallel, ppSpheroid, ppXAxisChanges,
    ppYAxisChanges, ppMaxLatitude, ppMinLatitude, ppMaxLongitude, ppMinLongitude,
    ppMaxZoom, ppMinZoom);

 { How Earth Render Objects Titles
     trtAfterAllLayers: Render first all Layers Objects and then Render all Layers Objects titles (default)
     trtAfterLayer: After Render a Layer, Render this Layer Objects titles
     trtAfterObject: After Render an object, Render this Object title
     }
  TTitlesRenderMethod = (trtAfterAllLayers, trtAfterLayer, trtAfterObject);

  TPresenterStoreNotifyEvent = procedure(Sender: TObject; PresenterStore: TEarthPresenterStore) of object;
  TObjectStoreNotifyEvent = procedure(Sender: TObject; ObjectStore: TEarthObjectStore) of object;
  TEarthObjectNotifyEvent = procedure(Sender: TCustomEarth; EarthObject: TEarthObject) of object;

  TEarthAttribute = class(TEarthENVPersistent)
  private
    FEarthCanvas: TEarthCanvas;
  public
    destructor Destroy; override;
    function IsEquals(Attr: TEarthAttribute): boolean; virtual;
    procedure RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean); virtual;
    function Clone(aParent: TPersistent): TEarthAttribute; virtual;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
  end;

  TEarthAttributeClass = class of TEarthAttribute;

  //Pen for GISObjects
  TEarthPen = class(TEarthAttribute)
  private
    FParent: TPersistent;
    fLastProjectionAltitude: double;
    FPenColor: TColor32;
    FPenStyle: TPenStyle;
    FPenWidth: double;   //Store Pen Width
    FPenUnit: TEarthUnitTypes;
    FDrawPenWidth: double;  //Real Draw Pen Width
    procedure SetPenColor(Value: TColor32);
    procedure SetPenStyle(Value: TPenStyle);
    procedure SetPenWidth(Value: double);
    procedure SetPenUnit(Value: TEarthUnitTypes);
  public
    constructor Create(aParent: TPersistent); virtual;
    procedure RedrawObject; override;
    procedure Define(aColor: TColor32; aStyle: TPenStyle; aWidth: integer; aUnit: TEarthUnitTypes);

    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    procedure WriteProperties(Writer: TEarthStreamWriter); override;
    procedure ReadProperties(Reader: TEarthStreamReader); override;
    procedure RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean); override;
    procedure Assign(Attr: TPersistent); override;
    function Clone(aParent: TPersistent): TEarthAttribute; override;
    function IsEquals(Attr: TEarthAttribute): boolean; override;
    function GetDrawPenWidthInPixels: double;
    //Real Draw Pen Width
    property DrawPenWidth: double read FDrawPenWidth;
    property PenStyle: TPenStyle read FPenStyle write SetPenStyle;
  published
    property PenColor: TColor32 read FPenColor write SetPenColor;
    property PenWidth: double read FPenWidth write SetPenWidth;
    property PenUnit: TEarthUnitTypes read FPenUnit write SetPenUnit;
  end;

  TEarthPenClass = class of TEarthPen;

  //Brush for GisObjects
  TEarthBrush = class(TEarthAttribute)
  private
    FParent: TPersistent;
    FBrushColor: TColor32;
    FBrushStyle: TEarthBrushStyle;
    FBrushBkColor: TColor32;
    FBrushFillImage: TBitmap32;
    FDrawFillWithImage: boolean;
    procedure SetBrushColor(Value: TColor32);
    procedure SetBrushBkColor(const Value: TColor32);
    procedure SetBrushStyle(Value: TEarthBrushStyle);
    procedure SetBrushFillImage(Val: TBitmap32);
    procedure SetDrawFillWithImage(Val: boolean);
    procedure OnBrushFillImageChanged(Sender: TObject);
  public
    constructor Create(aParent: TPersistent); virtual;
    destructor Destroy; override;
    procedure RedrawObject; override;
    procedure Define(Color: TColor; Style: TEarthBrushStyle);
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    procedure WriteProperties(Writer: TEarthStreamWriter); override;
    procedure ReadProperties(Reader: TEarthStreamReader); override;
    procedure RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean); override;
    procedure Assign(Attr: TPersistent); override;
    function Clone(aParent: TPersistent): TEarthAttribute; override;
    function IsEquals(Attr: TEarthAttribute): boolean; override;
  published
    property BrushBkColor: TColor32 read FBrushBkColor write SetBrushBkColor;
    property BrushColor: TColor32 read FBrushColor write SetBrushColor;
    property BrushStyle: TEarthBrushStyle read FBrushStyle write SetBrushStyle;
    property BrushFillImage: TBitmap32 read FBrushFillImage write SetBrushFillImage;
    property DrawFillWithImage: boolean read FDrawFillWithImage write SetDrawFillWithImage;
  end;

  TEarthBrushClass = class of TEarthBrush;

  //Font for GisObjects
  TEarthFont = class(TEarthAttribute)
  private
    FParent: TPersistent;
    fLastProjectionAltitude: double;
    fDrawHeight: double;
    fDrawOutLineWidth: double;

    FFontFillColor: TColor32;
    FFontOutlineColor: TColor32;
    fFontOutlineUnits: TEarthUnitTypes;
    FFontSize: double; { Size of text in text units }
    FFontOutlineSize: double;
    FFontSizeUnits: TEarthUnitTypes;
    FFontAngle: double;
    FStyle: integer;
    FFontName: TEarthFontName;
    FFontFillImage: TBitmap32;
    fDrawOutLine: boolean;
    fDrawFillWithColor: boolean;
    FDrawFillWithImage: boolean;
    function GetStyle: TFontStyles;
    procedure SetStyle(Value: TFontStyles);

    procedure SetFontFillColor(Value: TColor32);
    procedure SetFontOutlineColor(Value: TColor32);
    procedure SetFontOutlineUnits(Value: TEarthUnitTypes);
    procedure SetFontOutlineSize(Value: double);
    procedure SetFontAngle(Value: double);
    procedure SetFontSize(Value: double);
    procedure SetFontSizeUnits(Value: TEarthUnitTypes);
    procedure SetFontName(Value: TEarthFontName);
    procedure SetDrawOutLine(Value: boolean);
    procedure SetDrawFillWithColor(Value: boolean);
    procedure SetFontFillImage(Val: TBitmap32);
    procedure SetDrawFillWithImage(Val: boolean);
    procedure OnFontFillImageChanged(Sender: TObject);
  public
    constructor Create(aParent: TPersistent); virtual;
    destructor Destroy; override;
    procedure RedrawObject; override;
    procedure Define(const Name: TEarthFontName; FillColor, OutLineColor: TColor32; Size: integer; AUnit: TEarthUnitTypes;
      Angle: integer; Style: TFontStyles);
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    procedure WriteProperties(Writer: TEarthStreamWriter); override;
    procedure ReadProperties(Reader: TEarthStreamReader); override;
    procedure RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean); override;
    procedure Assign(Attr: TPersistent); override;
    function Clone(aParent: TPersistent): TEarthAttribute; override;
    function IsEquals(Attr: TEarthAttribute): boolean; override;
    function GetDrawHeightInPixels: double;
    function GetDrawOutLineWithdInPixels: double;
  published
    property FontAngle: double read fFontAngle write SetFontAngle;
    property FontFillColor: TColor32 read FFontFillColor write SetFontFillColor;
    property FontFillImage: TBitmap32 read FFontFillImage write SetFontFillImage;
    property FontOutlineColor: TColor32 read FFontOutlineColor write SetFontOutlineColor;
    property FontOutlineUnits: TEarthUnitTypes read FFontOutlineUnits write SetFontOutlineUnits;
    property FontOutlineSize: double read FFontOutlineSize write SetFontOutlineSize;
    property FontName: TEarthFontName read FFontName write SetFontName;
    property FontSize: double read FFontSize write SetFontSize;
    property FontSizeUnits: TEarthUnitTypes read FFontSizeUnits write SetFontSizeUnits;
    property FontStyle: TFontStyles read GetStyle write SetStyle;

    property DrawOutLine: boolean read FDrawOutLine write SetDrawOutLine;
    property DrawFillWithColor: boolean read FDrawFillWithColor write SetDrawFillWithColor;
    property DrawFillWithImage: boolean read FDrawFillWithImage write SetDrawFillWithImage;
  end;

  TEarthFontClass = class of TEarthFont;


  TPresOnRenderObject = procedure(Sender: TObject; Earth: TCustomEarth; Geod: TEarthObject; State: TEarthObjectStateSet) of object;
  TPresOnRenderTitle = procedure(Sender: TObject; Earth: TCustomEarth; Geod: TEarthObject; var bDone: boolean) of object;

  //Base Presenter Class
  TEarthPresenter = class(TEarthENVPersistent)
  private
    FEarth: TCustomEarth;
    FTitleFont: TEarthFont;
    FTitleAlignment: TTitleAlignment;
    FTitleOffset: double;
    FPresenterID: integer;
    FName: string;
    FHidden: boolean;
    FParent: TEarthPresenterStore;

    fOnRenderObject: TPresOnRenderObject;
    fOnRenderTitle: TPresOnRenderTitle;

    procedure SetHidden(const val: boolean);
    procedure SetTitleAlignment(const val: TTitleAlignment);
    procedure SetTitleFont(val: TEarthFont);
    procedure SetTitleOffset(const val: double);
    procedure SetPresenterID(const val: integer);
    procedure SetName(const val: string);
  public
    constructor Create(aPresenterStore: TEarthPresenterStore; iID: integer); virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure RedrawObject; override;
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
    procedure RenderObject(Earth: TCustomEarth; Geod: TEarthObject; State: TEarthObjectStateSet); virtual;
    procedure RenderTitle(Earth: TCustomEarth; Geod: TEarthObject; var bDone: boolean); virtual;
    procedure AdjustObjectMER(Geod: TEarthObject; var ObjectMER: TMER); virtual;
    function Clone(aPresenterStore: TEarthPresenterStore): TEarthPresenter; virtual;
    function LLInObject(Earth: TCustomEarth; ptLL: TPointLL; Geod: TEarthObject; iTolerance: double): boolean; virtual;

    property ParentEarth: TCustomEarth read FEarth write FEarth;
    property Parent: TEarthPresenterStore read FParent;

    property OnRenderObject: TPresOnRenderObject read FOnRenderObject write FOnRenderObject;
    property OnRenderTitle: TPresOnRenderTitle read FOnRenderTitle write FOnRenderTitle;
  published
    {Set this value to false and all objects with this Presenter
     will be Hidden}
    property Hidden: boolean read FHidden write SetHidden;
    {Presenter Name}
    property Name: string read FName write SetName;
    {Presenter ID this value used by all Gis Objects to assing a Presenter}
    property PresenterID: integer read FPresenterID write SetPresenterID;
    {Title Alignment off Render Gis Objects
     If you set TitleAlignment=taNone then Title of Gis Objects will invisible}
    property TitleAlignment: TTitleAlignment read FTitleAlignment write SetTitleAlignment;
    property TitleFont: TEarthFont read FTitleFont write SetTitleFont;
    property TitleOffset: double read FTitleOffset write SetTitleOffset;
  end;

  TEarthPresenterClass = class of TEarthPresenter;


  TEarthObjectOnRender = procedure(Sender: TObject; Earth: TCustomEarth; State: TEarthObjectStateSet) of object;
  //Base GisObject Class
  TEarthObject = class(TEarthObjectData)
  private
    FEarth: TCustomEarth;
    FParent: TEarthObjectStore;
    FLayers: TEarthLayerStore;
    FObjectID: integer;
    FiVisibleHeightMaxEU: double; //Temp for speed
    FiVisibleHeightMinEU: double; //Temp for speed
    FVisibleHeightMax: single;
    FVisibleHeightMin: single;
    FVisibleHeightUnits: THeightUnitTypes;
    FObjectGUID: string;
    fTag1: integer;
    fTag2: integer;
    fTag3: integer;
    fTag1String: string;
    fTag2String: string;
    fTag3String: string;
    FOnRender: TEarthObjectOnRender;

    procedure SetSelected(aValue: boolean);
    function GetSelected: boolean;
    procedure SetHidden(aValue: boolean);
    function GetHidden: boolean;
    procedure SetClosed(aValue: boolean);
    function GetClosed: boolean;
  protected
    FTitle: string;
    FCentroid: TPointLL;
    procedure SetPresenterID(const aValue: integer); override;
    function GetCentroid: TPointLL; virtual;
    procedure SetCentroid(const ptLL: TPointLL); virtual;
    procedure SetTitle(const val: string);
    procedure SetVisibleHeightMin(const Val: single);
    procedure SetVisibleHeightMax(const Val: single);
    procedure SetVisibleHeightUnits(const Val: THeightUnitTypes);
  public
    MRU: integer;
    constructor Create(aParent: TEarthObjectStore); virtual;
    destructor Destroy; override;
    procedure Clear;
    function Clone(aParent: TEarthObjectStore): TEarthObject; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
    procedure RedrawObject; override;
    procedure Render(Earth: TCustomEarth; State: TEarthObjectStateSet); virtual;
    procedure UnRender; virtual;
    procedure SelectedChanged; virtual;
    procedure FixupReferences; virtual;
    function ObjectInstanceSize: integer; override;
    function GetObjectMER: TMER; override;
    function LLInObject(ptLL: TPointLL; iTolerance: double): boolean; virtual;
    function LLInObjectMER(ptLL: TPointLL; iTolerance: double): integer; virtual;
    procedure MoveDelta(DLongX, DLatY: double); virtual;
    procedure MoveTo(ToPoint: TPointLL); virtual;
    function CanMoveMinus: boolean;
    function CanMovePlus: boolean;
    function GetVisibleHeightMinAsEU: double;
    function GetVisibleHeightMaxAsEU: double;
    function IsObjectMERVisible(aProjection: TEarthProjection): boolean; virtual;

    procedure SetCenter(const X, Y: double);
    procedure SetCenterStr(const X, Y: string);
    procedure SetCenterStrFmt(const X, Y, Fmt: string);
    procedure GetCenterStrFmt(var X, Y: string; const Fmt: string);
    procedure SetCentroidX(const X: double);
    function GetCentroidX: double;
    procedure SetCentroidY(const Y: double);
    function GetCentroidY: double;

    property ParentEarth: TCustomEarth read FEarth;
    property Parent: TEarthObjectStore read FParent;
    property Layers: TEarthLayerStore read fLayers;
    property GISMER: TMER read GetObjectMER;
    property Centroid: TPointLL read GetCentroid write SetCentroid;
    property CentroidX: double read GetCentroidX write SetCentroidX;
    property CentroidY: double read GetCentroidY write SetCentroidY;

    property Tag1: integer read fTag1 write fTag1;
    property Tag2: integer read fTag2 write fTag2;
    property Tag3: integer read fTag3 write fTag3;
    property Tag1String: string read fTag1String write fTag1String;
    property Tag2String: string read fTag2String write fTag2String;
    property Tag3String: string read fTag3String write fTag3String;

    property OnRender: TEarthObjectOnRender read fOnRender write fOnRender;

  published
    property Closed: boolean read GetClosed write SetClosed;
    property Hidden: boolean read GetHidden write SetHidden;
    property ID: integer read FObjectID write FObjectID;
    //A string for user use
    property GUID: string read FObjectGUID write FObjectGUID;
    property Selected: boolean read GetSelected write SetSelected;
    property Title: string read FTitle write SetTitle;
    property VisibleHeightMin: single read fVisibleHeightMin write SetVisibleHeightMin;
    property VisibleHeightMax: single read fVisibleHeightMax write SetVisibleHeightMax;
    property VisibleHeightUnits: THeightUnitTypes read fVisibleHeightUnits write SetVisibleHeightUnits;
  end;

  TEarthObjectClass = class of TEarthObject;

  //Class for store EarthLayers
  TEarthLayerStore = class(TEarthENVPersistent)
  private
    FiCount: integer;
    FLayers: TObjectList;//DynArray;
    FEarth: TCustomEarth;
    FSelectedObject: TEarthObject;
    FGlobalPresenters: TEarthPresenterStore;
    FParent: TPersistent;
  protected
    function GetLayer(iIndex: integer): TEarthLayer; virtual;
    function GetLayerStoreMER: TMER; virtual;
    procedure SetCount(iValue: integer); virtual;
    function GetParentLayer: TEarthLayer;
  public
    //aParent Can be TCustomEarth or a TEarthLayer or TEarthObject
    constructor Create(aParent: TPersistent); virtual;
    destructor Destroy; override;
    procedure RedrawObject; override;
    procedure Assign(Source: TPersistent); override;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
    procedure WriteToSteam(aStream: TStream);
    procedure ReadFromSteam(aStream: TStream);
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    procedure Clear; virtual;
    function IndexOf(ALayer: TEarthLayer): integer; virtual;
    function IndexByName(const sName: string): integer; virtual;
    function ByName(const sName: string): TEarthLayer; virtual;
    function AddNew: TEarthLayer;
    procedure Add(ALayer: TEarthLayer); virtual;
    procedure Remove(ALayer: TEarthLayer); virtual;
    procedure Delete(ALayer: TEarthLayer); virtual;
    procedure DeleteByName(const sName: string); virtual;
    procedure Move(iFrom, iTo: integer); virtual;

    function ObjectAtLL(ptLL: TPointLL): TEarthObject; virtual;
    function ObjectAtXY(iX, iY: integer): TEarthObject; virtual;
    function ObjectByTitle(const sTitle: string; Obj: TEarthObject): TEarthObject; virtual;
    function ObjectByID(iID: integer): TEarthObject; virtual;
    procedure ObjectsUnSelectAll;

    procedure Render(bAnimated: boolean); virtual;

    property Count: integer read FiCount write SetCount;

    property Earth: TCustomEarth read fEarth;
    property Parent: TPersistent read FParent;
    property GlobalPresenters: TEarthPresenterStore read FGlobalPresenters write FGlobalPresenters;
    property Layer[iIndex: integer]: TEarthLayer read GetLayer; default;

    property LayerStoreMER: TMER read GetLayerStoreMER;

  end;

  //Class for store GISObjects
  TEarthObjectStore = class(TEarthENVPersistent)
  private
    fParent: TEarthLayer;
    FActive: boolean;
    FNoCache: boolean;
    FEarth: TCustomEarth;
    FiCount: integer;
    FiCapacity: integer;
    FObjectArray: DynArray;
    FSubscribers: TObjectList;
    FSourceName: string;
    FPresenters: TEarthPresenterStore;
    FModified: boolean;
  protected
    bLoadingObject: boolean;
    function GetMER(iIndex: integer): TMER; virtual;
    function GetPresenterID(iIndex: integer): integer; virtual;
    function GetState(iIndex: integer): TEarthObjectStateSet; virtual;
    function GetObject(iIndex: integer): TEarthObject; virtual;
    function GetObjectData(iIndex: integer): TEarthObjectData; virtual;
    function GetUserObject(iIndex: integer): TObject; virtual;
    function GetValue(iIndex: integer): double; virtual;
    function GetLayer(iIndex: integer): TEarthLayer; virtual;
    function GetObjectsMER: TMER; virtual;

    procedure SetMER(iIndex: integer; Value: TMER); virtual;
    procedure SetPresenterID(iIndex: integer; const Value: integer); virtual;
    procedure SetCount(iValue: integer); virtual;
    procedure SetCapacity(iValue: integer); virtual;
    procedure SetState(iIndex: integer; AState: TEarthObjectStateSet); virtual;
    procedure SetValue(iIndex: integer; Value: double); virtual;
    procedure SetUserObject(iIndex: integer; Obj: TObject); virtual;
    procedure SetObjectData(iIndex: integer; Obj: TEarthObjectData); virtual;

    function LoadObject(iIndex: integer; bNewObject: boolean): TEarthObject; virtual;

    function InternalOpen: boolean; virtual;
    function InternalClose: boolean; virtual;
    procedure SetActive(const Value: boolean); virtual;
    procedure Notify(Notification: TObjectSourceNotification; Obj: TEarthObject); virtual;
  public
    constructor Create(aLayer: TEarthLayer); virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
    procedure Subscribe(aLayer: TEarthLayer); virtual;
    procedure UnSubscribe(aLayer: TEarthLayer); virtual;
    procedure SaveMetaData; virtual;
    procedure LoadMetaData; virtual;
    procedure ReadMetaData(Reader: TEarthStreamReader); virtual;
    procedure WriteMetaData(Writer: TEarthStreamWriter); virtual;
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    function MetaDataFilename(const sFilename: TFilename): string; virtual;
    procedure Open; virtual;
    procedure Close; virtual;

    procedure Add(Obj: TEarthObject); virtual;
    procedure Delete(Obj: TEarthObject); virtual;
    procedure Move(iFrom, iTo: integer); virtual;

    procedure UnloadObjects; virtual;
    function ObjectInstanceSize: integer;
    procedure ListActiveObjects(AList: TList);
    procedure Clear;
    procedure SetPresenterToAllObjects(index: integer);
    property Capacity: integer read FiCapacity write SetCapacity;
    property Earth: TCustomEarth read FEarth write FEarth;
    property EarthObject[iIndex: integer]: TEarthObject read GetObject; default;
    property EarthObjectData[iIndex: integer]: TEarthObjectData read GetObjectData write SetObjectData;
    property Layers[iIndex: integer]: TEarthLayer read GetLayer;
    property MER[iIndex: integer]: TMER read GetMER write SetMER;
    property Modified: boolean read FModified write FModified;
    property ObjectsMER: TMER read GetObjectsMER;
    property PresenterID[iIndex: integer]: integer read GetPresenterID write SetPresenterID;
    property Presenters: TEarthPresenterStore read FPresenters write FPresenters;
    property State[iIndex: integer]: TEarthObjectStateSet read GetState write SetState;
    property UserObject[iIndex: integer]: TObject read GetUserObject write SetUserObject;
    property Value[iIndex: integer]: double read GetValue write SetValue;
    property Parent: TEarthLayer read FParent;
  published
    property Active: boolean read FActive write SetActive;
    property Count: integer read FiCount write SetCount;
    property Name: string read FSourceName write FSourceName;
    property NoCache: boolean read FNoCache write FNoCache;
  end;

  TEarthObjectStoreClass = class of TEarthObjectStore;

  TEarthFileReader = class(TEarthObjectStore)
  private
    FFileName: TFileName;
  protected
    procedure SetFileName(const sFilename: TFileName); virtual;

    function MapDataHeader(aStream: TStream): boolean; virtual;
    function MapDataFile(aStream: TStream): boolean; virtual;
    function MapDataObject(aStream: TStream; iIndex: integer): boolean; virtual;
  public
    function SaveEnvironment: TGXML_Element; override;
    procedure LoadEnvironment(Element: TGXML_Element); override;
  published
    property FileName: TFileName read FFileName write SetFileName;
  end;

  TEarthFileReaderClass = class of TEarthFileReader;

  TEarthFileWriter = class(TEarthRoot)
  protected
    FLayer: TEarthLayer;
  public
    // Creates the writer for a given layer
    constructor Create(aLayer: TEarthLayer); virtual;
    // Saves to the given filename
    function SaveToFile(const Filename: TFilename): boolean; virtual;
  end;

  TEarthFileWriterClass = class of TEarthFileWriter;

  //Class to store Presenters
  TEarthPresenterStore = class(TEarthENVPersistent)
  private
    FEarth: TCustomEarth;
    FParent: TPersistent;   //Can be TEarthLayer or TEarthObjectStore or TEarthLayerStore
    FPresenters: TObjectList;//DynArray;
    FModified: boolean;
  protected
    function GetPresenter(iIndex: integer): TEarthPresenter; virtual;
    function GetCount: integer; virtual;
    function Add(aPresenter: TEarthPresenter): integer; virtual;
  public
    constructor Create(aParent: TPersistent); virtual;//Can be TEarthLayer or TEarthObjectStore or TEarthLayerStore
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
    procedure RedrawObject; override;
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    procedure Move(iFrom, iTo: integer);
    function ByID(iPresenterID: integer; bGlobal: boolean): TEarthPresenter; virtual;
    function ByName(const aName: string; bGlobal: boolean): TEarthPresenter; virtual;
    procedure Delete(aPresenter: TEarthPresenter); virtual;
    procedure Clear; virtual;
    property Count: integer read GetCount;
    property Earth: TCustomEarth read fEarth;
    property Modified: boolean read FModified write FModified;
    property Presenters[iIndex: integer]: TEarthPresenter read GetPresenter; default;
    property Parent: TPersistent read FParent;
  end;

  //Base Layer Class
  TEarthLayer = class(TEarthENVPersistent)
  private
    FParent: TEarthLayerStore;  //Link to parent ParentLayerStore
    FLayers: TEarthLayerStore;
    FEarth: TCustomEarth;
    FsLayerName: string;
    fTag: integer;
    fTagString: string;
    FLayerMER: TMER;
    FPresenters: TEarthPresenterStore;
    FObjects: TEarthObjectStore;

    FbAnimated: boolean;
    FbEnabled: boolean;
    FbShowTitles: boolean;
    FbVisible: boolean;
    FbNoInterrupt: boolean;

    FiVisibleHeightMaxEU: double; //Temp for speed
    FiVisibleHeightMinEU: double; //Temp for speed

    FVisibleHeightMax: single;
    FVisibleHeightMin: single;
    FVisibleHeightUnits: THeightUnitTypes;

    FUserLayerObject: TObject;
    FModified: boolean;
    FLayerColorAdjustPercent: integer;
    FLayerColorAdjustTarget: TColor;

    FFullFileName: string;
    FFilePath: string;
    FFileName: string;
  protected
    procedure SetObjects(Value: TEarthObjectStore);
    function GetObjects: TEarthObjectStore;
    function GetLayerMER: TMER;
    function GetIndex: integer;
    function GetEarthCanvas: TEarthCanvas;
    function GetProjection: TEarthProjection;
    procedure SetIndex(iIndex: integer);
    function GetLayerName: string;
    procedure SetLayerName(const Value: string);
    procedure SetLayerBoolean(iIndex: integer; bValue: boolean);
    procedure SetLayerValue(iIndex: integer; Value: integer);
    procedure SetVisibleHeightMin(const Val: single);
    procedure SetVisibleHeightMax(const Val: single);
    procedure SetVisibleHeightUnits(const Val: THeightUnitTypes);
    procedure SetFullFileName(const Value: string);
    procedure SetParent(aLayerStore: TEarthLayerStore);
  public
    LayerState: TEarthLayerStateSet;
    constructor Create(aLayerStore: TEarthLayerStore); virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure WriteProperties(Writer: TEarthStreamWriter); virtual;
    procedure ReadProperties(Reader: TEarthStreamReader); virtual;
    procedure LoadEnvironment(Element: TGXML_Element); override;
    function SaveEnvironment: TGXML_Element; override;
    function Clone(aLayerStore: TEarthLayerStore): TEarthLayer;
    procedure Clear; virtual;
    class function PrintableClassName: string; override;
    procedure RenderObjects; virtual;
    procedure RenderObjectsTitles;
    procedure RenderTitle(Obj: TEarthObject; aPresenter: TEarthPresenter); virtual;
    procedure Delete; virtual;
    procedure Notify(Notification: TObjectSourceNotification; Obj: TEarthObject); virtual;
    function IsLayerMERVisible: boolean; virtual;
    function FindPresenter(iPresenterID: integer): TEarthPresenter;
    function ObjectAtLL(ptLL: TPointLL): TEarthObject; virtual;
    function ObjectAtXY(iX, iY: integer): TEarthObject; virtual;
    function ObjectByTitle(const sTitle: string; Obj: TEarthObject): TEarthObject; virtual;
    function ObjectByID(iID: integer): TEarthObject; virtual;
    function SelectedCount: integer; virtual;
    function UpdateObjectState(idx: integer): TEarthObjectStateSet;

    procedure SetPresenterToAllObjects(index: integer);
    function CanMoveMinus: boolean;
    function CanMovePlus: boolean;
    function GetVisibleHeightMinAsEU: double;
    function GetVisibleHeightMaxAsEU: double;

    property Earth: TCustomEarth read FEarth;
    property EarthCanvas: TEarthCanvas read GetEarthCanvas;
    property Parent: TEarthLayerStore read fParent write SetParent;
    property Layers: TEarthLayerStore read FLayers;
    property LayerMER: TMER read GetLayerMER;
    property Modified: boolean read FModified write FModified;
    property Objects: TEarthObjectStore read GetObjects write SetObjects;
    property Presenters: TEarthPresenterStore read FPresenters;
    property Projection: TEarthProjection read GetProjection;
    property UserLayerObject: TObject read FUserLayerObject write FUserLayerObject;

    property FullFileName: string read FFullFileName write SetFullFileName;
    property FilePath: string read FFilePath;
    property FileName: string read FFileName;
    //user Integer
    property Tag: integer read fTag write fTag;
    //user String
    property TagString: string read fTagString write fTagString;
  published
    property Animated: boolean index 2 read FbAnimated write SetLayerBoolean;
    property Enabled: boolean index 0 read FbEnabled write SetLayerBoolean;
    property ShowTitles: boolean index 3 read FbShowTitles write SetLayerBoolean;
    property Name: string read GetLayerName write SetLayerName;
    property NoInterrupt: boolean index 4 read FbNoInterrupt write SetLayerBoolean;
    property Visible: boolean index 1 read FbVisible write SetLayerBoolean;
    property Index: integer read GetIndex write SetIndex;
    property VisibleHeightMin: single read fVisibleHeightMin write SetVisibleHeightMin;
    property VisibleHeightMax: single read fVisibleHeightMax write SetVisibleHeightMax;
    property VisibleHeightUnits: THeightUnitTypes read fVisibleHeightUnits write SetVisibleHeightUnits;

  end;

  TEarthLayerClass = class of TEarthLayer;

  //Base ProjectionModel Class
  TCustomProjectionModel = class(TEarthENVPersistent)
  private
    FProjection: TEarthProjection;
  public
    constructor Create(Parent: TEarthProjection); virtual;
    function PointStoreToXY(Points: TPointStore; iStart: integer; bClosed: boolean): integer; virtual;
    function LLToXY(iLong, iLat: double; iIndex: integer): boolean; virtual;
    function PointLLToXY(const ptLL: TPointLL; iIndex: integer): boolean; virtual;
    function XYToLL(iX, iY: double; iIndex: integer): boolean; virtual;
    function ExtentsLL: TGRectFloat; virtual;
    function XYListVisible(iCount: integer; var ClipCode: byte): boolean; virtual;
    function VisibleMER(const MER: TMER; var State: TEarthObjectStateSet): boolean; virtual;
    procedure PropertyChanged(ProjectionProperty: TProjectionProperty); virtual;
    procedure PaintSurface; virtual;
    function Clone(aParent: TEarthProjection): TCustomProjectionModel; virtual;
    procedure Assign(AModel: TPersistent); override;

    property Projection: TEarthProjection read FProjection write FProjection;
  end;

  TCustomProjectionModelClass = class of TCustomProjectionModel;

  //Base Earth Projection class
  TEarthProjection = class(TEarthENVPersistent)
  private
    FEarth: TCustomEarth;

    FiXOrigin: extended;
    FiYOrigin: extended;
    FiAltitude: extended;
    FXRotation: extended;
    FYRotation: extended;
    FZRotation: extended;

    FMaxLatitude: TEarthDegree;
    FMinLatitude: TEarthDegree;
    FMaxLongitude: TEarthDegree;
    FMinLongitude: TEarthDegree;

    FMaxZoom: TEarthDegree;
    FMinZoom: TEarthDegree;

    FScaleFactor: extended; { 1/FeScaleFactor = number of EarthUnits per pixel }

    FCentralMeridian: TEarthDegree;
    FCentralParallel: TEarthDegree;
    FFirstParallel: TEarthDegree;
    FSecondParallel: TEarthDegree;

    FSpheroid: TSpheroid;
    FXAxisChanges: TXAxisChanges;
    FYAxisChanges: TYAxisChanges;
    FProjectionModel: TCustomProjectionModel;

    FiGraticuleLongSteps: integer;
    FiGraticuleLatSteps: integer;
    FFlags: TProjectionFlagSet;
    FRenderBackFace: boolean;
    FUseMinMaxLimits: boolean;
    function GetProjectionClass: string;
    procedure SetProjectionClass(ProjectionClassName: string);

    function GetEarthCenterXY: TPointDouble;
    procedure SetEarthCenterXY(PT: TPointDouble);
    function GetUnitsPerInch: double;
    procedure SetUnitsPerInch(UPI: double);
  protected
    procedure SetIntegerProp(ProjectionProperty: integer; Value: integer); virtual;
    procedure SetFloatProp(ProjectionProperty: integer; Value: extended); virtual;
    procedure SetProjectionModel(Value: TCustomProjectionModel); virtual;
    procedure SetUseMinMaxLimits(const Value: boolean); virtual;
    procedure SetXAxisChanges(Value: TXAxisChanges); virtual;
    procedure SetYAxisChanges(Value: TYAxisChanges); virtual;
    procedure SetScaleFactor(eSF: extended); virtual;
    procedure SetAltitude(Value: extended); virtual;
    procedure SetSpheroid(Value: TSpheroid); virtual;
    procedure SetCenterLat(Value: extended);
    procedure SetCenterLong(Value: extended);
    function GetCenterLat: extended;
    function GetCenterLong: extended;
  public
    constructor Create(ParentEarth: TCustomEarth); virtual;
    destructor Destroy; override;

    function SaveEnvironment: TGXML_Element; override;
    procedure LoadEnvironment(Element: TGXML_Element); override;

    procedure Assign(Source: TPersistent); override;
    function Clone: TEarthProjection; virtual;

    function DeviceXYToLL(iX, iY: double; var ptLL: TPointLL): boolean;
    function LLToDeviceXY(iLong, iLat: double; var ptXY: TPointDouble): boolean;
    //  function  PointLLToXY(ptLL : TPointLL; var ptXY : TPoint) : Boolean; Overload;
    function PointLLToXY(ptLL: TPointLL; var ptXY: TPointDouble): boolean; overload;
    function ExtentsLL: TGRectFloat;
    procedure SetCenterLatlon(const aCenterLatitude, aCenterLongitude: TEarthDegree);
    property GraticuleLongSteps: integer read FiGraticuleLongSteps write FiGraticuleLongSteps;
    property GraticuleLatSteps: integer read FiGraticuleLatSteps write FiGraticuleLatSteps;
    property Flags: TProjectionFlagSet read FFlags write FFLags;
    property RenderBackFace: boolean read FRenderBackFace write FRenderBackFace;

    property ProjectionModel: TCustomProjectionModel read FProjectionModel write SetProjectionModel;
    property Earth: TCustomEarth read FEarth;
    property UnitsPerInch: double read GetUnitsPerInch write SetUnitsPerInch;
    property CenterXY: TPointDouble read GetEarthCenterXY write SetEarthCenterXY;

  published
    property CentralMeridian: TEarthDegree index Ord(ppCentralMeridian) read FCentralMeridian write SetFloatProp;
    property CentralParallel: TEarthDegree index Ord(ppCentralParallel) read FCentralParallel write SetFloatProp;
    property FirstParallel: TEarthDegree index Ord(ppFirstParallel) read FFirstParallel write SetFloatProp;
    property SecondParallel: TEarthDegree index Ord(ppSecondParallel) read FSecondParallel write SetFloatProp;

    property Altitude: extended read FiAltitude write SetAltitude;
    property CenterLatitude: TEarthDegree read GetCenterLat write SetCenterLat;
    property CenterLongitude: TEarthDegree read GetCenterLong write SetCenterLong;
    property MaxLatitude: TEarthDegree index Ord(ppMaxLatitude) read FMaxLatitude write SetFloatProp;
    property MaxLongitude: TEarthDegree index Ord(ppMaxLongitude) read FMaxLongitude write SetFloatProp;
    property MaxZoom: TEarthDegree index Ord(ppMaxZoom) read FMaxZoom write SetFloatProp;
    property MinLatitude: TEarthDegree index Ord(ppMinLatitude) read FMinLatitude write SetFloatProp;
    property MinLongitude: TEarthDegree index Ord(ppMinLongitude) read FMinLongitude write SetFloatProp;
    property MinZoom: TEarthDegree index Ord(ppMinZoom) read FMinZoom write SetFloatProp;
    property ProjectionClass: TProjectionClass read GetProjectionClass write SetProjectionClass;
    property ScaleFactor: extended read FScaleFactor write SetScaleFactor;
    property Spheroid: TSpheroid read FSpheroid write SetSpheroid default WGS84;
    property UseMinMaxLimits: boolean read FUseMinMaxLimits write SetUseMinMaxLimits;
    property XRotation: TEarthDegree index Ord(ppXRotation) read FXRotation write SetFloatProp;
    property YRotation: TEarthDegree index Ord(ppYRotation) read FYRotation write SetFloatProp;
    property ZRotation: TEarthDegree index Ord(ppZRotation) read FZRotation write SetFloatProp;
    property XOrigin: extended index Ord(ppXOrigin) read FiXOrigin write SetFloatProp;
    property YOrigin: extended index Ord(ppYOrigin) read FiYOrigin write SetFloatProp;
    property XAxisChanges: TXAxisChanges read FXAxisChanges write SetXAxisChanges;
    property YAxisChanges: TYAxisChanges read FYAxisChanges write SetYAxisChanges;
  end;

  TEarthProjectionClass = class of TEarthProjection;

  //Earth Canvas class
  TEarthCanvas = class(TCustomEarthCanvas)
  private
    FEarth: TCustomEarth;
    FDefPen: TEarthPen;
    FDefBrush: TEarthBrush;
    FDefFont: TEarthFont;
    FDefTitleFont: TEarthFont;
    function GetProjection: TEarthProjection;
  protected
    procedure SetDefFont(val: TEarthFont);
    procedure SetDefTitleFont(val: TEarthFont);
  public
    constructor Create(ParentEarth: TCustomEarth); virtual;
    destructor Destroy; override;
    procedure SizeBMP(iWidth, iHeight: integer; iPixelsPerInch: double);
    function ClipPoly(iCount: integer; State: TEarthObjectStateSet; iPenWidth: double): integer;
    procedure DrawClippedPoly(iPoints: integer; State: TEarthObjectStateSet; iPenWidth: extended);
    procedure DrawVectorMatrixLine(vec: TV3D; const mat: TGMatrix; iHeight: double; iSteps: integer);
    function ResizeProjection(iWidth, iHeight: double): boolean;
    function FontRenderHeight(FontSize: extended; FontUnits: TEarthUnitTypes): extended;
    function EarthUnitsFrom(const eValue: extended; Units: TEarthUnitTypes): extended;
    function HeightUnitsFrom(const eValue: extended; Units: THeightUnitTypes): extended;
    function ScaleUnitsToDevice(Value: extended; Units: TEarthUnitTypes): extended;
    procedure Assign(Source: TPersistent); override;
    function Clone: TEarthCanvas;
    procedure RenderChainStore(Chains: TGroupsStore; State: TEarthObjectStateSet; iPenWidth: double);
    procedure RenderTextureMap(const TextureMap: TTextureData);
    procedure RenderTextureTriangle(WorldTri: TTriangleLL; const TextureMap: TTextureData);
    function RenderShadow(GMTDateTime: TDateTime; ColorNight, ColorTwilight: TColor; Density: integer): TPointLL;
    function RenderDIBShadow(GMTDateTime: TDateTime; ColorNight, ColorTwilight: TColor; Density: integer): TPointLL;
    procedure RenderPolygon(const Points: array of TPointLL);
    procedure RenderPolyLine(const Points: array of TPointLL);
    procedure RenderCircle(const ptLL: TPointLL; iRadius, iSteps: integer);
    procedure RenderLine(const ptFromLL, ptToLL: TPointLL; iSteps: integer);
    procedure RenderGreatCircleLine(const ptFromLL, ptToLL: TPointLL; CircleType: TGreatCircle);
    procedure RenderMER(const aMer: Tmer);
    procedure RenderProjectedMER(const aMer: Tmer);
    procedure RenderBox(const aCenterXY: TPoint; DX, DY: integer); overload;
    procedure RenderBox(const aCenterXY: TPointDouble; DX, DY: double); overload;
    procedure RenderBoxLL(const aCenterLL: TPointLL; DX, DY: integer); overload;
    procedure RenderBoxLL(const aCenterLL: TPointLL; DX, DY: double); overload;

    property DefPen: TEarthPen read FDefPen;
    property DefBrush: TEarthBrush read FDefBrush;
    property DefFont: TEarthFont read FDefFont write SetDefFont;
    property DefTitleFont: TEarthFont read FDefTitleFont write SetDefTitleFont;
    property Earth: TCustomEarth read FEarth write FEarth;
    property Projection: TEarthProjection read GetProjection;
  end;

  TEarthCanvasClass = class of TEarthCanvas;

  TEarthTransparent = class(TEarthRoot)
  private
    FPen: TEarthPen;
    FBrush: TEarthBrush;
    fOnChange: TNotifyEvent;
  protected
    procedure SetPen(aPen: TEarthPen);
    procedure SetBrush(aBrush: TEarthBrush);
    procedure DoChange(Sender: TObject);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure RedrawObject; override;
    procedure Clear;
    function SaveEnvironment: TGXML_Element;
    procedure LoadEnvironment(Element: TGXML_Element);
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  published
    property Pen: TEarthPen read FPen write SetPen;
    property Brush: TEarthBrush read FBrush write SetBrush;
  end;

  TSelectedOjectOptions = class(TEarthRoot)
  private
    FPen: TEarthPen;
    FBrush: TEarthBrush;
    FFont: TEarthFont;
    FBrushSameWithNormal: boolean;
    FPenWidthSameWithNormal: boolean;
    FFontSameWithNormal: boolean;
    fOnChange: TNotifyEvent;
  protected
    procedure SetPen(aPen: TEarthPen);
    procedure SetBrush(aBrush: TEarthBrush);
    procedure SetFont(aFont: TEarthFont);
    procedure SetBrushSameWithNormal(const val: boolean);
    procedure SetPenWidthSameWithNormal(const val: boolean);
    procedure SetFontSameWithNormal(const val: boolean);
    procedure DoChange(Sender: TObject);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure RedrawObject; override;
    procedure Clear;
    function SaveEnvironment: TGXML_Element;
    procedure LoadEnvironment(Element: TGXML_Element);
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  published
    property Pen: TEarthPen read FPen write SetPen;
    property Brush: TEarthBrush read FBrush write SetBrush;
    property Font: TEarthFont read FFont write SetFont;
    property FontSameWithNormal: boolean read FFontSameWithNormal write SetFontSameWithNormal;
    property BrushSameWithNormal: boolean read FBrushSameWithNormal write SetBrushSameWithNormal;
    property PenWidthSameWithNormal: boolean read FPenWidthSameWithNormal write SetPenWidthSameWithNormal;
  end;

  //Base Earth class
  TCustomEarth = class(TCustomImage32)
  protected
    //FEarthFont : TEarthFont;
    FCacheMemoryUsed: integer;
    FCacheMRU: integer;
    FCacheCapacity: integer;
    FLayerStore: TEarthLayerStore;
    FProjection: TEarthProjection;
    FEarthCanvas: TEarthCanvas;
    FEarthOptions: TEarthOptionsSet;
    FBackgroundTextureMap: TTextureData;
    FiMaxTextHeight: double;
    FiMinTextHeight: double;
    FViewRect: TRect;
    FSelectedOjectOptions: TSelectedOjectOptions;
    FTransparentOptions: TEarthTransparent;
    FScrollBars: TScrollStyle;
    FbRedrawLayers: boolean; { redraws all layers }
    bRenderToCanvas: boolean;
    PaintAbort: boolean; { If True then the Painting will terminate }
    FDataDirectory: string;
    FTitlesRenderMethod: TTitlesRenderMethod;
    // Selected Object
    FSelectedLayer: TEarthLayer;                       // By sternas
    FSelectedPresentersStore: TEarthPresenterStore;    // By sternas
    FSelectedObjectStore: TEarthObjectStore;           // By sternas
    FSelectedObjects: TList;
    // Events
    FOnSelectedLayerChange: TNotifyEvent;
    FOnObjectSelectChange: TEarthObjectNotifyEvent;
    FOnPresenterStoreChange: TPresenterStoreNotifyEvent;
    FOnObjectStoreChange: TObjectStoreNotifyEvent;

    function NextMRU: integer; virtual;
    function GetMaxTextHeight: double; virtual;
    function GetMinTextHeight: double; virtual;
    procedure SetDataDirectory(const Value: string); virtual;
    procedure SetEarthOptions(Value: TEarthOptionsSet); virtual;
    procedure SetLongitudeStep(Value: double); virtual;
    procedure SetLatitudeStep(Value: double); virtual;
    procedure SetMaxTextHeight(iValue: double); virtual;
    procedure SetMinTextHeight(iValue: double); virtual;
    procedure SetScrollBars(Value: TScrollStyle); virtual;
    procedure SetViewRect(const sRect: TRect); virtual;
    procedure SeTTitlesRenderMethod(const val: TTitlesRenderMethod); virtual;
    procedure SetSelectedLayer(aLayer: TEarthLayer);
    function GetSelectedObjectCount: integer;
    function GetSelectedObjects(aIndex: integer): TEarthObject;
    function GetSelectedObject: TEarthObject;
    procedure SetSelectedObject(aEarthObject: TEarthObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Notify(aNotification: TEarthNotification; Obj: TEarthRoot); virtual; abstract;
    procedure RedrawLayers; virtual; abstract;
    function ResolveFilename(const sFilename: TFilename): TFilename; virtual; abstract;
    procedure UpdateScrollBars; virtual; abstract;
    function ProgressMessage(MsgType: TGISProgressMessage; const sMsg: string): boolean; virtual; abstract;

    procedure LocateToLL(const iLong, iLat: double);
    procedure LocateToLL2(const ptLL: TPointLL);
    procedure LocateToXY(const X, Y: integer);
    procedure LocateToObject(Obj: TEarthObject);
    procedure LocateToCenter;

    function MouseXYToLL(iX, iY: double; var ptLL: TPointLL): boolean;
    function LLToMouseXY(iLong, iLat: double; var ptXY: TPoint): boolean;

    function UpdateTotalMemoryInUse: integer; virtual; abstract;
    function CacheUsedDelta(delta: integer): integer; virtual; abstract;
    function ReduceCacheMemory(iTargetSize: integer): integer; virtual; abstract;
    procedure RenderToCanvas(ACanvas: TCanvas; ARect: TRect); virtual; abstract;

    property SelectedLayer: TEarthLayer read FSelectedLayer write SetSelectedLayer;
    property SelectedPresentersStore: TEarthPresenterStore read FSelectedPresentersStore write FSelectedPresentersStore;
    property SelectedObjectStore: TEarthObjectStore read FSelectedObjectStore write FSelectedObjectStore;
    property SelectedObjectsCount: integer read GetSelectedObjectCount;
    property SelectedObjects[aIndex: integer]: TEarthObject read GetSelectedObjects;
    property SelectedObject: TEarthObject read GetSelectedObject write SetSelectedObject;

    property Layers: TEarthLayerStore read FLayerStore;
    property BackgroundTextureMap: TTextureData read FBackgroundTextureMap;
    property EarthCanvas: TEarthCanvas read FEarthCanvas;
    property ViewRect: TRect read FViewRect write SetViewRect;
    property MouseCapture;
    property Canvas;
  published
    property CacheCapacity: integer read FCacheCapacity write FCacheCapacity default 1024;
    property DataDirectory: string read FDataDirectory write SetDataDirectory;
    property EarthOptions: TEarthOptionsSet read FEarthOptions write SetEarthOptions;
    property MinTextHeight: double read GetMinTextHeight write SetMinTextHeight;
    property MaxTextHeight: double read GetMaxTextHeight write SetMaxTextHeight;
    property TitlesRenderMethod: TTitlesRenderMethod read FTitlesRenderMethod write SeTTitlesRenderMethod default trtAfterLayer;
    property Projection: TEarthProjection read FProjection write FProjection;
    property ScrollBars: TScrollStyle read FScrollBars write SetScrollBars default ssBoth;
    property OnSelectedLayerChange: TNotifyEvent read FOnSelectedLayerChange write FOnSelectedLayerChange;
    property OnObjectSelectChange: TEarthObjectNotifyEvent read FOnObjectSelectChange write FOnObjectSelectChange;
    property OnPresenterStoreChange: TPresenterStoreNotifyEvent read FOnPresenterStoreChange write FOnPresenterStoreChange;
    property OnObjectStoreChange: TObjectStoreNotifyEvent read FOnObjectStoreChange write FOnObjectStoreChange;

  end;

implementation

{$R SEarthBrushes.res}// Include the Brush bitmap resource

uses GIS_Resource, GIS_EarthBase, GIS_Projections, GIS_EarthObjects, GIS_Presenters, GIS_EarthUtils, Math;

//===============================================================================================
//===================== TEarthProjection ========================================================
//===============================================================================================

constructor TEarthProjection.Create(ParentEarth: TCustomEarth);
begin
  inherited Create;

  Assert(ParentEarth <> nil, 'TEarthProjection.Create passed nil parent');

  FEarth := ParentEarth;
  FProjectionModel := TSphericalPrj.Create(Self);

  FYAxisChanges := yaCentralMeridian;
  FXAxisChanges := xaYOrigin;
  FSpheroid := WGS84;

  FMinLongitude := -180.0;
  FMinLatitude := -90.0;
  FMaxLongitude := 180.0;
  FMaxLatitude := 90.0;

  FXRotation := 23.5;
  FYRotation := 0;
  FZRotation := 0;

  FCentralMeridian := 0;
  FFirstParallel := 0;
  FSecondParallel := 0;

  with Earth.EarthCanvas, ExtentsLL do
  begin
    FScaleFactor := min(CanvasWidth / Abs(Right - Left), CanvasHeight / Abs(Bottom - Top));

    if FScaleFactor > GMAXSCALEFACTOR then
      FScaleFactor := GMAXSCALEFACTOR;
    if FScaleFactor < GEPSILON then
      FScaleFactor := GEPSILON;

    FiXOrigin := CanvasWidth / 2;
    FiYOrigin := CanvasHeight / 2;
  end;
end;

destructor TEarthProjection.Destroy;
begin
  ProjectionModel.Free;
  FProjectionModel := nil;
  inherited Destroy;
end;

procedure TEarthProjection.Assign(Source: TPersistent);
var
  Prj: TEarthProjection;
begin
  if Source is TEarthProjection then
  begin
    Prj := TEarthProjection(Source);

    FiXOrigin := Prj.XOrigin;
    FiYOrigin := Prj.YOrigin;
    FiAltitude := Prj.Altitude;
    FXRotation := Prj.XRotation;
    FYRotation := Prj.YRotation;
    FZRotation := Prj.ZRotation;

    FMaxLatitude := Prj.MaxLatitude;
    FMinLatitude := Prj.MinLatitude;
    FMaxLongitude := Prj.MaxLongitude;
    FMinLongitude := Prj.MinLongitude;

    FMaxZoom := Prj.MaxZoom;
    FMinZoom := Prj.MinZoom;

    FScaleFactor := Prj.ScaleFactor;

    FCentralMeridian := Prj.CentralMeridian;
    FCentralParallel := Prj.CentralParallel;
    FFirstParallel := Prj.FirstParallel;
    FSecondParallel := Prj.SecondParallel;

    FSpheroid := Prj.Spheroid;
    FXAxisChanges := Prj.XAxisChanges;
    FYAxisChanges := Prj.YAxisChanges;

    FiGraticuleLongSteps := Prj.GraticuleLongSteps;
    FiGraticuleLatSteps := Prj.GraticuleLatSteps;
    FFlags := Prj.Flags;
    FRenderBackFace := Prj.RenderBackFace;

    FProjectionModel.Free;
    FProjectionModel := Prj.ProjectionModel.Clone(Self);
  end
  else
    inherited Assign(Source);
end;

function TEarthProjection.Clone: TEarthProjection;
begin
  Result := TEarthProjectionClass(Self.ClassType).Create(Earth);
  Result.Assign(Self);
end;

function TEarthProjection.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddFloatAttribute('ScaleFactor', ScaleFactor);

  Result.AddFloatAttribute('CentralMeridian', CentralMeridian);
  Result.AddFloatAttribute('CentralParallel', CentralParallel);
  Result.AddFloatAttribute('FirstParallel', FirstParallel);
  Result.AddFloatAttribute('SecondParallel', SecondParallel);

  Result.AddFloatAttribute('MaxLatitude', MaxLatitude);
  Result.AddFloatAttribute('MaxLongitude', MaxLongitude);
  Result.AddFloatAttribute('MaxZoom', MaxZoom);
  Result.AddFloatAttribute('MinLatitude', MinLatitude);
  Result.AddFloatAttribute('MinLongitude', MinLongitude);
  Result.AddFloatAttribute('MinZoom', MinZoom);

  Result.AddIntAttribute('Spheroid', Ord(Spheroid));
  Result.AddFloatAttribute('XRotation', XRotation);
  Result.AddFloatAttribute('YRotation', YRotation);
  Result.AddFloatAttribute('ZRotation', ZRotation);
  Result.AddFloatAttribute('XOrigin', XOrigin);
  Result.AddFloatAttribute('YOrigin', YOrigin);
  Result.AddIntAttribute('XAxisChanges', Ord(XAxisChanges));
  Result.AddIntAttribute('YAxisChanges', Ord(YAxisChanges));
  Result.AddFloatAttribute('CenterLatitude', CenterLatitude);
  Result.AddFloatAttribute('CenterLongitude', CenterLongitude);

  Result.AddElement('ProjectionModel', ProjectionModel.SaveEnvironment);
end;

procedure TEarthProjection.LoadEnvironment(Element: TGXML_Element);
var
  PrjModelElement: TGXML_Element;
  PrjModel: TCustomProjectionModel;
  sClassName: string;
  px, py: TEarthDegree;
begin
  if Element <> nil then
    with Element do
    begin
      PrjModelElement := ElementByName('ProjectionModel', 0);
      if PrjModelElement <> nil then
      begin
        // Get the ClassName of the projection model
        sClassName := PrjModelElement.AttributeByName('Class', ProjectionModel.ClassName);
        // Create the Object
        PrjModel := TCustomProjectionModelClass(FindClass(sClassName)).Create(Self);
        // Load the properties
        PrjModel.LoadEnvironment(PrjModelElement);
        //assign the new ProjectionModel to the Earth.
        ProjectionModel := PrjModel;
      end;

      Spheroid := TSpheroid(IntAttributeByName('Spheroid', Ord(Spheroid)));
      YAxisChanges := TYAxisChanges(IntAttributeByName('YAxisChanges', Ord(YAxisChanges)));
      XAxisChanges := TXAxisChanges(IntAttributeByName('XAxisChanges', Ord(XAxisChanges)));
      ScaleFactor := FloatAttributeByName('ScaleFactor', ScaleFactor);

      MaxLatitude := FloatAttributeByName('MaxLatitude', MaxLatitude);
      MaxLongitude := FloatAttributeByName('MaxLongitude', MaxLongitude);
      MaxZoom := FloatAttributeByName('MaxZoom', MaxZoom);
      MinLatitude := FloatAttributeByName('MinLatitude', MinLatitude);
      MinLongitude := FloatAttributeByName('MinLongitude', MinLongitude);
      MinZoom := FloatAttributeByName('MinZoom', MinZoom);

      XRotation := FloatAttributeByName('XRotation', XRotation);
      YRotation := FloatAttributeByName('YRotation', YRotation);
      ZRotation := FloatAttributeByName('ZRotation', ZRotation);
      XOrigin := FloatAttributeByName('XOrigin', XOrigin);
      YOrigin := FloatAttributeByName('YOrigin', YOrigin);

      CentralMeridian := FloatAttributeByName('CentralMeridian', CentralMeridian);
      CentralParallel := FloatAttributeByName('CentralParallel', CentralParallel);
      FirstParallel := FloatAttributeByName('FirstParallel', FirstParallel);
      SecondParallel := FloatAttributeByName('SecondParallel', SecondParallel);

      px := FloatAttributeByName('CenterLatitude', 0);
      pY := FloatAttributeByName('CenterLongitude', 0);

      if (px <> 0) and (py <> 0) then
        SetCenterLatlon(px, py);
    end;
end;

procedure TEarthProjection.SetUseMinMaxLimits(const Value: boolean);
begin
  FUseMinMaxLimits := Value;

  if Value then
  begin
    YAxisChanges := yaXOrigin;
    XAxisChanges := xaYOrigin;
    CentralMeridian := 0;
    CentralParallel := 0;
    ProjectionClass := 'TCartesianPrj';
  end;
end;

procedure TEarthProjection.SetProjectionModel(Value: TCustomProjectionModel);
var
  bValid: boolean;
  iProp: TProjectionProperty;
  iLong, iLat: double;
begin
  bValid := False;

  { locate the center of the screen in LL }
  if ProjectionModel.ClassType <> Value.ClassType then
    bValid := ProjectionModel.XYToLL(Earth.ClientWidth / 2, Earth.ClientHeight / 2, 0);

  if bValid then
    iLong := Earth.EarthCanvas.gaPoints^[0].X
  else
    iLong := 0;
  if bValid then
    iLat := Earth.EarthCanvas.gaPoints^[0].Y
  else
    iLat := 0;

  ProjectionModel.Free;
  if Value = nil then
    Value := TSphericalPrj.Create(Self);

  FProjectionModel := Value;

  // Update all of the Projections properties
  for iProp := Low(TProjectionProperty) to High(TProjectionProperty) do
    Value.PropertyChanged(iProp);

  { adjust the new projection to display the same map }
  if bValid then
  begin
    Earth.SetViewRect(Earth.ViewRect);
    Earth.Projection.CenterXY := PointDouble(Earth.ClientWidth / 2, Earth.ClientHeight / 2);
    Earth.LocateToLL(iLong, iLat);
  end;

  Earth.RedrawLayers;
end;

procedure TEarthProjection.SetIntegerProp(ProjectionProperty: integer; Value: integer);
begin
  case TProjectionProperty(ProjectionProperty) of
    ppXOrigin:
      if Value <> XOrigin then
        FiXOrigin := Value
      else
        Exit;
    ppYOrigin:
      if Value <> YOrigin then
        FiYOrigin := Value
      else
        Exit;
  end;

  ProjectionModel.PropertyChanged(TProjectionProperty(ProjectionProperty));
  Earth.RedrawLayers;
end;

procedure TEarthProjection.SetFloatProp(ProjectionProperty: integer; Value: extended);
begin
  case TProjectionProperty(ProjectionProperty) of
    ppXRotation:
      if Value <> XRotation then
        FXRotation := Value
      else
        Exit;
    ppYRotation:
      if Value <> YRotation then
        FYRotation := Mod180Float(Value)
      else
        Exit;
    ppZRotation:
      if Value <> ZRotation then
        FZRotation := Value
      else
        Exit;
    ppMaxLatitude:
      if Value <> MaxLatitude then
        FMaxLatitude := Min(Value, 90.0)
      else
        Exit;
    ppMinLatitude:
      if Value <> MinLatitude then
        FMinLatitude := Max(Value, -90.0)
      else
        Exit;
    ppMaxLongitude:
      if Value <> MaxLongitude then
        FMaxLongitude := Min(Value, 180.0)
      else
        Exit;
    ppMinLongitude:
      if Value <> MinLongitude then
        FMinLongitude := Max(Value, -180.0)
      else
        Exit;
    ppMaxZoom:
      if Value <> MaxZoom then
        FMaxZoom := Min(Value, GMAXSCALEFACTOR)
      else
        Exit;
    ppMinZoom:
      if Value <> MinZoom then
        FMinZoom := Min(Value, GMAXSCALEFACTOR)
      else
        Exit;
    ppCentralMeridian:
      if Value <> CentralMeridian then
        FCentralMeridian := Value
      else
        Exit;
    ppCentralParallel:
      if Value <> CentralParallel then
        FCentralParallel := Value
      else
        Exit;
    ppFirstParallel:
      if Value <> FirstParallel then
        FFirstParallel := Value
      else
        Exit;
    ppSecondParallel:
      if Value <> SecondParallel then
        FSecondParallel := Value
      else
        Exit;
    ppXOrigin:
      if Value <> XOrigin then
        FiXOrigin := Value
      else
        Exit;
    ppYOrigin:
      if Value <> YOrigin then
        FiYOrigin := Value
      else
        Exit;
  end;

  if ProjectionModel = nil then
    exit; //to correct Time event Rotate event

  ProjectionModel.PropertyChanged(TProjectionProperty(ProjectionProperty));
  Earth.UpdateScrollBars;
  Earth.RedrawLayers;
end;

procedure TEarthProjection.SetAltitude(Value: extended);
var
  Dt: extended;
begin
  Dt := Value;

  if Dt = 0 then
  begin
    ScaleFactor := 0.0;
    exit;
  end;

  if Dt <= GMINALTITUDE then
    Dt := GMINALTITUDE;
  if Dt >= GMAXALTITUDE then
    Dt := GMAXALTITUDE;
  ScaleFactor := 1 / (Dt * Tan(GFOV_ANGLE * DEG_TORADIANS));
end;

procedure TEarthProjection.SetScaleFactor(eSF: extended);
var
  OrgX, OrgY: extended;
  iCanvasWidth, iCanvasHeight: double;
begin
  if (eSF < 0) or (abs(eSF - ScaleFactor) < GEPSILON) then
    Exit;

  // check that the scalefactor is within limits.
  if FMaxZoom > FMinZoom then
  begin
    if eSF < FMinZoom then
      eSF := FMinZoom;
    if eSF > FMaxZoom then
      eSF := FMaxZoom;
  end;

  iCanvasWidth := Earth.EarthCanvas.CanvasWidth;
  iCanvasHeight := Earth.EarthCanvas.CanvasHeight;

  if eSF = 0 then // Zoom to the world extents
    with ProjectionModel do
    begin
      XOrigin := (iCanvasWidth / 2);
      YOrigin := (iCanvasHeight / 2);
      with ExtentsLL do
        eSF := min(iCanvasWidth / Abs(Right - Left), iCanvasHeight / Abs(Bottom - Top));
    end;

  // Limit the zoom range to avoid integer overflow
  if eSF > GMAXSCALEFACTOR then
    eSF := GMAXSCALEFACTOR;

  if Max(iCanvasWidth, iCanvasHeight) / eSF > GMaxDouble then
    eSF := Max(iCanvasWidth, iCanvasHeight) / GMaxDouble;

  if eSF <> ScaleFactor then
    try
      OrgX := (XOrigin - iCanvasWidth / 2) / ScaleFactor;
      XOrigin := (iCanvasWidth / 2 + OrgX * eSF);
      OrgY := (YOrigin - iCanvasHeight / 2) / ScaleFactor;
      YOrigin := (iCanvasHeight / 2 + OrgY * eSF);

      FiAltitude := (1 / (eSF * Tan(GFOV_ANGLE * DEG_TORADIANS)));

      Earth.FViewRect := Classes.Rect(0, 0, Round(iCanvasWidth / eSF), Round(iCanvasHeight / eSF));
      FScaleFactor := eSF;

      ProjectionModel.PropertyChanged(ppScaleFactor);

      Earth.RedrawLayers;

      if Assigned(TEarthBase(Earth).OnZoomed) and not (csLoading in Earth.ComponentState) then
        TEarthBase(Earth).OnZoomed(Self);
    except
      { swallow exception }
    end;

  Earth.UpdateScrollBars;
end;

function TEarthProjection.GetProjectionClass: string;
begin
  Result := ProjectionModel.ClassName;
end;

procedure TEarthProjection.SetProjectionClass(ProjectionClassName: string);
begin
  if CompareText(ProjectionModel.ClassName, ProjectionClassName) = 0 then
    Exit;

  if not FindClass(ProjectionClassName).InheritsFrom(TCustomProjectionModel) then
    raise EEarthException.CreateFmt(rsEBadProjectionClassMsg, [ProjectionClassName]);

  ProjectionModel := TCustomProjectionModelClass(FindClass(ProjectionClassName)).Create(Self);

  // Check if UseMinMaxLimits set - only works in the Cartesian Projection
  if UseMinMaxLimits and (CompareText(ProjectionClassName, 'TCartesianPrj') = 0) then
    UseMinMaxLimits := False;
end;

procedure TEarthProjection.SetSpheroid(Value: TSpheroid);
begin
  FSpheroid := Value;
  Earth.RedrawLayers;
end;

procedure TEarthProjection.SetUnitsPerInch(UPI: double);
begin
  ScaleFactor := Earth.EarthCanvas.ScreenPixelsPerInch / UPI;
end;

function TEarthProjection.ExtentsLL: TGRectFloat;
begin
  Result := ProjectionModel.ExtentsLL;
end;

function TEarthProjection.GetUnitsPerInch: double;
begin
  with Earth.EarthCanvas do
    Result := (CanvasPixelsPerInch / ScaleFactor);
end;

procedure TEarthProjection.SetEarthCenterXY(pt: TPointDouble);
begin
  if (pt.X <> XOrigin) or (pt.Y <> YOrigin) then
  begin
    XOrigin := (pt.X);
    YOrigin := (pt.Y);
    if Assigned(TEarthBase(Earth).OnPanned) and not (csLoading in Earth.ComponentState) then
      TEarthBase(Earth).OnPanned(Self);
  end;

  Earth.UpdateScrollBars;
  Earth.RedrawLayers;
end;

function TEarthProjection.GetEarthCenterXY: TPointDouble;
begin
  Result := PointDouble(XOrigin, YOrigin);
end;

procedure TEarthProjection.SetXAxisChanges(Value: TXAxisChanges);
begin
  if FXAxisChanges <> Value then
  begin
    FXAxisChanges := Value;
    ProjectionModel.PropertyChanged(ppXAxisChanges);
  end;
end;

procedure TEarthProjection.SetYAxisChanges(Value: TYAxisChanges);
begin
  if FYAxisChanges <> Value then
  begin
    FYAxisChanges := Value;
    ProjectionModel.PropertyChanged(ppYAxisChanges);
  end;
end;

procedure TEarthProjection.SetCenterLat(Value: extended);
var
  ptLL: TPointLL;
begin
  with Earth.EarthCanvas do
  begin
    YOrigin := CanvasHeight / 2;
  end;
  DeviceXYToLL(XOrigin, YOrigin, ptLL);

  XRotation := XRotation - (ptLL.iLatY / GU_DEGREE - Value);
end;

procedure TEarthProjection.SetCenterLong(Value: extended);
var
  ptLL: TPointLL;
begin
  with Earth.EarthCanvas do
  begin
    XOrigin := CanvasWidth / 2;
  end;

  DeviceXYToLL(XOrigin, YOrigin, ptLL);
  YRotation := YRotation - (ptLL.iLongX / GU_DEGREE - Value);
end;

procedure TEarthProjection.SetCenterLatlon(const aCenterLatitude, aCenterLongitude: TEarthDegree);
var
  ptLL: TPointLL;
begin
  with Earth.EarthCanvas do
  begin
    FiYOrigin := CanvasHeight / 2;
    ProjectionModel.PropertyChanged(ppXOrigin);
    FiXOrigin := CanvasWidth / 2;
    ProjectionModel.PropertyChanged(ppYOrigin);
  end;

  DeviceXYToLL(XOrigin, YOrigin, ptLL);
  // DeviceXYToLLx(XOrigin, YOrigin, ptLL);

  FXRotation := XRotation - (ptLL.iLatY / GU_DEGREE - aCenterLatitude);
  ProjectionModel.PropertyChanged(ppXRotation);
  FYRotation := Mod180Float(YRotation - (ptLL.iLongX / GU_DEGREE - aCenterLongitude));
  ProjectionModel.PropertyChanged(ppYRotation);

  Earth.UpdateScrollBars;
  Earth.RedrawLayers;

end;

function TEarthProjection.GetCenterLat: extended;
var
  ptLL: TPointLL;
begin
  with Earth.EarthCanvas do
    if DeviceXYToLL(CanvasWidth / 2, CanvasHeight / 2, ptLL) then
      Result := ptLL.iLatY / GU_DEGREE
    else
      Result := GMaxDouble;
end;

function TEarthProjection.GetCenterLong: extended;
var
  ptLL: TPointLL;
begin
  with Earth.EarthCanvas do
    if DeviceXYToLL(CanvasWidth / 2, CanvasHeight / 2, ptLL) then
      Result := ptLL.iLongX / GU_DEGREE
    else
      Result := GMaxDouble;
end;

function TEarthProjection.DeviceXYToLL(iX, iY: double; var ptLL: TPointLL): boolean;
begin
  Result := ProjectionModel.XYToLL(iX, iY, 0);
  if Result then
    ptLL := PointLL(Earth.EarthCanvas.gaPoints^[0].X, Earth.EarthCanvas.gaPoints^[0].Y)
  else
    ptLL := PointLL(GU_180_DEGREE, GU_180_DEGREE);
end;

function TEarthProjection.LLToDeviceXY(iLong, iLat: double; var ptXY: TPointDouble): boolean;
begin
  Result := ProjectionModel.LLToXY(iLong, iLat, 0);
  ptXY := Earth.EarthCanvas.gaPoints^[0];
end;

    {
function TEarthProjection.PointLLToXY(ptLL : TPointLL; var ptXY : TPoint) : Boolean;
begin
  Result := ProjectionModel.PointLLToXY(ptLL, 0);
  ptXY := PointDouble(Earth.EarthCanvas.gaPoints^[0]);
end;
    }
function TEarthProjection.PointLLToXY(ptLL: TPointLL; var ptXY: TPointDouble): boolean;
begin
  Result := ProjectionModel.PointLLToXY(ptLL, 0);
  ptXY := Earth.EarthCanvas.gaPoints^[0];
end;

//===============================================================================================
//===================== TEarthCanvas ============================================================
//===============================================================================================

constructor TEarthCanvas.Create(ParentEarth: TCustomEarth);
begin
  inherited Create;

  Assert(ParentEarth <> nil, 'TEarthCanvas.Create passed nil Parent');

  FEarth := ParentEarth;
  FBitMap := FEarth.bitmap;

  FDefPen := TEarthPen.Create(self);
  FDefPen.Define(clBlack32, psSolid, 1, euPixel);

  FDefBrush := TEarthBrush.Create(self);
  FDefBrush.Define(clWhite32, gbsSolid);

  FDefFont := TEarthFont.Create(self);
  FDefFont.Define('WingDings', clBlack32, clWhite32, 9, euPixel, 0, []);

  FDefTitleFont := TEarthFont.Create(self);
  FDefTitleFont.Define('Arial', clBlack32, clWhite32, 9, euPixel, 0, []);

end;

destructor TEarthCanvas.Destroy;
begin
  FDefPen.Free;
  FDefBrush.Free;
  FDefFont.Free;
  FDefTitleFont.Free;
  inherited Destroy;
end;

procedure TEarthCanvas.SetDefFont(val: TEarthFont);
begin
  if val = nil then
    Exit;
  FDefFont.Assign(val);

end;

procedure TEarthCanvas.SetDefTitleFont(val: TEarthFont);
begin
  if val = nil then
    Exit;
  FDefTitleFont.Assign(val);
end;

function TEarthCanvas.RenderDIBShadow(GMTDateTime: TDateTime; ColorNight, ColorTwilight: TColor; Density: integer): TPointLL;
const
  GREYLINE = 12 * GU_DEGREE;
var
  Year, Tmp: word;
  iRotation, iDays, iSecs: integer;
  iCol, iRow, iTmp, idx: integer;
  tmpColor, DIBColorNight, DIBColorTwilight: TRGBTriple;
  eAngle, eTmp, eER, eSunX, eSunY, eSunZ: extended;
  iDiv, iState: integer;
  bFlag: boolean;
  RGBBufferPtr: PtrInt; //integer;  7777

  function GetDIBColor(iState: integer): TRGBTriple;
  begin
    if iState = 2 then
    begin
      Result := DIBColorNight;
      iDiv := 2;
    end
    else
    begin
      Result := DIBColorTwilight;
      iDiv := 4;
    end;
  end;

  {---------------------------------------------}

  procedure SetShadowPixel(iX, iY: integer; c: TRGBTriple);
  var
    NewColor: TRGBTriple;
  begin
    if iX < 0 then Exit;

    //  RGBBufferPtr := Integer(RGBStart) + (CanvasHeight - 1 - iY)
    //    * RGBLineWidth + iX * (24 shr 3);

    RGBBufferPtr := PtrInt(RGBStart) + (CanvasHeight - 1 - iY) * RGBLineWidth + iX * (BitmapInfo.bmiHeader.biBitCount shr 3);

    NewColor := PRGBTriple(RGBBufferPtr)^;

    NewColor.rgbtRed := NewColor.rgbtRed + MulDiv(1, c.rgbtRed - NewColor.rgbtRed, iDiv);
    NewColor.rgbtBlue := NewColor.rgbtBlue + MulDiv(1, c.rgbtBlue - NewColor.rgbtBlue, iDiv);
    NewColor.rgbtGreen := NewColor.rgbtGreen + MulDiv(1, c.rgbtGreen - NewColor.rgbtGreen, iDiv);
    PRGBTriple(RGBBufferPtr)^ := NewColor;
  end;

  {---------------------------------------------}

  function DoPixel(iX, iY: integer): integer;
  begin
    Result := 0;
    if Projection.ProjectionModel.XYToLL(iX, iY, 0) then
    begin
      Inc(Result);
      with Point3D(gaPoints^[0].X, gaPoints^[0].Y, 0) do
        eTmp := iWorldX * eSunX + iWorldY * eSunY + iWorldZ * eSunZ;
      if eTmp <= 0 then
      begin
        Inc(Result, 1 + Ord(eTmp >= -GREYLINE));
        SetShadowPixel(iX, iY, GetDIBColor(Result));
      end;
    end;
  end;

  function GetDIBFromColor(c: TColor): TRGBTriple;
  var
    nc: TColor;
  begin
    nc := ColorToRGB(c);
    with Result do //scanline
    begin
      rgbtRed := GetRValue(nc);
      rgbtGreen := GetGValue(nc);
      rgbtBlue := GetBValue(nc);
    end;
  end;

begin
  DecodeDate(GMTDateTime, Year, Tmp, Tmp);
  iDays := Trunc(GMTDateTime) - Trunc(EncodeDate(Year - 1, 12, 21));
  { 0.4333823 = Tan( 23.452294 ) = Tan( Axial tilt ) }
  eAngle := 180 * ArcTan(0.4333823 * Cos(DoublePi * iDays / 365.24225)) / LocalPi;

  iSecs := Round(Frac(GMTDateTime) * 86400);
  iRotation := (Round(iSecs * (21600 / 86400)) mod 21600) - 10800;

  eER := 2 / EARTHRADIUS;
  Result := PointLL(-iRotation * GU_MINUTE, -Round(eAngle * GU_DEGREE));
  with Point3D(Result.ILongX, Result.iLatY, 0) do
  begin
    eSunX := iWorldX * eER;
    eSunY := iWorldY * eER;
    eSunZ := iWorldZ * eER;
  end;

  Density := Max(Density, 1);

  iRow := 0;
  bFlag := False;

  DIBColorNight := GetDIBFromColor(ColorNight);
  DIBColorTwilight := GetDIBFromColor(ColorTwilight);

  while iRow < CanvasHeight do
  begin
    iCol := Ord(bFlag) * (Density div 2);
    iState := DoPixel(iCol, iRow);
    while iCol < CanvasWidth do
    begin
      iTmp := DoPixel(iCol, iRow);
      if iTmp <> iState then
      begin
        iState := iTmp;
        for idx := 1 to 15 do
          DoPixel(iCol - idx * Density, iRow);
      end
      else
      if iState >= 2 then
      begin
        tmpColor := GetDIBColor(iState);
        for idx := 1 to 15 do
          SetShadowPixel(iCol - idx * Density, iRow, tmpColor);
      end;
      Inc(iCol, Density * 16);
    end;
    // Fix to render the full width of the canvas
    Dec(iCol, Density * 15);
    while iCol < CanvasWidth do
    begin
      DoPixel(iCol, iRow);
      Inc(iCol, Density);
    end;

    bFlag := not bFlag;
    Inc(iRow, Density);
  end;
end;

function TEarthCanvas.RenderShadow(GMTDateTime: TDateTime; ColorNight, ColorTwilight: TColor; Density: integer): TPointLL;
const
  GREYLINE = 12 * GU_DEGREE;
var
  Year, Tmp: word;
  iRotation, iDays, iSecs: integer;
  iCol, iRow, iTmp, idx: integer;
  eAngle, eTmp, eER, eSunX, eSunY, eSunZ: extended;
  iState: integer;
  bFlag: boolean;
  tmpColor: TColor;

  function GetColor(iState: integer): TColor;
  begin
    if iState = 2 then
      Result := ColorNight
    else
      Result := ColorTwilight;
  end;

  {---------------------------------------------}
  function doPixel(iX, iY: integer): integer;

  begin
    Result := 0;
    if Projection.ProjectionModel.XYToLL(iX, iY, 0) then
    begin
      Inc(Result);
      with Point3D(gaPoints^[0].X, gaPoints^[0].Y, 0) do
        eTmp := iWorldX * eSunX + iWorldY * eSunY + iWorldZ * eSunZ;
      if eTmp <= 0 then
      begin
        Inc(Result, 1 + Ord(eTmp >= -GREYLINE));
        gDrawPixel(iX, iY, GetColor(Result));
      end;
    end;
  end;

begin
  DecodeDate(GMTDateTime, Year, Tmp, Tmp);
  iDays := Trunc(GMTDateTime) - Trunc(EncodeDate(Year - 1, 12, 21));
  { 0.4333823 = Tan( 23.452294 ) = Tan( Axial tilt ) }
  eAngle := 180 * ArcTan(0.4333823 * Cos(DoublePi * iDays / 365.24225)) / LocalPi;

  iSecs := Round(Frac(GMTDateTime) * 86400);
  iRotation := (Round(iSecs * (21600 / 86400)) mod 21600) - 10800;

  eER := 2 / EARTHRADIUS;
  Result := PointLL(-iRotation * GU_MINUTE, -Round(eAngle * GU_DEGREE));
  with point3D(Result.iLongX, Result.iLatY, 0) do
  begin
    eSunX := iWorldX * eER;
    eSunY := iWorldY * eER;
    eSunZ := iWorldZ * eER;
  end;

  if Density < 1 then
    Density := 1;

  iRow := 0;
  bFlag := False;
  while iRow < CanvasHeight do
  begin
    iCol := Ord(bFlag) * (Density div 2);
    iState := DoPixel(iCol, iRow);
    while iCol < CanvasWidth do
    begin
      Inc(iCol, Density * 16);
      iTmp := DoPixel(iCol, iRow);
      if iTmp <> iState then
      begin
        iState := iTmp;
        for idx := 1 to 15 do
          DoPixel(iCol - idx * Density, iRow);
      end
      else
      if iState >= 2 then
      begin
        tmpColor := GetColor(iState);
        for idx := 1 to 15 do
          gDrawPixel(iCol - idx * Density, iRow, tmpColor);
      end;
    end;
    bFlag := not bFlag;
    Inc(iRow, Density);
  end;
end;

procedure TEarthCanvas.RenderPolygon(const Points: array of TPointLL);
var
  idx: integer;
  Chains: TGroupsStore;
begin
  Chains := TGroupsStore.Create;
  Chains.StoreHeight := True;
  try
    for idx := 0 to High(Points) do
      Chains[0].Add(Points[idx]);

    RenderChainStore(Chains,
      [osClosed, osClipLeft, osClipTop, osClipBottom, osClipRight],
      Canvas.Pen.Width);
  finally
    Chains.Free;
  end;
end;

procedure TEarthCanvas.RenderMER(const aMer: Tmer);
var
  Points: array[0..4] of TPointLL;
begin
  Points[0].iLongX := aMer.iLongX;
  Points[0].iLatY := aMer.iLatY;

  Points[1].iLongX := aMer.iLongX + aMer.iWidthX;
  Points[1].iLatY := aMer.iLatY;

  Points[2].iLongX := aMer.iLongX + aMer.iWidthX;
  Points[2].iLatY := aMer.iLatY + aMer.iHeightY;

  Points[3].iLongX := aMer.iLongX;
  Points[3].iLatY := aMer.iLatY + aMer.iHeightY;

  Points[4].iLongX := Points[0].iLongX;
  Points[4].iLatY := Points[0].iLatY;

  RenderPolyLine(Points);
end;

procedure TEarthCanvas.RenderProjectedMER(const aMer: Tmer);
begin
  // display the bounding rectangle
  with aMer do
  begin
    RenderLine(PointLL(iLongX, iLatY), PointLL(iLongX + iWidthX, iLatY), 32);
    RenderLine(PointLL(iLongX + iWidthX, iLatY), PointLL(iLongX + iWidthX, iLatY + iHeightY), 32);
    RenderLine(PointLL(iLongX + iWidthX, iLatY + iHeightY), PointLL(iLongX, iLatY + iHeightY), 32);
    RenderLine(PointLL(iLongX, iLatY + iHeightY), PointLL(iLongX, iLatY), 32);
  end;
end;

procedure TEarthCanvas.RenderBox(const aCenterXY: TPoint; DX, DY: integer);
begin
  RenderBox(PointDouble(aCenterXY), DX, DY);
end;

procedure TEarthCanvas.RenderBox(const aCenterXY: TPointDouble; DX, DY: double);
var
  Points: array[0..4] of TPointDouble;
  CXY: TPointDouble;
begin
  CXY := aCenterXY;
  Points[0].X := CXY.X - DX - 1;
  Points[0].Y := CXY.Y - DY - 1;

  Points[1].X := CXY.X + DX + 1;
  Points[1].Y := CXY.Y - DY - 1;

  Points[2].X := CXY.X + DX + 1;
  Points[2].Y := CXY.Y + DY + 1;

  Points[3].X := CXY.X - DX - 1;
  Points[3].Y := CXY.Y + DY + 1;

  Points[4].X := CXY.X - DX - 1;
  Points[4].Y := CXY.Y - DY - 1;
  Earth.EarthCanvas.Polyline(@Points, 5);
end;

procedure TEarthCanvas.RenderBoxLL(const aCenterLL: TPointLL; DX, DY: integer);
var
  CXY: TPointdouble;
begin
  Earth.Projection.PointLLToXY(aCenterLL, CXY);
  RenderBox(CXY, DX, DY);
end;

procedure TEarthCanvas.RenderBoxLL(const aCenterLL: TPointLL; DX, DY: double);
var
  CXY: TPointDouble;
begin
  Earth.Projection.PointLLToXY(aCenterLL, CXY);
  RenderBox(CXY, DX, DY);
end;

procedure TEarthCanvas.RenderPolyLine(const Points: array of TPointLL);
var
  idx: integer;
  Chains: TGroupsStore;
begin
  Chains := TGroupsStore.Create;
  Chains.StoreHeight := True;
  try
    for idx := 0 to High(Points) do
      Chains[0].Add(Points[idx]);

    RenderChainStore(Chains,
      [osClipLeft, osClipTop, osClipBottom, osClipRight],
      Canvas.Pen.Width);
  finally
    Chains.Free;
  end;
end;

procedure TEarthCanvas.RenderGreatCircleLine(const ptFromLL, ptToLL: TPointLL; CircleType: TGreatCircle);
var
  a, b: TV3D;
  angle: extended;
  iSteps: integer;
  mat: TGMatrix;
begin
  //  EarthCanvas.RefreshDC;
  a := PointLLToV3D(ptFromLL);
  b := PointLLToV3D(ptToLL);

  case CircleType of
    gcShortest:
      angle := ArcCos(V3DDot(a, b));
    gcLongest:
      angle := DoublePi - ArcCos(V3DDot(a, b));
    else
      angle := DoublePi; { gcCircle }
  end;

  iSteps := Trunc(angle / (GU_DEGREE * GU_TORADIANS));
  if iSteps > 0 then
  begin
    QuatToMatrix(AxisAngleToQuat(V3DCross(a, b), -angle / iSteps), mat);

    if CircleType = gcLongest then
      DrawVectorMatrixLine(b, mat, 0, iSteps)
    else
      DrawVectorMatrixLine(a, mat, 0, iSteps);
  end
  else
    RenderLine(ptFromLL, ptToLL, 1);
end;

procedure TEarthCanvas.RenderLine(const ptFromLL, ptToLL: TPointLL; iSteps: integer);
var
  idx: integer;
  iXDist, iYDist: double;
  iXtmp: double;
  Chains: TGroupsStore;
  State: TEarthObjectStateSet;
begin
  //  EarthCanvas.RefreshDC;
  if iSteps <= 0 then
    iSteps := 1;

  State := [osFace, osBackFace];
  Chains := TGroupsStore.Create;
  Chains.StoreHeight := (ptFromLL.iHeightZ <> 0) or (ptToLL.iHeightZ <> 0);

  iYDist := ptToLL.iLatY - ptFromLL.iLatY;
  iXDist := ptToLL.iLongX - ptFromLL.iLongX;

  iXtmp := ptFromLL.iLongX / GU_90_DEGREE;

  if (iXtmp <> 0) and (iXtmp + ptToLL.iLongX / GU_90_DEGREE = 0) then
    iXDist := iXtmp * (GU_360_DEGREE - abs(iXDist));

  Chains[0].Count := iSteps + 1;
  for idx := 0 to iSteps do
    Chains[0].AsLL[idx] := PointLL(ptFromLL.iLongX + MulDivFloat(iXDist, idx, iSteps), ptFromLL.iLatY + MulDivFloat(iYDist, idx, iSteps));

  // with EarthCanvas do
  if Projection.ProjectionModel.VisibleMER(Chains.GroupStoreMER, State) then
    RenderChainStore(Chains, State, Canvas.Pen.Width);

  Chains.Free;
end;

procedure TEarthCanvas.RenderCircle(const ptLL: TPointLL; iRadius, iSteps: integer);
var
  mat: TGMatrix;
begin
  //  EarthCanvas.RefreshDC;
  { use 4 Degree steps to draw circle }
  if (iSteps > 360) or (iSteps < 3) then
    iSteps := 36;
  with ptLL do
  begin
    QuatToMatrix(AxisAngleToQuat(PointLLToV3D(PointLLH(iLongX, iLatY, iHeightZ)), (360 / iSteps) * GU_DEGREE * GU_TORADIANS), mat);
    DrawVectorMatrixLine(
      PointLLToV3D(PointLLH(iLongX, iLatY + iRadius, iHeightZ)),
      mat, iHeightZ, iSteps);
  end;
end;

procedure TEarthCanvas.Assign(Source: TPersistent);
var
  GC: TEarthCanvas;
begin
  if Source is TEarthCanvas then
  begin
    GC := TEarthCanvas(Source);

    CanvasWidth := GC.CanvasWidth;
    CanvasHeight := GC.CanvasHeight;

    FDefPen.Assign(GC.DefPen);
    FDefBrush.Assign(GC.DefBrush);
    FDefFont.Assign(GC.DefFont);
    FDefTitleFont.Assign(GC.DefTitleFont);

    CanvasPixelsPerInch := GC.CanvasPixelsPerInch;
    SizeBMP(GC.CanvasWidth, GC.CanvasHeight, GC.ScreenPixelsPerInch);
  end
  else
    inherited Assign(Source);
end;

function TEarthCanvas.Clone: TEarthCanvas;
begin
  Result := TEarthCanvasClass(Self.ClassType).Create(Earth);
  Result.Assign(Self);
end;

function TEarthCanvas.ClipPoly(iCount: integer; State: TEarthObjectStateSet; iPenWidth: double): integer;
var
  apIn, apOut, apTmp: PTPointDoubleArray;

  {---------------------------------------------------------------------------}

  function ClipEdge(edge: byte; iMax: double; iCount: integer): integer;
  var
    idx: integer;
    bClipStart, bClipEnd: boolean;
    iStartX, iStartY: double;
    iEndX, iEndY: double;
  begin
    if osClosed in State then
      apIn^[iCount] := apIn^[0]
    else
      apIn^[iCount] := apIn^[iCount - 1];

    iStartX := apIn^[0].X;
    iStartY := apIn^[0].Y;

    bClipStart := ((Ord(iStartY < 0) or (Ord(iStartY > CanvasHeight) shl 1) or (Ord(iStartX < 0) shl 2) or
      (Ord(iStartX > CanvasWidth) shl 3)) and edge) <> 0;

    Result := 0;
    Dec(iCount);

    for idx := 0 to iCount do  //add Points
    begin
      iEndX := apIn^[idx + 1].X;
      iEndY := apIn^[idx + 1].Y;

      bClipEnd := ((Ord(iEndY < 0) or (Ord(iEndY > CanvasHeight) shl 1) or (Ord(iEndX < 0) shl 2) or (Ord(iEndX > CanvasWidth) shl 3)) and
        edge) <> 0;

      if not bClipStart then { the point is visible }
      begin
        apOut^[Result] := PointDouble(iStartX, iStartY);
        Inc(Result);
      end;

      if bClipStart xor bClipEnd then { Line crosses edge }
      begin
        if (edge and 3) <> 0 then
          apOut^[Result] := PointDouble(iEndX + MulDivFloat(iMax - iEndY, iStartX - iEndX, iStartY - iEndY), iMax)
        else
          apOut^[Result] := PointDouble(iMax, iEndY + MulDivFloat(iMax - iEndX, iStartY - iEndY, iStartX - iEndX));
        Inc(Result);
      end;

      bClipStart := bClipEnd;
      iStartX := iEndX;
      iStartY := iEndY;
    end;

    { swap the input and output buffers }
    apTmp := apIn;
    apIn := apOut;
    apOut := apTmp;
  end;

begin
  Result := iCount;
  if [osClipTop, osClipLeft, osClipBottom, osClipRight] * State = [] then
    Exit;

  apIn := gaPoints;
  apOut := fgaClipPoints;
  // Clip to Top edge
  if (Result > 1) and (osClipTop in State) then
    Result := ClipEdge(1, -iPenWidth, Result);
  // Clip to Right Edge
  if (Result > 1) and (osClipRight in State) then
    Result := ClipEdge(8, CanvasWidth + iPenWidth, Result);
  //Clip to Bottom edge
  if (Result > 1) and (osClipBottom in State) then
    Result := ClipEdge(2, CanvasHeight + iPenWidth, Result);
  // Clip to Left Edge
  if (Result > 1) and (osClipLeft in State) then
    Result := ClipEdge(4, -iPenWidth, Result);

  if apOut = gaPoints then
    Move(fgaClipPoints^[0], gaPoints^[0], Result * SizeOf(TPointDouble));
end;

procedure TEarthCanvas.DrawClippedPoly(iPoints: integer; State: TEarthObjectStateSet; iPenWidth: extended);
begin
  if iPoints > 1 then
    iPoints := ClipPoly(iPoints, State, iPenWidth);
  if iPoints > 1 then
    if osClosed in State then
      gDrawPolygon(gaPoints, iPoints)
    else
      gDrawPolyline(gaPoints, iPoints);

end;

function TEarthCanvas.ScaleUnitsToDevice(Value: extended; Units: TEarthUnitTypes): extended;
begin
  if Units = euPixel then
    Result := MulDivFloat(Value, CanvasPixelsPerInch, ScreenPixelsPerInch)
  else
    Result := EarthUnitsFrom(Value, Units) * Projection.ScaleFactor;
end;

function TEarthCanvas.EarthUnitsFrom(const eValue: extended; Units: TEarthUnitTypes): extended;
begin
  try
    if Units = euPixel then
      Result := (eValue / Projection.Scalefactor)
    else
      Result := (eValue * GU_Conversions[Ord(Units)]);
  except
    Result := (eValue);
  end;
end;

function TEarthCanvas.HeightUnitsFrom(const eValue: extended; Units: THeightUnitTypes): extended;
begin
  try
    Result := (eValue * GU_HeightConversions[Ord(Units)]);
  except
    Result := (eValue);
  end;
end;

function TEarthCanvas.FontRenderHeight(FontSize: extended; FontUnits: TEarthUnitTypes): extended;
begin
  if FontUnits = euPixel then
    Result := MulDivFloat(FontSize, CanvasPixelsPerInch, ScreenPixelsPerInch)
  else
    Result := FontSize * (EarthUnitsFrom(1, TEarthUnitTypes(FontUnits)) * Projection.ScaleFactor);
end;

procedure TEarthCanvas.RenderTextureMap(const TextureMap: TTextureData);
var
  WorldTri: TTriangleLL;

  procedure SubDivide(ptTL, ptBR: TPointLL);
  var
    iWidth, iHeight: double;
  begin
    if ptTL.iLongX < ptBR.iLongX then
      iWidth := (ptBR.iLongX - ptTL.iLongX) / 2
    else
      iWidth := ((GU_180_DEGREE + ptBR.iLongX) + (GU_180_DEGREE - ptTL.iLongX)) / 2;

    iHeight := abs(ptBR.iLatY - ptTL.iLatY) / 2;

    if (iWidth <= 0) or (iHeight <= 0) then
      Exit;

    if (iWidth < GU_DEGREE) or (iHeight < GU_DEGREE) then
    begin
      WorldTri[0] := ptTL;
      WorldTri[1] := PointLL(ptBR.iLongX, ptTL.iLatY);
      WorldTri[2] := ptBR;
      RenderTextureTriangle(WorldTri, TextureMap);

      WorldTri[1] := PointLL(ptTL.iLongX, ptBR.iLatY);

      RenderTextureTriangle(WorldTri, TextureMap);

      Exit;
    end;

    with ptTL do
    begin
      SubDivide(PointLL(iLongX, iLatY),
        PointLL(Mod180Float(iLongX + iWidth - 1), iLatY - iHeight - 1));
      SubDivide(PointLL(Mod180Float(iLongX + iWidth), iLatY),
        PointLL(Mod180Float(iLongX + iWidth * 2 - 1), iLatY - iHeight - 1));
      SubDivide(PointLL(Mod180Float(iLongX + iWidth), iLatY - iHeight - 1),
        PointLL(Mod180Float(iLongX + iWidth * 2 - 1), iLatY - iHeight * 2 - 1));
      SubDivide(PointLL(iLongX, iLatY - iHeight),
        PointLL(Mod180Float(iLongX + iWidth - 1), iLatY - iHeight * 2 - 1));
    end;
  end;

begin
  with TextureMap do
    SubDivide(TextureXYToLL(Point(0, 0)), TextureXYToLL(Point(Width - 1, Height - 1)));
end;
//.......................................................................
//........................ RenderTextureTriangle .......................
//......................................................................
procedure TEarthCanvas.RenderTextureTriangle(WorldTri: TTriangleLL; const TextureMap: TTextureData);
var
  iTmp: double;
  idx, top, a, b, y, iTransColorIndex: integer;
  Height_A, Height_B: integer;
  xA, xB, uA, uB, vA, vB: extended;
  dx_A, du_A, dv_A, dx_B, du_B, dv_B: extended;
  iTexColourStart, iTexWidth, iTexHeight: integer;
  TextureTri: TTriangle;
  iTransColor32: Tcolor32;
  //---------------------------------------------------
  procedure DrawScanline(x1, x2: integer; u1, u2, v1, v2: extended);
  var
    tmp, du, dv: extended;
    Xwidth, idx: integer;
    ColorRGBValue: integer;
  begin
    if x2 < x1 then
    begin { swap coordinates so we can draw left-to-right }
      idx := x1;
      x1 := x2;
      x2 := idx;
      tmp := u1;
      u1 := u2;
      u2 := tmp;
      tmp := v1;
      v1 := v2;
      v2 := tmp;
    end;

    Xwidth := x2 - x1;

    if (Xwidth <= 0) or (x2 < 0) or (x1 > CanvasWidth) then
      Exit;

    // calc interpolants
    du := (u2 - u1) / Xwidth;
    dv := (v2 - v1) / Xwidth;

    if x1 < 0 then
    begin
      v1 := v1 - x1 * dv;
      u1 := u1 - x1 * du;
      x1 := 0;
    end;
    //draw the scanline

    for idx := x1 to Min(x2, CanvasWidth) - 1 do
    begin
      bitmap.setpixelts(idx, Round(y), TextureMap.TextureStream.Pixels[Round(u1), Round(iTexHeight - v1)]);
      v1 := v1 + dv;
      u1 := u1 + du;
    end;

  end;

  //---------------------------------------------------
  procedure InterpolantsA(iA, iB: integer);
  begin
    xA := gaPoints^[iB].X;
    uA := TextureTri[iB].X;
    vA := TextureTri[iB].Y;
    if Height_A = 0 then
      Exit;
    dx_A := (gaPoints^[iA].X - xA) / Height_A;
    du_A := (TextureTri[iA].X - uA) / Height_A;
    dv_A := (TextureTri[iA].Y - vA) / Height_A;
  end;

  //---------------------------------------------------
  procedure InterpolantsB(iA, iB: integer);
  begin
    xB := gaPoints^[iB].X;
    uB := TextureTri[iB].X;
    vB := TextureTri[iB].Y;
    if Height_B = 0 then
      Exit;
    dx_B := (gaPoints^[iA].X - xB) / Height_B;
    du_B := (TextureTri[iA].X - uB) / Height_B;
    dv_B := (TextureTri[iA].Y - vB) / Height_B;
  end;
  //---------------------------------------------------
begin
  // if displaying on a flat surface
  if not (pfContinuous in Projection.Flags) then
  begin
    iTmp := Mod180Float(GU_180_DEGREE + (Projection.CentralMeridian * GU_DEGREE));

    // Check that the triangle does not cross the edge of the surface
    if (Sign(WorldTri[0].iLongX - iTmp) <> Sign(WorldTri[1].iLongX - iTmp)) or (Sign(WorldTri[0].iLongX - iTmp) <>
      Sign(WorldTri[2].iLongX - iTmp)) then
      Exit;
  end;

  for idx := 0 to 2 do
    TextureTri[idx] := TextureMap.LLToTextureXY(WorldTri[idx]);

  // Project the points and copy them to a safe place
  for idx := 0 to 2 do
  begin
    if not Projection.ProjectionModel.PointLLToXY(WorldTri[idx], idx) then
      Exit;
    gaPoints^[idx + 32] := gaPoints^[idx];
  end;

  with PointsDoubleToRect([gaPoints^[0], gaPoints^[1], gaPoints^[2]]) do
  begin
    if (Right < 0) or (Bottom < 0) then
      Exit;
    if (Left > CanvasWidth) or (Top > CanvasHeight) then
      Exit;
  end;

  // copy the points back
  for idx := 0 to 2 do
    gaPoints^[idx] := gaPoints^[idx + 32];

  //-------------------------------------------------------
  iTransColor32 := Color32(TextureMap.TransparentColor);
  iTexWidth := TextureMap.Width;
  iTexHeight := TextureMap.Height;
  //---------------------------------------------------

  top := 0;
  for idx := 1 to 2 do
    if gaPoints^[idx].Y < gaPoints^[top].Y then
      top := idx;

  Y := Round(gaPoints^[top].Y);
  a := ((top + 1) mod 3);
  b := ((top + 2) mod 3);

  Height_A := Round(gaPoints^[a].Y - Y);
  Height_B := Round(gaPoints^[b].Y - Y);

  InterpolantsA(a, top);
  InterpolantsB(b, top);

  idx := 2;

  Bitmap.BeginUpdate;  //to Speed up freeze Bitmap

  while idx > 0 do
  begin

    while (Height_A <> 0) and (Height_B <> 0) do
    begin
      if (y >= 0) and (y < CanvasHeight) then
        DrawScanline(Round(xA), Round(xB), uA, uB, vA, vB);

      Inc(y);
      Dec(Height_A);
      Dec(Height_B);
      xA := xA + dx_A; // add the interpolants
      xB := xB + dx_B;
      uA := uA + du_A;
      uB := uB + du_B;
      vA := vA + dv_A;
      vB := vB + dv_B;
    end;

    if Height_A = 0 then
    begin
      Height_A := Round(gaPoints^[b].y - gaPoints^[a].y);
      // recalculate the interpolants for the new edge
      InterpolantsA(b, a);
      Dec(idx); // one less vertex
      a := b;
    end;

    if Height_B = 0 then
    begin
      Height_B := Round(gaPoints^[a].y - gaPoints^[b].y);
      // recalculate the interpolants for the new edge
      InterpolantsB(a, b);
      Dec(idx); // one less vertex
      b := a;
    end;
  end;

  Bitmap.EndUpdate; //End freeze Bitmap

end;

procedure TEarthCanvas.SizeBMP(iWidth, iHeight: integer; iPixelsPerInch: double);
begin
  if ResizeProjection(iWidth, iHeight) then
  begin
    CanvasPixelsPerInch := iPixelsPerInch;
    setsize(iWidth, iHeight);
  end;
end;

function TEarthCanvas.ResizeProjection(iWidth, iHeight: double): boolean;
var
  eRatio, eSF: extended;
  iXOrg, iYorg: extended;
  ptCenter: TPointLL;
  ptXY: TPointDouble;
  ValidPoint: boolean;
  OrgX, OrgY: extended;
begin
  Result := (iWidth > 0) and (iHeight > 0);

  if CanvasWidth = 0 then
    exit;
  if CanvasHeight = 0 then
    exit;

  if Result then
    with Projection do
    begin
      ValidPoint := DeviceXYToLL(CanvasWidth / 2, CanvasHeight / 2, ptCenter);
      { Use the window diagonal to control the sizing ratio }
      eRatio := Hypot(iWidth, iHeight) / Hypot(CanvasWidth, CanvasHeight);

      iXOrg := MulDivFloat(XOrigin, iWidth, CanvasWidth);
      iYOrg := MulDivFloat(YOrigin, iHeight, CanvasHeight);

      if not (goConstantScaleOnResize in Earth.EarthOptions) then
      begin
        eSF := ScaleFactor * eRatio;

        if eSF > GMAXSCALEFACTOR then
          eSF := GMAXSCALEFACTOR;

        if Max(iWidth, iHeight) / eSF > GMaxDouble then
          eSF := Max(iWidth, iHeight) / GMaxDouble;

        // Adjust the origin and the scalefactor
        OrgX := (XOrigin - CanvasWidth / 2) / ScaleFactor;
        XOrigin := (iWidth / 2 + OrgX * eSF);
        OrgY := (YOrigin - CanvasHeight / 2) / ScaleFactor;
        YOrigin := (iHeight / 2 + OrgY * eSF);

        FiAltitude := (1 / (eSF * Tan(GFOV_ANGLE * DEG_TORADIANS)));

        Earth.FViewRect := RectD(0, 0, (iWidth / eSF), (iHeight / eSF));
        FScaleFactor := eSF;

        ProjectionModel.PropertyChanged(ppScaleFactor);
      end;

      if not (goFixedCenterXYOnResize in Earth.EarthOptions) then
      begin
        if ValidPoint then
        begin
          PointLLToXY(ptCenter, ptXY);
          iXOrg := XOrigin + ((iWidth / 2) - ptXY.X);
          iYOrg := YOrigin + ((iHeight / 2) - ptXY.Y);
        end;
        XOrigin := (iXOrg);
        YOrigin := (iYOrg);
      end;
      CanvasWidth := Round(iWidth);
      CanvasHeight := Round(iHeight);
    end;
end;

function TEarthCanvas.GetProjection: TEarthProjection;
begin
  Result := Earth.Projection;
end;

procedure TEarthCanvas.RenderChainStore(Chains: TGroupsStore; State: TEarthObjectStateSet; iPenWidth: double);
var
  iEdge, iCM: double;
  iZone, iLastZone: integer;   //GaiaCAD 3.0 Must integer;
  jdx, idx, iPoints, iLastPoint: integer;
  iChain, iPoly: integer;
  PolyChain: TGroupsStore;
  PointStore: TPointStore;
  ptLL, tmpLL, lastLL: TPointLL;
begin
  //RequiredState([csHandleValid, csPenValid, csBrushValid, csFontValid]);

  with Earth do
    if Projection.RenderBackFace then
    begin
      FTransparentOptions.Pen.RenderAttribute(self, False);
      FTransparentOptions.Brush.RenderAttribute(self, False);
    end;

  with Projection do
    if (pfContinuous in Flags) and (osClosed in State) then
    begin

      for iChain := 0 to Chains.Count - 1 do
      begin
        iPoints := ProjectionModel.PointStoreToXY(Chains[iChain], 0, osClosed in State);
        // ------------ Draw-----------------------
        DrawClippedPoly(iPoints, State + [osClipLeft, osClipRight, osClipTop, osClipBottom], iPenWidth);
        //------------------------------------------
      end;
      Exit;
    end;

  // Render the poly in parts
  iCM := (Projection.CentralMeridian * GU_DEGREE);
  iEdge := Mod180Float(GU_180_DEGREE + iCM);

  PolyChain := TGroupsStore.Create;
  PolyChain.StoreHeight := Chains.StoreHeight;
  try
    for iChain := 0 to Chains.Count - 1 do
    begin
      PolyChain.Count := 0;
      iPoly := 0;

      iPoints := Chains[iChain].Count - 1;
      if iPoints < 1 then
        Continue;

      // set the height flag
      //      PolyChain[iPoly].StoreHeight := Chains[iChain].StoreHeight;

      if osClosed in State then // adjust last point for polygons
        iLastPoint := iPoints
      else
        iLastPoint := 0;

      iLastZone := Round(Mod180Float(Chains[iChain].AsLL[iLastPoint].iLongX - iCM) / GU_90_DEGREE);

      // split into chains
      for idx := 0 to iPoints do
      begin
        ptLL := Chains[iChain].AsLL[idx];
        iZone := Round(Mod180Float(ptLL.iLongX - iCM) / GU_90_DEGREE);
        if ((iZone < 0) and (iLastZone > 0)) or ((iZone > 0) and (iLastZone < 0)) then
        begin
          tmpLL := ptLL;

          if idx > 0 then
            iLastPoint := idx - 1;
          lastLL := Chains[iChain].AsLL[iLastPoint];

          // find crossover point for edge
          with tmpLL do
            iLatY := iLatY + ((lastLL.iLatY - iLatY) * (LongDiff(iEdge, iLongX) / LongDiff(lastLL.iLongX, iLongX)));

          tmpLL.iLongX := iEdge - iLastZone * 2;
          PolyChain[iPoly].Add(tmpLL); // last point on current chain

          Inc(iPoly);
          tmpLL.iLongX := iEdge + iLastZone * 2;
          PolyChain[iPoly].Add(tmpLL); // fist point on new chain
        end;
        PolyChain[iPoly].Add(ptLL);
        iLastZone := iZone;
      end;

      if osClosed in State then // render alternate chains as the polygons
        with Projection.ProjectionModel do
        begin
          iPoints := 0;
          idx := 0;
          while idx < PolyChain.Count do
          begin
            iPoints := PointStoreToXY(PolyChain[idx], iPoints, osClosed in State);
            Inc(idx, 2);
          end;

          if idx <> PolyChain.Count then
          begin
            //----------Draw --------------------
            DrawClippedPoly(iPoints, State, iPenWidth);
            //-----------------------------------
            iPoints := 0;
          end
          else
          begin // connect to first chain
            PointStore := TPointStore.Create;
            PointStore.StoreHeight := Chains.StoreHeight;

            with PolyChain[idx - 2] do
              tmpLL := AsLL[Count - 1];
            ptLL := tmpLL;

            while tmpLL.iLatY > -GU_90_DEGREE do
            begin
              tmpLL.iLatY := tmpLL.iLatY - GU_DEGREE;
              PointStore.Add(tmpLL);
            end;

            tmpLL.iLatY := -GU_90_DEGREE + 1;

            PointStore.Add(tmpLL);

            tmpLL.iLongX := iEdge + 2;
            ptLL.iLongX := iEdge + 2;

            while tmpLL.iLatY < ptLL.iLatY do
            begin
              tmpLL.iLatY := tmpLL.iLatY + GU_DEGREE;
              PointStore.Add(tmpLL);
            end;
            PointStore.Add(ptLL);
            iPoints := PointStoreToXY(PointStore, iPoints, osClosed in State);
            PointStore.Free;
          end;

          idx := 1;
          while idx < PolyChain.Count do
          begin
            iPoints := PointStoreToXY(PolyChain[idx], iPoints, osClosed in State);
            Inc(idx, 2);
          end;

          //----------Draw --------------------
          DrawClippedPoly(iPoints, State, iPenWidth);
          //-----------------------------------------
        end
      else // render as polyline
        with Projection.ProjectionModel do
          for idx := 0 to PolyChain.Count - 1 do
          begin
            iPoints := 0;
            for jdx := 0 to PolyChain[idx].Count - 1 do
              if PointLLToXY(PolyChain[idx][jdx], iPoints) then
                Inc(iPoints)
              else
              begin
                if iPoints > 1 then
                  DrawClippedPoly(iPoints, State, iPenWidth);
                iPoints := 0;
              end;

            //----------Draw --------------------
            DrawClippedPoly(iPoints, State, iPenWidth);
            //----------------------------------
          end;
    end;
  finally
    PolyChain.Free;
  end;
end;

procedure TEarthCanvas.DrawVectorMatrixLine(vec: TV3D; const mat: TGMatrix; iHeight: double; iSteps: integer);
var
  idx: integer;
  Chains: TGroupsStore;
  pt: TPointLL;
begin
  Chains := TGroupsStore.Create;
  Chains.StoreHeight := iHeight > 0;

  for idx := 0 to iSteps do
  begin
    pt := V3DToPointLL(vec);
    pt.iHeightZ := iHeight;
    Chains[0].Add(pt);
    vec := V3DMatrixMul(mat, vec);
  end;
  RenderChainStore(Chains,
    [osFace, osBackFace, osClipLeft, osClipRight, osClipTop, osClipBottom],
    Earth.EarthCanvas.Canvas.Pen.Width);
  Chains.Free;
end;

//===============================================================================================
//===================== TEarthAttribute =========================================================
//===============================================================================================

procedure TEarthAttribute.RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean);
begin
  FEarthCanvas := EarthCanvas;
end;

destructor TEarthAttribute.Destroy;
begin
  inherited Destroy;
end;

function TEarthAttribute.IsEquals(Attr: TEarthAttribute): boolean;
begin
  Result := False;
end;

function TEarthAttribute.Clone(aParent: TPersistent): TEarthAttribute;
begin
  Result := nil;
end;

procedure TEarthAttribute.WriteProperties(Writer: TEarthStreamWriter);
begin
  //...
end;

procedure TEarthAttribute.ReadProperties(Reader: TEarthStreamReader);
begin
  //...
end;

//===============================================================================================
//===================== TEarthPen ===============================================================
//===============================================================================================

constructor TEarthPen.Create(aParent: TPersistent);
begin
  inherited Create;
  fEarthCanvas := nil;
  fParent := aParent;
  fLastProjectionAltitude := -9999;
  FPenColor := clBlack32;
  FPenStyle := psSolid;
  FPenWidth := 1.0;
  FPenUnit := euPixel;
end;

procedure TEarthPen.RedrawObject;
begin
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate
  if fParent = nil then
    exit;

  if fparent is TEarthRoot then
    TEarthRoot(fParent).RedrawObject;
end;

procedure TEarthPen.Define(aColor: TColor32; aStyle: TPenStyle; aWidth: integer; aUnit: TEarthUnitTypes);
begin
  FPenColor := aColor;
  FPenWidth := aWidth;
  FPenStyle := aStyle;
  FPenUnit := aUnit;
end;

procedure TEarthPen.Assign(Attr: TPersistent);
begin
  if Attr is TEarthPen then
    with TEarthPen(Attr) do
    begin
      BeginUpdate;
      Self.PenColor := PenColor;
      Self.PenStyle := PenStyle;
      Self.PenWidth := PenWidth;
      Self.PenUnit := PenUnit;
      EndUpdate;
    end
  else
    inherited Assign(Attr);
end;

function TEarthPen.Clone(aParent: TPersistent): TEarthAttribute;
begin
  Result := TEarthPenClass(Self.ClassType).Create(aParent);
  Result.Assign(Self);
end;

function TEarthPen.IsEquals(Attr: TEarthAttribute): boolean;
begin
  Result := Attr is TEarthPen;
  if Result then
    with TEarthPen(Attr) do
      Result := (Self.PenColor = PenColor) and (Self.PenStyle = PenStyle) and (Self.PenWidth = PenWidth) and (Self.PenUnit = PenUnit);
end;

procedure TEarthPen.SetPenColor(Value: TColor32);
begin
  FPenColor := Value;
  GetDrawPenWidthInPixels;//Must find Real fDrawPenWidth
  RedrawObject;
end;

procedure TEarthPen.SetPenStyle(Value: TPenStyle);
begin
  FPenStyle := Value;
  GetDrawPenWidthInPixels;
  RedrawObject;
end;

procedure TEarthPen.SetPenWidth(Value: double);
begin
  FPenWidth := Value;
  GetDrawPenWidthInPixels;
  RedrawObject;
end;

procedure TEarthPen.SetPenUnit(Value: TEarthUnitTypes);
begin
  FPenUnit := Value;
  GetDrawPenWidthInPixels;
  RedrawObject;
end;

function TEarthPen.GetDrawPenWidthInPixels: double;
begin
  if fPenUnit = euPixel then
  begin
    FDrawPenWidth := FPenWidth;
  end
  else
  begin
    FDrawPenWidth := FPenWidth;  //init
    if (fEarthCanvas <> nil) then
    begin
      FDrawPenWidth := FEarthCanvas.ScaleUnitsToDevice(fPenWidth, fPenUnit);
    end;
  end;

  if (fEarthCanvas <> nil) then
    fLastProjectionAltitude := FEarthCanvas.Projection.Altitude;

  if FDrawPenWidth < 0.01 then
    FDrawPenWidth := 0.01;
  Result := FDrawPenWidth;
end;

procedure TEarthPen.RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean);
begin
  inherited RenderAttribute(EarthCanvas, bSelected);

  if fLastProjectionAltitude <> EarthCanvas.Projection.Altitude then
    GetDrawPenWidthInPixels;

  if bSelected then
  begin
    EarthCanvas.Earth.fSelectedOjectOptions.Pen.RenderAttribute(EarthCanvas, False);
    if EarthCanvas.Earth.fSelectedOjectOptions.PenWidthSameWithNormal then
      EarthCanvas.LineWidthSet(FDrawPenWidth);

  end
  else
  begin
    EarthCanvas.LineColorSet(EarthCanvas.Color32ToColorAgx(fPenColor));
    EarthCanvas.LineWidthSet(FDrawPenWidth);
  end;
end;

procedure TEarthPen.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      FPenColor := StringToColor(AttributeByName('Color', ColorToString(PenColor)));
      FPenStyle := TPenStyle(IntAttributeByName('Style', Ord(PenStyle)));
      FPenWidth := FloatAttributeByName('Width', PenWidth);
      FPenUnit := StrToUnits(AttributeByName('Unit', UnitsToStr(PenUnit)));
    end;
end;

function TEarthPen.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddAttribute('Color', ColorToString(PenColor));
  Result.AddIntAttribute('Style', Ord(PenStyle));
  Result.FloatAttributeByName('Width', PenWidth);
  Result.AddAttribute('Unit', UnitsToStr(PenUnit));
end;

procedure TEarthPen.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(2);   //Write Object Stream Version

  Writer.WriteBuffer(FPenColor, SizeOf(TColor32));
  Writer.WriteBuffer(FPenStyle, SizeOf(TPenStyle));
  Writer.WriteBuffer(FPenWidth, SizeOf(double));
  Writer.WriteBuffer(FPenUnit, SizeOf(TEarthUnitTypes));
end;

procedure TEarthPen.ReadProperties(Reader: TEarthStreamReader);
var
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;
  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version
  //...........................................................
  if iVer = 1 then
  begin
    Reader.ReadBuffer(FPenColor, SizeOf(TColor));
    Reader.ReadBuffer(FPenStyle, SizeOf(TPenStyle));

    if giFileVersion >= TG_FILEVERSION400 then
    begin
      FPenWidth := Reader.ReadInteger;
      Reader.ReadBuffer(FPenUnit, SizeOf(TEarthUnitTypes));
    end
    else
    begin
      Reader.ReadBuffer(FPenWidth, SizeOf(byte));
      FPenUnit := euPixel;
    end;

    FPenColor := Color32(FPenColor);     //correct color32
  end;
  //.................................................
  if iVer > 1 then
  begin
    Reader.ReadBuffer(FPenColor, SizeOf(TColor32));
    Reader.ReadBuffer(FPenStyle, SizeOf(TPenStyle));
    FPenWidth := Reader.ReadDouble;
    Reader.ReadBuffer(FPenUnit, SizeOf(TEarthUnitTypes));
  end;
end;

//===============================================================================================
//===================== TEarthBrush =============================================================
//===============================================================================================

constructor TEarthBrush.Create(aParent: TPersistent);
begin
  inherited Create;
  fParent := aParent;
  FBrushColor := clBlack32;
  FBrushStyle := gbsSolid;
  FBrushBkColor := clLightGray32;
  FBrushFillImage := TBitmap32.Create;
  FBrushFillImage.OnChange := @OnBrushFillImageChanged;
  FDrawFillWithImage := False;
end;

destructor TEarthBrush.Destroy;
begin
  FBrushFillImage.Free;
  inherited Destroy;
end;

procedure TEarthBrush.OnBrushFillImageChanged(Sender: TObject);
begin
  RedrawObject;
end;

procedure TEarthBrush.RedrawObject;
begin
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate
  if fParent = nil then
    exit;

  if fparent is TEarthRoot then
    TEarthRoot(fParent).RedrawObject;
end;

procedure TEarthBrush.Define(Color: TColor; Style: TEarthBrushStyle);
begin
  FBrushColor := Color;
  FBrushBkColor := Color;
  FBrushStyle := Style;
end;

procedure TEarthBrush.Assign(Attr: TPersistent);
begin
  if Attr is TEarthBrush then
    with TEarthBrush(Attr) do
    begin
      Self.BrushColor := BrushColor;
      Self.BrushBkColor := BrushBkColor;
      Self.BrushStyle := BrushStyle;
    end
  else
    inherited Assign(Attr);
end;

function TEarthBrush.Clone(aParent: TPersistent): TEarthAttribute;
begin
  Result := TEarthBrushClass(Self.ClassType).Create(aParent);
  Result.Assign(Self);
end;

function TEarthBrush.IsEquals(Attr: TEarthAttribute): boolean;
begin
  Result := Attr is TEarthBrush;
  if Result then
    with TEarthBrush(Attr) do
      Result := (Self.BrushColor = BrushColor) and (Self.BrushBkColor = BrushBkColor) and (Self.BrushStyle = BrushStyle);
end;

procedure TEarthBrush.SetBrushBkColor(const Value: TColor32);
begin
  FBrushBkColor := Value;
  RedrawObject;
end;

procedure TEarthBrush.SetBrushColor(Value: TColor32);
begin
  FBrushColor := Value;
  RedrawObject;
end;

procedure TEarthBrush.SetBrushStyle(Value: TEarthBrushStyle);
begin
  FBrushStyle := Value;
  RedrawObject;
end;

procedure TEarthBrush.SetBrushFillImage(Val: TBitmap32);
begin
  if val = nil then
  begin
    fBrushFillImage.SetSize(0, 0);
  end
  else
  begin
    fBrushFillImage.Assign(val);
  end;
end;

procedure TEarthBrush.SetDrawFillWithImage(Val: boolean);
begin
  FDrawFillWithImage := Val;
  RedrawObject;
end;

procedure TEarthBrush.RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean);
begin
  inherited RenderAttribute(EarthCanvas, bSelected);

  if (bSelected) and (EarthCanvas.Earth.fSelectedOjectOptions.BrushSameWithNormal = False) then
  begin
    EarthCanvas.Earth.fSelectedOjectOptions.Brush.RenderAttribute(EarthCanvas, False);
  end
  else
  begin
    EarthCanvas.FillColorSet(EarthCanvas.Color32ToColorAgx(fBrushColor));
  end;

end;

procedure TEarthBrush.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      FBrushColor := StringToColor(AttributeByName('Color', ColorToString(BrushColor)));
      FBrushBkColor := StringToColor(AttributeByName('BkColor', ColorToString(BrushColor)));
      FBrushStyle := TEarthBrushStyle(IntAttributeByName('Style', Ord(BrushStyle)));
    end;
end;

function TEarthBrush.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddAttribute('Color', ColorToString(BrushColor));
  Result.AddAttribute('BkColor', ColorToString(BrushBkColor));
  Result.AddIntAttribute('Style', Ord(BrushStyle));
end;

procedure TEarthBrush.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(2);   //Write Object Stream Version

  Writer.WriteBuffer(FBrushColor, SizeOf(TColor32));
  Writer.WriteBuffer(FBrushBkColor, SizeOf(TColor32));
  Writer.WriteBuffer(FBrushStyle, SizeOf(TEarthBrushStyle));
  Writer.WriteBirmap32(fBrushFillImage);
  Writer.WriteBoolean(FDrawFillWithImage);
end;

procedure TEarthBrush.ReadProperties(Reader: TEarthStreamReader);
var
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;
  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version
  //...................... Old .......................................
  if iVer = 1 then
  begin
    Reader.ReadBuffer(FBrushColor, SizeOf(TColor));
    if giFileVersion >= TG_FILEVERSION405 then
      Reader.ReadBuffer(FBrushBkColor, SizeOf(TColor))
    else
      FBrushBkColor := FBrushColor;

    Reader.ReadBuffer(FBrushStyle, SizeOf(TEarthBrushStyle));

    FBrushColor := Color32(FBrushColor);     //correct color32
    FBrushBkColor := Color32(FBrushBkColor); //correct color32
  end;
  //.............................................................
  if iVer > 1 then
  begin
    Reader.ReadBuffer(FBrushColor, SizeOf(TColor));
    if giFileVersion >= TG_FILEVERSION405 then
      Reader.ReadBuffer(FBrushBkColor, SizeOf(TColor))
    else
      FBrushBkColor := FBrushColor;

    Reader.ReadBuffer(FBrushStyle, SizeOf(TEarthBrushStyle));
    Reader.ReadBirmap32(fBrushFillImage);
    FDrawFillWithImage := Reader.ReadBoolean;

  end;
end;

//===============================================================================================
//===================== TEarthFont ==============================================================
//===============================================================================================

constructor TEarthFont.Create(aParent: TPersistent);
begin
  inherited Create;
  fLastProjectionAltitude := -9999;
  fParent := aParent;
  FFontName := 'Arial';
  FFontSizeUnits := eupixel;
  FFontOutlineUnits := eupixel;
  FFontFillColor := clBlack32;
  FFontOutlineColor := clWhite32;
  FFontSize := 9;
  FFontAngle := 90;
  fDrawOutLine := False;
  FDrawFillWithColor := True;
  FFontOutlineSize := 1;
  FFontFillImage := TBitmap32.Create;
  FFontFillImage.OnChange := @OnFontFillImageChanged;
  FDrawFillWithImage := False;
end;

destructor TEarthFont.Destroy;
begin
  FFontFillImage.Free;
  inherited Destroy;
end;

procedure TEarthFont.RedrawObject;
begin
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate
  if fParent = nil then
    exit;

  if fparent is TEarthRoot then
    TEarthRoot(fParent).RedrawObject;
end;

procedure TEarthFont.Define(const Name: TEarthFontName; FillColor, OutLineColor: TColor32; Size: integer;
  AUnit: TEarthUnitTypes; Angle: integer; Style: TFontStyles);
begin
  FFontName := Name;
  FFontFillColor := FillColor;
  FFontOutLineColor := OutLineColor;
  FFontSize := Size;
  FFontSizeUnits := AUnit;
  FFontAngle := Angle;
  FontStyle := Style;
end;

procedure TEarthFont.Assign(Attr: TPersistent);
begin

  if Attr is TEarthFont then
    with TEarthFont(Attr) do
    begin
      BeginUpdate;
      Self.FontName := FontName;
      Self.FontFillColor := FontFillColor;
      Self.FontOutLineColor := FontOutLineColor;
      Self.FontStyle := FontStyle;
      Self.FontSizeUnits := FontSizeUnits;
      Self.FontSize := FontSize;
      Self.FontAngle := FontAngle;
      EndUpdate;
    end
  else
    inherited Assign(Attr);
end;

function TEarthFont.Clone(aParent: TPersistent): TEarthAttribute;
begin
  Result := TEarthFontClass(Self.ClassType).Create(aParent);
  Result.Assign(Self);
end;

function TEarthFont.IsEquals(Attr: TEarthAttribute): boolean;
begin
  Result := Attr is TEarthFont;
  if Result then
    with TEarthFont(Attr) do
      Result := (CompareText(Self.FontName, FontName) = 0) and (Self.FontFillColor = FontFillColor) and
        (Self.FontOutLineColor = FontOutLineColor) and (Self.FontStyle = FontStyle) and (Self.FontSizeUnits = FontSizeUnits) and
        (Self.FontSize = FontSize) and (Self.FontAngle = FontAngle);
end;

procedure TEarthFont.OnFontFillImageChanged(Sender: TObject);
begin
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontFillImage(Val: TBitmap32);
begin
  if val = nil then
  begin
    fFontFillImage.SetSize(0, 0);
  end
  else
  begin
    fFontFillImage.Assign(val);
  end;
end;

procedure TEarthFont.SetDrawFillWithImage(Val: boolean);
begin
  FDrawFillWithImage := Val;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontSizeUnits(Value: TEarthUnitTypes);
begin
  FFontSizeUnits := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontFillColor(Value: TColor32);
begin
  FFontFillColor := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontOutlineColor(Value: TColor32);
begin
  FFontOutlineColor := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontOutlineUnits(Value: TEarthUnitTypes);
begin
  FFontOutlineUnits := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontAngle(Value: double);
begin
  FFontAngle := Value;
  if FFontAngle < 0 then
    FFontAngle := 0;
  if FFontAngle > 360 then
    FFontAngle := 360;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontSize(Value: double);
begin
  FFontSize := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontOutlineSize(Value: double);
begin
  FFontOutlineSize := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetFontName(Value: TEarthFontName);
begin
  FFontName := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetDrawOutLine(Value: boolean);
begin
  FDrawOutLine := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

procedure TEarthFont.SetDrawFillWithColor(Value: boolean);
begin
  FDrawFillWithColor := Value;
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

function TEarthFont.GetStyle: TFontStyles;
begin
  Result := TFontStyles(FStyle);
end;

procedure TEarthFont.SetStyle(Value: TFontStyles);
begin
  FStyle := integer(Value);
  GetDrawHeightInPixels;
  GetDrawOutLineWithdInPixels;
  RedrawObject;
end;

function TEarthFont.GetDrawHeightInPixels: double;
begin
  fDrawHeight := FFontSize;

  if (fEarthCanvas <> nil) then
  begin
    fDrawHeight := FEarthCanvas.FontRenderHeight(FFontSize, FontSizeUnits);
    if fDrawHeight >= FEarthCanvas.Earth.MinTextHeight then
      if (FEarthCanvas.Earth.MaxTextHeight > 0) and (fDrawHeight > FEarthCanvas.Earth.MaxTextHeight) then
        fDrawHeight := FEarthCanvas.Earth.MaxTextHeight;
  end;

  fDrawHeight := ABS(fDrawHeight * 1.4);
  Result := fDrawHeight;
end;

function TEarthFont.GetDrawOutLineWithdInPixels: double;
begin
  if fFontOutlineUnits = euPixel then
  begin
    fDrawOutLineWidth := fFontOutlineSize;
  end
  else
  begin
    fDrawOutLineWidth := fFontOutlineSize;  //init
    if (fEarthCanvas <> nil) then
    begin
      fDrawOutLineWidth := FEarthCanvas.ScaleUnitsToDevice(fFontOutlineSize, fFontOutlineUnits);
    end;
  end;

  if fDrawOutLineWidth < 0.01 then
    fDrawOutLineWidth := 0.01;
  Result := fDrawOutLineWidth;
end;

procedure TEarthFont.RenderAttribute(EarthCanvas: TEarthCanvas; bSelected: boolean);
begin
  inherited RenderAttribute(EarthCanvas, bSelected);

  if fLastProjectionAltitude <> EarthCanvas.Projection.Altitude then
  begin
    GetDrawHeightInPixels;
    GetDrawOutLineWithdInPixels;
    fLastProjectionAltitude := FEarthCanvas.Projection.Altitude;
  end;

  FEarthCanvas.TextAlignmentSet(AGX_AlignCenter, AGX_AlignCenter);

  FEarthCanvas.Font(FontName,
    fDrawHeight,
    (fsBold in FontStyle),
    (fsItalic in FontStyle),
    AGX_VectorFontCache,
    FontAngle - 90);

  if fDrawOutLine then
  begin
    FEarthCanvas.LineWidthSet(fDrawOutLineWidth);
    FEarthCanvas.LineColorSet(EarthCanvas.Color32ToColorAgx(fFontOutLineColor));
  end
  else
    FEarthCanvas.NoLine;

  if bSelected then
  begin

    if EarthCanvas.Earth.fSelectedOjectOptions.FontSameWithNormal then
    begin
      EarthCanvas.FillColorSet(EarthCanvas.Color32ToColorAgx(EarthCanvas.Earth.fSelectedOjectOptions.Pen.PenColor));
      FEarthCanvas.NoLine;
    end
    else
    begin
      EarthCanvas.Earth.fSelectedOjectOptions.Font.RenderAttribute(EarthCanvas, False);
    end;

  end
  else
  begin
    if FDrawFillWithColor then
      EarthCanvas.FillColorSet(EarthCanvas.Color32ToColorAgx(fFontFillColor))
    else
      EarthCanvas.NoFill;
  end;
end;

procedure TEarthFont.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      FFontName := AttributeByName('Name', FontName);
      FFontFillColor := StringToColor(AttributeByName('Color', ColorToString(FontFillColor)));
      FFontOutlineColor := StringToColor(AttributeByName('ColorOutline', ColorToString(FFontOutlineColor)));
      FStyle := byte(IntAttributeByName('Style', Ord(FStyle)));
      FFontSize := FloatAttributeByName('Size', FontSize);
      FFontAngle := FloatAttributeByName('Angle', FontAngle);
      FFontSizeUnits := StrToUnits(AttributeByName('FontSizeUnits', UnitsToStr(FontSizeUnits)));
      FontOutlineUnits := StrToUnits(AttributeByName('FontOutlineUnits', UnitsToStr(FontOutlineUnits)));
      FFontOutlineSize := FloatAttributeByName('FontOutlineSize', FontOutlineSize);
      FDrawOutLine := BoolAttributeByName('DrawOutLine', DrawOutLine);
      FDrawFillWithColor := BoolAttributeByName('DrawFillWithColor', DrawFillWithColor);
    end;
end;

function TEarthFont.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddAttribute('Name', FontName);
  Result.AddAttribute('Color', ColorToString(fFontFillColor));
  Result.AddAttribute('ColorOutline', ColorToString(FFontOutlineColor));
  Result.AddIntAttribute('Style', FStyle);
  Result.AddFloatAttribute('Size', FontSize);
  Result.AddFloatAttribute('Angle', FontAngle);
  Result.AddAttribute('FontSizeUnits', UnitsToStr(FontSizeUnits));
  Result.AddAttribute('FontOutlineUnits', UnitsToStr(FontOutlineUnits));
  Result.AddFloatAttribute('FontOutlineSize', FFontOutlineSize);
  Result.AddBoolAttribute('DrawOutLine', FDrawOutLine);
  Result.AddBoolAttribute('DrawFillWithColor', FDrawFillWithColor);
end;

procedure TEarthFont.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(2);   //Write Object Stream Version

  Writer.WriteBuffer(FontFillColor, SizeOf(TColor32));
  Writer.WriteBuffer(FFontOutlineColor, SizeOf(TColor32));
  Writer.WriteBuffer(FontSizeUnits, SizeOf(TEarthUnitTypes));
  Writer.WriteBuffer(FontOutlineUnits, SizeOf(TEarthUnitTypes));
  Writer.WriteDouble(FontSize);
  Writer.WriteDouble(FontOutlineSize);
  Writer.WriteDouble(FontAngle);
  Writer.WriteBuffer(FStyle, SizeOf(byte));
  Writer.WriteShortString(FFontName);
  Writer.WriteBoolean(FDrawOutLine);
  Writer.WriteBoolean(FDrawFillWithColor);
  Writer.WriteBirmap32(fFontFillImage);
  Writer.WriteBoolean(FDrawFillWithImage);
end;

procedure TEarthFont.ReadProperties(Reader: TEarthStreamReader);
var
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;
  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version
  //...................... Old ..................................
  if iVer = 1 then
  begin
    Reader.ReadBuffer(fFontFillColor, SizeOf(TColor));
    Reader.ReadBuffer(fFontSizeUnits, SizeOf(TEarthUnitTypes));
    FontSize := Round(Reader.ReadSmallInt);
    FontAngle := Round(Reader.ReadSmallInt);
    Reader.ReadBuffer(FStyle, SizeOf(byte));
    FontName := Reader.ReadShortString;

    fFontFillColor := color32(FontFillColor);  //corect color32
    fFontAngle := fFontAngle + 90;               //corect Angle;
    FFontOutlineColor := clWhite32;
    FdrawOutline := False;
    FFontOutlineUnits := eupixel;
    FFontOutlineSize := 1;
    fDrawOutLine := False;
    fDrawFillWithColor := True;
  end;
  //...................... ..................................
  if iVer > 1 then
  begin
    Reader.ReadBuffer(FFontFillColor, SizeOf(TColor32));
    Reader.ReadBuffer(FFontOutlineColor, SizeOf(TColor32));
    Reader.ReadBuffer(FFontSizeUnits, SizeOf(TEarthUnitTypes));
    Reader.ReadBuffer(FFontOutlineUnits, SizeOf(TEarthUnitTypes));
    FontSize := Reader.ReadDouble;
    FontOutlineSize := Reader.ReadDouble;
    FontAngle := Reader.ReadDouble;
    Reader.ReadBuffer(FStyle, SizeOf(byte));
    FontName := Reader.ReadShortString;
    DrawOutLine := Reader.ReadBoolean;
    DrawFillWithColor := Reader.ReadBoolean;
    Reader.ReadBirmap32(fFontFillImage);
    FDrawFillWithImage := Reader.ReadBoolean;
  end;
end;

//===============================================================================================
//===================== TEarthLayerStore ========================================================
//===============================================================================================

constructor TEarthLayerStore.Create(aParent: TPersistent);
var
  aPresenter: TEarthPresenter;
begin
  if not (aParent is TCustomEarth) or not (aParent is TEarthLayer) or not (aParent is TEarthObject) or (aParent = nil) then
    Assert(True, 'TEarthLayerStore.Create passed NOT Correct Class as Parent or NIL');
  //................................
  inherited Create;
  FParent := aParent;

  if fParent is TCustomEarth then
    FEarth := TCustomEarth(fParent);
  if fParent is TEarthLayer then
    FEarth := TEarthLayer(fParent).Earth;
  if fParent is TEarthObject then
    FEarth := TEarthObject(fParent).ParentEarth;

  FGlobalPresenters := TEarthPresenterStore.Create(self);
  FLayers := TObjectList.Create(false);

  // Add in 2 default presenters
  if fParent is TCustomEarth then
  begin
    aPresenter := TPointPresenter.Create(FGlobalPresenters, GLOBAL_POINT_PRESENTER);
    aPresenter := TPolyPresenter.Create(FGlobalPresenters, GLOBAL_POLY_PRESENTER);
  end;
end;

destructor TEarthLayerStore.Destroy;
begin
  Clear;
  FLayers.free;
  FGlobalPresenters.Free;
  FGlobalPresenters := nil;
  inherited Destroy;
end;

function TEarthLayerStore.GetParentLayer: TEarthLayer;
begin
  Result := nil;
  if fParent is TEarthLayer then
    Result := TEarthLayer(fParent);
end;

function TEarthLayerStore.AddNew: TEarthLayer;
var
  ALayer: TEarthLayer;
begin
  ALayer := TEarthLayer.Create(Self);
  aLayer.Name := 'NewLayer' + IntToStr(aLayer.Index);
  RedrawObject;
  Result := ALayer;
end;

procedure TEarthLayerStore.RedrawObject;
begin
  if FIsUpdating then
    exit;
  if Earth <> nil then
    Earth.Notify(gnLayersChanged, Self);
end;

procedure TEarthLayerStore.SetCount(iValue: integer);
begin
  // Free up Layers if reducing count
  while Count > iValue do
    Delete(Layer[Count - 1]);

  FiCount := FLayers.Count;
end;

function TEarthLayerStore.IndexByName(const sName: string): integer;
begin
  for Result := 0 to FLayers.Count - 1 do
    if CompareText(TEarthLayer(FLayers.Items[Result]).Name, sName) = 0 then
      Exit;
  Result := -1;
end;

function TEarthLayerStore.ByName(const sName: string): TEarthLayer;
var
  idx: integer;
begin
  idx := IndexByName(sName);
  if idx >= 0 then
    Result := Layer[idx]
  else
    Result := nil;
end;

function TEarthLayerStore.GetLayer(iIndex: integer): TEarthLayer;
begin
  try
    Result := TEarthLayer(FLayers.Items[iIndex]);
  except
    raise EEarthException.CreateFmt(rsEBadLayerIndexMsg, [iIndex]);
  end;
end;

function TEarthLayerStore.GetLayerStoreMER: TMER;
var
  idx: integer;
begin
  Result := MER(0, 0, 0, 0);
  for idx := 0 to Count - 1 do
    Result := UnionMER(Result, Layer[idx].LayerMER);
end;

procedure TEarthLayerStore.Add(ALayer: TEarthLayer);
begin
  ALayer.Parent := Self;
  if FLayers <> nil then
  begin
    FLayers.Add(ALayer);
    FiCount:=FLayers.Count;
    Earth.RedrawLayers;
  end;
end;

procedure TEarthLayerStore.Assign(Source: TPersistent);
var
  idx: integer;
begin
  if Source is TEarthLayerStore then
  begin
    Clear;
    Self.GlobalPresenters.Assign(TEarthLayerStore(Source).GlobalPresenters);

    for idx := 0 to TEarthLayerStore(Source).Count - 1 do
      TEarthLayerStore(Source)[idx].Clone(self);
  end
  else
  if Source is TEarthLayer then
    Add(TEarthLayer(Source).Clone(self));
end;

procedure TEarthLayerStore.WriteProperties(Writer: TEarthStreamWriter);
var
  idx: integer;
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(1);   //Write Object Stream Version

  Writer.WriteInteger(Count);
  for idx := 0 to Count - 1 do
  begin
    Writer.WriteShortString(Layer[idx].ClassName);
    Layer[idx].WriteProperties(Writer);
  end;

end;

procedure TEarthLayerStore.ReadProperties(Reader: TEarthStreamReader);
var
  idx, iCount: integer;
  oNew: TEarthLayer;
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;

  Clear;
  BeginUpdate;

  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger; //Read Object Stream Version

  iCount := Reader.ReadInteger;
  for idx := 0 to iCount - 1 do
  begin
    oNew := TEarthLayerClass(FindClass(Reader.ReadShortString)).Create(self);
    oNew.ReadProperties(Reader);
  end;

  endUpdate;
end;

procedure TEarthLayerStore.WriteToSteam(aStream: TStream);
var
  Writer: TEarthStreamWriter;
begin
  if aStream = nil then
    exit;

  Writer := TEarthStreamWriter.Create(aStream);
  WriteProperties(Writer);
  Writer.DataStream := nil;
  Writer.Free;
end;

procedure TEarthLayerStore.ReadFromSteam(aStream: TStream);
var
  Reader: TEarthStreamReader;
begin
  if aStream = nil then
    exit;
  beginUpdate;
  Reader := TEarthStreamReader.Create(aStream);
  ReadProperties(Reader);
  Reader.DataStream := nil;
  Reader.Free;
  endUpdate;
end;

procedure TEarthLayerStore.Remove(ALayer: TEarthLayer);
var
  idx: integer;
begin
  idx := IndexOf(ALayer);
  if idx >= 0 then
  begin
    FLayers.Delete(idx);
    FiCount:=FLayers.Count;
    Earth.RedrawLayers;
  end;
end;

procedure TEarthLayerStore.Delete(ALayer: TEarthLayer);
begin
  if ALayer <> nil then
  begin
    if alayer = Earth.selectedLayer then
      Earth.selectedLayer := nil;

    Remove(ALayer);
    ALayer.Free;
    RedrawObject;
  end;
end;

procedure TEarthLayerStore.DeleteByName(const sName: string);
var
  Temp: TEarthLayer;
begin
  Temp := nil;
  ByName(sName);
  if temp = nil then
    exit;
  Delete(Temp);
end;

procedure TEarthLayerStore.Move(iFrom, iTo: integer);
begin
  if (iTo >= 0) and (iTo < Count) then
  begin
    FLayers.Move(iFrom, iTo);
    RedrawObject;
  end;
end;

procedure TEarthLayerStore.Clear;
begin
  beginUpdate;
  SetCount(0);
  EndUpdate;
end;

function TEarthLayerStore.IndexOf(ALayer: TEarthLayer): integer;
begin
  if FLayers <> nil then
    for Result := 0 to FLayers.Count - 1 do
      if FLayers.Items[Result] = ALayer then
        Exit;
  Result := -1;
end;

function TEarthLayerStore.SaveEnvironment: TGXML_Element;
var
  idx: integer;
begin
  Result := TGXML_Element.Create;

  Result.AddElement('GlobalPresenters', GlobalPresenters.SaveEnvironment);

  for idx := 0 to Count - 1 do
    Result.AddElement('Layer', Layer[idx].SaveEnvironment);

end;

procedure TEarthLayerStore.LoadEnvironment(Element: TGXML_Element);
var
  idx: integer;
  //..........................
  function CreateLayer(const Ident: string; iOccourance: integer): TEarthLayer;
  var
    sClassName: string;
    LayerElement: TGXML_Element;
  begin
    Result := nil;
    LayerElement := Element.ElementByName(Ident, iOccourance);

    if LayerElement <> nil then
    begin
      // Get the ClassName of the Layer
      sClassName := LayerElement.AttributeByName('Class', '');

      // Create the Object
      Result := TEarthLayerClass(FindClass(sClassName)).Create(Earth.Layers);

      // Load the properties
      Result.LoadEnvironment(LayerElement);
      Result.Objects.Active := True;
      if not Result.Objects.Active then
      begin
        Result.Free;
        Result := nil;
      end;
    end;
  end;
  //..........................
begin
  Clear;
  if Element <> nil then
    with Element do
    begin
      GlobalPresenters.Clear;
      GlobalPresenters.LoadEnvironment(ElementByName('GlobalPresenters', 0));

      // Load in all the Layers
      idx := 0;
      while CreateLayer('Layer', idx) <> nil do
        Inc(idx);

    end;
end;

procedure TEarthLayerStore.ObjectsUnSelectAll;
var
  idx, jdx: integer;
begin
  for idx := 0 to Count - 1 do
  begin
    //... Unselect All Layer.Layers................
    Layer[idx].Layers.ObjectsUnSelectAll;

    for jdx := 0 to Layer[idx].Objects.Count - 1 do
    begin
      //.. Unselect All Objects.Layers............
      Layer[idx].Objects[jdx].Layers.ObjectsUnSelectAll;
      //.. Unselect All Objects............
      if osSelected in Layer[idx].Objects.State[jdx] then
        Layer[idx].Objects[jdx].Selected := False;

    end;
  end;
end;

function TEarthLayerStore.ObjectAtLL(ptLL: TPointLL): TEarthObject;
var
  idx1: integer;
begin
  Result := nil;
  for idx1 := Count - 1 downto 0 do
  begin
    Result := Layer[idx1].ObjectAtLL(ptLL);
    if Result <> nil then
      Break;
  end;
end;

function TEarthLayerStore.ObjectAtXY(iX, iY: integer): TEarthObject;
var
  idx: integer;
begin
  Result := nil;
  for idx := Count - 1 downto 0 do
    with Layer[idx] do
      if Enabled and Visible then
      begin
        Result := ObjectAtXY(iX, iY);
        if Result <> nil then
          Break;
      end;
end;

function TEarthLayerStore.ObjectByID(iID: integer): TEarthObject;
var
  idx: integer;
begin
  for idx := 0 to Count - 1 do
  begin
    Result := Layer[idx].ObjectByID(iID);
    if Result <> nil then
      Exit;
  end;
  Result := nil;
end;

function TEarthLayerStore.ObjectByTitle(const sTitle: string; Obj: TEarthObject): TEarthObject;
var
  idx: integer;
begin
  for idx := 0 to Count - 1 do
  begin
    Result := Layer[idx].ObjectByTitle(sTitle, Obj);
    if Result <> nil then
      Exit;
  end;
  Result := nil;
end;

procedure TEarthLayerStore.Render(bAnimated: boolean);
var
  idL, idOb: integer;
begin
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate

  case Earth.TitlesRenderMethod of
    trtAfterAllLayers:
    begin
      for idL := 0 to Count - 1 do
        with Layer[idL] do
          if Animated = bAnimated then
            RenderObjects;
      //........................
      for idL := 0 to Count - 1 do
        with Layer[idL] do
          if Animated = bAnimated then
            RenderObjectsTitles;
    end;
    trtAfterLayer:
    begin
      for idL := 0 to Count - 1 do
        with Layer[idL] do
          if Animated = bAnimated then
          begin
            RenderObjects;
            RenderObjectsTitles;
          end;
    end;
    trtAfterObject:  // at this option the Render Layer direct and object titlies
    begin
      for idL := 0 to Count - 1 do
        with Layer[idL] do
          if Animated = bAnimated then
            RenderObjects;
    end;
  end;

  //... Render Layers.Layers ..............
  for idL := 0 to fLayers.Count - 1 do
  begin

    Layer[idL].Layers.Render(bAnimated);
    //Render Layers.Layers.Objects
    for idOb := 0 to Layer[idL].Objects.Count - 1 do
      if TEarthObject(Layer[idL].Objects[idOb]).Hidden = False then
        Layer[idL].Objects[idOb].Layers.Render(bAnimated);
  end;
end;

//===============================================================================================
//===================== TEarthPresenter =========================================================
//===============================================================================================

constructor TEarthPresenter.Create(aPresenterStore: TEarthPresenterStore; iID: integer);
begin
  inherited Create;
  FParent := aPresenterStore;
  FEarth := FParent.Earth;
  Assert(FEarth <> nil, 'TEarthPresenter.Create passed nil Earth');

  Name := ClassName;
  PresenterID := iID;
  if (FTitleFont = nil) then
  begin
    FTitleFont := TEarthFont.Create(self);
  end;
  FTitleAlignment := taCenter; //New Pressents show Objects title
  FTitleOffset := 0;

  FParent.Add(self);
end;

procedure TEarthPresenter.RedrawObject;
begin
  if FIsUpdating then
    exit;
  FEarth.Notify(gnPresenterPropertyChange, Self);
end;

procedure TEarthPresenter.SetHidden(const val: boolean);
begin
  FHidden := val;
  RedrawObject;
end;

procedure TEarthPresenter.SetTitleAlignment(const val: TTitleAlignment);
begin
  FTitleAlignment := val;
  RedrawObject;
end;

procedure TEarthPresenter.SetTitleFont(val: TEarthFont);
begin
  if val = nil then
    Exit;
  FTitleFont.Assign(val);
  RedrawObject;
end;

procedure TEarthPresenter.SetTitleOffset(const val: double);
begin
  FTitleOffset := val;
  RedrawObject;
end;

procedure TEarthPresenter.SetPresenterID(const val: integer);
begin
  FPresenterID := val;
  RedrawObject;
end;

procedure TEarthPresenter.SetName(const val: string);
begin
  fName := StringReplace(val, ' ', '', [rfReplaceAll]);
end;

destructor TEarthPresenter.Destroy;
begin
  FTitleFont.Free;
  FTitleFont := nil;
  inherited Destroy;
end;

procedure TEarthPresenter.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      Name := AttributeByName('Name', Name);
      PresenterID := IntAttributeByName('PresenterID', PresenterID);
      TitleAlignment := TTitleAlignment(IntAttributeByName('Alignment', Ord(TitleAlignment)));
      TitleOffset := FloatAttributeByName('Offset', TitleOffset);
      Hidden := BoolAttributeByName('Hidden', Hidden);
      TitleFont.LoadEnvironment(ElementByName('TitleFont', 0));
    end;
end;

function TEarthPresenter.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddAttribute('Class', ClassName);

  Result.AddAttribute('Name', Name);
  Result.AddIntAttribute('PresenterID', PresenterID);
  Result.AddIntAttribute('Alignment', Ord(TitleAlignment));
  Result.AddFloatAttribute('Offset', TitleOffset);
  Result.AddBoolAttribute('Hidden', Hidden);

  Result.AddElement('TitleFont', TitleFont.SaveEnvironment);
end;

procedure TEarthPresenter.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(2);   //Write Object Stream Version

  Writer.WriteInteger(PresenterID);
  Writer.WriteDouble(TitleOffset);
  Writer.WriteShortString(Name);
  Writer.WriteBuffer(FTitleAlignment, 1);
  TitleFont.WriteProperties(Writer);
end;

procedure TEarthPresenter.ReadProperties(Reader: TEarthStreamReader);
var
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;
  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version

  //.............. Old ...................................
  if iVer = 1 then
  begin
    PresenterID := Reader.ReadInteger;

    if giFileVersion >= TG_FILEVERSION400 then
      FTitleOffset := Reader.ReadInteger
    else
    if giFileVersion >= TG_FILEVERSION301 then
      FTitleOffset := Reader.ReadShortInt;

    Name := Reader.ReadShortString; // Presenter name

    Reader.ReadBuffer(FTitleAlignment, 1);
    FTitleFont.ReadProperties(Reader);
  end;
  //.......................................................
  if iVer > 1 then
  begin
    PresenterID := Reader.ReadInteger;
    FTitleOffset := Reader.ReadDouble;
    Name := Reader.ReadShortString;
    Reader.ReadBuffer(FTitleAlignment, 1);
    FTitleFont.ReadProperties(Reader);
  end;
end;

function TEarthPresenter.LLInObject(Earth: TCustomEarth; ptLL: TPointLL; Geod: TEarthObject; iTolerance: double): boolean;
begin
  Result := Geod.LLInObject(ptLL, iTolerance);
end;

procedure TEarthPresenter.Assign(Source: TPersistent);
begin
  if Source is TEarthPresenter then
    with TEarthPresenter(Source) do
    begin
      Self.PresenterID := PresenterID;
      Self.TitleAlignment := TitleAlignment;
      Self.TitleOffset := TitleOffset;
      Self.TitleFont.Assign(TitleFont);
      Self.Name := Name;
      Self.Hidden := Hidden;
    end
  else
    inherited Assign(Source);
end;

function TEarthPresenter.Clone(aPresenterStore: TEarthPresenterStore): TEarthPresenter;

begin
  Result := nil;
  if aPresenterStore = nil then
    exit;

  Result := TEarthPresenterClass(ClassType).Create(aPresenterStore, PresenterID);
  Result.Assign(Self);
end;

procedure TEarthPresenter.RenderObject(Earth: TCustomEarth; Geod: TEarthObject; State: TEarthObjectStateSet);
begin
  if Assigned(OnRenderObject) then
    OnRenderObject(Self, Earth, Geod, State);
end;

procedure TEarthPresenter.RenderTitle(Earth: TCustomEarth; Geod: TEarthObject; var bDone: boolean);
begin
  if Assigned(OnRenderTitle) then
    OnRenderTitle(Self, Earth, Geod, bDone);
end;

procedure TEarthPresenter.AdjustObjectMER(Geod: TEarthObject; var ObjectMER: TMER);
begin
  //...
end;


//===================== TCustomProjectionModel ==================================================

constructor TCustomProjectionModel.Create(Parent: TEarthProjection);
begin
  inherited Create;
end;

procedure TCustomProjectionModel.Assign(AModel: TPersistent);
begin
  if not (AModel is TCustomProjectionModel) then
    inherited Assign(AModel);
end;

function TCustomProjectionModel.Clone(aParent: TEarthProjection): TCustomProjectionModel;
begin
  Result := TCustomProjectionModelClass(Self.ClassType).Create(aParent);
  Result.Assign(Self);
end;

function TCustomProjectionModel.PointStoreToXY(Points: TPointStore; iStart: integer; bClosed: boolean): integer;
begin
  Result := -1;
end;

function TCustomProjectionModel.LLToXY(iLong, iLat: double; iIndex: integer): boolean;
begin
  Result := False;
end;

function TCustomProjectionModel.PointLLToXY(const ptLL: TPointLL; iIndex: integer): boolean;
begin
  Result := False;
end;

function TCustomProjectionModel.XYToLL(iX, iY: double; iIndex: integer): boolean;
begin
  Result := False;
end;

function TCustomProjectionModel.ExtentsLL: TGRectFloat;
begin
  Result := GRectFloat(0, 0, 0, 0);
end;

function TCustomProjectionModel.XYListVisible(iCount: integer; var ClipCode: byte): boolean;
begin
  Result := False;
end;

function TCustomProjectionModel.VisibleMER(const MER: TMER; var State: TEarthObjectStateSet): boolean;
begin
  Result := False;
end;

procedure TCustomProjectionModel.PropertyChanged(ProjectionProperty: TProjectionProperty);
begin
  //...
end;

procedure TCustomProjectionModel.PaintSurface;
begin
  //...
end;

//===================== TEarthPresenterStore ====================================================

constructor TEarthPresenterStore.Create(aParent: TPersistent);
begin
  if not (aParent is TEarthLayer) or not (aParent is TEarthObjectStore) or not (aParent is TEarthLayerStore) or (aParent = nil) then



    Assert(True, 'TEarthPresenterStore.Create passed NOT Correct Class as Parent or NIL');

  inherited Create;
  FParent := aParent;

  if fParent is TEarthLayer then
    FEarth := TEarthLayer(fParent).Earth;
  if fParent is TEarthObjectStore then
    FEarth := TEarthObjectStore(fParent).Earth;
  if fParent is TEarthLayerStore then
    FEarth := TEarthLayerStore(fParent).Earth;
  Assert(FEarth <> nil, 'TEarthPresenter.Create passed nil Earth');

  FPresenters := TObjectList.Create(false);
  FModified := False;
  FIsUpdating := False;
end;

destructor TEarthPresenterStore.Destroy;
begin
  Clear;
  FPresenters.Free;
  inherited Destroy;
end;

function TEarthPresenterStore.Add(aPresenter: TEarthPresenter): integer;
var
  idx: integer;
begin
  Result := 0;
  if aPresenter <> nil then
  begin
    Result := aPresenter.PresenterID;
    if Result = 0 then
    begin
      // Find the highest presenter ID
      for idx := 0 to FPresenters.Count - 1 do
        if Presenters[idx].PresenterID > Result then
          Result := Presenters[idx].PresenterID;

      Inc(Result);
      aPresenter.PresenterID := Result;

      FPresenters.Add(aPresenter);
      Modified := True;
    end
    else
    if ByID(Result, False) = nil then // add to list if Presenter not already in list
    begin
      FPresenters.Add(aPresenter);
      Modified := True;
    end;
    aPresenter.fparent := self;
    RedrawObject;
  end;
end;

function TEarthPresenterStore.ByID(iPresenterID: integer; bGlobal: boolean): TEarthPresenter;
var
  idx: integer;
begin
  // Search for the requested Presenter on this Layer
  for idx := 0 to Count - 1 do
  begin
    Result := Presenters[idx];
    if Result <> nil then
      if iPresenterID = Result.PresenterID then
        Exit;
  end;

  // Search for the requested Presenter in the GlobalPresenter List
  if bGlobal then
    Result := Earth.Layers.GlobalPresenters.ByID(iPresenterID, False)
  else
    Result := nil;
end;

function TEarthPresenterStore.ByName(const aName: string; bGlobal: boolean): TEarthPresenter;
var
  idx: integer;
begin
  // Search for the requested Presenter on this Layer
  for idx := 0 to Count - 1 do
  begin
    Result := Presenters[idx];
    if Result <> nil then
      if sametext(aName, Result.Name) then
        Exit;
  end;

  { Search for the requested Presenter in the GlobalPresenter List }
  if bGlobal then
    Result := Earth.Layers.GlobalPresenters.ByName(aName, False)
  else
    Result := nil;
end;

procedure TEarthPresenterStore.Clear;
var
  idx: integer;
begin

  if Count > 0 then
    for idx := Count - 1 downto 1 do     // 7777 ??? must downto 0
      Presenters[idx].Free;

  FPresenters.Clear;

  Modified := False;
  RedrawObject;
end;

procedure TEarthPresenterStore.RedrawObject;
begin
  FModified := True;
  if Earth = nil then
    exit;
  if FIsUpdating then
    exit;
  Earth.Notify(gnPresenterStoreChange, Self);
end;

procedure TEarthPresenterStore.Delete(aPresenter: TEarthPresenter);
var
  idx: integer;
begin
  if aPresenter = nil then
    exit;

  for idx := 0 to Count - 1 do
    if Presenters[idx] = aPresenter then
    begin
      FPresenters.Delete(idx);
      Modified := True;
      Break;
    end;
  RedrawObject;
end;

procedure TEarthPresenterStore.Move(iFrom, iTo: integer);
var
  idx: integer;
begin
  if (iTo >= 0) and (iTo < Count) then
  begin
    FPresenters.Move(iFrom, iTo);
    // Update the Presenters index values
    for idx := 0 to Count - 1 do
      Presenters[idx].FPresenterID := idx + 1;

    FModified := True;
    RedrawObject;
  end;
end;

procedure TEarthPresenterStore.Assign(Source: TPersistent);
var
  idx: integer;
begin
  if Source is TEarthPresenterStore then
  begin
    self.BeginUpdate;
    self.Clear;
    for idx := 0 to TEarthPresenterStore(Source).Count - 1 do
      TEarthPresenterStore(Source)[idx].Clone(self);
    self.EndUpdate;

  end
  else
  if Source is TEarthPresenter then
    TEarthPresenter(Source).Clone(self);
end;

procedure TEarthPresenterStore.WriteProperties(Writer: TEarthStreamWriter);
var
  idx: integer;
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(1);   //Write Object Stream Version

  Writer.WriteInteger(Count);
  for idx := 0 to Count - 1 do
  begin
    Writer.WriteShortString(Presenters[idx].ClassName);
    Presenters[idx].WriteProperties(Writer);
  end;
end;

procedure TEarthPresenterStore.ReadProperties(Reader: TEarthStreamReader);
var
  idx, iCount: integer;
  oNew: TEarthPresenter;
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;

  BeginUpdate;
  Clear;

  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version

  iCount := Reader.ReadInteger;
  for idx := 0 to iCount - 1 do
  begin
    oNew := TEarthPresenterClass(FindClass(Reader.ReadShortString)).Create(self, 0);
    oNew.ReadProperties(Reader);
  end;

  endUpdate;
end;

function TEarthPresenterStore.GetCount: integer;
begin
  Result := FPresenters.Count;
end;

function TEarthPresenterStore.GetPresenter(iIndex: integer): TEarthPresenter;
begin
  Result := TEarthPresenter(FPresenters.Items[iIndex]);
end;

procedure TEarthPresenterStore.LoadEnvironment(Element: TGXML_Element);
var
  idx: integer;

  function CreatePresenter(const Ident: string; iOccourance: integer): TEarthPresenter;
  var
    sClassName: string;
    PresenterElement: TGXML_Element;
  begin
    Result := nil;
    PresenterElement := Element.ElementByName(Ident, iOccourance);

    if PresenterElement <> nil then
    begin
      // Get the ClassName of the Layer
      sClassName := PresenterElement.AttributeByName('Class', '');
      // Create the Object
      Result := TEarthPresenterClass(FindClass(sClassName)).Create(self, 0);
      // Load the properties
      Result.LoadEnvironment(PresenterElement);
    end;
  end;

begin
  Clear; // Clear the Presenter Store First;
  if Element <> nil then
    with Element do
    begin
      // Load in all the Presenter
      idx := 0;
      while CreatePresenter('Presenter', idx) <> nil do
        Inc(idx);
    end;
end;

function TEarthPresenterStore.SaveEnvironment: TGXML_Element;
var
  idx: integer;
begin
  Result := TGXML_Element.Create;

  for idx := 0 to Count - 1 do
    Result.AddElement('Presenter', Presenters[idx].SaveEnvironment);
end;

//===============================================================================================
//===================== TEarthLayer ============================================================
//===============================================================================================

constructor TEarthLayer.Create(aLayerStore: TEarthLayerStore);
begin
  Assert(aLayerStore <> nil, 'TEarthLayer.Create passed nil TEarthLayerStore Parent');
  inherited Create;
  FParent := aLayerStore;
  FEarth := FParent.Earth;
  fLayers := TEarthLayerStore.Create(Self);
  FPresenters := TEarthPresenterStore.Create(self);
  Clear;
  FParent.Add(self);
  Earth.Notify(gnLayerCreate, Self);
  FModified := False;
end;

destructor TEarthLayer.Destroy;
begin
  FParent.Remove(self);
  fLayers.Free;
  Objects := nil;
  Presenters.Free;
  FPresenters := nil;
  inherited Destroy;
end;

procedure TEarthLayer.SetParent(aLayerStore: TEarthLayerStore);
var
  ap: TEarthLayerStore;
begin
  if aLayerStore = nil then
    exit;
  if fParent = aLayerStore then
    exit;
  ap := fParent;
  ap.Remove(self);
  fParent := aLayerStore;
  FEarth := FParent.Earth;
  fParent.Add(self);
  Earth.Notify(gnLayerPropertyChanged, self);
end;

procedure TEarthLayer.SetPresenterToAllObjects(index: integer);
var
  i: integer;
begin
  if Objects <> nil then
    if Objects <> nil then
      for i := 0 to Objects.Count - 1 do
      begin
        if Objects[i].PresenterID <> index then;
        Objects[i].PresenterID := index;
      end;
end;

procedure TEarthLayer.Delete;
begin
  Free;
end;

function TEarthLayer.CanMoveMinus: boolean;
begin
  Result := False;
  if (index > 0) and (FParent.Count > 1) then
    Result := True;
end;

function TEarthLayer.CanMovePlus: boolean;
begin
  Result := False;
  if (index < FParent.Count - 1) and (FParent.Count > 1) then
    Result := True;
end;

procedure TEarthLayer.SetObjects(Value: TEarthObjectStore);
begin
  if FObjects <> nil then
    FObjects.UnSubscribe(Self);

  FObjects := Value;

  if Value <> nil then
    Value.Subscribe(Self);

  Earth.RedrawLayers;
end;

function TEarthLayer.GetObjects: TEarthObjectStore;
begin
  if FObjects = nil then
    Objects := TEarthObjectStore.Create(self);
  Result := FObjects;
end;

procedure TEarthLayer.Clear;
begin
  Exclude(LayerState, lsValidMER);
  fFullFileName := '';
  fFilePath := '';
  fFileName := '';
  FbEnabled := True;
  FbVisible := True;
  FbAnimated := False;
  FbShowTitles := True;
  FModified := False;

  FiVisibleHeightMaxEU := -9999;
  FiVisibleHeightMinEU := -9999;
  FVisibleHeightMin := 0;
  FVisibleHeightMax := 0;
  FVisibleHeightUnits := huEarthUnit;

  FLayerColorAdjustPercent := 0;
  FLayerColorAdjustTarget := clGray;
  FLayers.Clear;
  Objects := nil;
  Presenters.Clear;
  Earth.RedrawLayers;
end;

procedure TEarthLayer.Assign(Source: TPersistent);
begin
  if Source is TEarthLayer then
  begin
    Self.BeginUpdate;
    Self.Clear;
    //.......
    Self.Objects.Assign(TEarthLayer(Source).Objects);
    Self.Presenters.Assign(TEarthLayer(Source).Presenters);
    Self.Layers.Assign(TEarthLayer(Source).Layers);
    //........
    Self.FbAnimated := TEarthLayer(Source).Animated;
    Self.FbEnabled := TEarthLayer(Source).Enabled;
    Self.FbShowTitles := TEarthLayer(Source).ShowTitles;
    Self.Name := TEarthLayer(Source).Name;
    Self.NoInterrupt := TEarthLayer(Source).NoInterrupt;
    //   Self.fLayerColorAdjustPercent:=TEarthLayer(Source).LayerColorAdjustPercent;
    //  Self.fLayerColorAdjustTarget:=TEarthLayer(Source).LayerColorAdjustTarget;
    Self.FbVisible := TEarthLayer(Source).Visible;
    Self.fVisibleHeightMin := TEarthLayer(Source).VisibleHeightMin;
    Self.fVisibleHeightMax := TEarthLayer(Source).VisibleHeightMax;
    Self.fVisibleHeightUnits := TEarthLayer(Source).VisibleHeightUnits;
    Self.Tag := TEarthLayer(Source).Tag;
    Self.TagString := TEarthLayer(Source).TagString;
    Self.EndUpdate;
  end
  else
    inherited Assign(Source);
end;

procedure TEarthLayer.WriteProperties(Writer: TEarthStreamWriter);
begin

  if Writer = nil then
    exit;
  Writer.WriteInteger(3);   //Write Object Stream Version

  Presenters.WriteProperties(Writer);
  Objects.WriteProperties(Writer);

  Writer.WriteBoolean(FbAnimated);
  Writer.WriteBoolean(FbEnabled);
  Writer.WriteBoolean(FbNoInterrupt);
  Writer.WriteBoolean(FbShowTitles);

  Writer.WriteDouble(VisibleHeightMax);
  Writer.WriteDouble(VisibleHeightMin);
  Writer.WriteBuffer(VisibleHeightUnits, SizeOf(THeightUnitTypes));

  Writer.WriteInteger(FLayerColorAdjustPercent);
  Writer.WriteColor(fLayerColorAdjustTarget);
  writer.WriteShortString(Name);

  //.... Write Owned Layers ......
  FLayers.WriteProperties(Writer);

end;

procedure TEarthLayer.ReadProperties(Reader: TEarthStreamReader);
var
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;

  BeginUpdate;
  Clear;

  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger; //Read Object Stream Version

  Presenters.ReadProperties(Reader);
  Objects.ReadProperties(Reader);

  FbAnimated := Reader.ReadBoolean;
  FbEnabled := Reader.ReadBoolean;
  FbNoInterrupt := Reader.ReadBoolean;
  FbShowTitles := Reader.ReadBoolean;

  if iver < 2 then
  begin
    VisibleHeightMax := Reader.ReadInteger;
    VisibleHeightMin := Reader.ReadInteger;
  end
  else
  begin
    fVisibleHeightMax := Reader.ReadDouble;
    fVisibleHeightMin := Reader.ReadDouble;
    Reader.ReadBuffer(fVisibleHeightUnits, SizeOf(THeightUnitTypes));
  end;

  FLayerColorAdjustPercent := Reader.ReadInteger;
  fLayerColorAdjustTarget := Reader.ReadColor;
  Name := reader.ReadShortString;

  //.... Read Owned Layers ...... 12-4-2006................
  if iver >= 3 then
    FLayers.ReadProperties(Reader);

  EndUpdate;
end;

function TEarthLayer.Clone(aLayerStore: TEarthLayerStore): TEarthLayer;
begin
  Result := nil;
  if aLayerStore = nil then
    exit;

  Result := TEarthLayerClass(ClassType).Create(aLayerStore);
  Result.Assign(Self);
end;

procedure TEarthLayer.Notify(Notification: TObjectSourceNotification; Obj: TEarthObject);
begin
  case Notification of
    osnOpen:
      Visible := True;
    osnClose:
      Visible := False;
    osnAdd:
    begin
      Exclude(LayerState, lsValidMER);
      Earth.RedrawLayers;
    end;
    osnDelete:
      Earth.RedrawLayers;
    osnRedraw:
      // If is a animation Layer, don't redraw all layers...
      if FbAnimated then
        Earth.Invalidate
      else
        Earth.RedrawLayers;
    osnSelectedChanged:
      Earth.Notify(gnObjectSelectChange, Obj);
  end;
end;

function TEarthLayer.GetIndex: integer;
begin
  Result := Parent.IndexOf(Self);
end;

procedure TEarthLayer.SetIndex(iIndex: integer);
begin
  Parent.Move(Index, iIndex);
end;

function TEarthLayer.GetEarthCanvas: TEarthCanvas;
begin
  Result := Earth.EarthCanvas;
end;

function TEarthLayer.GetProjection: TEarthProjection;
begin
  Result := Earth.Projection;
end;

function TEarthLayer.GetLayerName: string;
begin
  Result := Trim(FsLayerName);

  if Result = '' then
    if FObjects <> nil then
      Result := Objects.Name;

  if Result = '' then
    Result := ClassName;
end;

procedure TEarthLayer.SetLayerName(const Value: string);
begin
  if Name = Trim(Value) then
    exit;

  FsLayerName := Trim(Value);
  FsLayerName := ChangeFileExt(FsLayerName, '');

  if FsLayerName = '' then
    if Parent <> nil then
      FsLayerName := 'NewLayer' + IntToStr(Parent.Count);

  Modified := True;
end;

procedure TEarthLayer.SetFullFileName(const Value: string);
begin
  if SameText(fFullFileName, Value) then
    exit;

  fFullFileName := Value;
  FFilePath := ExtractFilePath(fFullFileName);
  fFileName := ExtractFileName(fFullFileName);
  fFileName := ChangeFileExt(fFileName, '');

  Objects.Name := fFileName;
  if Name = '' then
    Name := fFileName;
  if Objects.Name = '' then
    Objects.Name := Name;

end;

function TEarthLayer.SelectedCount: integer;
var
  idx: integer;
begin
  Result := 0;
  for idx := 0 to Objects.Count - 1 do
    if osSelected in Objects.State[idx] then
      Inc(Result);
end;

function TEarthLayer.GetLayerMER: TMER;
begin
  if not (lsValidMER in LayerState) or (FLayerMER.iWidthX = 0) then
    FLayerMER := Objects.ObjectsMER;

  Result := FLayerMER;
end;

function TEarthLayer.IsLayerMERVisible: boolean;
begin
  Result := Visible;

  with Earth, Projection do
  begin
    if fiVisibleHeightMinEU < 0 then
      fiVisibleHeightMinEU := GetVisibleHeightMinAsEU;

    if Result and (fiVisibleHeightMinEU > 0) then
      Result := Altitude > fiVisibleHeightMinEU;

    if fiVisibleHeightMaxEU < 0 then
      fiVisibleHeightMaxEU := GetVisibleHeightMaxAsEU;
    if Result and (fiVisibleHeightMaxEU > 0) then
      Result := Altitude <= fiVisibleHeightMaxEU;
  end;
end;

function TEarthLayer.FindPresenter(iPresenterID: integer): TEarthPresenter;
begin
  Result := nil;

  // search for local Layer Presenter Store then the Global Presenter Store
  if fPresenters <> nil then
    Result := fPresenters.ByID(iPresenterID, True);

  // finally check the ObjectStore for the Presenter
  if Result = nil then
    if Objects.Presenters <> nil then
      Result := Objects.Presenters.ByID(iPresenterID, False);
end;

function TEarthLayer.GetVisibleHeightMinAsEU: double;
begin
  Result := HeightUnitsFrom(fVisibleHeightMin, fVisibleHeightUnits);
end;

function TEarthLayer.GetVisibleHeightMaxAsEU: double;
begin
  Result := HeightUnitsFrom(fVisibleHeightMax, fVisibleHeightUnits);
end;

procedure TEarthLayer.SetVisibleHeightMin(const Val: single);
begin
  if fVisibleHeightMin = Val then
    exit;
  if val < 0 then
    exit;

  fVisibleHeightMin := Val;
  fiVisibleHeightMinEU := GetVisibleHeightMinAsEU;
  Modified := True;
  Earth.Notify(gnLayerPropertyChanged, self);
end;

procedure TEarthLayer.SetVisibleHeightMax(const Val: single);
begin
  if fVisibleHeightMax = Val then
    exit;
  if val < 0 then
    exit;

  fVisibleHeightMax := Val;
  fiVisibleHeightMaxEU := GetVisibleHeightMaxAsEU;
  Modified := True;
  Earth.Notify(gnLayerPropertyChanged, self);
end;

procedure TEarthLayer.SetVisibleHeightUnits(const Val: THeightUnitTypes);
begin
  if fVisibleHeightUnits = Val then
    exit;

  fVisibleHeightUnits := Val;
  fiVisibleHeightMinEU := GetVisibleHeightMinAsEU;
  fiVisibleHeightMaxEU := GetVisibleHeightMaxAsEU;
  Modified := True;
  Earth.Notify(gnLayerPropertyChanged, self);
end;

procedure TEarthLayer.SetLayerValue(iIndex: integer; Value: integer);
begin
  case iIndex of
    0:
      if FLayerColorAdjustPercent <> Value then
      begin
        FLayerColorAdjustPercent := Value;
        Modified := True;
        Earth.Notify(gnLayerPropertyChanged, self);
      end;
  end;
end;

procedure TEarthLayer.SetLayerBoolean(iIndex: integer; bValue: boolean);
begin
  case iIndex of
    0:
      if FbEnabled <> bValue then
        FbEnabled := bValue
      else
        Exit;
    1:
      if FbVisible <> bValue then
        FbVisible := bValue
      else
        Exit;
    2:
      if FbAnimated <> bValue then
        FbAnimated := bValue
      else
        Exit;
    3:
      if FbShowTitles <> bValue then
        FbShowTitles := bValue
      else
        Exit;
    4:
      if FbNoInterrupt <> bValue then
        FbNoInterrupt := bValue
      else
        Exit;
  end;
  Modified := True;
  Earth.RedrawLayers;
end;

 {
procedure TEarthLayer.SetLayerColorAdjustTarget(const Value: TColor);
begin
  if LayerColorAdjustTarget <> Value then
  begin
    FLayerColorAdjustTarget := Value;
    Modified := True;
    fEarth.Notify(gnLayerPropertyChanged,self);
  end;
end;
    }
function TEarthLayer.ObjectAtXY(iX, iY: integer): TEarthObject;
var
  ptLL: TPointLL;
begin
  if fEarth.MouseXYToLL(iX, iY, ptLL) then
    Result := ObjectAtLL(ptLL)
  else
    Result := nil;
end;

function TEarthLayer.ObjectAtLL(ptLL: TPointLL): TEarthObject;
var
  idx: integer;
  iTolerance: double;
  aMER: TMER;
  aPresenter: TEarthPresenter;
begin

  //... Search to Owned Layers ..........
  Result := FLayers.ObjectAtLL(ptLL);
  if Result <> nil then
    exit;

  //.... Search to Owned Object .........
  if IsLayerMERVisible then
  begin
    iTolerance := (2 / Projection.ScaleFactor);

    for idx := Objects.Count - 1 downto 0 do
    begin
      //..... Search to Object.Layers ...........
      Result := Objects[idx].Layers.ObjectAtLL(ptLL);
      if Result <> nil then
        exit;
      //.........................................

      aMER := Objects.MER[idx];
      if ((aMER.iWidthX = 0) and (aMER.iHeightY = 0)) or PointLLinMER(ptLL, aMER) then
      begin
        Result := Objects[idx];
        aPresenter := FindPresenter(Result.PresenterID);

        if aPresenter <> nil then
        begin
          if aPresenter.LLInObject(Earth, ptLL, Result, iTolerance) then
            Exit;
        end
        else
        if Result.LLInObject(ptLL, iTolerance) then
          Exit;
      end;
    end;
  end;

  Result := nil;
end;

function TEarthLayer.ObjectByTitle(const sTitle: string; Obj: TEarthObject): TEarthObject;
var
  idx: integer;
begin

  //.... Search to Owned Object .........
  idx := 0;
  if Obj <> nil then
  begin
    while idx < Objects.Count do
    begin
      if Objects[idx] = Obj then
        Break;
      Inc(idx);
    end;
    Inc(idx); // start from the next object
  end;

  // search for a matching title
  while idx < Objects.Count do
  begin
    Result := Objects[idx];
    if CompareText(Result.Title, sTitle) = 0 then
      Exit;
    Inc(idx);
  end;

  //... Search to Owned Layers ..........
  Result := FLayers.ObjectByTitle(sTitle, Obj);
  if Result <> nil then
    exit;

  Result := nil;
end;

function TEarthLayer.ObjectByID(iID: integer): TEarthObject;
var
  idx: integer;
begin
  //.... Search to Owned Object .........
  for idx := 0 to Objects.Count - 1 do
  begin
    Result := Objects[idx];
    if Result.ID = iID then
      Exit;
    //..... Search to Object.Layers ...........
    Result := Objects[idx].Layers.ObjectByID(iID);
    if Result <> nil then
      exit;
    //.........................................
  end;

  //... Search to Owned Layers ..........
  Result := FLayers.ObjectByID(iID);
  if Result <> nil then
    exit;

  Result := nil;
end;

class function TEarthLayer.PrintableClassName: string;
begin
  Result := rsTEarthLayerClassName;
end;

function TEarthLayer.UpdateObjectState(idx: integer): TEarthObjectStateSet;
var
  iPPP: extended;
begin
  // Calculate visiblilty of object
  with Projection do
  begin
    Result := Objects.State[idx];
    if osNew in Result then
      Result := Objects[idx].ObjectState; // Read the object state

    if not (osHidden in Result) and ProjectionModel.VisibleMER(Objects.MER[idx], Result) then
    begin

      Include(Result, osVisible);
      iPPP := (2 / ScaleFactor);

      with Objects.MER[idx] do
        if (iWidthX > 0) and (iHeightY > 0) then
        begin
          if ((iWidthX / iPPP) <= 1.0) and ((iHeightY / iPPP) <= 1.0) then
            Include(Result, osTiny)
          else
            Exclude(Result, osTiny);
        end;
    end
    else
      Exclude(Result, osVisible);

    // Objects.State[idx] := Result;
  end;
end;

procedure TEarthLayer.RenderTitle(Obj: TEarthObject; aPresenter: TEarthPresenter);
var
  ptXY: TPointDouble;
  iAlignment: integer;
  iOffset: double;
  bDone: boolean;
begin
  if aPresenter.TitleAlignment = taNone then
    Exit;
  if Obj.Title = '' then
    Exit;

  bDone := False;
  aPresenter.RenderTitle(Earth, Obj, bDone); // Give the Presenter a chance to render the title

  if bDone then
    Exit;

  with aPresenter.TitleFont do
  begin

    if Earth.EarthCanvas.FontRenderHeight(FontSize, FontSizeUnits) < Max(2, Earth.MinTextHeight) then
      Exit;

    if aPresenter.TitleOffset = 0 then
      iOffset := 0
    else
      iOffset := Earth.EarthCanvas.FontRenderHeight(aPresenter.TitleOffset, FontSizeUnits);
  end;

  aPresenter.TitleFont.RenderAttribute(Earth.EarthCanvas, osSelected in Obj.ObjectState);

  { Get the X and Y of the Centroid of the object }
  with Obj.Centroid do
    if Projection.LLToDeviceXY(iLongX, iLatY, ptXY) then
    begin
      case aPresenter.TitleAlignment of
        taLeft: iAlignment := TA_BASELINE + TA_Left;
        taBottomLeft: iAlignment := TA_Bottom + TA_Left;
        taCenter: iAlignment := TA_BASELINE + TA_CENTER;
        taBottom: iAlignment := TA_Bottom + TA_CENTER;
        taRight: iAlignment := TA_BASELINE + TA_Right;
        taBottomRight: iAlignment := TA_Bottom + TA_Right;
        taTopLeft: iAlignment := TA_BOTTOM + TA_Left;
        taTop: iAlignment := TA_Top + TA_CENTER;
        taTopRight: iAlignment := TA_Top + TA_Right;
        else
          iAlignment := 0;
      end;

      if iOffset <> 0 then
      begin
        case aPresenter.TitleAlignment of
          taLeft, taBottomLeft, taTopLeft: ptXY.X := ptXY.X - iOffset;
          taRight, taBottomRight, taTopRight: ptXY.X := ptXY.X + iOffset;
        end;

        case aPresenter.TitleAlignment of
          taTop, taTopLeft, taTopRight: ptXY.Y := ptXY.Y - iOffset;
          taBottom, taBottomLeft, taBottomRight: ptXY.Y := ptXY.Y + iOffset;
        end;
      end;

      case aPresenter.TitleAlignment of
        taRight: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignLeft, AGX_AlignCenter);
        taTopRight: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignLeft, AGX_AlignBottom);
        taCenter: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignCenter, AGX_AlignCenter);
        taTop: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignCenter, AGX_AlignBottom);
        taLeft: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignRight, AGX_AlignCenter);
        taTopLeft: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignRight, AGX_AlignBottom);
        taBottomRight: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignLeft, AGX_AlignTop);
        taBottom: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignCenter, AGX_AlignTop);
        taBottomLeft: Earth.EarthCanvas.TextAlignmentSet(AGX_AlignRight, AGX_AlignTop);
      end;

      Earth.EarthCanvas.gDrawText(ptXY.X, ptXY.Y, Obj.Title);
      //....... Fill Patttern ...............................
      if aPresenter.TitleFont.DrawFillWithImage then
        Earth.EarthCanvas.PathFillWithImage32(aPresenter.TitleFont.FontFillImage);
    end;
end;

procedure TEarthLayer.RenderObjects;
var
  idx, IdL: integer;
  ObjState: TEarthObjectStateSet;
  StateFlag: TEarthObjectState;
  bValidLayerMER: boolean;
  aPresenter: TEarthPresenter;
begin
  if (Earth = nil) or ((not NoInterrupt) and Earth.PaintAbort) then
    Exit;
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate

  if not IsLayerMERVisible then
    Exit;

  bValidLayerMER := True;

  if Projection.RenderBackFace then
    StateFlag := osBackFace
  else
    StateFlag := osFace;

  for idx := 0 to Objects.Count - 1 do
  begin
    ObjState := UpdateObjectState(idx);
    if not (osValidMER in ObjState) then
      bValidLayerMER := False;

    if (osVisible in ObjState) and (StateFlag in ObjState) then //Start check 1
    begin
      if osTiny in ObjState then
      begin

        Objects[idx].UnRender;

      end
      else
        with Objects[idx] do
        begin

          if IsObjectMERVisible(Projection) then
          begin

            aPresenter := nil;
            if Objects[idx].PresenterID > 0 then
              aPresenter := FindPresenter(Objects[idx].PresenterID);

            if aPresenter <> nil then
            begin
              if aPresenter.Hidden then
                Continue;// If Hidden flag set then don't display the object
              aPresenter.RenderObject(Earth, Objects[idx], ObjState); //finally render the object from aPresenter
            end
            else
            begin
              Render(Earth, ObjState); //finally render the object with out aPresenter
              if Assigned(TEarthBase(Earth).OnObjectRender) then
                TEarthBase(Earth).OnObjectRender(TEarthBase(Earth), Objects[idx], ObjState);
            end;
            //...........................................................
            // draw titles only if Fearth.TitlesRenderMethod=trtAfterObject
            if (aPresenter <> nil) and ShowTitles then
              if Earth.TitlesRenderMethod = trtAfterObject then
                RenderTitle(Objects[idx], aPresenter);
          end;
        end;
    end
    else  //End check 1
    begin
      Objects[idx].UnRender;
    end;

  end; // Loop inx ENds

  if not bValidLayerMER then
    GetLayerMER;
end;

procedure TEarthLayer.RenderObjectsTitles;
var
  idx, IdL: integer;
  ObjState: TEarthObjectStateSet;
  StateFlag: TEarthObjectState;
  bValidLayerMER: boolean;
  aPresenter: TEarthPresenter;
begin
  if (Earth = nil) or ((not NoInterrupt) and Earth.PaintAbort) then
    Exit;
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate
  if not IsLayerMERVisible then
    Exit;

  bValidLayerMER := True;

  if Projection.RenderBackFace then
    StateFlag := osBackFace
  else
    StateFlag := osFace;

  for idx := 0 to Objects.Count - 1 do
  begin
    ObjState := UpdateObjectState(idx);
    if not (osValidMER in ObjState) then
      bValidLayerMER := False;

    if (osVisible in ObjState) and not (osTiny in ObjState) then
      if Objects[idx].IsObjectMERVisible(Projection) then
      begin
        aPresenter := nil;
        aPresenter := FindPresenter(Objects[idx].PresenterID);

        if aPresenter <> nil then
          if aPresenter.Hidden = False then
            if ShowTitles then
              RenderTitle(Objects[idx], aPresenter);
      end;

  end; // Loop inx ENds

  if not bValidLayerMER then
    GetLayerMER;
end;

function TEarthLayer.SaveEnvironment: TGXML_Element;
begin
  FModified := False;

  Result := TGXML_Element.Create;
  Result.AddAttribute('Class', ClassName);
  Result.AddAttribute('Name', Name);
  Result.AddBoolAttribute('Animated', Animated);
  Result.AddBoolAttribute('Enabled', Enabled);
  Result.AddBoolAttribute('ShowTitles', ShowTitles);
  Result.AddBoolAttribute('Visible', Visible);
  Result.AddFloatAttribute('VisibleHeightMax', FVisibleHeightMax);
  Result.AddFloatAttribute('VisibleHeightMin', FVisibleHeightMin);
  Result.AddIntAttribute('VisibleHeightUnits', Ord(fVisibleHeightUnits));

  Result.AddElement('Presenters', Presenters.SaveEnvironment);

  if FObjects <> nil then
    Result.AddElement('Objects', Objects.SaveEnvironment);
end;

procedure TEarthLayer.LoadEnvironment(Element: TGXML_Element);
var
  sClassName: string;
begin
  Clear; // Clear the Layer first;
  if Element <> nil then
    with Element do
    begin
      Name := AttributeByName('Name', Name);
      Animated := BoolAttributeByName('Animated', Animated);
      Enabled := BoolAttributeByName('Enabled', Enabled);
      ShowTitles := BoolAttributeByName('ShowTitles', ShowTitles);
      Visible := BoolAttributeByName('Visible', Visible);
      VisibleHeightMax := Element.FloatAttributeByName('VisibleHeightMax', FVisibleHeightMax);
      VisibleHeightMin := Element.FloatAttributeByName('VisibleHeightMin', FVisibleHeightMin);
      VisibleHeightUnits := THeightUnitTypes(IntAttributeByName('VisibleHeightUnits', Ord(fVisibleHeightUnits)));

      // Load the Layer Presenters object
      Presenters.LoadEnvironment(ElementByName('Presenters', 0));

      // Load the Data Reader object
      Element := Element.ElementByName('Objects', 0);
      if Element <> nil then
      begin
        // Get the ClassName of the Object Source
        sClassName := Element.AttributeByName('Class', '');

        // Create the Object Source
        Self.Objects := TEarthObjectStoreClass(FindClass(sClassName)).Create(self);
        Self.Objects.LoadEnvironment(Element); // Load the properties
      end;
      Self.Modified := False;
    end;
end;

//===============================================================================================
//===================== TEarthObject ============================================================
//===============================================================================================
constructor TEarthObject.Create(aParent: TEarthObjectStore);
begin
  Assert(aParent <> nil, 'TEarthObject.Create passed nil Parent');

  inherited Create;
  FParent := aParent;
  FEarth := FParent.Earth;
  fLayers := TEarthLayerStore.Create(Self);

  FiVisibleHeightMaxEU := -9999;
  FiVisibleHeightMinEU := -9999;
  FVisibleHeightMin := 0;
  FVisibleHeightMax := 0;
  FVisibleHeightUnits := huEarthUnit;
  FiIndex := -1;
  FCentroid := PointLLH(0, 0, -GMaxDouble);
  FPresenterID := 0;

  fparent.Add(self);
end;

destructor TEarthObject.Destroy;
begin
  fLayers.Free;
  if fParent <> nil then
    fParent.Delete(Self);
  if osSelected in ObjectState then
    Selected := False;
  inherited Destroy;
end;

procedure TEarthObject.Clear;
begin
  FiVisibleHeightMaxEU := -9999;
  FiVisibleHeightMinEU := -9999;
  fLayers.Clear;
end;

procedure TEarthObject.MoveDelta(DLongX, DLatY: double);
begin
  Centroid := PointLL(Centroid.iLongX + DLongX, Centroid.iLatY + DLatY);
end;

procedure TEarthObject.MoveTo(ToPoint: TPointLL);
begin
  Centroid := ToPoint;
end;

function TEarthObject.CanMoveMinus: boolean;
begin
  Result := False;
  if (index > 0) and (FParent.Count > 1) then
    Result := True;
end;

function TEarthObject.CanMovePlus: boolean;
begin
  Result := False;
  if (index < FParent.Count - 1) and (FParent.Count > 1) then
    Result := True;
end;

function TEarthObject.ObjectInstanceSize: integer;
begin
  Result := inherited ObjectInstanceSize + Length(Title);
end;

function TEarthObject.GetObjectMER: TMER;
begin
  with Centroid do
    Result := MER(iLongX, iLatY, 0, 0);

  Include(ObjectState, osValidMER); { Flag the object as valid }
end;

function TEarthObject.GetCentroid: TPointLL;
begin
  Result := FCentroid;
end;

procedure TEarthObject.SetCenter(const X, Y: double);
var
  ptLL: TPointLL;
begin
  ptLL.iLongX := X;
  ptLL.iLatY := Y;
  Centroid := ptLL;
end;

procedure TEarthObject.SetCenterStr(const X, Y: string);
var
  ptLL: TPointLL;
  ss: string;
begin
  ss := '%d:%m:%s,%t %E';
  ptLL.iLongX := StrToEarthUnits(x, ss);
  ptLL.iLatY := StrToEarthUnits(y, ss);
  Centroid := ptLL;
end;

procedure TEarthObject.SetCenterStrFmt(const X, Y, Fmt: string);
var
  ptLL: TPointLL;
begin
  ptLL.iLongX := StrToEarthUnits(x, Fmt);
  ptLL.iLatY := StrToEarthUnits(y, Fmt);
  Centroid := ptLL;
end;

procedure TEarthObject.GetCenterStrFmt(var X, Y: string; const Fmt: string);
begin
  X := EarthUnitsToStr(Centroid.iLongX, Fmt);
  Y := EarthUnitsToStr(Centroid.iLatY, Fmt);
end;

procedure TEarthObject.SetCentroidX(const X: double);
var
  ptLL: TPointLL;
begin
  ptLL.iLongX := X;
  ptLL.iLatY := FCentroid.iLatY;
  Centroid := ptLL;
end;

procedure TEarthObject.SetCentroidY(const Y: double);
var
  ptLL: TPointLL;
begin
  ptLL.iLongX := FCentroid.iLongX;
  ptLL.iLatY := Y;
  Centroid := ptLL;
end;

function TEarthObject.GetCentroidX: double;
begin
  Result := FCentroid.iLongX;
end;

function TEarthObject.GetCentroidY: double;
begin
  Result := FCentroid.iLatY;
end;

procedure TEarthObject.SetCentroid(const ptLL: TPointLL);
begin

  if not SamePixelPos(Parent.Earth.Projection, ptLL, FCentroid, 1) then
  begin
    FCentroid := ptLL;
    Exclude(ObjectState, osValidMER);
    RedrawObject;
  end
  else
    FCentroid := ptLL;  // just update the Centroid without a redraw
end;

procedure TEarthObject.SetClosed(aValue: boolean);
begin
  if (osClosed in ObjectState) = aValue then
    Exit;

  if aValue then
  begin
    Include(ObjectState, osClosed);
    if (Parent <> nil) and (Index >= 0) then
      Parent.State[Index] := Parent.State[Index] + [osClosed];
  end
  else
  begin
    Exclude(ObjectState, osClosed);
    if (Parent <> nil) and (Index >= 0) then
      Parent.State[Index] := Parent.State[Index] - [osClosed];
  end;
  RedrawObject;
end;

function TEarthObject.GetClosed: boolean;
begin
  Result := osClosed in ObjectState;
end;

procedure TEarthObject.SetPresenterID(const aValue: integer);
begin
  FPresenterID := 0;
end;

function TEarthObject.Clone(aParent: TEarthObjectStore): TEarthObject;
begin
  Result := nil;
  if aParent = nil then
    exit;

  Result := TEarthObjectClass(ClassType).Create(aParent);
  Result.Assign(Self);
end;

procedure TEarthObject.Render(Earth: TCustomEarth; State: TEarthObjectStateSet);
begin
  if Assigned(OnRender) then
    OnRender(Self, Earth, State);
end;

procedure TEarthObject.UnRender;
begin
  { does not display anything }
end;

procedure TEarthObject.RedrawObject;
begin
  Include(ObjectState, osRedraw);

  if FIsUpdating = True then
    exit;
  if Parent <> nil then
    Parent.Notify(osnRedraw, Self);
end;

procedure TEarthObject.FixupReferences;
begin
  { Do nothing for TEarthObject }
  { Called after all objects on a Layer have been loaded from the data file }
  { Can be used by derived objects to fixup and references to other objects }
end;

procedure TEarthObject.SelectedChanged;
begin
  if (Parent <> nil) and (Index >= 0) then
  begin
    if Selected then
      Parent.State[Index] := Parent.State[Index] + [osSelected]
    else
      Parent.State[Index] := Parent.State[Index] - [osSelected];

    Parent.Notify(osnSelectedChanged, Self);
  end;
end;

procedure TEarthObject.SetSelected(aValue: boolean);
begin
  if (osSelected in ObjectState) = aValue then
    Exit;

  if aValue then
  begin
    Include(ObjectState, osSelected);
    FEarth.fSelectedObjects.Add(Self);
  end
  else
  begin
    Exclude(ObjectState, osSelected);
    FEarth.fSelectedObjects.Remove(Self);
  end;

  SelectedChanged; //call the SelectedSelectedChanged handler
  RedrawObject;
end;

procedure TEarthObject.SetTitle(const val: string);
begin
  if Sametext(fTitle, val) then
    exit;
  fTitle := val;
  RedrawObject;
end;

function TEarthObject.GetVisibleHeightMinAsEU: double;
begin
  Result := HeightUnitsFrom(fVisibleHeightMin, fVisibleHeightUnits);
end;

function TEarthObject.GetVisibleHeightMaxAsEU: double;
begin
  Result := HeightUnitsFrom(fVisibleHeightMax, fVisibleHeightUnits);
end;

procedure TEarthObject.SetVisibleHeightMin(const Val: single);
begin
  if fVisibleHeightMin = Val then
    exit;
  if val < 0 then
    exit;

  fVisibleHeightMin := Val;
  fiVisibleHeightMinEU := GetVisibleHeightMinAsEU;
  RedrawObject;
end;

procedure TEarthObject.SetVisibleHeightMax(const Val: single);
begin
  if fVisibleHeightMax = Val then
    exit;
  if val < 0 then
    exit;

  fVisibleHeightMax := Val;
  fiVisibleHeightMaxEU := GetVisibleHeightMaxAsEU;
  RedrawObject;
end;

procedure TEarthObject.SetVisibleHeightUnits(const Val: THeightUnitTypes);
begin
  if fVisibleHeightUnits = Val then
    exit;

  fVisibleHeightUnits := Val;
  fiVisibleHeightMinEU := GetVisibleHeightMinAsEU;
  fiVisibleHeightMaxEU := GetVisibleHeightMaxAsEU;
  RedrawObject;
end;

function TEarthObject.GetSelected: boolean;
begin
  Result := osSelected in ObjectState;
end;

procedure TEarthObject.SetHidden(aValue: boolean);
begin
  if (osHidden in ObjectState) = aValue then
    Exit;

  if aValue then
    Include(ObjectState, osHidden)
  else
    Exclude(ObjectState, osHidden);

  Include(ObjectState, osRedraw);

  RedrawObject;
end;

function TEarthObject.GetHidden: boolean;
begin
  Result := osHidden in ObjectState;
end;

procedure TEarthObject.WriteProperties(Writer: TEarthStreamWriter);
var
  inx: integer;
begin
  if Writer = nil then
    exit;
  Writer.WriteInteger(4);   //Write Object Stream Version  //Ver 4 is at 28-6-2008

  if Length(FTitle) > 0 then
    Include(ObjectFlags, ofTitle);
  if FObjectID <> 0 then
    Include(ObjectFlags, ofID);
  if FObjectGUID <> '' then
    Include(ObjectFlags, ofGUID);

  if (VisibleHeightMin <> FVisibleHeightMax) or (VisibleHeightUnits <> huEarthUnit) then
    Include(ObjectFlags, ofZoomMinMax);

  if Closed then
    Include(ObjectFlags, ofClosed);

  Writer.WriteInteger(integer(ObjectFlags)); // Save the Flag value

  if ofTitle in ObjectFlags then
    Writer.WriteShortString(FTitle);  // Save Title if present
  if ofID in ObjectFlags then
    Writer.WriteInteger(FObjectID);  // Save Object ID if present
  if ofGUID in ObjectFlags then
    Writer.WriteShortString(FObjectGUID); // Save Object GUID if present

  if ofZoomMinMax in ObjectFlags then
  begin
    Writer.WriteDouble(VisibleHeightMax);
    Writer.WriteDouble(VisibleHeightMin);
    inx := Ord(VisibleHeightUnits);
    Writer.WriteInteger(inx);
    //Writer.WriteBuffer(VisibleHeightUnits,SizeOf(THeightUnitTypes));
  end;

  Writer.WriteInteger(PresenterID); // Save PresenterID for Presenter reference
  FLayers.WriteProperties(Writer);   //Write Object.layers

end;

procedure TEarthObject.ReadProperties(Reader: TEarthStreamReader);
var
  iVer, inx: integer;

  //..................................................
  procedure _Old_Read_V3;
  begin
    if iver > 3 then
      exit;

    ObjectFlags := TEarthObjectFlagSet(Ord(Reader.ReadInteger));
    if ofTitle in ObjectFlags then
      FTitle := Reader.ReadShortString; // Read Title if present
    if ofID in ObjectFlags then
      FObjectID := Reader.ReadInteger; // Read Object ID if present
    if ofGUID in ObjectFlags then
      FObjectGUID := Reader.ReadShortString; // Read Object GUID if present

    if ofZoomMinMax in ObjectFlags then
    begin
      fVisibleHeightMax := Reader.ReadInteger;
      fVisibleHeightMin := Reader.ReadInteger;
    end;

    if ofClosed in ObjectFlags then
      Include(ObjectState, osClosed);
    FPresenterID := Reader.ReadSmallInt;
    if iVer >= 3 then
      fLayers.ReadProperties(Reader);
  end;

  //..................................................
begin
  if Reader = nil then
    exit;
  iVer := -1;

  Clear;

  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;  //Read Object Stream Version;

  if iVer <= 3 then
  begin
    _Old_Read_V3;
    exit;
  end;


  ObjectFlags := TEarthObjectFlagSet(Ord(Reader.ReadInteger));

  if ofTitle in ObjectFlags then
    FTitle := Reader.ReadShortString; // Read Title if present
  if ofID in ObjectFlags then
    FObjectID := Reader.ReadInteger; // Read Object ID if present
  if ofGUID in ObjectFlags then
    FObjectGUID := Reader.ReadShortString; // Read Object GUID if present

  if ofZoomMinMax in ObjectFlags then
  begin
    fVisibleHeightMax := Reader.ReadDouble;
    fVisibleHeightMin := Reader.ReadDouble;
    inx := Reader.ReadInteger;
    fVisibleHeightUnits := THeightUnitTypes(inx);
    //Reader.ReadBuffer(fVisibleHeightUnits,SizeOf(THeightUnitTypes));
  end;

  if ofClosed in ObjectFlags then
    Include(ObjectState, osClosed);
  FPresenterID := Reader.ReadInteger;
  fLayers.ReadProperties(Reader);

end;



procedure TEarthObject.Assign(Source: TPersistent);
begin
  if Source is TEarthObject then
  begin

    Exclude(self.ObjectState, osValidMER);
    self.BeginUpdate;
    Clear;
    Self.Layers.Assign(TEarthObject(Source).Layers);
    //......
    Self.fPresenterID := TEarthObject(Source).PresenterID;
    Self.fTitle := TEarthObject(Source).Title;
    Self.FCentroid := TEarthObject(Source).Centroid;
    Self.fVisibleHeightMin := TEarthObject(Source).VisibleHeightMin;
    Self.fVisibleHeightMax := TEarthObject(Source).VisibleHeightMax;
    Self.fVisibleHeightUnits := TEarthObject(Source).VisibleHeightUnits;
    Self.FObjectID := TEarthObject(Source).ID;
    Self.FObjectGUID := TEarthObject(Source).GUID;
    Self.fTag1 := TEarthObject(Source).Tag1;
    Self.fTag2 := TEarthObject(Source).Tag2;
    Self.fTag3 := TEarthObject(Source).Tag3;
    Self.fTag1String := TEarthObject(Source).Tag1String;
    Self.fTag2String := TEarthObject(Source).Tag2String;
    Self.fTag3String := TEarthObject(Source).Tag3String;
    Self.Hidden := TEarthObject(Source).Hidden;
    Self.Closed := TEarthObject(Source).Closed;
    Self.Selected := TEarthObject(Source).Selected;

    self.EndUpdate;
  end
  else
    inherited Assign(Source);
end;

function TEarthObject.LLInObjectMER(ptLL: TPointLL; iTolerance: double): integer;
begin
  Result := 0;
  if not (osHidden in ObjectState) then
    with ObjectMER do
      if (ptLL.iLatY >= iLatY - iTolerance) and (ptLL.iLatY < iLatY + iHeightY + iTolerance) then
        if iLongX + iWidthX + iTolerance <= GU_180_DEGREE then
        begin
          if (ptLL.iLongX >= iLongX - iTolerance) and (ptLL.iLongX < iLongX + iWidthX + iTolerance) then
            Result := 3;
        end
        else { split across the 180 meridian }
        if (ptLL.iLongX >= iLongX - iTolerance) and (ptLL.iLongX < GU_180_DEGREE) then
          Result := 2
        else
        if (ptLL.iLongX >= -GU_180_DEGREE) and (ptLL.iLongX < Mod180Float(iLongX + iWidthX + iTolerance)) then
          Result := 1;
end;

function TEarthObject.LLInObject(ptLL: TPointLL; iTolerance: double): boolean;
begin
  Result := False;
end;

function TEarthObject.IsObjectMERVisible(aProjection: TEarthProjection): boolean;
begin
  if aProjection = nil then
    exit;

  if fiVisibleHeightMinEU < 0 then
    fiVisibleHeightMinEU := GetVisibleHeightMinAsEU;
  if fiVisibleHeightMaxEU < 0 then
    fiVisibleHeightMaxEU := GetVisibleHeightMaxAsEU;

  Result := ((fiVisibleHeightMinEU = fiVisibleHeightMaxEU) or (aProjection.Altitude > fiVisibleHeightMinEU) and
    (aProjection.Altitude <= fiVisibleHeightMaxEU));
end;

//================= TEarthFileWriter ======================================

constructor TEarthFileWriter.Create(aLayer: TEarthLayer);
begin
  inherited Create;
  FLayer := aLayer;
end;

function TEarthFileWriter.SaveToFile(const Filename: TFilename): boolean;
begin
  //...
end;

//================= TEarthFileReader ======================================

procedure TEarthFileReader.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    Filename := Element.AttributeByName('Filename', Filename);

  inherited LoadEnvironment(Element);
end;

function TEarthFileReader.SaveEnvironment: TGXML_Element;
begin
  Result := inherited SaveEnvironment;

  Result.AddAttribute('Filename', ExtractRelativePath(ExpandFileNameUTF8(Earth.DataDirectory),
    ExpandFileNameUTF8(FFilename)));
end;

procedure TEarthFileReader.SetFileName(const sFilename: TFileName);
begin
  FFilename := Earth.ResolveFilename(sFilename);
end;

function TEarthFileReader.MapDataFile(aStream: TStream): boolean;
var
  iFileSize: integer;
begin
  Earth.ProgressMessage(pmStart, rsMapping);

  iFileSize := aStream.Size;
  aStream.Position := 0;

  Count := 0;
  Result := MapDataHeader(aStream);

  if Result then
    //    while aStream.Position < iFileSize do
    while True do
    begin
      if not MapDataObject(aStream, Count) then Break;

      Count := Count + 1;
      EarthObject[Count - 1];  // Load in the object

      if (Count mod 256) = 0 then
        Earth.ProgressMessage(pmPercent, IntToStr(MulDiv(aStream.Position, 100, iFileSize)));
    end;
  Earth.ProgressMessage(pmEnd, rsFinished);
end;

function TEarthFileReader.MapDataHeader(aStream: TStream): boolean;
begin
  Result := True;
end;

function TEarthFileReader.MapDataObject(aStream: TStream; iIndex: integer): boolean;
begin
  Result := False;
end;


//==============================  TCustomEarth ==================================================

constructor TCustomEarth.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FSelectedObjects := TList.Create;
  FiMinTextHeight := 5;
  FiMaxTextHeight := GMaxDouble;
end;

destructor TCustomEarth.Destroy;
begin
  FSelectedObjects.Free;
  inherited;
end;

procedure TCustomEarth.SetSelectedLayer(aLayer: TEarthLayer);
begin
  FSelectedLayer := aLayer;
  if Assigned(OnSelectedLayerChange) then
    OnSelectedLayerChange(Self);
end;

function TCustomEarth.GetSelectedObjects(aIndex: integer): TEarthObject;
begin
  Result := nil;
  if FSelectedObjects.Count = 0 then
    exit;

  if (aIndex >= 0) and (aIndex < FSelectedObjects.Count) then
    Result := TEarthObject(FSelectedObjects.Items[aIndex]);
end;

function TCustomEarth.GetSelectedObjectCount: integer;
begin
  Result := FSelectedObjects.Count;
end;

function TCustomEarth.GetSelectedObject: TEarthObject;
begin
  Result := nil;
  if FSelectedObjects.Count = 0 then
    exit;
  Result := TEarthObject(FSelectedObjects.Items[FSelectedObjects.Count - 1]);
end;

procedure TCustomEarth.SetSelectedObject(aEarthObject: TEarthObject);
begin
  Layers.ObjectsUnSelectAll;
  FSelectedObjects.Clear;
  if aEarthObject = nil then
    exit;
  aEarthObject.Selected := True;
end;

function TCustomEarth.MouseXYToLL(iX, iY: double; var ptLL: TPointLL): boolean;
var
  cp: TPoint;
begin
  cp := ControlToBitmap(pointD(iX, iY));
  Result := Projection.DeviceXYToLL(cp.X, cp.Y, ptLL);
end;

function TCustomEarth.LLToMouseXY(iLong, iLat: double; var ptXY: TPoint): boolean;
var
  p: TpointDouble;
begin
  p := Pointdouble(ptXY);
  Result := Projection.LLToDeviceXY(iLong, iLat, p);
  ptXY := PointD(p);
  ptXY := BitmapToControl(ptXY);
end;

procedure TCustomEarth.LocateToLL(const iLong, iLat: double);
begin
  with Projection do
  begin
    CenterLongitude := Mod180Float(iLong) / GU_DEGREE;
    CenterLatitude := iLat / GU_DEGREE;
  end;
end;

procedure TCustomEarth.LocateToLL2(const ptLL: TPointLL);
begin
  LocateToLL(ptLL.iLongX, ptLL.iLatY);
end;

procedure TCustomEarth.LocateToXY(const X, Y: integer);
var
  p: TPointLL;
begin
  MouseXYToLL(x, Y, p);
  LocateToLL(p.iLongX, p.iLatY);
end;

procedure TCustomEarth.LocateToObject(Obj: TEarthObject);
begin
  if Obj <> nil then
    with Obj.ObjectMER do
      LocateToLL(iLongX + iWidthX / 2, iLatY + iHeightY / 2);
end;

procedure TCustomEarth.LocateToCenter;
begin
  LocateToXY(Round(Width / 2), Round(Height / 2));
end;

function TCustomEarth.NextMRU: integer;
begin
  Result := 1;
end;

function TCustomEarth.GetMaxTextHeight: double;
begin
  Result := 1;
end;

function TCustomEarth.GetMinTextHeight: double;
begin
  Result := 1;
end;

procedure TCustomEarth.SetDataDirectory(const Value: string);
begin
  //----
end;

procedure TCustomEarth.SetEarthOptions(Value: TEarthOptionsSet);
begin
  //----
end;

procedure TCustomEarth.SetLongitudeStep(Value: double);
begin
  //----
end;

procedure TCustomEarth.SetLatitudeStep(Value: double);
begin
  //----
end;

procedure TCustomEarth.SetMaxTextHeight(iValue: double);
begin
  //----
end;

procedure TCustomEarth.SetMinTextHeight(iValue: double);
begin
  //----
end;

procedure TCustomEarth.SetScrollBars(Value: TScrollStyle);
begin
  //----
end;

procedure TCustomEarth.SetViewRect(const sRect: TRect);
begin
  //----
end;

procedure TCustomEarth.SeTTitlesRenderMethod(const val: TTitlesRenderMethod);
begin
  //----
end;


//===============================================================================================
//===================== TEarthObjectStore =======================================================
//===============================================================================================

constructor TEarthObjectStore.Create(aLayer: TEarthLayer);
begin
  inherited Create;
  FParent := aLayer;
  FEarth := FParent.Earth;
  Assert(FEarth <> nil, 'TEarthObjectStore.Create passed nil Parent');

  FObjectArray := DynArrayCreate(SizeOf(TObject), 0);
  FSubscribers := TObjectList.Create(false);
  FPresenters := TEarthPresenterStore.Create(self);
  FModified := False;
  Earth.Notify(gnObjectSourceCreate, Self);
end;

destructor TEarthObjectStore.Destroy;
var
  idx: integer;
  aLayer: TEarthLayer;
begin
  Earth.Notify(gnObjectSourceFree, Self);

  Active := False;

  SetCount(0);

  DynArrayFree(FObjectArray);

  // Clear any layers still attached to this object
  for idx := FSubscribers.Count - 1 downto 0 do
  begin
    aLayer := TEarthLayer(FSubscribers.Items[idx]);
    FSubscribers.Delete(idx);
    aLayer.Objects := nil;
  end;
  FSubscribers.Free;

  FPresenters.Free;
  FPresenters := nil;

  inherited Destroy;
end;

procedure TEarthObjectStore.Clear;
var
  i: integer;
begin
  for i := Count - 1 downto 0 do
    Delete(EarthObject[i]);
end;

procedure TEarthObjectStore.Assign(Source: TPersistent);
var
  i: integer;
begin
  if Source is TEarthObjectStore then
  begin
    self.BeginUpdate;
    Self.Clear;
    Self.Presenters.Assign(TEarthObjectStore(Source).Presenters);

    for I := 0 to TEarthObjectStore(Source).Count - 1 do
      TEarthObjectStore(Source).EarthObject[i].Clone(self);

    self.EndUpdate;
  end
  else
    inherited Assign(Source);
end;

procedure TEarthObjectStore.WriteProperties(Writer: TEarthStreamWriter);
var
  idx: integer;
begin

  if Writer = nil then
    exit;
  Writer.WriteInteger(1);   //Write Object Stream Version

  //Write Presenters
  Writer.WriteInteger(Presenters.Count);  // write the number of Layer Presenters
  for idx := 0 to Presenters.Count - 1 do
  begin
    Writer.WriteShortString(Presenters[idx].ClassName);
    Presenters[idx].WriteProperties(Writer);
  end;

  //Write Objects
  Writer.WriteInteger(Count);             // write the number of Object
  for idx := 0 to Count - 1 do
  begin
    Writer.WriteShortString(getobject(idx).ClassName);
    getobject(idx).WriteProperties(Writer);
  end;

end;

procedure TEarthObjectStore.ReadProperties(Reader: TEarthStreamReader);
var
  idx, iCount: integer;
  oNew: TEarthPresenter;
  oNewObj: TEarthObject;
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;

  BeginUpdate;
  Clear;
  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version
  //Read Presenters
  iCount := Reader.ReadInteger;
  for idx := 0 to iCount - 1 do
  begin
    oNew := TEarthPresenterClass(FindClass(Reader.ReadShortString)).Create(Presenters, 0);
    oNew.ReadProperties(Reader);
  end;
  //Read Objects
  iCount := Reader.ReadInteger;
  for idx := 0 to iCount - 1 do
  begin
    oNewObj := TEarthObjectClass(FindClass(Reader.ReadShortString)).Create(self);
    oNewObj.ReadProperties(Reader);
  end;

  EndUpdate;
end;

procedure TEarthObjectStore.WriteMetaData(Writer: TEarthStreamWriter);
var
  idx: integer;
begin

  if Writer = nil then
    exit;
  Writer.WriteInteger(1);   //Write Object Stream Version

  //Write Presenters
  Writer.WriteInteger(Presenters.Count);
  for idx := 0 to Presenters.Count - 1 do
  begin
    Writer.WriteShortString(Presenters[idx].ClassName);
    Presenters[idx].WriteProperties(Writer);
  end;
  //Write Objects
  Writer.WriteInteger(Count);
  for idx := 0 to Count - 1 do
  begin
    Writer.WriteMER(MER[idx]);
    Writer.WriteInteger(PresenterID[idx]);
    Writer.WriteBoolean(osClosed in State[idx]);
  end;
end;

procedure TEarthObjectStore.ReadMetaData(Reader: TEarthStreamReader);
var
  idx: integer;
  oNew: TEarthPresenter;
  iVer: integer;
begin
  if Reader = nil then
    exit;
  iVer := -1;

  if giFileVersion >= TG_FILEVERSION1002 then
    iVer := Reader.ReadInteger;            //Read Object Stream Version

  //Read Presenters
  idx := Reader.ReadInteger;  // Get the number of Presenters
  while idx > 0 do
  begin
    oNew := TEarthPresenterClass(FindClass(Reader.ReadShortString)).Create(Presenters, 0);
    oNew.ReadProperties(Reader);
    Dec(idx);
  end;
  //Read Objects
  Count := Reader.ReadInteger;
  for idx := 0 to Count - 1 do
    with EarthObjectData[idx] do
    begin
      ObjectMER := Reader.ReadMER;
      ObjectState := [osValidMER];
      PresenterID := Reader.ReadInteger;
      if Reader.ReadBoolean then
        ObjectState := ObjectState + [osClosed];
    end;
end;

function TEarthObjectStore.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;
  Result.AddAttribute('Class', ClassName);
end;

procedure TEarthObjectStore.LoadEnvironment(Element: TGXML_Element);
begin
  // Do Nothing
end;

procedure TEarthObjectStore.SaveMetaData;
begin
  // Do Nothing
end;

procedure TEarthObjectStore.LoadMetaData;
begin
  // Do Nothing
end;

procedure TEarthObjectStore.Open;
begin
  Active := True;
  Modified := False;
end;

procedure TEarthObjectStore.Close;
begin
  Active := False;
end;

procedure TEarthObjectStore.Add(Obj: TEarthObject);
var
  iSlot: integer;
begin
  if not bLoadingObject then
  begin
    iSlot := Count;
    Count := Count + 1; // make some space in the data array

    EarthObjectData[iSlot] := Obj;
    Obj.MRU := Earth.NextMRU;
    Obj.Index := iSlot;
    Obj.FixupReferences;
    Exclude(Obj.ObjectState, osValidMER);

    Notify(osnAdd, Obj);
    Modified := True;
  end;
end;

procedure TEarthObjectStore.Move(iFrom, iTo: integer);
var
  idx: integer;
begin
  DynArrayMove(FObjectArray, iFrom, iTo);

  // Update the object index values
  for idx := 0 to Count - 1 do
    EarthObjectData[idx].Index := idx;

  Notify(osnRedraw, nil);
  Modified := True;
end;

procedure TEarthObjectStore.Delete(Obj: TEarthObject);
var
  idx, iSlot: integer;
begin
  if (Obj = nil) or (Obj.Index < 0) then
    Exit;

  iSlot := Obj.Index;
  Notify(osnDelete, Obj);

  Earth.CacheUsedDelta(-Obj.ObjectInstanceSize);
  Obj.ObjectState := [osDeleted];

  DynArrayDelete(FObjectArray, iSlot);
  DynArraySetLength(FObjectArray, FiCapacity);  // maintain the length of the array
  FiCount := FiCount - 1;

  // Update the object index values
  for idx := Obj.Index to Count - 1 do
    EarthObjectData[idx].Index := idx;

  Obj.Index := -1; // Indicate that the object does not belong to a layer
  Modified := True;
end;

function TEarthObjectStore.GetLayer(iIndex: integer): TEarthLayer;
begin
  if (iIndex >= 0) and (iIndex < FSubscribers.Count) then
    Result := TEarthLayer(FSubscribers.Items[iIndex])
  else
    Result := nil;
end;

function TEarthObjectStore.GetObjectsMER: TMER;
var
  idx: integer;
  iLeft, iTop, iRight, iBottom: double;
begin
  if Count > 0 then
  begin
    Result := GIS_SysUtils.MER(GMaxDouble, GMaxDouble, 0, 0);

    iLeft := GMaxDouble;
    iTop := GMaxDouble;
    iRight := -GMaxDouble;
    iBottom := -GMaxDouble;

    for idx := 0 to Count - 1 do
      with MER[idx] do
      begin
        if iLongX < iLeft then
          iLeft := iLongX - 0.0000000000000001;
        if iLatY < iTop then
          iTop := iLatY - 0.0000000000000001;
        if iLongX + iWidthX > iRight then
          iRight := iLongX + iWidthX;
        if iLatY + iHeightY > iBottom then
          iBottom := iLatY + iHeightY;
      end;

    if iLeft < -GU_180_DEGREE then
      iLeft := -GU_180_DEGREE;
    if iRight > GU_180_DEGREE then
      iRight := GU_180_DEGREE;

    Result := GIS_SysUtils.MER(iLeft, iTop, iRight - iLeft, iBottom - iTop);
  end
  else
    Result := GIS_SysUtils.MER(0, 0, 1, 1);
end;

function TEarthObjectStore.GetMER(iIndex: integer): TMER;
begin
  Result := EarthObjectData[iIndex].ObjectMER;
end;

function TEarthObjectStore.GetPresenterID(iIndex: integer): integer;
begin
  Result := EarthObjectData[iIndex].PresenterID;
end;

function TEarthObjectStore.GetObject(iIndex: integer): TEarthObject;
var
  bNewObject: boolean;
  ObjData: TEarthObjectData;
begin
  ObjData := EarthObjectData[iIndex];

  if ObjData is TEarthObject then
    Result := TEarthObject(ObjData)
  else
    try
      bLoadingObject := True;
      bNewObject := osNew in ObjData.ObjectState;

      // Read the object from the data reader
      Result := LoadObject(iIndex, bNewObject);

      if Result <> nil then
      begin
        if not bNewObject then
        begin
          Result.ObjectState := ObjData.ObjectState;
          Result.ObjectFlags := ObjData.ObjectFlags;
          Result.FObjectMER := ObjData.ObjectMER;
          Result.UserObject := ObjData.UserObject;
          Result.Value := ObjData.Value;
        end;
        if ObjData.PresenterID <> 0 then
          Result.PresenterID := ObjData.PresenterID;

        // ObjData is Freed when Result is assigned
        EarthObjectData[iIndex] := Result;

        Result.Index := iIndex;
        Result.FixupReferences;

        if Assigned(TEarthBase(Earth).OnObjectCreate) then
          TEarthBase(Earth).OnObjectCreate(TEarthBase(Earth), Result);

        Exclude(Result.ObjectState, osDiscardable); // Stop the object from being swapped out
        if NoCache = False then  // do not cache the objects
        begin
          Earth.CacheUsedDelta(Result.ObjectInstanceSize);
          Include(Result.ObjectState, osDiscardable); // Allow the object to be swapped out
        end;
      end;
    finally
      bLoadingObject := False;
    end;

  if Result = nil then
    raise EEarthException.Create(rsEEarthBadObjectMsg);

  Result.MRU := Earth.NextMRU;
end;

function TEarthObjectStore.GetObjectData(iIndex: integer): TEarthObjectData;
begin
  Assert((iIndex >= 0) and (iIndex < Count), 'GIS_Classes-> GetObjectData: Invalid Index ' + IntToStr(iIndex));
  //............ Generate errors ....

  //=====================================================================================================
  Result := TEarthObjectData(DynArrayAsObject(FObjectArray, iIndex));
  if Result = nil then
  begin
    Result := TEarthObjectData.Create;
    Result.ObjectState := [osNew];
    DynArraySetAsObject(FObjectArray, iIndex, Result);
  end;
end;

procedure TEarthObjectStore.SetObjectData(iIndex: integer; Obj: TEarthObjectData);
var
  ObjData: TEarthObjectData;
begin
  Assert((iIndex >= 0) and (iIndex < Count), 'GIS_Classes-> SetObjectData: Invalid Index ' + IntToStr(iIndex));
  ObjData := TEarthObjectData(DynArraySetAsObject(FObjectArray, iIndex, Obj));

  if ObjData <> nil then
  begin
    if ObjData is TEarthObject then
      TEarthObject(ObjData).FParent := nil;
    ObjData.Free;
  end;
end;

function TEarthObjectStore.GetState(iIndex: integer): TEarthObjectStateSet;
begin
  Result := EarthObjectData[iIndex].ObjectState;
end;

function TEarthObjectStore.GetUserObject(iIndex: integer): TObject;
begin
  Result := EarthObjectData[iIndex].UserObject;
end;

function TEarthObjectStore.GetValue(iIndex: integer): double;
begin
  Result := EarthObjectData[iIndex].Value;
end;

procedure TEarthObjectStore.ListActiveObjects(AList: TList);
var
  idx: integer;
  Obj: TObject;
begin
  for idx := 0 to Count - 1 do
  begin
    Obj := DynArrayAsObject(FObjectArray, idx);
    if (Obj <> nil) and (obj is TEarthObject) then
      if osDiscardable in TEarthObject(Obj).ObjectState then
        AList.Add(Obj);
  end;
end;

function TEarthObjectStore.LoadObject(iIndex: integer; bNewObject: boolean): TEarthObject;
begin
  Result := nil;
end;

function TEarthObjectStore.MetaDataFilename(const sFilename: TFilename): string;
begin
  Result := Trim(sFilename);

  if Result <> '' then
  begin
    Result := Result + '.md';

    case TEarthBase(Earth).SaveMetaDataLocation of
      smdInDataDirectory:
        Result := TEarthBase(Earth).DataDirectory + ExtractFilename(Result);
      smdWithExeFile:
        Result := ExtractFilePath(Application.ExeName) + ExtractFilename(Result);
      smdDiscard:
        Result := '';
    end;
  end;
end;

procedure TEarthObjectStore.Notify(Notification: TObjectSourceNotification; Obj: TEarthObject);
var
  idx: integer;
begin
  for idx := 0 to FSubscribers.Count - 1 do
    TEarthLayer(FSubscribers.Items[idx]).Notify(Notification, Obj);
end;

function TEarthObjectStore.ObjectInstanceSize: integer;
var
  idx: integer;
  ObjData: TEarthObjectData;
begin
  Result := 0;

  for idx := 0 to Count - 1 do
  begin
    ObjData := TEarthObjectData(DynArrayAsObject(FObjectArray, idx));
    if ObjData <> nil then
      Inc(Result, ObjData.ObjectInstanceSize);
  end;
end;

function TEarthObjectStore.InternalOpen: boolean;
begin
  Notify(osnRedraw, nil);
  Result := True;
end;

function TEarthObjectStore.InternalClose: boolean;
begin
  Notify(osnRedraw, nil);
  Result := True;
end;

procedure TEarthObjectStore.SetActive(const Value: boolean);
begin
  if FActive <> Value then
  begin
    if Value then
      FActive := InternalOpen
    else
      FActive := not InternalClose;

    if Earth <> nil then
      Earth.Notify(gnObjectSourceActiveChange, Self);
  end;
end;

procedure TEarthObjectStore.SetCapacity(iValue: integer);
begin
  while FiCapacity > iValue do // free up objects if reducing length
  begin
    EarthObjectData[FiCapacity - 1] := nil;
    Dec(FiCapacity);
  end;

  FiCapacity := iValue;
  DynArraySetLength(FObjectArray, iValue);

  if FiCount > iValue then
    FiCount := iValue;
end;

procedure TEarthObjectStore.SetCount(iValue: integer);
begin
  if FObjectArray = nil then
    FObjectArray := DynArrayCreate(SizeOf(TObject), iValue);

  while FiCount > iValue do
  begin
    EarthObjectData[FiCount - 1] := nil;
    Dec(FiCount);
  end;

  if iValue >= Capacity then
    SetCapacity(iValue + 255);

  FiCount := iValue;
end;

procedure TEarthObjectStore.SetMER(iIndex: integer; Value: TMER);
begin
  EarthObjectData[iIndex].ObjectMER := Value;
end;

procedure TEarthObjectStore.SetPresenterID(iIndex: integer; const Value: integer);
begin
  EarthObjectData[iIndex].PresenterID := Value;
end;

procedure TEarthObjectStore.SetState(iIndex: integer; AState: TEarthObjectStateSet);
begin
  EarthObjectData[iIndex].ObjectState := AState;
end;

procedure TEarthObjectStore.SetUserObject(iIndex: integer; Obj: TObject);
begin
  EarthObjectData[iIndex].UserObject := Obj;
end;

procedure TEarthObjectStore.SetValue(iIndex: integer; Value: double);
begin
  EarthObjectData[iIndex].Value := Value;
end;

procedure TEarthObjectStore.Subscribe(aLayer: TEarthLayer);
begin
  if FSubscribers.IndexOf(aLayer) <0 then
  begin
    FSubscribers.Add(aLayer);
  end;
end;

procedure TEarthObjectStore.UnloadObjects;
var
  idx: integer;
  EarthObj: TEarthObject;
  ObjData: TEarthObjectData;
begin
  // Discard all Objects from the Store
  for idx := 0 to Count - 1 do
  begin
    EarthObj := TEarthObject(DynArrayAsObject(FObjectArray, idx));
    if (EarthObj <> nil) and (osDiscardable in EarthObj.ObjectState) then
      if EarthObj is TEarthObject then
      begin
        if osSelected in EarthObj.ObjectState then
          EarthObj.Selected := False;

        ObjData := TEarthObjectData.Create;
        ObjData.Assign(EarthObj);
        EarthObjectData[idx] := ObjData;
      end;
  end;
end;

procedure TEarthObjectStore.UnSubscribe(aLayer: TEarthLayer);
var
  iPos: integer;
begin
  iPos := FSubscribers.IndexOf(aLayer);
  if iPos >= 0 then
    FSubscribers.Delete(iPos);

  if FSubscribers.Count = 0 then // Free self if no layers now subscribed
    Free;
end;

procedure TEarthObjectStore.SetPresenterToAllObjects(index: integer);
var
  i: integer;
begin
  if Count < 1 then
    exit;
  Earth.BeginUpdate;
  for i := 0 to Count - 1 do
    if EarthObject[i].PresenterID <> index then
      EarthObject[i].PresenterID := index;
  Earth.EndUpdate;
end;

{=================== TEarthTransparent ========================================}
constructor TEarthTransparent.Create;
begin
  FPen := TEarthPen.Create(self);
  FBrush := TEarthBrush.Create(self);
  Clear;
end;

destructor TEarthTransparent.Destroy;
begin

  FPen.Free;
  FBrush.Free;
  inherited;
end;

procedure TEarthTransparent.Clear;
begin
  FPen.PenColor := clGray32;
  FBrush.FBrushColor := cldimGray32;
end;

procedure TEarthTransparent.RedrawObject;
begin
  DoChange(self);
end;

procedure TEarthTransparent.DoChange(Sender: TObject);
begin
  if Assigned(fOnChange) then
    fOnChange(Self);
end;

procedure TEarthTransparent.SetPen(aPen: TEarthPen);
begin
  FPen.Assign(aPen);
end;

procedure TEarthTransparent.SetBrush(aBrush: TEarthBrush);
begin
  FBrush.Assign(aBrush);
end;

function TEarthTransparent.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddElement('Pen', Pen.SaveEnvironment);
  Result.AddElement('Brush', Brush.SaveEnvironment);
end;

procedure TEarthTransparent.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      Pen.LoadEnvironment(ElementByName('Pen', 0));
      Brush.LoadEnvironment(ElementByName('Brush', 0));

    end;
end;

//======================= TSelectedOjectOptions ====================================
constructor TSelectedOjectOptions.Create;
begin
  FPen := TEarthPen.Create(self);
  FBrush := TEarthBrush.Create(self);
  FFont := TEarthFont.Create(self);
  Clear;
end;

destructor TSelectedOjectOptions.Destroy;
begin
  FFont.Free;
  FPen.Free;
  FBrush.Free;
  inherited;
end;

procedure TSelectedOjectOptions.Clear;
begin
  FFont.FFontFillColor := clRed32;
  FPen.PenColor := clRed32;
  FBrush.BrushColor := clYellow32;
  FBrushSameWithNormal := False;
  FPenWidthSameWithNormal := False;
  FFontSameWithNormal := True;
end;

procedure TSelectedOjectOptions.RedrawObject;
begin
  DoChange(self);
end;

procedure TSelectedOjectOptions.DoChange(Sender: TObject);
begin
  if Assigned(fOnChange) then
    fOnChange(Self);
end;

procedure TSelectedOjectOptions.SetPen(aPen: TEarthPen);
begin
  FPen.Assign(aPen);
end;

procedure TSelectedOjectOptions.SetBrush(aBrush: TEarthBrush);
begin
  FBrush.Assign(aBrush);
end;

procedure TSelectedOjectOptions.SetFont(aFont: TEarthFont);
begin
  FFont.Assign(aFont);
end;

procedure TSelectedOjectOptions.SetBrushSameWithNormal(const val: boolean);
begin
  FBrushSameWithNormal := val;
  DoChange(self);
end;

procedure TSelectedOjectOptions.SetPenWidthSameWithNormal(const val: boolean);
begin
  FPenWidthSameWithNormal := val;
  DoChange(self);
end;

procedure TSelectedOjectOptions.SetFontSameWithNormal(const val: boolean);
begin
  FFontSameWithNormal := val;
  DoChange(self);
end;

function TSelectedOjectOptions.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddBoolAttribute('BrushSameWithNormal', BrushSameWithNormal);
  Result.AddBoolAttribute('PenWidthSameWithNormal', PenWidthSameWithNormal);

  Result.AddElement('Pen', Pen.SaveEnvironment);
  Result.AddElement('Brush', Brush.SaveEnvironment);
  Result.AddElement('Font', Font.SaveEnvironment);
end;

procedure TSelectedOjectOptions.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      BrushSameWithNormal := BoolAttributeByName('BrushSameWithNormal', BrushSameWithNormal);
      PenWidthSameWithNormal := BoolAttributeByName('PenWidthSameWithNormal', PenWidthSameWithNormal);

      Pen.LoadEnvironment(ElementByName('Pen', 0));
      Brush.LoadEnvironment(ElementByName('Brush', 0));
      Font.LoadEnvironment(ElementByName('Font', 0));
    end;
end;

//==============================================================================================
initialization
  RegisterClasses([TEarthLayer]);
end.

