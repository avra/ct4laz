
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_MapperE002;



interface

uses
  LCLIntf, LCLType, LMessages,
  Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, GIS_Classes, GIS_SysUtils, GIS_EarthObjects, GIS_TextReader, Math;

type
  TDataProjection = (dpUNKNOWN, dpGEOGRAPHIC, dpPOLAR, dpLAMBERT);

  TfrmE00Import = class(TForm)
    btnImport: TBitBtn;
    btnCancel: TBitBtn;
    cbxArcs: TCheckBox;
    cbxText: TCheckBox;
  private
  public
  end;


procedure EarthImportE00(gLayer: TEarthLayer; const sFilename: string);

var
  frmE00Import: TfrmE00Import;

implementation

{$R *.lfm}
uses GIS_EarthUtils;

{-------------------------------------------------------------------------}

var
  gEarth: TCustomEarth;
  gLayer: TEarthLayer;
  gTextReader: TEarthTextReader;
  gARCPolygon: TGeoDataObject;
  giUnits, giCurrentChain: integer;
  gDataProjection: TDataProjection;
  gPhi0, gPhi1, gPhi2, gLambda0, gN, gF, gRho0: extended;
  gX1, gCSF, gM1: extended;
  gEquitorialRadius, gEccentricity: extended;
  giFalseEasting, giFalseNorthing: integer;

  {------------------------------------------------------------------------------
    PolarInit
  ------------------------------------------------------------------------------}
procedure PolarInit(iOriginLong, iOriginLat: double; eCSF: extended; Spheroid: TSpheroid);
var
  e2: extended;
begin
  gCSF := eCSF;
  gPhi1 := iOriginLat * GU_TORADIANS;
  gLambda0 := iOriginLong * GU_TORADIANS;

  gEquitorialRadius := SpheroidData[Ord(Spheroid)].a * GU_TORADIANS;
  e2 := 2 * SpheroidData[Ord(Spheroid)].f - Sqr(SpheroidData[Ord(Spheroid)].f);
  gEccentricity := Sqrt(e2);

  gM1 := Cos(gPhi1) / Sqrt(1 - e2 * Sqr(Sin(gPhi1)));

  gX1 := 2 * ArcTan(Tan(QuarterPi + gPhi1 / 2) * Power((1 - gEccentricity * Sin(gPhi1)) / (1 + gEccentricity * Sin(gPhi1)),
    gEccentricity / 2)) - HalfPi;
end;

{------------------------------------------------------------------------------
  PolarToGeographic
------------------------------------------------------------------------------}
procedure PolarToGeographic(var iLong, iLat: double);
var
  X, Y, Rho, Phi, C, cappa, Tmp: extended;
begin
  X := iLong * GU_TORADIANS;
  Y := iLat * GU_TORADIANS;

  Rho := Sqrt(X * X + Y * Y);
  if Rho = 0 then
  begin
    iLong := Round(gPhi1 * GU_FROMRADIANS);
    iLat := Round(gLambda0 * GU_FROMRADIANS);
  end
  else
  begin
    C := 2 * ArcTan2(Tan(Rho * Cos(gX1)), (2 * gEquitorialRadius * gCSF * gM1));
    cappa := ArcSin(Cos(C) * Sin(gX1) + (Y * Sin(C) * Cos(gX1) / Rho));

    Phi := cappa;
    repeat
      Tmp := Phi;
      Phi := 2 * ArcTan(Tan(QuarterPi + cappa / 2) * Power((1 + gEccentricity * Sin(Phi)) / (1 - gEccentricity * Sin(Phi)),
        gEccentricity * 0.5)) - HalfPi;
    until Abs(Phi - Tmp) < 0.00000005;

    iLong := ((gLambda0 + ArcTan2(X * Sin(C), (Rho * Cos(gX1) * Cos(C) - Y * Sin(gX1) * Sin(C)))) * GU_FROMRADIANS);
    iLat := (Phi * GU_FROMRADIANS);
  end;
end;

{------------------------------------------------------------------------------
  LambertInit
------------------------------------------------------------------------------}
procedure LambertInit(iFirstParallel, iSecondParallel, iOriginLong, iOriginLat: double; Spheroid: TSpheroid);
var
  e2, m1, m2, t1, t2, t0: extended;

  function Tx(Phi: extended): extended;
  begin
    Result := Tan(QuarterPi - Phi * 0.5) / Power((1 - gEccentricity * Sin(Phi)) / (1 + gEccentricity * Sin(Phi)), gEccentricity * 0.5);
  end;

begin
  gPhi1 := iFirstParallel * GU_TORADIANS;
  gPhi2 := iSecondParallel * GU_TORADIANS;
  gPhi0 := iOriginLat * GU_TORADIANS;
  gLambda0 := iOriginLong * GU_TORADIANS;

  gEquitorialRadius := SpheroidData[Ord(Spheroid)].a * GU_TORADIANS;

  e2 := 2 * SpheroidData[Ord(Spheroid)].f - Sqr(SpheroidData[Ord(Spheroid)].f);
  gEccentricity := Sqrt(e2);

  m1 := Cos(gPhi1) / Sqrt(1 - e2 * Sqr(Sin(gPhi1)));
  m2 := Cos(gPhi2) / Sqrt(1 - e2 * Sqr(Sin(gPhi2)));

  t0 := Tx(gPhi0);
  t1 := Tx(gPhi1);
  t2 := Tx(gPhi2);

  gN := Ln(m1 / m2) / ln(t1 / t2);
  gF := m1 / (gN * Power(t1, gN));
  gRho0 := gEquitorialRadius * gF * Power(t0, gN);
end;

{------------------------------------------------------------------------------
  LambertToGeographic
------------------------------------------------------------------------------}
procedure LambertToGeographic(var iLong, iLat: double);
var
  Tmp, Phi, Rho, Theta, t: extended;
  X, Y: extended;
begin
  X := iLong * GU_TORADIANS;
  Y := iLat * GU_TORADIANS;

  Rho := Sign(gN) * Sqrt(X * X + Sqr(gRho0 - Y));

  Theta := ArcTan2(X, (gRho0 - Y));

  t := Power(Rho / (gEquitorialRadius * gF), 1 / gN);

  Phi := HalfPi - 2 * ArcTan(t);
  repeat
    Tmp := Phi;
    Phi := HalfPi - 2 * ArcTan(t * Power((1 - gEccentricity * Sin(Tmp)) / (1 + gEccentricity * Sin(Tmp)), gEccentricity * 0.5));
  until Abs(Phi - tmp) < 0.00000005;

  iLong := ((Theta / gN + gLambda0) * GU_FROMRADIANS) + giFalseNorthing;
  iLat := (Phi * GU_FROMRADIANS) + giFalseEasting;
end;

{-------------------------------------------------------------------------}
function StrToGU(const sVal: string): double;
var
  eVal: extended;
begin
  eVal := StrToFloat(sVal);
  case giUnits of
    0: Result := DecimalToEarthUnits(eVal);
    1: Result := EarthUnitsFrom(eVal, euMeter);
    2: Result := EarthUnitsFrom(eVal, euFoot);
    else
      Result := 0;
  end;
end;

{-------------------------------------------------------------------------}
procedure ProjectPoint(var iLong, iLat: double);
begin
  case gDataProjection of
    dpLAMBERT:
      LambertToGeographic(iLong, iLat);
    dpPOLAR:
      PolarToGeographic(iLong, iLat);
  end;
end;

{-------------------------------------------------------------------------}
procedure AddNewPoint(const sTmp: string; iLongStart, iLatStart: integer);
var
  iLong, iLat: double;
begin
  iLong := StrToGU(Copy(sTmp, iLongStart, 14));
  iLat := StrToGU(Copy(sTmp, iLatStart, 14));

  ProjectPoint(iLong, iLat);

  gARCPolygon.PointsGroups[giCurrentChain].Add(PointLL(iLong, iLat));
end;

{-------------------------------------------------------------------------}
function CopyPoints(APolygon: TGeoDataObject; const sTmp: string; iNodeStart: integer): boolean;
var
  iArcNum, idx, iCount: integer;
begin
  iArcNum := StrToInt(Trim(Copy(sTmp, iNodeStart, 10)));
  Result := iArcNum = 0;
  if not Result then
  begin
    iCount := gARCPolygon.PointsGroups[Abs(iArcNum) - 1].Count - 1;

    with gARCPolygon.PointsGroups[Abs(iArcNum) - 1] do
    begin
      if iArcNum > 0 then
        for idx := 0 to iCount do
          APolygon.PointsGroups[0].Add(AsLL[idx])
      else
        for idx := iCount downto 0 do
          APolygon.PointsGroups[0].Add(AsLL[idx]);
    end;
  end;
end;

{-------------------------------------------------------------------------}
procedure ReadARCData;
var
  sTmp: string;
  iIndex, iPoints, iPoly, iCoverID, iFromNode, iToNode: integer;
begin
  if not frmE00Import.cbxArcs.Checked then
    Exit;
  gEarth.ProgressMessage(pmMessage, Format('%s: Reading ARC data...', [gLayer.Name]));
  giCurrentChain := -1;
  repeat
    gTextReader.ReadLn(sTmp);

    iIndex := StrToInt(Copy(sTmp, 1, 10));
    iCoverID := StrToInt(Copy(sTmp, 11, 10));
    iFromNode := StrToInt(Copy(sTmp, 21, 10));
    iToNode := StrToInt(Copy(sTmp, 31, 10));
    iPoly := StrToInt(Copy(sTmp, 41, 10));
    iPoints := StrToInt(Copy(sTmp, 61, 10));

    if iIndex = -1 then
      Break;

    if iPoints > 0 then
      with gARCPolygon do
      begin
        Inc(giCurrentChain);
        if giCurrentChain = PointsGroups.Count then
          PointsGroups.Count := PointsGroups.Count + 32;
        gARCPolygon.PointsGroups[giCurrentChain] := TPointStore.Create;
      end;

    while iPoints > 1 do
    begin
      gTextReader.ReadLn(sTmp);
      AddNewPoint(sTmp, 1, 15);
      AddNewPoint(sTmp, 29, 43);
      Dec(iPoints, 2);
    end;

    if iPoints > 0 then
    begin
      gTextReader.ReadLn(sTmp);
      AddNewPoint(sTmp, 1, 15);
    end;
  until gTextReader.EOT;

  gARCPolygon.PointsGroups.Count := giCurrentChain + 1;
end;

{-------------------------------------------------------------------------}
procedure ReadCNTData;
begin
  gEarth.ProgressMessage(pmMESSAGE, Format('%s: Reading CNT data...', [gLayer.Name]));
end;

{-------------------------------------------------------------------------}
procedure ReadPALData;
var
  idx, iArcs, iPolygonNumber: integer;
  APolygon: TGeoDataObject;
  sTmp: string;
begin
  if not frmE00Import.cbxArcs.Checked then
    Exit;

  gEarth.ProgressMessage(pmMESSAGE, Format('%s: Reading PAL data...', [gLayer.Name]));
  { Process the PAL file }
  gTextReader.ReadLn(sTmp);
  iArcs := StrToInt(Copy(sTmp, 1, 10));

  { Skip the universal polygon }
  idx := (iArcs div 2) + (iArcs mod 2);
  while idx > 0 do
  begin
    gTextReader.ReadLn(sTmp);
    Dec(idx);
  end;
  iPolygonNumber := 1;

  { read in the other polygons }
  repeat
    gTextReader.ReadLn(sTmp);
    iArcs := StrToInt(Copy(sTmp, 1, 10));
    if iArcs = -1 then
      Exit;

    APolygon := TGeoDataObject.Create(gLayer.Objects);
    APolygon.Title := IntToStr(iPolygonNumber);
    Inc(iPolygonNumber);
    APolygon.PointsGroups.Count := 1;
    APolygon.PointsGroups[0] := TPointStore.Create;

    while iArcs > 0 do
    begin
      gTextReader.ReadLn(sTmp);
      if CopyPoints(APolygon, sTmp, 1) then
        with APolygon do
          if PointsGroups[PointsGroups.Count - 1].Count > 0 then
          begin
            PointsGroups.Count := PointsGroups.Count + 1;
            PointsGroups[PointsGroups.Count - 1] := TPointStore.Create;
          end;
      Dec(iArcs);

      if iArcs > 0 then
      begin
        if CopyPoints(APolygon, sTmp, 31) then
          with APolygon do
            if PointsGroups[PointsGroups.Count - 1].Count > 0 then
            begin
              PointsGroups.Count := PointsGroups.Count + 1;
              PointsGroups[PointsGroups.Count - 1] := TPointStore.Create;
            end;
        Dec(iArcs);
      end;
    end;
  until gTextReader.EOT;
end;

{-------------------------------------------------------------------------}
procedure ReadTX7Data;
var
  idx, iPoints: integer;
  sTmp, sText: string;
  iLong, iLat: double;
begin
  if not frmE00Import.cbxText.Checked then
    Exit;

  gEarth.ProgressMessage(pmMESSAGE, Format('%s: Reading TX7 data...', [gLayer.Name]));
  { Process the TX7 file }
  gTextReader.ReadLn(sTmp);
  while not gTextReader.EOT do
  begin
    gTextReader.ReadLn(sTmp);
    if StrToInt(Copy(sTmp, 1, 10)) = -1 then
      Break;

    iPoints := StrToInt(Copy(sTmp, 21, 10));
    for idx := 0 to 8 do
      gTextReader.ReadLn(sTmp);

    iLong := StrToGU(Copy(sTmp, 1, 14));
    iLat := StrToGU(Copy(sTmp, 15, 14));

    for idx := 0 to iPoints - 1 do
      gTextReader.ReadLn(sTmp);
    sText := Trim(sTmp);

    if sText <> '' then
      with TGeoDataObject.Create(gLayer.Objects) do
      begin
        Title := sText;
        Centroid := PointLL(iLong, iLat);
      end;
  end;
end;

{-------------------------------------------------------------------------}
procedure ReadPRJData;
var
  sTmp, sProj: string;
  Spheroid: TSpheroid;
  iParameter: integer;
  sParams: array[0..8] of string;
begin
  gEarth.ProgressMessage(pmMESSAGE, Format('%s: Reading PRJ data...', [gLayer.Name]));

  Spheroid := gEarth.Projection.Spheroid;

  giFalseEasting := 0;
  giFalseNorthing := 0;

  sParams[4] := '000000.000000';
  sParams[5] := '000000.000000';

  iParameter := -1;
  giUnits := 0;
  gDataProjection := dpUNKNOWN;

  repeat
    gTextReader.ReadLn(sTmp);
    sTmp := Uppercase(sTmp);
    if Trim(sTmp) = 'EOP' then
      Break;

    if Trim(Copy(sTmp, 1, 14)) = 'PROJECTION' then
    begin
      sProj := Uppercase(Trim(Copy(sTmp, 15, 255)));
      if sProj = 'GEOGRAPHIC' then
        gDataProjection := dpGEOGRAPHIC;
      if sProj = 'POLAR' then
        gDataProjection := dpPOLAR;
      if sProj = 'LAMBERT' then
        gDataProjection := dpLAMBERT;
    end;

    if Trim(Copy(sTmp, 1, 14)) = 'UNITS' then
    begin
      if Trim(Copy(sTmp, 15, 255)) = 'METERS' then
        giUnits := 1;
      if Trim(Copy(sTmp, 15, 255)) = 'FEET' then
        giUnits := 2;
    end;

    if Trim(Copy(sTmp, 1, 14)) = 'SPHEROID' then
    begin
      if Trim(Copy(sTmp, 15, 255)) = 'CLARKE1866' then
        Spheroid := Clarke1866;
      if Trim(Copy(sTmp, 15, 255)) = 'WGS84' then
        Spheroid := WGS84;
    end;

    if (Trim(sTmp) <> '~') and (iParameter >= 0) and (iParameter <= 8) then
    begin
      sParams[iParameter] := sTmp;
      Inc(iParameter);
    end;

    if Trim(Copy(sTmp, 1, 14)) = 'PARAMETERS' then
      iParameter := 0;
  until gTextReader.EOT;


  case gDataProjection of
    dpLAMBERT:
    begin
      LambertInit(DecimalToEarthUnits(70),
        DecimalToEarthUnits(50),
        DecimalToEarthUnits(-100),
        DecimalToEarthUnits(31.10),
        Spheroid);
      //      giFalseEasting := EarthUnitsFrom( StrToFloat( Copy( sParams[4], 1, 12 )), Meter );
      //      giFalseNorthing := EarthUnitsFrom( StrToFloat( Copy( sParams[5], 1, 12 )), Meter );
    end;
    dpPOLAR:
      PolarInit(DecimalToEarthUnits(StrToInt(Trim(Copy(sParams[0], 1, 4)))),
        DecimalToEarthUnits(-90.0),
        1.0,
        Spheroid);
  end;
end;

{-------------------------------------------------------------------------}
procedure EarthImportE00(gLayer: TEarthLayer; const sFilename: string);
var
  cDecSep: char;
  sTmp: string;
  bReadPALData: boolean;
begin
  if gLayer = nil then
    exit;

  gLayer := gLayer;
  gEarth := gLayer.Earth;

  cDecSep := DecimalSeparator;
  Screen.Cursor := crHourGlass;
  DecimalSeparator := '.';
  gARCpolygon := TGeoDataObject.Create(nil);

  try
    frmE00Import := TfrmE00Import.Create(Application);
    if frmE00Import.ShowModal = mrCancel then
      Exit;

    frmE00Import.Visible := False;
    Application.ProcessMessages;

    gLayer.Name := ChangeFileExt(sFileName, '');
    //      gLayer.MaxFontHeight := 9;

    gEarth.ProgressMessage(pmSTART, 'ARC Import of ' + sFileName);

    gTextReader := TEarthTextReader.Create(sFilename);

    gEarth.ProgressMessage(pmMESSAGE, Format('%s: Searching for PRJ data...', [gLayer.Name]));



    while not gTextReader.EOT do
    begin
      gTextReader.ReadLn(sTmp);
      if (CompareText(sTmp, 'PRJ  2') = 0) then
      begin
        ReadPRJData;
        gTextReader.Position := 0;
        Break;
      end;
    end;

    bReadPALData := False;

    while not gTextReader.EOT do
    begin
      gTextReader.ReadLn(sTmp);

      if (CompareText(sTmp, 'ARC  2') = 0) then
        ReadARCData;
      if (CompareText(sTmp, 'CNT  2') = 0) then
        ReadCNTData;
      if (CompareText(sTmp, 'TX7  2') = 0) then
        ReadTX7Data;
      if (CompareText(sTmp, 'PAL  2') = 0) then
      begin
        ReadPALData;
        bReadPALData := True;
      end;
    end;
  finally
    if not bReadPALData then
      SplitGeoDataObject(gARCPolygon, gLayer);
    gARCPolygon.Free;

    frmE00Import.Free;

    Screen.Cursor := crDefault;

    gEarth.ProgressMessage(pmEND, 'E00 Import Finished');
  end;
end;

end.
