
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_EarthUtils;



interface

uses
  LCLIntf, LCLType, LMessages,
  Classes, SysUtils, Graphics, GIS_EarthBase, GIS_SysUtils,
  GIS_EarthObjects, GIS_Classes, Clipbrd, FileUtil, lazfileutils, Math;



function FindLayerFromObject(Earth : TCustomEarth; Obj : TEarthObject) : TEarthLayer;
function FindEarthObjectFromGroupsStore(Earth : TCustomEarth; aGroupsStore : TGroupsStore) : TGeoDataObject;
function FindEarthObjectFromPointStore(Earth : TCustomEarth; aPointStore : TPointStore) : TGeoDataObject;
function FindEarthObjectFromTitle(Earth : TCustomEarth;aTitle:string;Obj : TEarthObject) : TEarthObject;
function FindPresenterFromObject(Earth : TCustomEarth; Obj : TEarthObject) : TEarthPresenter;
//........ Layers File Functions ................
//
Function IsLayerValidFile(const sFilename : TFilename):boolean;

function LayerNewFromAnyFile(aLayerStore:TEarthLayerStore; const sFilename : TFilename) : integer;

function LayerFromResourceName(aLayerStore:TEarthLayerStore; const ResourceName : string) : TEarthLayer;
function LayerFromResourceID(aLayerStore:TEarthLayerStore; ResourceID : Integer) : TEarthLayer;

function LayerNewFromLYRfile(aLayerStore:TEarthLayerStore; const sFilename : TFilename ) :integer;
function LayerNewFromLYRStream(aLayerStore:TEarthLayerStore; aStream:Tstream ) :integer;
function LayerReadFromLYRfile(aLayer : TEarthLayer; const sFilename : TFilename ) : Boolean;
function LayerReadFromLYRStream(aLayer : TEarthLayer; aStream:Tstream ) : Boolean;

//........................................................
//

function EllipsoidDistanceLLToLL(const FromLL, ToLL : TPointLL; Spheroid : TSpheroid) : Double;
function DistanceLLToLL(const FromLL, ToLL : TPointLL) : Double;
function AngleLLToLL(const FromLL, ToLL : TPointLL) : Double;      //In Earth Units
Function AnglePointToPoint(const FromLL, ToLL : TPointLL):Extended; //In Degree
Function AngleDistToGCPoint(const ptLL : TPointLL; iAngle, iDistance : Integer) : TPointLL;

function  GreatCirclePoint(const FromLL, ToLL : TPointLL; alpha : Extended) : TPointLL;
procedure DrawProjectedMER(Earth : TEarthBase; const MER : TMER);

function  SplitGeoDataObject(APoly : TGeoDataObject; ALayer : TEarthLayer) : integer;
function  CombineGeoDataObjects(const Polys : array of TGeoDataObject; ALayer : TEarthLayer) : TGeoDataObject;
procedure ConcatPoints(ptStore1, ptStore2 : TPointStore);

procedure ZoomToLayers(Layers : TEarthLayerStore);
procedure ZoomToLayer(aLayer : TEarthLayer);
procedure ZoomToMER(Earth : TCustomEarth; MER : TMER);
procedure ZoomToObject(Earth : TCustomEarth; Obj : TEarthObject);
procedure ZoomToSelectedObjects(Earth : TCustomEarth);

function IsMERVisible(Earth : TCustomEarth; MER : TMER) : Boolean;
function IsObjectVisible(Earth : TCustomEarth; Obj : TEarthObject) : Boolean;
function IsObjectVisibleInRect(const Earth : TCustomEarth; const Obj : TEarthObject;const aRect : TRect) : boolean;
function SamePixelPos(Projection: TEarthProjection; const ptLL1, ptLL2 : TPointLL;Const RangeInPixel:Double): Boolean;

{------------------------------------------------------------------------------}
implementation
uses GIS_MapperLYR, GIS_MapperSHP, GIS_MapperMIF, GIS_MapperE002,
      sFileForGISLayer,sFileForGISObject,sFileForGISPresenter;

function FindPresenterFromObject(Earth : TCustomEarth; Obj : TEarthObject) : TEarthPresenter;
 var Lay:TEarthLayer;
begin
 result:=nil;
 Lay:=FindLayerFromObject(Earth,Obj);
 if Lay=nil then exit;
 result:=Lay.FindPresenter(Obj.PresenterID);
end;

function FindLayerFromObject(Earth : TCustomEarth; Obj : TEarthObject) : TEarthLayer;
var id1,id2: integer;
    Lay: TEarthLayer;
//..........................................
 Function FindAtObject(axobj:TEarthObject):TEarthLayer;
  var ix1,ix2:integer;
  begin
    Result:=nil;
    if axobj=nil then exit;
    for ix1 := 0 to axobj.Layers.Count - 1 do
     begin
       result:=axobj.Layers[ix1];
       if Result.Objects = Obj.Parent then Exit;

       for ix2 := 0 to axobj.Layers[ix1].Objects.Count - 1 do
        begin
          result:=FindAtObject(axobj.Layers[ix1].Objects[ix2]);
          if result<>nil then exit;
        end;

     end;
     Result:=nil;
  end;
 //.........................................
  Function FindAtLayer(axLay:TEarthLayer):TEarthLayer;
  var ix1,ix2:integer;
  begin
    Result:=nil;
    if axLay=nil then exit;
    for ix1 := 0 to axLay.Layers.Count - 1 do
     begin
       result:=axLay.Layers[ix1];
       if Result.Objects = Obj.Parent then Exit;

       for ix2 := 0 to axLay.Layers[ix1].Objects.Count - 1 do
        begin
          result:=FindAtObject(axLay.Layers[ix1].Objects[ix2]);
          if result<>nil then exit;
        end;

     end;
     Result:=nil;
  end;

 //.........................................
begin
 result:=nil;
 if Obj=nil then exit;
 if Earth=nil then exit;

  for id1 := 0 to Earth.Layers.count - 1 do
  begin
   Result:= Earth.Layers[id1];
   if Result.Objects = Obj.Parent then Exit;

   //Find at Layer Objects
   for id2 := 0 to Result.Objects.Count-1 do
    begin
         Lay:=nil;
         Lay :=FindAtObject(Result.Objects[id2]);
         if Lay<>nil then
          begin
            result:=lay;
            exit;
          end;
    end;
   //Find at Layer Layers
   for id2 := 0 to Result.Layers.Count-1 do
     begin
       Lay := FindAtLayer(Result.Layers[id2]);
       if Lay.Objects = Obj.Parent then
       begin
         result:=lay;
         exit;
        end;

     end;
   end;
  Result := nil;
end;
{-------------------------- FindEarthObjectFromTitle -------------}
function FindEarthObjectFromTitle(Earth : TCustomEarth;aTitle:string;Obj : TEarthObject) : TEarthObject;
var  idx1,idx2: integer;
begin
  Result := nil;
  for idx1 := 0 to Earth.Layers.count - 1 do    //layers
   for idx2 := 0 to Earth.Layers[idx1].Layers.count - 1 do    //layers.Layers
    begin
     result:=Earth.Layers[idx1].Layers[idx2].ObjectByTitle(aTitle,Obj);
     if result<>nil then  exit;
  end;
end;

function FindEarthObjectFromGroupsStore(Earth : TCustomEarth; aGroupsStore : TGroupsStore) : TGeoDataObject;
var idx1,idx2,idx3 : integer;
begin
  Result := nil;
  for idx1 := 0 to Earth.Layers.count - 1 do    //layers
   for idx2 := 0 to Earth.Layers[idx1].Layers.count - 1 do    //layers.Layers
    begin
     for idx3 := 0 to Earth.Layers[idx2].Layers[idx3].Objects.Count - 1 do  //objets
      begin
       if Earth.Layers[idx1].Layers[idx2].Objects[idx3]  is  TGeoDataObject then
           if TGeoDataObject(Earth.Layers[idx1].Layers[idx2].Objects[idx3]).PointsGroups=aGroupsStore then
             begin
               Result:=TGeoDataObject(Earth.Layers[idx1].Layers[idx2].Objects[idx3]);
               exit;
             end;
      end;
  end;
end;
//-------------------------------------------------------
function FindEarthObjectFromPointStore(Earth : TCustomEarth; aPointStore : TPointStore) : TGeoDataObject;
var
  idx1,idx2,idx3,idx4 : integer;
begin
  Result := nil;
  for idx1 := 0 to Earth.Layers.count - 1 do    //layers
  for idx2 := 0 to Earth.Layers[idx1].Layers.count - 1 do    //layers.Layers
  begin
     for idx3 := 0 to Earth.Layers[idx1].Layers[idx2].Objects.Count - 1 do  //objets
      begin
       if Earth.Layers[idx1].Layers[idx2].Objects[idx3]  is  TGeoDataObject then
          for idx4 := 0 to TGeoDataObject(Earth.Layers[idx1].Layers[idx2].Objects[idx3]).PointsGroups.Count - 1 do  //objets
            begin
              if TGeoDataObject(Earth.Layers[idx1].Layers[idx2].Objects[idx3]).PointsGroups[idx4]=aPointStore then
               begin
                Result:=TGeoDataObject(Earth.Layers[idx1].Layers[idx2].Objects[idx3]);
                exit;
              end;
            end;
      end;
  end;
end;
//============================ Layers file functions =====================================================
Function IsLayerValidFile(const sFilename : TFilename):boolean;
  var iext:string;
 begin
  iext:=ExtractFileExt(sFilename);

  if (SameText(iext,'.MIF')=true) or (SameText(iext,'.SHP')=true) or
     (SameText(iext,'.LYR')=true) or (SameText(iext,'.E00')=true) or
     (SameText(iext,'.LYR')=true) or (SameText(iext,'.GMF')=true) then
     result:=True;
 end;

function LayerFromObject(Earth : TCustomEarth; Obj : TEarthObject) : TEarthLayer;
var idx,idy : integer;
begin
 if Earth<>nil then
  for idx := 0 to Earth.Layers.count - 1 do
   for idy := 0 to Earth.Layers[idx].Layers.Count-1 do
   begin
    Result := Earth.Layers[idx].Layers[idy];
    if Result.Objects = Obj.Parent then  Exit;
   end;
  Result := nil;
end;

function LayerNewFromAnyFile(aLayerStore:TEarthLayerStore; const sFilename : TFilename) : integer;
var aLayer : TEarthLayer;
    iext:string;
begin
  Result:=0;
  aLayer:=nil;
  if aLayerStore=nil then exit;
  if FileExists(sFilename)=false then exit;
  iext:=ExtractFileExt(sFilename);

 if (Sametext(iext,'.GLF'))  then
    begin
      Result:=Load_GISLayersFromFile(aLayerStore,sFilename);
    end else
   if (Sametext(iext, '.LYR')) or
      (Sametext(iext, '.DAT')) then   // By Sternas Stefanos
    begin
      aLayer := TEarthLayer.Create(aLayerStore);
      aLayer.Objects := TEarthLYRReader.Create(aLayer);
      TEarthLYRReader(aLayer.Objects).Filename := sFilename;
      aLayer.FullFileName:=sFilename;
      aLayer.Objects.Active := True;
      Result:=1;
    end else
      if Sametext(iext, '.SHP') then
      begin
        aLayer := TEarthLayer.Create(aLayerStore);
        aLayer.Objects := TEarthSHPReader.Create(aLayer);
        TEarthSHPReader(aLayer.Objects).Filename := sFilename;
        aLayer.FullFileName:=sFilename;
        aLayer.Objects.Active := True;
        Result:=1;
      end else
        if Sametext(iext, '.MIF') then
        begin
          aLayer := TEarthLayer.Create(aLayerStore);
          aLayer.Objects := TEarthMIFReader.Create(aLayer);
          TEarthMIFReader(aLayer.Objects).Filename :=sFilename;
          aLayer.FullFileName:=sFilename;
          aLayer.Objects.Active := True;
          Result:=1;
        end else
         if Sametext(iext, '.E00') then
          begin
            aLayer := TEarthLayer.Create(aLayerStore);
            EarthImportE00(aLayer, sFilename);
            aLayer.FullFileName:=sFilename;
            aLayer.Objects.Active := True;
            Result:=1;
          end;
end;

function LayerFromResourceName(aLayerStore:TEarthLayerStore; const ResourceName : string) : TEarthLayer;
var
  aLayer : TEarthLayer;
  Stream : TCustomMemoryStream;
begin
  Result := nil;
  Stream := TResourceStream.Create(hInstance, ResourceName, RT_RCDATA);
  if Stream = nil then exit;

  try
    aLayer := TEarthLayer.Create(aLayerStore);
    aLayer.Name := ResourceName;
    aLayer.Objects := TEarthLYRStreamReader.Create(aLayer);
    aLayer.Objects.NoCache := true;  // disable caching of objects
    TEarthLYRStreamReader(aLayer.Objects).LYRStream := Stream;

    aLayer.FullFileName:=''; //Reset File Path

    aLayer.Objects.Active := True;
    Result := aLayer;
  finally
    Stream.Free;
  end;
end;

function LayerFromResourceID(aLayerStore:TEarthLayerStore; ResourceID : Integer) : TEarthLayer;
var
  aLayer : TEarthLayer;
  Stream : TCustomMemoryStream;
begin
  Result := nil;
  Stream := TResourceStream.CreateFromID(hInstance, ResourceID, RT_RCDATA);
  if Stream = nil then exit;

  try
    aLayer := TEarthLayer.Create(aLayerStore);
    aLayer.Name := IntToStr( ResourceID );
    aLayer.Objects := TEarthLYRStreamReader.Create(aLayer);
    aLayer.Objects.NoCache := true;  // disable caching of objects
    TEarthLYRStreamReader(aLayer.Objects).LYRStream := Stream;

    aLayer.FullFileName:='';
    aLayer.Objects.Active := True;
    Result := aLayer;
  finally
    Stream.Free;
  end;
end;


//........... Old LYR file ...............
function LayerNewFromLYRStream(aLayerStore:TEarthLayerStore; aStream:Tstream ) :integer;
  var ler:TEarthLayer;
begin
  result:=-1;
 if aLayerStore=nil then exit;
 if aStream=nil then exit;
  try
    aStream.Position:=0;
    ler := TEarthLayer.Create(aLayerStore);
    if LayerReadFromLYRStream(ler,aStream) then result:=ler.Index;
  finally
  end;
end;

function LayerNewFromLYRfile(aLayerStore:TEarthLayerStore; const sFilename : TFilename ) :integer;
var ler:TEarthLayer;
 begin
    result:=-1;
    if aLayerStore=nil then exit;
    if FileExists(aLayerStore.Earth.ResolveFilename(sFilename))=false then exit;
    try
      ler := TEarthLayer.Create(aLayerStore);
      if LayerReadFromLYRFile(ler,sFilename) then result:=ler.Index;
  finally
  end;
 end;


function LayerReadFromLYRStream(aLayer : TEarthLayer; aStream:Tstream ) : Boolean;
var ler:TEarthLayer;
begin
  result:=false;
 if aLayer=nil then exit;
 if aStream=nil then exit;
  try
    aStream.Position:=0;
    ler := aLayer;
    ler.Objects := TEarthLYRStreamReader.Create(ler);
    ler.Objects.NoCache := true;  // disable caching of objects
    TEarthLYRStreamReader(ler.Objects).LYRStream := aStream;
    ler.Objects.Name:='NewLayer'+inttostr(ler.Index);
  //  TEarthLYRStreamReader(ler.Objects).Filename :=ler.Objects.Name;
    ler.FullFileName:=''; //Reset File Path
    ler.Objects.Active := True;
    result:=true;
  finally
  end;
end;

function LayerReadFromLYRfile(aLayer : TEarthLayer; const sFilename : TFilename ) : Boolean;
var ler:TEarthLayer;
 begin
    result:=false;
    if aLayer=nil then exit;
    if FileExists(aLayer.Earth.ResolveFilename(sFilename))=false then exit;
    try
      ler := aLayer;
      ler.Objects := TEarthLYRReader.Create(ler);
      ler.Objects.NoCache := true;  // disable caching of objects
      TEarthLYRReader(ler.Objects).Filename := sFilename;
     // TEarthLYRReader(ler.Objects).Filename :=ler.Objects.Name;
      ler.FullFileName:=sFilename;
      ler.Objects.Active := True;
      result:=true;
  finally
  end;
 end;

//==========================================================================

function AngleDistToGCPoint(const ptLL : TPointLL; iAngle, iDistance : integer) : TPointLL;
var
  sinlat1, coslat1,
    sind, cosd, sintc, costc,
    dlon, lat, lon : extended;
begin
  with PtLL do
  begin
    SinCos(ilatY * GU_TORADIANS, sinlat1, coslat1);
    SinCos(iDistance / EARTHRADIUS, sind, cosd);
    SinCos(iAngle * GU_TORADIANS, sintc, costc);
    lat := arcsin(sinlat1 * cosd + coslat1 * sind * costc);
    dlon := arctan2(sintc * sind * coslat1, cosd - sinlat1 * sin(lat));
    lon := SphericalMod(ilongX * GU_TORADIANS - dlon + LocalPI) - LocalPI;
  end;
  Result := PointLL(Round(lon * GU_FROMRADIANS), Round(lat * GU_FROMRADIANS));
end;

function GreatCirclePoint(const FromLL, ToLL : TPointLL;
  alpha : Extended) : TPointLL;
var
  a, b : TV3D;
  mat : TGMatrix;
begin
  a := PointLLToV3D(FromLL);
  b := PointLLToV3D(ToLL);
  QuatToMatrix(AxisAngleToQuat(V3DCross(a, b),
    -ArcCos(V3DDot(a, b)) * alpha), mat);

  with V3DToPointLL(V3DMatrixMul(mat, a)) do
    Result := PointLL(iLongX, iLatY);
end;

function AngleLLToLL(const FromLL, ToLL : TPointLL) : Double;
var
  SLat1, SLat2, CLat1, CLat2 : Extended;
  SLonDiff, CLonDiff : Extended;
begin
  SinCos(FromLL.iLatY * GU_TORADIANS, SLat1, CLat1);
  SinCos(ToLL.iLatY * GU_TORADIANS, SLat2, CLat2);
  SinCos((FromLL.iLongX - ToLL.iLongX) * GU_TORADIANS, SLonDiff, CLonDiff);

  result := (SphericalMod(ArcTan2(SLonDiff * CLat2, CLat1 * SLat2 - SLat1 * CLat2 * CLonDiff)) * GU_FROMRADIANS);

end;

Function AnglePointToPoint(const FromLL, ToLL : TPointLL):Extended;
 begin
  result := EarthUnitsTo(AngleLLToLL(FromLL,ToLL),euDegree);

   if result<=0 then
     result:=-result else
     result :=360-result;

 end;

function EllipsoidDistanceLLToLL(const FromLL, ToLL : TPointLL; Spheroid : TSpheroid) : Double;
var
  C, c_value_1, c_value_2, c2a, cy, cz : Extended;
  D, E, r_value : Extended;
  S, s_value_1, sA, sy : Extended;
  tangent_1, tangent_2, X, Y : Extended;
  Heading_FromTo, Heading_ToFrom : Extended;
  term1, term2 : Extended;
  Flattening : Extended;
begin
  Result := 0;

  if (FromLL.iLongX - ToLL.iLongX = 0) and (FromLL.iLatY - ToLL.iLatY = 0) then
    Exit;

  Flattening := SpheroidData[Ord(Spheroid)].f;

  r_value := 1.0 - Flattening;
  tangent_1 := (r_value * Sin(FromLL.iLatY * GU_TORADIANS)) / Cos(FromLL.iLatY * GU_TORADIANS);
  tangent_2 := (r_value * Sin(ToLL.iLatY * GU_TORADIANS)) / Cos(ToLL.iLatY * GU_TORADIANS);
  c_value_1 := 1.0 / Sqrt((tangent_1 * tangent_1) + 1.0);
  s_value_1 := c_value_1 * tangent_1;
  c_value_2 := 1.0 / Sqrt((tangent_2 * tangent_2) + 1.0);
  S := c_value_1 * c_value_2;

  Heading_ToFrom := S * tangent_2; { backward_azimuth }
  Heading_FromTo := Heading_ToFrom * tangent_1;

  X := ToLL.iLongX * GU_TORADIANS - FromLL.iLongX * GU_TORADIANS;

  repeat
    tangent_1 := c_value_2 * Sin(X);
    tangent_2 := Heading_ToFrom - (s_value_1 * c_value_2 * Cos(X));
    sy := Sqrt((tangent_1 * tangent_1) + (tangent_2 * tangent_2));
    cy := (S * Cos(X)) + Heading_FromTo;
    Y := ArcTan2(sy, cy);
    sA := (S * Sin(X)) / sy;
    c2a := (-sA * sA) + 1.0;
    cz := Heading_FromTo + Heading_FromTo;

    if c2a > 0.0 then
      cz := (-cz / c2a) + cy;

    E := (cz * cz * 2.0) - 1.0;
    C := (((((-3.0 * c2a) + 4.0) * Flattening) + 4.0) * c2a * Flattening) / 16.0;
    D := X;
    X := ((((E * cy * C) + cz) * sy * C) + Y) * sA;
    X := ((1.0 - C) * X * Flattening) + ToLL.iLongX * GU_TORADIANS - FromLL.iLongX * GU_TORADIANS;
  until Abs(D - X) < 5.0E20;

  X := Sqrt((((1.0 / r_value / r_value) - 1) * c2a) + 1.0) + 1.0;
  X := (X - 2.0) / X;
  C := 1.0 - X;
  C := (((X * X) * 0.25) + 1.0) / C;
  D := ((0.375 * (X * X)) - 1.0) * X;
  X := X * cy;

  S := (1.0 - E) - E;

  term1 := ((sy * sy * 4.0) - 3.0) * (((S * cz * D) / 6.0) - X);
  term2 := ((((term1 * D) * 0.25) + cz) * sy * D) + Y;

  Result := (term2 * C * SpheroidData[Ord(Spheroid)].a * r_value);
end;

function DistanceLLToLL(const FromLL, ToLL : TPointLL) : Double;
var
  dist, latFrom, LatTo, lonDist : Extended;
begin
  lonDist := FromLL.iLongX * GU_TORADIANS - ToLL.iLongX * GU_TORADIANS;
  latFrom := FromLL.iLatY * GU_TORADIANS;
  LatTo := ToLL.iLatY * GU_TORADIANS;

  dist := 2 * ArcSin(Sqrt((Sqr(Sin((latFrom - LatTo) * 0.5)))
    + Cos(latFrom) * Cos(LatTo) * Sqr(Sin(lonDist * 0.5))));

  Result := (dist * GU_FROMRADIANS);
end;


function SplitGeoDataObject(APoly : TGeoDataObject; ALayer : TEarthLayer) : integer;
var
  idx : integer;
begin
  ALayer.Objects.Capacity := ALayer.Objects.Count + APoly.PointsGroups.Count;

  Result := APoly.PointsGroups.Count;
  for idx := 0 to Result - 1 do
    with TGeoDataObject.Create(ALayer.Objects) do
    begin
      Title := APoly.Title;
      PointsGroups.Count := 1;
      PointsGroups[0] := APoly.PointsGroups[idx].Clone;
      APoly.PointsGroups[idx] := nil;
    end;
  APoly.PointsGroups.Count := 0;
end;

function CombineGeoDataObjects(const Polys : array of TGeoDataObject; ALayer : TEarthLayer) : TGeoDataObject;
var
  idx, jdx, iPointsGroups : integer;
begin
  Result := TGeoDataObject.Create(ALayer.Objects);
  Result.Title := 'New Object';

  iPointsGroups := 0;
  for idx := 0 to High(Polys) do
    Inc(iPointsGroups, Polys[idx].PointsGroups.Count);

  Result.PointsGroups.Count := iPointsGroups;

  iPointsGroups := 0;
  for idx := 0 to High(Polys) do
  begin
    for jdx := 0 to Polys[idx].PointsGroups.Count - 1 do
    begin
      Result.PointsGroups[iPointsGroups] := Polys[idx].PointsGroups[jdx].Clone;
      //      Polys[idx].PointsGroups[jdx] := nil;
      Inc(iPointsGroups);
    end;
  end;
end;

procedure ConcatPoints(ptStore1, ptStore2 : TPointStore);
var
  idx : integer;
begin
  for idx := 0 to ptStore2.Count - 1 do
    ptStore1.Add(ptStore2[idx]);
end;

procedure DrawProjectedMER(Earth : TEarthBase; const MER : TMER);
begin
  // display the bounding rectangle
  with MER do
  begin
    Earth.EarthCanvas.RenderLine(PointLL(iLongX, iLatY), PointLL(iLongX + iWidthX, iLatY), 32);
    Earth.EarthCanvas.RenderLine(PointLL(iLongX + iWidthX, iLatY), PointLL(iLongX + iWidthX, iLatY + iHeightY), 32);
    Earth.EarthCanvas.RenderLine(PointLL(iLongX + iWidthX, iLatY + iHeightY), PointLL(iLongX, iLatY + iHeightY), 32);
    Earth.EarthCanvas.RenderLine(PointLL(iLongX, iLatY + iHeightY), PointLL(iLongX, iLatY), 32);
  end;
end;

procedure ZoomToMER(Earth : TCustomEarth; MER : TMER);
begin
  Earth.Projection.CenterXY := PointDouble(Earth.Width/2, Earth.Height/2);
  with MER do
  begin
    if (iWidthX <> 0) and (iHeightY <> 0) then
      Earth.ViewRect := RectD(0, 0, iWidthX, iHeightY);

    Earth.LocateToLL(iLongX + iWidthX / 2, iLatY + iHeightY / 2);
  end;
end;

procedure ZoomToSelectedObjects(Earth : TCustomEarth);
var
  idx : Integer;
  MER : TMER;
begin
  MER := Earth.SelectedObjects[0].ObjectMER;

  if Earth.SelectedObjectsCount > 1 then
    for idx := 1 to Earth.SelectedObjectsCount - 1 do
      MER := UnionMER(MER, Earth.SelectedObjects[idx].ObjectMER);

  ZoomToMER(Earth, MER);
end;

procedure ZoomToLayers(Layers : TEarthLayerStore);
var
  idx : Integer;
  MER : TMER;
begin
  if Layers.Count = 0 then Exit;

  MER := Layers[0].LayerMER;

  for idx := 1 to Layers.Count - 1 do
    MER := UnionMER(MER, Layers[idx].LayerMER);

  ZoomToMER(Layers.Earth, MER);
end;

procedure ZoomToLayer(aLayer : TEarthLayer);
begin
  ZoomToMER(aLayer.Earth, aLayer.LayerMER);
end;

procedure ZoomToObject(Earth : TCustomEarth; Obj : TEarthObject);
begin
  ZoomToMER(Earth, Obj.ObjectMER);
end;

function IsMERVisible(Earth : TCustomEarth; MER : TMER) : Boolean;
var
  aState : TEarthObjectStateSet;
begin
  Result := Earth.Projection.ProjectionModel.VisibleMER(MER, aState);
end;

function IsObjectVisible(Earth : TCustomEarth; Obj : TEarthObject) : Boolean;
begin
  Result := IsObjectVisibleInRect( Earth, Obj, Earth.ClientRect );
end;

function IsObjectVisibleInRect(const Earth : TCustomEarth; const Obj : TEarthObject;
  const aRect : TRect) : boolean;
var
  iChain, idx : integer;
  ptXY : TPointDouble;
  ptLL : TPointLL;
begin
  Result := IsMERVisible(Earth, Obj.ObjectMER);

  if Result then
  begin
    if (Obj is TGeoDataObject) and (TGeoDataObject(Obj).PointsGroups.Count > 0) then
    begin
      // Check all the points in a TGeoDataObject
      with TGeoDataObject(Obj) do
        for iChain := 0 to PointsGroups.count - 1 do
          for idx := 0 to PointsGroups[iChain].Count - 1 do
            if Earth.Projection.PointLLToXY(PointsGroups[iChain][idx], ptXY) then
              if (ptXY.X >= aRect.Left) and (ptXY.X < aRect.Right) then
                if (ptXY.Y >= aRect.Top) and (ptXY.Y < aRect.Bottom) then
                  Exit;
    end
    else // Check the objects Centroid
      if Earth.Projection.PointLLToXY(Obj.Centroid, ptXY) then
        if (ptXY.X >= aRect.Left) and (ptXY.X < aRect.Right) then
          if (ptXY.Y >= aRect.Top) and (ptXY.Y < aRect.Bottom) then
            Exit;

    with Earth.Projection do
      DeviceXYToLL((aRect.Left + aRect.Right)/ 2, (aRect.Top + aRect.Bottom) / 2, ptLL);

    if Obj.LLInObject(ptLL, 0) then Exit;
  end;
  Result := False;
end;

function SamePixelPos(Projection: TEarthProjection; const ptLL1, ptLL2 : TPointLL;Const RangeInPixel:Double): Boolean;
var  pt1, pt2: TPointDouble;
begin
  Projection.LLToDeviceXY(ptLL1.iLongX, ptLL1.iLatY, pt1);
  Projection.LLToDeviceXY(ptLL2.iLongX, ptLL2.iLatY, pt2);
  Result:= (abs(pt1.x-pt2.x)<=RangeInPixel) and (abs(pt1.Y-pt2.Y)<=RangeInPixel);
end;


end.

