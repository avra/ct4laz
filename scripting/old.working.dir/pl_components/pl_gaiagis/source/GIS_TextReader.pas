
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_TextReader;



interface

uses
  SysUtils, Classes, GIS_SysUtils, GIS_MMStream;

const
  DEFAULT_BUF_SIZE = 4096;

type
  TEarthTextReader = class(TEarthRoot)
  private
    FbEOT : Boolean;
    FTextBufferSize : integer;
    FDataStream: TGFileMapStream;
    BufEnd, BufPos: integer;
    FiPosition : integer;
    FiLineStartPosition : integer;

    function GetSize : integer;
    procedure SetPosition(iPosition: integer);
    procedure SetTextBufferSize( Value : integer );
  public
    TextBuffer: PChar;
    constructor Create(const FileName: TFilename);
    destructor Destroy; override;

    procedure ReadLn(var Line: string);

    property DataStream: TGFileMapStream read FDataStream;
    property EOT: Boolean read FbEOT;
    property LineStartPosition: integer read FiLineStartPosition;
    property Position: integer read FiPosition write SetPosition;
    property Size : integer read GetSize;
    property TextBufferSize : integer read FTextBufferSize write SetTextBufferSize;
  end;

implementation

constructor TEarthTextReader.Create(const FileName: TFilename);
begin
  inherited Create;

  FDataStream := TGFileMapStream.Create(FileName);

  SetTextBufferSize( DEFAULT_BUF_SIZE );
end;

procedure TEarthTextReader.SetTextBufferSize(Value: integer);
begin
  if FTextBufferSize <> Value then
  begin
    FTextBufferSize := Value;

    FreeMem( TextBuffer );
    TextBuffer := AllocMem( FTextBufferSize + 1 );
    FiPosition := MaxInt;
    SetPosition( 0 );
  end;
end;

destructor TEarthTextReader.Destroy;
begin
  DataStream.Free;
  FDataStream := nil;

  FreeMem( TextBuffer );

  inherited Destroy;
end;

function TEarthTextReader.GetSize : integer;
begin
  Result := DataStream.Size;
end;

procedure TEarthTextReader.SetPosition(iPosition: integer);
begin
  if ( FiPosition div FTextBufferSize ) <> ( iPosition div FTextBufferSize ) then
  begin
    DataStream.Position := ( iPosition div FTextBufferSize ) * FTextBufferSize;

    BufEnd := DataStream.Read(TextBuffer^, FTextBufferSize);
  end;

  BufPos := iPosition mod FTextBufferSize;
  FbEOT := BufEnd < BufPos;
  FiPosition := iPosition;
end;

procedure TEarthTextReader.ReadLn(var Line: string);
var
  P, Start: PChar;
  S: string;
begin
  Line := '';

  FiLineStartPosition := FiPosition; // remember start of this line
  repeat
    P := TextBuffer;
    Inc(P, BufPos);
    Start := P;

    while not (P^ in [#0, #10, #13]) do
      Inc(P);

    SetString(S, Start, P - Start);

    if P^ = #13 then
      Inc(P);
    Inc( FiPosition, P - Start );

    if P^ <> #0 then // if we have not hit the end of the Buffer
      Break;

    Line := Line + S;
    BufEnd := DataStream.Read(TextBuffer^, FTextBufferSize);
    TextBuffer[BufEnd] := #0;
    FbEOT := BufEnd = 0;
    BufPos := 0;
  until FbEOT;

  if P^ = #10 then
  begin
//    Inc(P);
    Position := Position + 1;
  end;
//  Inc( BufPos, P - Start );

  Line := Line + S;
end;

end.

