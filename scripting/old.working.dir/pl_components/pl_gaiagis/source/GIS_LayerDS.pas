
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_LayerDS;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  SysUtils, Forms, DB, Classes, GIS_EarthBase, GIS_Classes,
  Dialogs;

type

  TBookMarkInfo = record
    BookmarkRecNo : integer;
    BookmarkFlag : TBookmarkFlag;
  end;
  PTBookMarkInfo = ^TBookMarkInfo;

  TEarthRecord = record
    Title : shortstring;
    PresenterID : Integer;
    ID : Integer;
    Index : Integer;
    Selected : Boolean;
    Hidden : Boolean;
  end;
  PTEarthRecord = ^TEarthRecord;

  TEarthLayerSort = ( lsIndex, lsTitle );

  TEarthLayerDS = class(TDataSet)
  private
    FiCurRec : Integer;
    FEarth : TEarthBase;
    FLayer : TEarthLayer;
    FLayerName : string;
    FLayerSort : TEarthLayerSort;
    FSortedList : TList;

    procedure SetLayerSort( Value : TEarthLayerSort );
    procedure SetLayerName( sValue : string );
    procedure SetLayer( ALayer : TEarthLayer );
    procedure SetEarth( AEarth : TEarthBase );
    procedure SortLayer;
  protected
 { Overriden abstract methods (required) }
    function AllocRecordBuffer : PChar; override;
    procedure FreeRecordBuffer( var Buffer : PChar ); override;
    function IsCursorOpen : Boolean; override;

    procedure GetBookmarkData( Buffer : PChar; Data : Pointer ); override;
    procedure SetBookmarkData( Buffer : PChar; Data : Pointer ); override;
    function  GetBookmarkFlag( Buffer : PChar ) : TBookmarkFlag; override;
    procedure SetBookmarkFlag( Buffer : PChar; Value : TBookmarkFlag ); override;

    procedure InternalAddRecord( Buffer : Pointer; Append : Boolean ); override;
    procedure InternalClose; override;
    procedure InternalDelete; override;
    procedure InternalFirst; override;
    procedure InternalGotoBookmark( Bookmark : Pointer ); override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord( Buffer : PChar ); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalPost; override;
    procedure InternalSetToRecord( Buffer : PChar ); override;
    procedure xNotification( AComponent : TComponent; Operation : TOperation );

  public
    constructor Create( AOwner : TComponent ); override;
    function  GetFieldData( Field : TField; Buffer : Pointer ) : Boolean; override;
    procedure SetFieldData( Field : TField; Buffer : Pointer ); override;
    function  GetRecord( Buffer : PChar; GetMode : TGetMode; DoCheck : Boolean ) : TGetResult; override;
    function  GetRecordSize : Word; override;
    function  GetRecordCount : Integer; override;
    function  GetRecNo : Integer; override;
    procedure SetRecNo( Value : Integer ); override;
    property  Layer : TEarthLayer read FLayer write SetLayer;
  published
    property Earth : TEarthBase read FEarth write SetEarth;
    property LayerName : string read FLayerName write SetLayerName;
    property SortColumn : TEarthLayerSort read FLayerSort write SetLayerSort;
    property Active;
    property CanModify;
 //   property DataSetField;
    property DataSource;
    property DefaultFields;
    property FieldCount;
    property FieldDefs;
 //   property FieldDefList;
    property Fields;
//    property FieldList;
    property IsUniDirectional;
 //   property ObjectView;
    property State;
    property Filter;  
    property FilterOptions;
    property AutoCalcFields;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
    property BeforeRefresh;
    property AfterRefresh;
    property OnCalcFields;
    property OnDeleteError;
    property OnEditError;
    property OnFilterRecord;
    property OnNewRecord;
    property OnPostError;
  end;

{------------------------------------------------------------------------------}
implementation

{------------------------------------------------------------------------------
  TEarthLayerDS.Create
------------------------------------------------------------------------------}
constructor TEarthLayerDS.Create( AOwner : TComponent );
begin
  inherited Create( AOwner );

  FEarth := nil;
  FLayer := nil;
  FLayerName := '';
  FSortedList := nil;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.SetEarth
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetEarth( AEarth : TEarthBase );
begin
  if FEarth <> AEarth then
  begin
    Active := False;
    if FEarth <> nil then
      FEarth.FreeNotification( Self );
    FEarth := AEarth;
    Layer := nil;
    FLayerName := '';
  end;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.SetLayer
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetLayer( ALayer : TEarthLayer );
begin
  if FLayer <> ALayer then
  begin
    FiCurRec := -1;
    FLayer := ALayer;
    if FLayer = nil then
      Active := False
    else
    begin
      FLayerName := FLayer.Name;
      FEarth := TEarthBase( FLayer.Earth );
      SortLayer;
    end;
  end;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.xNotification
------------------------------------------------------------------------------}
procedure TEarthLayerDS.xNotification( AComponent : TComponent; Operation : TOperation );
begin
  inherited Notification( AComponent, Operation );

  if ( Operation = opRemove ) and ( AComponent = FEarth ) then
    FEarth := nil;
end;

{------------------------------------------------------------------------------
  TitleCompare
------------------------------------------------------------------------------}
function TitleCompare( Item1, Item2 : Pointer ) : Integer;
begin
  Result := CompareText( TEarthObject( Item1 ).Title, TEarthObject( Item2 ).Title );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.SortLayer
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SortLayer;
var
  idx : integer;
begin
  if FLayer = nil then
    Exit;
  if FLayerSort = lsIndex then
    FSortedList := nil
  else
    FSortedList := TList.Create;

  if FSortedList <> nil then
  begin
    FSortedList.Capacity := FLayer.Objects.Count;
    for idx := 0 to FLayer.Objects.Count - 1 do
      FSortedList.Add( FLayer.Objects[idx] );

    FSortedList.Sort( TitleCompare );
  end;
  if not IsEmpty then
    Resync( [] );
end;

procedure TEarthLayerDS.SetLayerName( sValue : string );
begin
  FLayerName := sValue;
  if FEarth <> nil then
    with FEarth do
      SetLayer( Layers[Layers.IndexByName( sValue )]);
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.SetLayerSort
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetLayerSort( Value : TEarthLayerSort );
begin
  if FLayerSort <> Value then
  begin
    FSortedList.Free;
    FLayerSort := Value;
    SortLayer;
  end;
end;

procedure TEarthLayerDS.InternalOpen;
begin
  FiCurRec := -1;

  if ( FEarth <> nil ) and ( FLayer = nil ) then
    with FEarth do
      SetLayer( Layers[Layers.IndexByName( FLayerName )]);

  if FLayer = nil then
    raise Exception.Create( 'Missing LayerName property' );

  BookmarkSize := SizeOf( TBookmarkInfo );
  InternalInitFieldDefs;
  if DefaultFields then
    CreateFields;
  BindFields( True );
end;

procedure TEarthLayerDS.InternalClose;
begin
  BindFields( False );
  if DefaultFields then
    DestroyFields;
  FiCurRec := -1;
  Flayer := nil;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.IsCursorOpen
------------------------------------------------------------------------------}
function TEarthLayerDS.IsCursorOpen : Boolean;
begin
  Result := FLayer <> nil;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalHandleException
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalHandleException;
begin
  Application.HandleException( Self );
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.InternalGotoBookmark
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalGotoBookmark( Bookmark : Pointer );
begin
  FiCurRec := PTBookMarkInfo( Bookmark )^.BookmarkRecNo;
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.InternalSetToRecord
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalSetToRecord( Buffer : PChar );
begin
  InternalGotoBookmark( PTBookmarkInfo( Buffer + SizeOf( TEarthRecord ) ) );
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.GetBookmarkFlag
------------------------------------------------------------------------------}
function TEarthLayerDS.GetBookmarkFlag( Buffer : PChar ) : TBookmarkFlag;
begin
  Result := PTBookMarkInfo( Buffer + SizeOf( TEarthRecord ) )^.BookmarkFlag;
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.GetBookmarkData
------------------------------------------------------------------------------}
procedure TEarthLayerDS.GetBookmarkData( Buffer : PChar; Data : Pointer );
begin
  PInteger( Data )^ := Integer( PTBookmarkInfo( Buffer + SizeOf( TEarthRecord ) )^.BookmarkRecNo );
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.SetBookmarkData
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetBookmarkData( Buffer : PChar; Data : Pointer );
begin
  PTBookmarkInfo( Buffer + SizeOf( TEarthRecord ) )^.BookmarkRecNo := PInteger( Data )^;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.SetBookmarkFlag
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetBookmarkFlag( Buffer : PChar; Value : TBookmarkFlag );
begin
  PTBookMarkInfo( Buffer + SizeOf( TEarthRecord ) )^.BookmarkFlag := Value;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.GetRecordSize
------------------------------------------------------------------------------}
function TEarthLayerDS.GetRecordSize : Word;
begin
  Result := SizeOf( TEarthRecord );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.AllocRecordBuffer
------------------------------------------------------------------------------}
function TEarthLayerDS.AllocRecordBuffer : PChar;
begin
  GetMem( Result, SizeOf( TEarthRecord ) + SizeOf( TBookMarkInfo ) );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.FreeRecordBuffer
------------------------------------------------------------------------------}
procedure TEarthLayerDS.FreeRecordBuffer( var Buffer : PChar );
begin
  FreeMem( Buffer, SizeOf( TEarthRecord ) + SizeOf( TBookMarkInfo ) );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.GetRecord
------------------------------------------------------------------------------}
function TEarthLayerDS.GetRecord( Buffer : PChar; GetMode : TGetMode;
  DoCheck : Boolean ) : TGetResult;
var
  EarthObject : TEarthObject;
begin
  if FLayer.Objects.Count < 1 then
    Result := grEOF
  else
  begin
    Result := grOK;
    case GetMode of
      gmNext :
        if FiCurRec >= FLayer.Objects.Count - 1 then
          Result := grEOF
        else
          Inc( FiCurRec );
      gmPrior :
        if FiCurRec <= 0 then
          Result := grBOF
        else
          Dec( FiCurRec );
      gmCurrent :
        if ( FiCurRec < 0 ) or ( FiCurRec >= FLayer.Objects.Count ) then
          Result := grError;
    end;

    if Result = grOK then
    begin
      if FSortedList <> nil then
        EarthObject := TEarthObject( FSortedList[FiCurRec] )
      else
        EarthObject := FLayer.objects[FiCurRec];
      with PTEarthRecord( Buffer )^ do
      begin
        Index := EarthObject.Index;
        Title := EarthObject.Title;
        PresenterID := EarthObject.PresenterID;
        ID := EarthObject.ID;
        Selected := EarthObject.Selected;
        Hidden := EarthObject.Hidden;
      end;
      with PTBookMarkInfo( Buffer + SizeOf( TEarthRecord ) )^ do
      begin
        BookMarkRecNo := FiCurRec;
        BookmarkFlag := bfCurrent;
      end;
    end
    else
      if ( Result = grError ) and DoCheck then
        DatabaseError( 'No Records' );
  end;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalInitRecord
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalInitRecord( Buffer : PChar );
begin
  if Buffer = nil then Exit;
  FillChar( Buffer^, SizeOf( TEarthRecord ), 0 );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalInitFieldDefs
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalInitFieldDefs;
begin
  FieldDefs.Clear;
  TFieldDef.Create( FieldDefs, 'Index', ftInteger, 0, True, 1 );
  TFieldDef.Create( FieldDefs, 'Title', ftString, 32, False, 2 );
  TFieldDef.Create( FieldDefs, 'PresenterID', ftInteger, 0, False, 3 );
  TFieldDef.Create( FieldDefs, 'ID', ftInteger, 0, False, 4 );
  TFieldDef.Create( FieldDefs, 'Hidden', ftBoolean, 0, False, 5 );
  TFieldDef.Create( FieldDefs, 'Selected', ftBoolean, 0, False, 6 );
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.GetFieldData
------------------------------------------------------------------------------}
function TEarthLayerDS.GetFieldData( Field : TField; Buffer : Pointer ) : Boolean;
begin
  Result := not IsEmpty;
  if ( not isCursorOpen ) or ( Buffer = nil ) then Exit;

  with PTEarthRecord( ActiveBuffer )^ do
    case Field.FieldNo of
      1 : Move( Index, Buffer^, Field.DataSize );
      2 : StrPCopy( PChar( Buffer ), Title );
      3 : Move( PresenterID, Buffer^, Field.DataSize );
      4 : Move( ID, Buffer^, Field.DataSize );
      5 : Move( Hidden, Buffer^, Field.DataSize );
      6 : Move( Selected, Buffer^, Field.DataSize );
    end;
  Result := True;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.SetFieldData
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetFieldData( Field : TField; Buffer : Pointer );
begin
  if Buffer = nil then Exit;

  with PTEarthRecord( ActiveBuffer )^ do
    case Field.FieldNo of
      1 : Index := PInteger( Buffer )^;
      2 : Title := string( PChar( Buffer ) );
      3 : PresenterID := PInteger( Buffer )^;
      4 : ID := PInteger( Buffer )^;
      5 : Hidden := Boolean( PInteger( Buffer )^ );
      6 : Selected := Boolean( PInteger( Buffer )^ );
    end;
  DataEvent( deFieldChange, Longint( Field ) );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalFirst
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalFirst;
begin
  FiCurRec := -1;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalLast
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalLast;
begin
  if FLayer <> nil then
    FiCurRec := FLayer.Objects.Count
  else
    FiCurRec := -1;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalPost
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalPost;
var
  EarthObject : TEarthObject;
  iTmpIndex : integer;
begin
  EarthObject := nil;

  if State = dsEdit then
    if FSortedList <> nil then
      EarthObject := TEarthObject( FSortedList[FiCurRec] )
    else
      EarthObject := FLayer.Objects[FiCurRec];

  if EarthObject = nil then
    raise Exception.Create( 'Insert or Append not supported in TEarthLayerDS' );

  with PTEarthRecord( ActiveBuffer )^ do
  begin
    EarthObject.Title := Title;
    EarthObject.PresenterID := PresenterID;
    EarthObject.ID := ID;
    EarthObject.Hidden := Hidden;
    EarthObject.Selected := Selected;

    iTmpIndex := EarthObject.Index;
    EarthObject.Index := Index;
    if iTmpIndex <> Index then
      Resync( [] );
  end;
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalAddRecord
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalAddRecord( Buffer : Pointer; Append : Boolean );
begin
  raise Exception.Create( 'Insert or Append not supported in TEarthLayerDS' );
end;

{------------------------------------------------------------------------------
  TEarthLayerDS.InternalDelete
------------------------------------------------------------------------------}
procedure TEarthLayerDS.InternalDelete;
begin
//  FLayer.Objects[FiCurRec].delete;
  Resync( [] );
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.GetRecordCount
------------------------------------------------------------------------------}
function TEarthLayerDS.GetRecordCount : Longint;
begin
  if FLayer <> nil then
    Result := FLayer.Objects.Count
  else
    Result := 0;
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.GetRecNo
------------------------------------------------------------------------------}
function TEarthLayerDS.GetRecNo : Longint;
begin
  UpdateCursorPos;
  if ( FiCurRec = -1 ) and ( FLayer <> nil ) and ( FLayer.Objects.Count > 0 ) then
    Result := 1
  else
    Result := FiCurRec + 1;
end;

{------------------------------------------------------------------------------
 TEarthLayerDS.SetRecNo
------------------------------------------------------------------------------}
procedure TEarthLayerDS.SetRecNo( Value : Integer );
begin
  if ( Value >= 0 ) and ( Value < FLayer.Objects.Count ) then
  begin
    FiCurRec := Value - 1;
    Resync( [] );
  end;
end;


end.



