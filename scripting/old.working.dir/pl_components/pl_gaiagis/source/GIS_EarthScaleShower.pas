
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_EarthScaleShower;

interface

uses
  LCLIntf, LCLType, SysUtils, Classes, Graphics, Controls,
  GIS_SysUtils, GIS_Classes, GIS_XML, agx_canvas, Gr32;

type
TEarthScaleShower = class(TEarthRoot)
  private
    FEarth: TCustomEarth;
    FVisible: boolean;
    FUnits: TEarthUnitTypes;
    FFont: TEarthFont;
    FPen: TEarthPen;
    FPositionX: integer;
    FPositionY: integer;
    fOnChange : TNotifyEvent;
    procedure draw;
    Procedure setVisible(value:boolean);
    Procedure SetUnits(value:TEarthUnitTypes);
    Procedure setPositionX(value:integer);
    Procedure setPositionY(value:integer);
    Procedure SetPen(aPen:TEarthPen);
    Procedure SetFont(aFont:TEarthFont);
  protected
    Procedure DoChange(Sender : TObject);
  public
    constructor Create(aEarth: TCustomEarth); virtual;
    destructor Destroy; override;
    procedure Render;
    function  SaveEnvironment : TGXML_Element;
    procedure LoadEnvironment(Element : TGXML_Element);
    Procedure Clear;
    Property  OnChange : TNotifyEvent read fOnChange write fOnChange;
  published
    property  Visible: boolean read FVisible write setVisible;
    property  Font: TEarthFont read FFont write setFont;
    property  Pen: TEarthPen read FPen write SetPen;
    property  Units: TEarthUnitTypes read FUnits write SetUnits;
    property  PositionX: integer read FPositionX write setPositionX default 150;
    property  PositionY: integer read FPositionY write setPositionY default 50;
  end;

implementation

{===========================================================}
constructor TEarthScaleShower.Create(aEarth: TCustomEarth);
begin
  FEarth := aEarth;
  FFont:=TEarthFont.Create(self);
  Fpen:=TEarthPen.Create(self);
  clear;
  //FFont.OnChange:=DoChange;
 // FPen.OnChange:=DoChange;
end;

destructor TEarthScaleShower.Destroy;
 begin
 // FPen.OnChange:=nil;
 // FFont.OnChange:=nil;
  FPen.Free;
  FFont.Free;
  inherited;
 end;

Procedure TEarthScaleShower.Clear;
 begin  
  fFont.FontFillColor:= clRed32;
  fFont.FontSize:= 8;
  fpen.PenColor := clRed32;
  FPositionX := 130;
  FPositionY := 10;
  FUnits := euNauticalMile;
  FVisible := True;
 end;

Procedure TEarthScaleShower.DoChange(Sender : TObject);
 begin
  if Assigned(fOnChange) then fOnChange(Self);
 end;
//...............................................
Procedure TEarthScaleShower.setPositionX(value:integer);
begin
  FPositionX:=value;
  DoChange(self)
end;
Procedure TEarthScaleShower.setPositionY(value:integer);
begin
  FPositionY:=value;
  DoChange(self)
end;
Procedure TEarthScaleShower.SetUnits(value:TEarthUnitTypes);
begin
  FUnits:=value;
  DoChange(self)
end;
Procedure TEarthScaleShower.setVisible(value:boolean);
 begin
  FVisible:=value;
  DoChange(self)
 end;

Procedure TEarthScaleShower.SetPen(aPen:TEarthPen);
 begin
   fPen.Assign(aPen);
 end;

Procedure TEarthScaleShower.SetFont(aFont:TEarthFont);
  begin
    fFont.Assign(aFont);
  end;

function TEarthScaleShower.SaveEnvironment : TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddIntAttribute('PositionX',PositionX);
  Result.AddIntAttribute('PositionY',PositionY);
  Result.AddIntAttribute('Units',Ord(Units));
  Result.AddBoolAttribute('Visible',Visible);

  Result.AddElement('Pen', Pen.SaveEnvironment);
  Result.AddElement('Font', Font.SaveEnvironment);
  {
  //...Pen.....
  Result.AddAttribute('PenColor',ColorToString(Pen.Color));
  Result.AddIntAttribute('PenWidth',Pen.Width);
  Result.AddIntAttribute('PenMode',Ord(Pen.Mode));
  Result.AddIntAttribute('PenStyle',Ord(Pen.Style));
  //...Font...
  Result.AddAttribute('FontName',Font.Name);
  Result.AddIntAttribute('FontSize',Font.Size);
  Result.AddAttribute('FontColor',ColorToString(Font.Color));
         }
end;

procedure TEarthScaleShower.LoadEnvironment(Element : TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
     PositionX := IntAttributeByName('PositionX', PositionX);
     PositionY := IntAttributeByName('PositionY', PositionY);
     Units:=TEarthUnitTypes(IntAttributeByName('Units', Ord(Units)));
     Visible:=BoolAttributeByName('Visible',Visible);

     Pen.LoadEnvironment(ElementByName('Pen', 0));
     Font.LoadEnvironment(ElementByName('Font', 0));
   {
      //...Pen....
     Pen.Color:=StringToColor(AttributeByName('PenColor',ColorToString(Pen.Color)));
     Pen.Width :=IntAttributeByName('PenWidth',Pen.Width);
     Pen.Mode:=TPenMode(IntAttributeByName('PenMode', Ord(Pen.Mode)));
     Pen.Style:=TPenStyle(IntAttributeByName('PenStyle', Ord(Pen.Style)));

     //...Font...
     Font.Name:=AttributeByName('FontName',Font.Name);
     Font.Size :=IntAttributeByName('FontSize',Font.Size);
     Font.Color:=StringToColor(AttributeByName('FontColor',ColorToString(Font.Color)));  }
    end;
end;
//..........................................................

procedure TEarthScaleShower.Render;
begin
  if FVisible = True then  draw;
end;
//.......................... Draw ............................

procedure TEarthScaleShower.Draw;
var
  iLeft, iTop, iTA, iBkMode: Double;
  Check: extended;
  iH:Double;
  iXL,iXR:Double;
begin

  iLeft := fEarth.EarthCanvas.CanvasWidth - FPositionX;
  iTop := fEarth.EarthCanvas.CanvasHeight - FPositionY;

  if iLeft < 1 then iLeft := fEarth.EarthCanvas.CanvasWidth - 150;
  if iTop <= 1 then iTop := fEarth.EarthCanvas.CanvasHeight-50;

    //..................................................

     FFont.RenderAttribute(fEarth.EarthCanvas,false);  
    
     iH:=(Ffont.FontSize);
     fEarth.EarthCanvas.Text(iLeft, iTop-iH, '0');

    check := EarthUnitsTo(FEarth.Projection.UnitsPerInch, FUnits);

    if check < 10 then
    begin
      fEarth.EarthCanvas.Text(iLeft + fEarth.EarthCanvas.CanvasPixelsPerInch, iTop-iH,
                  FloatToStrF(check, ffNumber, 10,4) + UnitsToStr(FUnits));
    end 
    else
    begin
      fEarth.EarthCanvas.Text(iLeft + fEarth.EarthCanvas.CanvasPixelsPerInch, iTop-iH,
                  Format('%d %s', [Round(check), UnitsToStr(FUnits)]));
    end;
    //..........................................................
    FPen.RenderAttribute(fEarth.EarthCanvas,false);
    iXL:=iLeft;
    iXR:=iLeft + fEarth.EarthCanvas.CanvasPixelsPerInch;

    fEarth.EarthCanvas.ResetPath;
    fEarth.EarthCanvas.MoveTo(iXL, iTop);
    fEarth.EarthCanvas.LineTo(iXR, iTop);

    //draw Left line
    fEarth.EarthCanvas.MoveTo(iXL, iTop-3);
    fEarth.EarthCanvas.LineTo(iXL, iTop+3);

    //draw Left line
    fEarth.EarthCanvas.MoveTo(iXR, iTop-3);
    fEarth.EarthCanvas.LineTo(iXR, iTop+3);
    fEarth.EarthCanvas.DrawPath(AGX_StrokeOnly);

end;

end.
