{**********************************************************************}
{                                                                      }
{    "The contents of this file are subject to the Mozilla Public      }
{    License Version 1.1 (the "License"); you may not use this         }
{    file except in compliance with the License. You may obtain        }
{    a copy of the License at                                          }
{                                                                      }
{    http://www.mozilla.org/MPL/                                       }
{                                                                      }
{    Software distributed under the License is distributed on an       }
{    "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express       }
{    or implied. See the License for the specific language             }
{    governing rights and limitations under the License.               }
{                                                                      }
{    The Original Code is DelphiWebScriptII source code, released      }
{    January 1, 2001                                                   }
{                                                                      }
{    The Initial Developer of the Original Code is Matthias            }
{    Ackermann. Portions created by Matthias Ackermann are             }
{    Copyright (C) 2000 Matthias Ackermann, Switzerland. All           }
{    Rights Reserved.                                                  }
{                                                                      }
{    Contributor(s): Willibald Krenn                                   }
{                                                                      }
{**********************************************************************}

// DWS include file

{global compiler options}

{$A+} // align on
{$B-} // BoolEval off
{$X+} // extended syntax
{$H+} // long strings
{$Q-} // overflow checks off
{$J-} // assignable typed constants off

{$T-} // typed addr off - otherwise you'll get problems with ComSupport
      // <<.CreateResFmt(@SNoMethod, [Names]);>>

{.$WARN DUPLICATE_CTOR_DTOR OFF}   //=== ct9999 ===

   {$mode delphi}
   {$DEFINE PUREPASCAL}
   {$ASMMODE INTEL}

{.$DEFINE PUREPASCAL}
{.$DEFINE UNICODE_IDENTIFIERS}
{.$DEFINE COALESCE_VAR_INITIALIZATION}

