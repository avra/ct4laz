{**********************************************************************}
{                                                                      }
{    "The contents of this file are subject to the Mozilla Public      }
{    License Version 1.1 (the "License"); you may not use this         }
{    file except in compliance with the License. You may obtain        }
{    a copy of the License at http://www.mozilla.org/MPL/              }
{                                                                      }
{    Software distributed under the License is distributed on an       }
{    "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express       }
{    or implied. See the License for the specific language             }
{    governing rights and limitations under the License.               }
{                                                                      }
{    The Initial Developer of the Original Code is Matthias            }
{    Ackermann. For other initial contributors, see contributors.txt   }
{    Subsequent portions Copyright Creative IT.                        }
{                                                                      }
{    Current maintainer: Eric Grange                                   }
{                                                                      }
{**********************************************************************}

//=== ct9999 Modify for CodeTyphon Studio ===========

unit dwsXPlatform;

{$I dws.inc}


{$WARN SYMBOL_PLATFORM OFF}

interface

uses
   Classes, SysUtils, Types, Masks, Registry, SyncObjs, Variants, StrUtils,
   LCLIntf, LCLType, LMessages, fileutil, LazSysUtils, DateUtils;

const
{$IFDEF UNIX}
   cLineTerminator  = #10;
{$ELSE}
   cLineTerminator  = #13#10;
{$ENDIF}

   // following is missing from D2010
   INVALID_HANDLE_VALUE = NativeUInt(-1);

   // FreePascal RTL declares this constant, but does not support it,
   // so it just leads to runtime crashes, this attempts to trigger compile-time crashes instead
//   varUString = 'varUString is not supported by FreePascal';


type

{$HINTS OFF}

   TdwsCriticalSection = class
      private
         FDummy : array [0..95-SizeOf(TRTLCRiticalSection)-2*SizeOf(Pointer)] of Byte;
         FCS : TRTLCriticalSection;

      public
         constructor Create;
         destructor Destroy; override;

         procedure Enter;
         procedure Leave;

         function TryEnter : Boolean;
   end;

   IMultiReadSingleWrite = interface
      procedure BeginRead;
      function  TryBeginRead : Boolean;
      procedure EndRead;

      procedure BeginWrite;
      function  TryBeginWrite : Boolean;
      procedure EndWrite;
   end;

   TMultiReadSingleWriteState = (mrswUnlocked, mrswReadLock, mrswWriteLock);



   TMultiReadSingleWrite = class (TInterfacedObject, IMultiReadSingleWrite)
      private
         FSRWLock : Pointer;
         FDummy : array [0..95-4*SizeOf(Pointer)] of Byte; // padding
      public

         procedure BeginRead; inline;
         function  TryBeginRead : Boolean; inline;
         procedure EndRead; inline;

         procedure BeginWrite; inline;
         function  TryBeginWrite : Boolean; inline;
         procedure EndWrite; inline;

         // use for diagnostic only
         function State : TMultiReadSingleWriteState;
   end;

{$HINTS ON}

procedure SetDecimalSeparator(c : Char);
function GetDecimalSeparator : Char;

type
   TCollectFileProgressEvent = procedure (const directory : TFileName; var skipScan : Boolean) of object;

procedure CollectFiles(const directory, fileMask : TFileName;
                       list : TStrings; recurseSubdirectories: Boolean = False;
                       onProgress : TCollectFileProgressEvent = nil);
procedure CollectSubDirs(const directory : TFileName; list : TStrings);

type

   TBytes = array of Byte;

   RawByteString = String;

   PNativeInt = ^NativeInt;
   PUInt64 = ^UInt64;


   TPath = class
      class function GetTempPath : String; static;
      class function GetTempFileName : String; static;
   end;

   TFile = class
      class function ReadAllBytes(const filename : String) : TBytes; static;
   end;

   TdwsThread = class (TThread)
   end;

// 64bit system clock reference in milliseconds since boot
function GetSystemMilliseconds : Int64;
function UTCDateTime : TDateTime;
function UnixTime : Int64;

function LocalDateTimeToUTCDateTime(t : TDateTime) : TDateTime;
function UTCDateTimeToLocalDateTime(t : TDateTime) : TDateTime;

function SystemMillisecondsToUnixTime(t : Int64) : Int64;
function UnixTimeToSystemMilliseconds(ut : Int64) : Int64;

procedure SystemSleep(msec : Integer);

function FirstWideCharOfString(const s : String; const default : WideChar = #0) : WideChar; inline;
procedure CodePointToUnicodeString(c : Integer; var result : UnicodeString);
procedure CodePointToString(const c : Integer; var result : String); inline;

function UnicodeCompareP(p1 : PWideChar; n1 : Integer; p2 : PWideChar; n2 : Integer) : Integer; overload;
function UnicodeCompareP(p1, p2 : PWideChar; n : Integer) : Integer; overload;

function UnicodeLowerCase(const s : String) : String; overload;
function UnicodeUpperCase(const s : String) : String; overload;

function ASCIICompareText(const s1, s2 : String) : Integer; inline;
function ASCIISameText(const s1, s2 : String) : Boolean; inline;

function NormalizeString(const s, form : String) : String;
function StripAccents(const s : String) : String;

function InterlockedIncrement(var val : Integer) : Integer; overload; {$IFDEF PUREPASCAL} inline; {$endif}
function InterlockedDecrement(var val : Integer) : Integer; {$IFDEF PUREPASCAL} inline; {$endif}

procedure FastInterlockedIncrement(var val : Integer); {$IFDEF PUREPASCAL} inline; {$endif}
procedure FastInterlockedDecrement(var val : Integer); {$IFDEF PUREPASCAL} inline; {$endif}

function InterlockedExchangePointer(var target : Pointer; val : Pointer) : Pointer; {$IFDEF PUREPASCAL} inline; {$endif}

function InterlockedCompareExchangePointer(var destination : Pointer; exchange, comparand : Pointer) : Pointer; {$IFDEF PUREPASCAL} inline; {$endif}

procedure SetThreadName(const threadName : PAnsiChar; threadID : Cardinal = Cardinal(-1));

procedure OutputDebugString(const msg : String);

procedure WriteToOSEventLog(const logName, logCaption, logDetails : String;
                            const logRawData : RawByteString = ''); overload;


procedure VarCopy(out dest : Variant; const src : Variant); inline;

function Utf8ToUnicodeString(const buf : RawByteString) : UnicodeString; inline;


function RawByteStringToBytes(const buf : RawByteString) : TBytes;
function BytesToRawByteString(const buf : TBytes; startIndex : Integer = 0) : RawByteString; overload;
function BytesToRawByteString(p : Pointer; size : Integer) : RawByteString; overload;

function LoadDataFromFile(const fileName : TFileName) : TBytes;
procedure SaveDataToFile(const fileName : TFileName; const data : TBytes);

function LoadRawBytesFromFile(const fileName : TFileName) : RawByteString;
function SaveRawBytesToFile(const fileName : TFileName; const data : RawByteString) : Integer;

procedure LoadRawBytesAsScriptStringFromFile(const fileName : TFileName; var result : String);

function LoadTextFromBuffer(const buf : TBytes) : UnicodeString;
function LoadTextFromRawBytes(const buf : RawByteString) : UnicodeString;
function LoadTextFromStream(aStream : TStream) : UnicodeString;
function LoadTextFromFile(const fileName : TFileName) : UnicodeString;
procedure SaveTextToUTF8File(const fileName : TFileName; const text : UTF8String);
procedure AppendTextToUTF8File(const fileName : TFileName; const text : UTF8String);
function OpenFileForSequentialReadOnly(const fileName : TFileName) : THandle;
function OpenFileForSequentialWriteOnly(const fileName : TFileName) : THandle;
procedure CloseFileHandle(hFile : THandle);
function FileWrite(hFile : THandle; buffer : Pointer; byteCount : Integer) : Cardinal;
function FileFlushBuffers(hFile : THandle) : Boolean;
function FileCopy(const existing, new : TFileName; failIfExists : Boolean) : Boolean;
function FileMove(const existing, new : TFileName) : Boolean;
function FileDelete(const fileName : TFileName) : Boolean;
function FileRename(const oldName, newName : TFileName) : Boolean;
function FileSize(const name : TFileName) : Int64;
function FileDateTime(const name : TFileName) : TDateTime;
procedure FileSetDateTime(hFile : THandle; aDateTime : TDateTime);
function DeleteDirectory(const path : String) : Boolean;

function DirectSet8087CW(newValue : Word) : Word; register;
function DirectSetMXCSR(newValue : Word) : Word; register;

function SwapBytes(v : Cardinal) : Cardinal;
procedure SwapInt64(src, dest : PInt64);

function RDTSC : UInt64;

function GetCurrentUserName : String;

procedure InitializeWithDefaultFormatSettings(var fmt : TFormatSettings);

type
   TTimerEvent = procedure of object;

   ITimer = interface
      procedure Cancel;
   end;

   TTimerTimeout = class (TInterfacedObject, ITimer)
      private
         FTimer : THandle;
         FOnTimer : TTimerEvent;

      public
         class function Create(delayMSec : Cardinal; onTimer : TTimerEvent) : ITimer;
         destructor Destroy; override;

         procedure Cancel;
   end;

type
   TModuleVersion = record
      Major, Minor : Word;
      Release, Build : Word;
      function AsString : String;
   end;

function GetModuleVersion(instance : THandle; var version : TModuleVersion) : Boolean;
function GetApplicationVersion(var version : TModuleVersion) : Boolean;
function ApplicationVersion : String;

// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------
implementation
// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------

   
var
   vGetSystemMilliseconds : function : Int64; stdcall;

function GetSystemTimeMilliseconds : Int64; stdcall;
begin
  Result:=GetTickCount64;
end;

function GetSystemMilliseconds : Int64;
begin
   Result:=vGetSystemMilliseconds;
end;

procedure InitializeGetSystemMilliseconds;
begin
   vGetSystemMilliseconds:=@GetSystemTimeMilliseconds;
end;

function UTCDateTime : TDateTime;
begin
   result:=NowUTC;
end;

function UnixTime : Int64;
begin
   Result:=Trunc(UTCDateTime*86400)-Int64(25569)*86400;
end;

type
   TDynamicTimeZoneInformation = record
      Bias : Longint;
      StandardName : array[0..31] of WCHAR;
      StandardDate : TSystemTime;
      StandardBias : Longint;
      DaylightName : array[0..31] of WCHAR;
      DaylightDate : TSystemTime;
      DaylightBias : Longint;
      TimeZoneKeyName : array[0..127] of WCHAR;
      DynamicDaylightTimeDisabled : Boolean;
   end;
   PDynamicTimeZoneInformation = ^TDynamicTimeZoneInformation;

function LocalDateTimeToUTCDateTime(t : TDateTime) : TDateTime;
begin
  result:=t;
end;

function UTCDateTimeToLocalDateTime(t : TDateTime) : TDateTime;
begin
  result:=t;
end;

function SystemMillisecondsToUnixTime(t : Int64) : Int64;
begin
   Result := UnixTime - (GetSystemTimeMilliseconds-t) div 1000;
end;

function UnixTimeToSystemMilliseconds(ut : Int64) : Int64;
begin
   Result := GetSystemTimeMilliseconds - (UnixTime-ut)*1000;
end;

procedure SystemSleep(msec : Integer);
begin
   if msec>=0 then
      Sleep(msec);
end;

function FirstWideCharOfString(const s : String; const default : WideChar = #0) : WideChar;
begin
   if s <> '' then
      Result := PWideChar(String(s))^
   else Result := default;
end;

procedure CodePointToUnicodeString(c : Integer; var result : UnicodeString);
begin
   case c of
      0..$FFFF :
         Result := WideChar(c);
      $10000..$10FFFF : begin
         c := c-$10000;
         Result := WideChar($D800+(c shr 10))+WideChar($DC00+(c and $3FF));
      end;
   else
      raise EConvertError.CreateFmt('Invalid codepoint: %d', [c]);
   end;
end;

procedure CodePointToString(const c : Integer; var result : String); inline;
var
   buf : UnicodeString;
begin
   CodePointToUnicodeString(c, buf);
   result := String(buf);
end;

function UnicodeStringReplace(const s, oldPattern, newPattern: String; flags: TReplaceFlags) : String;
begin
   Result := SysUtils.StringReplace(s, oldPattern, newPattern, flags);
end;

function UnicodeCompareP(p1 : PWideChar; n1 : Integer; p2 : PWideChar; n2 : Integer) : Integer;
begin
   //Result := CompareStr(String(@pi),String(p2));
end;

function UnicodeCompareP(p1, p2 : PWideChar; n : Integer) : Integer; overload;
begin
  //Result := CompareStr(String(@pi),String(p2));
end;

function UnicodeLowerCase(const s : String) : String;
begin
   Result := String(UnicodeLowerCase(UnicodeString(s)));
end;

function UnicodeUpperCase(const s : String) : String;
begin
   Result := String(UnicodeUpperCase(UnicodeString(s)));
end;

function ASCIICompareText(const s1, s2 : String) : Integer; inline;
begin
   Result:=CompareText(UTF8Encode(s1), UTF8Encode(s2));
end;

function ASCIISameText(const s1, s2 : String) : Boolean; inline;
begin
   Result:=(ASCIICompareText(s1, s2)=0);
end;

function NormalizeString(const s, form : String) : String;
begin
   result:=s;
end;

function StripAccents(const s : String) : String;
var
   i : Integer;
   pSrc, pDest : PWideChar;
begin
   Result := NormalizeString(s, 'NFD');
   pSrc := Pointer(Result);
   pDest := pSrc;
   for i := 1 to Length(Result) do begin
      case Ord(pSrc^) of
         $300..$36F : ; // diacritic range
      else
         pDest^ := pSrc^;
         Inc(pDest);
      end;
      Inc(pSrc);
   end;
   SetLength(Result, (NativeUInt(pDest)-NativeUInt(Pointer(Result))) div 2);
end;

function InterlockedIncrement(var val : Integer) : Integer;
begin
   Result:=system.InterlockedIncrement(val);
end;

function InterlockedDecrement(var val : Integer) : Integer;
begin
   Result:=system.InterlockedDecrement(val);
end;

procedure FastInterlockedIncrement(var val : Integer);
begin
   InterlockedIncrement(val);
end;

procedure FastInterlockedDecrement(var val : Integer);
begin
   InterlockedDecrement(val);
end;

function InterlockedExchangePointer(var target : Pointer; val : Pointer) : Pointer;
begin
   Result:=System.InterLockedExchange(target, val);
end;

function InterlockedCompareExchangePointer(var destination : Pointer; exchange, comparand : Pointer) : Pointer; {$IFDEF PUREPASCAL} inline; {$endif}
begin
      {$ifdef CPU64}
      Result := Pointer(System.InterlockedCompareExchange64(QWord(destination), QWord(exchange), QWord(comparand)));
      {$else}
      Result:=System.InterLockedCompareExchange(destination, exchange, comparand);
      {$endif}
end;

procedure SetThreadName(const threadName : PAnsiChar; threadID : Cardinal = Cardinal(-1));
type
   TThreadNameInfo = record
      dwType : Cardinal;      // must be 0x1000
      szName : PAnsiChar;     // pointer to name (in user addr space)
      dwThreadID : Cardinal;  // thread ID (-1=caller thread)
      dwFlags : Cardinal;     // reserved for future use, must be zero
   end;
var
   info : TThreadNameInfo;
begin

   info.dwType:=$1000;
   info.szName:=threadName;
   info.dwThreadID:=threadID;
   info.dwFlags:=0;

end;

procedure OutputDebugString(const msg : String);
begin

end;

procedure WriteToOSEventLog(const logName, logCaption, logDetails : String;
                            const logRawData : RawByteString = '');
begin

end;

procedure SetDecimalSeparator(c : Char);
begin
      FormatSettings.DecimalSeparator:=c;
end;

function GetDecimalSeparator : Char;
begin
      Result:=FormatSettings.DecimalSeparator;
end;

type

   TMasks = array of TMask;

procedure CollectFilesMasked(const directory : TFileName;
                             const masks : TMasks; list : TStrings;
                             recurseSubdirectories: Boolean = False;
                             onProgress : TCollectFileProgressEvent = nil);

begin

end;

procedure CollectFiles(const directory, fileMask : TFileName; list : TStrings;
                       recurseSubdirectories: Boolean = False;
                       onProgress : TCollectFileProgressEvent = nil);
begin

end;

procedure CollectSubDirs(const directory : TFileName; list : TStrings);
begin

end;

procedure VarCopy(out dest : Variant; const src : Variant);
begin
   dest:=src;
end;

function Utf8ToUnicodeString(const buf : RawByteString) : UnicodeString; inline;
begin
   Result := UTF8Decode(buf);
end;

function RawByteStringToBytes(const buf : RawByteString) : TBytes;
var
   n : Integer;
begin
   n:=Length(buf);
   SetLength(Result, n);
   if n>0 then
      System.Move(buf[1], Result[0], n);
end;

function BytesToRawByteString(const buf : TBytes; startIndex : Integer = 0) : RawByteString;
var
   n : Integer;
begin
   n:=Length(buf)-startIndex;
   if n<=0 then
      Result:=''
   else begin
      SetLength(Result, n);
      System.Move(buf[startIndex], Pointer(Result)^, n);
   end;
end;

function BytesToRawByteString(p : Pointer; size : Integer) : RawByteString;
begin
   SetLength(Result, size);
   System.Move(p^, Pointer(Result)^, size);
end;

function TryTextToFloat(const s : PChar; var value : Extended; const formatSettings : TFormatSettings) : Boolean;

var
   cw : Word;
begin
   cw:=Get8087CW;
   Set8087CW($133F);
   if TryStrToFloat(s, value, formatSettings) then
      Result:=(value>-1.7e308) and (value<1.7e308);
   if not Result then
      value:=0;
   asm fclex end;
   Set8087CW(cw);
end;

function TryTextToFloatW(const s : PWideChar; var value : Extended;
                        const formatSettings : TFormatSettings) : Boolean;

var
   bufU : UnicodeString;
   buf : String;
begin
   bufU := s;
   buf := String(bufU);
   Result := TryTextToFloat(PChar(buf), value, formatSettings);
end;

function LoadTextFromBuffer(const buf : TBytes) : UnicodeString;
var
   n, sourceLen, len : Integer;
   encoding : TEncoding;
begin
   if buf=nil then
      Result:=''
   else begin
      encoding:=nil;
      n:=TEncoding.GetBufferEncoding(buf, encoding);
      if n=0 then
         encoding:=TEncoding.UTF8;
      if encoding=TEncoding.UTF8 then begin
         // handle UTF-8 directly, encoding.GetString returns an empty string
         // whenever a non-utf-8 character is detected, the implementation below
         // will return a '?' for non-utf8 characters instead
         sourceLen := Length(buf)-n;
         SetLength(Result, sourceLen);
         len := Utf8ToUnicode(Pointer(Result), sourceLen+1, PAnsiChar(buf)+n, sourceLen)-1;
         if len>0 then begin
            if len<>sourceLen then
               SetLength(Result, len);
         end else Result:=''
      end else begin
         Result:=encoding.GetString(buf, n, Length(buf)-n);
      end;
   end;
end;

function LoadTextFromRawBytes(const buf : RawByteString) : UnicodeString;
var
   b : TBytes;
begin
   if buf='' then Exit('');
   SetLength(b, Length(buf));
   System.Move(buf[1], b[0], Length(buf));
   Result:=LoadTextFromBuffer(b);
end;

function LoadTextFromStream(aStream : TStream) : UnicodeString;
var
   n : Integer;
   buf : TBytes;
begin
   n := aStream.Size-aStream.Position;
   SetLength(buf, n);
   aStream.Read(buf[0], n);
   Result:=LoadTextFromBuffer(buf);
end;


function LoadTextFromFile(const fileName : TFileName) : UnicodeString;
var
   buf : TBytes;
begin
   buf:=LoadDataFromFile(fileName);
   Result:=LoadTextFromBuffer(buf);
end;

function ReadFileChunked(hFile : THandle; const buffer; size : Integer) : Integer;
const
   CHUNK_SIZE = 16384;
var
   p : PByte;
   nRemaining : Integer;
   nRead : Cardinal;
begin
   p := @buffer;
   nRemaining := size;
   repeat
      if nRemaining > CHUNK_SIZE then
         nRead := CHUNK_SIZE
      else nRead := nRemaining;
    {
      if not ReadFile(hFile, p^, nRead, nRead, nil) then
         RaiseLastOSError
      else if nRead = 0 then begin
         // file got trimmed while we were reading
         Exit(size-nRemaining);
      end;
       }
      Dec(nRemaining, nRead);
      Inc(p, nRead);
   until nRemaining <= 0;
   Result := size;
end;


function LoadDataFromFile(const fileName : TFileName) : TBytes;
const
   INVALID_FILE_SIZE = DWORD($FFFFFFFF);
var
   hFile : THandle;
   n, nRead : Cardinal;
begin
   if fileName='' then Exit(nil);
   hFile:=OpenFileForSequentialReadOnly(fileName);
   if hFile=INVALID_HANDLE_VALUE then Exit(nil);


end;

procedure SaveDataToFile(const fileName : TFileName; const data : TBytes);
var
   hFile : THandle;
   n, nWrite : DWORD;
begin
   hFile:=OpenFileForSequentialWriteOnly(fileName);

end;

function LoadRawBytesFromFile(const fileName : TFileName) : RawByteString;
const
   INVALID_FILE_SIZE = DWORD($FFFFFFFF);
var
   hFile : THandle;
   n, nRead : Cardinal;
begin
   if fileName='' then Exit;
   hFile := OpenFileForSequentialReadOnly(fileName);
   if hFile = INVALID_HANDLE_VALUE then Exit;

end;

function SaveRawBytesToFile(const fileName : TFileName; const data : RawByteString) : Integer;
var
   hFile : THandle;
   nWrite : DWORD;
begin
   Result:=0;
   hFile:=OpenFileForSequentialWriteOnly(fileName);

end;

procedure LoadRawBytesAsScriptStringFromFile(const fileName : TFileName; var result : String);
const
   INVALID_FILE_SIZE = DWORD($FFFFFFFF);
var
   hFile : THandle;
   n, i, nRead : Cardinal;
   pDest : PWord;
   buffer : array [0..16383] of Byte;
begin
   if fileName='' then Exit;
   hFile:=OpenFileForSequentialReadOnly(fileName);
   if hFile=INVALID_HANDLE_VALUE then Exit;

end;

procedure SaveTextToUTF8File(const fileName : TFileName; const text : UTF8String);
begin
   SaveRawBytesToFile(fileName, UTF8Encode(text));
end;

procedure AppendTextToUTF8File(const fileName : TFileName; const text : UTF8String);
var
   fs : TFileStream;
begin
   if text='' then Exit;
   if FileExists(fileName) then
      fs:=TFileStream.Create(fileName, fmOpenWrite or fmShareDenyNone)
   else fs:=TFileStream.Create(fileName, fmCreate);
   try
      fs.Seek(0, soFromEnd);
      fs.Write(text[1], Length(text));
   finally
      fs.Free;
   end;
end;

function OpenFileForSequentialReadOnly(const fileName : TFileName) : THandle;
begin

end;

function OpenFileForSequentialWriteOnly(const fileName : TFileName) : THandle;
begin

end;

procedure CloseFileHandle(hFile : THandle);
begin

end;

function FileWrite(hFile : THandle; buffer : Pointer; byteCount : Integer) : Cardinal;
begin

end;

function FileFlushBuffers(hFile : THandle) : Boolean;
begin

end;

function FileCopy(const existing, new : TFileName; failIfExists : Boolean) : Boolean;
begin
   Result:=CopyFile(PWideChar(existing), PWideChar(new), failIfExists);
end;

function FileMove(const existing, new : TFileName) : Boolean;
begin
   //Result:=MoveFile(PWideChar(existing), PWideChar(new));
end;

function FileDelete(const fileName : TFileName) : Boolean;
begin
   Result:=SysUtils.DeleteFile(fileName);
end;

function FileRename(const oldName, newName : TFileName) : Boolean;
begin
   Result:=RenameFile(oldName, newName);
end;

function FileSize(const name : TFileName) : Int64;
begin
    Result:=FileUtil.FileSize(name);
end;

function FileDateTime(const name : TFileName) : TDateTime;
begin

end;

procedure FileSetDateTime(hFile : THandle; aDateTime : TDateTime);
begin
   FileSetDate(hFile, DateTimeToFileDate(aDateTime));
end;

function DeleteDirectory(const path : String) : Boolean;
begin
   Result := RemoveDir(path);
end;

function DirectSet8087CW(newValue: Word): Word; register;
begin
   Result:=newValue;
end;

function DirectSetMXCSR(newValue : Word) : Word; register;
begin
   Result:=newValue;
end;

function SwapBytes(v : Cardinal) : Cardinal;
type
   TCardinalBytes = array [0..3] of Byte;
begin
   TCardinalBytes(Result)[0] := TCardinalBytes(v)[3];
   TCardinalBytes(Result)[1] := TCardinalBytes(v)[2];
   TCardinalBytes(Result)[2] := TCardinalBytes(v)[1];
   TCardinalBytes(Result)[3] := TCardinalBytes(v)[0];
end;

procedure SwapInt64(src, dest : PInt64);
begin
   PByteArray(dest)[0] := PByteArray(src)[7];
   PByteArray(dest)[1] := PByteArray(src)[6];
   PByteArray(dest)[2] := PByteArray(src)[5];
   PByteArray(dest)[3] := PByteArray(src)[4];
   PByteArray(dest)[4] := PByteArray(src)[3];
   PByteArray(dest)[5] := PByteArray(src)[2];
   PByteArray(dest)[6] := PByteArray(src)[1];
   PByteArray(dest)[7] := PByteArray(src)[0];
end;

// RDTSC
//
function RDTSC : UInt64;
asm
   RDTSC
end;

function GetCurrentUserName : String;
begin

end;

procedure InitializeWithDefaultFormatSettings(var fmt : TFormatSettings);
begin
   fmt:=SysUtils.TFormatSettings((@CurrencyString{%H-})^);
end;

function TModuleVersion.AsString : String;
begin
   Result := Format('%d.%d.%d.%d', [Major, Minor, Release, Build]);
end;

function GetModuleVersion(instance : THandle; var version : TModuleVersion) : Boolean;
begin
   Result:=False;
end;

var
   vApplicationVersion : TModuleVersion;
   vApplicationVersionRetrieved : Integer;

function GetApplicationVersion(var version : TModuleVersion) : Boolean;
begin
   if vApplicationVersionRetrieved = 0 then begin
      if GetModuleVersion(HInstance, vApplicationVersion) then
         vApplicationVersionRetrieved := 1
      else vApplicationVersionRetrieved := -1;
   end;
   Result := (vApplicationVersionRetrieved = 1);
   if Result then
      version := vApplicationVersion;
end;

function ApplicationVersion : String;
var
   version : TModuleVersion;
begin
   {$ifdef CPU64}
   if GetApplicationVersion(version) then
      Result := version.AsString + ' 64bit'
   else Result := '?.?.?.? 64bit';
   {$else}
   if GetApplicationVersion(version) then
      Result := version.AsString + ' 32bit'
   else Result := '?.?.?.? 32bit';
   {$endif}
end;

// ------------------
// ------------------ TdwsCriticalSection ------------------
// ------------------

constructor TdwsCriticalSection.Create;
begin
   system.InitCriticalSection(FCS);
end;

destructor TdwsCriticalSection.Destroy;
begin
   system.DoneCriticalSection(FCS);
end;

procedure TdwsCriticalSection.Enter;
begin
   system.EnterCriticalSection(FCS);
end;

procedure TdwsCriticalSection.Leave;
begin
    system.LeaveCriticalSection(FCS);
end;

function TdwsCriticalSection.TryEnter : Boolean;
begin
   Result:= (system.TryEnterCriticalSection(FCS)=0);
end;

// ------------------
// ------------------ TPath ------------------
// ------------------

class function TPath.GetTempPath : String;
begin
   Result:=GetTempPath;
end;

class function TPath.GetTempFileName : String;
begin
 Result:=GetTempFileName;
end;

// ------------------
// ------------------ TFile ------------------
// ------------------

class function TFile.ReadAllBytes(const filename : String) : TBytes;
var
   fileStream : TFileStream;
   n : Integer;
begin
   fileStream:=TFileStream.Create(filename, fmOpenRead or fmShareDenyWrite);
   try
      n:=fileStream.Size;
      SetLength(Result, n);
      if n>0 then
         fileStream.ReadBuffer(Result[0], n);
   finally
      fileStream.Free;
   end;
end;

// ------------------
// ------------------ TMultiReadSingleWrite ------------------
// ------------------

procedure TMultiReadSingleWrite.BeginRead;
begin

end;

function TMultiReadSingleWrite.TryBeginRead : Boolean;
begin

end;

procedure TMultiReadSingleWrite.EndRead;
begin

end;

procedure TMultiReadSingleWrite.BeginWrite;
begin

end;

function TMultiReadSingleWrite.TryBeginWrite : Boolean;
begin

end;

procedure TMultiReadSingleWrite.EndWrite;
begin

end;

function TMultiReadSingleWrite.State : TMultiReadSingleWriteState;
begin
   // Attempt to guess the state of the lock without making assumptions
   // about implementation details
   // This is only for diagnosing locking issues
   if TryBeginWrite then begin
      EndWrite;
      Result:=mrswUnlocked;
   end else if TryBeginRead then begin
      EndRead;
      Result:=mrswReadLock;
   end else begin
      Result:=mrswWriteLock;
   end;
end;

// ------------------
// ------------------ TTimerTimeout ------------------
// ------------------

procedure TTimerTimeoutCallBack(Context: Pointer; {%H-}Success: Boolean); stdcall;
var
   tt : TTimerTimeout;
   event : TTimerEvent;
begin
   tt := TTimerTimeout(Context);
   tt._AddRef;
   try
      event := tt.FOnTimer;
      if Assigned(event) then
         event();
    //  DeleteTimerQueueTimer(0, tt.FTimer, 0);
      tt.FTimer := 0;
   finally
      tt._Release;
   end;
end;

// Create
//
class function TTimerTimeout.Create(delayMSec : Cardinal; onTimer : TTimerEvent) : ITimer;
var
   obj : TTimerTimeout;
begin
   obj := TTimerTimeout(inherited Create);
   Result := obj;
   obj.FOnTimer := onTimer;
  // CreateTimerQueueTimer(obj.FTimer, 0, TTimerTimeoutCallBack, obj,
  //                       delayMSec, 0,
  //                       WT_EXECUTEDEFAULT or WT_EXECUTELONGFUNCTION or WT_EXECUTEONLYONCE);
end;

// Destroy
//
destructor TTimerTimeout.Destroy;
begin
   Cancel;
   inherited;
end;

// Cancel
//
procedure TTimerTimeout.Cancel;
begin
   FOnTimer := nil;
   if FTimer = 0 then Exit;
  // DeleteTimerQueueTimer(0, FTimer, INVALID_HANDLE_VALUE);
   FTimer:=0;
end;

// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------
initialization
// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------

   InitializeGetSystemMilliseconds;

end.
