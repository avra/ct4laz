﻿# default parameters:
param  # example usage:
(      # .\pack.ps1 -ct4laz "c:\other\ct4laz\dir\" -dbg 1
  # [string]  $zip    = "c:\Program Files\7-pl_zip\7z.exe",
  [string]  $ct4laz = "..\", # or absolute path: "m:\Lazarus\ct4laz\" to get to get same effect (this is the place where components and examples dirs will be stored
  [boolean] $dbg    = $false # use parameter 0 for false and 1 for true
)

$StartTime = Get-Date

# use Log instead of Write-Host
set-alias Log Write-Host

$CurrentDir = $(get-location).Path
cd $ct4laz

$zip_path = $ct4laz + "pl_zip\"
if (Test-Path $zip_path)
  { Remove-Item $zip_path -Recurse }
New-Item -ItemType Directory -Force -Path $zip_path

# $src_component = $ct4laz + "pl_components\pl_aggpas"
# $src_example   = $ct4laz + "pl_examples\pl_aggpas"
# $dest          = $ct4laz + "pl_zip\pl_aggpas.zip"
# Compress-Archive -Path $src_component -DestinationPath $dest
# Compress-Archive -Path $src_example -Update -DestinationPath $dest
# $zip_cmd = "7z a " + $dest + " -tzip -mx9 -r " + $src_component # + " " + $src_example
# cmd.exe /c $zip_cmd

# zip files with px_ prefix instead of pl_ are not good for OPM so they will be only on ct4laz repo in case someone is desparately needing them

cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_0_libs.zip           -r pl_components\pl_0_libs           pl_examples\pl_0_libs"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_aggpas.zip           -r pl_components\pl_aggpas           pl_examples\pl_aggpas"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_aggpasvs.zip         -r pl_components\pl_aggpasvs         pl_examples\pl_aggpasvs"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_ape.zip              -r pl_components\pl_apelib           pl_examples\pl_ape"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_asiovst.zip          -r pl_components\pl_asiovst          pl_examples\pl_asiovst"
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_chipmunkpas.zip      -r pl_components\pl_chipmunkpas      pl_examples\pl_chipmunkpas"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_cindy.zip            -r pl_components\pl_cindy            pl_examples\pl_cindy"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_cmdrunner.zip        -r pl_components\pl_cmdrunner        pl_examples\pl_cmdrunner"                                     # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_dwscript.zip         -r pl_components\pl_dwscript         pl_examples\pl_dwscript"                                      # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_excompress.zip       -r pl_components\pl_excompress       pl_examples\pl_excompress"                                    # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_excontrols.zip       -r pl_components\pl_excontrols       pl_examples\pl_excontrols"                                    # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_exdatabase.zip       -r pl_components\pl_exdatabase       pl_examples\pl_exdatabase"                                    # new 20x32x
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_exdesign.zip         -r pl_components\pl_exdesign         pl_examples\pl_exdesign"                                      # new 20x32x
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_exgeographic.zip     -r pl_components\pl_exgeographic     pl_examples\pl_exgeographic"                                  # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_expatpas.zip         -r pl_components\pl_expatpas         pl_examples\pl_gaiagis"                                       # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_gaiagis.zip          -r pl_components\pl_gaiagis          pl_examples\pl_gaiagis"                                       # new 20x32x
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_geogis.zip           -r pl_components\pl_geogis           pl_examples\pl_geogis"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_graphics32.zip       -r pl_components\pl_graphics32       pl_examples\pl_graphics32"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_graphics32ext.zip    -r pl_components\pl_graphics32ext    pl_examples\pl_graphics32ext"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_graphics32magic.zip  -r pl_components\pl_graphics32magic  pl_examples\pl_graphics32magic"                               # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_graphics32mg.zip     -r pl_components\pl_graphics32mg     pl_examples\pl_graphics32mg"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_graphics32vpr        -r pl_components\pl_graphics32vpr    pl_examples\pl_graphics32vpr"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_html5canvas.zip      -r pl_components\pl_html5canvas      pl_examples\pl_html5canvas"
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_jvcl.zip             -r pl_components\pl_jvcl             pl_examples\pl_jvcl"                                        # deprecated with CCR version in OPM
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_lockbox.zip          -r pl_components\pl_lockbox          pl_examples\pl_lockbox"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_macosmetal.zip       -r pl_components\pl_macosmetal       pl_examples\pl_macosmetal"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_magicscript.zip      -r pl_components\pl_magicscript      pl_examples\pl_magicscript"
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_mapviewer.zip        -r pl_components\pl_mapviewer        pl_examples\pl_mapviewer"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_opencl.zip           -r pl_components\pl_opencl           pl_examples\pl_opencl"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_opengl.zip           -r pl_components\pl_opengl           pl_examples\pl_opengl            pl_examples\pl_openglcanvas"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_opengladv.zip        -r pl_components\pl_opengladv        pl_examples\pl_opengladv"                                     # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_opengles.zip         -r pl_components\pl_opengles         pl_examples\pl_opengles1         pl_examples\pl_opengles2"
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_openglpt.zip         -r pl_components\pl_openglpt         pl_examples\pl_openglpt"
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_openweather.zip      -r pl_components\pl_openweather      pl_examples\pl_openweather"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_openwire.zip         -r pl_components\pl_openwire"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_pappe.zip            -r pl_components\pl_pappe            pl_examples\pl_pappe"                                         # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_sdl2.zip             -r pl_components\pl_sdl2             pl_examples\pl_sdl2"                                          # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_shapes.zip           -r pl_components\pl_shapes           pl_examples\pl_shapes"                                        # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_synapsevs.zip        -r pl_components\pl_synapsevs        pl_examples\pl_synapsevs"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_titanscript.zip      -r pl_components\pl_titanscript      pl_examples\pl_titanscript"                                   # new 20x32x
# cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_titansound.zip       -r pl_components\pl_titansound       pl_examples\pl_titansound"                                    # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\px_tsmbios.zip          -r pl_components\pl_tsmbios          pl_examples\pl_tsmbios"                                       # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_usb.zip              -r pl_components\pl_usb              pl_examples\pl_usb"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_vampyreimaging.zip   -r pl_components\pl_vampyreimaging   pl_examples\pl_vampyreimaging"                                # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_vulkan.zip           -r pl_components\pl_vulkan           pl_examples\pl_vulkan"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_api.zip          -r pl_components\pl_win_api"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_directx.zip      -r pl_components\pl_win_directx      pl_examples\pl_win_directx9"
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_directx11.zip    -r pl_components\pl_win_directx11"                                                                 # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_directx12.zip    -r pl_components\pl_win_directx12    pl_examples\pl_win_directx12"                                 # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_directxut.zip    -r pl_components\pl_win_directxut    pl_examples\pl_win_directx9ut"                                # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_dspack.zip       -r pl_components\pl_win_dspack       pl_examples\pl_win_dspack"                                    # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_gdi.zip          -r pl_components\pl_win_gdi"                                                                       # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_media.zip        -r pl_components\pl_win_media        pl_examples\pl_win_media"                                     # new 20x32x
cmd.exe /c "scripting\7z -tzip -mx9 a pl_zip\pl_win_midi.zip         -r pl_components\pl_win_midi"                                                                      # new 20x32x


cd $CurrentDir

$EndTime          = Get-Date
$PassedTime       = $EndTime - $StartTime
$PassedSeconds    = [math]::Round($PassedTime.TotalSeconds, 0)
$PassedSecondsStr = ([string]$PassedSeconds).PadLeft(6)
Log "      ___________________________________________________"
Log "     /                                                   \"
Log "    <   Script time: $PassedSecondsStr seconds.   Job finished !!!   >"
Log "     \___________________________________________________/"
