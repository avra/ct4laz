{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_sdl2;

{$warn 5023 off : no warning about unused units}
interface

uses
  sdl2lib, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_sdl2', @Register);
end.
