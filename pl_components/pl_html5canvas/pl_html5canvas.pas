{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Html5Canvas;

{$warn 5023 off : no warning about unused units}
interface

uses
  Html5CanvasGR32, Html5CanvasInterfaces, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_Html5Canvas', @Register);
end.
