{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_MagicScript;

{$warn 5023 off : no warning about unused units}
interface

uses
  AllMagicScriptReg, msbaseobjects, mscoreconst, mscoreengine, 
  msLibrariesTree, msScriptEditor, msScriptHighlighter, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('AllMagicScriptReg', @AllMagicScriptReg.Register);
end;

initialization
  RegisterPackage('pl_MagicScript', @Register);
end.
