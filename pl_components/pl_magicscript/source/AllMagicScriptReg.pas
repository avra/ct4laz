{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit AllMagicScriptReg;

{$i magicscript.inc}

interface

uses
  Classes,
  PropEdits,
  LazarusPackageIntf,
  LResources,

  mscoreengine,
  msLibrariestree,
  msscripteditor,
  msscriptHighlighter;

procedure Register;

implementation

{$R AllMagicScriptReg.res}

procedure Register;
begin
  RegisterComponents('MagicScript',
    [
     TmscrPascalScript,
     TmscrScriptEditor,
     TmscrScriptHighlighter,
     TmscrScriptTerminal,
     TmscrRTTILibrariesTree

    ]);
end;


end.
