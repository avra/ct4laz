{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msScriptEditor;

interface

{$i magicscript.inc}

uses
  LCLType, LMessages, Buttons, LCLIntf,
  StdCtrls, Controls, Classes, Clipbrd, ComCtrls,
  SysUtils, Graphics, Forms, Menus, SynEdit;

type


TmscrScriptEditor = class(TSynEdit)
  private
  end;

TmscrScriptTerminal = class(TMemo)
  private
  end;


implementation



end.
