{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}


{$R-} {- Range-Checking }
{$H+} {- Use long strings by default }
{$B-} {- Complete Boolean Evaluation }
{$T-} {- Typed @ operator }
{$P+} {- Open string params }


{$MODE DELPHI}

{$PACKSET 1}

{$IFDEF WINDOWS}
  {$DEFINE MAGSCR_USEOLE} 
{$ENDIF}

//{$DEFINE MAGSCR_NOFORMS}
