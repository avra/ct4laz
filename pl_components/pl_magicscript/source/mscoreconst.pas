{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit mscoreconst;

interface

resourcestring

   msEUnError           = 'Unknown ERROR ???????';  
   msEUnknownType       = 'Unknown type: ';
   msELangNotFound      = 'Language ''%s'' not found';
   msEIdRedeclared      = 'Identifier redeclared: ';
   msEIncompatibleTypes = 'Incompatible types';    
   msEIndexRequired     = 'Index required';     
   msEArrayRequired     = 'Array type required';  
   msEVarRequired       = 'Variable required';
   msEUndeclaredId      = 'Undeclared identifier: ';
   msEClassRequired     = 'Class type required';
   msEStringError       = 'Strings doesn''t have properties or methods';
   msEClassError        = 'Class %s does not have a default property';
   msEClassCreateError  = 'Variable Class of type %s is NIL, NOT Created';
   msENotEnoughParams   = 'Not enough actual parameters';
   msETooManyParams     = 'Too many actual parameters';
   msELeftCantAssigned  = 'Left side cannot be assigned to';
   msEForError          = 'For loop variable must be numeric variable';
   msEEventError        = 'Event handler must be a procedure';    
   msEInvalidLanguage   = 'Invalid language definition';
   msError01           = 'Identifier expected';
   msError02           = 'Expression expected';
   msError03           = 'Statement expected';
   msError04           = ''':'' expected';
   msError05           = ''';'' expected';
   msError06           = '''.'' expected';
   msError07           = ''')'' expected';
   msError08           = ''']'' expected';
   msError09           = '''='' expected';
   msError10           = '''BEGIN'' expected';
   msError11           = '''END'' expected';
   msError12           = '''OF'' expected';
   msError13           = '''THEN'' expected';
   msError14           = '''UNTIL'' expected';
   msError15           = '''TO'' or ''DOWNTO'' expected';
   msError16           = '''DO'' expected';
   msError17           = '''FINALLY'' or ''EXCEPT'' expected';
   msError18           = '''['' expected';
   msError19           = '''..'' expected';
   msError20           = '''&gt;'' expected';

implementation


end.
