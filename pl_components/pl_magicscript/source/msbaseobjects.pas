{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msbaseobjects;

interface

{$i magicscript.inc}

uses
  SysUtils, Classes, TypInfo;

type

TmscrIdentifierCharset = set of AnsiChar;

TmscrDATAItem = class(TObject)
 private
   FData: Pointer;
   FItems: TList;
   FName: string;
   FParent: TmscrDATAItem;
   FText: string;
   function GetCount: integer;
   function GetItems(Index: integer): TmscrDATAItem;
   function GetProp(Index: string): string;
   procedure SetProp(Index: string; const Value: string);
   procedure SetParent(const Value: TmscrDATAItem);
 public
   destructor Destroy; override;
   procedure Clear;                            
   function  Root: TmscrDATAItem;
   function  Add: TmscrDATAItem;
   procedure AddItem(Item: TmscrDATAItem);
   procedure Assign(Item: TmscrDATAItem);  
   procedure InsertItem(Index: integer; Item: TmscrDATAItem);
   function  Find(const Name: string): integer;
   function  FindItem(const Name: string): TmscrDATAItem;
   function  IndexOf(Item: TmscrDATAItem): integer;
   function  PropExists(const Index: string): boolean; 
   property Data: Pointer read FData write FData;                                      
   property Name: string read FName write FName;  
   property Text: string read FText write FText;
   property Count: integer read GetCount;
   property Items[Index: integer]: TmscrDATAItem read GetItems; default;
   property Parent: TmscrDATAItem read FParent write SetParent;
   property Prop[Index: string]: string read GetProp write SetProp;
 end;

TmscrDATADocument = class(TObject)
 private
    FRoot: TmscrDATAItem;
    FAutoIndent: boolean;
 public
   constructor Create;
   destructor Destroy; override;
   procedure Clear;
   procedure SaveToFile(const FileName: string);
   procedure LoadFromFile(const FileName: string);
   procedure SaveToStream(Stream: TStream);
   procedure LoadFromStream(Stream: TStream);
   property Root: TmscrDATAItem read FRoot;
   property AutoIndent: boolean read FAutoIndent write FAutoIndent;
 end;

TmscrDATAReader = class(TObject)
 private
   FStream: TStream;
   FSize: int64;   
   FPosition: int64;
   FBuffer: PAnsiChar;
   FBufPos: integer;
   FBufEnd: integer;
   procedure ReadBuffer;
   procedure ReadItem(var Name: string; var Text: string); 
   procedure SetPosition(const Value: int64);
 public
   constructor Create(Stream: TStream);
   destructor Destroy; override;
   procedure ReadHeader;
   procedure ReadRootItem(Item: TmscrDATAItem);   
   procedure RaiseException;         
   property Size: int64 read FSize;
   property Position: int64 read FPosition write SetPosition;
 end;

TmscrDATAWriter = class(TObject)
 private
   FStream: TStream;
   FBuffer: ansistring;
   FTempStream: TStream;
   FAutoIndent: boolean;
   procedure FlushBuffer;
   procedure WriteItem(Item: TmscrDATAItem; Level: integer = 0);
   procedure WriteLn(const s: ansistring);
 public
   constructor Create(Stream: TStream);
   procedure WriteHeader;
   procedure WriteRootItem(RootItem: TmscrDATAItem);
   property TempStream: TStream read FTempStream write FTempStream;
 end;

TmscrParser = class(TObject)
 private
   FYList: TList;
   FKeywords: TStrings;
   FCaseSensitive: Boolean;
   FStringQuotes: String;
   FText: String;
   FCommentBlock1: String;
   FCommentBlock11: String;
   FCommentBlock12: String;
   FCommentBlock2: String;
   FCommentBlock21: String;
   FCommentBlock22: String;
   FCommentLine1: String;
   FCommentLine2: String;
   FHexSequence: String;
   FIdentifierCharset: TmscrIdentifierCharset;
   FSize: Integer;
   FPosition: Integer;
   FLastPosition: Integer;
   FSkipEOL: Boolean;
   FSkipSpace: Boolean;
   FSpecStrChar: Boolean;
   FSkiPChar: String;
   FUseY: Boolean;
   function  Ident: String;
   procedure SetCommentBlock1(const Value: String);
   procedure SetCommentBlock2(const Value: String);
   function  DoDigitSequence: Boolean;
   function  DoHexDigitSequence: Boolean;
   function  DoScaleFactor: Boolean;
   function  DoUnsignedInteger: Boolean;
   function  DoUnsignedReal: Boolean;
   procedure SetPosition(const Value: Integer);
   procedure SetText(const Value: String);
 public
   constructor Create;
   destructor Destroy; override;
   procedure Clear;
   procedure InitKeywords;
   procedure ConstructCharset(const s: String);
   procedure SkipSpaces;
   function  GetEOL: Boolean;
   function  GetIdent: String;
   function  GetChar: String;
   function  GetWord: String;
   function  GetNumber: String;
   function  GetString: String;
   function  GetFRString: String;
   function  GetXYPosition: String;
   function  GetPlainPosition(pt: TPoint): Integer;
   function  IsKeyWord(const s: String): Boolean;                                                    
   property Text: String read FText write SetText;  
   property Position: Integer read FPosition write SetPosition; 
   property UseY: Boolean read FUseY write FUseY;
   property Keywords: TStrings read FKeywords;
   property CaseSensitive: Boolean read FCaseSensitive write FCaseSensitive;
   property CommentBlock1: String read FCommentBlock1 write SetCommentBlock1;
   property CommentLine1: String read FCommentLine1 write FCommentLine1;
   property CommentBlock2: String read FCommentBlock2 write SetCommentBlock2;
   property CommentLine2: String read FCommentLine2 write FCommentLine2;
   property HexSequence: String read FHexSequence write FHexSequence;
   property IdentifierCharset: TmscrIdentifierCharset read FIdentifierCharset write FIdentifierCharset;
   property SkipEOL: Boolean read FSkipEOL write FSkipEOL;
   property SkiPChar: String read FSkiPChar write FSkiPChar;
   property SkipSpace: Boolean read FSkipSpace write FSkipSpace;
   property StringQuotes: String read FStringQuotes write FStringQuotes;
   property SpecStrChar: Boolean read FSpecStrChar write FSpecStrChar;
 end;

//===========================================

function msStrToXML(const s: string): string;
function msValueToXML(const Value: variant): string;
function msXMLToStr(const s: string): string;

implementation

function msXMLToStr(const s: string): string;
 var i, j, h, n: integer;
begin
 Result:= s;

 i:= 1;
 n:= Length(s);
 while i < n do
 begin
   if Result[i] = '&' then
     if (i + 3 <= n) and (Result[i + 1] = '#') then
     begin
       j:= i + 3;
       while Result[j] <> ';' do
         Inc(j);
       h:= StrToInt(Copy(Result, i + 2, j - i - 2));
       Delete(Result, i, j - i);
       Result[i]:= Chr(h);
       Dec(n, j - i);
     end else 
     if Copy(Result, i + 1, 5) = 'quot;' then
     begin   
       Delete(Result, i, 6);
       Result[i]:= '"';
       Dec(n, 6);
     end;
   Inc(i);
 end;
end;

function msStrToXML(const s: string): string;
 const SpecChars = ['<', '>', '"', #10, #13, '&'];
 var i: integer;
  //...........................
 procedure ReplaceChars(var s: string; i: integer);
 begin
   Insert('#' + IntToStr(Ord(s[i])) + ';', s, i + 1);
   s[i]:= '&';
 end;   
  //...........................
begin
 Result:= s;
 for i:= Length(s) downto 1 do
   if s[i] in SpecChars then
     if s[i] <> '&' then
       ReplaceChars(Result, i)
     else
     begin
       if Copy(s, i + 1, 5) = 'quot;' then
       begin
         Delete(Result, i, 6);
         Insert('&#34;', Result, i);
       end;
     end;
end;

function msValueToXML(const Value: variant): string;
begin
 case TVarData(Value).VType of
   varSmallint, varInteger, varByte : Result:= IntToStr(integer(Value));
   varSingle, varDouble, varCurrency: Result:= FloatToStr(double(Value));
   varDate                          : Result:= DateToStr(TDateTime(Value));
   varOleStr, varString, varVariant : Result:= msStrToXML(Value);

   varBoolean:
     if Value = True then
       Result:= '1' else
       Result:= '0';

 else
     Result:= '';
 end;
end;

//==============  TmscrDATADocument =========================

constructor TmscrDATADocument.Create;
begin
  FRoot:= TmscrDATAItem.Create;
end;

destructor TmscrDATADocument.Destroy;
begin
  FRoot.Free;
  inherited;
end;

procedure TmscrDATADocument.Clear;
begin
  FRoot.Clear;
end;

procedure TmscrDATADocument.LoadFromStream(Stream: TStream);
 var dr: TmscrDATAReader;
begin
  dr:= TmscrDATAReader.Create(Stream);
  try
    FRoot.Clear;
    dr.ReadHeader;
    dr.ReadRootItem(FRoot);
  finally
    dr.Free;
  end;
end;

procedure TmscrDATADocument.SaveToStream(Stream: TStream);
 var dw: TmscrDATAWriter;
begin
  dw:= TmscrDATAWriter.Create(Stream);
  dw.FAutoIndent:= FAutoIndent;

  try
    dw.WriteHeader;
    dw.WriteRootItem(FRoot);
  finally
    dw.Free;
  end;
end;

procedure TmscrDATADocument.LoadFromFile(const FileName: string);
 var s: TFileStream;
begin
  s:= TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
  try
    LoadFromStream(s);
  finally
    s.Free;
  end;
end;

procedure TmscrDATADocument.SaveToFile(const FileName: string);
 var s: TFileStream;
begin
  s:= TFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(s);
  finally
    s.Free;
  end;
end;

//==============  TmscrDATAItem ============================

procedure TmscrDATAItem.Clear;
begin
 if FItems <> nil then
 begin
   while FItems.Count > 0 do
     TmscrDATAItem(FItems[0]).Free;
   FItems.Free;
   FItems:= nil;
 end;
end;

destructor TmscrDATAItem.Destroy;
begin
 Clear;
 if FParent <> nil then
   FParent.FItems.Remove(Self);
 inherited;
end;


function TmscrDATAItem.GetItems(Index: integer): TmscrDATAItem;
begin
  Result:= TmscrDATAItem(FItems[Index]);
end;

function TmscrDATAItem.GetCount: integer;
begin
 if FItems = nil then
   Result:= 0 else
   Result:= FItems.Count;
end;

procedure TmscrDATAItem.SetParent(const Value: TmscrDATAItem);
begin
 if FParent <> nil then
   FParent.FItems.Remove(Self);
 FParent:= Value;
end;

function TmscrDATAItem.Add: TmscrDATAItem;
begin
 Result:= TmscrDATAItem.Create;
 AddItem(Result);
end;

procedure TmscrDATAItem.InsertItem(Index: integer; Item: TmscrDATAItem);
begin
 AddItem(Item);
 FItems.Delete(FItems.Count - 1);
 FItems.Insert(Index, Item);
end;

procedure TmscrDATAItem.AddItem(Item: TmscrDATAItem);
begin
 if FItems = nil then
   FItems:= TList.Create;

 FItems.Add(Item);
 if Item.FParent <> nil then
   Item.FParent.FItems.Remove(Item);
 Item.FParent:= Self;
end;


function TmscrDATAItem.FindItem(const Name: string): TmscrDATAItem;
 var i: integer;
begin
 i:= Find(Name);

 if i = -1 then
 begin
   Result:= Add;
   Result.Name:= Name;
 end else
 begin
   Result:= Items[i];
 end;
end;

function TmscrDATAItem.Find(const Name: string): integer;
 var i: integer;
begin
 Result:= -1;
 for i:= 0 to Count - 1 do
  if AnsiCompareText(Items[i].Name, Name) = 0 then
   begin
     Result:= i;
     break;
   end;
end;

function TmscrDATAItem.Root: TmscrDATAItem;
begin
 Result:= Self;
 while Result.Parent <> nil do
   Result:= Result.Parent;
end;

function TmscrDATAItem.GetProp(Index: string): string;
 var i: integer;
begin
 i:= Pos(' ' + AnsiUpperCase(Index) + '="', AnsiUpperCase(' ' + FText));

 if i <> 0 then
 begin
   Result:= Copy(FText, i + Length(Index + '="'), MaxInt);
   Result:= msXMLToStr(Copy(Result, 1, Pos('"', Result) - 1));
 end else
 begin
   Result:= '';
 end;
end;

function TmscrDATAItem.PropExists(const Index: string): boolean;
begin
  Result:= Pos(' ' + AnsiUpperCase(Index) + '="', ' ' + AnsiUpperCase(FText)) > 0;
end;

function TmscrDATAItem.IndexOf(Item: TmscrDATAItem): integer;
begin
 Result:= FItems.IndexOf(Item);
end;

procedure TmscrDATAItem.SetProp(Index: string; const Value: string);
 var
 i, j: integer;
 s: string;
begin
 i:= Pos(' ' + AnsiUpperCase(Index) + '="', AnsiUpperCase(' ' + FText));

 if i <> 0 then
 begin
   j:= i + Length(Index + '="');
   while (j <= Length(FText)) and (FText[j] <> '"') do
     Inc(j);
   Delete(FText, i, j - i + 1);
 end else
 begin
   i:= Length(FText) + 1;
 end;

 s:= Index + '="' + msStrToXML(Value) + '"';
 if (i > 1) and (FText[i - 1] <> ' ') then
   s:= ' ' + s;
 Insert(s, FText, i);
end;

procedure TmscrDATAItem.Assign(Item: TmscrDATAItem);
  //..............................
 procedure Compile_Assign(ItemFrom, ItemTo: TmscrDATAItem);
 var
   i: integer;
 begin
   ItemTo.Name:= ItemFrom.Name;
   ItemTo.Text:= ItemFrom.Text;
   ItemTo.Data:= ItemFrom.Data;
   for i:= 0 to ItemFrom.Count - 1 do
     Compile_Assign(ItemFrom[i], ItemTo.Add);
 end;
  //.............................
begin
 Clear;

 if Item <> nil then
   Compile_Assign(Item, Self);
end;

//==============  TmscrDATAWriter ==============

constructor TmscrDATAWriter.Create(Stream: TStream);
begin
  FStream:= Stream;
end;

procedure TmscrDATAWriter.FlushBuffer;
begin
  if FBuffer <> '' then
    FStream.Write(FBuffer[1], Length(FBuffer));
  FBuffer:= '';
end;

procedure TmscrDATAWriter.WriteLn(const s: ansistring);
begin
  if not FAutoIndent then
    Insert(s, FBuffer, MaxInt) else
    Insert(s + #13#10, FBuffer, MaxInt);

  if Length(FBuffer) > 4096 then FlushBuffer;
end;

procedure TmscrDATAWriter.WriteHeader;
begin
  WriteLn('<?xml version="1.0"?>');
end;

function Dup(n: integer): ansistring;
begin
  SetLength(Result, n);
  FillChar(Result[1], n, ' ');
end;

procedure TmscrDATAWriter.WriteItem(Item: TmscrDATAItem; Level: integer = 0);
var
  s: ansistring;
begin
  if Item.FText <> '' then
  begin
    s:= Item.FText;
    if (s = '') or (s[1] <> ' ') then
      s:= ' ' + s;

  end else
  begin
    s:= '';
  end;

  if Item.Count = 0 then
    s:= s + '/>' else
    s:= s + '>';

  if not FAutoIndent then
    s:= '<' + ansistring(Item.Name) + s else
    s:= Dup(Level) + '<' + ansistring(Item.Name) + s;

  WriteLn(s);
end;

procedure TmscrDATAWriter.WriteRootItem(RootItem: TmscrDATAItem);
 //............................
  procedure DoWrite(RootItem: TmscrDATAItem; Level: integer = 0);
  var
    i: integer;
    NeedClear: boolean;
  begin
    NeedClear:= False;
    if not FAutoIndent then
      Level:= 0;

    WriteItem(RootItem, Level);
    for i:= 0 to RootItem.Count - 1 do
      DoWrite(RootItem[i], Level + 2);

    if RootItem.Count > 0 then
      if not FAutoIndent then
        WriteLn('</' + ansistring(RootItem.Name) + '>') else
        WriteLn(Dup(Level) + '</' + ansistring(RootItem.Name) + '>');

    if NeedClear then
      RootItem.Clear;
  end;
  //..............................
begin
  DoWrite(RootItem);
  FlushBuffer;
end;

//==============  TmscrDATAReader =========================

constructor TmscrDATAReader.Create(Stream: TStream);
begin
 FStream:= Stream;
 FSize:= Stream.Size;
 FPosition:= Stream.Position;
 GetMem(FBuffer, 4096);
end;

destructor TmscrDATAReader.Destroy;
begin
 FreeMem(FBuffer, 4096);
 FStream.Position:= FPosition;
 inherited;
end;

procedure TmscrDATAReader.ReadBuffer;
begin
 FBufEnd:= FStream.Read(FBuffer^, 4096);
 FBufPos:= 0;
end;

procedure TmscrDATAReader.SetPosition(const Value: int64);
begin
 FPosition:= Value;
 FStream.Position:= Value;
 FBufPos:= 0;
 FBufEnd:= 0;
end;

procedure TmscrDATAReader.RaiseException;
begin
 raise Exception.Create('Invalid file format');
end;

procedure TmscrDATAReader.ReadHeader;
var
 s1: string;
 s2: string;
begin
 ReadItem(s1, s2);
 if Pos('?xml', s1) <> 1 then
   RaiseException;
end;

procedure TmscrDATAReader.ReadItem(var Name: string; var Text: string);
var
 ps: PAnsiChar;
 cb: byte;
 i, ic: integer;
 curpos, len: integer;
 xstate: (FindLeft, FindRight, FindComment, Done);
begin
 Text:= '';
 ic:= 0;
 xstate:= FindLeft;
 curpos:= 0;
 len:= 4096;
 SetLength(Name, len);
 ps:= @Name[1];

 while FPosition < FSize do
 begin
   if FBufPos = FBufEnd then
     ReadBuffer;
   cb:= Ord(FBuffer[FBufPos]);
   Inc(FBufPos);
   Inc(FPosition);

   if xstate = FindLeft then
   begin
     if cb = Ord('<') then
       xstate:= FindRight;
   end else
   if xstate = FindRight then
   begin

     if cb = Ord('>') then
     begin
       xstate:= Done;
       break;
     end else
     if cb = Ord('<') then
       RaiseException
     else
     begin
       ps[curpos]:= AnsiChar(Chr(cb));
       Inc(curpos);
       if (curpos = 3) and (Pos(ansistring('!--'), Name) = 1) then
       begin
         xstate:= FindComment;
         ic:= 0;
         curpos:= 0;
       end;
       if curpos >= len - 1 then
       begin
         Inc(len, 4096);
         SetLength(Name, len);
         ps:= @Name[1];
       end;
     end;

   end else
   if xState = FindComment then
   begin
     if ic = 2 then
     begin

       if cb = Ord('>') then
         xstate:= FindLeft else
         ic:= 0;

     end else
     begin

       if cb = Ord('-') then
         Inc(ic) else
         ic:= 0;

     end;
   end;
 end;

 len:= curpos;
 SetLength(Name, len);

 if xstate = FindRight then RaiseException;

 if (Name <> '') and (Name[len] = ' ') then
   SetLength(Name, len - 1);

 i:= Pos(ansistring(' '), Name);
 if i <> 0 then
 begin
   Text:= Copy(Name, i + 1, len - i);
   Delete(Name, i, len - i + 1);
 end;
end;

procedure TmscrDATAReader.ReadRootItem(Item: TmscrDATAItem);
var
  xLastName: string;
  //........................................
  function DoRead(RootItem: TmscrDATAItem): boolean;
  var
    n: integer;
    ChildItem: TmscrDATAItem;
    Done: boolean;
  begin
    Result:= False;
    ReadItem(RootItem.FName, RootItem.FText);
    xLastName:= RootItem.FName;

    if (RootItem.Name = '') or (RootItem.Name[1] = '/') then
    begin
      Result:= True;
      Exit;
    end;

    n:= Length(RootItem.Name);
    if RootItem.Name[n] = '/' then
    begin
      SetLength(RootItem.FName, n - 1);
      Exit;
    end;

    n:= Length(RootItem.Text);
    if (n > 0) and (RootItem.Text[n] = '/') then
    begin
      SetLength(RootItem.FText, n - 1);
      Exit;
    end;

    repeat
      ChildItem:= TmscrDATAItem.Create;
      Done:= DoRead(ChildItem);
      if not Done then
        RootItem.AddItem(ChildItem)
      else
        ChildItem.Free;
    until Done;

    if (xLastName <> '') and (AnsiCompareText(xLastName, '/' + RootItem.Name) <> 0) then
      RaiseException;
  end;
  //.............................
begin
  DoRead(Item);
end;

//=============== TmscrParser ==========================================

constructor TmscrParser.Create;
begin
  FKeywords:= TStringList.Create;
  TStringList(FKeywords).Sorted:= True;
  FYList:= TList.Create;
  FUseY:= True;
  Clear;
end;

destructor TmscrParser.Destroy;
begin
  FKeywords.Free;
  FYList.Free;
  inherited;
end;

procedure TmscrParser.Clear;
begin
  FSpecStrChar:= False;
  FCommentLine1:= '//';
  CommentBlock1:= '{,}';
  CommentBlock2:= '(*,*)';
  FHexSequence:= '$';
  FIdentifierCharset:= ['_', '0'..'9', 'a'..'z', 'A'..'Z'];
  FSkipChar:= '';
  FSkipEOL:= True;
  FStringQuotes:= '''';
  FSkipSpace:= True;
  FCaseSensitive:= False;
end;

procedure TmscrParser.InitKeywords;
begin
  FKeywords.Clear;
  with FKeywords do
  begin
    Add('and');
    Add('array');
    Add('begin');
    Add('break');
    Add('case');
    Add('const');
    Add('continue');
    Add('div');
    Add('do');
    Add('downto');
    Add('else');
    Add('end');
    Add('except');
    Add('exit');
    Add('finally');
    Add('for');
    Add('function');
    Add('goto');
    Add('if');
    Add('in');
    Add('is');
    Add('label');
    Add('mod');
    Add('not');
    Add('of');
    Add('or');
    Add('procedure');
    Add('program');
    Add('repeat');
    Add('shl');
    Add('shr');
    Add('then');
    Add('to');
    Add('try');
    Add('until');
    Add('uses');
    Add('var');
    Add('while');
    Add('with');
    Add('xor');
  end;

end;

procedure TmscrParser.SetCommentBlock1(const Value: String);
 var sl: TStringList;
begin
  FCommentBlock1:= Value;
  FCommentBlock11:= '';
  FCommentBlock12:= '';

  sl:= TStringList.Create;
  sl.CommaText:= FCommentBlock1;
  if sl.Count > 0 then
    FCommentBlock11:= sl[0];
  if sl.Count > 1 then
    FCommentBlock12:= sl[1];
  sl.Free;
end;

procedure TmscrParser.SetCommentBlock2(const Value: String);
 var sl: TStringList;
begin
  FCommentBlock2:= Value;
  FCommentBlock21:= '';
  FCommentBlock22:= '';

  sl:= TStringList.Create;
  sl.CommaText:= FCommentBlock2;
  if sl.Count > 0 then
    FCommentBlock21:= sl[0];
  if sl.Count > 1 then
    FCommentBlock22:= sl[1];

  sl.Free;
end;

procedure TmscrParser.SetPosition(const Value: Integer);
begin
  FPosition:= Value;
  FLastPosition:= Value;
end;

procedure TmscrParser.SetText(const Value: String);
 var i: Integer;
begin
  FText:= Value + #0;
  FLastPosition:= 1;
  FPosition:= 1;
  FSize:= Length(Value);

  if FUseY then
  begin
    FYList.Clear;
    FYList.Add(TObject(0));
    for i:= 1 to FSize do
      if FText[i] = #10 then
        FYList.Add(TObject(i));
  end;
end;

procedure TmscrParser.ConstructCharset(const s: String);
 var i: Integer;
begin
  FIdentifierCharset:= [];
  for i:= 1 to Length(s) do
    FIdentifierCharset:= FIdentifierCharset + [s[i]];
end;

function TmscrParser.GetEOL: Boolean;
begin
  SkipSpaces;

  if FText[FPosition] in [#10, #13] then
  begin
    Result:= True;
    while FText[FPosition] in [#10, #13] do
      Inc(FPosition);
  end else
  begin
    Result:= False;
  end;
end;

procedure TmscrParser.SkipSpaces;
var
  s1, s2: String;
  Flag, CLine: Boolean;
  Spaces: set of AnsiChar;
begin
  Spaces:= [#0..#32];
  if not FSkipEOL then

{$IFDEF Windows}
    Spaces:= Spaces - [#13];
{$ELSE}
    Spaces:= Spaces - [#10];
{$ENDIF}

  while (FPosition <= FSize) and (FText[FPosition] in Spaces) do
    Inc(FPosition);

  if (FPosition <= FSize) and (FSkipChar <> '') and (FText[FPosition] = FSkipChar[1]) then
  begin
    Inc(FPosition);
    GetEOL;
    SkipSpaces;
  end;

  if FPosition < FSize then
  begin
    if FCommentLine1 <> '' then
      s1:= Copy(FText, FPosition, Length(FCommentLine1)) else
      s1:= ' ';

    if FCommentLine2 <> '' then
      s2:= Copy(FText, FPosition, Length(FCommentLine2)) else
      s2:= ' ';

    if (s1 = FCommentLine1) or (s2 = FCommentLine2) then
    begin
      CLine:= (FPosition - 1 > 0) and (FText[FPosition - 1] <> #10) and not FSkipEOL;
      while (FPosition <= FSize) and (FText[FPosition] <> #10) do
      begin
        if (FText[FPosition] = {$IFDEF UNIX}#10{$ELSE}#13{$ENDIF}) and CLine then break;
        Inc(FPosition);
      end;
      SkipSpaces;
    end else
    begin
      Flag:= False;

      if FCommentBlock1 <> '' then
      begin
        s1:= Copy(FText, FPosition, Length(FCommentBlock11));
        if s1 = FCommentBlock11 then
        begin
          Flag:= True;
          s2:= FCommentBlock12;
        end;
      end;

      if not Flag and (FCommentBlock2 <> '') then
      begin
        s1:= Copy(FText, FPosition, Length(FCommentBlock21));
        if s1 = FCommentBlock21 then
        begin
          Flag:= True;
          s2:= FCommentBlock22;
        end;
      end;

      if Flag then
      begin
        Inc(FPosition, Length(s2));
        while (FPosition <= FSize) and (Copy(FText, FPosition, Length(s2)) <> s2) do
          Inc(FPosition);
        Inc(FPosition, Length(s2));
        SkipSpaces;
      end;

    end;
  end;

  FLastPosition:= FPosition;
end;

function TmscrParser.Ident: String;
begin
  if FSkipSpace then
     SkipSpaces;

  if (FText[FPosition] in FIdentifierCharset) and not (FText[FPosition] in ['0'..'9']) then
  begin
    while (FText[FPosition] in FIdentifierCharset) do
      Inc(FPosition);

    Result:= Copy(FText, FLastPosition, FPosition - FLastPosition);
  end else
  begin
    Result:= '';
  end;
end;

function TmscrParser.IsKeyWord(const s: String): Boolean;
var
  i: Integer;
begin
  if FCaseSensitive then
  begin
    Result:= False;
    for i:= 0 to FKeywords.Count - 1 do
    begin
      Result:= FKeywords[i] = s;
      if Result then break;
    end;
  end else
  begin
    Result:= FKeywords.IndexOf(s) <> -1;
  end;
end;

function TmscrParser.GetIdent: String;
begin
  Result:= Ident;
  if IsKeyWord(Result) then
    Result:= '';
end;

function TmscrParser.GetWord: String;
begin
  Result:= Ident;
end;

function TmscrParser.GetChar: String;
begin
  if FText[FPosition] in ['!', '@', '#', '$', '%', '^', '&', '|', '\',
    '.', ',', ':', ';', '?', '''', '"', '~', '`', '_', '[', ']', '{', '}',
    '(', ')', '+', '-', '*', '/', '=', '<', '>'] then

  begin
    Result:= FText[FPosition];
    Inc(FPosition);
  end else
  begin
    Result:= '';
  end;
end;

function TmscrParser.GetString: String;
var
  Flag: Boolean;
  Str: String;
  FError: Boolean;
  FCpp: Boolean;

  //....................................
  function DoQuotedString: Boolean;
  var
    i, j: Integer;
  begin
    Result:= False;
    i:= FPosition;

    if FText[FPosition] = FStringQuotes[1] then
    begin
      repeat
        Inc(FPosition);

        if FCpp and (FText[FPosition] = '\') then
        begin

          case Lowercase(FText[FPosition + 1]) of
            'n':  begin
                    Str:= Str + #10;
                    Inc(FPosition);
                  end;

            'r': begin
                    Str:= Str + #13;
                    Inc(FPosition);
                  end;

            'x': begin
                    Inc(FPosition, 2);
                    j:= FPosition;
                    Result:= DoHexDigitSequence;

                    if Result then
                      Str:= Str + Chr(StrToInt('$' + Copy(FText, j, FPosition - j))) else
                      FPosition:= j;

                    Dec(FPosition);
                  end;

          else
              begin
                Str:= Str + FText[FPosition + 1];
                Inc(FPosition);
              end;
          end;

        end else
        if FText[FPosition] = FStringQuotes[1] then
        begin

          if not FCpp and (FText[FPosition + 1] = FStringQuotes[1]) then
          begin
            Str:= Str + FStringQuotes[1];
            Inc(FPosition);
          end else
          begin
            break;
          end;

        end else
        begin
          Str:= Str + FText[FPosition];
        end;

      until FText[FPosition] in [#0..#31] - [#9];

      if FText[FPosition] = FStringQuotes[1] then
      begin
        Inc(FPosition);
        Result:= True;
      end else
      begin
        FPosition:= i;
      end;

    end;
  end;
  //..................................
  function DoControlString: Boolean;
  var
    i: Integer;
  begin
    Result:= False;
    i:= FPosition;

    if FText[FPosition] = '#' then
    begin
      Inc(FPosition);
      Result:= DoUnsignedInteger;
      if Result then
        Str:= Chr(StrToInt(Copy(FText, i + 1, FPosition - i - 1))) else
        FPosition:= i;
    end;
  end;    
  //..................................

{$HINTS OFF}
begin
  Result:= '';
  if FSkipSpace then
    SkipSpaces;
  Flag:= True;
  FError:= False;
  FCpp:= {FStringQuotes = '"'} FSpecStrChar;

  repeat
    Str:= '';
    if DoQuotedString or DoControlString then
      Result:= Result + Str
    else
    begin
      FError:= Flag;
      break;
    end;

    Flag:= False;
  until False;

  if not FError then
    Result:= '''' + Result + '''';
end;
{$HINTS ON}

function TmscrParser.DoDigitSequence: Boolean;
begin
  Result:= False;

  if FText[FPosition] in ['0'..'9'] then
  begin
    while FText[FPosition] in ['0'..'9'] do
      Inc(FPosition);
    Result:= True;
  end;
end;

function TmscrParser.DoHexDigitSequence: Boolean;
begin
  Result:= False;

  if FText[FPosition] in ['0'..'9', 'a'..'f', 'A'..'F'] then
  begin
    while FText[FPosition] in ['0'..'9', 'a'..'f', 'A'..'F'] do
      Inc(FPosition);
    Result:= True;
  end;
end;

function TmscrParser.DoUnsignedInteger: Boolean;
var
  Pos1: Integer;
  s: String;
begin
  Pos1:= FPosition;
  s:= Copy(FText, FPosition, Length(FHexSequence));

  if s = FHexSequence then
  begin
    Inc(FPosition, Length(s));
    Result:= DoHexDigitSequence;
  end else
  begin
    Result:= DoDigitSequence;
  end;

  if not Result then
    FPosition:= Pos1;
end;

function TmscrParser.DoUnsignedReal: Boolean;
var
  Pos1, Pos2: Integer;
begin
  Pos1:= FPosition;
  Result:= DoUnsignedInteger;

  if Result then
  begin
    if FText[FPosition] = '.' then
    begin
      Inc(FPosition);
      Result:= DoDigitSequence;
    end;

    if Result then
    begin
      Pos2:= FPosition;
      if not DoScaleFactor then
        FPosition:= Pos2;
    end;
  end;

  if not Result then
    FPosition:= Pos1;
end;

function TmscrParser.DoScaleFactor: Boolean;
begin
  Result:= False;

  if FText[FPosition] in ['e', 'E'] then
  begin
    Inc(FPosition);

    if FText[FPosition] in ['+', '-'] then
       Inc(FPosition);

    Result:= DoDigitSequence;
  end;
end;

function TmscrParser.GetNumber: String;
 var Pos1: Integer;
begin
  Result:= '';
  if FSkipSpace then
    SkipSpaces;
  Pos1:= FPosition;

  if DoUnsignedReal or DoUnsignedInteger then
    Result:= Copy(FText, FLastPosition, FPosition - FLastPosition) else
    FPosition:= Pos1;

  if FHexSequence <> '$' then
    while Pos(FHexSequence, Result) <> 0 do
    begin
      Pos1:= Pos(FHexSequence, Result);
      Delete(Result, Pos1, Length(FHexSequence));
      Insert('$', Result, Pos1);
    end;
end;

function TmscrParser.GetFRString: String;
var
  i, c: Integer;
  fl1, fl2: Boolean;
begin
  Result:= '';
  i:= FPosition;
  fl1:= True;
  fl2:= True;
  c:= 1;

  Dec(FPosition);

  repeat
    Inc(FPosition);

    if fl1 and fl2 then

      if FText[FPosition] in ['<', '['] then
        Inc(c)
      else
      if FText[FPosition] in ['>', ']'] then
        Dec(c);

    if fl1 then
      if FText[FPosition] = '"' then
        fl2:= not fl2;

    if fl2 then
      if FText[FPosition] = '''' then
        fl1:= not fl1;

  until (c = 0) or (FPosition >= Length(FText));

  Result:= Copy(FText, i, FPosition - i);
end;

function TmscrParser.GetXYPosition: String;
var
  i, i0, i1, c, pos, X, Y: Integer;
begin
  i0:= 0;
  i1:= FYList.Count - 1;

  while i0 <= i1 do
  begin
    i:= (i0 + i1) div 2;
    pos:= Integer(FYList[i]);

    if pos = FPosition then
      c:= 0
    else
    if pos > FPosition then
      c:= 1
    else
      c:= -1;

    if c < 0 then
      i0:= i + 1
    else
    begin
      i1:= i - 1;
      if c = 0 then
        i0:= i;
    end;
  end;

  X:= 1;
  Y:= i0;
  i:= Integer(FYList[i0 - 1]) + 1;

  while i < FPosition do
  begin
    Inc(i);
    Inc(X);
  end;
  Result:= IntToStr(Y) + ':' + IntToStr(X)
end;

function TmscrParser.GetPlainPosition(pt: TPoint): Integer;
var
  i: Integer;
begin
  Result:= -1;
  i:= pt.Y - 1;
  if (i >= 0) and (i < FYList.Count) then
    Result:= Integer(FYList[i]) + pt.X;
end;


//=========================================================

end.
