{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit mscoreengine;

interface

{$i magicscript.inc}

uses
  SysUtils, Classes, Variants, SyncObjs,
  Dialogs, TypInfo, lazutf8,
  Controls, StdCtrls, Forms,
  msbaseobjects,
  mscoreconst;


Const
  cnCatStr    = 'catString';
  cnCatDate   = 'catDate';
  cnCatConv   = 'catConv';
  cnCatFormat = 'catFormat';
  cnCatMath   = 'catMath';
  cnCatOther  = 'catOther';

type
  fmscrInteger = PtrInt;

TmscrStatement    = class;
TmscrVarForBase   = class;
TmscrVarForClass  = class;
TmscrVarForRecord = class;
TmscrVarForProc   = class;
TmscrVarForMethod = class;  
TmscrVarForParamItem = class;
TmscrEngProperty  = class;
TmscrDScannerItem = class;   
TmscrDScanner     = class;
TmscrExpressionBase = class;
TmscrSetExpression = class;  
TmscrScript       = class;


TmscrEmitOp = (mseoNone, mseoCreate, mseoFree);

TmscrDScannerKind = (msskOther, msskVariable, msskStringArray, msskArray);

TmscrVarType = (msvtInt, msvtBool, msvtFloat, msvtChar, msvtString, msvtClass, msvtRecord,
                  msvtArray, msvtVariant, msvtEnum, msvtConstructor, msvtInt64);

TmscrOperatorType = (msopNone, msopGreat, msopLess, msopLessEq, msopGreatEq, msopNonEq, msopEq,
                       msopPlus, msopMinus, msopOr, msopXor, msopMul, msopDivFloat, msopDivInt, msopMod, msopAnd,
                       msopShl, msopShr, msopLeftBracket, msopRightBracket, msopNot, msopUnMinus, msopIn, msopIs);

TmscrVarRecArray = array of TVarRec;

TmscrTypeRec = packed record
     Typ: TmscrVarType;
     TypeName: String[64];
  end;


TmscrRecGetValueElement = function(aInstance: Pointer; aTypeInfo: PTypeInfo;  const aPropName: String): Variant of object;
TmscrRecSetValueElement = procedure(aInstance: Pointer; aTypeInfo: PTypeInfo; const aPropName: String; aValue: Variant) of object;

TmscrClassGetProperty = function(aInstance: TObject; aClassType: TClass; const aPropName: String; aCallVar: TmscrEngProperty): Variant of object;
TmscrClassSetProperty = procedure(aInstance: TObject; aClassType: TClass; const aPropName: String; aValue: Variant; aCallVar: TmscrEngProperty) of object;

TmscrCallMethod = function(aInstance: TObject; aClassType: TClass; const aMethodName: String; aCallVar: TmscrVarForMethod): Variant of object;

TmscrRunLineEvent = procedure(aSender: TmscrScript; const aUnitName, aSrcPosition: String) of object;
TmscrGetUnitEvent = procedure(aSender: TmscrScript;const aUnitName: String; var aUnitText: String) of object;
TmscrGetVariableValueEvent = function(aVarName: String; aVarTyp: TmscrVarType; OldValue: Variant): Variant of object;


TmscrItemList = class(TObject)
 protected
   FItems: TList;
 protected
    procedure Clear; virtual;
 public
   constructor Create;
   destructor Destroy; override;
   function Count: Integer;
    procedure Add(Item: TObject);
    procedure Remove(Item: TObject);
 end;

TmscrScript = class(TComponent)
 protected
   FCalledBreak: Boolean;
   FCalledContinue: Boolean;
   FCalledExit: Boolean;
   FAddedBy: TObject;
   FOnGetFinalUnit: TmscrGetUnitEvent;
   FOnGetUnit: TmscrGetUnitEvent;
   FOnRunLine: TmscrRunLineEvent;
   FOnGetVarValue: TmscrGetVariableValueEvent;
   FParent: TmscrScript;
   FRTTIAdded: Boolean;
   FUseExtendCharset: Boolean;
   FItems: TStringList;
   FLines: TStrings;
   FMacros: TStrings;
   FMainProj: Boolean;
   FStatement: TmscrStatement;
   FTerminated: Boolean;
   FUnitLines: TStringList;
   FIncludePath: TStrings;
   FUseClassLateBinding: Boolean;
   FEvaluteRiseError: Boolean;
   FClearLocalVars: Boolean;
   FSyntaxType: String;
   FErrorMsg: String;
   FErrorPos: String;
   FErrorUnit: String;
   FLastSrcPosition : String;
   FProjName : String;    
   FProjRunning: TmscrScript;
   FIsRunning: Boolean;
   function  GetItem(Index: Integer): TmscrVarForBase;
   procedure RunLine(const UnitName, Index: String);
   function  GetVariables(Index: String): Variant;
   procedure SetVariables(Index: String; const Value: Variant);
   procedure SetLines(const Value: TStrings);
   function  GetProgName: String;
   procedure SetProgName(const Value: String);
 public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   procedure Add(const Name: String; Item: TObject);
   procedure AddCodeLine(const UnitName, APos: String);
   procedure AddRTTI;
   procedure Remove(Item: TObject);
   procedure RemoveItems(Owner: TObject);
   procedure Clear;
   procedure ClearItems(Owner: TObject);
   procedure ClearRTTI;
   function  Count: Integer;

   function  Register_Class(AClass: TClass; const ParentClassStr: String): TmscrVarForClass; dynamic;   
   procedure Register_Object(const Name: String; Obj: TObject); dynamic;       
   procedure Register_Component(Form: TComponent); dynamic;
   procedure Register_Form(Form: TComponent); dynamic;
   function  Register_Record(const aTypeName: String; pti: PTypeInfo): TmscrVarForRecord; dynamic;
   procedure Register_Const(const Name, Typ: String; const Value: Variant); dynamic;
   procedure Register_Enum(const Typ, Names: String); dynamic;
   procedure Register_EnumSet(const Typ, Names: String); dynamic;
   procedure Register_Method(const Syntax: String; CallEvent: TmscrCallMethod; const Category: String = ''; const Description: String = ''); dynamic;
   procedure Register_Variable(const Name, Typ: String; const Value: Variant); dynamic;
   procedure Register_Type(const TypeName: String; ParentType: TmscrVarType); dynamic;

   function  CallFunction(const Name: String; const Params: Variant; sGlobal: Boolean = false): Variant;
   function  CallFunction1(const Name: String; var Params: Variant; sGlobal: Boolean = false): Variant;
   function  CallFunction2(const Func: TmscrVarForProc; const Params: Variant): Variant;
   function  Compile: Boolean;
   procedure Execute;
   function  Run: Boolean;
   procedure Terminate;
   function  Evaluate(const Expression: String): Variant;
   function  IsExecutableLine(LineN: Integer; const UnitName: String = ''): Boolean;
   function  SetFinalCode(Stream: TStream): Boolean;
   function  GetFinalCode(Stream: TStream): Boolean;

   function Find(const Name: String): TmscrVarForBase;
   function FindClass(const Name: String): TmscrVarForClass; 
   function FindRecord(const Name: String): TmscrVarForRecord;
   function FindLocal(const Name: String): TmscrVarForBase;

   property AddedBy: TObject read FAddedBy write FAddedBy;
   property ClearLocalVars: Boolean read FClearLocalVars write FClearLocalVars;
   property ErrorMsg: String read FErrorMsg write FErrorMsg;
   property ErrorPos: String read FErrorPos write FErrorPos;
   property ErrorUnit: String read FErrorUnit write FErrorUnit;
   property UseExtendCharset: Boolean read FUseExtendCharset write FUseExtendCharset;
   property Items[Index: Integer]: TmscrVarForBase read GetItem;
   property Macros: TStrings read FMacros;
   property MainProj: Boolean read FMainProj write FMainProj;
   property Parent: TmscrScript read FParent write FParent;
   property ProjRunning: TmscrScript read FProjRunning;
   property ProjName: String read GetProgName write SetProgName;
   property Statement: TmscrStatement read FStatement;
   property Variables[Index: String]: Variant read GetVariables write SetVariables;
   property IncludePath: TStrings read FIncludePath;
   property UseClassLateBinding: Boolean read FUseClassLateBinding write FUseClassLateBinding;
   property EvaluteRiseError: Boolean read FEvaluteRiseError;
   property IsRunning: Boolean read FIsRunning;
  published
   property Lines: TStrings read FLines write SetLines;
   property SyntaxType: String read FSyntaxType write FSyntaxType;
   property OnGetFinalUnit: TmscrGetUnitEvent read FOnGetFinalUnit write FOnGetFinalUnit;
   property OnGetUnit: TmscrGetUnitEvent read FOnGetUnit write FOnGetUnit;
   property OnRunLine: TmscrRunLineEvent read FOnRunLine write FOnRunLine;
   property OnGetVarValue: TmscrGetVariableValueEvent read FOnGetVarValue write FOnGetVarValue;
 end;

TmscrStatement = class(TmscrItemList)
 private
   FScrProject: TmscrScript;
   FSrcPosition: String;
   FUnitName: String;
   function GetItem(Index: Integer): TmscrStatement;
   procedure RunLine;
 public
   constructor Create(aProject: TmscrScript; const UnitName, SrcPosition: String); virtual;
   procedure Execute; virtual;
   property Items[Index: Integer]: TmscrStatement read GetItem;
 end;

TmscrCodeStmAssign_Base = class(TmscrStatement)
 private
   FDScanner: TmscrDScanner;
   FExpression: TmscrExpressionBase;
   FVar: TmscrVarForBase;
   FExpr: TmscrVarForBase;
 public
   destructor Destroy; override;
   procedure Execute; override;
   procedure Optimize;
   property DScanner: TmscrDScanner read FDScanner write FDScanner;
   property Expression: TmscrExpressionBase read FExpression write FExpression;
 end;

TmscrCodeStmAssign_Plus = class(TmscrCodeStmAssign_Base)
 public
   procedure Execute; override;
 end;

TmscrCodeStmAssign_Minus = class(TmscrCodeStmAssign_Base)
 public
   procedure Execute; override;
 end;

TmscrCodeStmAssign_Mul = class(TmscrCodeStmAssign_Base)
 public
   procedure Execute; override;
 end;

TmscrCodeStmAssign_Div = class(TmscrCodeStmAssign_Base)
 public
   procedure Execute; override;
 end;

TmscrCodeStm_Call = class(TmscrStatement)
 private
   FDScanner: TmscrDScanner;
   FModificator: String;
 public
   destructor Destroy; override;
   procedure Execute; override;
   property DScanner: TmscrDScanner read FDScanner write FDScanner;
   property Modificator: String read FModificator write FModificator;
 end;

TmscrCodeStm_If = class(TmscrStatement)
 private
   FCondition: TmscrExpressionBase;
   FElsesrcstm: TmscrStatement;
 public
   constructor Create(aProject: TmscrScript; const UnitName, SrcPosition: String); override;
   destructor Destroy; override;
   procedure Execute; override;
   property Condition: TmscrExpressionBase read FCondition write FCondition;
   property Elsesrcstm: TmscrStatement read FElsesrcstm write FElsesrcstm;
 end;

TmscrCaseSelector = class(TmscrStatement)
 private
   FSetExpression: TmscrSetExpression;
 public
   destructor Destroy; override;
   function Check(const Value: Variant): Boolean;
   property SetExpression: TmscrSetExpression read FSetExpression write FSetExpression;
 end;

TmscrCodeStm_Case = class(TmscrStatement)
 private
   FCondition: TmscrExpressionBase;
   FElsesrcstm: TmscrStatement;
 public
   constructor Create(aProject: TmscrScript; const UnitName, SrcPosition: String); override;
   destructor Destroy; override;
   procedure Execute; override;
   property Condition: TmscrExpressionBase read FCondition write FCondition;
   property Elsesrcstm: TmscrStatement read FElsesrcstm write FElsesrcstm;
 end;

TmscrCodeStm_Repeat = class(TmscrStatement)
 private
   FCondition: TmscrExpressionBase;
   FInverseCondition: Boolean;
 public
   destructor Destroy; override;
   procedure Execute; override;
   property Condition: TmscrExpressionBase read FCondition write FCondition;
   property InverseCondition: Boolean read FInverseCondition write FInverseCondition;
 end;

TmscrCodeStm_While = class(TmscrStatement)
 private
   FCondition: TmscrExpressionBase;
 public
   destructor Destroy; override;
   procedure Execute; override;
   property Condition: TmscrExpressionBase read FCondition write FCondition;
 end;

TmscrCodeStm_For = class(TmscrStatement)
 private
   FValueBegin: TmscrExpressionBase;  
   FValueEnd  : TmscrExpressionBase;
   FDown: Boolean;
   FVariable: TmscrVarForBase;
 public
   destructor Destroy; override;
   procedure Execute; override;
   property ValueBegin: TmscrExpressionBase read FValueBegin write FValueBegin;
   property Down: Boolean read FDown write FDown;
   property ValueEnd: TmscrExpressionBase read FValueEnd write FValueEnd;
   property Variable: TmscrVarForBase read FVariable write FVariable;
 end;

TmscrCodesrcstmry = class(TmscrStatement)
 private
   FIsExcept: Boolean;
   FExceptSrcStm: TmscrStatement;
 public
   constructor Create(aProject: TmscrScript; const UnitName, SrcPosition: String); override;
   destructor Destroy; override;
   procedure Execute; override;
   property IsExcept: Boolean read FIsExcept write FIsExcept;
   property ExceptSrcStm: TmscrStatement read FExceptSrcStm write FExceptSrcStm;
 end;

TmscrCodeStm_Break = class(TmscrStatement)
 public
   procedure Execute; override;
 end;

TmscrCodeStm_Continue = class(TmscrStatement)
 public
   procedure Execute; override;
 end;

TmscrCodeStm_Exit = class(TmscrStatement)
 public
   procedure Execute; override;
 end;

TmscrCodeStm_With = class(TmscrStatement)
 private
   FDScanner: TmscrDScanner;
   FVariable: TmscrVarForBase;
 public
   destructor Destroy; override;
   procedure Execute; override;
   property DScanner: TmscrDScanner read FDScanner write FDScanner;
   property Variable: TmscrVarForBase read FVariable write FVariable;
  end;

TmscrVarForBase = class(TmscrItemList)
 private
   FName: String;
   FAddedBy: TObject;
   FNeedResult: Boolean;
   FRefItem: TmscrVarForBase;
   FSrcPosition: String;
   FSourceUnit: String;
   FTyp: TmscrVarType;
   FTypeName: String;
   FUppercaseName: String;
   FValue: Variant;
   FIsMacro: Boolean;
   FIsReadOnly: Boolean;
   FOnGetVarValue: TmscrGetVariableValueEvent;
   function GetParam(Index: Integer): TmscrVarForParamItem;
   function GetPValue: PVariant;
 protected
   procedure SetValue(const Value: Variant); virtual;
   function  GetValue: Variant; virtual;
 public
   constructor Create(const AName: String; ATyp: TmscrVarType; const ATypeName: String);
   function GetFullTypeName: String;
   function GetNumberOfRequiredParams: Integer;
   property AddedBy: TObject read FAddedBy write FAddedBy;
   property IsMacro: Boolean read FIsMacro write FIsMacro;
   property IsReadOnly: Boolean read FIsReadOnly write FIsReadOnly;
   property Name: String read FName;
   property NeedResult: Boolean read FNeedResult write FNeedResult;
   property Params[Index: Integer]: TmscrVarForParamItem read GetParam; default;
   property PValue: PVariant read GetPValue;
   property RefItem: TmscrVarForBase read FRefItem write FRefItem;
   property SrcPosition: String read FSrcPosition write FSrcPosition;
   property SourceUnit: String read FSourceUnit write FSourceUnit;
   property Typ: TmscrVarType read FTyp write FTyp;
   property TypeName: String read FTypeName write FTypeName;
   property Value: Variant read GetValue write SetValue;
   property OnGetVarValue: TmscrGetVariableValueEvent read FOnGetVarValue write FOnGetVarValue;
  end;

TmscrVarForVariable = class(TmscrVarForBase)
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 end;

TmscrVarForType = class(TmscrVarForBase)
 end;

TmscrVarForString = class(TmscrVarForVariable)
 private
   FStr: String;
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 end;

TmscrVarForParamItem = class(TmscrVarForBase)
 private
   FIsOptional: Boolean;
   FIsVarParam: Boolean;
   FDefValue: Variant;
 public
   constructor Create(const AName: String; ATyp: TmscrVarType; const ATypeName: String; AIsOptional, AIsVarParam: Boolean);
   property DefValue: Variant read FDefValue write FDefValue;
   property IsOptional: Boolean read FIsOptional;
   property IsVarParam: Boolean read FIsVarParam;
 end;

TmscrEngLocalVariables = class(TObject)
 protected
   FValue: Variant;
   FVariableLink :TmscrVarForBase;
 end;

TmscrVarForProc = class(TmscrVarForBase)
 private
   FExecuting: Boolean;
   FIsFunc: Boolean;
   FScrProject: TmscrScript;
   FVarsStack: TList;
 protected
   function GetValue: Variant; override;
 public
   constructor Create(const AName: String; ATyp: TmscrVarType; const ATypeName: String; AParent: TmscrScript; AIsFunc: Boolean = True);
   destructor Destroy; override;
   function  SaveLocalVariables: Integer;
   procedure RestoreLocalVariables(StackIndex: Integer; bSkipVarParams: Boolean = False; dItem: TmscrDScannerItem = nil);
   property Executing: Boolean read FExecuting;
   property Project: TmscrScript read FScrProject;
   property IsFunc: Boolean read FIsFunc;
  end;

TmscrExpressionBase = class(TmscrVarForBase)
  end;

TmscrEngCustom = class(TmscrVarForBase)
 private
   FParentRef: TmscrVarForBase;
   FParentValue: Variant;
   FScrProject: TmscrScript;
 public
   property aProj: TmscrScript read FScrProject write FScrProject;
   property ParentRef: TmscrVarForBase read FParentRef write FParentRef;
   property ParentValue: Variant read FParentValue write FParentValue;
  end;

TmscrEngArray = class(TmscrEngCustom)
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   constructor Create(const AName: String; DimCount: Integer; Typ: TmscrVarType; const TypeName: String);
   destructor Destroy; override;
  end;

TmscrEngString = class(TmscrEngCustom)
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   constructor Create;
  end;

TmscrEngProperty = class(TmscrEngCustom)
 private
   FClassRef: TClass;
   FIsPublished: Boolean;
   FOnGetValue: TmscrClassGetProperty;
   FOnSetValue: TmscrClassSetProperty;
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   property IsPublished: Boolean read FIsPublished;
   property OnGetValue: TmscrClassGetProperty read FOnGetValue write FOnGetValue;
   property OnSetValue: TmscrClassSetProperty read FOnSetValue write FOnSetValue;
  end;

TmscrEngRecElement = class(TmscrEngCustom)
 private
   FOnGetValue: TmscrRecGetValueElement;
   FOnSetValue: TmscrRecSetValueElement;
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   property OnGetValue: TmscrRecGetValueElement read FOnGetValue write FOnGetValue;
   property OnSetValue: TmscrRecSetValueElement read FOnSetValue write FOnSetValue;
  end;

TmscrVarForMethod = class(TmscrEngCustom)
 private
   FCategory: String;
   FClassRef: TClass;
   FDescription: String;
   FIndexMethod: Boolean;
   FOnCall: TmscrCallMethod;
   FSetValue: Variant;
   FSyntax: String;
   FVarArray: Variant;
   function  GetVParam(Index: Integer): Variant;
   procedure SetVParam(Index: Integer; const Value: Variant);
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   constructor Create(const Syntax: String; Script: TmscrScript);
   destructor Destroy; override;
   property Params[Index: Integer]: Variant read GetVParam write SetVParam; default;
   property Category: String read FCategory write FCategory;
   property Description: String read FDescription write FDescription;
   property IndexMethod: Boolean read FIndexMethod;
   property Syntax: String read FSyntax;
   property OnCall: TmscrCallMethod read FOnCall write FOnCall;
  end;

TmscrEngComponent = class(TmscrEngCustom)
 private
   FComponent: TComponent;
 protected
   function GetValue: Variant; override;
 public
   constructor Create(Component: TComponent);
  end;

TmscrCustomEvent = class(TObject)
 private
   FInstance: TObject;
   FHandler: TmscrVarForProc;
 protected
   procedure CallHandler(Params: array of const);
 public
   constructor Create(AObject: TObject; AHandler: TmscrVarForProc); virtual;
   function GetMethod: Pointer; virtual; abstract;
   property Handler: TmscrVarForProc read FHandler;
   property Instance: TObject read FInstance;
  end;
TmscrCustomEventClass = class of TmscrCustomEvent;

TmscrEngEvent = class(TmscrEngCustom)
 private
   FEvent: TmscrCustomEventClass;
   FClassRef: TClass;
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   constructor Create(const Name: String; AEvent: TmscrCustomEventClass);
  end;

TmscrVarForClass = class(TmscrVarForBase)
 private
   FParentClassStr: String;
   FClassRef: TClass;
   FDefProperty: TmscrEngCustom;
   FMembers: TmscrItemList;
   FScrProject: TmscrScript;
   procedure AddComponent(c: TComponent);
   procedure AddPublishedProperties(AClass: TClass);
   function  GetMembers(Index: Integer): TmscrEngCustom;
   function  GetMembersCount: Integer;
 protected
   function GetValue: Variant; override;
 public
   constructor Create(AClass: TClass; const ParentClassStr: String);
   destructor Destroy; override;

   procedure Register_Constructor(Syntax: String; CallEvent: TmscrCallMethod);
   procedure Register_Property(const Name, Typ: String; GetEvent: TmscrClassGetProperty; SetEvent: TmscrClassSetProperty = nil);
   procedure Register_DefaultProperty(const Name, Params, Typ: String; CallEvent: TmscrCallMethod; AReadOnly: Boolean = False);
   procedure Register_IndexProperty(const Name, Params, Typ: String; CallEvent: TmscrCallMethod; AReadOnly: Boolean = False);
   procedure Register_Method(const Syntax: String; CallEvent: TmscrCallMethod); overload;
   procedure Register_Event(const Name: String; AEvent: TmscrCustomEventClass);

   function Find(const Name: String): TmscrEngCustom;
   property ClassRef: TClass read FClassRef;
   property ParentClassStr: String read FParentClassStr;
   property DefProperty: TmscrEngCustom read FDefProperty;
   property MembersCount: Integer read GetMembersCount;
   property Members[Index: Integer]: TmscrEngCustom read GetMembers;
  end;

TmscrVarForRecord = class(TmscrVarForBase)
 protected
   FTypeInfoStr: String;
   FTypeInfo: PTypeInfo;
   FRecData:Pointer;
   FRecDataSize:Int64;
   FDefProperty: TmscrEngCustom;
   FMembers: TmscrItemList;
   FScrProject: TmscrScript;
   procedure AddComponent(c: TComponent);
   function  GetMembers(Index: Integer): TmscrEngCustom;
   function  GetMembersCount: Integer;
 public
   constructor Create(ATypeInfo: PTypeInfo; const aTypeInfoStr: String);
   destructor Destroy; override;
                                                                                           
   procedure Register_Element(const Name, Typ: String; GetEvent: TmscrRecGetValueElement; SetEvent: TmscrRecSetValueElement = nil);

   function Find(const Name: String): TmscrEngCustom;
   property TypeInfo: PTypeInfo read FTypeInfo;
   property TypeInfoStr: String read FTypeInfoStr;
   property DefProperty: TmscrEngCustom read FDefProperty;
   property MembersCount: Integer read GetMembersCount;
   property Members[Index: Integer]: TmscrEngCustom read GetMembers;
  end;

TmscrDScannerItem = class(TmscrItemList)
 private
   FRef: TmscrVarForBase;
   FFlag: Boolean;
   FSrcPosition: String;
   function GetItem(Index: Integer): TmscrExpressionBase;
 public
   property Items[Index: Integer]: TmscrExpressionBase read GetItem; default;
   property Flag: Boolean read FFlag write FFlag;
   property Ref: TmscrVarForBase read FRef write FRef;
   property SrcPosition: String read FSrcPosition write FSrcPosition;
  end;

TmscrDScanner = class(TmscrVarForBase)
 private
   FKind: TmscrDScannerKind;
   FMainProj: TmscrScript;
   FScrProject: TmscrScript;
   FRef1: TmscrVarForBase;
   FRef2: TmscrDScannerItem;
   FLateBindingDATASource: TmscrDATAItem;
   procedure CheckLateBinding;
   function DoCalc(const AValue: Variant; Flag: Boolean): Variant;
   function GetItem(Index: Integer): TmscrDScannerItem;
 protected
   function  GetValue: Variant; override;
   procedure SetValue(const Value: Variant); override;
 public
   constructor Create(aProject: TmscrScript);
   destructor Destroy; override;
   procedure Borrow(ADScanner: TmscrDScanner);
   procedure Finalize;
   property Items[Index: Integer]: TmscrDScannerItem read GetItem; default;
   property Kind: TmscrDScannerKind read FKind;
   property LateBindingDATASource: TmscrDATAItem read FLateBindingDATASource write FLateBindingDATASource;
  end;

TmscrVarForVariableDScanner = class(TmscrDScanner)
 protected
   function  GetValue: Variant; override;
   procedure SetValue(const Value: Variant); override;
  end;

TmscrStringDScanner = class(TmscrDScanner)
 protected
   function  GetValue: Variant; override;
   procedure SetValue(const Value: Variant); override;
  end;

TmscrArrayDScanner = class(TmscrDScanner)
 protected
   function  GetValue: Variant; override;
   procedure SetValue(const Value: Variant); override;
  end;

TmscrSetExpression = class(TmscrVarForBase)
 private
   function GetItem(Index: Integer): TmscrExpressionBase;
 protected
   function GetValue: Variant; override;
 public
   function Check(const Value: Variant): Boolean;
   property Items[Index: Integer]: TmscrExpressionBase read GetItem;
  end;

TmscrRTTILibrary = class(TObject)
 private
   FScript: TmscrScript;
 public
   constructor Create(AScript: TmscrScript); virtual;
   property Script: TmscrScript read FScript;
  end;

//============ System Events ========================
type
TmscrNotifyEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject);
   function  GetMethod: Pointer; override;
 end;

TmscrMouseEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
   function  GetMethod: Pointer; override;
 end;

TmscrMouseMoveEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; Shift: TShiftState; X, Y: Integer);
   function  GetMethod: Pointer; override;
 end;

TmscrKeyEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; var Key: Word; Shift: TShiftState);
   function  GetMethod: Pointer; override;
 end;

TmscrKeyPressEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; var Key: Char);
   function  GetMethod: Pointer; override;
 end;

TmscrCloseEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; var Action: TCloseAction);
   function  GetMethod: Pointer; override;
 end;

TmscrCloseQueryEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; var CanClose: Boolean);
   function  GetMethod: Pointer; override;
 end;

TmscrCanResizeEvent = class(TmscrCustomEvent)
 public
   procedure DoEvent(Sender: TObject; var NewWidth, NewHeight: Integer; var Resize: Boolean);
   function  GetMethod: Pointer; override;
 end;

//=======================================

TmscrExpressionNode = class(TmscrVarForBase)
 private
   FParent,FLeft, FRight: TmscrExpressionNode;
   procedure AddNode(Node: TmscrExpressionNode);
   procedure RemoveNode(Node: TmscrExpressionNode);
 public
   destructor Destroy; override;
   function Priority: Integer; virtual; abstract;
 end;

TmscrActionNode = class(TmscrExpressionNode)
 public
   constructor Create(const AValue: Variant);
   function Priority: Integer; override;
 end;

TmscrActionBase = class(TmscrExpressionNode)
 private
   FOp: TmscrOperatorType;
   FOptimizeInt: Boolean;
   FOptimizeBool: Boolean;
 public
   constructor Create(Op: TmscrOperatorType);
   function Priority: Integer; override;
 end;

TmscrDScannerNode = class(TmscrActionNode)
 private
   FDScanner: TmscrDScanner;
   FVar: TmscrVarForBase;
 protected
   function GetValue: Variant; override;
 public
   constructor Create(ADScanner: TmscrDScanner);
   destructor Destroy; override;
 end;

TmscrSetNode = class(TmscrActionNode)
 private
   FSetExpression: TmscrSetExpression;
 protected
   function GetValue: Variant; override;
 public
   constructor Create(ASet: TmscrSetExpression);
   destructor Destroy; override;
 end;

TmscrExpression = class(TmscrExpressionBase)
 private
   FCurNode: TmscrExpressionNode;
   FNode: TmscrExpressionNode;
   FScript: TmscrScript;
   FSource: String;
   procedure AddOperand(Node: TmscrExpressionNode);
 protected
   function  GetValue: Variant; override;
   procedure SetValue(const Value: Variant); override;
 public
   constructor Create(Script: TmscrScript);
   destructor Destroy; override;
   procedure Register_Const(const AValue: Variant);
   procedure Register_ConstWithType(const AValue: Variant; aTyp: TmscrVarType);
   procedure AddDScanner(ADScanner: TmscrDScanner);
   procedure AddOperator(const Op: String);
   procedure AddSet(ASet: TmscrSetExpression);
   function Finalize: String;
   function Optimize(DScanner: TmscrDScanner): String;
   function SingleItem: TmscrVarForBase;
   property Source: String read FSource write FSource;
 end;

//======================================

TmscrEngParser = class(TObject)
 private
   FErrorPos: String;
   FGrammar: TmscrDATADocument;
   FFinalScript: TmscrDATADocument;
   FNeedDeclareVars: Boolean;
   FParser: TmscrParser;
   FScrProject: TmscrScript;
   FProjRoot: TmscrDATAItem;
   FRoot: TmscrDATAItem;
   FUnitName: String;
   FUsesList: TStrings;
   FWithList: TStringList;
   function propPos(xi: TmscrDATAItem): String;
   procedure ErrorPos(xi: TmscrDATAItem);        
   function  FindError(const aErr:Integer):String;
   function  FindClass(const TypeName: String): TmscrVarForClass; 
   function  FindRecord(const TypeName: String): TmscrVarForRecord;
   function  FindVar(aProj: TmscrScript; const Name: String): TmscrVarForBase;
   function  FindType(s: String): TmscrVarType;
   procedure CheckIdent(aProj: TmscrScript; const Name: String);
   procedure CheckTypeCompatibility(Var1, Var2: TmscrVarForBase);
   function  CreateVar(xi: TmscrDATAItem; aProj: TmscrScript; const Name: String;
                         Statement: TmscrStatement = nil;
                         CreateParam: Boolean = False;
                         IsVarParam: Boolean = False): TmscrVarForBase;

   function  Compile_Set(xi: TmscrDATAItem; aProj: TmscrScript): TmscrSetExpression;
   function  Compile_Expression(xi: TmscrDATAItem; aProj: TmscrScript): TmscrExpression;
   procedure Compile_Uses(xi: TmscrDATAItem; aProj: TmscrScript);
   procedure Compile_Var(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Const(xi: TmscrDATAItem; aProj: TmscrScript);
   procedure Compile_Parameters(xi: TmscrDATAItem; v: TmscrVarForProc);
   procedure Compile_ProcA(xi: TmscrDATAItem; aProj: TmscrScript);
   procedure Compile_ProcB(xi: TmscrDATAItem; aProj: TmscrScript);
   procedure Compile_FuncA(xi: TmscrDATAItem; aProj: TmscrScript);
   procedure Compile_FuncB(xi: TmscrDATAItem; aProj: TmscrScript);
   procedure Compile_Assign(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Call(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_If(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_For(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_While(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Repeat(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Case(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Try(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Break(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Continue(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Exit(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Return(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_With(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Delete(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_mscodecompound(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_SrcStm(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
   procedure Compile_Project(xi: TmscrDATAItem; aProj: TmscrScript);
 public
   constructor Create(aProject: TmscrScript);
   destructor Destroy; override;
   procedure InitGrammar;
   function  FinalScript_Build(const Text: String): Boolean;
   procedure FinalScript_Parse;
   function  DoDScanner(xi: TmscrDATAItem; aProj: TmscrScript; EmitOp: TmscrEmitOp = mseoNone): TmscrDScanner;
   property  FinalScript: TmscrDATADocument read FFinalScript;
 end;

//=======================================
{$IFDEF MAGSCR_USEOLE}
type
TmscrEngOLE = class(TmscrEngCustom)
 private
   function DispatchInvoke(const ParamArray: Variant; ParamCount: Integer; Flags: Word): Variant;
 protected
   procedure SetValue(const Value: Variant); override;
   function  GetValue: Variant; override;
 public
   constructor Create(const AName: String);
  end;
{$ENDIF}
//=======================================

type
TmscrSysFunctions = class(TmscrRTTILibrary)
 private
   FCatConv  : String;
   FCatDate  : String;
   FCatFormat: String;
   FCatMath  : String;
   FCatOther : String;
   FCatStr   : String;
   function CallMethod1(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
   function CallMethod2(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
   function CallMethod3(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
   function CallMethod4(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
   function CallMethod5(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
   function CallMethod6(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
   function CallMethod7(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
 public
   constructor Create(AScript: TmscrScript); override;
  end;

//=======================================

TmscrPascalScript = class(TmscrScript)
  end;

//=======================================

procedure GenerateDATAContents(aProj: TmscrScript; Item: TmscrDATAItem; FunctionsOnly: Boolean = False);
procedure GenerateMembers(aProj: TmscrScript; cl: TClass; Item: TmscrDATAItem);
function  TypesCompatible(Typ1, Typ2: TmscrTypeRec; Script: TmscrScript): Boolean;
function  AssignCompatible(Var1, Var2: TmscrVarForBase; Script: TmscrScript): Boolean;
function  VarRecToVariant(v: TVarRec): Variant;                                  
function  StrToVarType(const TypeName: String; Script: TmscrScript): TmscrVarType;
procedure VariantToVarRec(v: Variant; var ar: TmscrVarRecArray; var sPtrList: Tlist);
procedure ClearVarRec(var ar: TmscrVarRecArray; var sPtrList: TList);
function  ParserStringToVariant(s: String): Variant;
function  ParseMethodSyntax(const Syntax: String; Script: TmscrScript): TmscrVarForBase;

function  MSCR_PosToPoint(const ErrorPos: String): TPoint;
function  MSCR_GlobalScripter: TmscrScript;
function  MSCR_IsGlobalScripterExist: Boolean;
function  MSCR_RTTILibraries: TList;

Var
  VarMSCR_ConcoleOut:TMemo=NIL;

//===================================================
//===================================================
//===================================================

implementation

{$IFDEF MAGSCR_USEOLE}
uses
  Windows, ActiveX, ComObj;
{$ENDIF}

var
  FGlobalScripter: TmscrScript = nil;
  FGlobalScripterIsNIL: Boolean = False;

  FRTTILibraries: TList = nil;
  FRTTILibrariesIsNIL: Boolean = False;

const
  _SyntaxType  = 'ObjcPascalScript';


  _MAINGRAMMAR = '<?xml version="1.0"?><language text="ObjcPascalScript"><types/><empty/><program><optional>'+
                 '<keyword text="PROGRAM"/><ident errornum="1"/><char text=";" errornum="5"/></optional>'+
                 '<optional><usesclause/></optional><block/><char text="." errornum="6"/></program>'+
                 '<usesclause node="uses"><keyword text="USES"/><loop text=","><string add="file" errornum="1"/>'+
                 '</loop><char text=";" errornum="5"/></usesclause><block><optloop><declsection/></optloop>'+
                 '<mscodecompound errornum="10"/></block><declsection><switch><constsection/><varsection/><proceduredeclsection/>'+
                 '</switch></declsection><constsection><keyword text="CONST"/><loop><constantdecl/></loop></constsection>'+
                 '<constantdecl node="const"><ident add="ident" errornum="1" term="1"/><char text="=" errornum="9"/>'+
                 '<expression errornum="2"/><char text=";" errornum="5"/></constantdecl><varsection><keyword text="VAR"/>'+
                 '<loop><varlist/><char text=";" errornum="5"/></loop></varsection><varlist node="var"><loop text=",">'+
                 '<ident add="ident" errornum="1" term="1"/></loop><char text=":" errornum="4"/><typeident errornum="1"/>'+
                 '<optional><initvalue/></optional></varlist><typeident><optional><array/></optional><ident add="type"/>'+
                 '</typeident><array node="array"><keyword text="ARRAY"/><optional><char text="["/><loop text=",">'+
                 '<arraydim errornum="2"/></loop><char text="]" errornum="8"/></optional><keyword text="OF" errornum="12"/>'+
                 '</array><arraydim node="dim"><switch><dojob><expression/><char text="."/><char text="." skip="0"/>'+
                 '<expression/></dojob><expression/></switch></arraydim><initvalue node="init"><char text="="/>'+
                 '<expression errornum="2"/></initvalue><expression node="expr"><simpleexpression/><optloop><relop/>'+
                 '<simpleexpression/></optloop></expression><simpleexpression><optional><char text="-" add="op" addtext="unminus"/>'+
                 '</optional><term/><optloop><addop/><term/></optloop></simpleexpression><term><factor/>'+
                 '<optloop><mulop/><factor/></optloop></term><factor><switch><DScanner/><number add="number"/>'+
                 '<string add="string"/><dojob><char text="(" add="op"/><expression errornum="2"/><char text=")" add="op" errornum="7"/>'+
                 '</dojob><dojob><keyword text="NOT" add="op" addtext="not"/><factor errornum="2"/></dojob>'+
                 '<dojob><char text="["/><setconstructor errornum="2"/><char text="]" errornum="8"/></dojob><dojob>'+
                 '<char text="&#60;"/><frstring/><char text="&#62;" errornum="20"/></dojob></switch></factor><setconstructor node="set">'+
                 '<loop text=","><setnode/></loop></setconstructor><setnode><expression/><optional><char text="." add="range"/>'+
                 '<char text="." errornum="6"/><expression/></optional></setnode><relop><switch><dojob><char text="&#62;"/>'+
                 '<char text="=" skip="0" add="op" addtext="&#62;="/></dojob><dojob><char text="&#60;"/>'+
                 '<char text="&#62;" skip="0" add="op" addtext="&#60;&#62;"/></dojob><dojob><char text="&#60;"/>'+
                 '<char text="=" skip="0" add="op" addtext="&#60;="/></dojob><char text="&#62;" add="op" addtext="&#62;"/>'+
                 '<char text="&#60;" add="op" addtext="&#60;"/><char text="=" add="op" addtext="="/><keyword text="IN" add="op" addtext="in"/>'+
                 '<keyword text="IS" add="op" addtext="is"/></switch></relop><addop><switch><char text="+" add="op"/>'+
                 '<char text="-" add="op"/><keyword text="OR" add="op" addtext="or"/><keyword text="XOR" add="op" addtext="xor"/>'+
                 '</switch></addop><mulop><switch><char text="*" add="op"/><char text="/" add="op"/><keyword text="DIV" add="op" addtext="div"/>'+
                 '<keyword text="MOD" add="op" addtext="mod"/><keyword text="AND" add="op" addtext="and"/><keyword text="SHL" add="op" addtext="shl"/>'+
                 '<keyword text="SHR" add="op" addtext="shr"/></switch></mulop><DScanner node="dsgn"><optional><char text="@" add="addr"/>'+
                 '</optional><ident add="node"/><optloop><switch><dojob><char text="."/><keyword add="node" errornum="1"/></dojob><dojob>'+
                 '<char text="[" add="node"/><exprlist errornum="2"/><char text="]" errornum="8"/></dojob><dojob><char text="("/>'+
                 '<exprlist errornum="2"/><char text=")" errornum="7"/></dojob></switch></optloop></DScanner><exprlist><optloop text=",">'+
                 '<expression/></optloop></exprlist><statement><optswitch><simplestatement/><structsrcstm/></optswitch>'+
                 '</statement><srcstmlist><loop text=";"><statement/></loop></srcstmlist><simplestatement><switch><codeassign/><mscodecall/>'+
                 '<switch><keyword text="BREAK" node="break"/><keyword text="CONTINUE" node="continue"/><keyword text="EXIT" node="exit"/>'+
                 '</switch></switch></simplestatement><codeassign node="codeassign"><DScanner/><char text=":"/><char text="=" skip="0" errornum="9"/>'+
                 '<expression errornum="2"/></codeassign><mscodecall node="mscodecall"><DScanner/></mscodecall><structsrcstm><switch><mscodecompound/>'+
                 '<conditionalsrcstm/><loopsrcstm/><mscodetry/><withsrcstm/></switch></structsrcstm><mscodecompound node="mscodecompound">'+
                 '<keyword text="BEGIN"/><srcstmlist/><keyword text="END" errornum="5"/></mscodecompound><conditionalsrcstm><switch><mscodeif/>'+
                 '<mscodecases/></switch></conditionalsrcstm><mscodeif node="mscodeif"><keyword text="IF"/><expression errornum="2"/>'+
                 '<keyword text="THEN" errornum="13"/><statement node="thensrcstm"/><optional><keyword text="ELSE"/><statement node="elsesrcstm"/>'+
                 '</optional></mscodeif><mscodecases node="mscodecases"><keyword text="CASE"/><expression errornum="2"/><keyword text="OF" errornum="12"/>'+
                 '<loop text=";" skip="1"><caseselector/></loop><optional><keyword text="ELSE"/><statement/></optional><optional>'+
                 '<char text=";"/></optional><keyword text="END" errornum="11"/></mscodecases><caseselector node="caseselector">'+
                 '<setconstructor errornum="2"/><char text=":" errornum="4"/><statement/></caseselector><loopsrcstm><switch><mscoderepeat/>'+
                 '<mscodewhile/><mscodefor/></switch></loopsrcstm><mscoderepeat node="mscoderepeat"><keyword text="REPEAT"/><srcstmlist/>'+
                 '<keyword text="UNTIL" errornum="14"/><expression errornum="2"/></mscoderepeat><mscodewhile node="mscodewhile"><keyword text="WHILE"/>'+
                 '<expression errornum="2"/><keyword text="DO" errornum="16"/><statement/></mscodewhile><mscodefor node="mscodefor"><keyword text="FOR"/>'+
                 '<ident add="ident" errornum="1"/><char text=":" errornum="4"/><char text="=" skip="0" errornum="9"/><expression errornum="2"/>'+
                 '<switch errornum="15"><keyword text="TO"/><keyword text="DOWNTO" add="downto"/></switch><expression errornum="2"/>'+
                 '<keyword text="DO" errornum="16"/><statement/></mscodefor><mscodetry node="mscodetry"><keyword text="TRY"/><srcstmlist/><switch errornum="17">'+
                 '<dojob><keyword text="FINALLY"/><srcstmlist node="finallysrcstm"/></dojob><dojob><keyword text="EXCEPT"/>'+
                 '<srcstmlist node="ExceptSrcStm"/></dojob></switch><keyword text="END" errornum="11"/></mscodetry><withsrcstm node="with">'+
                 '<keyword text="WITH"/><loop text=","><DScanner errornum="2"/></loop><keyword text="DO" errornum="16"/><statement/>'+
                 '</withsrcstm><proceduredeclsection><switch><proceduredecl/><functiondecl/></switch></proceduredeclsection><proceduredecl node="procedure">'+
                 '<procedureheading/><char text=";" errornum="5"/><block/><char text=";" errornum="5"/></proceduredecl><procedureheading><keyword text="PROCEDURE"/>'+
                 '<ident add="name" errornum="1"/><optional><formalparameters/></optional></procedureheading><functiondecl node="function"><functionheading/>'+
                 '<char text=";" errornum="5"/><block/><char text=";" errornum="5"/></functiondecl><functionheading><keyword text="FUNCTION"/>'+
                 '<ident add="name" errornum="1"/><optional><formalparameters/></optional><char text=":" errornum="4"/><ident add="type" errornum="1"/>'+
                 '</functionheading><formalparameters node="parameters"><char text="("/><optloop text=";"><formalparam/></optloop>'+
                 '<char text=")" errornum="7"/></formalparameters><formalparam><optswitch><keyword text="VAR" node="varparams"/><keyword text="CONST"/>'+
                 '</optswitch><varlist/></formalparam></language>';



//============= TmscrItemsList =============================

procedure TmscrItemList.Clear;
begin
 while FItems.Count > 0 do
 begin
   TObject(FItems[0]).Free;
   FItems.Delete(0);
 end;
end;

function TmscrItemList.Count: integer;
begin
 Result:= FItems.Count;
end;

procedure TmscrItemList.Remove(Item: TObject);
begin
 FItems.Remove(Item);
end;

procedure TmscrItemList.Add(Item: TObject);
begin
 FItems.Add(Item);
end;

constructor TmscrItemList.Create;
begin
 FItems:= TList.Create;
end;

destructor TmscrItemList.Destroy;
begin
  Clear;
  FItems.Free;
  inherited;
end;

//============= TmscrVarForBase =============

function TmscrVarForBase.GetParam(Index: integer): TmscrVarForParamItem;
begin
 Result:= FItems[Index];
end;

function TmscrVarForBase.GetPValue: PVariant;
begin
 Result:= @FValue;
end;

function TmscrVarForBase.GetValue: variant;
begin
 Result:= FValue;
end;

procedure TmscrVarForBase.SetValue(const Value: variant);
begin
 if not FIsReadOnly then
  FValue:= Value;
end;

function TmscrVarForBase.GetFullTypeName: string;
begin
 Result:= 'Variant';
 case FTyp of
   msvtInt:    Result:= 'Integer';
   msvtInt64:  Result:= 'Int64';
   msvtBool:   Result:= 'Boolean';
   msvtFloat:  Result:= 'Extended';
   msvtChar:   Result:= 'Char';
   msvtString: Result:= 'String';
   msvtClass:  Result:= FTypeName; 
   msvtRecord: Result:= FTypeName;
   msvtArray:  Result:= 'Array';
   msvtEnum:   Result:= FTypeName;
 end;
end;

function TmscrVarForBase.GetNumberOfRequiredParams: integer;
var
  i: integer;
begin
  Result:= 0;
  for i:= 0 to Count - 1 do
     if not Params[i].IsOptional then
       Inc(Result);
end;

constructor TmscrVarForBase.Create(const AName: string; ATyp: TmscrVarType; const ATypeName: string);
begin
 inherited Create;
 FName:= AName;
 FTyp:= ATyp;
 FTypeName:= ATypeName;
 FValue:= Null;
 FNeedResult:= True;
 FUppercaseName:= AnsiUpperCase(FName);
end;

//============= TmscrVarForString =============

function TmscrVarForString.GetValue: variant;
begin
  Result:= FStr;
  if Assigned(FOnGetVarValue) then
  begin
     Result:= FOnGetVarValue(FName, FTyp, FStr);
     if Result = null then
       Result:= FStr;
  end;
end;

procedure TmscrVarForString.SetValue(const Value: variant);
begin
  FStr:= Value;
end;

//============= TmscrVarForParamItem =============

constructor TmscrVarForParamItem.Create(const AName: string; ATyp: TmscrVarType; const ATypeName: string; AIsOptional, AIsVarParam: boolean);
begin
  inherited Create(AName, ATyp, ATypeName);
  FIsOptional:= AIsOptional;
  FIsVarParam:= AIsVarParam;
  FDefValue:= Null;
end;

//============= TmscrVarForProc =============


function TmscrVarForProc.GetValue: variant;
var
 Temp: boolean;
 ParentProg, SaveProg: TmscrScript;
 i: integer;
begin
 Temp:= FExecuting;
 FExecuting:= True;
 if FIsFunc then
  FRefItem.Value:= Unassigned;

 ParentProg:= FScrProject;
 SaveProg:= nil;

 while ParentProg<>nil do
   if ParentProg.FMainProj then
    begin
      SaveProg:= ParentProg.FProjRunning;
      ParentProg.FProjRunning:= FScrProject;
      break;
    end else
     ParentProg:= ParentProg.FParent;

 try
  with FScrProject do
  begin
    FCalledExit:= False;
    FTerminated:= False;
    FIsRunning:= True;
    FProjName:= Self.FName;
     try
      FStatement.Execute;
     Finally
      FCalledExit:= False;
      FTerminated:= False;
      FIsRunning:= False;
      FProjName:= '';
     end;
  end;

   if FIsFunc then
     Result:= FRefItem.Value else
     Result:= Null;

 finally
    if ParentProg<>nil then
      ParentProg.FProjRunning:= SaveProg;
  FExecuting:= Temp;
  if (ParentProg<>nil) and ParentProg.FClearLocalVars then
    For i:= 0 to Project.Count - 1 do
       if (Project.Items[i] is TmscrVarForVariable) and (CompareText('Result', Project.Items[i].Name)<>0) and
          not VarIsClear(Project.Items[i].Value) then
          case TmscrVarForVariable(Project.Items[i]).Typ of
               msvtString:
               TmscrVarForVariable(Project.Items[i]).Value:= '';
               msvtInt, msvtFloat, msvtChar:
               TmscrVarForVariable(Project.Items[i]).Value:= 0;
               msvtVariant:
               TmscrVarForVariable(Project.Items[i]).Value:= Null;
           end;
 end;
end;

constructor TmscrVarForProc.Create(const AName: string; ATyp: TmscrVarType; 
                                     const ATypeName: string; AParent: TmscrScript;
                                     AIsFunc: boolean = True);
begin
 inherited Create(AName, ATyp, ATypeName);
 FIsReadOnly:= True;
 FVarsStack:= TList.Create;
 FIsFunc:= AIsFunc;
 FScrProject:= TmscrScript.Create(nil);
 FScrProject.Parent:= AParent;
 if FScrProject.Parent<>nil then
  FScrProject.UseClassLateBinding:= FScrProject.Parent.UseClassLateBinding;
  
 if FIsFunc then
 begin
  FRefItem:= TmscrVarForVariable.Create('Result', ATyp, ATypeName);
  FScrProject.Add('Result', FRefItem);
 end;
end;

destructor TmscrVarForProc.Destroy;
 var i: integer;
begin
 for i:= 0 to Count - 1 do
  FScrProject.FItems.Delete(FScrProject.FItems.IndexOfObject(Params[i]));

 FScrProject.Free;
 FVarsStack.Free;
 inherited;
end;

procedure TmscrVarForProc.RestoreLocalVariables(StackIndex: integer; bSkipVarParams: boolean; dItem: TmscrDScannerItem);
var
 i: integer;
 LocalVars: TList;
 StackItem: TmscrEngLocalVariables;
 bIsVar: boolean;
 Temp1: array of variant;
begin
 if (FVarsStack.Count<StackIndex) or (StackIndex<0) then Exit;

 LocalVars:= TList(FVarsStack[StackIndex]);
 SetLength(Temp1, Count);
 try
  if Assigned(dItem) then
    For i:= 0 to Count - 1 do
          if Params[i].IsVarParam then
            Temp1[i]:= Params[i].Value;
  For i:= 0 to LocalVars.Count - 1 do
    begin
      StackItem:= TmscrEngLocalVariables(LocalVars[i]);
      bIsVar:= TmscrVarForParamItem(StackItem.FVariableLink).FIsVarParam;
      if not (bSkipVarParams and (StackItem.FVariableLink is TmscrVarForParamItem) and bIsVar) then
         StackItem.FVariableLink.Value:= StackItem.FValue;
      StackItem.Free;
    end;
  if Assigned(dItem) then
    For i:= 0 to Count - 1 do
         if Params[i].IsVarParam then
           dItem[i].Value:= Temp1[i];
 finally
  Temp1:= nil;
  LocalVars.Free;
  FVarsStack.Delete(StackIndex);
 end;
end;

function TmscrVarForProc.SaveLocalVariables: integer;
 var
 i: integer;
 LocalVars: TList;
 StackItem: TmscrEngLocalVariables;
begin
 LocalVars:= TList.Create;
 FVarsStack.Add(LocalVars);
 Result:= FVarsStack.Count - 1;
 for i:= 0 to Project.Count - 1 do
   if (Project.Items[i] is TmscrVarForVariable) or (Project.Items[i] is TmscrVarForParamItem) then
   begin
     StackItem:= TmscrEngLocalVariables.Create;
     StackItem.FValue:= Project.Items[i].Value;
     StackItem.FVariableLink:= Project.Items[i];
     LocalVars.Add(StackItem);
   end;
end;

//============= TmscrEngProperty =============

procedure TmscrEngProperty.SetValue(const Value: variant);
var
 p: PPropInfo;
 Instance: TObject;
 IntVal: fmscrInteger;
begin

 if IsReadOnly then Exit;
 Instance:= TObject(fmscrInteger(ParentValue));

 if Instance=Nil then
    raise Exception.CreateFmt(msEClassCreateError, [FClassRef.ClassName]);

 if FIsPublished then
 begin
   p:= GetPropInfo(Instance.ClassInfo, Name);
   if p<>nil then
     case p.PropType^.Kind of
        tkInteger, tkSet, tkEnumeration, tkClass, tkBool:
        begin
          begin
             if Typ = msvtBool then
               if Value = True then
                  IntVal:= 1 else
                  IntVal:= 0
             else
               IntVal:= fmscrInteger(Value);

             SetOrdProp(Instance, p, IntVal);
          end;
        end;
        tkInt64:              SetInt64Prop(Instance, p, Value);
        tkFloat:              SetFloatProp(Instance, p, extended(Value));
        tkChar, tkWChar:      SetOrdProp(Instance, p, Ord(string(Value)[1]));
        tkVariant:            SetVariantProp(Instance, p, Value);
        
        tkString, tkLString, 
        tkWString, tkAString: SetStrProp(Instance, p, string(Value));
     end;
 end else
 if Assigned(FOnSetValue) then
  FOnSetValue(Instance, FClassRef, FUppercaseName, Value, Self);

end;

function TmscrEngProperty.GetValue: variant;
var
 p: PPropInfo;
 Instance: TObject;
begin
 Result:= Null;
 Instance:= TObject(fmscrInteger(ParentValue));

 if Instance=Nil then
    raise Exception.CreateFmt(msEClassCreateError, [FClassRef.ClassName]);

 if FIsPublished and Assigned(Instance) then
 begin
    p:= GetPropInfo(Instance.ClassInfo, Name);
    if p<>nil then
      case p.PropType^.Kind of
          tkInteger, tkSet, tkEnumeration, 
          tkClass, tkBool:    Result:= GetOrdProp(Instance, p);
          
          tkInt64:             Result:= GetInt64Prop(Instance, p);
          tkFloat:             Result:= GetFloatProp(Instance, p);
          tkChar, tkWChar:     Result:= Chr(GetOrdProp(Instance, p));
          tkVariant:           Result:= GetVariantProp(Instance, p);
          
          tkString, tkLString, 
          tkWString, tkAString:Result:= GetStrProp(Instance, p);
      end;
 end
 else if Assigned(FOnGetValue) then
    Result:= FOnGetValue(Instance, FClassRef, FUppercaseName, Self);

 if Typ = msvtBool then
   if Result = 0 then
       Result:= False else
      Result:= True;

end;


//============= TmscrEngRecElement =============

function TmscrEngRecElement.GetValue: variant;
var
  Instance: Pointer;
begin
  Result:= Null;

  Instance:=TmscrVarForRecord(FParentRef).FRecData;

  if Assigned(FOnGetValue) then
     Result:= FOnGetValue(Instance, TmscrVarForRecord(FParentRef).TypeInfo, FUppercaseName);

  if Typ = msvtBool then
     if Result = 0 then
       Result:= False else
       Result:= True;
end;

procedure TmscrEngRecElement.SetValue(const Value: variant);
var
  Instance: Pointer;
begin

  Instance:=TmscrVarForRecord(FParentRef).FRecData;

  if Assigned(FOnSetValue) then
   FOnSetValue(Instance, TmscrVarForRecord(FParentRef).TypeInfo, FUppercaseName, Value);
end;

//============= TmscrVarForMethod =============

procedure TmscrVarForMethod.SetVParam(Index: integer; const Value: variant);
begin
  TmscrVarForParamItem(FItems[Index]).Value:= Value;
end;

function TmscrVarForMethod.GetValue: variant;
var
 i: integer;
 Instance: TObject;
begin
 if Assigned(FOnCall) then
 begin
   Instance:= nil;
   if not VarIsNull(ParentValue) then
     Instance:= TObject(fmscrInteger(ParentValue));

   if FIndexMethod then
     Result:= FOnCall(Instance, FClassRef, FUppercaseName + '.GET', Self) else
     Result:= FOnCall(Instance, FClassRef, FUppercaseName, Self);

 end else
    Result:= 0;
end;

procedure TmscrVarForMethod.SetValue(const Value: variant);
var
 v: variant;
 i: integer;
begin
 if FIndexMethod then
   if Assigned(FOnCall) then
    begin
    FSetValue:= Value;
    FOnCall(TObject(fmscrInteger(ParentValue)), FClassRef, FUppercaseName + '.SET', Self);
    FSetValue:= Null;
    end;
end;

function TmscrVarForMethod.GetVParam(Index: integer): variant;
begin
 if Index = Count then
    Result:= FSetValue else
    Result:= TmscrVarForParamItem(FItems[Index]).Value;
end;

constructor TmscrVarForMethod.Create(const Syntax: string; Script: TmscrScript);
var
 i: integer;
 v: TmscrVarForBase;
begin
 v:= ParseMethodSyntax(Syntax, Script);
 inherited Create(v.Name, v.Typ, v.TypeName);
 FIsReadOnly:= True;
 FSyntax:= Syntax;
 IsMacro:= v.IsMacro;

 for i:= 0 to v.Count - 1 do
    Add(v.Params[i]);

 while v.Count > 0 do
    v.FItems.Delete(0);

 v.Free;

 if Count > 0 then
  FVarArray:= VarArrayCreate([0, Count - 1], varVariant);
end;

destructor TmscrVarForMethod.Destroy;
begin
 FVarArray:= Null;
 inherited;
end;

//============= TmscrEngComponent =============

function TmscrEngComponent.GetValue: variant;
begin
 Result:= fmscrInteger(FComponent);
end;

constructor TmscrEngComponent.Create(Component: TComponent);
begin
 inherited Create(Component.Name, msvtClass, Component.ClassName);
 FComponent:= Component;
end;

//============= TmscrEngEvent =============

procedure TmscrEngEvent.SetValue(const Value: variant);
var
 Instance: TPersistent;
 v: TmscrVarForBase;
 e: TmscrCustomEvent;
 p: PPropInfo;
 m: TMethod;
begin

 Instance:= TPersistent(fmscrInteger(ParentValue));
 if VarToStr(Value) = '0' then
 begin
    m.Code:= nil;
    m.Data:= nil;
 end else
 begin
    v:= FScrProject.Find(Value);
    if (v = nil) or not (v is TmscrVarForProc) then
      raise Exception.Create(msEEventError);

    e:= TmscrCustomEvent(FEvent.NewInstance);
    e.Create(Instance, TmscrVarForProc(v));
  FScrProject.Add('', e);
    m.Code:= e.GetMethod;
    m.Data:= e;
 end;

 p:= GetPropInfo(Instance.ClassInfo, Name);
 SetMethodProp(Instance, p, m);
end;

function TmscrEngEvent.GetValue: variant;
begin
 Result:= '';
end;

constructor TmscrEngEvent.Create(const Name: string; AEvent: TmscrCustomEventClass);
begin
 inherited Create(Name, msvtString, '');
 FEvent:= AEvent;
end;

//============= TmscrVarForClass =============

function TmscrVarForClass.GetMembers(Index: integer): TmscrEngCustom;
begin
  Result:= FMembers.FItems[Index];
end;

function TmscrVarForClass.GetMembersCount: integer;
begin
  Result:= FMembers.Count;
end;

procedure TmscrVarForClass.Register_Constructor(Syntax: string; CallEvent: TmscrCallMethod);
var
  i: integer;
begin
  i:= Pos(' ', Syntax);
  Delete(Syntax, 1, i - 1);
  Syntax:= 'function' + Syntax + ': ' + 'Constructor';
  Register_Method(Syntax, CallEvent);
end;

procedure TmscrVarForClass.Register_Method(const Syntax: string; CallEvent: TmscrCallMethod);
var
 m: TmscrVarForMethod;
begin
 m:= TmscrVarForMethod.Create(Syntax, FScrProject);
 m.FOnCall:= CallEvent;
 m.FClassRef:= FClassRef;
 FMembers.Add(m);
end;

procedure TmscrVarForClass.Register_Property(const Name, Typ: string; GetEvent: TmscrClassGetProperty; SetEvent: TmscrClassSetProperty);
var
 p: TmscrEngProperty;
begin
 p:= TmscrEngProperty.Create(Name, StrToVarType(Typ, FScrProject), Typ);
 p.FClassRef:= FClassRef;
 p.FOnGetValue:= GetEvent;
 p.FOnSetValue:= SetEvent;
 p.IsReadOnly:= not Assigned(SetEvent);
 FMembers.Add(p);
end;

procedure TmscrVarForClass.Register_Event(const Name: string; AEvent: TmscrCustomEventClass);
var
 e: TmscrEngEvent;
begin
 e:= TmscrEngEvent.Create(Name, AEvent);
 e.FClassRef:= FClassRef;
 FMembers.Add(e);
end;


procedure TmscrVarForClass.Register_DefaultProperty(const Name, Params, Typ: string; CallEvent: TmscrCallMethod; AReadOnly: boolean);
begin
  Register_IndexProperty(Name, Params, Typ, CallEvent, AReadOnly);
  FDefProperty:= Members[FMembers.Count - 1];
end;

procedure TmscrVarForClass.Register_IndexProperty(const Name, Params, Typ: string; CallEvent: TmscrCallMethod; AReadOnly: boolean);
var
 i: integer;
 sl: TStringList;
 s: string;
begin
 sl:= TStringList.Create;
 sl.CommaText:= Params;

 s:= '';
 for i:= 0 to sl.Count - 1 do
    s:= s + 'p' + IntToStr(i) + ': ' + sl[i] + '; ';

 SetLength(s, Length(s) - 2);
 try
    Register_Method('function ' + Name + '(' + s + '): ' + Typ, CallEvent);

    with TmscrVarForMethod(Members[FMembers.Count - 1]) do
    begin
     IsReadOnly:= AReadOnly;
     FIndexMethod:= True;
    end;

 finally
    sl.Free;
 end;
end;

procedure TmscrVarForClass.AddComponent(c: TComponent);
begin
  FMembers.Add(TmscrEngComponent.Create(c));
end;

procedure TmscrVarForClass.AddPublishedProperties(AClass: TClass);
var
 TypeInfo: PTypeInfo;
 propCount: integer;
 propList: PPropList;
 i: integer;
 cl: string;
 t: TmscrVarType;
 FClass: TClass;
 p: TmscrEngProperty;
begin
 TypeInfo:= AClass.ClassInfo;
 if TypeInfo = nil then Exit;

 propCount:= GetPropList(TypeInfo, tkProperties, nil);
 GetMem(PropList, PropCount * SizeOf(PPropInfo));
 GetPropList(TypeInfo, tkProperties, PropList);

 try
  For i:= 0 to PropCount - 1 do
    begin
      t:= msvtInt;
      cl:= '';

      case PropList[i].PropType^.Kind of
         tkInteger:
           t:= msvtInt;
         tkInt64:
           t:= msvtInt64;
         tkSet:
         begin
           t:= msvtEnum;
           cl:= string(PropList[i].PropType^.Name);
         end;
         tkEnumeration:
         begin
           t:= msvtEnum;
           cl:= string(PropList[i].PropType^.Name);
           if (CompareText(cl, 'Boolean') = 0) or (CompareText(cl, 'bool') = 0) then
              t:= msvtBool;
         end;
         tkBool:
           t:= msvtBool;
         tkFloat:
           t:= msvtFloat;
         tkChar, tkWChar:
           t:= msvtChar;
         tkString, tkLString, tkWString, tkAString:
           t:= msvtString;
         tkVariant:
           t:= msvtVariant;
         tkRecord:
           t:= msvtRecord;
         tkClass:
         begin
           t:= msvtClass;
           FClass:= GetTypeData(PropList[i].PropType).ClassType;
           cl:= FClass.ClassName;
         end;
      end;

     p:= TmscrEngProperty.Create(string(PropList[i].Name), t, cl);
     p.FClassRef:= FClassRef;
     p.FIsPublished:= True;
     FMembers.Add(p);
    end;

 finally
  FreeMem(PropList, PropCount * SizeOf(PPropInfo));
 end;
end;

function TmscrVarForClass.Find(const Name: string): TmscrEngCustom;
var
  cl: TmscrVarForClass;
  //.........................
  function DoFind(const Name: string): TmscrEngCustom;
  var
     i: integer;
  begin
     Result:= nil;
   For i:= 0 to FMembers.Count - 1 do
       if CompareText(Name, Members[i].Name) = 0 then
       begin
          Result:= Members[i];
          Exit;
       end;
  end;
  //.........................
begin
  Result:= DoFind(Name);
  if Result = nil then
  begin
     cl:= FScrProject.FindClass(FParentClassStr);
     if cl<>nil then
       Result:= cl.Find(Name);
  end;
end;

function TmscrVarForClass.GetValue: variant;
begin

  if Params[0].Value = Null then
     Result:= fmscrInteger(FClassRef.NewInstance) else
     Result:= Params[0].Value;

  Params[0].Value:= Null;
end;

constructor TmscrVarForClass.Create(AClass: TClass; const ParentClassStr: string);
begin
 inherited Create(AClass.ClassName, msvtClass, AClass.ClassName);
 FMembers:= TmscrItemList.Create;
 FParentClassStr:= ParentClassStr;
 FClassRef:= AClass;

 AddPublishedProperties(AClass);
 Add(TmscrVarForParamItem.Create('', msvtVariant, '', True, False));
end;

destructor TmscrVarForClass.Destroy;
begin
 FMembers.Free;
 inherited;
end;

//============= TmscrVarForRecord =============

constructor TmscrVarForRecord.Create(ATypeInfo: PTypeInfo; const aTypeInfoStr: String);
begin
  inherited Create(aTypeInfoStr, msvtRecord, ATypeInfo.Name);

  FMembers:= TmscrItemList.Create;
  FTypeInfoStr:= TypeInfoStr;
  FTypeInfo:= ATypeInfo;
  FRecData:=Nil;
  FRecDataSize:=0;
  FValue:=Nil;

  Add(TmscrVarForParamItem.Create('', msvtVariant, '', True, False));

  //..................

  FRecDataSize:=SizeOf(FTypeInfo^);
  GetMem(FRecData,FRecDataSize);
  FValue:=fmscrInteger(FRecData);
end;

destructor TmscrVarForRecord.Destroy;
begin
  if FRecDataSize>0 then FreeMem(FRecData,FRecDataSize);
  FMembers.Free;
  inherited;
end;

function TmscrVarForRecord.GetMembers(Index: integer): TmscrEngCustom;
begin
  Result:= FMembers.FItems[Index];
end;

function TmscrVarForRecord.GetMembersCount: integer;
begin
  Result:= FMembers.Count;
end;

procedure TmscrVarForRecord.Register_Element(const Name, Typ: String; GetEvent: TmscrRecGetValueElement; SetEvent: TmscrRecSetValueElement = nil);
var
  p: TmscrEngRecElement;
begin
  p:= TmscrEngRecElement.Create(Name, StrToVarType(Typ, FScrProject), Typ);
  p.ParentRef  := Self;
  p.ParentValue:= FValue;
  p.FOnGetValue:= GetEvent;
  p.FOnSetValue:= SetEvent;
  p.IsReadOnly := not Assigned(SetEvent);
  FMembers.Add(p);
end;

procedure TmscrVarForRecord.AddComponent(c: TComponent);
begin
  FMembers.Add(TmscrEngComponent.Create(c));
end;

function TmscrVarForRecord.Find(const Name: string): TmscrEngCustom;
var
  cl: TmscrVarForRecord;
  //.........................
  function DoFind(const Name: string): TmscrEngCustom;
  var
     i: integer;
  begin
     Result:= nil;
   For i:= 0 to FMembers.Count - 1 do
       if CompareText(Name, Members[i].Name) = 0 then
       begin
          Result:= Members[i];
          Exit;
       end;
  end;
  //.........................
begin
  Result:= DoFind(Name);
  if Result = nil then
  begin
     cl:= FScrProject.FindRecord(FTypeInfoStr);
     if cl<>nil then
       Result:= cl.Find(Name);
  end;
end;

//============= TmscrDScannerItem =============

function TmscrDScannerItem.GetItem(Index: integer): TmscrExpressionBase;
begin
  Result:= FItems[Index];
end;

//=============TmscrDScanner =============

constructor TmscrDScanner.Create(aProject: TmscrScript);
 var ParentProg: TmscrScript;
begin

{$IFDEF CPU64}
  inherited Create('', msvtInt64, '');
{$ELSE}
  inherited Create('', msvtInt, '');
{$ENDIF}

  FScrProject:= aProject;
  FMainProj:= FScrProject;
  ParentProg:= FScrProject;

  while ParentProg<>nil do
     if ParentProg.FMainProj then
     begin
     FMainProj:= ParentProg;
       break;
     end else
     begin
       ParentProg:= ParentProg.FParent;
     end;

  FScrProject.UseClassLateBinding:= FMainProj.UseClassLateBinding;
end;

destructor TmscrDScanner.Destroy;
begin
  if FLateBindingDATASource<>nil then
   FLateBindingDATASource.Free;
  inherited;
end;

procedure TmscrDScanner.Borrow(ADScanner: TmscrDScanner);
var
  SaveItems: TList;
begin
  SaveItems:= FItems;
  FItems:= ADScanner.FItems;
  ADScanner.FItems:= SaveItems;
  FKind:= ADScanner.FKind;
  FRef1:= ADScanner.FRef1;
  FRef2:= ADScanner.FRef2;
  FTyp:= ADScanner.Typ;
  FTypeName:= ADScanner.TypeName;
  FIsReadOnly:= ADScanner.IsReadOnly;
  RefItem:= ADScanner.RefItem;
end;

procedure TmscrDScanner.Finalize;
var
  Item: TmscrDScannerItem;
begin
  Item:= Items[Count - 1];
  FTyp:= Item.Ref.Typ;
  FTypeName:= Item.Ref.TypeName;

  if FTyp = msvtConstructor then
  begin
   FTyp:= msvtClass;
   FTypeName:= Items[Count - 2].Ref.TypeName;
  end;

  FIsReadOnly:= Item.Ref.IsReadOnly;

  if (Count = 1) and (Items[0].Ref is TmscrVarForVariable) then
  begin
     RefItem:= Items[0].Ref;
   FKind:= msskVariable;
  end else
  if (Count = 2) and (Items[0].Ref is TmscrVarForString) then
  begin
     RefItem:= Items[0].Ref;
   FRef1:= Items[1][0];
   FKind:= msskStringArray;
  end else
  if (Count = 2) and (Items[0].Ref is TmscrVarForVariable) and (Items[0].Ref.Typ = msvtArray) then
  begin
     RefItem:= Items[0].Ref;
   FRef1:= RefItem.RefItem;
   FRef2:= Items[1];
   FKind:= msskArray;
  end else
   FKind:= msskOther;
end;

function TmscrDScanner.GetItem(Index: integer): TmscrDScannerItem;
begin
  Result:= FItems[Index];
end;

function TmscrDScanner.DoCalc(const AValue: variant; Flag: boolean): variant;
var
  i, j: integer;
  Item: TmscrVarForBase;
  Val: variant;
  Ref: TmscrVarForBase;
  Temp1: array of variant;
  StackIndex: integer;
begin
  Ref:= nil;
  Val:= Null;
  StackIndex:= -1;
  for i:= 0 to Count - 1 do
  begin
     Item:= Items[i].Ref;

     if Item is TmscrDScanner then
     begin
       Ref:= Item;
       Val:= Item.Value;
       continue;
     end;

     try
       if (Item is TmscrVarForProc) and TmscrVarForProc(Item).Executing then
           StackIndex:= TmscrVarForProc(Item).SaveLocalVariables;

       if Item.Count > 0 then
       begin
          SetLength(Temp1, Item.Count);

          try
          For j:= 0 to Item.Count - 1 do
               if Item.IsMacro then
                 Temp1[j]:= TmscrExpression(Items[i][j]).Source else
                 Temp1[j]:= Items[i][j].Value;

          For j:= 0 to Item.Count - 1 do
               Item.Params[j].Value:= Temp1[j];

        Finally
            Temp1:= nil;
          end;
       end;

       if Item is TmscrEngCustom then
       begin
        TmscrEngCustom(Item).ParentRef:= Ref;
        TmscrEngCustom(Item).ParentValue:= Val;
        TmscrEngCustom(Item).aProj:= FScrProject;
       end;

       Ref:= Item;

       if Flag and (i = Count - 1) then
       begin
          Item.Value:= AValue;
       end else
       begin
          Item.NeedResult:= (i<>Count - 1) or NeedResult;
          Val:= Item.Value;
       end;

     For j:= 0 to Item.Count - 1 do
          if Item.Params[j].IsVarParam then
            Items[i][j].Value:= Item.Params[j].Value;

   Finally
       if (Item is TmscrVarForProc) and TmscrVarForProc(Item).Executing then
        TmscrVarForProc(Item).RestoreLocalVariables(StackIndex, False, Items[i]);
     end;
  end;

  Result:= Val;
end;

procedure TmscrDScanner.CheckLateBinding;
var
  NewDScanner: TmscrDScanner;
  Parser: TmscrEngParser;
begin
  if FLateBindingDATASource<>nil then
  begin
     Parser:= TmscrEngParser.Create(FScrProject);
     try
       NewDScanner:= Parser.DoDScanner(FLateBindingDATASource, FScrProject);
       Borrow(NewDScanner);
       NewDScanner.Free;
   Finally
       Parser.Free;
     FLateBindingDATASource.Free;
     FLateBindingDATASource:= nil;
     end;
  end;
end;

function TmscrDScanner.GetValue: variant;
begin
  CheckLateBinding;
  Result:= DoCalc(Null, False);
end;

procedure TmscrDScanner.SetValue(const Value: variant);
begin
  CheckLateBinding;
  DoCalc(Value, True);
end;

//=============TmscrVarForVariableDScanner =============

function TmscrVarForVariableDScanner.GetValue: variant;
begin
  Result:= RefItem.Value;
end;

procedure TmscrVarForVariableDScanner.SetValue(const Value: variant);
begin
  RefItem.Value:= Value;
end;

//=============TmscrStringDScanner =============

function TmscrStringDScanner.GetValue: variant;
begin
  Result:= TmscrVarForString(RefItem).FStr[integer(FRef1.Value)];
end;

procedure TmscrStringDScanner.SetValue(const Value: variant);
begin
TmscrVarForString(RefItem).FStr[integer(FRef1.Value)]:= VarToStr(Value)[1];
end;

//============= TmscrArrayDScanner =============

function TmscrArrayDScanner.GetValue: variant;
 var i: integer;
begin
TmscrEngCustom(FRef1).ParentRef:= RefItem;
  for i:= 0 to FRef2.Count - 1 do
   FRef1.Params[i].Value:= FRef2[i].Value;
  Result:= FRef1.Value;
end;

procedure TmscrArrayDScanner.SetValue(const Value: variant);
var i: integer;
begin
TmscrEngCustom(FRef1).ParentRef:= RefItem;
  for i:= 0 to FRef2.Count - 1 do
   FRef1.Params[i].Value:= FRef2[i].Value;
  FRef1.Value:= Value;
end;

//============= TmscrSetExpression =============

function TmscrSetExpression.Check(const Value: variant): boolean;
var
  i: integer;
  Expr: TmscrExpressionBase;
begin
  Result:= False;

  i:= 0;
  while i<Count do
  begin
     Expr:= Items[i];

     if (i<Count - 1) and (Items[i + 1] = nil) then
     begin
       Result:= (Value >= Expr.Value) and (Value<=Items[i + 2].Value);
       Inc(i, 2);
     end else
     begin
       Result:= Value = Expr.Value;
     end;

     if Result then break;
     Inc(i);
  end;
end;

function TmscrSetExpression.GetItem(Index: integer): TmscrExpressionBase;
begin
  Result:= FItems[Index];
end;

function TmscrSetExpression.GetValue: variant;
 var i: integer;
begin
  Result:= VarArrayCreate([0, Count - 1], varVariant);

  for i:= 0 to Count - 1 do
     if Items[i] = nil then
       Result[i]:= Null else
       Result[i]:= Items[i].Value;
end;

//=============TmscrScript =============

constructor TmscrScript.Create(AOwner: TComponent);
begin
  inherited;
  FEvaluteRiseError:= False;
  FItems:= TStringList.Create;
  FItems.Sorted:= True;
  FItems.Duplicates:= dupAccept;
  FLines:= TStringList.Create;
  FMacros:= TStringList.Create;
  FIncludePath:= TStringList.Create;
  FIncludePath.Add('');
  FStatement:= TmscrStatement.Create(Self, '', '');
  FSyntaxType:= _SyntaxType;
  FUnitLines:= TStringList.Create;
  FUseClassLateBinding:= False;
end;

destructor TmscrScript.Destroy;
begin
 inherited;
 Clear;
 ClearRTTI;
 FItems.Free;
 FLines.Free;
 FMacros.Free;
 FIncludePath.Free;
 FStatement.Free;
 FUnitLines.Free;
end;

procedure TmscrScript.Add(const Name: string; Item: TObject);
begin
 FItems.AddObject(Name, Item);
 if Item is TmscrVarForBase then
  TmscrVarForBase(Item).AddedBy:= FAddedBy;
end;

function TmscrScript.Count: integer;
begin
 Result:= FItems.Count;
end;

procedure TmscrScript.Remove(Item: TObject);
begin
 FItems.Delete(FItems.IndexOfObject(Item));
end;

function TmscrScript.GetItem(Index: integer): TmscrVarForBase;
begin
  Result:= TmscrVarForBase(FItems.Objects[Index]);
end;

function TmscrScript.GetProgName: string;
begin
  if Assigned(ProjRunning) then
      Result:= ProjRunning.ProjName else
      Result:= FProjName;
end;

function TmscrScript.Find(const Name: string): TmscrVarForBase;
begin
  Result:= FindLocal(Name);

  if (Result = nil) and (FParent<>nil) then
      Result:= FParent.Find(Name);
end;

function TmscrScript.FindLocal(const Name: string): TmscrVarForBase;
var i: integer;
begin
  Result:= nil;
  i:= FItems.IndexOf(Name);
  if (i<>-1) and (FItems.Objects[i] is TmscrVarForBase) then
      Result:= TmscrVarForBase(FItems.Objects[i]);
end;

procedure TmscrScript.ClearItems(Owner: TObject);
var i: integer;
begin
  RemoveItems(Owner);
  FStatement.Clear;
  for i:= 0 to FUnitLines.Count - 1 do FUnitLines.Objects[i].Free;
  FUnitLines.Clear;
end;

procedure TmscrScript.RemoveItems(Owner: TObject);
var i: integer;
begin
  for i:= Count - 1 downto 0 do
     if FItems.Objects[i] is TmscrVarForBase then
       if Items[i].AddedBy = Owner then
       begin
          Items[i].Free;
          Remove(Items[i]);
       end;
end;

procedure TmscrScript.Clear;
 var
 i: integer;
 item: TObject;
begin
 i:= 0;
 while i<FItems.Count do
 begin
  item:= FItems.Objects[i];
  if (item is TmscrRTTILibrary) or ((item is TmscrVarForBase) and (TmscrVarForBase(item).AddedBy = TObject(1))) then
   begin
     Inc(i);
   end else
   begin
     item.Free;
     FItems.Delete(i);
   end;
 end;

 FStatement.Clear;
 for i:= 0 to FUnitLines.Count - 1 do FUnitLines.Objects[i].Free;
 FUnitLines.Clear;
 FErrorPos:= '';
 FErrorMsg:= '';
 FErrorUnit:= '';
end;

procedure TmscrScript.Execute;
begin
  FCalledExit:= False;
  FTerminated:= False;
  FIsRunning:= True;
  FMainProj:= True;
  try
   FStatement.Execute;
  finally
   FCalledExit:= False;
   FTerminated:= False;
   FIsRunning:= False;
  end;
end;

function TmscrScript.Run: boolean;
begin
  Result:= Compile;
  if Result then Execute;
end;

function TmscrScript.Compile: boolean;
 var p: TmscrEngParser;
begin
 Result:= False;
 FErrorMsg:= '';
 p:= TmscrEngParser.Create(Self);
 try
    p.InitGrammar;
    if p.FinalScript_Build(FLines.Text) then p.FinalScript_Parse;
 finally
    p.Free;
 end;
 if FErrorMsg = '' then
 begin
   Result:= True;
   FErrorPos:= '';
 end;
end;

function TmscrScript.GetFinalCode(Stream: TStream): boolean;
var p: TmscrEngParser;
begin
 Result:= False;
 FErrorMsg:= '';

 p:= TmscrEngParser.Create(Self);
 try
    p.InitGrammar;
    if p.FinalScript_Build(FLines.Text) then
      p.FinalScript.SaveToStream(Stream);
 finally
    p.Free;
 end;

 if FErrorMsg = '' then
 begin
   Result:= True;
   FErrorPos:= '';
 end;
end;

function TmscrScript.SetFinalCode(Stream: TStream): boolean;
 var p: TmscrEngParser;
begin
 Result:= False;
 FErrorMsg:= '';

 p:= TmscrEngParser.Create(Self);
 try
   p.InitGrammar;
   p.FinalScript.LoadFromStream(Stream);
   p.FinalScript_Parse;
 finally
   p.Free;
 end;

 if FErrorMsg = '' then
 begin
   Result:= True;
   FErrorPos:= '';
 end;
end;

procedure TmscrScript.Register_Type(const TypeName: string; ParentType: TmscrVarType);
 var v: TmscrVarForType;
begin
  if Find(TypeName)<>nil then
     Exit;
  v:= TmscrVarForType.Create(TypeName, ParentType, '');
  Add(TypeName, v);
end;

function TmscrScript.Register_Record(const aTypeName: String; pti: PTypeInfo): TmscrVarForRecord;
 var rc: TmscrVarForRecord;
begin
  Result:= nil;
  if pti=nil then Exit;
  if Find(aTypeName)<>nil then Exit;
  if pti.Kind<>tkRecord then Exit;

  Result:= TmscrVarForRecord.Create(pti, aTypeName);
  Result.FScrProject:= Self;
  Add(Result.Name, Result);

  rc:= TmscrVarForRecord(Find(aTypeName));
  if rc<>nil then Result.FDefProperty:= rc.DefProperty;
end;

function TmscrScript.Register_Class(AClass: TClass; const ParentClassStr: string): TmscrVarForClass;
 var cl: TmscrVarForClass;
begin
  Result:= nil;
  if Find(AClass.ClassName)<>nil then Exit;

  Result:= TmscrVarForClass.Create(AClass, ParentClassStr);
  Result.FScrProject:= Self;
  Add(Result.Name, Result);

  cl:= TmscrVarForClass(Find(ParentClassStr));
  if cl<>nil then Result.FDefProperty:= cl.DefProperty;
end;

procedure TmscrScript.Register_Const(const Name, Typ: string; const Value: variant);
 var v: TmscrVarForVariable;
begin
  if Find(Name)<>nil then Exit;

  v:= TmscrVarForVariable.Create(Name, StrToVarType(Typ, Self), Typ);
  v.Value:= Value;
  v.IsReadOnly:= True;
  Add(v.Name, v);
end;

procedure TmscrScript.Register_Enum(const Typ, Names: string);
 var
  i: integer;
  v: TmscrVarForVariable;
  sl: TStringList;
begin
  v:= TmscrVarForVariable.Create(Typ, msvtEnum, Typ);
  Add(v.Name, v);

  sl:= TStringList.Create;
  sl.CommaText:= Names;

  try

   For i:= 0 to sl.Count - 1 do
     begin
       v:= TmscrVarForVariable.Create(Trim(sl[i]), msvtEnum, Typ);
       v.Value:= i;
       v.IsReadOnly:= True;
       Add(v.Name, v);
     end;

  finally
     sl.Free;
  end;
end;

procedure TmscrScript.Register_EnumSet(const Typ, Names: string);
var
 sl: TStringList;
 i, j: integer;
 v: TmscrVarForVariable;
begin
 v:= TmscrVarForVariable.Create(Typ, msvtEnum, Typ);
 Add(v.Name, v);
 sl:= TStringList.Create;
 sl.CommaText:= Names;
  try
   j:= 1;
   For i:= 0 to sl.Count - 1 do
    begin
     v:= TmscrVarForVariable.Create(Trim(sl[i]), msvtEnum, Typ);
     v.Value:= j;
     v.IsReadOnly:= True;
     Add(v.Name, v);
     j:= j * 2;
    end;
 finally
    sl.Free;
 end;
end;

procedure TmscrScript.Register_Method(const Syntax: string; CallEvent: TmscrCallMethod; const Category: string = ''; const Description: string = '');
 var v: TmscrVarForMethod;
begin
 v:= TmscrVarForMethod.Create(Syntax, Self);
 v.FOnCall:= CallEvent;
 if Description = '' then
    v.FDescription:= v.Name else
    v.FDescription:= Description;
 v.FCategory:= Category;
 Add(v.Name, v);
end;

procedure TmscrScript.Register_Object(const Name: string; Obj: TObject);
begin
 Register_Variable(Name, Obj.ClassName, fmscrInteger(Obj));
end;

procedure TmscrScript.Register_Variable(const Name, Typ: string; const Value: variant);
 var v: TmscrVarForVariable;
begin
 if Find(Name)<>nil then Exit;
 v:= TmscrVarForVariable.Create(Name, StrToVarType(Typ, Self), Typ);
 v.Value:= Value;
 v.OnGetVarValue:= FOnGetVarValue;
 Add(v.Name, v);
end;

procedure TmscrScript.Register_Form(Form: TComponent);
begin
 Register_Component(Form);
end;

procedure TmscrScript.Register_Component(Form: TComponent);
{$IFNDEF MAGSCR_NOFORMS}
var
 i: integer;
 v: TmscrVarForClass;
{$ENDIF}
begin
{$IFNDEF MAGSCR_NOFORMS}
 v:= FindClass(Form.ClassName);
 if v = nil then
 begin
    if Form.InheritsFrom(TForm) then
      Register_Class(Form.ClassType, 'TForm')
    else 
    if Form.InheritsFrom(TDataModule) then
      Register_Class(Form.ClassType, 'TDataModule')
    else 
    if Form.InheritsFrom(TFrame) then
      Register_Class(Form.ClassType, 'TFrame')
    else
      Exit;
    v:= FindClass(Form.ClassName);
 end;
 for i:= 0 to Form.ComponentCount - 1 do
    v.AddComponent(Form.Components[i]);

 Register_Object(Form.Name, Form);
{$ENDIF}
end;

procedure TmscrScript.ClearRTTI;
 var
 i: integer;
 item: TObject;
begin
 if not FRTTIAdded then Exit;
 i:= 0;
 while i<FItems.Count do
 begin
   item:= FItems.Objects[i];
   if (item is TmscrRTTILibrary) or ((item is TmscrVarForBase) and (TmscrVarForBase(item).AddedBy = TObject(1))) then
   begin
     item.Free;
     FItems.Delete(i);
   end else
   begin
       Inc(i);
   end;   
 end;
 FRTTIAdded:= False;
end;

procedure TmscrScript.AddRTTI;
 var
 i: integer;
 rtti: TmscrRTTILibrary;
 obj: TClass;
begin
 if FRTTIAdded then Exit;
 AddedBy:= TObject(1);
 for i:= 0 to FRTTILibraries.Count - 1 do
 begin
   obj:= TClass(FRTTILibraries[i]);
   rtti:= TmscrRTTILibrary(obj.NewInstance);
   rtti.Create(Self);
   Add('', rtti);
 end;
 AddedBy:= nil;
 FRTTIAdded:= True;
end;

function TmscrScript.CallFunction(const Name: string; const Params: variant; sGlobal: boolean): variant;
 var
 p: TmscrVarForProc;
 i: integer;
 v: TmscrVarForBase;
begin
 if sGlobal then
    v:= Find(Name) else
    v:= FindLocal(Name);
     
 if (v<>nil) and (v is TmscrVarForProc) then
 begin
    p:= TmscrVarForProc(v);
    if VarIsArray(Params) then
    For i:= 0 to VarArrayHighBound(Params, 1) do
         p.Params[i].Value:= Params[i];

    Result:= p.Value;
 end else
 begin
     Result:= Null;
 end;
end;

function TmscrScript.CallFunction1(const Name: string; var Params: variant; sGlobal: boolean): variant;
 var
 i: integer;
 v: TmscrVarForBase;
 p: TmscrVarForProc;
begin
 if sGlobal then
    v:= Find(Name) else
    v:= FindLocal(Name);

 if (v<>nil) and (v is TmscrVarForProc) then
 begin
    p:= TmscrVarForProc(v);
    if VarIsArray(Params) then
    For i:= 0 to VarArrayHighBound(Params, 1) do
         p.Params[i].Value:= Params[i];
    Result:= p.Value;
    if VarIsArray(Params) then
    For i:= 0 to VarArrayHighBound(Params, 1) do
         Params[i]:= p.Params[i].Value;
 end else
 begin
    Result:= Null;
 end;
end;

function TmscrScript.CallFunction2(const Func: TmscrVarForProc; const Params: variant): variant;
 var i: integer;
begin
  if (Func<>nil) then
  begin
     if VarIsArray(Params) then
     For i:= 0 to VarArrayHighBound(Params, 1) do
       Func.Params[i].Value:= Params[i];
     Result:= Func.Value;
  end else
  begin
     Result:= Null;
  end;
end;

function TmscrScript.Evaluate(const Expression: string): variant;
 var
  p: TmscrScript;
 aProj: TmscrScript;
  SaveEvent: TmscrRunLineEvent;
begin
  FEvaluteRiseError:= False;
  Result:= Null;

  if FProjRunning = nil then
     p:= Self else
     p:= FProjRunning;

 aProj:= TmscrScript.Create(nil);
  if not p.FRTTIAdded then aProj.AddRTTI;
 aProj.Parent:= p;
 aProj.OnGetVarValue:= p.OnGetVarValue;
  SaveEvent:= FOnRunLine;
  FOnRunLine:= nil;
  try
    aProj.SyntaxType:= SyntaxType;

     if CompareText(SyntaxType, _SyntaxType) = 0 then
      aProj.Lines.Text:= 'function mscrEvaluateFUNC: Variant; begin Result:= ' + Expression + ' end; begin end.';

     if not aProj.Compile then
     begin
       Result:= aProj.ErrorMsg;
     FEvaluteRiseError:= True;
     end else
     begin
       Result:= aProj.FindLocal('mscrEvaluateFUNC').Value;
     end;

  finally
    aProj.Free;
   FOnRunLine:= SaveEvent;
  end;
end;

function TmscrScript.FindClass(const Name: string): TmscrVarForClass;
 var Item: TmscrVarForBase;
begin
  Item:= Find(Name);
  if (Item<>nil) and (Item is TmscrVarForClass) then
     Result:= TmscrVarForClass(Item) else
     Result:= nil;
end;

function TmscrScript.FindRecord(const Name: String): TmscrVarForRecord; 
 var Item: TmscrVarForBase;
begin
  Item:= Find(Name);
  if (Item<>nil) and (Item is TmscrVarForRecord) then
     Result:= TmscrVarForRecord(Item) else
     Result:= nil;
end;

procedure TmscrScript.RunLine(const UnitName, Index: string);
 var p: TmscrScript;
begin
  p:= Self;
  while p<>nil do
     if Assigned(p.FOnRunLine) then
     begin
       p.FOnRunLine(Self, UnitName, Index);
       break;
     end else
     begin
       p:= p.FParent;
     end;
end;

function TmscrScript.GetVariables(Index: string): variant;
 var v: TmscrVarForBase;
begin
  v:= Find(Index);
  if v<>nil then
     Result:= v.Value else
     Result:= Null;
end;

procedure TmscrScript.SetVariables(Index: string; const Value: variant);
 var v: TmscrVarForBase;
begin
  v:= Find(Index);
  if v<>nil then
     v.Value:= Value else
     Register_Variable(Index, 'Variant', Value);
end;

procedure TmscrScript.SetLines(const Value: TStrings);
begin
  FLines.Assign(Value);
end;

procedure TmscrScript.SetProgName(const Value: string);
begin
  if Assigned(FProjRunning) then
   FProjRunning.FProjName:= Value else
   FProjName:= Value;
end;

procedure TmscrScript.Terminate;

//.......................
 procedure TerminateAll(Script: TmscrScript);
   var i: integer;
  begin
     Script.FCalledExit:= True;
     Script.FTerminated:= True;
   For i:= 0 to Script.Count - 1 do
       if Script.Items[i] is TmscrVarForProc then
          TerminateAll(TmscrVarForProc(Script.Items[i]).Project);
  end;
  //......................

begin
  TerminateAll(Self);
end;

procedure TmscrScript.AddCodeLine(const UnitName, APos: string);
 var
  sl: TStringList;
  LineN: string;
  i: integer;
begin
  i:= FUnitLines.IndexOf(UnitName);

  if (i = -1) then
  begin
     sl:= TStringList.Create;
     sl.Sorted:= True;
   FUnitLines.AddObject(UnitName, sl);
  end else
  begin
     sl:= TStringList(FUnitLines.Objects[i]);
  end;

  LineN:= Copy(APos, 1, Pos(':', APos) - 1);
  if sl.IndexOf(LineN) = -1 then
  begin
     sl.Add(LineN);
  end;
end;

function TmscrScript.IsExecutableLine(LineN: integer; const UnitName: string = ''): boolean;
 var
  sl: TStringList;
  i: integer;
begin
  Result:= False;

  i:= FUnitLines.IndexOf(UnitName);
  if (i = -1) then Exit;

  sl:= TStringList(FUnitLines.Objects[i]);
  if sl.IndexOf(IntToStr(LineN))<>-1 then
     Result:= True;
end;

//=============TmscrStatement =============

constructor TmscrStatement.Create(aProject: TmscrScript; const UnitName, SrcPosition: string);
begin
  inherited Create;
  FScrProject:= aProject;
  FSrcPosition:= SrcPosition;
  FUnitName:= UnitName;
end;

function TmscrStatement.GetItem(Index: integer): TmscrStatement;
begin
  Result:= FItems[Index];
end;

procedure TmscrStatement.Execute;
var
  i: integer;
begin
  FScrProject.ErrorPos:= '';
  for i:= 0 to Count - 1 do
  begin
     if FScrProject.FTerminated then
       break;
     try
     FScrProject.FLastSrcPosition:= Items[i].FSrcPosition;
       Items[i].Execute;
     except
       on E: Exception do
       begin
          if FScrProject.ErrorPos = '' then
          FScrProject.ErrorPos:= FScrProject.FLastSrcPosition;
          raise;
       end;
     end;
     if FScrProject.FCalledBreak or FScrProject.FCalledContinue or FScrProject.FCalledExit then
       break;
  end;
end;

procedure TmscrStatement.RunLine;
begin
  FScrProject.RunLine(FUnitName, FSrcPosition);
end;

//=============TmscrCodeStmAssign_Base =============

destructor TmscrCodeStmAssign_Base.Destroy;
begin
  FDScanner.Free;
  FExpression.Free;
  inherited;
end;

procedure TmscrCodeStmAssign_Base.Optimize;
begin
  FVar:= FDScanner;
  FExpr:= FExpression;

  if FDScanner is TmscrVarForVariableDScanner then
   FVar:= FDScanner.RefItem;
  if TmscrExpression(FExpression).SingleItem<>nil then
   FExpr:= TmscrExpression(FExpression).SingleItem;
end;

procedure TmscrCodeStmAssign_Base.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then Exit;
  FVar.Value:= FExpr.Value;
end;

procedure TmscrCodeStmAssign_Plus.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then Exit;
  FVar.Value:= FVar.Value + FExpr.Value;
end;

procedure TmscrCodeStmAssign_Minus.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then Exit;
  FVar.Value:= FVar.Value - FExpr.Value;
end;

procedure TmscrCodeStmAssign_Mul.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then Exit;
  FVar.Value:= FVar.Value * FExpr.Value;
end;

procedure TmscrCodeStmAssign_Div.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then Exit;
  FVar.Value:= FVar.Value / FExpr.Value;
end;

//=============TmscrCodeStm_Call =============

destructor TmscrCodeStm_Call.Destroy;
begin
  FDScanner.Free;
  inherited;
end;

procedure TmscrCodeStm_Call.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then
     Exit;
  if FModificator = '' then
  begin
   FDScanner.NeedResult:= False;
   FDScanner.Value;
  end else
  if FModificator = '+' then
   FDScanner.Value:= FDScanner.Value + 1
  else
  if FModificator = '-' then
   FDScanner.Value:= FDScanner.Value - 1;
end;

//=============TmscrCodeStm_If =============

constructor TmscrCodeStm_If.Create(aProject: TmscrScript; const UnitName, SrcPosition: string);
begin
  inherited;
  FElsesrcstm:= TmscrStatement.Create(FScrProject, UnitName, SrcPosition);
end;

destructor TmscrCodeStm_If.Destroy;
begin
  FCondition.Free;
  FElsesrcstm.Free;
  inherited;
end;

procedure TmscrCodeStm_If.Execute;
begin
  RunLine;
  if FScrProject.FTerminated then Exit;

  if boolean(FCondition.Value) = True then
     inherited Execute else
   FElsesrcstm.Execute;
end;

//=============TmscrCodeStm_While =============

destructor TmscrCodeStm_While.Destroy;
begin
 FCondition.Free;
 inherited;
end;

procedure TmscrCodeStm_While.Execute;
begin
 RunLine;
 if FScrProject.FTerminated then Exit;
 while boolean(FCondition.Value) = True do
 begin
  inherited Execute;
  if FScrProject.FCalledBreak or FScrProject.FCalledExit then break;
  FScrProject.FCalledContinue:= False;
 end;
 FScrProject.FCalledBreak:= False;
end;

//=============TmscrCodeStm_For =============

destructor TmscrCodeStm_For.Destroy;
begin
  FValueBegin.Free;
  FValueEnd.Free;
  inherited;
end;

procedure TmscrCodeStm_For.Execute;
var
 i, bValue, eValue: integer;
begin
 try
   bValue:= FValueBegin.Value;
   eValue:= FValueEnd.Value;
 finally
   RunLine;
 end;
 if FScrProject.FTerminated then Exit;

 if FDown then
  For i:= bValue downto eValue do
   begin
     FVariable.FValue:= i;
     inherited Execute;
     if FScrProject.FCalledBreak or FScrProject.FCalledExit then break;
    FScrProject.FCalledContinue:= False;
  end else
  For i:= bValue to eValue do
   begin
    FVariable.FValue:= i;
    inherited Execute;
    if FScrProject.FCalledBreak or FScrProject.FCalledExit then break;
    FScrProject.FCalledContinue:= False;
   end;
 FScrProject.FCalledBreak:= False;
end;

//=============TmscrCodeStm_Repeat =============

procedure TmscrCodeStm_Repeat.Execute;
begin
 RunLine;
 if FScrProject.FTerminated then Exit;
 repeat
  inherited Execute;
  if FScrProject.FCalledBreak or FScrProject.FCalledExit then break;
   FScrProject.FCalledContinue:= False;
 until boolean(FCondition.Value) = not FInverseCondition;
 FScrProject.FCalledBreak:= False;
end;

destructor TmscrCodeStm_Repeat.Destroy;
begin
 FCondition.Free;
 inherited;
end;

//=============TmscrCaseSelector =============

destructor TmscrCaseSelector.Destroy;
begin
  FSetExpression.Free;
  inherited;
end;

function TmscrCaseSelector.Check(const Value: variant): boolean;
begin
  Result:= FSetExpression.Check(Value);
end;

//=============TmscrCodeStm_Case =============

constructor TmscrCodeStm_Case.Create(aProject: TmscrScript; const UnitName, SrcPosition: string);
begin
  inherited;
  FElsesrcstm:= TmscrStatement.Create(FScrProject, UnitName, SrcPosition);
end;

destructor TmscrCodeStm_Case.Destroy;
begin
  FCondition.Free;
  FElsesrcstm.Free;
  inherited;
end;

procedure TmscrCodeStm_Case.Execute;
var
  i: integer;
  Value: variant;
  Executed: boolean;
begin
  Value:= FCondition.Value;
  Executed:= False;
  RunLine;
  if FScrProject.FTerminated then Exit;

  for i:= 0 to Count - 1 do
     if TmscrCaseSelector(Items[i]).Check(Value) then
     begin
       Items[i].Execute;
       Executed:= True;
       break;
     end;

  if not Executed then FElsesrcstm.Execute;
end;

//=============TmscrCodesrcstmry =============

constructor TmscrCodesrcstmry.Create(aProject: TmscrScript; const UnitName, SrcPosition: string);
begin
  inherited;
  FExceptSrcStm:= TmscrStatement.Create(aProject, UnitName, SrcPosition);
end;

destructor TmscrCodesrcstmry.Destroy;
begin
  FExceptSrcStm.Free;
  inherited;
end;

procedure TmscrCodesrcstmry.Execute;
 var SaveExitCalled: boolean;
begin
  RunLine;
  if FScrProject.FTerminated then
     Exit;
  if IsExcept then
  begin
     try
       inherited Execute;
     except
       on E: Exception do
       begin
        FScrProject.SetVariables('ExceptionClassName', E.ClassName);
        FScrProject.SetVariables('ExceptionMessage', E.Message);
        FScrProject.ErrorPos:= FScrProject.FLastSrcPosition;
          ExceptSrcStm.Execute;
       end;
     end;
  end else
  begin
     try
       inherited Execute;
   Finally
       SaveExitCalled:= FScrProject.FCalledExit;
     FScrProject.FCalledExit:= False;
       ExceptSrcStm.Execute;
     FScrProject.FCalledExit:= SaveExitCalled;
     end;
  end;
end;

//=============TmscrCodeStm_Break =============

procedure TmscrCodeStm_Break.Execute;
begin
  FScrProject.FCalledBreak:= True;
end;

//=============TmscrCodeStm_Continue =============

procedure TmscrCodeStm_Continue.Execute;
begin
  FScrProject.FCalledContinue:= True;
end;

//=============TmscrCodeStm_Exit =============

procedure TmscrCodeStm_Exit.Execute;
begin
  RunLine;
  FScrProject.FCalledExit:= True;
end;

//=============TmscrCodeStm_With =============

destructor TmscrCodeStm_With.Destroy;
begin
  FDScanner.Free;
  inherited;
end;

procedure TmscrCodeStm_With.Execute;
begin
  inherited;
  FVariable.Value:= FDScanner.Value;
end;


//=============TmscrEngArray =============

constructor TmscrEngArray.Create(const AName: string; DimCount: integer; Typ: TmscrVarType; const TypeName: string);
 var i: integer;
begin
  inherited Create(AName, Typ, TypeName);

  if DimCount<>-1 then
  begin
   For i:= 0 to DimCount - 1 do

  {$IFDEF CPU64}
       Add(TmscrVarForParamItem.Create('', msvtInt64, '', False, False));
  {$ELSE}
       Add(TmscrVarForParamItem.Create('', msvtInt, '', False, False));
  {$ENDIF}

  end else
   For i:= 0 to 2 do

  {$IFDEF CPU64}
       Add(TmscrVarForParamItem.Create('', msvtInt64, '', i > 0, False));
  {$ELSE}
       Add(TmscrVarForParamItem.Create('', msvtInt, '', i > 0, False));
  {$ENDIF}

end;

destructor TmscrEngArray.Destroy;
begin
  inherited;
end;

function TmscrEngArray.GetValue: variant;
 var DimCount: integer;
begin
  DimCount:= VarArrayDimCount(ParentRef.PValue^);
  case DimCount of
     1: Result:= ParentRef.PValue^[Params[0].Value];
     2: Result:= ParentRef.PValue^[Params[0].Value, Params[1].Value];
     3: Result:= ParentRef.PValue^[Params[0].Value, Params[1].Value, Params[2].Value];
     else
       Result:= Null;
  end;
end;

procedure TmscrEngArray.SetValue(const Value: variant);
var
  DimCount: integer;
begin
  DimCount:= VarArrayDimCount(ParentRef.PValue^);
  case DimCount of
     1: ParentRef.PValue^[Params[0].Value]:= Value;
     2: ParentRef.PValue^[Params[0].Value, Params[1].Value]:= Value;
     3: ParentRef.PValue^[Params[0].Value, Params[1].Value, Params[2].Value]:= Value;
  end;
end;

//=============TmscrEngString =============

constructor TmscrEngString.Create;
begin
  inherited Create('__HelperForString', msvtChar, '');
  Add(TmscrVarForParamItem.Create('', msvtInt, '', False, False));
end;

function TmscrEngString.GetValue: variant;
begin
  Result:= string(ParentValue)[integer(Params[0].Value)];
end;

procedure TmscrEngString.SetValue(const Value: variant);
var
  s: string;
begin
  s:= ParentValue;
  s[integer(Params[0].Value)]:= string(Value)[1];
TmscrVarForBase(fmscrInteger(ParentRef)).Value:= s;
end;


//============= TmscrCustomEvent =============

constructor TmscrCustomEvent.Create(AObject: TObject; AHandler: TmscrVarForProc);
begin
  FInstance:= AObject;
  FHandler:= AHandler;
end;

procedure TmscrCustomEvent.CallHandler(Params: array of const);
var
  i, StackIndex: integer;
begin
  StackIndex:= -1;
  if FHandler.Executing then
     StackIndex:= FHandler.SaveLocalVariables;
  try
   For i:= 0 to FHandler.Count - 1 do
     FHandler.Params[i].Value:= VarRecToVariant(Params[i]);
   FHandler.Value;
  finally
   FHandler.RestoreLocalVariables(StackIndex, True);
  end;
end;

//============= TmscrRTTILibrary =============

constructor TmscrRTTILibrary.Create(AScript: TmscrScript);
begin
  FScript:= AScript;
end;

function MSCR_GlobalScripter: TmscrScript;
begin
  if (FGlobalScripter = nil) and not FGlobalScripterIsNIL then
  begin
   FGlobalScripter:= TmscrScript.Create(nil);
   FGlobalScripter.AddRTTI;
  end;
  Result:= FGlobalScripter;
end;

function MSCR_IsGlobalScripterExist: boolean;
begin
  Result:= Assigned(FGlobalScripter);
end;

function MSCR_RTTILibraries: TList;
begin
  if (FRTTILibraries = nil) and not FRTTILibrariesIsNIL then
  begin
   FRTTILibraries:= TList.Create;
   FRTTILibraries.Add(TmscrSysFunctions);
  end;
  Result:= FRTTILibraries;
end;

//============= TmscrVarForVariable =============

function TmscrVarForVariable.GetValue: variant;
begin
  Result:= inherited GetValue;
  if Assigned(FOnGetVarValue) then
  begin
     Result:= FOnGetVarValue(FName, FTyp, FValue);
     if Result = null then
       Result:= FValue;
  end;
end;

procedure TmscrVarForVariable.SetValue(const Value: variant);
begin
  if not FIsReadOnly then
     case FTyp of
       msvtInt:   FValue:= VarAsType(Value, varInteger);
       msvtInt64: FValue:= VarAsType(Value, varInt64);
       msvtBool:  FValue:= VarAsType(Value, varBoolean);

       msvtFloat: if (VarType(Value) = varDate) then
                  FValue:= VarAsType(Value, varDate) else
                  FValue:= VarAsType(Value, varDouble);

       msvtString: FValue:= VarAsType(Value, varString);

       else
        FValue:= Value;
     end;
end;

//==========================================

type
 TByteSet = set of 0..7;
 PByteSet = ^TByteSet;


//=============TmscrNotifyEvent =============

procedure TmscrNotifyEvent.DoEvent(Sender: TObject);
begin
 CallHandler([Sender]);
end;
function TmscrNotifyEvent.GetMethod: Pointer;
begin
 Result:= @TmscrNotifyEvent.DoEvent;
end;

//============= TmscrMouseEvent =============

procedure TmscrMouseEvent.DoEvent(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  b: byte;
begin
  b:= byte(PByteSet(@Shift)^);
  CallHandler([Sender, integer(Button), b, X, Y]);
end;
function TmscrMouseEvent.GetMethod: Pointer;
begin
  Result:= @TmscrMouseEvent.DoEvent;
end;

//=============TmscrMouseMoveEvent =============

procedure TmscrMouseMoveEvent.DoEvent(Sender: TObject; Shift: TShiftState; X, Y: integer);
var
  b: byte;
begin
  b:= byte(PByteSet(@Shift)^);
  CallHandler([Sender, b, X, Y]);
end;
function TmscrMouseMoveEvent.GetMethod: Pointer;
begin
  Result:= @TmscrMouseMoveEvent.DoEvent;
end;

//=============TmscrKeyEvent =============

procedure TmscrKeyEvent.DoEvent(Sender: TObject; var Key: word; Shift: TShiftState);
var
  b: byte;
begin
  b:= byte(PByteSet(@Shift)^);
  CallHandler([Sender, Key, b]);
  Key:= Handler.Params[1].Value;
end;
function TmscrKeyEvent.GetMethod: Pointer;
begin
  Result:= @TmscrKeyEvent.DoEvent;
end;

//=============TmscrKeyPressEvent =============

procedure TmscrKeyPressEvent.DoEvent(Sender: TObject; var Key: char);
begin
  CallHandler([Sender, Key]);
  Key:= string(Handler.Params[1].Value)[1];
end;
function TmscrKeyPressEvent.GetMethod: Pointer;
begin
  Result:= @TmscrKeyPressEvent.DoEvent;
end;

//=============TmscrCloseEvent =============

procedure TmscrCloseEvent.DoEvent(Sender: TObject; var Action: TCloseAction);
begin
  CallHandler([Sender, integer(Action)]);
  Action:= Handler.Params[1].Value;
end;
function TmscrCloseEvent.GetMethod: Pointer;
begin
  Result:= @TmscrCloseEvent.DoEvent;
end;

//=============TmscrCloseQueryEvent =============

procedure TmscrCloseQueryEvent.DoEvent(Sender: TObject; var CanClose: boolean);
begin
  CallHandler([Sender, CanClose]);
  CanClose:= Handler.Params[1].Value;
end;
function TmscrCloseQueryEvent.GetMethod: Pointer;
begin
  Result:= @TmscrCloseQueryEvent.DoEvent;
end;

//=============TmscrCanResizeEvent =============

procedure TmscrCanResizeEvent.DoEvent(Sender: TObject; var NewWidth, NewHeight: integer; var Resize: boolean);
begin
  CallHandler([Sender, NewWidth, NewHeight, Resize]);
  NewWidth:= Handler.Params[1].Value;
  NewHeight:= Handler.Params[2].Value;
  Resize:= Handler.Params[3].Value;
end;
function TmscrCanResizeEvent.GetMethod: Pointer;
begin
  Result:= @TmscrCanResizeEvent.DoEvent;
end;
//====================================================

function StrToVarType(const TypeName: string; Script: TmscrScript): TmscrVarType;
var
  v: TmscrVarForBase;
begin
  v:= Script.Find(TypeName);
  if v = nil then
     Result:= msvtClass else
     Result:= v.Typ;
end;

function ClassesCompatible(const Class1, Class2: string; Script: TmscrScript): boolean;
var
  cl1, cl2: TmscrVarForClass;
begin
  Result:= False;
  cl1:= Script.FindClass(Class1);
  cl2:= Script.FindClass(Class2);
  if (cl1<>nil) and (cl2<>nil) then
     Result:= cl2.ClassRef.InheritsFrom(cl1.ClassRef);
end;


function RecordsCompatible(const Record1, Record2: string; Script: TmscrScript): boolean;
var
  cl1, cl2: TmscrVarForRecord;
begin
  Result:= False;
  cl1:= Script.FindRecord(Record1);
  cl2:= Script.FindRecord(Record2);
  if (cl1<>nil) and (cl2<>nil) then
     Result:= (cl1.TypeInfo=cl2.TypeInfo);
end;

function TypesCompatible(Typ1, Typ2: TmscrTypeRec; Script: TmscrScript): boolean;
begin
  Result:= False;

  case Typ1.Typ of
     msvtInt:
       Result:= Typ2.Typ in [msvtInt, msvtFloat, msvtVariant, msvtEnum, msvtInt64];
     msvtInt64:
       Result:= Typ2.Typ in [msvtInt64, msvtInt, msvtFloat, msvtVariant, msvtEnum];
     msvtFloat:
       Result:= Typ2.Typ in [msvtInt, msvtFloat, msvtVariant];
     msvtBool:
       Result:= Typ2.Typ in [msvtBool, msvtVariant];
     msvtChar, msvtString:
       Result:= Typ2.Typ in [msvtChar, msvtString, msvtVariant];
     msvtClass:
       Result:= (Typ2.Typ = msvtVariant) or ((Typ2.Typ = msvtClass) and ClassesCompatible(Typ1.TypeName, Typ2.TypeName, Script));
     msvtRecord:
       Result:= (Typ2.Typ = msvtVariant) or ((Typ2.Typ = msvtRecord) and RecordsCompatible(Typ1.TypeName, Typ2.TypeName, Script));
     msvtArray:
       Result:= Typ2.Typ in [msvtArray, msvtVariant];
     msvtVariant:
       Result:= True;
     msvtEnum:
     begin
       Result:= Typ2.Typ in [msvtInt, msvtInt64, msvtVariant, msvtEnum];
       if Typ2.Typ = msvtEnum then
          Result:= AnsiCompareText(Typ1.TypeName, Typ2.TypeName) = 0;
     end;
  end;
end;

function AssignCompatible(Var1, Var2: TmscrVarForBase; Script: TmscrScript): boolean;
var
  t1, t2: TmscrTypeRec;
begin
  t1.Typ:= Var1.Typ;
  t1.TypeName:= Var1.TypeName;
  t2.Typ:= Var2.Typ;
  t2.TypeName:= Var2.TypeName;

  Result:= TypesCompatible(t1, t2, Script);
  if Result and (Var1.Typ = msvtInt) and (Var2.Typ = msvtFloat) then
     Result:= False;
end;

function VarRecToVariant(v: TVarRec): variant;
begin
  with v do
     case VType of
       vtInt64:
          Result:= VInt64^;

{$IFDEF CPU64}
       vtInteger:
          Result:= VInteger;
       vtObject:
          Result:= fmscrInteger(VObject);
{$ELSE}
       vtInteger, vtObject:
          Result:= VInteger;
{$ENDIF}
       vtBoolean:
          Result:= VBoolean;
       vtExtended, vtCurrency:
          Result:= VExtended^;
       vtChar:
          Result:= VChar;
       vtString:
          Result:= VString^;
       vtAnsiString:
          Result:= ansistring(VAnsiString);
       vtVariant:
          Result:= VVariant^;
       else
          Result:= Null;
     end;
end;

procedure VariantToVarRec(v: variant; var ar: TmscrVarRecArray; var sPtrList: TList);
var
  i: integer;
  pAStr: PAnsiString;
begin
  SetLength(ar, VarArrayHighBound(v, 1) + 1);

  sPtrList:= TList.Create;
  for i:= 0 to VarArrayHighBound(v, 1) do
     case TVarData(v[i]).VType of
       varSmallint, varInteger, varByte, varShortInt, varWord, varLongWord:
       begin
          ar[i].VType:= vtInteger;
          ar[i].VInteger:= v[i];
       end;
       varInt64:
       begin
          ar[i].VType:= vtInt64;
          New(ar[i].VInt64);
          ar[i].VInt64^:= v[i];
       end;
       varSingle, varDouble, varCurrency, varDate:
       begin
          ar[i].VType:= vtExtended;
          New(ar[i].VExtended);
          ar[i].VExtended^:= v[i];
       end;
       varBoolean:
       begin
          ar[i].VType:= vtBoolean;
          ar[i].VBoolean:= v[i];
       end;
       varString:
       begin
          ar[i].VType:= vtString;
          New(ar[i].VString);
          ar[i].VString^:= v[i];
       end;
       varOleStr:
       begin
          ar[i].VType:= vtAnsiString;
          New(pAStr);
          sPtrList.Add(pAStr);
          pAStr^:= ansistring(v[i]);
          ar[i].VAnsiString:= PAnsiString(pAStr^);
       end;
       varVariant:
       begin
          ar[i].VType:= vtVariant;
          New(ar[i].VVariant);
          ar[i].VVariant^:= v[i];
       end;
     end;
end;

procedure ClearVarRec(var ar: TmscrVarRecArray; var sPtrList: TList);
var
  i: integer;
begin
  for i:= 0 to Length(ar) - 1 do
     if ar[i].VType in [vtExtended, vtString, vtVariant, vtInt64] then
       Dispose(ar[i].VExtended);

  for i:= 0 to sPtrList.Count - 1 do
     Dispose(sPtrList[i]);

  sPtrList.Free;
  Finalize(ar);
end;

function ParserStringToVariant(s: string): variant;
var
  i: int64;
  k: integer;
  iPos: integer;
begin
  Result:= Null;
  if s<>'' then
     if s[1] = '''' then
       Result:= Copy(s, 2, Length(s) - 2)
     else
     begin
       Val(s, i, k);
       if k = 0 then

          if i > MaxInt then
            Result:= i else
            Result:= integer(i)

       else
       begin

          if DecimalSeparator<>'.' then
          begin
            iPos:= Pos('.', s);
            if iPos > 0 then
               s[iPos]:= DecimalSeparator;
          end;
          Result:= StrToFloat(s);
       end;
     end;
end;

function ParseMethodSyntax(const Syntax: string; Script: TmscrScript): TmscrVarForBase;
var
  Parser: TmscrParser;
  i, j: integer;
  Name, Params, TypeName, s: string;
  isFunc, isMacro, varParam: boolean;
  InitValue: variant;
  IsParamWithDef: boolean;
  v: TmscrVarForBase;
  //................................
 procedure AddParams;
  var
     i: integer;
     p: TmscrVarForParamItem;
     sl: TStringList;
  begin
     sl:= TStringList.Create;
     try
       Delete(Params, Length(Params), 1);
       sl.CommaText:= Params;
     For i:= 0 to sl.Count - 2 do
       begin
          p:= TmscrVarForParamItem.Create(sl[i], StrToVarType(TypeName, Script), TypeName, False, varParam);
          Result.Add(p);
       end;
       p:= TmscrVarForParamItem.Create(sl[sl.Count - 1], StrToVarType(TypeName, Script), TypeName, IsParamWithDef, varParam);
       p.DefValue:= InitValue;
       Result.Add(p);

   Finally
       sl.Free;
     end;
  end;
  //...................................
begin
  Parser:= TmscrParser.Create;
  if Script.UseExtendCharset then
   For i:= 128 to 255 do
       Parser.IdentifierCharset:= Parser.IdentifierCharset + [Chr(i)];
  Parser.Text:= Syntax;

  s:= Parser.GetIdent;
  isMacro:= Pos('macro', AnsiLowercase(s)) = 1;
  if isMacro then
     s:= Copy(s, 6, 255);
  isFunc:= CompareText(s, 'function') = 0;
  Name:= Parser.GetIdent;

  if isFunc then
  begin
     j:= Length(Syntax);
     while (Syntax[j]<>':') and (j<>0) do
       Dec(j);

     i:= Parser.Position;
     Parser.Position:= j + 1;
     TypeName:= Parser.GetIdent;
     Parser.Position:= i;
  end else
  begin
     TypeName:= '';
  end;

  Result:= TmscrVarForBase.Create(Name, StrToVarType(TypeName, Script), TypeName);
  Result.IsMacro:= IsMacro;

  Parser.SkipSpaces;
  s:= Parser.GetChar;
  if s = '(' then
  begin
     repeat
       varParam:= False;
       Params:= '';

       repeat
          s:= Parser.GetIdent;
          if CompareText(s, 'var') = 0 then
            varParam:= True
          else if CompareText(s, 'const') = 0 then // do nothing
          else
            Params:= Params + s + ',';
          Parser.SkipSpaces;
          s:= Parser.GetChar;
          if s = ':' then
          begin
            TypeName:= Parser.GetIdent;
            Parser.SkipSpaces;
            i:= Parser.Position;
            IsParamWithDef:= False;
            if Parser.GetChar = '=' then
            begin
               IsParamWithDef:= True;
               s:= Parser.GetNumber;

               if s = '' then s:= Parser.GetString;

               if s = '' then
               begin
                 i:= Parser.Position;
                 s:= Parser.GetChar;

                 if s = '-' then
                    s:= '-' + Parser.GetNumber else
                    Parser.Position:= i;
               end;

               if s<>'' then
               begin
                 InitValue:= ParserStringToVariant(s)
               end else
               begin
                 s:= Parser.GetIdent;
                 v:= Script.Find(s);
                 if v<>nil then
                    InitValue:= v.Value else
                    InitValue:= Null;
               end;

            end else
            begin
               InitValue:= Null;
               Parser.Position:= i;
            end;
            AddParams;
            s:= ';';
          end
          else if s = ')' then
          begin
            Parser.Position:= Parser.Position - 1;
            break;
          end;
       until s = ';';

       Parser.SkipSpaces;
     until Parser.GetChar = ')';
  end;

  Parser.Free;
end;

function MSCR_PosToPoint(const ErrorPos: string): TPoint;
begin
  Result.X:= 0;
  Result.Y:= 0;
  if ErrorPos<>'' then
  begin
     Result.Y:= StrToInt(Copy(ErrorPos, 1, Pos(':', ErrorPos) - 1));
     Result.X:= StrToInt(Copy(ErrorPos, Pos(':', ErrorPos) + 1, 255));
  end;
end;

procedure GenerateDATAContents(aProj: TmscrScript; Item: TmscrDATAItem; FunctionsOnly: boolean = False);
var
  i, j: integer;
  v: TmscrVarForBase;
  c: TmscrVarForClass;
  r: TmscrVarForRecord;
  xi: TmscrDATAItem;
  clItem: TmscrEngCustom;
  s: string;
begin
  Item.FindItem('Functions');
  Item.FindItem('Classes');  
  Item.FindItem('Records');
  Item.FindItem('Types');
  Item.FindItem('Variables');
  Item.FindItem('Constants');

  for i:= 0 to aProj.Count - 1 do
  begin
     v:= aProj.Items[i];
     if not (v is TmscrVarForMethod) and FunctionsOnly then continue;

     if v is TmscrVarForMethod then
     begin
       xi:= Item.FindItem('Functions');
       xi:= xi.FindItem(TmscrVarForMethod(v).Category);
       xi.Text:= 'text="' + xi.Name + '"';
       with xi.Add do
       begin
          Name:= 'item';
          s:= TmscrVarForMethod(v).Syntax;
          Text:= 'text="' + s + '" description="' + TmscrVarForMethod(v).Description + '"';
       end;
     end else //---------------- Classes ----------------------
     if v is TmscrVarForClass then

     begin
       c:= TmscrVarForClass(v);
       xi:= Item.FindItem('Classes');
       xi:= xi.Add;
       with xi do
       begin
          Name:= 'item';
          Text:= 'text="' + c.Name + ' = class(' + c.ParentClassStr + ')"';
       end;

     For j:= 0 to c.MembersCount - 1 do
       begin
          clItem:= c.Members[j];
          with xi.Add do
          begin
            Name:= 'item';
            Text:= 'text="';
            if clItem is TmscrEngProperty then
               Text:= Text + 'property ' + clItem.Name + ': ' + clItem.GetFullTypeName + '"'
            else if clItem is TmscrVarForMethod then
            begin
               s:= TmscrVarForMethod(clItem).Syntax;
               if TmscrVarForMethod(clItem).IndexMethod then
                 s:= 'index property' + Copy(s, Pos(' ', s), 255);
               Text:= Text + s + '"';
            end
            else
               Text:= Text + 'event ' + clItem.Name + '"';
          end;
       end;

     end else //--------------- Records -----------------------
     if v is TmscrVarForRecord then
     begin

       r:= TmscrVarForRecord(v);
       xi:= Item.FindItem('Records');
       xi:= xi.Add;

       with xi do
       begin
          Name:= 'item';
          Text:= 'text="' + r.Name + ' = ' + r.TypeInfo.Name + '"';
       end;

     For j:= 0 to r.MembersCount - 1 do
       begin
          clItem:= r.Members[j];
          with xi.Add do
          begin
            Name:= 'item';
            Text:= 'text="';
            if clItem is TmscrEngRecElement then
               Text:= Text + clItem.Name + ': ' + clItem.GetFullTypeName + '"'

          end;
       end;

     end else //------------- Variables -------------------------
     if v is TmscrVarForVariable then
     begin
       if v.Typ = msvtEnum then
       begin
          xi:= Item.FindItem('Types');
          with xi.FindItem(v.TypeName) do
          begin
            if v.Name<>v.TypeName then
               if Text = '' then
                 Text:= v.Name
               else
                 Text:= Text + ',' + v.Name;
          end;
       end else
       begin

          if v.IsReadOnly then
            xi:= Item.FindItem('Constants') else
            xi:= Item.FindItem('Variables');

          with xi.Add do
          begin
            Name:= 'item';
            Text:= 'text="' + v.Name + ': ' + v.GetFullTypeName;

            if v.IsReadOnly then
               Text:= Text + ' = ' + VarToStr(v.Value);

            Text:= Text + '"';
          end;
       end;
     end;
  end;

  xi:= Item.FindItem('types');
  for i:= 0 to xi.Count - 1 do
     if xi[i].Name<>'item' then
     begin
       xi[i].Text:= 'text="' + xi[i].Name + ': (' + xi[i].Text + ')"';
       xi[i].Name:= 'item';
     end;
end;

procedure GenerateMembers(aProj: TmscrScript; cl: TClass; Item: TmscrDATAItem);
var
  i, j: integer;
  v: TmscrVarForBase;
  c: TmscrVarForClass;
  xi: TmscrDATAItem;
  clItem: TmscrEngCustom;
  s: string;
begin
  for i:= 0 to aProj.Count - 1 do
  begin
     v:= aProj.Items[i];
     if v is TmscrVarForClass then
     begin
       c:= TmscrVarForClass(v);
       if cl.InheritsFrom(c.ClassRef) then
       begin
          xi:= Item;
        For j:= 0 to c.MembersCount - 1 do
          begin
            clItem:= c.Members[j];
            with xi.Add do
            begin
               Name:= 'item';
               Text:= 'text="';
               if clItem is TmscrEngProperty then
                 Text:= Text + 'property ' + clItem.Name + ': ' + clItem.GetFullTypeName + '"'
               else if clItem is TmscrVarForMethod then
               begin
                 s:= TmscrVarForMethod(clItem).Syntax;
                 if TmscrVarForMethod(clItem).IndexMethod then
                    s:= 'index property' + Copy(s, Pos(' ', s), 255);
                 Text:= Text + s + '"';
               end
               else
                 Text:= Text + 'event ' + clItem.Name + '"';
            end;
          end;
       end;
     end;
  end;
end;

function fsSetToString(PropInfo: PPropInfo; const Value: variant): string;
var
  S: TIntegerSet;
  TypeInfo: PTypeInfo;
  I: integer;
begin
  Result:= '';
  TypeInfo:= GetTypeData(PropInfo^.PropType)^.CompType;

  integer(S):= 0;
  if VarIsArray(Value) then
   For I:= 0 to VarArrayHighBound(Value, 1) do
     begin
       integer(S):= integer(S) or Value[I];
     end;

  for I:= 0 to SizeOf(integer) * 8 - 1 do
     if I in S then
     begin
       if Result<>'' then
          Result:= Result + ',';
       Result:= Result + GetEnumName(TypeInfo, I);
     end;
  Result:= '[' + Result + ']';
end;

//====================================================

type

TmsAction_None = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Great = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Less = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_LessEq = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_GreatEq = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_NonEq = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Eq = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Plus = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_StrCat = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Minus = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Or = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Xor = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Mul = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_DivFloat = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_DivInt = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Mod = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_And = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Shl = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Shr = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_LeftBracket = class(TmscrActionBase);

TmsAction_RightBracket = class(TmscrActionBase);

TmsAction_Not = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_UnMinus = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_In = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

TmsAction_Is = class(TmscrActionBase)
 protected
   function GetValue: variant; override;
 end;

//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

function TmsAction_None.GetValue: variant;
begin
 Result:= FLeft.Value;
end;

function TmsAction_Great.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result > FRight.Value;
end;

function TmsAction_Less.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result<FRight.Value;
end;

function TmsAction_LessEq.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result<=FRight.Value;
end;

function TmsAction_GreatEq.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result >= FRight.Value;
end;

function TmsAction_NonEq.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result<>FRight.Value;
end;

function TmsAction_Eq.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result = FRight.Value;
end;

function TmsAction_Plus.GetValue: variant;
begin
 Result:= FLeft.Value;
  if TVarData(Result).Vtype = varEmpty then
    Result:= 0;
 Result:= Result + FRight.Value;
end;

function TmsAction_StrCat.GetValue: variant;
begin
 Result:= FLeft.Value;
 if (TVarData(Result).VType = varString) then
   Result:= VarToStr(Result) + VarToStr(FRight.Value) else
   Result:= Result + FRight.Value;
end;

function TmsAction_Minus.GetValue: variant;
begin
 Result:= FLeft.Value;
 if FOptimizeInt then
 begin
    Result:= fmscrInteger(Result) - fmscrInteger(FRight.Value);
 end else
 begin
     if TVarData(Result).Vtype = varEmpty then
      Result:= 0;
    Result:= Result - FRight.Value;
 end;
end;

function TmsAction_Or.GetValue: variant;
begin
 Result:= FLeft.Value;

 if FOptimizeBool then
 begin
    if boolean(Result) = False then
     Result:= FRight.Value;
 end else
 begin
   Result:= Result or FRight.Value;
 end;  
end;

function TmsAction_Xor.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result xor FRight.Value;
end;

function TmsAction_Mul.GetValue: variant;
begin
 Result:= FLeft.Value;
 if FOptimizeInt then
 begin
  Result:= fmscrInteger(Result) * fmscrInteger(FRight.Value);
 end else
 begin
  if TVarData(Result).Vtype = varEmpty then
     Result:= 0;
   Result:= Result * FRight.Value;
 end;
end;

function TmsAction_DivFloat.GetValue: variant;
begin
 Result:= FLeft.Value;
  if TVarData(Result).Vtype = varEmpty then
    Result:= 0;
 Result:= Result / FRight.Value;
end;

function TmsAction_DivInt.GetValue: variant;
begin
 Result:= FLeft.Value;
 if FOptimizeInt then
 begin
    Result:= fmscrInteger(Result) div fmscrInteger(FRight.Value);
 end else
 begin
     if TVarData(Result).Vtype = varEmpty then
      Result:= 0;
    Result:= Result div FRight.Value;
 end;
end;

function TmsAction_And.GetValue: variant;
begin
 Result:= FLeft.Value;
 if FOptimizeBool then
 begin
   if boolean(Result) = True then
    Result:= FRight.Value;
 end else
 begin
   Result:= Result and FRight.Value;
 end;  
end;

function TmsAction_Mod.GetValue: variant;
begin
 Result:= FLeft.Value;
 if FOptimizeInt then
 begin
  Result:= fmscrInteger(Result) mod fmscrInteger(FRight.Value);
 end else
 begin
  if TVarData(Result).Vtype = varEmpty then
      Result:= 0;
  Result:= Result mod FRight.Value;
 end;
end;

function TmsAction_Shl.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result shl FRight.Value;
end;

function TmsAction_Shr.GetValue: variant;
begin
 Result:= FLeft.Value;
 Result:= Result shr FRight.Value;
end;

function TmsAction_Not.GetValue: variant;
begin
 Result:= not FLeft.Value;
end;

function TmsAction_UnMinus.GetValue: variant;
begin
 Result:= -FLeft.Value;
end;

function TmsAction_In.GetValue: variant;
var
  i: integer;
  ar, val, selfVal: variant;
  Count: integer;
begin
 if FRight is TmscrSetNode then
  begin
    Result:= TmscrSetNode(FRight).FSetExpression.Check(FLeft.Value);
  end else
  begin
    Result:= False;
    ar:= FRight.Value;
    Count:= VarArrayHighBound(ar, 1);
    selfVal:= FLeft.Value;

    i:= 0;
   while i<=Count do
   begin
    val:= ar[i];
    Result:= selfVal = val;
    if (i<Count - 1) and (ar[i + 1] = Null) and not Result then
     begin
       Result:= (selfVal >= val) and (selfVal<=ar[i + 2]);
       Inc(i, 2);
     end;

     if Result then BREAK;       
     Inc(i);
   end;
 end;
end;

function TmsAction_Is.GetValue: variant;
begin
 Result:= TObject(fmscrInteger(FLeft.Value)) is TmscrVarForClass(TmscrDScannerNode(FRight).FDScanner[0].Ref).ClassRef;
end;

//=================TmscrExpressionNode ============================

destructor TmscrExpressionNode.Destroy;
begin
 FLeft.Free;
 FRight.Free;
 inherited;
end;

procedure TmscrExpressionNode.RemoveNode(Node: TmscrExpressionNode);
begin
 if FLeft = Node then
   FLeft:= nil
 else 
 if FRight = Node then
   FRight:= nil;
end;

procedure TmscrExpressionNode.AddNode(Node: TmscrExpressionNode);
begin
 if FLeft = nil then
   FLeft:= Node
 else 
 if FRight = nil then
   FRight:= Node;
   
 if Node<>nil then  
    Node.FParent:= Self;
end;

//=================TmscrActionNode ========================

constructor TmscrActionNode.Create(const AValue: variant);
 var t: TmscrVarType;
begin

{$IFDEF CPU64}
  inherited Create('', msvtInt64, '');
{$ELSE}
  inherited Create('', msvtInt, '');
{$ENDIF}

  Value:= AValue;

  t:= msvtInt;
  if TVarData(AValue).VType = varBoolean then
     t:= msvtBool
  else 
  if TVarData(AValue).VType in [varSingle, varDouble, varCurrency] then
     t:= msvtFloat
  else 
  if (TVarData(AValue).VType = varInt64) then
     t:= msvtInt64
  else 
  if (TVarData(AValue).VType = varOleStr) or (TVarData(AValue).VType = varString) then
     t:= msvtString;

  Typ:= t;
end;

function TmscrActionNode.Priority: integer;
begin
  Result:= 0;
end;

//=================TmscrActionBase ====================

constructor TmscrActionBase.Create(Op: TmscrOperatorType);
begin

{$IFDEF CPU64}
  inherited Create('', msvtInt64, '');
{$ELSE}
  inherited Create('', msvtInt, '');
{$ENDIF}

  FOp:= Op;
end;

function TmscrActionBase.Priority: integer;
begin
  case FOp of
     msopNone:
       Result:= 7;
     msopLeftBracket:
       Result:= 6;
     msopRightBracket:
       Result:= 5;
     msopGreat, msopLess, msopGreatEq, msopLessEq, msopNonEq, msopEq, msopIn, msopIs:
       Result:= 4;
     msopPlus, msopMinus, msopOr, msopXor:
       Result:= 3;
     msopMul, msopDivFloat, msopDivInt, msopMod, msopAnd, msopShr, msopShl:
       Result:= 2;
     msopNot, msopUnMinus:
       Result:= 1;
     else
       Result:= 0;
  end;
end;


//=================TmscrDScannerNode ========================

constructor TmscrDScannerNode.Create(ADScanner: TmscrDScanner);
begin
  inherited Create(0);
  FDScanner:= ADScanner;
  Typ:= ADScanner.Typ;
  TypeName:= ADScanner.TypeName;
  if FDScanner is TmscrVarForVariableDScanner then
   FVar:= FDScanner.RefItem
  else
   FVar:= FDScanner;
end;

destructor TmscrDScannerNode.Destroy;
begin
  FDScanner.Free;
  inherited;
end;

function TmscrDScannerNode.GetValue: variant;
begin
  Result:= FVar.Value;
end;

//=================TmscrSetNode ==============================

constructor TmscrSetNode.Create(ASet: TmscrSetExpression);
begin
  inherited Create(0);
  FSetExpression:= ASet;
  Typ:= msvtVariant;
end;

destructor TmscrSetNode.Destroy;
begin
  FSetExpression.Free;
  inherited;
end;

function TmscrSetNode.GetValue: variant;
begin
  Result:= FSetExpression.Value;
end;

//================= TmscrExpression =============================

function TmscrExpression.GetValue: variant;
begin
 Result:= FNode.Value;
end;

constructor TmscrExpression.Create(Script: TmscrScript);
begin

{$IFDEF CPU64}
  inherited Create('', msvtInt64, '');
{$ELSE}
  inherited Create('', msvtInt, '');
{$ENDIF}

  FNode:= TmsAction_None.Create(msopNone);
  FCurNode:= FNode;
  FScript:= Script;
end;

destructor TmscrExpression.Destroy;
begin
 FNode.Free;
 inherited;
end;

procedure TmscrExpression.AddOperator(const Op: string);
var
  Node: TmscrExpressionNode;
  n, n1: TmscrExpressionNode;
  //......................................
  function CreateOperaTmsAction_Or(s: string): TmscrActionBase;
  begin
     s:= AnsiUpperCase(s);
     if s = ' ' then
       Result:= TmsAction_None.Create(msopNone)
     else if s = '>' then
       Result:= TmsAction_Great.Create(msopGreat)
     else if s = '<' then
       Result:= TmsAction_Less.Create(msopLess)
     else if s = '<=' then
       Result:= TmsAction_LessEq.Create(msopLessEq)
     else if s = '>=' then
       Result:= TmsAction_GreatEq.Create(msopGreatEq)
     else if s = '<>' then
       Result:= TmsAction_NonEq.Create(msopNonEq)
     else if s = '=' then
       Result:= TmsAction_Eq.Create(msopEq)
     else if s = '+' then
       Result:= TmsAction_Plus.Create(msopPlus)
     else if s = 'STRCAT' then
       Result:= TmsAction_StrCat.Create(msopPlus)
     else if s = '-' then
       Result:= TmsAction_Minus.Create(msopMinus)
     else if s = 'OR' then
       Result:= TmsAction_Or.Create(msopOr)
     else if s = 'XOR' then
       Result:= TmsAction_Xor.Create(msopXor)
     else if s = '*' then
       Result:= TmsAction_Mul.Create(msopMul)
     else if s = '/' then
       Result:= TmsAction_DivFloat.Create(msopDivFloat)
     else if s = 'DIV' then
       Result:= TmsAction_DivInt.Create(msopDivInt)
     else if s = 'MOD' then
       Result:= TmsAction_Mod.Create(msopMod)
     else if s = 'AND' then
       Result:= TmsAction_And.Create(msopAnd)
     else if s = 'SHL' then
       Result:= TmsAction_Shl.Create(msopShl)
     else if s = 'SHR' then
       Result:= TmsAction_Shr.Create(msopShr)
     else if s = '(' then
       Result:= TmsAction_LeftBracket.Create(msopLeftBracket)
     else if s = ')' then
       Result:= TmsAction_RightBracket.Create(msopRightBracket)
     else if s = 'NOT' then
       Result:= TmsAction_Not.Create(msopNot)
     else if s = 'UNMINUS' then
       Result:= TmsAction_UnMinus.Create(msopUnMinus)
     else if s = 'IN' then
       Result:= TmsAction_In.Create(msopIn)
     else if s = 'IS' then
       Result:= TmsAction_Is.Create(msopIs)
     else
       Result:= nil;
  end;
   //...........................................
begin
 Node:= CreateOperaTmsAction_Or(Op);
 Node.SrcPosition:= SrcPosition;

 if (Op = '(') or (Op = 'unminus') or (Op = 'not') then
    AddOperand(Node)
 else if Op = ')' then
 begin
   n:= FCurNode;
   while n.Priority<=Node.Priority do
     n:= n.FParent;

   n.FParent.RemoveNode(n);
   n.FParent.AddNode(n.FLeft);

   Node.Free;
   Node:= n.FLeft;
   n.FLeft:= nil;
   n.Free;
 end else 
 if FCurNode = FNode then
   FNode.AddNode(Node)
 else
 begin
   n:= FCurNode;
   n1:= nil;
   if FCurNode.Priority<>6 then
   begin
     n:= FCurNode.FParent;
     n1:= FCurNode;
   end;

   while n.Priority<=Node.Priority do
   begin
     n1:= n;
     n:= n.FParent;
   end;

   n.RemoveNode(n1);
   n.AddNode(Node);
   Node.AddNode(n1);
 end;

 FCurNode:= Node;
end;

procedure TmscrExpression.AddOperand(Node: TmscrExpressionNode);
begin
 FCurNode.AddNode(Node);
 FCurNode:= Node;
end;

procedure TmscrExpression.Register_Const(const AValue: variant);
 var xNode: TmscrActionNode;
begin
 xNode:= TmscrActionNode.Create(AValue);
 xNode.SrcPosition:= SrcPosition;
 AddOperand(xNode);
end;

procedure TmscrExpression.Register_ConstWithType(const AValue: variant; aTyp: TmscrVarType);
begin
 Register_Const(AValue);
 if aTyp = msvtClass then
   FCurNode.Typ:= msvtVariant;
end;

procedure TmscrExpression.AddDScanner(ADScanner: TmscrDScanner);
var
 Node: TmscrDScannerNode;
begin
 Node:= TmscrDScannerNode.Create(ADScanner);
 Node.SrcPosition:= SrcPosition;
 AddOperand(Node);
end;

procedure TmscrExpression.AddSet(ASet: TmscrSetExpression);
var
  Node: TmscrSetNode;
begin
  Node:= TmscrSetNode.Create(ASet);
  Node.SrcPosition:= SrcPosition;
  AddOperand(Node);
end;

function TmscrExpression.Finalize: string;
var
  ErrorPos: string;
  TypeRec: TmscrTypeRec;
  //...................................
  function GetType(Item: TmscrExpressionNode): TmscrTypeRec;
  var
     Typ1, Typ2: TmscrTypeRec;
     op: TmscrOperatorType;
     Error: boolean;
  begin
     if Item = nil then
       Result.Typ:= msvtVariant
     else if Item is TmscrActionNode then
     begin
       Result.Typ:= Item.Typ;
       Result.TypeName:= Item.TypeName;
     end else
     begin
       Typ1:= GetType(Item.FLeft);
       Typ2:= GetType(Item.FRight);

       if (Typ1.Typ = msvtBool) and (Typ2.Typ = msvtBool) then
        TmscrActionBase(Item).FOptimizeBool:= True;

       op:= TmscrActionBase(Item).FOp;

       if (op = msopIs) and (Typ1.Typ = msvtClass) and (Typ2.Typ = msvtClass) then
       begin
          Error:= False
       end else
       begin

          Error:= not TypesCompatible(Typ1, Typ2, FScript);

          if not Error then
            case Typ1.Typ of
               msvtBool:
                 Error:= not (op in [msopNonEq, msopEq, msopOr, msopXor, msopAnd, msopNot]);
               msvtChar, msvtString:
                 Error:= not (op in [msopGreat, msopLess, msopLessEq, msopGreatEq, msopNonEq, msopEq, msopPlus, msopIn]);
               msvtClass,msvtRecord, msvtArray:
                 Error:= not (op in [msopNonEq, msopEq]);
            end;
       end;

       if not Error then
       begin
          Result:= Typ1;

          if [Typ1.Typ] + [Typ2.Typ] = [msvtInt, msvtFloat] then
            Result.Typ:= msvtFloat;

          if (Typ1.Typ = msvtInt) and (Typ2.Typ = msvtInt) and (op = msopDivFloat) then
            Result.Typ:= msvtFloat;
          if [Typ1.Typ] + [Typ2.Typ] = [msvtInt64, msvtFloat] then
            Result.Typ:= msvtFloat;

          if ((Typ1.Typ = msvtInt64) or (Typ1.Typ = msvtInt)) and ((Typ2.Typ = msvtInt64) or (Typ2.Typ = msvtInt64)) and (op = msopDivFloat) then
            Result.Typ:= msvtFloat;

          if op in [msopGreat, msopLess, msopLessEq, msopGreatEq, msopNonEq, msopEq, msopIn, msopIs] then
            Result.Typ:= msvtBool;
       end
       else if ErrorPos = '' then
          ErrorPos:= Item.SrcPosition;

       Item.Typ:= Result.Typ;
     end;
  end;
  //.............................................
begin

  FCurNode:= FNode.FLeft;
  FNode.RemoveNode(FCurNode);
  FNode.Free;
  FNode:= FCurNode;

  ErrorPos:= '';
  TypeRec:= GetType(FNode);
  Typ:= TypeRec.Typ;
  TypeName:= TypeRec.TypeName;
  Result:= ErrorPos;

  if not ((FNode is TmscrDScannerNode) and not (TmscrDScannerNode(FNode).FDScanner.IsReadOnly)) then
     IsReadOnly:= True;
end;

procedure TmscrExpression.SetValue(const Value: variant);
begin
  if not IsReadOnly then
   TmscrDScannerNode(FNode).FDScanner.Value:= Value;
end;

function TmscrExpression.Optimize(DScanner: TmscrDScanner): string;
var
  Op: TmscrOperatorType;
begin
  Result:= ' ';

  if not (DScanner is TmscrVarForVariableDScanner) or not (FNode is TmscrActionBase) then
     Exit;

  Op:= TmscrActionBase(FNode).FOp;
  if not (Op in [msopPlus, msopMinus, msopDivFloat, msopMul]) then
     Exit;


  if (FNode.FLeft is TmscrDScannerNode) and (TmscrDScannerNode(FNode.FLeft).FDScanner is TmscrVarForVariableDScanner) and
     (TmscrDScannerNode(FNode.FLeft).FDScanner.RefItem = DScanner.RefItem) then
  begin
   FCurNode:= FNode.FRight;
   FNode.RemoveNode(FCurNode);
   FNode.Free;
   FNode:= FCurNode;

     if Op = msopPlus then
       Result:= '+'
     else if Op = msopMinus then
       Result:= '-'
     else if Op = msopDivFloat then
       Result:= '/'
     else if Op = msopMul then
       Result:= '*';
  end

  else if (FNode.FRight is TmscrDScannerNode) and (TmscrDScannerNode(FNode.FRight).FDScanner is TmscrVarForVariableDScanner) and
     (TmscrDScannerNode(FNode.FRight).FDScanner.RefItem = DScanner.RefItem) and (Op in [msopPlus, msopMul]) and
     not (DScanner.RefItem.Typ in [msvtString, msvtVariant]) then
  begin
   FCurNode:= FNode.FLeft;
   FNode.RemoveNode(FCurNode);
   FNode.Free;
   FNode:= FCurNode;

     if Op = msopPlus then
       Result:= '+'
     else if Op = msopMul then
       Result:= '*';
  end;
end;

function TmscrExpression.SingleItem: TmscrVarForBase;
begin
  Result:= nil;

  if FNode is TmscrDScannerNode then
  begin
     if TmscrDScannerNode(FNode).FDScanner is TmscrVarForVariableDScanner then
       Result:= TmscrDScannerNode(FNode).FDScanner.RefItem
     else
       Result:= TmscrDScannerNode(FNode).FDScanner;
  end
  else if FNode is TmscrActionNode then
     Result:= FNode;
end;

//=============== TmscrEngParser =================================

constructor TmscrEngParser.Create(aProject: TmscrScript);
begin
 FNeedDeclareVars:= True;
 FScrProject := aProject;
 FGrammar := TmscrDATADocument.Create;
 FFinalScript:= TmscrDATADocument.Create;
 FParser  := TmscrParser.Create;
 FUsesList:= TStringList.Create;
 FWithList:= TStringList.Create;
end;

destructor TmscrEngParser.Destroy;
begin
 FGrammar.Free;
 FFinalScript.Free;
 FParser.Free;
 FUsesList.Free;
 FWithList.Free;
 inherited;
end;

procedure TmscrEngParser.InitGrammar;
var
 i: integer;
 ss: TStringStream;
begin
 FParser.Clear;
 //.....................................
 ss:= TStringStream.Create(_MAINGRAMMAR);
 try
  FGrammar.LoadFromStream(ss);
 finally
    ss.Free;
 end;
 //.....................................

 FRoot:= FGrammar.Root;

 FParser.InitKeywords;

 if FScrProject.UseExtendCharset then
  For i:= 128 to 255 do
    FParser.IdentifierCharset:= FParser.IdentifierCharset + [Chr(i)];
end;

function TmscrEngParser.FindError(const aErr: integer): string;
begin
 Result:= msEUnError;
 Case aErr of
     1 : Result:= msError01;
     2 : Result:= msError02;
     3 : Result:= msError03;
     4 : Result:= msError04;
     5 : Result:= msError05;
     6 : Result:= msError06;
     7 : Result:= msError07;
     8 : Result:= msError08;
     9 : Result:= msError09;
     10: Result:= msError10;
     11: Result:= msError11;
     12: Result:= msError12;
     13: Result:= msError13;
     14: Result:= msError14;
     15: Result:= msError15;
     16: Result:= msError16;
     17: Result:= msError17;
     18: Result:= msError18;
     19: Result:= msError19;
     20: Result:= msError20;
  end;
end;

function TmscrEngParser.FinalScript_Build(const Text: string): boolean;
var
  FList: TStrings;
  FStream: TStream;
  FErrorMsg: string;
  FErrorPos: string;
  FTermError: boolean;
  i: integer;
//.............................................
  function _Run(xi: TmscrDATAItem): boolean;
  var
     i, j, ParsPos, ParsPos1, LoopPos, ListPos: integer;
     s, NodeName, Token, PropText, PropAdd, PropAddText, PropNode: string;
     Completed, TopLevelNode, Flag: boolean;
  const
          PathD  = {$IFDEF MSWINDOWS} '\' {$ELSE} '/' {$ENDIF};
    //.............................................
   procedure __DoInclude(const Name: string);
     var
       sl: TStringList;
       p: TmscrEngParser;
       ss: TStringStream;
       s, UnitPath: string;
       idx: integer;
     begin
       if FUsesList.IndexOf(Name)<>-1 then
          Exit;
     FUsesList.Add(Name);
       sl:= TStringList.Create;
       try
          if Assigned(FScrProject.OnGetUnit) then
          begin
            s:= '';
          FScrProject.OnGetUnit(FScrProject, Name, s);
            sl.Text:= s;
          end
          else
          begin
            UnitPath:= '';
          For idx:= 0 to FScrProject.IncludePath.Count - 1 do
           begin
               UnitPath:= FScrProject.IncludePath[idx];
               if (UnitPath<>'') and (PathD<>UnitPath[Length(UnitPath)]) then
                 UnitPath:= UnitPath + PathD;
               if FileExists(UnitPath + Name) then
                 break;
           end;
            sl.LoadFromFile(UnitPath + Name);
         end;

          p:= TmscrEngParser.Create(FScrProject);
          p.FUnitName:= Name;
          ss:= TStringStream.Create('');
          try
            s:= '';
            if sl.Count > 0 then
            begin
               p.InitGrammar;
               p.FUsesList.Assign(FUsesList);
               if p.FinalScript_Build(sl.Text) then
               begin
               FUsesList.Assign(p.FUsesList);
                 p.FinalScript.SaveToStream(ss);
                 s:= ss.DataString;
                 Delete(s, 1, Pos('?>', s) + 1);
               end
               else
               begin
               FErrorMsg:= FScrProject.ErrorMsg;
               FErrorPos:= FScrProject.ErrorPos;
                 if FScrProject.ErrorUnit = '' then
                  FScrProject.ErrorUnit:= Name;
               end;
            end;

         FList.Insert(ListPos, '</uses>');
         FList.Insert(ListPos, s);
         FList.Insert(ListPos, '<uses' + ' unit="' + Name + '">');
         Inc(ListPos, 3);
        Finally
            p.Free;
            ss.Free;
          end;
     Finally
          sl.Free;
       end;
     end;
     //.............................................
   procedure __CheckPropNode(Flag: boolean);
     var
       i, ParsPos1: integer;
       s: string;
     begin
     
    if CompareText(PropNode, 'uses') = 0 then
     begin
         while FList.Count > ListPos do
         begin
           s:= FList[FList.Count - 1];
           i:= Pos('text="', s);
           Delete(s, 1, i + 5);
           i:= Pos('" ', s);
           Delete(s, i, 255);
           __DoInclude(Copy(s, 2, Length(s) - 2));
         FList.Delete(FList.Count - 1);
         end;
      end else 
      if PropNode<>'' then
          if Flag then
          begin
            ParsPos1:= FParser.Position;
            FParser.Position:= ParsPos;
            FParser.SkipSpaces;

            s:= '<' + PropNode + ' pos="' + FParser.GetXYPosition + '"';
            FParser.Position:= ParsPos1;

            if PropNode = 'expr' then
               s:= s + ' pos1="' + FParser.GetXYPosition + '"';
             s:= s + '>';

            FList.Insert(ListPos, s);
           FList.Add('</' + PropNode + '>');
       end else
       begin
         while FList.Count > ListPos do FList.Delete(FList.Count - 1);
       end;
     end;
     //.............................................
   procedure __AddError(xi: TmscrDATAItem);
     var
      propErr:Integer;
       ss:string;
   begin
     ss:=xi.Prop['errornum'];
     if ss='' then exit;

     propErr:= StrToInt(ss);
     if (PropErr>0) and (FErrorMsg='') then
     begin
       FErrorMsg:= FindError(PropErr);
       FParser.Position:= ParsPos;
       FParser.SkipSpaces;
       FErrorPos:= FParser.GetXYPosition;
       FTermError:= xi.Prop['term'] = '1';
     end;
   end;
     //.............................................

  begin
     Result:= True;
     ParsPos:= FParser.Position;
     ListPos:= FList.Count;
     NodeName:= AnsiLowerCase(xi.Name);
    propText:= AnsiLowerCase(xi.Prop['text']);
    propNode:= LowerCase(xi.Prop['node']);
     TopLevelNode:= xi.Parent = FRoot;

     Completed:= False;
   Flag:= False;
     Token:= '';

     if TopLevelNode then
       Completed:= True
     else
     if NodeName = 'char' then
     begin
      if xi.Prop['skip']<>'0' then
       FParser.SkipSpaces;
       Token:= FParser.GetChar;
       Flag:= True;
     end else
     if NodeName = 'keyword' then
     begin
       Token:= FParser.GetWord;
     Flag:= True;
     end else
     if NodeName = 'ident' then
     begin
       Token:= FParser.GetIdent;
     Flag:= True;
     end else
     if NodeName = 'number' then
     begin
       Token:= FParser.GetNumber;
     Flag:= True;
     end else
     if NodeName = 'string' then
     begin
       Token:= FParser.GetString;
     Flag:= True;
     end else
     if NodeName = 'frstring' then
     begin
       Token:= FParser.GetFRString;
       s:= FParser.GetXYPosition;
     FList.Add('<dsgn pos="' + s + '">');
     FList.Add('<node text="Get" pos="' + s + '"/>');
     FList.Add('<expr pos="' + s + '">');
     FList.Add('<string text="''' + msStrToXML(Token) + '''" pos="' + s + '"/>');
     FList.Add('</expr>');
     FList.Add('</dsgn>');
     Flag:= True;
     end else
     if NodeName = 'eol' then
       Completed:= FParser.GetEOL
     else
     if NodeName = 'dojob' then
       Completed:= True
     else
     if (NodeName = 'switch') or (NodeName = 'optswitch') then
     begin
       Completed:= True;

     For i:= 0 to xi.Count - 1 do
       begin
          Completed:= _Run(xi[i]);

          if Completed then BREAK;
       end;

       if not Completed then
          if NodeName<>'optswitch' then
          begin
            Result:= False;
            __AddError(xi);
          end;

       EXIT;

     end else
     if (NodeName = 'loop') or (NodeName = 'optloop') then
     begin
       j:= 0;
       repeat
          Inc(j);
        Flag:= False;
          LoopPos:= FParser.Position;

        For i:= 0 to xi.Count - 1 do
          begin
            Result:= _Run(xi[i]);
            if not Result then
            begin
             Flag:= True;
               break;
            end;
          end;

          ParsPos1:= FParser.Position;
          if Result and (PropText<>'') then
          begin
          FParser.SkipSpaces;
            if FParser.GetChar<>PropText then
            begin
             FParser.Position:= ParsPos1;
             Flag:= True;
            end;
          end;

          if FParser.Position = LoopPos then Flag:= True;

       until Flag;

       if j > 1 then
       begin
          if (xi.Prop['skip'] = '1') or FTermError then FErrorMsg:= '';
        FParser.Position:= ParsPos1;
          Result:= True;
       end;

       if NodeName = 'optloop' then
       begin
          if not Result then FParser.Position:= ParsPos;
          Result:= True;
       end;
       Exit;

     end else
     if NodeName = 'optional' then
     begin
     For i:= 0 to xi.Count - 1 do
          if not _Run(xi[i]) then
          begin
          FParser.Position:= ParsPos;
            break;
          end;
       Exit;
     end else
     begin
       j:= FRoot.Find(NodeName);
       if j = -1 then raise Exception.Create(msEInvalidLanguage);
       Completed:= _Run(FRoot[j]);
     end;

     if Flag then
     begin
       if FParser.CaseSensitive then
          Completed:= (Token<>'') and ((PropText = '') or (Token = PropText)) else
          Completed:= (Token<>'') and ((PropText = '') or (AnsiCompareText(Token, PropText) = 0));
     end;

     if not Completed then
     begin
       Result:= False;
       __AddError(xi);
     end else
     begin
       if not TopLevelNode then __CheckPropNode(True);

      propAdd:= xi.Prop['add'];
      propAddText:= xi.Prop['addtext'];

       if PropAdd<>'' then
       begin
          if PropAddText = '' then
            s:= Token else
            s:= PropAddText;

        FList.Add('<' + PropAdd + ' text="' + msStrToXML(s) + '" pos="' + FParser.GetXYPosition + '"/>');
       end;

     For i:= 0 to xi.Count - 1 do
       begin
          Result:= _Run(xi[i]);
          if not Result then
            break;
       end;
     end;

     if not Result then   FParser.Position:= ParsPos;
     if TopLevelNode then __CheckPropNode(Result);
  end;
//...............................................

begin
  FList:= TStringList.Create;
  FErrorMsg:= '';
  FErrorPos:= '';
  Result:= False;

  try
   FParser.Text:= Text;

     i:= 1;
     if FParser.GetChar = '#' then
     begin
       if CompareText(FParser.GetIdent, 'language') = 0 then
       begin
          i:= FParser.Position;

{$IFDEF MSWINDOWS}
          while (i<=Length(Text)) and (Text[i]<>#13) do
{$ELSE}
          while (i<=Length(Text)) and (Text[i]<>#10) do
{$ENDIF}
               Inc(i);
          InitGrammar;
          Inc(i, 2);
       end;
     end;
   FParser.Position:= i;

   if _Run(FRoot.FindItem('program')) and (FErrorMsg = '') then
   begin
    FErrorMsg:= '';
    FErrorPos:= '';
    FStream:= TMemoryStream.Create;
    try
       FList.Insert(0, '<?xml version="1.0"?>');
       FList.Insert(1, '<program>');
       FList.Add('</program>');
       FList.SaveToStream(FStream);
       FStream.Position:= 0;
       FFinalScript.LoadFromStream(FStream);
       FFinalScript.Root.Add.Assign(FRoot.FindItem('types'));
       Result:= True;
    Finally
       FStream.Free;
    end;

   end;

   FScrProject.ErrorPos:= FErrorPos;
   FScrProject.ErrorMsg:= FErrorMsg;
  finally
   FList.Free;
  end;
end;

function TmscrEngParser.PropPos(xi: TmscrDATAItem): string;
begin
  Result:= xi.Prop['pos'];
end;

procedure TmscrEngParser.ErrorPos(xi: TmscrDATAItem);
begin
  FErrorPos:= PropPos(xi);
end;

procedure TmscrEngParser.FinalScript_Parse;
begin
 FWithList.Clear;
 FScrProject.ErrorUnit:= '';
 FUnitName:= '';
 FUsesList.Clear;
 try
   Compile_Project(FFinalScript.Root, FScrProject);
   FScrProject.ErrorPos:= '';
 except
   on e: Exception do
   begin
    FScrProject.ErrorMsg:= e.Message;
    FScrProject.ErrorPos:= FErrorPos;
    FScrProject.ErrorUnit:= FUnitName;
   end;
 end;
end;

procedure TmscrEngParser.CheckIdent(aProj: TmscrScript; const Name: string);
begin
  if aProj.FindLocal(Name)<>nil then
     raise Exception.Create(msEIdRedeclared + '''' + Name + '''');
end;

function TmscrEngParser.FindClass(const TypeName: string): TmscrVarForClass;
begin
  Result:= FScrProject.FindClass(TypeName);
  if Result = nil then
     raise Exception.Create(msEUnknownType + '''' + TypeName + '''');
end;

function TmscrEngParser.FindRecord(const TypeName: string): TmscrVarForRecord;
begin
 Result:= FScrProject.FindRecord(TypeName);
 if Result = nil then
    raise Exception.Create(msEUnknownType + '''' + TypeName + '''');
end;

procedure TmscrEngParser.CheckTypeCompatibility(Var1, Var2: TmscrVarForBase);
begin
 if not AssignCompatible(Var1, Var2, FScrProject) then
    raise Exception.Create(msEIncompatibleTypes + ': ''' + Var1.GetFullTypeName + ''', ''' + Var2.GetFullTypeName + '''');
end;

function TmscrEngParser.FindType(s: string): TmscrVarType;
 var xi: TmscrDATAItem;
begin
 xi:= FProjRoot.FindItem('types');

 if xi.Find(s)<>-1 then
 begin
   s:= xi[xi.Find(s)].Prop['type']
 end else
 begin
   xi:= FGrammar.Root.FindItem('types');
   if xi.Find(s)<>-1 then
     s:= xi[xi.Find(s)].Prop['type'];
 end;

 Result:= StrToVarType(s, FScrProject);

 if Result = msvtClass then FindClass(s);
 if Result = msvtRecord then FindRecord(s);
end;

function TmscrEngParser.FindVar(aProj: TmscrScript; const Name: string): TmscrVarForBase;
begin
 Result:= aProj.Find(Name);
 if Result = nil then
    if not FNeedDeclareVars then
    begin
      Result:= TmscrVarForVariable.Create(Name, msvtVariant, '');
    FScrProject.Add(Name, Result);
    end else
    begin
      raise Exception.Create(msEUndeclaredId + '''' + Name + '''');
    end;
end;


// ct9999 zzzz
function TmscrEngParser.CreateVar(xi: TmscrDATAItem; aProj: TmscrScript; const Name: string; Statement: TmscrStatement = nil;
                                  CreateParam: boolean = False; IsVarParam: boolean = False): TmscrVarForBase;
var
  i, j: integer;
  Typ: TmscrVarType;
  TypeName: string;
  RefItem: TmscrVarForBase;
  InitValue: variant;
  InitItem: TmscrDATAItem;
  CodeAssign: TmscrCodeStmAssign_Base;
  IsPascal: boolean;
  SrcPosition: string;
  vRecA,vRecB:TmscrVarForRecord; 
  vRecElA,vRecElB:TmscrEngRecElement;

 //..............................................
 procedure _DoArray(xi: TmscrDATAItem);
  var
     i, n: integer;
     v: array of SizeInt;
     Expr: TmscrExpression;
  begin
     n:= xi.Count;
     SetLength(v, n * 2);

   For i:= 0 to n - 1 do
     begin
       Expr:= Compile_Expression(xi[i][0], aProj);
       v[i * 2]:= Expr.Value;
       Expr.Free;

       if xi[i].Count = 2 then
       begin
          Expr:= Compile_Expression(xi[i][1], aProj);
          v[i * 2 + 1]:= Expr.Value;
          Expr.Free;
       end
       else
       begin
          v[i * 2 + 1]:= v[i * 2] - 1;
          v[i * 2]:= 0;
       end;
     end;

     if n = 0 then
     begin
       SetLength(v, 2);
       v[0]:= 0;
       v[1]:= 0;
       n:= 1;
     end;

     InitValue:= VarArrayCreate(v, varVariant);
     RefItem:= TmscrEngArray.Create('', n, Typ, TypeName);
    aProj.Add('', RefItem);
     v:= nil;
     Typ:= msvtArray;
  end;
  //..............................................
 procedure _DoInit(xi: TmscrDATAItem);
  var
     Expr: TmscrExpression;
     Temp: TmscrVarForVariable;
  begin
     Temp:= TmscrVarForVariable.Create('', Typ, TypeName);
     try
       Expr:= Compile_Expression(xi[0], aProj);
       InitValue:= Expr.Value;
       try
          CheckTypeCompatibility(Temp, Expr);
     Finally
          Expr.Free;
       end;
   Finally
       Temp.Free;
     end;
  end;
  //..............................................
begin
  RefItem:= nil;
  InitItem:= nil;
  TypeName:= 'Variant';
  IsPascal:= False;
  SrcPosition:= FErrorPos;

  for i:= 0 to xi.Count - 1 do
     if CompareText(xi[i].Name, 'type') = 0 then
     begin
       IsPascal:= i<>0;
       TypeName:= xi[i].Prop['text'];
       ErrorPos(xi[i]);
       break;
     end;

  Typ:= FindType(TypeName);
  case Typ of
     msvtInt, msvtInt64, msvtFloat, msvtClass:
       InitValue:= 0;
     msvtRecord:
       InitValue:= nil;
     msvtBool:
       InitValue:= False;
     msvtChar, msvtString:
       InitValue:= '';
     else
       InitValue:= Null;
  end;

  for i:= 0 to xi.Count - 1 do
     if CompareText(xi[i].Prop['text'], Name) = 0 then
     begin

       j:= i + 1;
       while (j<xi.Count) and (IsPascal or (CompareText(xi[j].Name, 'ident')<>0)) do
       begin
          if CompareText(xi[j].Name, 'array') = 0 then
            _DoArray(xi[j])
          else if CompareText(xi[j].Name, 'init') = 0 then
          begin
            if Statement = nil then
               _DoInit(xi[j]);
            InitItem:= xi[j];
          end;
          Inc(j);
       end;
       break;
     end;

  if CreateParam then
     Result:= TmscrVarForParamItem.Create(Name, Typ, TypeName, InitItem<>nil, IsVarParam) 
  else
  if Typ in [msvtRecord] then
  begin
     vRecA:=FindRecord(TypeName);

     if vRecA<>Nil then
       begin
         Result:= TmscrVarForRecord.Create(vRecA.TypeInfo, TypeName);
         vRecB:=TmscrVarForRecord(Result);

         if vRecB.MembersCount<vRecA.MembersCount then
        For i:=0 to vRecA.GetMembersCount-1 do
            begin
               vRecElA:=TmscrEngRecElement(vRecA.Members[i]);
               vRecElB:=TmscrEngRecElement.Create(vRecElA.Name,vRecElA.Typ,vRecElA.TypeName);
               vRecElB.ParentRef  :=  Result;
               vRecElB.ParentValue:=  Result.Value;
               vRecElB.FOnGetValue:=  vRecElA.FOnGetValue;
               vRecElB.FOnSetValue:=  vRecElA.FOnSetValue;
               vRecElB.IsReadOnly :=  vRecElA.IsReadOnly;

             vRecB.FMembers.Add(vRecElB);
            end;

       end;

  end else
  if Typ in [msvtChar, msvtString] then
     Result:= TmscrVarForString.Create(Name, Typ, TypeName)
  else
     Result:= TmscrVarForVariable.Create(Name, Typ, TypeName);

  try
     if Typ<>msvtRecord then
     Result.Value:= InitValue;
     Result.RefItem:= RefItem;
     Result.SrcPosition:= SrcPosition;
     Result.SourceUnit:= FUnitName;
     Result.OnGetVarValue:= FScrProject.OnGetVarValue;


     if (InitItem<>nil) and (Statement<>nil) then
     begin
       CodeAssign:= TmscrCodeStmAssign_Base.Create(aProj, FUnitName, PropPos(xi));
       Statement.Add(CodeAssign);
       CodeAssign.DScanner:= TmscrVarForVariableDScanner.Create(aProj);
       CodeAssign.DScanner.RefItem:= Result;
       CodeAssign.Expression:= Compile_Expression(InitItem[0], aProj);
       CheckTypeCompatibility(Result, CodeAssign.Expression);
       CodeAssign.Optimize;
     end;

  except
     on e: Exception do
     begin
       Result.Free;
       raise;
     end;
  end;
end;

{$HINTS OFF}
function TmscrEngParser.DoDScanner(xi: TmscrDATAItem; aProj: TmscrScript; EmitOp: TmscrEmitOp = mseoNone): TmscrDScanner;
var
  i, j: integer;
  NodeName, NodeText, TypeName: string;
  Expr: TmscrExpression;
  Item, PriorItem: TmscrDScannerItem;
  ClassVar: TmscrVarForClass;   
  RecordVar: TmscrVarForRecord;
  StringVar: TmscrVarForString;
  Typ: TmscrVarType;
  LateBinding, PriorIsIndex: boolean;
  NewDScanner: TmscrDScanner;
  PriorValue: variant;
  Component: TComponent;
  //.....................................
  function FindInWithList(const Name: string; ResultDS: TmscrDScanner; Item: TmscrDScannerItem): boolean;
  var
     i: integer;
     Withsrcstm: TmscrCodeStm_With;
     WithItem: TmscrDScannerItem;
     ClassVar: TmscrVarForClass;
     xi1: TmscrDATAItem;
  begin
     Result:= False;
     LateBinding:= False;
   For i:= FWithList.Count - 1 downto 0 do
     begin

       if aProj.FindLocal(FWithList[i]) = nil then
          continue;
       Withsrcstm:= TmscrCodeStm_With(FWithList.Objects[i]);

       if Withsrcstm.Variable.Typ = msvtVariant then
       begin
          if aProj.Find(Name)<>nil then Exit;

          Item.Ref:= Withsrcstm.Variable;
          ResultDS.Finalize;
          ResultDS.LateBindingDATASource:= TmscrDATAItem.Create;
          ResultDS.LateBindingDATASource.Assign(xi);
          xi1:= TmscrDATAItem.Create;
          xi1.Name:= 'node';
          xi1.Text:= 'text="' + FWithList[i] + '"';
          ResultDS.LateBindingDATASource.InsertItem(0, xi1);
          LateBinding:= True;
          Result:= True;
          break;
       end else
       begin
          ClassVar:= FindClass(Withsrcstm.Variable.TypeName);
          Item.Ref:= ClassVar.Find(NodeText);
       end;

       if Item.Ref<>nil then
       begin
          WithItem:= TmscrDScannerItem.Create;
          WithItem.Ref:= Withsrcstm.Variable;
          WithItem.SrcPosition:= Item.SrcPosition;

          ResultDS.Remove(Item);
          ResultDS.Add(WithItem);
          ResultDS.Add(Item);
          Result:= True;
          break;
       end;
     end;
  end;
  //.....................................
{$IFDEF MAGSCR_USEOLE}
 procedure CreateOLEHelpers(Index: integer);
  var
     i: integer;
     OLEHelper: TmscrEngOLE;
  begin
   For i:= Index to xi.Count - 1 do
     begin
       ErrorPos(xi[i]);
       NodeName:= LowerCase(xi[i].Name);
       NodeText:= xi[i].Prop['text'];

       if (NodeName = 'node') and (NodeText<>'[') then
       begin
          Item:= TmscrDScannerItem.Create;
          Result.Add(Item);
          Item.SrcPosition:= FErrorPos;
          OLEHelper:= TmscrEngOLE.Create(NodeText);
         aProj.Add('', OLEHelper);
          Item.Ref:= OLEHelper;
       end else
       if NodeName = 'expr' then
       begin
          Expr:= Compile_Expression(xi[i], aProj);
          PriorItem:= Result.Items[Result.Count - 1];
          PriorItem.Add(Expr);
          PriorItem.Ref.Add(TmscrVarForParamItem.Create('', msvtVariant, '', False, False));
       end;
     end;
  end;

{$ENDIF}
 //.....................................
begin
  Result:= TmscrDScanner.Create(aProj);
  try

   For i:= 0 to xi.Count - 1 do
     begin
       ErrorPos(xi[i]);
       NodeName:= LowerCase(xi[i].Name);
       NodeText:= xi[i].Prop['text'];

       if NodeName = 'node' then
       begin
          Item:= TmscrDScannerItem.Create;
          Result.Add(Item);
          Item.SrcPosition:= FErrorPos;

          if Result.Count = 1 then
          begin
            if not FindInWithList(NodeText, Result, Item) then
               Item.Ref:= FindVar(aProj, NodeText);


            if LateBinding then Exit;

            if EmitOp = mseoCreate then
            begin
               if not (Item.Ref is TmscrVarForClass) then
                 raise Exception.Create(msEClassRequired);
               ClassVar:= TmscrVarForClass(Item.Ref);
               Item:= TmscrDScannerItem.Create;
               Result.Add(Item);
               Item.Ref:= ClassVar.Find('Create');
            end;
          end else
          begin
            PriorItem:= Result.Items[Result.Count - 2];
            PriorIsIndex:= (PriorItem.Ref is TmscrVarForMethod) and TmscrVarForMethod(PriorItem.Ref).IndexMethod and not PriorItem.Flag;
            Typ:= PriorItem.Ref.Typ;
            
            //---------- msvtVariant --------------
            if (Typ = msvtVariant) and not PriorIsIndex then
            begin
               PriorValue:= PriorItem.Ref.Value;
               if VarIsNull(PriorValue) then
               begin
                 Result.Remove(Item);
                 Item.Free;
                 Result.Finalize;
                 Result.LateBindingDATASource:= TmscrDATAItem.Create;
                 Result.LateBindingDATASource.Assign(xi);
                 Exit;
               end else
               begin
                 if (TVarData(PriorValue).VType = varString) then
                    Typ:= msvtString
  {$IFDEF MAGSCR_USEOLE}
                 else if TVarData(PriorValue).VType = varDispatch then
                 begin
                    Result.Remove(Item);
                    Item.Free;
                    CreateOLEHelpers(i);
                    Result.Finalize;
                    Exit;
                 end
  {$ENDIF}
                 else
                 if (TVarData(PriorValue).VType and varArray) = varArray then
                 begin
                    if NodeText = '[' then
                      Item.Ref:= FindVar(aProj, '__HelperForArray') else
                      raise Exception.Create(msEIndexRequired);
                    continue;
                 end else
                 if (TVarData(PriorValue).VType and varRecord) = varRecord then
                 begin
                    Typ:= msvtRecord;
                    PriorItem.Ref:= FindRecord(PriorItem.Ref.Value);
                    continue;
                 end else
                 begin
                    Typ:= msvtClass;
                    PriorItem.Ref.TypeName:= TObject(fmscrInteger(PriorItem.Ref.Value)).ClassName;
                 end;
               end;
            end;

            if PriorIsIndex then
            begin
               PriorItem.Flag:= True;
               Result.Remove(Item);
               Item.Free;
             FErrorPos:= PriorItem.SrcPosition;
               if NodeText<>'[' then
                 raise Exception.Create(msEIndexRequired);
            end else
            //---------- msvtString --------------
            if Typ = msvtString then
            begin
               if NodeText = '[' then
                 Item.Ref:= FindVar(aProj, '__HelperForString')
               else
                 raise Exception.Create(msEStringError);
            end else
            //---------- msvtClass ----------------
            if Typ = msvtClass then
            begin
               TypeName:= PriorItem.Ref.TypeName;
               ClassVar:= FindClass(TypeName);

               if NodeText = '[' then
               begin
                 Item.Flag:= True;
                 Item.Ref:= ClassVar.DefProperty;
                 if Item.Ref = nil then
                    raise Exception.CreateFmt(msEClassError, [TypeName]);
               end
               else
               begin
                 Item.Ref:= ClassVar.Find(NodeText);

                 if Item.Ref = nil then
                 begin
                    PriorValue:= PriorItem.Ref.Value;
                    if ((VarIsNull(PriorValue) or (PriorValue = 0)) and not aProj.IsRunning) and aProj.UseClassLateBinding then
                    begin
                      Result.Remove(Item);
                      Item.Free;
                      while Result.Count > 1 do
                      begin
                         Item:= Result.Items[Result.Count - 1];
                         Result.Remove(Item);
                         Item.Free;
                      end;
                      Item:= Result.Items[0];
                      Result.Finalize;
                      Result.Typ:= msvtVariant;
                      Result.LateBindingDATASource:= TmscrDATAItem.Create;
                      Result.LateBindingDATASource.Assign(xi);
                      Exit;
                    end
                    else
                    begin

                      if TObject(fmscrInteger(PriorValue)) is TComponent then
                      begin
                         Component:= TComponent(fmscrInteger(PriorValue)).FindComponent(NodeText);
                         if Component<>nil then
                         begin
                           Item.Ref:= TmscrVarForBase.Create('', msvtClass, Component.ClassName);
                           Item.Ref.Value:= fmscrInteger(Component);
                         end;
                      end;
                      if Item.Ref = nil then
                         raise Exception.Create(msEUndeclaredId + '''' + NodeText + '''');
                    end;
                 end;
               end;
            end else      
            //---------- msvtRecord ----------------
            if Typ = msvtRecord then
            begin
               TypeName:= PriorItem.Ref.TypeName;
               RecordVar:= FindRecord(TypeName);

               if NodeText = '[' then
               begin
                 Item.Flag:= True;
                 Item.Ref:= RecordVar.DefProperty;
                 if Item.Ref = nil then
                    raise Exception.CreateFmt(msEClassError, [TypeName]);
               end
               else
               begin
                 Item.Ref:= RecordVar.Find(NodeText);

                 if Item.Ref = nil then
                 begin
                    PriorValue:= PriorItem.Ref.Value;
                    if ((VarIsNull(PriorValue) or (PriorValue = 0)) and not aProj.IsRunning) and aProj.UseClassLateBinding then
                    begin
                      Result.Remove(Item);
                      Item.Free;
                      while Result.Count > 1 do
                      begin
                         Item:= Result.Items[Result.Count - 1];
                         Result.Remove(Item);
                         Item.Free;
                      end;
                      Item:= Result.Items[0];
                      Result.Finalize;
                      Result.Typ:= msvtVariant;
                      Result.LateBindingDATASource:= TmscrDATAItem.Create;
                      Result.LateBindingDATASource.Assign(xi);
                      Exit;
                    end
                    else
                    begin

                      if TObject(fmscrInteger(PriorValue)) is TComponent then
                      begin
                         Component:= TComponent(fmscrInteger(PriorValue)).FindComponent(NodeText);
                         if Component<>nil then
                         begin
                           Item.Ref:= TmscrVarForBase.Create('', msvtRecord, Component.ClassName);
                           Item.Ref.Value:= fmscrInteger(Component);
                         end;
                      end;
                      if Item.Ref = nil then
                         raise Exception.Create(msEUndeclaredId + '''' + NodeText + '''');
                    end;
                 end;
               end;
            end else
            //---------- msvtArray ----------------
            if Typ = msvtArray then
               Item.Ref:= PriorItem.Ref.RefItem
            else
            //---------- Nothing from Above -------
               raise Exception.Create(msEArrayRequired);
          end;
       end
       else if NodeName = 'expr' then
       begin
          Expr:= Compile_Expression(xi[i], aProj);
          Result.Items[Result.Count - 1].Add(Expr);
       end
       else if NodeName = 'addr' then
       begin
          if xi.Count<>2 then
            raise Exception.Create(msEVarRequired);

          Item:= TmscrDScannerItem.Create;
          Result.Add(Item);
          ErrorPos(xi[1]);
          Item.SrcPosition:= FErrorPos;

          StringVar:= TmscrVarForString.Create('', msvtString, '');
          StringVar.Value:= xi[1].Prop['text'];
         aProj.Add('', StringVar);
          Item.Ref:= StringVar;

          break;
       end;
     end;

     if EmitOp = mseoFree then
     begin
       PriorItem:= Result.Items[Result.Count - 1];
       if (PriorItem.Ref.Typ<>msvtClass) and (PriorItem.Ref.Typ<>msvtVariant) then
          raise Exception.Create(msEClassRequired);
       Item:= TmscrDScannerItem.Create;
       Result.Add(Item);
       ClassVar:= FindClass('TObject');
       Item.Ref:= ClassVar.Find('Free');
     end;

     Result.Finalize;
     if Result.Kind<>msskOther then
     begin
       NewDScanner:= nil;
       if Result.Kind = msskVariable then
          NewDScanner:= TmscrVarForVariableDScanner.Create(aProj)
       else if Result.Kind = msskStringArray then
          NewDScanner:= TmscrStringDScanner.Create(aProj)
       else if Result.Kind = msskArray then
          NewDScanner:= TmscrArrayDScanner.Create(aProj);

       NewDScanner.Borrow(Result);
       Result.Free;
       Result:= NewDScanner;
     end;

   For i:= 0 to Result.Count - 1 do
     begin
       Item:= Result[i];
     FErrorPos:= Item.SrcPosition;
       if Item.Ref is TmscrDScanner then
          continue;

       if Item.Count<Item.Ref.GetNumberOfRequiredParams then
          raise Exception.Create(msENotEnoughParams)
       else if Item.Count > Item.Ref.Count then
          raise Exception.Create(msETooManyParams)
       else if Item.Count<>Item.Ref.Count then
        For j:= Item.Count to Item.Ref.Count - 1 do
          begin
            Expr:= TmscrExpression.Create(FScrProject);
            Item.Add(Expr);
            Expr.Register_ConstWithType(Item.Ref[j].DefValue, Item.Ref[j].Typ);
            Expr.Finalize;
          end;

     For j:= 0 to Item.Count - 1 do
       begin
        FErrorPos:= Item[j].SrcPosition;
          CheckTypeCompatibility(Item.Ref[j], Item[j]);
       end;
     end;

  except
     on e: Exception do
     begin
       Result.Free;
       raise;
     end;
  end;
end;

{$HINTS ON}

function TmscrEngParser.Compile_Set(xi: TmscrDATAItem; aProj: TmscrScript): TmscrSetExpression;
var
  i: integer;
  Name: string;
begin
  Result:= TmscrSetExpression.Create('', msvtVariant, '');
  try
   For i:= 0 to xi.Count - 1 do
     begin
       Name:= LowerCase(xi[i].Name);
       if Name = 'expr' then
          Result.Add(Compile_Expression(xi[i], aProj))
       else if Name = 'range' then
          Result.Add(nil);
     end;

  except
     on e: Exception do
     begin
       Result.Free;
       raise;
     end;
  end;
end;

function TmscrEngParser.Compile_Expression(xi: TmscrDATAItem; aProj: TmscrScript): TmscrExpression;
var
  ErPos: string;
  SrcPosition1, SrcPosition2: TPoint;
  //.........................................
 procedure DoExpressionItems(xi: TmscrDATAItem; Expression: TmscrExpression);
  var
     i: integer;
     NodeName: string;
     OpName: string;
  begin
     i:= 0;
     while i<xi.Count do
     begin
       ErrorPos(xi[i]);
       Expression.SrcPosition:= FErrorPos;
       NodeName:= Lowercase(xi[i].Name);
       OpName:= xi[i].Prop['text'];

       if (NodeName = 'op') then
       begin
          OpName:= LowerCase(OpName);
          if (OpName = ')') or (i<xi.Count - 1) then
            Expression.AddOperator(OpName);
       end else 
       if (NodeName = 'number') or (NodeName = 'string') then
          Expression.Register_Const(ParserStringToVariant(OpName))
       else 
       if NodeName = 'dsgn' then
          Expression.AddDScanner(DoDScanner(xi[i], aProj))
       else 
       if NodeName = 'set' then
          Expression.AddSet(Compile_Set(xi[i], aProj))
       else 
       if NodeName = 'new' then
          Expression.AddDScanner(DoDScanner(xi[i][0], aProj, mseoCreate))
       else 
       if NodeName = 'expr' then
          DoExpressionItems(xi[i], Expression);

       Inc(i);
     end;
  end;
  //.........................................
  function GetSource(pt1, pt2: TPoint): string;
   var i1, i2: integer;
  begin
     i1:= FParser.GetPlainPosition(pt1);
     i2:= FParser.GetPlainPosition(pt2);
     if (i1 = -1) or (i2 = -1) then
       Result:= ''
     else
       Result:= Copy(FParser.Text, i1, i2 - i1);
  end;
  //.........................................
begin
  Result:= TmscrExpression.Create(FScrProject);
  try
     DoExpressionItems(xi, Result);
     SrcPosition1:= MSCR_PosToPoint(PropPos(xi));
     SrcPosition2:= MSCR_PosToPoint(xi.Prop['pos1']);
     Result.Source:= GetSource(SrcPosition1, SrcPosition2);

     ErPos:= Result.Finalize;
     if ErPos<>'' then
     begin
     FErrorPos:= ErPos;
       raise Exception.Create(msEIncompatibleTypes);
     end;

  except
     on e: Exception do
     begin
       Result.Free;
       raise;
     end;
  end;
end;

procedure TmscrEngParser.Compile_Uses(xi: TmscrDATAItem; aProj: TmscrScript);
var
  i: integer;
  SaveUnitName: string;
  s: string;
  sl: TStringList;
  ms: TMemoryStream;
  xd: TmscrDATADocument;
begin
 SaveUnitName:= FUnitName;
 FUnitName:= xi.Prop['unit'];
 xd:= nil;

 if FUsesList.IndexOf(FUnitName)<>-1 then
 begin
  FUnitName:= SaveUnitName;
    Exit;
 end;
 FUsesList.Add(FUnitName);
 if Assigned(FScrProject.OnGetFinalUnit) then
 begin
  s:= '';
  FScrProject.OnGetFinalUnit(FScrProject, FUnitName, s);
  if s<>'' then
   begin
     sl:= TStringList.Create;
     sl.Text:= s;

     ms:= TMemoryStream.Create;
     sl.SaveToStream(ms);
     sl.Free;
     ms.Position:= 0;

     xd:= TmscrDATADocument.Create;
     xd.LoadFromStream(ms);
     ms.Free;
   end;
 end;

 if xd<>nil then
 begin
  try
       Compile_Project(xd.Root, aProj);
  Finally
       xd.Free;
   end;
 end else
 begin
  For i:= 0 to xi.Count - 1 do
      Compile_Project(xi[i], aProj);
 end;

 FUnitName:= SaveUnitName;
end;

procedure TmscrEngParser.Compile_Var(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
  i: integer;
  Name: string;
begin
  for i:= 0 to xi.Count - 1 do
  begin
     ErrorPos(xi[i]);
     if CompareText(xi[i].Name, 'ident') = 0 then
     begin
       Name:= xi[i].Prop['text'];
       CheckIdent(aProj, Name);
      aProj.Add(Name, CreateVar(xi, aProj, Name, Statement));
     end;
  end;
end;

procedure TmscrEngParser.Compile_Const(xi: TmscrDATAItem; aProj: TmscrScript);
var
  Name: string;
  Expr: TmscrExpression;
  v: TmscrVarForVariable;
begin
  Name:= xi[0].Prop['text'];
  ErrorPos(xi[0]);
  CheckIdent(aProj, Name);

  Expr:= Compile_Expression(xi[1], aProj);
  v:= TmscrVarForVariable.Create(Name, Expr.Typ, Expr.TypeName);
  v.Value:= Expr.Value;
  v.IsReadOnly:= True;
  Expr.Free;

 aProj.Add(Name, v);
end;

procedure TmscrEngParser.Compile_Parameters(xi: TmscrDATAItem; v: TmscrVarForProc);
var
  i: integer;
  s: string;
  varParams: boolean;
 //.........................................
 procedure DoParam(xi: TmscrDATAItem);
  var
     i: integer;
     Name: string;
     Param: TmscrVarForParamItem;
     varParam: boolean;
  begin
     varParam:= False;

   For i:= 0 to xi.Count - 1 do
     begin
       ErrorPos(xi[i]);
       if CompareText(xi[i].Name, 'varparam') = 0 then
          varParam:= True
       else if CompareText(xi[i].Name, 'ident') = 0 then
       begin
          Name:= xi[i].Prop['text'];
          CheckIdent(v.Project, Name);
          Param:= TmscrVarForParamItem(CreateVar(xi, v.Project, Name, nil, True, varParams or VarParam));
          Param.DefValue:= Param.Value;
          v.Add(Param);
          v.Project.Add(Name, Param);
          varParam:= False;
       end;
     end;
  end;
 //.........................................
begin
  if CompareText(xi.Name, 'parameters')<>0 then
     Exit;
  varParams:= False;
  for i:= 0 to xi.Count - 1 do
  begin
     s:= LowerCase(xi[i].Name);
     if s = 'varparams' then
       varParams:= True
     else if s = 'var' then
     begin
       DoParam(xi[i]);
       varParams:= False;
     end;
  end;
end;

procedure TmscrEngParser.Compile_ProcA(xi: TmscrDATAItem; aProj: TmscrScript);
var
  i: integer;
  s, Name: string;
 proc: TmscrVarForProc;
begin
  ErrorPos(xi[0]);
  Name:= xi[0].Prop['text'];
  CheckIdent(aProj, Name);

{$IFDEF CPU64}
 proc:= TmscrVarForProc.Create(Name, msvtInt64, '', aProj, False);
{$ELSE}
 proc:= TmscrVarForProc.Create(Name, msvtInt, '', aProj, False);
{$ENDIF}

 proc.SrcPosition:= PropPos(xi);
 proc.SourceUnit:= FUnitName;
 aProj.Add(Name, Proc);

  for i:= 0 to xi.Count - 1 do
  begin
     s:= LowerCase(xi[i].Name);
     if s = 'parameters' then
       Compile_Parameters(xi[i], Proc);
  end;
end;

procedure TmscrEngParser.Compile_FuncA(xi: TmscrDATAItem; aProj: TmscrScript);
var
  i: integer;
  s, Name, TypeName: string;
  Typ: TmscrVarType;
  Func: TmscrVarForProc;
begin
  Name:= '';
  TypeName:= '';
  Typ:= msvtVariant;

  for i:= 0 to xi.Count - 1 do
  begin
     ErrorPos(xi[i]);
     s:= LowerCase(xi[i].Name);
     if s = 'type' then
     begin
       TypeName:= xi[i].Prop['text'];
       Typ:= FindType(TypeName);
     end
     else if s = 'name' then
     begin
       Name:= xi[i].Prop['text'];
       CheckIdent(aProj, Name);
     end;
  end;


  Func:= TmscrVarForProc.Create(Name, Typ, TypeName, aProj, CompareText(TypeName, 'void')<>0);
  Func.SrcPosition:= PropPos(xi);
  Func.SourceUnit:= FUnitName;
 aProj.Add(Name, Func);

  for i:= 0 to xi.Count - 1 do
  begin
     s:= LowerCase(xi[i].Name);
     if s = 'parameters' then
       Compile_Parameters(xi[i], Func);
  end;
end;

procedure TmscrEngParser.Compile_ProcB(xi: TmscrDATAItem; aProj: TmscrScript);
var
  Name: string;
 proc: TmscrVarForProc;
begin
  Name:= xi[0].Prop['text'];
 proc:= TmscrVarForProc(FindVar(aProj, Name));
  Compile_Project(xi, Proc.Project);
end;

procedure TmscrEngParser.Compile_FuncB(xi: TmscrDATAItem; aProj: TmscrScript);
var
  i: integer;
  s, Name: string;
  Func: TmscrVarForProc;
begin
  Name:= '';

  for i:= 0 to xi.Count - 1 do
  begin
     s:= LowerCase(xi[i].Name);
     if s = 'name' then
       Name:= xi[i].Prop['text'];
  end;

  Func:= TmscrVarForProc(FindVar(aProj, Name));
  Compile_Project(xi, Func.Project);
end;

procedure TmscrEngParser.Compile_Assign(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var
 i: integer;
 srcstm: TmscrCodeStmAssign_Base;
 DScanner: TmscrDScanner;
 Expression: TmscrExpression;
 Modificator: string;
begin
  DScanner:= nil;
  Expression:= nil;

 try
     Modificator:= ' ';
     DScanner:= DoDScanner(xi[0], aProj);

     i:= 1;
     if CompareText(xi[1].Name, 'modificator') = 0 then
     begin
       Modificator:= xi[1].Prop['text'];
       Inc(i);
     end;
     Expression:= Compile_Expression(xi[i], aProj);

     if DScanner.IsReadOnly then
       raise Exception.Create(msELeftCantAssigned);

     CheckTypeCompatibility(DScanner, Expression);
     if Modificator = ' ' then
       Modificator:= Expression.Optimize(DScanner);
 except
     on e: Exception do
     begin
       if DScanner<>nil then
          DScanner.Free;
       if Expression<>nil then
          Expression.Free;
       raise;
     end;
 end;

 case Modificator[1] of
     '+':       srcstm:= TmscrCodeStmAssign_Plus.Create(aProj, FUnitName, PropPos(xi));
     '-':       srcstm:= TmscrCodeStmAssign_Minus.Create(aProj, FUnitName, PropPos(xi));
     '*':       srcstm:= TmscrCodeStmAssign_Mul.Create(aProj, FUnitName, PropPos(xi));
     '/':       srcstm:= TmscrCodeStmAssign_Div.Create(aProj, FUnitName, PropPos(xi));
 else
    srcstm:= TmscrCodeStmAssign_Base.Create(aProj, FUnitName, PropPos(xi));
 end;

  Statement.Add(srcstm);
  srcstm.DScanner:= DScanner;
  srcstm.Expression:= Expression;
  srcstm.Optimize;
  FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Call(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var srcstm: TmscrCodeStm_Call;
begin
 srcstm:= TmscrCodeStm_Call.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(srcstm);
 srcstm.DScanner:= DoDScanner(xi[0], aProj);
 if xi.Count > 1 then
 begin
  srcstm.Modificator:= xi[1].Prop['text'];
  if srcstm.DScanner.IsReadOnly then
     raise Exception.Create(msELeftCantAssigned);
 end;

 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_If(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
 i: integer;
 s: string;
 srcstm: TmscrCodeStm_If;
begin
 srcstm:= TmscrCodeStm_If.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(srcstm);
 srcstm.Condition:= Compile_Expression(xi[0], aProj);

 for i:= 1 to xi.Count - 1 do
 begin
   s:= Lowercase(xi[i].Name);
   if s = 'thensrcstm' then
     Compile_mscodecompound(xi[1], aProj, srcstm)
   else 
   if s = 'elsesrcstm' then
     Compile_mscodecompound(xi[2], aProj, srcstm.Elsesrcstm);
 end;

 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_For(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
  i: integer;
  srcstm: TmscrCodeStm_For;
begin
 srcstm:= TmscrCodeStm_For.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(srcstm);
 ErrorPos(xi[0]);
 srcstm.Variable:= FindVar(aProj, xi[0].Prop['text']);
 if not ((srcstm.Variable is TmscrVarForVariable) and (srcstm.Variable.Typ in [msvtInt, msvtInt64, msvtVariant, msvtFloat])) then
    raise Exception.Create(msEForError);

 srcstm.ValueBegin:= Compile_Expression(xi[1], aProj);
 CheckTypeCompatibility(srcstm.Variable, srcstm.ValueBegin);

 i:= 2;
 if CompareText(xi[2].Name, 'downto') = 0 then
 begin
    srcstm.Down:= True;
    Inc(i);
 end;

 srcstm.ValueEnd:= Compile_Expression(xi[i], aProj);
 CheckTypeCompatibility(srcstm.Variable, srcstm.ValueEnd);
 if i + 1<xi.Count then
    Compile_SrcStm(xi[i + 1], aProj, srcstm);

 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_While(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var xsrcstm: TmscrCodeStm_While;
begin
    xsrcstm:= TmscrCodeStm_While.Create(aProj, FUnitName, PropPos(xi));
    Statement.Add(xsrcstm);
    xsrcstm.Condition:= Compile_Expression(xi[0], aProj);
    if xi.Count > 1 then
    Compile_SrcStm(xi[1], aProj, xsrcstm);

    FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Repeat(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
 i, j: integer;
 xsrcstm: TmscrCodeStm_Repeat;
begin
 xsrcstm:= TmscrCodeStm_Repeat.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(xsrcstm);

 j:= xi.Count - 1;
 if CompareText(xi[j].Name, 'inverse') = 0 then
 begin
  xsrcstm.InverseCondition:= True;
  Dec(j);
 end;
 xsrcstm.Condition:= Compile_Expression(xi[j], aProj);
 Dec(j);

 for i:= 0 to j do
   Compile_SrcStm(xi[i], aProj, xsrcstm);

 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Case(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
 i: integer;
 srcstm: TmscrCodeStm_Case;
//.........................................
 procedure DoCaseSelector(xi: TmscrDATAItem);
  var
    Selector: TmscrCaseSelector;
 begin
   if (CompareText(xi.Name, 'caseselector')<>0) or (xi.Count<>2) then
   Exit;
   Selector:= TmscrCaseSelector.Create(aProj, FUnitName, PropPos(xi));
   srcstm.Add(Selector);

   Selector.SetExpression:= Compile_Set(xi[0], aProj);
   Compile_SrcStm(xi[1], aProj, Selector);
 end;
//.........................................
begin
 srcstm:= TmscrCodeStm_Case.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(srcstm);
 srcstm.Condition:= Compile_Expression(xi[0], aProj);

 for i:= 1 to xi.Count - 1 do
    DoCaseSelector(xi[i]);
 if CompareText(xi[xi.Count - 1].Name, 'caseselector')<>0 then
    Compile_SrcStm(xi[xi.Count - 1], aProj, srcstm.Elsesrcstm);

 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Try(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
 i: integer;
 srcstm: TmscrCodesrcstmry;
begin
 srcstm:= TmscrCodesrcstmry.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(srcstm);

 for i:= 0 to xi.Count - 1 do
  if CompareText(xi[i].Name, 'ExceptSrcStm') = 0 then
   begin
     srcstm.IsExcept:= True;
     Compile_mscodecompound(xi[i], aProj, srcstm.ExceptSrcStm);
   end else 
   if CompareText(xi[i].Name, 'finallysrcstm') = 0 then
     Compile_mscodecompound(xi[i], aProj, srcstm.ExceptSrcStm)
   else
     Compile_SrcStm(xi[i], aProj, srcstm);

 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;


procedure TmscrEngParser.Compile_Continue(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var srcstm: TmscrCodeStm_Continue;
begin
 srcstm:= TmscrCodeStm_Continue.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(srcstm);
 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Exit(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var xsrcstm: TmscrCodeStm_Exit;
begin
  xsrcstm:= TmscrCodeStm_Exit.Create(aProj, FUnitName, PropPos(xi));
  Statement.Add(xsrcstm);
  FScrProject.AddCodeLine(FUnitName,PropPos(xi));
end;

procedure TmscrEngParser.Compile_Break(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
 xsrcstm: TmscrCodeStm_Break;
begin
 xsrcstm:= TmscrCodeStm_Break.Create(aProj, FUnitName, PropPos(xi));
 Statement.Add(xsrcstm);
 FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Return(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var xi1: TmscrDATAItem;
begin
 if xi.Count = 1 then
 begin
  xi1:= TmscrDATAItem.Create;
  xi1.Name:= 'dsgn';
  xi.InsertItem(0, xi1);
  with xi1.Add do
  begin
   Name:= 'node';
   Text:= 'text="Result" pos="' + xi[1].Prop['pos'] + '"';
  end;

  Compile_Assign(xi, aProj, Statement);
 end;

 Compile_Exit(xi, aProj, Statement);
end;

procedure TmscrEngParser.Compile_With(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
  d: TmscrDScanner;
  i, n: integer;
  s: string;
  v: TmscrVarForVariable;
  srcstm: TmscrCodeStm_With;
  //............................
  function CreateUniqueVariable: string;
  var i: integer;
  begin
     i:= 0;
     while (aProj.FindLocal(IntToStr(i))<>nil) or (FWithList.IndexOf(IntToStr(i))<>-1) do
       Inc(i);
     Result:= '_WithList_' + IntToStr(i);
  end;
  //............................
begin
  n:= xi.Count - 1;

  for i:= 0 to n - 1 do
  begin
     d:= DoDScanner(xi[i], aProj);
     if not ((d.Typ = msvtClass) or (d.Typ = msvtVariant)) then
     begin
       d.Free;
       raise Exception.Create(msEClassRequired);
     end;

     s:= CreateUniqueVariable;
     v:= TmscrVarForVariable.Create(s, d.Typ, d.TypeName);
    aProj.Add(s, v);

     srcstm:= TmscrCodeStm_With.Create(aProj, FUnitName, PropPos(xi));
     srcstm.Variable:= v;
     srcstm.DScanner:= d;
     Statement.Add(srcstm);
   FWithList.AddObject(s, srcstm);
  end;

  Compile_SrcStm(xi[xi.Count - 1], aProj, Statement);

  for i:= 0 to n - 1 do
   FWithList.Delete(FWithList.Count - 1);

  FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_Delete(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
  xsrcstm: TmscrCodeStm_Call;
begin
  xsrcstm:= TmscrCodeStm_Call.Create(aProj, FUnitName, PropPos(xi));
  Statement.Add(xsrcstm);
  xsrcstm.DScanner:= DoDScanner(xi[0], aProj, mseoFree);
  FScrProject.AddCodeLine(FUnitName, PropPos(xi));
end;

procedure TmscrEngParser.Compile_mscodecompound(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
var
  i: integer;
begin
  for i:= 0 to xi.Count - 1 do
     Compile_SrcStm(xi[i], aProj, Statement);
end;

procedure TmscrEngParser.Compile_SrcStm(xi: TmscrDATAItem; aProj: TmscrScript; Statement: TmscrStatement);
 var s: string;
begin
  s:= LowerCase(xi.Name);

  if s = 'codeassign' then begin
     Compile_Assign(xi, aProj, Statement); Exit; End;
  if s = 'mscodecall' then begin
     Compile_Call(xi, aProj, Statement); Exit; End;
  if s = 'mscodeif' then begin
     Compile_If(xi, aProj, Statement); Exit; End;
  if s = 'mscodecases' then begin
     Compile_Case(xi, aProj, Statement); Exit; End;
  if s = 'mscodefor' then begin
     Compile_For(xi, aProj, Statement); Exit; End;
  if s = 'mscodewhile' then begin
     Compile_While(xi, aProj, Statement); Exit; End;
  if s = 'mscoderepeat' then begin
     Compile_Repeat(xi, aProj, Statement); Exit; End;
  if s = 'mscodetry' then begin
     Compile_Try(xi, aProj, Statement); Exit; End;
  if s = 'break' then begin
     Compile_Break(xi, aProj, Statement); Exit; End;
  if s = 'continue' then begin
     Compile_Continue(xi, aProj, Statement); Exit; End;
  if s = 'exit' then begin
     Compile_Exit(xi, aProj, Statement); Exit; End;
  if s = 'return' then begin
     Compile_Return(xi, aProj, Statement); Exit; End;
  if s = 'with' then begin
     Compile_With(xi, aProj, Statement); Exit; End;
  if s = 'delete' then begin
     Compile_Delete(xi, aProj, Statement); Exit; End;
  if s = 'mscodecompound' then begin
     Compile_mscodecompound(xi, aProj, Statement); Exit; End;
  if s = 'uses' then begin
     Compile_Uses(xi, aProj); Exit; End;
  if s = 'var' then begin
     Compile_Var(xi, aProj, Statement); Exit; End;
  if s = 'const' then begin
     Compile_Const(xi, aProj); Exit; End;
  if s = 'procedure' then begin
     Compile_ProcB(xi, aProj); Exit; End;
  if s = 'function' then begin
     Compile_FuncB(xi, aProj); Exit; End;
end;

procedure TmscrEngParser.Compile_Project(xi: TmscrDATAItem; aProj: TmscrScript);
var
  TempRoot: TmscrDATAItem;
  //...........................................
 procedure DoFirstPass(xi: TmscrDATAItem);
  var
     i: integer;
     s: string;
  begin
   For i:= 0 to xi.Count - 1 do
     begin
       s:= LowerCase(xi[i].Name);
       if s = 'mscodecompound' then
          DoFirstPass(xi[i])
       else if s = 'procedure' then
          Compile_ProcA(xi[i], aProj)
       else if s = 'function' then
          Compile_FuncA(xi[i], aProj);
     end;
  end;
  //...........................................
begin
  TempRoot:= FProjRoot;
  FProjRoot:= xi;
  DoFirstPass(xi);
  Compile_mscodecompound(xi, aProj, aProj.Statement);
  FProjRoot:= TempRoot;
end;

//====================================================

{$IFDEF MAGSCR_USEOLE}
constructor TmscrEngOLE.Create(const AName: string);
begin
 inherited Create(AName, msvtVariant, '');
end;

function TmscrEngOLE.DispatchInvoke(const ParamArray: variant; ParamCount: integer; Flags: word): variant;
const
 DispIDArgs: longint = DISPID_PROPERTYPUT;
var
 DispId: TDispId;
 Params: TDISPPARAMS;
 pName: WideString;
 ExcepMess: WideString;
 Args: array[0..63] of variant;
 i: integer;
 PResult: PVariant;
 Status: integer;
 ExcepInfo: TExcepInfo;
begin
 ExcepMess:= '';
 pName:= WideString(Name);
 IDispatch(ParentValue).GetIDsOfNames(GUID_NULL, @pName, 1, GetThreadLocale, @DispId);

 for i:= 0 to ParamCount - 1 do
    Args[i]:= ParamArray[ParamCount - i - 1];

 Params.rgvarg:= @Args;
 Params.rgdispidNamedArgs:= nil;
 Params.cArgs:= ParamCount;
 Params.cNamedArgs:= 0;

 if (Flags = DISPATCH_PROPERTYPUT) or (Flags = DISPATCH_PROPERTYPUTREF) then
 begin
    Params.rgdispidNamedArgs:= @DispIDArgs;
    Params.cNamedArgs:= 1;
 end;

 if NeedResult and (Flags<>DISPATCH_PROPERTYPUT) and (Flags<>DISPATCH_PROPERTYPUTREF) then
    PResult:= @Result else
    PResult:= nil;

 if PResult<>nil then Variants.VarClear(PResult^);

 if (Flags = DISPATCH_METHOD) and (PResult<>nil) then
   Flags:= DISPATCH_METHOD or DISPATCH_PROPERTYGET;

 Status:= IDispatch(ParentValue).Invoke(DispId, GUID_NULL, 0, Flags, Params, PResult, @ExcepInfo, nil);

 if Status<>0 then
 begin
   if ExcepInfo.Source<>'' then
      ExcepMess:= #13 + #10 + 'Source          ::  ' + ExcepInfo.Source;
   if ExcepInfo.Description<>'' then
      ExcepMess:= ExcepMess + #13#10 + 'Description ::  ' + ExcepInfo.Description;
   if ExcepInfo.HelpFile<>'' then
      ExcepMess:= ExcepMess + #13#10 + 'Help File      ::  ' + ExcepInfo.HelpFile;

    raise Exception.Create('OLE error ' + IntToHex(Status, 8) + ': ' + Name + ': ' + SysErrorMessage(Status) + ExcepMess);
 end;
end;

procedure TmscrEngOLE.SetValue(const Value: variant);
var
 i: integer;
 v: variant;
 Flag: word;
begin
 v:= VarArrayCreate([0, Count], varVariant);

 for i:= 0 to Count - 1 do
    v[i]:= Params[i].Value;

 v[Count]:= Value;
 Flag:= DISPATCH_PROPERTYPUT;

 if VarType(Value) = varDispatch then
   Flag:= DISPATCH_PROPERTYPUTREF;

 DispatchInvoke(v, Count + 1, Flag);
 ParentValue:= Null;
end;

function TmscrEngOLE.GetValue: variant;
var
  i: integer;
  v: variant;
begin
  v:= VarArrayCreate([0, Count - 1], varVariant);
  for i:= 0 to Count - 1 do
     v[i]:= Params[i].Value;

  Result:= DispatchInvoke(v, Count, DISPATCH_METHOD);
  ParentValue:= Null;
end;
{$ENDIF}

//====================================================

function FormatV(const Fmt: string; Args: variant): string;
var
  ar: TmscrVarRecArray;
  sPtrList: TList;
begin
  VariantToVarRec(Args, ar, sPtrList);
  try
     Result:= Format(Fmt, ar);
  finally
     ClearVarRec(ar, sPtrList);
  end;
end;

function VArrayCreate(Args: variant; Typ: integer): variant;
var
  i, n: integer;
  ar: array of SizeInt;
begin
  n:= VarArrayHighBound(Args, 1) + 1;
  SetLength(ar, n);
  for i:= 0 to n - 1 do
     ar[i]:= Args[i];

  Result:= VarArrayCreate(ar, Typ);
  ar:= nil;
end;

function NameCase(const s: string): string;
 var i: integer;
begin

  Result:= UTF8LowerString(s);
  for i:= 1 to UTF8Length(s) do
     if i = 1 then
     begin
       UTF8Insert(UTF8UpperString(UTF8Copy(s, i, 1)), Result, i + 1);
       UTF8Delete(Result, i, 1);
     end else
     if i<UTF8Length(s) then
       if UTF8Copy(s, i, 1)[1] = ' ' then
       begin
          UTF8Insert(UTF8UpperString(UTF8Copy(s, i + 1, 1)), Result, i + 2);
          UTF8Delete(Result, i + 1, 1);
       end;
end;

function ValidInt(cInt: string): boolean;
begin
  Result:= True;
  try
     StrToInt(cInt);
  except
     Result:= False;
  end;
end;

function ValidFloat(cFlt: string): boolean;
begin
  Result:= True;
  try
     StrToFloat(cFlt);
  except
     Result:= False;
  end;
end;

function ValidDate(cDate: string): boolean;
begin
  Result:= True;
  try
     StrToDate(cDate);
  except
     Result:= False;
  end;
end;

function DaysInMonth(nYear, nMonth: integer): integer;
const
  Days: array[1..12] of integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result:= Days[nMonth];
  if (nMonth = 2) and IsLeapYear(nYear) then
     Inc(Result);
end;

procedure Write(Msg: variant);
var
  i: integer;
begin
  if VarMSCR_ConcoleOut = nil then Exit;
  i:= VarMSCR_ConcoleOut.Lines.Count - 1;

  VarMSCR_ConcoleOut.Lines[i]:= VarMSCR_ConcoleOut.Lines[i] + Msg;
end;

procedure WriteLN(Msg: variant);
begin
  if VarMSCR_ConcoleOut = nil then Exit;
  VarMSCR_ConcoleOut.Lines.Add(Msg);
end;

procedure Print(Msg: variant);
begin
  Write(Msg);
end;

procedure PrintLN(Msg: variant);
begin
  WriteLN(Msg);
end;

//============== TmscrSysFunctions ===============================

constructor TmscrSysFunctions.Create(AScript: TmscrScript);
begin
  inherited Create(AScript);
  FCatStr   := cnCatStr;
  FCatDate  := cnCatDate;
  FCatConv  := cnCatConv;
  FCatFormat:= cnCatFormat;
  FCatMath  := cnCatMath;
  FCatOther := cnCatOther;

  with AScript do
  begin
     Register_Type('Byte',      msvtInt);
     Register_Type('Word',      msvtInt);
     Register_Type('Integer',   msvtInt);
     Register_Type('Int64',     msvtInt64);
     Register_Type('Longint',   msvtInt);
     Register_Type('Cardinal',  msvtInt);
     Register_Type('TColor',    msvtInt);
     Register_Type('TAlphaColor', msvtInt);
     Register_Type('Boolean',   msvtBool);
     Register_Type('Real',      msvtFloat);
     Register_Type('Single',    msvtFloat);
     Register_Type('Double',    msvtFloat);
     Register_Type('Float',     msvtFloat);
     Register_Type('Extended',  msvtFloat);
     Register_Type('Currency',  msvtFloat);
     Register_Type('TDate',     msvtFloat);
     Register_Type('TTime',     msvtFloat);
     Register_Type('TDateTime', msvtFloat);
     Register_Type('Char',      msvtChar);
     Register_Type('String',    msvtString);
     Register_Type('Variant',   msvtVariant);
     Register_Type('Pointer',   msvtVariant);
     Register_Type('Array',      msvtArray);
     Register_Type('Constructor', msvtConstructor);

     Register_Const('True',  'Boolean', True);
     Register_Const('False', 'Boolean', False);
     Register_Const('nil',   'Variant', 0);
     Register_Const('Null',  'Variant', Null);

     //--- From fpcsrc/rtl/inc/varianth.inc ----
     Register_Const('varEmpty',    'Integer', 0);
     Register_Const('varNull',     'Integer', 1);
     Register_Const('varSmallint', 'Integer', 2);
     Register_Const('varInteger',  'Integer', 3);
     Register_Const('varSingle',   'Integer', 4);
     Register_Const('varDouble',   'Integer', 5);
     Register_Const('varDate',     'Integer', 7);
     Register_Const('varCurrency', 'Integer', 6);
     Register_Const('varOleStr',   'Integer', 8);
     Register_Const('varDispatch', 'Integer', 9);
     Register_Const('varError',    'Integer', 10);
     Register_Const('varBoolean',  'Integer', 11);
     Register_Const('varVariant',  'Integer', 12);
     Register_Const('varUnknown',  'Integer', 13);
     Register_Const('varDecimal',  'Integer', 14);
     Register_Const('varShortInt', 'Integer', 16);
     Register_Const('varByte',     'Integer', 17);
     Register_Const('varWord',     'Integer', 18);
     Register_Const('varLongWord', 'Integer', 19);
     Register_Const('varInt64',    'Integer', 20);
     Register_Const('varQword',    'Integer', 21);
     Register_Const('varRecord',   'Integer', 36);
     Register_Const('varStrArg',   'Integer', $48);
     Register_Const('varUStrArg',  'Integer', $49);
     Register_Const('varString',   'Integer', $100);
     Register_Const('varAny',      'Integer', $101);
     Register_Const('varUString',  'Integer', $102);
     Register_Const('varTypeMask', 'Integer', $fff);
     Register_Const('varArray',    'Integer', $2000);
     Register_Const('varByRef',    'Integer', $4000);

     Add('__HelperForString', TmscrEngString.Create);
     Add('__HelperForArray', TmscrEngArray.Create('__HelperForArray', -1, msvtVariant, ''));
     Register_Variable('ExceptionClassName', 'String', '');
     Register_Variable('ExceptionMessage', 'String', '');
                                         
     //.... Conversion ....
     Register_Method('function IntToStr(i: Integer): String', CallMethod1, FCatConv);
     Register_Method('function FloatToStr(e: Extended): String', CallMethod1, FCatConv);
     Register_Method('function DateToStr(e: Extended): String', CallMethod1, FCatConv);
     Register_Method('function TimeToStr(e: Extended): String', CallMethod1, FCatConv);
     Register_Method('function DateTimeToStr(e: Extended): String', CallMethod1, FCatConv);
     Register_Method('function BoolToStr(B: Boolean): string;', CallMethod1, FCatConv);
     Register_Method('function VarToStr(v: Variant): String', CallMethod7, FCatConv);
     Register_Method('function StrToInt(s: String): Integer', CallMethod2, FCatConv);
     Register_Method('function StrToInt64(s: String): Int64', CallMethod2, FCatConv);
     Register_Method('function StrToFloat(s: String): Extended', CallMethod2, FCatConv);
     Register_Method('function StrToDate(s: String): Extended', CallMethod2, FCatConv);
     Register_Method('function StrToTime(s: String): Extended', CallMethod2, FCatConv);
     Register_Method('function StrToDateTime(s: String): Extended', CallMethod2, FCatConv);
     Register_Method('function StrToBool(const S: string): Boolean;', CallMethod2, FCatConv);
                                   
     //.... Format ....
     Register_Method('function Format(Fmt: String; Args: array): String', CallMethod3, FCatFormat);
     Register_Method('function FormatFloat(Fmt: String; Value: Extended): String', CallMethod3, FCatFormat);
     Register_Method('function FormatDateTime(Fmt: String; DateTime: TDateTime): String', CallMethod3, FCatFormat);
     Register_Method('function FormatMaskText(EditMask: string; Value: string): string', CallMethod3, FCatFormat);

     //.... Date/Time ....
     Register_Method('function EncodeDate(Year, Month, Day: Word): TDateTime', CallMethod4, FCatDate);
     Register_Method('procedure DecodeDate(Date: TDateTime; var Year, Month, Day: Word)', CallMethod4, FCatDate);
     Register_Method('function EncodeTime(Hour, Min, Sec, MSec: Word): TDateTime', CallMethod4, FCatDate);
     Register_Method('procedure DecodeTime(Time: TDateTime; var Hour, Min, Sec, MSec: Word)', CallMethod4, FCatDate);
     Register_Method('function Date: TDateTime', CallMethod4, FCatDate);
     Register_Method('function Time: TDateTime', CallMethod4, FCatDate);
     Register_Method('function Now: TDateTime', CallMethod4, FCatDate);
     Register_Method('function DayOfWeek(aDate: TDateTime): Integer', CallMethod4, FCatDate);
     Register_Method('function IsLeapYear(Year: Word): Boolean', CallMethod4, FCatDate);
     Register_Method('function DaysInMonth(nYear, nMonth: Integer): Integer', CallMethod4, FCatDate);
             
     //.... String ....
     Register_Method('function Length(s: Variant): Integer', CallMethod5, FCatStr);
     Register_Method('function Copy(s: String; from, count: Integer): String', CallMethod5, FCatStr);
     Register_Method('function Pos(substr, s: String): Integer', CallMethod5, FCatStr);
     Register_Method('procedure Delete(var s: String; from, count: Integer)', CallMethod5, FCatStr);
     Register_Method('procedure DeleteStr(var s: String; from, count: Integer)', CallMethod5, FCatStr);
     Register_Method('procedure Insert(s: String; var s2: String; pos: Integer)', CallMethod5, FCatStr);
     Register_Method('function Uppercase(s: String): String', CallMethod5, FCatStr);
     Register_Method('function Lowercase(s: String): String', CallMethod5, FCatStr);
     Register_Method('function Trim(s: String): String', CallMethod5, FCatStr);
     Register_Method('function NameCase(s: String): String', CallMethod5, FCatStr);
     Register_Method('function CompareText(s, s1: String): Integer', CallMethod5, FCatStr);
     Register_Method('function Chr(i: Integer): Char', CallMethod5, FCatStr);
     Register_Method('function Ord(ch: Char): Integer', CallMethod5, FCatStr);
     Register_Method('procedure SetLength(var S: Variant; L: Integer)', CallMethod5, FCatStr);
                     
     //.... Math ....
     Register_Method('function Round(e: Extended): Integer', CallMethod6, FCatMath);
     Register_Method('function Trunc(e: Extended): Integer', CallMethod6, FCatMath);
     Register_Method('function Int(e: Extended): Integer', CallMethod6, FCatMath);
     Register_Method('function Frac(X: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Sqrt(e: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Abs(e: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Sin(e: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Cos(e: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function ArcTan(X: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Tan(X: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Exp(X: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Ln(X: Extended): Extended', CallMethod6, FCatMath);
     Register_Method('function Pi: Extended', CallMethod6, FCatMath);

     Register_Method('procedure Inc(var i: Integer; incr: Integer = 1)', CallMethod7, FCatOther);
     Register_Method('procedure Dec(var i: Integer; decr: Integer = 1)', CallMethod7, FCatOther);
     Register_Method('procedure RaiseException(Param: String)', CallMethod7, FCatOther);
     Register_Method('procedure ShowMessage(Msg: Variant)', CallMethod7, FCatOther);
     
     //.... Other ....
     Register_Method('procedure Print(Msg: Variant)', CallMethod7, FCatOther);
     Register_Method('procedure PrintLN(Msg: Variant)', CallMethod7, FCatOther);
     Register_Method('procedure Write(Msg: Variant)', CallMethod7, FCatOther);
     Register_Method('procedure WriteLN(Msg: Variant)', CallMethod7, FCatOther);

     Register_Method('procedure Randomize', CallMethod7, FCatOther);
     Register_Method('function Random: Extended', CallMethod7, FCatOther); 
     Register_Method('function RandomInt(l:Int64):Int64;', CallMethod7, FCatOther);
     Register_Method('function ValidInt(cInt: String): Boolean', CallMethod7, FCatOther);
     Register_Method('function ValidFloat(cFlt: String): Boolean', CallMethod7, FCatOther);
     Register_Method('function ValidDate(cDate: String): Boolean', CallMethod7, FCatOther);
     Register_Method('function ExtractFilePath(const FileName: string): string;', CallMethod7, FCatOther);
{$IFDEF MAGSCR_USEOLE}
     Register_Method('function CreateOleObject(ClassName: String): Variant', CallMethod7, FCatOther);
     Register_Method('function GetActiveOleObject(const ClassName: string) : Variant', CallMethod7, FCatOther);
{$ENDIF}
     Register_Method('function VarArrayCreate(Bounds: Array; Typ: Integer): Variant', CallMethod7, FCatOther);
     Register_Method('function VarType(V: Variant): Integer', CallMethod7, FCatOther);

  end;
end;

function TmscrSysFunctions.CallMethod1(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  i: int64;
begin
  if aMethodName = 'INTTOSTR' then
  begin
     i:= aCallVar.Params[0];
     Result:= IntToStr(i);
  exit; end;

  if aMethodName = 'FLOATTOSTR' then begin
     Result:= FloatToStr(aCallVar.Params[0]); exit; end;
  if aMethodName = 'DATETOSTR' then begin
     Result:= DateToStr(aCallVar.Params[0]); exit; end;
  if aMethodName = 'TIMETOSTR' then begin
     Result:= TimeToStr(aCallVar.Params[0]); exit; end;
  if aMethodName = 'DATETIMETOSTR' then begin
     Result:= DateTimeToStr(aCallVar.Params[0]); exit; end;
  if aMethodName = 'BOOLTOSTR' then begin
     Result:= BoolToStr(aCallVar.Params[0]); exit; end;
end;

function TmscrSysFunctions.CallMethod2(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  if aMethodName = 'STRTOINT' then begin
     Result:= StrToInt(aCallVar.Params[0]); exit; end;
  if aMethodName = 'STRTOINT64' then begin
     Result:= StrToInt64(aCallVar.Params[0]); exit; end;
  if aMethodName = 'STRTOFLOAT' then begin
     Result:= StrToFloat(aCallVar.Params[0]); exit; end;
  if aMethodName = 'STRTODATE' then begin
     Result:= StrToDate(aCallVar.Params[0]); exit; end;
  if aMethodName = 'STRTOTIME' then begin
     Result:= StrToTime(aCallVar.Params[0]); exit; end;
  if aMethodName = 'STRTODATETIME' then begin
     Result:= StrToDateTime(aCallVar.Params[0]); exit; end;
  if aMethodName = 'STRTOBOOL' then begin
     Result:= BoolToStr(aCallVar.Params[0]); exit; end;
end;

function TmscrSysFunctions.CallMethod3(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  if aMethodName = 'FORMAT' then begin
     Result:= FormatV(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
  if aMethodName = 'FORMATFLOAT' then begin
     Result:= FormatFloat(aCallVar.Params[0], aCallVar.Params[1] * 1.00000); exit; end;
  if aMethodName = 'FORMATDATETIME' then begin
     Result:= FormatDateTime(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
end;

function TmscrSysFunctions.CallMethod4(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  w1, w2, w3, w4: word;
begin
  if aMethodName = 'ENCODEDATE' then begin
     Result:= EncodeDate(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2]); exit; end;
  if aMethodName = 'ENCODETIME' then begin
     Result:= EncodeTime(aCallVar.Params[0], aCallVar.Params[1], aCallVar.Params[2], aCallVar.Params[3]); exit; end;

  if aMethodName = 'DECODEDATE' then
  begin
     DecodeDate(aCallVar.Params[0], w1, w2, w3);
     aCallVar.Params[1]:= w1;
     aCallVar.Params[2]:= w2;
     aCallVar.Params[3]:= w3;
  exit; end;

  if aMethodName = 'DECODETIME' then
  begin
     DecodeTime(aCallVar.Params[0], w1, w2, w3, w4);
     aCallVar.Params[1]:= w1;
     aCallVar.Params[2]:= w2;
     aCallVar.Params[3]:= w3;
     aCallVar.Params[4]:= w4;
  exit; end;

  if aMethodName = 'DATE' then begin
     Result:= SysUtils.Date; exit; end;
  if aMethodName = 'TIME' then begin
     Result:= SysUtils.Time; exit; end;
  if aMethodName = 'NOW' then begin
     Result:= SysUtils.Now; exit; end;
  if aMethodName = 'DAYOFWEEK' then begin
     Result:= SysUtils.DayOfWeek(aCallVar.Params[0]); exit; end;
  if aMethodName = 'ISLEAPYEAR' then begin
     Result:= SysUtils.IsLeapYear(aCallVar.Params[0]); exit; end;
  if aMethodName = 'DAYSINMONTH' then begin
     Result:= DaysInMonth(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
end;

function TmscrSysFunctions.CallMethod5(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
var
  s: string;
  v: variant;
begin
  if aMethodName = 'LENGTH' then begin
     v:= aCallVar.Params[0];
     if VarIsArray(v) then
       Result:= VarArrayHighBound(v, 1) - VarArrayLowBound(v, 1) + 1 else
       Result:= UTF8Length(v);
  exit; end;
  if aMethodName = 'COPY' then begin
     Result:= UTF8Copy(aCallVar.Params[0], fmscrInteger(aCallVar.Params[1]), fmscrInteger(aCallVar.Params[2])); exit; end;
  if aMethodName = 'POS' then begin
     Result:= UTF8Pos(ansistring(aCallVar.Params[0]), ansistring(aCallVar.Params[1])); exit; end;
  if (aMethodName = 'DELETE') or (aMethodName = 'DELETESTR') then begin
     s:= aCallVar.Params[0];
     UTF8Delete(s, fmscrInteger(aCallVar.Params[1]), fmscrInteger(aCallVar.Params[2]));
     aCallVar.Params[0]:= s;
  exit; end;
  if aMethodName = 'INSERT' then
  begin
     s:= aCallVar.Params[1];
     UTF8Insert(ansistring(aCallVar.Params[0]), s, integer(aCallVar.Params[2]));
     aCallVar.Params[1]:= s;
  exit; end;
  if aMethodName = 'UPPERCASE' then begin
     Result:= UTF8UpperString(aCallVar.Params[0]); exit; end;
  if aMethodName = 'LOWERCASE' then begin
     Result:= UTF8LowerString(aCallVar.Params[0]); exit; end;
  if aMethodName = 'TRIM' then begin
     Result:= UTF8Trim(aCallVar.Params[0]); exit; end;
  if aMethodName = 'NAMECASE' then begin
     Result:= NameCase(aCallVar.Params[0]); exit; end;
  if aMethodName = 'COMPARETEXT' then begin
     Result:= UTF8CompareText(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
  if aMethodName = 'CHR' then begin
     Result:= Chr(integer(aCallVar.Params[0])); exit; end;
  if aMethodName = 'ORD' then begin
     Result:= Ord(string(aCallVar.Params[0])[1]); exit; end;
  if aMethodName = 'SETLENGTH' then
  begin
     if (TVarData(aCallVar.Params[0]).VType = varString) or (TVarData(aCallVar.Params[0]).VType = varOleStr) then
     begin
       s:= aCallVar.Params[0];
       SetLength(s, integer(aCallVar.Params[1]));
       aCallVar.Params[0]:= s;
     end else
     begin
       v:= aCallVar.Params[0];
       VarArrayRedim(v, integer(aCallVar.Params[1]) - 1);
       aCallVar.Params[0]:= v;
     end;
  exit; end;
end;

function TmscrSysFunctions.CallMethod6(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
 if aMethodName = 'ROUND' then begin
     Result:= fmscrInteger(Round(extended(aCallVar.Params[0]) * 1.00000)); exit; end;
  if aMethodName = 'TRUNC' then begin
     Result:= fmscrInteger(Trunc(extended(aCallVar.Params[0]) * 1.00000)); exit; end;
  if aMethodName = 'INT' then begin
     Result:= Int(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'FRAC' then begin
     Result:= Frac(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'SQRT' then begin
     Result:= Sqrt(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'ABS' then begin
     Result:= Abs(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'SIN' then begin
     Result:= Sin(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'COS' then begin
     Result:= Cos(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'ARCTAN' then begin
     Result:= ArcTan(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'TAN' then begin
     Result:= Sin(aCallVar.Params[0] * 1.00000) / Cos(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'EXP' then begin
     Result:= Exp(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'LN' then begin
     Result:= Ln(aCallVar.Params[0] * 1.00000); exit; end;
  if aMethodName = 'PI' then begin
     Result:= Pi; exit; end;
end;

function TmscrSysFunctions.CallMethod7(aInstance: TObject; aClassType: TClass; const aMethodName: string; aCallVar: TmscrVarForMethod): variant;
begin
  if aMethodName = 'INC' then begin
     aCallVar.Params[0]:= aCallVar.Params[0] + aCallVar.Params[1]; exit; end;
  if aMethodName = 'DEC' then begin
     aCallVar.Params[0]:= aCallVar.Params[0] - aCallVar.Params[1]; exit; end;
  if aMethodName = 'RAISEEXCEPTION' then begin
     raise Exception.Create(aCallVar.Params[0]); exit; end;
{$IFNDEF MAGSCR_NOFORMS}
  if aMethodName = 'SHOWMESSAGE' then begin
     ShowMessage(aCallVar.Params[0]); exit; end;
{$ENDIF}
  if aMethodName = 'PRINT' then begin
     Print(aCallVar.Params[0]); exit; end;
  if aMethodName = 'PRINTLN' then begin
     PrintLN(aCallVar.Params[0]); exit; end;
  if aMethodName = 'WRITE' then begin
     Write(aCallVar.Params[0]); exit; end;
  if aMethodName = 'WRITELN' then begin
     WriteLN(aCallVar.Params[0]); exit; end;
  if aMethodName = 'RANDOMIZE' then begin
     Randomize; exit; end;
  if aMethodName = 'RANDOM' then begin
     Result:= Random; exit; end; 
  if aMethodName = 'RANDOMINT' then begin
     Result:= Random(aCallVar.Params[0]); exit; end;
  if aMethodName = 'VALIDINT' then begin
     Result:= ValidInt(aCallVar.Params[0]); exit; end;
  if aMethodName = 'VALIDFLOAT' then begin
     Result:= ValidFloat(aCallVar.Params[0]); exit; end;
  if aMethodName = 'VALIDDATE' then
  begin
     if VarType(aCallVar.Params[0]) = varDate then
        Result:= ValidDate(DateToStr(aCallVar.Params[0])) else
        Result:= ValidDate(aCallVar.Params[0]);
  exit; end;
{$IFDEF MAGSCR_USEOLE}
  if aMethodName = 'CREATEOLEOBJECT' then begin
     Result:= CreateOleObject(aCallVar.Params[0]); exit; end;
  if aMethodName = 'GETACTIVEOLEOBJECT' then begin
     Result:= GetActiveOleObject(aCallVar.Params[0]); exit; end;
{$ENDIF}
  if aMethodName = 'VARARRAYCREATE' then begin
     Result:= VArrayCreate(aCallVar.Params[0], aCallVar.Params[1]); exit; end;
  if aMethodName = 'VARTOSTR' then begin
     Result:= VarToStr(aCallVar.Params[0]); exit; end;
  if aMethodName = 'VARTYPE' then begin
     Result:= VarType(aCallVar.Params[0]); exit; end;
  if aMethodName = 'EXTRACTFILEPATH' then begin
     Result:= ExtractFilePath(aCallVar.Params[0]); exit; end;
end;


//===================================================================
//===================================================================
//===================================================================

initialization
  FGlobalScripterIsNIL:= False;
  FRTTILibrariesIsNIL:= False;
  MSCR_RTTILibraries;

finalization
  VarMSCR_ConcoleOut:= nil;
  if FGlobalScripter<>nil then FGlobalScripter.Free;
  FGlobalScripter:= nil;
  FGlobalScripterIsNIL:= True;
  FRTTILibraries.Free;
  FRTTILibraries:= nil;
  FRTTILibrariesIsNIL:= True;

end.

