{***************************************************
 Magic Script
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_MagicScript
 This file is part of CodeTyphon Studio
****************************************************}

unit msLibrariesTree;

interface

{$i magicscript.inc}

uses
  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ComCtrls, SynEdit,
  msbaseobjects, mscoreengine;

type

TmscrRTTILibrariesTree = class(TPanel)
 private
     FDATAList: TmscrDATADocument;
     FScript: TmscrScript;
     FTreeView: TTreeView;
     FImages: TImageList;
     FShowFunctions: boolean;
     FShowClasses: boolean;    
     FShowRecords: boolean;
     FShowTypes: boolean;
     FShowVariables: boolean;
     FExpanded: boolean;
     FExpandLevel: integer;
     FScriptEditor: TSynEdit;
     FUpdating: boolean;
     procedure SetScriptEditor(Value: TSynEdit);
     procedure SetImageIndex(Node: TTreeNode; Index: integer);
     procedure SetScript(const Value: TmscrScript);
 protected
     procedure Notification(AComponent: TComponent; Operation: TOperation); override;
     function  GetCategoryByName(s: string): string;

     procedure FindAllClasses(xi: TmscrDATAItem; Root: TTreeNode);
     procedure FindAllRecords(xi: TmscrDATAItem; Root: TTreeNode);
     procedure FindAllFuncProc(xi: TmscrDATAItem; Root: TTreeNode);
     procedure FindAllTypes(xi: TmscrDATAItem; Root: TTreeNode);
     procedure FindAllVariables(xi: TmscrDATAItem; Root: TTreeNode);

     procedure TreeViewBuild;   
     procedure TreeViewExpandNodes(level: integer);
     procedure TreeChange(Sender: TObject; Node: TTreeNode);

     procedure DoOnTreeViewDblClick(Sender: TObject);
     procedure DoOnTreeViewCollapsed(Sender: TObject; Node: TTreeNode);
     procedure DoOnTreeViewExpanded(Sender: TObject; Node: TTreeNode);

 public
     constructor Create(AOwner: TComponent); override;
     destructor Destroy; override;
     procedure UpdateItems;
     function  GetFieldName: string;

 published
     property Script: TmscrScript read FScript write SetScript;
     property ScriptEditor: TSynEdit read FScriptEditor write SetScriptEditor;

     property ShowClasses: boolean read FShowClasses write FShowClasses; 
     property ShowRecords: boolean read FShowRecords write FShowRecords;
     property ShowFunctions: boolean read FShowFunctions write FShowFunctions;
     property ShowTypes: boolean read FShowTypes write FShowTypes;
     property ShowVariables: boolean read FShowVariables write FShowVariables;
     property Expanded: boolean read FExpanded write FExpanded;
     property ExpandLevel: integer read FExpandLevel write FExpandLevel;

     property Align;
     property Anchors;
     property DragCursor;
     property DragKind;
     property DragMode;
     property BiDiMode;
     property ParentBiDiMode;
     property Color;
     property Constraints;
     property Enabled;
     property Font;
     property ParentColor;
     property ParentFont;
     property ParentShowHint;
     property PopupMenu;
     property ShowHint;
     property TabOrder;
     property TabStop;
     property Visible;
     property OnClick;
     property OnDblClick;
     property OnDragDrop;
     property OnDragOver;
     property OnEndDrag;
     property OnEnter;
     property OnExit;
     property OnKeyDown;
     property OnMouseDown;
     property OnMouseMove;
     property OnMouseUp;
     property OnStartDrag;
 end;

implementation

 {$R *.res}

Const      
  cnImgBook           =0;
  cnImgBookOpen       =1;
  cnImgBookGray       =2;
  cnImgGlobalfunction =3;
  cnImgProcedure      =4;
  cnImgFunction       =5;
  cnImgEvent          =6;
  cnImgElement        =7;
  cnImgVariable       =8; 
  cnImgType           =9; 
  cnImgProperty       =10;  
  cnImgConstructor    =11;

constructor TmscrRTTILibrariesTree.Create(AOwner: TComponent);
begin
  inherited;
  Parent :=AOwner as TWinControl;
  BevelOuter :=bvNone;
  //...........
  FTreeView :=TTreeView.Create(Self);
  FTreeView.Parent :=Self;
  FTreeView.Align :=alClient;
  FTreeView.DragMode :=dmAutomatic;
  FTreeView.ReadOnly :=True;
  FTreeView.TabOrder :=0;

  //...........
  FImages :=TImageList.Create(Self);
  FImages.Masked :=True;                              
  FImages.AddResourceName(hInstance,'mcrpic_book');   
  FImages.AddResourceName(hInstance,'mcrpic_bookopen');  
  FImages.AddResourceName(hInstance,'mcrpic_bookgray');
  FImages.AddResourceName(hInstance,'mcrpic_globalfunction');
  FImages.AddResourceName(hInstance,'mcrpic_procedure');  
  FImages.AddResourceName(hInstance,'mcrpic_function');  
  FImages.AddResourceName(hInstance,'mcrpic_event');
  FImages.AddResourceName(hInstance,'mcrpic_element');
  FImages.AddResourceName(hInstance,'mcrpic_variable');  
  FImages.AddResourceName(hInstance,'mcrpic_type');
  FImages.AddResourceName(hInstance,'mcrpic_property'); 
  FImages.AddResourceName(hInstance,'mcrpic_constructor');

  FTreeView.Images :=FImages;
  FTreeView.OnCollapsed :=DoOnTreeViewCollapsed;
  FTreeView.OnExpanded  :=DoOnTreeViewExpanded;
  FTreeView.OnDblClick  :=DoOnTreeViewDblClick;

  FDATAList :=TmscrDATADocument.Create;
  Expanded :=True;
  ExpandLevel :=2;

  ShowClasses :=True;
  ShowRecords :=True;
  ShowFunctions :=True;
  ShowTypes :=True;
  ShowVariables :=True;

  UpdateItems;
  Height :=250;
  Width  :=200;
end;

destructor TmscrRTTILibrariesTree.Destroy;
begin
  FImages.Free;
  FTreeView.Free;
  FUpdating :=True;
  FDATAList.Free;
  inherited;
end;

procedure TmscrRTTILibrariesTree.SetImageIndex(Node: TTreeNode; Index: integer);
begin
  Node.ImageIndex :=Index;
  Node.StateIndex :=Index;
  Node.SelectedIndex :=Index;
end;

procedure TmscrRTTILibrariesTree.SetScriptEditor(Value: TSynEdit);
begin
  FScriptEditor :=Value;
end;

procedure TmscrRTTILibrariesTree.SetScript(const Value: TmscrScript);
begin
  FScript :=Value;
  UpdateItems;
end;

procedure TmscrRTTILibrariesTree.UpdateItems;
begin
  TreeViewBuild;
end;

procedure TmscrRTTILibrariesTree.TreeChange(Sender: TObject; Node: TTreeNode);
begin
  if FUpdating then
    Exit;
end;

procedure TmscrRTTILibrariesTree.DoOnTreeViewDblClick(Sender: TObject);
begin
  if Assigned(ScriptEditor) then
    if FTreeView.Selected.Count = 0 then
       ScriptEditor.SelText :=FTreeView.Selected.Text;

  if Assigned(OnDblClick) then OnDblClick(Self);
end;

procedure TmscrRTTILibrariesTree.DoOnTreeViewCollapsed(Sender: TObject; Node: TTreeNode);
begin
  SetImageIndex(Node, cnImgBook);
end;

procedure TmscrRTTILibrariesTree.DoOnTreeViewExpanded(Sender: TObject; Node: TTreeNode);
begin
  SetImageIndex(Node, cnImgBookOpen);
end;

procedure TmscrRTTILibrariesTree.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation = opRemove then
    if AComponent = FScript then
      FScript :=nil
    else if AComponent = FScriptEditor then
      FScriptEditor :=nil;
end;

function TmscrRTTILibrariesTree.GetFieldName: string;
 var
 i, n: integer;
 s: string;
begin
 Result :='';

 if (FTreeView.Selected <> nil) and (FTreeView.Selected.Count = 0) then
 begin
   s :=FTreeView.Selected.Text;

   if Pos('(', s) <> 0 then
     n :=1 else
     n :=0;

   for i :=1 to Length(s) do
     if s[i] in [',', ';'] then
       Inc(n);

   if n = 0 then
   begin
     s :=Copy(s, 1, Pos(':', s) - 1)
   end else
   begin
     s :=Copy(s, 1, Pos('(', s));
     
     for i :=1 to n - 1 do
       s :=s + ',';
       
     s :=s + ')';
   end;

  Result :=s;
 end;
end;

//=========================================================== 
//===========================================================
//===========================================================

function TmscrRTTILibrariesTree.GetCategoryByName(s: string): string;
begin
    Result:=s;

    if SameText(s,'catConv')   then  begin Result :='Conversion'; exit; end;
    if SameText(s,'catFormat') then  begin Result :='Formatting'; exit; end;
    if SameText(s,'catDate')   then  begin Result :='Date/Time'; exit; end;
    if SameText(s,'catString') then  begin Result :='String Routines'; exit; end;
    if SameText(s,'catMath')   then  begin Result :='Math'; exit; end;
    if SameText(s,'catOther')  then  begin Result :='Other'; exit; end;

end;

procedure TmscrRTTILibrariesTree.FindAllClasses(xi: TmscrDATAItem; Root: TTreeNode);
var
 i: integer;
 xNode: TTreeNode;
 s: string;
begin
    s :=xi.Prop['text'];
    xNode :=FTreeView.Items.AddChild(Root, s);

    if xi.Count = 0 then
      xNode.Data :=xi;

    if Root = nil then
      xNode.Text :=xi.Name+' (Total: '+IntToStr(xi.Count)+')';

    Case xNode.Level of
     0,1:begin
          if xi.Count = 0 then
           SetImageIndex(xNode, cnImgBookGray) else
           SetImageIndex(xNode, cnImgBook);
         end;
     2:  begin
            if pos('Constructor',xNode.Text)>0 then
               SetImageIndex(xNode, cnImgConstructor) else
            if pos('procedure',xNode.Text)>0 then
               SetImageIndex(xNode, cnImgProcedure) else
            if pos('function',xNode.Text)>0 then
               SetImageIndex(xNode, cnImgFunction) else
            if pos('event',xNode.Text)>0 then
               SetImageIndex(xNode, cnImgEvent)
            else
               SetImageIndex(xNode, cnImgProperty);
         end;
    end;

    for i :=0 to xi.Count - 1 do
      FindAllClasses(xi[i], xNode);
end;

procedure TmscrRTTILibrariesTree.FindAllRecords(xi: TmscrDATAItem; Root: TTreeNode);
var
 i: integer;
 xNode: TTreeNode;
 s: string;
begin
    s :=xi.Prop['text'];
    xNode :=FTreeView.Items.AddChild(Root, s);

    if xi.Count = 0 then
      xNode.Data :=xi;

    if Root = nil then
      xNode.Text :=xi.Name+' (Total: '+IntToStr(xi.Count)+')';

    Case xNode.Level of
     0,1:begin
          if xi.Count = 0 then
           SetImageIndex(xNode, cnImgBookGray) else
           SetImageIndex(xNode, cnImgBook);
         end;
     2:  begin
          SetImageIndex(xNode, cnImgElement);
         end;
    end;

    for i :=0 to xi.Count - 1 do
      FindAllRecords(xi[i], xNode);
end;

procedure TmscrRTTILibrariesTree.FindAllFuncProc(xi: TmscrDATAItem; Root: TTreeNode);
var
 i: integer;
 xNode: TTreeNode;
 s: string;
begin
    s :=xi.Prop['text'];
    if xi.Count = 0 then
      s :=Copy(s, Pos(' ', s) + 1, 255) else
      s :=GetCategoryByName(s);

    xNode :=FTreeView.Items.AddChild(Root, s);

    if xi.Count = 0 then
      xNode.Data :=xi;

    if Root = nil then
      xNode.Text :=xi.Name+' (Total: '+IntToStr(xi.Count)+')';

    Case xNode.Level of
     0,1:begin
          if xi.Count = 0 then
           SetImageIndex(xNode, cnImgBookGray) else
           SetImageIndex(xNode, cnImgBook);
         end;
     2:  begin
            if (pos('):',xNode.Text)>0) or
               ((pos('(',xNode.Text)<1) and (pos(':',xNode.Text)>0)) then
               SetImageIndex(xNode, cnImgFunction) else
               SetImageIndex(xNode, cnImgProcedure);
         end;
    end;

    for i :=0 to xi.Count - 1 do
      FindAllFuncProc(xi[i], xNode);
end;

procedure TmscrRTTILibrariesTree.FindAllTypes(xi: TmscrDATAItem; Root: TTreeNode);
var
 i: integer;
 xNode: TTreeNode;
 s: string;
begin
    s :=Copy(xi.Prop['text'], 1, 255);
    xNode :=FTreeView.Items.AddChild(Root, s);

    if xi.Count = 0 then
      xNode.Data :=xi;

    if Root = nil then
      xNode.Text :=xi.Name+' (Total: '+IntToStr(xi.Count)+')';

     Case xNode.Level of
     0:begin
          if xi.Count = 0 then
           SetImageIndex(xNode, cnImgBookGray) else
           SetImageIndex(xNode, cnImgBook);
         end;
     1:  begin
            SetImageIndex(xNode, cnImgType)
         end;
    end;

    for i :=0 to xi.Count - 1 do
      FindAllTypes(xi[i], xNode);
 end;

procedure TmscrRTTILibrariesTree.FindAllVariables(xi: TmscrDATAItem; Root: TTreeNode);
 var
  i: integer;
  xNode: TTreeNode;
  s: string;
begin
    s :=xi.Prop['text'];
    xNode :=FTreeView.Items.AddChild(Root, s);

    if xi.Count = 0 then
      xNode.Data :=xi;

    if Root = nil then
      xNode.Text :=xi.Name+' (Total: '+IntToStr(xi.Count)+')';

     Case xNode.Level of
     0:begin
          if xi.Count = 0 then
           SetImageIndex(xNode, cnImgBookGray) else
           SetImageIndex(xNode, cnImgBook);
         end;
     1:  begin
            SetImageIndex(xNode, cnImgVariable)
         end;
    end;

    for i :=0 to xi.Count - 1 do
      FindAllVariables(xi[i], xNode);
end;

procedure TmscrRTTILibrariesTree.TreeViewExpandNodes(level: integer);
 var i: integer;
begin
    if level > 1 then
      for i :=0 to FTreeView.Items.Count - 1 do
        if FTreeView.Items[i].Level < Level - 1 then
         FTreeView.Items[i].Expand(False);
end;

procedure TmscrRTTILibrariesTree.TreeViewBuild;
begin
  FUpdating :=True;
  FDATAList.Root.Clear;

  GenerateDATAContents(MSCR_GlobalScripter, FDATAList.Root);

  if FScript <> nil then
    GenerateDATAContents(FScript, FDATAList.Root);

  FTreeView.Items.BeginUpdate;
  FTreeView.Items.Clear;

  if ShowClasses then
    FindAllClasses(FDATAList.Root.FindItem('Classes'), nil);

  if ShowRecords then
    FindAllRecords(FDATAList.Root.FindItem('Records'), nil);

  if ShowFunctions then
    FindAllFuncProc(FDATAList.Root.FindItem('Functions'), nil);

  if ShowTypes then
    FindAllTypes(FDATAList.Root.FindItem('Types'), nil);

  if ShowVariables then
    FindAllVariables(FDATAList.Root.FindItem('Variables'), nil);

  if Expanded then
    TreeViewExpandNodes(ExpandLevel);

  FTreeView.TopItem :=FTreeView.Items[0];
  FTreeView.Items.EndUpdate;
  FUpdating :=False;

end;

end.
