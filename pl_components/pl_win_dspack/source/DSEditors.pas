{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_DSPack
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}

unit DSEditors;

interface

{$I dspack.inc}

uses
 componenteditors, propedits, LResources,
 Forms, Controls, Classes,
 DXSUtil,
 DSPack,
 MediaTypeEditor,
 BaseFilterEditor;

type

  TMediaTypePropertyClass = class(TClassProperty)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;


  TBaseFilterPropertyClass = class(TClassProperty)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

  procedure Register;

implementation

{$R DSEditors.res}

  procedure TMediaTypePropertyClass.Edit;
  var
    Dlg: TFormMediaType;
  begin
    Dlg := TFormMediaType.create(nil); //==== ct9999
    try
      Dlg.MediaType.Assign(TMediaType(GetOrdValue));
      if Dlg.ShowModal = mrOk then
      begin
        TMediaType(GetOrdValue).Assign(Dlg.MediaType);
        if (GetComponent(0) is TSampleGrabber) then
          IFilter(GetComponent(0) as TSampleGrabber).NotifyFilter(foRefresh);
        Modified;
      end;
    finally
      Dlg.Free;
    end;
  end;

  function TMediaTypePropertyClass.GetAttributes: TPropertyAttributes;
  begin
    Result := [paDialog];
  end;

//============================================================

  procedure TBaseFilterPropertyClass.Edit;
  var
    Dlg: TFormBaseFilter;
  begin
    Dlg := TFormBaseFilter.create(nil);  //==== ct9999
    try

      Dlg.Filter.BaseFilter.Assign(TBaseFilter(GetOrdValue));

      if Dlg.ShowModal = mrOk then
      begin
        TBaseFilter(GetOrdValue).Assign(Dlg.Filter.BaseFilter);

        if (GetComponent(0) is TFilter) then
          IFilter(GetComponent(0) as TFilter).NotifyFilter(foRefresh);

        Modified;
      end;
    finally
      Dlg.Free;
    end;
  end;

  function TBaseFilterPropertyClass.GetAttributes: TPropertyAttributes;
  begin
    Result := [paDialog];
  end;

  procedure Register;
  begin
    RegisterComponents('Win_DSPack', [TFilterGraph,
                                      TVideoWindow,
                                      TSampleGrabber,
                                      TFilter,
                                      TASFWriter,
                                      TDSTrackBar,
                                      TDSVideoWindowEx2]);
    
    RegisterPropertyEditor(TypeInfo(TMediaType) , nil, '', TMediaTypePropertyClass);
    RegisterPropertyEditor(TypeInfo(TBaseFilter), nil, '', TBaseFilterPropertyClass);
  end;

end.
