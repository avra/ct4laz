{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_DSPack;

{$warn 5023 off : no warning about unused units}
interface

uses
  BaseClass, BaseFilterEditor, DSEditors, DSPack, DXSUtil, MediaTypeEditor, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('DSEditors', @DSEditors.Register);
end;

initialization
  RegisterPackage('pl_Win_DSPack', @Register);
end.
