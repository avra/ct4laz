unit oglContext;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, ctGL,
  OpenGLPanel; //--- ct9999 -----

type
  TContext = class(TCustomOpenGLPanel)
  public
    constructor Create(TheOwner: TWinControl; AAutoResize: boolean = True; AMajorVersion: integer = 3; AMinorVersion: integer = 3);
  end;

implementation

constructor TContext.Create(TheOwner: TWinControl; AAutoResize: boolean; AMajorVersion: integer; AMinorVersion: integer);
begin
  inherited Create(TheOwner);
  AutoResizeViewport := AAutoResize;
  OpenGLMajorVersion := AMajorVersion;
  OpenGLMinorVersion := AMinorVersion;
  Align := alClient;
  //  MultiSampling:=4;
  Parent := TheOwner;
  MakeCurrent;

 OpenGL_InitializeAdvance;
end;

end.
