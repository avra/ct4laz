{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_opengladv;

{$warn 5023 off : no warning about unused units}
interface

uses
  oglBackground, oglBody, oglCamera, oglColorBody, oglContext, oglControl, 
  oglFontTexture, oglLighting, oglLightingShader, oglLinesVAO, oglMatrix, 
  oglPresentation, oglShader, oglTexture, oglTextureBody, oglTextureVAO, 
  oglUBO, oglUnit, oglVAO, oglVBO, oglWaveFrontOBJ, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_opengladv', @Register);
end.
