{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_DirectX
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}

unit DXErr8;

interface

uses
  Windows;

const
  //////////// DLL export definitions ///////////////////////////////////////
  dxerr8dll = 'dxerr81ab.dll';

function DXGetErrorString8A(hr: HRESULT): PAnsiChar; stdcall; external dxerr8dll;
function DXGetErrorString8W(hr: HRESULT): PWideChar; stdcall; external dxerr8dll;

function DXGetErrorString8(hr: HRESULT): PChar;  stdcall; external dxerr8dll
  name {$IFDEF UNICODE}'DXGetErrorString8W'{$ELSE}'DXGetErrorString8A'{$ENDIF};


function DXTraceA(strFile: PAnsiChar; dwLine: DWORD; hr: HRESULT; strMsg: PAnsiChar; bPopMsgBox: BOOL): HRESULT; stdcall; external dxerr8dll;
function DXTraceW(strFile: PWideChar; dwLine: DWORD; hr: HRESULT; strMsg: PWideChar; bPopMsgBox: BOOL): HRESULT; stdcall; external dxerr8dll;

function DXTrace(strFile: PChar; dwLine: DWORD; hr: HRESULT; strMsg: PChar; bPopMsgBox: BOOL): HRESULT; stdcall; external dxerr8dll
  name {$IFDEF UNICODE}'DXTraceW'{$ELSE}'DXTraceA'{$ENDIF};


implementation

end.
