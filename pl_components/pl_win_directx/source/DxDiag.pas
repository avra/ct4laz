{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_DirectX
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}


{$I DirectX.inc}

unit DxDiag;

interface

uses
  Windows;


(*==========================================================================;
 *
 *  Copyright (C) Microsoft Corporation.  All rights reserved
 *
 *  File:       dxdiag.h
 *  Content:    DirectX Diagnostic Tool include file
 *
 ****************************************************************************)

const
  // This identifier is passed to IDxDiagProvider::Initialize in order to ensure that an
  // application was built against the correct header files. This number is
  // incremented whenever a header (or other) change would require applications
  // to be rebuilt. If the version doesn't match, IDxDiagProvider::Initialize will fail.
  // (The number itself has no meaning.)
  DXDIAG_DX9_SDK_VERSION = 111;


  (****************************************************************************
   *
   * DxDiag Errors
   *
   ****************************************************************************)
  DXDIAG_E_INSUFFICIENT_BUFFER       = HResult($8007007A);  // HRESULT_FROM_WIN32(ERROR_INSUFFICIENT_BUFFER)


  (****************************************************************************
   *
   * DxDiag CLSIDs
   *
   ****************************************************************************)

  // {A65B8071-3BFE-4213-9A5B-491DA4461CA7}
  CLSID_DxDiagProvider: TGUID = '{A65B8071-3BFE-4213-9A5B-491DA4461CA7}';


type
  (****************************************************************************
   *
   * DxDiag Structures
   *
   ****************************************************************************)

  PDxDiagInitParams = ^TDxDiagInitParams;
  _DXDIAG_INIT_PARAMS = record
    dwSize: DWORD;                 // Size of this structure.
    dwDxDiagHeaderVersion: DWORD;  // Pass in DXDIAG_DX9_SDK_VERSION.  This verifies
                                   // the header and dll are correctly matched.
    bAllowWHQLChecks: BOOL;        // If true, allow dxdiag to check if drivers are
                                   // digital signed as logo'd by WHQL which may
                                   // connect via internet to update WHQL certificates.
    pReserved: Pointer;            // Reserved. Must be NULL.
  end;
  DXDIAG_INIT_PARAMS = _DXDIAG_INIT_PARAMS;
  TDxDiagInitParams = _DXDIAG_INIT_PARAMS;


  (****************************************************************************
   *
   * DxDiag Application Interfaces
   *
   ****************************************************************************)

  IDxDiagContainer = interface;

  //
  // COM definition for IDxDiagProvider
  //
  IDxDiagProvider = interface(IUnknown)
    ['{9C6B4CB0-23F8-49CC-A3ED-45A55000A6D2}']
    (*** IDxDiagProvider methods ***)
    function Initialize(const Params: TDxDiagInitParams): HResult; stdcall;
    function GetRootContainer(out ppInstance: IDxDiagContainer): HResult; stdcall;
  end;


  //
  // COM definition for IDxDiagContainer
  //
  IDxDiagContainer = interface(IUnknown)
    ['{7D0F462F-4064-4862-BC7F-933E5058C10F}']
    (*** IDxDiagContainer methods ***)
    function GetNumberOfChildContainers(out dwCount: DWORD): HResult; stdcall;
    function EnumChildContainerNames(dwIndex: DWORD; pwszContainer: PWideChar; cchContainer: DWORD): HResult; stdcall;
    function GetChildContainer(pwszContainer: PWideChar; out ppInstance: IDxDiagContainer): HResult; stdcall;
    function GetNumberOfProps(out pdwCount: DWORD): HResult; stdcall;
    function EnumPropNames(dwIndex: DWORD; pwszPropName: PWideChar; cchPropName: DWORD): HResult; stdcall;
    function GetProp(pwszPropName: PWideChar; out varProp: OleVariant): HResult; stdcall;
  end;


  (****************************************************************************
   *
   * DxDiag Interface IIDs
   *
   ****************************************************************************)

  IID_IDxDiagProvider         = IDxDiagProvider;
  IID_IDxDiagContainer        = IDxDiagContainer;

implementation

end.
