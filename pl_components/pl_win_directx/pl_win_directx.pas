{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_DirectX;

{$warn 5023 off : no warning about unused units}
interface

uses
  D3DX8, D3DX9, Direct3D, Direct3D8, Direct3D9, DirectDraw, DirectInput, 
  DirectMusic, DirectPlay8, DirectSetup, DirectShow9, DirectSound, DX7toDX8, 
  DxDiag, DXErr8, DXErr9, DXFile, DXTypes, WMF9, X3DAudio, XAct, XInput, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_Win_DirectX', @Register);
end.
