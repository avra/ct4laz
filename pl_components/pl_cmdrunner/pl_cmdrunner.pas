{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_cmdrunner;

{$warn 5023 off : no warning about unused units}
interface

uses
  cmdrunner, cmdrunnercustom, cmdrunnerbase, cmdstrings, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_cmdrunner', @Register);
end.
