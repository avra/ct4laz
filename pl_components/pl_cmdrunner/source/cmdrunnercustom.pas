{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}

unit cmdrunnercustom;

interface

{$DEFINE USE_INLINE}

uses
  Classes, SysUtils,AsyncProcess,Process,UTF8Process, Pipes, forms,ExtCtrls,
  SynEdit, SynEditTypes,
  cmdrunnerbase;

type

 TCharSet = set of AnsiChar;

 TCommandBase = class(TComponent)
  private
  protected
    fProcess:TAsyncProcess;
    FCommandLine: string;
    FParameters: string;
    FRunDir:string;
    FExitCode: Integer;

    FLines_SHARED: TStringList;
    FInputLines_SHARED: TStringList;
    FOutputLines_SHARED: TSynEdit; //Link to External TStrings
    FInputToOutput: Boolean;

    FOnStartExecution:TNotifyEvent;
    FOnNewLine: TctNewLineEvent;
    FOnTerminated: TTerminateEvent;

    FTerminalAutoClose:boolean;
    FTerminalTitle:string;
    FIsScript,FIsWaitOnExit:boolean;

    function GetActive:boolean; virtual;
    procedure SetOutputLines_SHARED(Value: TSynEdit); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; virtual;
    procedure Stop(Const ExitCode: integer); virtual;

    procedure SendMsg(Const Value: string);virtual;
    procedure SendLine(Const Value: string; const aLineType:TMsgLineType);

    property OutputLines: TSynEdit read FOutputLines_SHARED write SetOutputLines_SHARED;
    property Lines: TStringList read FLines_SHARED;
    property Active: Boolean read GetActive;

    property ExitCode: Integer read FExitCode write FExitCode;
    property CommandLine: string read FCommandLine write FCommandLine;
    property Parameters: string read FParameters write FParameters;
    property RunDir:string read FRunDir write FRunDir;
    property TerminalTitle:string read FTerminalTitle write FTerminalTitle;
    property TerminalAutoClose:boolean read FTerminalAutoClose write FTerminalAutoClose;
    property IsScript:boolean read FIsScript write FIsScript;
    property IsWaitOnExit:boolean read FIsWaitOnExit write FIsWaitOnExit;

    property OnStartExecution: TNotifyEvent read FOnStartExecution write FOnStartExecution;
    property OnNewLine: TctNewLineEvent read FOnNewLine write FOnNewLine;
    property OnTerminated: TTerminateEvent read FOnTerminated write FOnTerminated;
  end;


  TTextReader = class
  protected
    Stream: TInputPipeStream;
    FOwnsStream: boolean;
    // This is either #0 or #10 (tells to ignore next #13 char) or #13
    // tells to ignore next #10 char)
    LastNewLineChar: char;
    ReadBuf: string;
  public
    // If AOwnsStream then in Destroy we will free Stream object.
    constructor Create(AStream: TInputPipeStream; AOwnsStream: boolean);
    destructor Destroy; override;

    // Reads next line from Stream. Returned string does not contain
    //  any end-of-line characters.
    function Readln: string;
    function Eof: boolean;
  end;

  TProcessLineTalk = class(TProcessUTF8)
  protected
    OutputLineReader: TTextReader;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; override;

    function  Eof: boolean;
    procedure WriteLine(const S: string);
    function  ReadLine: string;
  end;

TCommandCustom = class(TCommandBase)
  private
  protected
    FRunMethods: TStringList;
    FSelRunMethod:integer;
    procedure DoOnTimer(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Execute; override;

    procedure WriteLineEX(Const Value: string);

    property RunMethods:TStringList read FRunMethods;
    property SelRunMethod:integer read FSelRunMethod write FSelRunMethod;
  end;


function  IsCharInSet(C: AnsiChar; const CharSet: TCharSet): Boolean; overload; {$IFDEF USE_INLINE} inline; {$ENDIF}
function  IsCharInSet(C: WideChar; const CharSet: TCharSet): Boolean; overload; {$IFDEF USE_INLINE} inline; {$ENDIF}
procedure StreamWriteLine(const AStream: TStream; const AString: AnsiString);   {$IFDEF USE_INLINE} inline; {$ENDIF}

var
 VarIsScriptRunning:boolean=false;
 VarMustExit:boolean=false;

implementation

function IsCharInSet(C: AnsiChar; const CharSet: TCharSet): Boolean;
begin
  Result := C in CharSet;
end;

function IsCharInSet(C: WideChar; const CharSet: TCharSet): Boolean;
begin
  Result := (C < #$0100) and (AnsiChar(C) in CharSet);
end;

procedure StreamWriteLine(const AStream: TStream; const AString: AnsiString);
var
 LineTerminator: AnsiString;
begin
  if length(AString) > 0 then begin
    AStream.WriteBuffer(AString[1], Length(AString));
  end;
  LineTerminator := LineEnding;
  AStream.Write(LineTerminator[1], Length(LineTerminator));
end;

//===================== TCommandBase ================================================

constructor TCommandBase.Create(AOwner: TComponent);
begin
  inherited;
  FInputToOutput:=true;
  FLines_SHARED := TStringList.Create;
  FLines_SHARED.Clear;
  FInputLines_SHARED := TStringList.Create;
  FInputLines_SHARED.Clear;
  FCommandLine := '';
  FRunDir:='';
  fProcess:=nil;
  FExitCode:=0;
  FTerminalAutoClose:=true;
  FTerminalTitle:='';
  FIsScript:=true;
  FIsWaitOnExit:=true;
  varMustExit:=false;
end;

destructor TCommandBase.Destroy;
begin
  FInputLines_SHARED.free;
  FLines_SHARED.free;
  inherited;
end;

procedure TCommandBase.SendMsg(Const Value: string);
 begin
   if Assigned(FOutputLines_SHARED) then FOutputLines_SHARED.Lines.Add(Value);
 end;

procedure TCommandBase.SendLine(Const Value: string; const aLineType:TMsgLineType);
begin
  if Assigned(OnNewLine) then
    OnNewLine(self,Value,aLineType) else
    OutputLines.Lines.Add(Value);
end;

procedure TCommandBase.SetOutputLines_SHARED(Value: TSynEdit);
begin
    FOutputLines_SHARED := Value;
end;

procedure TCommandBase.Stop(Const ExitCode: integer);
begin
 FExitCode:=ExitCode;

 if fProcess=nil then
 begin
   varMustExit:=true;
   Exit;
 end;

 fProcess.Terminate(ExitCode);
 fProcess.free;
 fProcess:=nil;
 if Assigned(OnTerminated) then OnTerminated(self,FExitCode);
end;

function TCommandBase.GetActive:boolean;
begin
  result:=False;
end;

procedure TCommandBase.Execute;
begin
 //Nothing
end;

//======================== TTextReader =============================================

constructor TTextReader.Create(AStream: TInputPipeStream; AOwnsStream: boolean);
begin
 inherited Create;
 Stream := AStream;
 FOwnsStream := AOwnsStream;
 LastNewLineChar := #0;
end;

destructor TTextReader.Destroy;
begin
 if FOwnsStream then Stream.Free;
 inherited;
end;

function TTextReader.Readln: string;
//================================ SOS SOS
// Extra Numbers of Bytes read from buffer
{$IFDEF Windows}
  const BUF_INC = 2;
{$ELSE}
  const BUF_INC = 1;   //MUST BE 1
{$ENDIF}
//================================

var ReadCnt, i: integer;
begin
 i := 1;

 repeat

 if Stream.NumBytesAvailable=0 then
  begin
    Result := ReadBuf;
    exit;
  end;

  if i > Length(ReadBuf) then
  begin
   SetLength(ReadBuf, Length(ReadBuf) + BUF_INC);
   ReadCnt := Stream.Read(ReadBuf[Length(ReadBuf) - BUF_INC + 1], BUF_INC);
   SetLength(ReadBuf, Length(ReadBuf) - BUF_INC + ReadCnt);

   if ReadCnt = 0 then
   begin
    Result := ReadBuf+LineEnding;
    ReadBuf := '';
    Exit;
   end;
  end;

  if ((ReadBuf[i] = #10) and (LastNewLineChar = #13)) or
     ((ReadBuf[i] = #13) and (LastNewLineChar = #10)) then
  begin
   { We got 2nd newline character ? Ignore it. }
   Assert(i = 1);
   Delete(ReadBuf, 1, 1);
   LastNewLineChar := #0;
  end else
  if IsCharInSet(ReadBuf[i], [#10, #13]) then
  begin
   Result := Copy(ReadBuf, 1, i-1);
   LastNewLineChar := ReadBuf[i];
   Delete(ReadBuf, 1, i);
   Exit;
  end else
  begin
   LastNewLineChar := #0;
   Inc(i);

  end;

 until false;
end;

function TTextReader.Eof: boolean;
var ReadCnt: Integer;
begin
 if ReadBuf = '' then
 begin
  SetLength(ReadBuf, 1);
  ReadCnt := Stream.Read(ReadBuf[1], 1);
  SetLength(ReadBuf, ReadCnt);
 end;
 Result := ReadBuf = '';
end;

//================== TProcessLineTalk ================================

constructor TProcessLineTalk.Create(AOwner: TComponent);
begin
  inherited;
  Options := Options + [poUsePipes,poStderrToOutPut];
end;

destructor TProcessLineTalk.Destroy;
begin
  FreeAndNil(OutputLineReader);
  Active := False;
  inherited;
end;

procedure TProcessLineTalk.WriteLine(const S: string);
begin
  StreamWriteLine(Input, S);
end;

function TProcessLineTalk.ReadLine: string;
begin
  Result := OutputLineReader.Readln;
end;

function TProcessLineTalk.Eof: boolean;
 begin
  Result := OutputLineReader.Eof;
 end;

procedure TProcessLineTalk.Execute;
begin
  inherited;
  FreeAndNil(OutputLineReader);
  OutputLineReader := TTextReader.Create(Output, false);
end;

//================= TCommandCustom ================================

constructor TCommandCustom.Create(AOwner: TComponent);
begin
  inherited;
  FSelRunMethod:=0;
  FRunMethods:=TStringList.Create;
end;

destructor TCommandCustom.Destroy;
begin
  FRunMethods.Free;
  inherited;
end;

var
  xProcess:TProcessLineTalk;

procedure TCommandCustom.WriteLineEX(Const Value: string);
   var ss:String;
       p,l:Integer;
begin
    if xProcess=nil then exit;

    xProcess.Input.WriteBuffer(Value[1], Length(Value));

  // Remove LineEnding and write to Screen, for Unix Problem

    ss:= Value;
    L:=Length(ss);
    p:=Length(LineEnding);
    delete(ss,l-p+1,p);
    SendLine(ss,ltIsLine);
end;


procedure TCommandCustom.DoOnTimer(Sender: TObject);
var
  sss:string;
begin
 if xProcess.Output.NumBytesAvailable > 0 then
     begin

       repeat
          sss:=xProcess.ReadLine;
          SendLine(sss,ltIsLine);
          Application.ProcessMessages;

       until (xProcess.Eof) or (xProcess.Output.NumBytesAvailable<1) or (varMustExit=true);

       Application.ProcessMessages;
     end;
end;

procedure TCommandCustom.Execute;
var
  sss:string;
  xtimer:TTimer;
begin
   if VarIsScriptRunning then
      begin
       SendMsg('Please wait, to finish previews Script first...');
       exit;
      end;

   if Assigned(FOnStartExecution) then FOnStartExecution(self);
   FLines_SHARED.Clear;

   if FCommandLine='' then
    begin
      SendMsg('Error: No Executable Name');
      if Assigned(FOnTerminated) then FOnTerminated(self,0);
      Exit;
    end;

  varMustExit:=false;

  application.ProcessMessages;
  xProcess:=TProcessLineTalk.Create(nil);

  xtimer:=TTimer.Create(nil);
  xtimer.Enabled:=false;
  xtimer.OnTimer:=@DoOnTimer;
  xtimer.Interval:=50;

{$IFDEF Windows}
  if FIsScript=true then  // if is Shell Script
  sss:='cmd /c '+FCommandLine else
  sss:=FCommandLine;
{$ELSE}
  if FIsScript=true then  // if is Shell Script
   sss:='bash '+FCommandLine else
   sss:=FCommandLine;
{$ENDIF}

  if FParameters<>'' then
   sss:=sss+' '+FParameters;

  xProcess.CommandLine:=sss;
  xProcess.CurrentDirectory:=FRunDir;
  xProcess.ShowWindow:=swoHIDE;
                  
  xtimer.Enabled:=true;
  xProcess.Active:=true;

//.......................................................
while (xProcess.Running) and (not Application.terminated) and (varMustExit=false) do
begin

   //--- XTimer do the job ---
   Sleep(100);   // SOS
   Application.ProcessMessages;
end;
//.......................................................
  Sleep(100);

  xtimer.Enabled:=false;
  xtimer.OnTimer:=nil;
  xtimer.Free;
  xProcess.Free;
  VarIsScriptRunning:=false;
  if Assigned(OnTerminated) then OnTerminated(self,FExitCode);
end;

//===============================================================

end.

