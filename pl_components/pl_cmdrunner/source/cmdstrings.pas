{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}

unit cmdstrings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;


resourcestring
  sErrTimeout = 'Application timed out';
  sTerminated = 'Application terminated';
  sBadShareMode = 'Invalid sharing mode';
  sBadOpenMode = 'Invalid file open mode';   
  sCantCreatePipe = 'Can''t create pipe: %s';
  sCantPeekPipe   = 'Can''t read pipe: peek attempt failed';
  sPipeReadError  = 'Error reading pipe';
  sBadWriteHandle = 'Can''t write pipe: handle closed';
  sPipeWriteError = 'Error writing pipe';

implementation

end.

