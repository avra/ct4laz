{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}


unit cmdrunnerwin2;

{$mode Delphi}

interface

uses
   WINDOWS, 
   Messages, SysUtils, Classes, Graphics, Controls, Forms, ExtCtrls,
   SynEdit,SynEditTypes,
   cmdrunnercustom,
   cmdrunnerbase;

type
  TCreatePipeError = class(Exception); //exception raised when a pipe cannot be created
  TCreateProcessError = class(Exception); //exception raised when the process cannot be created

  TReturnCode = (rcCRLF, rcLF);

  TProcessTimer = class(TTimer) //timer for stopping the process after XXX sec
  private
    FSinceBeginning: Integer;
    FSinceLastOutput: Integer;
    procedure MyTimer(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    procedure Beginning; //call this at the beginning of a process
    procedure NewOutput; //call this when a new output is received
    procedure Ending; //call this when the process is terminated
    property SinceBeginning: Integer read FSinceBeginning;
    property SinceLastOutput: Integer read FSinceLastOutput;
  end;


  TShowWindow = (swHIDE, swMAXIMIZE, swMINIMIZE, swRESTORE, swSHOW, swSHOWDEFAULT, swSHOWMAXIMIZED, swSHOWMINIMIZED, swSHOWMINNOACTIVE, swSHOWNA, swSHOWNOACTIVATE, swSHOWNORMAL);
  TCreationFlag = (fCREATE_DEFAULT_ERROR_MODE, fCREATE_NEW_CONSOLE, fCREATE_NEW_PROCESS_GROUP, fCREATE_SEPARATE_WOW_VDM, fCREATE_SHARED_WOW_VDM, fCREATE_SUSPENDED, fCREATE_UNICODE_ENVIRONMENT, fDEBUG_PROCESS, fDEBUG_ONLY_THIS_PROCESS, fDETACHED_PROCESS);

  TDosThreadStatus = ( dtsAllocatingMemory , dtsAllocateMemoryFail ,
                       dtsCreatingPipes    , dtsCreatePipesFail    ,
                       dtsCreatingProcess  , dtsCreateProcessFail  ,
                       dtsRunning          , dtsRunningError       ,
                       dtsSuccess,
                       dtsUserAborted,
                       dtsTimeOut );

  TDosCommand = class;

  TDosThread = class(TThread) //the thread that is waiting for outputs through the pipe
  private
    FOwner: TDosCommand;
    FCommandLine: string;
    FParameters: string;
    FTimer: TProcessTimer;
    FMaxTimeAfterBeginning: Integer;
    FMaxTimeAfterLastOutput: Integer;
    FOnNewLine: TctNewLineEvent;
    FOnTerminated: TTerminateEvent;
    FCreatePipeError: TCreatePipeError;
    FCreateProcessError: TCreateProcessError;
    FPriority: Integer;
    FShowWindow: TShowWindow;
    FCreationFlag: TCreationFlag;
    FProcessInfo_SHARED: ^PROCESS_INFORMATION;
    FOutputStr: String;
    FOutputType: TMsgLineType;
    procedure FExecute;
  protected
    procedure Execute; override; //call this to create the process
    procedure AddString;
    procedure AddString_SHARED(Str: string; OutType: TMsgLineType);
  public
    InputLines_SHARED: TstringList;
    FLineBeginned: Boolean;
    FActive: Boolean;
    FRunDir:string;
    constructor Create( AOwner: TDosCommand );
  end;

TDosCommand = class(TCommandBase) //the component to put on a form
  private
    FTimer: TProcessTimer;
    FThread: TDosThread;
    FThreadStatus: TDosThreadStatus;

    FMaxTimeAfterBeginning: Integer;
    FMaxTimeAfterLastOutput: Integer;
    FPriority: Integer;

    FShowWindow: TShowWindow;
    FCreationFlag: TCreationFlag;

    FProcessInfo_SHARED: PROCESS_INFORMATION;
    FReturnCode: TReturnCode;
    FOutputReturnCode: TReturnCode;
    FSync :TMultiReadExclusiveWriteSynchronizer;

    function  GetPrompting:boolean;
    function  GetSinceBeginning: Integer;
    function  GetSinceLastOutput:integer;

  protected
    function  GetActive:boolean;override;
    procedure SetOutputLines_SHARED(Value: TSynEdit); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Stop(Const ExitCode: integer); override;
    procedure SendLine(Value: string; Eol: Boolean);
    function  Execute2: Integer;

    property Priority: Integer read FPriority write FPriority;

    property Prompting:boolean read GetPrompting;
    property SinceBeginning: Integer read GetSinceBeginning;
    property SinceLastOutput:integer read GetSinceLastOutput;

    property ProcessInfo:PROCESS_INFORMATION read FProcessInfo_SHARED;
    property ThreadStatus:TDosThreadStatus read FThreadStatus write FThreadStatus;
    property Sync: TMultiReadExclusiveWriteSynchronizer read FSync;
  published

    property InputToOutput: Boolean read FInputToOutput write FInputToOutput;
       //maximum time of execution
    property MaxTimeAfterBeginning: Integer read FMaxTimeAfterBeginning  write FMaxTimeAfterBeginning;
       //maximum time of execution without an output
    property MaxTimeAfterLastOutput: Integer read FMaxTimeAfterLastOutput write FMaxTimeAfterLastOutput;

    property ShowWindow : TShowWindow read FShowWindow write FShowWindow;
    property CreationFlag : TCreationFlag read FCreationFlag write FCreationFlag;

    property ReturnCode: TReturnCode read FReturnCode write FReturnCode;
    property OutputReturnCode: TReturnCode read FOutputReturnCode;
  end;


implementation

type TCharBuffer = array[0..MaxInt - 1] of Char;

const ShowWindowValues : array [0..11] of Integer = (SW_HIDE, SW_MAXIMIZE, SW_MINIMIZE, SW_RESTORE, SW_SHOW, SW_SHOWDEFAULT, SW_SHOWMAXIMIZED, SW_SHOWMINIMIZED, SW_SHOWMINNOACTIVE, SW_SHOWNA, SW_SHOWNOACTIVATE, SW_SHOWNORMAL);
const CreationFlagValues : array [0..8] of Integer = (CREATE_DEFAULT_ERROR_MODE, CREATE_NEW_CONSOLE, CREATE_NEW_PROCESS_GROUP, CREATE_SEPARATE_WOW_VDM, {CREATE_SHARED_WOW_VDM,} CREATE_SUSPENDED, CREATE_UNICODE_ENVIRONMENT, DEBUG_PROCESS, DEBUG_ONLY_THIS_PROCESS, DETACHED_PROCESS);

//================== TProcessTimer ===========================================

constructor TProcessTimer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Enabled := False; //timer is off
  OnTimer := MyTimer;
end;

procedure TProcessTimer.MyTimer(Sender: TObject);
begin
  Inc(FSinceBeginning);
  Inc(FSinceLastOutput);
end;

procedure TProcessTimer.Beginning;
begin
  Interval := 1000; //time is in sec
  FSinceBeginning := 0; //this is the beginning
  FSinceLastOutput := 0;
  Enabled := True; //set the timer on
end;

procedure TProcessTimer.NewOutput;
begin
  FSinceLastOutput := 0; //a new output has been caught
end;

procedure TProcessTimer.Ending;
begin
  Enabled := False; //set the timer off
end;

//===================== TDosThread ================================================

constructor TDosThread.Create( AOwner: TDosCommand );
begin
  FOwner := AOwner;
  FCommandline := FOwner.CommandLine;   // copy.  not shared;
  InputLines_SHARED := FOwner.FInputLines_SHARED;
  InputLines_SHARED.Clear;
  //FInputToOutput := FOwner.InputToOutput;
  FOnNewLine := FOwner.FOnNewLine;
  FOnTerminated := FOwner.FOnTerminated;
  FTimer := FOwner.FTimer;  // can access private!!
  FMaxTimeAfterBeginning := FOwner.FMaxTimeAfterBeginning;
  FMaxTimeAfterLastOutput := FOwner.FMaxTimeAfterLastOutput;
  FPriority := FOwner.FPriority;
  FShowWindow := FOwner.FShowWindow;
  FCreationFlag := FOwner.FCreationFlag;
  FRunDir:=FOwner.RunDir;
  FParameters:=FOwner.Parameters;
  FLineBeginned := False;
  FProcessInfo_SHARED := @FOwner.FProcessInfo_SHARED;
  FreeOnTerminate:=true;
  FActive := True;
  inherited Create(False);

end;

procedure TDosThread.FExecute;
const
  MaxBufSize = 1024;
var
  pBuf: ^TCharBuffer; //i/o buffer
  iBufSize: Cardinal;
  si: STARTUPINFOA;
  sa: PSECURITYATTRIBUTES; //security information for pipes
  sd: PSECURITY_DESCRIPTOR;
  pi: PROCESS_INFORMATION;
  newstdin, newstdout, read_stdout, write_stdin: THandle; //pipe handles
  Exit_Code: LongWord; //process exit code
  bread: LongWord; //bytes read
  avail: LongWord; //bytes available
  Str, Last: string;
  PStr: PChar;
  I, II: LongWord;
  eol, EndCR:boolean;
  ssTotal:string;
  PDir:PChar;
begin

  if FCommandLine='' then
   begin
     FOwner.SendMsg('Error: No Executable Name');
     Exit;
   end;

  if FileExists(FCommandLine)=false then
   begin
     FOwner.SendMsg('  ERROR: File '+FCommandLine+' NOT Found');
     exit;
   end;

try

  FOwner.ThreadStatus := dtsAllocatingMemory;
  GetMem(sa, sizeof(SECURITY_ATTRIBUTES));

  //initialize security descriptor (Windows NT)
    if (Win32Platform = VER_PLATFORM_WIN32_NT) then
     begin
       GetMem(sd, sizeof(SECURITY_DESCRIPTOR));
       InitializeSecurityDescriptor(sd, SECURITY_DESCRIPTOR_REVISION);
       SetSecurityDescriptorDacl(sd, true, nil, false);
       sa.lpSecurityDescriptor := sd;
     end else
     begin
       sa.lpSecurityDescriptor := nil;
       sd := nil;
     end;

    sa.nLength := sizeof(SECURITY_ATTRIBUTES);
    sa.bInheritHandle := true; //allow inheritable handles
    iBufSize := MaxBufSize;
    pBuf := AllocMem(iBufSize); // Reserve and init Buffer

    try /// memory allocated

     FOwner.ThreadStatus := dtsCreatingPipes;
     if not (CreatePipe(newstdin, write_stdin, sa, 0)) then //create stdin pipe
       begin
          FOwner.SendMsg('Pipe Error...');
          Exit;
       end;

     if not (CreatePipe(read_stdout, newstdout, sa, 0)) then //create stdout pipe
       begin
         CloseHandle(newstdin);
         CloseHandle(write_stdin);
         FOwner.SendMsg('Process Error...');
         Exit;
       end;

     try /// handles for pipes

     FOwner.ThreadStatus := dtsCreatingProcess;
     GetStartupInfo(si);
    //set startupinfo for the spawned process
     {The dwFlags member tells CreateProcess how to make the process.
      STARTF_USESTDHANDLES validates the hStd* members. STARTF_USESHOWWINDOW
      validates the wShowWindow member.}
     si.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
     si.wShowWindow := ShowWindowValues[Ord(FShowWindow)]; //SW_SHOW; //SW_HIDE; //SW_SHOWMINIMIZED;
     si.hStdOutput := newstdout;
     si.hStdError := newstdout; //set the new handles for the child process
     si.hStdInput := newstdin;

  if FParameters = '' then
     ssTotal:=FCommandLine else
     ssTotal:=FCommandLine+' '+Trim(FParameters);

  if FRunDir='' then
       PDir:=nil else
       PDir:=PChar(FRunDir);


  if not (CreateProcess(nil,
                        PChar(ssTotal),
                        nil,                          //pointer to command line string
                        nil,                          //pointer to thread security attributes
                        true,                         //handle inheritance flag
                        CreationFlagValues[Ord(FCreationFlag)] or FPriority, //creation flags
                        nil,                          //pointer to new environment block
                        PDir,                         //pointer to current directory name
                        si,                           //pointer to STARTUPINFO
                        pi)) then
  begin

    FOwner.SendMsg('ERROR While try to run '+FCommandLine);
    Exit;
  end;

  FTimer.Beginning; //turn the timer on
  Exit_Code := STILL_ACTIVE;
  try /// handles of process

    FOwner.ThreadStatus := dtsRunning;
    Last := ''; // Buffer to save last output without finished with CRLF
    FLineBeginned := False;
    EndCR := False;

    repeat //main program loop

      GetExitCodeProcess(pi.hProcess, Exit_Code); //while the process is running

      PeekNamedPipe(read_stdout, pBuf, iBufSize, @bread, @avail, nil);
      //check to see if there is any data to read from stdout

      if (bread <> 0) then begin
        if (iBufSize < avail) then begin // If BufferSize too small then rezize
          iBufSize := avail;
          ReallocMem(pBuf, iBufSize);
        end;
        FillChar(pBuf^, iBufSize, #0); //empty the buffer
        ReadFile(read_stdout, pBuf^, iBufSize, bread, nil); //read the stdout pipe
        Str := Last; //take the begin of the line (if exists)
        i := 0;
        while ((i < bread) and not (Terminated)) do
        begin
          case pBuf^[i] of
            #0: Inc(i);
            #10, #13:
              begin
                Inc(i);
                if not (EndCR and (pBuf^[i - 1] = #10)) then
                begin
                  if (i < bread) and (pBuf^[i - 1] = #13) and (pBuf^[i] = #10) then
                  begin
                    Inc(i);
                    FOwner.FOutputReturnCode := rcCRLF;
                    //Str := Str + #13#10;
                  end
                  else
                  begin
                    FOwner.FOutputReturnCode := rcLF;
                    //Str := Str + #10;
                  end;
                  //so we don't test the #10 on the next step of the loop
                  //FTimer.NewOutput; //a new ouput has been caught
                  AddString_SHARED(Str, ltIsLine);
                  Str := '';
                end;
              end;
          else
            begin
              Str := Str + pBuf^[i]; //add a character
              Inc(i);
            end;
          end;
        end;
        EndCR := (pBuf^[i - 1] = #13);
        Last := Str; // no CRLF found in the rest, maybe in the next output
        if (Last <> '') then
        begin
          AddString_SHARED(Last, ltAddToLastLine);
        end;

      end
      else
      begin
      //send Lines in input (if exist)
        FOwner.sync.beginWrite ;
        try
          while ((InputLines_SHARED.Count > 0) and not (Terminated)) do
          begin
            	//	enough size?
            II := Length(InputLines_SHARED[0]);
            if (iBufSize < II) then
              iBufSize := II;
            FillChar(pBuf^, iBufSize, #0); //clear the buffer
            eol := (Pos(#13#10, InputLines_SHARED[0]) = II - 1) or (Pos(#10, InputLines_SHARED[0]) = II);
            for I := 0 to II - 1 do
              pBuf^[I]:=InputLines_SHARED[0][I + 1];
            WriteFile(write_stdin, pBuf^, II, bread, nil); //send it to stdin
            if FOwner.FInputToOutput then //if we have to output the inputs
            begin
              if FLineBeginned then
                Last := Last + InputLines_SHARED[0]
              else
                Last := InputLines_SHARED[0];
              if eol then
              begin
                AddString_SHARED(Last, ltIsLine);
                Last := '';
              end
              else
                AddString_SHARED(Last, ltAddToLastLine);
            end;
            InputLines_SHARED.Delete(0); //delete the line that has been send
          end;
        finally
          FOwner.sync.EndWrite ;
        end;
      end;

      Sleep(1); // Give other processes a chance

      if Exit_Code <> STILL_ACTIVE then begin
        FOwner.ThreadStatus := dtsSuccess;
        FOwner.FExitCode := Exit_Code;
        //ReturnValue := Exit_Code;
        break;
      end;

      if Terminated then begin //the user has decided to stop the process
        FOwner.ThreadStatus := dtsUserAborted;
        break;
      end;

      if ((FMaxTimeAfterBeginning < FTimer.FSinceBeginning)
      and (FMaxTimeAfterBeginning > 0)) //time out
      or ((FMaxTimeAfterLastOutput < FTimer.FSinceLastOutput)
      and (FMaxTimeAfterLastOutput > 0))
      then begin
        FOwner.ThreadStatus := dtsTimeOut;
        break;
      end;

    until (Exit_Code <> STILL_ACTIVE); //process terminated (normally)

    if (Last <> '') then // If not empty flush last output
      AddString_SHARED(Last, ltAddToLastLine);

  finally /// handles of process
    if (Exit_Code = STILL_ACTIVE) then
      TerminateProcess(pi.hProcess, 0);
    FTimer.Ending; //turn the timer off
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
  end;
  finally /// handles for pipes
    CloseHandle(newstdin); //clean stuff up
    CloseHandle(newstdout);
    CloseHandle(read_stdout);
    CloseHandle(write_stdin);
  end;
  finally /// memory(1)
    FreeMem(pBuf);
    if (Win32Platform = VER_PLATFORM_WIN32_NT) then
      FreeMem(sd);
    FreeMem(sa);
  end;
  finally /// free self
    if Assigned(FOnTerminated) then FOnTerminated(FOwner, Exit_Code);

    case FOwner.ThreadStatus of
    dtsAllocatingMemory:
      begin
        FOwner.ThreadStatus := dtsAllocateMemoryFail ;
        FOwner.FExitCode := GetLastError;
      end;
    dtsCreatingPipes:
      begin
        FOwner.ThreadStatus := dtsCreatePipesFail ;
        FOwner.FExitCode := GetLastError;
      end;
    dtsCreatingProcess:
      begin
        FOwner.ThreadStatus := dtsCreateProcessFail ;
        FOwner.FExitCode := GetLastError;
      end;
    dtsRunning:
      begin
        FOwner.ThreadStatus := dtsRunningError ;
        FOwner.FExitCode := GetLastError;
      end;
    end;
    FreeOnTerminate := true;
    FActive := False;
    terminate;
  end;
end;


procedure TDosThread.Execute;
begin
  try
    FExecute;
  except
    on E: TCreatePipeError do Application.ShowException(E);
    on E: TCreateProcessError do Application.ShowException(E);
  end;
end;

procedure TDosThread.AddString_SHARED(Str: string; OutType: TMsgLineType);
begin
  try
    FOwner.Lines.Add(str);  // ??
    FTimer.NewOutput; //a new ouput has been caught
    FOutputStr := Str;
    FOutputType := OutType;
    Synchronize(AddString);
  except
  end;
end;

procedure TDosThread.AddString;
 var ic:integer;
begin      {
    if Assigned(FOwner.OutputLines) then
    begin
      ic:=FOwner.OutputLines.Count;

      FOwner.OutputLines.BeginUpdate;
      try

        if ic = 0 then
        begin
          if (FOutputType = otIsLine) then
            FOwner.OutputLines.Add(FOutputStr) else
            FOwner.OutputLines.Text := FOutputStr;
        end else
        begin
          // change the way to add by last addstring type
          if FLineBeginned then
            FOwner.OutputLines[ic - 1] := FOutputStr else
            FOwner.OutputLines.Add(FOutputStr);
        end;
      finally
        FOwner.OutputLines.EndUpdate;
      end;
    end;
                        }
    FLineBeginned := (FOutputType = ltAddToLastLine);
    if Assigned(FOnNewLine) then FOnNewLine(FOwner, FOutputStr, FOutputType);
end;


//===================== TDosCommand ================================================

constructor TDosCommand.Create(AOwner: TComponent);
begin
  inherited;

  FSync := TMultiReadExclusiveWriteSynchronizer.Create;
  FTimer := nil;
  FThread:=nil;
  FMaxTimeAfterBeginning := 0;
  FMaxTimeAfterLastOutput := 0;
  FPriority := NORMAL_PRIORITY_CLASS;
  FShowWindow := swHide;
  FCreationFlag := fCREATE_NEW_CONSOLE;
end;


destructor TDosCommand.Destroy;
begin
  if FThread <> nil then Stop(0);
  if FTimer <> nil then FTimer.free;
  FSync.Free;
  inherited;
end;

procedure TDosCommand.SetOutputLines_SHARED(Value: TSynEdit);
begin
  Sync.beginWrite ;
  try
  if (FOutputLines_SHARED <> Value) then
  begin
    FOutputLines_SHARED := Value;
  end;
  finally Sync.EndWrite ; end;
end;

procedure TDosCommand.Execute;
begin
  if FThread <> nil then Stop(0);

  if FCommandLine ='' then exit;

  if Assigned(FOnStartExecution) then
      begin
        FOnStartExecution(self);
      end;

  if (FTimer = nil) then FTimer := TProcessTimer.Create(self);

  FLines_SHARED.Clear;

  if FThread=nil then FThread := TDosThread.Create(Self);
end;


function TDosCommand.Execute2: Integer;
begin
  Execute;

  while Self.Active do Application.ProcessMessages;

  Result := FExitCode;
end;

procedure TDosCommand.Stop;
begin
  if (FThread = nil) then exit;
  FThread.Terminate;
  FThread := nil;
end;


procedure TDosCommand.SendLine(Value: string; Eol: Boolean);
var
  i, sp, L: Integer;
  Str: String;
begin

  try
    if (FThread <> nil) then
    begin
      if Eol then
      begin
        if FReturnCode = rcCRLF then
          Value := Value + #13#10   else
          Value := Value + #10;
      end;

      FInputLines_SHARED.Add(Value);

    end;
  finally
  end;
end;

function TDosCommand.GetPrompting:boolean;
begin
  result := Active and (( FTimer.FSinceLastOutput > 3 ) or FThread.FLineBeginned);
end;

function TDosCommand.GetActive:boolean;
begin
  result := ( FThread <> nil ) and ( FThread.FActive ) and (not FThread.Terminated);
end;

function TDosCommand.GetSinceLastOutput:integer;
begin
  result := -1;
  if GetActive then result := FTimer.FSinceLastOutput;
end;

function TDosCommand.GetSinceBeginning:integer;
begin
  result := -1;
  if GetActive then result := FTimer.FSinceBeginning;
end;

end.

