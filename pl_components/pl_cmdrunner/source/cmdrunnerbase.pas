{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}

unit cmdrunnerbase;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  SynEdit, SynEditTypes;


Const
  cnUserExitCode=9999;


const
{$IFDEF WINDOWS}
   XTempApp='CMD';
{$ELSE}
  {$IFDEF DARWIN}
  XTempApp='/usr/local/bin/xterm' ;
  {$ELSE}
  XTempApp='xterm' ;
  {$ENDIF}
{$ENDIF}

{$IFDEF WINDOWS}
   cnCodeTyphonFont='Courier New';
   cnCodeTyphonFontHeight=8;
 {$ELSE}

  {$IFDEF DARWIN}

   {$IFDEF LCLCocoa}
     cnCodeTyphonFont='Courier 10 Pitch';
     cnCodeTyphonFontHeight=9;
   {$ELSE}
     cnCodeTyphonFont='Courier 10 Pitch';
     cnCodeTyphonFontHeight=9;
   {$ENDIF}

   {$ELSE}
   cnCodeTyphonFont='Courier 10 Pitch';
   cnCodeTyphonFontHeight=10;
  {$ENDIF}

 {$ENDIF}

Type

 TMsgLineType = (ltClear, ltIsLine, ltAddToLastLine, ltReplaceLastLine); //to know if the newline is finished.

 TctNewLineEvent = procedure(Sender: TObject; const NewLine: string; const OutputType: TMsgLineType) of object;
 TTerminateEvent = procedure(Sender: TObject; ExitCode: Integer) of object;

 TCmdSynEditBase = class(TSynEdit)

 end;


implementation

end.

