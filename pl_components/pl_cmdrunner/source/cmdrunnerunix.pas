{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}

unit cmdrunnerunix;

interface

uses
  Classes, SysUtils,AsyncProcess,Process,forms,
  cmdrunnercustom;

type

TCommandUnix = class(TCommandCustom)
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Execute_0;
    procedure Execute_1;
  published
  end;

implementation

const

{$IFDEF DARWIN}
  XTempApp='/usr/local/bin/xterm' ;
{$ELSE}
  XTempApp='xterm' ;
{$ENDIF}


//===================== TCommandUnix ================================================

constructor TCommandUnix.Create(AOwner: TComponent);
begin
  inherited;
  FRunMethods.add('Run Embedded');
  FRunMethods.add('Run in xterm');
  FRunMethods.add('Run in xterm and wait');
end;

destructor TCommandUnix.Destroy;
begin
  inherited;
end;

procedure TCommandUnix.Execute;
begin

 case FSelRunMethod of
   0:Execute_0;

   1:begin
      TerminalAutoClose:=true;
      Execute_1;
      end;
   2:begin
      TerminalAutoClose:=false;
      Execute_1;
      end;

 end;
end;

procedure TCommandUnix.Execute_0;
 begin
   inherited Execute;
 end;

procedure TCommandUnix.Execute_1;
 var sss:string;
begin
   if varIsScriptRunning then
      begin
       SendMsg('Please wait, to finish previews Script first...');
       exit;
      end;

   if Assigned(FOnStartExecution) then FOnStartExecution(self);
   FLines_SHARED.Clear;

   if FCommandLine='' then
    begin
      SendMsg('Error: No Executable Name');
      if Assigned(FOnTerminated) then FOnTerminated(self,0);
      Exit;
    end;
  //--------------------------------------------
 varMustExit:=false;

 application.ProcessMessages;
 fProcess:=TAsyncProcess.Create(nil);

  sss:='';

  if FTerminalTitle<>'' then
  //sss := XTempApp+' -ls -rightbar -sb -sl 5000 -bg black -fg green -title '+FTerminalTitle else
  //-title NOT work
    sss := XTempApp+' -ls -rightbar -sb -sl 5000 -bg black -fg green' else
    sss := XTempApp+' -ls -rightbar -sb -sl 5000 -bg black -fg green';

//....................................

 if FIsScript=true then  //======= if is Shell Script
  begin

  if FParameters='' then
   begin
    if FTerminalAutoClose then
      sss := sss + ' -e "bash '+FCommandLine+'"' else
      sss := sss + ' -e "bash '+FCommandLine+' ; read -p ''Script finish, Press Enter to close...''" ';
   end else
   begin
    if FTerminalAutoClose then
      sss := sss + ' -e "bash '+FCommandLine+' '+FParameters+'"' else
      sss := sss + ' -e "bash '+FCommandLine+' '+FParameters+' ; read -p ''Script finish, Press Enter to close...''"';
   end;

  end else               //======= if is Application
  begin

  if FParameters='' then
   begin
    if FTerminalAutoClose then
      sss := sss + ' -e "'+FCommandLine+'"' else
      sss := sss + ' -e "'+FCommandLine+' ; read -p ''Press Enter to close...''" ';
   end else
   begin
    if FTerminalAutoClose then
      sss := sss + ' -e "'+FCommandLine+' '+FParameters+'"' else
      sss := sss + ' -e "'+FCommandLine+' '+FParameters+' ; read -p ''Press Enter to close...''"';
   end;

  end;

//....................................

  fProcess.CommandLine := sss;
  fProcess.CurrentDirectory:=FRunDir;
  fProcess.ShowWindow:=swoShowNormal;

  if FIsWaitOnExit then
    fProcess.Options:=fProcess.Options+[poWaitOnExit];

//=====================================
  application.MainForm.Enabled:=false;
  fProcess.Execute;
  application.ProcessMessages;
  application.MainForm.Enabled:=true;
//=====================================

  FExitCode:=fProcess.ExitStatus;
  fProcess.free;
  fProcess:=nil;
  varIsScriptRunning:=false;
  if Assigned(OnTerminated) then OnTerminated(self,FExitCode);
end;



end.
