{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}

unit cmdrunner;

{$mode objfpc}{$H+}

interface

uses            
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls, 
  Process,UTF8Process,
  SynEdit, SynEditTypes,
  cmdrunnercustom,
  cmdrunnerbase;

Type

 TCmdRunner = class(TComponent)
  protected
     FShowStartFinish:Boolean;
     FCmdEngine:TCommandCustom;
     FCmdEngineStartTime:TDateTime;
     FCmdEngineBoxLastPoint:Tpoint;

     Function  GetRunMethods:TStringList;
     Function  GetRunMethod:integer;
     Procedure SetRunMethod(const aValue:integer);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure DoiScriptStartExecution(Sender: TObject);
    procedure DoiScriptTerminate(Sender: TObject; ExitCode: integer);
    procedure DoiScriptNewLine(Sender: TObject; const NewLine: string; const aLineType: TMsgLineType);
    procedure DoiScript_SetCursorToEnd(const aType:integer=1);

    function API_RunFileInCommandBox(const aFile,aDir,aPar,aTitle:string; aIsScript:boolean=true):boolean;
    function API_ExernalProgramRun(const aExeFile:string; Const IsConcole:boolean=false; Const MustWait:boolean=false):THandle;
    function API_ExernalProgramRunEx(const aExeFile,Dir,Par:string; Const IsConcole:boolean=false; Const MustWait:boolean=false):THandle;
    function API_WinExernalProgramRunEx(const aExeFile,Dir,Par:string; Const IsConcole:boolean=false; Const MustWait:boolean=false):THandle;
    function API_ExernalProgramIsRunning(const aProgramName:string):boolean;
    function API_ExernalProgramClose(const aProgramName:string; const ShowError:boolean=false):boolean;

    Property CmdEngine:TCommandCustom read FCmdEngine;
    property CmdRunMethods:TStringList read GetRunMethods;
    property CmdRunMethod:integer read GetRunMethod write SetRunMethod;
    property ShowStartFinish:Boolean read fShowStartFinish write fShowStartFinish;
  end;

 TCmdSynEditRunner = class(TCmdSynEditBase)
  private
    FRunner:TCmdRunner;
    procedure DoiScript_SetCursorToEnd(const aType:integer=1);
  public         
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteLine(const NewLine: string; const aLineType: TMsgLineType=ltIsLine);

    Property Runner:TCmdRunner read FRunner;
  end;

implementation
  
uses
  {$IFDEF WINDOWS}
   WINDOWS,
   cmdrunnerwin;
  {$ELSE}
   cmdrunnerunix;
  {$ENDIF}

//==== TCmdSynEditRunner ==========================================

constructor TCmdSynEditRunner.Create(AOwner: TComponent);  
Begin
  inherited;
             
  FRunner:=TCmdRunner.Create(self);
  FRunner.CmdEngine.OutputLines:=self;

  Font.Name:=cnCodeTyphonFont;
  Font.size:=cnCodeTyphonFontHeight;

  Font.Color:=clLime;//$FFC000;//$FD9F26;//clAqua;
  color:=clBlack;

  self.RightEdgeColor:=clBlack;
  self.RightEdge:=250;

  Gutter.AutoSize := False;
  Gutter.Visible := False;
  Gutter.Width := 0;
  ParentColor := False;
  ParentFont := False;
  ParentShowHint := False;
end;

destructor TCmdSynEditRunner.Destroy;  
Begin
        
  Clear;
  FRunner.Free;
  inherited;
end;

procedure TCmdSynEditRunner.DoiScript_SetCursorToEnd(const aType:integer=1);
begin
 FRunner.DoiScript_SetCursorToEnd(aType);
end;

procedure TCmdSynEditRunner.WriteLine(const NewLine: string; const aLineType: TMsgLineType=ltIsLine);
begin
 FRunner.DoiScriptNewLine(self,NewLine,aLineType);
end;

//======= TCmdRunner ===============================

constructor TCmdRunner.Create(AOwner: TComponent);
Begin
  inherited;
  {$IFDEF WINDOWS}
   FCmdEngine:=TCommandWindows.create(self);
  {$ELSE}
   FCmdEngine:=TCommandUnix.create(self);
  {$ENDIF}

  FShowStartFinish:=True;
  FCmdEngine.OnStartExecution:=@DoiScriptStartExecution;
  FCmdEngine.OnTerminated    :=@DoiScriptTerminate;
  FCmdEngine.OnNewLine       :=@DoiScriptNewLine;
end;

destructor TCmdRunner.Destroy;
Begin

  inherited;
end;

Function  TCmdRunner.GetRunMethods:TStringList;
begin
  Result:=FCmdEngine.RunMethods;
end;

Function  TCmdRunner.GetRunMethod:integer;
begin
 Result:=FCmdEngine.SelRunMethod;
end;

Procedure TCmdRunner.SetRunMethod(const aValue:integer);
begin
  FCmdEngine.SelRunMethod:=aValue;
end;

procedure TCmdRunner.DoiScriptStartExecution(Sender: TObject);
  var ss:string;
 begin

   //FCmdEngine.OutputLines.Lines.clear;
   FCmdEngineStartTime:=now;
   ss:=DateTimeTostr(FCmdEngineStartTime);

   if FShowStartFinish then
   begin
     FCmdEngine.OutputLines.Lines.Add(' ');
     FCmdEngine.OutputLines.Lines.Add('Start Action at '+ss);
     FCmdEngine.OutputLines.Lines.Add('_______________________________________________________________');
   end;

   VarIsScriptRunning:=true;
   //ToolButton_StopScript.Enabled:=VarIsScriptRunning;
 end;


procedure TCmdRunner.DoiScriptTerminate(Sender: TObject; ExitCode: integer);
  var ss,ss2:string;
      xCommandBoxEndTime:TdateTime;
 begin
   xCommandBoxEndTime:=now;

   if FShowStartFinish then
   begin
     ss:=DateTimeTostr(xCommandBoxEndTime);
     ss2:=FormatDateTime('hh:nn:ss.zzz',xCommandBoxEndTime-FCmdEngineStartTime);
     FCmdEngine.OutputLines.Lines.Add('_______________________________________________________________');
     FCmdEngine.OutputLines.Lines.Add(' ');
     FCmdEngine.OutputLines.Lines.Add('Terminate Action at '+ss);
     FCmdEngine.OutputLines.Lines.Add('Total time: '+ss2);
     FCmdEngine.OutputLines.Lines.Add('ExitCode : '+intTostr(ExitCode));
   end;

   DoiScript_SetCursorToEnd(2);

   VarIsScriptRunning:=false;

 end;


procedure TCmdRunner.DoiScriptNewLine(Sender: TObject; const NewLine: string; const aLineType: TMsgLineType);
  var ic:integer;
begin
     ic:=FCmdEngine.OutputLines.Lines.Count;

      FCmdEngine.OutputLines.BeginUpdate;
      try
        if ic = 0 then
        begin

          case aLineType of
            ltClear          :begin
                               FCmdEngine.OutputLines.Lines.Clear;    
                               FCmdEngine.OutputLines.Lines.Add(NewLine);
                               FCmdEngine.OutputLines.Lines.Add(' ');
                               FCmdEngine.OutputLines.Lines.Add('_______________________________________________________________');
                              end;
            ltIsLine         :FCmdEngine.OutputLines.Lines.Add(NewLine);
            ltAddToLastLine  :FCmdEngine.OutputLines.Lines.Text := FCmdEngine.OutputLines.Lines.Text+NewLine;
            ltReplaceLastLine:FCmdEngine.OutputLines.Lines.Text := NewLine;
          end;

        end else
        begin

          case aLineType of
            ltClear          :begin
                               FCmdEngine.OutputLines.Lines.Clear;     
                               FCmdEngine.OutputLines.Lines.Add(NewLine);
                               FCmdEngine.OutputLines.Lines.Add(' ');
                               FCmdEngine.OutputLines.Lines.Add('_______________________________________________________________');
                              end;
            ltIsLine         :FCmdEngine.OutputLines.Lines.Add(NewLine);
            ltAddToLastLine  :FCmdEngine.OutputLines.Lines[ic - 1] := FCmdEngine.OutputLines.Lines[ic - 1]+NewLine;
            ltReplaceLastLine:FCmdEngine.OutputLines.Lines[ic - 1] := NewLine;
          end;

        end;

        DoiScript_SetCursorToEnd;

      finally
        FCmdEngine.OutputLines.EndUpdate;
      end;
end;


procedure TCmdRunner.DoiScript_SetCursorToEnd(const aType:integer=1);
begin
case atype of
  0:exit;
  1:FCmdEngine.OutputLines.TopLine:=FCmdEngine.OutputLines.Lines.Count;
  2:begin
     //vCommandBox.SetFocus;
     FCmdEngineBoxLastPoint:=Classes.Point(Length(FCmdEngine.OutputLines.Lines[FCmdEngine.OutputLines.Lines.Count-1])+1,
                                    FCmdEngine.OutputLines.Lines.Count);
     FCmdEngine.OutputLines.CaretXY:=FCmdEngineBoxLastPoint;
    end;
end;
end;

//===================================================
     
function TCmdRunner.API_RunFileInCommandBox(const aFile,aDir,aPar,aTitle:string; aIsScript:boolean=true):boolean;
 begin
   FCmdEngine.IsScript:=aIsScript;
   FCmdEngine.CommandLine:=aFile;
   FCmdEngine.RunDir:=aDir;
   FCmdEngine.Parameters:=aPar;
   FCmdEngine.TerminalTitle:=aTitle;
   FCmdEngine.Execute;
end;

function TCmdRunner.API_ExernalProgramRun(const aExeFile:string; Const IsConcole:boolean=false; Const MustWait:boolean=false):THandle;
begin
  result:=API_ExernalProgramRunEx(aExeFile,'','',IsConcole,MustWait);
end;

function TCmdRunner.API_ExernalProgramRunEx(const aExeFile,Dir,Par:string; Const IsConcole:boolean=false; Const MustWait:boolean=false):THandle;
  var ReturnValue:THandle;
      AProcess:TProcessUTF8;
      xxPar:string;
begin

  if Par='' then
     xxPar:='' else
     xxPar:=' '+Par;

  AProcess:=TProcessUTF8.Create(nil);
  AProcess.CommandLine := aExeFile;
  AProcess.CurrentDirectory:=Dir;
  AProcess.ShowWindow:=swoShowNormal;

  {$IFDEF DARWIN}
    if FileExists(aExeFile+'.app/Contents/MacOS/'+ExtractFileName(aExeFile)) then
      AProcess.CommandLine:=aExeFile+'.app/Contents/MacOS/'+ExtractFileName(aExeFile);
  {$ENDIF}

   if IsConcole then
   begin
    if MustWait=false then
     {$IFDEF WINDOWS}
      AProcess.CommandLine := XTempApp+' /C '+AProcess.CommandLine+xxPar  else
      AProcess.CommandLine := XTempApp+' /K '+AProcess.CommandLine+xxPar;
     {$ELSE}
      AProcess.CommandLine := XTempApp+' -e bash -c '+AProcess.CommandLine+xxPar  else
      AProcess.CommandLine := XTempApp+' -e "'+AProcess.CommandLine+xxPar+' ; read -p ''Press Enter to close...''"';
     {$ENDIF}
   end else
   begin
     AProcess.CommandLine:=AProcess.CommandLine+xxPar;
   end;

  AProcess.Execute;
  AProcess.free;
end;


function TCmdRunner.API_ExernalProgramIsRunning(const aProgramName:string):boolean;
{$IFDEF WINDOWS}
 var H: HWND;
 begin
   H := FindWindow(nil, pchar(aProgramName));
   result:=(H<>0);
 end;
{$ELSE}
begin
end;
{$ENDIF}

function TCmdRunner.API_ExernalProgramClose(const aProgramName:string; const ShowError:boolean=false):boolean;
{$IFDEF WINDOWS}
 var H: HWND;
begin
  H := FindWindow(nil, pchar(aProgramName));
  if H <> 0 then
   begin
    SendMessage(H, WM_CLOSE, 0, 0);
    result:=true;
   end else
   begin
    if ShowError then  ShowMessage(aProgramName+' NOT Found');
    result:=false;
   end;
end;
{$ELSE}
begin
end;
{$ENDIF}

//==============================================

function TCmdRunner.API_WinExernalProgramRunEx(const aExeFile,Dir,Par:string; Const IsConcole:boolean=false; Const MustWait:boolean=false):THandle;
{$IFDEF WINDOWS}
  var ReturnValue:THandle;
      aexe,adir,aPar:string;
begin
  aexe:=aExeFile;
  adir:=Dir;
  aPar:=Par;
  ReturnValue := 0;

   if IsConcole then
   begin

    if MustWait=false then
      ReturnValue :=  ShellExecute(0, 'open', pchar('CMD'),  pchar(aExeFile+' '+apar),pchar(adir), SW_NORMAL)  else
      ReturnValue :=  ShellExecute(0, 'open', pchar('CMD'),  pchar('/K '+aExeFile+' '+apar),pchar(adir), SW_NORMAL);

   end else
   begin
      ReturnValue :=  ShellExecute(0, 'open', pchar(aexe),  pchar(apar),pchar(adir), SW_NORMAL);
   end;

  result:=ReturnValue;
  if ReturnValue <= 32 then
    showmessage(SysErrorMessage(ReturnValue)+ ' '+ IntToStr(ReturnValue));
end;
{$ELSE}
begin
end;
{$ENDIF}


end.

