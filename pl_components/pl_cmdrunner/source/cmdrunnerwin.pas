{**********************************************************************
          Copyright (c) PilotLogic Software House
                   All rights reserved

 Package pl_CMDRunner
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
           
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
   
***********************************************************************}

unit cmdrunnerwin;

interface

uses
  Classes, SysUtils, AsyncProcess, Process, forms,
  cmdrunnercustom,
  cmdrunnerwin2;

type

TCommandWindows = class(TCommandCustom)
  protected  
   fEmbedOLD:TDosCommand;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Execute_0;
    procedure Execute_1;
    procedure Execute_2;
  published
  end;

implementation


//===================== TCommandWindows ================================================

constructor TCommandWindows.Create(AOwner: TComponent);
begin
  inherited;

  FRunMethods.add('Run Embedded');
  FRunMethods.add('Run in CMD Window');
  FRunMethods.add('Run in CMD Window and wait');   
  FRunMethods.add('Run Embedded Win 2');
end;

destructor TCommandWindows.Destroy;
begin
  inherited;
end;

procedure TCommandWindows.Execute;
begin
 case FSelRunMethod of
   0:Execute_0;
   1:begin
      TerminalAutoClose:=true;
      Execute_1;
      end;
   2:begin
      TerminalAutoClose:=false;
      Execute_1;
      end;    
   3:Execute_2;
 end;
end;

procedure TCommandWindows.Execute_0; //**************** Run Embedded
 begin
   inherited Execute;
 end;


procedure TCommandWindows.Execute_1; //**************** Run in CMD Window
 var sss:string;
begin
   if VarIsScriptRunning then
      begin
       SendMsg('Please wait, to finish previews Script first...');
       exit;
      end;

   if Assigned(FOnStartExecution) then FOnStartExecution(self);
   FLines_SHARED.Clear;

   if FCommandLine='' then
    begin
      SendMsg('Error: No Executable Name');
      if Assigned(FOnTerminated) then FOnTerminated(self,0);
      Exit;
    end;
  //--------------------------------------------
  varMustExit:=false;

  application.ProcessMessages;
  fProcess:=TAsyncProcess.Create(nil);    
  fProcess.ConsoleTitle:=FCommandLine;    
  fProcess.CurrentDirectory:=FRunDir;
  fProcess.ShowWindow:=swoShowNormal;

  if FTerminalAutoClose then
    sss := 'CMD /C' else
    sss := 'CMD /K';

  fProcess.CommandLine :=sss+' '+FCommandLine;
  if FIsWaitOnExit then
    fProcess.Options:=fProcess.Options+[poWaitOnExit];

 // fProcess.FillAttribute:=0002;
//=====================================
  application.MainForm.Enabled:=false;
  fProcess.Active:=true;
  application.ProcessMessages;
  application.MainForm.Enabled:=true;
//=====================================

  FExitCode:=fProcess.ExitStatus;
  fProcess.free;
  fProcess:=nil;
  VarIsScriptRunning:=false;
  if Assigned(OnTerminated) then OnTerminated(self,FExitCode);
end;   


procedure TCommandWindows.Execute_2;   //**************** Run Embedded Second
 begin
    if fEmbedOLD=nil then
       fEmbedOLD:=TDosCommand.Create(nil);

    varMustExit:=false;

    fEmbedOLD.OutputLines:= self.OutputLines;
    fEmbedOLD.CommandLine:= self.CommandLine;
    fEmbedOLD.Parameters:=self.Parameters;
    fEmbedOLD.RunDir:=self.RunDir;
    fEmbedOLD.TerminalTitle:=self.TerminalTitle;
    fEmbedOLD.TerminalAutoClose:=self.TerminalAutoClose;
    fEmbedOLD.IsScript:=self.IsScript;
    fEmbedOLD.IsWaitOnExit:=self.IsWaitOnExit;

    fEmbedOLD.OnStartExecution:=self.OnStartExecution;
    fEmbedOLD.OnNewLine:=self.OnNewLine;
    fEmbedOLD.OnTerminated:=self.OnTerminated;

    fEmbedOLD.Execute;
 end;


end.
