{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_Media;

{$warn 5023 off : no warning about unused units}
interface

uses
  Media.AMAudio, Media.AMVideo, Media.AVIFMT, Media.CodecAPI, Media.DDraw, 
  Media.DEVIOCTL, Media.DSound, Media.DV, Media.DVDMedia, Media.DWMAPI, 
  Media.DXVA, Media.DXVA2API, Media.DXVA2SWDev, Media.DXVA2Trace, 
  Media.DXVA9Typ, Media.DXVAHD, Media.KSMedia, Media.KSUUIDs, Media.MediaObj, 
  Media.MFAPI, Media.MFCaptureEngine, Media.MFError, Media.MFIdl, 
  Media.MFMediaCapture, Media.MFMediaEngine, Media.MFMP2DLNA, Media.MFObjects, 
  Media.MFPlay, Media.MFReadWrite, Media.MFSharingEngine, Media.MFTransform, 
  Media.MMReg, Media.MMSysCom, Media.MPEG2Data, Media.MPEG2Structs, 
  Media.strmif, Media.UUIDs, Media.EVNTrace, Media.EVR, Media.EVR9, 
  Media.HString, Media.Inspectable, Media.KS, Media.MMEAPI, Media.PropSys, 
  Media.UXTheme, Media.WMContainer, Media.WTypes, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_Win_Media', @Register);
end.
