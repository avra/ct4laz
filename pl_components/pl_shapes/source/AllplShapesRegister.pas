{*************************************************************************
                PilotLogic Software House

  Package pl_Shapes
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

 ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * ***** END LICENSE BLOCK *****
 ************************************************************************}

unit AllplShapesRegister;


interface

 uses
  Classes,SysUtils,TypInfo,lresources,PropEdits,ComponentEditors,
  //...........................
  TplShapeObjects,TplShapeObjectsExt,
  TplFillShapeUnit,
  TplShapesUnit,
  TplShapeLineUnit;

procedure Register;

implementation

{$R AllplShapesRegister.res}

//==========================================================
procedure Register;
begin

  RegisterComponents('Shapes', [TplFillShape,
                                TplShapeLine,
                                TplSquareShape,TplRoundSquareShape,TplRectShape,TplRoundRectShape,
                                TplEllipseShape, TplCircleShape,
                                TplTriangleShape,TplRectangleShape,
                                TplParallelogramShape,
                                TplTrapezoidShape,TplPentagonShape,TplHexagonShape,TplOctagonShape,
                                TplStarShape,TplBubbleShape]);


  RegisterComponents('Shapes Objects',
    [TplLine, TplLLine, TplZLine, TplBezier, TplText, TplSolidPoint, TplDrawPicture, TplRectangle,
    TplDiamond, TplEllipse, TplArc, TplPolygon, TplStar, TplSolidArrow, TplSolidBezier,
    TplTextBezier, TplRandomPoly,TplDrawObjectComposite]);
end;

end.
