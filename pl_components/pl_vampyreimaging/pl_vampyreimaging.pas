{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_VampyreImaging;

{$warn 5023 off : no warning about unused units}
interface

uses
  ImagingBinary, ImagingBitmap, ImagingCanvases, ImagingClasses, 
  ImagingColors, ImagingComponents, ImagingCore, ImagingDds, ImagingGif, 
  ImagingJpeg, ImagingNetworkGraphics, ImagingNIF, ImagingPortableMaps, 
  ImagingTarga, ImagingTypes, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_VampyreImaging', @Register);
end.
