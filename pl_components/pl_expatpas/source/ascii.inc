{***************************************************************************
                    PilotLogic Software House
                   
 Package pl_ExpatPas
 is a native pascal of "Expat" XML parser library (https://libexpat.github.io/)
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

   ****** BEGIN LICENSE BLOCK *****

   The contents of this file are used with permission, subject to the Mozilla
   Public License Version 2.0 (the "License"); you may not use this file except
   in compliance with the License. You may obtain a copy of the License at
   https://www.mozilla.org/en-US/MPL/2.0/

   Software distributed under the License is distributed on an "AS IS" basis,
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
   the specific language governing rights and limitations under the License.

   ****** END LICENSE BLOCK *****

****************************************************************************}

 ASCII_A = char($41 );
 ASCII_B = char($42 );
 ASCII_C = char($43 );
 ASCII_D = char($44 );
 ASCII_E = char($45 );
 ASCII_F = char($46 );
 ASCII_G = char($47 );
 ASCII_H = char($48 );
 ASCII_I = char($49 );
 ASCII_J = char($4A );
 ASCII_K = char($4B );
 ASCII_L = char($4C );
 ASCII_M = char($4D );
 ASCII_N = char($4E );
 ASCII_O = char($4F );
 ASCII_P = char($50 );
 ASCII_Q = char($51 );
 ASCII_R = char($52 );
 ASCII_S = char($53 );
 ASCII_T = char($54 );
 ASCII_U = char($55 );
 ASCII_V = char($56 );
 ASCII_W = char($57 );
 ASCII_X = char($58 );
 ASCII_Y = char($59 );
 ASCII_Z = char($5A );

 ASCII_al = char($61 );
 ASCII_bl = char($62 );
 ASCII_cl = char($63 );
 ASCII_dl = char($64 );
 ASCII_el = char($65 );
 ASCII_fl = char($66 );
 ASCII_gl = char($67 );
 ASCII_hl = char($68 );
 ASCII_il = char($69 );
 ASCII_jl = char($6A );
 ASCII_kl = char($6B );
 ASCII_ll = char($6C );
 ASCII_ml = char($6D );
 ASCII_nl = char($6E );
 ASCII_ol = char($6F );
 ASCII_pl = char($70 );
 ASCII_ql = char($71 );
 ASCII_rl = char($72 );
 ASCII_sl = char($73 );
 ASCII_tl = char($74 );
 ASCII_ul = char($75 );
 ASCII_vl = char($76 );
 ASCII_wl = char($77 );
 ASCII_xl = char($78 );
 ASCII_yl = char($79 );
 ASCII_zl = char($7A );

 ASCII_0 = char($30 );
 ASCII_1 = char($31 );
 ASCII_2 = char($32 );
 ASCII_3 = char($33 );
 ASCII_4 = char($34 );
 ASCII_5 = char($35 );
 ASCII_6 = char($36 );
 ASCII_7 = char($37 );
 ASCII_8 = char($38 );
 ASCII_9 = char($39 );

 ASCII_TAB        = char($09 );
 ASCII_SPACE      = char($20 );
 ASCII_EXCL       = char($21 );
 ASCII_QUOT       = char($22 );
 ASCII_AMP        = char($26 );
 ASCII_APOS       = char($27 );
 ASCII_MINUS      = char($2D );
 ASCII_PERIOD     = char($2E );
 ASCII_COLON      = char($3A );
 ASCII_SEMI       = char($3B );
 ASCII_LT         = char($3C );
 ASCII_EQUALS     = char($3D );
 ASCII_GT         = char($3E );
 ASCII_LSQB       = char($5B );
 ASCII_RSQB       = char($5D );
 ASCII_UNDERSCORE = char($5F );
