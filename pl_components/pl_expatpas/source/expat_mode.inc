{***************************************************************************
                    PilotLogic Software House
                   
 Package pl_ExpatPas
 is a native pascal of "Expat" XML parser library (https://libexpat.github.io/)
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

   ****** BEGIN LICENSE BLOCK *****

   The contents of this file are used with permission, subject to the Mozilla
   Public License Version 2.0 (the "License"); you may not use this file except
   in compliance with the License. You may obtain a copy of the License at
   https://www.mozilla.org/en-US/MPL/2.0/

   Software distributed under the License is distributed on an "AS IS" basis,
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
   the specific language governing rights and limitations under the License.

   ****** END LICENSE BLOCK *****

****************************************************************************}

{DEFINE EXPAT_DEBUG }

//----------------------------------------------------------------------------

{DEFINE XML_UNICODE_WCHAR_T }
{DEFINE XML_LARGE_SIZE }
{DEFINE XML_MIN_SIZE }

//----- PLATFORM DEPENDENT CONFIG --------------------------------------------

{IFDEF Windows}
 {$I winconfig.inc }
{ENDIF } 

//----------------------------------------------------------------------------
{$IFDEF XML_UNICODE_WCHAR_T }
 {$DEFINE XML_UNICODE }
{$ENDIF }

{$IFDEF CPUPOWERPC }
 {$DEFINE EXPAT_CPU_PPC }

{$ENDIF }

{$IFDEF CPUI386 }
 {$DEFINE EXPAT_CPU_386 }

{$ENDIF }

{$MODE DELPHI }

//----------------------------------------------------------------------------
{$IFDEF EXPAT_DEBUG }
 {$DEFINE EXPAT_FULL_DEBUG }

{$ENDIF }

//----------------------------------------------------------------------------
{ SWITCHES CONFIGURATION }

{$B- }{ Complete boolean evaluation }
{$V- }{ String type checking }
{$X+ }{ Extended syntax }

{$IFDEF EXPAT_FULL_DEBUG }
 {$R+ }{ Range checking }
 {$I+ }{ IO checking }
 {$Q+ }{ Overflow checking }

 {$IFNDEF FPC }
  {$O- }{ Code Optimization }
 {$ENDIF }

 {$D+ }{ Debug Info ON }
 {$Y+ }{ References Info ON }

{$ELSE }

 {$R- }{ Range checking }
 {$I- }{ IO checking }
 {$Q- }{ Overflow checking }
 {$D- }{ Debug Info OFF }
 {$Y- }{ References Info OFF }

{$ENDIF }
