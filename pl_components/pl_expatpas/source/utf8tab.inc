{***************************************************************************
                    PilotLogic Software House
                   
 Package pl_ExpatPas
 is a native pascal of "Expat" XML parser library (https://libexpat.github.io/)
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

   ****** BEGIN LICENSE BLOCK *****

   The contents of this file are used with permission, subject to the Mozilla
   Public License Version 2.0 (the "License"); you may not use this file except
   in compliance with the License. You may obtain a copy of the License at
   https://www.mozilla.org/en-US/MPL/2.0/

   Software distributed under the License is distributed on an "AS IS" basis,
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
   the specific language governing rights and limitations under the License.

   ****** END LICENSE BLOCK *****

****************************************************************************} 

{ $80 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $84 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $88 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $8C } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $90 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $94 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $98 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $9C } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $A0 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $A4 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $A8 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $AC } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $B0 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $B4 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $B8 } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $BC } BT_TRAIL  ,BT_TRAIL  ,BT_TRAIL   ,BT_TRAIL   ,
{ $C0 } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $C4 } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $C8 } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $CC } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $D0 } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $D4 } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $D8 } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $DC } BT_LEAD2  ,BT_LEAD2  ,BT_LEAD2   ,BT_LEAD2   ,
{ $E0 } BT_LEAD3  ,BT_LEAD3  ,BT_LEAD3   ,BT_LEAD3   ,
{ $E4 } BT_LEAD3  ,BT_LEAD3  ,BT_LEAD3   ,BT_LEAD3   ,
{ $E8 } BT_LEAD3  ,BT_LEAD3  ,BT_LEAD3   ,BT_LEAD3   ,
{ $EC } BT_LEAD3  ,BT_LEAD3  ,BT_LEAD3   ,BT_LEAD3   ,
{ $F0 } BT_LEAD4  ,BT_LEAD4  ,BT_LEAD4   ,BT_LEAD4   ,
{ $F4 } BT_LEAD4  ,BT_NONXML ,BT_NONXML  ,BT_NONXML  ,
{ $F8 } BT_NONXML ,BT_NONXML ,BT_NONXML  ,BT_NONXML  ,
{ $FC } BT_NONXML ,BT_NONXML ,BT_MALFORM ,BT_MALFORM
