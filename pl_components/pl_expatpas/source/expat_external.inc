{***************************************************************************
                    PilotLogic Software House
                   
 Package pl_ExpatPas
 is a native pascal of "Expat" XML parser library (https://libexpat.github.io/)
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

   ****** BEGIN LICENSE BLOCK *****

   The contents of this file are used with permission, subject to the Mozilla
   Public License Version 2.0 (the "License"); you may not use this file except
   in compliance with the License. You may obtain a copy of the License at
   https://www.mozilla.org/en-US/MPL/2.0/

   Software distributed under the License is distributed on an "AS IS" basis,
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
   the specific language governing rights and limitations under the License.

   ****** END LICENSE BLOCK *****

****************************************************************************}

(* General Integer Types *)
 int8   = shortint;
 int8u  = byte;
 int16  = smallint;
 int16u = word;
 int32  = longint;
 int32u = longword;
 int64  = system.int64;
 int64u = qword;

(* General Character Types *)
 char8  = char;
 char16 = int16u;
 char32 = int32u;

(* C/C++ compatibility Types *)
 int      = int32;
 unsigned = int32u;
 size_t   = int32u;

(* Pascal Pointer Computation Type *)
{$IFDEF CPU64 }
 ptrcomp = system.int64;
{$ELSE }
 ptrcomp = integer;
{$ENDIF } 

(* Type Pointers *)
 int8_ptr         = ^int8;
 int8_ptr_ptr     = ^int8_ptr;

 int8u_ptr        = ^int8u;
 int8u_ptr_ptr    = ^int8u_ptr;

 int16_ptr        = ^int16;
 int16_ptr_ptr    = ^int16_ptr;

 int16u_ptr       = ^int16u;
 int16u_ptr_ptr   = ^int16u_ptr;

 int32_ptr        = ^int32;
 int32_ptr_ptr    = ^int32_ptr;

 int32u_ptr       = ^int32u;
 int32u_ptr_ptr   = ^int32u_ptr;

 int64_ptr        = ^int64;
 int64_ptr_ptr    = ^int64_ptr;

 int64u_ptr       = ^int64u;
 int64u_ptr_ptr   = ^int64u_ptr;

 char8_ptr        = ^char8;
 char8_ptr_ptr    = ^char8_ptr;

 char16_ptr       = ^char16;
 char16_ptr_ptr   = ^char16_ptr;

 char32_ptr       = ^char32;
 char32_ptr_ptr   = ^char32_ptr;

 int_ptr          = ^int;
 int_ptr_ptr      = ^int_ptr;

 unsigned_ptr     = ^unsigned;
 unsigned_ptr_ptr = ^unsigned_ptr;

 char_ptr         = ^char;
 char_ptr_ptr     = ^char_ptr;

(* Expat Types *)
{$IFDEF XML_UNICODE } // Information is UTF-16 encoded.
{$IFDEF XML_UNICODE_WCHAR_T }
 XML_Char  = int16u;
 XML_LChar = int16u;

{$ELSE }
 XML_Char  = word;
 XML_LChar = char;

{$ENDIF }

{$ELSE }              // Information is UTF-8 encoded.
 XML_Char  = char;
 XML_LChar = char;

{$ENDIF }

 XML_Char_ptr  = ^XML_Char;
 XML_LChar_ptr = ^XML_LChar;

 XML_Char_ptr_ptr = ^XML_Char_ptr;

{$IFDEF XML_LARGE_SIZE } // Use large integers for file/stream positions.
 XML_Index = int64;
 XML_Size  = int64u;

{$ELSE }
 XML_Index = longint;
 XML_Size  = longword;

{$ENDIF }

