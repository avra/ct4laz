{***************************************************************************
                    PilotLogic Software House
                   
 Package pl_ExpatPas
 is a native pascal of "Expat" XML parser library (https://libexpat.github.io/)
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

   ****** BEGIN LICENSE BLOCK *****

   The contents of this file are used with permission, subject to the Mozilla
   Public License Version 2.0 (the "License"); you may not use this file except
   in compliance with the License. You may obtain a copy of the License at
   https://www.mozilla.org/en-US/MPL/2.0/

   Software distributed under the License is distributed on an "AS IS" basis,
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
   the specific language governing rights and limitations under the License.

   ****** END LICENSE BLOCK *****

****************************************************************************}

{ $00 } BT_NONXML ,BT_NONXML ,BT_NONXML ,BT_NONXML ,
{ $04 } BT_NONXML ,BT_NONXML ,BT_NONXML ,BT_NONXML ,
{ $08 } BT_NONXML ,BT_S      ,BT_LF     ,BT_NONXML ,
{ $0C } BT_NONXML ,BT_CR     ,BT_NONXML ,BT_NONXML ,
{ $10 } BT_NONXML ,BT_NONXML ,BT_NONXML ,BT_NONXML ,
{ $14 } BT_NONXML ,BT_NONXML ,BT_NONXML ,BT_NONXML ,
{ $18 } BT_NONXML ,BT_NONXML ,BT_NONXML ,BT_NONXML ,
{ $1C } BT_NONXML ,BT_NONXML ,BT_NONXML ,BT_NONXML ,
{ $20 } BT_S      ,BT_EXCL   ,BT_QUOT   ,BT_NUM    ,
{ $24 } BT_OTHER  ,BT_PERCNT ,BT_AMP    ,BT_APOS   ,
{ $28 } BT_LPAR   ,BT_RPAR   ,BT_AST    ,BT_PLUS   ,
{ $2C } BT_COMMA  ,BT_MINUS  ,BT_NAME   ,BT_SOL    ,
{ $30 } BT_DIGIT  ,BT_DIGIT  ,BT_DIGIT  ,BT_DIGIT  ,
{ $34 } BT_DIGIT  ,BT_DIGIT  ,BT_DIGIT  ,BT_DIGIT  ,
{ $38 } BT_DIGIT  ,BT_DIGIT  ,BT_COLON  ,BT_SEMI   ,
{ $3C } BT_LT     ,BT_EQUALS ,BT_GT     ,BT_QUEST  ,
{ $40 } BT_OTHER  ,BT_HEX    ,BT_HEX    ,BT_HEX    ,
{ $44 } BT_HEX    ,BT_HEX    ,BT_HEX    ,BT_NMSTRT ,
{ $48 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $4C } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $50 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $54 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $58 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_LSQB   ,
{ $5C } BT_OTHER  ,BT_RSQB   ,BT_OTHER  ,BT_NMSTRT ,
{ $60 } BT_OTHER  ,BT_HEX    ,BT_HEX    ,BT_HEX    ,
{ $64 } BT_HEX    ,BT_HEX    ,BT_HEX    ,BT_NMSTRT ,
{ $68 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $6C } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $70 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $74 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,
{ $78 } BT_NMSTRT ,BT_NMSTRT ,BT_NMSTRT ,BT_OTHER  ,
{ $7C } BT_VERBAR ,BT_OTHER  ,BT_OTHER  ,BT_OTHER  ,
