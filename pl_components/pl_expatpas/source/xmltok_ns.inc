{***************************************************************************
                    PilotLogic Software House
                   
 Package pl_ExpatPas
 is a native pascal of "Expat" XML parser library (https://libexpat.github.io/)
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

   ****** BEGIN LICENSE BLOCK *****

   The contents of this file are used with permission, subject to the Mozilla
   Public License Version 2.0 (the "License"); you may not use this file except
   in compliance with the License. You may obtain a copy of the License at
   https://www.mozilla.org/en-US/MPL/2.0/

   Software distributed under the License is distributed on an "AS IS" basis,
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
   the specific language governing rights and limitations under the License.

   ****** END LICENSE BLOCK *****

****************************************************************************} 

const
{$IFDEF XML_NS }
 encodingsNS : array[0..6 ] of ENCODING_ptr = (
  @latin1_encoding_ns.enc  ,
  @ascii_encoding_ns.enc   ,
  @utf8_encoding_ns.enc    ,
  @big2_encoding_ns.enc    ,
  @big2_encoding_ns.enc    ,
  @little2_encoding_ns.enc ,
  @utf8_encoding_ns.enc    ); { NO_ENC }

{$ENDIF }

 encodings : array[0..6 ] of ENCODING_ptr = (
  @latin1_encoding.enc  ,
  @ascii_encoding.enc   ,
  @utf8_encoding.enc    ,
  @big2_encoding.enc    ,
  @big2_encoding.enc    ,
  @little2_encoding.enc ,
  @utf8_encoding.enc    ); { NO_ENC }

{ initScanProlog }
function initScanProlog(enc : ENCODING_ptr; ptr ,end_ : char_ptr; nextTokPtr : char_ptr_ptr ) : int;
begin
 result:=initScan(@encodings ,INIT_ENCODING_ptr(enc ) ,XML_PROLOG_STATE ,ptr ,end_ ,nextTokPtr );

end;

{ initScanContent {..}
function initScanContent(enc : ENCODING_ptr; ptr ,end_ : char_ptr; nextTokPtr : char_ptr_ptr ) : int;
begin
end;

{ XMLINITENCODING }
function XmlInitEncoding;
var
 i : int;

begin
 i:=getEncodingIndex(name );

 if i = UNKNOWN_ENC then
  begin
   result:=0;

   exit;

  end;

 SET_INIT_ENC_INDEX(p ,i );

 p.initEnc.scanners[XML_PROLOG_STATE  ]:=@initScanProlog;
 p.initEnc.scanners[XML_CONTENT_STATE ]:=@initScanContent;

 p.initEnc.updatePosition:=@initUpdatePosition;

 p.encPtr:=encPtr;
 encPtr^ :=@p.initEnc;

 result:=1;

end;

{ XMLINITENCODINGNS }
function XmlInitEncodingNS;
begin
end;

{ XmlGetUtf8InternalEncoding }
function XmlGetUtf8InternalEncoding : ENCODING_ptr;
begin
 result:=@internal_utf8_encoding.enc;

end;

{ XmlGetUtf16InternalEncoding {..}
function XmlGetUtf16InternalEncoding : ENCODING_ptr;
begin
end;

{ XMLGETINTERNALENCODING }
function XmlGetInternalEncoding;
begin
{$IFDEF XML_UNICODE }
 result:=XmlGetUtf16InternalEncoding;

{$ELSE }
 result:=XmlGetUtf8InternalEncoding;

{$ENDIF }

end;

{ XmlGetUtf8InternalEncodingNS {..}
function XmlGetUtf8InternalEncodingNS : ENCODING_ptr;
begin
end;

{ XmlGetUtf16InternalEncodingNS {..}
function XmlGetUtf16InternalEncodingNS : ENCODING_ptr;
begin
end;

{ XMLGETINTERNALENCODINGNS }
function XmlGetInternalEncodingNS;
begin
{$IFDEF XML_UNICODE }
 result:=XmlGetUtf16InternalEncodingNS;

{$ELSE }
 result:=XmlGetUtf8InternalEncodingNS;

{$ENDIF }

end;

{ findEncoding {..}
function findEncoding(enc : ENCODING_ptr; ptr ,end_ : char_ptr ) : ENCODING_ptr;
begin
end;

{ XMLPARSEXMLDECL }
function XmlParseXmlDecl;
begin
 result:=
  doParseXmlDecl(
   @findEncoding ,
   isGeneralTextEntity ,
   enc ,ptr ,end_ ,
   badPtr ,
   versionPtr ,
   versionEndPtr ,
   encodingNamePtr ,
   namedEncodingPtr ,
   standalonePtr );

end;

