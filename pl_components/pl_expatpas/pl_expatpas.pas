{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_ExpatPas;

{$warn 5023 off : no warning about unused units}
interface

uses
  expat, expat_basics, expat_external, xmlrole, xmltok, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_ExpatPas', @Register);
end.
