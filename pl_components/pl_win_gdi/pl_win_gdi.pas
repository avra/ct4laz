{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_GDI;

{$warn 5023 off : no warning about unused units}
interface

uses
  GDICONST, GDIPAPI, GDIPOBJ, GDIPUTIL, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_Win_GDI', @Register);
end.
