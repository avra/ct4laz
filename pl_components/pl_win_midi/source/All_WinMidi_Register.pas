{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}

 unit All_WinMidi_Register;

{$IFNDEF WINDOWS}
//////////////////////////////////////////////////////////////////////
 'Sorry, This is WINDOWS 32/64 MIDI Component package'
//////////////////////////////////////////////////////////////////////
{$ENDIF}

interface


procedure Register;

implementation

{$R All_WinMidi_Register.res}

uses Classes,lresources,
     cmpMidiData, cmpMidiGrid, cmpPianoRoll, cmpMidiInput, cmpMidiOutput,
     cmpMidiPlayer, cmpTrackOutputs, cmpPosDisplay, cmpTimeDisplay, cmpKeyboard,
     cmpControllerMap, cmpTempoMap, cmpTimerSpeedButton, cmpTG77Controller,
     cmpMidiMixer,cmpMidiIterator,cmpSynthController,cmpBarControl,
     WinMIDIEngine;

procedure Register;
begin
  RegisterComponents('Win_MIDI', [
                                 TMidiData, TMidiGrid, TPianoRoll, TMidiInput, TMidiOutputPort,
                                 TMidiPlayer, TTrackOutputs, TPosDisplay, TTimeDisplay,
                                 TControllerMap, TTempoMap, TTimerSpeedButton,
                                 TTG77Controller, TMidiMixer,TMidiIterator,
                                 TSynthController,TBarControl,
                                 TMidiKeys, TMidiPiano,
                                 TWinMidiEngine]);
end;


end.
