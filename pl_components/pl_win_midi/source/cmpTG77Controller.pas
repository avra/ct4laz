{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}
 unit cmpTG77Controller;

interface

uses Windows, Classes, SysUtils, cmpSynthController, cmpMidiOutput, unitMidiGlobals;

type
TDeviceNo = 0..15;
TBulkDumpType = (dtMulti, dtAdditionalMulti, dtPan, dtMicroTuning, dtSystemSetup, dtProgramChange);

TBulkDumpRequestMessage = record
  header : array [0..3] of byte;
  idString : array [0..9] of char;
  zeros : array [0..13] of char;
  p1, p2 : byte;
  eom : byte;
end;
PBulkDumpRequestMessage = ^TBulkDumpRequestMessage;

TBulkDumpMessage = record
  header : array [0..3] of byte;
  len : word;
  idString : array [0..9] of char;
  zeros : array [0..11] of char;
  p1, p2 : byte;
  eom : byte;
end;
PBulkDumpMessage = ^TBulkDumpMessage;

TTinyInt = -64..63;
TTinyByte = 0..127;

TSystemSetupParameterChangeTable = packed record
  GreetingMessageU : array [0..19] of char;
  GreetingMessageL : array [0..19] of char;
  MasterNoteShift : TTinyInt;
  MasterFineTuning : TTinyInt;
  FixedVelocity : TTinyByte;
  VelocityCurveSelect : 0..7;
  Mod2ControlAssign : 0..120;
  FootSwitchAssign : 0..120;
  EditConfirm : Boolean;
  KeyboardTXChannel : 0..15;
  VoiceRXChannel : 0..16;
  LocalSwitch : Boolean;
  DeviceNo : 0..17;
  NoteEvenOddSwitch : 0..2;
  BulkDataMemoryProtectSwitch : Boolean;
  ProgramChangeMode : 0..3;
  reserved : array [0..9] of char;
  EffectBypassSwitch : boolean;
end;
PSystemSetupParameterChangeTable = ^TSystemSetupParameterChangeTable;




TTG77Controller = class (TSynthController)
private
  fDeviceNo: TDeviceNo;
  procedure SetDeviceNo(const Value: TDeviceNo);
protected
  procedure HandlePowerOn; override;
  procedure HandleSysexMessage (msg : TInputMessage); override;
public
  procedure Reset; override;
  property DeviceNo : TDeviceNo read fDeviceNo write SetDeviceNo;
  procedure RequestBulkDump (tp : TBulkDumpType; p1, p2 : byte);
end;

implementation

{ TTG77Controller }


procedure TTG77Controller.HandlePowerOn;
begin
  RequestBulkDump (dtSystemSetup, 0, 0);
  RequestBulkDump (dtProgramChange, 0, 0);
end;

procedure TTG77Controller.HandleSysexMessage(msg: TInputMessage);
var
  DeviceNo : Byte;
  Len : Word;
  MsgType : string;
  sysParams : PSystemSetupParameterChangeTable;

begin
  inherited;

  if (Msg.len > 32) and (Msg.data [0] = #$f0) and (Msg.data [1] = #$43) and (Msg.data [3] = #$7a) then
  begin
    DeviceNo := Byte (Msg.data [2]);
    len := Swap (PWord (@Msg.data [4])^);

    SetLength (MsgType, 10);
    Move (Msg.data [6], MsgType [1], 10);

    Windows.Beep (880,10);

    if MsgType = 'LM  8101SY' then
      sysParams := PSystemSetupParameterChangeTable (@Msg.Data [32])
    else
      if MsgType = 'LM  8104PC' then

  end
end;

procedure TTG77Controller.RequestBulkDump(tp: TBulkDumpType; p1, p2 : byte);
const
  DumpStrings : array [TBulkDumpType] of string = (
    'LM  8101MU',
    'LM  8104MU',
    'LM  8101PN',
    'LM  8101MT',
    'LM  8101SY',
    'LM  8104PC');
var
  msg : TBulkDumpRequestMessage;
begin
  msg.header [0] := $f0;
  msg.header [1] := $43;
  msg.header [2] := $20 + DeviceNo;
  msg.header [3] := $7A;
  Move (DumpStrings [tp][1], msg.idString, sizeof (msg.idString));
  FillChar (msg.zeros, SizeOf (msg.zeros), 0);
  msg.p1 := p1;
  msg.p2 := p2;
  msg.eom := $f7;

  OutSysex (PChar (@msg), sizeof (msg));
  WaitForSysex
end;

procedure TTG77Controller.Reset;
begin
  inherited;

end;

procedure TTG77Controller.SetDeviceNo(const Value: TDeviceNo);
begin
  fDeviceNo := Value;
end;

end.
