{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}

unit cmpTrackOutputs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  cmpMidiOutput, cmpMidiData, unitMidiTrackStream, unitMidiGlobals;

type


  TTrackOutput = class(TComponent)
  private
    fTrackData: TMidiTrackStream;
    fPort: TMidiOutputPort;
    fEventIndex: integer;
    fMute: boolean;
    procedure Open(Data: TMidiTrackStream; OutputPortNo: integer);
    procedure Close;
    procedure SetPortID(Value: integer);
    function GetPortID: integer;
    procedure SetMute(const Value: boolean);
  public
    destructor Destroy; override;
    procedure SetPatchForPosition;
    property Port: TMidiOutputPort read fPort;
    property PortID: integer read GetPortID write SetPortID;
    property EventIndex: integer read fEventIndex write fEventIndex;
    property TrackData: TMidiTrackStream read fTrackData;
    property Mute: boolean read fMute write SetMute;
  end;

  TOnEvent = procedure(Sender: TObject; port: integer; Data: TEventData) of object;

  TTrackOutputs = class(TComponent)
  private
    fActive: boolean;
    fMidiData: TMidiData;
    fDefaultOutputPort: integer;
    fOnEvent: TOnEvent;
    function GetNoTracks: integer;
    function GetTrackOutput(index: integer): TTrackOutput;
    procedure SetMidiData(Value: TMidiData);
    procedure SetActive(Value: boolean);
    procedure Open;
    procedure Close;
  protected
  public
    procedure AllNotesOff;
    procedure ResetAllControllers;
    property NoTracks: integer read GetNoTracks;
    property TrackOutput[index: integer]: TTrackOutput read GetTrackOutput; default;
    function GetTrackPortID(trackNo: integer): integer;
    procedure OpenTrack(Data: TMidiTrackStream; prtID: integer);
    procedure ResetEventIndexes;
    procedure SetPatchForPosition;
    function IndexOf(Data: TMidiTrackStream): integer;
  published
    property DefaultOutputPort: integer read fDefaultOutputPort write fDefaultOutputPort;
    property MidiData: TMidiData read fMidiData write SetMidiData;
    property Active: boolean read fActive write SetActive;
    property OnEvent: TOnEvent read fOnEvent write fOnEvent;
  end;

implementation

function TTrackOutputs.GetNoTracks: integer;
begin
  Result := ComponentCount;
end;

function TTrackOutputs.GetTrackOutput(index: integer): TTrackOutput;
begin
  if index < ComponentCount then
    Result := TTrackOutput(Components[index])
  else
    Result := nil;
end;

procedure TTrackOutputs.SetMidiData(Value: TMidiData);
var
  OldActive: boolean;
begin
  if Value <> fMidiData then
  begin
    OldActive := Active;
    Active := False;
    fMidiData := Value;
    Active := OldActive;
  end;
end;

procedure TTrackOutputs.SetActive(Value: boolean);
begin
  if Value = fActive then
    exit;

  fActive := Value;

  if fActive then
    Open
  else
    Close;
end;

procedure TTrackOutputs.Open;
var
  i: integer;
begin
    for i := 0 to fMidiData.NoTracks - 1 do
      OpenTrack(fMidiData.Tracks[i], DefaultOutputPort);
end;

procedure TTrackOutputs.OpenTrack(Data: TMidiTrackStream; prtID: integer);
begin
  with TTrackOutput.Create(self) do
    Open(Data, prtID);
end;

procedure TTrackOutputs.Close;
begin
  while ComponentCount > 0 do
    Components[0].Free;
end;

procedure TTrackOutputs.AllNotesOff;
var
  i: integer;
begin
  for i := 0 to NoTracks - 1 do
    TrackOutput[i].Port.AllNotesOff;
end;

procedure TTrackOutputs.ResetAllControllers;
var
  i: integer;
begin
  for i := 0 to NoTracks - 1 do
    TrackOutput[i].Port.ResetControllers;
end;

function TTrackOutputs.GetTrackPortID(trackNo: integer): integer;
begin
  if trackNo < NoTracks then
    Result := TrackOutput[trackNo].Port.PortId
  else
    Result := DefaultOutputPort;
end;

procedure TTrackOutputs.ResetEventIndexes;
var
  i: integer;
begin
  for i := 0 to NoTracks - 1 do
    TrackOutput[i].EventIndex := 0;
end;

procedure TTrackOutputs.SetPatchForPosition;
var
  i: integer;
begin
  for i := 0 to NoTracks - 1 do
    TrackOutput[i].SetPatchForPosition;
end;

function TTrackOutputs.IndexOf(Data: TMidiTrackStream): integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to NoTracks - 1 do
    if TrackOutput[i].TrackData = Data then
    begin
      Result := i;
      break;
    end;
end;

destructor TTrackOutput.Destroy;
begin
  Close;
  inherited;
end;

procedure TTrackOutput.Open(Data: TMidiTrackStream; OutputPortNo: integer);
begin
  fPort := TMidiOutputPort.Create(self);
  fTrackData := Data;
  fEventIndex := 0;
  fPort.PortID := OutputPortNo;
  fPort.Active := True;
end;

procedure TTrackOutput.Close;
begin
  fPort.Free;
end;

procedure TTrackOutput.SetPatchForPosition;
begin
  with fTrackData do
    fPort.PatchChange(bank, patch, channel);
end;

procedure TTrackOutput.SetPortID(Value: integer);
begin
  fPort.PortID := Value;
end;

function TTrackOutput.GetPortID: integer;
begin
  Result := fPort.PortID;
end;

procedure TTrackOutput.SetMute(const Value: boolean);
begin
  if fMute <> Value then
  begin
    if Value then
      Port.AllNotesOff;
    fMute := Value;
  end;
end;

end.
