{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}
 unit MIDI_Msgs;

interface
  uses Windows,Controls, Classes,forms;


const
  MsgInfo=0;
  MsgError=1;
  MsgTimeEvent=2;
type

 TMIDIEvent = procedure (Sender : TObject;Text:string;aType:integer) of object;
 TMIDIStatusEvent = procedure (Sender : TObject; const Status:integer) of object;

 Procedure MIDI_SendMessage(Sender : TObject;Text:string;aType:integer=0);

var _VarMIDIEvent       :TMIDIEvent;
    _VarMIDIStatusEvent :TMIDIStatusEvent;
    _VarMainForm        :TForm;

implementation

Procedure MIDI_SendMessage(Sender : TObject;Text:string;aType:integer=0);
 begin
    if Assigned(_VarMIDIEvent) then _VarMIDIEvent(Sender,text,aType);
 end;

end.
 
