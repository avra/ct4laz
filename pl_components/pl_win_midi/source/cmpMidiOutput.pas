{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}
unit cmpMidiOutput;


interface

uses
  Windows, Messages, SysUtils, Classes, Forms, MMSystem, cmpInstrument, unitMidiGlobals;

const

  //-------------------------------------------------------------------------
  // Default values for controllers...
  ControllerDefaults: array [TController] of integer = (
    0, 0, 0, 0, 0, 0, 0, 127 {Volume}, 0, 0, 64 {Pan }, 127 {Expression}, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    );

type

  TMidiOutputPort = class;

  TPhysicalOutputPort = class
  private
    Handle: HMIDIOUT;
    PortID: integer;
    PortNo: integer;       // Index in port list
    UserList: TList;
    CurrentBank: array [TChannel] of TBankNo;
    CurrentPatch: array [TChannel] of TPatchNo;
    ControllerArray: array [TChannel, TController] of integer;
    procedure ResetControllers;
    procedure PatchChange(bank: TBankNo; patch: TPatchNo; channel: TChannel);

    constructor Create(pID: integer; user: TMidiOutputPort);
    procedure RemoveUser(user: TMidiOutputPort);
    procedure AddUser(user: TMidiOutputPort);
  public
    destructor Destroy; override;
  end;


  TChannelRec = record
    fVolume: integer;
    fInstrument: integer;
    fAfterTouch: integer;
    fPitchWheel: integer;
    fModulation: integer;
    fBreath: integer;
    fFoot: integer;
    fPortamentoTime: integer;
    fBalance: integer;
    fPan: integer;

    fExpression: integer;
    fEffect1: integer;
    fEffect2: integer;

    fSustain: boolean;
    fPortamento: boolean;
    fSustenuto: boolean;
    fSoftPetal: boolean;
    fLegatoFootSwitch: boolean;
    fHold2: boolean;
  end;

  TMidiOutputPort = class(TComponent)
  private
    fPortID: integer;
    fPhysicalPort: TPhysicalOutputPort;
    fSysexHeaders: TList;
    userNo: integer;
    NoteArray: array [TChannel, TNote] of integer;
    fSysexLatency: integer;
    fCurChannel: integer;
    procedure SetPortId(Value: integer);
    procedure SetActive(Value: boolean);
    function GetActive: boolean;
    function GetHandle: HMidiOut;
    procedure TidySysexHeaders;
  protected
    FChannels: array [0..15] of TChannelRec;
    function GetChannel(Index: integer): TChannelRec;
    procedure PutChannel(Index: integer; const Ch: TChannelRec);

    procedure midiOutCallback(uMsg: UINT; dw1, dw2: longint); virtual;
    procedure ClearChannelRec(const ch: integer);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure OutSysex(Data: PChar; len: word);
    procedure OutEvent(const Event: TEventData);

    function GetPatch(bank: TBankNo; Patch: TPatchNo): TPatch;
    procedure PatchChange(bank: TBankNo; patch: TPatchNo; channel: TChannel);
    procedure NoteOn(channel, note, velocity: integer);
    procedure NoteOff(channel, note, velocity: integer);
    procedure WaitForSysex;
    property Handle: HMIDIOUT read GetHandle;
    procedure SetCurChannel(const ch: integer);
    procedure AllNotesOff;
    procedure ResetControllers;
    //..................................................
    procedure ChannelAllSoundsOFF(const Ch: integer);
    procedure ChannelAllNotesOFF(const Ch: integer);
    procedure ChannelResetAllControlers(const Ch: integer);
    procedure ChannelSetAfterTouch(const Ch, val: integer);
    procedure ChannelSetPitchWheel(const Ch, val: integer);
    procedure ChannelSetInstrument(const Ch, val: integer);
    procedure ChannelSetVolume(const Ch, val: integer);
    procedure ChannelSetModulation(const Ch, val: integer);
    procedure ChannelSetBreath(const Ch, val: integer);
    procedure ChannelSetFoot(const Ch, val: integer);
    procedure ChannelSetPortamentoTime(const Ch, val: integer);
    procedure ChannelSetBalance(const Ch, val: integer);
    procedure ChannelSetPan(const Ch, val: integer);
    procedure ChannelSetExpression(const Ch, val: integer);
    procedure ChannelSetEffect1(const Ch, val: integer);
    procedure ChannelSetEffect2(const Ch, val: integer);

    procedure ChannelSetSustain(const Ch: integer; const val: boolean);
    procedure ChannelSetPortamento(const Ch: integer; const val: boolean);
    procedure ChannelSetSustenuto(const Ch: integer; const val: boolean);
    procedure ChannelSetSoftPetal(const Ch: integer; const val: boolean);
    procedure ChannelSetLegatoFootSwitch(const Ch: integer; const val: boolean);
    procedure ChannelSetHold2(const Ch: integer; const val: boolean);
    //...................................................
    procedure CurChanSetAllSoundsOFF;
    procedure CurChanSetAllNotesOFF;
    procedure CurChanResetAllControlers;

    procedure SetAfterTouch(const val: integer);
    procedure SetPitchWheel(const val: integer);
    procedure SetInstrument(const val: integer);
    procedure SetVolume(const val: integer);
    procedure SetModulation(const val: integer);
    procedure SetBreath(const val: integer);
    procedure SetFoot(const val: integer);
    procedure SetPortamentoTime(const val: integer);
    procedure SetBalance(const val: integer);
    procedure SetPan(const val: integer);
    procedure SetExpression(const val: integer);
    procedure SetEffect1(const val: integer);
    procedure SetEffect2(const val: integer);
    procedure SetSustain(const val: boolean);
    procedure SetPortamento(const val: boolean);
    procedure SetSustenuto(const val: boolean);
    procedure SetSoftPetal(const val: boolean);
    procedure SetLegatoFootSwitch(const val: boolean);
    procedure SetHold2(const val: boolean);
    //....................................................
    procedure SetNoteON(const note: integer; velocity: integer);
    procedure SetNoteOFF(const note: integer; velocity: integer);
    property CurChannel: integer read fCurChannel write SetCurChannel;
    property Channel[Index: integer]: TChannelRec read GetChannel write PutChannel; default;
  published
    property PortId: integer read fPortID write SetPortId;
    property Active: boolean read GetActive write SetActive;
    property SysexLatency: integer read fSysexLatency write fSysexLatency default 50;
  end;

  EMidiOutputPort = class(Exception);

procedure SetOutputPrtInstrument(id: integer; instrument: TInstrument);

implementation
//=========================================================================
var
  PortList: TList;
  instrumentCache: array [0..7] of TInstrument;

procedure SetOutputPrtInstrument(id: integer; instrument: TInstrument);
begin
  instrumentCache[id] := instrument;
end;

procedure MidiOutCallback(handle: HMIDIOUT; uMsg: UINT; dwUser, dw1, dw2: DWORD); stdcall;
var
  portNo, userNo: word;
  port: TPhysicalOutputPort;
  instance: TMidiOutputPort;
begin
  portNo := LoWord(dwUser);
  userNo := HiWord(dwUser);
  port := TPhysicalOutputPort(PortList.Items[portNo]);
  instance := port.UserList.Items[userNo];
  instance.MidiOutCallback(uMsg, dw1, dw2);
end;

//======================= TPhysicalOutputPort ============================================

constructor TPhysicalOutputPort.Create(pID: integer; user: TMidiOutputPort);
var
  chan: TChannel;
begin
  inherited Create;
  portID := pID;
  if not Assigned(PortList) then
    PortList := TList.Create;
  PortNo := PortList.Count;
  PortList.Add(self);
  UserList := TList.Create;
  AddUser(user);

  for chan := Low(TChannel) to High(TChannel) do
  begin
    Move(ControllerDefaults[0], ControllerArray[chan, 0], sizeof(integer) * High(TController));
    CurrentPatch[chan] := 0;
    CurrentBank[chan] := 127;
  end;

  if midiOutOpen(@Handle, portID, DWORD_PTR(@MidiOutCallback), MAKELONG(DWORD(PortNo), DWORD(user.userNo)),
    CALLBACK_FUNCTION) <> MMSYSERR_NOERROR then
    raise EMidiOutputPort.Create('Unable to open port');
end;

destructor TPhysicalOutputPort.Destroy;
var
  i: integer;
  keepList: boolean;
begin
  midiOutReset(Handle);
  Application.ProcessMessages;
  midiOutClose(Handle);
  Application.ProcessMessages;
  keepList := False;

  PortList[PortNo] := nil;

  for i := 0 to PortList.Count - 1 do
    if PortList.items[i] <> nil then
    begin
      keepList := True;
      break;
    end;

  if not KeepList then
  begin
    PortList.Free;
    PortList := nil;
  end;
  UserList.Free;
  inherited;
end;

//===================== TMidiOutputPort ====================================

constructor TMidiOutputPort.Create(AOwner: TComponent);
var
  i: integer;
begin
  inherited Create(AOwner);
  fSysexHeaders := TList.Create;
  fSysexLatency := 50;
  fCurChannel := 0;

  //---- reset channels rec -------------------
  for I := 0 to 15 do
    ClearChannelRec(i);
end;

destructor TMidiOutputPort.Destroy;
begin
  Active := False;
  WaitForSysex;
  fSysexHeaders.Free;

  inherited;
end;

procedure TMidiOutputPort.ClearChannelRec(const ch: integer);
begin
  fchannels[Ch].fVolume := 127;
  fchannels[Ch].fInstrument := 0;
  fchannels[Ch].fAfterTouch := 0;
  fchannels[Ch].fPitchWheel := 0;
  fchannels[Ch].fModulation := 0;
  fchannels[Ch].fBreath := 0;
  fchannels[Ch].fFoot := 0;
  fchannels[Ch].fPortamentoTime := 0;
  fchannels[Ch].fBalance := 0;
  fchannels[Ch].fPan := 64;
  fchannels[Ch].fExpression := 127;
  fchannels[Ch].fEffect1 := 0;
  fchannels[Ch].fEffect2 := 0;
  fchannels[Ch].fSustain := False;
  fchannels[Ch].fPortamento := False;
  fchannels[Ch].fSustenuto := False;
  fchannels[Ch].fSoftPetal := False;
  fchannels[Ch].fLegatoFootSwitch := False;
  fchannels[Ch].fHold2 := False;

end;

procedure TPhysicalOutputPort.RemoveUser(user: TMidiOutputPort);
var
  stillInUse: boolean;
  i: integer;
begin
  stillInUse := False;
  for i := 0 to UserList.Count - 1 do
    if (UserList.items[i] <> nil) and (i <> user.userNo) then
    begin
      stillInUse := True;
      break;
    end;

  if not stillInUse then
  begin
    if user.userNo <> 0 then
      UserList.Items[0] := userList.Items[user.userNo];
    Free;
  end
  else
    UserList.Items[user.userNo] := nil;
end;


procedure TPhysicalOutputPort.AddUser(user: TMidiOutputPort);
var
  slotNo, i: integer;
begin
  slotNo := -1;
  for i := 0 to UserList.Count - 1 do
    if not Assigned(userList.Items[i]) then
    begin
      slotNo := i;
      break;
    end;

  if slotNo <> -1 then
  begin
    UserList.Items[slotNo] := user;
    user.userNo := slotNo;
  end
  else
  begin
    user.userNo := UserList.Count;
    UserList.Add(user);
  end;
end;

procedure TMidiOutputPort.SetPortId(Value: integer);
var
  oldActive: boolean;
begin
  if Value <> fPortID then
  begin
    oldActive := Active;
    Active := False;
    fPortID := Value;
    Active := oldActive;
  end;
end;

function TMidiOutputPort.GetActive: boolean;
begin
  Result := Assigned(fPhysicalPort);
end;

procedure TMidiOutputPort.SetActive(Value: boolean);
var
  i: integer;
begin
  if Value = Active then exit;

  case Value of
    True:
    begin
      // Try to find the required physical output port in the port list
      if Assigned(PortList) then
        for i := 0 to PortList.Count - 1 do
          if Assigned(PortList.Items[i]) then
            with TPhysicalOutputPort(PortList.Items[i]) do
              if PortID = self.PortID then
              begin
                fPhysicalPort := TPhysicalOutputPort(PortList.Items[i]);
                break;
              end;


      // Create the physical port of not found (this adds it to the port list)
      if not Assigned(fPhysicalPort) then
        fPhysicalPort := TPhysicalOutputPort.Create(fPortID, self)
      else
        fPhysicalPort.AddUser(self);

      // Add ourself to the port's user list.  If we're the last user of the
      // port, free the port.
    end;
    False:
    begin
      AllNotesOff;
      ResetControllers;
      // Remove ourself from the port's user list.
      fPhysicalPort.RemoveUser(self);
      fPhysicalPort := nil;
    end
  end;

end;

function TMidiOutputPort.GetHandle: HMidiOut;
begin
  if Active then
    Result := fPhysicalPort.Handle
  else
    Result := 0;
end;

procedure TMidiOutputPort.midiOutCallback(uMsg: UINT; dw1, dw2: longint);
begin
end;

procedure TMidiOutputPort.OutEvent(const Event: TEventData);
var
  channel: byte;
begin
  if Assigned(fPhysicalPort) then
    with Event do
    begin
      if status < midiSysex then
      begin
        midiOutShortMsg(handle, PInteger(@status)^);

        channel := status and midiChannelMask;

        case status and midiStatusMask of
          midiNoteOn:        // Note on  (but it's a note-off if the
            // velocity's 0
          begin
            if b3 = 0 then  // It's a note-off after all...
            begin
              if NoteArray[channel, b2] > 0 then
                Dec(NoteArray[channel, b2]);
            end
            else
              Inc(NoteArray[channel, b2]);
          end;

          midiNoteOff: if NoteArray[channel, b2] > 0 then
              Dec(NoteArray[channel, b2]);

          midiController: fPhysicalPort.ControllerArray[channel, b2] := b3;

          midiProgramChange: fPhysicalPort.CurrentPatch[channel] := b2
        end;
      end;
    end;
end;

procedure TMidiOutputPort.AllNotesOff;
var
  channel: TChannel;
  Note: TNote;
  Event: TEventData;

begin
  for channel := Low(TChannel) to High(TChannel) do
  begin
    Event.Status := midiNoteOff + channel;
    Event.b3 := 0;
    for Note := Low(TNote) to High(TNote) do
    begin
      event.b2 := note;
      while NoteArray[channel, note] > 0 do
        OutEvent(event);
    end;
  end;

  if Assigned(fPhysicalPort) then
    for channel := Low(TChannel) to High(TChannel) do
      if fPhysicalPort.ControllerArray[channel, 64] <> ControllerDefaults[64] then
      begin
        Event.b3 := ControllerDefaults[64];
        Event.Status := midiController + channel;  // Reset sostenuto
        event.b2 := 64;
        OutEvent(event);
      end;
end;

procedure TPhysicalOutputPort.ResetControllers;
var
  channel: TChannel;
  Controller: TController;
  Event: TEventData;
begin
  for channel := Low(TChannel) to High(TChannel) do
  begin
    Event.Status := midiController + channel;
    for Controller := Low(TController) to High(TController) do
      if ControllerArray[channel, controller] <> ControllerDefaults[controller] then
      begin
        event.b2 := controller;
        event.b3 := ControllerDefaults[controller];
        midiOutShortMsg(handle, PInteger(@event.status)^);
        ControllerArray[channel, controller] := ControllerDefaults[controller];
      end;
  end;
end;

procedure TMidiOutputPort.ResetControllers;
begin
  if not Assigned(fPhysicalPort) then exit;

  if Active then
    fPhysicalPort.ResetControllers;
end;

function TMidiOutputPort.GetPatch(bank: TBankNo; Patch: TPatchNo): TPatch;
var
  i: integer;
begin
  with instrumentCache[PortID] do
    for i := 0 to ComponentCount - 1 do
      with Components[i] as TPatch do
        if (BankNo = bank) and (PatchNo = patch) then
        begin
          Result := TPatch(Components[i]);
          exit;
        end;
  Result := nil;
end;

procedure TPhysicalOutputPort.PatchChange(bank: TBankNo; patch: TPatchNo; channel: TChannel);
var
  event: TEventData;
  bankChanged: boolean;
begin
  if (bank <> CurrentBank[channel]) and Assigned(InstrumentCache[PortID]) then
  begin
    bankChanged := True;
    CurrentBank[channel] := bank;
    with instrumentCache[PortID].fBankChangeRec do
      case bcType of
        bcControl:
        begin
          Event.status := midiController + Channel;
          Event.b2 := Control;
          Event.b3 := bank;
          midiOutShortMsg(handle, PInteger(@event.status)^);
        end;
        bcProgramChange:
          if bank < 8 then           // Only support 8 banks as program changes - TG77, etc.
          begin
            Event.status := midiProgramChange + Channel;
            Event.b2 := programOffsets[bank];
            Event.b3 := 0;
            midiOutShortMsg(handle, PInteger(@event.status)^);
          end
      end;
  end
  else
    bankChanged := False;

  if bankChanged or (patch <> CurrentPatch[channel]) then
  begin
    Event.status := midiProgramChange + Channel;
    Event.b2 := Patch;
    Event.b3 := 0;
    CurrentPatch[channel] := patch;
    midiOutShortMsg(handle, PInteger(@event.status)^);
  end;
end;

procedure TMidiOutputPort.NoteOn(channel, note, velocity: integer);
var
  event: TEventData;
begin
  Event.status := midiNoteOn + Channel;
  Event.b2 := note;
  Event.b3 := velocity;
  OutEvent(event);
end;

procedure TMidiOutputPort.NoteOff(channel, note, velocity: integer);
var
  event: TEventData;
begin
  Event.status := midiNoteOff + Channel;
  Event.b2 := note;
  Event.b3 := velocity;
  OutEvent(event);
end;

procedure TMidiOutputPort.PatchChange(bank: TBankNo; patch: TPatchNo; channel: TChannel);
begin
  EXIT; //=== ct9999 ======  Give ERROR on Win64 ???
  
  if not Assigned(fPhysicalPort) then exit;

  if Active then
    fPhysicalPort.PatchChange(bank, patch, channel);
end;

procedure CloseAllPhysicalPorts;
var
  i: integer;
begin
  if Assigned(PortList) then
    for i := 0 to PortList.Count - 1 do
      if PortList.items[i] <> nil then
        with TPhysicalOutputPort(PortList.items[i]) do
          Free;
end;

procedure TMidiOutputPort.OutSysex(Data: PChar; len: word);
var
  hdr: PMidiHdr;
begin
  TidySysexHeaders;
  GetMem(hdr, sizeof(TMidiHdr));
  ZeroMemory(hdr, sizeof(TMidiHdr));
  GetMem(hdr^.lpData, len);
  hdr^.dwBufferLength := len;
  Move(Data^, hdr^.lpData^, len);
  fSysexHeaders.Add(hdr);

  midiOutPrepareHeader(handle, hdr, sizeof(hdr^));
  midiOutLongMsg(handle, hdr, sizeof(hdr^));
end;

procedure TMidiOutputPort.TidySysexHeaders;
var
  i: integer;
  hdr: PMidiHdr;
begin
  i := 0;
  while i < fSysexHeaders.Count do
  begin
    hdr := PMidiHdr(fSysexHeaders[i]);
    if (hdr^.dwFlags and MHDR_DONE) = MHDR_DONE then
    begin
      MidiOutUnprepareHeader(handle, hdr, sizeof(hdr^));
      FreeMem(hdr^.lpData);
      fSysexHeaders.Delete(i);
    end
    else
      Inc(i);
  end;
end;

procedure TMidiOutputPort.WaitForSysex;
begin
  repeat
    TidySysexHeaders;
    Sleep(fSysexLatency);
  until fSysexHeaders.Count = 0;
end;

//============================================================================
function TMidiOutputPort.GetChannel(Index: integer): TChannelRec;
var
  i: integer;
begin
  i := Index;
  if Index > 15 then
    i := 15;
  if Index < 0 then
    i := 0;
  Result := FChannels[i];
end;

procedure TMidiOutputPort.PutChannel(Index: integer; const Ch: TChannelRec);
var
  i: integer;
begin
  i := Index;
  if Index > 15 then
    i := 15;
  if Index < 0 then
    i := 0;
  FChannels[i] := Ch;
end;

procedure TMidiOutputPort.SetCurChannel(const ch: integer);
begin
  fcurChannel := ch;
end;

procedure TMidiOutputPort.SetNoteON(const note: integer; velocity: integer);
begin
  NoteOn(fCurChannel, note, velocity);
end;

procedure TMidiOutputPort.SetNoteOFF(const note: integer; velocity: integer);
begin
  NoteOFF(fCurChannel, note, velocity);
end;

//====================================================================
//====================================================================

//........................ChannelAllSoundsOFF.......................
procedure TMidiOutputPort.ChannelAllSoundsOFF(const Ch: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_AllSoundsOff;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.CurChanSetAllSoundsOFF;
begin
  ChannelAllSoundsOFF(fcurchannel);
end;

//........................ChannelAllNotesOFF.......................
procedure TMidiOutputPort.ChannelAllNotesOFF(const Ch: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_AllNotesOff;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.CurChanSetAllNotesOFF;
begin
  ChannelAllNotesOFF(fcurchannel);
end;

//........................ChannelResetAllControlers......................
procedure TMidiOutputPort.ChannelResetAllControlers(const Ch: integer);
var
  xEvent: TEventData;
  i: integer;
begin
  if not Assigned(fPhysicalPort) then exit;

  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_ResetAllControlers;
  midiOutShortMsg(handle, PtrInt(xevent.status));

  xEvent.Status := midiController + Ch;
  for i := Low(TController) to High(TController) do
  begin
    xevent.b2 := i;
    xevent.b3 := ControllerDefaults[i];
    midiOutShortMsg(handle, PtrInt(xevent.status));
  end;

  SetInstrument(0);
  SetAfterTouch(0);
  SetPitchWheel(0);
  ClearChannelRec(ch);
end;

procedure TMidiOutputPort.CurChanResetAllControlers;
begin
  ChannelResetAllControlers(fcurchannel);
end;

//........................... Instrument ..............................
procedure TMidiOutputPort.ChannelSetInstrument(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fInstrument := val;
  xEvent.status := seINSTRUMENT + Ch;
  xEvent.b2 := val;

  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetInstrument(const val: integer);
begin
  ChannelSetInstrument(fcurchannel, val);
end;

//........................... AfterTouch ..............................
procedure TMidiOutputPort.ChannelSetAfterTouch(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fAfterTouch := val;
  xEvent.status := seAFTERTOUCH + Ch;
  xEvent.b2 := val;

  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetAfterTouch(const val: integer);
begin
  ChannelSetAfterTouch(fcurchannel, val);
end;

//........................... PitchWheel ..............................
procedure TMidiOutputPort.ChannelSetPitchWheel(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fPitchWheel := val;
  xEvent.status := sePITCHWHEEL + Ch;
  xEvent.b2 := val;
  xEvent.b3 := val;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetPitchWheel(const val: integer);
begin
  ChannelSetPitchWheel(fcurchannel, val);
end;

//............................ Volume .............................
procedure TMidiOutputPort.ChannelSetVolume(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fVolume := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Volume;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetVolume(const val: integer);
begin
  ChannelSetVolume(fcurchannel, val);
end;

//............................ Modulation .............................
procedure TMidiOutputPort.ChannelSetModulation(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fModulation := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Modulation;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetModulation(const val: integer);
begin
  ChannelSetModulation(fcurchannel, val);
end;

//............................ Breath .............................
procedure TMidiOutputPort.ChannelSetBreath(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fBreath := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Breath;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetBreath(const val: integer);
begin
  ChannelSetBreath(fcurchannel, val);
end;

//............................ Foot .............................
procedure TMidiOutputPort.ChannelSetFoot(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fFoot := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Foot;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetFoot(const val: integer);
begin
  ChannelSetFoot(fcurchannel, val);
end;

//............................ PortamentoTime .............................
procedure TMidiOutputPort.ChannelSetPortamentoTime(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fPortamentoTime := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_PortamentoTime;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetPortamentoTime(const val: integer);
begin
  ChannelSetPortamentoTime(fcurchannel, val);
end;

//............................ Balance .............................
procedure TMidiOutputPort.ChannelSetBalance(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fBalance := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Balance;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetBalance(const val: integer);
begin
  ChannelSetBalance(fcurchannel, val);
end;

//............................ Pan .............................
procedure TMidiOutputPort.ChannelSetPan(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fPan := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Pan;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetPan(const val: integer);
begin
  ChannelSetPan(fcurchannel, val);
end;

//............................ Expression .............................
procedure TMidiOutputPort.ChannelSetExpression(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fExpression := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Expression;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetExpression(const val: integer);
begin
  ChannelSetExpression(fcurchannel, val);
end;

//............................ Effect1 .............................
procedure TMidiOutputPort.ChannelSetEffect1(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fEffect1 := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Effect1;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetEffect1(const val: integer);
begin
  ChannelSetEffect1(fcurchannel, val);
end;

//............................ Effect2 .............................
procedure TMidiOutputPort.ChannelSetEffect2(const Ch, val: integer);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fEffect2 := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Effect2;
  xEvent.b3 := val;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetEffect2(const val: integer);
begin
  ChannelSetEffect2(fcurchannel, val);
end;

//............................ Sustain .............................
procedure TMidiOutputPort.ChannelSetSustain(const Ch: integer; const val: boolean);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fSustain := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Sustain;

  if val then
    xEvent.b3 := 127
  else
    xEvent.b3 := 0;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetSustain(const val: boolean);
begin
  ChannelSetSustain(fcurchannel, val);
end;

//............................ Portamento .............................
procedure TMidiOutputPort.ChannelSetPortamento(const Ch: integer; const val: boolean);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fPortamento := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Portamento;

  if val then
    xEvent.b3 := 127
  else
    xEvent.b3 := 0;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetPortamento(const val: boolean);
begin
  ChannelSetPortamento(fcurchannel, val);
end;

//............................ Sustenuto .............................
procedure TMidiOutputPort.ChannelSetSustenuto(const Ch: integer; const val: boolean);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fSustenuto := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Sustenuto;

  if val then
    xEvent.b3 := 127
  else
    xEvent.b3 := 0;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetSustenuto(const val: boolean);
begin
  ChannelSetSustenuto(fcurchannel, val);
end;

//............................ SoftPetal .............................
procedure TMidiOutputPort.ChannelSetSoftPetal(const Ch: integer; const val: boolean);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fSoftPetal := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_SoftPetal;

  if val then
    xEvent.b3 := 127
  else
    xEvent.b3 := 0;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetSoftPetal(const val: boolean);
begin
  ChannelSetSoftPetal(fcurchannel, val);
end;

//............................ LegatoFootSwitch .............................
procedure TMidiOutputPort.ChannelSetLegatoFootSwitch(const Ch: integer; const val: boolean);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fLegatoFootSwitch := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_LegatoFootSwitch;

  if val then
    xEvent.b3 := 127
  else
    xEvent.b3 := 0;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetLegatoFootSwitch(const val: boolean);
begin
  ChannelSetLegatoFootSwitch(fcurchannel, val);
end;

//............................ Hold2 .............................
procedure TMidiOutputPort.ChannelSetHold2(const Ch: integer; const val: boolean);
var
  xEvent: TEventData;
begin
  if not Assigned(fPhysicalPort) then exit;

  fchannels[ch].fHold2 := val;
  xEvent.status := seCONTROLMODE + Ch;
  xEvent.b2 := teCONTROLMODE_Hold2;

  if val then
    xEvent.b3 := 127
  else
    xEvent.b3 := 0;

  fPhysicalPort.ControllerArray[ch, xEvent.b2] := xEvent.b3;
  midiOutShortMsg(handle, PtrInt(xevent.status));
end;

procedure TMidiOutputPort.SetHold2(const val: boolean);
begin
  ChannelSetHold2(fcurchannel, val);
end;



//=======================================================================
initialization

finalization
  CloseAllPhysicalPorts;
end.
