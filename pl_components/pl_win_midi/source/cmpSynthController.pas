{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}
 unit cmpSynthController;

interface

uses Windows, Classes, Messages, ExtCtrls, Forms, SysUtils, cmpMidiOutput, cmpMidiInput, unitMIDIGlobals;

type

TInputMessage = class
  data : PChar;
  len : word;
  valid : Boolean;
  constructor Create (AData : PChar; ALen : word);
  destructor Destroy; override;
end;

TOnSysexMessage = procedure (Sender : TObject; const Msg : TInputMessage) of object;

TSynthController = class (TMidiOutputPort)
private
  fInput : TMidiInput;
  fActiveSenseTimer : TTimer;
  fWindowHandle : HWND;
  fMessageQueue : TList;
  fActiveSensingCount : Integer;
  fOnPowerOn: TNotifyEvent;
  fOnPowerOff: TNotifyEvent;
  fOnSysexMessage: TOnSysexMessage;
  fMessageQueueCount : Integer;
  procedure OnSysexData (data : PChar; len : word);
  procedure OnStepData (const data : TEventData);
  procedure OnSystemMessage (const data : TEventData);
  procedure OnActiveSenseTimer (Sender : TObject);
  procedure WndProc (var Msg : TMessage);
  function  GetActive: Boolean;
  procedure SetActive(const Value: Boolean);
  procedure DeactivateInput;
  procedure ActivateInput;
protected
  procedure HandleSysexMessage (msg : TInputMessage); virtual;
  procedure HandleStepData (const data : TEventData); virtual;
  procedure HandlePowerOn; virtual;
public
  constructor Create (AOwner : TComponent); override;
  destructor Destroy; override;
  procedure Reset; virtual;
  procedure Loaded; override;
published
  property Active : Boolean read GetActive Write SetActive;
  property OnPowerOn : TNotifyEvent read fOnPowerOn write fOnPowerOn;
  property OnPowerOff : TNotifyEvent read fOnPowerOff write fOnPowerOff;
  property OnSysexMessage : TOnSysexMessage read fOnSysexMessage Write fOnSysexMessage;
end;

implementation

{ TSynthController }

procedure TSynthController.ActivateInput;
begin
  if not (csDesigning in ComponentState) then
  begin
    fMessageQueue := TList.Create;
    fWindowHandle := classes.AllocateHWnd (WndProc);
    fInput := TMidiInput.Create (Nil);
    fInput.StepMode := True;
    fInput.OnStepData := OnStepData;
    fInput.OnSysexData := OnSysexData;
    fInput.OnSystemMessage := OnSystemMessage;
    fInput.OpenPorts [0] := True;

    fActiveSenseTimer := TTimer.Create (nil);
    fActiveSenseTimer.OnTimer := OnActiveSenseTimer;
    fActiveSenseTimer.Interval := 500
  end
end;

constructor TSynthController.Create(AOwner: TComponent);
begin
  inherited Create (AOwner);
end;

procedure TSynthController.DeactivateInput;
var
  i : Integer;
begin
  fInput.Free;

  if fWindowHandle <> 0 then
    classes.DeallocateHWnd (fWindowHandle);

  if Assigned (fMessageQueue) then
    for i := 0 to fMessageQueue.Count - 1 do
      TInputMessage (fMessageQueue [i]).Free;

  fMessageQueue.Free;
  fInput := nil;
  fMessageQueue := nil;
end;

destructor TSynthController.Destroy;
begin
  DeactivateInput;
  inherited;
end;

function TSynthController.GetActive: Boolean;
begin
  Result := inherited Active
end;

procedure TSynthController.HandlePowerOn;
begin

end;

procedure TSynthController.HandleStepData(const data: TEventData);
begin

end;

procedure TSynthController.HandleSysexMessage(msg: TInputMessage);
begin
end;

procedure TSynthController.Loaded;
begin
  inherited;

  if Active then ActivateInput

end;

procedure TSynthController.OnActiveSenseTimer(Sender: TObject);
begin
  if fActiveSensingCount <= 1 then
  begin
    if fActiveSensingCount = 1 then
      if Assigned (fOnPowerOff) then
        OnPowerOff (Self);
    fActiveSensingCount := 0;
  end
  else
    fActiveSensingCount := 1
end;

procedure TSynthController.OnStepData(const data: TEventData);
begin
  HandleStepData (data);
end;

procedure TSynthController.OnSysexData(data: PChar; len: word);
begin
  Inc (fMessageQueueCount);
  fMessageQueue.Add (TInputMessage.Create (data, len));
  PostMessage (fWindowHandle, WM_USER + $200, 0, 0);
end;

procedure TSynthController.OnSystemMessage(const data: TEventData);
begin
  if data.status = $fe then
  begin
    Inc (fActiveSensingCount, 2);
    if fActiveSensingCount = 2 then
      PostMessage (fWindowHandle, WM_USER + $201, 0, 0)
  end
end;

procedure TSynthController.Reset;
begin

end;

procedure TSynthController.SetActive(const Value: Boolean);
begin
  inherited Active := Value;

  if value then
    ActivateInput
  else
    DeactivateInput
end;

procedure TSynthController.WndProc(var Msg: TMessage);
var
  M : TInputMessage;
begin
  if Msg.Msg = WM_USER + $200 then
  begin
    while fMessageQueue.Count > 0 do
    begin
      M := TInputMessage (fMessageQueue [0]);
      M.Valid := True;

      HandleSysexMessage (M);
      if Assigned (fOnSysexMessage) and M.Valid then
        OnSysexMessage (Self, M);

      fMessageQueue.Delete (0)
    end
  end
  else
    if Msg.Msg = WM_USER + $201 then
    begin
      HandlePowerOn;
      if Assigned (fOnPowerOn) then
        OnPowerOn (Self)
    end
end;

{ TInputMessage }

constructor TInputMessage.Create(AData: PChar; ALen: word);
begin
  len := ALen;
  GetMem (data, len);
  Move (AData^, data^, len);
end;

destructor TInputMessage.Destroy;
begin
  FreeMem (data);
  inherited Destroy
end;

end.
