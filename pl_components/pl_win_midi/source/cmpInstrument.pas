{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}

 unit cmpInstrument;

interface

uses
  Windows, Messages, SysUtils, Classes, unitMidiGlobals;

type
  TBankChangeType = (bcNone, bcControl, bcProgramChange);
  TBankChangeRec = record
  case bcType : TBankChangeType of
    bcControl : (control : TController);
    bcProgramChange : (programOffsets : array [0..7] of byte)
  end;

  TInstrument = class(TComponent)
  private
    fInstrumentName : string;
  protected
    procedure DefineProperties (Filer : TFiler); override;
    procedure GetChildren (Proc : TGetChildProc; Root : TComponent); override;
    procedure ReadBankChange (Stream : TStream);
    procedure WriteBankChange (Stream : TStream);
  public
    fBankChangeRec : TBankChangeRec;
  published
    property InstrumentName : string read fInstrumentName write fInstrumentName;
  end;

  TPatchType = (ptSynthPad, ptAcousticPiano, ptBrass, ptElectricPiano, ptMusicalEffect, ptWoodwind, ptStrings, ptBass, ptSynthComp, ptSynthLead, ptKeyboard, ptPlucked, ptOrgan, ptPercussion, ptChoir, ptSoundEffects, ptDrums);


  TPatch = class (TComponent)
  private
    fBankNo : TBankNo;
    fPatchNo : TPatchNo;
    fPatchName : string;
    fComment : string;
    fDefaultChannel : TChannel;
    fMandatoryChannel : TChannel;
    fDefaultVolume : TControllerValue;
    fPatchType : TPatchType;
  protected
  public
  published
    property BankNo : TBankNo read fBankNo write fBankNo;
    property PatchNo : TPatchNo read fPatchNo write fPatchNo;
    property PatchName : string read fPatchName write fPatchName;
    property Comment : string read fComment write fComment;
    property DefaultChannel : TChannel read fDefaultChannel write fDefaultChannel;
    property MandatoryChannel : TChannel read fMandatoryChannel write fMandatoryChannel;
    property DefaultVolume : TControllerValue read fDefaultVolume write fDefaultVolume;
    property PatchType : TPatchType read fPatchType write fPatchType;
  end;

const
  PatchTypeNames : array [Low (TPatchType)..High (TPatchType)] of string =
   ('Synth Pad', 'Acoustic Piano', 'Brass', 'Electric Piano', 'Musical Effect', 'Winds',
    'Strings', 'Basses', 'Synth Comp', 'Synth Lead', 'Plucked', 'Keyboards', 'Organ',
    'Percussion', 'Choir', 'Sound Effects', 'Drum Voices');

implementation

procedure TInstrument.ReadBankChange (Stream : TStream);
begin
  Stream.ReadBuffer (fBankChangeRec, sizeof (fBankChangeRec));
end;

procedure TInstrument.WriteBankChange (Stream : TStream);
begin
  Stream.WriteBuffer (fBankChangeRec, sizeof (fBankChangeRec));
end;

procedure TInstrument.DefineProperties (Filer : TFiler);
begin
  inherited DefineProperties (Filer);
  Filer.DefineBinaryProperty ('BankChangeRec', ReadBankChange, WriteBankChange, True);
end;

procedure TInstrument.GetChildren (Proc : TGetChildProc; Root : TComponent);
var i : Integer;
begin
  for i := 0 to ComponentCount - 1 do
    proc (Components [i])
end;

begin
  RegisterClasses ([TInstrument, TPatch]);
end.

