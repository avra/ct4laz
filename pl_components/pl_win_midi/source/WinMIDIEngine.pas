{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}
unit WinMIDIEngine;

interface

uses
  Windows, Messages, SysUtils, Classes,forms,
  cmpMidiInput, cmpMidiOutput,cmpTrackOutputs,cmpMidiPlayer,
  cmpMidiData,cmpSynthController;


type


TWinMidiEngine = class(TComponent)
  private
  protected
     FMidiOutput:TSynthController;
     FMidiInput:TMidiInput;
     FMidiPlayer:TMidiPlayer;
     FTrackOutputs:TTrackOutputs;
     FMidiData:TMidiData;
     procedure SetActive (const value : boolean);
     function  GetActive : boolean;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    Procedure Clear;
    Function  SaveToStream(astream:Tstream):boolean;
    Function  LoadFromStream(astream:Tstream):boolean;
    Function  SaveToFile(aFile:String):boolean;
    Function  LoadFromFile(aFile:String):boolean;
    Procedure LoadMidiFile(const afilename:string);
    Procedure Play;
    Procedure Stop;
  published
    property Active : boolean read GetActive write SetActive;
    Property MidiOutput:TSynthController read FMidiOutput;
    Property MidiInput:TMidiInput read FMidiInput;
    Property TrackOutputs:TTrackOutputs read FTrackOutputs;
    Property MidiPlayer:TMidiPlayer read FMidiPlayer;
    Property MidiData:TMidiData read FMidiData;
  end;

implementation

//================================================================================
  uses MIDI_Msgs;


type TSettingsFileRec=record
     Whatis:string[70];
     FileID:integer;
     Version:integer;
     FutureB0:boolean;
     FutureB1:boolean;
     FutureB2:boolean;
     FutureI0:boolean;
     FutureI1:boolean;
     FutureI2:boolean;
    end;


const
     SettingsFileWhatis='PilotLogic MidiMakeEngine File';
     SettingsFileID=44444;
     SettingsFileVersion=1;



//constructor **********************************************
constructor TWinMidiEngine.Create(AOwner: TComponent);
begin

Inherited Create(AOwner);  

 FMidiOutput:=TSynthController.Create(self);
 FMidiInput:=TMidiInput.Create(self);
 FTrackOutputs:=TTrackOutputs.Create(self);
 FMidiPlayer:=TMidiPlayer.Create(self);
 FMidiData:=TMidiData.Create(self);
 //..........................................
 fTrackOutputs.MidiData:=fMidiData;
 fMidiPlayer.TrackOutputs:=fTrackOutputs;
 fMidiPlayer.MidiInput:=fMidiInput;
 fMidiPlayer.ResetAllControllers;

end;

//destructor **********************************************
destructor TWinMidiEngine.Destroy;
begin
  fMidiOutput.ResetControllers;
  fMidiOutput.Active:=false;
  FMidiData.clear;
  inherited Destroy;
end;

Procedure TWinMidiEngine.Clear;
 begin
   fMidiOutput.ResetControllers;
   FMidiPlayer.Play:=false;
   FMidiData.clear;
 end;

Procedure TWinMidiEngine.LoadMidiFile(const afilename:string);
 begin
   if fMidiPlayer.Play=true then fMidiPlayer.Play:=false; 

   fMidiData.clear;

   fMidiData.FileName:=afilename;
   fMidiData.Active:=true;
   fTrackOutputs.Active:=true;
 end;

Procedure TWinMidiEngine.Play;
 begin
   fMidiPlayer.Play:=true;
 end;

 Procedure TWinMidiEngine.Stop;
 begin
   fMidiPlayer.Play:=false;
   fMidiPlayer.Reset;
 end;

function TWinMidiEngine.GetActive : boolean;
begin
  result := fMidiOutput.Active;
end;

procedure TWinMidiEngine.SetActive (const value : boolean);
begin
  fMidiOutput.Active:=value;  
end;


//===============================================


Function  TWinMidiEngine.SaveToStream(astream:Tstream):boolean;
 var Srec:TSettingsFileRec;
     Writer:Twriter;
 begin
  result:=false;
  if astream=nil then exit;
  try
    sRec.Whatis:=SettingsFileWhatis;
    srec.FileID:=SettingsFileID;
    srec.Version:=SettingsFileVersion;

    astream.Write(srec,sizeof(TSettingsFileRec));

    //------------------------------------
    Writer:=Twriter.Create(astream, 16384);
       {
    Writer.writeinteger(Volume);
    Writer.writeinteger(Pan);
    Writer.writeinteger(Reverb);
    Writer.writeinteger(Chorus);
    Writer.writeinteger(Modulation);
    Writer.writeinteger(PitchBend);
    Writer.writeinteger(PBDelay);
    Writer.writeinteger(PBDuration);
    Writer.writeinteger(Duration);
    Writer.writeinteger(Octave);
    Writer.writeinteger(Loops);
    Writer.writeboolean(Sustain);
    Writer.WriteWideString(MidiText);
    Writer.WriteVariant(Note);
    Writer.WriteVariant(Instrument);
    Writer.WriteVariant(Channel);
                }
    result:=true;
  finally
   Writer.Free;
  end;
 end;

Function  TWinMidiEngine.LoadFromStream(astream:Tstream):boolean;
 var Srec:TSettingsFileRec;
     Reader:TReader;
 begin
 result:=false;
 Reader:=nil;

  if astream=nil then exit;
  try
    astream.Read(srec,sizeof(TSettingsFileRec));
    if sRec.FileID=SettingsFileID then
      begin  
       Reader:=TReader.Create(astream, 16384);
       {
       Volume:=Reader.Readinteger;
       Pan:=Reader.Readinteger;
       Reverb:=Reader.Readinteger;
       Chorus:=Reader.Readinteger;
       Modulation:=Reader.Readinteger;
       PitchBend:=Reader.Readinteger;
       PBDelay:=Reader.Readinteger;
       PBDuration:=Reader.Readinteger;
       Duration:=Reader.Readinteger;
       Octave:=Reader.Readinteger;
       Loops:=Reader.Readinteger;
       Sustain:=Reader.Readboolean;
       MidiText:=Reader.ReadWideString;
       Note:=Reader.ReadVariant;
       Instrument:=Reader.ReadVariant;
       Channel:=Reader.ReadVariant;
            }
       result:=true;
     end;
  finally
    if Reader<>nil then Reader.Free;
  end;
 end;

Function  TWinMidiEngine.SaveToFile(aFile:String):boolean;
var s:TFileStream;
 begin
  result:=false;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=SaveToStream(s);
  finally
   s.Free;
  end;
 end;

Function  TWinMidiEngine.LoadFromFile(aFile:String):boolean;
var s:TFileStream;
 begin
  result:=false;
  if afile='' then exit;
  if fileexists(afile)=false then exit;
  try
   s:=TFileStream.Create(afile,fmOpenRead);
   result:=LoadFromStream(s);
  finally
   s.Free;
  end;
 end;




end.
