{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_MIDI
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  
  
**********************************************************************************}
 unit unitMidiGlobals;

interface
    uses Classes;
const
  midiNoteOff 	        = $80;
  midiNoteOn            = $90;
  midiKeyAftertouch 	= $a0;
  midiController 	= $b0;
  midiProgramChange 	= $c0;
  midiChannelAftertouch = $d0;
  midiPitchBend 	= $e0;
  midiSysex       	= $f0;
  midiSysexCont		= $f7;
  midiMeta		= $ff;

  midiStatusMask	= $f0;
  midiStatus		= $80;
  midiChannelMask	= $0f;

  metaSeqno		= $00;
  metaText		= $01;
  metaCopyright		= $02;
  metaTrackName		= $03;
  metaInstrumentName	= $04;
  metaLyric		= $05;
  metaMarker		= $06;
  metaCuePoint		= $07;
  metaMiscText0		= $08;
  metaMiscText1		= $09;
  metaMiscText2		= $0a;
  metaMiscText3		= $0b;
  metaMiscText4		= $0c;
  metaMiscText5		= $0d;
  metaMiscText6		= $0e;
  metaMiscText7		= $0f;
  metaTrackStart	= $21;
  metaTrackEnd		= $2f;
  metaTempoChange	= $51;
  metaSMPTE		= $54;
  metaTimeSig		= $58;
  metaKeySig		= $59;
  metaSequencer		= $7f;

  //............................
  LASTINSTRUMENT=127;//174;
  LASTCHANNEL=15;
  //...........................
  seINSTRUMENT =$C0;
  seCONTROLMODE =$B0;
  seAFTERTOUCH =$D0;
  sePITCHWHEEL =$E0;
  //.............................
  teCONTROLMODE_Modulation =$01;
  teCONTROLMODE_Breath =$02;
  teCONTROLMODE_Foot =$04;
  teCONTROLMODE_PortamentoTime =$05;
  teCONTROLMODE_Volume =$07;
  teCONTROLMODE_Balance =$08;
  teCONTROLMODE_Pan =$0A;
  teCONTROLMODE_Expression =$0B;
  teCONTROLMODE_Effect1 =$0C;
  teCONTROLMODE_Effect2 =$0D;

  teCONTROLMODE_Sustain =$40;
  teCONTROLMODE_Portamento =$41;
  teCONTROLMODE_Sustenuto =$42;
  teCONTROLMODE_SoftPetal =$43;
  teCONTROLMODE_LegatoFootSwitch =$44;
  teCONTROLMODE_Hold2 =$45;

  teCONTROLMODE_AllSoundsOff =$78;
  teCONTROLMODE_ResetAllControlers =$79;
  teCONTROLMODE_AllNotesOff =$7B;

type
  TTrack = 0..255;
  TChannel = 0..15;
  TNote = 0..127;
  TController = 0..127;
  TPatchNo = 0..127;
  TBankNo = 0..127;
  TControllerValue = 0..127;

  TMGPercentage = 0..100;
  //TMGNote= (mg00_C,mg01_Dsharp,mg02_D,mg03_Eb,mg04_E,mg05_F,mg06_Gb,mg07_G,mg08_Ab,mg09_A,mg10_Bb,mg11_B);
  TMGNote= (mgC,mgCsharp,mgD,mgDsharp,mgE,mgF,mgFsharp,mgG,mgGsharp,mgA,mgAsharp,mgB);
  TMGChannel=(mgCh01,mgCh02,mgCh03,mgCh04,mgCh05,mgCh06,mgCh07,mgCh08,mgCh09,mgCh10,mgCh11,mgCh12,mgCh13,mgCh14,mgCh15,mgCh16);
  //instruments (type)
  TMGInstrument=(
  mgAcousticGrandPiano,mgBrightAcousticPiano,mgElectricGrandPiano,
  mgHonkyTonkPiano,mgElectricPiano1,mgElectricPiano2,mgHarpsichord,mgClavinet,
  mgCelesta,mgGlockenspiel,mgMusicBox,mgVibraphone,mgMarimba,mgXylophone,
  mgTubularBells,mgDulcimer,
  mgDrawbarOrgan,mgPercussiveOrgan,mgRockOrgan,mgChurchOrgan,
  mgReedOrgan,mgAccordion,mgHarmonica,mgTangoAccordion,
  mgAcousticNylonGuitar,mgAcousticSteelGuitar,mgJazzElectricGuitar,
  mgCleanElectricGuitar,mgMutedElectricGuitar,mgOverdrivenGuitar,
  mgDistortionGuitar,mgGuitarHarmonics,mgAcousticBass,
  mgFingeredElectricBass,mgPickedElectricBass,mgFretlessBass,
  mgSlapBass1,mgSlapBass2,mgSynthBass1,mgSynthBass2,
  mgViolin,mgViola,mgCello,mgContrabass,
  mgTremoloStrings,mgPizzicatoStrings,mgOrchestralHarp,mgTimpani,
  mgStringEnsemble1,mgStringEnsemble2,mgSynthStrings1,
  mgSynthStrings2,mgChoirAahs,mgVoiceOohs,mgSynthVoice,mgOrchestraHit,
  mgTrumpet,mgTrombone,mgTuba,mgMutedTrumpet,mgFrenchHorn,
  mgBrassSection,mgSynthBrass1,mgSynthBrass2,
  mgSopranoSax,mgAltoSax,mgTenorSax,mgBaritoneSax,
  mgOboe,mgEnglishHorn,mgBassoon,mgClarinet,
  mgPiccolo,mgFlute,mgRecorder,mgPanFlute,mgBlownBottle,
  mgShakuhachi,mgWhistle,mgOcarina,
  mgSquareLead,mgSawtoothLead,mgCalliopeLead,mgChiffLead,
  mgCharangLead,mgVoiceLead,mgFifthsLead,mgBassandLead,
  mgNewAgePad,mgWarmPad,mgPolySynthPad,mgChoirPad,
  mgBowedPad,mgMetallicPad,mgHaloPad,mgSweepPad,
  mgSynthFXRain,mgSynthFXSoundtrack,mgSynthFXCrystal,mgSynthFXAtmosphere,
  mgSynthFXBrightness,mgSynthFXGoblins,mgSynthFXEchoes,mgSynthFXSciFi,
  mgSitar,mgBanjo,mgShamisen,mgKoto,mgKalimba,
  mgBagpipe,mgFiddle,mgShanai,
  mgTinkleBell,mgAgogo,mgSteelDrums,mgWoodblock,
  mgTaikoDrum,mgMelodicTom,mgSynthDrum,mgReverseCymbal,
  mgGuitarFretNoise,mgBreathNoise,mgSeashore,mgBirdTweet,
  mgTelephoneRing,mgHelicopter,mgApplause,mgGunshot,
  //percussion
  mgAcousticBassDrum,mgBassDrum1,mgSideStick,mgAcousticSnare,
  mgHandClap,mgElectricSnare,mgLowFloorTom,mgClosedHiHat,
  mgHighFloorTom,mgPedalHiHat,mgLowTom,mgOpenHiHat,
  mgLowMidTom,mgHiMidTom,mgCrashCymbal1,mgHighTom,
  mgRideCymbal1,mgChineseCymbal,mgRideBell,mgTambourine,
  mgSplashCymbal,mgCowbell,mgCrashCymbal2,mgVibraslap,
  mgRideCymbal2,mgHiBongo,mgLowBongo,mgMuteHiConga,
  mgOpenHiConga,mgLowConga,mgHighTimbale,mgLowTimbale,
  mgHighAgogo,mgLowAgogo,mgCabasa,mgMaracas,
  mgShortWhistle,mgLongWhistle,mgShortGuiro,mgLongGuiro,
  mgClaves,mgHiWoodBlock,mgLowWoodBlock,mgMuteCuica,
  mgOpenCuica,mgMuteTriangle,mgOpenTriangle
  );


//---------------------------------------------------------------------------
// Four byte MIDI message.  (No running status, but Note off may be Note on
//                           with zero velocity )

  TEventData = packed record    // ** nb takes 5 bytes
  case status : byte of
    0 : (b2, b3 : byte);
    1 : (sysex : PChar)
  end;
  PEventData = ^TEventData;

//---------------------------------------------------------------------------
// Midi event
  PMidiEventData = ^TMidiEventData;
  TMidiEventData = packed record // ** nb takes 11 bytes
    pos : LongInt;               // Position in ticks from start of song.
    sysexSize : word;            // Size of sysex or meta message
    data : TEventData;           // Event data
    OnOffEvent : PMidiEventData;
  end;

  TMidiEventClipboardHeader = packed record
    noEvents, startPosn : Integer;
  end;
  PMidiEventClipboardHeader = ^TMidiEventClipboardHeader;

const
  ControllerNames : array [TController] of string [20] = (
  'Bank Select',          {0  :0-127 MSB}
  'Modulation Wheel',     {1  :0-127 MSB}
  'Breath Controller',    {2  :0-127 MSB}
  '',
  'Foot Control',         {4  :0-127 MSB}
  'Portamento Time',      {5  :0-127 MSB}
  'Data Entry MSB',       {6  :0-127 MSB}
  'Volume',               {7  :0-127 MSB}
  'Balance',              {8  :0-127 MSB}
  '',
  'Pan',                  {10 :0-127 MSB}
  'Expression',           {11 :0-127 MSB}
  'Effect Control 1',     {12 :0-127 MSB}
  'Effect Control 2',     {13 :0-127 MSB}
  '', '',
  'General 1',            {16 :0-127 MSB}
  'General 2',            {17 :0-127 MSB}
  'General 3',            {18 :0-127 MSB}
  'General 4',            {19 :0-127 MSB}
  '', '', '', '', '','', '', '', '', '', '', '',
  'Bank Select',          {32 :0-127 LSB}
  'Modulation Wheel',     {33 :0-127 LSB}
  'Breath Controller',    {34 :0-127 LSB}
  '',
  'Foot Control',         {36 :0-127 LSB}
  'Portamento Time',      {37 :0-127 LSB}
  'Data Entry MSB',       {38 :0-127 LSB}
  'Volume',               {39 :0-127 LSB}
  'Balance',              {40 :0-127 LSB}
  '',
  'Pan',                  {42 :0-127 LSB}
  'Expression',           {43 :0-127 LSB}
  'Effect Control 1',     {44 :0-127 LSB}
  'Effect Control 2',     {45 :0-127 LSB}
  '', '',
  'General 1',            {48 :0-127 LSB}
  'General 2',            {49 :0-127 LSB}
  'General 3',            {50 :0-127 LSB}
  'General 4',            {51 :0-127 LSB}
  '', '', '', '', '', '', '', '','', '', '', '',
  'Sustain',              {64 : <63 off, >64 on}
  'Portamento',           {65 : <63 off, >64 on}
  'Sostenuto',            {66 : <63 off, >64 on}
  'Soft Pedal',           {67 : <63 off, >64 on}
  'Legato Foots Switch',  {68 : <63 Normal, >64 Legato}
  'Hold 2',               {69 : <63 off, >64 on}
  'Sound Variation',      {70 :0-127 LSB}
  'Timbre/Harmonic Intens',{71 :0-127 LSB}
  'Release Time',         {72 :0-127 LSB}
  'Attack Time',          {73 :0-127 LSB}
  'Brightness',           {74 :0-127 LSB}
  'Decay Time',           {75 :0-127 LSB}
  'Vibrato Rate',         {76 :0-127 LSB}
  'Vibrato Depth ',       {77 :0-127 LSB}
  'Vibrato Delay ',       {78 :0-127 LSB}
  '',
  'General 5',            {80 :0-127 LSB}
  'General 6',            {81 :0-127 LSB}
  'General 7',            {82 :0-127 LSB}
  'General 8',            {83 :0-127 LSB}
  'Portamento',           {83 :0-127 LSB}
  '', '', '', '', '', '',
  'Reverb',               {91 :0-127 LSB}
  'Tremolo Depth',        {92 :0-127 LSB}
  'Chorus Depth',         {93 :0-127 LSB}
  'Detune',               {94 :0-127 LSB}
  'Phaser Depth',         {95 :0-127 LSB}
  'Data Entry +1',        {96}
  'Data Entry -1',        {97}
  'Non reg LMB',          {98  :0-127 LSB}
  'Non reg MSB',          {99  :0-127 MSB}
  'Reg MSB',              {100 :0-127 LSB}
  'Reg LMB',              {101 :0-127 MSB}
  '', '', '','', '', '', '', '', '', '', '', '',  '', '', '', '', '', '',
  'All Sounds Off',       {120 :0}
  'Reset All Controllers',{121 :0}
  'Local Mode ON/OFF',    {122 :0 off, 127 on}
  'All Notes Off',        {123 :0}
  'Omni Mode Off',        {124 :0}
  'Omni Mode On',         {125 :0}
  'Poly Mode On/Off ',    {126 :**}
  'Poly Mode On');        {127 :0}

   {** Note: This equals the number of channels,
       or zero if the number of channels equals
       the number of voices in the receiver.  }

function AdjustForTimesig (n, beatDiv : Integer) : Integer;
function UnadjustForTimesig (n, beatDiv : Integer) : Integer;
function GetBPM (tempo, beatDiv : Integer) : Integer;
function SwapLong (value : LongInt) : LongInt;
function GetNoteName (note : Integer) : string;
Procedure ChannelsAddNamesToStrings(Str:TStrings);
Procedure InstrumentsAddNamesToStrings(Str:TStrings);
Function  InstrumentGetName(const ins:integer):string;
Function  InstrumentGetNameFx(const insFx:TMGInstrument):string;
function LimitValue(const amin,amax,aval: Integer):Integer;




implementation

uses sysutils;

//------------------------------------------------------------
var

//notes
DN:array[0..11] of string=('C','C#','D','D#','E','F','F#','G','G#','A','A#','B');

//note names
NoteNames:array [0..11] of String=('C','Csharp','D','Dsharp','E','F','Fsharp','G','Gsharp','A','Asharp','B');
//instrument names
Instruments:array [0..174] of String=(
'AcousticGrandPiano','BrightAcousticPiano','ElectricGrandPiano',
'HonkyTonkPiano','ElectricPiano1','ElectricPiano2','Harpsichord','Clavinet',
'Celesta','Glockenspiel','MusicBox','Vibraphone','Marimba','Xylophone',
'TubularBells','Dulcimer',
'DrawbarOrgan','PercussiveOrgan','RockOrgan','ChurchOrgan',
'ReedOrgan','Accordion','Harmonica','TangoAccordion',
'AcousticNylonGuitar','AcousticSteelGuitar','JazzElectricGuitar',
'CleanElectricGuitar','MutedElectricGuitar','OverdrivenGuitar',
'DistortionGuitar','GuitarHarmonics','AcousticBass',
'FingeredElectricBass','PickedElectricBass','FretlessBass',
'SlapBass1','SlapBass2','SynthBass1','SynthBass2',
'Violin','Viola','Cello','Contrabass',
'TremoloStrings','PizzicatoStrings','OrchestralHarp','Timpani',
'StringEnsemble1','StringEnsemble2','SynthStrings1',
'SynthStrings2','ChoirAahs','VoiceOohs','SynthVoice','OrchestraHit',
'Trumpet','Trombone','Tuba','MutedTrumpet','FrenchHorn',
'BrassSection','SynthBrass1','SynthBrass2',
'SopranoSax','AltoSax','TenorSax','BaritoneSax',
'Oboe','EnglishHorn','Bassoon','Clarinet',
'Piccolo','Flute','Recorder','PanFlute','BlownBottle',
'Shakuhachi','Whistle','Ocarina',
'SquareLead','SawtoothLead','CalliopeLead','ChiffLead',
'CharangLead','VoiceLead','FifthsLead','BassandLead',
'NewAgePad','WarmPad','PolySynthPad','ChoirPad',
'BowedPad','MetallicPad','HaloPad','SweepPad',
'SynthFXRain','SynthFXSoundtrack','SynthFXCrystal','SynthFXAtmosphere',
'SynthFXBrightness','SynthFXGoblins','SynthFXEchoes','SynthFXSciFi',
'Sitar','Banjo','Shamisen','Koto','Kalimba',
'Bagpipe','Fiddle','Shanai',
'TinkleBell','Agogo','SteelDrums','Woodblock',
'TaikoDrum','MelodicTom','SynthDrum','ReverseCymbal',
'GuitarFretNoise','BreathNoise','Seashore','BirdTweet',
'TelephoneRing','Helicopter','Applause','Gunshot',
//percussion 128-174
'AcousticBassDrum','BassDrum1','SideStick','AcousticSnare',
'HandClap','ElectricSnare','LowFloorTom','ClosedHiHat',
'HighFloorTom','PedalHiHat','LowTom','OpenHiHat',
'LowMidTom','HiMidTom','CrashCymbal1','HighTom',
'RideCymbal1','ChineseCymbal','RideBell','Tambourine',
'SplashCymbal','Cowbell','CrashCymbal2','Vibraslap',
'RideCymbal2','HiBongo','LowBongo','MuteHiConga',
'OpenHiConga','LowConga','HighTimbale','LowTimbale',
'HighAgogo','LowAgogo','Cabasa','Maracas',
'ShortWhistle','LongWhistle','ShortGuiro','LongGuiro',
'Claves','HiWoodBlock','LowWoodBlock','MuteCuica',
'OpenCuica','MuteTriangle','OpenTriangle');
//---------------------------------------------------------------------------------

Procedure InstrumentsAddNamesToStrings(Str:TStrings);
 var i:integer;
 begin
  if Str=nil then exit;
  Str.Clear;

  for i:=0 to LASTINSTRUMENT do
    begin
     Str.Add(inttostr(i)+'  '+Instruments[i]);
    end;

 end;

Procedure ChannelsAddNamesToStrings(Str:TStrings);
 var i:integer;
 begin
  if Str=nil then exit;
  Str.Clear;

  for i:=0 to LASTCHANNEL do
    begin
      Str.Add(inttostr(i)+'  Chan '+inttostr(i+1));
    end;

    Str.Strings[9]:='9  Chan 10 (Percussion)';


 end;

Function  InstrumentGetName(const ins:integer):string;
 begin
   result:=IntToStr(ins)+' : mg'+Instruments[ins];
 end;

Function  InstrumentGetNameFx(const insFx:TMGInstrument):string;
 begin
    result:=InstrumentGetName(integer(insFx))
 end;

function LimitValue(const amin,amax,aval: Integer):Integer;
 begin
 if (aval>=amin) and (aval<=amax) then
   begin
     Result:=aval;
     Exit;
   end;

if aval>amax then
   Result:=amax else
   Result:=amin;

end;

(*---------------------------------------------------------------------*
 | function SwapLong () : LongInt;                                     |
 |                                                                     |
 | Byte swaps a four-byte integer eg: $01020304 becomes $04030201      |
 |                                                                     |
 | Parameters:                                                         |
 |   value : LongInt      The integer to swap                          |
 |                                                                     |
 | The function returns the swapped integer                            |
 *---------------------------------------------------------------------*)
function SwapLong (value : LongInt) : LongInt;
var
  r : packed record case Integer of
    1 : (a : byte; b : word; c : byte);
    2 : (l : longint);
  end;
  t : byte;

begin
  r.l := value;
  r.b := swap (r.b);
  t := r.a; r.a := r.c; r.c := t;
  result := r.l;
end;


function AdjustForTimesig (n, beatDiv : Integer) : Integer;
begin
  if BeatDiv > 2 then
    result := n shr (BeatDiv - 2)
  else
    if BeatDiv < 2 then
      result := n shl (2 - BeatDiv)
    else
      result := n;
end;



function UnAdjustForTimesig (n, beatDiv : Integer) : Integer;
begin
  if BeatDiv > 2 then
    result := n shl (BeatDiv - 2)
  else
    if BeatDiv < 2 then
      result := n shr (2 - BeatDiv)
    else
      result := n;
end;

function GetBPM (tempo, beatDiv : Integer) : Integer;
begin
  result := UnAdjustForTimesig (60000 div tempo, beatDiv);
end;

function GetNoteName (note : Integer) : string;
var
  ch : char;
  Octave : Integer;
begin
  Octave := note div 12;
  Note := Note mod 12;
  case note of
    0, 1 : ch := 'C';
    2, 3 : ch := 'D';
    4 : ch := 'E';
    5, 6 : ch := 'F';
    7, 8 : ch := 'G';
    9, 10 : ch := 'A';
    11 : ch := 'B';
    else ch := '?'
  end;

  if note in [1, 3, 6, 8, 10] then
    result := ch + '#' + IntToStr (Octave)
  else
    result := ch + IntToStr (Octave);
end;

end.
