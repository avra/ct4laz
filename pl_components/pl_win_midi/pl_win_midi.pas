{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_Midi;

{$warn 5023 off : no warning about unused units}
interface

uses
  All_WinMidi_Register, cmpBarControl, cmpControllerMap, cmpInstrument, 
  cmpKeyboard, cmpMidiData, cmpMidiGrid, cmpMidiInput, cmpMidiIterator, 
  cmpMidiMixer, cmpMidiOutput, cmpMidiPlayer, cmpPianoRoll, cmpPosDisplay, 
  cmpriffStream, cmpSynthController, cmpTempoMap, cmpTG77Controller, 
  cmpTimeDisplay, cmpTimerSpeedButton, cmpTrackOutputs, WinMIDIEngine, 
  unitMidiGlobals, unitMidiTrackStream, unitVirtualMemory, MIDI_Msgs, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('All_WinMidi_Register', @All_WinMidi_Register.Register);
end;

initialization
  RegisterPackage('pl_Win_Midi', @Register);
end.
