{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_DirectXUT;

{$warn 5023 off : no warning about unused units}
interface

uses
  DDUtil, DirectXUT, DirectXUTConst, usp10, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_Win_DirectXUT', @Register);
end.
