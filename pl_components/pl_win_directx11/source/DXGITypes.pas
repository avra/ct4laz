{******************************************************************************
                      Pilotlogic Software House
                     
 Package pl_Win_DirectX11
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)      
               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK ***** 
  
**********************************************************************************}

unit DXGITypes;

interface

uses
  Windows;

type
  TInteger64=Int64;
  TUnsignedInteger64=UInt64;
  PLongBool=^LongBool;
  PHRESULT=^HRESULT;
  SIZE_T=LongWord; // 32 bit only!
  PSIZE_T=^SIZE_T;
  HANDLE=THandle;
  RECT=TRect;
  PTRect=PRect;
  PTPoint=PPoint;
  TColorArray=array[0..3] of Single;
  TQuadArray_UInt=array[0..3] of LongWord;
  TQuadArray_Float=array[0..3] of Single;

  D3DCOLOR=LongWord;
  TD3DColor=D3DCOLOR;
  PD3DColor=^TD3DColor;
  PTD3DColor=^TD3DColor;

  D3DCOLORVALUE=packed record
    R:Single;
    G:Single;
    B:Single;
    A:Single;
  end;
  TD3DColorValue=D3DCOLORVALUE;
  PD3DColorValue=^TD3DColorValue;
  PTD3DColorValue=^TD3DColorValue;

function ColorArray(R,G,B,A:Single):TColorArray;

const
  INT64_MAX=Int64(9223372036854775807);
  ULONGLONG_MAX=UInt64($FFFFFFFFFFFFFFFF);
  UINT64_MAX=UInt64($FFFFFFFFFFFFFFFF);

implementation

function ColorArray(R,G,B,A:Single):TColorArray;
begin
  Result[0]:=R;
  Result[1]:=G;
  Result[2]:=B;
  Result[3]:=A;
end;

end.



