{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_macosmetal;

{$warn 5023 off : no warning about unused units}
interface

uses
  Metal, MetalKit, MetalControl, MetalPipeline, MetalUtils, VectorMath, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('MetalControl', @MetalControl.Register);
end;

initialization
  RegisterPackage('pl_macosmetal', @Register);
end.
