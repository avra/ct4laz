{**********************************************************************
                    PilotLogic Software House
                   
 Package pl_AGGPas
 is a modification of
 Anti-Grain Geometry 2D Graphics Library (http://www.aggpas.org/)
 for CodeTyphon Studio (https://www.pilotlogic.com/)
 This file is part of CodeTyphon Studio
***********************************************************************}

unit agg_pixelmap;

{$if defined(WINDOWS)}
    {$IFDEF WINCE}
       {$i wince_agg_pixelmap.inc}
    {$ELSE}
       {$i win_agg_pixelmap.inc}
    {$ENDIF}     
{$elseif defined(DARWIN)}
   {$i mac_agg_pixelmap.inc}
{$ELSE}
   {$i lin_agg_pixelmap.inc}
{$ENDIF}  
