{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_apelib;

{$warn 5023 off : no warning about unused units}
interface

uses
  uAbstractCollection, uAbstractConstraint, uAbstractItem, uAbstractParticle, 
  uApeEngine, uBridge, uCapsule, ucar, uCircleParticle, uCollision, 
  uCollisionDetector, uCollisionResolver, uComposite, uGDIRender, uGroup, 
  uInterval, uMathUtil, uRectangleParticle, uRectComposite, uRender, 
  uRimParticle, uRotator, uSpringConstraint, uSpringConstraintParticle, 
  uSprite, uSurfaces, uSwingDoor, uVector, uWheelParticle, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_apelib', @Register);
end.
