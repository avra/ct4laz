{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uRectangleParticle;

interface

uses uAbstractParticle, uMathUtil, uVector, uInterval, uRender, sysUtils;

Type

TRctTypeDouble = Array[0..1] of Double;
TRctTypeVector = Array[0..1] of Vector;

RectangleParticle = Class(AbstractParticle)
Private
  _extents : TRctTypeDouble;
  _axes : TRctTypeVector;
  _radian : Double;
    function GetAngle: Double;
    function GetHEight: Double;
    function GetRadian: Double;
    function GetWidth: Double;
    procedure SetAngle(const Value: Double);
    procedure SetAxes(const Value: Double);
    procedure SetHeight(const Value: Double);
    procedure SetRadian(const Value: Double);
    procedure SetWidth(const Value: Double);
Public

  Constructor Create(x,y,awidth,aheight, arotation : double; aFixed : Boolean; aMass : Double = 1; aElasticity : Double = 0.3; aFriction: Double = 0); reintroduce;

  Procedure Init; Override;
  Procedure Paint; OVerride;
  Function GetProjection(AnAxis : Vector) : Interval;

  Property Radian : Double read GetRadian Write SetRadian;
  Property Angle : Double read GetAngle Write SetAngle;
  Property Width : Double read GetWidth Write SetWidth;
  Property Height : Double read GetHEight Write SetHeight;
  Property Axes : TRctTypeVector read _axes;
  Property Extents : TRctTypeDouble read _Extents Write _Extents;

end;

implementation

uses uAbstractItem;

{ RectangleParticle }


constructor RectangleParticle.Create(x,y,awidth,aheight, arotation : double; aFixed : Boolean; aMass : Double = 1; aElasticity : Double = 0.3; aFriction: Double = 0);

begin
  inherited Create(x,y,amass,aelasticity,afriction,afixed);
  _Extents[0]:=awidth/2;
  _Extents[1]:=aheight/2;
  _axes[0]:=Vector.Create(0,0);
  _axes[1]:=Vector.Create(0,0);
  Radian:=aRotation;
end;

function RectangleParticle.GetAngle: Double;
begin
  Result :=Radian * ONE_EIGHTY_OVER_PI;
end;

function RectangleParticle.GetHEight: Double;
begin
  result:= _Extents[1] * 2;
end;

function RectangleParticle.GetProjection(AnAxis: Vector): Interval;
var radius : Double;
    c : Double;
begin
  Radius := extents[0] * Abs(AnAxis.Dot(axes[0]))+
            extents[1] * Abs(AnAxis.Dot(axes[1]));
  c := Samp.Dot(AnAxis);
  aInterval.min:=c-Radius;
  aInterval.max:=c+Radius;
  Result:=aInterval;
end;

function RectangleParticle.GetRadian: Double;
begin
  result :=_Radian;
end;

function RectangleParticle.GetWidth: Double;
begin
  Result:= _extents[0] * 2;
end;

procedure RectangleParticle.Init;
begin
  //CleanUp;
  Paint;
end;

procedure RectangleParticle.Paint;
begin
  // hop:
  aRenderer.Rectangle(px,py,Width,Height,Angle);
  //aRenderer.Text(px,py,FloatToStr(Angle));

end;

procedure RectangleParticle.SetAngle(const Value: Double);
begin
  Radian := Value * PI_OVER_ONE_EIGHTY;
end;

procedure RectangleParticle.SetAxes(const Value: Double);
var s : Double;
    c : Double;
begin
  s:= Sin(Value);
  c:= Cos(Value);
  axes[0].x:=c;
  axes[0].y:=s;
  axes[1].x:=-s;
  axes[1].y:=c;
end;

procedure RectangleParticle.SetHeight(const Value: Double);
begin
  _Extents[1] := Value /2;
end;

procedure RectangleParticle.SetRadian(const Value: Double);
begin
  _Radian := Value;
  SetAxes(Value);
end;

procedure RectangleParticle.SetWidth(const Value: Double);
begin
  _Extents[0] := Value /2;
end;


end.
