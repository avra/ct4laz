{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uRimParticle;

interface

Uses uVector, MAth, uApeEngine;

type

RimPArticle = class(TObject)
Private
		wr : Double;
		av : Double ;
		sp : Double;
		maxTorque : Double;
    FApeEngine : ApeEngine;
Public
  	curr : Vector;
		prev : Vector;

    Constructor Create(anApeEngine : ApeEngine; Ar : Double; Mt : Double);
    Procedure Update(dt : Double);

    Property Speed : Double read sp Write sp;
    property AngularVelocity : Double read av Write av;
End;

implementation

{ RimPArticle }

constructor RimPArticle.Create(anApeEngine : ApeEngine; Ar : Double; Mt : Double);
begin
  Curr := Vector.Create(ar,0);
  prev := Vector.Create(0,0);
  sp := 0;
  av := 0;
  FApeEngine := anApeEngine;

  maxTorque := mt;
  wr := ar;
end;

procedure RimParticle.Update(dt: Double);
var dx, dy, Len, ox, oy, px, py, clen, diff : Double;
begin
  //Origins of this code are from Raigan Burns, Metanet Software
  //Clamp torques to valid range
  sp := max(-maxTorque, min(maxTorque, sp + av));

  //Apply torque
	//This is the tangent vector at the rim particle
	dx := -curr.y;
	dy := curr.x;

	//Normalize so we can scale by the rotational speed
	len := sqrt(dx * dx + dy * dy);

  If Len <>0 then
  begin
	  dx := dx/len;
	  dy := dy/len;
  end;

	curr.x := curr.x + sp * dx;
	curr.y := curr.y + sp * dy;

	ox := prev.x;
	oy := prev.y;
  prev.x := curr.x;
  prev.y := curr.y;
	px := prev.x;
	py := prev.y;

	curr.x := FApeEngine.damping * (px - ox);
	curr.y := FApeEngine.damping * (py - oy);

	// hold the rim particle in place
	clen := sqrt(curr.x * curr.x + curr.y * curr.y);
  if clen<>0 then
  Begin
	  diff := (clen - wr) / clen;
	  curr.x := curr.x - curr.x * diff;
	  curr.y := curr.y - curr.y * diff;
  end;
end;

end.
