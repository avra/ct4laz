{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uCollisionResolver;

Interface

Uses uAbstractParticle, uVector, uCollision, uMathUtil;

Type

CollisionResolver = Class
Public
  Procedure ResolveParticleParticle(pa,pb : AbstractPArticle; normal : Vector; Depth : Double);
  //Function Clamp(input,min,max : Double) : Double; Yet in mathutils !
end;

var CollisionResolverInstance : CollisionResolver;


implementation

{ CollisionResolver }

procedure CollisionResolver.ResolveParticleParticle(pa,
  pb: AbstractPArticle; normal: Vector; Depth: Double);
var mtd,vna,vnb,mtda,mtdb : Vector;
    te,suminvmass,tf : Double;
    ca,cb : Collision;
begin
	// a collision has occured. set the current positions to sample locations
  pa.Curr.Copy(pa.Samp);
  pb.Curr.Copy(pb.Samp);

  mtd:=normal.Mult(depth);
  te:=pa.Elasticity+pb.Elasticity;
  suminvmass:=pa.InvMass+pb.InvMass;

  // the total friction in a collision is combined but clamped to [0,1]
  tf := Clamp(1- (pa.Friction + pb.Friction),0,1);

  // get the collision components, vn and vt
  ca:=pa.GetComponents(normal);
  cb:=pb.GetComponents(normal);

  // calculate the coefficient of restitution based on the mass, as the normal component
  vnA:=(cb.vn.mult((te + 1) * pa.invMass).plus(
     		ca.vn.mult(pb.invMass - te * pa.invMass))).divEquals(sumInvMass);
  vnB:=(ca.vn.mult((te + 1) * pb.invMass).plus(
     		cb.vn.mult(pa.invMass - te * pb.invMass))).divEquals(sumInvMass);

  // apply friction to the tangental component
  ca.vt.multEquals(tf);
  cb.vt.multEquals(tf);

  // scale the mtd by the ratio of the masses. heavier particles move less
  mtdA:= mtd.mult( pa.invMass / sumInvMass);
  mtdB:= mtd.mult(-pb.invMass / sumInvMass);

  // add the tangental component to the normal component for the new velocity
  vnA.plusEquals(ca.vt);
  vnB.plusEquals(cb.vt);

  if not pa.Fixed then
    pa.ResolveCollision(mtdA, vnA, normal, depth, -1, pb);
  if not pb.Fixed then
    pb.resolveCollision(mtdB, vnB, normal, depth,  1, pa);
end;

Initialization
CollisionResolverInstance:=CollisionResolver.Create;

Finalization
CollisionResolverInstance.Free;

end.
