{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}

unit uAbstractConstraint;

interface

Uses uAbstractITem, uRender;

type

AbstractConstraint = Class(AbstractITem)
Private
  _stiffness : double;
Public
  aRenderer : AbstractRenderer;

  constructor Create(Stiffness : Double); Reintroduce;

  Function Stiffness : Double; Overload;
  Procedure Stiffness(Value : Double); Overload;

  Procedure SetRenderer(TheRenderer : AbstractRenderer);

  Procedure Resolve; Virtual; Abstract;
end;

implementation

{ AbstractConstraint }

constructor AbstractConstraint.Create(Stiffness: Double);
begin
  Inherited Create;
  Self.Stiffness(Stiffness);
end;

function AbstractConstraint.Stiffness: Double;
begin
  Result := _stiffness;
end;

procedure AbstractConstraint.SetRenderer(TheRenderer: AbstractRenderer);
begin
  aRenderer := TheRenderer;
end;

procedure AbstractConstraint.Stiffness(Value: Double);
begin
  _stiffness := Value;
end;

end.


