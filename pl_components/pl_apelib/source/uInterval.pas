{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uInterval;

interface

type

Interval = Class
Public
  min,max : Double;

  Constructor Create(amin,amax : Double);
  Function ToString : String;
end;

implementation

uses SysUtils;

{ Interval }

constructor Interval.Create(amin,amax: Double);
begin
  Self.min:=amin;
  Self.max:=amax;
end;

function Interval.ToString: String;
begin
  result:=FloatToStr(min)+' : '+FloatToStr(max);
end;

end.
