{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uAbstractItem;

interface

Type

AbstractItem = class
  private
    _Visible : Boolean;
    _AlwaysRepaint : Boolean;
    function GetAR: Boolean;
    function GetVisible: Boolean;
    procedure SetAR(const Value: Boolean);
    procedure SetVisible(const Value: Boolean);
Public
  Constructor Create; Virtual;
  Procedure Init; Virtual; Abstract;
  Procedure Paint; Virtual; Abstract;
  Procedure CleanUp; Virtual; Abstract;
  Property Visible : Boolean read GetVisible Write SetVisible;
  Property AlwaysRepaint : Boolean read GetAR Write SetAR;
end;

implementation

{ AbstractItem }

constructor AbstractItem.Create;
begin
  Visible := true;
  AlwaysRepaint := False;
end;

function AbstractItem.GetAR: Boolean;
begin
  Result := _AlwaysRepaint;
end;

function AbstractItem.GetVisible: Boolean;
begin
  Result := _Visible;
end;

procedure AbstractItem.SetAR(const Value: Boolean);
begin
  _AlwaysRepaint := Value;
end;

procedure AbstractItem.SetVisible(const Value: Boolean);
begin
   _Visible := Value;
end;

end.
