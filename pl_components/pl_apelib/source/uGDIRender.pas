{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uGDIRender;

interface

Uses uRender, Graphics, Classes, Math, uvector;

Type

GDIRenderer = Class(AbstractRenderer)
Private
  FCanvas : TCanvas;
Public
  Constructor Create(aCanvas : TCanvas); Reintroduce;

  Procedure Circle(xcenter,ycenter,Radius, Rotate : Double); Override;
  Procedure Line(x,y,xx,yy : Double); Override;
  Procedure Text(x,y : Double; Text : String); Override;
end;

implementation

{ GDIRenderer }

procedure GDIRenderer.Circle(xcenter, ycenter, Radius, Rotate: Double);
begin
  FCanvas.Ellipse(Round(xcenter-Radius),Round(ycenter-radius),Round(xcenter+radius),Round(ycenter+radius));
end;

constructor GDIRenderer.Create(aCanvas: TCanvas);
begin
  Assert(Assigned(aCanvas));
  FCanvas:=aCanvas;
end;

procedure GDIRenderer.Line(x, y, xx, yy: Double);
begin
  FCanvas.MoveTo(Round(x),Round(y));
  FCanvas.LineTo(Round(xx),Round(yy));
end;

//procedure GDIRenderer.Rectangle(xcenter, ycenter, Width, height,
//  Rotate: Double);
//begin
//  Fcanvas.Rectangle(Round(xcenter-Width/2),Round(ycenter-Height/2),Round(xcenter+Width/2),Round(ycenter+Height/2));
//end;

procedure GDIRenderer.Text(x, y : Double; Text: String);
begin
  FCanvas.TextOut(Round(x),Round(y),Text);
end;

end.
