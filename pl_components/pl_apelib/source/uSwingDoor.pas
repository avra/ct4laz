{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uSwingDoor;

interface


Uses uGroup, uRectangleParticle, uCircleParticle, uSpringConstraint, uApeEngine, uRender;

Type

SwingDoor = class(Group)
Private
Public
  Constructor Create(Render : AbstractRenderer; aEngine : ApeEngine); reintroduce; Virtual;
End;


implementation

{ SwingDoor }

constructor SwingDoor.Create(Render: AbstractRenderer; aEngine: ApeEngine);
var swingDoorP1,swingDoorP2, swingDoorAnchor : CircleParticle;
    swingdoor, swingDoorSpring : SpringConstraint;
    StopperA : CircleParticle;
    StopperB : RectangleParticle;
begin
  inherited Create;
	// setting collideInternal allows the arm to hit the hidden stoppers.
	// you could also make the stoppers its own group and tell it to collide
	// with the SwingDoor
	collideInternal := true;

	swingDoorP1 := CircleParticle.Create(543,55,7);
	swingDoorP1.mass := 0.001;
	swingDoorP1.SetRenderer(Render);
	addParticle(swingDoorP1);

  swingDoorP2 := CircleParticle.Create(620,55,7,true);
	swingDoorP2.SetRenderer(Render);
	addParticle(swingDoorP2);

  swingDoor := SpringConstraint.Create(swingDoorP1, swingDoorP2, 1, true, 13);
	swingDoor.SetRenderer(Render);
	addConstraint(swingDoor);

	swingDoorAnchor := CircleParticle.create(543,5,2,true);
	swingDoorAnchor.visible := false;
	swingDoorAnchor.collidable := false;
  swingDoorAnchor.SetRenderer(Render);
	addParticle(swingDoorAnchor);

	swingDoorSpring := SpringConstraint.Create(swingDoorP1, swingDoorAnchor, 0.02);
	swingDoorSpring.restLength := 40;
	swingDoorSpring.visible := false;
  swingDoorSpring.SetRenderer(Render);
	addConstraint(swingDoorSpring);

  stopperA := CircleParticle.create(550,-60,70,true);
	stopperA.visible := false;
  StopperA.SetRenderer(Render);
	addParticle(stopperA);

	stopperB := RectangleParticle.create(650,130,42,70,0,true);
	stopperB.visible := false;
  StopperB.SetRenderer(Render);
	addParticle(stopperB);
end;

end.
