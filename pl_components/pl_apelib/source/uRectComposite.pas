{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uRectComposite;

interface


Uses uGroup, uCircleParticle, uSpringConstraint, uApeEngine, uRender, uComposite, uVector;

Type

RectCOmposite = class(Composite)
Private
Public
  CpA : CircleParticle;
  CpC : CircleParticle;

  Constructor Create(Render : AbstractRenderer; aEngine : ApeEngine; Ctr : Vector); reintroduce; Virtual;
End;


implementation

{ RectCOmposite }

constructor RectComposite.Create(Render: AbstractRenderer;
  aEngine: ApeEngine; Ctr: Vector);
var rw, rh, rad : Double;
    cpb, cpd : CircleParticle;
    spra, sprb, sprc, sprd : SpringConstraint;
begin
  Inherited Create;
  // just hard coding here for the purposes of the demo, you should pass
  // everything in the constructor to do it right.
  rw := 75;
  rh := 18;
  rad := 4;

  // going clockwise from left top..
  cpA := CircleParticle.Create(ctr.x-rw/2, ctr.y-rh/2, rad, true);
  cpB := CircleParticle.Create(ctr.x+rw/2, ctr.y-rh/2, rad, true);
  cpC := CircleParticle.Create(ctr.x+rw/2, ctr.y+rh/2, rad, true);
  cpD := CircleParticle.Create(ctr.x-rw/2, ctr.y+rh/2, rad, true);

  cpA.SetRenderer(Render);
  cpB.SetRenderer(Render);
  cpC.SetRenderer(Render);
  cpD.SetRenderer(Render);

  sprA := SpringConstraint.Create(cpA,cpB,0.5,true,rad * 2);
  sprB := SpringConstraint.Create(cpB,cpC,0.5,true,rad * 2);
  sprC := SpringConstraint.Create(cpC,cpD,0.5,true,rad * 2);
  sprD := SpringConstraint.Create(cpD,cpA,0.5,true,rad * 2);

  sprA.SetRenderer(Render);
  sprB.SetRenderer(Render);
  sprC.SetRenderer(Render);
  sprD.SetRenderer(Render);

  addParticle(cpA);
  addParticle(cpB);
  addParticle(cpC);
  addParticle(cpD);

  addConstraint(sprA);
  addConstraint(sprB);
  addConstraint(sprC);
  addConstraint(sprD);
end;

end.
