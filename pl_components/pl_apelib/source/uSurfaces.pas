{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uSurfaces;

interface

Uses uGroup, uRectangleParticle, uCircleParticle, uSpringConstraint, uApeEngine, uRender;

Type

Surfaces = class(Group)
Private
Public
  Constructor Create(Render : AbstractRenderer; aEngine : ApeEngine); reintroduce; Virtual;
End;

implementation

{ Surfaces }

constructor Surfaces.Create(Render: AbstractRenderer; aEngine: ApeEngine);
var Floor, Ceil, RampRight, RampLeft, rampLeft2, BouncePad : RectangleParticle;
    RampCircle, FloorBump : CircleParticle;
    leftWall, leftWallChannelInner, leftWallChannel, leftWallChannelAng,
    topLeftAng, rightWall, bridgeStart,bridgeEnd:RectangleParticle;

begin
  Inherited Create(False);
  floor := RectangleParticle.Create(340,327,550,50,0,true);
  Floor.SetRenderer(Render);
  addParticle(floor);

  ceil := RectangleParticle.Create(325,-33,649,80,0,true);
  ceil.SetRenderer(Render);
  addParticle(ceil);

  rampRight := RectangleParticle.Create(375,220,390,20,0.405,true);
  rampRight.SetRenderer(Render);
  addParticle(rampRight);

  rampLeft := RectangleParticle.Create(90,200,102,20,-0.7,true);
  rampLeft.SetRenderer(Render);
  addParticle(rampLeft);

  rampLeft2 := RectangleParticle.Create(96,129,102,20,-0.7,true);
  rampLeft2.SetRenderer(Render);
  addParticle(rampLeft2);

  rampCircle := CircleParticle.Create(175,190,60,true);
  rampCircle.SetRenderer(Render);
  addParticle(rampCircle);

  floorBump := CircleParticle.Create(600,660,400,true);
  floorBump.SetRenderer(Render);
  addParticle(floorBump);

  bouncePad := RectangleParticle.Create(35,370,40,60,0,true);
  bouncePad.SetRenderer(Render);
  bouncePad.elasticity := 4;
  addParticle(bouncePad);

  leftWall := RectangleParticle.Create(1,99,30,500,0,true);
  leftWall.SetRenderer(Render);
  addParticle(leftWall);

  leftWallChannelInner := RectangleParticle.Create(54,300,20,150,0,true);
  leftWallChannelInner.SetRenderer(Render);
  addParticle(leftWallChannelInner);

  leftWallChannel := RectangleParticle.Create(54,122,20,94,0,true);
  leftWallChannel.SetRenderer(Render);
  addParticle(leftWallChannel);

  leftWallChannelAng := RectangleParticle.Create(75,65,60,25,- 0.7,true);
  leftWallChannelAng.SetRenderer(Render);
  addParticle(leftWallChannelAng);

  topLeftAng := RectangleParticle.Create(23,11,65,40,-0.7,true);
  topLeftAng.SetRenderer(Render);
  addParticle(topLeftAng);

  rightWall := RectangleParticle.Create(654,230,50,500,0,true);
  rightWall.SetRenderer(Render);
  addParticle(rightWall);

  bridgeStart := RectangleParticle.Create(127,49,75,25,0,true);
  bridgeStart.SetRenderer(Render);
  addParticle(bridgeStart);

  bridgeEnd := RectangleParticle.Create(483,55,100,15,0,true);
  bridgeEnd.SetRenderer(Render);
  addParticle(bridgeEnd);
end;

end.
