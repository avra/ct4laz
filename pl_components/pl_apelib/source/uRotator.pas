{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uRotator;

interface


Uses uGroup, uCircleParticle, uSpringConstraint, uApeEngine,
    uRectangleParticle, uRender, uRectComposite, uVector;

Type

Rotator = class(Group)
Private
  ctr : Vector;
  arectcomposite : RectComposite;
Public
  Constructor Create(Render : AbstractRenderer; aEngine : ApeEngine); reintroduce; Virtual;

  function RotateByRadian( a : Double) : Double;
End;


implementation

{ Rotator }

constructor Rotator.Create(Render: AbstractRenderer; aEngine: ApeEngine);
var circA : CircleParticle;
  rectA, rectB : RectanglePArticle;
  ConnectorA, ConnectorB : SpringConstraint;
begin
  inherited Create;
	collideInternal := true;

  ctr := Vector.Create(555,175);
	arectComposite := RectComposite.Create(Render, aEngine, ctr);
	addComposite(arectComposite);

  circA := CircleParticle.create(ctr.x,ctr.y,5);
	circA.SetRenderer(Render);
	addParticle(circA);

	rectA := RectangleParticle.Create(555,160,10,10,0,false,3);
	rectA.SetRenderer(Render);
	addParticle(rectA);

	connectorA := SpringConstraint.create(arectComposite.CpA, rectA, 1);
	connectorA.SetRenderer(Render);
	addConstraint(connectorA);

	rectB := RectangleParticle.Create(555,190,10,10,0,false,3);
	rectB.SetRenderer(Render);
	addParticle(rectB);

	connectorB := SpringConstraint.Create(arectComposite.cpc, rectB, 1);
	connectorB.SetRenderer(render);
	addConstraint(connectorB);
end;

function Rotator.RotateByRadian(a: Double): Double;
begin
  arectcomposite.RotateByRadian(a,ctr);
end;

end.
