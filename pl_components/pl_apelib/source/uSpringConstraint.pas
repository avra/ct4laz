{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uSpringConstraint;

interface

uses uAbstractConstraint, uAbstractParticle, uMathUtil, uVector, MAth, SysUtils;

Type
SpringConstraint = Class(AbstractConstraint)
Private
	p1:AbstractParticle;
	p2:AbstractParticle;

	_restLength : Double;
	_collidable : Boolean;
	_scp : AbstractParticle;

  function GetCurrLenght: Double;
  function GetCenter: Vector;
    function GetAngle: Double;
    function GetRadian: Double;
    function GetRectHeight: Double;
    function GetRectScale: Double;
    function GetDelta: Vector;
    procedure SetRestLength(const Value: Double);
    function GetFixedEndLimit: Double;
    procedure SetFixedEndLimit(const Value: Double);
    function GetFixed: Boolean;
Public
  constructor Create( ap1,ap2 : AbstractParticle; aStiffness : Double = 0.5; Collidable : Boolean = False;
                    rectHeight : Double = 1; rectScale : Double = 1; ScaleToLEngth : Boolean = False);

  Procedure checkParticlesLocation;
  Procedure Resolve; OVerride;
  procedure SetCollidable(b : boolean; aRectHeight, aRectScale : Double; ScaleLen : Boolean = false);
  Function IsConnectedTo(p : AbstractParticle) : Boolean;

  Procedure Paint; OverridE;

  Property CurrLength : Double read GetCurrLenght;
  property Center : Vector read GetCenter;
  property Angle : Double read GetAngle;
  property Radian : Double Read GetRadian;
  property RectScale : Double read GetRectScale;
  Property RectHeight : Double Read GetRectHEight;
  property Delta : Vector read GetDelta;
  property RestLength : Double read _restLength Write SetRestLength;
  property Collidable : Boolean read _collidable Write _collidable;
  property SCP : AbstractParticle read _scp;
  property FixedEndLimit : Double read GetFixedEndLimit Write SetFixedEndLimit;
  PRoperty Fixed : Boolean read GetFixed; 

end;


implementation

uses uSpringConstraintParticle, uAbstractItem;

{ SpringConstraint }


//* if the two particles are at the same location offset slightly
procedure SpringConstraint.checkParticlesLocation;
begin
  if ((p1.curr.x = p2.curr.x) and (p1.curr.y = p2.curr.y)) Then
	  p2.curr.x := p2.curr.x + 0.0001;
end;

constructor SpringConstraint.Create( ap1,ap2 : AbstractParticle; aStiffness : Double = 0.5; Collidable : Boolean = False;
                    rectHeight : Double = 1; rectScale : Double = 1; ScaleToLEngth : Boolean = False);
begin
  inherited Create(aStiffness);
  Self.p1 := ap1;
  Self.p2 := ap2;
  checkParticlesLocation;
  _restLength := CurrLength;
  SetCollidable(Collidable,rectHeight,rectScale,ScaleToLength);
end;

function SpringConstraint.GetAngle: Double;
begin
  Result := radian * ONE_EIGHTY_OVER_PI;
end;

function SpringConstraint.GetCenter: Vector;
begin
  Result :=(p1.curr.plus(p2.curr)).divEquals(2);
end;

function SpringConstraint.GetCurrLenght: Double;
begin
  result := p1.Curr.Distance(p2.Curr);
end;


function SpringConstraint.GetDelta: Vector;
begin
  Result := p1.curr.minus(p2.curr);
end;

function SpringConstraint.GetFixed: Boolean;
begin
  Result := p1.Fixed And p2.Fixed;
end;

function SpringConstraint.GetFixedEndLimit: Double;
begin
  Result := SpringConstraintParticle(_scp).FixedEndLimit;
end;

function SpringConstraint.GetRadian: Double;
var d : Vector;
begin
  d := Delta;
	Result := Arctan2(d.y, d.x);
end;

function SpringConstraint.GetRectHeight: Double;
begin
  Result := SpringConstraintPArticle(_scp).RectHeight;
end;

function SpringConstraint.GetRectScale: Double;
begin
  Result := SpringConstraintPArticle(_scp).RectScale;
end;

function SpringConstraint.IsConnectedTo(p: AbstractParticle): Boolean;
begin
  Result := (p = p1) Or (p=p2);
end;

procedure SpringConstraint.Paint;
begin
//  inherited;
  if Collidable then
  begin
    //aRenderer.Line(p1.px,p1.py,p2.px,p2.py);
    SpringConstraintParticle(_scp).Paint
  end
  else
    aRenderer.Line(p1.px,p1.py,p2.px,p2.py);
end;

procedure SpringConstraint.Resolve;
var DeltaLength : Double;
    Diff : Double;
    dmds : Vector;
begin
  if p1.Fixed and p2.Fixed then
    Exit;

  deltaLength := currLength;
  diff := (deltaLength - restLength) / (deltaLength * (p1.invMass + p2.invMass));
	dmds := delta.mult(diff * stiffness);

  p1.curr.minusEquals(dmds.mult(p1.invMass));
	p2.curr.plusEquals (dmds.mult(p2.invMass));
end;

procedure SpringConstraint.SetCollidable(b: boolean; aRectHeight,
  aRectScale: Double; ScaleLen: Boolean);
begin
  _collidable := b;
  _scp := nil;
  if _collidable then
  begin
    _scp := SpringConstraintPArticle.Create(p1, p2, self, arectHeight, aRectScale, ScaleLen);
  end;
end;

procedure SpringConstraint.SetFixedEndLimit(const Value: Double);
begin
  if Assigned(_scp) then
    SpringConstraintParticle(_scp).FixedEndLimit := Value;
end;

procedure SpringConstraint.SetRestLength(const Value: Double);
begin
  if Value<=0 then
    raise Exception.Create('SpringConstant.RestLength must be grater than 0');
  _RestLength := Value;
end;

end.
