{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uGroup;

interface

uses uAbstractCollection, uvector, Classes, uComposite, uAbstractPArticle, uAbstractConstraint;

Type

Group = Class(AbstractCollection)
Private
  _CollideInternal : Boolean;

  Procedure CheckCollisionGroupInternal;
  Procedure CheckCollisionVsGroup(g : Group);

public
  Composites : TList;
  CollisionList : TList;

  constructor Create(CollideInternal : Boolean = False); Reintroduce; Virtual;
  Procedure Init; Override;

  Procedure AddComposite(c : Composite);
  Procedure RemoveComposite(c : Composite);

  Procedure Paint; Override;

  Procedure AddCollidable(g : Group);
  Procedure RemoveCollidable(g : Group);

  Procedure AddCollidableList(list : Tlist);
  Function GetAll : Tlist;

  //Function Cleanup ------> destructor.

  Procedure Integrate(dt2 : Double; Force, MassLessForce : Vector; Damping : Double); Override;
  Procedure SatisfyConstraints; Override;
  Procedure CheckCollision;

  Property CollideInternal : Boolean read _CollideInternal Write _CollideInternal;

end;

implementation

uses MaskUtils;

{ Group }

procedure Group.AddCollidable(g: Group);
begin
  CollisionList.Add(g);
end;

procedure Group.AddCollidableList(list: Tlist);
var i : Integer;
begin
  for i:=0 to list.Count-1 do
    CollisionList.Add(List[i]);
end;

procedure Group.AddComposite(c: Composite);
begin
  Composites.Add(c);
  c.IsParented := true;
  if IsParented then
    c.Init;
end;

procedure Group.CheckCollision;
var i : integer;
begin
  if CollideInternal then
    CheckCollisionGroupInternal;

  for i:=0 to CollisionList.Count-1 do
  begin
    CheckCollisionVsGroup(Group(CollisionList[i]));
  end;
end;

procedure Group.CheckCollisionGroupInternal;
var i,j : Integer;
    ca : Composite;
begin
  CheckInternalCollision;
  for j:=0 to Composites.Count-1 do
  begin
    ca:=Composite(Composites[j]);
    ca.CheckCollisionVsCollection(Self);
    for i:=j+1 to Composites.Count-1 do
      ca.CheckCollisionVsCollection(Composite(Composites[i]));
  end;
end;

procedure Group.CheckCollisionVsGroup(g : Group);
var clen, gclen : Integer;
    i,j : integer;
    c : Composite;
begin
  CheckCollisionVsCollection(g);
  clen := Composites.Count;
  gclen := g.Composites.Count;
  for i:=0 to clen-1 do
  begin
    c:=Composite(Composites[i]);
    c.CheckCollisionVsCollection(g);
    for j:=0 to gclen -1 do
      c.CheckCollisionVsCollection(AbstractCollection(g.Composites[j]));
  end;

  For j:=0 to gclen-1 do
    CheckCollisionVsCollection(Composite(g.Composites[j]));

end;

constructor Group.Create(CollideInternal: Boolean);
begin
  inherited Create;
  _CollideInternal:=CollideInternal;
  Composites := TList.Create;
  CollisionList := Tlist.Create;
end;

function Group.GetAll: Tlist;
var i : integer;
begin
  Result:=Tlist.create;
  for i:=0 to Particles.Count-1 do
    Result.Add(Particles[i]);
  for i:=0 to Constraints.Count-1 do
    Result.Add(Constraints[i]);
  for i:=0 to Composites.Count-1 do
    Result.Add(Composites[i]);
end;

procedure Group.Init;
var i : integer;
begin
  //inherited Init;
  For i:=0 to Composites.Count-1 do
    Composite(Composites[i]).Init;
end;

procedure Group.Integrate(dt2 : Double; Force, MassLessForce : Vector; Damping : Double);
var i : integer;
begin
  inherited Integrate(dt2,Force,MasslessForce,Damping);
  for i:=0 to Composites.Count-1 do
    Composite(Composites[i]).Integrate(dt2,Force,MAssLessForce,Damping);
end;

procedure Group.Paint;
var i : integer;
begin
  //Inherited Paint;
  for i:=0 to Composites.Count-1 do
    Composite(Composites[i]).Paint;

  for i:=0 to Particles.Count-1 do
    AbstractParticle(Particles[i]).Paint;

  for i:=0 to Constraints.Count-1 do
    AbstractConstraint(Constraints[i]).Paint;

end;

procedure Group.RemoveCollidable(g: Group);
begin
  CollisionList.Remove(g);
end;

procedure Group.removeComposite(c: Composite);
begin
  Composites.Remove(c);
  c.IsParented:=False;
  //c.Free; ?
end;

procedure Group.SatisfyConstraints;
var i : integer;
begin
  inherited SatisfyConstraints;
  for i:=0 to Composites.Count-1 do
    Composite(Composites[i]).SatisfyConstraints;
end;

end.
