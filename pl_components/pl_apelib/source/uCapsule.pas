{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uCapsule;

interface


Uses uGroup, uCircleParticle, uSpringConstraint, uApeEngine, uRender;

Type

Capsule = class(Group)
Private
Public
  Constructor Create(Render : AbstractRenderer; aEngine : ApeEngine); reintroduce; Virtual;
End;


implementation

{ Capsule }

constructor Capsule.Create(Render: AbstractRenderer; aEngine: ApeEngine);
var capsuleP1, capsuleP2 : CircleParticle;
    Capsule : SpringConstraint;
begin
  inherited Create;
  capsuleP1 := CircleParticle.Create(300,10,14,false,1.3,0.4);
	capsuleP1.SetRenderer(Render);
	addParticle(capsuleP1);

  capsuleP2 := CircleParticle.Create(325,35,14,false,1.3,0.4);
	capsuleP2.SetRenderer(render);
	addParticle(capsuleP2);

  capsule := SpringConstraint.Create(capsuleP1, capsuleP2, 1, true, 24);
	capsule.SetRenderer(Render);
	addConstraint(capsule);
end;

end.
