{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit ucar;

interface

Uses uGroup, uWheelParticle, uSpringConstraint, uApeEngine, uRender;

type

Car = class(Group)
private
    wheelparticleA : WheelParticle;
    wheelparticleB : WheelParticle;
    wheelconnector : SpringConstraint;

    function GetSpeed: Double;
    procedure SetSpeed(const Value: Double);

public
   Constructor Create(aRenderer : AbstractRenderer; anApeEngine : ApeEngine); Reintroduce;

   property Speed : Double read GetSpeed Write SetSpeed;

end;

implementation

uses uAbstractCollection;

{ Car }

constructor Car.Create(aRenderer : AbstractRenderer; anApeEngine : ApeEngine);
begin
  inherited Create(True);
  wheelparticleA := WheelParticle.Create(anApeEngine,140,10,14,False,2);
  wheelparticleA.SetRenderer(aRenderer);
  wheelparticleB := WheelParticle.Create(anApeEngine,200,10,14,False,2);
  wheelparticleB.SetRenderer(aRenderer);
  wheelconnector := SpringConstraint.Create(wheelparticleA,wheelparticleB,0.5,True,8);
  wheelconnector.SetRenderer(aRenderer);

  AddParticle(wheelparticleA);
  AddParticle(wheelparticleB);
  AddConstraint(wheelconnector);
end;

function Car.GetSpeed: Double;
begin
  result :=(wheelparticleA.AngularVelocity + wheelparticleB.AngularVelocity) / 2;
end;

procedure Car.SetSpeed(const Value: Double);
begin
  wheelparticleA.AngularVelocity:=Value;
  wheelparticleb.AngularVelocity:=Value;
end;

end.
