{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uCircleParticle;

interface

Uses uAbstractParticle, uVector, uInterval;

Type

CircleParticle = class(AbstractParticle)
Private
  _Radius : Double;
Public

		{*
		 * @param x The initial x position of this particle.
		 * @param y The initial y position of this particle.
		 * @param radius The radius of this particle.
		 * @param fixed Determines if the particle is fixed or not. Fixed particles
		 * are not affected by forces or collisions and are good to use as surfaces.
		 * Non-fixed particles move freely in response to collision and forces.
		 * @param mass The mass of the particle.
		 * @param elasticity The elasticity of the particle. Higher values mean more elasticity or 'bounciness'.
		 * @param friction The surface friction of the particle.
		 *}
  Constructor Create( x,y,radius : Double;
                      aFixed : Boolean = False;
                      aMass : Double = 1;
                      aElasticity : Double = 0.3;
                      aFriction : Double = 0); Reintroduce;

  Procedure Init; Override;
  Procedure Paint; OVerride;
  Function GetProjection(AnAxis : Vector) : Interval;
  Function GetIntervalX : Interval;
  Function GetIntervalY : Interval;

  Property Radius : Double read _Radius Write _Radius;
End;

implementation

{ CircleParticle }

constructor CircleParticle.Create( x,y,radius : Double;
                                  aFixed : Boolean = False;
                                  aMass : Double = 1;
                                  aElasticity : Double = 0.3;
                                  aFriction : Double = 0);
begin
  inherited Create(x,y,amass,aelasticity,afriction,afixed);
  _Radius := Radius;
end;

function CircleParticle.GetIntervalX: Interval;
begin
  ainterval.min := curr.x - _radius;
  ainterval.max := curr.x + _radius;
	result := ainterval;
end;

function CircleParticle.GetIntervalY: Interval;
begin
  ainterval.min := curr.y - _radius;
  ainterval.max := curr.y + _radius;
  Result := ainterval;
end;

function CircleParticle.GetProjection(AnAxis: Vector): Interval;
var c : Double;
begin
  c := Samp.Dot(anAxis);
  aInterval.min := c - _radius;
  ainterval.max := c + _radius;
  Result := aInterval;
end;

procedure CircleParticle.Init;
begin
  //inherited ;
  //none ?
end;

procedure CircleParticle.Paint;
begin
 // inherited;
  aRenderer.Circle(curr.x,curr.y,_Radius,0);
end;

end.
