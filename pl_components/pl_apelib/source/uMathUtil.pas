{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uMathUtil;

interface

Uses MAth;

Const
  ONE_EIGHTY_OVER_PI = 180 / PI;
  PI_OVER_ONE_EIGHTY = PI / 180;


  Function Clamp(n,min,max : Double) : Double;
  Function Sign(val : Double) : Integer;



implementation

  Function Clamp(n,min,max : Double) : Double;
  begin
    If n<min then
    begin
      result := min;
      exit;
    end
    else
    if n> max then
    begin
      Result:=max;
      Exit;
    end
    else
      Result:=n;
  end;

  Function Sign(val : Double) : Integer;
  begin
    if val<0 then
      result:=-1
    else
      Result:=1;
  end;


end.
