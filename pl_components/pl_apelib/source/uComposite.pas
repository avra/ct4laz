{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uComposite;

interface

Uses uVector, uAbstractCollection, MAth, uAbstractPArticle, uMathUtil, Classes,
     uSpringConstraint;

Type

Composite = Class(AbstractCollection)
Private
  delta : Vector;
  function GetFixed: Boolean;
  procedure SetFixed(const Value: Boolean);
Public

  Constructor Create; Override;

  Procedure RotateByRadian(AngleRadian : double; Center : Vector);
  Procedure RotateByAngle(AngleDegree : Double; Center : Vector);
  Function GetRelativeAngle(Center, p : Vector) : Double;

  Property Fixed : Boolean read GetFixed Write SetFixed;

end;

implementation

{ Composite }

constructor Composite.Create;
begin
  inherited Create;
  Delta := Vector.Create(0,0);
end;

function Composite.GetFixed: Boolean;
var i : integer;
begin
  result:=true;
  for i:=0 to Particles.Count-1 do
  begin
    if not AbstractPArticle(Particles[i]).Fixed then
    begin
      Result:=False;
      exit;
    end;
  end;
end;

function Composite.GetRelativeAngle(Center, p: Vector): Double;
begin
  Delta.SetTo(p.x-Center.x,p.y-center.y);
  Result := ArcTan2(delta.y,delta.x);
end;


procedure Composite.RotateByAngle(AngleDegree: Double; Center: Vector);
var angleRadians : Double;
begin
  AngleRadians := AngleDegree * PI_OVER_ONE_EIGHTY;
  RotateByRadian(angleRadians, Center);
end;

procedure Composite.RotateByRadian(AngleRadian: double; Center: Vector);
var p : AbstractPArticle;
    pa : TList;
    len : Integer;
    radius, angle : Double;
    i : integer;
begin
  pa:=Particles;
  len:=pa.Count;
  For i:=0 to len-1 do
  begin
    p:=AbstractPArticle(pa[i]);
    Radius := p.center.distance(center);
    Angle := GetRelativeAngle(Center,p.Center) + AngleRadian;
    p.Px := (Cos(Angle) * Radius) + center.x;
    p.Py := (Sin(Angle) * Radius) + center.y;
  end;
end;

procedure Composite.SetFixed(const Value: Boolean);
var i : integer;
begin
  for i:=0 to Particles.Count-1 do
    AbstractPArticle(Particles[i]).Fixed:=Value;
end;

end.
