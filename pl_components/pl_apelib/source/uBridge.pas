{**********************************************************************
                   PilotLogic Software House
                
  Package pl_APELib
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
    
  Converted to ObjectPascal from APE (Actionscript Physics Engine)
  by Vincent Gsell     
                                                                               
   ****** BEGIN LICENSE BLOCK *****     
                                                              
   The contents of this file are used with permission, subject to the Mozilla   
   Public License Version 2.0 (the "License"); you may not use this file except 
   in compliance with the License. You may obtain a copy of the License at      
   https://www.mozilla.org/en-US/MPL/2.0/                                     
                                                                               
   Software distributed under the License is distributed on an "AS IS" basis,   
   WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for 
   the specific language governing rights and limitations under the License. 
   
   ****** END LICENSE BLOCK *****  

************************************************************************}
  
unit uBridge;

interface

Uses uGroup, uCircleParticle, uSpringConstraint, uApeEngine, uRender;

Type

Bridge = class(Group)
Private
Public
  Constructor Create(Render : AbstractRenderer; aEngine : ApeEngine); reintroduce; Virtual;
End;

implementation



{ Bridge }

constructor Bridge.Create(Render : AbstractRenderer; aEngine : ApeEngine);
var bx : Double;
    By : Double;
    bsize : Double;
    yslope : Double;
    ParticleSize : Double;

    bridgePAA, bridgePA, bridgePB, bridgePC, bridgePD, bridgePDD : CircleParticle;
    bridgeConnA, bridgeConnB, bridgeConnC, bridgeConnD, bridgeConnE : SpringConstraint;
begin
  inherited Create(False);
  bx := 170;
  By := 40;
  bsize := 51.5;
  yslope := 2.4;
  ParticleSize := 4;

	bridgePAA:= CircleParticle.Create(bx,by,particleSize,true);
	addParticle(bridgePAA);

	bx := bx + bsize;
	by := By + yslope;

	bridgePA := CircleParticle.Create(bx,by,particleSize);
	addParticle(bridgePA);

	bx := bx + bsize;
	by := By + yslope;
	bridgePB := CircleParticle.Create(bx,by,particleSize);
	addParticle(bridgePB);

	bx := bx + bsize;
	by := By + yslope;
	bridgePC := CircleParticle.Create(bx,by,particleSize);
	addParticle(bridgePC);

	bx := bx + bsize;
	by := By + yslope;
  bridgePD := CircleParticle.Create(bx,by,particleSize);
	addParticle(bridgePD);

	bx := bx + bsize;
	by := By + yslope;
	bridgePDD := CircleParticle.Create(bx,by,particleSize,true);
	addParticle(bridgePDD);


  bridgeConnA := SpringConstraint.Create(bridgePAA, bridgePA, 0.9, true, 10, 0.8);

	// collision response on the bridgeConnA will be ignored on
	// on the first 1/4 of the constraint. this avoids blow ups
	// particular to springcontraints that have 1 fixed particle.
	bridgeConnA.FixedEndLimit := 0.25;
	addConstraint(bridgeConnA);

	bridgeConnB := SpringConstraint.Create(bridgePA, bridgePB,0.9, true, 10, 0.8);
	addConstraint(bridgeConnB);

  bridgeConnC := SpringConstraint.Create(bridgePB, bridgePC,0.9, true, 10, 0.8);
	addConstraint(bridgeConnC);

  bridgeConnD := SpringConstraint.Create(bridgePC, bridgePD,	0.9, true, 10, 0.8);
	addConstraint(bridgeConnD);

  bridgeConnE := SpringConstraint.Create(bridgePD, bridgePDD,	0.9, true, 10, 0.8);
	bridgeConnE.fixedEndLimit := 0.25;
  addConstraint(bridgeConnE);

  bridgePAA.SetRenderer(Render);
  bridgePA.SetRenderer(Render);
  bridgePB.SetRenderer(Render);
  bridgePC.SetRenderer(Render);
  bridgePD.SetRenderer(Render);
  bridgePDD.SetRenderer(Render);

  bridgeConnA.SetRenderer(Render);
  bridgeConnB.SetRenderer(Render);
  bridgeConnC.SetRenderer(Render);
  bridgeConnD.SetRenderer(Render);
  bridgeConnE.SetRenderer(Render);


end;

end.
