{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_titanscript;

{$warn 5023 off : no warning about unused units}
interface

uses
  AllTitanScripterRegister, BASE_Consts, BASE_Engine, BASE_Lists, 
  BASE_RegExpr, BASE_Sync, Titan_Debug, titan_frm, Titan_Import, 
  titan_import_activex, Titan_PascalLanguage, Titan_PascalRegFunctions, 
  Titan_PascalScanner, Titan_RTTI, titan_runtimeapidata, titan_runtimeapidlg, 
  titan_runtimeapiexplorer, titan_runtimeapirtti, TitanScripter, 
  TitanScripterBaseEngine, TitanScripterBaseEngineMsg, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('AllTitanScripterRegister', @AllTitanScripterRegister.Register);
end;

initialization
  RegisterPackage('pl_titanscript', @Register);
end.
