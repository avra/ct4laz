{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}


unit Titan_Import;

interface

uses
  TypInfo;

type
  TTitanRegisterNamespace = function (const Name: String;
                            OwnerIndex: Integer = -1;
                            UserData: Integer = 0): Integer;
  TTitanRegisterClassType = function (PClass: TClass;
                            OwnerIndex: Integer = -1;
                            UserData: Integer = 0): Integer;

  TTitanRegisterRTTIType = function (pti: PTypeInfo;
                                  UserData: Integer = 0): Integer;
  TTitanRegisterMethod = procedure (PClass: TClass; const Header: String;
                                  Address: Pointer;
                                  Fake: Boolean = false;
                                  UserData: Integer = 0);
  TTitanRegisterBCBMethod = procedure (PClass: TClass; const Header: String;
                                     Address: Pointer;
                                     Fake: Boolean = false;
                                     UserData: Integer = 0);
  TTitanRegisterProperty = procedure (PClass: TClass;
                                    const PropDef: String;
                                    UserData: Integer = 0);
  TTitanRegisterRoutine = procedure (const Header: String; Address: Pointer;
                                   OwnerIndex: Integer = -1;
                                   UserData: Integer = 0);

  TTitanRegisterConstant = procedure (const Name: String; const Value: Variant;
                                   OwnerIndex: Integer = -1;
                                   UserData: Integer = 0);
  TTitanRegisterVariable = procedure (const Name, TypeName: String; Address: Pointer;
                                    OwnerIndex: Integer = -1;
                                    UserData: Integer = 0);

  TRegisterInterfaceType = function (const Name: String; const Guid: TGuid;
                               const ParentName: String; const ParentGuid: TGUID;
                               OwnerIndex: Integer = -1;
                               UserData: Integer = 0): Integer;

  TRegisterInterfaceMethod = procedure (const Guid: TGUID; const Header: String;
                                  MethodIndex: Integer = -1;
                                  UserData: Integer = 0);

  TRegisterInterfaceProperty =  procedure (const guid: TGUID; const PropDef: String;
                           UserData: Integer = 0);

  TTitanRegisterProcs = record
    RegisterNamespace: TTitanRegisterNamespace;
    RegisterClassType: TTitanRegisterClassType;
    RegisterRTTIType: TTitanRegisterRTTIType;
    RegisterMethod: TTitanRegisterMethod;
    RegisterBCBMethod: TTitanRegisterBCBMethod;
    RegisterProperty: TTitanRegisterProperty;
    RegisterRoutine: TTitanRegisterRoutine;
    RegisterConstant: TTitanRegisterConstant;
    RegisterVariable: TTitanRegisterVariable;
    RegisterInterfaceType: TRegisterInterfaceType;
    RegisterInterfaceMethod: TRegisterInterfaceMethod;
    RegisterInterfaceProperty: TRegisterInterfaceProperty;
  end;

  TTitanRegisterDllProc = procedure (R: TTitanRegisterProcs);

implementation

end.

