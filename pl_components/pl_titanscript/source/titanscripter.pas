{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit TitanScripter;

interface

uses

LCLIntf, LCLType, LMessages,
  Variants,
  SysUtils,
  TypInfo,
  Classes,
  ComCtrls,
  dynlibs,
  BASE_CONSTS,
  BASE_SYNC,
  BASE_Engine,
  titan_Import;
const
  rmRun = 0;
  rmStepOver = 1;
  rmTraceInto = 2;
  rmRunToCursor = 3;
  rmTraceToNextSourceLine = 4;

  TitanVersion: Double = 5.0;
  TitanBuild = '';
  TitanCompiledModuleVersion: Integer = BASE_Engine._CompiledModuleVersion;
  MaxModuleNumber = 255;

type
  TScripterState =
  (
  ssInit, // scripter is not assigned by a script
  ssReadyToCompile, // scripter is assigned by a script and ready to compile it.
  ssCompiling, // compiles script
  ssCompiled, // all modules were compiled
  ssLinking, // links modules in a script
  ssReadyToRun, // script was linked and it is ready to run now
  ssRunning, // runs script
  ssPaused, // script was paused
  ssTerminated // script was terminated
  );

  TTitanScripter = class;
  TTitanLanguage = class;

  TCallStackRecord = class
  public
    ModuleName: String;
    LineNumber: Integer;
    ProcName: String;
    Parameters: TStringList;
    constructor Create;
    destructor Destroy; override;
  end;

  TCallStack = class
  private
    fScripter: TTitanBaseScripter;
    fRecords: TList;
    function GetCount: Integer;
    function GetRecord(Index: Integer): TCallStackRecord;
    procedure Clear;
    constructor Create(TitanScripter: TTitanScripter);
    procedure Add(R: TCallStackRecord);
  public
    destructor Destroy; override;
    property Count: Integer read GetCount;
    property Records[I: Integer]: TCallStackRecord read GetRecord;
  end;

  TTitanScripterEvent = procedure (Sender: TTitanScripter) of object;
  TTitanCompilerProgressEvent = procedure(Sender: TTitanScripter; ModuleNumber: Integer) of object;
  TTitanScripterPrintEvent = procedure (Sender: TTitanScripter; const S: String) of object;
  TTitanScripterDefineEvent = procedure (Sender: TTitanScripter; const S: String) of object;
  TTitanScripterReadEvent = procedure (Sender: TTitanScripter; var S: String) of object;

  TTitanScripterVarEvent = procedure (Sender: TTitanScripter; ID: Integer) of object;
  TTitanScripterVarEventEx = procedure (Sender: TTitanScripter; ID: Integer; var Mode: Integer) of object;

  TTitanCodeEvent = procedure(Sender: TTitanScripter;  N: Integer; var Handled: Boolean) of object;

  TTitanUsedModuleEvent = procedure(Sender: TTitanScripter; const UsedModuleName, FileName: String; var SourceCode: String) of object;

  TTitanIncludeEvent = procedure(Sender: TObject; const IncludedFileName: String; var SourceCode: String) of object;

  TTitanLoadSourceCodeEvent = procedure(Sender: TTitanScripter; const UsedModuleName, FileName: String; var SourceCode: String) of object;

  TTitanScanPropertiesEvent = procedure(Sender: TTitanScripter; const PropName: String; var Value: Variant) of object;

  TTitanMemberKind = (mkUnknown, mkConst, mkField, mkProp, mkParam, mkResult, mkMethod, mkClass, mkStructure, mkEnum, mkNamespace, mkEvent);

  TTitanCallConv = (ccRegister, ccPascal, ccCDecl, ccStdCall, ccSafeCall);

  TTitanMemberCallback = procedure (const Name: String;
                                  ID: Integer;
                                  Kind: TTitanMemberKind;
                                  ml: TTitanModifierList;
                                  Data: Pointer) of object;

  TTitanInstruction = record
    N, Op, Arg1, Arg2, Res: Integer;
  end;

  PTitanTreeNodeData = ^TTitanTreeNodeData;
  TTitanTreeNodeData = record
    Value: Variant;
    Modified: Boolean;
    ID: Integer;
    Prop: TTitanProperty;
  end;

  TTitanScripterStreamEvent = procedure(Sender: TTitanScripter; Stream: TStream;
                            const ModuleName: String) of object;

  TTitanScripterChangeStateEvent = procedure(Sender: TTitanScripter; OldState, NewState: Integer) of object;

  TTitanScripterInstanceEvent = procedure(Sender: TTitanScripter; Instance: TObject) of object;

  TTitanLoadDllEvent = procedure(Sender: TTitanScripter; const DllName, ProcName: String; var Address: Pointer) of object;

  TTitanVarArray = array of Variant;

  TTitanVirtualObjectMethodCallEvent = function(Sender: TTitanScripter; const ObjectName,
      PropName: String; const Params: TTitanVarArray): Variant of object;

  TTitanVirtualObjectPutPropertyEvent = procedure(Sender: TTitanScripter; const ObjectName,
      PropName: String; const Params: TTitanVarArray; const Value: Variant) of object;

  TTitanOverrideHandlerMode = (Replace, Before, After);


TTitanScripter = class(TCustomScripter)
  protected
    fCallStack: TCallStack;
    fOnAssignScript,
    fOnAfterCompileStage,
    fOnAfterRunStage,
    fOnBeforeCompileStage,
    fOnBeforeRunStage,
    fOnHalt: TTitanScripterEvent;
    fOnScanProperties: TTitanScanPropertiesEvent;

    function GetModules: TStringList;

    function GetScripterState: TScripterState;
    procedure SetScripterState(Value: TScripterState);

    function GetOverrideHandlerMode: TTitanOverrideHandlerMode;
    procedure SetOverrideHandlerMode(Value: TTitanOverrideHandlerMode);

    procedure ExtractCallStack;
    function  GetSourceCode(const ModuleName: String): String;
    procedure SetSourceCode(const ModuleName, SourceCode: String);
    function  GetOnCompilerProgress: TTitanCompilerProgressEvent;
    procedure SetOnCompilerProgress(Value: TTitanCompilerProgressEvent);
    function  GetOnPrint: TTitanScripterPrintEvent;
    procedure SetOnPrint(Value: TTitanScripterPrintEvent);
    function  GetOnDefine: TTitanScripterDefineEvent;
    procedure SetOnDefine(Value: TTitanScripterDefineEvent);
    function  GetOnChangedVariable: TTitanScripterVarEvent;
    procedure SetOnChangedVariable(Value: TTitanScripterVarEvent);
    function  GetOnRunning: TTitanCodeEvent;
    procedure SetOnRunning(Value: TTitanCodeEvent);
    function  GetOnInclude: TTitanIncludeEvent;
    procedure SetOnInclude(Value: TTitanIncludeEvent);
    function  GetOnHalt: TTitanScripterEvent;
    procedure SetOnHalt(Value: TTitanScripterEvent);

    function  GetOnLoadDll: TTitanLoadDllEvent;
    procedure SetOnLoadDll(Value: TTitanLoadDllEvent);

    function  GetOnVirtualObjectMethodCallEvent: TTitanVirtualObjectMethodCallEvent;
    procedure SetOnVirtualObjectMethodCallEvent(Value: TTitanVirtualObjectMethodCallEvent);

    function  GetOnVirtualObjectPutPropertyEvent: TTitanVirtualObjectPutPropertyEvent;
    procedure SetOnVirtualObjectPutPropertyEvent(Value: TTitanVirtualObjectPutPropertyEvent);

    function  GetOnRunningUpdate: TTitanScripterEvent;
    procedure SetOnRunningUpdate(Value: TTitanScripterEvent);
    function  GetOnRunningUpdateActive: Boolean;
    procedure SetOnRunningUpdateActive(Value: Boolean);
    function  GetOnRunningSync: TTitanScripterEvent;
    procedure SetOnRunningSync(Value: TTitanScripterEvent);

    function  GetOnUndeclaredIdentifier: TTitanScripterVarEventEx;
    procedure SetOnUndeclaredIdentifier(Value: TTitanScripterVarEventEx);

    function  GetOnReadExtraData: TTitanScripterStreamEvent;
    procedure SetOnReadExtraData(Value: TTitanScripterStreamEvent);
    function  GetOnWriteExtraData: TTitanScripterStreamEvent;
    procedure SetOnWriteExtraData(Value: TTitanScripterStreamEvent);
    function  GetOnUsedModule: TTitanUsedModuleEvent;
    procedure SetOnUsedModule(Value: TTitanUsedModuleEvent);
    function  GetOnLoadSourceCode: TTitanLoadSourceCodeEvent;
    procedure SetOnLoadSourceCode(Value: TTitanLoadSourceCodeEvent);
    function  GetOnChangeState: TTitanScripterChangeStateEvent;
    procedure SetOnChangeState(Value: TTitanScripterChangeStateEvent);

    function  GetOnTyphonInstanceCreate: TTitanScripterInstanceEvent;
    procedure SetOnTyphonInstanceCreate(Value: TTitanScripterInstanceEvent);
    function  GetOnTyphonInstanceDestroy: TTitanScripterInstanceEvent;
    procedure SetOnTyphonInstanceDestroy(Value: TTitanScripterInstanceEvent);


    function GetTotalLineCount: Integer;
    function GetErrorClassType: TClass;
    function GetErrorDescription: String;
    function GetErrorModuleName: String;
    function GetErrorTextPos: Integer;
    function GetErrorPos: Integer;
    function GetErrorLine: Integer;
    function GetErrorMethodId: Integer;
    procedure InvokeOnAssignScript;
    function  GetCurrentSourceLine: Integer;
    function  GetCurrentModuleName: String;
    procedure RegisterLanguages;
    procedure UnregisterLanguages;
    procedure SetStackSize(Value: Integer);
    function  GetStackSize: Integer;
    procedure SetOptimization(Value: Boolean);
    function  GetOptimization: Boolean;
    function  GetLanguage(I: Integer): TTitanLanguage;
    procedure Unregister(What: TTitanDefKind; const Name: String; Owner: Integer = -1);
    function  GetParam(const ParamName: String): Variant; virtual;
    procedure SetParam(const ParamName: String; const Value: Variant); virtual;
    function  GetValue(const Name: String): Variant; virtual;
    procedure SetValue(const Name: String; const Value: Variant); virtual;
    function  RegisterParser(ParserClass: TTitanParserClass; const LanguageName, FileExt: String; CallConvention: TTitanCallConv): Integer;
    procedure SetUpSearchPathes; virtual;
  public
    fScripter: TTitanBaseScripter;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function  FindTempObject(const Key: TVarRec): TObject;
    function  AddTempObject(const Key: TVarRec; Obj: TObject): Integer;
    function  AddModule(const ModuleName, LanguageName: String): Integer;
    procedure AddCode(const ModuleName, Code: String);
    procedure AddCodeLine(const ModuleName, Code: String);
    procedure AddCodeFromFile(const ModuleName, FileName: String);
    procedure AddTyphonForm(const FRMFileName, UnitFileName: String;
                            const TitanLanguage: String = 'TitanPascal'); overload;
    procedure AddTyphonForm(const FRMFileName: String; UsedUnits: TStringList;
                                const TitanLanguage: String = 'TitanPascal'); overload;
    procedure AddTyphonForm(const ModuleName: String; FRM, Source: TStream;
                            const TitanLanguage: String = 'TitanPascal'); overload;
    procedure LoadProject(const FileName: String);
    function  CompileModule(const ModuleName: String;
                            SyntaxCheckOnly: Boolean = false): Boolean;
    function  Compile(SyntaxCheckOnly: Boolean = false): Boolean;
    procedure Run(RunMode: Integer = rmRun; const ModuleName: String = ''; LineNumber: Integer = 0);
    procedure RunInstruction;
    function  InstructionCount: Integer;
    function  CurrentInstructionNumber: Integer;
    function  GetInstruction(N: Integer): TTitanInstruction;
    procedure SetInstruction(N: Integer; I: TTitanInstruction);
    function  Eval(const Expression, LanguageName: String; var Res: Variant): Boolean;
    function  EvalStatementList(const Expression, LanguageName: String): Boolean;
    function  EvalJS(const Expression: String): Boolean;

    function CallFunction(const Name: String; const Params: array of const; AnObjectName: String = ''): Variant;
    function CallFunctionEx(const Name: String;
                            const Params: array of const;
                            const StrTypes: array of String;
                            AnObjectName: String = ''): Variant;
    function CallFunctionByID(SubID: Integer; const Params: array of const; ObjectID: Integer = 0): Variant;
    function CallFunctionByIDEx(SubID: Integer; const Params: array of const;
                                const StrTypes: array of String;
                                ObjectID: Integer = 0): Variant;
    function CallMethod(const Name: String; const Params: array of const; Instance: TObject): Variant; overload;
    function CallMethod(const Name: String; const Params: array of const; const This: Variant): Variant; overload;
    function CallMethodByID(SubID: Integer; const Params: array of const; Instance: TObject): Variant; overload;
    function CallMethodByID(SubID: Integer; const Params: array of const; const This: Variant): Variant; overload;

    function  GetLastResult: Variant;
    procedure CancelCompiling(const AMessage: String);
    procedure Dump;
    procedure RemoveAllBreakpoints;
    function  AddBreakpoint(const ModuleName: String; LineNumber: Integer; const Condition: String = ''; PassCount: Integer = 0): Boolean;
    function  RemoveBreakpoint(const ModuleName: String; LineNumber: Integer): Boolean;
    procedure RegisterConstant(const Name: String; Value: Variant; Owner: Integer = -1);
    procedure RegisterVariable(const Name, TypeName: String; Address: Pointer; Owner: Integer = -1);
    procedure RegisterObject(const Name: String; Instance: TObject; Owner: Integer = -1);
    procedure RegisterVirtualObject(const Name: String; Owner: Integer = -1);
    procedure RegisterObjectSimple(const Name: String; Instance: TObject; Owner: Integer = -1);
    procedure RegisterInterfaceVar(const Name: String; PIntf: PUnknown;
                                   const guid: TGUID;
                                   Owner: Integer = -1);
    procedure UnregisterConstant(const Name: String; Owner: Integer = -1);
    procedure UnregisterVariable(const Name: String; Owner: Integer = -1);
    procedure UnregisterObject(const Name: String; Owner: Integer = -1); overload;
    procedure UnregisterObject(Instance: TObject; Owner: Integer = -1); overload;
    procedure UnregisterAllVariables;
    procedure UnregisterAllObjects;
    procedure UnregisterAllConstants;

    procedure ForbidAllPublishedProperties(AClass: TClass);
    procedure ForbidPublishedProperty(AClass: TClass; const PropName: String);

    procedure RegisterField(const ObjectName: String;
                            ObjectType: String;
                            FieldName: String;
                            FieldType: String;
                            Address: Pointer);
    function  ToString(const V: Variant): String;

    procedure ResetScripter;
    procedure ResetScripterEx;
    procedure Terminate;
    procedure DisconnectObjects;

    function  IsError: Boolean;
    procedure DiscardError;

    function GetMemberID(const Name: String): Integer;
    function GetParamID(SubID, ParamIndex: Integer): Integer;
    function GetResultID(SubID: Integer): Integer;
    function GetParamCount(SubID: Integer): Integer;
    function GetTypeName(ID: Integer): String;
    function GetParamTypeName(SubID, ParamIndex: Integer): String;
    function GetSignature(SubID: Integer): String;
    function GetParamName(SubID, ParamIndex: Integer): String;
    function GetTypeID(ID: Integer): Integer;
    function GetName(ID: Integer): String;
    function GetFullName(ID: Integer): String;
    function GetPosition(ID: Integer): Integer;
    function GetStartPosition(ID: Integer): Integer;
    function GetModule(ID: Integer): Integer;
    function GetAddress(ID: Integer): Pointer;
    function GetUserData(ID: Integer): Integer;
    function IsLocalVariable(ID: Integer): Boolean;
    function GetOwnerID(ID: Integer): Integer;
    function GetKind(ID: Integer): TTitanMemberKind;
    function GetCurrentProcID: Integer;
    function IsStatic(ID: Integer): Boolean;
    function IsConstructor(ID: Integer): Boolean;
    function IsDestructor(ID: Integer): Boolean;
    function IsVarParameter(ID: Integer): Boolean;
    function IsConstParameter(ID: Integer): Boolean;
    function IDCount: Integer;
    function IsMethod(ID: Integer): Boolean;

    function  GetOnShowError: TTitanScripterEvent;
    procedure SetOnShowError(Value: TTitanScripterEvent);

    property ErrorClassType: TClass read GetErrorClassType;
    property ErrorDescription: String read GetErrorDescription;
    property ErrorModuleName: String read GetErrorModuleName;
    property ErrorTextPos: Integer read GetErrorTextPos;
    property ErrorPos: Integer read GetErrorPos;
    property ErrorLine: Integer read GetErrorLine;
    property ErrorMethodId: Integer read GetErrorMethodId;
    function GetSourceLine(const ModuleName: String; LineNumber: Integer): String;
    function IsExecutableSourceLine(const ModuleName: String; L: Integer): Boolean;

    function LanguageCount: Integer;
    function FindLanguage(const LanguageName: String): TTitanLanguage;
    function FileExtToLanguageName(const FileExt: String): String;
    procedure RegisterLanguage(L: TTitanLanguage);
    procedure UnregisterLanguage(const LanguageName: String);

    function  GetRootID: Integer;
    procedure EnumMembers(ID: Integer;
                          Module: Integer;
                          CallBack: TTitanMemberCallback;
                          Data: Pointer);

    function  GetValueByID(ID: Integer): Variant;
    procedure SetValueByID(ID: Integer; const Value: Variant);

    procedure SaveToStream(S: TStream);
    procedure LoadFromStream(S: TStream);

    procedure SaveToFile(const FileName: String);
    procedure LoadFromFile(const FileName: String);
    procedure SaveModuleToStream(const ModuleName: String;  S: TStream);
    procedure SaveModuleToFile(const ModuleName, FileName: String);
    procedure LoadModuleFromStream(const ModuleName: String; S: TStream);
    procedure LoadModuleFromFile(const ModuleName, FileName: String);  

    procedure AssignEventHandlerRunner(MethodAddress: Pointer; Instance: TObject);
    function  AssignEventHandler(Instance: TObject; EventPropName: String; EventHandlerName: String): Boolean;
    
    function  GeTTitanModule(I: Integer): TTitanModule;
    procedure DeleteModule(I: Integer);
    function  FindFullFileName(const FileName: String): String;
    function  FindNamespaceHandle(const Name: String): Integer;

    function  ToScriptObject(TyphonInstance: TObject): Variant;
    procedure Rename(ID: Integer; const NewName: String);

    procedure GetClassInfo(const FullName: String; mk: TTitanMemberKind; L: TStrings);
    function  GetHostClass(const FullName: String): TClass;

    procedure ScanProperties(const ObjectName: String);

    property ScripterState: TScripterState read GetScripterState write SetScripterState;
    property CallStack: TCallStack read fCallStack;
    property SourceCode[const ModuleName: String]: String read GetSourceCode write SetSourceCode;
    property Modules: TStringList read GetModules;
    property TotalLineCount: Integer read GetTotalLineCount;
    property CurrentSourceLine: Integer read GetCurrentSourceLine;
    property CurrentModuleName: String read GetCurrentModuleName;
    property Params[const ParamName: String]: Variant read GetParam write SetParam;
    property Values[const VarName: String]: Variant read GetValue write SetValue;
    property Languages[I: Integer]: TTitanLanguage read GetLanguage;
  published
    property OverrideHandlerMode: TTitanOverrideHandlerMode read GetOverrideHandlerMode write SetOverrideHandlerMode;
    property StackSize: Integer read GetStackSize write SetStackSize;
    property Optimization: Boolean read GetOptimization write SetOptimization;
    property OnAfterCompileStage: TTitanScripterEvent read fOnAfterCompileStage write fOnAfterCompileStage;
    property OnAfterRunStage: TTitanScripterEvent read fOnAfterRunStage write fOnAfterRunStage;
    property OnAssignScript: TTitanScripterEvent read fOnAssignScript write fOnAssignScript;
    property OnBeforeCompileStage: TTitanScripterEvent read fOnBeforeCompileStage write fOnBeforeCompileStage;
    property OnBeforeRunStage: TTitanScripterEvent read fOnBeforeRunStage write fOnBeforeRunStage;
    property OnCompilerProgress: TTitanCompilerProgressEvent read GetOnCompilerProgress write SetOnCompilerProgress;
    property OnPrint: TTitanScripterPrintEvent read GetOnPrint write SetOnPrint;
    property OnDefine: TTitanScripterDefineEvent read GetOnDefine write SetOnDefine;
    property OnRunning: TTitanCodeEvent read GetOnRunning write SetOnRunning;
    property OnRunningUpdate: TTitanScripterEvent read GetOnRunningUpdate write SetOnRunningUpdate;
    property OnRunningUpdateActive: Boolean read GetOnRunningUpdateActive write SetOnRunningUpdateActive;
    property OnRunningSync: TTitanScripterEvent read GetOnRunningSync write SetOnRunningSync;
    property OnShowError: TTitanScripterEvent read GetOnShowError write SetOnShowError;
    property OnUndeclaredIdentifier: TTitanScripterVarEventEx read GetOnUndeclaredIdentifier write SetOnUndeclaredIdentifier;
    property OnChangedVariable: TTitanScripterVarEvent read GetOnChangedVariable write SetOnChangedVariable;
    property OnReadExtraData: TTitanScripterStreamEvent read GetOnReadExtraData write SetOnReadExtraData;
    property OnWriteExtraData: TTitanScripterStreamEvent read GetOnWriteExtraData write SetOnWriteExtraData;
    property OnUsedModule: TTitanUsedModuleEvent read GetOnUsedModule write SetOnUsedModule;
    property OnLoadSourceCode: TTitanLoadSourceCodeEvent read GetOnLoadSourceCode write SetOnLoadSourceCode;
    property OnChangeState: TTitanScripterChangeStateEvent read GetOnChangeState write SetOnChangeState;
    property OnInclude: TTitanIncludeEvent read GetOnInclude write SetOnInclude;
    property OnHalt: TTitanScripterEvent read GetOnHalt write SetOnHalt;
    property OnTyphonInstanceCreate: TTitanScripterInstanceEvent read GetOnTyphonInstanceCreate write SetOnTyphonInstanceCreate;
    property OnTyphonInstanceDestroy: TTitanScripterInstanceEvent read GetOnTyphonInstanceDestroy write SetOnTyphonInstanceDestroy;
    property OnLoadDll: TTitanLoadDllEvent read GetOnLoadDll write SetOnLoadDll;
    property OnVirtualObjectMethodCallEvent: TTitanVirtualObjectMethodCallEvent read GetOnVirtualObjectMethodCallEvent write SetOnVirtualObjectMethodCallEvent;
    property OnVirtualObjectPutPropertyEvent: TTitanVirtualObjectPutPropertyEvent read GetOnVirtualObjectPutPropertyEvent write SetOnVirtualObjectPutPropertyEvent;
    property OnScanProperties: TTitanScanPropertiesEvent read fOnScanProperties write fOnScanProperties;
  end;

  TTitanLanguage = class(TTitanBaseLanguage)
  private
    fContainer: TComponent;
    fInformalName: String;
    fScripters: TList;
    fLongStrLiterals: Boolean;
    fNamespaceAsModule: Boolean;
    fCallConv: TTitanCallConv;
    fCompilerDirectives: TStrings;
    fIncludeFileExt: String;
    fJavaScriptOperators: Boolean;
    fDeclareVariables: Boolean;
    fZeroBasedStrings: Boolean;
    fBackslash: Boolean;
    procedure RegisterScripter(S: TTitanScripter);
    procedure UnRegisterScripter(S: TTitanScripter);
    function GetKeywords: TStringList;
    function GetCaseSensitive: Boolean;
    procedure SetCompilerDirectives(const Value: TStrings);
  protected
    fInitArrays: boolean;
    fVBArrays: Boolean;
    procedure SetFileExt(const Value: String); virtual;
    function GeTTitanParserClass: TTitanParserClass; virtual;
    procedure SetLanguageName(const Value: String); virtual;
    function GetLanguageName: String; virtual;
    function GetFileExt: String; virtual;
    function GetLongStrLiterals: Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property LanguageName: String read GetLanguageName write SetLanguageName;
    property FileExt: String read GetFileExt write SetFileExt;
    property Keywords: TStringList read GetKeywords;
    property CaseSensitive: Boolean read GetCaseSensitive;
  published
    property CompilerDirectives: TStrings read fCompilerDirectives write SetCompilerDirectives;
    property InformalName: String read fInformalName write fInformalName;
    property Container: TComponent read fContainer write fContainer;
    property LongStrLiterals: Boolean read fLongStrLiterals write fLongStrLiterals;
    property CallConvention: TTitanCallConv read fCallConv write fCallConv;
    property NamespaceAsModule: Boolean read fNamespaceAsModule write fNamespaceAsModule;
    property IncludeFileExt: String read fIncludeFileExt write fIncludeFileExt;
    property JavaScriptOperators: Boolean read fJavaScriptOperators write fJavaScriptOperators;
    property DeclareVariables: Boolean read fDEclareVariables write fDeclareVariables;
    property ZeroBasedStrings: Boolean read fZeroBasedStrings write fZeroBasedStrings;
    property Backslash: Boolean read fBackslash write fBackslash;
  end;

function RegisterNamespace(const Name: String; OwnerIndex: Integer = -1;
                           UserData: Integer = 0): Integer;
function RegisterClassType(PClass: TClass; OwnerIndex: Integer = -1;
                           UserData: Integer = 0): Integer;
function RegisterInterfaceType(const Name: String; const Guid: TGuid;
                               const ParentName: String; const ParentGuid: TGUID;
                               OwnerIndex: Integer = -1;
                               UserData: Integer = 0): Integer;
procedure RegisterTypeAlias(const TypeName1, TypeName2: String);
function RegisterClassTypeEx(PClass: TClass; ReadProp, WriteProp: TTitanMethodImpl;
                             OwnerIndex: Integer = -1;
                             UserData: Integer = 0): Integer;
function RegisterRTTItype(pti: PTypeInfo; UserData: Integer = 0): Integer;
function RegisterEnumTypeByDef(const TypeName: PChar; UserData: Integer = 0): Integer;

procedure RegisterMethod(PClass: TClass; const Header: String; Address: Pointer;
                        Fake: Boolean = false;
                        UserData: Integer = 0);
procedure RegisterInterfaceMethod(pti: PTypeInfo; const Header: String;
                                  MethodIndex: Integer = -1;
                                  UserData: Integer = 0); overload;
procedure RegisterInterfaceMethod(const Guid: TGUID; const Header: String;
                                  MethodIndex: Integer = -1;
                                  UserData: Integer = 0); overload;
procedure RegisterBCBMethod(PClass: TClass; const Header: String; Address: Pointer;
                        Fake: Boolean = false;
                        UserData: Integer = 0);
procedure RegisterStdMethod(PClass: TClass;
                            const Name: String;
                            Proc: TTitanMethodImpl;
                            NP: Integer;
                            UserData: Integer = 0);
procedure RegisterStdMethodEx(PClass: TClass;
                              const Name: String;
                              Proc: TTitanMethodImpl;
                              NP: Integer;
                              const Types: array of Integer;
                              UserData: Integer = 0);
procedure RegisterField(PClass: TClass; const FieldName, FieldType: String;
                        Offset: Integer; UserData: Integer = 0);
procedure RegisterProperty(PClass: TClass; const PropDef: String;
                           UserData: Integer = 0);
procedure RegisterInterfaceProperty(const guid: TGUID; const PropDef: String;
                           UserData: Integer = 0);
procedure RegisterRoutine(const Header: String; Address: Pointer;
                          OwnerIndex: Integer = -1;
                          UserData: Integer = 0);
procedure RegisterStdRoutine(const Name: String;
                             Proc: TTitanMethodImpl;
                             NP: Integer;
                             OwnerIndex: Integer = -1;
                             UserData: Integer = 0);
procedure RegisterStdRoutineEx(const Name: String;
                               Proc: TTitanMethodImpl;
                               NP: Integer;
                               const Types: array of Integer;
                               OwnerIndex: Integer = -1;
                               UserData: Integer = 0);
procedure RegisterStdRoutineEx2(const Name: String;
                                Proc: TTitanMethodImpl;
                                NP: Integer;
                                const Types: array of Integer;
                                const ByRefs: array of boolean;
                                OwnerIndex: Integer = -1;
                                UserData: Integer = 0);

procedure RegisterConstant(const Name: String; const Value: Double;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0); overload;
procedure RegisterConstant(const Name: String; const Value: Integer;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0); overload;


procedure RegisterConstant(const Name: String; const Value: Variant;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0); overload;
procedure RegisterInt64Constant(const Name: String; const Value: Int64;
                                OwnerIndex: Integer = -1;
                                UserData: Integer = 0);
procedure RegisterVariable(const Name, TypeName: String; Address: Pointer;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0);
procedure RegisterDynamicArrayType(const TypeName, ElementTypeName: String;
                                   OwnerIndex: Integer = -1;
                                   UserData: Integer = 0);
procedure RegisterStaticArrayType(const TypeName, ElementTypeName: String;
                                   OwnerIndex: Integer = -1;
                                   UserData: Integer = 0);
function RegisterRecordType(const TypeName: String;
                            Size: Integer;
                            OwnerIndex: Integer = -1;
                            UserData: Integer = 0): Integer;
procedure RegisterRecordField(OwnerIndex: Integer; const FieldName, FieldType: String;
                              Offset: Integer; UserData: Integer = 0);
//===================================================================================

procedure RegisterConstantSX(const Name:String; const Value:Double);overload;
procedure RegisterConstantSX(const Name:String; const Value:Integer);overload;
procedure RegisterConstantSX(const Name:String; const Value:Variant);overload;
procedure RegisterInt64ConstantSX(const Name:String; const Value:Int64);

procedure RegisterVariableSX(const Name, TypeName:String; Address:Pointer);
Procedure RegisterRTTItypeSX(pti:PTypeInfo);
procedure RegisterTypeAliasSX(const TypeName1, TypeName2:String);

function  RegisterClassTypeSx(PClass:TClass):Integer;
Function  RegisterMethodSX(ClassID:integer; const Header:String; Address:Pointer):TTitanMethodDefinition;
Function  RegisterMethodSXF(ClassID:integer; const Header:String; Address:Pointer):TTitanMethodDefinition;
procedure RegisterPropertySXi(ClassID:Integer;GR,GW:TTitanMethodDefinition; const PropDef:String);
procedure RegisterPropertySX(ClassID:Integer;GR,GW:TTitanMethodDefinition; const aName,aResType:String);

procedure RegisterRoutineSX(const Header:String; Address:Pointer);

procedure RegisterDynArrayTypeSX(const TypeName,ElementTypeName:String);
procedure RegisterArrayTypeSX(const TypeName,ElementTypeName:String; Const aMin,aMax:Integer);

function  RegisterRecordTypeSX(const TypeName:String; CaseSize:integer=0):Integer;
procedure RegisterRecordFieldSX(OwnerIndex:Integer; const FieldName,FieldType:String; CaseOffset:Integer=0);

//===================================================================================

function _Self: TObject;
function _Scripter: TTitanScripter;

function RunFile(const FileName: String; TitanScripter: TTitanScripter): Boolean;
function RunString(const Script, LanguageName: String; TitanScripter: TTitanScripter): Boolean;

const
  Fake = true;

var
  _OP_CALL,
  _OP_PRINT,
  _OP_GET_PUBLISHED_PROPERTY,
  _OP_PUT_PUBLISHED_PROPERTY,
  _OP_PUT_PROPERTY,
  _OP_NOP,
  _OP_ASSIGN: Integer;

function LoadImportLibrary(const DllName: String): Cardinal;
function FreeImportLibrary(H: Cardinal): LongBool;

function GetExtraDataPos(S: TStream): Integer;
function GetCompiledModuleVersion(S: TStream): Integer;

implementation

uses
  Titan_RTTI;

var
  _Count: Integer = 3;

function GetCompiledModuleVersion(S: TStream): Integer;
begin
  if not S.Position = 0 then
    raise Exception.Create(errStream_position_must_be_equal_0);
  LoadInteger(S); // modules count
  LoadInteger(S); // module size
  result := LoadInteger(S);
  S.Position := 0;
end;

function GetExtraDataPos(S: TStream): Integer;
var
  Version: Integer;
begin
  if not S.Position = 0 then
    raise Exception.Create(errStream_position_must_be_equal_0);
  LoadInteger(S); // modules count
  LoadInteger(S); // module size
  Version := LoadInteger(S);
  if Version <> _CompiledModuleVersion then
    raise TTitanScriptFailure.Create(Format(errIncorrectCompiledModuleVersion,
        [Version, _CompiledModuleVersion]));

  result := LoadInteger(S); // extra data pos
  S.Position := 0;
end;

function TTitanScripter.LanguageCount: Integer;
begin
  result := fScripter.ParserList.Count;
end;

function TTitanScripter.GetLanguage(I: Integer): TTitanLanguage;
begin
  result := TTitanLanguage(fScripter.ParserList[I]._Language);
end;

function TTitanScripter.FindLanguage(const LanguageName: String): TTitanLanguage;
var
  I: Integer;
begin
  result := nil;
  for I:=0 to LanguageCount - 1 do
    if StrEql(fScripter.ParserList.Names[I], LanguageName) then
    begin
      result := TTitanLanguage(fScripter.ParserList[I]._Language);
      Exit;
    end;
end;

function TTitanScripter.FileExtToLanguageName(const FileExt: String): String;
begin
  if Pos('.', FileExt) = 0 then
    result :=  fScripter.ParserList.GetLanguageName('xxx.' + FileExt)
  else
    result :=  fScripter.ParserList.GetLanguageName('xxx' + FileExt);
end;

function RegisterNamespace(const Name: String; OwnerIndex: Integer = -1;
                           UserData: Integer = 0): Integer;
var
  D, Owner: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddNamespace(Name, TTitanClassDefinition(Owner));
  result := D.Index;
  D.UserData := UserData;
end;

function RegisterClassType(PClass: TClass; OwnerIndex: Integer = -1;
                           UserData: Integer = 0): Integer;
var
  D, Owner: TTitanDefinition;
  DefMethod: TTitanMethodDefinition;
  I, J: Integer;
  S: String;
begin
  if PClass.InheritsFrom(TPersistent) then
    if GetClass(PClass.ClassName) = nil then
      RegisterClass(TPersistentClass(PClass));

  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddClass2(PClass, TTitanClassDefinition(Owner), nil, nil);
  result := D.Index;
  D.UserData := UserData;

  S := PClass.ClassName;

  if Pos('CLASS', UpperCase(S)) > 0 then
  begin
     for I:=0 to GlobalDefinitionList.Count - 1 do
     if GlobalDefinitionList.Records[I].DefKind = dkMethod then
     begin
       DefMethod := GlobalDefinitionList.Records[I] as TTitanMethodDefinition;
       if DefMethod.StrTypes <> nil then
       for J:= 0 to Length(DefMethod.StrTypes) - 1 do
       if StrEql(DefMethod.StrTypes[J], S) then
         DefMethod.Types[J] := typeCLASS;
     end;
  end;
end;

function RegisterClassTypeEx(PClass: TClass; ReadProp, WriteProp: TTitanMethodImpl;
                             OwnerIndex: Integer = -1;
                             UserData: Integer = 0): Integer;
var
  D, Owner: TTitanDefinition;
begin
  if PClass.InheritsFrom(TPersistent) then
    if GetClass(PClass.ClassName) = nil then
       RegisterClass(TPersistentClass(PClass));

  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddClass2(PClass, TTitanClassDefinition(Owner),
          ReadProp, WriteProp);
  result := D.Index;
  D.UserData := UserData;
end;

procedure RegisterTypeAlias(const TypeName1, TypeName2: String);
var
  D: TTitanClassDefinition;
begin
  D := GlobalDefinitionList.FindClassDefByName(TypeName1);
  if D <> nil then
  begin
    GlobalDefinitionList.AddClass1(TypeName2, TTitanClassDefinition(D.Owner), TTitanClassDefinition(D.Ancestor), nil, nil);
  end;

  AddTypeAlias(TypeName1, TypeName2);
end;

function RegisterRTTItype(pti: PTypeInfo; UserData: Integer = 0): Integer;
var
  D: TTitanDefinition;
begin
  D := GlobalDefinitionList.AddRTTIType(pti);
  result := D.Index;
  D.UserData := UserData;
end;

function RegisterInterfaceType(const Name: String; const Guid: TGuid;
                               const ParentName: String; const ParentGuid: TGUID;
                               OwnerIndex: Integer = -1;
                               UserData: Integer = 0): Integer;
var
  D, Owner: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddInterfaceType(Name, Guid, ParentName, ParentGuid, TTitanClassDefinition(Owner));
  result := D.Index;
  D.UserData := UserData;
end;

function RegisterEnumTypeByDef(const TypeName: PChar; UserData: Integer = 0): Integer;
var
  D: TTitanDefinition;
begin
  D := GlobalDefinitionList.AddEnumTypeByDef(TypeName);
  result := D.Index;
  D.UserData := UserData;
end;

procedure RegisterDynamicArrayType(const TypeName, ElementTypeName: String;
                                   OwnerIndex: Integer = -1;
                                   UserData: Integer = 0);
var
  D, Owner: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddDynamicArrayType(TypeName, ElementTypeName, Owner);
  D.UserData := UserData;
end;

procedure RegisterStaticArrayType(const TypeName, ElementTypeName: String;
                                   OwnerIndex: Integer = -1;
                                   UserData: Integer = 0);
var
  Owner: TTitanDefinition;
  D: TTitanClassDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddDynamicArrayType(TypeName, ElementTypeName, Owner);
  D.UserData := UserData;
  D.IsStaticArray := true;
end;

function RegisterRecordType(const TypeName: String;
                            Size: Integer;
                            OwnerIndex: Integer = -1;
                            UserData: Integer = 0): Integer;
var
  D, Owner: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddRecordType(TypeName, Size, Owner);
  D.UserData := UserData;
  result := D.Index;
end;

procedure RegisterRecordField(OwnerIndex: Integer; const FieldName, FieldType: String;
                              Offset: Integer; UserData: Integer = 0);
var
  D, Owner: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddRecordField(TTitanClassDefinition(Owner), FieldName, FieldType, Offset);
  D.UserData := UserData;
end;

procedure RegisterMethod(PClass: TClass; const Header: String; Address: Pointer;
                        Fake: Boolean = false;
                        UserData: Integer = 0);
var
  D: TTitanMethodDefinition;
begin
  if Fake then
    D := GlobalDefinitionList.AddFakeMethod(PClass, Header, Address)
  else
    D := GlobalDefinitionList.AddMethod2(PClass, Header, Address);
  D.UserData := UserData;
end;

procedure RegisterInterfaceMethod(pti: PTypeInfo; const Header: String;
                        MethodIndex: Integer = -1;
                        UserData: Integer = 0);
var
  D: TTitanMethodDefinition;
begin
  D := GlobalDefinitionList.AddMethod5(pti, Header);
  D.UserData := UserData;

  if MethodIndex > 0 then
    D.MethodIndex := MethodIndex;
end;

procedure RegisterInterfaceMethod(const Guid: TGUID; const Header: String;
                                  MethodIndex: Integer = -1;
                                  UserData: Integer = 0);
var
  D: TTitanMethodDefinition;
begin
  D := GlobalDefinitionList.AddMethod6(guid, Header, MethodIndex);
  D.UserData := UserData;

  if MethodIndex > 0 then
    D.MethodIndex := MethodIndex;
end;

procedure RegisterBCBMethod(PClass: TClass; const Header: String; Address: Pointer;
                            Fake: Boolean = false;
                            UserData: Integer = 0);
var
  D: TTitanMethodDefinition;
begin
  if Fake then
    D := GlobalDefinitionList.AddFakeMethod(PClass, Header, Address)
  else
    D := GlobalDefinitionList.AddMethod2(PClass, Header, Pointer(Address^));
  D.UserData := UserData;
end;

procedure RegisterProperty(PClass: TClass; const PropDef: String;
                           UserData: Integer = 0);
var
  D: TTitanDefinition;
begin
  D := GlobalDefinitionList.AddProperty3(PClass, PropDef);
  if D <> nil then
    D.UserData := UserData;
end;

procedure RegisterInterfaceProperty(const guid: TGUID; const PropDef: String;
                                    UserData: Integer = 0);
var
  D: TTitanDefinition;
begin
  D := GlobalDefinitionList.AddInterfaceProperty(guid, PropDef);
  if D <> nil then
    D.UserData := UserData;
end;

procedure RegisterRoutine(const Header: String; Address: Pointer;
                          OwnerIndex: Integer = -1;
                          UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddMethod1(Header, Address, Owner, true);
  D.UserData := UserData;
end;

procedure RegisterStdRoutine(const Name: String;
                             Proc: TTitanMethodImpl;
                             NP: Integer;
                             OwnerIndex: Integer = -1;
                             UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddMethod3(Name, Proc, NP, Owner, true);
  D.UserData := UserData;
end;

procedure RegisterStdRoutineEx(const Name: String;
                               Proc: TTitanMethodImpl;
                               NP: Integer;
                               const Types: array of Integer;
                               OwnerIndex: Integer = -1;
                               UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanMethodDefinition;
  I: Integer;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddMethod3(Name, Proc, NP, Owner, true);
  D.UserData := UserData;

  if Length(Types) <> NP + 1 then
    raise Exception.Create(errIncorrect_parameters_in_RegisterStdRoutineEx);

  SetLength(D.Types, NP + 1);
  SetLength(D.ByRefs, NP + 1);
  SetLength(D.ExtraTypes, NP + 1);
  SetLength(D.StrTypes, NP + 1);
  SetLength(D.ParamNames, NP + 1);
  for I:=0 to NP do
  begin
    D.Types[I] := Types[I];
    D.ByRefs[I] := false;
    D.ExtraTypes[I] := 0;
    D.StrTypes[I] := '';
    D.ParamNames[I] := '';
  end;
end;

procedure RegisterStdRoutineEx2(const Name: String;
                                Proc: TTitanMethodImpl;
                                NP: Integer;
                                const Types: array of Integer;
                                const ByRefs: array of boolean;
                                OwnerIndex: Integer = -1;
                                UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanMethodDefinition;
  I: Integer;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddMethod3(Name, Proc, NP, Owner, true);
  D.UserData := UserData;

  if NP = -1 then
    NP := Length(Types) - 1;

  if Length(Types) <> NP + 1 then
    raise Exception.Create(errIncorrect_parameters_in_RegisterStdRoutineEx);

  SetLength(D.Types, NP + 1);
  SetLength(D.ByRefs, NP + 1);
  SetLength(D.ExtraTypes, NP + 1);
  SetLength(D.StrTypes, NP + 1);
  SetLength(D.ParamNames, NP + 1);
  for I:=0 to NP do
  begin
    D.Types[I] := Types[I];
    D.ByRefs[I] := ByRefs[I];
    D.ExtraTypes[I] := 0;
    D.StrTypes[I] := '';
    D.ParamNames[I] := '';
  end;
end;

procedure RegisterStdMethod(PClass: TClass;
                            const Name: String;
                            Proc: TTitanMethodImpl;
                            NP: Integer;
                            UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  Owner := GlobalDefinitionList.FindClassDef(PClass);
  D := GlobalDefinitionList.AddMethod3(Name, Proc, NP, Owner);
  D.UserData := UserData;
end;

procedure RegisterStdMethodEx(PClass: TClass;
                              const Name: String;
                              Proc: TTitanMethodImpl;
                              NP: Integer;
                              const Types: array of Integer;
                              UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanMethodDefinition;
  I: Integer;
begin
  Owner := GlobalDefinitionList.FindClassDef(PClass);
  D := GlobalDefinitionList.AddMethod3(Name, Proc, NP, Owner);
  D.UserData := UserData;

  if Length(Types) <> NP + 1 then
    raise Exception.Create(errIncorrect_parameters_in_RegisterStdMethodEx);

  SetLength(D.Types, NP + 1);
  SetLength(D.ByRefs, NP + 1);
  SetLength(D.ExtraTypes, NP + 1);
  SetLength(D.StrTypes, NP + 1);
  SetLength(D.ParamNames, NP + 1);
  for I:=0 to NP do
  begin
    D.Types[I] := Types[I];
    D.ByRefs[I] := false;
    D.ExtraTypes[I] := 0;
    D.StrTypes[I] := '';
    D.ParamNames[I] := '';
  end;
end;

procedure RegisterConstant(const Name: String; const Value: Double;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddConstant(Name, Value, Owner, true);
  D.UserData := UserData;
end;

procedure RegisterConstant(const Name: String; const Value: Integer;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddConstant(Name, Value, Owner, true);
  D.UserData := UserData;
end;

procedure RegisterConstant(const Name: String; const Value: Variant;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddConstant(Name, Value, Owner, true);
  D.UserData := UserData;
end;

procedure RegisterInt64Constant(const Name: String; const Value: Int64;
                                OwnerIndex: Integer = -1;
                                UserData: Integer = 0);
var
  Owner: TTitanClassDefinition;
  Dbl: Double;
  I: Integer;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    Owner := nil
  else
    Owner := GlobalDefinitionList[OwnerIndex];
  if Abs(Value) > MaxInt then
  begin
    Dbl := Value;
    D := GlobalDefinitionList.AddConstant(Name, Dbl, Owner, true);
  end
  else
  begin
    I := Value;
    D := GlobalDefinitionList.AddConstant(Name, I, Owner, true);
  end;
  D.UserData := UserData;
end;

procedure RegisterVariable(const Name, TypeName: String; Address: Pointer;
                           OwnerIndex: Integer = -1;
                           UserData: Integer = 0);
var
  O: TTitanClassDefinition;
  D: TTitanDefinition;
begin
  if OwnerIndex = -1 then
    O := nil
  else
    O := GlobalDefinitionList[OwnerIndex];
  D := GlobalDefinitionList.AddVariable(Name, TypeName, Address, O);
  D.UserData := UserData;
end;

procedure RegisterField(PClass: TClass; const FieldName, FieldType: String;
                        Offset: Integer; UserData: Integer = 0);
var
  D: TTitanDefinition;
begin
  D := GlobalDefinitionList.AddField(PClass, FieldName, FieldType, Offset);
  D.UserData := UserData;
end;
//=================================================================================

function RegisterClassTypeSx(PClass:TClass):Integer;
 var D:TTitanDefinition;
     DefMethod:TTitanMethodDefinition;
    { I, J,}inx:Integer;
    // S:String;
begin

 inx:=GlobalDefinitionList.fListOfClasses.IndexOf(PClass);
 if inx>-1 then
  begin
    result:=inx;
    exit;
  end;

  if PClass.InheritsFrom(TPersistent) then RegisterClass(TPersistentClass(PClass));

  D := GlobalDefinitionList.AddClassSX(PClass);
  result := D.Index;
   {
   S := PClass.ClassName;
  
  if Pos('CLASS', UpperCase(S)) > 0 then
  begin
     for I:=0 to GlobalGlobalDefinitionList.Count - 1 do
     if GlobalGlobalDefinitionList.Records[I].DefKind=dkMethod then
     begin
       DefMethod := GlobalGlobalDefinitionList.Records[I] as TTitanMethodDefinition;
       if DefMethod.StrTypes <> nil then
       for J:= 0 to Length(DefMethod.StrTypes) - 1 do
          if StrEql(DefMethod.StrTypes[J], S) then DefMethod.Types[J] := typeCLASS;
     end;
  end;  }
end;


procedure RegisterTypeAliasSX(const TypeName1, TypeName2:String);
var
  i:integer;
  D:TTitanClassDefinition;
begin

  d:=nil;
  i:=GlobalDefinitionList.fListOfClasses.IndexOfByName(TypeName1);
  if i>-1 then D:=TTitanClassDefinition(GlobalDefinitionList.Records[i]);

  if D <> nil then
  begin
    GlobalDefinitionList.AddClassSXFromType(TypeName2, TTitanClassDefinition(D.Owner), TTitanClassDefinition(D));
  end;

  AddTypeAlias(TypeName1, TypeName2);
end;


Procedure RegisterRTTItypeSX(pti:PTypeInfo);
begin
  GlobalDefinitionList.AddRTTIType(pti);
end;

procedure RegisterDynArrayTypeSX(const TypeName,ElementTypeName:String);
begin
  GlobalDefinitionList.AddDynArrayTypeSX(TypeName,ElementTypeName);
end;

procedure RegisterArrayTypeSX(const TypeName,ElementTypeName:String; Const aMin,aMax:Integer);
begin
 GlobalDefinitionList.AddArrayTypeSX(TypeName, ElementTypeName,aMin,aMax);
end;

procedure RegisterStaticArrayTypeSX(const TypeName, ElementTypeName:String);
var D:TTitanClassDefinition;
begin
  D := GlobalDefinitionList.AddDynamicArrayType(TypeName, ElementTypeName,nil);
  D.IsStaticArray := true;
end;

function RegisterRecordTypeSX(const TypeName:String; CaseSize:integer=0):Integer;
var D:TTitanDefinition;
begin
  D := GlobalDefinitionList.AddRecordType(TypeName,CaseSize,nil);
  result := D.Index;
end;

procedure RegisterRecordFieldSX(OwnerIndex:Integer; const FieldName,FieldType:String; CaseOffset:Integer=0);
 var Owner:TTitanDefinition;
begin
  if OwnerIndex=-1 then
    Owner := nil  else
    Owner := GlobalDefinitionList[OwnerIndex];

  GlobalDefinitionList.AddRecordField(TTitanClassDefinition(Owner), FieldName, FieldType, CaseOffset);
end;


procedure RegisterRoutineSX(const Header:String; Address:Pointer);
begin
   GlobalDefinitionList.AddRoutineSX(Header, Address);
end;

Function RegisterMethodSX(ClassID:integer; const Header:String; Address:Pointer):TTitanMethodDefinition;
begin
   Result:=GlobalDefinitionList.AddMethodSX(ClassID, Header, Address);
end;

Function RegisterMethodSXF(ClassID:integer; const Header:String; Address:Pointer):TTitanMethodDefinition;
begin
   Result:=GlobalDefinitionList.AddFakeMethodSX(ClassID, Header, Address);
end;

procedure RegisterPropertySXi(ClassID:Integer;GR,GW:TTitanMethodDefinition; const PropDef:String);
begin
  GlobalDefinitionList.AddProperty3SXi(ClassID,GR,GW, PropDef);
end;

procedure RegisterPropertySX(ClassID:Integer;GR,GW:TTitanMethodDefinition; const aName,aResType:String);
begin
  GlobalDefinitionList.AddProperty3SX(ClassID,GR,GW,aName,aResType);
end;


procedure RegisterVariableSX(const Name, TypeName:String; Address:Pointer);
begin
  GlobalDefinitionList.AddVariableSX(Name, TypeName, Address);
end;

//=================================================================

procedure RegisterConstantSX(const Name:String; const Value:Double);
begin
  GlobalDefinitionList.AddConstantSX(Name, Value);
end;

procedure RegisterConstantSX(const Name:String; const Value:Integer);
begin
  GlobalDefinitionList.AddConstantSX(Name, Value);
end;

procedure RegisterConstantSX(const Name:String; const Value:Variant);
begin
  GlobalDefinitionList.AddConstantSX(Name, Value);
end;

procedure RegisterInt64ConstantSX(const Name:String; const Value:Int64);
var
  Dbl:Double;
  I:Integer;
begin   
  if Abs(Value) > MaxInt then
  begin
    Dbl := Value;
    GlobalDefinitionList.AddConstantSX(Name, Dbl);
  end
  else
  begin
    I := Value;
    GlobalDefinitionList.AddConstantSX(Name, I);
  end;

end;
//=================================================================

function _Self: TObject;
begin
  result := BASE_Engine.__Self;
end;

function _Scripter: TTitanScripter;
begin
  result := TTitanScripter(BASE_Engine.__Scripter);
end;

constructor TCallStackRecord.Create;
begin
  Parameters := TStringList.Create;
end;

destructor TCallStackRecord.Destroy;
begin
  Parameters.Free;
end;

//================================ TCallStack =================================================

constructor TCallStack.Create(TitanScripter: TTitanScripter);
begin
  fRecords := TList.Create;
  fScripter := TitanScripter.fScripter;
end;

function TCallStack.GetCount: Integer;
begin
  result := fRecords.Count;
end;

procedure TCallStack.Add(R: TCallStackRecord);
begin
  fRecords.Add(R);
end;

function TCallStack.GetRecord(Index: Integer): TCallStackRecord;
begin
  result := TCallStackRecord(fRecords[Index]);
end;

procedure TCallStack.Clear;
var
  I: Integer;
begin
  for I:=0 to fRecords.Count - 1 do
    TCallStackRecord(fRecords[I]).Free;
   fRecords.Clear;
end;

destructor TCallStack.Destroy;
begin
  Clear;
  fRecords.Free;
end;

//==================================== TTitanScripter ============================

constructor TTitanScripter.Create(AOwner: TComponent);
begin
  inherited;
  fScripter := TTitanBaseScripter.Create(Self);
  fCallStack := TCallStack.Create(Self);
  RegisterLanguages;
  Optimization := true;
end;

destructor TTitanScripter.Destroy;
begin
  UnregisterLanguages;
  fScripter.Free;
  fCallStack.Free;
  inherited;
end;

function TTitanScripter.FindTempObject(const Key: TVarRec): TObject;
begin
  result := fScripter.TempObjectList.FindObject(Key);
end;

function TTitanScripter.AddTempObject(const Key: TVarRec; Obj: TObject): Integer;
begin
  result := fScripter.TempObjectList.Add(Key, Obj);
end;

function TTitanScripter.GetCurrentProcID: Integer;
begin
  result := fScripter.Code.LevelStack.Top;
end;

function TTitanScripter.IsConstructor(ID: Integer): Boolean;
begin
   result := fScripter.SymbolTable.TypeSub[ID] = tsConstructor;
end;

function TTitanScripter.IsDestructor(ID: Integer): Boolean;
begin
  result := fScripter.SymbolTable.TypeSub[ID] = tsDestructor;
end;

function TTitanScripter.IsVarParameter(ID: Integer): Boolean;
begin
  result := fScripter.SymbolTable.ByRef[ID] = 1;
end;

function TTitanScripter.IsConstParameter(ID: Integer): Boolean;
begin
  result := fScripter.SymbolTable.ByRef[ID] = 2;
end;

function TTitanScripter.IsStatic(ID: Integer): Boolean;
var
  K: TTitanMemberKind;
  ClassRec: TTitanClassRec;
  I, L: Integer;
  MemberRec: TTitanMemberRec;
  D: TTitanMethodDefinition;
begin
  result := true;
  K := GetKind(ID);
  case K of
    mkClass:
    begin
      ClassRec := fScripter.ClassList.FindClass(ID);
      if ClassRec <> nil then
        result := ClassRec.IsStatic;
    end;
    mkMethod:
    if ID > 0 then
    begin
      L := fScripter.SymbolTable.Level[ID];
      if fScripter.SymbolTable.Kind[ID] = KindType then
      begin
        ClassRec := fScripter.ClassList.FindClass(L);
        if ClassRec <> nil then
        begin
          result := ClassRec.IsStatic;
          if result then
            Exit;

          for I:=0 to ClassRec.MemberList.Count - 1 do
          begin
            MemberRec := ClassRec.MemberList[I];
            if MemberRec.ID = ID then
            begin
              result := MemberRec.IsStatic;
              Exit;
            end;
          end;
        end;
      end
      else // nested function
        result := false;
    end
    else if ID < 0 then
    begin
      D := GlobalDefinitionList[-ID];
      result := D.IsStatic;
    end;
  end
end;

procedure TTitanScripter.SetStackSize(Value: Integer);
begin
  fScripter.fStackSize := Value;
end;

function TTitanScripter.GetStackSize: Integer;
begin
  result := fScripter.fStackSize;
end;

procedure TTitanScripter.SetOptimization(Value: Boolean);
begin
  fScripter.Code.SignFOP := Value;
end;

function TTitanScripter.GetOptimization: Boolean;
begin
  result := fScripter.Code.SignFOP;
end;

function TTitanScripter.GetTotalLineCount: Integer;
begin
  result := fScripter.TotalLineCount;
end;

function TTitanScripter.GetOnPrint: TTitanScripterPrintEvent;
begin
  result := TTitanScripterPrintEvent(fScripter.OnPrint);
end;

procedure TTitanScripter.SetOnPrint(Value: TTitanScripterPrintEvent);
begin
  fScripter.OnPrint := TPrintEvent(Value);
end;

function TTitanScripter.GetOnDefine: TTitanScripterDefineEvent;
begin
  result := TTitanScripterDefineEvent(fScripter.OnDefine);
end;

procedure TTitanScripter.SetOnDefine(Value: TTitanScripterDefineEvent);
begin
  fScripter.OnDefine := TOnDefineEvent(Value);
end;

function TTitanScripter.GetOnChangedVariable: TTitanScripterVarEvent;
begin
  result := TTitanScripterVarEvent(fScripter.OnChangedVariable);
end;

procedure TTitanScripter.SetOnChangedVariable(Value: TTitanScripterVarEvent);
begin
  fScripter.OnChangedVariable := TVarEvent(Value);
end;

procedure TTitanScripter.SetOnUndeclaredIdentifier(Value: TTitanScripterVarEventEx);
begin
  fScripter.OnUndeclaredIdentifier := TVarEventEx(Value);
end;

function TTitanScripter.GetOnUndeclaredIdentifier: TTitanScripterVarEventEx;
begin
  result := TTitanScripterVarEventEx(fScripter.OnUndeclaredIdentifier);
end;

function TTitanScripter.GetOnHalt: TTitanScripterEvent;
begin
  result := TTitanScripterEvent(fScripter.OnHalt);
end;

procedure TTitanScripter.SetOnHalt(Value: TTitanScripterEvent);
begin
  fScripter.OnHalt := TScripterEvent(Value);
end;

function TTitanScripter.GetOnLoadDll: TTitanLoadDllEvent;
begin
  result := TTitanLoadDllEvent(fScripter.OnLoadDll);
end;

procedure TTitanScripter.SetOnLoadDll(Value: TTitanLoadDllEvent);
begin
  fScripter.OnLoadDll := TLoadDllEvent(Value);
end;

function TTitanScripter.GetOnVirtualObjectMethodCallEvent: TTitanVirtualObjectMethodCallEvent;
begin
  result := TTitanVirtualObjectMethodCallEvent(fScripter.OnVirtualObjectMethodCall);
end;

procedure TTitanScripter.SetOnVirtualObjectMethodCallEvent(Value: TTitanVirtualObjectMethodCallEvent);
begin
  fScripter.OnVirtualObjectMethodCall := TVirtualObjectMethodCallEvent(Value);
end;

function TTitanScripter.GetOnVirtualObjectPutPropertyEvent: TTitanVirtualObjectPutPropertyEvent;
begin
  result := TTitanVirtualObjectPutPropertyEvent(fScripter.OnVirtualObjectPutProperty);
end;

procedure TTitanScripter.SetOnVirtualObjectPutPropertyEvent(Value: TTitanVirtualObjectPutPropertyEvent);
begin
  fScripter.OnVirtualObjectPutProperty := TVirtualObjectPutPropertyEvent(Value);
end;


function TTitanScripter.GetOnReadExtraData: TTitanScripterStreamEvent;
begin
  result := TTitanScripterStreamEvent(fScripter.OnReadExtraData);
end;

procedure TTitanScripter.SetOnReadExtraData(Value: TTitanScripterStreamEvent);
begin
  fScripter.OnReadExtraData := TStreamEvent(Value);
end;

function TTitanScripter.GetOnWriteExtraData: TTitanScripterStreamEvent;
begin
  result := TTitanScripterStreamEvent(fScripter.OnWriteExtraData);
end;

procedure TTitanScripter.SetOnWriteExtraData(Value: TTitanScripterStreamEvent);
begin
  fScripter.OnWriteExtraData := TStreamEvent(Value);
end;

function TTitanScripter.GetOnShowError: TTitanScripterEvent;
begin
  result := TTitanScripterEvent(fScripter.OnShowError);
end;

procedure TTitanScripter.SetOnShowError(Value: TTitanScripterEvent);
begin
  fScripter.OnShowError := TScripterEvent(Value);
end;

function TTitanScripter.GetOnRunning: TTitanCodeEvent;
begin
  result := TTitanCodeEvent(fScripter.OnRunning);
end;

procedure TTitanScripter.SetOnRunning(Value: TTitanCodeEvent);
begin
  fScripter.OnRunning := TCodeEvent(Value);
end;

function TTitanScripter.GetOnRunningUpdate: TTitanScripterEvent;
begin
  result := TTitanScripterEvent(fScripter.OnRunningUpdate);
end;

procedure TTitanScripter.SetOnRunningUpdate(Value: TTitanScripterEvent);
begin
  fScripter.OnRunningUpdate := TScripterEvent(Value);
end;

function TTitanScripter.GetOnRunningUpdateActive: Boolean;
begin
  result := fScripter.OnRunningUpdateActive;
end;

procedure TTitanScripter.SetOnRunningUpdateActive(Value: Boolean);
begin
  fScripter.OnRunningUpdateActive := Value;
end;

function TTitanScripter.GetOnRunningSync: TTitanScripterEvent;
begin
  result := TTitanScripterEvent(fScripter.OnRunningSync);
end;

procedure TTitanScripter.SetOnRunningSync(Value: TTitanScripterEvent);
begin
  fScripter.OnRunningSync := TScripterEvent(Value);
end;

function TTitanScripter.GetOnUsedModule: TTitanUsedModuleEvent;
begin
  result := TTitanUsedModuleEvent(fScripter.OnUsedModule);
end;

procedure TTitanScripter.SetOnUsedModule(Value: TTitanUsedModuleEvent);
begin
  fScripter.OnUsedModule := TUsedModuleEvent(Value);
end;

function TTitanScripter.GetOnInclude: TTitanIncludeEvent;
begin
  result := TTitanIncludeEvent(fScripter.OnInclude);
end;

procedure TTitanScripter.SetOnInclude(Value: TTitanIncludeEvent);
begin
  fScripter.OnInclude := TIncludeEvent(Value);
end;

function TTitanScripter.GetOnLoadSourceCode: TTitanLoadSourceCodeEvent;
begin
  result := TTitanLoadSourceCodeEvent(fScripter.OnLoadSourceCode);
end;

procedure TTitanScripter.SetOnLoadSourceCode(Value: TTitanLoadSourceCodeEvent);
begin
  fScripter.OnLoadSourceCode := TLoadSourceCodeEvent(Value);
end;

function TTitanScripter.GetOnChangeState: TTitanScripterChangeStateEvent;
begin
  result := TTitanScripterChangeStateEvent(fScripter.OnChangeState);
end;

procedure TTitanScripter.SetOnChangeState(Value: TTitanScripterChangeStateEvent);
begin
  fScripter.OnChangeState := TOnChangeStateEvent(Value);
end;

function TTitanScripter.GetOnTyphonInstanceCreate: TTitanScripterInstanceEvent;
begin
  result := TTitanScripterInstanceEvent(fScripter.OnTyphonInstanceCreate);
end;

procedure TTitanScripter.SetOnTyphonInstanceCreate(Value: TTitanScripterInstanceEvent);
begin
  fScripter.OnTyphonInstanceCreate := TOnInstanceEvent(Value);
end;

function TTitanScripter.GetOnTyphonInstanceDestroy: TTitanScripterInstanceEvent;
begin
  result := TTitanScripterInstanceEvent(fScripter.OnTyphonInstanceDestroy);
end;

procedure TTitanScripter.SetOnTyphonInstanceDestroy(Value: TTitanScripterInstanceEvent);
begin
  fScripter.OnTyphonInstanceDestroy := TOnInstanceEvent(Value);
end;

function TTitanScripter.GetOnCompilerProgress: TTitanCompilerProgressEvent;
begin
  result := TTitanCompilerProgressEvent(fScripter.OnCompilerProgress);
end;

procedure TTitanScripter.SetOnCompilerProgress(Value: TTitanCompilerProgressEvent);
begin
  fScripter.OnCompilerProgress := TCompilerProgressEvent(Value);
end;

function TTitanScripter.GetModules: TStringList;
begin
  result := fScripter.Modules;
end;

function TTitanScripter.GeTTitanModule(I: Integer): TTitanModule;
begin
  result := fScripter.Modules.Items[I];
end;

procedure TTitanScripter.DeleteModule(I: Integer);
begin
  fScripter.Modules.Delete(I);
end;

function TTitanScripter.GetAddress(ID: Integer): Pointer;
var
  MemberRec: TTitanMemberRec;
  D: TTitanDefinition;
begin
  result := nil;
  with fScripter.SymbolTable do
  begin
    if Kind[ID] = KindREF then
    begin
      MemberRec := GetMemberRec(ID, maAny);
      if MemberRec = nil then
        Exit;
      D := MemberRec.Definition;
      if D <> nil then
        case D.DefKind of
          dkMethod: result := TTitanMethodDefinition(D).DirectProc;
          dkVariable: result := TTitanVariableDefinition(D).Address;
        end;
    end
    else
      result := Address[ID];
  end;
end;

function TTitanScripter.IsLocalVariable(ID: Integer): Boolean;
begin
  if ID > 0 then
    result := fScripter.SymbolTable.IsLocal(ID)
  else
    result := false;
end;

function TTitanScripter.GetUserData(ID: Integer): Integer;
begin
  result := fScripter.SymbolTable.GetUserData(ID);
end;

function TTitanScripter.GetOwnerID(ID: Integer): Integer;
begin
  if ID > 0 then
    result := fScripter.SymbolTable.Level[ID]
  else
    result := 0
end;

function TTitanScripter.GetKind(ID: Integer): TTitanMemberKind;
var
  K: Integer;
  ClassRec: TTitanClassRec;
begin
  result := mkUnknown;
  with fScripter.SymbolTable do
  begin
    K := Kind[ID];
    case K of
      kindVAR: result := mkField;
      kindCONST: result := mkConst;
      kindPROP: result := mkProp;
      kindSUB: result := mkMethod;
      kindTYPE:
      begin
        result := mkClass;
        ClassRec := fScripter.ClassList.FindClass(ID);
        if ClassRec <> nil then
          if ClassRec.IsStatic then
            result := mkNamespace;
      end;
    end;
  end;
end;

procedure TTitanScripter.AddCode(const ModuleName, Code: String);
begin
  fScripter.AddCode(ModuleName, Code);
  fScripter.State := Ord(ssReadyToCompile);
end;

procedure TTitanScripter.AddCodeLine(const ModuleName, Code: String);
begin
  AddCode(ModuleName, Code + #13#10);
end;

procedure TTitanScripter.AddCodeFromFile(const ModuleName, FileName: String);
begin
  fScripter.AddCodeFromFile(ModuleName, FileName);
end;

procedure TTitanScripter.AddTyphonForm(const FRMFileName, UnitFileName: String;
                                     const TitanLanguage: String = 'TitanPascal');
begin
  fScripter.AddTyphonForm(FRMFileName, UnitFileName, TitanLanguage);
end;

procedure TTitanScripter.AddTyphonForm(const FRMFileName: String; UsedUnits: TStringList;
                                     const TitanLanguage: String = 'TitanPascal');
var
  S, ModuleName: String;
  P: Integer;
begin
  ModuleName := ExtractFileName(FRMFileName);
  P := Pos('.', ModuleName);
  if P > 0 then
    ModuleName := Copy(ModuleName, 1, P - 1);

  S := fScripter.ConvertTyphonForm2(FRMFileName, UsedUnits, TitanLanguage);
  fScripter.AddModule(ModuleName, TitanLanguage);
  fScripter.AddCode(ModuleName, S);
end;

procedure TTitanScripter.AddTyphonForm(const ModuleName: String; FRM, Source: TStream;
                                     const TitanLanguage: String = 'TitanPascal');
var
  L, Src, UsedUnits, FRMStrings: TStringList;
  I, J: Integer;
  P: TPascalParser;
  S: String;
begin
  L := TStringList.Create;
  Src := TStringList.Create;
  FRMStrings := TStringList.Create;
  UsedUnits := TStringList.Create;
  try
    FRMStrings.LoadFromStream(FRM);
    Src.LoadFromStream(Source);

    for I:=0 to Src.Count - 1 do
      if Pos('USES', UpperCase(Src[I])) = 1 then
      begin
        S := '';
        for J := I to Src.Count - 1 do
          S := S + Src[J];
        P := TPascalParser.Create;
        P. Scanner.SourceCode := S;
        P.Call_SCANNER;
        P.ParseUsesClause(UsedUnits);

        break;
      end;
    for J:= UsedUnits.Count - 1 downto 0 do
    begin
      S := UsedUnits[J];
      if GlobalDefinitionList.FindClassDefByName(S) = nil then
        UsedUnits.Delete(J); // not registered
    end;

    ConvFRMStringtoScript(FRMStrings.Text, UsedUnits, L, true, ModuleName, Src, TitanLanguage);

    fScripter.AddModule(ModuleName, TitanLanguage);
    fScripter.AddCode(ModuleName, L.Text);
  finally
    FRMStrings.Free;
    UsedUnits.Free;
    L.Free;
    Src.Free;

    fScripter.State := Ord(ssReadyToCompile);
  end;
end;

function TTitanScripter.AddModule(const ModuleName, LanguageName: String): Integer;
begin
  SetUpSearchPathes;
  result := fScripter.AddModule(ModuleName, LanguageName);
end;

procedure TTitanScripter.SaveModuleToStream(const ModuleName: String;
                                          S: TStream);
var
  M: TTitanModule;
  I: Integer;
begin
  I := fScripter.Modules.IndexOf(ModuleName);
  if I = -1 then
    Exit;
  M := fScripter.Modules.Items[I];

//  SaveInteger(1, S);
  fScripter.SaveCompiledModuleToStream(M, S);
end;

procedure TTitanScripter.SaveModuleToFile(const ModuleName, FileName: String);
var
  S: TFileStream;
begin
  S := TFileStream.Create(FileName, fmCreate);
  try
    SaveModuleToStream(ModuleName, S);
  finally
    S.Free;
  end;
end;

procedure TTitanScripter.LoadModuleFromStream(const ModuleName: String;
                                            S: TStream);
var
  M: TTitanModule;
  I: Integer;
begin
  I := fScripter.Modules.IndexOf(ModuleName);
  if I = -1 then
    I := fScripter.AddModule(ModuleName, 'TitanPascal');

  M := fScripter.Modules.Items[I];

  fScripter.LoadCompiledModuleFromStream(M, S);
end;

procedure TTitanScripter.LoadModuleFromFile(const ModuleName, FileName: String);
var
  S: TFileStream;
begin
  S := TFileStream.Create(FileName, fmOpenRead);
  try
    LoadModuleFromStream(ModuleName, S);
  finally
    S.Free;
  end;
end;

function TTitanScripter.CompileModule(const ModuleName: String;
                                    SyntaxCheckOnly: Boolean = false): Boolean;
var
  Parser: TTitanParser;
  M: TTitanModule;
  I: Integer;
  L: TTitanLanguage;
begin
  I := fScripter.Modules.IndexOf(ModuleName);
  if I = -1 then
    raise Exception.Create(Format(errModuleNotFound, [ModuleName]));

  M := fScripter.Modules.Items[I];

  Parser := fScripter.ParserList.FindParser(M.LanguageName);
  if Parser = nil then
    raise Exception.Create(errUnregisteredLanguage + M.LanguageName);

  fScripter.DefList.Clear;

  L := FindLanguage(M.LanguageName);
  if L <> nil then
  begin
    fScripter.DefList.Text := L.fCompilerDirectives.Text;
    Parser.NamespaceAsModule := L.NamespaceAsModule;
    Parser.IncludeFileExt := L.IncludeFileExt;
    Parser.JavaScriptOperators := L.JavaScriptOperators;
    Parser.ZeroBasedStrings := L.ZeroBasedStrings;
    Parser.DeclareVariables := L.DeclareVariables;
    Parser.VBArrays := L.fVBArrays;
    Parser.IsArrayInitialization := L.fInitArrays;
    Parser.Backslash := L.Backslash;
    if Length(Parser.IncludeFileExt) > 0 then
      if Pos('.', Parser.IncludeFileExt) = 0 then
        Parser.IncludeFileExt := '.' + Parser.IncludeFileExt;
  end;

  fScripter.CurrModule := I;
  result := fScripter.CompileModule(ModuleName, Parser, SyntaxCheckOnly);
end;

function TTitanScripter.Eval(const Expression, LanguageName: String; var Res: Variant): Boolean;
var
  Parser: TTitanParser;
begin
  Parser := fScripter.ParserList.FindParser(LanguageName);
  if Parser = nil then
    raise Exception.Create(errUnregisteredLanguage + LanguageName);

  Res := fScripter.Eval(Expression, Parser);
  if fScripter.IsError then
  begin
    result := false;
    fScripter.DiscardError;
  end
  else
    result := true;
end;

function TTitanScripter.EvalStatementList(const Expression, LanguageName: String): Boolean;
var
  Parser: TTitanParser;
begin
  Parser := fScripter.ParserList.FindParser(LanguageName);
  if Parser = nil then
    raise Exception.Create(errUnregisteredLanguage + LanguageName);

  fScripter.EvalStatementList(Expression, Parser);
  if fScripter.IsError then
  begin
    result := false;
    fScripter.DiscardError;
  end
  else
    result := true;
end;

function TTitanScripter.EvalJS(const Expression: String): Boolean;
var
  Parser: TTitanParser;
begin
  Parser := fScripter.ParserList.FindParser('TitanJavaScript');
  if Parser = nil then
    raise Exception.Create(errUnregisteredLanguage + 'TitanJavaScript');

  _Eval(Expression, fScripter, Parser);
  if fScripter.IsError then
  begin
    result := false;
    fScripter.DiscardError;
  end
  else
    result := true;
end;

function TTitanScripter.GetMemberID(const Name: String): Integer;
var
  P, ID: Integer;
  ClassRec: TTitanClassRec;
  S: String;
begin
  result := -1;

  if StrEql(Name, 'NonameNamespace') then
  begin
    result := fScripter.SymbolTable.RootNamespaceID;
    Exit;
  end;

  S := Name;
  ClassRec := fScripter.ClassList[0];

  P := Pos('.', S);
  while P > 0 do
  begin
    ID := ClassRec.MemberList.GetMemberID(Copy(S, 1, P - 1));
    if ID = 0 then
      Exit;

    ClassRec := fScripter.ClassList.FindClass(ID);

    if ClassRec = nil then
    begin
      result := -1;
      Exit;
    end;

    S := Copy(S, P + 1, Length(S) - P);
    P := Pos('.', S);
  end;

  result := ClassRec.MemberList.GetMemberID(S);
end;

function TTitanScripter.GetValueByID(ID: Integer): Variant;
begin
  with fScripter do
  if ((ID > 0) and (ID <= SymbolTable.Card)) or (ID < 0) then
    result := SymbolTable.VariantValue[ID]
end;

function TTitanScripter.GetParamID(SubID, ParamIndex: Integer): Integer;
begin
  with fScripter do
  if (SubID > 0) and (SubID <= SymbolTable.Card) then
    result := SymbolTable.GetParamID(SubID, ParamIndex)
  else
    result := 0;
end;

function TTitanScripter.IDCount: Integer;
begin
  result := fScripter.SymbolTable.Card;
end;

function TTitanScripter.IsMethod(ID: Integer): Boolean;
begin
  result := fScripter.SymbolTable.Kind[ID] = KindSUB;
end;

function TTitanScripter.GetResultID(SubID: Integer): Integer;
begin
  result := fScripter.SymbolTable.GetResultID(SubID);
end;

function TTitanScripter.GetParamCount(SubID: Integer): Integer;
begin
  result := fScripter.SymbolTable.Count[SubID];
end;

function TTitanScripter.GetTypeName(ID: Integer): String;
begin
  result := fScripter.SymbolTable.GetTypeName(ID);
end;

function TTitanScripter.GetParamTypeName(SubID, ParamIndex: Integer): String;
begin
  result := fScripter.SymbolTable.GetParamTypeName(SubID, ParamIndex);
end;

function TTitanScripter.GetSignature(SubID: Integer): String;
var
  j, np: Integer;
begin
  result := '';
  np := GetParamCount(SubId);

  for j:=1 to np do
  begin
    result := result + GetParamTypeName(SubId, j);
    if j < np then
      result := result + ',';
  end;
end;

function TTitanScripter.GetParamName(SubID, ParamIndex: Integer): String;
begin
  result := fScripter.SymbolTable.GetParamName(SubID, ParamIndex);
end;

function TTitanScripter.GetTypeID(ID: Integer): Integer;
begin
  result := fScripter.GetTypeID(ID);
end;

function TTitanScripter.GetPosition(ID: Integer): Integer;
begin
  with fScripter do
  if (ID > 0) and (ID <= SymbolTable.Card) then
    result := SymbolTable.Position[ID]
  else
    result := 0;
end;

function TTitanScripter.GetStartPosition(ID: Integer): Integer;
begin
  with fScripter do
  if (ID > 0) and (ID <= SymbolTable.Card) then
    result := SymbolTable.StartPosition[ID]
  else
    result := 0;
end;

function TTitanScripter.GetModule(ID: Integer): Integer;
var
  D: TTitanDefinition;
begin
  with fScripter do
  if (ID > 0) and (ID <= SymbolTable.Card) then
    result := SymbolTable.Module[ID]
  else
  if ID < 0 then
  begin
    D := GlobalDefinitionList[-ID];
    result := D.Module;
  end
  else
    result := -1;
end;

function TTitanScripter.GetName(ID: Integer): String;
begin
  with fScripter do
    result := SymbolTable.Name[ID];
end;

function TTitanScripter.GetFullName(ID: Integer): String;
begin
  with fScripter do
    result := SymbolTable.FullName[ID];
end;

function TTitanScripter.GetValue(const Name: String): Variant;
begin
  result := GetValueByID(GetMemberID(Name));
end;

procedure TTitanScripter.SetValueByID(ID: Integer; const Value: Variant);
begin
  with fScripter do
  if (ID > 0) and (ID <= SymbolTable.Card) then
    SymbolTable.VariantValue[ID] := Value;
end;

procedure TTitanScripter.SetValue(const Name: String; const Value: Variant);
begin
  SetValueByID(GetMemberID(Name), Value);
end;

function TTitanScripter.CallFunctionByID(SubID: Integer; const Params: array of const; ObjectID: Integer = 0): Variant;
var  This: Variant;
begin
  case ScripterState of
    ssReadyToRun:
    begin
      fScripter.InitRunStage;
      fScripter.ClassList.InitStaticFields(0);
    end;
    ssTerminated:
    begin
      with fScripter.Code do
      begin
        DebugInfo.Clear;

//      UsingList.Clear;
//      WithStack.Clear;
        TryStack.Clear;
        RefStack.Clear;
        Stack.Clear;
        SubRunCount := 0;

      end;
      fScripter.VariantStack.Clear;
    end
  end;

  if ScripterState <> ssRunning then
  if Assigned(fOnBeforeRunStage) then
    fOnBeforeRunStage(Self);

  if ObjectID <> 0 then
    This := fScripter.SymbolTable.VariantValue[ObjectID]
  else
    This := Undefined;

  fScripter.State := Ord(ssRunning);
  result := fScripter.CallMethod(SubID, This, Params);
  ExtractCallStack;
  if IsObject(result) then
    result := PtrInt(VariantToScriptObject(result).Instance);

  if ScripterState <> ssRunning then
  begin
    if fScripter.Code.SignHaltGlobal then
    if Assigned(fScripter.OnHalt) then
      fScripter.OnHalt(Self);

    if Assigned(fOnAfterRunStage) then
      fOnAfterRunStage(Self);
  end;
end;

function TTitanScripter.CallFunction(const Name: String; const Params: array of const; AnObjectName: String = ''): Variant;
var
  SubID, ObjectID: Integer;
begin
  if ScripterState in [ssInit, ssReadyToCompile] then
  begin
    Compile;
    if IsError then
      Exit;
    result := CallFunction(Name, Params, AnObjectName);
    Exit;
  end;

  SubID := GetMemberID(Name);
  if SubID <> 0 then
  begin
    ObjectID := GetMemberID(AnObjectName);
    result := CallFunctionByID(SubID, Params, ObjectID);
  end
  else
    raise Exception.Create(Format(errFunctionNotFound, [Name]));
end;

function TTitanScripter.CallFunctionByIDEx(SubID: Integer;
                                         const Params: array of const;
                                         const StrTypes: array of String;
                                         ObjectID: Integer = 0): Variant;
var
  This: Variant;
begin
  case ScripterState of
    ssReadyToRun:
    begin
      fScripter.InitRunStage;
      fScripter.ClassList.InitStaticFields(0);
    end;
    ssTerminated:
    begin
      with fScripter.Code do
      begin
        DebugInfo.Clear;

//      UsingList.Clear;
//      WithStack.Clear;
        TryStack.Clear;
        RefStack.Clear;
        Stack.Clear;
        SubRunCount := 0;

      end;
      fScripter.VariantStack.Clear;
    end
  end;

  if ScripterState <> ssRunning then
  if Assigned(fOnBeforeRunStage) then
    fOnBeforeRunStage(Self);

  if ObjectID <> 0 then
    This := fScripter.SymbolTable.VariantValue[ObjectID]
  else
    This := Undefined;

  fScripter.State := Ord(ssRunning);
  result := fScripter.CallMethodEx(SubID, This, Params, StrTypes);
  ExtractCallStack;
  if IsObject(result) then
    result := PtrInt(VariantToScriptObject(result).Instance);

  if ScripterState <> ssRunning then
  begin
    if fScripter.Code.SignHaltGlobal then
      if Assigned(fScripter.OnHalt) then
         fScripter.OnHalt(Self);

    if Assigned(fOnAfterRunStage) then
      fOnAfterRunStage(Self);
  end;
end;

function TTitanScripter.CallFunctionEx(const Name: String; const Params: array of const;
                                     const StrTypes: array of String;
                                     AnObjectName: String = ''): Variant;
var
  SubID, ObjectID: Integer;
begin
  if ScripterState in [ssInit, ssReadyToCompile] then
  begin
    Compile;
    if IsError then
      Exit;
    result := CallFunction(Name, Params, AnObjectName);
    Exit;
  end;

  SubID := GetMemberID(Name);
  if SubID <> 0 then
  begin
    ObjectID := GetMemberID(AnObjectName);
    result := CallFunctionByIDEx(SubID, Params, StrTypes, ObjectID);
  end
  else
    raise Exception.Create(Format(errFunctionNotFound, [Name]));
end;

function TTitanScripter.CallMethod(const Name: String; const Params: array of const; Instance: TObject): Variant;
var
  SubID: Integer;
begin
  if ScripterState in [ssInit, ssReadyToCompile] then
  begin
    Compile;
    if IsError then Exit;
    result := CallMethod(Name, Params, Instance);
    Exit;
  end;

  SubID := GetMemberID(Name);
  if SubID <> 0 then
  begin
    result := CallMethodByID(SubID, Params, Instance);
  end
  else
    raise Exception.Create(Format(errMethodNotFound, [Name]));
end;

function TTitanScripter.CallMethod(const Name: String; const Params: array of const; const This: Variant): Variant;
var
  SubID: Integer;
begin
  if ScripterState in [ssInit, ssReadyToCompile] then
  begin
    Compile;
    if IsError then Exit;
    result := CallMethod(Name, Params, This);
    Exit;
  end;

  SubID := GetMemberID(Name);
  if SubID <> 0 then
  begin
    result := CallMethodByID(SubID, Params, This);
  end
  else
    raise Exception.Create(Format(errMethodNotFound, [Name]));
end;


function TTitanScripter.CallMethodByID(SubID: Integer; const Params: array of const; Instance: TObject): Variant;
var
  This: Variant;
begin
  case ScripterState of
    ssReadyToRun:
    begin
      fScripter.InitRunStage;
      fScripter.ClassList.InitStaticFields(0);
    end;
    ssTerminated:
    begin
      with fScripter.Code do
      begin
        DebugInfo.Clear;
//      UsingList.Clear;
//      WithStack.Clear;
        TryStack.Clear;
        RefStack.Clear;
        Stack.Clear;
        SubRunCount := 0;

      end;
      fScripter.VariantStack.Clear;
    end
  end;

  This := ScriptObjectToVariant(TyphonInstanceToScriptObject(Instance, fScripter));

  fScripter.State := Ord(ssRunning);
  result := fScripter.CallMethod(SubID, This, Params);
  ExtractCallStack;
  if IsObject(result) then
    result := PtrInt(VariantToScriptObject(result).Instance);
end;

function TTitanScripter.CallMethodByID(SubID: Integer; const Params: array of const; const This: Variant): Variant;
begin
  case ScripterState of
    ssReadyToRun:
    begin
      fScripter.InitRunStage;
      fScripter.ClassList.InitStaticFields(0);
    end;
    ssTerminated:
    begin
      with fScripter.Code do
      begin
        DebugInfo.Clear;

//      UsingList.Clear;
//      WithStack.Clear;
        TryStack.Clear;
        RefStack.Clear;
        Stack.Clear;
        SubRunCount := 0;

      end;
      fScripter.VariantStack.Clear;
    end
  end;

  fScripter.State := Ord(ssRunning);
  result := fScripter.CallMethod(SubID, This, Params);
  ExtractCallStack;
  if IsObject(result) then
    result := PtrInt(VariantToScriptObject(result).Instance);
end;


procedure TTitanScripter.InvokeOnAssignScript;
begin
  if Assigned(fOnAssignScript) then
  begin
    if ScripterState <> ssReadyToCompile then
    begin
      ScripterState := ssInit;
      fOnAssignScript(Self);
      ScripterState := ssReadyToCompile;
    end;
  end;
//  else
//    raise Exception.Create('OnAssignScript property is not assigned !');
end;

function TTitanScripter.Compile(SyntaxCheckOnly: Boolean = false): Boolean;

function HasSourceCodeModules: Boolean;
var
  I: Integer;
  M: TTitanModule;
begin
  for I:=0 to fScripter.Modules.Count - 1 do
  begin
    M := fScripter.Modules.Items[I];
    if M.IsSource then
    begin
      result := true;
      Exit;
    end;
  end;
  result := false;
end;

var
  P, I, J, K: Integer;
  M: TTitanModule;
  S, S1, FileExt: String;
  L: TStringList;
  Stream: TStream;
begin
  DiscardError;
  fScripter.Code.ResetCompileStage;
  fScripter.SymbolTable.ResetCompileStage;

  InvokeOnAssignScript;

  If Modules.Count = 0 then
    raise TTitanScriptFailure.Create(errScripterDosNotContainAnyModule);

  if HasSourceCodeModules then
    if Assigned(fOnBeforeCompileStage) then
       fOnBeforeCompileStage(Self);

  fScripter.State := Ord(ssCompiling);

  fScripter.AddDefs;
  fScripter.AddLocalDefs;

  fScripter.TotalLineCount := 0;
  for I:=0 to fScripter.Modules.Count - 1 do
  begin
    M := fScripter.Modules.Items[I];
    if M.IsSource then
    begin
      CompileModule(M.Name, SyntaxCheckOnly);
      if fScripter.IsError then
        Break;
    end
    else
    begin
      M.BuffStream.Position := 0;
      fScripter.LoadCompiledModuleFromStream(M, M.BuffStream);
    end;
  end;

  while fScripter.ExtraModuleList.Count > 0 do
  begin
    L := TStringList.Create;
    try
      for I:=0 to fScripter.ExtraModuleList.Count - 1 do
      begin
        S := fScripter.ExtraModuleList[I];

        P := Pos('=', S);
        if P > 0 then
        begin
          S1 := Copy(S, 1, P - 1);
          if Modules.IndexOf(S1) >= 0 then
            continue;
        end;

        L.Add(fScripter.ExtraModuleList[I]);
      end;

      fScripter.ExtraModuleList.Clear;

      for I:=0 to L.Count - 1 do
      begin
        S := L[I];

        if fScripter.IsCompiledScript(S) then
        begin
          Stream := TFileStream.Create(fScripter.FindFullName(S), fmOpenRead);
          try
            K := LoadInteger(Stream);
            for J:=0 to K - 1 do
            begin
              M := TTitanModule.Create(fScripter);
              M.FileName := fScripter.FindFullName(S);

              fScripter.LoadCompiledModuleFromStream(M, Stream);
              Modules.AddObject(M.Name, M);
              M.BuffStream.Position := 0;
              fScripter.LoadCompiledModuleFromStream(M, M.BuffStream);
            end;
          finally
            Stream.Free;
          end;
        end
        else
        begin
          M := TTitanModule.Create(fScripter);

          P := Pos('=', S);
          if P > 0 then
          begin
            M.Name := Copy(S, 1, P - 1);
            M.Text := Copy(S, P + 1, Length(S));
          end
          else
          begin
            M.Name := S;
            M.FileName := fScripter.FindFullName(M.Name);
          end;

          M.LanguageName := '';

          FileExt := ExtractFileExt(M.Name);

          if FileExt = '' then
          begin
            if StrEql('unit ', Copy(M.Text, 1, 5)) then
              M.LanguageName := 'TitanPascal';
          end;

          for J:=0 to fScripter.ParserList.Count - 1 do
            if FileExt = fScripter.ParserList[J].FileExt then
            begin
              M.LanguageName := fScripter.ParserList[J].LanguageName;
              Break;
            end;

          if M.LanguageName = '' then
          begin
            fScripter.Error := errUnknownLanguage;
            raise TTitanScriptFailure.Create(errUnknownLanguage);
          end
          else
          begin
            fScripter.Modules.AddObject(M.Name, M);
            if M.Text = '' then
              AddCodeFromFile(M.Name, M.FileName);

            CompileModule(M.Name, SyntaxCheckOnly);
            if fScripter.IsError then
              Break;
          end;
        end;
        if fScripter.IsError then
          Break;
      end;
    finally
      L.Free;
    end;
  end;

  SetScripterState(ssCompiled);

  if not IsError then if not SyntaxCheckOnly then
  begin
    fScripter.CheckForUndeclared;
    if IsError then
    begin
      Dump();
      if Assigned(fScripter.OnShowError) then
        fScripter.OnShowError(fScripter.Owner)
      else
        fScripter.ShowError;
    end;
  end;

  if IsError then
    ScripterState := ssReadyToCompile
  else
    fScripter.Link(false);

  if SyntaxCheckOnly then
    ScripterState := ssReadyToCompile;

  if HasSourceCodeModules then
    if Assigned(fOnAfterCompileStage) then
      fOnAfterCompileStage(Self);

  result := not  IsError;
end;

procedure TTitanScripter.RunInstruction;
begin
  with fScripter.Code do
    ArrProc[Prog[N].Op];
end;

function TTitanScripter.InstructionCount: Integer;
begin
  result := fScripter.Code.Card;
end;

function TTitanScripter.CurrentInstructionNumber: Integer;
begin
  result := fScripter.Code.N;
end;

function TTitanScripter.GetInstruction(N: Integer): TTitanInstruction;
begin
  with fScripter.Code.Prog[N] do
  begin
    result.N := N;
    result.Op := Op;
    result.Arg1 := Arg1;
    result.Arg2 := Arg2;
    result.Res := Res;
  end;
end;

procedure TTitanScripter.SetInstruction(N: Integer; I: TTitanInstruction);
begin
  with fScripter.Code.Prog[N] do
  begin
    Op := I.Op;
    Arg1 := I.Arg1;
    Arg2 := I.Arg2;
    Res := I.Res;
  end;
end;

procedure TTitanScripter.Run(RunMode: Integer = rmRun; const ModuleName: String = ''; LineNumber: Integer = 0);
var
  I, PCodeLineNumber: Integer;
  M: TTitanModule;
begin

  if not (ScripterState in [ssReadyToRun, ssPaused, ssTerminated, ssRunning]) then
  begin
    Compile;
    if IsError then
      Exit;
  end;

  case ScripterState of
    ssReadyToRun:
      fScripter.InitRunStage;
    ssTerminated:
    begin
      fScripter.ResetRunStage;
      fScripter.InitRunStage;
    end;
  end;

  if fScripter.IsError then
    Exit;

  if Assigned(fOnBeforeRunStage) then
    fOnBeforeRunStage(Self);

  PCodeLineNumber := fScripter.SourceLineToPCodeLine(ModuleName, LineNumber);

  if (LineNumber = 0) and (ModuleName <> '') then
    for I:=1 to fScripter.RunList.Card do
      if fScripter.RunList[I] = 0 then
        fScripter.RunList.fItems[I] := PCodeLineNumber;

  if (RunMode in [rmRun, rmStepOver, rmTraceInto]) and (PCodeLineNumber <> -1) then
  begin
    if LineNumber = 0 then
    while fScripter.Code.Prog[PCodeLineNumber].Op <> OP_BEGIN_WITH do
      Dec(PCodeLineNumber);
    fScripter.Code.N := PCodeLineNumber - 1;

    fScripter.Run(RunMode, -1);
  end
  else
  begin
    //run module
    if (ModuleName <> '') and (LineNumber = 0) then
    begin
      I := fScripter.Modules.IndexOf(ModuleName);
      if I <> -1 then
      begin
        M := fScripter.Modules.Items[I];
        fScripter.Code.N := M.P1 - 1;
        fScripter.Run(RunMode, -1);
      end;
    end //run module
    else
      fScripter.Run(RunMode, PCodeLineNumber);
  end;
  ExtractCallStack;

  if fScripter.Code.SignHaltGlobal then
  if Assigned(fScripter.OnHalt) then
    fScripter.OnHalt(Self);

  if Assigned(fOnAfterRunStage) then
    fOnAfterRunStage(Self);
end;

procedure TTitanScripter.Dump;
begin
  fScripter.Dump;
end;

function TTitanScripter.IsError: Boolean;
begin
  result := fScripter.IsError;
end;

procedure TTitanScripter.DiscardError;
begin
  fScripter.DiscardError;
end;

function TTitanScripter.GetOverrideHandlerMode: TTitanOverrideHandlerMode;
begin
  result := TTitanOverrideHandlerMode(fScripter.OverrideHandlerMode);
end;

procedure TTitanScripter.SetOverrideHandlerMode(Value: TTitanOverrideHandlerMode);
begin
  fScripter.OverrideHandlerMode := PtrInt(Value);
end;

function TTitanScripter.GetScripterState: TScripterState;
begin
  result := TScripterState(fScripter.State);
end;

procedure TTitanScripter.SetScripterState(Value: TScripterState);

procedure SetInit;
begin
  case ScripterState of
    ssInit: begin end;
  else
    begin
      fScripter.Modules.Clear;
      fScripter.ResetCompileStage;
    end;
  end;
  fScripter.State := Ord(ssInit);

  DiscardError;
end;

procedure SetReadyToCompile;
begin
  case ScripterState of
    ssInit: begin end;
    ssReadyToCompile: begin end;
    ssCompiling:
      begin
        fScripter.ResetCompileStage;
      end;
    ssCompiled:
      begin
        fScripter.ResetCompileStage;
      end;
    ssLinking:
      begin
        fScripter.ResetCompileStage;
      end;
    ssReadyToRun:
      begin
        fScripter.ResetCompileStage;
      end;
    ssRunning:
      begin
        fScripter.ResetRunStage;
        fScripter.ResetCompileStage;
      end;
    ssPaused:
      begin
        fScripter.ResetCompileStage;
      end;
    ssTerminated:
      begin
        fScripter.ResetCompileStage;
      end;
  end;

  fScripter.State := Ord(ssReadyToCompile);
end;

procedure SetCompiling;
var
  I: Integer;
begin
  fScripter.State := Ord(ssReadyToCompile);
  fScripter.State := Ord(ssCompiling);

  fScripter.TotalLineCount := 0;
  for I:=0 to fScripter.Modules.Count - 1 do
  begin
    CompileModule(fScripter.Modules.Items[I].Name);
    if fScripter.IsError then
      Break;
  end;

  SetScripterState(ssCompiled);
end;

procedure SetCompiled;
begin
  fScripter.State := Ord(ssCompiled);
end;

procedure SetLinking;
begin
  fScripter.State := Ord(ssLinking);
end;

procedure SetReadyToRun;
begin
  case ScripterState of
    ssReadyToCompile:
      begin
        SetScripterState(ssCompiling);
        if not IsError then
          SetScripterState(ssReadyToRun);
      end;
    ssCompiled:
    begin
      fScripter.Link(true);
      fScripter.State := Ord(ssReadyToRun);
    end;
    ssPaused:
    begin
      fScripter.ResetRunStage;
    end;
    ssTerminated:
    begin
      fScripter.ResetRunStage;
    end;
  end;
  fScripter.State := Ord(ssReadyToRun);
//  fScripter.InitRunStage;
end;

procedure SetRunning;
begin
  fScripter.State := Ord(ssRunning);
  fScripter.Code.Terminated := false;
end;

procedure SetPaused;
begin
  if ScripterState = ssTerminated then
  begin
    fScripter.ResetRunStage;
    fScripter.InitRunStage;
  end;

  fScripter.State := Ord(ssPaused);
  fScripter.Code.CurrRunMode := _rmTraceInto;
end;

procedure SetTerminated;
begin
  fScripter.State := Ord(ssTerminated);
  fScripter.Code.Terminated := true;
  fScripter.Code.N := fScripter.Code.Card;
end;

begin
  case Value of
    ssInit: SetInit;
    ssReadyToCompile: SetReadyToCompile;
    ssCompiling: SetCompiling;
    ssCompiled: SetCompiled;
    ssLinking: SetLinking;
    ssReadyToRun: SetReadyToRun;
    ssRunning: SetRunning;
    ssPaused: SetPaused;
    ssTerminated: SetTerminated;
  end;
end;

procedure TTitanScripter.RemoveAllBreakpoints;
begin
  fScripter.RemoveAllBreakpoints;
end;

function TTitanScripter.AddBreakpoint(const ModuleName: String;
                                    LineNumber: Integer; const Condition: String = ''; PassCount: Integer = 0): Boolean;
begin
  result := fScripter.AddBreakpoint(ModuleName, LineNumber, Condition, PassCount);
end;

function TTitanScripter.RemoveBreakpoint(const ModuleName: String;
                                    LineNumber: Integer): Boolean;
begin
  result := fScripter.RemoveBreakpoint(ModuleName, LineNumber, '', 0);
end;

function TTitanScripter.GetCurrentSourceLine: Integer;
begin
  result := fScripter.Code.LineID;
end;

function TTitanScripter.GetCurrentModuleName: String;
var
  ModuleID: Integer;
begin
  ModuleID := fScripter.Code.ModuleID;
  if ModuleID = -1 then
    result := ''
  else
    result := fScripter.Modules.Items[ModuleID].Name;
end;

function TTitanScripter.ToString(const V: Variant): String;
begin
  result := ToStr(fScripter, V);
end;

procedure TTitanScripter.ExtractCallStack;
var
  I, J, N, SubID, ParamCount: Integer;
  CSR: TCallStackRecord;
  P: Pointer;
begin
  CallStack.Clear;
  I := fScripter.Code.DebugInfo.Card;

  repeat

    if I = 0 then
      Exit;

    CSR := TCallStackRecord.Create;

    SubID := fScripter.Code.DebugInfo[I];
    Dec(I);
    N := fScripter.Code.DebugInfo[I];
    Dec(I);
    ParamCount := fScripter.Code.DebugInfo[I];

    for J:=1 to ParamCount do
    begin
      Dec(I);
      P := Pointer(fScripter.Code.DebugInfo[I]);
      CSR.Parameters.Add(ToStr(fScripter, Variant(P^)));
    end;

    CSR.ProcName := fScripter.SymbolTable.Name[SubID];

    fScripter.Code.SaveState;
    fScripter.Code.N := N;

    CSR.LineNumber := CurrentSourceLine;
    CSR.ModuleName := CurrentModuleName;

    fScripter.Code.RestoreState;

    CallStack.Add(CSR);

    Dec(I);
  until false;
end;

function TTitanScripter.GetSourceCode(const ModuleName: String): String;
var
  I: Integer;
begin
  I := fScripter.Modules.IndexOf(ModuleName);
  if I = -1 then
    raise Exception.Create(Format(errModuleNotFound, [ModuleName]));

  result := fScripter.Modules.Items[I].Text;
end;

procedure TTitanScripter.SetSourceCode(const ModuleName, SourceCode: String);
var
  I: Integer;
begin
  I := fScripter.Modules.IndexOf(ModuleName);
  if I = -1 then
    raise Exception.Create(Format(errModuleNotFound, [ModuleName]));

  fScripter.Modules.Items[I].Text := SourceCode;
end;

procedure TTitanScripter.LoadProject(const FileName: String);
var
  L: TStringList;
  I: Integer;
begin
  if not FileExists(FileName) then
    raise Exception.Create(Format(errFileNotFound, [FileName]));

  fScripter.Modules.Clear;
  L := TStringList.Create;
  try
    L.LoadFromFile(FileName);
    for I:=0 to L.Count - 1 do
    if Pos('#!', L[I]) = 0 then
    begin
      AddModule(L[I], FileExtToLanguageName(L[I]));
      AddCodeFromFile(L[I], L[I]);
    end;
  finally
    L.Free;
  end;
end;

function RunFile(const FileName: String; TitanScripter: TTitanScripter): Boolean;
begin
  TitanScripter.ResetScripter;
  TitanScripter.LoadProject(FileName);
  TitanScripter.Run;
  result := TitanScripter.IsError;
end;

function RunString(const Script, LanguageName: String; TitanScripter: TTitanScripter): Boolean;
const
  ModuleName = 'main';
begin
  TitanScripter.ResetScripter;
  TitanScripter.AddModule(ModuleName, LanguageName);
  TitanScripter.AddCode(ModuleName, Script);
  TitanScripter.Run;
  result := TitanScripter.IsError;
end;

function TTitanScripter.GetErrorClassType: TClass;
begin
  result := fScripter.ErrorInstance.ErrClassType;
end;

function TTitanScripter.GetErrorDescription: String;
begin
  result := fScripter.ErrorInstance.Description;
end;

function TTitanScripter.GetErrorModuleName: String;
begin
  result := fScripter.ErrorInstance.ModuleName;
end;

function TTitanScripter.GetErrorTextPos: Integer;
begin
  result := fScripter.ErrorInstance.TextPosition;
end;

function TTitanScripter.GetErrorPos: Integer;
begin
  result := fScripter.ErrorInstance.Position;
end;

function TTitanScripter.GetErrorLine: Integer;
begin
  result := fScripter.ErrorInstance.LineNumber
end;

function TTitanScripter.GetErrorMethodId: Integer;
begin
  result := fScripter.ErrorInstance.MethodId;
end;

procedure TTitanScripter.RegisterVariable(const Name, TypeName: String; Address: Pointer; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
  D: TTitanVariableDefinition;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  D := TTitanVariableDefinition(fScripter.LocalDefinitions.Lookup(Name, dkVariable, O, fScripter));
  if D <> nil then
  begin
    UnregisterVariable(Name, Owner);
    RegisterVariable(Name, TypeName, Address, Owner);
    Exit;
  end;

  fScripter.LocalDefinitions.AddVariable(Name, TypeName, Address, O, fScripter);
end;

procedure TTitanScripter.RegisterConstant(const Name: String; Value: Variant; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
  D: TTitanConstantDefinition;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  D := TTitanConstantDefinition(fScripter.LocalDefinitions.Lookup(Name, dkConstant, O, fScripter));
  if D <> nil then
  begin
    UnregisterConstant(Name, Owner);
    RegisterConstant(Name, Value, Owner);
    Exit;
//    D.Value := Value;
    Exit;
  end;

  fScripter.LocalDefinitions.AddConstant(Name, Value, O, true, ckNone, fScripter);
end;

procedure TTitanScripter.RegisterObject(const Name: String; Instance: TObject; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
  C: TComponent;
  I: Integer;
  AClass: TClass;
  D: TTitanObjectDefinition;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  D := TTitanObjectDefinition(fScripter.LocalDefinitions.Lookup(Name, dkObject, O, fScripter));
  if D <> nil then
  begin
    UnregisterObject(Name, Owner);
    RegisterObject(Name, Instance, Owner);
    Exit;
  end;

  if Instance.InheritsFrom(TComponent) then
  begin
    C := TComponent(Instance);
    for I:=0 to C.ComponentCount - 1 do
    begin
      AClass := C.Components[I].ClassType;

      if GlobalDefinitionList.FindClassDef(AClass) = nil then
        GlobalDefinitionList.AddClass2(AClass, nil, nil, nil);
    end;
  end;

  if GlobalDefinitionList.FindClassDef(Instance.ClassType) = nil then
     GlobalDefinitionList.AddClass2(Instance.ClassType, nil, nil, nil);

  fScripter.LocalDefinitions.AddObject(Name, Instance, O, fScripter);
end;

procedure TTitanScripter.RegisterVirtualObject(const Name: String; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  fScripter.LocalDefinitions.AddVirtualObject(Name, O, fScripter);
end;

procedure TTitanScripter.RegisterObjectSimple(const Name: String; Instance: TObject; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];
  fScripter.LocalDefinitions.AddObject(Name, Instance, O, fScripter);
end;

procedure TTitanScripter.RegisterInterfaceVar(const Name: String; PIntf: PUnknown;
                                            const guid: TGUID;
                                            Owner: Integer = -1);
var
  O: TTitanClassDefinition;
  D: TTitanInterfaceVarDefinition;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  D := TTitanInterfaceVarDefinition(fScripter.LocalDefinitions.Lookup(Name, dkInterface, O, fScripter));
  if D <> nil then
  begin
//    if D.PIntf <> nil then
//      D.PIntf^ := nil;
    D.PIntf := PIntf;
    D.guid := guid;
    Exit;
  end;

  fScripter.LocalDefinitions.AddInterfaceVar(Name, PIntf, guid, O, fScripter);
end;

procedure TTitanScripter.Unregister(What: TTitanDefKind; const Name: String; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
  ClassRec: TTitanClassRec;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  fScripter.LocalDefinitions.Unregister(Name, What, O, Self.fScripter);

  if O = nil then
    ClassRec := fScripter.ClassList[0]
  else
    ClassRec := fScripter.ClassList.FindClassByName(O.Name);
  ClassRec.MemberList.DeleteMember(Name, true);
end;

procedure TTitanScripter.UnregisterObject(const Name: String; Owner: Integer = -1);
var
  D: TTitanObjectDefinition;
  O: TTitanClassDefinition;
  I: Integer;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  D := TTitanObjectDefinition(fScripter.LocalDefinitions.Lookup(Name, dkObject, O, fScripter));
  if D <> nil then
  begin
    I := fScripter.ScriptObjectList.IndexOfTyphonObject(D.Instance);
    if I >= 0 then
      fScripter.ScriptObjectList.Delete(I);
  end;

  Unregister(dkObject, Name, Owner);
end;

procedure TTitanScripter.UnregisterObject(Instance: TObject; Owner: Integer = -1);
var
  O: TTitanClassDefinition;
  D: TTitanObjectDefinition;
  I: Integer;
  ClassRec: TTitanClassRec;
begin
  if Owner = -1 then
    O := nil
  else
    O := GlobalDefinitionList[Owner];

  I := fScripter.ScriptObjectList.IndexOfTyphonObject(Instance);
  if I >= 0 then
    fScripter.ScriptObjectList.Delete(I);

  D := fScripter.LocalDefinitions.FindObjectDef(Instance, O);
  if D <> nil then
  begin
    fScripter.LocalDefinitions.UnregisterObject(Instance, O);
    if O = nil then
      ClassRec := fScripter.ClassList[0]
    else
      ClassRec := fScripter.ClassList.FindClassByName(O.Name);
    ClassRec.MemberList.DeleteMember(D.Name, true);
  end;
end;

procedure TTitanScripter.UnregisterConstant(const Name: String; Owner: Integer = -1);
begin
  Unregister(dkConstant, Name, Owner);
end;

procedure TTitanScripter.UnregisterVariable(const Name: String; Owner: Integer = -1);
begin
  Unregister(dkVariable, Name, Owner);
end;

procedure TTitanScripter.UnregisterAllVariables;
begin
  fScripter.LocalDefinitions.UnregisterAll(dkVariable, Self.fScripter);
end;

procedure TTitanScripter.UnregisterAllObjects;
var
  I, Index: Integer;
  D: TTitanObjectDefinition;
begin
  for I:=0 to fScripter.LocalDefinitions.Count - 1 do
  if Assigned(fScripter.LocalDefinitions.Records[I]) and
     (fScripter.LocalDefinitions.Records[I].DefKind = dkObject) then
  begin
    D := TTitanObjectDefinition(fScripter.LocalDefinitions.Records[I]);
    Index := fScripter.ScriptObjectList.IndexOfTyphonObject(D.Instance);
    if Index >= 0 then
      fScripter.ScriptObjectList.Delete(Index);
  end;
  fScripter.LocalDefinitions.UnregisterAll(dkObject, Self.fScripter);
end;

procedure TTitanScripter.UnregisterAllConstants;
begin
  fScripter.LocalDefinitions.UnregisterAll(dkConstant, Self.fScripter);
end;

procedure TTitanScripter.ForbidAllPublishedProperties(AClass: TClass);
begin
  fScripter.ForbiddenPublishedProperties.Add(AClass);
end;

procedure TTitanScripter.ForbidPublishedProperty(AClass: TClass; const PropName: String);
begin
  fScripter.ForbiddenPublishedPropertiesEx.AddObject(PropName, TObject(AClass));
end;

procedure TTitanScripter.GetClassInfo(const FullName: String; mk: TTitanMemberKind; L: TStrings);
var
  ClassRec: TTitanClassRec;
  MemberRec: TTitanMemberRec;
  I, ID: integer;
  S: String;
  ClassDef: TTitanClassDefinition;
  PClass: TClass;
begin
  ID := GetMemberID(FullName);
  if ID = 0 then
    Exit;

  ClassRec := fScripter.ClassList.FindClass(ID);
  PClass := nil;

  repeat

    if ClassRec = nil then
      break;

    if PClass = nil then
    begin
      ClassDef := ClassRec.GetClassDef;
      if ClassDef <> nil then
        PClass := ClassDef.PClass;
    end;


    for I:= 0 to ClassRec.MemberList.Count - 1 do
    begin
      MemberRec := ClassRec.MemberList.Records[I];
      if MemberRec.ID <> 0 then
      begin
        S := MemberRec.GetName;
        if L.IndexOf(S) >= 0 then
           continue;
        case mk of
          mkMethod:
          begin
            if MemberRec.Kind = KindSUB then
            begin
              if PosCh('_', S) > 0 then
                continue;
              L.AddObject(S, TObject(MemberRec.ID));
            end;
          end;
          mkField:
          begin
            if MemberRec.Kind = KindVAR then
              L.AddObject(S, TObject(MemberRec.ID));
          end;
          mkProp:
          begin
            if MemberRec.Kind = KindPROP then
              L.AddObject(S, TObject(MemberRec.ID));
          end;
        end;
      end;
    end;

    if ClassRec.AncestorClassRec = nil then break;
    ClassRec := ClassRec.AncestorClassRec;

  until false;

  if mk = mkProp then
  if PClass <> nil then
    GetPublishedPropertiesEx2(PClass, L);

  if mk = mkEvent then
  if PClass <> nil then
    GetPublishedEvents(PClass, L);
end;

function TTitanScripter.GetHostClass(const FullName: String): TClass;
var
  ClassRec: TTitanClassRec;
  ID: integer;
  ClassDef: TTitanClassDefinition;
begin
  result := nil;

  ID := GetMemberID(FullName);
  if ID = 0 then
    Exit;
  ClassRec := fScripter.ClassList.FindClass(ID);
  repeat
    if ClassRec = nil then
      break;
    if result = nil then
    begin
      ClassDef := ClassRec.GetClassDef;
      if ClassDef <> nil then
        result := ClassDef.PClass;
    end;
    if ClassRec.AncestorClassRec = nil then break;
    ClassRec := ClassRec.AncestorClassRec;
  until false;
end;


procedure TTitanScripter.SaveToStream(S: TStream);
begin
  fScripter.SaveToStream(S);
end;

procedure TTitanScripter.LoadFromStream(S: TStream);
begin
  SetUpSearchPathes;

  fScripter.LoadFromStream(S);
  fScripter.State := Ord(ssReadyToCompile);
end;

procedure TTitanScripter.SaveToFile(const FileName: String);
var
  S: TStream;
begin
  S := TFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(S);
  finally
    S.Free;
  end;
end;

procedure TTitanScripter.LoadFromFile(const FileName: String);
var
  S: TStream;
begin
  S := TFileStream.Create(FileName, fmOpenRead);
  try
    LoadFromStream(S);
  finally
    S.Free;
  end;
end;

procedure TTitanScripter.ResetScripter;
begin
  ScripterState := ssInit;
  fScripter._ObjCount := 0;
end;

procedure TTitanScripter.ResetScripterEx;
begin
  UnregisterAllObjects;

  fScripter.Modules.Clear;
  fScripter.ResetScripterEx;

  fScripter.State := Ord(ssInit);
  DiscardError;
  fScripter._ObjCount := 0;
end;


procedure TTitanScripter.Terminate;
begin
  fScripter.Code.Terminated := true;
end;

procedure TTitanScripter.DisconnectObjects;
begin
  fScripter.DisconnectObjects;
end;

function TTitanScripter.GetParam(const ParamName: String): Variant;
begin
  result := fScripter.ParamList.GetParam(ParamName);
end;

procedure TTitanScripter.SetParam(const ParamName: String; const Value: Variant);
begin
  fScripter.ParamList.SetParam(ParamName, Value);
end;

procedure TTitanScripter.RegisterField(const ObjectName: String;
                                     ObjectType: String;
                                     FieldName: String;
                                     FieldType: String;
                                     Address: Pointer);
begin
  fScripter.RegisteredFieldList.AddRec(ObjectName, ObjectType,
                                       FieldName, FieldType, Address);
end;

function TTitanScripter.GetLastResult: Variant;
begin
  with fScripter do
  if LastResultID = 0 then
    result := Undefined
  else
  begin
    if LastResultID > SymbolTable.Card then
      result := fLastResult
    else
      result := SymbolTable.GetVariant(LastResultID)
  end;
end;

function TTitanScripter.GetSourceLine(const ModuleName: String; LineNumber: Integer): String;
var
  L: TStringList;
begin
  L := TStringList.Create;
  L.Text := GetSourceCode(ModuleName);
  if (LineNumber >= 0) and (LineNumber < L.Count - 1) then
    result := L[LineNumber]
  else
    result := '';
  L.Free;
end;

function TTitanScripter.IsExecutableSourceLine(const ModuleName: String; L: Integer): Boolean;
begin
  result := fScripter.Code.IsExecutableSourceLine(ModuleName, L);
end;

procedure TTitanScripter.CancelCompiling(const AMessage: String);
begin
  if ScripterState = ssCompiling then
     fScripter.CancelMessage := AMessage;
end;

function TTitanScripter.GetRootID: Integer;
begin
  result := fScripter.ClassList[0].ClassID;
end;

function TTitanScripter.ToScriptObject(TyphonInstance: TObject): Variant;
begin
  result := ScriptObjectToVariant(TyphonInstanceToScriptObject(TyphonInstance, fScripter));
end;

function TTitanScripter.AssignEventHandler(Instance: TObject; EventPropName: String; EventHandlerName: String): Boolean;
var
  SubID: Integer;
  V, This: Variant;
  SO: TTitanScriptObject;
  pti: PTypeInfo;
  PropInfo: PPropInfo;
  EventHandler: TTitanEventHandler;
  M: TMethod;
begin
  result := false;
  SubID := GetMemberID(EventHandlerName);
  if (SubID = 0) or (SubID = -1) then Exit;
  V := fScripter.SymbolTable.GetVariant(SubID);
  SO := VariantToScriptObject(V);

  pti := PTypeInfo(Instance.ClassType.ClassInfo);

  if pti = nil then
    PropInfo := nil else
    PropInfo := GetPropInfo(pti, EventPropName);

  if PropInfo = nil then Exit;


  if PropInfo.PropType^.Kind = tkMethod then

  begin
    if SO.ClassRec.AncestorName = 'Function' then
      SubID := SO.ClassRec.GetConstructorID   else
      SubID := TTitanDelegate(SO.Instance).SubID;

    This := ScriptObjectToVariant(TyphonInstanceToScriptObject(Instance, fScripter));


    EventHandler := TTitanEventHandler.Create(fScripter, PropInfo.PropType, SubID, This);


    M.Code := @TTitanEventHandler.HandleEvent;
    M.Data := Pointer(EventHandler);

    EventHandler.TyphonInstance := Instance;
    EventHandler.PropInfo := PropInfo;
    EventHandler.This := Undefined;

    fScripter.EventHandlerList.Add(EventHandler);
    SetMethodProp(Instance, PropInfo, M); // Look at TypInfo.pas
    

    result := true;
  end;
end;

procedure TTitanScripter.EnumMembers(ID: Integer;
                                   Module: Integer;
                                   CallBack: TTitanMemberCallBack;
                                   Data: Pointer);
var
  I, K, N, IDL, ResID, SubID: Integer;
  mk: TTitanMemberKind;
  MemberRec: TTitanMemberRec;
  ClassRec, MemberClassRec: TTitanClassRec;
  L: TStringList;
begin
  if ID = 0 then
    Exit;

  K := fScripter.SymbolTable.Kind[ID];
  if K = KindTYPE then
  begin
    ClassRec := fScripter.ClassList.FindClass(ID);
    if ClassRec = nil then
      Exit;

    if ClassRec.IsImported then
    if ClassRec.fClassDef <> nil then
    if ClassRec.fClassDef.PClass <> nil then
    begin
      L := TStringList.Create;
      GetPublishedPropertiesEx(ClassRec.fClassDef.PClass, L);
      for I:=0 to L.Count - 1 do
      begin
        CallBack(L[I], 0, mkProp, [], Data);
      end;
      L.Free;
    end;

    for I:=0 to ClassRec.MemberList.Count - 1 do
    begin
      MemberRec := ClassRec.MemberList[I];
      mk := mkUnknown;

      case MemberRec.Kind of
        KindVAR: mk := mkField;
        KindCONST: mk := mkConst;
        KindSUB: mk := mkMethod;
        KindPROP:
        begin
          mk := mkProp;
          if MemberRec.ID = 0 then
            if MemberRec.Definition <> nil then
              MemberRec.ID := - MemberRec.Definition.Index;
        end;
        KindTYPE:
        begin
          MemberClassRec := fScripter.ClassList.FindClass(MemberRec.ID);
          if MemberClassRec <> nil then
          case MemberClassRec.ck of
            ckClass:
            begin
              mk := mkClass;
              if MemberClassRec.IsStatic then
                mk := mkNamespace;
            end;
            ckStructure: mk := mkStructure;
            ckEnum: mk := mkEnum;
          end;
        end;
      end;

      if MemberRec.ID > 0 then
      begin
       if (Module = GetModule(MemberRec.ID)) then
          CallBack(MemberRec.GetName(), MemberRec.ID, mk,
                   TTitanModifierList(MemberRec.ml), Data)
        else if (mk = mkClass) and (Module >= 0) then
          CallBack(MemberRec.GetName(), MemberRec.ID, mk,
                   TTitanModifierList(MemberRec.ml), Data)
        else if (mk = mkNamespace) and (Module >= 0) then
          CallBack(MemberRec.GetName(), MemberRec.ID, mk,
                   TTitanModifierList(MemberRec.ml), Data);
      end
      else if MemberRec.ID <> 0 then
      begin
        if (Module < 0) or (Module > MaxModuleNumber) then
        if Module = GetModule(MemberRec.ID) then
          CallBack(MemberRec.GetName(), MemberRec.ID, mk,
                   TTitanModifierList(MemberRec.ml), Data);
      end;
    end;
  end
  else if K = KindSUB then
  begin
     SubID := ID;

     N := fScripter.SymbolTable.Count[SubID];
     IDL := fScripter.SymbolTable.GetParamID(SubID, N);
     ResID := fScripter.SymbolTable.GetResultID(SubID);

     for ID := SubID + 1 to fScripter.SymbolTable.Card do
       if fScripter.SymbolTable.Level[ID] = SubID then
       begin
         K := fScripter.SymbolTable.Kind[ID];
         mk := mkUnknown;
         if ID = ResID then
           mk := mkResult
         else if (ID > ResID + 1) and (ID <= IDL) then
           mk := mkParam
         else
           case K of
             KindVAR:
               if ChPos('$', fScripter.SymbolTable.Name[ID]) = 0 then
                 mk := mkField;
             KindSUB: mk := mkMethod;
             KindTYPE:
             begin
               ClassRec := fScripter.ClassList.FindClass(ID);
               if ClassRec <> nil then
                 case ClassRec.ck of
                   ckClass: mk := mkClass;
                   ckStructure: mk := mkStructure;
                   ckEnum: mk := mkEnum;
                 end;
             end;
           end;

         if fScripter.SymbolTable.Module[ID] = Module then
            if fScripter.SymbolTable.IsLocal(ID) or (mk = mkParam) or
               (ID = fScripter.SymbolTable.GetThisID(SubID))
            then
              CallBack(fScripter.SymbolTable.Name[ID], ID, mk, [], Data);
       end;
  end;
end;

procedure TTitanScripter.Rename(ID: Integer; const NewName: String);
var
  OldName: String;
  L, OldNameIndex, NewNameIndex, I: Integer;
  ClassRec: TTitanClassRec;
  MemberRec: TTitanMemberRec;
begin
  if ID <= 0 then
    Exit;
  OldName := fScripter.SymbolTable.Name[ID];
  fScripter.SymbolTable.Name[ID] := NewName;

  L := fScripter.SymbolTable.Level[ID];
  ClassRec := fScripter.ClassList.FindClass(L);
  if ClassRec = nil then Exit;

  OldNameIndex := CreateNameIndex(OldName, fScripter);
  NewNameIndex := CreateNameIndex(NewName, fScripter);

  MemberRec := ClassRec.GetMember(OldNameIndex);

  if MemberRec = nil then Exit;

  I := ClassRec.MemberList.IndexOf(OldNameIndex);
  if I >= 0 then
    ClassRec.MemberList.NameID[I] := NewNameIndex;
end;

function TTitanScripter.RegisterParser(ParserClass: TTitanParserClass; const LanguageName, FileExt: String;
                                     CallConvention: TTitanCallConv): Integer;
var
  Parser: TTitanParser;
begin
  Parser := ParserClass.Create;

  result := fScripter.ParserList.IndexOf(Parser);
  if result = -1 then
  begin
    result := fScripter.ParserList.Add(Parser);
    Parser.LanguageName := LanguageName;
    Parser.FileExt := FileExt;
    Parser.DefaultCallConv := Ord(CallConvention);
  end;
end;

procedure TTitanScripter.RegisterLanguages;
var
  I: Integer;
  C: TComponent;
  L: TTitanLanguage;
begin
  if Owner = nil then
    Exit;

  for I:=0 to Owner.ComponentCount - 1 do
  begin
    C := TComponent(Owner.Components[I]);
    if C.InheritsFrom(TTitanLanguage) then
    begin
      L := TTitanLanguage(C);
      if FindLanguage(L.GetLanguageName) = nil then
        RegisterLanguage(L);
    end;
  end;
end;

procedure TTitanScripter.UnregisterLanguages;
var
  I: Integer;
  C: TComponent;
  L: TTitanLanguage;
begin
  if Owner = nil then
    Exit;

  for I:=0 to Owner.ComponentCount - 1 do
  begin
    C := TComponent(Owner.Components[I]);
    if C.InheritsFrom(TTitanLanguage) then
    begin
      L := TTitanLanguage(C);
      if FindLanguage(L.LanguageName) <> nil then
        UnRegisterLanguage(L.LanguageName);
    end;
  end;
end;

procedure TTitanScripter.RegisterLanguage(L: TTitanLanguage);
var
  I: Integer;
begin
  I := RegisterParser(L.GeTTitanParserClass,
                      L.GetLanguageName, '.' + L.GetFileExt,
                      L.CallConvention);
  fScripter.ParserList[I]._Language := L;
  fScripter.ParserList[I].DefaultCallConv := Ord(L.CallConvention);

  L.RegisterScripter(Self);
end;

procedure TTitanScripter.UnregisterLanguage(const LanguageName: String);
var
  I: Integer;
begin
  for I:=fScripter.ParserList.Count - 1 downto 0 do
    if StrEql(fScripter.ParserList[I].LanguageName, LanguageName) then
    begin
      TTitanLanguage(fScripter.ParserList[I]._Language).UnregisterScripter(Self);
      fScripter.ParserList.RemoveParser(I);
      Break;
    end;
end;

procedure TTitanScripter.AssignEventHandlerRunner(MethodAddress: Pointer;
                                                Instance: TObject);
begin
  fScripter.OwnerEventHandlerMethod.Code := MethodAddress;
  fScripter.OwnerEventHandlerMethod.Data := Instance;
end;

procedure TTitanScripter.SetUpSearchPathes;
var
  I: Integer;
  S: String;
begin
  fScripter.fSearchPathes.Clear;
  for I:=0 to fSearchPathes.Count - 1 do
  begin
    S := fSearchPathes[I];
    if Length(S) > 0 then
    begin
      if S[Length(S)] <> PathDelim then
        S := S + PathDelim;

      fScripter.fSearchPathes.Add(S);
    end;
  end;
end;

function TTitanScripter.FindFullFileName(const FileName: String): String;
begin
  result := fScripter.FindFullName(FileName);
end;

function TTitanScripter.FindNamespaceHandle(const Name: String): Integer;
var
  I: Integer;
begin
  for I:=0 to fScripter.ClassList.Count - 1 do
    if fScripter.ClassList[I].Name = Name then
    begin
      result := fScripter.ClassList[I].ClassID;
      Exit;
    end;
  result := -1;
end;

procedure TTitanScripter.ScanProperties(const ObjectName: String);
var
  L: TStringList;
  A: array[0..10] of Variant;

//==============================================================

procedure Push(const S: String; const V: Variant);
begin
  L.Add(S);
  A[L.Count - 1] := V;
end;

procedure Pop;
begin
  L.Delete(L.Count - 1);
end;

function GetFullName(const S: String): String;
var
  I: Integer;
begin
  result := '';
  for I:=0 to L.Count - 1 do
    result := result + L[I] + '.';
  result := result + S;
end;

procedure Backtrak(const V: Variant; const PropName: String);
var
  I, K: Integer;
  temp: Variant;
  S: String;
begin
  if IsPaxObject(V) then
  begin
    Push(PropName, V);
    K := GeTTitanObjectPropertyCount(V);
    for I:=0 to K - 1 do
    begin
      temp := GeTTitanObjectPropertyByIndex(V, I);
      S := GeTTitanObjectPropertyNameByIndex(V, I);
      Backtrak(temp, S);
    end;
    Pop;
  end
  else
  begin // terminal branch
    if Assigned(OnScanProperties) then
    begin
      temp := V;
      OnScanProperties(Self, GetFullName(PropName), temp);

      if VarType(temp) <> VarType(V) then
        PuTTitanObjectProperty(A[L.Count - 1], PropName, temp)
      else if V <> temp then
        PuTTitanObjectProperty(A[L.Count - 1], PropName, temp);
    end;
  end;
end;

procedure Backtraking(const V: Variant; const PropName: String);
begin
  L := TStringList.Create;
  Backtrak(V, PropName);
  L.Free;
end;

var
  V: Variant;
begin
  V := Values[ObjectName];
  Backtraking(V, ObjectName);
end;

//====================== TTitanLanguage ====================================


constructor TTitanLanguage.Create(AOwner: TComponent);
var
  I: Integer;
  C: TComponent;
  S: TTitanScripter;
begin
  inherited;

  fScripters := TList.Create;
  fContainer := nil;
  fInformalName := '';
  fLongStrLiterals := true;
  fBackslash := true;
  fNamespaceAsModule := false;
  fJavaScriptOperators := false;
  fZeroBasedStrings := false;
  fDeclareVariables := false;
  fInitArrays := true;
  fCompilerDirectives := TStringList.Create;
  fCompilerDirectives.Add('WIN32');
  fIncludeFileExt := '';
  fVBArrays := false;

  if AOwner = nil then Exit;

  for I:=0 to AOwner.ComponentCount - 1 do
  begin
    C := TComponent(AOwner.Components[I]);
    if C.InheritsFrom(TTitanScripter) then
    begin
      S := TTitanScripter(C);
      if S.FindLanguage(GetLanguageName) = nil then
        S.RegisterLanguage(Self);
    end;
  end;
end;

destructor TTitanLanguage.Destroy;
var
  I: Integer;
  C: TComponent;
  S: TTitanScripter;
begin
  if Owner <> nil then
  begin
    for I:=0 to Owner.ComponentCount - 1 do
    begin
      C := TComponent(Owner.Components[I]);
      if C.InheritsFrom(TTitanScripter) then
      begin
        S := TTitanScripter(C);
        if S.FindLanguage(GetLanguageName) <> nil then
          S.UnregisterLanguage(GetLanguageName);
      end;
    end;
  end;

  fScripters.Free;
  fCompilerDirectives.Free;

  inherited;
end;

procedure TTitanLanguage.RegisterScripter(S: TTitanScripter);
begin
  if fScripters.IndexOf(S) = -1 then
    fScripters.Add(S);
end;

procedure TTitanLanguage.UnRegisterScripter(S: TTitanScripter);
var
  I: Integer;
begin
  I := fScripters.IndexOf(S);
  if I <> -1 then
    fScripters.Delete(I);
end;

function TTitanLanguage.GetKeywords: TStringList;
var
  P: TTitanParser;
begin
  result := nil;
  if fScripters.Count > 0 then
  begin
    P := TTitanScripter(fScripters[0]).fScripter.ParserList.FindParser(LanguageName);
    if P <> nil then
      result := P.Keywords;
  end;
end;

function TTitanLanguage.GetCaseSensitive: Boolean;
var
  P: TTitanParser;
begin
  result := false;
  if fScripters.Count > 0 then
  begin
    P := TTitanScripter(fScripters[0]).fScripter.ParserList.FindParser(LanguageName);
    if P <> nil then
      result := not P.UpCase;
  end;
end;

procedure TTitanLanguage.SetFileExt(const Value: String);
var
  I: Integer;
  C: TComponent;
  S: TTitanScripter;
  P: TTitanParser;
begin
  if Owner = nil then
    Exit;
  for I:=0 to Owner.ComponentCount - 1 do
  begin
    C := TComponent(Owner.Components[I]);
    if C.InheritsFrom(TTitanScripter) then
    begin
      S := TTitanScripter(C);
      P := S.fScripter.ParserList.FindParser(GetLanguageName);
      if P <> nil then
        P.FileExt := '.' + Value;
    end;
  end;
end;

procedure TTitanLanguage.SetLanguageName(const Value: String);
var
  I: Integer;
  C: TComponent;
  S: TTitanScripter;
  P: TTitanParser;
begin
  if Owner = nil then
    Exit;
  for I:=0 to Owner.ComponentCount - 1 do
  begin
    C := TComponent(Owner.Components[I]);
    if C.InheritsFrom(TTitanScripter) then
    begin
      S := TTitanScripter(C);
      P := S.fScripter.ParserList.FindParser(GetLanguageName);
      if P <> nil then
        P.LanguageName := Value;
    end;
  end;
end;

procedure TTitanLanguage.SetCompilerDirectives(const Value: TStrings);
begin
  fCompilerDirectives.Assign(Value);
end;

function TTitanLanguage.GetLongStrLiterals: Boolean;
begin
  result := fLongStrLiterals;
end;

function TTitanLanguage.GeTTitanParserClass: TTitanParserClass;
begin
  result := nil;
end;

function TTitanLanguage.GetLanguageName: String;
begin
  result := '';
end;

function TTitanLanguage.GetFileExt: String;
begin
  result := '';
end;

//=========================================================

function LoadImportLibrary(const DllName: String): Cardinal;
var
  P: TTitanRegisterDllProc;
  R: TTitanRegisterProcs;
  OldCount, I: Integer;
begin
  R.RegisterNamespace := RegisterNamespace;
  R.RegisterClassType := RegisterClassType;
  R.RegisterRTTIType := RegisterRTTIType;
  R.RegisterMethod := RegisterMethod;
  R.RegisterBCBMethod := RegisterBCBMethod;
  R.RegisterProperty := RegisterProperty;
  R.RegisterRoutine := RegisterRoutine;
  R.RegisterVariable := RegisterVariable;
  R.RegisterConstant := RegisterConstant;
  R.RegisterInterfaceType := RegisterInterfaceType;
  R.RegisterInterfaceMethod := RegisterInterfaceMethod;

{$IFDEF UNIX}
   result := HMODULE(dynlibs.LoadLibrary(DLLName));
{$ELSE}
   result := dynlibs.LoadLibrary(PChar(DllName));
{$ENDIF}
  if result > 0 then
  begin
    OldCount := GlobalDefinitionList.Count;

    P := GetProcAddress(result, 'RegisterDllProcs');
    P(R);

    for I:=OldCount - 1 to GlobalDefinitionList.Count - 1 do
      GlobalDefinitionList.Records[I].Module := result;
  end;
end;

function FreeImportLibrary(H: Cardinal): LongBool;
begin
  result := FreeLibrary(H);
end;

//==============================================================

initialization
  _OP_CALL := BASE_Engine.OP_CALL;
  _OP_PRINT := BASE_Engine.OP_PRINT;
  _OP_GET_PUBLISHED_PROPERTY := BASE_Engine.OP_GET_PUBLISHED_PROPERTY;
  _OP_PUT_PUBLISHED_PROPERTY := BASE_Engine.OP_PUT_PUBLISHED_PROPERTY;
  _OP_PUT_PROPERTY := BASE_Engine.OP_PUT_PROPERTY;
  _OP_ASSIGN := BASE_Engine.OP_ASSIGN;
  _OP_NOP := BASE_Engine.OP_NOP;
end.
