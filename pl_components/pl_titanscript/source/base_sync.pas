{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit BASE_Sync;
interface

procedure _BeginRead;
procedure _EndRead;
procedure _BeginWrite;
procedure _EndWrite;

procedure Initialization_BASE_SYNC;
procedure Finalization_BASE_SYNC;

implementation


procedure _BeginRead;
begin
end;

procedure _EndRead;
begin
end;

procedure _BeginWrite;
begin
end;

procedure _EndWrite;
begin
end;

procedure Initialization_BASE_SYNC;
begin
end;

procedure Finalization_BASE_SYNC;
begin
end;


end.










