{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

unit titan_runtimeapidata;

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, LCLType, LMessages,
  Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Clipbrd, TypInfo, Variants,
  BASE_Engine,
  TitanScripterBaseEngine;

type
  TBaseScriptAPIData = class(TPersistent)
  protected
    FExtensionsIniFileName: string;
    FExtensionFileFilter: string;
    FExtensionFileExt: string;
    FExtensionRecWhatIs: string;
    FScriptSettingsFile: string;
    FScriptUnitFileExt: string;
    FScriptUnitRFileFilter: string;
    FScriptUnitWFileFilter: string;
    fListForClassesVars: TStringList;
    fListForClasses: TStringList;
    fListForInterfaces: TStringList;
    fListForNoObjProcedures: TStringList;
    fListForTypes: TStringList;
    fListForVars: TStringList;
    fListForConsts: TStringList;
    fListForArrays: TStringList;
    fListForRecords: TStringList;
    fListForTypeAliases: TStringList;
    fListForUnresolvedTypes: TStringList;
    FScriptEngine: TBaseScriptEngine;
    procedure Register_AllWorldClassesVars; virtual;
  public
    constructor CreateEX(aScriptEngine: TBaseScriptEngine); virtual;
    destructor Destroy; override;

    procedure Initialase;
    procedure Clear;

    function StrGetBracketsText(const astr: string): string;
    function IsClassRegister(aClassName: string): integer;

    //.......... Var Classes  ...................
    function FindWorldVarObjectType(const Atext: string; var aTypeString: string): boolean;
    //.......... Classes      ...................
    function FindClassPropertyTypeName(const aClassName, aProperty: string; var PropertyClassName: string; var IsReadOnly: boolean): boolean;
    function FindRegisterClassProcedure(const aClassName, aProcedureName: string): boolean;
    function FindClassFunctionTypeName(const aClassName, aProcedureName: string; var ItemTypeName: string): boolean;
    //.......... Records     ...................
    function FindRecordItemTypeName(const aRecordName, aItemName: string; var ItemTypeName: string): boolean;
    //.......... Global Procedures  ...................
    function FindRegisterFunctionResultStr(const aFunctionName: string): string;
    //.......... Arrays  ...................
    function FindArrayItemTypeName(const aArrayName: string; var ItemTypeName: string): boolean;
    //.......... Types    ...................
    function FindTypeItemTypeName(const aTypeName: string; var ItemTypeName: string): boolean;
    //.......... Types Aliases   ...................
    function FindTypeAliasesItemTypeName(const aTypeAliase: string; var ItemTypeName: string): boolean;
    //.......... Variables  ...................
    function FindVarItemTypeName(const aVarName: string; var ItemTypeName: string): boolean;
    //.......... Constants  ...................
    function FindConstItemTypeName(const aConstName: string; var ItemTypeName: string): boolean;

    property ExtensionsIniFileName: string read FExtensionsIniFileName;
    property ExtensionFileFilter: string read FExtensionFileFilter;
    property ExtensionFileExt: string read FExtensionFileExt;
    property ExtensionRecWhatIs: string read FExtensionRecWhatIs;

    property ScriptSettingsFile: string read FScriptSettingsFile;

    property ScriptUnitFileExt: string read FScriptUnitFileExt;
    property ScriptUnitRFileFilter: string read FScriptUnitRFileFilter;
    property ScriptUnitWFileFilter: string read FScriptUnitWFileFilter;

  published

    property ListForClassesVars: TStringList read fListForClassesVars;
    property ListForClasses: TStringList read fListForClasses;
    property ListForInterfaces: TStringList read fListForInterfaces;
    property ListForArrays: TStringList read fListForArrays;
    property ListForTypes: TStringList read fListForTypes;
    property ListForVars: TStringList read fListForVars;
    property ListForConsts: TStringList read fListForConsts;
    property ListForNoObjProcedures: TStringList read fListForNoObjProcedures;
    property ListForRecords: TStringList read fListForRecords;

    property ListForTypeAliases: TStringList read fListForTypeAliases;
    property ListForUnresolvedTypes: TStringList read fListForUnresolvedTypes;

    property ScriptEngine: TBaseScriptEngine read FScriptEngine;

  end;

var
  VarAPIData: TBaseScriptAPIData;

implementation

procedure TBaseScriptAPIData.Register_AllWorldClassesVars;
begin

end;

constructor TBaseScriptAPIData.CreateEX(aScriptEngine: TBaseScriptEngine);
begin
  inherited Create;

  fListForClassesVars := TStringList.Create;
  fListForClasses := TStringList.Create;
  fListForInterfaces := TStringList.Create;
  fListForNoObjProcedures := TStringList.Create;
  fListForVars := TStringList.Create;
  fListForConsts := TStringList.Create;
  fListForTypes := TStringList.Create;
  fListForArrays := TStringList.Create;
  fListForRecords := TStringList.Create;
  fListForTypeAliases := TStringList.Create;
  fListForUnresolvedTypes := TStringList.Create;

  FScriptEngine := aScriptEngine;

  FExtensionsIniFileName := '';
  FExtensionFileFilter := '';
  FExtensionFileExt := '';
  FExtensionRecWhatIs := '';
  FScriptSettingsFile := '';
  FScriptUnitFileExt := '';
  FScriptUnitRFileFilter := '';
  FScriptUnitWFileFilter := '';
end;

destructor TBaseScriptAPIData.Destroy;
begin

  fListForClassesVars.Free;
  fListForClasses.Free;
  fListForInterfaces.Free;
  fListForTypes.Free;
  fListForVars.Free;
  fListForConsts.Free;
  fListForArrays.Free;
  fListForNoObjProcedures.Free;
  fListForRecords.Free;
  fListForTypeAliases.Free;
  fListForUnresolvedTypes.Free;

  inherited Destroy;
end;

procedure TBaseScriptAPIData.Clear;
begin
  fListForClassesVars.Clear;
  fListForClasses.Clear;
  fListForInterfaces.Clear;
  fListForNoObjProcedures.Clear;
  fListForVars.Clear;
  fListForConsts.Clear;
  fListForTypes.Clear;
  fListForArrays.Clear;
  fListForRecords.Clear;
  fListForTypeAliases.Clear;
  fListForUnresolvedTypes.Clear;
end;

procedure TBaseScriptAPIData.Initialase;
var
  i: integer;
  PD: TTitanDefinition;
  ss: string;
begin
  Clear;
  if fListForClassesVars.Count > 0 then
    exit;

  Register_AllWorldClassesVars;

  //.......................................

  if GlobalDefinitionList = nil then
    exit;

  for i := 0 to GlobalDefinitionList.Count - 1 do
  begin
    pd := GlobalDefinitionList.Records[i];

    ss := UpperCase(pd.Name);
    if system.Pos('Titan', ss) > 0 then
      Continue;

    //.......... Class Types ...........
    if (pd is TTitanClassDefinition) then
    begin
      //.......... Classes ...........
      if (TTitanClassDefinition(pd).ClassKind = ckClass) then
      begin
        if TTitanClassDefinition(pd).PClass <> nil then
          fListForClasses.AddObject(pd.Name, TTitanClassDefinition(pd));
      end;
      //...........Interfaces ...............................
      if (TTitanClassDefinition(pd).ClassKind = ckInterface) then
      begin
        if pd.Owner = nil then
          fListForInterfaces.AddObject(pd.Name, TTitanClassDefinition(pd));
      end;
      //...........Arrays ...............................
      if (TTitanClassDefinition(pd).ClassKind = ckArray) or (TTitanClassDefinition(pd).ClassKind = ckDynamicArray) then
      begin
        if pd.Owner = nil then
          fListForArrays.AddObject(pd.Name, TTitanClassDefinition(pd));
      end;
      //...........Records ...............................
      if (TTitanClassDefinition(pd).ClassKind = ckStructure) then
      begin
        if pd.Owner = nil then
          fListForRecords.AddObject(pd.Name, TTitanClassDefinition(pd));
      end;
    end;
    //...........Variables ...............................
    if (pd is TTitanVariableDefinition) then
    begin
      if pd.Owner = nil then
        fListForVars.AddObject(pd.Name, TTitanVariableDefinition(pd));
    end;
    //...........Constants ...............................
    if (pd is TTitanConstantDefinition) then
    begin
      if pd.Owner = nil then
        if TTitanConstantDefinition(pd).ck <> ckEnum{ckNone } then
          fListForConsts.AddObject(pd.Name, TTitanConstantDefinition(pd));
    end;

    //...........RTTI Types ...............................
    if (pd is TTitanRTTITypeDefinition) then
    begin
      if pd.Owner = nil then
        if pd.Name[1] <> '.' then
          fListForTypes.AddObject(pd.Name, TTitanRTTITypeDefinition(pd));
    end;

    //...........Procedures ...............................
    if (pd is TTitanMethodDefinition) then
    begin
      if pd.Owner = nil then
        fListForNoObjProcedures.AddObject(pd.Name, TTitanMethodDefinition(pd));
    end;
  end;


  for I := 0 to GlobalTypeAliases.Count - 1 do
    fListForTypeAliases.Add(GlobalTypeAliases.IndexOfByIndex(I));

  for I := 0 to UnresolvedTypes.Count - 1 do
    fListForUnresolvedTypes.Add(UnresolvedTypes.IndexOfByIndex(I));

end;

//----------------------------------------------------------

function TBaseScriptAPIData.IsClassRegister(aClassName: string): integer;
begin
  Result := -1;
  if fListForClasses = nil then
    exit;
  Result := fListForClasses.IndexOf(aClassName);
end;

function TBaseScriptAPIData.FindClassFunctionTypeName(const aClassName, aProcedureName: string; var ItemTypeName: string): boolean;
var
  inx: integer;
  ClassInf: TTitanClassDefinition;
  MethodInf: TTitanMethodDefinition;
  ss, sclName: string;
  cl: TClass;
begin
  Result := False;
  ItemTypeName := '';
  if (aClassName = '') or (aProcedureName = '') then
    exit;

  inx := IsClassRegister(aClassName);
  if inx < 0 then
    exit;

  ClassInf := TTitanClassDefinition(fListForClasses.objects[inx]);
  if ClassInf = nil then
    exit;

  cl := ClassInf.PClass;
  sclName := aClassName;
  repeat

    MethodInf := GlobalDefinitionList.FindMethod(sclName, aProcedureName);
    cl := cl.ClassParent;
    if cl = nil then
      sclName := ''
    else
      sclName := cl.ClassName;

  until (MethodInf <> nil) or (sclName = '');

  if MethodInf = nil then
    exit;

  Result := True;
  ItemTypeName := MethodInf.ResultType;
end;


function TBaseScriptAPIData.FindClassPropertyTypeName(const aClassName, aProperty: string; var PropertyClassName: string;
  var IsReadOnly: boolean): boolean;
var
  inx: integer;
  ClassPublicInf: TTitanClassDefinition;
  aPropInfo: PPropInfo;
  aTypeData: PTypeData;
  aPropertyInfo: TTitanPropertyDefinition;
begin
  Result := False;
  IsReadOnly := False;

  PropertyClassName := '';
  if (aClassName = '') or (aProperty = '') then
    exit;

  inx := IsClassRegister(aClassName);
  if inx <= 0 then
    exit;
  ClassPublicInf := TTitanClassDefinition(fListForClasses.objects[inx]);// Get Registered Class Info
  if ClassPublicInf = nil then
    exit;

  //-------------------- Published Properties Typhon RTII functions ---------------

  if IsPublishedProp(ClassPublicInf.PClass, aProperty) then
  begin
    aPropInfo := GetPropInfo(ClassPublicInf.PClass, aProperty, tkAny);
    if aPropInfo <> nil then
      aTypeData := GetTypeData(aPropInfo^.PropType);
    if aPropInfo^.PropType^.Kind = tkClass then
    begin
      Result := True;
      PropertyClassName := aPropInfo^.PropType^.Name;
      IsReadOnly := (aPropInfo^.SetProc = nil);
    end;
  end;
  //-------------------- For Public Properties from Scripter Registration ---------------
  if Result = False then
  begin
    aPropertyInfo := GlobalDefinitionList.FindProperty(ClassPublicInf.pClass.ClassName, aProperty);
    if aPropertyInfo <> nil then
    begin
      Result := True;
      PropertyClassName := aPropertyInfo.PropType;
      IsReadOnly := (aPropertyInfo.WriteDef = nil);
    end;
  end;
end;


function TBaseScriptAPIData.FindRegisterClassProcedure(const aClassName, aProcedureName: string): boolean;
var
  inx: integer;
  ClassPublicInf: TTitanClassDefinition;
  aMethodInfo: TTitanMethodDefinition;
begin
  Result := False;

  if (aClassName = '') or (aProcedureName = '') then
    exit;

  inx := IsClassRegister(aClassName);
  if inx < 0 then
    exit;

  ClassPublicInf := TTitanClassDefinition(fListForClasses.objects[inx]);// Get Registered Class Info
  if ClassPublicInf = nil then
    exit;

  aMethodInfo := GlobalDefinitionList.FindMethod(ClassPublicInf.pClass.ClassName, aProcedureName);
  if aMethodInfo <> nil then
    Result := True;
end;

function TBaseScriptAPIData.FindWorldVarObjectType(const Atext: string; var aTypeString: string): boolean;
var
  n: integer;
begin
  Result := False;
  aTypeString := '';
  n := -1;
  Register_AllWorldClassesVars;

  n := fListForClassesVars.IndexOf(Atext);

  if n > -1 then
    if TObject(fListForClassesVars.Objects[n]) is TObject then
    begin
      aTypeString := TObject(fListForClassesVars.Objects[n]).ClassName;
      Result := True;
    end;
end;

//----------- For Records ------------------------------

function TBaseScriptAPIData.FindRecordItemTypeName(const aRecordName, aItemName: string; var ItemTypeName: string): boolean;
var
  dv: TTitanClassDefinition;
  df: TTitanRecordFieldDefinition;
  inx: integer;
  ss: string;
begin
  Result := False;
  ItemTypeName := '';
  if aRecordName = '' then
    exit;
  if aItemName = '' then
    exit;

  inx := ListForRecords.IndexOf(aRecordName);
  if inx < 0 then
    exit;

  dv := TTitanClassDefinition(fListForRecords.Objects[inx]);
  if dv = nil then
    exit;
  df := GlobalDefinitionList.FindRecordField(dv, aItemName);
  if df = nil then
    exit;
  Result := True;
  ItemTypeName := df.FieldType;
end;

//---------- For Arrays ----------------------------------

function TBaseScriptAPIData.FindArrayItemTypeName(const aArrayName: string; var ItemTypeName: string): boolean;
var
  dv: TTitanClassDefinition;
  inx: integer;
  ss: string;
begin
  Result := False;
  ItemTypeName := '';
  if aArrayName = '' then
    exit;

  inx := ListForArrays.IndexOf(aArrayName);
  if inx < 0 then
    exit;

  dv := TTitanClassDefinition(VarAPIData.ListForArrays.Objects[inx]);
  if dv = nil then
    exit;
  ItemTypeName := dv.ElTypeName;
end;

//------------- For Types -------------------------------

function TBaseScriptAPIData.FindTypeItemTypeName(const aTypeName: string; var ItemTypeName: string): boolean;
var
  int: integer;
  dv: TTitanRTTITypeDefinition;
  ss: string;
begin
  Result := False;
  ItemTypeName := '';
  if aTypeName = '' then
    exit;

  int := VarAPIData.ListForTypes.IndexOf(aTypeName);
  if int < 0 then
    exit;

  dv := TTitanRTTITypeDefinition(VarAPIData.ListForTypes.Objects[int]);
  if dv.pti <> nil then
  begin
    Result := True;
    ss := dv.Name + ' = ' + dv.pti^.Name;
    ItemTypeName := dv.pti^.Name + ' ===>';
  end
  else
  begin
    Result := True;
    ss := dv.Name + ' = ' + '---- ?????????? ----';
    ItemTypeName := dv.Name;
  end;
end;

//---------------- For Types Aliases ------------------------------

function TBaseScriptAPIData.FindTypeAliasesItemTypeName(const aTypeAliase: string; var ItemTypeName: string): boolean;
var
  int: integer;
  ss, s: string;
begin
  Result := False;
  ItemTypeName := '';
  if aTypeAliase = '' then
    exit;
  ss := aTypeAliase;

  repeat
    int := VarAPIData.ListForTypeAliases.IndexOf(ss);

    if int > -1 then
    begin
      Result := True;
      ss := GlobalTypeAliases.IndexOfByName(ss);
    end;

  until int < 0;

  if Result = True then
    ItemTypeName := ss;

end;


//---------------- For Variables ---------------------------

function TBaseScriptAPIData.FindVarItemTypeName(const aVarName: string; var ItemTypeName: string): boolean;
var
  int: integer;
  dv: TTitanVariableDefinition;
  ss: string;
begin
  Result := False;
  ItemTypeName := '';
  if aVarName = '' then
    exit;

  int := VarAPIData.ListForVars.IndexOf(aVarName);
  if int < 0 then
    exit;

  dv := TTitanVariableDefinition(VarAPIData.ListForVars.Objects[int]);
  ss := dv.FullName + ' : ' + dv.TypeName + ';';

  Result := True;
  ItemTypeName := dv.TypeName;
end;

//------------------ For Constants ---------------------------

function TBaseScriptAPIData.FindConstItemTypeName(const aConstName: string; var ItemTypeName: string): boolean;
var
  int: integer;
  ss: string;
  dc: TTitanConstantDefinition;
begin
  Result := False;
  ItemTypeName := '';
  if aConstName = '' then
    exit;

  int := VarAPIData.ListForConsts.IndexOf(aConstName);
  if int < 0 then
    exit;

  dc := TTitanConstantDefinition(VarAPIData.ListForConsts.Objects[int]);

  if VarType(dc.DefValue) in [varEmpty, varNull] then
    ss := 'Constant ' + dc.Name + ' = Undefined (' + dc.ResultType + ')';
  ss := 'Constant ' + dc.Name + ' = ' + VarToStrDef(dc.DefValue, '') + ' (' + dc.ResultType + ')';

  Result := True;
  ItemTypeName := dc.ResultType;

end;

//-------------- For Global Proc/Functions -----------------------

function TBaseScriptAPIData.FindRegisterFunctionResultStr(const aFunctionName: string): string;
var
  aMethodInfo: TTitanMethodDefinition;
  inx: integer;
  ss: string;
begin
  Result := '';
  inx := fListForNoObjProcedures.IndexOf(aFunctionName);
  if inx < 0 then
    exit;
  aMethodInfo := TTitanMethodDefinition(fListForNoObjProcedures.Objects[inx]);
  Result := aMethodInfo.ResultType;
end;

function TBaseScriptAPIData.strGetBracketsText(const astr: string): string;
var
  i1, i2: integer;
begin
  Result := '';
  i1 := Pos('(', astr);
  if i1 > 0 then
  begin
    i2 := Pos(')', astr);
    Result := System.Copy(astr, i1, i2 - i1 + 1);
  end;
end;


end.
