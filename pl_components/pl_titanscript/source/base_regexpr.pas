{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit BASE_RegExpr;

interface

{$BOOLEVAL OFF}
{$EXTENDEDSYNTAX ON}
{$LONGSTRINGS ON}
{$OPTIMIZATION ON}
{$WARN SYMBOL_PLATFORM OFF}

uses
  Classes,  // TStrings in Split method
  SysUtils; // Exception

type
  PRegExprChar = PWideChar;
  RegExprString = WideString;
  REChar = widechar;
  TREOp = REChar; // internal p-code type //###0.933
  PREOp = ^TREOp;
  TRENextOff = integer; // internal Next "pointer" (offset to current p-code) //###0.933
  PRENextOff = ^TRENextOff; // used for extracting Next "pointers" from compiled r.e. //###0.933
  TREBracesArg = integer; // type of {m,n} arguments
  PREBracesArg = ^TREBracesArg;

const
  REOpSz = SizeOf(TREOp) div SizeOf(REChar); // size of p-code in RegExprString units
  RENextOffSz = SizeOf(TRENextOff) div SizeOf(REChar); // size of Next 'pointer' -"-
  REBracesArgSz = SizeOf(TREBracesArg) div SizeOf(REChar); // size of BRACES arguments -"-

  EscChar = '\'; // 'Escape'-char ('\' in common r.e.) used for escaping metachars (\w, \d etc).
  RegExprModifierI: boolean = False;    // default value for ModifierI
  RegExprModifierR: boolean = True;     // default value for ModifierR
  RegExprModifierS: boolean = True;     // default value for ModifierS
  RegExprModifierG: boolean = True;     // default value for ModifierG
  RegExprModifierM: boolean = False;    // default value for ModifierM
  RegExprModifierX: boolean = False;    // default value for ModifierX
  RegExprSpaceChars: RegExprString = ' '#$9#$A#$D#$C; // default value for SpaceChars
  RegExprWordChars: RegExprString = '0123456789' + 'abcdefghijklmnopqrstuvwxyz' +
    'ABCDEFGHIJKLMNOPQRSTUVWXYZ_';    // default value for WordChars
  RegExprLineSeparators: RegExprString = #$d#$a#$b#$c#$2028#$2029#$85;
  RegExprLinePairedSeparator: RegExprString = #$d#$a;

  NSUBEXP = 15;
  NSUBEXPMAX = 255;
  MaxBracesArg = $7FFFFFFF - 1;
  LoopStackMax = 10;
  TinySetLen = 3;

type
  TRegExprInvertCaseFunction = function(const Ch: REChar): REChar of object;

  TRegExpr = class;

  TRegExprReplaceFunction = function(ARegExpr: TRegExpr): string
    of object;

  TRegExpr = class
  private
    startp: array [0 .. NSUBEXP - 1] of PRegExprChar; // founded expr starting points
    endp: array [0 .. NSUBEXP - 1] of PRegExprChar; // founded expr end points


    LoopStack: array [1 .. LoopStackMax] of integer; // state before entering loop
    LoopStackIdx: integer; // 0 - out of all loops

    // The "internal use only" fields to pass info from compile
    // to execute that permits the execute phase to run lots faster on
    // simple cases.
    regstart: REChar; // char that must begin a match; '\0' if none obvious
    reganch: REChar; // is the match anchored (at beginning-of-line only)?
    regmust: PRegExprChar; // string (pointer into program) that match must include, or nil
    regmlen: integer; // length of regmust string
    // Regstart and reganch permit very fast decisions on suitable starting points
    // for a match, cutting down the work a lot.  Regmust permits fast rejection
    // of lines that cannot possibly match.  The regmust tests are costly enough
    // that regcomp() supplies a regmust only if the r.e. contains something
    // potentially expensive (at present, the only such thing detected is * or +
    // at the start of the r.e., which can involve a lot of backup).  Regmlen is
    // supplied because the test in regexec() needs it and regcomp() is computing
    // it anyway.

    // work variables for Exec's routins - save stack in recursion}
    reginput: PRegExprChar; // String-input pointer.
    fInputStart: PRegExprChar; // Pointer to first char of input string.
    fInputEnd: PRegExprChar; // Pointer to char AFTER last char of input string

    // work variables for compiler's routines
    regparse: PRegExprChar;  // Input-scan pointer.
    regnpar: integer; // count.
    regdummy: char;
    regcode: PRegExprChar;   // Code-emit pointer; @regdummy = don't.
    regsize: integer; // Code size.

    regexpbeg: PRegExprChar; // only for error handling. Contains
    // pointer to beginning of r.e. while compiling
    fExprIsCompiled: boolean; // true if r.e. successfully compiled

    // programm is essentially a linear encoding
    // of a nondeterministic finite-state machine (aka syntax charts or
    // "railroad normal form" in parsing technology).  Each node is an opcode
    // plus a "next" pointer, possibly plus an operand.  "Next" pointers of
    // all nodes except BRANCH implement concatenation; a "next" pointer with
    // a BRANCH on both ends of it is connecting two alternatives.  (Here we
    // have one of the subtle syntax dependencies:  an individual BRANCH (as
    // opposed to a collection of them) is never concatenated with anything
    // because of operator precedence.)  The operand of some types of node is
    // a literal string; for others, it is a node leading into a sub-FSM.  In
    // particular, the operand of a BRANCH node is the first node of the branch.
    // (NB this is *not* a tree structure:  the tail of the branch connects
    // to the thing following the set of BRANCHes.)  The opcodes are:
    programm: PRegExprChar; // Unwarranted chumminess with compiler.

    fExpression: PRegExprChar; // source of compiled r.e.
    fInputString: PRegExprChar; // input string

    fLastError: integer; // see Error, LastError

    fModifiers: integer; // modifiers
    fCompModifiers: integer; // compiler's copy of modifiers
    fProgModifiers: integer; // modifiers values from last programm compilation

    fSpaceChars: RegExprString;
    fWordChars: RegExprString;
    fInvertCase: TRegExprInvertCaseFunction;

    fLineSeparators: RegExprString;
    fLinePairedSeparatorAssigned: boolean;
    fLinePairedSeparatorHead, fLinePairedSeparatorTail: REChar;

    procedure InvalidateProgramm;

    function IsProgrammOk: boolean;

    function GetExpression: RegExprString;
    procedure SetExpression(const s: RegExprString);

    function GetModifierStr: RegExprString;
    class function ParseModifiersStr(const AModifiers: RegExprString; var AModifiersInt: integer): boolean;

    procedure SetModifierStr(const AModifiers: RegExprString);

    function GetModifier(AIndex: integer): boolean;
    procedure SetModifier(AIndex: integer; ASet: boolean);

    procedure Error(AErrorID: integer); virtual;


    {==================== Compiler section ===================}
    function CompileRegExpr(exp: PRegExprChar): boolean;
    // compile a regular expression into internal code

    procedure Tail(p: PRegExprChar; val: PRegExprChar);
    // set the next-pointer at the end of a node chain

    procedure OpTail(p: PRegExprChar; val: PRegExprChar);
    // regoptail - regtail on operand of first argument; nop if operandless

    function EmitNode(op: TREOp): PRegExprChar;
    // regnode - emit a node, return location

    procedure EmitC(b: REChar);
    // emit (if appropriate) a byte of code

    procedure InsertOperator(op: TREOp; opnd: PRegExprChar; sz: integer);
    // insert an operator in front of already-emitted operand
    // Means relocating the operand.

    function ParseReg(paren: integer; var flagp: integer): PRegExprChar;
    // regular expression, i.e. main body or parenthesized thing

    function ParseBranch(var flagp: integer): PRegExprChar;
    // one alternative of an | operator

    function ParsePiece(var flagp: integer): PRegExprChar;
    // something followed by possible [*+?]

    function ParseAtom(var flagp: integer): PRegExprChar;
    // the lowest level

    function GetCompilerErrorPos: integer;
    // current pos in r.e. - for error hanling

    {===================== Mathing section ===================}
    function regrepeat(p: PRegExprChar; AMax: integer): integer;
    // repeatedly match something simple, report how many

    function regnext(p: PRegExprChar): PRegExprChar;
    // dig the "next" pointer out of a node

    function MatchPrim(prog: PRegExprChar): boolean;
    // recursively matching routine

    function ExecPrim(AOffset: integer): boolean;
    // Exec for stored InputString

    function DumpOp(op: REChar): RegExprString;

    function GetSubExprMatchCount: integer;
    function GetMatchPos(Idx: integer): integer;
    function GetMatchLen(Idx: integer): integer;
    function GetMatch(Idx: integer): RegExprString;

    function GetInputString: RegExprString;
    procedure SetInputString(const AInputString: RegExprString);

    function StrScanCI(s: PRegExprChar; ch: REChar): PRegExprChar;

    procedure SetLineSeparators(const AStr: RegExprString);
    procedure SetLinePairedSeparator(const AStr: RegExprString);
    function GetLinePairedSeparator: RegExprString;

  public
    constructor Create;
    destructor Destroy; override;

    class function VersionMajor: integer;
    class function VersionMinor: integer;

    property Expression: RegExprString read GetExpression write SetExpression;
    // Regular expression.
    // For optimization, TRegExpr will automatically compiles it into 'P-code'
    // (You can see it with help of Dump method) and stores in internal
    // structures. Real [re]compilation occures only when it really needed -
    // while calling Exec[Next], Substitute, Dump, etc
    // and only if Expression or other P-code affected properties was changed
    // after last [re]compilation.
    // If any errors while [re]compilation occures, Error method is called
    // (by default Error raises exception - see below)

    property ModifierStr: RegExprString read GetModifierStr write SetModifierStr;
    // Set/get default values of r.e.syntax modifiers. Modifiers in
    // r.e. (?ismx-ismx) will replace this default values.
    // If you try to set unsupported modifier, Error will be called
    // (by defaul Error raises exception ERegExpr).

    property ModifierI: boolean index 1 read GetModifier write SetModifier;
    // Modifier /i - caseinsensitive, initialized from RegExprModifierI

    property ModifierR: boolean index 2 read GetModifier write SetModifier;
    // Modifier /r - use r.e.syntax extended for russian,
    // (was property ExtSyntaxEnabled in previous versions)
    // If true, then ΰ-  additional include russian letter 'Έ',
    // ΐ-ί  additional include '¨', and ΰ-ί include all russian symbols.
    // You have to turn it off if it may interfere with you national alphabet.
    // , initialized from RegExprModifierR

    property ModifierS: boolean index 3 read GetModifier write SetModifier;
    // Modifier /s - '.' works as any char (else as [^\n]),
    // , initialized from RegExprModifierS

    property ModifierG: boolean index 4 read GetModifier write SetModifier;
    // Switching off modifier /g switchs all operators in
    // non-greedy style, so if ModifierG = False, then
    // all '*' works as '*?', all '+' as '+?' and so on.
    // , initialized from RegExprModifierG

    property ModifierM: boolean index 5 read GetModifier write SetModifier;
    // Treat string as multiple lines. That is, change `^' and `$' from
    // matching at only the very start or end of the string to the start
    // or end of any line anywhere within the string.
    // , initialized from RegExprModifierM

    property ModifierX: boolean index 6 read GetModifier write SetModifier;
    // Modifier /x - eXtended syntax, allow r.e. text formatting,
    // see description in the help. Initialized from RegExprModifierX

    function Exec(const AInputString: RegExprString): boolean; overload;
    function Exec(AOffset: integer): boolean; overload;

    // match a programm against a string AInputString
    // !!! Exec store AInputString into InputString property
    // For Delphi 5 and higher available overloaded versions - first without
    // parameter (uses already assigned to InputString property value)
    // and second that has integer parameter and is same as ExecPos

    function ExecNext: boolean;
    // find next match:
    //    ExecNext;
    // works same as
    //    if MatchLen [0] = 0 then ExecPos (MatchPos [0] + 1)
    //     else ExecPos (MatchPos [0] + MatchLen [0]);
    // but it's more simpler !
    // Raises exception if used without preceeding SUCCESSFUL call to
    // Exec* (Exec, ExecPos, ExecNext). So You always must use something like
    // if Exec (InputString) then repeat { proceed results} until not ExecNext;

    function ExecPos(AOffset: integer = 1): boolean;
    // find match for InputString starting from AOffset position
    // (AOffset=1 - first char of InputString)

    property InputString: RegExprString read GetInputString write SetInputString;
    // returns current input string (from last Exec call or last assign
    // to this property).
    // Any assignment to this property clear Match* properties !

    function Substitute(const ATemplate: RegExprString): RegExprString;
    // Returns ATemplate with '$&' or '$0' replaced by whole r.e.
    // occurence and '$n' replaced by occurence of subexpression #n.
    // Since v.0.929 '$' used instead of '\' (for future extensions
    // and for more Perl-compatibility) and accept more then one digit.
    // If you want place into template raw '$' or '\', use prefix '\'
    // Example: '1\$ is $2\\rub\\' -> '1$ is <Match[2]>\rub\'
    // If you want to place raw digit after '$n' you must delimit
    // n with curly braces '{}'.
    // Example: 'a$12bc' -> 'a<Match[12]>bc'
    // 'a${1}2bc' -> 'a<Match[1]>2bc'.

    procedure Split(AInputStr: RegExprString; APieces: TStrings);
    // Split AInputStr into APieces by r.e. occurencies
    // Internally calls Exec[Next]

    function Replace(AInputStr: RegExprString; const AReplaceStr: RegExprString; AUseSubstitution: boolean = False): RegExprString; overload;
    function Replace(AInputStr: RegExprString; AReplaceFunc: TRegExprReplaceFunction): RegExprString; overload;
    function ReplaceEx(AInputStr: RegExprString; AReplaceFunc: TRegExprReplaceFunction): RegExprString;
    // Returns AInputStr with r.e. occurencies replaced by AReplaceStr
    // If AUseSubstitution is true, then AReplaceStr will be used
    // as template for Substitution methods.
    // For example:
    //  Expression := '({-i}block|var)\s*\(\s*([^ ]*)\s*\)\s*';
    //  Replace ('BLOCK( test1)', 'def "$1" value "$2"', True);
    //   will return:  def 'BLOCK' value 'test1'
    //  Replace ('BLOCK( test1)', 'def "$1" value "$2"')
    //   will return:  def "$1" value "$2"
    // Internally calls Exec[Next]
    // Overloaded version and ReplaceEx operate with call-back function,
    // so You can implement really complex functionality.

    property SubExprMatchCount: integer read GetSubExprMatchCount;
    // Number of subexpressions has been found in last Exec* call.
    // If there are no subexpr. but whole expr was found (Exec* returned True),
    // then SubExprMatchCount=0, if no subexpressions nor whole
    // r.e. found (Exec* returned false) then SubExprMatchCount=-1.
    // Note, that some subexpr. may be not found and for such
    // subexpr. MathPos=MatchLen=-1 and Match=''.
    // For example: Expression := '(1)?2(3)?';
    //  Exec ('123'): SubExprMatchCount=2, Match[0]='123', [1]='1', [2]='3'
    //  Exec ('12'): SubExprMatchCount=1, Match[0]='12', [1]='1'
    //  Exec ('23'): SubExprMatchCount=2, Match[0]='23', [1]='', [2]='3'
    //  Exec ('2'): SubExprMatchCount=0, Match[0]='2'
    //  Exec ('7') - return False: SubExprMatchCount=-1

    property MatchPos[Idx: integer]: integer read GetMatchPos;
    // pos of entrance subexpr. #Idx into tested in last Exec*
    // string. First subexpr. have Idx=1, last - MatchCount,
    // whole r.e. have Idx=0.
    // Returns -1 if in r.e. no such subexpr. or this subexpr.
    // not found in input string.

    property MatchLen[Idx: integer]: integer read GetMatchLen;
    // len of entrance subexpr. #Idx r.e. into tested in last Exec*
    // string. First subexpr. have Idx=1, last - MatchCount,
    // whole r.e. have Idx=0.
    // Returns -1 if in r.e. no such subexpr. or this subexpr.
    // not found in input string.
    // Remember - MatchLen may be 0 (if r.e. match empty string) !

    property Match[Idx: integer]: RegExprString read GetMatch;
    // == copy (InputString, MatchPos [Idx], MatchLen [Idx])
    // Returns '' if in r.e. no such subexpr. or this subexpr.
    // not found in input string.

    function LastError: integer;
    // Returns ID of last error, 0 if no errors (unusable if
    // Error method raises exception) and clear internal status
    // into 0 (no errors).

    function ErrorMsg(AErrorID: integer): RegExprString; virtual;
    // Returns Error message for error with ID = AErrorID.

    property CompilerErrorPos: integer read GetCompilerErrorPos;
    // Returns pos in r.e. there compiler stopped.
    // Usefull for error diagnostics

    property SpaceChars: RegExprString read fSpaceChars write fSpaceChars;
    // Contains chars, treated as /s (initially filled with RegExprSpaceChars
    // global constant)

    property WordChars: RegExprString read fWordChars write fWordChars; //###0.929
    // Contains chars, treated as /w (initially filled with RegExprWordChars
    // global constant)

    property LineSeparators: RegExprString read fLineSeparators write SetLineSeparators; //###0.941
    // line separators (like \n in Unix)

    property LinePairedSeparator: RegExprString read GetLinePairedSeparator write SetLinePairedSeparator; //###0.941
    // paired line separator (like \r\n in DOS and Windows).
    // must contain exactly two chars or no chars at all

    class function InvertCaseFunction(const Ch: REChar): REChar;
    // Converts Ch into upper case if it in lower case or in lower
    // if it in upper (uses current system local setings)

    property InvertCase: TRegExprInvertCaseFunction read fInvertCase write fInvertCase; //##0.935
    // Set this property if you want to override case-insensitive functionality.
    // Create set it to RegExprInvertCaseFunction (InvertCaseFunction by default)

    procedure Compile; //###0.941
    // [Re]compile r.e. Usefull for example for GUI r.e. editors (to check
    // all properties validity).

    function Dump: RegExprString;
    // dump a compiled regexp in vaguely comprehensible form

  end;

  ERegExpr = class(Exception)
  public
    ErrorCode: integer;
    CompilerErrorPos: integer;
  end;

const
  RegExprInvertCaseFunction: TRegExprInvertCaseFunction = nil;

function ExecRegExpr(const ARegExpr, AInputStr: RegExprString): boolean;


procedure SplitRegExpr(const ARegExpr, AInputStr: RegExprString; APieces: TStrings);


function ReplaceRegExpr(const ARegExpr, AInputStr, AReplaceStr: RegExprString; AUseSubstitution: boolean = False): RegExprString;


function QuoteRegExprMetaChars(const AStr: RegExprString): RegExprString;


function RegExprSubExpressions(const ARegExpr: string; ASubExprs: TStrings; AExtendedSyntax: boolean = False): integer;


implementation

const
  TRegExprVersionMajor: integer = 0;
  TRegExprVersionMinor: integer = 952;

  MaskModI = 1;
  MaskModR = 2;
  MaskModS = 4;
  MaskModG = 8;
  MaskModM = 16;
  MaskModX = 32;
  HASWIDTH = 01;
  SIMPLE = 02;
  SPSTART = 04;
  WORST = 0;    
  XIgnoredChars = ' '#9#$d#$a;
  META: array [0 .. 12] of REChar = (
    '^', '$', '.', '[', '(', ')', '|', '?', '+', '*', EscChar, '{', #0);


  RusRangeLo: array [0 .. 33] of REChar =
    (#$430, #$431, #$432, #$433, #$434, #$435, #$451, #$436, #$437,
    #$438, #$439, #$43A, #$43B, #$43C, #$43D, #$43E, #$43F,
    #$440, #$441, #$442, #$443, #$444, #$445, #$446, #$447,
    #$448, #$449, #$44A, #$44B, #$44C, #$44D, #$44E, #$44F, #0);
  RusRangeHi: array [0 .. 33] of REChar =
    (#$410, #$411, #$412, #$413, #$414, #$415, #$401, #$416, #$417,
    #$418, #$419, #$41A, #$41B, #$41C, #$41D, #$41E, #$41F,
    #$420, #$421, #$422, #$423, #$424, #$425, #$426, #$427,
    #$428, #$429, #$42A, #$42B, #$42C, #$42D, #$42E, #$42F, #0);
  RusRangeLoLow = #$430;
  RusRangeLoHigh = #$44F;
  RusRangeHiLow = #$410;
  RusRangeHiHigh = #$42F;
  
const
  reeOk = 0;
  reeCompNullArgument = 100;
  reeCompRegexpTooBig = 101;
  reeCompParseRegTooManyBrackets = 102;
  reeCompParseRegUnmatchedBrackets = 103;
  reeCompParseRegUnmatchedBrackets2 = 104;
  reeCompParseRegJunkOnEnd = 105;
  reePlusStarOperandCouldBeEmpty = 106;
  reeNestedSQP = 107;
  reeBadHexDigit = 108;
  reeInvalidRange = 109;
  reeParseAtomTrailingBackSlash = 110;
  reeNoHexCodeAfterBSlashX = 111;
  reeHexCodeAfterBSlashXTooBig = 112;
  reeUnmatchedSqBrackets = 113;
  reeInternalUrp = 114;
  reeQPSBFollowsNothing = 115;
  reeTrailingBackSlash = 116;
  reeRarseAtomInternalDisaster = 119;
  reeBRACESArgTooBig = 122;
  reeBracesMinParamGreaterMax = 124;
  reeUnclosedComment = 125;
  reeComplexBracesNotImplemented = 126;
  reeUrecognizedModifier = 127;
  reeBadLinePairedSeparator = 128;
  reeRegRepeatCalledInappropriately = 1000;
  reeMatchPrimMemoryCorruption = 1001;
  reeMatchPrimCorruptedPointers = 1002;
  reeNoExpression = 1003;
  reeCorruptedProgram = 1004;
  reeNoInpitStringSpecified = 1005;
  reeOffsetMustBeGreaterThen0 = 1006;
  reeExecNextWithoutExec = 1007;
  reeGetInputStringWithoutInputString = 1008;
  reeDumpCorruptedOpcode = 1011;
  reeModifierUnsupported = 1013;
  reeLoopStackExceeded = 1014;
  reeLoopWithoutEntry = 1015;
  reeBadPCodeImported = 2000;


const
  MAGIC = TREOp(216);// programm signature

  // name            opcode    opnd? meaning
  EEND = TREOp(0);  // -    End of program
  BOL = TREOp(1);  // -    Match "" at beginning of line
  EOL = TREOp(2);  // -    Match "" at end of line
  ANY = TREOp(3);  // -    Match any one character
  ANYOF = TREOp(4);  // Str  Match any character in string Str
  ANYBUT = TREOp(5);  // Str  Match any char. not in string Str
  BRANCH = TREOp(6);  // Node Match this alternative, or the next
  BACK = TREOp(7);  // -    Jump backward (Next < 0)
  EXACTLY = TREOp(8);  // Str  Match string Str
  NOTHING = TREOp(9);  // -    Match empty string
  STAR = TREOp(10); // Node Match this (simple) thing 0 or more times
  PLUS = TREOp(11); // Node Match this (simple) thing 1 or more times
  ANYDIGIT = TREOp(12); // -    Match any digit (equiv [0-9])
  NOTDIGIT = TREOp(13); // -    Match not digit (equiv [0-9])
  ANYLETTER = TREOp(14); // -    Match any letter from property WordChars
  NOTLETTER = TREOp(15); // -    Match not letter from property WordChars
  ANYSPACE = TREOp(16); // -    Match any space char (see property SpaceChars)
  NOTSPACE = TREOp(17); // -    Match not space char (see property SpaceChars)
  BRACES = TREOp(18); // Node,Min,Max Match this (simple) thing from Min to Max times.
  //      Min and Max are TREBracesArg
  COMMENT = TREOp(19); // -    Comment ;)
  EXACTLYCI = TREOp(20); // Str  Match string Str case insensitive
  ANYOFCI = TREOp(21); // Str  Match any character in string Str, case insensitive
  ANYBUTCI = TREOp(22); // Str  Match any char. not in string Str, case insensitive
  LOOPENTRY = TREOp(23); // Node Start of loop (Node - LOOP for this loop)
  LOOP = TREOp(24); // Node,Min,Max,LoopEntryJmp - back jump for LOOPENTRY.
  //      Min and Max are TREBracesArg
  //      Node - next node in sequence,
  //      LoopEntryJmp - associated LOOPENTRY node addr
  ANYOFTINYSET = TREOp(25); // Chrs Match any one char from Chrs (exactly TinySetLen chars)
  ANYBUTTINYSET = TREOp(26); // Chrs Match any one char not in Chrs (exactly TinySetLen chars)
  ANYOFFULLSET = TREOp(27); // Set  Match any one char from set of char
  // - very fast (one CPU instruction !) but takes 32 bytes of p-code
  BSUBEXP = TREOp(28); // Idx  Match previously matched subexpression #Idx (stored as REChar) //###0.936
  BSUBEXPCI = TREOp(29); // Idx  -"- in case-insensitive mode

  // Non-Greedy Style Ops //###0.940
  STARNG = TREOp(30); // Same as START but in non-greedy mode
  PLUSNG = TREOp(31); // Same as PLUS but in non-greedy mode
  BRACESNG = TREOp(32); // Same as BRACES but in non-greedy mode
  LOOPNG = TREOp(33); // Same as LOOP but in non-greedy mode

  // Multiline mode \m
  BOLML = TREOp(34);  // -    Match "" at beginning of line
  EOLML = TREOp(35);  // -    Match "" at end of line
  ANYML = TREOp(36);  // -    Match any one character

  // Word boundary
  BOUND = TREOp(37);  // Match "" between words //###0.943
  NOTBOUND = TREOp(38);  // Match "" not between words //###0.943

  Open = TREOp(39); // -    Mark this point in input as start of \n
  //      OPEN + 1 is \1, etc.
  Close = TREOp(Ord(Open) + NSUBEXP);



//=================== WideString functions ====================


function StrPCopy(Dest: PRegExprChar; const Source: RegExprString): PRegExprChar;
var
  i, Len: integer;
begin
  Len := length(Source);
  for i := 1 to Len do
    Dest[i - 1] := Source[i];
  Dest[Len] := #0;
  Result := Dest;
end;

function StrLCopy(Dest, Source: PRegExprChar; MaxLen: cardinal): PRegExprChar;
var
  i: integer;
begin
  for i := 0 to MaxLen - 1 do
    Dest[i] := Source[i];
  Result := Dest;
end;

function StrLen(Str: PRegExprChar): cardinal;
begin
  Result := 0;
  while Str[Result] <> #0 do
    Inc(Result);
end;

function StrPos(Str1, Str2: PRegExprChar): PRegExprChar;
var
  n: integer;
begin
  Result := nil;
  n := Pos(RegExprString(Str2), RegExprString(Str1));
  if n = 0 then
    EXIT;
  Result := Str1 + n - 1;
end;

function StrLComp(Str1, Str2: PRegExprChar; MaxLen: cardinal): integer;
var
  S1, S2: RegExprString;
begin
  S1 := Str1;
  S2 := Str2;
  if Copy(S1, 1, MaxLen) > Copy(S2, 1, MaxLen) then
    Result := 1
  else
  if Copy(S1, 1, MaxLen) < Copy(S2, 1, MaxLen) then
    Result := -1
  else
    Result := 0;
end;

function StrScan(Str: PRegExprChar; Chr: widechar): PRegExprChar;
begin
  Result := nil;
  while (Str^ <> #0) and (Str^ <> Chr) do
    Inc(Str);
  if (Str^ <> #0) then
    Result := Str;
end;


//===================== Global functions =====================


function ExecRegExpr(const ARegExpr, AInputStr: RegExprString): boolean;
var
  r: TRegExpr;
begin
  r := TRegExpr.Create;
  try
    r.Expression := ARegExpr;
    Result := r.Exec(AInputStr);
  finally
    r.Free;
  end;
end;

procedure SplitRegExpr(const ARegExpr, AInputStr: RegExprString; APieces: TStrings);
var
  r: TRegExpr;
begin
  APieces.Clear;
  r := TRegExpr.Create;
  try
    r.Expression := ARegExpr;
    r.Split(AInputStr, APieces);
  finally
    r.Free;
  end;
end;

function ReplaceRegExpr(const ARegExpr, AInputStr, AReplaceStr: RegExprString; AUseSubstitution: boolean = False): RegExprString;
begin
  with TRegExpr.Create do
    try
      Expression := ARegExpr;
      Result := Replace(AInputStr, AReplaceStr, AUseSubstitution);
    finally
      Free;
    end;
end;

function QuoteRegExprMetaChars(const AStr: RegExprString): RegExprString;
const
  RegExprMetaSet: RegExprString = '^$.[()|?+*' + EscChar + '{' + ']}';
var
  i, i0, Len: integer;
begin
  Result := '';
  Len := length(AStr);
  i := 1;
  i0 := i;
  while i <= Len do
  begin
    if Pos(AStr[i], RegExprMetaSet) > 0 then
    begin
      Result := Result + System.Copy(AStr, i0, i - i0) + EscChar + AStr[i];
      i0 := i + 1;
    end;
    Inc(i);
  end;
  Result := Result + System.Copy(AStr, i0, MaxInt);
end;

function RegExprSubExpressions(const ARegExpr: string; ASubExprs: TStrings; AExtendedSyntax: boolean = False): integer;
type
  TStackItemRec = record
    SubExprIdx: integer;
    StartPos: integer;
  end;
  TStackArray = packed array [0 .. NSUBEXPMAX - 1] of TStackItemRec;
var
  Len, SubExprLen: integer;
  i, i0: integer;
  Modif: integer;
  Stack: ^TStackArray;
  StackIdx, StackSz: integer;
begin
  Result := 0;

  ASubExprs.Clear;


  Len := length(ARegExpr);

  StackSz := 1;
  for i := 1 to Len do
    if ARegExpr[i] = '(' then
      Inc(StackSz);

  GetMem(Stack, SizeOf(TStackItemRec) * StackSz);
  try

    StackIdx := 0;
    i := 1;
    while (i <= Len) do
    begin
      case ARegExpr[i] of
        '(':
        begin
          if (i < Len) and (ARegExpr[i + 1] = '?') then
          begin

            Inc(i, 2);
            i0 := i;
            while (i <= Len) and (ARegExpr[i] <> ')') do
              Inc(i);
            if i > Len then
              Result := -1
            else
            if TRegExpr.ParseModifiersStr(System.Copy(ARegExpr, i, i - i0), Modif) then
              AExtendedSyntax := (Modif and MaskModX) <> 0;
          end
          else
          begin
            ASubExprs.Add('');

            Stack^[StackIdx].SubExprIdx := ASubExprs.Count - 1;
            Stack^[StackIdx].StartPos := i;

            Inc(StackIdx);
          end;
        end;
        ')':
        begin
          if StackIdx = 0 then
            Result := i
          else
          begin
            Dec(StackIdx);

            SubExprLen := i - Stack^[StackIdx].StartPos + 1;
            ASubExprs.Objects[Stack^[StackIdx].SubExprIdx] :=
              TObject(PtrInt(Stack^[StackIdx].StartPos or (SubExprLen shl 16)));
            ASubExprs[Stack^[StackIdx].SubExprIdx] := System.Copy(ARegExpr, Stack^[StackIdx].StartPos + 1, SubExprLen - 2);

          end;
        end;
        EscChar: Inc(i);
        '[':
        begin

          i0 := i;
          Inc(i);
          if ARegExpr[i] = ']' then
            Inc(i);
          while (i <= Len) and (ARegExpr[i] <> ']') do
            if ARegExpr[i] = EscChar then
              Inc(i, 2)
            else
              Inc(i);
          if (i > Len) or (ARegExpr[i] <> ']') then
            Result := -(i0 + 1);
        end;
        '#': if AExtendedSyntax then
          begin

            while (i <= Len) and (ARegExpr[i] <> #$d) and (ARegExpr[i] <> #$a) do
              Inc(i);
            while (i + 1 <= Len) and ((ARegExpr[i + 1] = #$d) or (ARegExpr[i + 1] = #$a)) do
              Inc(i);
          end;

      end;
      Inc(i);

    end;

    if StackIdx <> 0 then
      Result := -1;

    if (ASubExprs.Count = 0) or ((PtrInt(ASubExprs.Objects[0]) and $FFFF) <> 1) or
      (((PtrInt(ASubExprs.Objects[0]) shr 16) and $FFFF) <> Len) then
      ASubExprs.InsertObject(0, ARegExpr, TObject(PtrInt(Len shl 16) or 1));

  finally
    FreeMem(Stack);
  end;
end;


//================== Error handling section ===================

function TRegExpr.ErrorMsg(AErrorID: integer): RegExprString;
begin
  case AErrorID of
    reeOk: Result := 'No errors';
    reeCompNullArgument: Result := 'TRegExpr(comp): Null Argument';
    reeCompRegexpTooBig: Result := 'TRegExpr(comp): Regexp Too Big';
    reeCompParseRegTooManyBrackets: Result := 'TRegExpr(comp): ParseReg Too Many ()';
    reeCompParseRegUnmatchedBrackets: Result := 'TRegExpr(comp): ParseReg Unmatched ()';
    reeCompParseRegUnmatchedBrackets2: Result := 'TRegExpr(comp): ParseReg Unmatched ()';
    reeCompParseRegJunkOnEnd: Result := 'TRegExpr(comp): ParseReg Junk On End';
    reePlusStarOperandCouldBeEmpty: Result := 'TRegExpr(comp): *+ Operand Could Be Empty';
    reeNestedSQP: Result := 'TRegExpr(comp): Nested *?+';
    reeBadHexDigit: Result := 'TRegExpr(comp): Bad Hex Digit';
    reeInvalidRange: Result := 'TRegExpr(comp): Invalid [] Range';
    reeParseAtomTrailingBackSlash: Result := 'TRegExpr(comp): Parse Atom Trailing \';
    reeNoHexCodeAfterBSlashX: Result := 'TRegExpr(comp): No Hex Code After \x';
    reeHexCodeAfterBSlashXTooBig: Result := 'TRegExpr(comp): Hex Code After \x Is Too Big';
    reeUnmatchedSqBrackets: Result := 'TRegExpr(comp): Unmatched []';
    reeInternalUrp: Result := 'TRegExpr(comp): Internal Urp';
    reeQPSBFollowsNothing: Result := 'TRegExpr(comp): ?+*{ Follows Nothing';
    reeTrailingBackSlash: Result := 'TRegExpr(comp): Trailing \';
    reeRarseAtomInternalDisaster: Result := 'TRegExpr(comp): RarseAtom Internal Disaster';
    reeBRACESArgTooBig: Result := 'TRegExpr(comp): BRACES Argument Too Big';
    reeBracesMinParamGreaterMax: Result := 'TRegExpr(comp): BRACE Min Param Greater then Max';
    reeUnclosedComment: Result := 'TRegExpr(comp): Unclosed (?#Comment)';
    reeComplexBracesNotImplemented: Result :=
        'TRegExpr(comp): If you want take part in beta-testing BRACES ''{min,max}'' and non-greedy ops ''*?'', ''+?'', ''??'' for complex cases - remove ''.''  ';
    reeUrecognizedModifier: Result := 'TRegExpr(comp): Urecognized Modifier';
    reeBadLinePairedSeparator: Result := 'TRegExpr(comp): LinePairedSeparator must countain two different chars or no chars at all';
    reeRegRepeatCalledInappropriately: Result := 'TRegExpr(exec): RegRepeat Called Inappropriately';
    reeMatchPrimMemoryCorruption: Result := 'TRegExpr(exec): MatchPrim Memory Corruption';
    reeMatchPrimCorruptedPointers: Result := 'TRegExpr(exec): MatchPrim Corrupted Pointers';
    reeNoExpression: Result := 'TRegExpr(exec): Not Assigned Expression Property';
    reeCorruptedProgram: Result := 'TRegExpr(exec): Corrupted Program';
    reeNoInpitStringSpecified: Result := 'TRegExpr(exec): No Input String Specified';
    reeOffsetMustBeGreaterThen0: Result := 'TRegExpr(exec): Offset Must Be Greater Then 0';
    reeExecNextWithoutExec: Result := 'TRegExpr(exec): ExecNext Without Exec[Pos]';
    reeGetInputStringWithoutInputString: Result := 'TRegExpr(exec): GetInputString Without InputString';
    reeDumpCorruptedOpcode: Result := 'TRegExpr(dump): Corrupted Opcode';
    reeLoopStackExceeded: Result := 'TRegExpr(exec): Loop Stack Exceeded';
    reeLoopWithoutEntry: Result := 'TRegExpr(exec): Loop Without LoopEntry !';

    reeBadPCodeImported: Result := 'TRegExpr(misc): Bad p-code imported';
    else
      Result := 'Unknown error';
  end;
end;

function TRegExpr.LastError: integer;
begin
  Result := fLastError;
  fLastError := reeOk;
end;

//===================== Common section =======================

class function TRegExpr.VersionMajor: integer;
begin
  Result := TRegExprVersionMajor;
end;

class function TRegExpr.VersionMinor: integer;
begin
  Result := TRegExprVersionMinor;
end;

constructor TRegExpr.Create;
begin
  inherited;
  programm := nil;
  fExpression := nil;
  fInputString := nil;

  regexpbeg := nil;
  fExprIsCompiled := False;

  ModifierI := RegExprModifierI;
  ModifierR := RegExprModifierR;
  ModifierS := RegExprModifierS;
  ModifierG := RegExprModifierG;
  ModifierM := RegExprModifierM;

  SpaceChars := RegExprSpaceChars;
  WordChars := RegExprWordChars;
  fInvertCase := RegExprInvertCaseFunction;

  fLineSeparators := RegExprLineSeparators;
  LinePairedSeparator := RegExprLinePairedSeparator;
end;

destructor TRegExpr.Destroy;
begin
  if programm <> nil then
    FreeMem(programm);
  if fExpression <> nil then
    FreeMem(fExpression);
  if fInputString <> nil then
    FreeMem(fInputString);
end;

class function TRegExpr.InvertCaseFunction(const Ch: REChar): REChar;
begin

  if Ch >= #128 then
    Result := Ch
  else

  begin
    Result := AnsiUpperCase(Ch) [1];
    if Result = Ch then
      Result := AnsiLowerCase(Ch) [1];
  end;
end;

function TRegExpr.GetExpression: RegExprString;
begin
  if fExpression <> nil then
    Result := fExpression
  else
    Result := '';
end;

procedure TRegExpr.SetExpression(const s: RegExprString);
var
  Len: integer;
begin
  if (s <> fExpression) or not fExprIsCompiled then
  begin
    fExprIsCompiled := False;
    if fExpression <> nil then
    begin
      FreeMem(fExpression);
      fExpression := nil;
    end;
    if s <> '' then
    begin
      Len := length(s);
      GetMem(fExpression, (Len + 1) * SizeOf(REChar));

      StrPCopy(fExpression, Copy(s, 1, Len));

      InvalidateProgramm;
    end;
  end;
end;

function TRegExpr.GetSubExprMatchCount: integer;
begin
  if Assigned(fInputString) then
  begin
    Result := NSUBEXP - 1;
    while (Result > 0) and ((startp[Result] = nil) or (endp[Result] = nil)) do
      Dec(Result);
  end
  else
    Result := -1;
end;

function TRegExpr.GetMatchPos(Idx: integer): integer;
begin
  if (Idx >= 0) and (Idx < NSUBEXP) and Assigned(fInputString) and Assigned(startp[Idx]) and Assigned(endp[Idx]) then
  begin
    Result := (startp[Idx] - fInputString) + 1;
  end
  else
    Result := -1;
end;

function TRegExpr.GetMatchLen(Idx: integer): integer;
begin
  if (Idx >= 0) and (Idx < NSUBEXP) and Assigned(fInputString) and Assigned(startp[Idx]) and Assigned(endp[Idx]) then
  begin
    Result := endp[Idx] - startp[Idx];
  end
  else
    Result := -1;
end;

function TRegExpr.GetMatch(Idx: integer): RegExprString;
begin
  if (Idx >= 0) and (Idx < NSUBEXP) and Assigned(fInputString) and Assigned(startp[Idx]) and Assigned(endp[Idx]) then
    SetString(Result, startp[idx], endp[idx] - startp[idx])
  else
    Result := '';
end;

function TRegExpr.GetModifierStr: RegExprString;
begin
  Result := '-';

  if ModifierI then
    Result := 'i' + Result
  else
    Result := Result + 'i';
  if ModifierR then
    Result := 'r' + Result
  else
    Result := Result + 'r';
  if ModifierS then
    Result := 's' + Result
  else
    Result := Result + 's';
  if ModifierG then
    Result := 'g' + Result
  else
    Result := Result + 'g';
  if ModifierM then
    Result := 'm' + Result
  else
    Result := Result + 'm';
  if ModifierX then
    Result := 'x' + Result
  else
    Result := Result + 'x';

  if Result[length(Result)] = '-' then
    System.Delete(Result, length(Result), 1);
end;

class function TRegExpr.ParseModifiersStr(const AModifiers: RegExprString; var AModifiersInt: integer): boolean;

var
  i: integer;
  IsOn: boolean;
  Mask: integer;
begin
  Result := True;
  IsOn := True;
  Mask := 0;
  for i := 1 to length(AModifiers) do
    if AModifiers[i] = '-' then
      IsOn := False
    else
    begin
      if Pos(AModifiers[i], 'iI') > 0 then
        Mask := MaskModI
      else if Pos(AModifiers[i], 'rR') > 0 then
        Mask := MaskModR
      else if Pos(AModifiers[i], 'sS') > 0 then
        Mask := MaskModS
      else if Pos(AModifiers[i], 'gG') > 0 then
        Mask := MaskModG
      else if Pos(AModifiers[i], 'mM') > 0 then
        Mask := MaskModM
      else if Pos(AModifiers[i], 'xX') > 0 then
        Mask := MaskModX
      else
      begin
        Result := False;
        EXIT;
      end;
      if IsOn then
        AModifiersInt := AModifiersInt or Mask
      else
        AModifiersInt := AModifiersInt and not Mask;
    end;
end;

procedure TRegExpr.SetModifierStr(const AModifiers: RegExprString);
begin
  if not ParseModifiersStr(AModifiers, fModifiers) then
    Error(reeModifierUnsupported);
end;

function TRegExpr.GetModifier(AIndex: integer): boolean;
var
  Mask: integer;
begin
  Result := False;
  case AIndex of
    1: Mask := MaskModI;
    2: Mask := MaskModR;
    3: Mask := MaskModS;
    4: Mask := MaskModG;
    5: Mask := MaskModM;
    6: Mask := MaskModX;
    else
    begin
      Error(reeModifierUnsupported);
      EXIT;
    end;
  end;
  Result := (fModifiers and Mask) <> 0;
end;

procedure TRegExpr.SetModifier(AIndex: integer; ASet: boolean);
var
  Mask: integer;
begin
  case AIndex of
    1: Mask := MaskModI;
    2: Mask := MaskModR;
    3: Mask := MaskModS;
    4: Mask := MaskModG;
    5: Mask := MaskModM;
    6: Mask := MaskModX;
    else
    begin
      Error(reeModifierUnsupported);
      EXIT;
    end;
  end;
  if ASet then
    fModifiers := fModifiers or Mask
  else
    fModifiers := fModifiers and not Mask;
end;

//==================== Compiler section ======================

procedure TRegExpr.InvalidateProgramm;
begin
  if programm <> nil then
  begin
    FreeMem(programm);
    programm := nil;
  end;
end;

procedure TRegExpr.Compile;
begin
  if fExpression = nil then
  begin
    Error(reeNoExpression);
    EXIT;
  end;
  CompileRegExpr(fExpression);
end;

function TRegExpr.IsProgrammOk: boolean;

begin
  Result := False;

  if fModifiers <> fProgModifiers then
    InvalidateProgramm;

  if programm = nil then
    Compile;

  if programm = nil then
    EXIT
  else
  if programm[0] <> MAGIC then
    Error(reeCorruptedProgram)
  else
    Result := True;
end;

procedure TRegExpr.Tail(p: PRegExprChar; val: PRegExprChar);

var
  scan: PRegExprChar;
  temp: PRegExprChar;

begin
  if p = @regdummy then
    EXIT;


  scan := p;
  repeat
    temp := regnext(scan);
    if temp = nil then
      BREAK;
    scan := temp;
  until False;

  if val < scan then
    PRENextOff(scan + REOpSz)^ := -(scan - val)

  else
    PRENextOff(scan + REOpSz)^ := val - scan;

end;

procedure TRegExpr.OpTail(p: PRegExprChar; val: PRegExprChar);
begin

  if (p = nil) or (p = @regdummy) or (PREOp(p)^ <> BRANCH) then
    EXIT;
  Tail(p + REOpSz + RENextOffSz, val);
end;

function TRegExpr.EmitNode(op: TREOp): PRegExprChar;
begin
  Result := regcode;
  if Result <> @regdummy then
  begin
    PREOp(regcode)^ := op;
    Inc(regcode, REOpSz);
    PRENextOff(regcode)^ := 0;
    Inc(regcode, RENextOffSz);
  end
  else
    Inc(regsize, REOpSz + RENextOffSz);
end;

procedure TRegExpr.EmitC(b: REChar);
begin
  if regcode <> @regdummy then
  begin
    regcode^ := b;
    Inc(regcode);
  end
  else
    Inc(regsize);
end;

procedure TRegExpr.InsertOperator(op: TREOp; opnd: PRegExprChar; sz: integer);
var
  src, dst, place: PRegExprChar;
  i: integer;
begin
  if regcode = @regdummy then
  begin
    Inc(regsize, sz);
    EXIT;
  end;
  src := regcode;
  Inc(regcode, sz);
  dst := regcode;
  while src > opnd do
  begin
    Dec(dst);
    Dec(src);
    dst^ := src^;
  end;
  place := opnd;
  PREOp(place)^ := op;
  Inc(place, REOpSz);
  for i := 1 + REOpSz to sz do
  begin
    place^ := #0;
    Inc(place);
  end;
end;

function strcspn(s1: PRegExprChar; s2: PRegExprChar): integer;
var
  scan1, scan2: PRegExprChar;
begin
  Result := 0;
  scan1 := s1;
  while scan1^ <> #0 do
  begin
    scan2 := s2;
    while scan2^ <> #0 do
      if scan1^ = scan2^ then
        EXIT
      else
        Inc(scan2);
    Inc(Result);
    Inc(scan1);
  end;
end;

function TRegExpr.CompileRegExpr(exp: PRegExprChar): boolean;
  // compile a regular expression into internal code
  // We can't allocate space until we know how big the compiled form will be,
  // but we can't compile it (and thus know how big it is) until we've got a
  // place to put the code.  So we cheat:  we compile it twice, once with code
  // generation turned off and size counting turned on, and once "for real".
  // This also means that we don't allocate space until we are sure that the
  // thing really will compile successfully, and we never have to move the
  // code and thus invalidate pointers into it.  (Note that it has to be in
  // one piece because free() must be able to free it all.)
  // Beware that the optimization-preparation code in here knows about some
  // of the structure of the compiled regexp.
var
  scan, longest: PRegExprChar;
  len: cardinal;
  flags: integer;
begin
  Result := False;

  regparse := nil;
  regexpbeg := exp;
  try

    if programm <> nil then
    begin
      FreeMem(programm);
      programm := nil;
    end;

    if exp = nil then
    begin
      Error(reeCompNullArgument);
      EXIT;
    end;

    fProgModifiers := fModifiers;

    fCompModifiers := fModifiers;
    regparse := exp;
    regnpar := 1;
    regsize := 0;
    regcode := @regdummy;
    EmitC(MAGIC);
    if ParseReg(0, flags) = nil then EXIT;

    GetMem(programm, regsize * SizeOf(REChar));

    fCompModifiers := fModifiers;
    regparse := exp;
    regnpar := 1;
    regcode := programm;
    EmitC(MAGIC);
    if ParseReg(0, flags) = nil then  EXIT;

    regstart := #0;
    reganch := #0;
    regmust := nil;
    regmlen := 0;
    scan := programm + REOpSz;
    if PREOp(regnext(scan))^ = EEND then
    begin
      scan := scan + REOpSz + RENextOffSz;

      // Starting-point info.
      if PREOp(scan)^ = EXACTLY then
        regstart := (scan + REOpSz + RENextOffSz)^
      else if PREOp(scan)^ = BOL then
        Inc(reganch);

      // If there's something expensive in the r.e., find the longest
      // literal string that must appear and make it the regmust.  Resolve
      // ties in favor of later strings, since the regstart check works
      // with the beginning of the r.e. and avoiding duplication
      // strengthens checking.  Not a strong reason, but sufficient in the
      // absence of others.
      if (flags and SPSTART) <> 0 then
      begin
        longest := nil;
        len := 0;
        while scan <> nil do
        begin
          if (PREOp(scan)^ = EXACTLY) and (strlen(scan + REOpSz + RENextOffSz) >= len) then
          begin
            longest := scan + REOpSz + RENextOffSz;
            len := strlen(longest);
          end;
          scan := regnext(scan);
        end;
        regmust := longest;
        regmlen := len;
      end;
    end;

    Result := True;

  finally
    begin
      if not Result then
        InvalidateProgramm;
      regexpbeg := nil;
      fExprIsCompiled := Result;
    end;
  end;

end;

function TRegExpr.ParseReg(paren: integer; var flagp: integer): PRegExprChar;
  // regular expression, i.e. main body or parenthesized thing
  // Caller must absorb opening parenthesis.
  // Combining parenthesis handling with the base level of regular expression
  // is a trifle forced, but the need to tie the tails of the branches to what
  // follows makes it hard to avoid.
var
  ret, br, ender: PRegExprChar;
  parno: integer;
  flags: integer;
  SavedModifiers: integer;
begin
  Result := nil;
  flagp := HASWIDTH; // Tentatively.
  parno := 0; // eliminate compiler stupid warning
  SavedModifiers := fCompModifiers;

  // Make an OPEN node, if parenthesized.
  if paren <> 0 then
  begin
    if regnpar >= NSUBEXP then
    begin
      Error(reeCompParseRegTooManyBrackets);
      EXIT;
    end;
    parno := regnpar;
    Inc(regnpar);
    ret := EmitNode(TREOp(Ord(Open) + parno));
  end
  else
    ret := nil;

  // Pick up the branches, linking them together.
  br := ParseBranch(flags);
  if br = nil then
  begin
    Result := nil;
    EXIT;
  end;
  if ret <> nil then
    Tail(ret, br) // OPEN -> first.
  else
    ret := br;
  if (flags and HASWIDTH) = 0 then
    flagp := flagp and not HASWIDTH;
  flagp := flagp or flags and SPSTART;
  while (regparse^ = '|') do
  begin
    Inc(regparse);
    br := ParseBranch(flags);
    if br = nil then
    begin
      Result := nil;
      EXIT;
    end;
    Tail(ret, br); // BRANCH -> BRANCH.
    if (flags and HASWIDTH) = 0 then
      flagp := flagp and not HASWIDTH;
    flagp := flagp or flags and SPSTART;
  end;

  // Make a closing node, and hook it on the end.
  if paren <> 0 then
    ender := EmitNode(TREOp(Ord(Close) + parno))
  else
    ender := EmitNode(EEND);
  Tail(ret, ender);

  // Hook the tails of the branches to the closing node.
  br := ret;
  while br <> nil do
  begin
    OpTail(br, ender);
    br := regnext(br);
  end;

  // Check for proper termination.
  if paren <> 0 then
    if regparse^ <> ')' then
    begin
      Error(reeCompParseRegUnmatchedBrackets);
      EXIT;
    end
    else
      Inc(regparse); // skip trailing ')'
  if (paren = 0) and (regparse^ <> #0) then
  begin
    if regparse^ = ')' then
      Error(reeCompParseRegUnmatchedBrackets2)
    else
      Error(reeCompParseRegJunkOnEnd);
    EXIT;
  end;
  fCompModifiers := SavedModifiers; // restore modifiers of parent
  Result := ret;
end;

function TRegExpr.ParseBranch(var flagp: integer): PRegExprChar;
  // one alternative of an | operator
  // Implements the concatenation operator.
var
  ret, chain, latest: PRegExprChar;
  flags: integer;
begin
  flagp := WORST; // Tentatively.

  ret := EmitNode(BRANCH);
  chain := nil;
  while (regparse^ <> #0) and (regparse^ <> '|') and (regparse^ <> ')') do
  begin
    latest := ParsePiece(flags);
    if latest = nil then
    begin
      Result := nil;
      EXIT;
    end;
    flagp := flagp or flags and HASWIDTH;
    if chain = nil // First piece.
    then
      flagp := flagp or flags and SPSTART
    else
      Tail(chain, latest);
    chain := latest;
  end;
  if chain = nil // Loop ran zero times.
  then
    EmitNode(NOTHING);
  Result := ret;
end;

function TRegExpr.ParsePiece(var flagp: integer): PRegExprChar;
  // something followed by possible [*+?{]
  // Note that the branching code sequences used for ? and the general cases
  // of * and + and { are somewhat optimized:  they use the same NOTHING node as
  // both the endmarker for their branch list and the body of the last branch.
  // It might seem that this node could be dispensed with entirely, but the
  // endmarker role is not redundant.
  function parsenum(AStart, AEnd: PRegExprChar): TREBracesArg;
  begin
    Result := 0;
    if AEnd - AStart + 1 > 8 then
    begin // prevent stupid scanning
      Error(reeBRACESArgTooBig);
      EXIT;
    end;
    while AStart <= AEnd do
    begin
      Result := Result * 10 + (Ord(AStart^) - Ord('0'));
      Inc(AStart);
    end;
    if (Result > MaxBracesArg) or (Result < 0) then
    begin
      Error(reeBRACESArgTooBig);
      EXIT;
    end;
  end;

var
  op: REChar;
  NonGreedyOp, NonGreedyCh: boolean; //###0.940
  TheOp: TREOp; //###0.940
  NextNode: PRegExprChar;
  flags: integer;
  BracesMin, Bracesmax: TREBracesArg;
  p, savedparse: PRegExprChar;

  procedure EmitComplexBraces(ABracesMin, ABracesMax: TREBracesArg; ANonGreedyOp: boolean);
  var
    off: integer;
  begin

    if ANonGreedyOp then
      TheOp := LOOPNG
    else
      TheOp := LOOP;
    InsertOperator(LOOPENTRY, Result, REOpSz + RENextOffSz);
    NextNode := EmitNode(TheOp);
    if regcode <> @regdummy then
    begin
      off := (Result + REOpSz + RENextOffSz) - (regcode - REOpSz - RENextOffSz); // back to Atom after LOOPENTRY
      PREBracesArg(regcode)^ := ABracesMin;
      Inc(regcode, REBracesArgSz);
      PREBracesArg(regcode)^ := ABracesMax;
      Inc(regcode, REBracesArgSz);
      PRENextOff(regcode)^ := off;
      Inc(regcode, RENextOffSz);
    end
    else
      Inc(regsize, REBracesArgSz * 2 + RENextOffSz);
    Tail(Result, NextNode); // LOOPENTRY -> LOOP
    if regcode <> @regdummy then
      Tail(Result + REOpSz + RENextOffSz, NextNode); // Atom -> LOOP

  end;

  procedure EmitSimpleBraces(ABracesMin, ABracesMax: TREBracesArg; ANonGreedyOp: boolean); //###0.940
  begin
    if ANonGreedyOp //###0.940
    then
      TheOp := BRACESNG
    else
      TheOp := BRACES;
    InsertOperator(TheOp, Result, REOpSz + RENextOffSz + REBracesArgSz * 2);
    if regcode <> @regdummy then
    begin
      PREBracesArg(Result + REOpSz + RENextOffSz)^ := ABracesMin;
      PREBracesArg(Result + REOpSz + RENextOffSz + REBracesArgSz)^ := ABracesMax;
    end;
  end;

begin
  Result := ParseAtom(flags);
  if Result = nil then
    EXIT;

  op := regparse^;
  if not ((op = '*') or (op = '+') or (op = '?') or (op = '{')) then
  begin
    flagp := flags;
    EXIT;
  end;
  if ((flags and HASWIDTH) = 0) and (op <> '?') then
  begin
    Error(reePlusStarOperandCouldBeEmpty);
    EXIT;
  end;

  case op of
    '*':
    begin
      flagp := WORST or SPSTART;
      NonGreedyCh := (regparse + 1)^ = '?'; //###0.940
      NonGreedyOp := NonGreedyCh or ((fCompModifiers and MaskModG) = 0); //###0.940
      if (flags and SIMPLE) = 0 then
      begin
        if NonGreedyOp //###0.940
        then
          EmitComplexBraces(0, MaxBracesArg, NonGreedyOp)
        else
        begin // Emit x* as (x&|), where & means "self".
          InsertOperator(BRANCH, Result, REOpSz + RENextOffSz); // Either x
          OpTail(Result, EmitNode(BACK)); // and loop
          OpTail(Result, Result); // back
          Tail(Result, EmitNode(BRANCH)); // or
          Tail(Result, EmitNode(NOTHING)); // nil.
        end;
      end
      else
      begin // Simple
        if NonGreedyOp //###0.940
        then
          TheOp := STARNG
        else
          TheOp := STAR;
        InsertOperator(TheOp, Result, REOpSz + RENextOffSz);
      end;
      if NonGreedyCh //###0.940
      then
        Inc(regparse); // Skip extra char ('?')
    end; { of case '*'}
    '+':
    begin
      flagp := WORST or SPSTART or HASWIDTH;
      NonGreedyCh := (regparse + 1)^ = '?'; //###0.940
      NonGreedyOp := NonGreedyCh or ((fCompModifiers and MaskModG) = 0); //###0.940
      if (flags and SIMPLE) = 0 then
      begin
        if NonGreedyOp //###0.940
        then
          EmitComplexBraces(1, MaxBracesArg, NonGreedyOp)
        else
        begin // Emit x+ as x(&|), where & means "self".
          NextNode := EmitNode(BRANCH); // Either
          Tail(Result, NextNode);
          Tail(EmitNode(BACK), Result);    // loop back
          Tail(NextNode, EmitNode(BRANCH)); // or
          Tail(Result, EmitNode(NOTHING)); // nil.
        end;
      end
      else
      begin // Simple
        if NonGreedyOp //###0.940
        then
          TheOp := PLUSNG
        else
          TheOp := PLUS;
        InsertOperator(TheOp, Result, REOpSz + RENextOffSz);
      end;
      if NonGreedyCh //###0.940
      then
        Inc(regparse); // Skip extra char ('?')
    end; { of case '+'}
    '?':
    begin
      flagp := WORST;
      NonGreedyCh := (regparse + 1)^ = '?'; //###0.940
      NonGreedyOp := NonGreedyCh or ((fCompModifiers and MaskModG) = 0); //###0.940
      if NonGreedyOp then
      begin //###0.940  // We emit x?? as x{0,1}?
        if (flags and SIMPLE) = 0 then
          EmitComplexBraces(0, 1, NonGreedyOp)
        else
          EmitSimpleBraces(0, 1, NonGreedyOp);
      end
      else
      begin // greedy '?'
        InsertOperator(BRANCH, Result, REOpSz + RENextOffSz); // Either x
        Tail(Result, EmitNode(BRANCH));  // or
        NextNode := EmitNode(NOTHING); // nil.
        Tail(Result, NextNode);
        OpTail(Result, NextNode);
      end;
      if NonGreedyCh //###0.940
      then
        Inc(regparse); // Skip extra char ('?')
    end; { of case '?'}
    '{':
    begin
      savedparse := regparse;
      // !!!!!!!!!!!!
      // Filip Jirsak's note - what will happen, when we are at the end of regparse?
      Inc(regparse);
      p := regparse;
      while Pos(regparse^, '0123456789') > 0  // <min> MUST appear
        do
        Inc(regparse);
      if (regparse^ <> '}') and (regparse^ <> ',') or (p = regparse) then
      begin
        regparse := savedparse;
        flagp := flags;
        EXIT;
      end;
      BracesMin := parsenum(p, regparse - 1);
      if regparse^ = ',' then
      begin
        Inc(regparse);
        p := regparse;
        while Pos(regparse^, '0123456789') > 0 do
          Inc(regparse);
        if regparse^ <> '}' then
        begin
          regparse := savedparse;
          EXIT;
        end;
        if p = regparse then
          BracesMax := MaxBracesArg
        else
          BracesMax := parsenum(p, regparse - 1);
      end
      else
        BracesMax := BracesMin; // {n} == {n,n}
      if BracesMin > BracesMax then
      begin
        Error(reeBracesMinParamGreaterMax);
        EXIT;
      end;
      if BracesMin > 0 then
        flagp := WORST;
      if BracesMax > 0 then
        flagp := flagp or HASWIDTH or SPSTART;

      NonGreedyCh := (regparse + 1)^ = '?'; //###0.940
      NonGreedyOp := NonGreedyCh or ((fCompModifiers and MaskModG) = 0); //###0.940
      if (flags and SIMPLE) <> 0 then
        EmitSimpleBraces(BracesMin, BracesMax, NonGreedyOp)
      else
        EmitComplexBraces(BracesMin, BracesMax, NonGreedyOp);
      if NonGreedyCh then
        Inc(regparse);
    end;

  end;

  Inc(regparse);
  if (regparse^ = '*') or (regparse^ = '+') or (regparse^ = '?') or (regparse^ = '{') then
  begin
    Error(reeNestedSQP);
    EXIT;
  end;
end;


function TRegExpr.ParseAtom(var flagp: integer): PRegExprChar;
  // the lowest level
  // Optimization:  gobbles an entire sequence of ordinary characters so that
  // it can turn them into a single node, which is smaller to store and
  // faster to run.  Backslashed characters are exceptions, each becoming a
  // separate node; the code is simpler that way and it's not worth fixing.
var
  ret: PRegExprChar;
  flags: integer;
  RangeBeg, RangeEnd: REChar;
  CanBeRange: boolean;
  len: integer;
  ender: REChar;
  begmodfs: PRegExprChar;



  procedure EmitExactly(ch: REChar);
  begin
    if (fCompModifiers and MaskModI) <> 0 then
      ret := EmitNode(EXACTLYCI)
    else
      ret := EmitNode(EXACTLY);
    EmitC(ch);
    EmitC(#0);
    flagp := flagp or HASWIDTH or SIMPLE;
  end;

  procedure EmitStr(const s: RegExprString);
  var
    i: integer;
  begin
    for i := 1 to length(s) do
      EmitC(s[i]);
  end;

  function HexDig(ch: REChar): integer;
  begin
    Result := 0;
    if (ch >= 'a') and (ch <= 'f') then
      ch := REChar(Ord(ch) - (Ord('a') - Ord('A')));
    if (ch < '0') or (ch > 'F') or ((ch > '9') and (ch < 'A')) then
    begin
      Error(reeBadHexDigit);
      EXIT;
    end;
    Result := Ord(ch) - Ord('0');
    if ch >= 'A' then
      Result := Result - (Ord('A') - Ord('9') - 1);
  end;

  function EmitRange(AOpCode: REChar): PRegExprChar;
  begin
    Result := EmitNode(AOpCode);
  end;

  procedure EmitRangeC(b: REChar);
  begin
    CanBeRange := False;
    EmitC(b);
  end;

  procedure EmitSimpleRangeC(b: REChar);
  begin
    RangeBeg := b;
    EmitRangeC(b);
    CanBeRange := True;
  end;

  procedure EmitRangeStr(const s: RegExprString);
  var
    i: integer;
  begin
    for i := 1 to length(s) do
      EmitRangeC(s[i]);
  end;

  function UnQuoteChar(var APtr: PRegExprChar): REChar; //###0.934
  begin
    case APtr^ of
      't': Result := #$9;  // tab (HT/TAB)
      'n': Result := #$a;  // newline (NL)
      'r': Result := #$d;  // car.return (CR)
      'f': Result := #$c;  // form feed (FF)
      'a': Result := #$7;  // alarm (bell) (BEL)
      'e': Result := #$1b; // escape (ESC)
      'x':
      begin // hex char
        Result := #0;
        Inc(APtr);
        if APtr^ = #0 then
        begin
          Error(reeNoHexCodeAfterBSlashX);
          EXIT;
        end;
        if APtr^ = '{' then
        begin // \x{nnnn} //###0.936
          repeat
            Inc(APtr);
            if APtr^ = #0 then
            begin
              Error(reeNoHexCodeAfterBSlashX);
              EXIT;
            end;
            if APtr^ <> '}' then
            begin
              if (Ord(Result) shr (SizeOf(REChar) * 8 - 4)) and $F <> 0 then
              begin
                Error(reeHexCodeAfterBSlashXTooBig);
                EXIT;
              end;
              Result := REChar((Ord(Result) shl 4) or HexDig(APtr^));
              // HexDig will cause Error if bad hex digit found
            end
            else
              BREAK;
          until False;
        end
        else
        begin
          Result := REChar(HexDig(APtr^));
          // HexDig will cause Error if bad hex digit found
          Inc(APtr);
          if APtr^ = #0 then
          begin
            Error(reeNoHexCodeAfterBSlashX);
            EXIT;
          end;
          Result := REChar((Ord(Result) shl 4) or HexDig(APtr^));
          // HexDig will cause Error if bad hex digit found
        end;
      end;
      else
        Result := APtr^;
    end;
  end;

begin
  Result := nil;
  flagp := WORST; // Tentatively.

  Inc(regparse);
  case (regparse - 1)^ of
    '^': if ((fCompModifiers and MaskModM) = 0) or ((fLineSeparators = '') and not fLinePairedSeparatorAssigned) then
        ret := EmitNode(BOL)
      else
        ret := EmitNode(BOLML);
    '$': if ((fCompModifiers and MaskModM) = 0) or ((fLineSeparators = '') and not fLinePairedSeparatorAssigned) then
        ret := EmitNode(EOL)
      else
        ret := EmitNode(EOLML);
    '.':
      if (fCompModifiers and MaskModS) <> 0 then
      begin
        ret := EmitNode(ANY);
        flagp := flagp or HASWIDTH or SIMPLE;
      end
      else
      begin // not /s, so emit [^:LineSeparators:]
        ret := EmitNode(ANYML);
        flagp := flagp or HASWIDTH; // not so simple ;)
        //          ret := EmitRange (ANYBUT);
        //          EmitRangeStr (LineSeparators); //###0.941
        //          EmitRangeStr (LinePairedSeparator); // !!! isn't correct if have to accept only paired
        //          EmitRangeC (#0);
        //          flagp := flagp or HASWIDTH or SIMPLE;
      end;
    '[':
    begin
      if regparse^ = '^' then
      begin // Complement of range.
        if (fCompModifiers and MaskModI) <> 0 then
          ret := EmitRange(ANYBUTCI)
        else
          ret := EmitRange(ANYBUT);
        Inc(regparse);
      end
      else
      if (fCompModifiers and MaskModI) <> 0 then
        ret := EmitRange(ANYOFCI)
      else
        ret := EmitRange(ANYOF);

      CanBeRange := False;

      if (regparse^ = ']') then
      begin
        EmitSimpleRangeC(regparse^); // []-a] -> ']' .. 'a'
        Inc(regparse);
      end;

      while (regparse^ <> #0) and (regparse^ <> ']') do
      begin
        if (regparse^ = '-') and ((regparse + 1)^ <> #0) and ((regparse + 1)^ <> ']') and CanBeRange then
        begin
          Inc(regparse);
          RangeEnd := regparse^;
          if RangeEnd = EscChar then
          begin

            if (Ord((regparse + 1)^) < 256) and (char((regparse + 1)^) in
              ['d', 'D', 's', 'S', 'w', 'W']) then
            begin

              EmitRangeC('-'); // or treat as error ?!!
              CONTINUE;
            end;
            Inc(regparse);
            RangeEnd := UnQuoteChar(regparse);
          end;

          // r.e.ranges extension for russian
          if ((fCompModifiers and MaskModR) <> 0) and (RangeBeg = RusRangeLoLow) and (RangeEnd = RusRangeLoHigh) then
          begin
            EmitRangeStr(RusRangeLo);
          end
          else if ((fCompModifiers and MaskModR) <> 0) and (RangeBeg = RusRangeHiLow) and (RangeEnd = RusRangeHiHigh) then
          begin
            EmitRangeStr(RusRangeHi);
          end
          else if ((fCompModifiers and MaskModR) <> 0) and (RangeBeg = RusRangeLoLow) and (RangeEnd = RusRangeHiHigh) then
          begin
            EmitRangeStr(RusRangeLo);
            EmitRangeStr(RusRangeHi);
          end
          else
          begin // standard r.e. handling
            if RangeBeg > RangeEnd then
            begin
              Error(reeInvalidRange);
              EXIT;
            end;
            Inc(RangeBeg);
            EmitRangeC(RangeEnd); // prevent infinite loop if RangeEnd=$ff
            while RangeBeg < RangeEnd do
            begin //###0.929
              EmitRangeC(RangeBeg);
              Inc(RangeBeg);
            end;
          end;
          Inc(regparse);
        end
        else
        begin
          if regparse^ = EscChar then
          begin
            Inc(regparse);
            if regparse^ = #0 then
            begin
              Error(reeParseAtomTrailingBackSlash);
              EXIT;
            end;
            case regparse^ of // r.e.extensions
              'd': EmitRangeStr('0123456789');
              'w': EmitRangeStr(WordChars);
              's': EmitRangeStr(SpaceChars);
              else
                EmitSimpleRangeC(UnQuoteChar(regparse));
            end;
          end
          else
            EmitSimpleRangeC(regparse^);
          Inc(regparse);
        end;
      end;
      EmitRangeC(#0);
      if regparse^ <> ']' then
      begin
        Error(reeUnmatchedSqBrackets);
        EXIT;
      end;
      Inc(regparse);
      flagp := flagp or HASWIDTH or SIMPLE;
    end;
    '(':
    begin
      if regparse^ = '?' then
      begin

        if (regparse + 1)^ = '#' then
        begin
          Inc(regparse, 2); // find closing ')'
          while (regparse^ <> #0) and (regparse^ <> ')') do
            Inc(regparse);
          if regparse^ <> ')' then
          begin
            Error(reeUnclosedComment);
            EXIT;
          end;
          Inc(regparse); // skip ')'
          ret := EmitNode(COMMENT); // comment
        end
        else
        begin // modifiers ?
          Inc(regparse); // skip '?'
          begmodfs := regparse;
          while (regparse^ <> #0) and (regparse^ <> ')') do
            Inc(regparse);
          if (regparse^ <> ')') or not ParseModifiersStr(copy(begmodfs, 1, (regparse - begmodfs)), fCompModifiers) then
          begin
            Error(reeUrecognizedModifier);
            EXIT;
          end;
          Inc(regparse); // skip ')'
          ret := EmitNode(COMMENT); // comment
          //             Error (reeQPSBFollowsNothing);
          //             EXIT;
        end;
      end
      else
      begin
        ret := ParseReg(1, flags);
        if ret = nil then
        begin
          Result := nil;
          EXIT;
        end;
        flagp := flagp or flags and (HASWIDTH or SPSTART);
      end;
    end;
    #0, '|', ')':
    begin // Supposed to be caught earlier.
      Error(reeInternalUrp);
      EXIT;
    end;
    '?', '+', '*':
    begin
      Error(reeQPSBFollowsNothing);
      EXIT;
    end;
    EscChar:
    begin
      if regparse^ = #0 then
      begin
        Error(reeTrailingBackSlash);
        EXIT;
      end;
      case regparse^ of // r.e.extensions
        'b': ret := EmitNode(BOUND); //###0.943
        'B': ret := EmitNode(NOTBOUND); //###0.943
        'A': ret := EmitNode(BOL); //###0.941
        'Z': ret := EmitNode(EOL); //###0.941
        'd':
        begin // r.e.extension - any digit ('0' .. '9')
          ret := EmitNode(ANYDIGIT);
          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        'D':
        begin // r.e.extension - not digit ('0' .. '9')
          ret := EmitNode(NOTDIGIT);
          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        's':
        begin // r.e.extension - any space char

          ret := EmitNode(ANYSPACE);

          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        'S':
        begin // r.e.extension - not space char

          ret := EmitNode(NOTSPACE);

          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        'w':
        begin // r.e.extension - any english char / digit / '_'

          ret := EmitNode(ANYLETTER);

          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        'W':
        begin // r.e.extension - not english char / digit / '_'

          ret := EmitNode(NOTLETTER);

          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        '1' .. '9':
        begin //###0.936
          if (fCompModifiers and MaskModI) <> 0 then
            ret := EmitNode(BSUBEXPCI)
          else
            ret := EmitNode(BSUBEXP);
          EmitC(REChar(Ord(regparse^) - Ord('0')));
          flagp := flagp or HASWIDTH or SIMPLE;
        end;
        else
          EmitExactly(UnQuoteChar(regparse));
      end; { of case}
      Inc(regparse);
    end;
    else
    begin
      Dec(regparse);
      if ((fCompModifiers and MaskModX) <> 0) and // check for eXtended syntax
        ((regparse^ = '#') or (StrScan(XIgnoredChars, regparse^) <> nil)) then
      begin
        if regparse^ = '#' then
        begin // Skip eXtended comment
          // find comment terminator (group of \n and/or \r)
          while (regparse^ <> #0) and (regparse^ <> #$d) and (regparse^ <> #$a) do
            Inc(regparse);
          while (regparse^ = #$d) or (regparse^ = #$a) // skip comment terminator
            do
            Inc(regparse); // attempt to support different type of line separators
        end
        else
        begin // Skip the blanks!
          while StrScan(XIgnoredChars, regparse^) <> nil do
            Inc(regparse);
        end;
        ret := EmitNode(COMMENT);
      end
      else
      begin
        len := strcspn(regparse, META);
        if len <= 0 then
          if regparse^ <> '{' then
          begin
            Error(reeRarseAtomInternalDisaster);
            EXIT;
          end
          else
            len := strcspn(regparse + 1, META) + 1; // bad {n,m} - compile as EXATLY
        ender := (regparse + len)^;
        if (len > 1) and ((ender = '*') or (ender = '+') or (ender = '?') or (ender = '{')) then
          Dec(len); // Back off clear of ?+*{ operand.
        flagp := flagp or HASWIDTH;
        if len = 1 then
          flagp := flagp or SIMPLE;
        if (fCompModifiers and MaskModI) <> 0 then
          ret := EmitNode(EXACTLYCI)
        else
          ret := EmitNode(EXACTLY);
        while (len > 0) and (((fCompModifiers and MaskModX) = 0) or (regparse^ <> '#')) do
        begin
          if ((fCompModifiers and MaskModX) = 0) or not (StrScan(XIgnoredChars, regparse^) <> nil) then
            EmitC(regparse^);
          Inc(regparse);
          Dec(len);
        end;
        EmitC(#0);
      end;
    end;
  end;

  Result := ret;
end;


function TRegExpr.GetCompilerErrorPos: integer;
begin
  Result := 0;
  if (regexpbeg = nil) or (regparse = nil) then
    EXIT; // not in compiling mode ?
  Result := regparse - regexpbeg;
end;

//===================== Matching section ======================

function TRegExpr.StrScanCI(s: PRegExprChar; ch: REChar): PRegExprChar; //###0.928 - now method of TRegExpr
begin
  while (s^ <> #0) and (s^ <> ch) and (s^ <> InvertCase(ch)) do
    Inc(s);
  if s^ <> #0 then
    Result := s
  else
    Result := nil;
end;

function TRegExpr.regrepeat(p: PRegExprChar; AMax: integer): integer;
  // repeatedly match something simple, report how many
var
  scan: PRegExprChar;
  opnd: PRegExprChar;
  TheMax: integer;
  {Ch,} InvCh: REChar; //###0.931
  sestart, seend: PRegExprChar; //###0.936
begin
  Result := 0;
  scan := reginput;
  opnd := p + REOpSz + RENextOffSz; //OPERAND
  TheMax := fInputEnd - scan;
  if TheMax > AMax then
    TheMax := AMax;
  case PREOp(p)^ of
    ANY:
    begin
      // note - ANYML cannot be proceeded in regrepeat because can skip
      // more than one char at once
      Result := TheMax;
      Inc(scan, Result);
    end;
    EXACTLY:
    begin // in opnd can be only ONE char !!!
      //      Ch := opnd^; // store in register //###0.931
      while (Result < TheMax) and (opnd^ = scan^) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    end;
    EXACTLYCI:
    begin // in opnd can be only ONE char !!!
      //      Ch := opnd^; // store in register //###0.931
      while (Result < TheMax) and (opnd^ = scan^) do
      begin // prevent unneeded InvertCase //###0.931
        Inc(Result);
        Inc(scan);
      end;
      if Result < TheMax then
      begin //###0.931
        InvCh := InvertCase(opnd^); // store in register
        while (Result < TheMax) and ((opnd^ = scan^) or (InvCh = scan^)) do
        begin
          Inc(Result);
          Inc(scan);
        end;
      end;
    end;
    BSUBEXP:
    begin //###0.936
      sestart := startp[Ord(opnd^)];
      if sestart = nil then
        EXIT;
      seend := endp[Ord(opnd^)];
      if seend = nil then
        EXIT;
      repeat
        opnd := sestart;
        while opnd < seend do
        begin
          if (scan >= fInputEnd) or (scan^ <> opnd^) then
            EXIT;
          Inc(scan);
          Inc(opnd);
        end;
        Inc(Result);
        reginput := scan;
      until Result >= AMax;
    end;
    BSUBEXPCI:
    begin //###0.936
      sestart := startp[Ord(opnd^)];
      if sestart = nil then
        EXIT;
      seend := endp[Ord(opnd^)];
      if seend = nil then
        EXIT;
      repeat
        opnd := sestart;
        while opnd < seend do
        begin
          if (scan >= fInputEnd) or ((scan^ <> opnd^) and (scan^ <> InvertCase(opnd^))) then
            EXIT;
          Inc(scan);
          Inc(opnd);
        end;
        Inc(Result);
        reginput := scan;
      until Result >= AMax;
    end;
    ANYDIGIT:
      while (Result < TheMax) and (scan^ >= '0') and (scan^ <= '9') do
      begin
        Inc(Result);
        Inc(scan);
      end;
    NOTDIGIT:
      while (Result < TheMax) and ((scan^ < '0') or (scan^ > '9')) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    ANYLETTER:
      while (Result < TheMax) and (Pos(scan^, fWordChars) > 0) //###0.940
     {  ((scan^ >= 'a') and (scan^ <= 'z') !! I've forgotten (>='0') and (<='9')
       or (scan^ >= 'A') and (scan^ <= 'Z') or (scan^ = '_'))} do
      begin
        Inc(Result);
        Inc(scan);
      end;
    NOTLETTER:
      while (Result < TheMax) and (Pos(scan^, fWordChars) <= 0)  //###0.940
     {   not ((scan^ >= 'a') and (scan^ <= 'z') !! I've forgotten (>='0') and (<='9')
         or (scan^ >= 'A') and (scan^ <= 'Z')
         or (scan^ = '_'))} do
      begin
        Inc(Result);
        Inc(scan);
      end;
    ANYSPACE:
      while (Result < TheMax) and (Pos(scan^, fSpaceChars) > 0) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    NOTSPACE:
      while (Result < TheMax) and (Pos(scan^, fSpaceChars) <= 0) do
      begin
        Inc(Result);
        Inc(scan);
      end;

    ANYOFTINYSET:
    begin
      while (Result < TheMax) and //!!!TinySet
        ((scan^ = opnd^) or (scan^ = (opnd + 1)^) or (scan^ = (opnd + 2)^)) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    end;
    ANYBUTTINYSET:
    begin
      while (Result < TheMax) and //!!!TinySet
        (scan^ <> opnd^) and (scan^ <> (opnd + 1)^) and (scan^ <> (opnd + 2)^) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    end;
    ANYOF:
      while (Result < TheMax) and (StrScan(opnd, scan^) <> nil) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    ANYBUT:
      while (Result < TheMax) and (StrScan(opnd, scan^) = nil) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    ANYOFCI:
      while (Result < TheMax) and (StrScanCI(opnd, scan^) <> nil) do
      begin
        Inc(Result);
        Inc(scan);
      end;
    ANYBUTCI:
      while (Result < TheMax) and (StrScanCI(opnd, scan^) = nil) do
      begin
        Inc(Result);
        Inc(scan);
      end;

    else
    begin
      Result := 0;
      Error(reeRegRepeatCalledInappropriately);
      EXIT;
    end;
  end;
  reginput := scan;
end;

function TRegExpr.regnext(p: PRegExprChar): PRegExprChar;
var
  offset: TRENextOff;
begin
  if p = @regdummy then
  begin
    Result := nil;
    EXIT;
  end;
  offset := PRENextOff(p + REOpSz)^;
  if offset = 0 then
    Result := nil
  else
    Result := p + offset;
end;

function TRegExpr.MatchPrim(prog: PRegExprChar): boolean;
  // recursively matching routine
  // Conceptually the strategy is simple:  check to see whether the current
  // node matches, call self recursively to see whether the rest matches,
  // and then act accordingly.  In practice we make some effort to avoid
  // recursion, in particular by going through "ordinary" nodes (that don't
  // need to know whether the rest of the match failed) by a loop instead of
  // by recursion.
var
  scan: PRegExprChar; // Current node.
  Next: PRegExprChar; // Next node.
  len: integer;
  opnd: PRegExprChar;
  no: integer;
  save: PRegExprChar;
  nextch: REChar;
  BracesMin, BracesMax: integer; // we use integer instead of TREBracesArg for better support */+

  SavedLoopStack: array [1 .. LoopStackMax] of integer; // :(( very bad for recursion
  SavedLoopStackIdx: integer; //###0.925

begin
  Result := False;
  scan := prog;

  while scan <> nil do
  begin
    len := PRENextOff(scan + 1)^; //###0.932 inlined regnext
    if len = 0 then
      Next := nil
    else
      Next := scan + len;

    case scan^ of
      NOTBOUND, //###0.943 //!!! think about UseSetOfChar !!!
      BOUND:
        if (scan^ = BOUND) xor (((reginput = fInputStart) or (Pos((reginput - 1)^, fWordChars) <= 0)) and
          (reginput^ <> #0) and (Pos(reginput^, fWordChars) > 0) or (reginput <> fInputStart) and
          (Pos((reginput - 1)^, fWordChars) > 0) and ((reginput^ = #0) or (Pos(reginput^, fWordChars) <= 0))) then
          EXIT;

      BOL: if reginput <> fInputStart then
          EXIT;
      EOL: if reginput^ <> #0 then
          EXIT;
      BOLML: if reginput > fInputStart then
        begin
          nextch := (reginput - 1)^;
          if (nextch <> fLinePairedSeparatorTail) or ((reginput - 1) <= fInputStart) or
            ((reginput - 2)^ <> fLinePairedSeparatorHead) then
          begin
            if (nextch = fLinePairedSeparatorHead) and (reginput^ = fLinePairedSeparatorTail) then
              EXIT; // don't stop between paired separator
            if (pos(nextch, fLineSeparators) <= 0) then
              EXIT;
          end;
        end;
      EOLML: if reginput^ <> #0 then
        begin
          nextch := reginput^;
          if (nextch <> fLinePairedSeparatorHead) or ((reginput + 1)^ <> fLinePairedSeparatorTail) then
          begin
            if (nextch = fLinePairedSeparatorTail) and (reginput > fInputStart) and
              ((reginput - 1)^ = fLinePairedSeparatorHead) then
              EXIT; // don't stop between paired separator
            if (pos(nextch, fLineSeparators) <= 0) then
              EXIT;
          end;
        end;
      ANY:
      begin
        if reginput^ = #0 then
          EXIT;
        Inc(reginput);
      end;
      ANYML:
      begin //###0.941
        if (reginput^ = #0) or ((reginput^ = fLinePairedSeparatorHead) and
          ((reginput + 1)^ = fLinePairedSeparatorTail)) or (pos(reginput^, fLineSeparators) > 0) then
          EXIT;
        Inc(reginput);
      end;
      ANYDIGIT:
      begin
        if (reginput^ = #0) or (reginput^ < '0') or (reginput^ > '9') then
          EXIT;
        Inc(reginput);
      end;
      NOTDIGIT:
      begin
        if (reginput^ = #0) or ((reginput^ >= '0') and (reginput^ <= '9')) then
          EXIT;
        Inc(reginput);
      end;

      ANYLETTER:
      begin
        if (reginput^ = #0) or (Pos(reginput^, fWordChars) <= 0) //###0.943
        then
          EXIT;
        Inc(reginput);
      end;
      NOTLETTER:
      begin
        if (reginput^ = #0) or (Pos(reginput^, fWordChars) > 0) //###0.943
        then
          EXIT;
        Inc(reginput);
      end;
      ANYSPACE:
      begin
        if (reginput^ = #0) or not (Pos(reginput^, fSpaceChars) > 0) //###0.943
        then
          EXIT;
        Inc(reginput);
      end;
      NOTSPACE:
      begin
        if (reginput^ = #0) or (Pos(reginput^, fSpaceChars) > 0) //###0.943
        then
          EXIT;
        Inc(reginput);
      end;

      EXACTLYCI:
      begin
        opnd := scan + REOpSz + RENextOffSz; // OPERAND
        // Inline the first character, for speed.
        if (opnd^ <> reginput^) and (InvertCase(opnd^) <> reginput^) then
          EXIT;
        len := strlen(opnd);
        //###0.929 begin
        no := len;
        save := reginput;
        while no > 1 do
        begin
          Inc(save);
          Inc(opnd);
          if (opnd^ <> save^) and (InvertCase(opnd^) <> save^) then
            EXIT;
          Dec(no);
        end;
        //###0.929 end
        Inc(reginput, len);
      end;
      EXACTLY:
      begin
        opnd := scan + REOpSz + RENextOffSz; // OPERAND
        // Inline the first character, for speed.
        if opnd^ <> reginput^ then
          EXIT;
        len := strlen(opnd);
        //###0.929 begin
        no := len;
        save := reginput;
        while no > 1 do
        begin
          Inc(save);
          Inc(opnd);
          if opnd^ <> save^ then
            EXIT;
          Dec(no);
        end;
        //###0.929 end
        Inc(reginput, len);
      end;
      BSUBEXP:
      begin //###0.936
        no := Ord((scan + REOpSz + RENextOffSz)^);
        if startp[no] = nil then
          EXIT;
        if endp[no] = nil then
          EXIT;
        save := reginput;
        opnd := startp[no];
        while opnd < endp[no] do
        begin
          if (save >= fInputEnd) or (save^ <> opnd^) then
            EXIT;
          Inc(save);
          Inc(opnd);
        end;
        reginput := save;
      end;
      BSUBEXPCI:
      begin //###0.936
        no := Ord((scan + REOpSz + RENextOffSz)^);
        if startp[no] = nil then
          EXIT;
        if endp[no] = nil then
          EXIT;
        save := reginput;
        opnd := startp[no];
        while opnd < endp[no] do
        begin
          if (save >= fInputEnd) or ((save^ <> opnd^) and (save^ <> InvertCase(opnd^))) then
            EXIT;
          Inc(save);
          Inc(opnd);
        end;
        reginput := save;
      end;
      ANYOFTINYSET:
      begin
        if (reginput^ = #0) or //!!!TinySet
          ((reginput^ <> (scan + REOpSz + RENextOffSz)^) and (reginput^ <> (scan + REOpSz + RENextOffSz + 1)^) and
          (reginput^ <> (scan + REOpSz + RENextOffSz + 2)^)) then
          EXIT;
        Inc(reginput);
      end;
      ANYBUTTINYSET:
      begin
        if (reginput^ = #0) or //!!!TinySet
          (reginput^ = (scan + REOpSz + RENextOffSz)^) or (reginput^ = (scan + REOpSz + RENextOffSz + 1)^) or
          (reginput^ = (scan + REOpSz + RENextOffSz + 2)^) then
          EXIT;
        Inc(reginput);
      end;

      ANYOF:
      begin
        if (reginput^ = #0) or (StrScan(scan + REOpSz + RENextOffSz, reginput^) = nil) then
          EXIT;
        Inc(reginput);
      end;
      ANYBUT:
      begin
        if (reginput^ = #0) or (StrScan(scan + REOpSz + RENextOffSz, reginput^) <> nil) then
          EXIT;
        Inc(reginput);
      end;
      ANYOFCI:
      begin
        if (reginput^ = #0) or (StrScanCI(scan + REOpSz + RENextOffSz, reginput^) = nil) then
          EXIT;
        Inc(reginput);
      end;
      ANYBUTCI:
      begin
        if (reginput^ = #0) or (StrScanCI(scan + REOpSz + RENextOffSz, reginput^) <> nil) then
          EXIT;
        Inc(reginput);
      end;

      NOTHING: ;
      COMMENT: ;
      BACK: ;
      Succ(Open) .. TREOp(Ord(Open) + NSUBEXP - 1):
      begin //###0.929
        no := Ord(scan^) - Ord(Open);
        //            save := reginput;
        save := startp[no]; //###0.936
        startp[no] := reginput; //###0.936
        Result := MatchPrim(Next);
        if not Result //###0.936
        then
          startp[no] := save;
        //            if Result and (startp [no] = nil)
        //             then startp [no] := save;
        // Don't set startp if some later invocation of the same
        // parentheses already has.
        EXIT;
      end;
      Succ(Close) .. TREOp(Ord(Close) + NSUBEXP - 1):
      begin //###0.929
        no := Ord(scan^) - Ord(Close);
        //            save := reginput;
        save := endp[no]; //###0.936
        endp[no] := reginput; //###0.936
        Result := MatchPrim(Next);
        if not Result //###0.936
        then
          endp[no] := save;
        //            if Result and (endp [no] = nil)
        //             then endp [no] := save;
        // Don't set endp if some later invocation of the same
        // parentheses already has.
        EXIT;
      end;
      BRANCH:
      begin
        if (Next^ <> BRANCH) // No choice.
        then
          Next := scan + REOpSz + RENextOffSz // Avoid recursion
        else
        begin
          repeat
            save := reginput;
            Result := MatchPrim(scan + REOpSz + RENextOffSz);
            if Result then
              EXIT;
            reginput := save;
            scan := regnext(scan);
          until (scan = nil) or (scan^ <> BRANCH);
          EXIT;
        end;
      end;

      LOOPENTRY:
      begin //###0.925
        no := LoopStackIdx;
        Inc(LoopStackIdx);
        if LoopStackIdx > LoopStackMax then
        begin
          Error(reeLoopStackExceeded);
          EXIT;
        end;
        save := reginput;
        LoopStack[LoopStackIdx] := 0; // init loop counter
        Result := MatchPrim(Next); // execute LOOP
        LoopStackIdx := no; // cleanup
        if Result then
          EXIT;
        reginput := save;
        EXIT;
      end;
      LOOP, LOOPNG:
      begin //###0.940
        if LoopStackIdx <= 0 then
        begin
          Error(reeLoopWithoutEntry);
          EXIT;
        end;
        opnd := scan + PRENextOff(scan + REOpSz + RENextOffSz + 2 * REBracesArgSz)^;
        BracesMin := PREBracesArg(scan + REOpSz + RENextOffSz)^;
        BracesMax := PREBracesArg(scan + REOpSz + RENextOffSz + REBracesArgSz)^;
        save := reginput;
        if LoopStack[LoopStackIdx] >= BracesMin then
        begin // Min alredy matched - we can work
          if scan^ = LOOP then
          begin
            // greedy way - first try to max deep of greed ;)
            if LoopStack[LoopStackIdx] < BracesMax then
            begin
              Inc(LoopStack[LoopStackIdx]);
              no := LoopStackIdx;
              Result := MatchPrim(opnd);
              LoopStackIdx := no;
              if Result then
                EXIT;
              reginput := save;
            end;
            Dec(LoopStackIdx); // Fail. May be we are too greedy? ;)
            Result := MatchPrim(Next);
            if not Result then
              reginput := save;
            EXIT;
          end
          else
          begin
            // non-greedy - try just now
            Result := MatchPrim(Next);
            if Result then
              EXIT
            else
              reginput := save; // failed - move next and try again
            if LoopStack[LoopStackIdx] < BracesMax then
            begin
              Inc(LoopStack[LoopStackIdx]);
              no := LoopStackIdx;
              Result := MatchPrim(opnd);
              LoopStackIdx := no;
              if Result then
                EXIT;
              reginput := save;
            end;
            Dec(LoopStackIdx); // Failed - back up
            EXIT;
          end;
        end
        else
        begin // first match a min_cnt times
          Inc(LoopStack[LoopStackIdx]);
          no := LoopStackIdx;
          Result := MatchPrim(opnd);
          LoopStackIdx := no;
          if Result then
            EXIT;
          Dec(LoopStack[LoopStackIdx]);
          reginput := save;
          EXIT;
        end;
      end;

      STAR, PLUS, BRACES, STARNG, PLUSNG, BRACESNG:
      begin
        // Lookahead to avoid useless match attempts when we know
        // what character comes next.
        nextch := #0;
        if Next^ = EXACTLY then
          nextch := (Next + REOpSz + RENextOffSz)^;
        BracesMax := MaxInt; // infinite loop for * and + //###0.92
        if (scan^ = STAR) or (scan^ = STARNG) then
          BracesMin := 0  // STAR
        else if (scan^ = PLUS) or (scan^ = PLUSNG) then
          BracesMin := 1 // PLUS
        else
        begin // BRACES
          BracesMin := PREBracesArg(scan + REOpSz + RENextOffSz)^;
          BracesMax := PREBracesArg(scan + REOpSz + RENextOffSz + REBracesArgSz)^;
        end;
        save := reginput;
        opnd := scan + REOpSz + RENextOffSz;
        if (scan^ = BRACES) or (scan^ = BRACESNG) then
          Inc(opnd, 2 * REBracesArgSz);

        if (scan^ = PLUSNG) or (scan^ = STARNG) or (scan^ = BRACESNG) then
        begin
          // non-greedy mode
          BracesMax := regrepeat(opnd, BracesMax); // don't repeat more than BracesMax
          // Now we know real Max limit to move forward (for recursion 'back up')
          // In some cases it can be faster to check only Min positions first,
          // but after that we have to check every position separtely instead
          // of fast scannig in loop.
          no := BracesMin;
          while no <= BracesMax do
          begin
            reginput := save + no;
            // If it could work, try it.
            if (nextch = #0) or (reginput^ = nextch) then
            begin

              System.Move(LoopStack, SavedLoopStack, SizeOf(LoopStack)); //###0.925
              SavedLoopStackIdx := LoopStackIdx;

              if MatchPrim(Next) then
              begin
                Result := True;
                EXIT;
              end;

              System.Move(SavedLoopStack, LoopStack, SizeOf(LoopStack));
              LoopStackIdx := SavedLoopStackIdx;

            end;
            Inc(no); // Couldn't or didn't - move forward.
          end; { of while}
          EXIT;
        end
        else
        begin // greedy mode
          no := regrepeat(opnd, BracesMax); // don't repeat more than max_cnt
          while no >= BracesMin do
          begin
            // If it could work, try it.
            if (nextch = #0) or (reginput^ = nextch) then
            begin

              System.Move(LoopStack, SavedLoopStack, SizeOf(LoopStack)); //###0.925
              SavedLoopStackIdx := LoopStackIdx;

              if MatchPrim(Next) then
              begin
                Result := True;
                EXIT;
              end;

              System.Move(SavedLoopStack, LoopStack, SizeOf(LoopStack));
              LoopStackIdx := SavedLoopStackIdx;

            end;
            Dec(no); // Couldn't or didn't - back up.
            reginput := save + no;
          end; { of while}
          EXIT;
        end;
      end;
      EEND:
      begin
        Result := True;  // Success!
        EXIT;
      end;
      else
      begin
        Error(reeMatchPrimMemoryCorruption);
        EXIT;
      end;
    end;
    scan := Next;
  end;

  // We get here only if there's trouble -- normally "case EEND" is the
  // terminating point.
  Error(reeMatchPrimCorruptedPointers);
end;

function TRegExpr.Exec(const AInputString: RegExprString): boolean;
begin
  InputString := AInputString;
  Result := ExecPrim(1);
end;

function TRegExpr.Exec(AOffset: integer): boolean;
begin
  Result := ExecPrim(AOffset);
end;

function TRegExpr.ExecPos(AOffset: integer = 1): boolean;
begin
  Result := ExecPrim(AOffset);
end;


function TRegExpr.ExecPrim(AOffset: integer): boolean;

  procedure ClearMatchs;
  // Clears matchs array
  var
    i: integer;
  begin
    for i := 0 to NSUBEXP - 1 do
    begin
      startp[i] := nil;
      endp[i] := nil;
    end;
  end;

  function RegMatch(str: PRegExprChar): boolean;
    // try match at specific point
  begin
    //###0.949 removed clearing of start\endp
    reginput := str;
    Result := MatchPrim(programm + REOpSz);
    if Result then
    begin
      startp[0] := str;
      endp[0] := reginput;
    end;
  end;

var
  s: PRegExprChar;
  StartPtr: PRegExprChar;
  InputLen: integer;
begin
  Result := False; // Be paranoid...

  ClearMatchs; //###0.949
  // ensure that Match cleared either if optimization tricks or some error
  // will lead to leaving ExecPrim without actual search. That is
  // importent for ExecNext logic and so on.

  if not IsProgrammOk //###0.929
  then
    EXIT;

  // Check InputString presence
  if not Assigned(fInputString) then
  begin
    Error(reeNoInpitStringSpecified);
    EXIT;
  end;

  InputLen := length(fInputString);

  //Check that the start position is not negative
  if AOffset < 1 then
  begin
    Error(reeOffsetMustBeGreaterThen0);
    EXIT;
  end;
  // Check that the start position is not longer than the line
  // If so then exit with nothing found
  if AOffset > (InputLen + 1) // for matching empty string after last char.
  then
    EXIT;

  StartPtr := fInputString + AOffset - 1;

  // If there is a "must appear" string, look for it.
  if regmust <> nil then
  begin
    s := StartPtr;
    repeat
      s := StrScan(s, regmust[0]);
      if s <> nil then
      begin
        if StrLComp(s, regmust, regmlen) = 0 then
          BREAK; // Found it.
        Inc(s);
      end;
    until s = nil;
    if s = nil // Not present.
    then
      EXIT;
  end;

  // Mark beginning of line for ^ .
  fInputStart := fInputString;

  // Pointer to end of input stream - for
  // pascal-style string processing (may include #0)
  fInputEnd := fInputString + InputLen;

  // no loops started
  LoopStackIdx := 0; //###0.925

  // Simplest case:  anchored match need be tried only once.
  if reganch <> #0 then
  begin
    Result := RegMatch(StartPtr);
    EXIT;
  end;

  // Messy cases:  unanchored match.
  s := StartPtr;
  if regstart <> #0 then // We know what char it must start with.
    repeat
      s := StrScan(s, regstart);
      if s <> nil then
      begin
        Result := RegMatch(s);
        if Result then
          EXIT
        else
          ClearMatchs; //###0.949
        Inc(s);
      end;
    until s = nil
  else
  begin // We don't - general case.
    repeat //###0.948

      Result := RegMatch(s);

      if Result or (s^ = #0) // Exit on a match or after testing the end-of-string.
      then
        EXIT
      else
        ClearMatchs; //###0.949
      Inc(s);
    until False;
  end;

end;

function TRegExpr.ExecNext: boolean;
var
  offset: integer;
begin
  Result := False;
  if not Assigned(startp[0]) or not Assigned(endp[0]) then
  begin
    Error(reeExecNextWithoutExec);
    EXIT;
  end;
  //  Offset := MatchPos [0] + MatchLen [0];
  //  if MatchLen [0] = 0
  Offset := endp[0] - fInputString + 1; //###0.929
  if endp[0] = startp[0] //###0.929
  then
    Inc(Offset); // prevent infinite looping if empty string match r.e.
  Result := ExecPrim(Offset);
end;

function TRegExpr.GetInputString: RegExprString;
begin
  if not Assigned(fInputString) then
  begin
    Error(reeGetInputStringWithoutInputString);
    EXIT;
  end;
  Result := fInputString;
end;

procedure TRegExpr.SetInputString(const AInputString: RegExprString);
var
  Len: integer;
  i: integer;
begin
  // clear Match* - before next Exec* call it's undefined
  for i := 0 to NSUBEXP - 1 do
  begin
    startp[i] := nil;
    endp[i] := nil;
  end;

  // need reallocation of input string buffer ?
  Len := length(AInputString);
  if Assigned(fInputString) and (Length(fInputString) <> Len) then
  begin
    FreeMem(fInputString);
    fInputString := nil;
  end;
  // buffer [re]allocation
  if not Assigned(fInputString) then
    GetMem(fInputString, (Len + 1) * SizeOf(REChar));

  // copy input string into buffer

  StrPCopy(fInputString, Copy(AInputString, 1, Len));

end;

procedure TRegExpr.SetLineSeparators(const AStr: RegExprString);
begin
  if AStr <> fLineSeparators then
  begin
    fLineSeparators := AStr;
    InvalidateProgramm;
  end;
end;

procedure TRegExpr.SetLinePairedSeparator(const AStr: RegExprString);
begin
  if length(AStr) = 2 then
  begin
    if AStr[1] = AStr[2] then
    begin
      // it's impossible for our 'one-point' checking to support
      // two chars separator for identical chars
      Error(reeBadLinePairedSeparator);
      EXIT;
    end;
    if not fLinePairedSeparatorAssigned or (AStr[1] <> fLinePairedSeparatorHead) or (AStr[2] <> fLinePairedSeparatorTail) then
    begin
      fLinePairedSeparatorAssigned := True;
      fLinePairedSeparatorHead := AStr[1];
      fLinePairedSeparatorTail := AStr[2];
      InvalidateProgramm;
    end;
  end
  else if length(AStr) = 0 then
  begin
    if fLinePairedSeparatorAssigned then
    begin
      fLinePairedSeparatorAssigned := False;
      InvalidateProgramm;
    end;
  end
  else
    Error(reeBadLinePairedSeparator);
end;

function TRegExpr.GetLinePairedSeparator: RegExprString;
begin
  if fLinePairedSeparatorAssigned then
  begin

    // Here is some UniCode 'magic'
    // If You do know better decision to concatenate
    // two WideChars, please, let me know!
    Result := fLinePairedSeparatorHead; //###0.947
    Result := Result + fLinePairedSeparatorTail;

  end
  else
    Result := '';
end;

function TRegExpr.Substitute(const ATemplate: RegExprString): RegExprString;
  // perform substitutions after a regexp match
  // completely rewritten in 0.929
var
  TemplateLen: integer;
  TemplateBeg, TemplateEnd: PRegExprChar;
  p, p0, ResultPtr: PRegExprChar;
  ResultLen: integer;
  n: integer;
  Ch: REChar;

  function ParseVarName(var APtr: PRegExprChar): integer;
    // extract name of variable (digits, may be enclosed with
    // curly braces) from APtr^, uses TemplateEnd !!!
  const
    Digits = ['0' .. '9'];
  var
    p: PRegExprChar;
    Delimited: boolean;
  begin
    Result := 0;
    p := APtr;
    Delimited := (p < TemplateEnd) and (p^ = '{');
    if Delimited then
      Inc(p); // skip left curly brace
    if (p < TemplateEnd) and (p^ = '&') then
      Inc(p) // this is '$&' or '${&}'
    else
      while (p < TemplateEnd) and (Ord(p^) < 256) and (char(p^) in Digits) do
      begin
        Result := Result * 10 + (Ord(p^) - Ord('0')); //###0.939
        Inc(p);
      end;
    if Delimited then
      if (p < TemplateEnd) and (p^ = '}') then
        Inc(p) // skip right curly brace
      else
        p := APtr; // isn't properly terminated
    if p = APtr then
      Result := -1; // no valid digits found or no right curly brace
    APtr := p;
  end;

begin
  // Check programm and input string
  if not IsProgrammOk then
    EXIT;
  if not Assigned(fInputString) then
  begin
    Error(reeNoInpitStringSpecified);
    EXIT;
  end;
  // Prepare for working
  TemplateLen := length(ATemplate);
  if TemplateLen = 0 then
  begin // prevent nil pointers
    Result := '';
    EXIT;
  end;
  TemplateBeg := pointer(ATemplate);
  TemplateEnd := TemplateBeg + TemplateLen;
  // Count result length for speed optimization.
  ResultLen := 0;
  p := TemplateBeg;
  while p < TemplateEnd do
  begin
    Ch := p^;
    Inc(p);
    if Ch = '$' then
      n := ParseVarName(p)
    else
      n := -1;
    if n >= 0 then
    begin
      if (n < NSUBEXP) and Assigned(startp[n]) and Assigned(endp[n]) then
        Inc(ResultLen, endp[n] - startp[n]);
    end
    else
    begin
      if (Ch = EscChar) and (p < TemplateEnd) then
        Inc(p); // quoted or special char followed
      Inc(ResultLen);
    end;
  end;
  // Get memory. We do it once and it significant speed up work !
  if ResultLen = 0 then
  begin
    Result := '';
    EXIT;
  end;
  SetString(Result, nil, ResultLen);
  // Fill Result
  ResultPtr := pointer(Result);
  p := TemplateBeg;
  while p < TemplateEnd do
  begin
    Ch := p^;
    Inc(p);
    if Ch = '$' then
      n := ParseVarName(p)
    else
      n := -1;
    if n >= 0 then
    begin
      p0 := startp[n];
      if (n < NSUBEXP) and Assigned(p0) and Assigned(endp[n]) then
        while p0 < endp[n] do
        begin
          ResultPtr^ := p0^;
          Inc(ResultPtr);
          Inc(p0);
        end;
    end
    else
    begin
      if (Ch = EscChar) and (p < TemplateEnd) then
      begin // quoted or special char followed
        Ch := p^;
        Inc(p);
      end;
      ResultPtr^ := Ch;
      Inc(ResultPtr);
    end;
  end;
end;

procedure TRegExpr.Split(AInputStr: RegExprString; APieces: TStrings);
var
  PrevPos: integer;
begin
  PrevPos := 1;
  if Exec(AInputStr) then
    repeat
      APieces.Add(System.Copy(AInputStr, PrevPos, MatchPos[0] - PrevPos));
      PrevPos := MatchPos[0] + MatchLen[0];
    until not ExecNext;
  APieces.Add(System.Copy(AInputStr, PrevPos, MaxInt)); // Tail
end;

function TRegExpr.Replace(AInputStr: RegExprString; const AReplaceStr: RegExprString; AUseSubstitution: boolean = False): RegExprString;
var
  PrevPos: integer;
begin
  Result := '';
  PrevPos := 1;
  if Exec(AInputStr) then
    repeat
      Result := Result + System.Copy(AInputStr, PrevPos, MatchPos[0] - PrevPos);
      if AUseSubstitution //###0.946
      then
        Result := Result + Substitute(AReplaceStr)
      else
        Result := Result + AReplaceStr;
      PrevPos := MatchPos[0] + MatchLen[0];
    until not ExecNext;
  Result := Result + System.Copy(AInputStr, PrevPos, MaxInt); // Tail
end;

function TRegExpr.ReplaceEx(AInputStr: RegExprString; AReplaceFunc: TRegExprReplaceFunction): RegExprString;
var
  PrevPos: integer;
begin
  Result := '';
  PrevPos := 1;
  if Exec(AInputStr) then
    repeat
      Result := Result + System.Copy(AInputStr, PrevPos, MatchPos[0] - PrevPos) + AReplaceFunc(Self);
      PrevPos := MatchPos[0] + MatchLen[0];
    until not ExecNext;
  Result := Result + System.Copy(AInputStr, PrevPos, MaxInt);
end;

function TRegExpr.Replace(AInputStr: RegExprString; AReplaceFunc: TRegExprReplaceFunction): RegExprString;
begin
  ReplaceEx(AInputStr, AReplaceFunc);
end;

//====================== Debug section =======================

function TRegExpr.DumpOp(op: TREOp): RegExprString;

begin
  case op of
    BOL: Result := 'BOL';
    EOL: Result := 'EOL';
    BOLML: Result := 'BOLML';
    EOLML: Result := 'EOLML';
    BOUND: Result := 'BOUND'; //###0.943
    NOTBOUND: Result := 'NOTBOUND'; //###0.943
    ANY: Result := 'ANY';
    ANYML: Result := 'ANYML'; //###0.941
    ANYLETTER: Result := 'ANYLETTER';
    NOTLETTER: Result := 'NOTLETTER';
    ANYDIGIT: Result := 'ANYDIGIT';
    NOTDIGIT: Result := 'NOTDIGIT';
    ANYSPACE: Result := 'ANYSPACE';
    NOTSPACE: Result := 'NOTSPACE';
    ANYOF: Result := 'ANYOF';
    ANYBUT: Result := 'ANYBUT';
    ANYOFCI: Result := 'ANYOF/CI';
    ANYBUTCI: Result := 'ANYBUT/CI';
    BRANCH: Result := 'BRANCH';
    EXACTLY: Result := 'EXACTLY';
    EXACTLYCI: Result := 'EXACTLY/CI';
    NOTHING: Result := 'NOTHING';
    COMMENT: Result := 'COMMENT';
    BACK: Result := 'BACK';
    EEND: Result := 'END';
    BSUBEXP: Result := 'BSUBEXP';
    BSUBEXPCI: Result := 'BSUBEXP/CI';
    Succ(Open) .. TREOp(Ord(Open) + NSUBEXP - 1): //###0.929
      Result := Format('OPEN[%d]', [Ord(op) - Ord(Open)]);
    Succ(Close) .. TREOp(Ord(Close) + NSUBEXP - 1): //###0.929
      Result := Format('CLOSE[%d]', [Ord(op) - Ord(Close)]);
    STAR: Result := 'STAR';
    PLUS: Result := 'PLUS';
    BRACES: Result := 'BRACES';
    LOOPENTRY: Result := 'LOOPENTRY'; //###0.925
    LOOP: Result := 'LOOP'; //###0.925
    LOOPNG: Result := 'LOOPNG'; //###0.940
    ANYOFTINYSET: Result := 'ANYOFTINYSET';
    ANYBUTTINYSET: Result := 'ANYBUTTINYSET';

    STARNG: Result := 'STARNG'; //###0.940
    PLUSNG: Result := 'PLUSNG'; //###0.940
    BRACESNG: Result := 'BRACESNG'; //###0.940
    else
      Error(reeDumpCorruptedOpcode);
  end;
  Result := ':' + Result;
end;


function TRegExpr.Dump: RegExprString;

var
  s: PRegExprChar;
  op: TREOp;
  Next: PRegExprChar;
  i: integer;
  Diff: integer;
begin
  if not IsProgrammOk then
    EXIT;

  op := EXACTLY;
  Result := '';
  s := programm + REOpSz;
  while op <> EEND do
  begin
    op := s^;
    Result := Result + Format('%2d%s', [s - programm, DumpOp(s^)]);
    Next := regnext(s);
    if Next = nil then
      Result := Result + ' (0)'
    else
    begin
      if Next > s then
        Diff := Next - s
      else
        Diff := -(s - Next);
      Result := Result + Format(' (%d) ', [(s - programm) + Diff]);
    end;
    Inc(s, REOpSz + RENextOffSz);
    if (op = ANYOF) or (op = ANYOFCI) or (op = ANYBUT) or (op = ANYBUTCI) or (op = EXACTLY) or (op = EXACTLYCI) then
    begin
      // Literal string, where present.
      while s^ <> #0 do
      begin
        Result := Result + s^;
        Inc(s);
      end;
      Inc(s);
    end;
    if (op = ANYOFTINYSET) or (op = ANYBUTTINYSET) then
    begin
      for i := 1 to TinySetLen do
      begin
        Result := Result + s^;
        Inc(s);
      end;
    end;
    if (op = BSUBEXP) or (op = BSUBEXPCI) then
    begin
      Result := Result + ' \' + IntToStr(Ord(s^));
      Inc(s);
    end;

    if (op = BRACES) or (op = BRACESNG) then
    begin

      Result := Result + Format('{%d,%d}', [PREBracesArg(s)^, PREBracesArg(s + REBracesArgSz)^]);
      Inc(s, REBracesArgSz * 2);
    end;

    if (op = LOOP) or (op = LOOPNG) then
    begin
      Result := Result + Format(' -> (%d) {%d,%d}', [(s - programm - (REOpSz + RENextOffSz)) +
        PRENextOff(s + 2 * REBracesArgSz)^, PREBracesArg(s)^, PREBracesArg(s + REBracesArgSz)^]);
      Inc(s, 2 * REBracesArgSz + RENextOffSz);
    end;

    Result := Result + #$d#$a;
  end;


  if regstart <> #0 then
    Result := Result + 'start ' + regstart;
  if reganch <> #0 then
    Result := Result + 'anchored ';
  if regmust <> nil then
    Result := Result + 'must have ' + regmust;

  Result := Result + #$d#$a;
end;


procedure TRegExpr.Error(AErrorID: integer);
var
  xe: ERegExpr;
begin
  fLastError := AErrorID;
  if AErrorID < 1000 then
    xe := ERegExpr.Create(ErrorMsg(AErrorID) + ' (pos ' + IntToStr(CompilerErrorPos) + ')')
  else
    xe := ERegExpr.Create(ErrorMsg(AErrorID));
  xe.ErrorCode := AErrorID;
  xe.CompilerErrorPos := CompilerErrorPos;
  raise xe;
end;

initialization
  RegExprInvertCaseFunction := TRegExpr.InvertCaseFunction;

end.
