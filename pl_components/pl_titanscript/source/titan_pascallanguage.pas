{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit Titan_PascalLanguage;

interface

uses
  SysUtils,
  TypInfo,
  Classes,
  BASE_Engine,
  Titan_PascalScanner,
  Titan_PascalRegFunctions,
  TitanScripter;

type
  TTitanPascal = class(TTitanLanguage)
  private
    fFileExt: String;
    fLanguageName: String;
  protected
    function GetFileExt: String; override;
    procedure SetFileExt(const Value: String); override;
    function GetLanguageName: String; override;
    procedure SetLanguageName(const Value: String); override;
    function GeTTitanParserClass: TTitanParserClass; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property FileExt: String read fFileExt write SetFileExt;
    property LanguageName: String read fLanguageName write SetLanguageName;
    property InitArrays: Boolean read fInitArrays write fInitArrays;
  end;

implementation

constructor TTitanPascal.Create(AOwner: TComponent);
begin
  fFileExt := 'pp';
  fLanguageName := 'TitanPascal';
  CallConvention := ccRegister;

  inherited;

  NamespaceAsModule := true;
  JavaScriptOperators := false;
  DeclareVariables := true;
  fInitArrays := true;
end;

function TTitanPascal.GetFileExt: String;
begin
  result := fFileExt;
end;

procedure TTitanPascal.SetFileExt(const Value: String);
begin
  fFileExt := Value;
  inherited;
end;

function TTitanPascal.GetLanguageName: String;
begin
  result := fLanguageName;
end;

procedure TTitanPascal.SetLanguageName(const Value: String);
begin
  fLanguageName := Value;
  inherited;
end;

function TTitanPascal.GeTTitanParserClass: TTitanParserClass;
begin
  result := TTitanPascalParser;
end;

end.
