{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit Titan_RTTI;

interface

uses
  Variants,
  SysUtils,
  Classes,
  BASE_CONSTS,
  BASE_Engine, TitanScripter;

type
  TTitanPropKind = (pkAll, pkPublished, pkIndexed, pkStatic, pkImported);
  TTitanPropKindSet = set of TTitanPropKind;

procedure PuTTitanObjectProperty(const O: variant; const PropName: string; const Value: variant);
function GeTTitanObjectProperty(const O: variant; const PropName: string): variant;
function GeTTitanObjectPropertyAsString(const O: variant; const PropName: string): string;

function GeTTitanObjectPropertyCount(const O: variant): integer;
function GeTTitanObjectPropertyNameByIndex(const O: variant; Index: integer): string;
function GeTTitanObjectPropertyByIndex(const O: variant; Index: integer): variant;
procedure PuTTitanObjectPropertyByIndex(const O: variant; Index: integer; const Value: variant);
function IsPaxObject(const V: variant): boolean;
function GeTTitanObjectClassName(const O: variant): string;
function GetArrayValue(const A: variant; Indexes: array of integer): variant;
procedure PutArrayValue(const A: variant; const Indexes: array of integer; const Value: variant);
function GetArrayHighBound(const A: variant; Dim: integer): integer;
function GetArrayLowBound(const A: variant; Dim: integer): integer;
procedure GeTTitanPropNames(const O: variant; L: TStringList; PropKinds: TTitanPropKindSet = [pkAll]);
function CreateScriptObject(Scripter: TTitanScripter; const ClassName: string; DelphiInstance: TObject = nil): variant;
function CreateScriptObjectEx(Scripter: TTitanScripter; const ClassName: string; const Params: array of const): variant;
function FindScriptObject(Scripter: TTitanScripter; DelphiInstance: TObject): variant;

implementation

function IsPaxObject(const V: variant): boolean;
begin
  Result := IsObject(V);
end;

procedure PuTTitanObjectProperty(const O: variant; const PropName: string; const Value: variant);
var
  SO: TTitanScriptObject;
  NameIndex: integer;
begin
  SO := VariantToScriptObject(O);
  NameIndex := CreateNameIndex(PropName, SO.Scripter);
  SO.PutProperty(NameIndex, Value, 0);
end;

function GeTTitanObjectProperty(const O: variant; const PropName: string): variant;
var
  SO: TTitanScriptObject;
  NameIndex: integer;
begin
  SO := VariantToScriptObject(O);
  NameIndex := CreateNameIndex(PropName, SO.Scripter);
  Result := SO.GetProperty(NameIndex, 0);
end;

procedure PuTTitanObjectPropertyByIndex(const O: variant; Index: integer; const Value: variant);
var
  SO: TTitanScriptObject;
  P: TTitanProperty;
begin
  SO := VariantToScriptObject(O);
  P := SO.PropertyList.Properties[Index];
  P.Value[0] := Value;
end;

function GeTTitanObjectPropertyByIndex(const O: variant; Index: integer): variant;
var
  SO: TTitanScriptObject;
  P: TTitanProperty;
begin
  SO := VariantToScriptObject(O);
  P := SO.PropertyList.Properties[Index];
  Result := P.Value[0];
end;

function GeTTitanObjectPropertyNameByIndex(const O: variant; Index: integer): string;
var
  SO: TTitanScriptObject;
begin
  SO := VariantToScriptObject(O);
  Result := SO.PropertyList.Names[Index];
end;

function GeTTitanObjectPropertyCount(const O: variant): integer;
var
  SO: TTitanScriptObject;
begin
  SO := VariantToScriptObject(O);
  Result := SO.PropertyList.Count;
end;

function GeTTitanObjectClassName(const O: variant): string;
var
  SO: TTitanScriptObject;
begin
  SO := VariantToScriptObject(O);
  Result := SO.ClassRec.Name;
end;

function GetArrayValue(const A: variant; Indexes: array of integer): variant;
var
  P: Pointer;
  SO: TTitanScriptObject;
  PaxArray: TTitanArray;
begin
  if IsVBArray(A) then
  begin
    P := ArrayGet(@A, Indexes);
    Result := variant(P^);
  end
  else
  begin
    SO := VariantToScriptObject(A);
    PaxArray := TTitanArray(SO.ExtraInstance);
    Result := PaxArray.Get(Indexes);
  end;
end;

procedure PutArrayValue(const A: variant; const Indexes: array of integer; const Value: variant);
var
  SO: TTitanScriptObject;
  PaxArray: TTitanArray;
begin
  if IsVBArray(A) then
    ArrayPut(@A, Indexes, Value)
  else
  begin
    SO := VariantToScriptObject(A);
    PaxArray := TTitanArray(SO.ExtraInstance);
    PaxArray.Put(Indexes, Value);
  end;
end;

function GetArrayHighBound(const A: variant; Dim: integer): integer;
var
  SO: TTitanScriptObject;
  PaxArray: TTitanArray;
begin
  if IsVBArray(A) then
    Result := GetArrayHighBound(A, Dim)
  else
  begin
    SO := VariantToScriptObject(A);
    PaxArray := TTitanArray(SO.ExtraInstance);
    Result := PaxArray.HighBound(dim);
  end;
end;

function GetArrayLowBound(const A: variant; Dim: integer): integer;
begin
  if IsVBArray(A) then
    Result := GetArrayLowBound(A, Dim)
  else
    Result := 0;
end;

procedure GeTTitanPropNames(const O: variant; L: TStringList; PropKinds: TTitanPropKindSet = [pkAll]);
var
  SO: TTitanScriptObject;
  I, K, Kind: integer;
  S: string;
  MemberRec: TTitanMemberRec;
  ok: boolean;
begin
  SO := VariantToScriptObject(O);
  K := SO.PropertyList.Count;
  for I := 0 to K - 1 do
  begin
    Kind := SO.PropertyList.Properties[I].GetKind;
    if Kind in [KindProp] then
    begin
      S := SO.PropertyList.Names[I];
      MemberRec := SO.PropertyList.Properties[I].MemberRec;
      ok := Pos('ON', UpperCase(S)) <> 1;

      if ok then
      begin
        if pkIndexed in PropKinds then
          ok := MemberRec.NParams > 0
        else
          ok := MemberRec.NParams = 0;
      end;

      if ok then
      begin
        if pkImported in PropKinds then
          ok := MemberRec.IsImported
        else
          ok := not MemberRec.IsImported;
      end;

      if ok then
      begin
        if pkPublished in PropKinds then
          ok := MemberRec.IsPublished
        else
          ok := not MemberRec.IsPublished;
      end;

      if ok then
      begin
        if pkStatic in PropKinds then
          ok := MemberRec.IsStatic
        else
          ok := not MemberRec.IsStatic;
      end;

      if pkAll in PropKinds then
        ok := True;

      if ok then
        L.Add(S);
    end;
  end;
end;

function GeTTitanObjectPropertyAsString(const O: variant; const PropName: string): string;
var
  SO: TTitanScriptObject;
  V: variant;
begin
  SO := VariantToScriptObject(O);
  V := GeTTitanObjectProperty(O, PropName);
  Result := ToStr(SO.Scripter, V);
end;

function FindScriptObject(Scripter: TTitanScripter; DelphiInstance: TObject): variant;
var
  SO: TTitanScriptObject;
begin
  SO := Scripter.fScripter.ScriptObjectList.FindScriptObject(DelphiInstance);
  if SO <> nil then
    Result := ScriptObjectToVariant(SO)
  else
    Result := Undefined;
end;

function CreateScriptObject(Scripter: TTitanScripter; const ClassName: string; DelphiInstance: TObject = nil): variant;
var
  ClassRec: TTitanClassRec;
  SO: TTitanScriptObject;
  ID: integer;
begin
  if Pos('.', ClassName) <= 0 then
    ClassRec := Scripter.fScripter.ClassList.FindClassByName(ClassName)
  else
  begin
    ID := scripter.GetMemberID(ClassName);
    ClassRec := Scripter.fScripter.ClassList.FindClass(ID);
  end;

  if ClassRec <> nil then
  begin
    SO := ClassRec.CreateScriptObject();
    SO.Instance := DelphiInstance;
    Result := ScriptObjectToVariant(SO);
  end
  else
    raise TTitanScriptFailure.Create(Format(errClassIsNotFound, [ClassName]));
end;

function CreateScriptObjectEx(Scripter: TTitanScripter; const ClassName: string; const Params: array of const): variant;
var
  ClassRec: TTitanClassRec;
  SubID, ID: integer;
begin
  Result := CreateScriptObject(Scripter, ClassName);

  if Pos('.', ClassName) <= 0 then
    ClassRec := Scripter.fScripter.ClassList.FindClassByName(ClassName)
  else
  begin
    ID := scripter.GetMemberID(ClassName);
    ClassRec := Scripter.fScripter.ClassList.FindClass(ID);
  end;

  if ClassRec <> nil then
  begin
    SubID := ClassRec.GetConstructorIDEx(Params);
    if SubID <> 0 then
      Scripter.fScripter.CallMethod(SubID, Result, Params, False)
    else
      raise TTitanScriptFailure.Create(Format(errConstructorIsNotFound, [ClassName]));
  end
  else
    raise TTitanScriptFailure.Create(Format(errClassIsNotFound, [ClassName]));
end;

end.
