{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit titan_frm;

interface

uses
  SysUtils,
  TypInfo,
  Classes,
  Graphics,
  BASE_Engine,
  TitanScripter;

type
  TTitanFRMConverter = class(TComponent)
  private
    fUsedUnits: TStrings;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Parse(const FRMFileName: string; Output: TStrings; AsUnit: boolean = True); overload;
    procedure Parse(const FRMFileName: string; Output: TStrings; const PaxLanguage: string; AsUnit: boolean = True); overload;
    procedure ParseText(const Text: string; Output: TStrings; const UnitName: string = '';
                        const PaxLanguage: string = 'TitanPascal');
    property UsedUnits: TStrings read fUsedUnits write fUsedUnits;
  end;

implementation

constructor TTitanFRMConverter.Create(AOwner: TComponent);
begin
  inherited;
  fUsedUnits := TStringList.Create;
end;

destructor TTitanFRMConverter.Destroy;
begin
  fUsedUnits.Free;
  inherited;
end;

procedure TTitanFRMConverter.Parse(const FRMFileName: string; Output: TStrings; AsUnit: boolean = True);
begin
  ConvertFRMFile(FRMFileName, UsedUnits, Output, AsUnit);
end;

procedure TTitanFRMConverter.Parse(const FRMFileName: string; Output: TStrings; const PaxLanguage: string; AsUnit: boolean = True);
begin
  ConvertFRMFile(FRMFileName, UsedUnits, Output, AsUnit, nil, PaxLanguage);
end;

procedure TTitanFRMConverter.ParseText(const Text: string; Output: TStrings; const UnitName: string = '';
  const PaxLanguage: string = 'TitanPascal');
begin
  ConvFRMStringtoScript(Text, UsedUnits, Output, UnitName <> '', UnitName, nil, PaxLanguage);
end;

procedure _AssignBmp(X: TObject; S: string);
begin
  SaveStr(S, 'temp.bmp');
  if X.InheritsFrom(TGraphic) then
    TGraphic(X).LoadFromFile('temp.bmp');
end;

initialization

  RegisterRoutine('procedure _AssignBmp(X: TObject; S: String);', @_AssignBmp);


end.
