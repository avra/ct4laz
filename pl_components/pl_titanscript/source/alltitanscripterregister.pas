{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

{$I TitanScript.inc}

unit AllTitanScripterRegister;

interface

uses
  Classes,lresources,
  TitanScripterBaseEngine,
  TitanScripter,
  Titan_PascalLanguage,
  titan_frm,
  titan_runtimeapiexplorer;

procedure Register;

Implementation

  {$R alltitanscripterregister.res}

procedure Register;
begin
  RegisterComponents('TitanScripter', [TBaseScriptEngine,
                                       TTitanScripter,
                                       TTitanPascal,
                                       TTitanFRMConverter,
                                       TTitanRunTimeAPIExplorer]);

end;

end.
