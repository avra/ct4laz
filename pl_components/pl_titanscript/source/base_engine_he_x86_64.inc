{%mainunit base_engine.pas}

procedure TTitanEventHandler.HandleEvent; assembler;
const
  LocalFrameSize = 40;

{$asmmode intel}
asm
  mov dword ptr Self._EAX, rax
  mov dword ptr Self._EDX, rdx
  mov dword ptr Self._ECX, rcx
  mov dword ptr Self._P, rsp

  push rbp
  mov rbp, rsp

  sub rsp, LocalFrameSize

  mov [rbp-12], rcx
  mov [rbp- 8], rdx
  mov [rbp- 4], rax

  push rax
  call Invoke
  pop rax

  mov rcx, RetSize // self.RetSize

  mov rsp, rbp
  pop rbp

  cmp rcx, 0

  jnz @@Ret4
  ret

@@Ret4:
  cmp rcx, 4
  jnz @@Ret8
  ret 4

@@Ret8:
  cmp rcx, 8
  jnz @@Ret12
  ret 8

@@Ret12:
  cmp rcx, $0c
  jnz @@Ret16
  ret $0c

@@Ret16:
  cmp rcx, $10
  jnz @@Ret20
  ret $10

@@Ret20:
  cmp rcx, $14
  jnz @@Ret24
  ret $14

@@Ret24:
  cmp rcx, $18
  jnz @@Ret28
  ret $18

@@Ret28:
  ret $1C
end;
