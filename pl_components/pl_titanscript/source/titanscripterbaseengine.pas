{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit TitanScripterBaseEngine;

interface

uses
      SysUtils, Classes,Dialogs,
      BASE_Engine,
      TitanScripter, Titan_PascalLanguage,
      TitanScripterBaseEngineMsg;


const ScriptEngineVersion=2000;  //to set a Version

type

TEventsOverrideMode = (omReplace, omBefore, omAfter);

TExecuteLineEvent = procedure (Sender : TObject; const LineNo : integer) of object;
TStopRunAtLineEvent = procedure (Sender : TObject; const LineNo,LastPos : integer) of object;
TScriptErrorHandler = procedure (Sender : TObject; Const aErrorType:integer) of object;

TScriptType=(SUnit,SBinary,sExtension);
TScriptStatus=(ssEmpty,ssReady,ssRunOK,ssSyntaxCheckError,ssCompilerError,ssRunTimeError);
TScriptRunOrder=(soNone,soCheckSyntax,soCompile,soRun,soTerminate,soRunToLine,soRunToNextLine,soTraceInto,soStepOver,
                 soRunFirstScript,soRunExtension,soTimeEvent);


TBaseScriptEngine = class(TComponent)
  private
  protected
   fScripter:TTitanScripter;
   fPascalLanguage:TTitanPascal;
   //................
   FErrorType:integer;  //1:fCompiler error 2: Exception error from fProgram

   fErrorDescription:String;
   fErrorModuleName:String;
   fErrorTextPos:Integer;
   fErrorPos:Integer;
   fErrorLine:Integer;
   fErrorMethodId:Integer;

   fOnExecuteLine :TExecuteLineEvent;
   fOnStopRunAtLine:TStopRunAtLineEvent;
   fOnError : TScriptErrorHandler;
   //............................

   FScriptType:TScriptType;
   fStatus:TScriptStatus;
   fRunOrder:TScriptRunOrder;
   fLastRunLine:integer;
   FFirstScript:String;
   FScriptText:String;
   FOnBeforeRunScript : TNotifyEvent;
   FOnAfterRunScript : TNotifyEvent;
   FOnAfterCompile : TNotifyEvent;
   FOnStopScript : TNotifyEvent;

   procedure OnThreadExecute(Sender: TObject); // Main Thread procedure..
   procedure DoShowErrorWithDialog;
   procedure DoShowErrorExtension;
   procedure DoShowErrorTimeEvent;
   procedure DoOnError(Sender:TTitanScripter);
   Procedure _InitCompiler;

   Function  GetEventsOverrideMode:TEventsOverrideMode;
   Procedure SetEventsOverrideMode(const val:TEventsOverrideMode);

   procedure AddObjectToScript(Instance : TObject; const Name : string; Global : boolean);
   Procedure CommonRegisterVars;virtual;      

  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   
   Function  IsScriptFileFormat(Filename:string):boolean;virtual;

   Function  LoadScriptFromFile(const aFilename:string):boolean;
   Function  LoadScriptFromStream(aStream:TStream):boolean;
   

   Property  Scripter:TTitanScripter read fScripter;
   Property  ScriptType:TScriptType read FScriptType write FScriptType;
   Property  Status:TScriptStatus  read FStatus;
   Property  LastRunOrder:TScriptRunOrder  read fRunOrder;    

   procedure EventsDeleteAll;

   procedure  SyntaxCheck;
   procedure  Compile;
   procedure  Run;
   Procedure  RunToLine(const aline:Integer);
   Procedure  RunToNextLine;
   procedure  RunFirstScript;
   Procedure  RunExtension;
   Procedure  RunTimeEvent;
   Procedure  Stop;
   procedure  Clear;
   procedure  TraceInto;
   procedure  StepOver;
   //
   Property ScriptText:String read FScriptText write FScriptText;
   Property EventsOverrideMode:TEventsOverrideMode read GetEventsOverrideMode write SetEventsOverrideMode;

   property ErrorType:integer read fErrorType;
   property ErrorDescription: String read fErrorDescription;
   property ErrorModuleName: String read fErrorModuleName;
   property ErrorTextPos: Integer read fErrorTextPos;
   property ErrorPos: Integer read fErrorPos;
   property ErrorLine: Integer read fErrorLine;
   property ErrorMethodId: Integer read fErrorMethodId;

   property  OnExecuteLine : TExecuteLineEvent read fOnExecuteLine write fOnExecuteLine;
   property  OnStopRunAtLine: TStopRunAtLineEvent read fOnStopRunAtLine write fOnStopRunAtLine;
   property  OnError : TScriptErrorHandler read fOnError write fOnError;

  published
    Property FirstScript:String read FFirstScript write FFirstScript;
    property OnBeforeRunScript : TNotifyEvent read FOnBeforeRunScript write FOnBeforeRunScript;
    property OnAfterRunScript : TNotifyEvent read FOnAfterRunScript write FOnAfterRunScript;
    property OnAfterCompile : TNotifyEvent read FOnAfterCompile write FOnAfterCompile;
    property OnStopScript : TNotifyEvent read FOnStopScript write FOnStopScript;

  end;


implementation
  uses  {STEForScripter,STEForTimeEventEngine,}forms;

//....................................................
constructor TBaseScriptEngine.Create(AOwner: TComponent);
 begin
 inherited Create(AOwner);
  Clear;  //This create  FScripter    
 end;

destructor TBaseScriptEngine.Destroy;
 begin  

  if fScripter<>nil then fScripter.Free;
  if fPascalLanguage<>nil then fPascalLanguage.Free;

  inherited Destroy;
 end;

{  We use this function from load project etc..
  Must Free fScripter for NO errors 14-6-2008  }
procedure TBaseScriptEngine.Clear;
 begin

  if fScripter<>nil then fScripter.Free;
  if fPascalLanguage<>nil then fPascalLanguage.Free;

  fScripter:=TTitanScripter.Create(self);
  fScripter.Name:='Scripter1';
  fScripter.OnShowError:=DoOnError;

  fPascalLanguage:=TTitanPascal.Create(self);
  fPascalLanguage.Name:='Pascal1';
  fPascalLanguage.Backslash:=false; //To fix "\" problem
  //..........
  fPascalLanguage.DeclareVariables := true; //testing
  fPascalLanguage.ZeroBasedStrings := true; //testing
  //..........

  fScriptType:=sunit;
  FStatus:=ssEmpty;
  fRunOrder:=soRun;    
  FScriptText:='';
  FFirstScript:='';

 end;

Function  TBaseScriptEngine.GetEventsOverrideMode:TEventsOverrideMode;
 var i:integer;
 begin
   i:=integer(fScripter.OverrideHandlerMode);
   result:=TEventsOverrideMode(i);
 end;

Procedure TBaseScriptEngine.SetEventsOverrideMode(const val:TEventsOverrideMode);
 var i:integer;
 begin
   i:=integer(val);
   fScripter.OverrideHandlerMode:=TTitanOverrideHandlerMode(i);
 end;

procedure  TBaseScriptEngine.DoShowErrorWithDialog;
begin
   MessageDlg('Project First Scenario Problem!'+#13#10+#13#10+
              'During the execution of Project First Scenario'+#13#10+
              'the Scenario Engine Reports'+#13#10+
              'an Error: '+fErrorDescription+#13#10+
              'At Line:'+IntTostr(fErrorLine)+#13#10+
              'Please Edit project first scenario Source'+#13#10+
              'to correct this error.', mtError,[mbOk], 0);

  ScriptEngineSendMessage(self,'Execute "First Scenario" With Error : '+fErrorDescription+#13#10+
                             'At Line:'+IntTostr(fErrorLine));
 end;

procedure  TBaseScriptEngine.DoShowErrorExtension;
begin
    MessageDlg('Run Extensions Problem!'+#13#10+#13#10+
              'During the execution of an Extension'+#13#10+
              'the Scenario Engine Reports'+#13#10+
              'an Error: '+fErrorDescription+#13#10+
              'At Line:'+IntTostr(fErrorLine)+#13#10+
              'Please Edit this Extension Source'+#13#10+
              'to correct this error.', mtError,[mbOk], 0);

  ScriptEngineSendMessage(self,'Execute "Extension" With Error : '+fErrorDescription+#13#10+
                             'At Line:'+IntTostr(fErrorLine));
 end;

procedure  TBaseScriptEngine.DoShowErrorTimeEvent;
begin
    MessageDlg('TimeEvent "Run Scenario" Problem!'+#13#10+#13#10+
              'During the execution of TimeEvent with name: '+self.Name+#13#10+
              'the Scenario Engine Reports'+#13#10+
              'an Error: '+fErrorDescription+#13#10+
              'At Line:'+IntTostr(fErrorLine)+#13#10+
              'Please Edit this TimeEvent "Scenario" property'+#13#10+
              'to correct this error.', mtError,[mbOk], 0);

  ScriptEngineSendMessage(self,'Execute "TimeEvent Script" With Error : '+fErrorDescription+#13#10+
                             'At Line:'+IntTostr(fErrorLine));
 end;

procedure TBaseScriptEngine.DoOnError(Sender:TTitanScripter);
 begin
      fErrorType:=1;    //FCompiler Error
      fErrorDescription:=fScripter.ErrorDescription;
      fErrorModuleName:=fScripter.ErrorModuleName;
      fErrorTextPos:=fScripter.ErrorTextPos;
      fErrorPos:=fScripter.ErrorPos;
      fErrorLine:=fScripter.ErrorLine;
      fErrorMethodId:=fScripter.ErrorMethodId;


      case fRunOrder of
        soRunFirstScript  :DoShowErrorWithDialog;
        soRunExtension    :DoShowErrorExtension;
        soTimeEvent       :DoShowErrorTimeEvent;
      else
         if Assigned(fOnError) then fOnError(self,fErrorType);
      end;

 end;

//============= Init function ==================================
Procedure TBaseScriptEngine._InitCompiler;
begin
  fScripter.ResetScripter;
  fScripter.RegisterLanguage(fPascalLanguage);
  fScripter.AddModule('1', fPascalLanguage.LanguageName);
  fScripter.AddCode('1',FScriptText);
  fScripter.OnShowError:=DoOnError;
  CommonRegisterVars;
end;

 //==================== Run Functions =============================================  

procedure TBaseScriptEngine.OnThreadExecute(Sender: TObject); // Main Thread procedure..  s
  var i:integer;
      isError:boolean;
begin

  fErrorType:=-1;
  fErrorDescription:='';
  fErrorModuleName:='';
  fErrorTextPos:=-1;
  fErrorPos:=-1;
  fErrorLine:=-1;
  fErrorMethodId:=-1;

 case fRunOrder of
 //soNone:
 soTerminate:fScripter.Terminate;
 soCheckSyntax:
              begin
                  _InitCompiler;
                  fScripter.Compile(true);
                  isError:=fScripter.IsError;
                  if isError=false then
                    fStatus:=ssRunOk else
                    fStatus:=ssSyntaxCheckError;

              end;
 soCompile: begin
            _InitCompiler;
            fScripter.Compile;
            isError:=fScripter.IsError;
            if isError=false then
                fStatus:=ssRunOk else
                fStatus:=ssCompilerError;
            end;
 soRun,soRunFirstScript,soRunExtension,soTimeEvent:
         begin
            _InitCompiler;
            fScripter.Run(rmRun);
            isError:=fScripter.IsError;
            if isError=false then
                fStatus:=ssRunOk else
                fStatus:=ssCompilerError;

          end;

 soRunToLine:begin
              _InitCompiler;
              fScripter.Run(rmRunToCursor,'1',fLastRunLine);
              isError:=fScripter.IsError;

              if isError=false then
                 fStatus:=ssRunOk else
                 fStatus:=ssCompilerError;
            end;
 soRunToNextLine:begin
              fScripter.Run(rmTraceToNextSourceLine,'1',fLastRunLine);
              isError:=fScripter.IsError;

              if isError=false then
                 fStatus:=ssRunOk else
                 fStatus:=ssCompilerError;
            end;
 soTraceInto:begin
              fScripter.Run(rmTraceInto);
              isError:=fScripter.IsError;

              if isError=false then
                 fStatus:=ssRunOk else
                 fStatus:=ssCompilerError;
            end;
 soStepOver:begin
              fScripter.Run(rmStepOver);
              isError:=fScripter.IsError;

              if isError=false then
                 fStatus:=ssRunOk else
                 fStatus:=ssCompilerError;
            end;
        
 end;
end;
//============================== Run Functions ==========================================
procedure TBaseScriptEngine.SyntaxCheck;
 begin
   fRunOrder:=soCheckSyntax;
   OnThreadExecute(self);
 end;


Procedure TBaseScriptEngine.Compile;
    var el,ec:integer;
        em:string;
 begin

 if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
   fRunOrder:=soCompile;
   OnThreadExecute(self);
  if Assigned(FOnAfterCompile) then FOnAfterCompile(self);
 end;

Procedure TBaseScriptEngine.RunFirstScript;
 begin
 if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
// if fScripter.IsRunning=true then begin exit; end;
  fRunOrder:=soRunFirstScript;
  FScriptText:=FFirstScript;
   OnThreadExecute(self);
  if Assigned(FOnAfterRunScript) then FOnAfterRunScript(self);
  if fErrorType=-1 then ScriptEngineSendMessage(self,'Execute "First Script" --> OK');
 end;

Procedure  TBaseScriptEngine.RunExtension;
 begin
   if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
   fRunOrder:=soRunExtension;
   OnThreadExecute(self);
   if Assigned(FOnAfterRunScript) then  FOnAfterRunScript(self);
   if fErrorType=-1 then ScriptEngineSendMessage(self,'Execute "Extension" --> OK');
 end;

 Procedure  TBaseScriptEngine.RunTimeEvent;
 begin
   if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
   fRunOrder:=soTimeEvent;
   OnThreadExecute(self);
   if fErrorType=-1 then ScriptEngineSendMessage(self,'Execute "TimeEvent Script" --> OK');
 end;

Procedure TBaseScriptEngine.Run;
 begin
   if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
   fRunOrder:=soRun;
   OnThreadExecute(self);
   if Assigned(FOnAfterRunScript) then  FOnAfterRunScript(self);
 end;

Procedure TBaseScriptEngine.Stop;
begin  
   if fScripter=nil then exit;

   if Assigned(FOnStopScript) then FOnStopScript(self);
   fScripter.Terminate;
 end;

Procedure TBaseScriptEngine.RunToLine(const aline:Integer);
 begin
   if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
   fLastRunLine:=aline;
   fRunOrder:=soRunToLine;
   OnThreadExecute(self);
   if Assigned(FOnAfterRunScript) then FOnAfterRunScript(self);
 end;

Procedure TBaseScriptEngine.RunToNextLine;
 begin
   if Assigned(FOnBeforeRunScript) then FOnBeforeRunScript(self);
   fRunOrder:=soRunToNextLine;
   OnThreadExecute(self);
   if Assigned(FOnAfterRunScript) then FOnAfterRunScript(self);
 end;

 procedure  TBaseScriptEngine.TraceInto;
 begin
   fRunOrder:=soTraceInto;
   OnThreadExecute(self);
 end;

procedure  TBaseScriptEngine.StepOver;
 begin
   fRunOrder:=soStepOver;
   OnThreadExecute(self);
 end;


//..........................AddObjectToScript........................
procedure TBaseScriptEngine.AddObjectToScript(Instance : TObject; const Name : string; Global : boolean);
 begin
  if Instance=nil then exit;
  if fScripter=nil then exit;
  
  fScripter.RegisterObject(Name,Instance);
 end;


procedure TBaseScriptEngine.EventsDeleteAll;
var i: integer;
begin
  if fScripter=nil then exit;
  if fScripter.fScripter=nil then exit;

  fScripter.fScripter.EventHandlerList.ClearHandlers; 
  ScriptEngineSendMessage(self,'Delete All Objects Events');
end;

Function  TBaseScriptEngine.IsScriptFileFormat(Filename:string):boolean;
var sss:string;
 begin
  result:=false;
  sss:=ExtractFileExt(Filename);
  if (SameText(sss,'.txt'))  then
   result:=true;
 end;

Function  TBaseScriptEngine.LoadScriptFromFile(const aFilename:string):boolean;
 var msl:TStringList;
 begin
   result:=false;
   if FileExists(aFilename)=false then exit;

   msl:=TStringList.Create;
   try
    msl.LoadFromFile(aFilename);
    fScriptText:=msl.Text;
    result:=true;
   finally
    msl.Free;
   end;
 end;

Function  TBaseScriptEngine.LoadScriptFromStream(aStream:TStream):boolean;
 var msl:TStringList;
 begin
   result:=false;
   if aStream=nil then exit;

   msl:=TStringList.Create;
   try
    msl.LoadFromStream(aStream);
    fScriptText:=msl.Text;
    result:=true;
   finally
    msl.Free;
   end;
 end;

Procedure TBaseScriptEngine.CommonRegisterVars;
 begin
   //Nothing...
 end;




end.

