{%mainunit base_engine.pas}

procedure TTitanEventHandler.HandleEvent;assembler;
const
  LocalFrameSize = 40;

{$asmmode intel}
asm
  mov dword ptr Self._EAX, eax
  mov dword ptr Self._EDX, edx
  mov dword ptr Self._ECX, ecx
  mov dword ptr Self._P, esp

  push ebp
  mov ebp, esp

  sub esp, LocalFrameSize

  mov [ebp-12], ecx
  mov [ebp- 8], edx
  mov [ebp- 4], eax

  push eax
  call Invoke
  pop eax

  mov ecx, RetSize //self.RetSize  FPC SVN 35962

  mov esp, ebp
  pop ebp

  cmp ecx, 0

  jnz @@Ret4
  ret

@@Ret4:
  cmp ecx, 4
  jnz @@Ret8
  ret 4

@@Ret8:
  cmp ecx, 8
  jnz @@Ret12
  ret 8

@@Ret12:
  cmp ecx, $0c
  jnz @@Ret16
  ret $0c

@@Ret16:
  cmp ecx, $10
  jnz @@Ret20
  ret $10

@@Ret20:
  cmp ecx, $14
  jnz @@Ret24
  ret $14

@@Ret24:
  cmp ecx, $18
  jnz @@Ret28
  ret $18

@@Ret28:
  ret $1C
end;
