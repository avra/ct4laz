{%mainunit base_engine.pas}

 if IsIntf then
  asm
    push rsi
    push rdi

    sub rsp, StkUsage
    mov rdi, rsp
    mov rsi, StkAdr
    mov rcx, StkUsage
    shr rcx, 4

    cmp LTR, true
    jnz @@RTL

    @@LTR:
    add rdi, StkUsage;
    sub rdi,8
    sub rsi,8
    std
    rep movsd
    cld
    jmp @@ExtraParam

    @@RTL:
    cld
    rep movsd

    @@ExtraParam:
    cmp PushExtraParam, true;
    jnz @@Go
    push ResAdr

    @@GO:
    cmp Reg_Call, true
    jnz @@EXEC
    mov RAX, EAX_
    mov RDX, EDX_
    mov RCX, ECX_

    @@EXEC:

    mov rdi, CallAddr
   // call dword ptr [rdi] // ??????????????????????

    call qword ptr [rdi]

    cmp CDecl_Call, true
    jnz @@Restore
    add rsp, StkUsage

    @@Restore:
    pop rdi
    pop rsi
  end
  else
  asm
    push rsi
    push rdi
    sub rsp, StkUsage
    mov rdi, rsp
    mov rsi, StkAdr
    mov rcx, StkUsage
    shr rcx, 4

    cmp LTR, true
    jnz @@RTL

    @@LTR:
    add rdi, StkUsage;
    sub rdi,8
    sub rsi,8
    std
    rep movsd
    cld
    jmp @@ExtraParam

    @@RTL:
    cld
    rep movsd

    @@ExtraParam:
    cmp PushExtraParam, true;
    jnz @@Go
    push ResAdr

    @@GO:
    cmp Reg_Call, true
    jnz @@EXEC
    mov RAX, EAX_
    mov RDX, EDX_
    mov RCX, ECX_

    @@EXEC:
    call CallAddr
    cmp CDecl_Call, true
    jnz @@Restore
    add rsp, StkUsage

    @@Restore:
    pop rdi
    pop rsi
  end;

  if ResType <> 0 then
  begin
    asm
      mov dword ptr IntRes2, rdx

      mov rdx, qword ResType
      cmp rdx, typeDOUBLE
      jnz @@PS1
      fstp qword ptr DoubleRes
      jmp @@Done

      @@PS1:
      mov rdx, qword ResType
      cmp rdx, typeCOMP
      jnz @@PS2
      fistp qword ptr CompRes

      @@PS2:
      mov rdx, qword ResType
      cmp rdx, typeREAL48
      jnz @@PS3
      fstp qword ptr DoubleRes
      jmp @@Done

      @@PS3:
      mov rdx, qword ResType
      cmp rdx, typeCURRENCY
      jnz @@PS4
      fistp qword ptr CurrencyRes
      jmp @@Done

      @@PS4:
      mov rdx, qword ResType
      cmp rdx, typeSingle
      jnz @@PS5
      fstp dword ptr SingleRes
      jmp @@Done

      @@PS5:
      mov rdx, qword ResType
      cmp rdx, typeExtended
      jnz @@PS6
      fstp tbyte ptr ExtendedRes
      jmp @@Done

      @@PS6:
      mov rdx, qword ResType
      cmp rdx, typeInt64
      jnz @@PS7
      mov dword ptr IntRes1, rax
      jmp @@Done

      @@PS7:
      cmp ResSize, 4
      jg @@Done
      cmp Safe_Call, true
      jne @@NoSafeCall
      mov rax, ResAdr
      mov rax, [rax]
      @@NoSafeCall:
      mov dword ptr IntRes, rax

      @@Done:
    end;
