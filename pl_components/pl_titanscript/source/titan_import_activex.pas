{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

unit titan_import_activex;

interface

//===============================================================================
{$IFDEF MSWINDOWS}
uses        
   Windows,
   Variants,
   ComObj,
   ActiveX,
   BASE_Engine;

implementation

uses
   SysUtils,
   ComConst;

const


  MaxDispArgs = 64;     // Maximum number of dispatch arguments
  varStrArg = $0048;    // Special variant type codes

// Parameter type masks
  atVarMask  = $3F;
  atTypeMask = $7F;
  atByRef    = $80;

{ Call GetIDsOfNames method on the given IDispatch interface }
//== OK ===========================================================================
function  GetIDsOfNames(const Dispatch: IDispatch;
                        Names: PAnsiChar;
                        NameCount: Integer;
                        DispIDs: PDispIDList): Boolean;
type
  PNamesArray = ^TNamesArray;
  TNamesArray = array[0..100] of PWideChar;

  TArrayOfNamesArray = array[0..20] of TNamesArray;

  procedure RaiseNameException;
  begin
    raise EOleError.CreateFmt(SNoMethod, [Names]);
  end;

var
  N, SrcLen, DestLen: Integer;
  Src: PAnsiChar;
  Dest: PWideChar;
  NameRefs: TNamesArray;
  StackTop: Pointer;
  Temp: Integer;

  buff: array[0..20] of TNamesArray;

begin
  result := true;

  Src := Names;
  N := 0;

  repeat
    SrcLen := SysUtils.StrLen(Src);
    DestLen := MultiByteToWideChar(0, 0, Src, SrcLen, nil, 0) + 1;

    Dest := @ buff[N];

    if N = 0 then
      NameRefs[0] := Dest
    else
      NameRefs[NameCount - N] := Dest;

    MultiByteToWideChar(0, 0, Src, SrcLen, Dest, DestLen);
    Dest[DestLen-1] := #0;
    Inc(Src, SrcLen+1);
    Inc(N);
  until N = NameCount;
  Temp := Dispatch.GetIDsOfNames(GUID_NULL, @NameRefs, NameCount,GetThreadLocale, DispIDs);

  if Temp = Integer(DISP_E_UNKNOWNNAME) then
    result := false
  else
    OleCheck(Temp);

end;
//=============================================================================

function DispHasNames(const Instance: Variant; CallDesc: PCallDesc): Boolean;
var
  Dispatch: Pointer;
  DispIDs: array[0..MaxDispArgs - 1] of Integer;
begin
  if TVarData(Instance).VType = varDispatch then
    Dispatch := TVarData(Instance).VDispatch
  else
  if TVarData(Instance).VType = (varDispatch or varByRef) then
    Dispatch := Pointer(TVarData(Instance).VPointer^)
  else
  begin
    result := false;
    Exit;
  end;

  result := GetIDsOfNames(IDispatch(Dispatch), @CallDesc^.ArgTypes[CallDesc^.ArgCount], CallDesc^.NamedArgCount + 1, @DispIDs);
end;

function DispatchHasNames(ModeCall: Byte;
                          const Instance: Variant;
                          const Name: String;
                          P: Variant;
                          ParamsCount: Integer): Boolean;
var
  CallDesc: TCallDesc;
  S: ShortString;
  I, VCount: Integer;
  VT: Byte;
begin
  FillChar(CallDesc, SizeOf(TCallDesc ), 0);
  S := Name;

  with CallDesc do
  begin
    CallType := ModeCall;
    NamedArgCount := 0;

    ArgCount := 0;
    for I := 1 to ParamsCount do
    begin
      VT := TVarData(P[I]).VType;
      VCount := VarArrayDimCount(P[I]);
      ArgTypes[ArgCount] := VT;

      if VT = VarOleStr then
        ArgTypes[ArgCount] := VarStrArg
      else if (VT = VarVariant) or (VT = VarDispatch) or (VCount > 0) then
        ArgTypes[ArgCount] := VarVariant;

      Inc(ArgCount);
    end;
    Move(S[1], ArgTypes[ArgCount], Length(S));
  end;

  result := DispHasNames(Instance, @CallDesc);
end;


{ Call Invoke method on the given IDispatch interface using the given
  call descriptor, dispatch IDs, parameters, and result }
//==================================================================

procedure MyDispatchInvoke(const Dispatch: IDispatch; CallDesc: PCallDesc;
  DispIDs: PDispIDList; Params: Pointer; Result: PVariant);
type
  PVarArg = ^TVarArg;
{$IFDEF CPU64}
  TVarArg = array[0..5] of DWORD;
{$ELSE}
  TVarArg = array[0..3] of DWORD;
{$ENDIF}
  TStringDesc = record
    BStr: PWideChar;
    PStr: PString;
  end;
var
  I, J, K, ArgType, ArgCount, StrCount, DispID, InvKind, Status: Integer;
  VarFlag: Byte;
  ParamPtr: ^Integer;
  ArgPtr, VarPtr: PVarArg;
  DispParams: TDispParams;
  ExcepInfo: TExcepInfo;
  Strings: array[0..MaxDispArgs - 1] of TStringDesc;
  Args: array[0..MaxDispArgs - 1] of TVarArg;
begin

  StrCount := 0;
  try
    ArgCount := CallDesc^.ArgCount;
    if ArgCount <> 0 then
    begin
      ParamPtr := Params;
      ArgPtr := @Args[ArgCount];
      I := 0;
      repeat
        Dec(Ptrint(ArgPtr), SizeOf(TVarData));

        ArgType := CallDesc^.ArgTypes[I];
        VarFlag := 0;

        if ArgType = varError then
        begin
          ArgPtr^[0] := varError;
        end else
        begin
          if ArgType = varStrArg then
          begin
            with Strings[StrCount] do
              if VarFlag <> 0 then
              begin
                BStr := StringToOleStr(PString(ParamPtr^)^);
                PStr := PString(ParamPtr^);
                ArgPtr^[0] := varOleStr or varByRef;
                ArgPtr^[2] := Integer(@BStr);
              end else
              begin
                BStr := StringToOleStr(PString(ParamPtr)^);
                PStr := nil;
                ArgPtr^[0] := varOleStr;
                ArgPtr^[2] := Integer(BStr);
              end;
            Inc(StrCount);
          end else
          if VarFlag <> 0 then
          begin
            if (ArgType = varVariant) and
              (PVarData(ParamPtr^)^.VType = varString) then
              VarCast(PVariant(ParamPtr^)^, PVariant(ParamPtr^)^, varOleStr);
            ArgPtr^[0] := ArgType or varByRef;
            ArgPtr^[2] := ParamPtr^;
          end else
          if ArgType = varVariant then
          begin
            if PVarData(ParamPtr)^.VType = varString then
            begin
              with Strings[StrCount] do
              begin
                BStr := StringToOleStr(string(PVarData(ParamPtr^)^.VString));
                PStr := nil;
                ArgPtr^[0] := varOleStr;
                ArgPtr^[2] := Integer(BStr);
              end;
              Inc(StrCount);
            end else
            begin
              VarPtr := PVarArg(ParamPtr);

              ArgPtr^[0] := VarPtr^[0];
              ArgPtr^[1] := VarPtr^[1];
              ArgPtr^[2] := VarPtr^[2];
              ArgPtr^[3] := VarPtr^[3];
              Inc(PtrInt(ParamPtr), 12);
            end;
          end else
          begin
            ArgPtr^[0] := ArgType;
            ArgPtr^[2] := ParamPtr^;
            if (ArgType >= varDouble) and (ArgType <= varDate) then
            begin
              Inc(PtrInt(ParamPtr), 4);
              ArgPtr^[3] := ParamPtr^;
            end;
          end;
          Inc(PtrInt(ParamPtr), 4);
        end;
        Inc(I);
      until I = ArgCount;
    end;
    DispParams.rgvarg := @Args;
    DispParams.rgdispidNamedArgs := @DispIDs[1];
    DispParams.cArgs := ArgCount;
    DispParams.cNamedArgs := CallDesc^.NamedArgCount;
    DispID := DispIDs[0];
    InvKind := CallDesc^.CallType;
    if InvKind = DISPATCH_PROPERTYPUT then
    begin
      if Args[0][0] and varTypeMask = varDispatch then
        InvKind := DISPATCH_PROPERTYPUTREF;
      DispIDs[0] := DISPID_PROPERTYPUT;
      Dec(PtrInt(DispParams.rgdispidNamedArgs), SizeOf(Integer));
      Inc(DispParams.cNamedArgs);
    end else
    begin

      if (InvKind = DISPATCH_METHOD) and (ArgCount = 0) and (Result <> nil) then
        InvKind := DISPATCH_METHOD or DISPATCH_PROPERTYGET;

    end;
    Status := Dispatch.Invoke(DispID, GUID_NULL, 0, InvKind, DispParams,
      Result, @ExcepInfo, nil);
    if Status <> 0 then DispatchInvokeError(Status, ExcepInfo);
    J := StrCount;
    while J <> 0 do
    begin
      Dec(J);
      with Strings[J] do
        if PStr <> nil then OleStrToStrVar(BStr, PStr^);
    end;
  finally
    K := StrCount;
    while K <> 0 do
    begin
      Dec(K);
      SysFreeString(Strings[K].BStr);
    end;
  end;
end;

{ Call GetIDsOfNames method on the given IDispatch interface }

{ Central call dispatcher }

procedure MyVarDispInvoke(Result: PVariant; const Instance: Variant;
  CallDesc : PCallDesc; Params: Pointer); cdecl;

  procedure RaiseException;
  begin
    raise EOleError.Create(SVarNotObject);
  end;

var
  Dispatch: Pointer;
  DispIDs: array[0..MaxDispArgs - 1] of Integer;
begin

  if TVarData(Instance).VType = varDispatch then
    Dispatch := TVarData(Instance).VDispatch
  else if TVarData(Instance).VType = (varDispatch or varByRef) then
    Dispatch := Pointer(TVarData(Instance).VPointer^)
  else
    RaiseException;

  GetIDsOfNames(IDispatch(Dispatch), @CallDesc^.ArgTypes[CallDesc^.ArgCount],
    CallDesc^.NamedArgCount + 1, @DispIDs);

  if Result <> nil then VarClear(Result^);

  MyDispatchInvoke(IDispatch(Dispatch), CallDesc, @DispIDs, Params, Result);
end;


function DispatchProcedure(ModeCall: Byte;
                           const Instance: Variant;
                           const Name: String;
                           const P: Variant;
                           ParamsCount: Integer): Variant;
var
  CallDesc: TCallDesc;
  Params: array[0..100] of LongInt;
  S: ShortString;
  I, K, VCount: Integer;
  VT: Byte;
  D: Double;
  V: Variant;
  SS: array [0..30] of String;
begin
  FillChar(CallDesc, SizeOf(TCallDesc ), 0);
  FillChar(Params, SizeOf(Params), 0);

  S := Name;

  with CallDesc do
  begin
    CallType := ModeCall;
    NamedArgCount := 0;

    ArgCount := 0;
    K := -1;

    for I := 1 to ParamsCount do
    begin
      VT := TVarData(P[I]).VType;
      VCount := VarArrayDimCount(P[I]);

      ArgTypes[ArgCount] := VT;

      if (VT in [VarInteger,VarSmallInt,VarByte]) and (VCount=0) then
      begin
        Inc(K);
        Params[K] := P[I];
      end
      else if   VT = VarError then
      begin
//      Inc(K);
//      Params[K] := P[I];
      end
      else if VT = VarOleStr then
      begin
        ArgTypes[ArgCount] := VarStrArg;
        SS[I] := P[I];
        Inc(K);
        Params[K] := PtrInt(SS[I]);
      end
      else if (VT = VarVariant) or (VT = VarDispatch) or (VCount > 0) then
      begin
        ArgTypes[ArgCount] := VarVariant;
        Inc(K);
        V := P[I];
        Move(V, Params[K], SizeOf(Variant));
        Inc(K);
        Inc(K);
        Inc(K);
      end
      else if (VT = VarDouble) or (VT = VarCurrency) then
      begin
        Inc(K);
        D := P[I];
        Move(D, Params[K], SizeOf(Double));
        Inc(K);
      end;

//    ArgTypes[ ArgCount ] := ArgTypes[ ArgCount ]{ or atByRef };
//    ArgTypes[ ArgCount ] := ArgTypes[ ArgCount ] or atTypeMask;

      Inc(ArgCount);
    end;

    Move(S[1], ArgTypes[ArgCount], Length(S));
  end;

  MyVarDispInvoke(@Result, Instance, @CallDesc, @Params);
end;

procedure ActiveXObject_GetProperty(M: TTitanMethodBody);
var
  ParamCount: Integer;
  I: Integer;
  Params: Variant;
  ModeCall: Byte;
  D, V, Value: Variant;
  X: ActiveXObject;
  S: String;
begin
  ParamCount := M.ParamCount;
  Params := VarArrayCreate([1, ParamCount], varVariant);
  for I:=1 to ParamCount do
  begin
    Value := M.Params[I - 1].AsVariant;

    if VarType(Value) = varBoolean then
    begin
      if Value then
        Params[I] := Integer(1)
      else
        Params[I] := Integer(0);
    end
    else if VarType(Value) = varScriptObject then
    begin
      Params[I] := ActiveXObject(VariantToScriptObject(Value).Instance).D;
    end
    else
      Params[I] := Value;
 end;
  ModeCall := DISPATCH_METHOD + DISPATCH_PROPERTYGET;
  D := ActiveXObject(M.Self).D;
  V := DispatchProcedure(ModeCall, D, M.Name, Params, ParamCount);

  with M do
    if VarType(V) = varDispatch then
    begin
      // Make sure the object is properly cast as an IDispatch
      V := IUnknown(V) as IDispatch;
      if (IDispatch(V) <> NIL)
      then
      begin
        X := ActiveXObject.Create(M.Scripter);
        X.D := V;
        result.AsTObject := X;
      end
      else
        result.AsVariant := NULL;
    end
    else if VarType(V) = varOleStr then
    begin
      S := V;
      result.AsVariant := S;
    end
    else if VarType(V) = varEmpty then
    begin
      result.AsVariant := V;
    end
    else if VarType(V) = varNull then
    begin
      result.AsVariant := V;
    end
    else
      result.AsVariant := V;
end;

procedure ActiveXObject_PutProperty(M: TTitanMethodBody);
var
  ParamCount: Integer;
  I: Integer;
  Params: Variant;
  ModeCall: Byte;
  D, Value: Variant;
begin
  ParamCount := M.ParamCount;
  Params := VarArrayCreate([1, ParamCount], varVariant);
  for I:=1 to ParamCount do
  begin
    Value := M.Params[I - 1].AsVariant;

    if VarType(Value) = varBoolean then
    begin
      if Value then
        Params[I] := Integer(1)
      else
        Params[I] := Integer(0);
    end
    else if VarType(Value) = varScriptObject then
    begin
      Params[I] := ActiveXObject(VariantToScriptObject(Value).Instance).D;
    end
    else
      Params[I] := Value;
 end;
  ModeCall := DISPATCH_PROPERTYPUT;
  D := ActiveXObject(M.Self).D;
  DispatchProcedure(ModeCall, D, M.Name, Params, ParamCount);
end;

procedure Create_ActiveXObject(MethodBody: TTitanMethodBody);
begin
  with MethodBody do
  begin
    Self := ActiveXObject.Create(Scripter);
    ActiveXObject(Self).D := CreateOleObject(Params[0].AsString);
  end;
end;

procedure _CreateOleObject(MethodBody: TTitanMethodBody);
  var aax:ActiveXObject;
begin
  with MethodBody do
  begin
    aax := ActiveXObject.Create(Scripter);
    aax.D := CreateOleObject(Params[0].AsString);
    result.AsVariant:=aax.D;
  end;
end;

procedure _GetActiveOleObject(MethodBody: TTitanMethodBody);
  var aax:ActiveXObject;
begin
  with MethodBody do
  begin
    aax := ActiveXObject.Create(Scripter);
    aax.D := GetActiveOleObject(Params[0].AsString);
    result.AsVariant:=aax.D;
  end; 
end;

initialization
  CoInitialize(nil);

  with GlobalDefinitionList do
  begin
    AddClass2(ActiveXObject, nil, ActiveXObject_GetProperty,ActiveXObject_PutProperty);
    AddMethod4(ActiveXObject, 'New', Create_ActiveXObject, 1);
    AddMethod4(ActiveXObject, 'Create', Create_ActiveXObject, 1);
    AddMethod4(ActiveXObject, 'ActiveXObject', Create_ActiveXObject, 1);

    AddMethod3('CreateOleObject',@_CreateOleObject,1,nil,true);
    AddMethod3('GetActiveOleObject',@_GetActiveOleObject,1,nil,true);
  end;
end.
{$ELSE}
//=================================================================================
implementation
end.
{$ENDIF}
