{%mainunit base_engine.pas}

  if IsIntf then
  asm
    push esi
    push edi

    sub esp, StkUsage
    mov edi, esp
    mov esi, StkAdr
    mov ecx, StkUsage
    shr ecx, 2

    cmp LTR, true
    jnz @@RTL

    @@LTR:
    add edi, StkUsage;
    sub edi,4
    sub esi,4
    std
    rep movsd
    cld
    jmp @@ExtraParam

    @@RTL:
    cld
    rep movsd

    @@ExtraParam:
    cmp PushExtraParam, true;
    jnz @@Go
    push ResAdr

    @@GO:
    cmp Reg_Call, true
    jnz @@EXEC
    mov EAX, EAX_
    mov EDX, EDX_
    mov ECX, ECX_

    @@EXEC:

    mov edi, CallAddr
    call dword ptr [edi]

    cmp CDecl_Call, true
    jnz @@Restore
    add esp, StkUsage

    @@Restore:
    pop edi
    pop esi
  end
  else
  asm
    push esi
    push edi
    sub esp, StkUsage
    mov edi, esp
    mov esi, StkAdr
    mov ecx, StkUsage
    shr ecx, 2

    cmp LTR, true
    jnz @@RTL

    @@LTR:
    add edi, StkUsage;
    sub edi,4
    sub esi,4
    std
    rep movsd
    cld
    jmp @@ExtraParam

    @@RTL:
    cld
    rep movsd

    @@ExtraParam:
    cmp PushExtraParam, true;
    jnz @@Go
    push ResAdr

    @@GO:
    cmp Reg_Call, true
    jnz @@EXEC
    mov EAX, EAX_
    mov EDX, EDX_
    mov ECX, ECX_

    @@EXEC:
    call CallAddr
    cmp CDecl_Call, true
    jnz @@Restore
    add esp, StkUsage

    @@Restore:
    pop edi
    pop esi
  end;

  if ResType <> 0 then
  begin
    asm
      mov dword ptr IntRes2, edx

      mov edx, ResType
      cmp edx, typeDOUBLE
      jnz @@PS1
      fstp qword ptr DoubleRes
      jmp @@Done

      @@PS1:
      mov edx, ResType
      cmp edx, typeCOMP
      jnz @@PS2
      fistp qword ptr CompRes

      @@PS2:
      mov edx, ResType
      cmp edx, typeREAL48
      jnz @@PS3
      fstp qword ptr DoubleRes
      jmp @@Done

      @@PS3:
      mov edx, ResType
      cmp edx, typeCURRENCY
      jnz @@PS4
      fistp qword ptr CurrencyRes
      jmp @@Done

      @@PS4:
      mov edx, ResType
      cmp edx, typeSingle
      jnz @@PS5
      fstp dword ptr SingleRes
      jmp @@Done

      @@PS5:
      mov edx, ResType
      cmp edx, typeExtended
      jnz @@PS6
      fstp tbyte ptr ExtendedRes
      jmp @@Done

      @@PS6:
      mov edx, ResType
      cmp edx, typeInt64
      jnz @@PS7
      mov dword ptr IntRes1, eax
      jmp @@Done

      @@PS7:
      cmp ResSize, 4
      jg @@Done
      cmp Safe_Call, true
      jne @@NoSafeCall
      mov eax, ResAdr
      mov eax, [eax]
      @@NoSafeCall:
      mov dword ptr IntRes, eax

      @@Done:
    end;
