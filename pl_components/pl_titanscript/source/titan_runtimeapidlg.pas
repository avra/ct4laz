{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

unit titan_runtimeapidlg;  

{$mode objfpc}{$H+}

interface

uses
  LCLIntf, LCLType, LMessages,
  Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ToolWin,
  TypInfo, ExtCtrls,Clipbrd,
  ImgList, ActnList,
  BASE_Engine,
  TitanScripterBaseEngine,
  titan_runtimeapirtti,
  titan_runtimeapidata,
  Menus;


const
  sssProReadOnly='(Is ReadOnly)';
  sssInherited='-->';

type

  TRunTimeAPIDlg = class(TForm)
    SaveDialog1: TSaveDialog;
    ToolBar1: TToolBar;
    StatusBar1: TStatusBar;
    Splitter1: TSplitter;
    TreeView1: TTreeView;
    TreeView2: TTreeView;
    ImageList1: TImageList;
    MainMenu1: TMainMenu;
    Actions1: TMenuItem;
    ActionList1: TActionList;
    ActionClose: TAction;
    ActionFind: TAction;
    ActionHelp: TAction;
    ToolButton1: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    Save1: TMenuItem;
    Find1: TMenuItem;
    Sendselectedtoscenarioeditor1: TMenuItem;
    N1: TMenuItem;
    Close1: TMenuItem;
    ActionGotoVarClasses: TAction;
    ActionGotoRegisterClasses: TAction;
    ActionGotoRegisterProcedures: TAction;
    ActionGotoRegisterVars: TAction;
    Goto1: TMenuItem;
    GotoGlobalVariablesClasses1: TMenuItem;
    GotoRegisterClasses1: TMenuItem;
    GotoRegisterGlobalFunctions1: TMenuItem;
    GotoRegisterGlobalVariables1: TMenuItem;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    PopupMenu1: TPopupMenu;
    Find2: TMenuItem;
    Sendselectedtoscenarioeditor2: TMenuItem;
    GotoTimeEvents1: TMenuItem;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ActionGotoRegisterConstants: TAction;
    ToolButton14: TToolButton;
    ActionGotoRegisterConsts1: TMenuItem;
    ActionGotoRegisterInterfaces: TAction;
    ToolButton15: TToolButton;
    ActionGotoRegisterInterfaces1: TMenuItem;
    ActionGotoRegisterRecords: TAction;
    ActionGotoRegisterArrays: TAction;
    ActionGotoRegisterTypes: TAction;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    GotoRecords1: TMenuItem;
    GotoTypes1: TMenuItem;
    GotoArrays1: TMenuItem;
    ActionGotoRegisterTypeAliases: TAction;
    ToolButton19: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure ActionCloseExecute(Sender: TObject);
    procedure ActionFindExecute(Sender: TObject);
    procedure ActionHelpExecute(Sender: TObject);
    procedure ActionGotoVarClassesExecute(Sender: TObject);
    procedure ActionGotoRegisterClassesExecute(Sender: TObject);
    procedure ActionGotoRegisterProceduresExecute(Sender: TObject);
    procedure ActionGotoRegisterVarsExecute(Sender: TObject);
    procedure ActionGotoRegisterConstantsExecute(Sender: TObject);
    procedure ActionGotoRegisterInterfacesExecute(Sender: TObject);
    procedure ActionGotoRegisterArraysExecute(Sender: TObject);
    procedure ActionGotoRegisterRecordsExecute(Sender: TObject);
    procedure ActionGotoRegisterTypesExecute(Sender: TObject);
    procedure ActionGotoRegisterTypeAliasesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
  private
    FVWorldVarRegClassesNode:TTreeNode;
    fVRegisterClassesNode:TTreeNode;
    fVRegisterInterfacesNode:TTreeNode;
    fVRegisterArraysNode:TTreeNode;
    fVRegisterRecordsNode:TTreeNode;
    fVRegisterEnumsNode:TTreeNode;
    fVRegisterTypesNode:TTreeNode;
    fVRegisterTypeAliases:TTreeNode;
    fVRegisterProceduresNode:TTreeNode;
    fVRegisterVarsNode:TTreeNode;
    fVRegisterConstsNode:TTreeNode;

    fOldFindOptions:TFindOptions; //for save find dialog options

    fLastTreeTypeName:string;

    function  GetIndexFromList(RegList : TStringList; const aName : string) : integer;
    Procedure Treeview2AddClassStrings(xclassName:string;note:TTreeNode);
    //......
    Procedure BuildTreeview1;

    procedure ExpantVarClass(aVarClassName:string);
    procedure ExpantClass(AclassName:string);
    procedure ExpantInterface(aInterface:string);
    procedure ExpantArray(aArray:string);
    procedure ExpantRecord(aRecord:string);
    procedure ExpantType(aType:string);
    procedure ExpantNoObjProcedure(aProcedure:string);
    procedure ExpantConstant(aConst:string);
    procedure ExpantVariable(aVariable:string);
    procedure ExpantUnresolvedType(aType:string);
    procedure ExpantTypeAliase(aTypeAliase:string);

    Function  FindDataByText(const aText:String):TTreeNode;

  public
    Function Execute(aScriptEngine:TBaseScriptEngine):integer;
  end;


var
 VarRunTimeAPIDlg:TRunTimeAPIDlg;

implementation
{$R *.lfm}

//===============================================================

Function  TRunTimeAPIDlg.Execute(aScriptEngine:TBaseScriptEngine):integer;
 begin
  if VarAPIData<>nil then VarAPIData.Free;

  if aScriptEngine<>nil then
  begin
    VarAPIData:=TBaseScriptAPIData.CreateEX(aScriptEngine);
    VarAPIData.Initialase;
  end;

   StatusBar1.SimpleText:='Please Wait.....It is The First Time..';
   
   if varRunTimeAPIDlg.visible=false then varRunTimeAPIDlg.show;
   StatusBar1.SimpleText:='';
 end;

procedure TRunTimeAPIDlg.FormCreate(Sender: TObject);
begin
 fOldFindOptions:=[];
end;

procedure TRunTimeAPIDlg.FormShow(Sender: TObject);
begin
   BuildTreeview1;
end;
          
Function TRunTimeAPIDlg.FindDataByText(const aText:String):TTreeNode;
 begin
 Result:=nil;

 if aText='' then exit;

 Result:=TreeView1.Items.FindNodeWithText(aText);
end;
//............................Treeview2AddClassStrings................
Procedure TRunTimeAPIDlg.Treeview2AddClassStrings(xclassName:string;note:TTreeNode);
var int1,int2,i2,ii:integer;
    ClassPublicInf:TTitanClassDefinition;
    Pub,pubed,subNote,subNote2: TTreeNode;
    aCount, Size: Integer;
    List: PPropList;
    PropInfo: PPropInfo;
    PropOrEventStr,sss: string;
    aMethodInfo:TTitanMethodDefinition;
    aPropertyInfo:TTitanPropertyDefinition;
    aTypeData:PTypeData;
    ssClass:string;
    ss:string;
    //.......
    D: TTitanDefinition;
    DP:TTitanMethodDefinition;
 begin

int1:=VarAPIData.IsClassRegister(xclassName);

if int1>-1 then  //chec for class Registration
begin

ClassPublicInf:= TTitanClassDefinition(VarAPIData.ListForClasses.objects[int1]);// Get Registered Class Info

if ClassPublicInf.pClass<>nil then
with TreeView2.Items do
begin
if note=nil then
 begin
   note:=AddChild(nil,xclassName);
   note.ImageIndex:=8;  note.SelectedIndex:=8;
 end;

//--------------- Public Sesion ---------
 pub:=AddChild( note, 'Public');
 pub.ImageIndex:=3;  pub.SelectedIndex:=3;
 for ii:=0 to GlobalDefinitionList.Count-1 do
  begin
    D := GlobalDefinitionList.Records[ii];
    if D<>nil then
     if D.Owner <> nil then
      if SameText(D.Owner.Name,ClassPublicInf.Name) then
        begin
          Case D.DefKind of

          dkMethod:
                   begin
                    DP:=TTitanMethodDefinition(d);
                    if DP.Fake=false then
                     begin
                      subnote:= AddChild(pub,DP.Header);
                      subnote.ImageIndex:=24; subnote.SelectedIndex:=24;
                      case DP.TypeSub  of
                        tsConstructor,
                        tsDestructor:begin subnote.ImageIndex:=23; subnote.SelectedIndex:=23; end; //For Function
                        tsFunction: begin subnote.ImageIndex:=10; subnote.SelectedIndex:=10; end; //For Function
                        tsProcedure:begin subnote.ImageIndex:=6; subnote.SelectedIndex:=6; end;   //For Procedure
                       end;

                     end;
                   end;
          dkProperty:
                    begin
                      ss:=TTitanPropertyDefinition(d).PropDef;
                      if TTitanPropertyDefinition(d).WriteDef=nil then ss :=ss+'     '+sssProReadOnly;
                      subnote:= AddChild(pub,ss);
                      subnote.ImageIndex:=5;  subnote.SelectedIndex:=5;
                   end;
           end;
       end;
   end;
//--------------- Published  Sesion ---------
if ClassPublicInf.pClass.ClassInfo<>nil then
begin
  aCount := GetPropList(ClassPublicInf.pClass.ClassInfo, tkAny, nil);

 if acount>0 then
  begin

  pubed:=AddChild( note, 'Published');
  pubed.ImageIndex:=4;  pubed.SelectedIndex:=4;
  Size  := aCount * SizeOf(Pointer);
  GetMem(List, Size);
  try
    aCount := GetPropList(ClassPublicInf.pClass.ClassInfo, tkAny, List);
    // write first not Event Properties
    for I2 := 0 to aCount - 1 do
    begin
      PropInfo := List^[I2];
      if PropInfo^.PropType^.Kind in tkMethods then
       begin
        Continue;
       end else
       begin
        PropOrEventStr := 'Property ';
        PropOrEventStr :=PropOrEventStr+PropInfo^.Name+' : '+PropInfo^.PropType^.Name+';';
        if PropInfo^.SetProc=nil then PropOrEventStr :=PropOrEventStr+'     '+sssProReadOnly;
        subnote2:= AddChild(pubed,PropOrEventStr);
        subnote2.ImageIndex:=5;  subnote2.SelectedIndex:=5;
       end;
    end;
     // write now Event Properties
    for I2 := 0 to aCount - 1 do
     begin
      PropInfo := List^[I2];
      if PropInfo^.PropType^.Kind in tkMethods then
      begin
        PropOrEventStr := 'Event ' ;
        aTypeData:=GetTypeData(PropInfo^.PropType);
        sss:=':='+RTTI_GetMethodString(ClassPublicInf.pClass,PropInfo^.Name,aTypeData,false,false,true,false,false);
        subnote2:= AddChild(pubed,PropOrEventStr+PropInfo^.Name+' : '+PropInfo^.PropType^.Name+sss);
        subnote2.ImageIndex:=7;  subnote2.SelectedIndex:=7;
      end;
    end;

  finally
    FreeMem(List);
  end;
end;
end;
end;
end;
end;

//.......................ExpantRegisterClass................................
procedure TRunTimeAPIDlg.ExpantClass(AclassName:string);
 var
    inx1,inx2:integer;
    ClassPublicInf:TTitanClassDefinition;
    ParentClass: TClass;
    Slist:tstringlist;
    note:TTreeNode;
begin
fLastTreeTypeName:=AclassName;

with TreeView2.Items do
begin
BeginUpdate;
clear;

inx1:=VarAPIData.IsClassRegister(AclassName);

if inx1>-1 then  //chec for class Registration
begin

ClassPublicInf:= TTitanClassDefinition(VarAPIData.ListForClasses.objects[inx1]);// Get Registered Class Info
if ClassPublicInf.pClass<>nil then
begin
Slist:=nil;
try
 Slist:=tstringlist.create;
 Slist.Add(AclassName);
  ParentClass := ClassPublicInf.pClass.ClassParent;
  if ParentClass <> nil then
    while ParentClass <> nil do
    begin
      Slist.Add(ParentClass.ClassName);
      ParentClass := ParentClass.ClassParent;
    end;
   
   if slist.Count>0 then
     for inx2:=slist.Count-1 downto 0 do
      begin
        note:=AddChild(nil,slist.Strings[inx2]);
        note.ImageIndex:=8;  note.SelectedIndex:=8;
        Treeview2AddClassStrings(slist.Strings[inx2],note);

      end;

finally
 Slist.free;
end;
end;    

if note<>nil then  note.Expand(true);
   
endUpdate;

if note<>nil then
   TreeView2.Selected:=note;


end;
end;
end;

//.......................ExpantVarClass................................
procedure TRunTimeAPIDlg.ExpantVarClass(aVarClassName:string);
var int:integer;
    Obj:Tobject;
begin
TreeView2.Items.Clear;

if aVarClassName='' then exit;

int:= VarAPIData.ListForClassesVars.IndexOf(aVarClassName);

if int>-1 then
 begin
   Obj:=VarAPIData.ListForClassesVars.Objects[int];
   if obj=nil then exit;


   if Tobject(obj) is TComponent then
      begin
        ExpantClass(TComponent(obj).ClassName);
        exit;
      end;

   if Tobject(obj) is TPersistent then
       begin
        ExpantClass(TPersistent(obj).ClassName);
        exit;
      end;
      
 end;

end;

//................................Expant Interface ..........
procedure  TRunTimeAPIDlg.ExpantInterface(aInterface:string);
  begin
fLastTreeTypeName:=aInterface;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;
   { }
 EndUpdate;
 end;
end;

//................................Expant Array ..........
procedure  TRunTimeAPIDlg.ExpantArray(aArray:string);
 var int:Integer;
     note:TTreeNode;
     dv:TTitanClassDefinition;
     ss:string;
  begin
fLastTreeTypeName:=aArray;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;
   
  int:= VarAPIData.ListForArrays.IndexOf(aArray);
  if int>-1 then
   begin
    dv:=TTitanClassDefinition(VarAPIData.ListForArrays.Objects[int]);
    case dv.ClassKind of
      ckDynamicArray: ss:=dv.Name+' = Array of '+dv.ElTypeName+';' ;
      ckArray:        ss:=dv.Name+' = Array['+IntToStr(dv.ElMin)+'..'+IntToStr(dv.ElMax)+'] of '+dv.ElTypeName+' ;' ;
    end;
    note:=AddChild(nil,ss);
    note.ImageIndex:=33;  note.SelectedIndex:=33;
   end;

 EndUpdate;
 end;
end;

//................................Expant Record ..........
procedure  TRunTimeAPIDlg.ExpantRecord(aRecord:string);
 var int,i:Integer;
     note,SubNote:TTreeNode;
     dv:TTitanClassDefinition;
     df:TTitanRecordFieldDefinition;
     st:TStringList;
  begin
fLastTreeTypeName:=aRecord;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;
  int:= VarAPIData.ListForRecords.IndexOf(aRecord);
  if int>-1 then
   begin
    dv:=TTitanClassDefinition(VarAPIData.ListForRecords.Objects[int]);
    note:=AddChild(nil,dv.Name+' = '+'Record');
    note.ImageIndex:=26;  note.SelectedIndex:=26;

    //.................
      st:=TStringList.Create;
      GlobalDefinitionList.FindRecordFields(st,dv);
      try
       for I := 0 to st.Count - 1 do
        begin
          df:=TTitanRecordFieldDefinition(st.Objects[i]);
          SubNote:=AddChild(Note,df.Name+' : '+df.FieldType+';');
          SubNote.ImageIndex:=28;  SubNote.SelectedIndex:=28;
        end; 
      finally
       st.Free;
      end;
    //.................
    note.Expand(true);
   end;
 EndUpdate;
 end;
end;

//................................Expant Type ..........
procedure  TRunTimeAPIDlg.ExpantType(aType:string);
 var int:Integer;
     note:TTreeNode;
     dv:TTitanRTTITypeDefinition;
     ss:string;
  begin
fLastTreeTypeName:=aType;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;
  int:= VarAPIData.ListForTypes.IndexOf(aType);
  if int>-1 then
   begin
    dv:=TTitanRTTITypeDefinition(VarAPIData.ListForTypes.Objects[int]);
    if dv.pti<>nil then
     ss:=dv.Name+' = '+ RTTI_GetTypeDef(dv.pti) else
     ss:=dv.Name+' = '+ '---- ?????????? ----';

    note:=AddChild(nil,ss);
    note.ImageIndex:=31;  note.SelectedIndex:=31;
   end;
 EndUpdate;
 end;
end;


//................................ExpantVariable..........
procedure  TRunTimeAPIDlg.ExpantVariable(aVariable:string);
 var int:Integer;
     note:TTreeNode;
     dv:TTitanVariableDefinition;
 begin
fLastTreeTypeName:=aVariable;

with TreeView2.Items do
 begin
  BeginUpdate;
  clear;

  int:= VarAPIData.ListForVars.IndexOf(aVariable);
  if int>-1 then
   begin
    dv:=TTitanVariableDefinition(VarAPIData.ListForVars.Objects[int]);
    note:=AddChild(nil,'Variable '+dv.FullName+' : '+dv.TypeName+';');
    note.ImageIndex:=30;  note.SelectedIndex:=30;
   end;

  EndUpdate;
 end;
end;

//................................ExpantConsts..........
procedure  TRunTimeAPIDlg.ExpantConstant(aConst:string);
 var int:Integer;
     note:TTreeNode;
     dc:TTitanConstantDefinition;
 begin
fLastTreeTypeName:=aConst;

with TreeView2.Items do
 begin
  BeginUpdate;
  clear;

  int:= VarAPIData.ListForConsts.IndexOf(aConst);
  if int>-1 then
   begin
    dc:=TTitanConstantDefinition(VarAPIData.ListForConsts.Objects[int]);

    if VarType(dc.DefValue) in [varEmpty, varNull] then
      note:=AddChild(nil,'Constant '+dc.Name+' = Undefined ('+dc.ResultType+')');
      note:=AddChild(nil,'Constant '+dc.Name+' = '+VarToStrDef(dc.DefValue,'')+' ('+dc.ResultType+')');

    note.ImageIndex:=29;  note.SelectedIndex:=29;
   end;

 EndUpdate;
 end;
end;
//................................Expant Procedures..........
procedure  TRunTimeAPIDlg.ExpantNoObjProcedure(aProcedure:string);
var int:Integer;
    note:TTreeNode;
    dm:TTitanMethodDefinition;
    ss:string;
    aTypeData:PTypeData;
 begin
fLastTreeTypeName:=aProcedure;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;

  int:= VarAPIData.ListForNoObjProcedures.IndexOf(aProcedure);
  if int>-1 then
   begin
    dm:=TTitanMethodDefinition(VarAPIData.ListForNoObjProcedures.Objects[int]);

    if dm.Header='' then
       ss:='Function '+dm.Name+';'else
       ss:=dm.Header;

      note:=AddChild(nil,ss);
      note.ImageIndex:=24;  note.SelectedIndex:=24;
   end;
 EndUpdate;
 end;
end;

//................................Expant TypeAliase..........
procedure  TRunTimeAPIDlg.ExpantTypeAliase(aTypeAliase:string);
var int:Integer;
    note:TTreeNode;
    dm:TTitanMethodDefinition;
    ss:string;
    aTypeData:PTypeData;
 begin
fLastTreeTypeName:=aTypeAliase;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;

  int:= VarAPIData.ListForTypeAliases.IndexOf(aTypeAliase);
  if int>-1 then
   begin

    ss:=aTypeAliase+' = '+GlobalTypeAliases.IndexOfByName(aTypeAliase)+' ;';

    note:=AddChild(nil,ss);
    note.ImageIndex:=34;  note.SelectedIndex:=34;
   end;
 EndUpdate;
 end;
end;

//................................Expant UnresolvedType..........
procedure  TRunTimeAPIDlg.ExpantUnresolvedType(aType:string);
var int:Integer;
    note:TTreeNode;
    dm:TTitanMethodDefinition;
    ss:string;
    aTypeData:PTypeData;
 begin
fLastTreeTypeName:=aType;
with TreeView2.Items do
 begin
  BeginUpdate;
  clear;

  int:= VarAPIData.ListForUnresolvedTypes.IndexOf(aType);
  if int>-1 then
   begin
    ss:=VarAPIData.ListForUnresolvedTypes.Strings[int];
    note:=AddChild(nil,ss);
    note.ImageIndex:=25;  note.SelectedIndex:=25;
   end;

 EndUpdate;
 end;
end;

//.................................GetRegID......................
function TRunTimeAPIDlg.GetIndexFromList(RegList : TStringList; const aName : string) : integer;
var
  index : integer;
begin
  result := -1;
  if RegList <> nil then
     result :=RegList.IndexOf(aname);
end;

procedure TRunTimeAPIDlg.ActionCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TRunTimeAPIDlg.ActionFindExecute(Sender: TObject);
var ss,sss:string;
   id:integer;
   fo:TFindOptions;
begin

ss:='';
if treeview2.Focused then
 if treeview2.Selected<>nil then
  if treeview2.Selected.Level=0 then
     ss:=treeview2.Selected.Text;

 StatusBar1.SimpleText:='';

 fo:=[frHideMatchCase,frHideWholeWord,frHideUpDown,frDisableMatchCase,frDisableUpDown,frDisableWholeWord];
end;

procedure TRunTimeAPIDlg.ActionHelpExecute(Sender: TObject);
 var int:integer;
   //  PV:PVirtualNode;
  //   vData: PVNodeData;
     ss:string;
begin
StatusBar1.SimpleText:='';
              {
PV:=TreeView1.GetNextSelected(nil);

//...treeview1
  if (Pv<>nil) and (TreeView1.Focused)  then
    begin
    StatusBar1.SimpleText:='No Help for this Item...';
    ss:=TreeView1.Text[PV,0];

    if (TreeView1.GetNodeLevel(PV)=1) then
        begin
        vData :=TreeView1.GetNodeData(Pv);

        Case vData^.ParentID of
        0: begin  //Help VarClasses
           int:= VarAPIData.ListForClassesVars.IndexOf(ss);
           if int>-1 then
           if VarAPIData.ListForClassesVars.Objects[int]<>nil then
              begin
               StatusBar1.SimpleText:='Getting.. Help for: '+VarAPIData.ListForClassesVars.Objects[int].ClassName;
              // CommonProgramSettings.Common_GethelpOnObjects(VarAPIData.ListForClassesVars.Objects[int].ClassName);
               StatusBar1.SimpleText:='';
               exit;
              end else
               StatusBar1.SimpleText:='Getting.. Help for: '+'Scenario Variables';
            // CommonProgramSettings.Common_Gethelp(HeGlobalVars);
               StatusBar1.SimpleText:='';
               exit;
              end;
         1,2,3,4,5,6,7,8,9: begin
                   StatusBar1.SimpleText:='Getting.. Help for: '+ ss;
                //   CommonProgramSettings.Common_GethelpOnObjects(ss);
                   StatusBar1.SimpleText:='';
                   exit;
                  end;
        End;  
          //.....
       end;
     end;

     }
//...treeview2
   if (treeview2.Selected<>nil) and (treeview2.Focused) then
    begin  {
      if (treeview2.Selected.Level=0) then
          CommonProgramSettings.Common_GethelpOnObjects(treeview2.Selected.Text);
      if (treeview2.Selected.Level=1) then
          CommonProgramSettings.Common_GethelpOnObjects(treeview2.Selected.Parent.Text);
      if (treeview2.Selected.Level=2) then
          CommonProgramSettings.Common_GethelpOnObjects(treeview2.Selected.Parent.Parent.Text);
          }
    end;
end;

procedure TRunTimeAPIDlg.ActionGotoVarClassesExecute(Sender: TObject);
begin
   TreeView1.Selected:=FVWorldVarRegClassesNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterClassesExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterClassesNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterInterfacesExecute(Sender: TObject);
begin
  TreeView1.Selected:=fVRegisterInterfacesNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterArraysExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterArraysNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterRecordsExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterRecordsNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterTypesExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterTypesNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterTypeAliasesExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterTypeAliases;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterVarsExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterVarsNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterConstantsExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterConstsNode;
end;

procedure TRunTimeAPIDlg.ActionGotoRegisterProceduresExecute(Sender: TObject);
begin
 TreeView1.Selected:=fVRegisterProceduresNode;
end;

//============================================================================
//=============================================================================
//============================================================================
   
//...................... BuildTreeview1..........................
Procedure TRunTimeAPIDlg.BuildTreeview1;
var int:integer;
    xDummy: TTreeNode;
 begin
if VarAPIData.ListForClassesVars=nil then  exit;

TreeView1.Items.Clear;
TreeView2.Items.Clear;

TreeView1.BeginUpdate;


FVWorldVarRegClassesNode :=TreeView1.Items.Add(NIL,'Objects Variables (Total:'+inttostr(VarAPIData.ListForClassesVars.Count)+')');
fVRegisterClassesNode    :=TreeView1.Items.Add(NIL,'Registered Classes (Total:'+inttostr(VarAPIData.ListForClasses.Count)+')');
fVRegisterInterfacesNode :=TreeView1.Items.Add(NIL,'Registered Interfaces (Total:'+inttostr(VarAPIData.ListForInterfaces.Count)+')');
fVRegisterArraysNode     :=TreeView1.Items.Add(NIL,'Registered Arrays (Total:'+inttostr(VarAPIData.ListForArrays.Count)+')');
fVRegisterRecordsNode    :=TreeView1.Items.Add(NIL,'Registered Records (Total:'+inttostr(VarAPIData.ListForRecords.Count)+')');
fVRegisterTypesNode      :=TreeView1.Items.Add(NIL,'Registered Types (Total:'+inttostr(VarAPIData.ListForTypes.Count)+')');
fVRegisterTypeAliases    :=TreeView1.Items.Add(NIL,'Registered Types Aliases (Total:'+inttostr(VarAPIData.ListForTypeAliases.Count)+')');
fVRegisterVarsNode       :=TreeView1.Items.Add(NIL,'Registered Variables (Total:'+inttostr(VarAPIData.ListForVars.Count)+')');
fVRegisterConstsNode     :=TreeView1.Items.Add(NIL,'Registered Const (Total:'+inttostr(VarAPIData.ListForConsts.Count)+')');
fVRegisterProceduresNode :=TreeView1.Items.Add(NIL,'Registered Functions (Total:'+inttostr(VarAPIData.ListForNoObjProcedures.Count)+')');

FVWorldVarRegClassesNode.ImageIndex:=19;
fVRegisterClassesNode.ImageIndex   :=8;    //RegisterClasses
fVRegisterInterfacesNode.ImageIndex:=32;   //Interfaces
fVRegisterArraysNode.ImageIndex    :=33;   //arrays
fVRegisterRecordsNode.ImageIndex   :=26;   //Records
fVRegisterTypesNode.ImageIndex     :=31;   //Types
fVRegisterTypeAliases.ImageIndex   :=34;   //TypeAliases
fVRegisterVarsNode.ImageIndex      :=30;   //Variables
fVRegisterConstsNode.ImageIndex    :=29;   //Constants
fVRegisterProceduresNode.ImageIndex:=24;   //NoObjProcedures

FVWorldVarRegClassesNode.SelectedIndex:=19;
fVRegisterClassesNode.SelectedIndex   :=8;    //RegisterClasses
fVRegisterInterfacesNode.SelectedIndex:=32;   //Interfaces
fVRegisterArraysNode.SelectedIndex    :=33;   //arrays
fVRegisterRecordsNode.SelectedIndex   :=26;   //Records
fVRegisterTypesNode.SelectedIndex     :=31;   //Types
fVRegisterTypeAliases.SelectedIndex   :=34;   //TypeAliases
fVRegisterVarsNode.SelectedIndex      :=30;   //Variables
fVRegisterConstsNode.SelectedIndex    :=29;   //Constants
fVRegisterProceduresNode.SelectedIndex:=24;   //NoObjProcedures

//....

for int:=0 to VarAPIData.ListForClassesVars.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(FVWorldVarRegClassesNode,VarAPIData.ListForClassesVars.Strings[int]);
    xDummy.ImageIndex:=19; xDummy.SelectedIndex:=19;
   end;

for int:=0 to VarAPIData.ListForClasses.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterClassesNode,VarAPIData.ListForClasses.Strings[int]);
    xDummy.ImageIndex:=8; xDummy.SelectedIndex:=8;
   end;

for int:=0 to VarAPIData.ListForInterfaces.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterInterfacesNode,VarAPIData.ListForInterfaces.Strings[int]);
    xDummy.ImageIndex:=32; xDummy.SelectedIndex:=32;
   end;

for int:=0 to VarAPIData.ListForArrays.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterArraysNode,VarAPIData.ListForArrays.Strings[int]);
    xDummy.ImageIndex:=33; xDummy.SelectedIndex:=33;
   end;

for int:=0 to VarAPIData.ListForRecords.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterRecordsNode,VarAPIData.ListForRecords.Strings[int]);
    xDummy.ImageIndex:=26; xDummy.SelectedIndex:=26;
   end;

for int:=0 to VarAPIData.ListForTypes.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterTypesNode,VarAPIData.ListForTypes.Strings[int]);
    xDummy.ImageIndex:=31; xDummy.SelectedIndex:=31;
   end;

for int:=0 to VarAPIData.ListForTypeAliases.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterTypeAliases,VarAPIData.ListForTypeAliases.Strings[int]);
    xDummy.ImageIndex:=34; xDummy.SelectedIndex:=34;
   end;

for int:=0 to VarAPIData.ListForVars.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterVarsNode,VarAPIData.ListForVars.Strings[int]);
    xDummy.ImageIndex:=30; xDummy.SelectedIndex:=30;
   end;

for int:=0 to VarAPIData.ListForConsts.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterConstsNode,VarAPIData.ListForConsts.Strings[int]);
    xDummy.ImageIndex:=29; xDummy.SelectedIndex:=29;
   end;

for int:=0 to VarAPIData.ListForNoObjProcedures.Count-1 do
   begin
    xDummy:=TreeView1.Items.AddChild(fVRegisterProceduresNode,VarAPIData.ListForNoObjProcedures.Strings[int]);
    xDummy.ImageIndex:=24; xDummy.SelectedIndex:=24;
   end;
//....

TreeView1.EndUpdate;

end;

procedure TRunTimeAPIDlg.TreeView1Click(Sender: TObject);
begin
   if TreeView1.Selected=nil then exit;
   if TreeView1.Selected.Level=0 then exit;

   if TreeView1.Selected.Parent=FVWorldVarRegClassesNode then ExpantVarClass(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterClassesNode    then ExpantClass(TreeView1.Selected.Text);  
   if TreeView1.Selected.Parent=fVRegisterInterfacesNode then ExpantInterface(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterArraysNode     then ExpantArray(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterRecordsNode    then ExpantRecord(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterTypesNode      then ExpantType(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterTypeAliases    then ExpantTypeAliase(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterVarsNode       then ExpantVariable(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterConstsNode     then ExpantConstant(TreeView1.Selected.Text);
   if TreeView1.Selected.Parent=fVRegisterProceduresNode then ExpantNoObjProcedure(TreeView1.Selected.Text);

end;


end.
