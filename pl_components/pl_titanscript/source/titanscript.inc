{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}


{$M+}
{$mode DELPHI}{$H+}
{$DEFINE THREADS}
{$DEFINE UseCThreads}

{$IFDEF CPU64}
  {.$ASMMODE intel}
{$ENDIF}

{$IFDEF CPU386}
  {.$ASMMODE intel}
{$ENDIF}

{.$DEFINE DUMP}
