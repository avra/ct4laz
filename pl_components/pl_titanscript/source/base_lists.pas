{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}


unit BASE_Lists;

interface

uses
  SysUtils,
  Classes;

type

  THClassRec = class
  public
    pClass: TClass;
    Inx: integer;
    constructor Create;
  end;

  THClassesList = class
  private
    fItems: TList;
    function GetRecord(I: integer): THClassRec;
    function GetCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function Add: THClassRec;
    procedure AddClass(aclass: TClass; const aInx: integer);
    function IndexOf(aClass: TClass): integer;
    function IndexOfByName(aClassName: string): integer;
    property Count: integer read GetCount;
    property Records[I: integer]: THClassRec read GetRecord; default;
  end;


  THGenRec = class
  public
    Name: string;
    Inx: integer;
    constructor Create;
  end;

  THGenList = class
  private
    fItems: TList;
    function GetRecord(I: integer): THGenRec;
    function GetCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function Add: THGenRec;
    procedure AddItem(aname: string; const aInx: integer);
    function IndexOfByName(aName: string): integer;
    property Count: integer read GetCount;
    property Records[I: integer]: THGenRec read GetRecord; default;
  end;
  //...............................

  Th2StringsRec = class
  public
    fType: string;
    fTypeRef: string;
    fObject: TObject;
    constructor Create;
  end;

  Th2StringsList = class
  private
    fItems: TList;
    function GetRecord(I: integer): Th2StringsRec;
    function GetCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function Add: Th2StringsRec;

    procedure AddItem(const aType, aTypeRef: string);
    procedure AddObject(const aType, aTypeRef: string; aObject: TObject);

    function IndexOf(const aType: string): integer;

    function IndexOfByIndex(const aIndex: integer): string;
    function IndexOfByName(const aType: string): string;

    procedure SaveToFile(const aFilename: string);
    property Count: integer read GetCount;
    property Records[I: integer]: Th2StringsRec read GetRecord; default;
  end;


implementation

// =========================== THClassesList ===========================================================

constructor THClassRec.Create;
begin
  inherited;
  pClass := nil;
  Inx := -1;
end;

constructor THClassesList.Create;
begin
  inherited;
  fItems := TList.Create;
end;

destructor THClassesList.Destroy;
begin
  Clear;
  fItems.Free;
  inherited;
end;

function THClassesList.GetRecord(I: integer): THClassRec;
begin
  Result := THClassRec(fItems[I]);
end;

function THClassesList.GetCount: integer;
begin
  Result := fItems.Count;
end;

procedure THClassesList.Clear;
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    Records[I].Free;
  fItems.Clear;
end;

function THClassesList.Add: THClassRec;
begin
  Result := THClassRec.Create;
  fItems.Add(Result);
end;

procedure THClassesList.AddClass(aclass: TClass; const aInx: integer);
begin
  if aclass = nil then
    exit;
  if aInx < 0 then
    exit;
  with Self.Add do
  begin
    pClass := aclass;
    Inx := aInx;
  end;
end;

function THClassesList.IndexOf(aClass: TClass): integer;
var
  I: integer;
  aa: THClassRec;
begin
  Result := -1;

  for I := 0 to Count - 1 do
  begin
    aa := Records[I];
    if aa.pClass = aClass then
    begin
      Result := aa.Inx;
      Exit;
    end;
  end;

end;

function THClassesList.IndexOfByName(aClassName: string): integer;
var
  I: integer;
  aa: THClassRec;
begin
  Result := -1;
  if aClassName = '' then
    exit;
  for I := 0 to Count - 1 do
  begin
    aa := Records[I];
    if SameText(aa.pClass.ClassName, aClassName) then
    begin
      Result := aa.Inx;
      Exit;
    end;
  end;

end;


// ============================ THGenList ============================================

constructor THGenRec.Create;
begin
  inherited;
  Name := '';
  Inx := -1;
end;


constructor THGenList.Create;
begin
  inherited;
  fItems := TList.Create;
end;

destructor THGenList.Destroy;
begin
  Clear;
  fItems.Free;
  inherited;
end;

function THGenList.GetRecord(I: integer): THGenRec;
begin
  Result := THGenRec(fItems[I]);
end;

function THGenList.GetCount: integer;
begin
  Result := fItems.Count;
end;

procedure THGenList.Clear;
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    Records[I].Free;
  fItems.Clear;
end;

function THGenList.Add: THGenRec;
begin
  Result := THGenRec.Create;
  fItems.Add(Result);
end;

procedure THGenList.AddItem(aName: string; const aInx: integer);
begin
  if aName = '' then
    exit;
  if aInx < 0 then
    exit;
  with Self.Add do
  begin
    Name := aName;
    Inx := aInx;
  end;
end;

function THGenList.IndexOfByName(aName: string): integer;
var
  I: integer;
  aa: THGenRec;
begin
  Result := -1;
  if aName = '' then
    exit;
  for I := 0 to Count - 1 do
  begin
    aa := Records[I];
    if SameText(aa.Name, aName) then
    begin
      Result := aa.Inx;
      Exit;
    end;
  end;

end;

//============================ THAliasesList ====================================================
constructor Th2StringsRec.Create;
begin
  inherited;
  fType := '';
  fTypeRef := '';
  fObject := nil;
end;

constructor Th2StringsList.Create;
begin
  inherited;
  fItems := TList.Create;
end;

destructor Th2StringsList.Destroy;
begin
  Clear;
  fItems.Free;
  inherited;
end;

function Th2StringsList.GetRecord(I: integer): Th2StringsRec;
begin
  Result := Th2StringsRec(fItems[I]);
end;

function Th2StringsList.GetCount: integer;
begin
  Result := fItems.Count;
end;

procedure Th2StringsList.Clear;
var
  I: integer;
begin
  for I := 0 to Count - 1 do
    Records[I].Free;
  fItems.Clear;
end;

function Th2StringsList.Add: Th2StringsRec;
begin
  Result := Th2StringsRec.Create;
  fItems.Add(Result);
end;

procedure Th2StringsList.AddItem(const aType, aTypeRef: string);
begin
  if aType = '' then
    exit;
  if aTypeRef = '' then
    exit;

  with Self.Add do
  begin
    fType := aType;
    fTypeRef := aTypeRef;
    fObject := nil;
  end;
end;

procedure Th2StringsList.AddObject(const aType, aTypeRef: string; aObject: TObject);
begin
  if aType = '' then
    exit;
  if aTypeRef = '' then
    exit;
  if aObject = nil then
    exit;

  with Self.Add do
  begin
    fType := aType;
    fTypeRef := aTypeRef;
    fObject := aObject;
  end;
end;

function Th2StringsList.IndexOf(const aType: string): integer;
var
  I: integer;
  aa: Th2StringsRec;
begin
  Result := -1;
  if aType = '' then
    exit;

  for I := 0 to Count - 1 do
  begin
    aa := Records[I];
    if SameText(aa.fType, aType) then
    begin
      Result := i;
      Exit;
    end;
  end;
end;

function Th2StringsList.IndexOfByName(const aType: string): string;
var
  I: integer;
  aa: Th2StringsRec;
begin
  Result := '';
  if aType = '' then
    exit;
  for I := 0 to Count - 1 do
  begin
    aa := Records[I];
    if SameText(aa.fType, aType) then
    begin
      Result := aa.fTypeRef;
      Exit;
    end;
  end;
end;


function Th2StringsList.IndexOfByIndex(const aIndex: integer): string;
begin
  Result := Records[aIndex].fType;
end;


procedure Th2StringsList.SaveToFile(const aFilename: string);
begin

end;


end.
