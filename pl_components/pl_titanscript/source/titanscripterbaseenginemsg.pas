{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit TitanScripterBaseEngineMsg;

interface
  uses Controls, Classes,forms;


const
  MsgInfo=0;
  MsgError=1;
  MsgTimeEvent=2;
type

 _TScriptEngineSendMsgEvent = procedure (Sender : TObject;Text:string;aType:integer) of object; //Connect
 _TScriptEngineStatusEvent = procedure (Sender : TObject; const Status:integer) of object;

 Procedure ScriptEngineSendMessage(Sender : TObject;Text:string;aType:integer=0);

var _xxScriptEngineSendMsgEvent:_TScriptEngineSendMsgEvent;
    _xxScriptEngineStatusEvent :_TScriptEngineStatusEvent;

implementation

Procedure ScriptEngineSendMessage(Sender : TObject;Text:string;aType:integer=0);
 begin
    if Assigned(_xxScriptEngineSendMsgEvent) then _xxScriptEngineSendMsgEvent(Sender,text,aType);
 end;


end.
 
