{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

{$I TitanScript.inc}

unit Titan_Debug;

interface

uses
  Classes, SysUtils;

type
  TDebugObject = class;

  TCollectionUpdateEvent = procedure(Sender: TObject; Item: TCollectionItem) of object;

  TExtCollection = class(TCollection)
  private
    fOnUpdate: TCollectionUpdateEvent;
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    property OnUpdate: TCollectionUpdateEvent read fOnUpdate write fOnUpdate;
  end;

  TBreakPointItem = class(TCollectionItem)
  private
    FEnabled: boolean;
    FLine: integer;
    FPassCount: integer;
    FCondition: string;
    fScriptName: string;
  public
    procedure Assign(Source: TPersistent); override;
    property Condition: string read FCondition write FCondition;
    property Enabled: boolean read FEnabled write FEnabled;
    property Line: integer read FLine write FLine;
    property PassCount: integer read FPassCount write FPassCount;
    property ScriptName: string read fScriptName write fScriptName;
  end;

  TBreakPointChangesHandler = procedure(Sender: TObject; Item: TBreakPointItem) of object;

  TBreakPointList = class(TCollection)
  private
    function GetItem(index: integer): TBreakPointItem;
  public
    constructor Create;
    function AddBreakPoint(const ALine: integer): TBreakPointItem;
    procedure RemoveBreakPoint(const ALine: integer);
    function GetBreakPoint(const ALine: integer): TBreakPointItem;
    property Items[index: integer]: TBreakPointItem read GetItem; default;
  end;

  TWatchItem = class(TCollectionItem)
  private
    fEnabled: boolean;
    fExpression: string;
    fValue: string;
    procedure SetEnabled(const Value: boolean);
    procedure SetExpression(const Value: string);
  public

    procedure Assign(Source: TPersistent); override;
    property Value: string read fValue;
    property Enabled: boolean read fEnabled write SetEnabled;
    property Expression: string read fExpression write SetExpression;
  end;

  TWatchItems = class(TExtCollection)
  private
    fDebugObject: TDebugObject;
    function GetItem(index: integer): TWatchItem;
  protected
  public
    constructor Create(ADebugObject: TDebugObject);
    procedure SetEnableForAll(Enable: boolean);
    property DebugObject: TDebugObject read fDebugObject;
    property Items[index: integer]: TWatchItem read GetItem; default;
  end;

  TDebugObject = class
  private
    fHandlers: TList;
    fWatchItems: TWatchItems;
    fBreakPoints: TBreakPointList;
    fPaused: boolean;
    fAnimate: boolean;
    FLastStopLine: integer;
    FLastStopPos: integer;
  protected

  public
    constructor Create;
    destructor Destroy; override;

    function AddBreakPoint(const ALine: integer): TBreakPointItem;
    procedure RemoveBreakPoint(const ALine: integer);
    function GetBreakPoint(const ALine: integer): TBreakPointItem;
    function IsBreakPoint(ALine: integer): boolean;
    function IsPassBreakPoint(ALine: integer): boolean;
    property WatchItems: TWatchItems read fWatchItems;
    property BreakPoints: TBreakPointList read fBreakPoints;
    property LastStopLine: integer read FLastStopLine write FLastStopLine;
    property LastStopPos: integer read FLastStopPos write FLastStopPos;
  end;

implementation

{******************************************************************}
{ TBreakPointItem }
procedure TBreakPointItem.Assign(Source: TPersistent);
begin
  if Source is TBreakPointItem then
    with TBreakPointItem(Source) do
    begin
      self.Condition := Condition;
      self.Enabled := Enabled;
      self.Line := Line;
      self.PassCount := PassCount;
      self.ScriptName := ScriptName;
      self.Changed(False);
    end
  else
    inherited;
end;

{******************************************************************}

{ TBreakPointList }

constructor TBreakPointList.Create;
begin
  inherited Create(TBreakPointItem);
end;

{------------------------------------------------------------------}

function TBreakPointList.GetItem(index: integer): TBreakPointItem;
begin
  Result := TBreakPointItem(inherited Items[index]);
end;

{------------------------------------------------------------------}

function TBreakPointList.AddBreakPoint(const ALine: integer): TBreakPointItem;
begin
  Result := GetBreakPoint(ALine);
  if Result <> nil then
    exit;

  Result := TBreakPointItem(Add);
  with Result do
  begin
    Line := ALine;
    Enabled := True;
  end;
end;

{------------------------------------------------------------------}

function TBreakPointList.GetBreakPoint(const ALine: integer): TBreakPointItem;
var
  i: integer;
begin
  for i := 0 to Count - 1 do
  begin
    Result := Items[i];
    if (Result.Line = ALine) then
      exit;
  end;

  Result := nil;
end;

{------------------------------------------------------------------}

procedure TBreakPointList.RemoveBreakPoint(const ALine: integer);
begin
  GetBreakPoint(ALine).Free;
end;

{******************************************************************}
{ TDebugObject }

function TDebugObject.AddBreakPoint(const ALine: integer): TBreakPointItem;
begin
  Result := fBreakPoints.AddBreakPoint(ALine);
end;

constructor TDebugObject.Create;
begin
  fHandlers := TList.Create;
  fBreakPoints := TBreakPointList.Create;
  fWatchItems := TWatchItems.Create(self);
  inherited;
end;

destructor TDebugObject.Destroy;
begin
  fHandlers.Free;
  fBreakPoints.Free;
  fWatchItems.Free;
  inherited;
end;

function TDebugObject.GetBreakPoint(const ALine: integer): TBreakPointItem;
begin
  Result := fBreakPoints.GetBreakPoint(ALine);
end;

function TDebugObject.IsBreakPoint(ALine: integer): boolean;
var
  breakitem: TBreakPointItem;
begin
  breakitem := GetBreakPoint(ALine);
  Result := (breakitem <> nil) and (breakitem.Enabled);
end;

function TDebugObject.IsPassBreakPoint(ALine: integer): boolean;
var
  breakitem: TBreakPointItem;
begin
  breakitem := GetBreakPoint(ALine);
  Result := (breakitem <> nil) and (breakitem.Enabled = False);
end;


procedure TDebugObject.RemoveBreakPoint(const ALine: integer);
begin
  fBreakPoints.RemoveBreakPoint(ALine);
end;

{******************************************************************}

{ TWatchItem }

procedure TWatchItem.Assign(Source: TPersistent);
begin
  if Source is TWatchItem then
  begin
    Enabled := False;
    Expression := TWatchItem(Source).Expression;
    Enabled := TWatchItem(Source).Enabled;

    Changed(False);
  end
  else
    inherited;
end;

procedure TWatchItem.SetEnabled(const Value: boolean);
begin
  fEnabled := Value;
end;

procedure TWatchItem.SetExpression(const Value: string);
begin
  fExpression := Value;
end;

{******************************************************************}
{ TWatchItems }

constructor TWatchItems.Create(ADebugObject: TDebugObject);
begin
  inherited Create(TWatchItem);
  fDebugObject := ADebugObject;
end;

function TWatchItems.GetItem(index: integer): TWatchItem;
begin
  Result := TWatchItem(inherited Items[index]);
end;

procedure TWatchItems.SetEnableForAll(Enable: boolean);
var
  i: integer;
begin
  BeginUpdate;
  try
    for i := 0 to Count - 1 do
      Items[i].Enabled := Enable;
  finally
    EndUpdate;
  end;
end;

{------------------------------------------------------------------}

{ TExtCollection }

procedure TExtCollection.Update(Item: TCollectionItem);
begin
  inherited;
  if Assigned(OnUpdate) then
    OnUpdate(self, TWatchItem(Item));
end;

end.
