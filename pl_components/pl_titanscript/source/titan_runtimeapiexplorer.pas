{***************************************************
 Titan Scripter
 Copyright (C) PilotLogic Software House
 https://www.pilotlogic.com

 Package pl_TitanScript
 This file is part of CodeTyphon Studio
****************************************************}

unit titan_runtimeapiexplorer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  TitanScripterBaseEngine,
  titan_runtimeapidlg;

type

  TTitanRunTimeAPIExplorer = class(TComponent)
   private
     fRunTimeAPIDlg:TRunTimeAPIDlg;
   public
     Function Execute(aScriptEngine:TBaseScriptEngine):integer;
  end;

implementation

Function TTitanRunTimeAPIExplorer.Execute(aScriptEngine:TBaseScriptEngine):integer;
 begin
  if fRunTimeAPIDlg=nil then
     fRunTimeAPIDlg:=TRunTimeAPIDlg.Create(self);

  Result:=fRunTimeAPIDlg.Execute(aScriptEngine);
 end;

end.

