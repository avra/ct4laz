{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_DWScript;

{$warn 5023 off : no warning about unused units}
interface

uses
  alldwsregister, dwsArrayElementContext, dwsByteBuffer, dwsComp, dwsCompiler, 
  dwsCompilerContext, dwsCompilerUtils, dwsConnectorExprs, 
  dwsConnectorSymbols, dwsConstExprs, dwsContextMap, dwsConvExprs, 
  dwsCoreExprs, dwsCustomData, dwsDataContext, dwsDateTime, dwsDebugFunctions, 
  dwsDebugger, dwsEncoding, dwsErrors, dwsEvaluate, dwsExperts, dwsExprList, 
  dwsExprs, dwsExternalFunctionJit, dwsExternalSymbols, dwsFileFunctions, 
  dwsFileSystem, dwsFilter, dwsFunctions, dwsGenericExprs, dwsGenericSymbols, 
  dwsGlobalVars, dwsGlobalVarsFunctions, dwsHtmlFilter, dwsInfo, 
  dwsInfoClasses, dwsJSON, dwsJSONConnector, dwsJSONPath, dwsJSONScript, 
  dwsLanguageExtension, dwsLegacy, dwsMagicExprs, dwsMath3DFunctions, 
  dwsMathComplexFunctions, dwsMathFunctions, dwsMethodExprs, dwsOperators, 
  dwsPascalTokenizer, dwsRandom, dwsRelExprs, dwsResultFunctions, 
  dwsRTTIFunctions, dwsSampling, dwsScriptSource, dwsSetOfExprs, 
  dwsSpecializationContext, dwsSpecializationMap, dwsStack, 
  dwsStringFunctions, dwsStringResult, dwsStrings, dwsSymbolDictionary, 
  dwsSymbols, dwsSystemOperators, dwsTimeFunctions, dwsTokenizer, dwsUnicode, 
  dwsUnifiedConstants, dwsUnitSymbols, dwsUtils, dwsVariantFunctions, 
  dwsVCLGUIFunctions, dwsWebUtils, dwsXPlatform, dwsXPlatformUI, dwsXXHash, 
  dwsClasses, dwsClassesLibModule, dwsEncodingLibModule, dwsIniFileModule, 
  dwsSymbolsLibModule, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('alldwsregister', @alldwsregister.Register);
end;

initialization
  RegisterPackage('pl_DWScript', @Register);
end.
