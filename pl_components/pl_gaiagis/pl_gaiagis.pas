{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_gaiagis;

{$warn 5023 off : no warning about unused units}
interface

uses
  AllKernelForGaiaCADRegister, DlgDbFieldSelector, ExtraTGSysUtils, 
  GIS_Classes, GIS_CustomEarthCanvas, GIS_DBFReader, GIS_DBSource, 
  GIS_EarthBase, GIS_Earth, GIS_EarthGrid, GIS_EarthObjectData, 
  GIS_EarthObjects, GIS_EarthObjectsForMaps, GIS_EarthObjPointsGenerators, 
  GIS_EarthScaleShower, GIS_EarthStreams, GIS_EarthUtils, GIS_ENVPersistent, 
  GIS_LayerDS, GIS_MapperDB, GIS_MapperE002, GIS_MapperLYR, GIS_MapperMIF, 
  GIS_MapperSHP, GIS_MMStream, GIS_Polygons, GIS_PresenterImage, 
  GIS_Presenters, GIS_PresentersShapes, GIS_PresenterThematic, 
  GIS_Projections, GIS_ProjectionsMore, GIS_Quaternion, GIS_ReadDBF, 
  GIS_Resource, GIS_SysUtils, GIS_TextReader, GIS_UTM, GIS_XML, 
  SBaseFilesLibrary, SBaseKernelMessages, sFileForGISLayer, sFileForGISObject, 
  sFileForGISPresenter, SGISEngine, SGISFilesLibrary, SMap_ADRG, SMap_ADRI, 
  SMap_DTED1, SMap_GaiaCAD, SMap_Sternas, SMap_UniDIB, STVclsLibraryStore, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('AllKernelForGaiaCADRegister', 
    @AllKernelForGaiaCADRegister.Register);
end;

initialization
  RegisterPackage('pl_gaiagis', @Register);
end.
