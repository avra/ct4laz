{**********************************************************************
          Copyright (c) PilotLogic Software House.
                   All rights reserved.
  
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit AllKernelForGaiaCADRegister;

interface
 uses
  LCLIntf, LCLType,
  Forms,  Classes, Graphics, Controls, SysUtils, StdCtrls,
  TypInfo, lresources, PropEdits, ComponentEditors,
  GIS_Classes,
  GIS_EarthBase,
  GIS_Earth,
  GIS_EarthObjects,
  GIS_EarthObjectsForMaps,
  GIS_EarthObjPointsGenerators,
  GIS_LayerDS,
  GIS_MapperLYR,
  GIS_MapperMIF,
  GIS_MapperSHP,
  GIS_PresenterImage,
  GIS_Presenters,
  GIS_PresentersShapes,
  GIS_Projections,
  GIS_ProjectionsMore,
  GIS_UTM,
  SGISEngine ,    
 // SGScriptEngine ,
 // SGTimeEventsEngine ,
  SGISFilesLibrary ;


procedure Register;

implementation

 {$R AllKernelForGaiaCADRegister.res}

procedure Register;
begin
   RegisterComponents('GaiaGIS',
                      [
                      TEarth,
                      TGISEngine,
                    // TGScriptEngine,
                       TEarthLayerDS
                      ]);
end;



end.
