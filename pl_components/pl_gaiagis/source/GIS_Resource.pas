
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_Resource;



interface

resourcestring

  rsStrToLLMsg = 'StrToLL: format error';
  rsEStrToUnitsMsg = 'StrToUnits: Unknown Unit %s';
  rsEFileNotFoundMsg = 'File %s not found';
  rsEDataFileVersionMsg = 'Invalid File Version %X in %s';
  rsEEarthTextureMsg = 'Earth texture must be a 256 color RGB encoded bitmap';
  rsEConversionError = 'StrToExtended: Conversion failed for %s';
  rsEClassRegisteredMsg = 'TEarth5.RegisterEarthClass: Class %s already registered';
  rsEBadLayerIndexMsg = 'TEarth5.GetLayer: Bad layer index %d';
  rsEBadProjectionClassMsg = 'Class %s does not inherit from TProjectionModel';
  rsEEarthBadObjectMsg = 'TEarthReader returned nil object';
  rsEEarthBadMethodMsg = 'Method %s not defined for %s';

  rsMapping = 'Mapping';
  rsFinished = 'Finished';
  rsMIFImportError = 'MIF Import only supports Lat/Long tables.';
  rsNewObject = 'New Object';
  rsNewPolygon = 'New Polygon';
  rsNewPolyline = 'New Polyline';

  rsPointPresenterName = 'PointPresenter';
  rsBasePolyPresenterName = 'BasePolyPresenter';
  rsPolyPresenterName = 'PolyPresenter';
  rsPolygonPresenterName = 'PolygonPresenter';
  rsPolylinePresenterName = 'PolylinePresenter';

  rsPoly32PresenterName = 'Poly32Presenter';
  rsPolygon32PresenterName = 'Polygon32Presenter';
  rsPolyline32PresenterName = 'Polyline32Presenter';

  rsOpenError = 'Unable to open file %s';
  rsFileMappingCreateError = 'Unable to create file mapping';
  rsFileMappingViewError = 'Unable to map view of file';

  rsTEarthLayerClassName = 'TEarthLayer';
  rsTGeoDataObjectClassName = 'TGeoDataObject';
  rsTCustomTextureMapObject ='TCustomTextureMapObject';
  rsTBitmapObjectClassName = 'TBitmapObject';
  rsTBitmapStoredObjectClassName = 'TBitmapStoredObject';
  rsTCustomMapObjectClassName = 'TCustomMapObject';
  rsTCustomMapStoredObjectClassName = 'TCustomMapStoredObject';

  rsTPointPresenterClassName = 'TPointPresenter';
  rsTBasePolyPresenterClassName = 'TbasePolyPresenter';
  rsTPolyPresenterClassName = 'TPolyPresenter';

  rsTPolygonPresenterClassName = 'TPolygonPresenter';
  rsTPolylinePresenterClassName = 'TPolylinePresenter';

  rsTPoly32PresenterClassName = 'TPoly32Presenter';
  rsTPolygon32PresenterClassName = 'TPolygon32Presenter';
  rsTPolyline32PresenterClassName = 'TPolyline32Presenter';

implementation

end.

