
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_EarthGrid;

interface

uses
  LCLIntf, LCLType,
  SysUtils, Classes, Graphics, Controls,
  GIS_SysUtils, GIS_Classes, GIS_XML, Gr32, Math;

type
 TGridDraw = (gdNone, gdBehind, gdInFront);

TEarthGrid = class(TEarthRoot)
  private
    FEarth: TCustomEarth;
    //..........................
    FPen :TEarthPen;
    FDrawMode : TGridDraw;
    FLatitudeStep:Integer;
    FLongitudeStep:Integer;
    FVisible:boolean;
    fOnChange : TNotifyEvent;
    Procedure SetVisible(val:boolean);
  protected
    Procedure SetPen(aPen:TEarthPen);
    Procedure SetDrawMode(Value:TGridDraw);
    Procedure SetLatitudeStep(Value:Integer);
    Procedure SetLongitudeStep(Value:Integer);
    Procedure DoChange(Sender : TObject);
  public
    constructor Create(aEarth: TCustomEarth);
    destructor Destroy; override;
    procedure RedrawObject;override;
    Procedure Clear;
    procedure PaintGrid;
    function  SaveEnvironment : TGXML_Element;
    procedure LoadEnvironment(Element : TGXML_Element);
    Property  OnChange : TNotifyEvent read fOnChange write fOnChange;
  published
    property Pen: TEarthPen read FPen write SetPen;
    property DrawMode: TGridDraw read FDrawMode write SetDrawMode;
    property LatitudeStep: integer read FLatitudeStep write SetLatitudeStep default 15;
    property LongitudeStep: integer read FLongitudeStep write SetLongitudeStep default 15;
    Property Visible:boolean read FVisible write SetVisible;

  end;

implementation
{===========================================================}
constructor TEarthGrid.Create(aEarth: TCustomEarth);
begin
  FEarth:=aEarth;
  FPen := TEarthPen.Create(self);
  clear;
end;

destructor TEarthGrid.Destroy;
 begin
  FPen.Free;
  inherited;
 end;

Procedure TEarthGrid.Clear;
 begin
  FPen.PenColor := clwhite32;
  FPen.PenWidth := 0.5;
  FLatitudeStep:=15;
  FLongitudeStep:=15;
  FDrawMode := gdInFront;
  Fvisible:=true;
 end;

procedure TEarthGrid.RedrawObject;
 begin
   DoChange(self);
 end;

Procedure TEarthGrid.DoChange(Sender : TObject);
 begin
  if Assigned(fOnChange) then fOnChange(Self);
 end;

Procedure TEarthGrid.SetPen(aPen:TEarthPen);
 begin
  FPen.Assign(aPen);
 end;

Procedure TEarthGrid.SetDrawMode(Value:TGridDraw);
begin
  FDrawMode:=Value;
  
  if FDrawMode=gdNone then
    fvisible:=false else
    fvisible:=true;

  DoChange(self);
 end;

Procedure TEarthGrid.SetLatitudeStep(Value:Integer);
begin
if (Value >= 0) and (Value < 90) then
  begin
    FLatitudeStep:=Value;
    DoChange(self);
  end;
end;

Procedure TEarthGrid.SetLongitudeStep(Value:Integer);
begin
if (Value >= 0) and (Value < 180) then
  begin
   FLongitudeStep:=Value;
   DoChange(self);
  end;
end;

Procedure TEarthGrid.SetVisible(val:boolean);
 begin
  Fvisible:=val;

   if Fvisible=false then      
      DrawMode:=gdNone else
      DrawMode:=gdInFront;
      
  DoChange(self);
 end;

function TEarthGrid.SaveEnvironment : TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddIntAttribute('LatitudeStep',LatitudeStep);
  Result.AddIntAttribute('LongitudeStep',LongitudeStep);
  Result.AddIntAttribute('DrawMode',Ord(DrawMode));
  Result.AddBoolAttribute('Visible',Visible);

  Result.AddElement('Pen', Pen.SaveEnvironment);
end;

procedure TEarthGrid.LoadEnvironment(Element : TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
     LatitudeStep := IntAttributeByName('LatitudeStep', LatitudeStep);
     LongitudeStep := IntAttributeByName('LongitudeStep', LongitudeStep);
     DrawMode:=TGridDraw(IntAttributeByName('DrawMode', Ord(DrawMode)));
     Visible:=BoolAttributeByName('Visible',Visible);

     Pen.LoadEnvironment(ElementByName('Pen', 0));
    end;
end;
//..........................PaintGrid.............................
procedure TEarthGrid.PaintGrid;
var   iSteps:integer;
      idx  : Double;
      ptTmp : TPointLL;
begin
  with FEarth.Projection do
  begin
  // draw lines of Longitude
    iSteps := Max( 2, GraticuleLongSteps );
    idx := -GU_180_DEGREE;
    if FLongitudeStep > 0 then
      repeat
        fEarth.EarthCanvas.RenderLine( PointLLH( idx, -GU_90_DEGREE + GU_DEGREE,0 ),
                                       PointLLH( idx, GU_90_DEGREE - GU_DEGREE,0 ),
                                       iSteps );

        idx:=idx+FLongitudeStep * GU_DEGREE;
      until idx >= GU_180_DEGREE;

  
   if fEarth.Projection.Altitude<2000 then exit;
  // draw lines of Latitude
    iSteps := Max( 2, GraticuleLatSteps );
    idx := 0;
    if FLatitudeStep > 0 then
      repeat
        ptTmp := PointLLH( 0, idx,0 );
        fEarth.EarthCanvas.RenderLine( PointLLH( GU_180_DEGREE - 1, idx,0 ), ptTmp, iSteps );
        fEarth.EarthCanvas.RenderLine( ptTmp, PointLLH( -GU_180_DEGREE + 1, idx,0 ), iSteps );

        ptTmp := PointLLH( 0, -idx,0 );
        fEarth.EarthCanvas.RenderLine( PointLLH( GU_180_DEGREE - 1, -idx,0 ), ptTmp, iSteps );
        fEarth.EarthCanvas.RenderLine( ptTmp, PointLLH( -GU_180_DEGREE + 1, -idx,0 ), iSteps );

        
        idx:=idx+FLongitudeStep * GU_DEGREE;
      until idx >= 85 * GU_DEGREE;
  end;
end;

end.
