
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_EarthObjects;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  Classes, SysUtils, Graphics,
  GIS_Classes, GIS_SysUtils, GIS_EarthObjectData, Forms,
  GIS_EarthStreams, GR32;

type

TMapFileName = type string;

TGeoDataObject = class(TEarthObject)
  private
    FPointsGroups : TGroupsStore;
  protected
    procedure SetCentroid( const ptLL : TPointLL ); override;
    function  GetCentroid : TPointLL; override;
    { This Procedure is for OLD Before GaiaCAD 3.000 version Object Format }
    procedure ReadPoints_Old( PointStore : TPointStore; Reader : TEarthStreamReader );
    procedure ReadPoints( PointStore : TPointStore; Reader : TEarthStreamReader );
    procedure WritePoints( PointStore : TPointStore; Writer : TEarthStreamWriter );
    procedure SetPresenterID(const Value : integer); override;
  public
    constructor Create( aParent : TEarthObjectStore ); override;
    destructor Destroy; override;
    function  Clone(aParent : TEarthObjectStore):TEarthObject; override;
    procedure Assign(Source : TPersistent); override;
    procedure WriteProperties(Writer : TEarthStreamWriter); override;
    procedure ReadProperties(Reader : TEarthStreamReader); override;

    class function PrintableClassName : string; override;
    procedure Render(Earth : TCustomEarth; State : TEarthObjectStateSet); override;
    procedure RedrawObject; override;

    function GetObjectMER : TMER; override;
    function ObjectInstanceSize : integer; override;
    function LLInObject(ptLL : TPointLL; iTolerance : Double ) : Boolean; override;
  published
    property PointsGroups : TGroupsStore read FPointsGroups write FPointsGroups;
    property PresenterID;
  end;

TGeoDataObjectClass = class of TGeoDataObject;


TCustomTextureMapObject=class(TEarthObject)
  protected 
    FTextureMap : TTextureData;
    FsMAPFilename : TMapFileName;
    FiGUPerXPixel : Double;
    FiGUPerYPixel : Double;
    FiTransparentColor : TColor;
    Function  GetTexture:TBitMap32;
    procedure OnTextureOnChange(Sender: TObject);
    procedure SetCentroid(const ptLL : TPointLL); override;
    procedure SetMAPFileName(const aFile : TMapFileName);virtual;
    procedure SetGUPerPixel(iIndex: Integer; Value:Double );
    procedure SetTransparentColor(const Value : TColor);
  public
    constructor Create( aParent : TEarthObjectStore ); override;
    destructor Destroy; override;
    procedure Assign(Source : TPersistent); override;
    class function PrintableClassName : string; override;
    procedure WriteProperties(Writer : TEarthStreamWriter); override;
    procedure ReadProperties(Reader : TEarthStreamReader); override;
    function  LoadMAPFile:boolean;virtual;
    procedure Render(Earth : TCustomEarth; State : TEarthObjectStateSet); override;
    procedure UnRender; override;
    function  GetObjectMER : TMER; override;
    Function  GetFullMAPFilename:String; virtual;
    function  LLInObject(ptLL : TPointLL; iTolerance : Double ) : Boolean; override;
    Function  IsTextureOK:boolean;
    function  SaveAsMapFile(const afile:string):boolean;virtual;
    property  TextureData : TTextureData read FTextureMap;
    property  Texture:TBitMap32 read GetTexture;
    property  MapFileName : TMapFileName read FsMAPFilename write SetMAPFileName;
    property  EarthUnitsPerPixel : Double index 0 read FiGUPerXPixel write SetGUPerPixel;
    property  EarthUnitsPerXPixel : Double index 1 read FiGUPerXPixel write SetGUPerPixel;
    property  EarthUnitsPerYPixel : Double index 2 read FiGUPerYPixel write SetGUPerPixel;
    property  TransparentColor : TColor read FiTransparentColor write SetTransparentColor;
  end;


TBitmapObject = class(TCustomTextureMapObject)
  public
  published
    property MAPFileName ;
    property EarthUnitsPerPixel ;
    property EarthUnitsPerXPixel;
    property EarthUnitsPerYPixel;
    property TransparentColor ;
  end;

 TBitmapStoredObject = class(TBitmapObject)
  private
  protected
    procedure SetMAPFileName(const afile : TMAPFilename);override;
  public
    procedure Assign(Source : TPersistent); override;
    function  LoadMapFile:boolean;override;
    procedure Render(Earth : TCustomEarth; State : TEarthObjectStateSet); override;
    procedure UnRender; override;
    procedure WriteProperties(Writer : TEarthStreamWriter); override;
    procedure ReadProperties(Reader : TEarthStreamReader); override;  
  published
    property Texture;
 end;

 TCustomMapObject = class(TCustomTextureMapObject)
  protected
  public
    function  LoadMapFile:boolean;override;
  published
    property MapFileName;
    property EarthUnitsPerPixel;
    property EarthUnitsPerXPixel;
    property EarthUnitsPerYPixel;
    property TransparentColor;
  end;

TCustomMapStoredObject = class(TCustomMapObject)
  protected
    procedure SetMapfilename(const sMapfile: TMapFileName);override;
  public     
    procedure  Assign(Source: TPersistent); override;
    procedure  WriteProperties(Writer : TEarthStreamWriter); override;
    procedure  ReadProperties(Reader : TEarthStreamReader); override;
    procedure  Render(Earth: TCustomEarth; State: TEarthObjectStateSet); override;
    procedure  UnRender; override;
  published
    property  Texture;
  end;     

implementation

Uses GIS_Resource,SMap_GaiaCAD;

//====================================================================================================
//=============================== TGeoDataObject =====================================================
//====================================================================================================

constructor TGeoDataObject.Create( aParent : TEarthObjectStore );
begin
  inherited Create( aParent );
  fPointsGroups := TGroupsStore.Create;
  if Parent<>nil then fTitle := 'NewObject' + IntToStr(Parent.Count); 
end;

destructor TGeoDataObject.Destroy;
begin
  PointsGroups.Free;
  PointsGroups := nil;

  inherited Destroy;
end;

procedure TGeoDataObject.SetPresenterID(const Value: integer);
begin
   if FPresenterID <> Value then
  begin
    FPresenterID := Value;
    if (Parent <> nil) and (Index >= 0) then
      Parent.PresenterID[Index] := Value;
    RedrawObject;
  end;
end;

procedure TGeoDataObject.Assign(Source : TPersistent);
var idx : integer;
begin
  inherited Assign(Source);

  if Source is TGeoDataObject then
  begin
    PointsGroups.Count := 0;  // To release any existing chains
    PointsGroups.Count := TGeoDataObject(Source).PointsGroups.Count;

    for idx := 0 to PointsGroups.Count - 1 do
      PointsGroups[idx] := TGeoDataObject(Source).PointsGroups[idx].Clone;
  end;
end;

function TGeoDataObject.Clone(aParent : TEarthObjectStore):TEarthObject;
begin
  Result := nil;
  if aParent=nil then exit;
  
  Result := TGeoDataObjectClass(Self.ClassType).Create(aParent);
  TGeoDataObject(Result).Assign( Self );
end;

function TGeoDataObject.ObjectInstanceSize : integer;
var
  idx : integer;
begin
  Result := inherited ObjectInstanceSize;

  if PointsGroups <> nil then
    for idx := 0 to PointsGroups.Count - 1 do
      Result := Result + PointsGroups[idx].ObjectInstanceSize;
end;

function TGeoDataObject.GetCentroid : TPointLL;
begin
 if PointsGroups.Count > 0 then
    FCentroid := PointsGroups.Centroid;

  Result := FCentroid;
end;

procedure TGeoDataObject.SetCentroid( const ptLL : TPointLL );
var
  iChain : integer;
  dx, dy, dz : Double;
  aPointStore: TPointStore;
begin

if PointsGroups<>nil then
 begin
  if PointsGroups.Count>0 then
  begin
    dx := ptLL.iLongX - Centroid.iLongX;
    dy := ptLL.iLatY - Centroid.iLatY;
    dz := ptLL.iHeightZ - Centroid.iHeightZ;

    for iChain := 0 to PointsGroups.Count - 1 do
      begin
        aPointStore:=PointsGroups[iChain];
        if aPointStore.Count>0 then
         aPointStore.Translate( dx, dy, dz )
         else
         aPointStore.Add(ptLL);
       end;
  end else
   begin
    aPointStore:=FPointsGroups.addnew; // add to PointStore
    aPointStore.Add(ptLL);
   end;
 end;

inherited SetCentroid( ptLL );

end;

function TGeoDataObject.GetObjectMER : TMER;
begin
  if not (osValidMER in ObjectState) then
  begin
    Include(ObjectState, osValidMER); { Flag the object as valid }

    if PointsGroups.Count > 0 then
      FObjectMER := PointsGroups.GroupStoreMER
    else
      with Centroid do
        FObjectMER := MER(iLongX, iLatY, 0, 0);
  end;

  Result := FObjectMER;
end;

procedure TGeoDataObject.RedrawObject;
var idx : integer;
begin
  inherited;

  Exclude( ObjectState, osValidMER );

  if PointsGroups <> nil then
  begin
    for idx := 0 to PointsGroups.Count -1 do
      PointsGroups[idx].Refresh;

  end;
end;

class function TGeoDataObject.PrintableClassName : string;
begin
  Result:=ClassName;
end;

procedure TGeoDataObject.Render(Earth : TCustomEarth; State : TEarthObjectStateSet);
var DrawAsPoint:boolean;
    i:integer;
begin
  if FIsUpdating then exit; //must set to work the Begin/EndUpdate

  DrawAsPoint:=true;

  with Earth do
  begin
    EarthCanvas.DefPen.RenderAttribute(EarthCanvas, osSelected in State);
    if Closed then
      EarthCanvas.DefBrush.RenderAttribute(EarthCanvas, osSelected in State);

    if ( PointsGroups <> nil ) and ( PointsGroups.Count <>0)  then
     begin
      for i:=0 to PointsGroups.Count-1 do
       if PointsGroups[i]<>nil then
        if PointsGroups[i].Count>1 then DrawAsPoint:=false;
     end;   

     //......Draw With Out Pressenter .........................
     if DrawAsPoint then
     begin
      if Projection.ProjectionModel.PointLLToXY(Centroid, 0) then
        with EarthCanvas.gaPoints^[0] do
          EarthCanvas.gDrawEllipse( X - 2, Y - 2, X + 2, Y + 2);
     end else
     begin    //draw as Polygon
         EarthCanvas.RenderChainStore( PointsGroups, State, EarthCanvas.DefPen.PenWidth);
     end
  end;
end;

function GetFlagSetFor( PointStore : TPointStore ) : TEarthObjectFlagSet;
var
  ptLast, ptLL : TPointLL;
  iDivisor,dX, dY, dH, iLimit : Double;
  iTmp,  idx : integer;
begin
  Result := [of16Bit]; { set the initial Flag value }

  with PointStore do
  begin
    for idx := Count - 1 downto 0 do
    begin
      ptLL := AsLL[idx];
      with ptLL do
      begin
        if iHeightZ <> 0 then { Check to see if any height data present }
          Include( Result, ofHeight );

     { check to see if 32 bit data points used }
       // if ( ( iLongX mod GU_MINUTE ) <> 0 ) or ( ( iLatY mod GU_MINUTE ) <> 0 ) then
          Exclude( Result, of16Bit );

        if idx > 1 then { Delete duplicate points }
          if CompareMem( @ptLast, @ptLL, SizeOf( TPointLL )) then Delete( idx );
        ptLast := ptLL;
      end;
    end;

    { Set the delta limit size }
    if of16Bit in Result then
    begin
      iLimit := 127;
      iDivisor := GU_MINUTE;
    end
    else
    begin
      iLimit := 32767;
      iDivisor := 1;
    end;

    { find out if storing deltas will compress the data }
    ptLast := AsLL[0];
    iTmp := 8 + Ord( ofHeight in Result ) * 4;
    for idx := 1 to Count - 1 do
    begin
      ptLL := AsLL[idx];

      dX := ( ptLL.iLongX - ptLast.iLongX ) / iDivisor;
      dY := ( ptLL.iLatY - ptLast.iLatY ) / iDivisor;
      dH := ( ptLL.iHeightZ - ptLast.iHeightZ ) / iDivisor;

      if ( Abs( dX ) >= iLimit ) or ( Abs( dY ) >= iLimit ) or ( Abs( dH ) >= iLimit ) then
        Inc( iTmp, 2 + 8 + Ord( ofHeight in Result ) * 4) else
        Inc( iTmp, 4 + Ord( ofHeight in Result ) * 2);

      ptLast := ptLL;
    end;
    if iTmp < Count * ( 8 + Ord( ofHeight in Result ) * 4 ) then
      Include( Result, ofCompressed );

    if Count > 1024 * 64 then
      Include( Result, ofLongCount );
   end;
end;

procedure TGeoDataObject.WriteProperties(Writer : TEarthStreamWriter);
var
  iChain : integer;
  ChainFlags : TEarthObjectFlagSet;
begin
  if Writer=nil then exit;

  if PointsGroups.Count = 0 then
    ObjectFlags := [ofSingle] else
    ObjectFlags := GetFlagSetFor( PointsGroups[0] );

  if PointsGroups.Count > 1 then Include( ObjectFlags, ofMultiRing );

  inherited WriteProperties(Writer);

  Writer.WriteInteger(2);   //Write Object Stream Version ver 2 is at 28-6-2008

  if ofSingle in ObjectFlags then
   begin
    Writer.WritePointLL(PointLL(Centroid.iLongX, Centroid.iLatY), ofHeight in ObjectFlags, of16Bit in ObjectFlags); // write centroid
  end else
  begin

    if ofMultiRing in ObjectFlags then Writer.WriteInteger(PointsGroups.Count);

    ChainFlags := ObjectFlags;

    for iChain := 0 to PointsGroups.Count - 1 do
    begin
      if of16Bit in ChainFlags then Include(PointsGroups[iChain].Flags, ps16Bit);
      if ofCompressed in ChainFlags then Include(PointsGroups[iChain].Flags, psCompressed);

      WritePoints( PointsGroups[iChain], Writer );

      if iChain < PointsGroups.Count -1 then
      begin
        ChainFlags := GetFlagSetFor( PointsGroups[iChain] );
       // Writer.WriteWord( Word( ChainFlags ));    //=== ct9999 ===========
        Writer.WriteInteger16bit(Integer(ChainFlags));
      end;

    end;
  end;
end;


procedure TGeoDataObject.ReadProperties(Reader : TEarthStreamReader);
var  iChain : integer;
     ChainFlags : TEarthObjectFlagSet;
     iVer:integer;

     //............................................
     {
       This Procedure is for OLD Before GaiaCAD 3.000 version
       Object Format
     }
      Procedure _OLD_Read;
        var ix:integer;
       begin
          if ofSingle in ObjectFlags then
          begin
            Centroid := Reader.ReadPointLL(ofHeight in ObjectFlags, of16Bit in ObjectFlags); { read first point }
         end else
         begin
          if ofMultiRing in ObjectFlags then
             PointsGroups.Count := Reader.ReadWord  else
             PointsGroups.Count := 1;

            ChainFlags := ObjectFlags;
        // Load each Point Store from the Reader
            for ix := 0 to PointsGroups.Count - 1 do
             begin
              PointsGroups[ix].StoreHeight := ofHeight in ChainFlags;

              if of16Bit in ChainFlags then      Include(PointsGroups[ix].Flags, ps16Bit);
              if ofCompressed in ChainFlags then Include(PointsGroups[ix].Flags, psCompressed);
              if ofLongCount in ChainFlags then  Include(PointsGroups[ix].Flags, psLongCount);
              ReadPoints_OLD( PointsGroups[ix], Reader);
              // if more rings then get a new flag set
              if iChain < PointsGroups.Count - 1 then
              //  ChainFlags := TEarthObjectFlagSet( Ord( Reader.ReadWord ));  //=== ct9999 ======
                ChainFlags := TEarthObjectFlagSet(Ord(Reader.ReadInteger16bit));
            end;
         end;
      end;
     //............................................


begin
  if Reader=nil then exit;

  inherited ReadProperties(Reader);
  
  if giFileVersion>=TG_FILEVERSION1002 then iVer:= Reader.ReadInteger;   //Read Object Stream Version

  if iVer<2 then begin _OLD_Read; exit; end;                            //Read OLD format

  if ofSingle in ObjectFlags then
   begin
    Centroid := Reader.ReadPointLL(ofHeight in ObjectFlags, of16Bit in ObjectFlags); { read first point }
  end else
  begin

    if ofMultiRing in ObjectFlags then
      PointsGroups.Count := Reader.ReadInteger else
      PointsGroups.Count := 1;

    ChainFlags := ObjectFlags;
    // Load each Point Store from the Reader
    for iChain := 0 to PointsGroups.Count - 1 do
    begin
      PointsGroups[iChain].StoreHeight := ofHeight in ChainFlags;

      if of16Bit in ChainFlags then Include(PointsGroups[iChain].Flags, ps16Bit);
      if ofCompressed in ChainFlags then Include(PointsGroups[iChain].Flags, psCompressed);
      if ofLongCount in ChainFlags then Include(PointsGroups[iChain].Flags, psLongCount);

      ReadPoints( PointsGroups[iChain], Reader);
       // if more rings then get a new flag set
      if iChain < PointsGroups.Count - 1 then
         // ChainFlags := TEarthObjectFlagSet( Ord( Reader.ReadWord )); //=== ct9999 ==========
          ChainFlags := TEarthObjectFlagSet(Ord(Reader.ReadInteger16bit));
    end;
  end;
end;


procedure TGeoDataObject.WritePoints( PointStore : TPointStore; Writer : TEarthStreamWriter );
var
  iDivisor, iLimit: Double;
  idx : Integer;
begin

    iLimit := 1;
    iDivisor := 1;
    Writer.WriteDouble( iLimit );            //DOT In use at GaiaCAD ver 3.000
    Writer.WriteDouble( iDivisor );          //DOT In use at GaiaCAD ver 3.000
    Writer.WriteInteger( PointStore.Count ); // write number of points in ring

    for idx := 0 to PointStore.Count - 1 do
        Writer.WritePointLL( PointStore.AsLL[idx], psHeight in PointStore.Flags, ps16bit in PointStore.Flags );

end;



procedure TGeoDataObject.ReadPoints( PointStore : TPointStore; Reader : TEarthStreamReader );
var
   idx  : integer;
   iDivisor, iLimit :Double;

begin

   iLimit:=Reader.ReadDouble;            //DOT In use at GaiaCAD ver 3.000
   iDivisor:=Reader.ReadDouble;          //DOT In use at GaiaCAD ver 3.000
   PointStore.Count:=Reader.ReadInteger; // Read number of points in ring 

   for idx := 0 to PointStore.Count - 1 do
        PointStore.AsLL[idx] := Reader.ReadPointLL( psHeight in PointStore.Flags, ps16Bit in PointStore.Flags )

end;




procedure TGeoDataObject.ReadPoints_OLD( PointStore : TPointStore; Reader : TEarthStreamReader );
var
   idx  : integer;
  dX, dY, dH,iLastX, iLastY, iLastH, iMultiplier :Double;

  {---------------------------------------------------}
  function ReadPointDiff( Flags : TPointStoreFlagsSet ) : Boolean;
  begin
    if ps16Bit in Flags then
    begin
      dX := Reader.ReadShortInt;
      Result := dX <> 127;
    end
    else
    begin
      dX := Reader.ReadSmallInt;
      Result := dX <> 32767;
    end;

    if Result then
    begin
      if ps16Bit in Flags then
        dY := Reader.ReadShortInt
      else
        dY := Reader.ReadSmallInt;

      dH := 0;
      if psHeight in Flags then
        if ps16Bit in Flags then
          dH := Reader.ReadShortInt  else
          dH := Reader.ReadSmallInt;
    end;
  end;

begin

  with PointStore do
  begin
    if psLongCount in Flags then
      Count := Reader.ReadInteger { get number of points in ring }
    else
      Count := Reader.ReadWord; { get number of points in ring }

    AsLL[0] := Reader.ReadPointLL( psHeight in Flags, ps16Bit in Flags ); { read first point }

    if ps16Bit in Flags then
      iMultiplier := GU_MINUTE  else
      iMultiplier := 1;

    if not ( psCompressed in Flags ) then { read un-compressed data }
      for idx := 1 to Count - 1 do
        AsLL[idx] := Reader.ReadPointLL( psHeight in Flags, ps16Bit in Flags )
    else
    begin { read compressed data }
      with AsLL[0] do
      begin
        iLastX := iLongX/ iMultiplier;
        iLastY := iLatY / iMultiplier;
        iLastH := iHeightZ / iMultiplier;
      end;

      for idx := 1 to Count - 1 do
        if ReadPointDiff( Flags ) then
        begin
          iLastX := iLastX + dX;
          iLastY := iLastY + dY;
          iLastH := iLastH + dH;
          AsLL[idx] := PointLLH( iLastX * iMultiplier, iLastY * iMultiplier, iLastH * iMultiplier );
        end
        else
        begin
          AsLL[idx] := Reader.ReadPointLL( psHeight in Flags, ps16Bit in FLags );
          with AsLL[idx] do
          begin
            iLastX := iLongX / iMultiplier;
            iLastY := iLatY / iMultiplier;
            iLastH := iHeightZ / iMultiplier;
          end;
        end;
    end;
  end;
end;


function TGeoDataObject.LLInObject( ptLL : TPointLL; iTolerance : Double ) : Boolean;
var
  iChain : integer;
begin
  Result := False;
  if LLInObjectMER( ptLL, iTolerance ) = 0 then  Exit;

  if PointsGroups.Count = 0 then begin Result:=True; exit; end;     

  for iChain := PointsGroups.Count - 1 downto 0 do
    with PointsGroups[iChain] do
      begin

        if Closed then // check point is inside of closed object
          Result := PointInPolygon( ptLL.iLongX, ptLL.iLatY ) else
          Result := PointOnPolyline( ptLL.iLongX, ptLL.iLatY, iTolerance );

        if Result then Exit;
      end;
end;

//=============================================================================================
//================================ TCustomTextureMapObject ====================================
//=============================================================================================

constructor TCustomTextureMapObject.Create( aParent : TEarthObjectStore );
begin
  inherited Create( aParent );
  FTextureMap := TTextureData.Create;
  FTextureMap.TextureStream.OnChange:=OnTextureOnChange;
  FiGUPerXPixel := GU_MINUTE;
  FiGUPerYPixel := GU_MINUTE;
  FiTransparentColor := clnone;
end;

destructor TCustomTextureMapObject.Destroy;
begin
  FTextureMap.TextureStream.OnChange:=nil;
  FTextureMap.Free;
  FTextureMap := nil;
  inherited Destroy;
end;

Function  TCustomTextureMapObject.GetTexture:TBitMap32;
 begin
  result:=nil;

  if FTextureMap<>nil then
    result:=FTextureMap.TextureStream;
 end;

procedure TCustomTextureMapObject.OnTextureOnChange(Sender: TObject);
 begin
    RedrawObject;
 end;

function TCustomTextureMapObject.IsTextureOK:boolean;
 begin
  result:=FTextureMap.IsTextureOK;
 end;


procedure TCustomTextureMapObject.SetCentroid(const ptLL : TPointLL);
 begin
    FCentroid := ptLL;
    Exclude(ObjectState, osValidMER);
    RedrawObject;
 end;

Function TCustomTextureMapObject.GetFullMAPFilename:String;
  begin
    result:=UnFixFilePath(FsMAPFilename,Parent.Parent.FilePath);
  end;

procedure TCustomTextureMapObject.SetGUPerPixel(iIndex: Integer; Value:Double );
begin
  case iIndex of
    0 :
      begin
        FiGUPerXPixel := Value;
        FiGUPerYPixel := Value;
      end;
    1 : FiGUPerXPixel := Value;
    2 : FiGUPerYPixel := Value;
  end;
  Centroid:=Centroid;
end;

function TCustomTextureMapObject.GetObjectMER : TMER;
var iWidthX, iHeightY : Double;
begin
  if not (osValidMER in ObjectState) then
  begin
    Include(ObjectState, osValidMER); { Flag the object as valid }

    if FTextureMap <> nil then
      with Centroid do
        if FTextureMap.IsTextureOK then
        begin
          iWidthX := FTextureMap.Width * FiGUPerXPixel;
          iHeightY := FTextureMap.Height * FiGUPerYPixel;

          // MER goes from bottom Left to Top Right
          FObjectMER := MER(iLongX , iLatY - iHeightY,iWidthX, iHeightY );

          // Map Top Left of bitmap
          FTextureMap.MapPoint( iLongX, iLatY, 0, 0 );
          // Map Bottom Right of bitmap
          FTextureMap.MapPoint( iLongX + iWidthX, iLatY - iHeightY,
              FTextureMap.Width, FTextureMap.Height );    

        end;
  end;
  Result := FObjectMER;
end;

procedure TCustomTextureMapObject.Assign(Source : TPersistent);
begin
  inherited Assign(Source);

  if Source is TCustomTextureMapObject then
  begin
    EarthUnitsPerXPixel := TCustomTextureMapObject(Source).EarthUnitsPerXPixel;
    EarthUnitsPerYPixel := TCustomTextureMapObject(Source).EarthUnitsPerYPixel;
    TransparentColor := TCustomTextureMapObject(Source).TransparentColor;
    MAPFileName := TCustomTextureMapObject(Source).MAPFileName;
  end;
end;   

class function TCustomTextureMapObject.PrintableClassName : string;
begin
  Result := rsTCustomTextureMapObject;
end;

function  TCustomTextureMapObject.LoadMAPFile:boolean;
 begin     
   result:=false;
   FTextureMap.LoadFromFile(FsMAPFilename);
   result:=IsTextureOK;
 end;

procedure TCustomTextureMapObject.SetMAPFileName(const afile : TMAPFilename);
begin
  FsMAPFilename:=afile;
  FsMAPFilename:=GetFullMAPFilename;
  FsMAPFilename := Parent.Earth.ResolveFilename( FsMAPFilename );

  if FsMAPFilename ='' then begin exit; end;

  if Not (osDiscardable in ObjectState) and
     Not (osTiny in ObjectState)        or
         (osRedraw in ObjectState)      then
         begin   
           LoadMAPFile;
           //Application.ProcessMessages;
          end;

    if Title = '' then //apply only if then object has NOT Title
        fTitle := ExtractFileName(FsMAPFilename);

    FsMAPFilename:=FixFilePath(FsMAPFilename,Parent.Parent.FilePath);   
end;

procedure TCustomTextureMapObject.Render(Earth : TCustomEarth; State : TEarthObjectStateSet);
begin
 if FIsUpdating then exit; //must set to work the Begin/EndUpdate

 if IsTextureOK=false then
    begin
     MAPFilename:=(fsMAPFilename); //MAPFilename->LoadTexture->Rerdaw object
    end else  
     Earth.EarthCanvas.RenderTextureMap(FTextureMap);

  if Selected then
    begin
      Earth.EarthCanvas.DefPen.RenderAttribute(Earth.EarthCanvas,true );
      Earth.EarthCanvas.RenderProjectedMER(GetObjectMER);
    end;
end;

procedure TCustomTextureMapObject.UnRender;
 begin
   FTextureMap.delete;
 end;


procedure TCustomTextureMapObject.SetTransparentColor(const Value : TColor);
begin
if FiTransparentColor=Value then exit;

  FiTransparentColor:=Value;
  FTextureMap.TransparentColor:=FiTransparentColor;
  RedrawObject;
end;

function TCustomTextureMapObject.LLInObject(ptLL : TPointLL; iTolerance : Double ) : Boolean;
begin
  Result := LLInObjectMER( ptLL, iTolerance ) > 0
end;

procedure TCustomTextureMapObject.WriteProperties(Writer : TEarthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(2); //Write Object Stream Version

  Writer.WriteDouble(Centroid.iLongX);
  Writer.WriteDouble(Centroid.iLatY);
  Writer.WriteDouble(EarthUnitsPerXPixel);
  Writer.WriteDouble(EarthUnitsPerYPixel);
  Writer.WriteShortString(ColorToString(FiTransparentColor));
  Writer.WriteShortString(FsMAPFilename);
end;

procedure TCustomTextureMapObject.ReadProperties(Reader : TEarthStreamReader);
var iX, iY, iVer: integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);
  iVer:=Reader.ReadInteger; //Read Object Stream Version

  iX := Reader.ReadInteger;
  iY := Reader.ReadInteger;
  FiGUPerXPixel := Reader.ReadInteger;
  FiGUPerYPixel := Reader.ReadInteger;
  FiTransparentColor := StringToColor(Reader.ReadShortString);
  MapFileName := Reader.ReadShortString;
  Centroid := PointLL( iX, iY );  //must be at last
end;

function  TCustomTextureMapObject.SaveAsMapFile(const afile:string):boolean;
var
  fInfo: xGenInfoRec;
  fGeo: xGeoRec;
  fPoints: xTestPointsRec;
  Srtm: TMemoryStream;

begin
result:=false;

    //----------xGenInfoRec-----------------------
    fInfo.WhatIs:='GaiaCAD Map File';
    fInfo.FileID:=GaiaCADMapIDFile;
    fInfo.FileVersion:=GaiaCADMapFileVersion ;
    fInfo.ouner:='PilotLogic';
    fInfo.ReleaseDate:=DateToStr(now);
    fInfo.Future0:=-999;
    fInfo.Future1:=-999;
    fInfo.Future2:=-999;
    fInfo.Future3:=-999;
    fInfo.Future4:=-999;
    fInfo.Future5:=-999;

    //-------xGeoRec------------------
    FillChar(fGeo, SizeOf(xGeoRec),'0');

    fGeo.Start_Hor:=EarthUnitsTo(Centroid.iLongX,euSecond);
    fGeo.Start_Ver:=EarthUnitsTo(Centroid.iLatY,euSecond);
    fGeo.Factor_Hor:=(EarthUnitsPerXPixel*60)/GU_MINUTE;
    fGeo.Factor_Ver:=(EarthUnitsPerYPixel*60)/(-GU_MINUTE);

    //---------xTestPointsRec------------------------
    FillChar(fPoints, SizeOf(xTestPointsRec),'0');
    fPoints[1].idle:=true;
    fPoints[2].idle:=true;
    fPoints[3].idle:=true;
    fPoints[4].idle:=true;
    fPoints[5].idle:=true;
    fPoints[6].idle:=true;
    //-------------finally save---------------------------
    try
     Srtm := TMemoryStream.Create;
     Srtm.Write(fInfo ,sizeof(fInfo));
     Srtm.Write(fGeo ,sizeof(fGeo));
     Srtm.Write(fPoints ,sizeof(fPoints));
     TextureData.TextureStream.SaveToStream(Srtm);
     Srtm.SaveToFile(afile);
     result:=true;
    finally
     Srtm.Free;
    end;

end;

//========================================================================================
//======================= TBitmapStoredObject ============================================
//========================================================================================

function  TBitmapStoredObject.LoadMAPFile:boolean;
 begin
   FTextureMap.LoadFromFile( FsMAPFilename );
 end;

procedure TBitmapStoredObject.SetMAPFileName(const afile : TMAPFilename);
begin
 if afile='' then exit;
 FsMAPFilename := Parent.Earth.ResolveFilename(afile);
 if FsMAPFilename = '' then exit;
 LoadMAPFile;

 if fTitle='' then   //apply only if then object has NOT Title
      fTitle:=ExtractFileName(FsMAPFilename);
  FsMAPFilename:='';
end;

procedure TBitmapStoredObject.Render(Earth : TCustomEarth; State : TEarthObjectStateSet);
begin
 if FIsUpdating then exit; //must set to work the Begin/EndUpdate

 if IsTextureOK=true then  Earth.EarthCanvas.RenderTextureMap(FTextureMap);

  if Selected then
    begin
      Earth.EarthCanvas.DefPen.RenderAttribute(Earth.EarthCanvas,true );
      Earth.EarthCanvas.RenderProjectedMER(GetObjectMER);
    end;
end;

procedure TBitmapStoredObject.UnRender;
 begin
  //nothing
 end;

procedure TBitmapStoredObject.Assign(Source : TPersistent);
begin
  inherited Assign(Source);

  if Source is TBitmapStoredObject then
  begin
    FTextureMap.Assign(TBitmapStoredObject(Source).TextureData);
    MAPFileName := TBitmapObject(Source).MAPFileName;
  end;
end;

procedure TBitmapStoredObject.WriteProperties(Writer : TEarthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1);   //Write Object Stream Version

  Writer.WriteBirmap32(FTextureMap.TextureStream);
end;

procedure TBitmapStoredObject.ReadProperties(Reader : TEarthStreamReader);
var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);

  if giFileVersion>=TG_FILEVERSION1002 then
     iVer:= Reader.ReadInteger;            //Read Object Stream Version


  Reader.ReadBirmap32(FTextureMap.TextureStream);
end;

//========================================================================================
//====================== TCustomMapObject ========================================
//========================================================================================

function TCustomMapObject.LoadMapFile:boolean;
 begin
  //do nothing...
 end;

//======================================================================================
//====================== TCustomMapStoredObject ========================================
//======================================================================================  

procedure TCustomMapStoredObject.SetMapfilename(const sMapfile: TMapFileName);
begin
  if sMapfile='' then  exit;

  FsMapFilename :=Parent.Earth.ResolveFilename(sMapfile);

  if sMapfile<>'' then
   begin
    LoadMapFile;
    if FTitle = '' then  //apply only if then object has NOT Title
      fTitle:=ExtractFileName(FsMapFilename);
  end;

  FsMapFilename:='';
end;


procedure TCustomMapStoredObject.Render(Earth: TCustomEarth; State: TEarthObjectStateSet);
begin
 if FIsUpdating then exit; //must set to work the Begin/EndUpdate

 if IsTextureOK=true then  Earth.EarthCanvas.RenderTextureMap(FTextureMap);

  if Selected then
    begin
      Earth.EarthCanvas.DefPen.RenderAttribute(Earth.EarthCanvas,true );
      Earth.EarthCanvas.RenderProjectedMER(GetObjectMER);
    end;  
end;

procedure TCustomMapStoredObject.UnRender;
 begin
  //..... Nothing......
 end;

procedure TCustomMapStoredObject.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TCustomMapStoredObject then
  begin
     FTextureMap.Assign(TCustomMapStoredObject(Source).TextureData);
     FsMapFilename := TCustomMapStoredObject(Source).MapFilename;
  end;
end;

procedure TCustomMapStoredObject.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1);   //Write Object Stream Version

  Writer.WriteBirmap32(FTextureMap.TextureStream);
end;

procedure TCustomMapStoredObject.ReadProperties(Reader: TEarthStreamReader);
var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);

  if giFileVersion>=TG_FILEVERSION1002 then
     iVer:= Reader.ReadInteger;            //Read Object Stream Version

  Reader.ReadBirmap32(FTextureMap.TextureStream);
end;


//*************************************************************************
//*************************************************************************
initialization
  RegisterClasses( [TGeoDataObject,
                    TCustomTextureMapObject,
                    TBitmapObject,
                    TBitmapStoredObject,
                    TCustomMapObject,
                    TCustomMapStoredObject] );
end.
