
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_EarthStreams;



interface

uses Forms, LCLIntf, LCLType, LMessages, Classes, Graphics, Controls, SysUtils, StdCtrls,
  Printers, GIS_SysUtils, ExtCtrls, GR32;
  
 type
 {---------------------------- TEarthStreamReader ----------------------------}
  TEarthStreamReader = class(TEarthRoot)
  private
    FDataStream : TStream;
  protected
    procedure _Read(var Buffer; iSize : integer); virtual;
  public
    constructor Create(aStream : TStream);
    destructor Destroy; override;

    procedure ReadBuffer(var Buffer; iSize : integer); virtual;
    function ReadBoolean : Boolean; virtual;
    function ReadDouble : Double; virtual;
    function ReadWord : Word; virtual;
    function ReadShortInt : ShortInt; virtual;
    function ReadSmallInt : SmallInt; virtual;
    function ReadInteger : integer; virtual;     
    function ReadInteger16bit : integer; virtual;   //Size 2
    function ReadShortString : ShortString; virtual;
    function ReadPointLL(bHeight, b16Bit : Boolean) : TPointLL; virtual;
    function ReadRect : TRect; virtual;
    function ReadMER : TMER; virtual;
    function ReadComponent(Instance: TComponent): TComponent;
    function ReadComponentRes(Instance: TComponent): TComponent;
    Function ReadColor:TColor;
    Procedure ReadPicture(Pic:TPicture);
    procedure ReadBirmap32(aBmp:TBitmap32);

    property DataStream : TStream read FDataStream write FDataStream;
  end;

  {---------------------------- TEarthStreamWriter ----------------------------}
  TEarthStreamWriter = class(TEarthRoot)
  private
    FDataStream : TStream;
  protected
    procedure _Write(const Buffer; iSize : integer); virtual;
  public
    constructor Create(aStream : TStream);
    destructor Destroy; override;

    procedure WriteBuffer(const Buffer; iSize : integer); virtual;
    procedure WriteBoolean(bVal : Boolean); virtual;
    procedure WriteDouble(Val : Double); virtual;
    procedure WriteWord(iVal : Word); virtual;
    procedure WriteShortInt(iVal : ShortInt); virtual;
    procedure WriteSmallInt(iVal : SmallInt); virtual;
    procedure WriteInteger(iVal : Integer); virtual;
    procedure WriteInteger16bit(iVal : Integer); virtual;  //Size 2
    procedure WriteShortString(const sValue : ShortString); virtual;
    procedure WriteRect(const ARect : TRect); virtual;
    procedure WritePointLL(pt : TPointLL; bHeight, b16Bit : Boolean); virtual;
    procedure WriteMER(const MER : TMER); virtual;
    procedure WriteComponent(Instance: TComponent);
    procedure WriteComponentRes(const ResName: string; Instance: TComponent);
    procedure WritePicture(Pic:TPicture);
    procedure WriteBirmap32(aBmp:TBitmap32);
    Procedure WriteColor(const ival:TColor);

    function  CopyFrom(Source: TStream; Count: Int64): Int64;
    property DataStream : TStream read FDataStream write FDataStream;
  end;


implementation
{------------------------------------------------------------------------------
 TEarthStreamReader.Create
------------------------------------------------------------------------------}

constructor TEarthStreamReader.Create(aStream : TStream);
begin
  inherited Create;
  DataStream := aStream;
end;

destructor TEarthStreamReader.Destroy;
begin
  DataStream := nil;
  inherited Destroy;
end;

Procedure TEarthStreamReader.ReadPicture(Pic:TPicture);
 Var img:Timage;
begin

 img:=Timage.Create(nil);
   try
    ReadComponent(img);
    if Pic<>nil then  Pic.Assign(img.Picture);
   finally
    img.Free;
  end;
end;

procedure TEarthStreamReader.ReadBirmap32(aBmp:TBitmap32);
 Var img:Timage;
begin   
 img:=Timage.Create(nil);
   try
    ReadComponent(img);
    if aBmp<>nil then  aBmp.Assign(img.Picture);
   finally
    img.Free;
  end;
end;

Function TEarthStreamReader.ReadColor:TColor;
 begin
   result:=StringToColor(ReadShortString);
 end;

function TEarthStreamReader.ReadComponent(Instance: TComponent): TComponent;
 begin
  result:=DataStream.ReadComponent(Instance);
 end;

function TEarthStreamReader.ReadComponentRes(Instance: TComponent): TComponent;
 begin
  result:=DataStream.ReadComponentRes(Instance);
 end;

procedure TEarthStreamReader.ReadBuffer(var Buffer; iSize : integer);
begin
  _Read(Buffer, iSize);
end;

function TEarthStreamReader.ReadBoolean : Boolean;
var
  Chr : Char;
begin
  _Read( Chr, 1);
  Result := Chr = 'T';
end;

function TEarthStreamReader.ReadDouble : Double;
begin
  _Read(Result, SizeOf(Double));
end;

function TEarthStreamReader.ReadWord : Word;
begin
  _Read(Result, SizeOf(Word));
end;

function TEarthStreamReader.ReadShortInt : ShortInt;
begin
  _Read(Result, SizeOf(ShortInt));
end;

function TEarthStreamReader.ReadSmallInt : Smallint;
begin
  _Read(Result, SizeOf(Smallint));
end;

function TEarthStreamReader.ReadInteger : Integer;
begin
  _Read(Result, SizeOf(Integer));
end;   

function TEarthStreamReader.ReadInteger16bit : Integer;
begin
  _Read(Result, 2);
end;

function TEarthStreamReader.ReadShortString : ShortString;
begin
  _Read(Result[0], 1);
  _Read(Result[1], Ord(Result[0]));
end;



function TEarthStreamReader.ReadRect : TRect;
begin
  Result.Left := ReadInteger;
  Result.Top := ReadInteger;
  Result.Right := ReadInteger;
  Result.Bottom := ReadInteger;
end;
  
function TEarthStreamReader.ReadPointLL(bHeight, b16Bit : Boolean) : TPointLL;
var iTmpX, iTmpY, iTmpH : Double;
begin
  iTmpH := 0;

if giFileVersion<TG_FILEVERSION3000 then //Old
  begin

      if b16Bit then
      begin
        iTmpX := ReadSmallInt * GU_MINUTE;
        iTmpY := ReadSmallInt * GU_MINUTE;
        if bHeight then
          iTmpH := ReadSmallInt * GU_MINUTE;
      end else
      begin
        iTmpX := ReadInteger;
        iTmpY := ReadInteger;
        if bHeight then iTmpH := ReadInteger;
      end;
      Result := PointLLH(iTmpX, iTmpY, iTmpH);

  end else                             //New  GaiaCAD 3.000
  begin

        iTmpX := ReadDouble;
        iTmpY := ReadDouble;
        if bHeight then iTmpH := ReadDouble;

        Result := PointLLH(iTmpX,iTmpY, iTmpH);
  end;
end;

function TEarthStreamReader.ReadMER : TMER;
begin
 if giFileVersion<TG_FILEVERSION3000 then   //Old
  begin
    Result.iLongX := ReadInteger;
    Result.iLatY := ReadInteger;
    Result.iWidthX := ReadInteger;
    Result.iHeightY := ReadInteger;
  end else                                  //New  GaiaCAD 3.000
  begin
    Result.iLongX := ReadDouble;
    Result.iLatY := ReadDouble;
    Result.iWidthX := ReadDouble;
    Result.iHeightY := ReadDouble;
  end;
end;
{------------------------------------------------------------------------------
  TEarthStreamWriter.Create
------------------------------------------------------------------------------}

constructor TEarthStreamWriter.Create(aStream : TStream);
begin
  inherited Create;
  DataStream := aStream;
end;

destructor TEarthStreamWriter.Destroy;
begin
 DataStream := nil;
 inherited Destroy;
end;

procedure TEarthStreamWriter.WritePicture(Pic:TPicture);
var img:Timage;
begin
  img:=Timage.Create(nil);
   try
    img.Picture:=Pic;
    WriteComponent(img);
   finally
  img.Free;
  end;

end;

procedure TEarthStreamWriter.WriteBirmap32(aBmp:TBitmap32);
var img:Timage;
begin
  img:=Timage.Create(nil);
   try
    img.Picture.Assign(aBmp);
    WriteComponent(img);
   finally
  img.Free;
  end;

end;

Procedure TEarthStreamWriter.WriteColor(const ival:TColor);
 begin
  WriteShortString(ColorToString(ival));
 end;

procedure TEarthStreamWriter.WriteComponent(Instance: TComponent);
 begin
  DataStream.WriteComponent(Instance);
 end;
procedure TEarthStreamWriter.WriteComponentRes(const ResName: string; Instance: TComponent);
 begin
  DataStream.WriteComponentRes(ResName,Instance);
 end;
function  TEarthStreamWriter.CopyFrom(Source: TStream; Count: Int64): Int64;
 begin
  result:=DataStream.CopyFrom(Source,Count);
 end;

procedure TEarthStreamWriter.WriteBuffer(const Buffer; iSize : integer);
begin
  _Write(Buffer, iSize);
end;


procedure TEarthStreamWriter.WriteBoolean(bVal : Boolean);
var
  Chr : Char;
begin
  Chr := 'FT'[Ord(bVal)+1];
  _Write( Chr, 1 );
end;


procedure TEarthStreamWriter.WriteDouble(Val : Double);
begin
  _Write(Val, SizeOf(Double));
end;


procedure TEarthStreamWriter.WriteWord(iVal : Word);
begin
  _Write(iVal, SizeOf(Word));
end;


procedure TEarthStreamWriter.WriteShortInt(iVal : ShortInt);
begin
  _Write(iVal, SizeOf(Shortint));
end;


procedure TEarthStreamWriter.WriteSmallInt(iVal : Smallint);
begin
  _Write(iVal, SizeOf(Smallint));
end;


procedure TEarthStreamWriter.WriteInteger(iVal : Integer);
begin
  _Write(iVal, SizeOf(Integer));
end; 


procedure TEarthStreamWriter.WriteInteger16bit(iVal : Integer);
begin
  _Write(iVal, 2);
end;


procedure TEarthStreamWriter.WriteShortString(const sValue : ShortString);
begin
  _Write(sValue, Length(sValue) + 1);
end;    

procedure TEarthStreamWriter.WritePointLL(pt : TPointLL; bHeight, b16Bit : Boolean);
begin
    WriteDouble(pt.iLongX);
    WriteDouble(pt.iLatY );
    if bHeight then WriteDouble(pt.iHeightZ);
end;

procedure TEarthStreamWriter.WriteRect(const ARect : TRect);
begin
  with ARect do
  begin
    WriteInteger(Left);
    WriteInteger(Top);
    WriteInteger(Right);
    WriteInteger(Bottom);
  end;
end;


procedure TEarthStreamWriter.WriteMER(const MER : TMER);
begin
  with MER do
  begin
    WriteDouble(iLongX);
    WriteDouble(iLatY);
    WriteDouble(iWidthX);
    WriteDouble(iHeightY);
  end;
end;

procedure TEarthStreamReader._Read(var Buffer; iSize : integer);
begin
  DataStream.Read(Buffer, iSize);
end;


procedure TEarthStreamWriter._Write(const Buffer; iSize : integer);
begin
  DataStream.Write(Buffer, iSize);
end;


end.
 
