
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_CustomEarthCanvas;

interface

uses
  LCLIntf, LCLType, Classes, SysUtils, Controls, Graphics, ExtCtrls,
  GR32, GIS_SysUtils, agg_rendering_buffer,
  agx_canvas, agg_pixfmt_rgba, agg_color;

type

  PAgxExImage = ^TAgxImage;

  TAgxExImage = object
  protected
    renBuf: rendering_buffer;
  public
    constructor Construct;
    destructor Destruct;
    function attach(bitmap: TBitmap; flip: boolean): boolean;
    function attach32(Bmp32: TBitmap32; flip: boolean): boolean;
    function Width: integer;
    function Height: integer;
  end;

  TAgxExCanvas = class(TAgxCanvas)
  protected
    fLinkBmp32: TBitmap32;
    renBuf: rendering_buffer;
  public
    function Attach(Bitmap: TBitmap; const flip_y: boolean = False): boolean; overload;
    function Attach32(Bmp32: TBitmap32; const flip_y: boolean = False): boolean;
    procedure TransformImagePath32(Bmp32: TBitmap32; imgX1, imgY1, imgX2, imgY2: integer; dstX1, dstY1, dstX2, dstY2: double);
    procedure PathFillWithImage32(Img: Tbitmap32);
    function ColorAgxToColor32(c: TAgxColor): TColor32;
    function Color32ToColorAgx(c: TColor32): TAgxColor;

    property LinkBmp32: TBitmap32 read fLinkBmp32;
  end;

  TCustomEarthCanvas = class(TAgxEXCanvas)
  private
    FiCanvasPixelsPerInch: double;
    FiScreenPixelsPerInch: double;
    FiColorAdjustPercent: integer;
    FiColorAdjustTarget: TColor;
    FiCanvasVersion: integer;

    //WARNING This is link not the original TBitmap32
    FBrushPattern32: TBitmap32;
    procedure SetColorAdjustPercent(const Value: integer);
    procedure SetColorAdjustTarget(const Value: TColor);
    function GetBitmap32DC: HDC;
    property DC: HDC read GetBitmap32DC;
  protected
    fgaPoints: PTPointDoubleArray;
    fgaClipPoints: PTPointDoubleArray;

    FBitmap: TBitmap32; //===== WARNING: This is link not the original TBitmap32 ==============

    function GetCanvasWidth: integer;
    procedure SetCanvasWidth(const val: integer);
    function GetCanvasHeight: integer;
    procedure SetCanvasHeight(const val: integer);
    function GetRGBLineWidth: integer;
    function GetFRGBStart: Pointer;
    function GetBitmapInfo: TBitmapInfo;
    function GetCanvas: TCanvas;
    function SetSize(NewWidth, NewHeight: integer): boolean;
  public
    constructor Create;
    destructor Destroy; override;

    //...........Windows Draw Functions .........................................

    procedure gDrawPixel(const X, Y: double; aColor: Tcolor);
    function gDrawPolygon(const Points: PTPointDoubleArray; const PointsNum: integer): boolean;
    function gDrawPolyline(const Points: PTPointDoubleArray; const PointsNum: integer): boolean;
    procedure gDrawEllipse(const X1, Y1, X2, Y2: double);
    function gDrawRectangle(const aLeft, aTop, aRight, aBottom: double): boolean;

    procedure gDrawText(X, Y: double; const aText: string);
    function gDrawTextOut(const X, Y: integer; const aText: string; const CharCount: integer): boolean; overload;

    //...........Picture Draw Functions .........................................

    procedure gDrawPicture(Src: TPicture; const DstRect: TRect);
    procedure gDrawBitMap32(const x, y: integer; Src: TBitMap32); overload;
    procedure gDrawBitMap32(Src: TBitMap32; const DstRect: TRect); overload;

    //.......................................................

    property gaPoints: PTPointDoubleArray read fgaPoints;
    property CanvasPixelsPerInch: double read FiCanvasPixelsPerInch write FiCanvasPixelsPerInch;
    property ScreenPixelsPerInch: double read FiScreenPixelsPerInch;
    property CanvasWidth: integer read GetCanvasWidth write SetCanvasWidth;
    property CanvasHeight: integer read GetCanvasHeight write SetCanvasHeight;
    property CanvasVersion: integer read FiCanvasVersion;
    property RGBStart: Pointer read GetFRGBStart;
    property RGBLineWidth: integer read GetRGBLineWidth;
    property BitmapInfo: TBitmapInfo read GetBitmapInfo;
    property Canvas: TCanvas read GetCanvas;
    property Bitmap: TBitmap32 read FBitmap;

    property BrushPattern32: TBitmap32 read FBrushPattern32 write FBrushPattern32;

  end;

implementation

//========================= TAggImage =====================================================

constructor TAgxExImage.Construct;
begin
  renBuf.Construct;
end;

destructor TAgxExImage.Destruct;
begin
  renBuf.Destruct;
end;

function TAgxExImage.attach(bitmap: TBitmap; flip: boolean): boolean;
var
  buffer: pointer;
  stride: integer;
begin
  Result := False;

  if Assigned(bitmap) and not bitmap.Empty then
    case bitmap.PixelFormat of
      pf32bit:
      begin
        stride := PtrInt(bitmap.ScanLine[1]) - PtrInt(bitmap.ScanLine[0]);

        if stride < 0 then
          buffer := bitmap.ScanLine[bitmap.Height - 1]
        else
          buffer := bitmap.ScanLine[0];

        if flip then
          stride := stride * -1;

        renBuf.attach(buffer, bitmap.Width, bitmap.Height, stride);
        Result := True;
      end;
    end;
end;

function TAgxExImage.attach32(Bmp32: TBitmap32; flip: boolean): boolean;
var
  buffer: pointer;
  stride: integer;
begin
  Result := False;

  if Assigned(Bmp32) and not Bmp32.Empty then
  begin
    stride := PtrInt(Bmp32.ScanLine[1]) - PtrInt(Bmp32.ScanLine[0]);

    if stride < 0 then
      buffer := Bmp32.ScanLine[Bmp32.Height - 1]
    else
      buffer := Bmp32.ScanLine[0];

    if flip then
      stride := stride * -1;

    renBuf.attach(buffer, Bmp32.Width, Bmp32.Height, stride);
    Result := True;
  end;

end;

function TAgxExImage.Width: integer;
begin
  Result := renBuf._width;
end;

function TAgxExImage.Height: integer;
begin
  Result := renBuf._height;
end;

//===================== TAgxExCanvas =================================================

function TAgxExCanvas.Attach(Bitmap: TBitmap; const flip_y: boolean = False): boolean;
begin
  fLinkBmp32 := nil;
  inherited;
end;

function TAgxExCanvas.attach32(Bmp32: TBitmap32; const flip_y: boolean = False): boolean;
var
  buffer: pointer;
  stride: integer;
begin
  Result := False;
  fLinkBmp32 := Bmp32;
  fLinkBmp := nil;

  if Assigned(Bmp32) and not Bmp32.Empty then
  begin
    fWidth := Bmp32.Width;
    fHeight := Bmp32.Height;

    stride := PtrInt(Bmp32.ScanLine[1]) - PtrInt(Bmp32.ScanLine[0]);

    if stride < 0 then
      buffer := Bmp32.ScanLine[Bmp32.Height - 1]
    else
      buffer := Bmp32.ScanLine[0];

    if flip_y then
      stride := stride * -1;

    m_rbuf.attach(buffer, fWidth, fHeight, stride);

    m_pixf := pf32bit;

    pixfmt_rgba32(m_pixFormat, @m_rbuf);
    pixfmt_custom_blend_rgba(m_pixFormatComp, @m_rbuf, @comp_op_adaptor_rgba, rgba_order);
    pixfmt_rgba32(m_pixFormatPre, @m_rbuf);
    pixfmt_custom_blend_rgba(m_pixFormatCompPre, @m_rbuf, @comp_op_adaptor_rgba, rgba_order);


    { Reset state }
    m_renBase.reset_clipping(True);
    m_renBaseComp.reset_clipping(True);
    m_renBasePre.reset_clipping(True);
    m_renBaseCompPre.reset_clipping(True);

    TransformationsReset;

    LineWidthSet(1.0);
    LineColorSet(0, 0, 0);
    FillColorSet(255, 255, 255);

    TextAlignmentSet(AGX_AlignLeft, AGX_AlignBottom);

    ClipBoxSet(0, 0, Bmp32.Width, Bmp32.Height);
    LineCapSet(AGX_CapRound);
    LineJoinSet(AGX_JoinRound);
    FlipText(False);

    ImageFilterSet(AGX_Bilinear);
    ImageResampleSet(AGX_NoResample);
    ImageFlip(False);

    m_masterAlpha := 1.0;
    m_antiAliasGamma := 1.0;

    m_rasterizer.gamma(@m_gammaNone);

    m_blendMode := AGX_BlendAlpha;

    FillEvenOddSet(False);
    BlendModeSet(AGX_BlendAlpha);

    FlipText(False);
    ResetPath;

    ImageFilterSet(AGX_Bilinear);
    ImageResampleSet(AGX_NoResample);

    viewport(0, 0, fwidth, fheight, 0, 0, fwidth, fheight, AGX_XMidYMid);
    { OK }
    Result := True;

  end;
end;


procedure TAgxExCanvas.TransformImagePath32(Bmp32: TBitmap32; imgX1, imgY1, imgX2, imgY2: integer; dstX1, dstY1, dstX2, dstY2: double);
var
  parall: array[0..5] of double;
  image: TAgxExImage;
begin
  image.Construct;
  if image.attach32(Bmp32, m_imageFlip) then
  begin
    parall[0] := dstX1;
    parall[1] := dstY1;
    parall[2] := dstX2;
    parall[3] := dstY1;
    parall[4] := dstX2;
    parall[5] := dstY2;
    renderImage(@image, imgX1, imgY1, imgX2, imgY2, @parall[0]);
    image.Destruct;
  end;
end;

procedure TAgxExCanvas.PathFillWithImage32(Img: Tbitmap32);
var
  x1, y1, x2, y2: double;
begin
  if Img = nil then
    exit;
  PathGetBoundRect(x1, y1, x2, y2);
  TransformImagePath32(Img, 0, 0, Img.Width, Img.Height, x1, y1, x2, y2);
end;

function TAgxExCanvas.ColorAgxToColor32(c: TAgxColor): TColor32;
begin
  Result := color32(c.r, c.g, c.b, c.a);
end;

function TAgxExCanvas.Color32ToColorAgx(c: TColor32): TAgxColor;
var
  R, G, B, A: byte;
begin
  R := RedComponent(c);
  G := GreenComponent(c);
  B := BlueComponent(c);
  A := AlphaComponent(c);
  Result.Construct(R, G, B, A);
end;
//=========================== TCustomEarthCanvas =========================

constructor TCustomEarthCanvas.Create;
var
  xDC: HDC;
begin
  inherited Create;
  fgaPoints := AllocMem(SizeOf(TPointArray)); // allocate the working point array
  fgaClipPoints := AllocMem(SizeOf(TPointArray)); // allocate the working point array

  FBitmap := nil;
  xDC := GetDC(0);
  FiScreenPixelsPerInch := GetDeviceCaps(xDC, LOGPIXELSY);
  FiCanvasPixelsPerInch := FiScreenPixelsPerInch;
  ReleaseDC(0, xDC);
  FiColorAdjustPercent := 0;
  FiColorAdjustTarget := clSilver;

  FBrushPattern32 := nil;
end;

destructor TCustomEarthCanvas.Destroy;
begin
  FBitmap := nil;
  Finalize(fgaPoints);
  FreeMem(fgaPoints, SizeOf(TPointArray)); // Free the working point array
  Finalize(fgaClipPoints);
  FreeMem(fgaClipPoints, SizeOf(TPointArray)); // Free the working point array
  inherited Destroy;
end;

function TCustomEarthCanvas.GetCanvasWidth: integer;
begin
  Result := 0;
  Result := FBitmap.Width;
end;

procedure TCustomEarthCanvas.SetCanvasWidth(const val: integer);
begin
  FBitmap.Width := val;
end;

function TCustomEarthCanvas.GetCanvasHeight: integer;
begin
  Result := 0;
  Result := FBitmap.Height;
end;

procedure TCustomEarthCanvas.SetCanvasHeight(const val: integer);
begin
  FBitmap.Height := val;
end;

function TCustomEarthCanvas.GetBitmap32DC: HDC;
begin
  Result := 0;
  Result := FBitmap.Canvas.Handle;
end;

function TCustomEarthCanvas.GetRGBLineWidth: integer;
begin
  Result := 0;
  Result := ((FBitmap.Width * BitmapInfo.bmiHeader.biBitCount + 31) shr 5) shl 2;
end;

function TCustomEarthCanvas.GetFRGBStart: Pointer;
begin
  Result := nil;
  Result := FBitmap.Bits;
end;

function TCustomEarthCanvas.GetBitmapInfo: TBitmapInfo;
begin
  Result := FBitmap.BitmapInfo;
end;

function TCustomEarthCanvas.GetCanvas: TCanvas;
begin
  Result := nil;
  Result := FBitmap.Canvas;
end;

function TCustomEarthCanvas.SetSize(NewWidth, NewHeight: integer): boolean;
begin
  FBitmap.SetSize(NewWidth, NewHeight);
end;

procedure TCustomEarthCanvas.SetColorAdjustPercent(const Value: integer);
begin
  if FiColorAdjustPercent <> Value then
  begin
    FiColorAdjustPercent := Value;
    Inc(FiCanvasVersion);
  end;
end;

procedure TCustomEarthCanvas.SetColorAdjustTarget(const Value: TColor);
begin
  if FiColorAdjustTarget <> Value then
  begin
    FiColorAdjustTarget := Value;
    Inc(FiCanvasVersion);
  end;
end;

//=========================================================================================
//...................... Picture Draw Functions ...........................................
//=========================================================================================
procedure TCustomEarthCanvas.gDrawPicture(Src: TPicture; const DstRect: TRect);
var
  b32: TbitMap32;
begin
  if Src = nil then
    exit;
  b32 := TbitMap32.Create;
  try
    b32.Assign(Src);
    fBitmap.Draw(DstRect,
      Rect(0, 0, b32.Width, b32.Height),
      b32);
  finally
    b32.Free;
  end;

end;

procedure TCustomEarthCanvas.gDrawBitMap32(const x, y: integer; Src: TBitMap32);
begin
  if Src = nil then
    exit;
  try
    fBitmap.Draw(x, y, Src);
  finally
  end;
end;

procedure TCustomEarthCanvas.gDrawBitMap32(Src: TBitMap32; const DstRect: TRect);
begin
  if Src = nil then
    exit;
  try
    fBitmap.Draw(DstRect, Rect(0, 0, Src.Width, Src.Height), Src);
  finally
  end;

end;
//=========================================================================================
//......................  Draw Functions............................................
//=========================================================================================

procedure TCustomEarthCanvas.gDrawPixel(const X, Y: double; aColor: Tcolor);
begin
  fBitmap.SetPixelTS(Round(X), Round(Y), Color32(aColor));
end;

procedure TCustomEarthCanvas.gDrawEllipse(const X1, Y1, X2, Y2: double);
begin
  Ellipse(X1, Y1, X2, Y2);
end;

function TCustomEarthCanvas.gDrawPolyline(const Points: PTPointDoubleArray; const PointsNum: integer): boolean;
begin
  Polyline(@Points^[0].x, PointsNum);
end;

function TCustomEarthCanvas.gDrawPolygon(const Points: PTPointDoubleArray; const PointsNum: integer): boolean;
begin
  Polygon(@Points^[0].x, PointsNum);
end;

function TCustomEarthCanvas.gDrawRectangle(const aLeft, aTop, aRight, aBottom: double): boolean;
begin
  self.Rectangle(aLeft, aTop, aRight, aBottom);
end;

procedure TCustomEarthCanvas.gDrawText(X, Y: double; const aText: string);
begin
  self.Text(X, Y, aText);
end;

function TCustomEarthCanvas.gDrawTextOut(const X, Y: integer; const aText: string; const CharCount: integer): boolean;

begin
  self.Text(X, Y, aText);
end;

//================================================================
end.
