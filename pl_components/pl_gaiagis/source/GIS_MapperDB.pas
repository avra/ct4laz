
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_MapperDB;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType,
  Classes, SysUtils, DB, GIS_EarthBase, GIS_SysUtils,
  GIS_EarthObjects, GIS_Presenters, GIS_Classes, GIS_XML;

const
  TG_DBPOINT_READERVERSION = $0101;

type
  TReaderDataLink = class;

  //---------------------------- TEarthDBReader --------------------------------
  //
  TEarthDBReader = class(TEarthObjectStore)
  private
    FDataLink : TReaderDataLink;
    function GetDataSource: TDataSource;
    procedure SetDataSource(const Value: TDataSource);
  protected
    IDArray : DynArray;
    procedure MapDataSource; virtual; abstract;
  public
    constructor Create(aLayer:TEarthLayer); override;
    destructor Destroy; override;

    property DataSource : TDataSource read GetDataSource write SetDataSource;
  end;

  TReaderDataLink = class(TDataLink)
  private
    FDBReader: TEarthDBReader;
  protected
    procedure ActiveChanged; override;
  public
    constructor Create(aDBReader: TEarthDBReader);
    destructor Destroy; override;
  end;

  //---------------------------- TEarthDBPointReader ---------------------------
  //
  TEarthDBPointReader = class(TEarthDBReader)
  private
    FLatitudeColName: string;
    FLongitudeColName: string;
    FIDColName: string;
    FTitleColName: string;

    iLongitudeColumn : integer;
    iLatitudeColumn : integer;
    iIDColumn : integer;
    iTitleColumn : integer;
  protected
    procedure MapDataSource; override;
    function InternalOpen : Boolean; override;
    function InternalClose : Boolean; override;
  public
    function LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject; override;

    function SaveEnvironment : TGXML_Element; override;
    procedure LoadEnvironment( Element : TGXML_Element ); override;

    property IDColName : string read FIDColName write FIDColName;
    property LongitudeColName : string read FLongitudeColName write FLongitudeColName;
    property LatitudeColName : string read FLatitudeColName write FLatitudeColName;
    property TitleColName : string read FTitleColName write FTitleColName;
  end;

implementation

//========================================================================
//============== TReaderDataLink ========================================
//======================================================================

procedure TReaderDataLink.ActiveChanged;
begin
  if ( FDBReader <> nil ) and ( DataSource <> nil )
      and ( DataSource.DataSet <> nil ) then
    FDBReader.Active := DataSource.DataSet.Active;
end;

constructor TReaderDataLink.Create(aDBReader: TEarthDBReader);
begin
  inherited Create;

  FDBReader := aDBReader;
//  VisualControl := True;
end;

destructor TReaderDataLink.Destroy;
begin
  FDBReader := nil;
  inherited Destroy;
end;

//========================================================================
//=================== TEarthDBReader ====================================
//========================================================================

constructor TEarthDBReader.Create(aLayer:TEarthLayer);
begin
  inherited;
  IDArray := DynArrayCreate( SizeOf( integer ), 0 );
  FDataLink := TReaderDataLink.Create( Self );
end;


destructor TEarthDBReader.Destroy;
begin
  FDataLink.DataSource.DataSet.Active := False;
  FDataLink.DataSource := nil;
  FDataLink.Free;
  FDataLink := nil;
  DynArrayFree( IDArray );
  inherited;
end;

function TEarthDBReader.GetDataSource: TDataSource;
begin
  if FDataLink <> nil then
    Result := FDataLink.DataSource
  else
    Result := nil;
end;

procedure TEarthDBReader.SetDataSource(const Value: TDataSource);
begin
  FDataLink.DataSource := Value;
end;

procedure TEarthDBPointReader.LoadEnvironment(Element: TGXML_Element);
begin
  inherited;

end;

//=====================================================================
//====================== TEarthDBPointReader ==========================
//=====================================================================

function TEarthDBPointReader.LoadObject(iIndex: integer;
  bNewObject: Boolean): TEarthObject;
var
  iID : integer;
begin
  Result := nil;
  iID := DynArrayAsInteger( IDArray, iIndex );
  with DataSource.DataSet do
    if ( Fields[iIDColumn].AsInteger = iID ) or Locate( IDColName, iID, [] ) then
    begin
      Result := TGeoDataObject.Create( Self );
      Result.Centroid := DecimalToPointLL(
        Fields[iLongitudeColumn].AsFloat,
        Fields[iLatitudeColumn].AsFloat );
    end;
end;

procedure TEarthDBPointReader.MapDataSource;
begin
  Count := 0;
  with DataSource.DataSet do
  begin
    DisableControls;
    try
      Earth.ProgressMessage( pmStart, 'Mapping');
      First;
      while not EOF do
      begin
        Count := Count + 1;
        if Count > IDArray.Count then
          DynArraySetLength( IDArray, Count + 32 );

        DynArraySetAsInteger( IDArray, Count - 1, Fields[iIDColumn].AsInteger );
        EarthObject[Count - 1];  // Load in the object

        Next;
      end;
    finally
      EnableControls;
      Earth.ProgressMessage( pmEnd, 'Finshed');
    end;
  end;
end;

function TEarthDBPointReader.SaveEnvironment: TGXML_Element;
begin
  Result := nil;
end;

function TEarthDBPointReader.InternalOpen : Boolean;
begin
  if ( DataSource = nil ) or ( DataSource.DataSet = nil ) then
    raise EEarthException.Create( 'TEarthDBReader: Invalid DataSource');
  if ( IDColName = '' ) or ( LongitudeColName = '' ) or ( LatitudeColName = '' ) then
    raise EEarthException.Create( 'TEarthDBReader: Missing Property');

  Name := DataSource.DataSet.Name;
  with DataSource.DataSet do
  begin
    Active := True;
    Result := Active;
    if Result then
    begin
      iIDColumn := FieldByName( IDColName ).Index;
      iLongitudeColumn := FieldByName( LongitudeColName ).Index;
      iLatitudeColumn := FieldByName( LatitudeColName ).Index;
      if TitleColName <> '' then
        iTitleColumn := FieldByName( TitleColName ).Index;
      MapDataSource;
      Result := inherited InternalOpen;
    end;
  end;
end;

function TEarthDBPointReader.InternalClose : Boolean;
begin
  if ( DataSource <> nil ) and ( DataSource.DataSet <> nil ) then
    DataSource.DataSet.Active := False;
  
  Result := inherited InternalClose;
end;

end.
