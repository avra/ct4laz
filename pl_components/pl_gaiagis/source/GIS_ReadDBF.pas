
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_ReadDBF;



interface

uses
  SysUtils, Classes;

{-------------------------------------------------------------------------}
type
  EDBFException = class(Exception);

  TDBFField = record
    FName: array [0..10] of char;
    FType: char;
    FData: PChar;
    FLen: byte;
    FCount: byte;
    FReserved: array [0..13] of char;
  end;
  TDBFFieldArray = array [0..255] of TDBFField;
  PTDBFFieldArray = ^TDBFFieldArray;

  TDBF = class(TObject)
  private
    iRecordID: longint;
    iHeaderLen: smallint;
    iRecordLen: smallint;
    DBFstream: TStream;
    Data: PChar;
    function GetFName(iField: integer): string;
    function GetFType(iField: integer): char;
    function GetFLength(iField: integer): integer;
    function GetString(iField: integer): string;
    function GetNumeric(iField: integer): extended;
    function GetDate(iField: integer): TDateTime;
    function GetLogical(iField: integer): char;
    procedure SetID(iRecord: longint);
  public
    iRecordCount: longint;
    iFieldCount: integer;
    Fields: PTDBFFieldArray;
    constructor Create(AStream: TStream);
    destructor Destroy; override;
    function First: boolean;
    function Next: boolean;
    property FName[iField: integer]: string read GetFName;
    property FType[iField: integer]: char read GetFType;
    property FLength[iField: integer]: integer read GetFLength;
    property AsString[iField: integer]: string read GetString;
    property AsNumeric[iField: integer]: extended read GetNumeric;
    property AsDate[iField: integer]: TDateTime read GetDate;
    property AsLogical[iField: integer]: char read GetLogical;
    property RecordID: longint read iRecordID write SetID;
  end;

  {-------------------------------------------------------------------------}
implementation

{-------------------------------------------------------------------------}
constructor TDBF.Create(AStream: TStream);
var
  cTmp: byte;
  idx, iTmp: integer;
begin
  Astream.Position := 0;
  Astream.Read(cTmp, 1);
  if (cTmp <> $03) and (cTmp <> $83) then
    raise EDBFException.Create('Unsupported DBase III version in Stream');

  DBFstream := AStream;

  for idx := 1 to 3 do
    DBFstream.Read(cTmp, 1);

  DBFstream.Read(iRecordCount, SizeOf(longint));
  DBFstream.Read(iHeaderLen, SizeOf(smallint));
  DBFstream.Read(iRecordLen, SizeOf(smallint));
  GetMem(Data, iRecordLen);

  for idx := 1 to 20 do
    DBFstream.Read(cTmp, 1);

  GetMem(Fields, iHeaderLen - 32);
  DBFstream.Read(Fields^, iHeaderLen - 32);

  iFieldCount := 0;
  iTmp := 1;
  repeat
    Fields^[iFieldCount].FData := Data + iTmp;
    Inc(iTmp, Fields^[iFieldCount].FLen);
    Inc(iFieldCount);
  until Ord(Fields^[iFieldCount].FName[0]) in [$0, $0D];

  First;
end;

{-------------------------------------------------------------------------}
destructor TDBF.Destroy;
begin
  if Data <> nil then
    FreeMem(Data, iRecordLen);
  if Fields <> nil then
    FreeMem(Fields, iHeaderLen - 32);
  inherited Destroy;
end;

{-------------------------------------------------------------------------}
function TDBF.First: boolean;
begin
  iRecordID := 1;
  Result := DBFstream <> nil;
  if Result then
    DBFstream.Position := iHeaderLen;
end;

{-------------------------------------------------------------------------}
function TDBF.Next: boolean;
begin
  Result := False;
  if DBFstream <> nil then
  begin
    repeat
      Inc(iRecordID);
      if DBFstream.Read(Data^, iRecordLen) <> iRecordLen then
        Exit;
    until Data[0] <> '*';
    Result := True;
  end;
end;

{-------------------------------------------------------------------------}
function TDBF.GetFName(iField: integer): string;
begin
  Result := StrPas(Fields^[iField].FName);
end;

{-------------------------------------------------------------------------}
function TDBF.GetFType(iField: integer): char;
begin
  Result := Fields^[iField].FType;
end;

{-------------------------------------------------------------------------}
function TDBF.GetFLength(iField: integer): integer;
begin
  Result := Fields^[iField].FLen;
end;

{-------------------------------------------------------------------------}
function TDBF.GetString(iField: integer): string;
begin
  Result := Trim(Copy(StrPas(Fields^[iField].FData), 1, Fields^[iField].FLen));
end;

{-------------------------------------------------------------------------}
function TDBF.GetNumeric(iField: integer): extended;
begin
  if Fields^[iField].FType <> 'N' then
    raise EDBFException.CreateFmt('Field %d is not a Numeric', [iField]);
  Result := StrToFloat(GetString(iField))
end;

{-------------------------------------------------------------------------}
function TDBF.GetDate(iField: integer): TDateTime;
var
  sTmp: string;
begin
  if Fields^[iField].FType <> 'D' then
    raise EDBFException.CreateFmt('Field %d is not a Date', [iField]);

  sTmp := GetString(iField);
  Result := EncodeDate(StrToInt(Copy(sTmp, 1, 4)), StrToInt(Copy(sTmp, 5, 2)),
    StrToInt(Copy(sTmp, 7, 2)));
end;

{-------------------------------------------------------------------------}
function TDBF.GetLogical(iField: integer): char;
begin
  if Fields^[iField].FType <> 'L' then
    raise EDBFException.CreateFmt('Field %d is not a Logical', [iField]);
  Result := Fields^[iField].FData[1]
end;

{-------------------------------------------------------------------------}
procedure TDBF.SetID(iRecord: longint);
begin
  if iRecord > 0 then
  begin
    Dec(iRecord);
    DBFstream.Position := iHeaderLen + iRecordLen * iRecord;
    iRecordID := iRecord;
  end;
end;

end.
