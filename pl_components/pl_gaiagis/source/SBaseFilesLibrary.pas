
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit SBaseFilesLibrary;

{.$DEFINE USEZIP}

interface

uses
  SysUtils, Classes;

type

  TxFiles = class;
  TxFile = class;

  TBaseFilesLibrary = class(TComponent)
  private
    FFiles: TxFiles;
    procedure SetFiles(Files: TxFiles);
    function GetSize: Integer;
    procedure SetSize(NewSize: Integer);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SaveToPath(PathName: string);
    function ItemIndex(ItemName: string): Integer;
    function GetResByName(ResName: string): TxFile;
    function GetResByIndex(Index: Integer): TxFile;
    Function GetFileAsStream(Filename: string):tstream;
    Procedure Clear;
  published
    property Files: TxFiles read FFiles write SetFiles;
    property Size: Integer read GetSize write SetSize;
  end;

  TxFiles = class(TCollection)
  private
    FOwner: TPersistent;
    function GetItem(Idx: Integer): TxFile;
    procedure SetItem(Idx: Integer; Item: TxFile);
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(aOwner: TPersistent);
    property Items[Idx: Integer]: TxFile read GetItem write SetItem; default;
  end;

  TStoreMode = (smStore{$IFDEF USEZIP}, smZipLow, smZipMed, smZipMax{$ENDIF});

  {A single file of CustomFilesLibrary}
  TxFile = class(TCollectionItem)
  private
    FData: TMemoryStream;
    FStoreMode: TStoreMode;
    FName: string;
    procedure SetStoreMode(StoreMode: TStoreMode);
    procedure ReadData(Stream: TStream);
    procedure WriteData(Stream: TStream);
    function  GetSize: Integer;
    procedure SetSize(NewSize: Integer);
    procedure SetName(Name: string);
  protected
    function  GetDisplayName: string; override;
    procedure DefineProperties(Filer: TFiler); override;
  public
    constructor Create(aCollection: TCollection); override;
    destructor Destroy; override;
    procedure  Assign(Source: TPersistent); override;
     {Save Stored file to given Stream}
    procedure SaveToStream(Stream: TStream);
     {Load file from given Stream}
    procedure LoadFromStream(Stream: TStream);
     {Save Stored file to given FileName}
    procedure SaveToFile(FileName: string);
     {Load file from given FileName}
    procedure LoadFromFile(FileName: string);
    function GetAsStream: TStream;
     {Return stored file as TStream}
    property AsStream: TStream read GetAsStream;
  published
    property StoreMode: TStoreMode read FStoreMode write SetStoreMode ;
      {The Size of Stored file}
    property Size: Integer read GetSize write SetSize;
      {The Name of Stored file}
    property Name: string read FName write SetName;
  end;   

implementation

{$I SBaseFilesLibrary.inc}

{$IFDEF USEZIP}
uses ZLib ;
{$ENDIF}



const

  msgRessourceNotFound = 'Ressource "%s" not found';
  msgNameAlreadyExists = 'Item name "%s" already exists !';


{ ****************************** TBaseFilesLibrary ****************************** }

constructor TBaseFilesLibrary.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFiles:= TxFiles.Create(self); 
end;

destructor TBaseFilesLibrary.Destroy;
begin
  FFiles.Free;
  inherited Destroy;
end;

Procedure TBaseFilesLibrary.Clear;
 begin
   FFiles.Clear;
 end;

function TBaseFilesLibrary.ItemIndex(ItemName: string): Integer;
var i: Integer;
begin
  Result:= -1; i:= 0;
  while (i < FFiles.Count) and (CompareText(ItemName, TxFile(FFiles.Items[i]).Name) <> 0) do
    Inc(i);
  if i < FFiles.Count then
    Result:= i;
end;

function TBaseFilesLibrary.GetResByName(ResName: string): TxFile;
var Idx: Integer;
begin
  result:=nil;
  Idx:= ItemIndex(ResName);
  if Idx <> -1 then
    Result:= TxFile(FFiles.Items[Idx])
end;

function TBaseFilesLibrary.GetResByIndex(Index: Integer): TxFile;
begin
  Result:= Files[Index];
end;

function TBaseFilesLibrary.GetSize: Integer;
var i: Integer;
begin
  Result:= 0;
  if FFiles.Count>0 then
  for i:= 0 to FFiles.Count - 1 do
    Result:= Result + TxFile(FFiles.Items[i]).Size;
end;

procedure TBaseFilesLibrary.SetSize(NewSize: Integer); // ReadOnly property
begin
end;

procedure TBaseFilesLibrary.SetFiles(Files: TxFiles);
begin
  FFiles.Assign(Files);
end;

procedure TBaseFilesLibrary.SaveToPath(PathName: string);
var i: Integer;
begin
  if (PathName <> '') and (PathName[Length(PathName)] <> '\') then
    PathName:= PathName + '\';
  for i:= 0 to Files.Count - 1 do
    Files[i].SaveToFile(PathName + Files[i].Name);
end;

Function TBaseFilesLibrary.GetFileAsStream(Filename: string):tstream;
 var ff:TxFile;
 begin
   result:=nil;
   ff:=GetResByName(Filename);
   if ff<>nil then
    begin
     result:=ff.AsStream;
    end;
 end;

{ ******************************** TxFile ******************************** }

constructor TxFile.Create(aCollection: TCollection);
begin
  FStoreMode:= smstore;  
 // {$IFDEF USEZIP}FStoreMode:= smZipMax;{$ENDIF}
  inherited Create(aCollection);
  FData:= TMemoryStream.Create;
end;

destructor TxFile.Destroy;
begin
  FData.Free; FData:= nil;
  inherited Destroy;
end;

procedure TxFile.Assign(Source: TPersistent);
begin
  if Source is TxFile then
  begin
    with TxFile(Source) do
    begin
      Self.Name:= Name;
      Self.FData.Clear;
      Self.StoreMode:= StoreMode;
      Self.FData.LoadFromStream(FData);
    end;
  end else AssignTo(Source);
end;

{$IFDEF USEZIP}
procedure TxFile.SaveToStream(Stream: TStream);
var S: TDecompressionStream;
    L: Integer;
begin
  FData.Position:= 0;
  if (StoreMode <> smStore) and (FData.Size > 0) then
  begin
    S:= TDecompressionStream.Create(FData);
    try
      S.Read(L, SizeOf(Integer));
      if L <> 0 then
        Stream.CopyFrom(S, L);
    finally
      S.Free;
    end;
  end else
    Stream.CopyFrom(FData, FData.Size);
end;

procedure TxFile.LoadFromStream(Stream: TStream);
var S: TCompressionStream;
    L: TCompressionLevel;
    Tmp: Integer;
begin
  case StoreMode of
    smZipLow: L:= clFastest;
    smZipMed: L:= clDefault;
    smZipMax: L:= clMax
    else L:= clNone;
  end;
  Stream.Position:= 0;
  FData.Clear;
  if Stream.Size = 0 then
    Exit;
  if L <> clNone then
  begin
    S:= TCompressionStream.Create(L, FData);
    try
      Tmp:= Stream.Size;
      S.Write(Tmp, SizeOf(Integer));
      S.CopyFrom(Stream, Tmp);
    finally
      S.Free;
    end;
  end else
    FData.CopyFrom(Stream, 0);
end;
{$ELSE}
procedure TxFile.SaveToStream(Stream: TStream);
begin
  FData.Position:= 0;
  Stream.CopyFrom(FData, 0);
end;

procedure TxFile.LoadFromStream(Stream: TStream);
begin
  FData.CopyFrom(Stream, 0);
end;
{$ENDIF}

procedure TxFile.SaveToFile(FileName: string);
var F: TFileStream;
begin
  F:= TFileStream.Create(FileName, fmCreate);
  try
    SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TxFile.LoadFromFile(FileName: string);
var F: TFileStream;
begin
  F:= TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  try
    Name:=ExtractFileName(FileName);
    LoadFromStream(F);

  finally
    F.Free;
  end;
end;

function TxFile.GetAsStream: TStream;
begin
  SetStoreMode(smStore);
  FData.Position:= 0;
  Result:= FData;
end;

procedure TxFile.SetStoreMode(StoreMode: TStoreMode);
{$IFDEF USEZIP}
var M: TMemoryStream;
begin
  if csLoading in TBaseFilesLibrary(TxFiles(GetOwner).GetOwner).ComponentState then
    FStoreMode:= StoreMode
  else if StoreMode <> FStoreMode then
  begin
    M:= TMemoryStream.Create;
    try
      SaveToStream(M);
      FStoreMode:= StoreMode;
      LoadFromStream(M);
    finally
      M.Free;
    end;
  end;
end;
{$ELSE}
begin
  FStoreMode:= smStore;
end;
{$ENDIF}

function TxFile.GetDisplayName: string;
begin
  if FData.Size = 0 then
    Result:= ClassName
  else
    Result:= FName;
end;

function TxFile.GetSize: Integer;
begin
  Result:= FData.Size;
end;

procedure TxFile.SetSize(NewSize: Integer);
begin end;

procedure TxFile.SetName(Name: string);
var i: Integer;
begin
  for i:= 0 to Collection.Count - 1 do
    if (Collection.Items[i].ID <> ID) and (CompareText(Name, TxFile(Collection.Items[i]).Name) = 0) then
     begin
      free;
      exit;
     end;

FName:= Name;
end;

procedure TxFile.ReadData(Stream: TStream);
var L: Integer;
begin
  FData.Clear;
  Stream.Read(L, SizeOf(Integer));
  FData.Size:= L; { Allocates what's needed }
  FData.CopyFrom(Stream, L);
end;

procedure TxFile.WriteData(Stream: TStream);
var L: Integer;
begin
  FData.Position:= 0;
  L:= FData.Size;
  Stream.Write(L, SizeOf(Integer));
  Stream.CopyFrom(FData, FData.Size);
end;

procedure TxFile.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineBinaryProperty('ResData', @ReadData, @WriteData, Assigned(FData) and (FData.Size > 0));
end;

{ ***************************** TxFiles ***************************** }

function TxFiles.GetOwner: TPersistent;
begin
  Result:= FOwner;
end;

function TxFiles.GetItem(Idx: Integer): TxFile;
begin
  Result:= TxFile(inherited Items[Idx]);
end;

procedure TxFiles.SetItem(Idx: Integer; Item: TxFile);
begin
  inherited Items[Idx]:= Item;
end;

constructor TxFiles.Create(aOwner: TPersistent);
begin
  FOwner:= aOwner;
  inherited Create(TxFile);
end;

initialization
   RegisterClasses([
                   TBaseFilesLibrary
                   ]);

end.
