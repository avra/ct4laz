
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_PresentersShapes;



interface
 
uses LCLIntf, LCLType, LMessages, Classes, Graphics, SysUtils,
     GIS_EarthBase, GIS_Classes, GIS_EarthObjects, GIS_SysUtils, GIS_XML, GIS_Resource,
     GIS_EarthStreams,GIS_Presenters;

type


TDotPresenter = class(TCustomDrawBrushPresenter)
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    procedure Drawshape; override;
  published
    property ShapePen;
  end;

TCirclePresenter = class(TCustomDrawBrushPresenter)
  public
    procedure Drawshape; override;  
  published
    property ShapeUnit;
    property ShapeSize;
    property MaxPixelsSize;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

TOvalPresenter = class(TCustomDrawBrushPresenter)
  public
    procedure Drawshape; override;  
  published
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

TRectPresenter = class(TCustomDrawBrushPresenter)
  public
    procedure Drawshape; override;
   published
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

TDiamondPresenter = class(TCustomDrawBrushPresenter)
  public
    procedure Drawshape; override;
   published
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

TExagonPresenter = class(TCustomDrawBrushPresenter)
  public  
    procedure Drawshape; override;
   published
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

TTrianglePresenter = class(TCustomDrawBrushPresenter)
  public
    procedure Drawshape; override;
   published
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

TRoundRectPresenter = class(TCustomDrawBrushPresenter)
  protected
    FRoundFactorTopX:Double;
    FRoundFactorTopY:Double;
    FRoundFactorBottomX:Double;
    FRoundFactorBottomY:Double;
    procedure SetRoundFactorTopX(Const Value:Double);
    procedure SetRoundFactorTopY(Const Value:Double);
    procedure SetRoundFactorBottomX(Const Value:Double);
    procedure SetRoundFactorBottomY(Const Value:Double);
  public
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    procedure  Assign(Source: TPersistent); override;
    procedure  LoadEnvironment(Element: TGXML_Element); override;
    function   SaveEnvironment: TGXML_Element; override;
    procedure  WriteProperties(Writer: TEarthStreamWriter); override;
    procedure  ReadProperties(Reader: TEarthStreamReader); override;
    procedure  Drawshape; override;
  published
    Property RoundFactorTopX:Double read FRoundFactorTopX write SetRoundFactorTopX;
    Property RoundFactorTopY:Double read FRoundFactorTopY write SetRoundFactorTopY;
    Property RoundFactorBottomX:Double  read FRoundFactorBottomX write SetRoundFactorBottomX;
    Property RoundFactorBottomY:Double  read FRoundFactorBottomY write SetRoundFactorBottomY;
    property ShapeUnit;
    property ShapeSizeX;
    property ShapeSizeY;
    property MaxPixelsSizeX;
    property MaxPixelsSizeY;
    property ShapePen;
    property ShapeBrush;
    property TransAngle;
    Property TransScaleX;
    Property TransScaleY;
    Property TransSkewX;
    Property TransSkewY;
  end;

implementation
//................... TRoundRectPresenter ..........................................
constructor TRoundRectPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create(aPresenterStore, iID);
  FRoundFactorTopX:=10.0;
  FRoundFactorTopY:=10.0;
  FRoundFactorBottomX:=10.0;
  FRoundFactorBottomY:=10.0;
end;

procedure TRoundRectPresenter.SetRoundFactorTopX(Const Value: Double);
 begin
  if Value <=0 then exit;
  if Value = FRoundFactorTopX then exit;
  FRoundFactorTopX:=Value;
  RedrawObject;
 end;

procedure TRoundRectPresenter.SetRoundFactorTopY(Const Value:Double);
 begin
  if Value <=0 then exit;
  if Value = FRoundFactorTopY then exit;
  FRoundFactorTopY:=Value;
  RedrawObject;
 end;

procedure TRoundRectPresenter.SetRoundFactorBottomX(Const Value:Double);
 begin
  if Value <=0 then exit;
  if Value = FRoundFactorBottomX then exit;
  FRoundFactorBottomX:=Value;
  RedrawObject;
 end;

procedure TRoundRectPresenter.SetRoundFactorBottomY(Const Value:Double);
 begin 
  if Value <=0 then exit;
  if Value = FRoundFactorBottomY then exit;
  FRoundFactorBottomY:=Value;
  RedrawObject;
 end;


procedure TRoundRectPresenter.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
  if Source is TRoundRectPresenter then
  begin
    RoundFactorTopX:=TRoundRectPresenter(Source).RoundFactorTopX;
    RoundFactorTopY:=TRoundRectPresenter(Source).RoundFactorTopY;
    RoundFactorBottomX:=TRoundRectPresenter(Source).RoundFactorBottomX;
    RoundFactorBottomY:=TRoundRectPresenter(Source).RoundFactorBottomY;
  end;
end;

procedure TRoundRectPresenter.LoadEnvironment(Element: TGXML_Element);
begin
  inherited LoadEnvironment(Element);

  if Element = nil then exit;
  with Element do
   begin
     RoundFactorTopX := FloatAttributeByName( 'RoundFactorTopX', RoundFactorTopX);
     RoundFactorTopY := FloatAttributeByName( 'RoundFactorTopY', RoundFactorTopY);
     RoundFactorBottomX := FloatAttributeByName( 'RoundFactorBottomX', RoundFactorBottomX);
     RoundFactorBottomY := FloatAttributeByName( 'RoundFactorBottomY', RoundFactorBottomY);
    end;
end;


function TRoundRectPresenter.SaveEnvironment: TGXML_Element;
begin
  Result := inherited SaveEnvironment;

  Result.AddFloatAttribute( 'RoundFactorTopX', RoundFactorTopX);
  Result.AddFloatAttribute( 'RoundFactorTopY', RoundFactorTopY);
  Result.AddFloatAttribute( 'RoundFactorBottomX', RoundFactorBottomX);
  Result.AddFloatAttribute( 'RoundFactorBottomY', RoundFactorBottomY);
end;


procedure TRoundRectPresenter.WriteProperties(Writer: TEarthStreamWriter);
begin
  if Writer=nil then exit;
  inherited WriteProperties(Writer);
  Writer.WriteInteger(1);//Write Object Stream Version

  Writer.WriteDouble(FRoundFactorTopX);
  Writer.WriteDouble(FRoundFactorTopY);
  Writer.WriteDouble(FRoundFactorBottomX);
  Writer.WriteDouble(FRoundFactorBottomY);
end;

procedure TRoundRectPresenter.ReadProperties(Reader: TEarthStreamReader);
var iVer:integer;
begin
  if Reader=nil then exit;
  inherited ReadProperties(Reader);
  iVer:=Reader.ReadInteger;//File version

  FRoundFactorTopX:=Reader.ReadDouble;
  FRoundFactorTopY:=Reader.ReadDouble;
  FRoundFactorBottomX:=Reader.ReadDouble;
  FRoundFactorBottomY:=Reader.ReadDouble;
end;


procedure TRoundRectPresenter.Drawshape;
begin
 ParentEarth.EarthCanvas.RoundedRect(FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - FDrawOffectY, FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectY,
                               FRoundFactorBottomX,FRoundFactorBottomY,FRoundFactorTopX,FRoundFactorTopY);
end;



//................... TDotPresenter....................................................
constructor TDotPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited Create( aPresenterStore, iID );
  FShapeSizeX:=10;
  FShapeSizeY:=10;
end;
procedure TDotPresenter.Drawshape;
begin
  ParentEarth.EarthCanvas.gDrawPixel(FDrawCenterXY.X, FDrawCenterXY.Y, ShapePen.PenColor);
end;

//................... TCirclePresenter ...........................................
procedure TCirclePresenter.Drawshape;
begin
  ParentEarth.EarthCanvas.Ellipse(FDrawCenterXY.X,FDrawCenterXY.Y,FDrawOffectX,FDrawOffectX);
end;

//................... TOvalPresenter ................................................
procedure TOvalPresenter.Drawshape;
begin
  ParentEarth.EarthCanvas.Ellipse(FDrawCenterXY.X,FDrawCenterXY.Y,FDrawOffectX,FDrawOffectY);
end;

//.................... TRectPresenter ............................................
procedure TRectPresenter.Drawshape;
begin
 ParentEarth.EarthCanvas.Rectangle(FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y - FDrawOffectY, FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectY);
end;
//................... TDiamondPresenter ...........................................
procedure TDiamondPresenter.Drawshape;
begin
 with ParentEarth.EarthCanvas do
  begin
    gaPoints^[0] := PointDouble(FDrawCenterXY.X, FDrawCenterXY.Y - FDrawOffectY);
    gaPoints^[1] := PointDouble(FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y);
    gaPoints^[2] := PointDouble(FDrawCenterXY.X, FDrawCenterXY.Y + FDrawOffectY);
    gaPoints^[3] := PointDouble(FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y);
    gDrawPolygon(gaPoints, 4);
  end;
end;

//................... TExagonPresenter .......................................
procedure TExagonPresenter.Drawshape;
 var ix:Double;
begin
 with ParentEarth.EarthCanvas do
  begin
   ix:=FDrawOffectX/2 ;
   gaPoints^[0] := PointDouble(FDrawCenterXY.X-FDrawOffectX, FDrawCenterXY.Y);
   gaPoints^[1] := PointDouble(FDrawCenterXY.X-ix, FDrawCenterXY.Y-FDrawOffectY);
   gaPoints^[2] := PointDouble(FDrawCenterXY.X+ix, FDrawCenterXY.Y-FDrawOffectY);
   gaPoints^[3] := PointDouble(FDrawCenterXY.X+FDrawOffectX, FDrawCenterXY.Y);
   gaPoints^[4] := PointDouble(FDrawCenterXY.X+ix, FDrawCenterXY.Y+FDrawOffectY);
   gaPoints^[5] := PointDouble(FDrawCenterXY.X-ix, FDrawCenterXY.Y+FDrawOffectY);
   gDrawPolygon(gaPoints, 6);
  end;
end;

//................... TTriangleUpPresenter  ...................................
procedure TTrianglePresenter.Drawshape;
begin
 with ParentEarth.EarthCanvas do
  begin
   gaPoints^[0] := PointDouble(FDrawCenterXY.X + FDrawOffectX, FDrawCenterXY.Y + FDrawOffectY);
   gaPoints^[1] := PointDouble(FDrawCenterXY.X, FDrawCenterXY.Y - FDrawOffectY);
   gaPoints^[2] := PointDouble(FDrawCenterXY.X - FDrawOffectX, FDrawCenterXY.Y + FDrawOffectY);
   gDrawPolygon(gaPoints, 3);
  end;
end;

//======================== Transparency ===========================================



initialization
RegisterClasses( [
                  TTrianglePresenter,
                  TExagonPresenter,
                  TDiamondPresenter,
                  TRoundRectPresenter,
                  TRectPresenter,
                  TOvalPresenter,
                  TDotPresenter,
                  TCirclePresenter
                  ] );


end.
