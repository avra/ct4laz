
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

Unit GIS_Quaternion;


 
Interface
 
Uses GIS_SysUtils,Math;
   
   Function QuatToEuler_XYZ(Const q: TQuaternion): TV3D;
   Function QuatToEuler(Const q: TQuaternion; order: String): TV3D;
   Function QuatToMatrix(Const q: TQuaternion): TGMatrix;
   Function MatrixtoEuler(Const matrix: TGMatrix; order: String): TV3D;
   Function AxisAngleToEuler(Const axis: TV3D; angle: Extended; order: String): TV3D;
   Function AxisAngleToMatrix(Const axis: TV3D; angle: Extended): TGMatrix;
   Function SpinX(Const angle: Extended): TGMatrix;
   Function SpinY(Const angle: Extended): TGMatrix;
   Function SpinZ(Const angle: Extended): TGMatrix;
   
Implementation
 
Function SpinX(Const angle: Extended): TGMatrix;
 
Var
   cosa, sina: extended;
 
Begin
   cosa := cos(angle);
   sina := sin(angle);
   Result[0] := 1;
   Result[1] := 0;
   Result[2] := 0;
   Result[3] := 0;
   Result[4] := cosa;
   Result[5] := sina;
   Result[6] := 0;
   Result[7] := - sina;
   Result[8] := cosa;
End;
 
Function SpinY(Const angle: Extended): TGMatrix;
 
Var
   cosa, sina: extended;
 
Begin
   cosa := cos(angle);
   sina := sin(angle);
   Result[0] := cosa;
   Result[1] := 0;
   Result[2] := - sina;
   Result[3] := 0;
   Result[4] := 1;
   Result[5] := 0;
   Result[6] := sina;
   Result[7] := 0;
   Result[8] := cosa;
End;
 
Function SpinZ(Const angle: Extended): TGMatrix;
 
Var
   cosa, sina: extended;
 
Begin
   cosa := cos(angle);
   sina := sin(angle);
   Result[0] := cosa;
   Result[1] := sina;
   Result[2] := 0;
   Result[3] := - sina;
   Result[4] := cosa;
   Result[5] := 0;
   Result[6] := 0;
   Result[7] := 0;
   Result[8] := 1;
End;
 
Function AxisAngleToMatrix(Const axis: TV3D; angle: Extended): TGMatrix;
 
Var
   mu, sina, cosa: extended;
 
Begin
   mu := 1 - cos(angle);
   sina := sin(angle);
   cosa := cos(angle);
   Result[0] := axis.X * axis.X * mu + cosa;
   Result[1] := axis.X * axis.Y * mu + axis.Z * sina;
   Result[2] := axis.X * axis.Z * mu - axis.Y * sina;
   Result[3] := axis.X * axis.Y * mu - axis.Z * sina;
   Result[4] := axis.Y * axis.Y * mu + cosa;
   Result[5] := axis.Y * axis.Z * mu + axis.X * sina;
   Result[6] := axis.X * axis.Z * mu + axis.Y * sina;
   Result[7] := axis.Y * axis.Z * mu - axis.X * sina;
   Result[8] := axis.Z * axis.Z * mu + cosa;
End;
 

Function AxisAngleToEuler(Const axis: TV3D; angle: Extended; order: String): TV3D;
 
Begin
   Result := MatrixToEuler(AxisAngleToMatrix(axis, angle), order);
End;
 

Function MatrixToEuler(Const matrix: TGMatrix;  order: String): TV3D;
 
Var
   a1, a2, b1, b2, c1, c2: extended;
 
Begin
   If (order = 'xyx') Then
   Begin
      b1 := sqrt(matrix[3] * matrix[3] + matrix[6] * matrix[6]);
      b2 := matrix[0];
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = 0) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[7], matrix[4]));
      End
      Else If (Result.Y = 180) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[7], - matrix[4]));
      End
      Else
      Begin
         a1 := matrix[1] / b1;
         a2 := - matrix[2] / b1;
         c1 := matrix[3] / b1;
         c2 := matrix[6] / b1;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'xyz') Then
   Begin
      b1 := matrix[6];
      b2 := sqrt(matrix[0] * matrix[0] + matrix[3] * matrix[3]);
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = - 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[1], matrix[4]));
      End
      Else If (Result.Y = 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[1], matrix[4]));
      End
      Else
      Begin
         a1 := - matrix[7] / b2;
         a2 := matrix[8] / b2;
         c1 := - matrix[3] / b2;
         c2 := matrix[0] / b2;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'xzx') Then
   Begin
      b1 := sqrt(matrix[1] * matrix[1] + matrix[2] * matrix[2]);
      b2 := matrix[0];
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = 0) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[5], matrix[4]));
      End
      Else If (Result.Y = 180) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[5], - matrix[4]));
      End
      Else
      Begin
         a1 := matrix[2] / b1;
         a2 := matrix[1] / b1;
         c1 := matrix[6] / b1;
         c2 := - matrix[3] / b1;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'xzy') Then
   Begin
      b1 := - matrix[3];
      b2 := sqrt(matrix[0] * matrix[0] + matrix[6] * matrix[6]);
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = - 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[2], - matrix[1]));
      End
      Else If (Result.Y = 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[2], matrix[1]));
      End
      Else
      Begin
         a1 := matrix[5] / b2;
         a2 := matrix[4] / b2;
         c1 := matrix[6] / b2;
         c2 := matrix[0] / b2;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'yxy') Then
   Begin
      b1 := sqrt(matrix[1] * matrix[1] + matrix[7] * matrix[7]);
      b2 := matrix[4];
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = 0) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[6], matrix[0]));
      End
      Else If (Result.Y = 180) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[6], matrix[0]));
      End
      Else
      Begin
         a1 := matrix[3] / b1;
         a2 := matrix[5] / b1;
         c1 := matrix[1] / b1;
         c2 := - matrix[7] / b1;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'yxz') Then
   Begin
      b1 := - matrix[7];
      b2 := sqrt(matrix[1] * matrix[1] + matrix[4] * matrix[4]);
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = - 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[3], matrix[0]));
      End
      Else If (Result.Y = 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[3], matrix[0]));
      End
      Else
      Begin
         a1 := matrix[6] / b2;
         a2 := matrix[8] / b2;
         c1 := matrix[1] / b2;
         c2 := matrix[4] / b2;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'yzx') Then
   Begin
      b1 := matrix[1];
      b2 := sqrt(matrix[0] * matrix[0] + matrix[2] * matrix[2]);
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = - 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[5], matrix[8]));
      End
      Else If (Result.Y = 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[5], matrix[8]));
      End
      Else
      Begin
         a1 := - matrix[2] / b2;
         a2 := matrix[0] / b2;
         c1 := - matrix[7] / b2;
         c2 := matrix[4] / b2;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'yzy') Then
   Begin
      b1 := sqrt(matrix[1] * matrix[1] + matrix[7] * matrix[7]);
      b2 := matrix[4];
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = 0) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[6], matrix[0]));
      End
      Else If (Result.Y = 180) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[6], - matrix[0]));
      End
      Else
      Begin
         a1 := matrix[5] / b1;
         a2 := - matrix[3] / b1;
         c1 := matrix[7] / b1;
         c2 := matrix[1] / b1;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'zxy') Then
   Begin
      b1 := matrix[5];
      b2 := sqrt(matrix[2] * matrix[2] + matrix[8] * matrix[8]);
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = - 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[6], matrix[0]));
      End
      Else If (Result.Y = 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[6], matrix[0]));
      End
      Else
      Begin
         a1 := - matrix[3] / b2;
         a2 := matrix[4] / b2;
         c1 := - matrix[2] / b2;
         c2 := matrix[8] / b2;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'zxz') Then
   Begin
      b1 := sqrt(matrix[2] * matrix[2] + matrix[5] * matrix[5]);
      b2 := matrix[8];
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = 0) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[3], matrix[0]));
      End
      Else If (Result.Y = 180) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[3], matrix[0]));
      End
      Else
      Begin
         a1 := matrix[6] / b1;
         a2 := - matrix[7] / b1;
         c1 := matrix[2] / b1;
         c2 := matrix[5] / b1;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'zyx') Then
   Begin
      b1 := - matrix[2];
      b2 := sqrt(matrix[5] * matrix[5] + matrix[8] * matrix[8]);
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = - 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[7], matrix[4]));
      End
      Else If (Result.Y = 90) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[7], matrix[4]));
      End
      Else
      Begin
         a1 := matrix[1] / b2;
         a2 := matrix[0] / b2;
         c1 := matrix[5] / b2;
         c2 := matrix[8] / b2;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
   If (order = 'zyz') Then
   Begin
      b1 := sqrt(matrix[2] * matrix[2] + matrix[5] * matrix[5]);
      b2 := matrix[8];
      Result.Y := RadiansToAngle(ArcTan2(b1, b2));
      If (Result.Y = 0) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2( - matrix[3], matrix[0]));
      End
      Else If (Result.Y = 180) Then
      Begin
         Result.X := 0;
         Result.Z := RadiansToAngle(ArcTan2(matrix[3], - matrix[0]));
      End
      Else
      Begin
         a1 := matrix[7] / b1;
         a2 := matrix[6] / b1;
         c1 := matrix[5] / b1;
         c2 := - matrix[2] / b1;
         Result.X := RadiansToAngle(ArcTan2(a1, a2));
         Result.Z := RadiansToAngle(ArcTan2(c1, c2));
      End;
   End;
End;
 
Function QuatToMatrix(Const q: TQuaternion): TGMatrix;
 
Begin
   Result[0] := 1 - 2 * q.y * q.y - 2 * q.z * q.z;
   Result[1] := 2 * (q.x * q.y + q.z * q.w);
   Result[2] := 2 * (q.x * q.z - q.y * q.w);
   Result[3] := 2 * (q.x * q.y - q.z * q.w);
   Result[4] := 1 - 2 * q.x * q.x - 2 * q.z * q.z;
   Result[5] := 2 * (q.y * q.z + q.x * q.w);
   Result[6] := 2 * (q.x * q.z + q.y * q.w);
   Result[7] := 2 * (q.y * q.z - q.x * q.w);
   Result[8] := 1 - 2 * q.x * q.x - 2 * q.y * q.y;
End;
 
Function QuatToEuler(Const q: TQuaternion; order: String): TV3D;
 
Begin
   Result := MatrixToEuler(QuatToMatrix(q), order);
End;
 
Function QuatToEuler_XYZ(Const q: TQuaternion): TV3D;
 
Var
   cosy, siny: extended;
   matrix: TGMatrix;
 
Begin
   matrix[0] := 1.0 - (2.0 * q.Y * q.Y) - (2.0 * q.Z * q.Z);
   // matrix[1] := (2.0 * q.X * q.Y) - (2.0 * q.w * q.Z); //
   // matrix[2] := (2.0 * q.X * q.Z) + (2.0 * q.w * q.Y); //
   matrix[3] := (2.0 * q.X * q.Y) + (2.0 * q.w * q.Z);
   matrix[4] := 1.0 - (2.0 * q.X * q.X) - (2.0 * q.Z * q.Z);
   matrix[5] := (2.0 * q.Y * q.Z) - (2.0 * q.w * q.X);
   matrix[6] := (2.0 * q.X * q.Z) - (2.0 * q.w * q.Y);
   matrix[7] := (2.0 * q.Y * q.Z) + (2.0 * q.w * q.X);
   matrix[8] := 1.0 - (2.0 * q.X * q.X) - (2.0 * q.Y * q.Y);
 
   siny := - matrix[6];
   cosy := sqrt(1 - (siny * siny));
   Result.Y := ArcTan2(siny, cosy) * DEG_FROMRADIANS;
   If (siny <> 1.0) And (siny <> - 1.0) Then
   Begin
      Result.X := ArcTan2((matrix[7] / cosy), (matrix[8] / cosy)) *
      DEG_FROMRADIANS;
      Result.Z := ArcTan2((matrix[3] / cosy), (matrix[0] / cosy)) *
      DEG_FROMRADIANS;
   End
   Else
   Begin
      Result.X := ArcTan2( - matrix[5], matrix[4]) * DEG_FROMRADIANS;
      Result.Z := ArcTan2(0.0, 1.0) * DEG_FROMRADIANS;
   End;
End;
 
End.
