
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit SMap_ADRI;


interface

uses LCLIntf, LCLType, LMessages, Classes, Graphics, SysUtils,
     GIS_EarthBase, GIS_Classes, Dialogs, GIS_SysUtils,
  GIS_EarthObjects, GIS_Resource,SMap_UniDIB;


const
 ADRI_CMS_Colors_offet=2048;
 ADRI_Size_of_File_header=18 ;
 ADRI_Size_of_Image_header=132;

 type
 ADRI_file_header = record            //At begin of file
    attribute_offset: LongInt;   // 4 bytes
    security_class: Char;
    security_code: Array[0..2-1] of Char;
    security_mark: Array[0..2-1] of Char;
    terminator: Char;
    pad: Array[0..3-1] of Char;
    number_extended: Byte;
    attribute_length:longint;   // 4 bytes
  end {ADRI_file_header};

ADRI_attribute_section = record   //  at  ADRI_file_header.attribute_offset file pozition
    record_length: LongInt;  // 4 bytes
    atype: smallint;         // 2 bytes  if 0:=ADRG 1:=ADRI
    subtype: smallint;       // 2 bytes
    coverages: smallint;     // 2 bytes
  end {ADRI_attribute_section};

ADRI_spatial_data = record
    spatial_length: LongInt;   // 4 bytes
    record_type:SmallInt;      // 2 bytes  IF 19:=ADRG or ADRI 21:=DTED
    image_length: LongInt;     // 4 bytes
  end {ADRI_spatial_data};


ADRI_image_header = record
    Number_rows: LongInt;  // 4 bytes
    Number_Cols: LongInt;  // 4 bytes
    element_type: LongInt; // 4 bytes
    lat_interval: Single;
    long_interval: Single;
    color_gray: LongInt;    // 4 bytes
    Colors:LongInt;// Array[0..4-1] of Char;
  end;

ADRI_image_Geo = record
    subFrame_sw_lat: Double;
    subFrame_sw_lon: Double;
    subFrame_se_lat: Double;
    subFrame_se_lon: Double;
    subFrame_ne_lat: Double;
    subFrame_ne_lon: Double;
    subFrame_nw_lat: Double;
    subFrame_nw_lon: Double;
    Frame_y_interval: Single;
    Frame_x_interval: Single;
    subFrame_sw_x: Single;
    subFrame_sw_y: Single;
    subFrame_se_x: Single;
    subFrame_se_y: Single;
    subFrame_ne_x: Single;
    subFrame_ne_y: Single;
    subFrame_nw_x: Single;
    subFrame_nw_y: Single;
  end {ADRI_image_header};

//===================================================
ADRI_Frame=record
      isload:boolean;
      Width:integer;
      Height:integer;
      DibPos:integer;
      spatialRec:ADRI_spatial_data;
      ImageRec:ADRI_image_header;
      ImageGeo:ADRI_image_Geo;
     end;

TMapFileADRI=class(TObject)
private

Protected
   FStream: TStream;    //points to a  FStream
   FImage: TBitmap;
   Fpalette: TxLogPalette256;
   FisImageload: boolean;
   FisRecordsLoad: boolean;
   FWidth: integer;
   FHeight: integer;
   Function  LoadHeader:boolean;
   function  LoadAttributes:boolean;
   Procedure LoadPalette;
   function  FindAndLoadADRI_FramesRec:boolean;
   Procedure FindWidthAndHeight;
   Procedure LoadADRI_FramesToImage;
   Procedure LoadADRI_FrameToImage(Uni:TUniDIB;Width,Height,X,Y:integer);
   Function  LoadRecords:boolean;
   Procedure LoadImage;
   Procedure LoadAll;
public
    FADRI_file_header:ADRI_file_header;
    FADRI_attribute_section:ADRI_attribute_section;
    FFrames:array[0..6,0..6] of ADRI_Frame;
    FNumOfFrames:integer;
    //--------------------------
    constructor Create;
    procedure Free;
    procedure Clear;
    Procedure WriteToTStrings(aTStrings:TStrings);
    function LoadFromStream(aStream: TStream):boolean;
    property IsImageLoad: boolean read FisImageload default False;
    property IsRecordsLoad: boolean read FisRecordsLoad default False;
    property Image: TBitmap read FImage write FImage;
    property Width: integer read FWidth;
    property Height: integer read FHeight;
end;  

implementation

//.........................TMapFileADRI................................................
function TMapFileADRI.LoadFromStream(aStream: TStream):boolean;
begin
result:=false;
Fstream:=nil;
if astream = nil then exit;

Fstream := aStream;

if LoadRecords=true then
      begin
       LoadImage;
       result:=true;
      end;

end;

constructor TMapFileADRI.Create;
begin
  inherited;
  FImage := TBitmap.Create;
  FisImageload := False;
  FisRecordsLoad := False;
  FWidth := 0;
  FHeight := 0;
end;

procedure TMapFileADRI.Free;
begin
  Fimage.Free;
  inherited;
end;
{-----------------------------------------------------------------------------
   clear variables
------------------------------------------------------------------------------}
Procedure TMapFileADRI.Clear;
  begin
     inherited;
  FisImageload := False;
  FisRecordsLoad := False;
  FWidth := 0;
  FHeight := 0;
    fillchar(FADRI_file_header,sizeof(FADRI_file_header),0);
    fillchar(FADRI_attribute_section,sizeof(FADRI_attribute_section),0);
    fillchar(FFrames,sizeof(FFrames),0);
  end;

{--------------------------------------------------------------------------------
  main procedure of load All
--------------------------------------------------------------------------------}
Procedure TMapFileADRI.LoadAll;
  begin
   LoadRecords;
   LoadImage;
  end;
{--------------------------------------------------------------------------------
  main procedure of load Records of map
--------------------------------------------------------------------------------}
Function  TMapFileADRI.LoadRecords:boolean;
  begin
  result:=false;
  Clear;
  FisRecordsload :=false;
   if LoadHeader=true then
     if FindAndLoadADRI_FramesRec=true then
       begin
         FindWidthAndHeight;
         if LoadAttributes=true then                //load Attribute rec
          begin
           FisRecordsload:=true;
           result:=true;
          end;
      end;
      
  end;
{--------------------------------------------------------------------------------
  main procedure of load Image from map
--------------------------------------------------------------------------------}
Procedure TMapFileADRI.LoadImage;
  begin
   If FisRecordsload=true then
    begin
   LoadPalette; //Load Color Palettee
   LoadADRI_FramesToImage;
   FisImageload:=true;
    end;
  end;
{--------------------------------------------------------------------------------
 --control of load ADRI_Frames DIB Data data for ADRI_Frames to Fimage
 --get initial data from FADRI_Frames var
--------------------------------------------------------------------------------}
Procedure TMapFileADRI.LoadADRI_FramesToImage;
 var col,row,X,Y:integer;
     uni:TUniDIB;
  begin

    Fimage.PixelFormat:= pf8bit;
    fimage.Width:=Fwidth;
    fimage.Height:=FHeight;
  //................................
     uni:=TUniDIB.Create(FImage.width,FImage.Height,8,SBU_NONE);
     uni.SetPalette(FPalette);
  //.................................
    x:=0;
    y:=0;
   try
     for row:=0 to 6 do //start of write ADRI_Frames loop
        begin
         for col:=0 to 6 do
          begin
            if  fFrames[row,col].isload=true then
              begin
                fStream.Position:=fFrames[row,col].DibPos;
                LoadADRI_FrameToImage(uni,
                           fFrames[row,col].Width,fFrames[row,col].Height,
                           X,Y);
              x:=x+fFrames[row,col].Width;
              end;
           end;
          // y:=y+fFrames[row,col].Height;
        end;
  //..............................
    uni.DIBtoCanvasXY(0,0,fimage.Canvas.Handle);
     finally
     uni.Free;
     end;
  end;

Procedure TMapFileADRI.LoadADRI_FrameToImage(Uni:TUniDIB;Width,Height,X,Y:integer);
  type
     TBuf=array[0..0] of byte;
  var
     i:integer;
     Buf:^TBuf;
     Pos:Longint;
  begin     
     getmem(Buf,Width*Height);
     try
     FStream.Read(buf^,Width*Height); //load ADRI_Frame DIB data to buf
      for i:=0 to Height-1 do
       begin
         Pos:=((Y+i)*FWidth)+x;
         System.Move(Pointer(Longint(buf) + (Width*i))^,Pointer(Longint(uni.bits) + Pos)^,Width);
       end;
     finally
     FreeMem(Buf);
     end;
  end;
{--------------------------------------------------------------------------------
 initialase Fimage to load ADRI_Frames DIB
 Find Width and Height of Fimage from fFrames var
--------------------------------------------------------------------------------}
Procedure TMapFileADRI.FindWidthAndHeight;
  var col,row,sWidth,sHeight:integer;
   begin
   swidth:=0;
   sHeight:=0;
     //for swidth
    for col:=0 to 6 do
     begin
         if fFrames[0,col].isload=true then
           begin
             swidth:=swidth+fFrames[0,col].Width;
           end;
     end;
     //for Height
    for row:=0 to 6 do
      begin
         if fFrames[row,0].isload=true then
               begin
                 sHeight:=sHeight+fFrames[row,0].Height;
               end;
      end;
     FWidth:=swidth;
     FHeight:=sHeight;
   end;
{--------------------------------------------------------------------------------
  Find numbers of ADRI_Frames in map file end load data to fFrames var
---------------------------------------------------------------------------------}
function TMapFileADRI.FindAndLoadADRI_FramesRec:boolean;
  var sd:ADRI_spatial_data;
      ih:ADRI_image_header;
      Geo:ADRI_image_Geo;
      row,col,count:integer;
      End_of_images:longint;
  begin
result:=true;
   row:=0;
   col:=0;
   count:=1;
   End_of_images:=FADRI_file_header.attribute_offset-ADRI_CMS_Colors_offet;
   repeat
   FStream.Read(sd,sizeof(sd));
   if sd.record_type<>21 then   //This is chec rec type of ADRG or ADRG file type
     begin
      result:=false;
      exit;
     end;
   Fstream.Position:=Fstream.Position+2;
   FStream.Read(ih,sizeof(ih));//Size_of_ADRI_image_header_ADRI);

   FStream.Read(Geo,sizeof(Geo));//Size_of_ADRI_image_header_ADRI);
   if count>1 then
     begin
       if fFrames[row,col].ImageGeo.subFrame_sw_lat=Geo.subFrame_sw_lat then
           begin
            inc(col);
           end else
           begin
            inc(row);
            col:=0;
           end;
     end; //must set set_cmslatlon procedure
   fFrames[row,col].isload:=true;
   fFrames[row,col].spatialRec:=sd;
   fFrames[row,col].ImageRec:=ih;
   fFrames[row,col].ImageGeo:=Geo;
   fFrames[row,col].Width:=ih.Number_Cols;
   fFrames[row,col].Height:=ih.number_rows;
   fFrames[row,col].DibPos:=Fstream.Position;
   inc(count);
   FStream.Position:=FStream.Position+(ih.Number_Cols*ih.number_rows);
   until FStream.Position>=End_of_images;
   FNumOfFrames:=count-1;
  end;
{-------------------------------------------------------------------------
  This Procedure load ADRI_file_header rec
--------------------------------------------------------------------------}
Function  TMapFileADRI.LoadHeader:boolean;
begin
   result:=false;
   FStream.Read(FADRI_file_header,ADRI_Size_of_File_header);

   if (FADRI_file_header.attribute_offset)<(FStream.Size-sizeof(FADRI_attribute_section)) then
      result:=true;

end;
{--------------------------------------------------------------------------------
   This Procedure load FADRI_attribute_section
   retent position of stream at start of read
---------------------------------------------------------------------------------}
function TMapFileADRI.LoadAttributes:boolean;
  Var Pos:integer;
   begin
   result:=false;
     Pos:=FStream.Position;
     FStream.Position:=FADRI_file_header.attribute_offset ;
     FStream.Read(FADRI_attribute_section,sizeof(FADRI_attribute_section));
     FStream.Position:=Pos;
     if  (FADRI_attribute_section.atype=1) then
       result:=true;
   end;
{--------------------------------------------------------------------------------
   This Procedure load palette from map file and then
   fix the Palette var of  TADRGMapFile
   retent position of stream at start of read
---------------------------------------------------------------------------------}

Procedure TMapFileADRI.LoadPalette;
  Var i,Pos:integer;
      FPal:array[0..256-1] of rgbquad;
   begin
      Pos:=FStream.Position;
      FStream.Position:=FADRI_file_header.attribute_offset-ADRI_CMS_Colors_offet;
      FStream.Read(FPal,sizeof(FPal));    // load palette from file
      FPalette.palVersion:=$0300;
      FPalette.palNumEntries:=256;
        for i := 0 to 255 do
          begin
           FPalette.palEntry[i].peRed:= fpal[i].rgbBlue;       // in Adrg colors store as  blue,Green,Red
           FPalette.palEntry[i].peGreen:= fpal[i].rgbGreen;   // in Bitmap colors store as Red,Green,blue
           FPalette.palEntry[i].peBlue:= fpal[i].rgbRed;
           FPalette.palEntry[i].peFlags:=fpal[i].rgbReserved;
          end;
      FStream.Position:=pos;
    //....
   end;
{--------------------------------------------------------------------------------
   Make a text report to  TStrings obj of load file
-------------------------------------------------------------------------------}
  Procedure TMapFileADRI.WriteToTStrings(aTStrings:TStrings);
   var  col,row:integer;
   begin
   with  aTStrings do
   begin
     aTStrings.Clear;
     add('Report of ADRG data format File...........................................');
     add('ADRI_file_header rec size:'+Inttostr(sizeof(ADRI_file_header))+' read from file: '+inttostr(ADRI_Size_of_File_header));
     add('attribute rec size:'+Inttostr(sizeof(ADRI_attribute_section)));
     add('ADRI_spatial_data rec size:'+Inttostr(sizeof(ADRI_spatial_data)));
     add('ADRI_image_header rec size:'+Inttostr(sizeof(ADRI_image_header))+' read from file: '+inttostr(ADRI_Size_of_File_header));
     add(' ');
       add('The_ADRI_file_header');
       add('         attribute_offset:'+Inttostr(FADRI_file_header.attribute_offset));
       add('         security_class:'+FADRI_file_header.security_class);
       add('         security_code:'+FADRI_file_header.security_code);
       add('         security_mark:'+FADRI_file_header.security_mark);
       add('         terminator:'+FADRI_file_header.terminator);
       add('         pad:'+FADRI_file_header.pad);
       add('         number_extended:'+Inttostr(FADRI_file_header.number_extended));
       add('         attribute_length:'+Inttostr(FADRI_file_header.attribute_length));
       add('ADRI_attribute_section');
       add('         record_length:'+Inttostr(FADRI_attribute_section.record_length));
       add('         atype:'+Inttostr(FADRI_attribute_section.atype));
       add('         subtype:'+Inttostr(FADRI_attribute_section.subtype));
       add('         coverages'+Inttostr(FADRI_attribute_section.coverages));
       add(' ');
       add('Total Number Of ADRI_Frames in file: '+Inttostr(FNumOfFrames));
       add('...............Start of ADRI_Frames ............................ ');
       for row:=0 to 6 do //start of write ADRI_Frames loop
        begin
         for col:=0 to 6 do
          begin
            if  fFrames[row,col].isload=true then
              begin
       add('ADRI_Frame at martix position Row,Col: '+ inttostr(row)+' , '+inttostr(col));
       add('DIB At file Possition : '+Inttostr(fFrames[row,col].DibPos));
       add('The_ADRI_spatial_data');
       add('        spatial_length:'+Inttostr(fFrames[row,col].spatialRec.spatial_length));
       add('        record_type:'+Inttostr(fFrames[row,col].spatialRec.record_type));
       add('        image_length:'+Inttostr(fFrames[row,col].spatialRec.image_length ));
       add('ADRI_image_header');
       add('        number_cols :'+Inttostr(fFrames[row,col].ImageRec.number_cols));
       add('        number_rows :'+Inttostr(fFrames[row,col].ImageRec.number_rows));
       add('        element_type :'+Inttostr(fFrames[row,col].ImageRec.element_type));
       add('        lat_interval :'+FloatToStr( fFrames[row,col].ImageRec.lat_interval ));
       add('        long_interval :'+FloatToStr( fFrames[row,col].ImageRec.long_interval ));
       add('        color_gray:'+Inttostr(fFrames[row,col].ImageRec.color_gray));
       add('        Colors       :'+Inttostr(fFrames[row,col].ImageRec.Colors ));
    //   add('        number_shades :'+Inttostr(fFrames[row,col].ImageRec.number_shades));
       add('ADRI_image_Geo');
       add('        subFrame_sw_lat :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_sw_lat ));
       add('        subFrame_sw_lon :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_sw_lon ));
       add('        subFrame_se_lat :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_se_lat ));
       add('        subFrame_se_lon :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_se_lon  ));
       add('        subFrame_ne_lat :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_ne_lat ));
       add('        subFrame_ne_lon :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_ne_lon ));
       add('        subFrame_nw_lat :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_nw_lat ));
       add('        subFrame_nw_lon :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_nw_lon ));
       add('        Frame_y_interval:'+FloatToStr( fFrames[row,col].ImageGeo.Frame_y_interval));
       add('        Frame_x_interval:'+FloatToStr( fFrames[row,col].ImageGeo.Frame_x_interval ));
       add('        subFrame_sw_x  :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_sw_x ));
       add('        subFrame_sw_y :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_sw_y ));
       add('        subFrame_se_x :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_se_x ));
       add('        subFrame_se_y :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_se_y  ));
       add('        subFrame_ne_x :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_ne_x ));
       add('        subFrame_ne_y :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_ne_y ));
       add('        subFrame_nw_x :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_nw_x  ));
       add('        subFrame_nw_y :'+FloatToStr( fFrames[row,col].ImageGeo.subFrame_nw_y ));
        end;
       end;
      end; //end of write ADRI_Frames loop
       add('...............End of ADRI_Frames ................................');
     end;
  end;
{-----------------------------------------------------------------------}



end.
