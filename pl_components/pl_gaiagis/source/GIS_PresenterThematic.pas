
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit GIS_PresenterThematic;

{$MODE DELPHI}

interface

uses LCLIntf, LCLType,Classes, Graphics, SysUtils,
  GIS_EarthBase, GIS_Classes, GIS_EarthObjects, GIS_SysUtils, GIS_XML;

type
	TThematicBreakType = (btRange, btStdDev, btPercentile);


TThematicBreak = class( TEarthRoot )
  private
    FBreakValue: Double;
    FBreakBrush: TEarthBrush;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadEnvironment( Element : TGXML_Element ); virtual;
    function SaveEnvironment : TGXML_Element; virtual;
  published
    property BreakBrush : TEarthBrush read FBreakBrush write FBreakBrush;
    property BreakValue : Double read FBreakValue write FBreakValue;
  end;

{TPolygonThematic Presenter}
TPolygonThematicPresenter = class(TEarthPresenter)
  private
    FPolygonPen: TEarthPen;
    FNoDataBrush: TEarthBrush;
    FNoDataValue: Double;
    FBreakList : TList;
    function GetBreak(iIndex: integer): TThematicBreak;
    procedure SetBreakCount(const Value: integer);
    function GetBreakCount: integer;
  protected
    function ValueToBrush( Value: Double): TEarthBrush;
  public
    Max : Double;
    Min : Double;
    Sum : Extended;
    Mean : Double;
    StdDev : Double;
    constructor Create(aPresenterStore:TEarthPresenterStore; iID : integer); override;
    destructor Destroy; override;
    procedure Assign(Source : TPersistent); override;
    procedure RenderObject( Earth : TCustomEarth; Geod : TEarthObject; State : TEarthObjectStateSet); override;
    procedure LoadEnvironment( Element : TGXML_Element ); override;
    function  SaveEnvironment : TGXML_Element; override;
    procedure GradientBreakColors( StartColor, EndColor : TColor );
    procedure CalcStatistics( aLayer : TEarthLayer; aPresenterID : integer );
    procedure CalcBreakValues( BreakLimitType: TThematicBreakType; aLayer : TEarthlayer );
    property  Breaks[iIndex : integer] : TThematicBreak read GetBreak;
  published
    property BreakCount : integer read GetBreakCount write SetBreakCount;
    property NoDataValue : Double read FNoDataValue write FNoDataValue;
    property NoDataBrush : TEarthBrush read FNoDataBrush write FNoDataBrush;
    property PolygonPen : TEarthPen read FPolygonPen write FPolygonPen;
  end;

implementation

//=============== TPolygonThematicPresenter ======================

procedure TPolygonThematicPresenter.CalcStatistics( aLayer : TEarthLayer;
  aPresenterID : integer );
var
	eTmp : Double;
	idx, iDataCount : integer;
  ObjSrc : TEarthObjectStore;
begin
  ObjSrc := aLayer.Objects;
	{ scan the objects to find the range of values }
	Max := -1.1E104932;
	Min := 1.1E104932;

	Sum := 0;
	iDataCount := 0;
	for idx := 0 to ObjSrc.Count - 1 do
		if ObjSrc.PresenterID[idx] = aPresenterID then
		begin
			eTmp := ObjSrc.Value[idx];

			{ update the range values }
			if eTmp <> NoDataValue then
			begin
				if eTmp < Min then Min := eTmp;
				if eTmp > Max then Max := eTmp;
				Sum := Sum + eTmp;
				Inc(iDataCount);
			end;
		end;

	if iDataCount > 0 then
	begin
		Mean := Sum / iDataCount;

		{ Calculate the Standard Deviation }
		eTmp := 0;
		for idx := 0 to ObjSrc.Count - 1 do
			if ObjSrc.PresenterID[idx] = aPresenterID then
				if ObjSrc.Value[idx] <> NoDataValue then
					eTmp := eTmp + Sqr( ObjSrc.Value[idx] - Mean );

		StdDev := Sqrt( eTmp / iDataCount);
	end;
end;

function SortByValue(Item1, Item2: Pointer): Integer;
var
  Value1, Value2 : Double;
begin
  Value1 := TEarthObject( Item1 ).Value;
  Value2 := TEarthObject( Item2 ).Value;

  Result := 0;
	if Value1 < Value2 then
		Result := -1
	else
		if Value1 > Value2 then
			Result := 1
end;

procedure TPolygonThematicPresenter.CalcBreakValues(
  BreakLimitType: TThematicBreakType; aLayer : TEarthlayer );
var
	eStep, eStart : Extended;
	idx, iBreakCount : integer;
  SortedList : TList;
begin
  CalcStatistics( aLayer, PresenterID );

	iBreakCount := BreakCount - 1;
  for idx := 0 to iBreakCount do  // clear all of the breaks
    Breaks[idx].BreakValue := 0;

	case BreakLimitType of
	btRange:
		begin
			eStep := (Max - Min) / iBreakCount;
			for idx := 0 to iBreakCount do
				Breaks[idx].BreakValue := Min + eStep * idx;
		end;
	btStdDev:
		begin
			eStep := StdDev;
			eStart := Mean - (iBreakCount div 2) * StdDev;
			for idx := 0 to iBreakCount do
				Breaks[idx].BreakValue := eStart + eStep * idx;
		end;
	btPercentile:
		begin
			SortedList := TList.Create;

			for idx := 0 to aLayer.Objects.Count - 1 do
				if aLayer.Objects.PresenterID[idx] = PresenterID then
					if aLayer.Objects.Value[idx] <> NoDataValue then
						SortedList.Add( aLayer.Objects[idx] );

			SortedList.Sort( SortByValue );

      // Remove duplicate values
			for idx := SortedList.Count - 1 downto 1 do
        if TEarthObject( SortedList[idx] ).Value = TEarthObject( SortedList[idx - 1] ).Value then
          SortedList.Delete( idx );

			if SortedList.Count > 0 then
      begin
        for idx := 1 to iBreakCount do
          Breaks[idx - 1].BreakValue := TEarthObject( SortedList[((SortedList.Count - 1 ) div iBreakCount) * idx]).Value;
        Breaks[iBreakCount].BreakValue := TEarthObject( SortedList[SortedList.Count - 1]).Value;
      end;
			SortedList.Free;
		end;
	end;
end;

procedure TPolygonThematicPresenter.Assign(Source: TPersistent);
var
  idx : integer;
begin
  if Source is TPolygonThematicPresenter then
    with TPolygonThematicPresenter( Source ) do
    begin
      Self.BreakCount := BreakCount;
      Self.NoDataValue := NoDataValue;
      Self.NoDataBrush.Assign( NoDataBrush );
      for idx := 0 to Self.BreakCount - 1 do
      begin
        Self.Breaks[idx].BreakValue := Breaks[idx].BreakValue;
        Self.Breaks[idx].BreakBrush.Assign(Breaks[idx].BreakBrush);
      end;
    end
    else
      inherited Assign( Source );
end;

constructor TPolygonThematicPresenter.Create(aPresenterStore:TEarthPresenterStore; iID : integer);
begin
  inherited;
  FBreakList := TList.Create;

  FPolygonPen := TEarthPen.Create(self);
  FNoDataBrush := TEarthBrush.Create(self);
  FNoDataValue := -1;
end;

destructor TPolygonThematicPresenter.Destroy;
begin
  FBreakList.Free;
  FNoDataBrush.Free;
  FPolygonPen.Free;
  
  inherited Destroy;
end;

function TPolygonThematicPresenter.GetBreak(iIndex: integer): TThematicBreak;
begin
  Result := TThematicBreak( FBreakList[iIndex] );
end;

function TPolygonThematicPresenter.GetBreakCount: integer;
begin
  Result := FBreakList.Count;
end;

procedure TPolygonThematicPresenter.GradientBreakColors(StartColor,
  EndColor: TColor);
var
	R, G, B : Byte;
	idx, iSteps : integer;
begin
	iSteps := BreakCount - 1;

	for idx := 0 to iSteps do
	begin
		R := GetRValue(StartColor);
		R := R + MulDiv( idx, GetRValue(EndColor) - R, iSteps );
		G := GetGValue(StartColor);
		G := G + MulDiv( idx, GetGValue(EndColor) - G, iSteps );
		B := GetBValue(StartColor);
		B := B + MulDiv( idx, GetBValue(EndColor) - B, iSteps );

		Breaks[idx].BreakBrush.BrushColor := RGB(R, G, B);
	end;
end;

procedure TPolygonThematicPresenter.LoadEnvironment( Element: TGXML_Element);
var
  idx : integer;

  function CreateBreak(const Ident : string; iOccourance : integer) : Boolean;
  var
    BreakElement : TGXML_Element;
    NewBreak : TThematicBreak;
  begin
    BreakElement := Element.ElementByName(Ident, iOccourance);
    Result := BreakElement <> nil;
    if Result then
    begin
      NewBreak := TThematicBreak.Create;
      NewBreak.LoadEnvironment(BreakElement);
      FBreaklist.Add(NewBreak);
    end;
  end;
begin
  BreakCount := 0;
  if Element <> nil then
    with Element do
    begin
      NoDataValue := FloatAttributeByName( 'NoDataValue', NoDataValue );
      NoDataBrush.LoadEnvironment(ElementByName('NoDataBrush', 0));

      // Load in all the Breaks
      idx := 0;
      while CreateBreak('Break', idx) do
        Inc(idx);
    end;
end;

procedure TPolygonThematicPresenter.RenderObject(Earth: TCustomEarth; Geod: TEarthObject; State: TEarthObjectStateSet);
begin
  if FIsUpdating then exit; //must set to work the Begin/EndUpdate
  inherited;

  ValueToBrush(Geod.Value).RenderAttribute( Earth.EarthCanvas, osSelected in State );

  PolygonPen.RenderAttribute( Earth.EarthCanvas, osSelected in State );

  Include( State, osClosed );
  Geod.Closed := True;
  Earth.EarthCanvas.RenderChainStore( TGeoDataObject( Geod ).PointsGroups, State,1);
end;

function TPolygonThematicPresenter.SaveEnvironment: TGXML_Element;
var
  idx : integer;
begin
  Result := TGXML_Element.Create;

  Result.AddFloatAttribute('NoDataValue', NoDataValue );
  Result.AddElement('NoDataBrush', NoDataBrush.SaveEnvironment);

  for idx := 0 to BreakCount - 1 do
    Result.AddElement('Break', TThematicBreak(FBreakList[idx]).SaveEnvironment);
end;

procedure TPolygonThematicPresenter.SetBreakCount(const Value: integer);
begin
  if BreakCount <> Value then
  begin
    while FBreakList.Count > Value do
    begin
      TObject( FBreakList.Last ).Free;
      FBreakList.Count := FBreakList.Count - 1;
    end;

    while FBreakList.Count < Value do
      FBreakList.Add( TThematicBreak.Create );
  end;
end;

function TPolygonThematicPresenter.ValueToBrush(Value: Double): TEarthBrush;
var
	idx: Integer;
begin
  Result := NoDataBrush;
  if Value <> NoDataValue then
    for idx := 0 to BreakCount - 1 do
      if Value <= Breaks[idx].BreakValue then
      begin
        Result := Breaks[idx].BreakBrush;
        Exit;
      end;
end;

constructor TThematicBreak.Create;
begin
  inherited;
  FBreakBrush := TEarthBrush.Create(self);
end;

destructor TThematicBreak.Destroy;
begin
  FBreakBrush.Free;

  inherited;
end;

procedure TThematicBreak.LoadEnvironment(Element: TGXML_Element);
begin
  if Element <> nil then
    with Element do
    begin
      BreakValue := FloatAttributeByName( 'BreakValue',BreakValue );
      BreakBrush.LoadEnvironment(ElementByName('BreakBrush', 0));
    end;
end;

function TThematicBreak.SaveEnvironment: TGXML_Element;
begin
  Result := TGXML_Element.Create;

  Result.AddFloatAttribute('BreakValue', BreakValue );
  Result.AddElement('BreakBrush', BreakBrush.SaveEnvironment);
end;

end.
