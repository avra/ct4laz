
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_EarthObjPointsGenerators;

{$MODE DELPHI}

interface

uses LCLIntf, LCLType,  Classes, SysUtils, Graphics, GIS_EarthBase,
  GIS_Classes, GIS_SysUtils, Math,
  GIS_EarthObjects;

type

TBasePointsGenerator = class(TEarthRoot)
  private
    FEarth: TCustomEarth;
    FGeoDataObject:TGeoDataObject; 
    fPointStore:TPointStore;
    FShapeUnit: TEarthUnitTypes;
    fShapeSizeX,fShapeSizeY:Double;
    FTmpOffectX,FTmpOffectY:Double;
    fMaxShapeSizeX,fMaxShapeSizeY: Double;
    FCenterXY: TPointDouble;
    x1,x2,y1,y2:Double;
    w,h:Double;
  protected
    procedure FindVariables(const ptLL: TPointLL);
    Procedure SetMaxShapeSizeX(const val:Double);
    Procedure SetMaxShapeSizeY(const val:Double);
    Procedure SetShapeSizeX(const val:Double);
    Procedure SetShapeSizeY(const val:Double);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    Procedure  Execute(GeoDataObject:TGeoDataObject;
                       PointStore:TPointStore;
                       Earth: TCustomEarth);
    Procedure BuildShape;Virtual;
    Procedure AddPoint(const aPoint:TPoint); Overload;
    Procedure AddPoint(const X,Y:Integer);  Overload;
    Procedure AddPoint(const X,Y:Double);   Overload;
    Function  GetPoint(const anum:Integer):Tpoint;
  published
    property  ShapeUnit: TEarthUnitTypes read FShapeUnit write FShapeUnit;
    property  MaxShapeSizeX: Double read FMaxShapeSizeX write SetMaxShapeSizeX;
    property  MaxShapeSizeY: Double read FMaxShapeSizeY write SetMaxShapeSizeY;
    property  ShapeSizeX:Double read FShapeSizeX write SetShapeSizeX;
    property  ShapeSizeY:Double read FShapeSizeY write SetShapeSizeY;
  end;

TBasePointsGeneratorClass = class of TBasePointsGenerator;

TOvalGenerator = class(TBasePointsGenerator)
  public
    Procedure BuildShape;override;
  end;

TRectangleGenerator = class(TBasePointsGenerator)
  public
    Procedure BuildShape;override;
  end;

TDiamondGenerator = class(TBasePointsGenerator)
  public
    Procedure BuildShape;override;
  end;

TExagonGenerator = class(TBasePointsGenerator)
  public
    Procedure BuildShape;override;
  end;

TRoundRectGenerator = class(TBasePointsGenerator)
  protected
   FRoundFactor:integer;
   Procedure SetRoundFactor(const val:integer);
  public
    constructor Create; override;
    Procedure BuildShape;override;
  published
    property  RoundFactor: integer read FRoundFactor write SetRoundFactor;
  end;

TGArrowType=(atLeft,atRight,atUp,atDown);
TArrowGenerator = class(TBasePointsGenerator)
  protected
   FArrowType:TGArrowType;
   FFactorFat:integer;
   FFactorLong:integer;
   FFactorBalance:integer;
   Procedure SetFactorFat(const val:integer);
   Procedure SetFactorLong(const val:integer);
   Procedure SetFactorBalance(const val:integer);
  public
    constructor Create; override;
    Procedure BuildShape;override;
  published
    property  ArrowType:TGArrowType read FArrowType write fArrowType;
    property  FactorFat:integer read FFactorFat write SetFactorFat;
    property  FactorLong:integer read FFactorLong write SetFactorLong;
    property  FactorBalance:integer read FFactorBalance write SetFactorBalance;

  end;

implementation
//==========================================================
constructor TBasePointsGenerator.Create;
begin
  FEarth := nil;
  FShapeUnit := euPixel;
  FMaxShapeSizeX:=0;
  FMaxShapeSizeY:=0;
  FShapeSizeX := 100;
  FShapeSizeY := 100;
end;

destructor TBasePointsGenerator.Destroy;
 begin
  inherited;
 end;

Procedure  TBasePointsGenerator.Execute(GeoDataObject:TGeoDataObject;
                                          PointStore:TPointStore;
                                          Earth: TCustomEarth);
 begin
   if GeoDataObject=nil then exit;
   if Earth=nil then exit;
   if PointStore=nil then exit;

   fEarth:=Earth;
   FGeoDataObject:=GeoDataObject;
   fPointStore:=PointStore;
   FindVariables(FGeoDataObject.Centroid);
   fPointStore.Clear;

   if (fPointStore=nil) or (FEarth=nil) then exit;
    x1:=fCenterXY.X - FTmpOffectX;
    y1:=fCenterXY.Y - FTmpOffectY;
    x2:=fCenterXY.X + FTmpOffectX;
    y2:=fCenterXY.Y + FTmpOffectY;
    w:=x2-x1;
    h:=y2-y1;

   BuildShape;

   FGeoDataObject.RedrawObject;
 end;

Procedure TBasePointsGenerator.AddPoint(const X,Y:Integer);
 var ll:TPointLL;
 begin
   FEarth.MouseXYToLL(x,y,ll); fPointStore.Add(ll);
 end;

Procedure TBasePointsGenerator.AddPoint(const X,Y:Double);
 var ll:TPointLL;
 begin
   FEarth.MouseXYToLL(Round(x),Round(y),ll); fPointStore.Add(ll);
 end;

Procedure TBasePointsGenerator.AddPoint(const aPoint:TPoint);
 begin
   AddPoint(apoint.x,apoint.y);
 end;

Function TBasePointsGenerator.GetPoint(const anum:Integer):Tpoint;
 var ll:TPointLL;
     p:Tpoint;
 begin
   ll:=fPointStore.AsLL[aNum];
   FEarth.LLToMouseXY(ll.iLongX,ll.iLatY,p);
   result:=p;
 end;

procedure TBasePointsGenerator.FindVariables(const ptLL: TPointLL);
begin
   if (FGeoDataObject=nil) or (FEarth=nil) then exit;

  if FEarth.Projection.PointLLToXY(ptLL, fCenterXY)then
  begin
    FTmpOffectX := FEarth.EarthCanvas.ScaleUnitsToDevice(ShapeSizeX, ShapeUnit);
    FTmpOffectY := FEarth.EarthCanvas.ScaleUnitsToDevice(ShapeSizeY, ShapeUnit);
   //... for X
    if MaxShapeSizeX <> 0 then
     begin
      with FEarth.EarthCanvas do // Scale the MaxShapeSize to the output canvas
       begin
        FTmpOffectX := Min(FTmpOffectX, Round(MaxShapeSizeX * CanvasPixelsPerInch / ScreenPixelsPerInch));
       end;
      end;
     FTmpOffectX := FTmpOffectX / 2;
   //... for X
   if MaxShapeSizeY <> 0 then
     begin
      with FEarth.EarthCanvas do // Scale the MaxShapeSize to the output canvas
       begin
        FTmpOffectY := Min(FTmpOffectY, Round(MaxShapeSizeY * CanvasPixelsPerInch / ScreenPixelsPerInch));
       end;
      end;
    FTmpOffectY := FTmpOffectY / 2;
  end;

end;

Procedure TBasePointsGenerator.SetMaxShapeSizeX(const val:Double);
 begin
  if val<0 then begin fMaxShapeSizeX:=0; exit; end;
  fMaxShapeSizeX:=val;
 end;
Procedure TBasePointsGenerator.SetMaxShapeSizeY(const val:Double);
 begin
  if val<0 then begin fMaxShapeSizeY:=0; exit; end;
  fMaxShapeSizeY:=val;
 end;
Procedure TBasePointsGenerator.SetShapeSizeX(const val:Double);
 begin
  if val<0 then begin fShapeSizeX:=0; exit; end;
  fShapeSizeX:=val;
 end;
Procedure TBasePointsGenerator.SetShapeSizeY(const val:Double);
 begin
  if val<0 then begin fShapeSizeY:=0; exit; end;
  fShapeSizeY:=val;
 end;

Procedure TBasePointsGenerator.BuildShape;
 begin
  //Nothing
 end;

//======================== TOvalGenerator =========================================
Procedure TOvalGenerator.BuildShape;
 var px,py,rx,ry,cx,cy:Double;
     i:integer;
begin

  rx:=(x2-x1)/ 2;
  ry:=(y2-y1)/ 2;

  cx:=x1+rx;
  cy:=y1+ry;

for i:= 0 to 360 do
 begin
  px:=(rx*sin(i*DEG_TORADIANS))+cx;
  py:=(ry*cos(i*DEG_TORADIANS))+cy;

  AddPoint(px,py);
 end;

end;
//==================== TRectangleGenerator ========================================
procedure TRectangleGenerator.BuildShape;
  begin
   AddPoint(x1,y1);
   AddPoint(x2,y1);
   AddPoint(x2,y2);
   AddPoint(x1,y2);
   AddPoint(x1,y1);
  end;

//==================== DiamondGenerator ===========================================
procedure TDiamondGenerator.BuildShape;
  begin
   AddPoint(fCenterXY.X, fCenterXY.Y - FTmpOffectY);
   AddPoint(fCenterXY.X + FTmpOffectX, fCenterXY.Y);
   AddPoint(fCenterXY.X, fCenterXY.Y + FTmpOffectY);
   AddPoint(fCenterXY.X - FTmpOffectX, fCenterXY.Y);
   AddPoint(fCenterXY.X, fCenterXY.Y - FTmpOffectY);
  end;

//==================== TExagonGenerator ===========================================
procedure TExagonGenerator.BuildShape;
  var ix:Double;
begin
  ix:=FTmpOffectX / 2 ;
  AddPoint(fCenterXY.X-FTmpOffectX, fCenterXY.Y);
  AddPoint(fCenterXY.X-ix, fCenterXY.Y-FTmpOffectY);
  AddPoint(fCenterXY.X+ix, fCenterXY.Y-FTmpOffectY);
  AddPoint(fCenterXY.X+FTmpOffectX, fCenterXY.Y);
  AddPoint(fCenterXY.X+ix, fCenterXY.Y+FTmpOffectY);
  AddPoint(fCenterXY.X-ix, fCenterXY.Y+FTmpOffectY);
  AddPoint(fCenterXY.X-FTmpOffectX, fCenterXY.Y);
end;
//===================== TRoundRectGenerator =======================================
constructor TRoundRectGenerator.Create;
begin
  inherited;
  fRoundFactor:=12;
end;

Procedure TRoundRectGenerator.SetRoundFactor(const val:integer);
 begin
   if val<1 then begin fRoundFactor:=1; exit; end;
   if val>44 then begin fRoundFactor:=44; exit; end;
   fRoundFactor:=val;
 end;

procedure TRoundRectGenerator.BuildShape;
 var px,py,rx,ry,cx,cy:Double;

 procedure FindArc(const a,b :integer);
  var j:integer;
   begin

     for j:=a to b do
       begin
        px:=round(rx*sin(j*DEG_TORADIANS))+cx;
        py:=round(ry*cos(j*DEG_TORADIANS))+cy;

        AddPoint(px,py);
       end;

   end;
begin

  rx:=(x2-x1)/ 2;
  ry:=(y2-y1)/ 2;

  cx:=x1+rx;
  cy:=y1+ry;

  FindArc(45-fRoundFactor,45+fRoundFactor);
  FindArc(135-fRoundFactor,135+fRoundFactor);
  FindArc(225-fRoundFactor,225+fRoundFactor);
  FindArc(315-fRoundFactor,315+fRoundFactor);
  AddPoint(GetPoint(0));
end;

//==================== TArrowGenerator ===========================================
constructor TArrowGenerator.Create;
begin
  inherited;
  FFactorFat:=50;
  FFactorLong:=50;
  FFactorBalance:=50;
end;

Procedure TArrowGenerator.SetFactorFat(const val:integer);
 begin
   if val<1 then begin fFactorFat:=1; exit; end;
   if val>99 then begin fFactorFat:=99; exit; end;
   fFactorFat:=val;
 end;

Procedure TArrowGenerator.SetFactorLong(const val:integer);
 begin
   if val<1 then begin fFactorLong:=1; exit; end;
   if val>99 then begin fFactorLong:=99; exit; end;
   fFactorLong:=val;
 end;

Procedure TArrowGenerator.SetFactorBalance(const val:integer); 
 begin
   if val<1 then begin fFactorBalance:=1; exit; end;
   if val>99 then begin fFactorBalance:=99; exit; end;
   fFactorBalance:=val;
 end;

procedure TArrowGenerator.BuildShape;
 var h16,w16,h4,w4,h2,w2:Double;
begin

  h16:=(h*fFactorFat/200);
  w16:=(W*fFactorFat/200);
  h4:=(h*fFactorLong/100);
  w4:=(W*fFactorLong/100);
  h2:=(h*fFactorBalance/100);
  w2:=(W*fFactorBalance/100);

  case fArrowType of
   atRight:
        begin
        AddPoint(x1,y1+h2-h16);
        AddPoint(x1+w-1-w4,y1+h2-h16);
        AddPoint(x1+w-1-w4,y1);
        AddPoint(x1+w-1,y1+h2);
        AddPoint(x1+w-1-w4,y1+h-1);
        AddPoint(x1+w-1-w4,y1+h2+h16);
        AddPoint(x1,y1+h2+h16);
        end;
    atLeft:
        begin
        AddPoint(x1+w-1,y1+h2-h16);
        AddPoint(x1+w4,y1+h2-h16);
        AddPoint(x1+w4,y1);
        AddPoint(x1,y1+h2);
        AddPoint(x1+w4,y1+h-1);
        AddPoint(x1+w4,y1+h2+h16);
        AddPoint(x1+w-1,y1+h2+h16);
        end;
    atUp:
        begin
        AddPoint(x1+w2-w16,y1+h-1);
        AddPoint(x1+w2-w16,y1+h4);
        AddPoint(x1,y1+h4);
        AddPoint(x1+w2,y1);
        AddPoint(x1+w-1,y1+h4);
        AddPoint(x1+w2+w16,y1+h4);
        AddPoint(x1+w2+w16,y1+h-1);
        end;
     atDown:
        begin
        AddPoint(x1+w2-w16,y1);
        AddPoint(x1+w2-w16,y1+h-1-h4);
        AddPoint(x1,y1+h-1-h4);
        AddPoint(x1+w2,y1+h-1);
        AddPoint(x1+w-1,y1+h-1-h4);
        AddPoint(x1+w2+w16,y1+h-1-h4);
        AddPoint(x1+w2+w16,y1);
        end;
     end;
end;




 //****************************************************************************
//****************************************************************************
initialization
  RegisterClasses([TBasePointsGenerator,
                   TOvalGenerator,
                   TRoundRectGenerator,
                   TRectangleGenerator,
                   TDiamondGenerator,
                   TExagonGenerator,
                   TArrowGenerator]);

end.
