
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_EarthBase;

interface

uses
  LCLIntf, LCLType, LMessages,
  SysUtils, Messages, Classes, Graphics, Controls, StdCtrls, Types, Math,
  Printers, Forms, Dialogs, contnrs,
  GIS_EarthObjectData, GIS_SysUtils, GIS_Classes, GIS_XML, GIS_EarthGrid,
  GIS_EarthScaleShower, ExtraTGSysUtils, GIS_EarthObjects,
  GR32, FileUtil, lazfileutils;

const
  CACHE_RELEASE_PERCENT = 25;
  TEarth_VERSION = 'TEarth 2018';
  TEarth_COPYRIGHT = 'Copyright PilotLogic';
  TEarth_EMAIL = 'Email: support@pilotlogic.com';
  TEarth_WWW = 'https://www.pilotlogic.com/';
  SHAREMSG = TEarth_VERSION + ' ' + TEarth_COPYRIGHT + ' ' + TEarth_EMAIL;

type
  TEarthBase = class;

  TEarthMouseButton = (mzbLeft, mzbRight, mzbMiddle, mzbNone);

  TSaveMetaDataLocation = (smdInDataDirectory, smdWithDataFile, smdWithExeFile, smdDiscard);

  TEarthProgressEvent = procedure(Sender: TEarthBase; MsgType: TGISProgressMessage; const MsgText: string; var Abort: boolean) of object;
  TEarthObjectRenderEvent = procedure(Sender: TEarthBase; EarthObject: TEarthObject; State: TEarthObjectStateSet) of object;
  TEarthSourceNotifyEvent = procedure(Sender: TEarthBase; ObjectSource: TEarthObjectStore) of object;
  TEarthEnvironmentEvent = procedure(Sender: TEarthBase; Document: TGXML_Document) of object;
  TEarthFileNotFound = procedure(Sender: TEarthBase; var Filename: TFilename; var TryFilename: boolean) of object;
  TEarthDirectoryNotFound = procedure(Sender: TEarthBase; var DirectoryName: string) of object;


  TEarthBase = class(TCustomEarth)
  private
    FZoomFactor:Single;
    FSaveMetaDataLocation: TSaveMetaDataLocation;
    FSurfaceColor: TColor32;
    FBackgroundColor: TColor32;
    FBackgroundImage: TBitmap32;
    FiEarthFontAngle: integer;
    FEarthFontUnit: TEarthUnitTypes;
    FGrid: TEarthGrid;
    FScaleShower: TEarthScaleShower;
    FMouseZoomButton: TEarthMouseButton;
    FMouseSelectButton: TEarthMouseButton;
    FMouseRotateButton: TEarthMouseButton;
    FbRotating, FbZooming: boolean;
    FZoomRect: TRect;
    FEnvironmentFile: TFilename;
    FSurfaceTextureName: TFilename;
    FObjectSources: TObjectList;
    FToolsPoints: TSPointsStore;
    //.... Events ......................
    FOnFileNotFound: TEarthFileNotFound;
    FOnReduceCacheMemory: TNotifyEvent;
    FOnPaintGrid: TNotifyEvent;
    FOnPaint: TNotifyEvent;
    FOnProgress: TEarthProgressEvent;
    FOnObjectRender: TEarthObjectRenderEvent;
    FOnObjectCreate: TEarthObjectNotifyEvent;
    FOnObjectFree: TEarthObjectNotifyEvent;
    FOnObjectClick: TEarthObjectNotifyEvent;
    FOnObjectSourceCreate: TEarthSourceNotifyEvent;
    FOnObjectSourceActiveChange: TEarthSourceNotifyEvent;
    FOnObjectSourceFree: TEarthSourceNotifyEvent;
    FOnSaveEnvironment: TEarthEnvironmentEvent;
    FOnLoadEnvironment: TEarthEnvironmentEvent;
    FOnLayersChange: TNotifyEvent;
    FOnDirectoryNotFound: TEarthDirectoryNotFound;
    FOnZoomed: TNotifyEvent;
    FOnRender: TNotifyEvent;
    FOnPanned: TNotifyEvent;

    function GetCacheMemoryUsed: integer;
    procedure SetBackgroundColor(Value: TColor32);
    procedure SetBackgroundBitmap(NewBitmap: TBitmap32);
    procedure SetSurfaceColor(Value: TColor32);
    procedure SetSurfaceTextureName(sName: TFilename);
    procedure WMSize(var Message: TLMSize); message LM_SIZE;
    procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Msg: TWMVScroll); message WM_VSCROLL;
    class function InstanceCount(iChange: integer): integer;

  protected
    ptRotate: TPointLL;
    FIsUpdating: boolean; //must set to work the Begin/EndUpdate  
    procedure MouseLeave; override;
    procedure MouseEnter; override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure SetParent(NewParent: TWinControl); override;
    function  GetMaxTextHeight: double; override;
    function  GetMinTextHeight: double; override;
    procedure SetDataDirectory(const Value: string); override;
    procedure SetEarthOptions(Value: TEarthOptionsSet); override;
    procedure SetMaxTextHeight(iValue: double); override;
    procedure SetMinTextHeight(iValue: double); override;
    procedure SetScrollBars(Value: TScrollStyle); override;
    procedure SetViewRect(const sRect: TRect); override;
    procedure SeTTitlesRenderMethod(const val: TTitlesRenderMethod); override;
    procedure PaintWindowBackground; virtual;
    procedure PaintGrid; virtual;
    procedure RenderLayers; virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: integer); override;
    procedure DrawLayers; virtual; //Main Draw Layers Procedure
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    // procedure Loaded; override;
    procedure LoadedAll; override;
    procedure UpdateScrollBars; override;
    function  NextMRU: integer; override;
    function  UpdateTotalMemoryInUse: integer; override;
    function  CacheUsedDelta(delta: integer): integer; override;
    function  ReduceCacheMemory(iTargetSize: integer): integer; override;
    function  ProgressMessage(MsgType: TGISProgressMessage; const sMsg: string): boolean; override;
    procedure GetPOZ(var POZ: TEarthPOZ); virtual;
    procedure SetPOZ(const POZ: TEarthPOZ); virtual;
    function  SaveEnvironment: TGXML_Element;
    procedure LoadEnvironment(Element: TGXML_Element);
    procedure SaveEnvironmentToFile(const sFilename: TFilename);
    procedure LoadEnvironmentFromFile(const sFilename: TFilename);
    procedure SaveEnvironmentToStream(Stream: TStream);
    procedure LoadEnvironmentFromStream(Stream: TStream);

    function GetLayerFromObject(Obj: TEarthObject): TEarthLayer;
    function GetPresenterFromObject(Obj: TEarthObject): TEarthPresenter;
    function GetEarthObjectFromGroupsStore(aGroupsStore: TGroupsStore): TGeoDataObject;
    function GetEarthObjectFromPointStore(aPointStore: TPointStore): TGeoDataObject;
    function GetEarthObjectFromTitle(aTitle: string; Obj: TEarthObject): TEarthObject;

    function PLLGetLong(const ptLL: TPointLL): double;
    function PLLGetLat(const ptLL: TPointLL): double;
    function PLLGetHeight(const ptLL: TPointLL): double;
    function PLLAsText(const iLong, iLat: double): string;
    function PLLAsText2(const ptLL: TPointLL): string;
    function PLLAsTextU(const iLong, iLat: double; aUnitsformat: TUnitsformat): string;
    function PLLAsTextU2(const ptLL: TPointLL; aUnitsformat: TUnitsformat): string;
    function PLLAsTextFm(const iLong, iLat: double; sFmt: string): string;
    function PLLAsTextFm2(const ptLL: TPointLL; sFmt: string): string;
    function EarthUnitsToText(const aValue: double; const sFmt: string): string;
    function TextToEarthUnits(const aText: string; const sFmt: string): double;

    function EarthUnitsFrom(const eValue: extended; Units: TEarthUnitTypes): double;
    function EarthUnitsTo(iValue: double; Units: TEarthUnitTypes): extended;
    function HeightUnitsFrom(const eValue: extended; Units: THeightUnitTypes): double;
    function HeightUnitsTo(iValue: double; Units: THeightUnitTypes): extended;

    procedure SetZoomFactorToDefault;
    function  GetZoomStep: double;
    procedure Zoom(const ZD: single);
    procedure ZoomIn;
    procedure ZoomOut;
    procedure ZoomToMax;
    procedure ZoomToMin;
    procedure ZoomToDefault;

    function  GetRotateStep: double;
    procedure Rotate(const XD, YD: single);
    procedure RotateUp;
    procedure RotateDown;
    procedure RotateLeft;
    procedure RotateRight;

    procedure PanViewXY(iX, iY: double); virtual;
    function  GetFullSurfaceTextureName: string;
    procedure Clear; virtual;
    procedure Resize; override;

    function  FindCenterPointLL(const a, b: TPointLL): TPointLL;
    procedure SetDefaultValues;
    procedure DoEarthPropertyChanged(Sender: TObject);
    procedure Notify(aNotification: TEarthNotification; Obj: TEarthRoot); override;
    function  ResolveFilename(const sFilename: TFilename): TFilename; override;
    procedure RenderToCanvas(ACanvas: TCanvas; ARect: TRect); override;
    procedure RedrawLayers; override;
    procedure Paint; override;
    procedure BeginUpdate; override;
    procedure EndUpdate; override;
    procedure DoOnRender;
    procedure DoOnPaint;
    procedure DoLayersChange;
    property CacheMemoryUsed: integer read GetCacheMemoryUsed;
    property ObjectSources: TObjectList read FObjectSources;
    property Layers;
    property BackgroundTextureMap;
    property EarthCanvas;
    property ViewRect;
    property MouseCapture;
    property ToolsPoints: TSPointsStore read fToolsPoints stored False;
  published
    property SelectedOjectOptions: TSelectedOjectOptions read FSelectedOjectOptions write FSelectedOjectOptions;
    property TransparentOptions: TEarthTransparent read FTransparentOptions write FTransparentOptions;
    property Grid: TEarthGrid read FGrid write FGrid;
    property ScaleShower: TEarthScaleShower read FScaleShower write FScaleShower;
    property BackgroundImage: TBitmap32 read FBackgroundImage write SetBackgroundBitmap;
    property BackgroundColor: TColor32 read FBackgroundColor write SetBackgroundColor;

    property DataDirectory: string read FDataDirectory write SetDataDirectory;
    property MouseZoomButton: TEarthMouseButton read FMouseZoomButton write FMouseZoomButton default mzbRight;
    property MouseSelectButton: TEarthMouseButton read FMouseSelectButton write FMouseSelectButton default mzbLeft;
    property MouseRotateButton: TEarthMouseButton read FMouseRotateButton write FMouseRotateButton default mzbLeft;
    property EnvironmentFile: TFilename read FEnvironmentFile write LoadEnvironmentFromFile;
    property SaveMetaDataLocation: TSaveMetaDataLocation read FSaveMetaDataLocation write FSaveMetaDataLocation;
    property SurfaceColor: TColor32 read FSurfaceColor write SetSurfaceColor;
    property SurfaceTextureName: TFilename read FSurfaceTextureName write SetSurfaceTextureName;
    property ZoomFactor: Single read FZoomFactor write FZoomFactor;

    property OnPanned: TNotifyEvent read FOnPanned write FOnPanned;
    property OnRender: TNotifyEvent read FOnRender write FOnRender;
    property OnZoomed: TNotifyEvent read FOnZoomed write FOnZoomed;
    property OnReduceCacheMemory: TNotifyEvent read FOnReduceCacheMemory write FOnReduceCacheMemory;
    property OnPaintGrid: TNotifyEvent read FOnPaintGrid write FOnPaintGrid;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
    property OnObjectRender: TEarthObjectRenderEvent read FOnObjectRender write FOnObjectRender;
    property OnObjectClick: TEarthObjectNotifyEvent read FOnObjectClick write FOnObjectClick;
    property OnObjectCreate: TEarthObjectNotifyEvent read FOnObjectCreate write FOnObjectCreate;
    property OnObjectFree: TEarthObjectNotifyEvent read FOnObjectFree write FOnObjectFree;
    property OnObjectSourceCreate: TEarthSourceNotifyEvent read FOnObjectSourceCreate write FOnObjectSourceCreate;
    property OnObjectSourceActiveChange: TEarthSourceNotifyEvent read FOnObjectSourceActiveChange write FOnObjectSourceActiveChange;
    property OnObjectSourceFree: TEarthSourceNotifyEvent read FOnObjectSourceFree write FOnObjectSourceFree;
    property OnProgress: TEarthProgressEvent read FOnProgress write FOnProgress;
    property OnSaveEnvironment: TEarthEnvironmentEvent read FOnSaveEnvironment write FOnSaveEnvironment;
    property OnLoadEnvironment: TEarthEnvironmentEvent read FOnLoadEnvironment write FOnLoadEnvironment;
    property OnDirectoryNotFound: TEarthDirectoryNotFound read FOnDirectoryNotFound write FOnDirectoryNotFound;
    property OnFileNotFound: TEarthFileNotFound read FOnFileNotFound write FOnFileNotFound;
    property OnLayersChange: TNotifyEvent read FOnLayersChange write FOnLayersChange;
    property Anchors;
    property OnSelectedLayerChange;
    property OnPresenterStoreChange;
    property OnObjectSelectChange;
    property OnObjectStoreChange;
    property CacheCapacity;
    property EarthOptions;
    property MinTextHeight;
    property MaxTextHeight;
    property Projection;
    property ScrollBars;
    property Align;
    property DragCursor;
    property DragMode;
    property Enabled;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Visible;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
  end;

implementation

uses GIS_EarthUtils;

constructor TEarthBase.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := ControlStyle + [csOpaque];

  SetInitialBounds(0, 0, 200, 200);

  FSelectedLayer := nil;
  FSelectedPresentersStore := nil;
  FSelectedObjectStore := nil;

  FIsUpdating := False;
  SetZoomFactorToDefault;
  fBackgroundColor := clblack32;
  FSurfaceColor := $FF7482B0;

  SetupBitmap(True);
  bitmap.SetSize(Width, Height);

  bitmap.Clear(fBackgroundColor);

  //..... Greate Earth Base objects..........................
  FEarthCanvas := TEarthCanvas.Create(Self);
  FLayerStore := TEarthLayerStore.Create(Self);
  FProjection := TEarthProjection.Create(Self);
  FBackgroundTextureMap := TTextureData.Create;
  FObjectSources := TObjectList.Create(false);

  //..... Greate Earth Attributes objects.....................
  FSelectedOjectOptions := TSelectedOjectOptions.Create;
  FSelectedOjectOptions.OnChange := @DoEarthPropertyChanged;

  FGrid := TEarthGrid.Create(self);
  FGrid.OnChange := @DoEarthPropertyChanged;

  FTransparentOptions := TEarthTransparent.Create;
  FTransparentOptions.OnChange := @DoEarthPropertyChanged;

  FScaleShower := TEarthScaleShower.Create(self);
  FScaleShower.OnChange := @DoEarthPropertyChanged;

  //..... Greate Earth Draw objects..........................

  FBackgroundImage := TBitmap32.Create;
  FBackgroundImage.OnChange := @DoEarthPropertyChanged;

  FToolsPoints := TSPointsStore.Create;
  FToolsPoints.OnPaint := nil;

  //.............. Setup Proprties ...........................
  FiEarthFontAngle := 0;
  FEarthFontUnit := euNauticalMile;
  Font.Name := 'Arial';
  Font.Color := clBlack;
  Font.Size := 9;
  FEarthOptions := [goDoubleBuffered, goCache3DPoints];
  fScrollBars := ssBoth;
  FMouseZoomButton := mzbRight;
  FMouseSelectButton := mzbLeft;
  FMouseRotateButton := mzbLeft;
  FiMinTextHeight := 5;
  FCacheCapacity := 1024 * 10;
  fTitlesRenderMethod := trtAfterLayer;

end;

destructor TEarthBase.Destroy;
var
  idx: integer;
begin
  if FToolsPoints <> nil then
    FToolsPoints.OnPaint := nil;

  if FToolsPoints <> nil then
    FToolsPoints.Free;
  if FLayerStore <> nil then
    FLayerStore.Free;
  if FSelectedOjectOptions <> nil then
    FSelectedOjectOptions.Free;
  if FGrid <> nil then
    FGrid.Free;
  if FScaleShower <> nil then
    FScaleShower.Free;
  if FTransparentOptions <> nil then
    FTransparentOptions.Free;

  if FBackgroundImage <> nil then
    FBackgroundImage.Free;
  if FEarthCanvas <> nil then
    FEarthCanvas.Free;
  if FProjection <> nil then
    FProjection.Free;
  if BackgroundTextureMap <> nil then
    BackgroundTextureMap.Free;

  // Destroy all owned ObjectSources
  if FObjectSources <> nil then
   begin
    for idx := FObjectSources.Count - 1 downto 0 do
      TEarthObjectStore(FObjectSources.Items[idx]).Free;
     
     FObjectSources.Free;
   end;


  inherited Destroy;
end;

class function TEarthBase.InstanceCount(iChange: integer): integer;
var
  iInstanceCount: integer;
begin
  iInstanceCount := 0;
  iInstanceCount := iInstanceCount + iChange;
  Result := iInstanceCount;
end;

procedure TEarthBase.SetParent(NewParent: TWinControl);
var
  i: integer;
begin
  inherited;
  //---dummy ----
  if NewParent <> nil then
  begin
    i := Width;
    Width := 100;
    Width := i;
  end;
end;

procedure TEarthBase.LoadedAll;
var
  i: integer;
begin
  inherited;
  //---dummy ----
  i := Width;
  Width := 100;
  Width := i;

  Projection.Altitude:=0;
end;

procedure TEarthBase.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);

  case FScrollBars of
    ssHorizontal:
      Params.Style := Params.Style or WS_HSCROLL;
    ssVertical:
      Params.Style := Params.Style or WS_VSCROLL;
    ssBoth:
      Params.Style := Params.Style or WS_HSCROLL or WS_VSCROLL;
  end;
  if goTransparentBackground in EarthOptions then
    Params.ExStyle := Params.ExStyle or WS_EX_TRANSPARENT;
end;

procedure TEarthBase.Notify(aNotification: TEarthNotification; Obj: TEarthRoot);
begin
  if Layers <> nil then
  begin

    case aNotification of
      gnLayerCreate:
      begin
        // Layers.Add(TEarthLayer(Obj));
        DoLayersChange;
      end;

      gnLayerFree:
      begin
        // Layers.Remove(TEarthLayer(Obj));
        DoLayersChange;
      end;
      gnLayersChanged:
      begin
        DoLayersChange;
      end;

      gnObjectSourceCreate:
      begin
        // Add to object source list
        FObjectSources.Add(TEarthObjectStore(Obj));

        if not (csDestroying in ComponentState) then
        begin
          if Assigned(OnObjectSourceCreate) then
            OnObjectSourceCreate(Self, TEarthObjectStore(Obj));
        end;
      end;

      gnObjectSourceActiveChange:
      begin
        if not (csDestroying in ComponentState) then
        begin
          if Assigned(OnObjectSourceActiveChange) then
            OnObjectSourceActiveChange(Self, TEarthObjectStore(Obj));

        end;
      end;

      gnObjectSourceFree:
      begin
        if not (csDestroying in ComponentState) then
        begin
          if Assigned(OnObjectSourceFree) then
            OnObjectSourceFree(Self, TEarthObjectStore(Obj));

        end;
        // Remove from object source list
        FObjectSources.Delete(FObjectSources.IndexOf(TEarthObjectStore(Obj)));
      end;

      gnObjectFree:
      begin
        if Assigned(OnObjectFree) then
          OnObjectFree(Self, TEarthObject(Obj));
      end;

      gnObjectSelectChange:
      begin
        if Assigned(OnObjectSelectChange) then
          OnObjectSelectChange(Self, TEarthObject(Obj));
      end;
      gnPresenterStoreChange:
      begin
        if Assigned(OnPresenterStoreChange) then
          OnPresenterStoreChange(Self, TEarthPresenterStore(Obj));
      end;
    end;

    RedrawLayers;
  end;
end;

procedure TEarthBase.MouseEnter;
begin
  inherited;
end;

procedure TEarthBase.MouseLeave;
begin
  inherited;
end;

procedure TEarthBase.Clear;
begin
  FEnvironmentFile := '';
  Layers.Clear;
end;

procedure TEarthBase.SetDefaultValues; //By Sternas Stefanos
begin
  FEnvironmentFile := '';
  SurfaceTextureName := '';
  BackgroundColor := clBlack32;
  SurfaceColor := $FF7482B0;

  FiEarthFontAngle := 0;
  FEarthFontUnit := euNauticalMile;
  Font.Name := 'Arial';
  Font.Color := clBlack;
  Font.Size := 9;

  Projection.Spheroid := WGS84;
  Projection.ProjectionClass := 'TSphericalPrj';

  Grid.Clear;
  ScaleShower.Clear;
  TransparentOptions.Clear;
  SelectedOjectOptions.Clear;
end;

//.............. Zoom ........................
procedure TEarthBase.SetZoomFactorToDefault;
begin
  FZoomFactor:=1.2;
end;

function TEarthBase.GetZoomStep: double;
var
  Alx: double;
begin
  Alx := fProjection.Altitude;
  Result := FZoomFactor * Alx / 100;   //0.9%
  if Result < GMINALTITUDE then
    Result := GMINALTITUDE;
end;

procedure TEarthBase.Zoom(const ZD: single);
begin
  Projection.Altitude := Projection.Altitude + ZD;
end;

procedure TEarthBase.ZoomOUT;
var
  Alx, d: double;
begin
  Alx := fProjection.Altitude;
  d := GetZoomStep;
  Alx := Alx + d;
  fProjection.Altitude := Alx;
end;

procedure TEarthBase.ZoomIN;
var
  Alx, d: double;
begin
  Alx := fProjection.Altitude;
  d := GetZoomStep;
  Alx := Alx - d;
  fProjection.Altitude := Alx;
end;

procedure TEarthBase.ZoomToMAX;
begin
  fProjection.Altitude := GMAXALTITUDE;
end;

procedure TEarthBase.ZoomToMIN;
begin
  fProjection.Altitude := GMINALTITUDE;
end;

procedure TEarthBase.ZoomToDefault;  
begin
  fProjection.Altitude := 0;
end;

//................. Rotate ..........................
function TEarthBase.GetRotateStep: double;
var
  Alx: double;
begin
  Alx := fProjection.Altitude;
  Result := 0.1 * Alx / 100;   //0.1%
  Result := Result / 2000;
end;

procedure TEarthBase.Rotate(const XD, YD: single);
begin
  with Projection do
  begin
    XRotation := XRotation + YD;
    YRotation := YRotation + XD;
    if goInterruptable in EarthOptions then
      PaintAbort := True;
  end;
end;

procedure TEarthBase.RotateUp;
begin
  fProjection.CenterLatitude := fProjection.CenterLatitude + GetRotateStep;
end;

procedure TEarthBase.RotateDown;
begin
  fProjection.CenterLatitude := fProjection.CenterLatitude - GetRotateStep;
end;

procedure TEarthBase.RotateLeft;
begin
  fProjection.CenterLongitude := fProjection.CenterLongitude - GetRotateStep;
end;

procedure TEarthBase.RotateRight;
begin
  fProjection.CenterLongitude := fProjection.CenterLongitude + GetRotateStep;
end;

//................................ RenderLayers ..................................
procedure TEarthBase.RenderLayers;
begin
  { Draw Earth back for traTransparent opion}
  if goTransparentEarth in EarthOptions then
  begin
    Projection.RenderBackFace := True; //invert Projection
    Layers.Render(False);
    if FGrid.DrawMode <> gdNone then
      PaintGrid; //Draw the Grid if required
    Projection.RenderBackFace := False;
  end;

  // Draw the Grid at Behind if required
  if FGrid.DrawMode = gdBehind then
    PaintGrid;

  Layers.Render(False);

  // Draw the Grid at Front if required
  if FGrid.DrawMode = gdInFront then
    PaintGrid;

  fscaleshower.Render;

  if fToolsPoints.Active then
    fToolsPoints.Paint;
end;
//................................ PaintWindowBackground ...........................
procedure TEarthBase.PaintWindowBackground;
var
  ifx, ify, iX, iY: integer;
begin
  with EarthCanvas do
  begin
    { Make sure background color is correct }
    // Brush.Color := Color;
    // Brush.Style := Graphics.bsSolid;
    ClearAll(FBackgroundColor);

    if not BackgroundImage.Empty then { if no bitmap specified }
      with BackgroundImage do { tile bitmap }
      begin
        ifx := (EarthCanvas.CanvasWidth div Width) * Width;
        ify := (EarthCanvas.CanvasHeight div Height) * Height;

        iX := 0;
        while iX <= ifx do
        begin
          iY := 0;
          while iY <= ify do
          begin
            { Draw the bitmap }
            EarthCanvas.gDrawBitMap32(iX, iY, BackgroundImage);
            Inc(iY, Height);
          end;
          Inc(iX, Width);
        end;
      end;

    { Draw the projection background}
    if not (goTransparentEarth in EarthOptions) then
    begin
      { Draw the Projection background }

      EarthCanvas.LineColorSet(EarthCanvas.Color32ToColorAgx(fSurfaceColor));
      EarthCanvas.FillColorSet(EarthCanvas.Color32ToColorAgx(fSurfaceColor));
      Projection.ProjectionModel.PaintSurface;
    end;
  end;
end;

//....................... PaintGrid ....................................
procedure TEarthBase.PaintGrid;
begin
  FGrid.Pen.RenderAttribute(EarthCanvas, Projection.RenderBackFace);
  FGrid.PaintGrid;

  if Assigned(FOnPaintGrid) then
    FOnPaintGrid(Self);
end;

//................................ RedrawLayers ...........................
procedure TEarthBase.RedrawLayers;
begin
  FbRedrawLayers := True;
  Invalidate;
end;

procedure TEarthBase.DrawLayers;
begin
  if Printer <> nil then
    if Printer.Printing then
      Exit;

  PaintAbort := False;
  try
    if FbRedrawLayers or not (goDoubleBuffered in EarthOptions) then
    begin
      EarthCanvas.Attach32(self.Bitmap, False);
      PaintWindowBackground;
      RenderLayers;
      if not PaintAbort then
        DoOnRender;
    end;
  finally
    Layers.Render(True); // paint the animated layers
    if not PaintAbort then
      DoOnPaint;
    FbRedrawLayers := False;

    if not PaintAbort then
      UpdateTotalMemoryInUse;// Update the Total memory in use
    if PaintAbort then
      RedrawLayers; // if painting has been interrupted then force a final redraw
  end;

end;

//.................................Paint .....................................
procedure TEarthBase.Paint;
begin
  if FIsUpdating then
    exit; //must set to work the Begin/EndUpdate
  DrawLayers;
  inherited Paint;
end;


procedure TEarthBase.SetEarthOptions(Value: TEarthOptionsSet);
begin
  // if Transparent background setting has changed
  if (goTransparentBackground in FEarthOptions) <> (goTransparentBackground in Value) then
  begin
    if goTransparentBackground in FEarthOptions then
    begin
      BackgroundImage.Width := 0;
      BackgroundImage.Height := 0;
    end;

    paint;
  end;

  FEarthOptions := Value;
  UpdateScrollBars;
  RedrawLayers;
end;

procedure TEarthBase.SetScrollBars(Value: TScrollStyle);
begin
  if Value <> FScrollBars then
  begin
    FScrollBars := Value;
    paint;
  end;
end;


procedure TEarthBase.DoEarthPropertyChanged(Sender: TObject);
begin
  RedrawLayers;
end;

procedure TEarthBase.BeginUpdate;
begin
  FIsUpdating := True;
  inherited BeginUpdate;
end;

procedure TEarthBase.EndUpdate;
begin
  FIsUpdating := False;
  inherited EndUpdate; //this call Invalidate procedure
end;

//----------------GetPOZ----------------------------------------------------
procedure TEarthBase.GetPOZ(var POZ: TEarthPOZ);
begin
  with POZ do
  begin
    eSf := Projection.ScaleFactor;
    iRotX := (Projection.XRotation * GU_DEGREE);
    iRotY := (Projection.YRotation * GU_DEGREE);
    iRotZ := (Projection.ZRotation * GU_DEGREE);
    CenterXY := Projection.CenterXY;
  end;
end;

procedure TEarthBase.SetPOZ(const POZ: TEarthPOZ);
begin
  with POZ do
  begin
    Projection.ScaleFactor := eSF;
    Projection.XRotation := iRotX / GU_DEGREE;
    Projection.YRotation := iRotY / GU_DEGREE;
    Projection.ZRotation := iRotZ / GU_DEGREE;
    Projection.CenterXY := CenterXY;
  end;
end;

procedure TEarthBase.SetMaxTextHeight(iValue: double);
begin
  if iValue <> FiMaxTextHeight then
  begin
    FiMaxTextHeight := iValue;
    RedrawLayers;
  end;
end;

procedure TEarthBase.SetMinTextHeight(iValue: double);
begin
  if iValue <> FiMinTextHeight then
  begin
    FiMinTextHeight := iValue;
    RedrawLayers;
  end;
end;

function TEarthBase.GetMaxTextHeight: double;
begin
  if csDesigning in ComponentState then
    Result := FiMaxTextHeight
  else
    with EarthCanvas do
      Result := MulDivFloat(FiMaxTextHeight, CanvasPixelsPerInch, ScreenPixelsPerInch);
end;

function TEarthBase.GetMinTextHeight: double;
begin
  if csDesigning in ComponentState then
    Result := FiMinTextHeight
  else
    with EarthCanvas do
      Result := MulDivFloat(FiMinTextHeight, CanvasPixelsPerInch, ScreenPixelsPerInch);
end;

procedure TEarthBase.SeTTitlesRenderMethod(const val: TTitlesRenderMethod);
begin
  if fTitlesRenderMethod = val then
    exit;
  fTitlesRenderMethod := val;
  RedrawLayers;
end;

procedure TEarthBase.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  Obj: TEarthObject;
begin

  if FbZooming then
  begin
    FbZooming := False;
    MouseCapture := False;
    Invalidate;
    Exit;
  end;

  if (Shift * [ssShift, ssAlt, ssCtrl, ssDouble]) = [] then //Check for zooming
    if Ord(Button) = Ord(FMouseZoomButton) then
    begin
      FZoomRect := Rect(X, Y, X + 1, Y + 1);
      FbZooming := True;
      MouseCapture := True;
    end;

  if Ord(Button) = Ord(FMouseRotateButton) then      //Check for Rotating
    if Projection.DeviceXYToLL(X, Y, ptRotate) then
    begin
      FbRotating := True;
      MouseCapture := True;
    end;

  //... Find Selected Object ...........
  Obj := Layers.ObjectAtXY(X, Y);
  if (Shift * [ssShift, ssAlt, ssCtrl, ssDouble]) = [] then
    if (Ord(Button) = Ord(FMouseSelectButton)) then
      self.SelectedObject := Obj;

  if Obj <> nil then
  begin
    if (goMultiSelect in EarthOptions) and (ssCtrl in Shift) and (Ord(Button) = Ord(FMouseSelectButton)) then
      Obj.Selected := True;// not Obj.Selected; <<Old selected method>>

    if Assigned(FOnObjectClick) and (Button = mbLeft) then
      FOnObjectClick(Self, Obj);

  end;
  //.................................

  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TEarthBase.MouseMove(Shift: TShiftState; X, Y: integer);
var
  pTmp: TPointLL;

  procedure XDrawXORRect;
  begin
    Canvas.Pen.Style := PsSolid;
    Canvas.Pen.Color := clRed;//clblack;
    Canvas.Pen.mode := pmNotXor;
    Canvas.Pen.Width := 2;
    Canvas.Brush.Style := bsClear;
    Canvas.Rectangle(FZoomRect.Left, FZoomRect.Top, FZoomRect.Right, FZoomRect.Bottom);

  end;

begin

  if FbZooming then //Make Zooming
  begin
    XDrawXORRect;//EarthCanvas.gDrawFocusRect(FZoomRect);
    FZoomRect.BottomRight := Point(X, Y);
    XDrawXORRect;//EarthCanvas.gDrawFocusRect(FZoomRect);
    if goInterruptable in EarthOptions then
      PaintAbort := True;

  end;

  if FbRotating then  //Make Rotating
    with Projection do
      if DeviceXYToLL(X, Y, pTmp) then
      begin
        XRotation := XRotation + (ptRotate.iLatY - pTmp.iLatY) / GU_DEGREE;
        YRotation := YRotation + Mod180Float(ptRotate.iLongX - pTmp.iLongX) / GU_DEGREE;
        if goInterruptable in EarthOptions then
          PaintAbort := True;
      end;

  inherited MouseMove(Shift, X, Y);
end;


procedure TEarthBase.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  MouseCapture := False;

  if FbZooming then // Make Zooming
  begin
    FbZooming := False;
    MouseCapture := False;

    with FZoomRect do
      with Projection do // Convert to EarthUnits from Device Points
      begin
        Left := Round(Left / ScaleFactor);
        Right := Round(Right / ScaleFactor) - Left;
        Top := Round(Top / ScaleFactor);
        Bottom := Round(Bottom / ScaleFactor) - Top;
      end;
    ViewRect := FZoomRect;
    RedrawLayers;
  end;

  FbRotating := False; //Stop Rotating

  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TEarthBase.SetDataDirectory(const Value: string);
begin
  FDataDirectory := Trim(Value);

  if Value <> '' then
    if Value[Length(Value)] <> '\' then
      FDataDirectory := Value + '\';

  if (DataDirectory <> '') and not DirectoryExistsUTF8(DataDirectory) then
  begin
    if Assigned(OnDirectoryNotFound) then
      OnDirectoryNotFound(Self, FDataDirectory);
  end;
end;

procedure TEarthBase.SetSurfaceColor(Value: TColor32);
begin
  FSurfaceColor := Value;
  ReDrawLayers;
end;

procedure TEarthBase.SetBackgroundColor(Value: TColor32);
begin
  FBackgroundColor := Value;
  ReDrawLayers;
end;

procedure TEarthBase.SetBackgroundBitmap(NewBitmap: TBitmap32);
begin
  BackgroundImage.Assign(NewBitmap);
  ReDrawLayers;
end;

procedure TEarthBase.WMSize(var Message: TLMSize);
begin
  inherited;
  EarthCanvas.SizeBMP(ClientWidth, ClientHeight, EarthCanvas.ScreenPixelsPerInch);

  //============================================================================
  ResizeBuffer;   //after 2 year I found this ???????????????????????????
  //must resize end Image32 buffer (tTbitmap32) size
  //============================================================================

  // if csDesigning in ComponentState then
  with Projection do
  begin
    XOrigin := ClientWidth / 2;
    YOrigin := ClientHeight / 2;
  end;

  RedrawLayers;
  UpdateScrollBars;  //finally Update ScrollBars
end;

procedure TEarthBase.WMHScroll(var Msg: TWMHScroll);
var
  ScrollInfo: TScrollInfo;
begin

  if Msg.ScrollBar = 0 then
  begin
    ScrollInfo.cbSize := SizeOf(TScrollInfo);
    ScrollInfo.fMask := SIF_ALL;
    GetScrollInfo(Handle, SB_HORZ, ScrollInfo);
    with ScrollInfo do
      case Msg.ScrollCode of
        SB_LINEUP: PanViewXY(-nPage / 20, 0);
        SB_LINEDOWN: PanViewXY(nPage / 20, 0);
        SB_PAGEUP: PanViewXY(-(nPage / 2), 0);
        SB_PAGEDOWN: PanViewXY(nPage / 2, 0);
        SB_THUMBTRACK: PanViewXY(nTrackPos - nPos, 0);
      end;
  end
  else
    inherited;
end;

procedure TEarthBase.WMVScroll(var Msg: TWMVScroll);
var
  ScrollInfo: TScrollInfo;
begin
  if Msg.ScrollBar = 0 then
  begin
    ScrollInfo.cbSize := SizeOf(TScrollInfo);
    ScrollInfo.fMask := SIF_ALL;
    GetScrollInfo(Handle, SB_VERT, ScrollInfo);
    with ScrollInfo do
      case Msg.ScrollCode of
        SB_LINEUP: PanViewXY(0, -(nPage / 20));
        SB_LINEDOWN: PanViewXY(0, nPage / 20);
        SB_PAGEUP: PanViewXY(0, -(nPage / 2));
        SB_PAGEDOWN: PanViewXY(0, nPage / 2);
        SB_THUMBTRACK: PanViewXY(0, nTrackPos - nPos);
      end;
  end
  else
    inherited;
end;

{-------------------------------------------------------------------------------
 TEarthBase.UpdateScrollBars
-------------------------------------------------------------------------------}
procedure TEarthBase.UpdateScrollBars;
var
  ScrollInfo: TScrollInfo;
  eTmp, HalfPage, MinLimit, MaxLimit: extended;
begin
  if bRenderToCanvas or (Parent = nil) then
    Exit;

  ScrollInfo.cbSize := SizeOf(TScrollInfo);
  ScrollInfo.fMask := SIF_RANGE or SIF_POS or SIF_PAGE or SIF_DISABLENOSCROLL;

  with Projection do // Vertical
  begin
    if UseMinMaxLimits then
    begin
      MinLimit := Max((MinLatitude * GU_DEGREE), ExtentsLL.Top);
      MaxLimit := Min((MaxLatitude * GU_DEGREE), ExtentsLL.Bottom);
    end
    else
    begin
      MinLimit := ExtentsLL.Top;
      MaxLimit := ExtentsLL.Bottom;
    end;

    ScrollInfo.nPage := Round(Min(GU_180_DEGREE, FViewRect.Bottom - FViewRect.Top));
    HalfPage := ScrollInfo.nPage / 2;

    if MinLimit < -GU_90_DEGREE then
      ScrollInfo.nMin := Round(-GU_90_DEGREE)
    else
      ScrollInfo.nMin := round(Sign(MinLimit) * (Min(GMaxDouble / 2, abs(MinLimit))));

    if MaxLimit > GU_90_DEGREE then
      ScrollInfo.nMax := Round(GU_90_DEGREE)
    else
      ScrollInfo.nMax := round(Sign(MaxLimit) * (Min(GMaxDouble / 2, abs(MaxLimit))));

    eTmp := -(EarthCanvas.CanvasHeight / 2 - YOrigin) / ScaleFactor;
    eTmp := LimitFloat(eTmp, ScrollInfo.nMin + HalfPage, ScrollInfo.nMax - HalfPage);

    if UseMinMaxLimits then
      with ScrollInfo do
      begin
        if PtrInt(nPage) > nMax - nMin then
        begin
          ScaleFactor := EarthCanvas.CanvasHeight / (nMax - nMin);
          Exit;
        end;

        YOrigin := (EarthCanvas.CanvasHeight / 2 + (eTmp * ScaleFactor));
      end;

    with ScrollInfo do
      nPos := nMax + nMin - Round(HalfPage + eTmp);

    if (FScrollBars = ssBoth) or (FScrollBars = ssVertical) then
      SetScrollInfo(Handle, SB_VERT, ScrollInfo, True);
  end;

  with Projection do // Horizontal
  begin
    MinLimit := ExtentsLL.Left;
    MaxLimit := ExtentsLL.Right;

    if UseMinMaxLimits then
    begin
      MinLimit := Max((MinLongitude * GU_DEGREE), MinLimit);
      MaxLimit := Min((MaxLongitude * GU_DEGREE), MaxLimit);
    end;

    ScrollInfo.nPage := Round(Min(GU_360_DEGREE, FViewRect.Right - FViewRect.Left));
    HalfPage := ScrollInfo.nPage / 2;

    if MinLimit < -GU_180_DEGREE then
      ScrollInfo.nMin := Round(-GU_180_DEGREE)
    else
      ScrollInfo.nMin := round(Sign(MinLimit) * (Min(GMaxDouble / 2, abs(MinLimit))));

    if MaxLimit > GU_180_DEGREE then
      ScrollInfo.nMax := Round(GU_180_DEGREE)
    else
      ScrollInfo.nMax := round(Sign(MaxLimit) * (Min(GMaxDouble / 2, abs(MaxLimit))));

    // limit movement
    eTmp := (EarthCanvas.CanvasWidth / 2 - XOrigin) / ScaleFactor;
    eTmp := LimitFloat(eTmp, ScrollInfo.nMin + HalfPage, ScrollInfo.nMax - HalfPage);

    if UseMinMaxLimits then
      with ScrollInfo do
      begin
        if PtrInt(nPage) > nMax - nMin then
        begin
          ScaleFactor := EarthCanvas.CanvasWidth / (nMax - nMin);
          Exit;
        end;

        XOrigin := EarthCanvas.CanvasWidth / 2 - (eTmp * ScaleFactor);
      end;

    ScrollInfo.nPos := -Round(HalfPage - eTmp);

    if (FScrollBars = ssBoth) or (FScrollBars = ssHorizontal) then
      SetScrollInfo(Handle, SB_HORZ, ScrollInfo, True);
  end;
end;


function TEarthBase.SaveEnvironment: TGXML_Element;
var
  ss: string;
begin
  Result := TGXML_Element.Create;

  Result.AddAttribute('DataDirectory', DataDirectory);
  Result.AddAttribute('SurfaceColor', ColorToString(SurfaceColor));
  Result.AddAttribute('SurfaceTextureName', SurfaceTextureName);
  Result.AddFloatAttribute('MaxTextHeight', MaxTextHeight);
  Result.AddFloatAttribute('MinTextHeight', MinTextHeight);
  Result.AddAttribute('BackgroundColor', ColorToString(BackgroundColor));
  Result.AddIntAttribute('TitlesRenderMethod', Ord(TitlesRenderMethod)); //How to save Type

  Result.AddElement('Projection', Projection.SaveEnvironment);
  Result.AddElement('ScaleShower', ScaleShower.SaveEnvironment);
  Result.AddElement('Grid', Grid.SaveEnvironment);
  Result.AddElement('TransparentOptions', TransparentOptions.SaveEnvironment);
  Result.AddElement('SelectedOjectOptions', SelectedOjectOptions.SaveEnvironment);
  //.........
  ss := '';
  if SelectedLayer <> nil then
    ss := SelectedLayer.Name;

  Result.AddAttribute('SelectedLayer', ss);
end;

procedure TEarthBase.LoadEnvironment(Element: TGXML_Element);
var
  ss: string;
  i: integer;
begin
  if Element <> nil then
    with Element do
    begin
      DataDirectory := AttributeByName('DataDirectory', DataDirectory);
      SurfaceColor := StringToColor(AttributeByName('SurfaceColor', ColorToString(SurfaceColor)));
      SurfaceTextureName := AttributeByName('SurfaceTextureName', SurfaceTextureName);
      MaxTextHeight := FloatAttributeByName('MaxTextHeight', MaxTextHeight);
      MinTextHeight := FloatAttributeByName('MinTextHeight', MinTextHeight);
      BackgroundColor := StringToColor(AttributeByName('BackgroundColor', ColorToString(Color)));
      i := IntAttributeByName('TitlesRenderMethod', Ord(TitlesRenderMethod));  //How to load Type
      TitlesRenderMethod := TTitlesRenderMethod(i);

      Projection.LoadEnvironment(ElementByName('Projection', 0));
      ScaleShower.LoadEnvironment(ElementByName('ScaleShower', 0));
      Grid.LoadEnvironment(ElementByName('Grid', 0));
      TransparentOptions.LoadEnvironment(ElementByName('TransparentOptions', 0));
      SelectedOjectOptions.LoadEnvironment(ElementByName('SelectedOjectOptions', 0));

      //.............
      ss := Element.AttributeByName('SelectedLayer', '');
      SelectedLayer := Layers.ByName(ss);
      LocateToCenter;
    end;
end;

procedure TEarthBase.SaveEnvironmentToFile(const sFilename: TFilename);
var
  Doc: TGXML_Document;
begin
  Doc := TGXML_Document.Create;
  try
    Doc.Document.AddElement('TEarthBase', SaveEnvironment);
    if Assigned(FOnSaveEnvironment) then
      FOnSaveEnvironment(Self, Doc);

    Doc.SaveToFile(sFilename);
  finally
    Doc.Free;
  end;
end;


procedure TEarthBase.SaveEnvironmentToStream(Stream: TStream);
var
  Doc: TGXML_Document;
begin
  if Stream = nil then
    exit;
  Doc := TGXML_Document.Create;
  try
    Doc.Document.AddElement('TEarthBase', SaveEnvironment);
    if Assigned(FOnSaveEnvironment) then
      FOnSaveEnvironment(Self, Doc);

    Doc.SaveToStream(Stream);
  finally
    Doc.Free;
  end;
end;


procedure TEarthBase.LoadEnvironmentFromFile(const sFilename: TFilename);
var
  Doc: TGXML_Document;
begin
  if sFilename = '' then
    exit;

  if FileExistsUTF8(sFilename) = False then
  begin
    ShowMessage('File :' + sFilename + ' Do NOT found. Cancel Load EarthView...');
    exit;
  end;

  Doc := TGXML_Document.Create;
  try
    if Doc.LoadFromFile(sFilename) then
    begin
      LoadEnvironment(Doc.Document.ElementByName('TEarthBase', 0));
      FEnvironmentFile := sFilename;

      if Assigned(FOnLoadEnvironment) then
        FOnLoadEnvironment(Self, Doc);
    end
    else
      FEnvironmentFile := '';
  finally
    Doc.Free;
  end;
end;

procedure TEarthBase.LoadEnvironmentFromStream(Stream: TStream);
var
  Doc: TGXML_Document;
begin
  if Stream = nil then
    exit;

  Doc := TGXML_Document.Create;
  try
    if Doc.LoadFromStream(Stream) then
    begin
      LoadEnvironment(Doc.Document.ElementByName('TEarthBase', 0));
      FEnvironmentFile := '';

      if Assigned(FOnLoadEnvironment) then
        FOnLoadEnvironment(Self, Doc);
    end
    else
      FEnvironmentFile := '';
  finally
    Doc.Free;
  end;
end;

procedure TEarthBase.PanViewXY(iX, iY: double);
begin
  if (iX <> 0) or (iY <> 0) then
    with Projection do
    begin
      XOrigin := XOrigin - (iX * ScaleFactor);
      YOrigin := YOrigin - (iY * ScaleFactor);

      if Assigned(FOnPanned) and not (csLoading in ComponentState) then
        FOnPanned(Self);
    end;

  UpdateScrollBars;
  RedrawLayers;
end;

procedure TEarthBase.SetViewRect(const sRect: TRect);
var
  eSF, iXOrg, iYOrg: extended;
begin
  if (sRect.Right = 0) or (sRect.Bottom = 0) then
  begin
    Projection.ScaleFactor := 0.0;
    Exit;
  end;

  with EarthCanvas, Projection do
  begin
    with sRect do
    begin
      eSF := min(Width / Right, Height / Bottom);
      if eSF > GMAXSCALEFACTOR then
        eSF := GMAXSCALEFACTOR;

      // Calculate the new Origin
      iXOrg := ((XOrigin / ScaleFactor - ((Left + Right) / 2)) * eSF) + Width / 2;
      iYOrg := ((YOrigin / ScaleFactor - ((Top + Bottom) / 2)) * eSF) + Height / 2;
    end;

    if Max(Width, Height) / eSF > GMaxDouble then
      eSF := Max(Width, Height) / GMaxDouble;

    if eSF <> ScaleFactor then
      try
        FViewRect := RectD(0, 0, (Width / eSF), (Height / eSF));

        ScaleFactor := eSF;
        XOrigin := iXOrg;
        YOrigin := iYOrg;

        if Assigned(FOnZoomed) and not (csLoading in ComponentState) then
          FOnZoomed(Self);

      except
      end;
  end;
  UpdateScrollBars;
end;

procedure TEarthBase.DoOnPaint;
begin
  if Assigned(FOnPaint) then
    FOnPaint(Self);
end;

procedure TEarthBase.DoOnRender;
begin
  if Assigned(FOnRender) then
    FOnRender(Self);
end;

procedure TEarthBase.DoLayersChange;
begin
  if Assigned(FOnLayersChange) then
    FOnLayersChange(Self);
end;

procedure TEarthBase.Resize;
begin
  inherited;
end;

procedure TEarthBase.RenderToCanvas(ACanvas: TCanvas; ARect: TRect);
begin
  if ACanvas = nil then
    exit;


  // must fix this 28-5-05
  {
 BitBlt(ACanvas.Handle, ARect.Left, ARect.Top,
        ARect.Right - ARect.Left, ARect.Bottom - ARect.Top,
        EarthCanvas.DC, 0, 0, SRCCOPY);
 }
end;


function TEarthBase.ProgressMessage(MsgType: TGISProgressMessage; const sMsg: string): boolean;
begin
  Result := False;
  if Assigned(FOnProgress) then
    FOnProgress(Self, MsgType, sMsg, Result);
end;

function TEarthBase.GetFullSurfaceTextureName: string;
begin
  Result := UnFixFilePath(FSurfaceTextureName, ExtractFilePath(Application.ExeName));
end;

procedure TEarthBase.SetSurfaceTextureName(sName: TFilename);
begin
  FSurfaceTextureName := sName;

 // FSurfaceTextureName := GetFullSurfaceTextureName;

  FSurfaceTextureName := ResolveFilename(FSurfaceTextureName);

  with BackgroundTextureMap do
  begin
    if FSurfaceTextureName = '' then
      BackgroundTextureMap.TextureStream.SetSize(0, 0);

    LoadFromFile(FSurfaceTextureName);

    if IsTextureOK then
      FSurfaceTextureName := FixFilePath(FSurfaceTextureName, ExtractFilePath(Application.ExeName))
    else
      FSurfaceTextureName := '';
  end;


  RedrawLayers;
end;

function TEarthBase.ResolveFilename(const sFilename: TFilename): TFilename;
  //var
  // bTryFilename: boolean;
  // aFile: string;
begin
  Result := sFilename;
 { if FileExists(Result) then Exit;// Check for an absolute filename

  // check for a file relative to the DataDirectory
  aFile := Trim(DataDirectory)+ExtractFilename(sFilename);
   if FileExists(aFile) then begin Result:=aFile; Exit; end;

   // check for a file relative to the App directory
  aFile := ExtractFilePath(Application.ExeName) +ExtractFilename(sFilename);
  if FileExists(aFile) then begin Result:=aFile; Exit; end;


 if Assigned(OnFileNotFound) then
  begin
    Result := sFilename;
    bTryFilename := False;

    OnFileNotFound(Self, Result, bTryFilename);

    if bTryFilename then
      Result := ResolveFilename(Result)
    else
      Result := '';
  end;

 if Assigned(xOnFileNotFound) then //For IDE
  begin
    Result := sFilename;
    bTryFilename := False;

    xOnFileNotFound(Self, Result, bTryFilename);

    if bTryFilename then
      Result := ResolveFilename(Result)
    else
      Result := '';
  end;
    }

end;
//.......................... Memory Functions .....................................
function TEarthBase.NextMRU: integer;
begin
  Inc(FCacheMRU);
  Result := FCacheMRU;
end;

function TEarthBase.GetCacheMemoryUsed: integer;
begin
  Result := FCacheMemoryUsed;
end;

function TEarthBase.CacheUsedDelta(delta: integer): integer;
var
  iTargetSize: integer;
begin
  Inc(FCacheMemoryUsed, delta);

  if CacheCapacity > 0 then
    if FCacheMemoryUsed > CacheCapacity * 1024 then
    begin

      if Assigned(FOnReduceCacheMemory) then
        FOnReduceCacheMemory(Self);

      // Calculate the target capacity for the cache
      iTargetSize := CacheCapacity * 1024;
      iTargetSize := iTargetSize - MulDiv(iTargetSize, CACHE_RELEASE_PERCENT, 100);

      FCacheMemoryUsed := ReduceCacheMemory(iTargetSize);
    end;
  Result := FCacheMemoryUsed;
end;

function TEarthBase.UpdateTotalMemoryInUse: integer;
var
  idx: integer;
begin
  FCacheMemoryUsed := 0;

  // Scan all objects to re-calculate the Memory used
  for idx := 0 to FObjectSources.Count - 1 do
    Inc(FCacheMemoryUsed, TEarthObjectStore(FObjectSources.Items[idx]).ObjectInstanceSize);

  Result := CacheUsedDelta(0); // to allow memory to be released if Total exceeds limit
end;

function SortByMRU(Item1, Item2: Pointer): integer;
begin
  Result := TEarthObject(Item1).MRU - TEarthObject(Item2).MRU;
end;

function TEarthBase.ReduceCacheMemory(iTargetSize: integer): integer;
var
  AllItems: TList;
  idx: integer;
  Obj: TEarthObject;
  ObjData: TEarthObjectData;
begin
  Result := 0;

  for idx := 0 to FObjectSources.Count - 1 do
    Inc(Result, TEarthObjectStore(FObjectSources.Items[idx]).ObjectInstanceSize);

  AllItems := TList.Create;
  try
    for idx := 0 to FObjectSources.Count - 1 do
      TEarthObjectStore(FObjectSources.Items[idx]).ListActiveObjects(AllItems);

    AllItems.Sort(@SortByMRU);
    for idx := 0 to AllItems.Count - 1 do
    begin
      Obj := TEarthObject(AllItems[idx]);
      Result := Result - Obj.ObjectInstanceSize;

      ObjData := TEarthObjectData.Create;
      ObjData.Assign(Obj);
      Obj.Parent.EarthObjectData[Obj.Index] := ObjData;

      if Result < iTargetSize then
        Exit;
    end;
  finally
    AllItems.Free;
  end;
end;

function TEarthBase.FindCenterPointLL(const a, b: TPointLL): TPointLL;
var
  ap, bp, cp: Tpoint;
begin
  self.LLToMouseXY(a.iLongX, a.iLatY, ap);
  LLToMouseXY(b.iLongX, b.iLatY, bp);
  cp := PointD(((ap.X + bp.X) / 2), ((ap.Y + bp.Y) / 2));
  MouseXYToLL(cp.X, cp.Y, Result);
end;

//============================ Find Functions ===============================================
function TEarthBase.GetLayerFromObject(Obj: TEarthObject): TEarthLayer;
begin
  Result := FindLayerFromObject(self, Obj);
end;

function TEarthBase.GetEarthObjectFromGroupsStore(aGroupsStore: TGroupsStore): TGeoDataObject;
begin
  Result := FindEarthObjectFromGroupsStore(self, aGroupsStore);
end;

function TEarthBase.GetEarthObjectFromPointStore(aPointStore: TPointStore): TGeoDataObject;
begin
  Result := FindEarthObjectFromPointStore(self, aPointStore);
end;

function TEarthBase.GetEarthObjectFromTitle(aTitle: string; Obj: TEarthObject): TEarthObject;
begin
  Result := FindEarthObjectFromTitle(self, aTitle, Obj);
end;

function TEarthBase.GetPresenterFromObject(Obj: TEarthObject): TEarthPresenter;
begin
  Result := FindPresenterFromObject(self, Obj);
end;


//========================= Unit functions ==================================
function TEarthBase.EarthUnitsFrom(const eValue: extended; Units: TEarthUnitTypes): double;
begin
  Result := EarthCanvas.EarthUnitsFrom(eValue, Units);
end;

function TEarthBase.EarthUnitsTo(iValue: double; Units: TEarthUnitTypes): extended;
begin
  if Units = euPixel then
    Result := iValue * Projection.Scalefactor
  else
    Result := GIS_SysUtils.EarthUnitsTo(iValue, Units);
end;

function TEarthBase.HeightUnitsFrom(const eValue: extended; Units: THeightUnitTypes): double;
begin
  Result := EarthCanvas.HeightUnitsFrom(eValue, Units);
end;

function TEarthBase.HeightUnitsTo(iValue: double; Units: THeightUnitTypes): extended;
begin
  Result := GIS_SysUtils.HeightUnitsTo(iValue, Units);
end;

function TEarthBase.PLLGetLong(const ptLL: TPointLL): double;
begin
  Result := GIS_SysUtils.PointLLGetLong(ptLL);
end;

function TEarthBase.PLLGetLat(const ptLL: TPointLL): double;
begin
  Result := GIS_SysUtils.PointLLGetLat(ptLL);
end;

function TEarthBase.PLLGetHeight(const ptLL: TPointLL): double;
begin
  Result := GIS_SysUtils.PointLLGetHeight(ptLL);
end;

function TEarthBase.PLLAsText(const iLong, iLat: double): string;
begin
  Result := GIS_SysUtils.PointLLAsText(iLong, iLat);
end;

function TEarthBase.PLLAsText2(const ptLL: TPointLL): string;
begin
  Result := GIS_SysUtils.PointLLAsText2(ptLL);
end;

function TEarthBase.PLLAsTextU(const iLong, iLat: double; aUnitsformat: TUnitsformat): string;
begin
  Result := GIS_SysUtils.PointLLAsTextU(iLong, iLat, aUnitsformat);
end;

function TEarthBase.PLLAsTextU2(const ptLL: TPointLL; aUnitsformat: TUnitsformat): string;
begin
  Result := GIS_SysUtils.PointLLAsTextU2(ptLL, aUnitsformat);
end;

function TEarthBase.PLLAsTextFm(const iLong, iLat: double; sFmt: string): string;
begin
  Result := GIS_SysUtils.PointLLAsTextFm(iLong, iLat, sFmt);
end;

function TEarthBase.PLLAsTextFm2(const ptLL: TPointLL; sFmt: string): string;
begin
  Result := GIS_SysUtils.PointLLAsTextFm2(ptLL, sFmt);
end;

function TEarthBase.EarthUnitsToText(const aValue: double; const sFmt: string): string;
begin
  Result := GIS_SysUtils.EarthUnitsToStr(aValue, sFmt);
end;

function TEarthBase.TextToEarthUnits(const aText: string; const sFmt: string): double;
begin
  Result := GIS_SysUtils.StrToEarthUnits(aText, sFmt);
end;

//===================================================================================
//===================================================================================
//===================================================================================

initialization
  RegisterClasses([TEarthPen, TEarthBrush, TEarthFont]);
end.
