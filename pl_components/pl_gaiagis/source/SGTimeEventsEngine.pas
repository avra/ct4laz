
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit SGTimeEventsEngine;



interface

uses
  SysUtils, Classes,{STimeEventsCollection,}SGISEngine,GIS_Earth,
  SGScriptEngine,Graphics,ExtCtrls{,SThreadsObjectsTimers,SBaseTimeEventsEngine};


type


TGTimeEventsEngine = class(TBaseTimeEventsEngine)
   private
      FGISEngine:TGISEngine;
      FEarth:TEarth;
   protected
      Procedure OnThreadTimer(Sender: TObject); override;
   public
      constructor Create(aOwner : TComponent); override;
      destructor Destroy; override;

      Property GISEngine:TGISEngine read FGISEngine write FGISEngine;
      Property Earth:TEarth read FEarth write FEarth;
   published
      

   end;


implementation

// ========================== TGTimeEventsEngine========================

constructor TGTimeEventsEngine.Create(aOwner : TComponent);
begin
    inherited Create(aOwner);
    FGISEngine:=nil;
    FEarth:=nil;

end;

destructor TGTimeEventsEngine.Destroy;
begin
    Enabled:=false;
    Clear;
    FGISEngine:=nil;
    FEarth:=nil;
    inherited Destroy;
end;

Procedure TGTimeEventsEngine.OnThreadTimer(Sender: TObject);
 begin
   if (FGISEngine=nil) or (FEarth=nil) then Exit;
   inherited OnThreadTimer(Sender);
 end;

initialization

RegisterClasses([
TGTimeEventsEngine
 ]);

end.
