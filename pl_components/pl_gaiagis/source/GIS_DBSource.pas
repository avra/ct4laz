
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit GIS_DBSource;

{$MODE DELPHI}

interface

uses
  LCLIntf, LCLType, LMessages,
  Classes, SysUtils, DB, GIS_EarthBase, GIS_SysUtils,
  GIS_EarthObjects, GIS_Presenters, GIS_Classes, GIS_XML;

type

  {---------------------------- TEarthDBReader --------------------------------}
  TEarthDBReader = class(TEarthObjectStore)
  private
    FDataSource: TDataSource;
  protected
    procedure SetActive(const Value: Boolean); override;
    procedure MapDataSource; virtual; abstract;
  public
    constructor Create(aLayer:TEarthLayer); override;
    destructor Destroy; override;

    property DataSource : TDataSource read FDataSource write FDataSource;
  end;

  {---------------------------- TEarthDBPointReader ---------------------------}
  TEarthDBPointReader = class(TEarthDBReader)
  protected
    procedure MapDataSource; override;

  public
    function LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject; override;

    function SaveEnvironment : TGXML_Element; override;
    procedure LoadEnvironment( Element : TGXML_Element ); override;
    procedure SaveMetaData; override;
    procedure LoadMetaData; override;
  end;

implementation

constructor TEarthDBReader.Create(aLayer:TEarthLayer);
begin
  inherited;

end;

destructor TEarthDBReader.Destroy;
begin
  inherited;

end;

procedure TEarthDBReader.SetActive(const Value: Boolean);
begin
  inherited;
  
  if ( DataSource <> nil ) and ( DataSource.DataSet <> nil ) then
    DataSource.DataSet.Active := Value;
end;
//.................................................
procedure TEarthDBPointReader.MapDataSource;
  begin
  end;
function TEarthDBPointReader.LoadObject(iIndex : integer; bNewObject : Boolean) : TEarthObject;
  begin
    result:=nil;
  end;
function TEarthDBPointReader.SaveEnvironment : TGXML_Element;
  begin
   result:=nil;
  end;
procedure TEarthDBPointReader.LoadEnvironment( Element : TGXML_Element );
  begin
  end;
procedure TEarthDBPointReader.SaveMetaData;
  begin
  end;
procedure TEarthDBPointReader.LoadMetaData;
  begin
  end;
end.
