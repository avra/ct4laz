
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}


unit sFileForGISPresenter;



interface
 uses
  LCLIntf, LCLType, LMessages, Messages, SysUtils,dialogs, Classes,
  GIS_SysUtils,GIS_Classes,GIS_EarthStreams, FileUtil, lazfileutils;

const
  GaiaCADGISPresenterFileFilter='GaiaCAD Presenters File (*.GPF)|*.GPF';
  GaiaCADGISPresenterFileExt='GPF';

Function Is_GISPresenterFile(const sFilename : TFilename):boolean;

Function Save_GISPresenterToStream(aPres:TEarthPresenter;astream:Tstream):boolean;
Function Save_GISPresenterToFile(aPres:TEarthPresenter;const aFile:String):boolean;
Function Save_GISPresentersListToStream(aList:TList;astream:Tstream):longint;
Function Save_GISPresentersListToFile(aList:TList;const aFile:String):longint;
Function Save_GISPresentersToStream(aPresenterStore:TEarthPresenterStore;astream:Tstream):longint;
Function Save_GISPresentersToFile(aPresenterStore:TEarthPresenterStore;const aFile:String):longint;

Function Load_GISPresentersFromStream(aParent:TEarthPresenterStore;astream:Tstream):longint;
Function Load_GISPresentersFromFile(aParent:TEarthPresenterStore;const aFile:String):longint;

implementation

 type

 TGISPresenterRec=record
        Whatis:string[70];
        FileID:integer;              //Use of intetify the file type
        Version:integer;             //Rec version
        TG_FILEVERSION:integer;
        PresentersNum:longint;
        FutureString1:String[100];
        FutureString2:String[100];
        FutureBool1:boolean;
        FutureBool2:boolean;
        FutureInt1:longint;
        FutureInt2:longint;
     end;

const
     GISPresenteFileCodeId=56964;     //Use to intetify the file type
     GISPresenteFileVersion=1;

Procedure Clear_TGISPresenterRec(var arec:TGISPresenterRec);
 begin
     arec.Whatis:='GaiaCAD GISPresenter file';
     arec.FileID:=GISPresenteFileCodeId;
     arec.Version:=GISPresenteFileVersion;
     arec.TG_FILEVERSION:=TG_FILEVERSION;
     arec.PresentersNum:=0;
     arec.FutureString1:='';
     arec.FutureString2:='';
     arec.FutureBool1:=false;
     arec.FutureBool2:=false;
     arec.FutureInt1:=-9999;
     arec.FutureInt2:=-9999;
 end;
//...............................................

Function Is_GISPresenterFile(const sFilename : TFilename):boolean;
 begin
  result:=(SameText(ExtractFileExt(sFilename),'.'+GaiaCADGISPresenterFileExt));
 end;

Function  Save_GISPresenterToStream(aPres:TEarthPresenter;astream:Tstream):boolean;
 var rec:TGISPresenterRec;
     ObjWS:TEarthStreamWriter;
 begin
  result:=false;

  if astream=nil then exit;
  if aPres=nil then exit;
  Clear_TGISPresenterRec(Rec);
  rec.PresentersNum:=1;
  ObjWS:=TEarthStreamWriter.Create(astream);
  try
    ObjWS.WriteBuffer(rec,sizeof(TGISPresenterRec));
    ObjWS.WriteShortString( aPres.ClassName );
    aPres.WriteProperties(ObjWS);
    result:=true;
  finally
    ObjWS.Free;
  end;
 end;


Function  Save_GISPresenterToFile(aPres:TEarthPresenter;const aFile:String):boolean;
var s:TFileStream;
 begin
  result:=false;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=Save_GISPresenterToStream(aPres,s);
  finally
   s.Free;
  end;
 end;

//..............................................
Function Save_GISPresentersListToStream(aList:TList;astream:Tstream):longint;
 var rec:TGISPresenterRec;
     ObjWS:TEarthStreamWriter;
     per:TEarthPresenter;
     i:longint;
 begin
  result:=0;

  if astream=nil then exit;
  if aList=nil then exit;
  Clear_TGISPresenterRec(Rec);
  rec.PresentersNum:=aList.Count;
  ObjWS:=TEarthStreamWriter.Create(astream);
  try

    ObjWS.WriteBuffer(rec,sizeof(TGISPresenterRec));
    for I := 0 to aList.Count - 1 do
     begin
      per:=nil;
      per:=TEarthPresenter(alist.Items[i]);
      ObjWS.WriteShortString( per.ClassName );
      per.WriteProperties(ObjWS);
      inc(result);
     end;

  finally
    ObjWS.Free;
  end;
 end;

Function Save_GISPresentersListToFile(aList:TList;const aFile:String):longint;
var s:TFileStream;
 begin
  result:=0;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=Save_GISPresentersListToStream(aList,s);
  finally
   s.Free;
  end;
 end;


Function Save_GISPresentersToStream(aPresenterStore:TEarthPresenterStore;astream:Tstream):longint;
var rec:TGISPresenterRec;
     ObjWS:TEarthStreamWriter;
     per:TEarthPresenter;
     i:longint;
 begin
  result:=0;

  if astream=nil then exit;
  if aPresenterStore=nil then exit;
  Clear_TGISPresenterRec(Rec);
  rec.PresentersNum:=aPresenterStore.Count;
  ObjWS:=TEarthStreamWriter.Create(astream);
  try

    ObjWS.WriteBuffer(rec,sizeof(TGISPresenterRec));
    for I := 0 to aPresenterStore.Count - 1 do
     begin
      per:=nil;
      per:=aPresenterStore.Presenters[i];
      ObjWS.WriteShortString( per.ClassName );
      per.WriteProperties(ObjWS);
      inc(result);
     end;

  finally
    ObjWS.Free;
  end;
 end;
Function Save_GISPresentersToFile(aPresenterStore:TEarthPresenterStore;const aFile:String):longint;
var s:TFileStream;
 begin
  result:=0;
  if afile='' then exit;
  try
   s:=TFileStream.Create(afile,fmCreate);
   result:=Save_GISPresentersToStream(aPresenterStore,s);
  finally
   s.Free;
  end;
 end;  

//...............................................

Function Load_GISPresentersFromStream(aParent:TEarthPresenterStore;astream:Tstream):longint;
 var rec:TGISPresenterRec;
     obj:TEarthPresenter;
     cs:TPersistentClass;
     ObjRS:TEarthStreamReader;
     i:longint;
//......................
Procedure FindNewPresenterID;
  var i,j:integer;
   begin
     if obj=nil then exit;
      j:=0;

      for i := 0 to aParent.Count - 1 do
       if aParent[i]<>obj then
        if aParent[i].PresenterID>=j then
          j:=aParent[i].PresenterID;

     obj.PresenterID:=j+1;
   end;

//......................
 begin
  result:=0;  
  if astream=nil then exit;
  if aParent=nil then exit;

  ObjRS:=TEarthStreamReader.Create(astream);

  try
    Cs:=nil;
    ObjRS.ReadBuffer(rec,sizeof(TGISPresenterRec));
    if Rec.FileID=GISPresenteFileCodeId then
     begin
      giFileVersion:=Rec.TG_FILEVERSION;
      for I := 0 to Rec.PresentersNum - 1 do
        begin
         Cs:=GetClass(ObjRS.ReadShortString);      //read class
         if Cs<>nil then
          begin
           obj:=TEarthPresenterClass(Cs).Create(aParent,0);  //Create class
           obj.ReadProperties(ObjRS );
           FindNewPresenterID;
           inc(result);
         end;
      end;
    end;
  finally
   ObjRS.Free;
  end;
 end;


Function Load_GISPresentersFromFile(aParent:TEarthPresenterStore;const aFile:String):longint;
var s:TFileStream;
 begin
  result:=0;
  if afile='' then exit;
  if FileExistsUTF8(afile)  =false then exit;
  try
   s:=TFileStream.Create(afile,fmOpenRead);
   result:=Load_GISPresentersFromStream(aparent,s);
  finally
   s.Free;
  end;
 end;
end.
