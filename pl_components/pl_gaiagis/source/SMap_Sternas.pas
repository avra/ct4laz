
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit SMap_Sternas;



interface
uses 
  LCLIntf, LCLType, LMessages, 
  Classes, Graphics, SysUtils, GIS_EarthBase, GIS_Classes, Dialogs, GIS_SysUtils,
  GIS_EarthObjects, GIS_Resource;

 type

  CheckPointRec = record
    idle: boolean;
    winx: integer;
    winy: integer;
    geo_Hor: longint;
    geo_Ver: longint;
  end;

  CheckPointsRec = array[1..6] of CheckPointRec;

  GeograficalRec = record
    Version: real;
    mapname:shortString;
    Width: smallint;
    Height: smallint;
    Start_Hor: real;     {this is map init hor}
    Start_Ver: real;     {this is map init ver}
    Factor_Hor: real;    {the sec/1 pixel for hor geo}
    Factor_Ver: real;    {the sec/1 pixel for ver geo}
    Rot_angle: real;     {the angle for rotation X axis in degrees}
    sin_angle: real;      {ngle for siglisis of axis X in degrees}
    Sin_AtpixelsX: real;  {pixels of X where sin_angle exist}
    Sin_AtpixelsY: real;  {pixels of Y where sin_angle exist}
    Cam_angle: real;     {the angle for y axis in degrees}
    Cam_atpixels: real;  {pixels of x where sin_angle exist}
  end;

  InfoRec = record
    mapstr: string[8];
    mapid: smallint;
    ouner: string[20];
    releasedata: string[10];
    scale: smallint;
    minmap: smallint;
    maxmap: smallint;
    color: byte;
    Dpi: smallint;
    callString: string[100];
    FamilyType: byte;
  end;
//.......................................................



function geotoreal(ageo:real):real;

implementation
{--------------------geotoreal-------------------------------------------}
function geotoreal(ageo:real):real;
   var inchec:real;
       sss,s1,s2,s3,s4:string;
       fs1,fs2,fs3:real;
       err,minusnum:integer;
   begin
      inchec:=ageo;
      if inchec<0 then begin minusnum:=-1; inchec:=inchec*(-1); end
      else begin minusnum:=1;  end;
      inchec:=500000000+inchec;
      str(inchec:20:30,sss);
      s1:=copy(sss,3,3);
      s2:=copy(sss,6,2);
      s3:=copy(sss,8,2);
      s4:=copy(sss,10,50);
      val(s4,inchec,err);
      {..} 
      fs1:=strtoint(s1);
      fs2:=strtoint(s2);
      fs3:=strtoint(s3);
      {..}
      if fs3>=60 then begin
       fs3:=fs3*0.6;
       end;
      if fs2>=60 then begin
       fs2:=fs2*0.6 
       end; 
       {..}
      geotoreal:= minusnum*((fs1*3600)+inchec+(fs2*60)+fs3);
   end;

end.
