
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit SMap_GaiaCAD;




interface

uses
  LCLIntf, LCLType, LMessages,
  Classes, Graphics, SysUtils, GIS_EarthBase, GIS_Classes, Dialogs, GIS_SysUtils,
  GIS_EarthObjects, GIS_Resource;

 const
  GaiaCADMapIDFile=22334491;
  GaiaCADMapFileVersion=1000;
 type

  xTestPointRec = record
    idle: boolean;
    winx: integer;
    winy: integer;
    geo_Hor: longint;
    geo_Ver: longint;
  end;

  xTestPointsRec = array[1..6] of xTestPointRec;

  xGeoRec = record
    color: byte;
    Dpi: smallint;
    Width: integer;
    Height: integer;
    scale: integer;
    Start_Hor: real;     {this is map init hor in Second}
    Start_Ver: real;     {this is map init ver in Second}
    Factor_Hor: real;    {the Second/1 pixel for hor geo}
    Factor_Ver: real;    {the Second/1 pixel for ver geo}
    Rot_angle: real;     {the angle for rotation X axis in degrees}
    sin_angle: real;      {ngle for siglisis of axis X in degrees}
    Sin_AtpixelsX: real;  {pixels of X where sin_angle exist}
    Sin_AtpixelsY: real;  {pixels of Y where sin_angle exist}
    Cam_angle: real;     {the angle for y axis in degrees}
    Cam_atpixels: real;  {pixels of x where sin_angle exist}
  end;

  xGenInfoRec = record
    WhatIs:string[50];
    FileID:longint;
    FileVersion:longint;
    Ouner: string[50];
    ReleaseDate: string[20];
    Future0:longint;
    Future1:longint;
    Future2:longint;
    Future3:longint;
    Future4:longint;
    Future5:longint;
  end;

implementation


end.
