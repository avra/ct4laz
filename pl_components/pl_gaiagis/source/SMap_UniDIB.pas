
{**********************************************************************
 Package pl_GaiaGIS
 This unit is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit SMap_UniDIB;

{$MODE DELPHI}

{TUniDIB version 1.21}


interface

uses LCLIntf, LCLType, LMessages, Classes, Graphics, Controls, SysUtils;

const
  C_xMaxAllowedBPP = 6;
  C_xAllowedBPP: array [1..C_xMaxAllowedBPP] of byte = (1,4,8,16,24,32);

const
  SBU_NONE = 0; {see SetPixel VARIABLE}
  SBU_RED = 1;
  SBU_GREEN = 2;
  SBU_BLUE = 3;

type
  PxLogPalette256 = ^TxLogPalette256;

  TxLogPalette256 = record
    palVersion: word;
    palNumEntries: word;
    palEntry: array[0..255] of TPaletteEntry;
  end;

type
  TxBitmapInfo256 = record
    bmiHeader: TBITMAPINFOHEADER;
    bmiColors: array[0..255] of TRGBQUAD;
  end;

type
  TxSetPixelProc = procedure(X, Y, Value: integer) of object;
  TxGetPixelFunc = function(X, Y: integer): integer of object;
  TxSetSeqPixelProc = procedure(Value: integer) of object;
  TxGetSeqPixelFunc = function: integer of object;

type
  TUniDIB = class
  protected
    FBMInfo: TxBitmapInfo256;           {Informations about this bitmap}
    FHandle: HBITMAP;                  {Handle of this bitmap}
    FDC: HDC;                          {DC (compatible with the current screen)}
    FBits: Pointer;                    {Pointer to array of bits}
    FPalHandle: HPALETTE;              {Handle of palette}
    FActPointer: Pointer;              {Pointer to next pixel (for sequential access)}
    FDWordWidth: cardinal;             {Width of row in bytes aligned to Double Word}
    XActX: integer;                    {Actual X coordinate (for sequential access)}
    XUsage: UINT;                      {DIB_PAL_COLORS or DIB_RGB_COLORS}
    XClrCount: integer;                {Number of colors in 1,4 and 8 bits modes}
    XSelPalette: HPALETTE;
    procedure SetPixel1(X, Y, Value: integer);
    procedure SetPixel4(X, Y, Value: integer);
    procedure SetPixel8(X, Y, Value: integer);
    procedure SetPixel16(X, Y, Value: integer);
    procedure SetPixel16R(X, Y, Value: integer);
    procedure SetPixel16G(X, Y, Value: integer);
    procedure SetPixel16B(X, Y, Value: integer);
    procedure SetPixel24(X, Y, Value: integer);
    procedure SetPixel32(X, Y, Value: integer);
    function GetPixel1(X, Y: integer): integer;
    function GetPixel4(X, Y: integer): integer;
    function GetPixel8(X, Y: integer): integer;
    function GetPixel16(X, Y: integer): integer;
    function GetPixel16R(X, Y: integer): integer;
    function GetPixel16G(X, Y: integer): integer;
    function GetPixel16B(X, Y: integer): integer;   
    function GetPixel24(X, Y: integer): integer;
    function GetPixel32(X, Y: integer): integer;
    procedure SetSeqPixel1(Value: integer);
    procedure SetSeqPixel4(Value: integer);
    procedure SetSeqPixel8(Value: integer);
    procedure SetSeqPixel16(Value: integer);
    procedure SetSeqPixel16R(Value: integer);
    procedure SetSeqPixel16G(Value: integer);
    procedure SetSeqPixel16B(Value: integer);
    procedure SetSeqPixel24(Value: integer);
    procedure SetSeqPixel32(Value: integer);
    function GetSeqPixel1: integer;
    function GetSeqPixel4: integer;
    function GetSeqPixel8: integer;
    function GetSeqPixel16: integer;
    function GetSeqPixel16R: integer;
    function GetSeqPixel16G: integer;
    function GetSeqPixel16B: integer;
    function GetSeqPixel24: integer;
    function GetSeqPixel32: integer;
  public
    SetPixel: TxSetPixelProc;
{In the procedure SetPixel depends the "Value" on ABPP specified
 in constructor.
 ABPP - 1,4,8 - simply set the Value to color from palette
        16,24 - 3 bytes are used, one for red, second for green and
                third for blue element of the desired color
        32    - strange number of bits - in Win32 Developer's References
                is written that 4th byte is NOT used (?)
                I don't know much about it yet.
                (It seems to be same as 24 bit depth, but with 4 bytes)

 ****!!!!!!*****
 Next thing is that coordinates [0,0] are NOT at the upper left corner,
 but at the LOWER left one.
 ****!!!!!!*****}
    GetPixel: TxGetPixelFunc;
    {procedures for sequential access to pixels}
    SetSeqPixel: TxSetSeqPixelProc;
    GetSeqPixel: TxGetSeqPixelFunc;
    procedure Seek(X, Y: integer);
         {Sets actual pointer to the pixel [X,Y]
          This pointer is pointing to the next pixel to operate with.}
    constructor Create(AWidth, AHeight: longint; ABPP: byte; SByteUse: byte);
      {ABPP - bits per pixel (up to 32)
              If this is not some of allowed values (see C_xAllowedBPP),
              it is rounded to the nearest upper one.
       SByte- use of 16th bit in 2 bytes
              used only when ABPP is 16
              SBU_NONE  - unused
              SBU_RED   - red element of pixel have
                          6 (SIX) bits; it is not visible, but it
                          is more accurate for image conversions
              SBU_GREEN - same as above, but for green component
              SBU_BLUE  - and for blue}
    destructor Destroy; override;
    procedure DIBtoScreen(DC: hDC);
    procedure DIBtoCanvasXY(X, Y: integer; DC: hDC);
      {After this procedure you should call Invalidate or
       something alike.}
    procedure SetPalette(Pal: TxLogPalette256);
    procedure Clear;
    procedure DrawHorizLine(X1, X2, Y, Col: integer);
    procedure DrawVertLine(X, Y1, Y2, Col: integer);
    procedure DrawLine(X1, Y1, X2, Y2: integer; Col: integer);
    procedure FillPolygon(Poly: array of TPoint; FillCol: integer);
    procedure CaptureScreen;
    {Captures actual screen content to this DIB}
    property ActPointer: Pointer read FActPointer write FActPointer;    
    property Bits: Pointer read FBits;
    property DC: HDC read FDC;
    property DWordWidth: cardinal read FDWordWidth;
    property Handle: HBITMAP read FHandle;
    property Height: longint read FBMInfo.bmiHeader.biHeight;
    property Width: longint read FBMInfo.bmiHeader.biWidth;
  end;

implementation

procedure TUniDIB.SetPixel4(X, Y, Value: integer);
var 
  A: byte;
  //Z: integer;
  Z: PtrUInt;
begin
  Z := PtrUInt(FBits) + y * FDWordWidth + X shr 1;
  A := pByte(Z)^;
  if X and 1 = 0 then
    pByte(Z)^ := (A and $0F) or (Value shl 4)
  else
    pByte(Z)^ := (A and $F0) or Value;
end;

function TUniDIB.GetPixel4(X, Y: integer): integer;
begin
  Result := pByte(integer(FBits) + y * FDWordWidth + X shr 1)^;
  if X and 1 > 0 then
    Result := Result and $F
  else
    Result := Result shr 4 and $F;
end;

procedure TUniDIB.SetPixel8(X, Y, Value: integer);
begin
  pByte(integer(FBits) + y * FDWordWidth + x)^ := Value;
end;

function TUniDIB.GetPixel8(X, Y: integer): integer;
begin
  Result := pByte(integer(FBits) + y * FDWordWidth + x)^;
end;

procedure TUniDIB.SetPixel16(X, Y, Value: integer);
begin
  pWord(integer(FBits) + y * FDWordWidth + x shl 1)^ :=
    word((Value and $1F0000 shr 6) or (Value and $001F00 shr 3) or (Value and $00001F));
end;

function TUniDIB.GetPixel16(X, Y: integer): integer;
var 
  Z: word;
begin
  Z := pWord(integer(FBits) + y * FDWordWidth + x shl 1)^;
  Result := (Z and $7C00 shl 6) or (Z and $3E0 shl 3) or (Z and $001F);
end;

procedure TUniDIB.SetPixel16R(X, Y, Value: integer);
begin
  pWord(integer(FBits) + y * FDWordWidth + x shl 1)^ :=
    word((Value and $3E0000 shr 7) or (Value and $001F00 shr 3) or
    (Value and $00001F) or (Value and $010000 shr 1));
end;

function TUniDIB.GetPixel16R(X, Y: integer): integer;
var 
  Z: word;
begin
  Z := pWord(integer(FBits) + y * FDWordWidth + x shl 1)^;
  Result := (Z and $7C00 shl 7) or (Z and $3E0 shl 3) or (Z and $001F)
    or (Z and $8000 shl 1);
end;

procedure TUniDIB.SetPixel16G(X, Y, Value: integer);
begin
  pWord(integer(FBits) + y * FDWordWidth + x shl 1)^ :=
    word((Value and $1F0000 shr 6) or (Value and $003E00 shr 4) or
    (Value and $00001F) or (Value and $000100 shl 7));
end;

function TUniDIB.GetPixel16G(X, Y: integer): integer;
var 
  Z: word;
begin
  Z := pWord(integer(FBits) + y * FDWordWidth + x shl 1)^;
  Result := (Z and $7C00 shl 6) or (Z and $3E0 shl 4) or (Z and $001F)
    or (Z and $8000 shr 7);
end;

procedure TUniDIB.SetPixel16B(X, Y, Value: integer);
var 
  Z: word;
begin
  Z := word((Value and $1F0000 shr 6) or (Value and $001F00 shr 3) or
    (Value and $00003E shr 1) or (Value and $000001 shl 15));
  pWord(integer(FBits) + y * FDWordWidth + x shl 1)^ := Z;
end;

function TUniDIB.GetPixel16B(X, Y: integer): integer;
var 
  Z: word;
begin
  Z := pWord(integer(FBits) + y * FDWordWidth + x shl 1)^;
  Result := (Z and $7C00 shl 6) or (Z and $3E0 shl 3) or (Z and $001E shl 1)
    or (Z and $8000 shr 1);
end;

procedure TUniDIB.SetPixel24(X, Y, Value: integer);
var 
  Z: PtrUInt;
begin
  Z := PtrUInt(FBits) + y * FDWordWidth + x + x + x;
  pByte(Z)^ := Value and $FF;
  pByte(integer(Z) + 1)^ := Value shr 8 and $FF;
  pByte(integer(Z) + 2)^ := Value shr 16 and $FF;
{It can't be something like :
    pInteger (Z)^:=pInteger(Z)^ AND $FF000000 OR Value;
 because there would be an EAccessViolation exception,
 if   X=Width-1  and  Y=Height-1  (only in some cases) !}
end;

function TUniDIB.GetPixel24(X, Y: integer): integer;
var 
  Z: integer;
begin
  Z := integer(FBits) + y * FDWordWidth + x + x + x;
  Result := pByte(Z)^ or (pByte(integer(Z) + 1)^ shl 8) or (pByte(integer(Z) + 2)^ shl 16);
end;

procedure TUniDIB.SetPixel32(X, Y, Value: integer);
begin
  pInteger(integer(FBits) + y * FDWordWidth + x shl 2)^ := Value;
end;

function TUniDIB.GetPixel32(X, Y: integer): integer;
begin
  Result := pInteger(integer(FBits) + y * FDWordWidth + x shl 2)^;
end;

procedure TUniDIB.SetSeqPixel4(Value: integer);
var 
  A: byte;
  Z: PtrUInt;
begin
  Z := PtrUInt(FActPointer) + XActX shr 1;
  A := pByte(Z)^;
  if XActX and 1 = 0 then
    pByte(Z)^ := (A and $0F) or (Value shl 4)
  else
    pByte(Z)^ := (A and $F0) or Value;
  Inc(XActX);
end;

function TUniDIB.GetSeqPixel4: integer;
begin
  Result := pByte(integer(FActPointer) + XActX shr 1)^;
  if XActX and 1 = 1 then
    Result := Result and $F
  else
    Result := Result shr 4 and $F;
  Inc(XActX);
end;

procedure TUniDIB.SetSeqPixel8(Value: integer);
begin
  pByte(FActPointer)^ := Value;
  FActPointer := Pointer(integer(FActPointer) + 1);
end;

function TUniDIB.GetSeqPixel8: integer;
begin
  Result := pByte(FActPointer)^;
  FActPointer := Pointer(integer(FActPointer) + 1);
end;

procedure TUniDIB.SetSeqPixel16(Value: integer);
begin
  pWord(FActPointer)^ := word((Value and $1F0000 shr 6) or (Value and $001F00 shr 3)
    or (Value and $00001F));
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

function TUniDIB.GetSeqPixel16: integer;
var 
  Z: word;
begin
  Z := pWord(FActPointer)^;
  Result := (Z and $7C00 shl 6) or (Z and $3E0 shl 3) or (Z and $001F);
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

procedure TUniDIB.SetSeqPixel16R(Value: integer);
begin
  pWord(FActPointer)^ := word((Value and $3E0000 shr 7) or (Value and $001F00 shr 3)
    or (Value and $00001F) or (Value and $010000 shr 1));
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

function TUniDIB.GetSeqPixel16R: integer;
var 
  Z: word;
begin
  Z := pWord(FActPointer)^;
  Result := (Z and $7C00 shl 7) or (Z and $3E0 shl 3) or (Z and $001F)
    or (Z and $8000 shl 1);
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

procedure TUniDIB.SetSeqPixel16G(Value: integer);
begin
  pWord(FActPointer)^ := word((Value and $1F0000 shr 6) or (Value and $003E00 shr 4)
    or (Value and $00001F) or (Value and $000100 shl 7));
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

function TUniDIB.GetSeqPixel16G: integer;
var 
  Z: word;
begin
  Z := pWord(FActPointer)^;
  Result := (Z and $7C00 shl 6) or (Z and $3E0 shl 4) or (Z and $001F)
    or (Z and $8000 shr 7);
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

procedure TUniDIB.SetSeqPixel16B(Value: integer);
begin
  pWord(FActPointer)^ := word((Value and $1F0000 shr 6) or (Value and $001F00 shr 3)
    or (Value and $00003E shr 1) or (Value and $000001 shl 15));
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

function TUniDIB.GetSeqPixel16B: integer;
var 
  Z: word;
begin
  Z := pWord(FActPointer)^;
  Result := (Z and $7C00 shl 6) or (Z and $3E0 shl 3) or (Z and $001E shl 1)
    or (Z and $8000 shr 1);
  FActPointer := Pointer(integer(FActPointer) + 2);
end;

procedure TUniDIB.SetSeqPixel24(Value: integer);
begin
  pByte(FActPointer)^ := Value and $FF;
  pByte(integer(FActPointer) + 1)^ := Value shr 8 and $FF;
  pByte(integer(FActPointer) + 2)^ := Value shr 16 and $FF;
  FActPointer := Pointer(integer(FActPointer) + 3);
end;

function TUniDIB.GetSeqPixel24: integer;
begin
  Result := pByte(FActPointer)^ or (pByte(integer(FActPointer) + 1)^ shl 8) or
    (pByte(integer(FActPointer) + 2)^ shl 16);
  FActPointer := Pointer(integer(FActPointer) + 3);
end;

procedure TUniDIB.SetSeqPixel32(Value: integer);
begin
  pInteger(FActPointer)^ := Value;
  FActPointer := Pointer(integer(FActPointer) + 4);
end;

function TUniDIB.GetSeqPixel32: integer;
begin
  Result := pInteger(FActPointer)^;
  FActPointer := Pointer(integer(FActPointer) + 4);
end;

procedure TUniDIB.Seek(X, Y: integer);
begin
  XActX := X;
  FActPointer := Pointer(integer(FBits) + Y * FDWordWidth + X *
    (FBMInfo.bmiHeader.biBitCount shr 3));
end;

constructor TUniDIB.Create(AWidth, AHeight: longint; ABPP: byte; SByteUse: byte);
var 
  A: integer;
begin
  inherited Create;
  FDC := CreateCompatibleDC(0);
  with FBMInfo.bmiHeader do
  begin
    biSize := SizeOf(TBitmapInfoHeader);
    biPlanes := 1;
    A := 1;
    while (A < C_xMaxAllowedBPP) and (ABPP > C_xAllowedBPP[A]) do
      Inc(A);
    biBitCount := C_xAllowedBPP[A];
    biCompression := BI_RGB;
    if AWidth <= 0 then
      biWidth := 1
    else
      biWidth := AWidth;
    if AHeight = 0 then
      biHeight := 1
    else
      biHeight := AHeight;
    biSizeImage := 0;
    biXPelsPerMeter := 0;
    biYPelsPerMeter := 0;
    biClrUsed := 0;
    biClrImportant := 0;
  end;

  if ABPP <= 8 then
  begin
    XClrCount := 1 shl FBMInfo.bmiHeader.biBitCount;
    {=> XClrCount:=2^FBMInfo.bmiHeader.biBitCount;}
    XUsage := DIB_PAL_COLORS;
  end
  else
    XUsage := DIB_RGB_COLORS;

  FDWordWidth := ((AWidth * FBMInfo.bmiHeader.biBitCount + 31) shr 5) shl 2;
  FHandle := CreateDIBSection(FDC, pBitmapInfo(@FBMInfo)^, XUsage, FBits, 0,0);
  case FBMInfo.bmiHeader.biBitCount of
    1:
    begin
      SetPixel := SetPixel1;
      GetPixel := GetPixel1;
      SetSeqPixel := SetSeqPixel1;
      GetSeqPixel := GetSeqPixel1;
    end;
    4:
    begin
      SetPixel := SetPixel4;
      GetPixel := GetPixel4;
      SetSeqPixel := SetSeqPixel4;
      GetSeqPixel := GetSeqPixel4;
    end;
    8:
    begin
      SetPixel := SetPixel8;
      GetPixel := GetPixel8;
      SetSeqPixel := SetSeqPixel8;
      GetSeqPixel := GetSeqPixel8;
    end;
    16:
    begin
      case SByteUse of
        SBU_NONE:
        begin
          SetPixel := SetPixel16;
          GetPixel := GetPixel16;
          SetSeqPixel := SetSeqPixel16;
          GetSeqPixel := GetSeqPixel16;
        end;
        SBU_RED:
        begin
          SetPixel := SetPixel16R;
          GetPixel := GetPixel16R;
          SetSeqPixel := SetSeqPixel16R;
          GetSeqPixel := GetSeqPixel16R;
        end;
        SBU_GREEN:
        begin
          SetPixel := SetPixel16G;
          GetPixel := GetPixel16G;
          SetSeqPixel := SetSeqPixel16G;
          GetSeqPixel := GetSeqPixel16G;
        end;
        SBU_BLUE:
        begin
          SetPixel := SetPixel16B;
          GetPixel := GetPixel16B;
          SetSeqPixel := SetSeqPixel16B;
          GetSeqPixel := GetSeqPixel16B;
        end;
      end;
    end;
    24:
    begin
      SetPixel := SetPixel24;
      GetPixel := GetPixel24;
      SetSeqPixel := SetSeqPixel24;
      GetSeqPixel := GetSeqPixel24;
    end;
    32:
    begin
      SetPixel := SetPixel32;
      GetPixel := GetPixel32;
      SetSeqPixel := SetSeqPixel32;
      GetSeqPixel := GetSeqPixel32;
    end;
  end;
  SelectObject(FDC, FHandle);
end;

destructor TUniDIB.Destroy;
begin
  DeleteDC(FDC);
  DeleteObject(FHandle);  
  inherited Destroy;
end;

procedure TUniDIB.DIBtoCanvasXY(X, Y: integer; DC: hDC);
var 
  Pal: HPalette;
begin
  if XUsage = DIB_PAL_COLORS then
  begin
    Pal := SelectPalette(DC, FPalHandle, False);
    if Pal <> 0 then
      DeleteObject(Pal);
    RealizePalette(DC);
  end;
  BitBlt(DC, x, y, FBMInfo.bmiHeader.biWidth, Abs(FBMInfo.bmiHeader.biHeight), FDC,
    0,0,SRCCOPY);
end;

procedure TUniDIB.DIBtoScreen(DC: hDC);
var 
  Pal: HPalette;
begin
  if XUsage = DIB_PAL_COLORS then
  begin
    Pal := SelectPalette(DC, FPalHandle, False);
    if Pal <> 0 then
      DeleteObject(Pal);
    RealizePalette(DC);
  end;
  BitBlt(DC, 0,0,FBMInfo.bmiHeader.biWidth, Abs(FBMInfo.bmiHeader.biHeight),
    FDC, 0,0,SRCCOPY);
end;

procedure TUniDIB.Clear;
var
  pDWord: pLongInt;
  i: integer;
begin
  pDWord := FBits;
  for i := 1 to FDWordWidth * Abs(FBMInfo.bmiHeader.biHeight) shr 2 do
  begin
    pLongInt(pDWord)^ := $00000000;
    inc(pDWord); {!! pDWord:=pDWord+ 4 }
    {===}
  end;
end;

procedure TUniDIB.DrawHorizLine(X1, X2, Y: integer; Col: integer);
var 
  X, T: integer;
  P: Pointer;
begin
  if X2 > X1 then
  begin
    X := X1;
    X1 := X2;
    X2 := X;
  end;
  T := XActX;
  P := FActPointer;
  Seek(X1, Y);
  for X := X1 to X2 do
    SetSeqPixel(Col);
  XActX := T;
  FActPointer := P;
end;

procedure TUniDIB.DrawVertLine(X, Y1, Y2, Col: integer);
var 
  Y: integer;
begin
  if Y1 > Y2 then
  begin
    Y := Y1;
    Y1 := Y2;
    Y2 := Y;
  end;
  for Y := Y1 to Y2 do
    SetPixel(X, Y, Col);
end;

procedure TUniDIB.DrawLine(x1, y1, x2, y2: integer; Col: integer);
var
  lp1: integer;
  x, y: integer;
  dy, dx, step, delta: integer;
begin
  dx := x2 - x1;
  dy := y2 - y1;
  { case nought }
  if (dy = 0) and (dx = 0) then
    SetPixel(x1, y1, Col)
    { case one }
  else if dy = 0 then
  begin
    DrawHorizLine(x1, x2, y1, Col);
    exit;
  end
  { case two }
  else if dx = 0 then
  begin
    DrawVertLine(x1, y1, y2, Col);
    exit;
  end
  { case three }
  else if (abs(dx) > abs(dy)) then
  begin
    if dy > 0 then
      step := 1
    else
    begin
      step := -1;
      dy := -dy;
    end;
    delta := dy shr 1;
    if dx >= 0 then
    begin
      y := y1;
      for lp1 := x1 to x2 do
      begin
        SetPixel(lp1, y, Col);
        delta := delta + dy;
        if delta > dx then
        begin
          y := y + step;
          delta := delta - dx;
        end;
      end;
    end
    else
    begin { dx<0 }
      y := y2;
      dx := -dx;
      dy := -dy;
      for lp1 := x2 to x1 do
      begin
        SetPixel(lp1, y, Col); 
        delta := delta - dy;
        if delta > dx then
        begin
          y := y - step;
          delta := delta - dx;
        end;
      end;
    end;
  end
  else
  begin  { dy>dx }
    if dx > 0 then
      step := 1
    else
    begin
      step := -1;
      dx := -dx;
    end;
    delta := dx shr 1;
    if dy >= 0 then
    begin
      x := x1;
      for lp1 := y1 to y2 do
      begin
        SetPixel(x, lp1, Col);
        delta := delta + dx;
        if delta > dy then
        begin
          x := x + step;
          delta := delta - dy;
        end;
      end;
    end
    else
    begin { dy<0 }
      x := x2;
      dy := -dy;
      dx := -dx;
      for lp1 := y2 to y1 do
      begin
        SetPixel(x, lp1, Col);
        delta := delta - dx;
        if delta > dy then
        begin
          x := x - step;
          delta := delta - dy;
        end;
      end;
    end;
  end;
end;

procedure TUniDIB.SetPalette(Pal: TxLogPalette256);
var 
  A: byte;
  Colors: array [0..255] of TRGBQuad;
begin
  if XUsage <> DIB_PAL_COLORS then
    Exit;
  for A := 0 to XClrCount - 1 do
  begin
    Colors[A].rgbRed := Pal.palEntry[A].peRed;
    Colors[A].rgbGreen := Pal.palEntry[A].peGreen;
    Colors[A].rgbBlue := Pal.palEntry[A].peBlue;
    Colors[A].rgbReserved := Pal.palEntry[A].peFlags;
  end;
  Pal.palVersion := $300;
  Pal.palNumEntries := XClrCount;
  SelectPalette(FDC, XSelPalette, False);
  DeleteObject(FPalHandle);
  FPalHandle := CreatePalette(PLogPalette(@Pal)^);
//  SetDIBColorTable(FDC, 0,XClrCount, Colors);
  XSelPalette := SelectPalette(FDC, FPalHandle, False);
end;

procedure TUniDIB.FillPolygon(poly: array of TPoint; fillcol: integer);
var
  loop1: integer;
  yval, ymax, ymin: integer;
  yval0, yval1, yval2, yval3: integer;
  ydifl, ydifr: integer;
  xval0, xval1, xval2, xval3: integer;
  xleft, xright: integer;
  mu: integer;
  minvertex: integer;
  vert0, vert1, vert2, vert3: integer;
  n: integer; {number of points}
begin
  ymax := -999999; 
  ymin := 999999;
  { get top & bottom scan lines to work with }
  n := High(poly);
  minvertex := 0;
  for loop1 := 0 to n - 1 do
  begin
    yval := poly[loop1].y;
    if yval > ymax then ymax := yval;
    if yval < ymin then 
    begin 
      ymin := yval; 
      minvertex := loop1; 
    end;
  end;
  vert0 := minvertex;      
  vert1 := (minvertex + 1) mod n;
  vert2 := minvertex;      
  vert3 := (minvertex - 1) mod n;
  yval0 := poly[vert0].y; 
  yval1 := poly[vert1].y;
  yval2 := poly[vert2].y; 
  yval3 := poly[vert3].y;
  ydifl := yval1 - yval0;    
  ydifr := yval3 - yval2;
  xval0 := poly[vert0].x; 
  xval1 := poly[vert1].x;
  xval2 := poly[vert2].x; 
  xval3 := poly[vert3].x;

  for loop1 := ymin to ymax do
  begin
    {intersection on left hand side }
    mu := (loop1 - yval0);
    if mu > ydifl then
    begin
      vert0 := vert1; 
      vert1 := (vert1 + 1) mod n;
      yval0 := poly[vert0].y; 
      yval1 := poly[vert1].y;
      xval0 := poly[vert0].x; 
      xval1 := poly[vert1].x;
      ydifl := yval1 - yval0;
      mu := (loop1 - yval0)
    end;
    if ydifl <> 0 then
      xleft := xval0 - (mu * integer(xval0 - xval1) div ydifl)
    else
      xleft := xval0;

    {intersection on right hand side }
    if ydifr <> 0 then
      mu := (loop1 - yval2)
    else
      mu := ydifr;
    if mu > ydifr then
    begin
      vert2 := vert3; 
      vert3 := (vert3 - 1) mod n;
      yval2 := poly[vert2].y; 
      yval3 := poly[vert3].y;
      xval2 := poly[vert2].x; 
      xval3 := poly[vert3].x;
      ydifr := yval3 - yval2;
      if ydifr <> 0 then
        mu := (loop1 - yval2)
      else
        mu := ydifr;
    end;
    if ydifr <> 0 then
      xright := xval2 + (mu * integer(xval3 - xval2) div ydifr)
    else
      xright := xval2;
    DrawHorizLine(xleft, xright, loop1, fillcol);
  end;
end;

procedure TUniDIB.CaptureScreen;
var 
  DC: HDC;
  A, B: integer;
  {    LP:TxLogPalette256;}
begin
  DC := GetDC(0);
  A := FBMInfo.bmiHeader.biWidth;
  if GetDeviceCaps(DC, HORZRES) < A then
    A := GetDeviceCaps(DC, HORZRES);
  B := Abs(FBMInfo.bmiHeader.biHeight);
  if GetDeviceCaps(DC, VERTRES) < B then
    B := GetDeviceCaps(DC, VERTRES);
  BitBlt(FDC, 0,0,A, B, DC, 0,0,SRCCOPY);
{  If GetDeviceCaps (DC,RASTERCAPS) AND RC_PALETTE>0 then
  begin
    A:=GetDeviceCaps (DC,NUMCOLORS);
    GetPaletteEntries (,0,A,LP.PalEntry);
    SetPalette (LP);
  end;}
  ReleaseDC(0,DC);
end;



{$IFDEF CPU386}
procedure TUniDIB.SetPixel1(X, Y, Value: integer); assembler;
asm
         PUSH    EBX
         PUSH    EAX
         MOV     EBX,x           //EBX:=X
         MOV     EAX,[EAX].FDWordWidth  //EAX:=FDWordWidth*8*Y
         SHL     EAX,1
         SHL     EAX,1
         SHL     EAX,1
         MUL     Y
{8 pixels are stored byte; first pixel is the most
 important bit in byte}
         XOR     EBX,7           //=>EBX:=(EBX div 8) + (7-EBX mod 8)
         ADD     EBX,EAX         //EBX:=EBX+EAX
         POP     EAX
         MOV     EAX,[EAX].FBits
         CMP     Value,0
         JZ      @@2
         @@1:
         BTS     [EAX],EBX       //Set this bit
         JMP     @@3
         @@2:
         BTR     [EAX],EBX       //Clear this bit
         @@3:
         POP     EBX
end;

function TUniDIB.GetPixel1(X, Y: integer): integer; assembler;
asm
         PUSH    EBX
         PUSH    EAX
         MOV     EBX,x           //EBX:=X
         MOV     EAX,[EAX].FDWordWidth  //EAX:=FDWordWidth*8*Y
         SHL     EAX,1
         SHL     EAX,1
         SHL     EAX,1
         MUL     y
         XOR     EBX,7           //EBX:=(EBX div 8) + (7-EBX mod 8)
         ADD     EBX,EAX         //EBX:=EBX+EAX
         POP     EAX
         MOV     EAX,[EAX].FBits
         BT      [EAX],EBX       //Copy bit to CF
         SBB     EAX,EAX         //Substract with borrow - EAX:=EAX-CF
         AND     EAX,1           //Return only one bit
         POP     EBX
end;

procedure TUniDIB.SetSeqPixel1(Value: integer); assembler;
asm
         MOV     ECX,[EAX].XActX
         INC     [EAX].XActX
         MOV     EAX,[EAX].FActPointer
         XOR     ECX,7
         CMP     Value,0
         JZ      @@2
         @@1:
         BTS     [EAX],ECX
         JMP     @@3
         @@2:
         BTR     [EAX],ECX
         @@3:
end;

function TUniDIB.GetSeqPixel1: integer; assembler;
asm
         MOV     ECX,[EAX].XActX
         INC     [EAX].XActX
         MOV     EAX,[EAX].FActPointer
         XOR     ECX,7
         BT      [EAX],ECX       //Copy bit to CF
         SBB     EAX,EAX         //Substract with borrow - EAX:=EAX-CF
         AND     EAX,1           //Return only one bit
end;

{$ENDIF}

{$IFDEF CPU64}

procedure TUniDIB.SetPixel1(X, Y, Value: integer); assembler;
asm
         PUSH    RBX
         PUSH    RAX
         MOV     RBX,x           //rbx:=X
         MOV     RAX,[RAX].FDWordWidth  //rax:=FDWordWidth*8*Y
         SHL     RAX,1
         SHL     RAX,1
         SHL     RAX,1
         MUL     Y
{8 pixels are stored byte; first pixel is the most
 important bit in byte}
         XOR     RBX,7           //=>rbx:=(rbx div 8) + (7-rbx mod 8)
         ADD     RBX,RAX         //rbx:=rbx+rax
         POP     RAX
         MOV     RAX,[RAX].FBits
         CMP     Value,0
         JZ      @@2
         @@1:
         BTS     [RAX],RBX       //Set this bit
         JMP     @@3
         @@2:
         BTR     [RAX],RBX       //Clear this bit
         @@3:
         POP     RBX
end;

function TUniDIB.GetPixel1(X, Y: integer): integer; assembler;
asm
         PUSH    RBX
         PUSH    RAX
         MOV     RBX,x           //rbx:=X
         MOV     RAX,[RAX].FDWordWidth  //rax:=FDWordWidth*8*Y
         SHL     RAX,1
         SHL     RAX,1
         SHL     RAX,1
         MUL     y
         XOR     RBX,7           //rbx:=(rbx div 8) + (7-rbx mod 8)
         ADD     RBX,RAX         //rbx:=rbx+rax
         POP     RAX
         MOV     RAX,[RAX].FBits
         BT      [RAX],RBX       //Copy bit to CF
         SBB     RAX,RAX         //Substract with borrow - rax:=rax-CF
         AND     RAX,1           //Return only one bit
         POP     RBX
end;

procedure TUniDIB.SetSeqPixel1(Value: integer); assembler;
asm
         MOV     RCX,[RAX].XActX
         INC     [RAX].XActX
         MOV     RAX,[RAX].FActPointer
         XOR     RCX,7
         CMP     Value,0
         JZ      @@2
         @@1:
         BTS     [RAX],RCX
         JMP     @@3
         @@2:
         BTR     [RAX],RCX
         @@3:
end;

function TUniDIB.GetSeqPixel1: integer; assembler;
asm
         MOV     RCX,[RAX].XActX
         INC     [RAX].XActX
         MOV     RAX,[RAX].FActPointer
         XOR     RCX,7
         BT      [RAX],RCX       //Copy bit to CF
         SBB     RAX,RAX         //Substract with borrow - rax:=rax-CF
         AND     RAX,1           //Return only one bit

end;

{$ENDIF}

end.
