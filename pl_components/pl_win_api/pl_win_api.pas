{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_Win_API;

{$warn 5023 off : no warning about unused units}
interface

uses
  Win.AccCtrl, Win.ACLApi, Win.ACLUi, Win.ActiveCf, Win.Adc, Win.AdoDef, 
  Win.AudioSessionTypes, Win.Authz, Win.BlueToothAPIs, Win.BlueToothLEAPIs, 
  Win.BTHDef, Win.BTHIOCtl, Win.BTHLEDef, Win.BTHSDPDef, Win.CAPI, Win.ComCat, 
  Win.DirectXMath, Win.DPAPI, Win.FileAPI, Win.IntSafe, Win.MinWinBase, 
  Win.NTDef, Win.NTStatus, Win.ObjectArray, Win.ObjIdl, Win.ObjIdlBase, 
  Win.OLEDB, Win.OleIdl, Win.ProcessThreadsAPI, Win.PropIdl, Win.PropSys, 
  Win.RPCNDR, Win.SDKDDKVer, Win.ServProv, Win.ShellAPI, Win.SHTypes, 
  Win.StructuredQueryCondition, Win.Synchapi, Win.SysInfoAPI, Win.TCGuid, 
  Win.WinBase, Win.WinNT, Win.WS2BTH, Win.WTypes, Win.WTypesBase, 
  Win.X3DAudio, Win.XAPO, Win.XAPOBase, Win.XAPOFx, Win.XAudio2, 
  Win.XAudio2Fx, Win.Xinput, Win.AudioClient, Win.AudioEndPoints, 
  Win.AudioMediaType, Win.AudioPolicy, Win.AVRT, Win.MMDeviceApi, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('pl_Win_API', @Register);
end.
