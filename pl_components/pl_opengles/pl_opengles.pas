{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_opengles;

{$warn 5023 off : no warning about unused units}
interface

uses
  AllOpenGLESRegister, ctGLES1, ctGLES1_rpt, ctGLES2, ctGLES2_rpt, 
  ctopengles1panel, ctopengles2panel, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('AllOpenGLESRegister', @AllOpenGLESRegister.Register);
end;

initialization
  RegisterPackage('pl_opengles', @Register);
end.
