{*************************************************************************
                PilotLogic Software House

  Package pl_OpenGLES
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

 ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * ***** END LICENSE BLOCK *****
 ************************************************************************}

unit ctopengles2panel;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LCLProc, Forms, Controls, LCLType, LCLIntf, LResources,
  Graphics, LMessages,
  ctGLES2;

type

  TOpenGLES2Panel = class(TComponent)
  private
     feglDisplay: EGLDisplay;
     feglConfig: EGLConfig;
     feglSurface: EGLSurface;
     feglContext: EGLContext;
     eglWindow: EGLNativeWindowType;
     pi32ConfigAttribs: array[0..128] of EGLint;

     iMajorVersion: EGLint;
     iMinorVersion: EGLint;
     iConfigs: integer;
     FForm:TForm;
     FHandle:HWND;
     fdc : hDC;
  protected
     flibEGL:string;
     flibOpenGLES:string;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    Procedure SwapBuffers;
    Function InitOpenGLES:boolean;
    Property libEGL:string read flibEGL write flibEGL;
    Property libOpenGLES:string read flibOpenGLES write flibOpenGLES;
  published
    Property Form:Tform read FForm;
  end;


implementation


{ TOpenGLES2Panel }

constructor TOpenGLES2Panel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  flibEGL:=LIBNAME_EGL;
  flibOpenGLES:=LIBNAME_OPENGLES;

  if AOwner is TForm then
   begin
     FForm:=TForm(AOwner);
     FHandle:= FForm.Handle;
   end;
end;

destructor TOpenGLES2Panel.Destroy;
begin
 if feglDisplay<>nil then
  begin
    eglMakeCurrent(feglDisplay, nil, nil, nil);
    eglTerminate(feglDisplay);
  end;

  inherited Destroy;
end;


Procedure TOpenGLES2Panel.SwapBuffers;
 begin
   eglSwapBuffers(feglDisplay, feglSurface);
 end;

Function TOpenGLES2Panel.InitOpenGLES:boolean;
 var i: integer;
begin
  Result:=false;

  if FHandle=0 then exit;

  if OpenGLES_Initialize(flibEGL,flibOpenGLES)= false then Exit;

  feglDisplay:= nil;
  feglConfig:= nil;
  feglSurface:= nil;
  feglContext:= nil;

  eglWindow:= fHandle;

  fdc := GetDC(eglWindow);

  feglDisplay := eglGetDisplay(fdc);
  //feglDisplay := eglGetDisplay(0);   //=== ct9999 ==== with previus give eERROR in eglInitialize

  if eglInitialize(feglDisplay, @iMajorVersion, @iMinorVersion) = 0 then      // <<<=====
    begin
      MessageBox(0, 'eglInitialize() failed.', 'Error', MB_OK or MB_ICONEXCLAMATION);
      Exit;
    end;

  i := 0;      pi32ConfigAttribs[i] := EGL_RED_SIZE;
  i := i + 1;  pi32ConfigAttribs[i] := 5;
  i := i + 1;  pi32ConfigAttribs[i] := EGL_GREEN_SIZE;
  i := i + 1;  pi32ConfigAttribs[i] := 6;
  i := i + 1;  pi32ConfigAttribs[i] := EGL_BLUE_SIZE;
  i := i + 1;  pi32ConfigAttribs[i] := 5;
  i := i + 1;  pi32ConfigAttribs[i] := EGL_ALPHA_SIZE;
  i := i + 1;  pi32ConfigAttribs[i] := 0;
  i := i + 1;  pi32ConfigAttribs[i] := EGL_SURFACE_TYPE;
  i := i + 1;  pi32ConfigAttribs[i] := EGL_WINDOW_BIT;
  i := i + 1;  pi32ConfigAttribs[i] := EGL_NONE;


  if eglChooseConfig(feglDisplay, @pi32ConfigAttribs, @feglConfig, 1, @iConfigs) = 0 then
	begin
          MessageBox(0, 'eglChooseConfig() failed.', 'Error', MB_OK or MB_ICONEXCLAMATION);
          Exit;
	end;


  feglSurface := eglCreateWindowSurface(feglDisplay, feglConfig, eglWindow, nil);
  feglContext := eglCreateContext(feglDisplay, feglConfig, nil, nil);

  If eglMakeCurrent(feglDisplay, feglSurface, feglSurface, feglContext)=0 then           // <<<=====
	begin
          MessageBox(0, 'eglMakeCurrent() failed.', 'Error', MB_OK or MB_ICONEXCLAMATION);
          Exit;
	end;

  OpenGLES_InitializeAdvance(feglDisplay);

  Result:=True;
 end;

end.

