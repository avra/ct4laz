{**********************************************************************
                Copyright (c) PilotLogic Software House

 Package pl_OpenGLES
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}
{
 This file Generated from the Khronos OpenGL and EGL API Registry
 at 28/9/2020 21:41:11
}

unit ctGLES2;

{$MODE OBJFPC}{$H+}

{$MACRO ON}
{$PACKRECORDS C}

{$IFDEF WINDOWS}
  {$DEFINE extdecl := stdcall}
{$ELSE}
  {$DEFINE extdecl := cdecl}
{$ENDIF}

interface
uses
   sysutils, dynlibs, types, LCLType,
  {$IF DEFINED(MSWINDOWS)} 
   Windows,
  {$ELSEIF DEFINED(DARWIN)}
 
  {$ELSE}
   X, XLib, XUtil,
  {$ENDIF}
   math;

Const
 {$IF DEFINED(MSWINDOWS)}
    LIBNAME_EGL      = 'libEGL.dll';
    LIBNAME_OPENGLES = 'libGLESv2.dll';
 {$ELSEIF DEFINED(DARWIN)}
    LIBNAME_EGL      = 'libEGL.dylib';
    LIBNAME_OPENGLES = 'libGLESv2.dylib';
 {$ELSE}
    LIBNAME_EGL      = 'libEGL.so';
    LIBNAME_OPENGLES = 'libGLESv2.so';
 {$ENDIF}

type
 
  int32_t  = LongInt;
  Pint32_t = ^int32_t;
  int64_t  = LongInt;
  Pint64_t = ^int64_t;
  Plong    = ^LongInt;
 
  GLenum = Cardinal;
  GLboolean = BYTEBOOL;
  GLbitfield = Cardinal;
  GLbyte = Shortint;
  GLshort = SmallInt;
  GLint = Integer;
  GLsizei = Integer;
  GLubyte = Byte;
  GLushort = Word;
  GLuint = Cardinal;
  GLfloat = Single;
  GLclampf = Single;
  GLdouble = Double;
  GLclampd = Double;
  GLvoid = Pointer;
  GLint64 = Int64;
  GLuint64 = UInt64;
  TGLenum = GLenum;
  TGLboolean = GLboolean;
  TGLbitfield = GLbitfield;
  TGLbyte = GLbyte;
  TGLshort = GLshort;
  TGLint = GLint;
  TGLsizei = GLsizei;
  TGLubyte = GLubyte;
  TGLushort = GLushort;
  TGLuint = GLuint;
  TGLfloat = GLfloat;
  TGLclampf = GLclampf;
  TGLdouble = GLdouble;
  TGLclampd = GLclampd;
  TGLvoid = GLvoid;
  TGLint64 = GLint64;
  TGLuint64 = GLuint64;
  PGLboolean = ^GLboolean;
  PGLbyte = ^GLbyte;
  PGLshort = ^GLshort;
  PGLint = ^GLint;
  PGLsizei = ^GLsizei;
  PGLushort = ^GLushort;
  PGLuint = ^GLuint;
  PGLclampf = ^GLclampf;
  PGLfloat = ^GLfloat;
  PGLdouble = ^GLdouble;
  PGLclampd = ^GLclampd;
  PGLenum = ^GLenum;
  PGLvoid = Pointer;
  PPGLvoid = ^PGLvoid;
  PGLint64 = ^GLint64;
  PGLuint64 = ^GLuint64;
  PGLfixed = ^GLfixed;
  GLhalfNV = WORD;
  TGLhalfNV = GLhalfNV;
  PGLhalfNV = ^GLhalfNV;
  PGLHandleARB = ^GLHandleARB;
  GLHandleARB = Integer;
  GLcharARB = AnsiChar;
  PGLcharARB = PAnsiChar;
  PPGLcharARB = ^PGLcharARB;
  GLintptr = GLint;
  PGLintptr = ^GLintptr;
  GLsizeiptr = GLsizei;
  PGLsizeiptr = ^GLsizeiptr;
  GLintptrARB = GLint;
  GLsizeiptrARB = GLsizei;
  GLHandle = Integer;
  PGLchar = PAnsiChar;
  PPGLchar = ^PGLChar;
  GLint64EXT = Int64;
  TGLint64EXT = GLint64EXT;
  PGLint64EXT = ^GLint64EXT;
  GLuint64EXT = GLuint64;
  TGLuint64EXT = GLuint64EXT;
  PGLuint64EXT = ^GLuint64EXT;
  PGLubyte = PAnsiChar;
  GLeglClientBufferEXT = Pointer;
  Pvoid = Pointer;
  PPvoid = ^Pointer;
  Pstruct_cl_context = Pointer;
  Pstruct_cl_event = Pointer;
  PPGLboolean = ^PGLboolean;
  GLeglImageOES = Pointer;
  GLclampx = LongInt;
  TGLclampx = GLclampx;
  PGLclampx = ^GLclampx;
  PPGLclampx = ^PGLclampx;
  PLPVOID = PGLvoid;
  GLBool = Boolean;
  TGLBool = GLBool;
  PGLBool = ^GLBool;
  PPGLBool = ^PGLBool;
  GLfixed = PtrInt;
  TGLfixed = GLfixed;
 
  GLunsignedint  = Cardinal;
  PGLunsignedint = ^GLunsignedint;
  GLPunsignedint = ^GLunsignedint;
  GLunsignedlong = Cardinal;
  GLPunsignedlong= ^GLunsignedlong;
 
  GLVULKANPROCNV = procedure; extdecl;
  TGLVULKANPROCNV = GLVULKANPROCNV;
  PGLVULKANPROCNV = ^GLVULKANPROCNV;
  PPGLVULKANPROCNV = ^PGLVULKANPROCNV;
  HPBUFFERARB = THandle;
  HPBUFFEREXT = THandle;
  PHVIDEOOUTPUTDEVICENV = ^HVIDEOOUTPUTDEVICENV;
  HVIDEOOUTPUTDEVICENV = THandle;
  PHPVIDEODEV = ^HPVIDEODEV;
  HPVIDEODEV = THandle;
  PHPGPUNV = ^HPGPUNV;
  PHGPUNV = ^HGPUNV;
  HVIDEOINPUTDEVICENV = THandle;
  PHVIDEOINPUTDEVICENV = ^HVIDEOINPUTDEVICENV;
  HPGPUNV = THandle;
  HGPUNV = THandle;
  GLsync = Pointer;
  _cl_context = record end;
  _cl_event = record end;
  p_cl_context = ^_cl_context;
  p_cl_event = ^_cl_event;
 
  TglDebugProcARB = procedure (source: GLenum; type_: GLenum; id: GLuint; severity: GLenum; length: GLsizei; const message_: PGLchar; userParam: PGLvoid); extdecl;
  GLDEBUGPROCARB = TglDebugProcARB;
  TglDebugProcAMD = procedure (id: GLuint; category: GLenum; severity: GLenum; length: GLsizei; const message_: PGLchar; userParam: PGLvoid); extdecl;
  GLDEBUGPROCAMD=TglDebugProcAMD;
  TglDebugProc = procedure(source : GLEnum; type_ : GLEnum; id : GLUInt; severity : GLUInt; length : GLsizei; const message_ : PGLCHar; userParam : PGLvoid); extdecl;
  GLDEBUGPROC=TglDebugProc;
  TglDebugProcKHR= procedure(source: GLenum; type_: GLenum; id: GLuint; severity: GLenum; length: GLsizei; message_: PGLchar; userParam: Pointer); extdecl;
  GLDEBUGPROCKHR =TglDebugProcKHR;
  GLvdpauSurfaceNV = GLintptr;
  PGLvdpauSurfaceNV = ^GLvdpauSurfaceNV;
 
 
 TGLVectorub2 = array[0..1] of GLubyte;
 TGLVectori2  = array[0..1] of GLint;
 TGLVectorf2  = array[0..1] of GLfloat;
 TGLVectord2  = array[0..1] of GLdouble;
 TGLVectorp2  = array[0..1] of Pointer;
 TGLVectorub3 = array[0..2] of GLubyte;
 TGLVectori3  = array[0..2] of GLint;
 TGLVectorf3  = array[0..2] of GLfloat;
 TGLVectord3  = array[0..2] of GLdouble;
 TGLVectorp3  = array[0..2] of Pointer;
 TGLVectorub4 = array[0..3] of GLubyte;
 TGLVectori4  = array[0..3] of GLint;
 TGLVectorf4  = array[0..3] of GLfloat;
 TGLVectord4  = array[0..3] of GLdouble;
 TGLVectorp4  = array[0..3] of Pointer;
 
 TGLArrayf4 = TGLVectorf4;
 TGLArrayf3 = TGLVectorf3;
 TGLArrayd3 = TGLVectord3;
 TGLArrayi4 = TGLVectori4;
 TGLArrayp4 = TGLVectorp4;
 
 TGlMatrixub3 = array[0..2, 0..2] of GLubyte;
 TGlMatrixi3  = array[0..2, 0..2] of GLint;
 TGLMatrixf3  = array[0..2, 0..2] of GLfloat;
 TGLMatrixd3  = array[0..2, 0..2] of GLdouble;
 TGlMatrixub4 = array[0..3, 0..3] of GLubyte;
 TGlMatrixi4  = array[0..3, 0..3] of GLint;
 TGLMatrixf4  = array[0..3, 0..3] of GLfloat;
 TGLMatrixd4  = array[0..3, 0..3] of GLdouble;
 
 TGLVector3f = TGLVectorf3;
 TVector3f = TGLVectorf3;
 TVector3d = TGLVectord3;
 TVector4i = TGLVectori4;
 TVector4f = TGLVectorf4;
 TVector4p = TGLVectorp4;
 TMatrix4f = TGLMatrixf4;
 TMatrix4d = TGLMatrixd4;
 PGLMatrixd4 = ^TGLMatrixd4;
 PVector4i = ^TVector4i;
 
  type
  EGLint                = Integer;
  EGLboolean            = Cardinal;
  EGLenum               = Cardinal;
  EGLConfig             = Pointer;
  EGLContext            = Pointer;
  EGLDisplay            = Pointer;
  EGLSurface            = Pointer;
  EGLClientBuffer       = Pointer;
 
  Pstructwl_display     = Pointer;
  Pstructwl_resource    = Pointer;
  Pstructwl_buffer      = Pointer;
 
  {$IF DEFINED(MSWINDOWS)}
           EGLNativeDisplayType = PtrInt;
           EGLNativeWindowType  = HWND;
           EGLNativePixmapType  = HBITMAP;
  {$ELSEIF DEFINED(DARWIN)}
           EGLNativeDisplayType = PtrInt;
           EGLNativeWindowType  = HWND;
           EGLNativePixmapType  = pointer;
  {$ELSEIF DEFINED(UNIX)}
           EGLNativeDisplayType = PtrInt;
           EGLNativeWindowType  = TWindow;
           EGLNativePixmapType  = TPixmap;
  {$ELSE}
           EGLNativeDisplayType = PtrInt;
           EGLNativeWindowType  = pointer;
           EGLNativePixmapType  = pointer;
  {$ENDIF}
 
  PEGLint               = ^EGLint;
  PEGLboolean           = ^EGLboolean;
  PEGLenum              = ^EGLenum;
  PEGLConfig            = ^EGLConfig;
  PEGLContext           = ^EGLContext;
  PEGLDisplay           = ^EGLDisplay;
  PEGLSurface           = ^EGLSurface;
  PEGLClientBuffer      = ^EGLClientBuffer;
  PEGLNativeDisplayType = ^EGLNativeDisplayType;
  PEGLNativePixmapType  = ^EGLNativePixmapType;
  PEGLNativeWindowType  = ^EGLNativeWindowType;
 
  EGLAttrib = PtrInt;
  PEGLAttrib = ^EGLAttrib;
  EGLAttribKHR  = PtrInt;
  PEGLAttribKHR = ^EGLAttrib;
  EGLuint64KHR = PtrUInt;
  PEGLuint64KHR = ^EGLuint64KHR;
  EGLOutputLayerEXT = Pointer;
  PEGLOutputLayerEXT = ^EGLOutputLayerEXT ;
  EGLObjectKHR = Integer;
  PEGLObjectKHR = ^EGLObjectKHR;
  EGLLabelKHR = Integer;
  PEGLLabelKHR = ^EGLLabelKHR;
  EGLDeviceEXT = Pointer;
  PEGLDeviceEXT = ^EGLDeviceEXT;
  EGLTime = Cardinal;
  EGLTimeKHR = Cardinal;
  PEGLTimeKHR = ^EGLTimeKHR;
  EGLTimeNV = Cardinal;
  EGLuint64NV = Cardinal;
 
  EGLNativeFileDescriptorKHR = EGLint;
  EGLnsecsANDROID = EGLint;
  PEGLnsecsANDROID = ^EGLnsecsANDROID;
  EGLSync   = Pointer;
  EGLImage  = Pointer;
  PEGLImage = Pointer;
  EGLImageKHR = Pointer;
  EGLOutputPortEXT = Pointer;
  PEGLOutputPortEXT = ^EGLOutputPortEXT;
  EGLStreamKHR = Pointer;
  EGLSyncKHR = Pointer;
  EGLSyncNV = Pointer;
  __eglMustCastToProperFunctionPointerType = Pointer;
  PstructAHardwareBuffer = Pointer;
  PstructEGLClientPixmapHI = Pointer;
  EGLDEBUGPROCKHR = Pointer;
  EGLSetBlobFuncANDROID = Pointer;
  EGLGetBlobFuncANDROID = Pointer;
 
//============= Constants ============================== 
 const
 
    //... CombinerPortionNV ....
    GL_RGB = $1907;
    GL_ALPHA = $1906;
 
    //... MapTypeNV ....
    GL_FLOAT = $1406;   // api(gles2) //extension: GL_OES_texture_float
 
    //... ScalarType ....
    GL_UNSIGNED_BYTE = $1401;
    GL_UNSIGNED_SHORT = $1403;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_UNSIGNED_INT = $1405;   // api(gles2) //extension: GL_ANGLE_depth_texture
 
    //... PathColorFormat ....
    GL_NONE = 0;   // api(gl|glcore|gles2) //extension: GL_KHR_context_flush_control
    GL_LUMINANCE = $1909;
    GL_LUMINANCE_ALPHA = $190A;
    GL_RGBA = $1908;
 
    //... SecondaryColorPointerTypeIBM ....
    GL_SHORT = $1402;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_INT = $1404;
 
    //... WeightPointerTypeARB ....
    GL_BYTE = $1400;   // api(gles2) //extension: GL_EXT_render_snorm
 
    //... IndexFunctionEXT ....
    GL_NEVER = $0200;
    GL_ALWAYS = $0207;
    GL_LESS = $0201;
    GL_LEQUAL = $0203;
    GL_EQUAL = $0202;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_GEQUAL = $0206;
    GL_GREATER = $0204;
    GL_NOTEQUAL = $0205;
 
    //... VertexShaderWriteMaskEXT ....
    GL_TRUE = 1;
    GL_FALSE = 0;
 
    //... CombinerComponentUsageNV ....
    GL_BLUE = $1905;
 
    //... VertexAttribPropertyARB ....
    GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = $889F;
    GL_VERTEX_ATTRIB_ARRAY_ENABLED = $8622;
    GL_VERTEX_ATTRIB_ARRAY_SIZE = $8623;
    GL_VERTEX_ATTRIB_ARRAY_STRIDE = $8624;
    GL_VERTEX_ATTRIB_ARRAY_TYPE = $8625;
    GL_VERTEX_ATTRIB_ARRAY_NORMALIZED = $886A;
    GL_VERTEX_ATTRIB_ARRAY_INTEGER = $88FD;
    GL_VERTEX_ATTRIB_ARRAY_DIVISOR = $88FE;
    GL_VERTEX_ATTRIB_BINDING = $82D4;
    GL_VERTEX_ATTRIB_RELATIVE_OFFSET = $82D5;
    GL_CURRENT_VERTEX_ATTRIB = $8626;
 
    //... VertexAttribPointerPropertyARB ....
    GL_VERTEX_ATTRIB_ARRAY_POINTER = $8645;
 
    //... BufferPointerNameARB ....
    GL_BUFFER_MAP_POINTER = $88BD;
 
    //... BufferPNameARB ....
    GL_BUFFER_SIZE = $8764;
    GL_BUFFER_USAGE = $8765;
    GL_BUFFER_ACCESS_FLAGS = $911F;
    GL_BUFFER_MAPPED = $88BC;
    GL_BUFFER_MAP_OFFSET = $9121;
    GL_BUFFER_MAP_LENGTH = $9120;
 
    //... GetMultisamplePNameNV ....
    GL_SAMPLE_POSITION = $8E50;
 
    //... AttribMask ....
    GL_COLOR_BUFFER_BIT = $00004000;
    GL_DEPTH_BUFFER_BIT = $00000100;
    GL_STENCIL_BUFFER_BIT = $00000400;
 
    //... BlendEquationModeEXT ....
    GL_FUNC_ADD = $8006;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_FUNC_REVERSE_SUBTRACT = $800B;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_FUNC_SUBTRACT = $800A;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_MAX = $8008;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_MIN = $8007;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
 
    //... BufferTargetARB ....
    GL_ARRAY_BUFFER = $8892;
    GL_ATOMIC_COUNTER_BUFFER = $92C0;
    GL_COPY_READ_BUFFER = $8F36;
    GL_COPY_WRITE_BUFFER = $8F37;
    GL_DISPATCH_INDIRECT_BUFFER = $90EE;
    GL_DRAW_INDIRECT_BUFFER = $8F3F;
    GL_ELEMENT_ARRAY_BUFFER = $8893;
    GL_PIXEL_PACK_BUFFER = $88EB;
    GL_PIXEL_UNPACK_BUFFER = $88EC;
    GL_SHADER_STORAGE_BUFFER = $90D2;
    GL_TEXTURE_BUFFER = $8C2A;
    GL_TRANSFORM_FEEDBACK_BUFFER = $8C8E;
    GL_UNIFORM_BUFFER = $8A11;
 
    //... BufferUsageARB ....
    GL_STREAM_DRAW = $88E0;
    GL_STREAM_READ = $88E1;
    GL_STREAM_COPY = $88E2;
    GL_STATIC_DRAW = $88E4;
    GL_STATIC_READ = $88E5;
    GL_STATIC_COPY = $88E6;
    GL_DYNAMIC_DRAW = $88E8;
    GL_DYNAMIC_READ = $88E9;
    GL_DYNAMIC_COPY = $88EA;
 
    //... BufferAccessARB ....
    GL_READ_ONLY = $88B8;
    GL_WRITE_ONLY = $88B9;
    GL_READ_WRITE = $88BA;
 
    //... BufferStorageMask ....
    GL_MAP_READ_BIT = $0001;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_MAP_WRITE_BIT = $0002;   // api(gles2) //extension: GL_EXT_buffer_storage
 
    //... ColorMaterialFace ....
    GL_BACK = $0405;
    GL_FRONT = $0404;
    GL_FRONT_AND_BACK = $0408;
 
    //... ContextFlagMask ....
    GL_CONTEXT_FLAG_DEBUG_BIT = $00000002;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT = $00000004;
 
    //... DrawBufferMode ....
    GL_COLOR_ATTACHMENT0 = $8CE0;
    GL_COLOR_ATTACHMENT1 = $8CE1;
    GL_COLOR_ATTACHMENT2 = $8CE2;
    GL_COLOR_ATTACHMENT3 = $8CE3;
    GL_COLOR_ATTACHMENT4 = $8CE4;
    GL_COLOR_ATTACHMENT5 = $8CE5;
    GL_COLOR_ATTACHMENT6 = $8CE6;
    GL_COLOR_ATTACHMENT7 = $8CE7;
    GL_COLOR_ATTACHMENT8 = $8CE8;
    GL_COLOR_ATTACHMENT9 = $8CE9;
    GL_COLOR_ATTACHMENT10 = $8CEA;
    GL_COLOR_ATTACHMENT11 = $8CEB;
    GL_COLOR_ATTACHMENT12 = $8CEC;
    GL_COLOR_ATTACHMENT13 = $8CED;
    GL_COLOR_ATTACHMENT14 = $8CEE;
    GL_COLOR_ATTACHMENT15 = $8CEF;
    GL_COLOR_ATTACHMENT16 = $8CF0;
    GL_COLOR_ATTACHMENT17 = $8CF1;
    GL_COLOR_ATTACHMENT18 = $8CF2;
    GL_COLOR_ATTACHMENT19 = $8CF3;
    GL_COLOR_ATTACHMENT20 = $8CF4;
    GL_COLOR_ATTACHMENT21 = $8CF5;
    GL_COLOR_ATTACHMENT22 = $8CF6;
    GL_COLOR_ATTACHMENT23 = $8CF7;
    GL_COLOR_ATTACHMENT24 = $8CF8;
    GL_COLOR_ATTACHMENT25 = $8CF9;
    GL_COLOR_ATTACHMENT26 = $8CFA;
    GL_COLOR_ATTACHMENT27 = $8CFB;
    GL_COLOR_ATTACHMENT28 = $8CFC;
    GL_COLOR_ATTACHMENT29 = $8CFD;
    GL_COLOR_ATTACHMENT30 = $8CFE;
    GL_COLOR_ATTACHMENT31 = $8CFF;
 
    //... EnableCap ....
    GL_BLEND = $0BE2;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_CULL_FACE = $0B44;
    GL_DEBUG_OUTPUT = $92E0;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_OUTPUT_SYNCHRONOUS = $8242;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEPTH_TEST = $0B71;
    GL_DITHER = $0BD0;
    GL_POLYGON_OFFSET_FILL = $8037;
    GL_PRIMITIVE_RESTART_FIXED_INDEX = $8D69;
    GL_RASTERIZER_DISCARD = $8C89;
    GL_SAMPLE_ALPHA_TO_COVERAGE = $809E;
    GL_SAMPLE_COVERAGE = $80A0;
    GL_SAMPLE_MASK = $8E51;
    GL_SAMPLE_SHADING = $8C36;
    GL_SCISSOR_TEST = $0C11;   // api(gles2) //extension: GL_NV_viewport_array
    GL_STENCIL_TEST = $0B90;
    GL_TEXTURE_2D = $0DE1;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_VERTEX_ARRAY = $8074;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
 
    //... ErrorCode ....
    GL_INVALID_ENUM = $0500;
    GL_INVALID_FRAMEBUFFER_OPERATION = $0506;
    GL_INVALID_OPERATION = $0502;
    GL_INVALID_VALUE = $0501;
    GL_NO_ERROR = 0;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_OUT_OF_MEMORY = $0505;
    GL_STACK_OVERFLOW = $0503;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_STACK_UNDERFLOW = $0504;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
 
    //... FogMode ....
    GL_LINEAR = $2601;
 
    //... FrontFaceDirection ....
    GL_CCW = $0901;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_CW = $0900;   // api(gles2) //extension: GL_EXT_tessellation_shader
 
    //... GetPName ....
    GL_ACTIVE_TEXTURE = $84E0;
    GL_ALIASED_LINE_WIDTH_RANGE = $846E;
    GL_ALIASED_POINT_SIZE_RANGE = $846D;
    GL_ALPHA_BITS = $0D55;
    GL_ARRAY_BUFFER_BINDING = $8894;
    GL_BLEND_COLOR = $8005;
    GL_BLEND_DST_ALPHA = $80CA;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_BLEND_DST_RGB = $80C8;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_BLEND_EQUATION_ALPHA = $883D;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_BLEND_EQUATION_RGB = $8009;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_BLEND_SRC_ALPHA = $80CB;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_BLEND_SRC_RGB = $80C9;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_BLUE_BITS = $0D54;
    GL_COLOR_CLEAR_VALUE = $0C22;
    GL_COLOR_WRITEMASK = $0C23;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_COMPRESSED_TEXTURE_FORMATS = $86A3;
    GL_CONTEXT_FLAGS = $821E;
    GL_CULL_FACE_MODE = $0B45;
    GL_CURRENT_PROGRAM = $8B8D;
    GL_DEBUG_GROUP_STACK_DEPTH = $826D;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEPTH_BITS = $0D56;
    GL_DEPTH_CLEAR_VALUE = $0B73;
    GL_DEPTH_FUNC = $0B74;
    GL_DEPTH_RANGE = $0B70;   // api(gles2) //extension: GL_NV_viewport_array
    GL_DEPTH_WRITEMASK = $0B72;
    GL_DISPATCH_INDIRECT_BUFFER_BINDING = $90EF;
    GL_DRAW_FRAMEBUFFER_BINDING = $8CA6;
    GL_ELEMENT_ARRAY_BUFFER_BINDING = $8895;
    GL_FRAGMENT_SHADER_DERIVATIVE_HINT = $8B8B;
    GL_FRONT_FACE = $0B46;
    GL_GREEN_BITS = $0D53;
    GL_IMPLEMENTATION_COLOR_READ_FORMAT = $8B9B;
    GL_IMPLEMENTATION_COLOR_READ_TYPE = $8B9A;
    GL_LAYER_PROVOKING_VERTEX = $825E;
    GL_LINE_WIDTH = $0B21;
    GL_MAJOR_VERSION = $821B;
    GL_MAX_3D_TEXTURE_SIZE = $8073;
    GL_MAX_ARRAY_TEXTURE_LAYERS = $88FF;
    GL_MAX_COLOR_TEXTURE_SAMPLES = $910E;
    GL_MAX_COMBINED_ATOMIC_COUNTERS = $92D7;
    GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = $8266;
    GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = $8A33;
    GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = $8A32;
    GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS = $90DC;
    GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS = $8B4D;
    GL_MAX_COMBINED_UNIFORM_BLOCKS = $8A2E;
    GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = $8A31;
    GL_MAX_COMPUTE_ATOMIC_COUNTERS = $8265;
    GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = $8264;
    GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS = $90DB;
    GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS = $91BC;
    GL_MAX_COMPUTE_UNIFORM_BLOCKS = $91BB;
    GL_MAX_COMPUTE_UNIFORM_COMPONENTS = $8263;
    GL_MAX_COMPUTE_WORK_GROUP_COUNT = $91BE;
    GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS = $90EB;
    GL_MAX_COMPUTE_WORK_GROUP_SIZE = $91BF;
    GL_MAX_CUBE_MAP_TEXTURE_SIZE = $851C;
    GL_MAX_DEBUG_GROUP_STACK_DEPTH = $826C;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_DEPTH_TEXTURE_SAMPLES = $910F;
    GL_MAX_DRAW_BUFFERS = $8824;
    GL_MAX_ELEMENTS_INDICES = $80E9;
    GL_MAX_ELEMENTS_VERTICES = $80E8;
    GL_MAX_ELEMENT_INDEX = $8D6B;
    GL_MAX_FRAGMENT_ATOMIC_COUNTERS = $92D6;
    GL_MAX_FRAGMENT_INPUT_COMPONENTS = $9125;
    GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = $90DA;
    GL_MAX_FRAGMENT_UNIFORM_BLOCKS = $8A2D;
    GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = $8B49;
    GL_MAX_FRAGMENT_UNIFORM_VECTORS = $8DFD;
    GL_MAX_FRAMEBUFFER_HEIGHT = $9316;
    GL_MAX_FRAMEBUFFER_LAYERS = $9317;
    GL_MAX_FRAMEBUFFER_SAMPLES = $9318;
    GL_MAX_FRAMEBUFFER_WIDTH = $9315;
    GL_MAX_GEOMETRY_ATOMIC_COUNTERS = $92D5;
    GL_MAX_GEOMETRY_INPUT_COMPONENTS = $9123;
    GL_MAX_GEOMETRY_OUTPUT_COMPONENTS = $9124;
    GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = $90D7;
    GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = $8C29;
    GL_MAX_GEOMETRY_UNIFORM_BLOCKS = $8A2C;
    GL_MAX_GEOMETRY_UNIFORM_COMPONENTS = $8DDF;
    GL_MAX_INTEGER_SAMPLES = $9110;
    GL_MAX_LABEL_LENGTH = $82E8;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_PROGRAM_TEXEL_OFFSET = $8905;
    GL_MAX_RENDERBUFFER_SIZE = $84E8;
    GL_MAX_SAMPLE_MASK_WORDS = $8E59;
    GL_MAX_SERVER_WAIT_TIMEOUT = $9111;
    GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS = $90DD;
    GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS = $92D3;
    GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = $90D8;
    GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS = $92D4;
    GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = $90D9;
    GL_MAX_TEXTURE_BUFFER_SIZE = $8C2B;
    GL_MAX_TEXTURE_IMAGE_UNITS = $8872;
    GL_MAX_TEXTURE_LOD_BIAS = $84FD;
    GL_MAX_TEXTURE_SIZE = $0D33;
    GL_MAX_UNIFORM_BLOCK_SIZE = $8A30;
    GL_MAX_UNIFORM_BUFFER_BINDINGS = $8A2F;
    GL_MAX_UNIFORM_LOCATIONS = $826E;
    GL_MAX_VARYING_COMPONENTS = $8B4B;
    GL_MAX_VARYING_VECTORS = $8DFC;
    GL_MAX_VERTEX_ATOMIC_COUNTERS = $92D2;
    GL_MAX_VERTEX_ATTRIBS = $8869;
    GL_MAX_VERTEX_ATTRIB_BINDINGS = $82DA;
    GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = $82D9;
    GL_MAX_VERTEX_OUTPUT_COMPONENTS = $9122;
    GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS = $90D6;
    GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS = $8B4C;
    GL_MAX_VERTEX_UNIFORM_BLOCKS = $8A2B;
    GL_MAX_VERTEX_UNIFORM_COMPONENTS = $8B4A;
    GL_MAX_VERTEX_UNIFORM_VECTORS = $8DFB;
    GL_MAX_VIEWPORT_DIMS = $0D3A;
    GL_MINOR_VERSION = $821C;
    GL_MIN_PROGRAM_TEXEL_OFFSET = $8904;
    GL_NUM_COMPRESSED_TEXTURE_FORMATS = $86A2;
    GL_NUM_EXTENSIONS = $821D;
    GL_NUM_PROGRAM_BINARY_FORMATS = $87FE;
    GL_NUM_SHADER_BINARY_FORMATS = $8DF9;
    GL_PACK_ALIGNMENT = $0D05;
    GL_PACK_ROW_LENGTH = $0D02;
    GL_PACK_SKIP_PIXELS = $0D04;
    GL_PACK_SKIP_ROWS = $0D03;
    GL_PIXEL_PACK_BUFFER_BINDING = $88ED;
    GL_PIXEL_UNPACK_BUFFER_BINDING = $88EF;
    GL_POLYGON_OFFSET_FACTOR = $8038;
    GL_POLYGON_OFFSET_UNITS = $2A00;
    GL_PROGRAM_BINARY_FORMATS = $87FF;
    GL_PROGRAM_PIPELINE_BINDING = $825A;
    GL_READ_BUFFER = $0C02;
    GL_READ_FRAMEBUFFER_BINDING = $8CAA;
    GL_RED_BITS = $0D52;
    GL_RENDERBUFFER_BINDING = $8CA7;
    GL_SAMPLER_BINDING = $8919;
    GL_SAMPLES = $80A9;
    GL_SAMPLE_BUFFERS = $80A8;
    GL_SAMPLE_COVERAGE_INVERT = $80AB;
    GL_SAMPLE_COVERAGE_VALUE = $80AA;
    GL_SCISSOR_BOX = $0C10;   // api(gles2) //extension: GL_NV_viewport_array
    GL_SHADER_COMPILER = $8DFA;
    GL_SHADER_STORAGE_BUFFER_BINDING = $90D3;
    GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = $90DF;
    GL_SHADER_STORAGE_BUFFER_SIZE = $90D5;
    GL_SHADER_STORAGE_BUFFER_START = $90D4;
    GL_STENCIL_BACK_FAIL = $8801;
    GL_STENCIL_BACK_FUNC = $8800;
    GL_STENCIL_BACK_PASS_DEPTH_FAIL = $8802;
    GL_STENCIL_BACK_PASS_DEPTH_PASS = $8803;
    GL_STENCIL_BACK_REF = $8CA3;
    GL_STENCIL_BACK_VALUE_MASK = $8CA4;
    GL_STENCIL_BACK_WRITEMASK = $8CA5;
    GL_STENCIL_BITS = $0D57;
    GL_STENCIL_CLEAR_VALUE = $0B91;
    GL_STENCIL_FAIL = $0B94;
    GL_STENCIL_FUNC = $0B92;
    GL_STENCIL_PASS_DEPTH_FAIL = $0B95;
    GL_STENCIL_PASS_DEPTH_PASS = $0B96;
    GL_STENCIL_REF = $0B97;
    GL_STENCIL_VALUE_MASK = $0B93;
    GL_STENCIL_WRITEMASK = $0B98;
    GL_SUBPIXEL_BITS = $0D50;
    GL_TEXTURE_BINDING_2D = $8069;
    GL_TEXTURE_BINDING_2D_ARRAY = $8C1D;
    GL_TEXTURE_BINDING_2D_MULTISAMPLE = $9104;
    GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY = $9105;
    GL_TEXTURE_BINDING_3D = $806A;
    GL_TEXTURE_BINDING_BUFFER = $8C2C;
    GL_TEXTURE_BINDING_CUBE_MAP = $8514;
    GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT = $919F;
    GL_TRANSFORM_FEEDBACK_BUFFER_BINDING = $8C8F;
    GL_TRANSFORM_FEEDBACK_BUFFER_SIZE = $8C85;
    GL_TRANSFORM_FEEDBACK_BUFFER_START = $8C84;
    GL_UNIFORM_BUFFER_BINDING = $8A28;
    GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = $8A34;
    GL_UNIFORM_BUFFER_SIZE = $8A2A;
    GL_UNIFORM_BUFFER_START = $8A29;
    GL_UNPACK_ALIGNMENT = $0CF5;
    GL_UNPACK_IMAGE_HEIGHT = $806E;
    GL_UNPACK_ROW_LENGTH = $0CF2;
    GL_UNPACK_SKIP_IMAGES = $806D;
    GL_UNPACK_SKIP_PIXELS = $0CF4;
    GL_UNPACK_SKIP_ROWS = $0CF3;
    GL_VERTEX_ARRAY_BINDING = $85B5;
    GL_VERTEX_BINDING_DIVISOR = $82D6;
    GL_VERTEX_BINDING_OFFSET = $82D7;
    GL_VERTEX_BINDING_STRIDE = $82D8;
    GL_VIEWPORT = $0BA2;   // api(gles2) //extension: GL_NV_viewport_array
 
    //... GetPointervPName ....
    GL_DEBUG_CALLBACK_FUNCTION = $8244;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_CALLBACK_USER_PARAM = $8245;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
 
    //... GetTextureParameter ....
    GL_TEXTURE_ALPHA_SIZE = $805F;
    GL_TEXTURE_BLUE_SIZE = $805E;
    GL_TEXTURE_BORDER_COLOR = $1004;
    GL_TEXTURE_GREEN_SIZE = $805D;
    GL_TEXTURE_HEIGHT = $1001;
    GL_TEXTURE_INTERNAL_FORMAT = $1003;
    GL_TEXTURE_MAG_FILTER = $2800;
    GL_TEXTURE_MIN_FILTER = $2801;
    GL_TEXTURE_RED_SIZE = $805C;
    GL_TEXTURE_WIDTH = $1000;
    GL_TEXTURE_WRAP_S = $2802;
    GL_TEXTURE_WRAP_T = $2803;
 
    //... HintMode ....
    GL_DONT_CARE = $1100;
    GL_FASTEST = $1101;
    GL_NICEST = $1102;
 
    //... HintTarget ....
    GL_GENERATE_MIPMAP_HINT = $8192;
    GL_PROGRAM_BINARY_RETRIEVABLE_HINT = $8257;
 
    //... LightEnvModeSGIX ....
    GL_REPLACE = $1E01;
 
    //... LogicOp ....
    GL_INVERT = $150A;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
 
    //... MapBufferAccessMask ....
    GL_MAP_FLUSH_EXPLICIT_BIT = $0010;
    GL_MAP_INVALIDATE_BUFFER_BIT = $0008;
    GL_MAP_INVALIDATE_RANGE_BIT = $0004;
    GL_MAP_UNSYNCHRONIZED_BIT = $0020;
 
    //... MatrixMode ....
    GL_TEXTURE = $1702;
 
    //... MemoryBarrierMask ....
    GL_ALL_BARRIER_BITS = $FFFFFFFF;
    GL_ATOMIC_COUNTER_BARRIER_BIT = $00001000;
    GL_BUFFER_UPDATE_BARRIER_BIT = $00000200;
    GL_COMMAND_BARRIER_BIT = $00000040;
    GL_ELEMENT_ARRAY_BARRIER_BIT = $00000002;
    GL_FRAMEBUFFER_BARRIER_BIT = $00000400;
    GL_PIXEL_BUFFER_BARRIER_BIT = $00000080;
    GL_SHADER_IMAGE_ACCESS_BARRIER_BIT = $00000020;
    GL_SHADER_STORAGE_BARRIER_BIT = $00002000;
    GL_TEXTURE_FETCH_BARRIER_BIT = $00000008;
    GL_TEXTURE_UPDATE_BARRIER_BIT = $00000100;
    GL_TRANSFORM_FEEDBACK_BARRIER_BIT = $00000800;
    GL_UNIFORM_BARRIER_BIT = $00000004;
    GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT = $00000001;
 
    //... PixelCopyType ....
    GL_COLOR = $1800;
    GL_DEPTH = $1801;
    GL_STENCIL = $1802;
 
    //... PixelFormat ....
    GL_DEPTH_COMPONENT = $1902;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_DEPTH_STENCIL = $84F9;
    GL_GREEN = $1904;
    GL_RED = $1903;
    GL_RED_INTEGER = $8D94;
    GL_RG = $8227;
    GL_RG_INTEGER = $8228;
    GL_RGB_INTEGER = $8D98;
    GL_RGBA_INTEGER = $8D99;
    GL_STENCIL_INDEX = $1901;
 
    //... InternalFormat ....
    GL_R8 = $8229;
    GL_R8_SNORM = $8F94;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_R16F = $822D;
    GL_R32F = $822E;
    GL_R8I = $8231;
    GL_R16I = $8233;
    GL_R32I = $8235;
    GL_R8UI = $8232;
    GL_R16UI = $8234;
    GL_R32UI = $8236;
    GL_RG8 = $822B;
    GL_RG8_SNORM = $8F95;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_RG16F = $822F;
    GL_RG32F = $8230;
    GL_RG8I = $8237;
    GL_RG16I = $8239;
    GL_RG32I = $823B;
    GL_RG8UI = $8238;
    GL_RG16UI = $823A;
    GL_RG32UI = $823C;
    GL_RGB8 = $8051;
    GL_RGB8_SNORM = $8F96;
    GL_RGB16F = $881B;
    GL_RGB32F = $8815;
    GL_RGB8I = $8D8F;
    GL_RGB16I = $8D89;
    GL_RGB32I = $8D83;
    GL_RGB8UI = $8D7D;
    GL_RGB16UI = $8D77;
    GL_RGB32UI = $8D71;
    GL_SRGB = $8C40;
    GL_SRGB8 = $8C41;
    GL_SRGB8_ALPHA8 = $8C43;
    GL_R11F_G11F_B10F = $8C3A;
    GL_RGB9_E5 = $8C3D;
    GL_RGBA4 = $8056;
    GL_RGB5_A1 = $8057;
    GL_RGBA8 = $8058;
    GL_RGBA8_SNORM = $8F97;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_RGB10_A2 = $8059;
    GL_RGBA16F = $881A;
    GL_RGBA32F = $8814;
    GL_RGBA8I = $8D8E;
    GL_RGBA16I = $8D88;
    GL_RGBA32I = $8D82;
    GL_RGBA8UI = $8D7C;
    GL_RGBA16UI = $8D76;
    GL_RGBA32UI = $8D70;
    GL_RGB10_A2UI = $906F;
    GL_DEPTH_COMPONENT16 = $81A5;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_DEPTH_COMPONENT32F = $8CAC;
    GL_DEPTH24_STENCIL8 = $88F0;
    GL_DEPTH32F_STENCIL8 = $8CAD;
    GL_STENCIL_INDEX8 = $8D48;
    GL_COMPRESSED_R11_EAC = $9270;
    GL_COMPRESSED_SIGNED_R11_EAC = $9271;
    GL_COMPRESSED_RGB8_ETC2 = $9274;
    GL_COMPRESSED_SRGB8_ETC2 = $9275;
    GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = $9276;
    GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = $9277;
    GL_COMPRESSED_RGBA8_ETC2_EAC = $9278;
    GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = $9279;
    GL_COMPRESSED_RG11_EAC = $9272;
    GL_COMPRESSED_SIGNED_RG11_EAC = $9273;
    GL_COMPRESSED_RGBA_ASTC_4x4 = $93B0;
    GL_COMPRESSED_RGBA_ASTC_5x4 = $93B1;
    GL_COMPRESSED_RGBA_ASTC_5x5 = $93B2;
    GL_COMPRESSED_RGBA_ASTC_6x5 = $93B3;
    GL_COMPRESSED_RGBA_ASTC_6x6 = $93B4;
    GL_COMPRESSED_RGBA_ASTC_8x5 = $93B5;
    GL_COMPRESSED_RGBA_ASTC_8x6 = $93B6;
    GL_COMPRESSED_RGBA_ASTC_8x8 = $93B7;
    GL_COMPRESSED_RGBA_ASTC_10x10 = $93BB;
    GL_COMPRESSED_RGBA_ASTC_10x5 = $93B8;
    GL_COMPRESSED_RGBA_ASTC_10x6 = $93B9;
    GL_COMPRESSED_RGBA_ASTC_10x8 = $93BA;
    GL_COMPRESSED_RGBA_ASTC_12x10 = $93BC;
    GL_COMPRESSED_RGBA_ASTC_12x12 = $93BD;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4 = $93D0;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4 = $93D1;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5 = $93D2;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5 = $93D3;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6 = $93D4;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5 = $93D5;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6 = $93D6;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8 = $93D7;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10 = $93DB;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5 = $93D8;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6 = $93D9;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8 = $93DA;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10 = $93DC;
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12 = $93DD;
 
    //... PixelType ....
    GL_UNSIGNED_SHORT_4_4_4_4 = $8033;
    GL_UNSIGNED_SHORT_5_5_5_1 = $8034;
 
    //... PrimitiveType ....
    GL_LINES = $0001;
    GL_LINES_ADJACENCY = $000A;
    GL_LINE_LOOP = $0002;
    GL_LINE_STRIP = $0003;
    GL_LINE_STRIP_ADJACENCY = $000B;
    GL_PATCHES = $000E;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_POINTS = $0000;
    GL_QUADS = $0007;
    GL_TRIANGLES = $0004;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TRIANGLES_ADJACENCY = $000C;
    GL_TRIANGLE_FAN = $0006;
    GL_TRIANGLE_STRIP = $0005;
    GL_TRIANGLE_STRIP_ADJACENCY = $000D;
 
    //... StencilOp ....
    GL_DECR = $1E03;
    GL_DECR_WRAP = $8508;
    GL_INCR = $1E02;
    GL_INCR_WRAP = $8507;
    GL_KEEP = $1E00;
    GL_ZERO = 0;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
 
    //... StringName ....
    GL_EXTENSIONS = $1F03;
    GL_RENDERER = $1F01;
    GL_VENDOR = $1F00;
    GL_VERSION = $1F02;
    GL_SHADING_LANGUAGE_VERSION = $8B8C;
 
    //... SyncObjectMask ....
    GL_SYNC_FLUSH_COMMANDS_BIT = $00000001;
 
    //... TextureMagFilter ....
    GL_NEAREST = $2600;
 
    //... TextureMinFilter ....
    GL_LINEAR_MIPMAP_LINEAR = $2703;
    GL_LINEAR_MIPMAP_NEAREST = $2701;
    GL_NEAREST_MIPMAP_LINEAR = $2702;
    GL_NEAREST_MIPMAP_NEAREST = $2700;
 
    //... TextureParameterName ....
    GL_TEXTURE_WRAP_R = $8072;
    GL_TEXTURE_BASE_LEVEL = $813C;
    GL_TEXTURE_COMPARE_MODE = $884C;
    GL_TEXTURE_COMPARE_FUNC = $884D;
    GL_TEXTURE_MIN_LOD = $813A;
    GL_TEXTURE_MAX_LOD = $813B;
    GL_TEXTURE_MAX_LEVEL = $813D;
    GL_TEXTURE_SWIZZLE_R = $8E42;
    GL_TEXTURE_SWIZZLE_G = $8E43;
    GL_TEXTURE_SWIZZLE_B = $8E44;
    GL_TEXTURE_SWIZZLE_A = $8E45;
    GL_DEPTH_STENCIL_TEXTURE_MODE = $90EA;
 
    //... TextureTarget ....
    GL_TEXTURE_3D = $806F;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_TEXTURE_CUBE_MAP = $8513;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_TEXTURE_CUBE_MAP_POSITIVE_X = $8515;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X = $8516;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y = $8517;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = $8518;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z = $8519;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = $851A;
    GL_TEXTURE_CUBE_MAP_ARRAY = $9009;
    GL_TEXTURE_2D_ARRAY = $8C1A;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_TEXTURE_2D_MULTISAMPLE = $9100;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
    GL_TEXTURE_2D_MULTISAMPLE_ARRAY = $9102;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
 
    //... TextureWrapMode ....
    GL_CLAMP_TO_BORDER = $812D;
    GL_CLAMP_TO_EDGE = $812F;
    GL_REPEAT = $2901;
    GL_MIRRORED_REPEAT = $8370;
 
    //... UseProgramStageMask ....
    GL_VERTEX_SHADER_BIT = $00000001;
    GL_FRAGMENT_SHADER_BIT = $00000002;
    GL_GEOMETRY_SHADER_BIT = $00000004;
    GL_TESS_CONTROL_SHADER_BIT = $00000008;
    GL_TESS_EVALUATION_SHADER_BIT = $00000010;
    GL_COMPUTE_SHADER_BIT = $00000020;
    GL_ALL_SHADER_BITS = $FFFFFFFF;
 
    //... FramebufferAttachment ....
    GL_DEPTH_ATTACHMENT = $8D00;
    GL_DEPTH_STENCIL_ATTACHMENT = $821A;
    GL_STENCIL_ATTACHMENT = $8D20;
 
    //... RenderbufferTarget ....
    GL_RENDERBUFFER = $8D41;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
 
    //... FramebufferTarget ....
    GL_FRAMEBUFFER = $8D40;
    GL_DRAW_FRAMEBUFFER = $8CA9;
    GL_READ_FRAMEBUFFER = $8CA8;
 
    //... TextureUnit ....
    GL_TEXTURE0 = $84C0;
    GL_TEXTURE1 = $84C1;
    GL_TEXTURE2 = $84C2;
    GL_TEXTURE3 = $84C3;
    GL_TEXTURE4 = $84C4;
    GL_TEXTURE5 = $84C5;
    GL_TEXTURE6 = $84C6;
    GL_TEXTURE7 = $84C7;
    GL_TEXTURE8 = $84C8;
    GL_TEXTURE9 = $84C9;
    GL_TEXTURE10 = $84CA;
    GL_TEXTURE11 = $84CB;
    GL_TEXTURE12 = $84CC;
    GL_TEXTURE13 = $84CD;
    GL_TEXTURE14 = $84CE;
    GL_TEXTURE15 = $84CF;
    GL_TEXTURE16 = $84D0;
    GL_TEXTURE17 = $84D1;
    GL_TEXTURE18 = $84D2;
    GL_TEXTURE19 = $84D3;
    GL_TEXTURE20 = $84D4;
    GL_TEXTURE21 = $84D5;
    GL_TEXTURE22 = $84D6;
    GL_TEXTURE23 = $84D7;
    GL_TEXTURE24 = $84D8;
    GL_TEXTURE25 = $84D9;
    GL_TEXTURE26 = $84DA;
    GL_TEXTURE27 = $84DB;
    GL_TEXTURE28 = $84DC;
    GL_TEXTURE29 = $84DD;
    GL_TEXTURE30 = $84DE;
    GL_TEXTURE31 = $84DF;
 
    //... FramebufferStatus ....
    GL_FRAMEBUFFER_COMPLETE = $8CD5;
    GL_FRAMEBUFFER_UNDEFINED = $8219;
    GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = $8CD6;
    GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = $8CD7;
    GL_FRAMEBUFFER_UNSUPPORTED = $8CDD;
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE = $8D56;
    GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS = $8DA8;
 
    //... GraphicsResetStatus ....
    GL_GUILTY_CONTEXT_RESET = $8253;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_INNOCENT_CONTEXT_RESET = $8254;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_UNKNOWN_CONTEXT_RESET = $8255;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
 
    //... SyncStatus ....
    GL_ALREADY_SIGNALED = $911A;
    GL_TIMEOUT_EXPIRED = $911B;
    GL_CONDITION_SATISFIED = $911C;
    GL_WAIT_FAILED = $911D;
 
    //... QueryTarget ....
    GL_ANY_SAMPLES_PASSED = $8C2F;
    GL_ANY_SAMPLES_PASSED_CONSERVATIVE = $8D6A;
    GL_PRIMITIVES_GENERATED = $8C87;
    GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN = $8C88;
 
    //... RenderbufferParameterName ....
    GL_RENDERBUFFER_WIDTH = $8D42;
    GL_RENDERBUFFER_HEIGHT = $8D43;
    GL_RENDERBUFFER_INTERNAL_FORMAT = $8D44;
    GL_RENDERBUFFER_SAMPLES = $8CAB;
    GL_RENDERBUFFER_RED_SIZE = $8D50;
    GL_RENDERBUFFER_GREEN_SIZE = $8D51;
    GL_RENDERBUFFER_BLUE_SIZE = $8D52;
    GL_RENDERBUFFER_ALPHA_SIZE = $8D53;
    GL_RENDERBUFFER_DEPTH_SIZE = $8D54;
    GL_RENDERBUFFER_STENCIL_SIZE = $8D55;
 
    //... FramebufferParameterName ....
    GL_FRAMEBUFFER_DEFAULT_WIDTH = $9310;
    GL_FRAMEBUFFER_DEFAULT_HEIGHT = $9311;
    GL_FRAMEBUFFER_DEFAULT_LAYERS = $9312;
    GL_FRAMEBUFFER_DEFAULT_SAMPLES = $9313;
    GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS = $9314;
 
    //... ProgramParameterPName ....
    GL_PROGRAM_SEPARABLE = $8258;
 
    //... BlendingFactor ....
    GL_ONE = 1;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_SRC_COLOR = $0300;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_ONE_MINUS_SRC_COLOR = $0301;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_DST_COLOR = $0306;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_ONE_MINUS_DST_COLOR = $0307;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_SRC_ALPHA = $0302;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_ONE_MINUS_SRC_ALPHA = $0303;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_DST_ALPHA = $0304;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_ONE_MINUS_DST_ALPHA = $0305;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_CONSTANT_COLOR = $8001;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_ONE_MINUS_CONSTANT_COLOR = $8002;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_CONSTANT_ALPHA = $8003;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_ONE_MINUS_CONSTANT_ALPHA = $8004;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
    GL_SRC_ALPHA_SATURATE = $0308;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
 
    //... BindTransformFeedbackTarget ....
    GL_TRANSFORM_FEEDBACK = $8E22;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
 
    //... ShaderType ....
    GL_COMPUTE_SHADER = $91B9;
    GL_VERTEX_SHADER = $8B31;
    GL_TESS_CONTROL_SHADER = $8E88;
    GL_TESS_EVALUATION_SHADER = $8E87;
    GL_GEOMETRY_SHADER = $8DD9;
    GL_FRAGMENT_SHADER = $8B30;
 
    //... DebugSource ....
    GL_DEBUG_SOURCE_API = $8246;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_WINDOW_SYSTEM = $8247;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_SHADER_COMPILER = $8248;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_THIRD_PARTY = $8249;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_APPLICATION = $824A;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_OTHER = $824B;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
 
    //... DebugType ....
    GL_DEBUG_TYPE_ERROR = $824C;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR = $824D;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR = $824E;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_PORTABILITY = $824F;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_PERFORMANCE = $8250;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_MARKER = $8268;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_PUSH_GROUP = $8269;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_POP_GROUP = $826A;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_OTHER = $8251;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
 
    //... DebugSeverity ....
    GL_DEBUG_SEVERITY_LOW = $9148;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_MEDIUM = $9147;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_HIGH = $9146;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_NOTIFICATION = $826B;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
 
    //... SyncCondition ....
    GL_SYNC_GPU_COMMANDS_COMPLETE = $9117;
 
    //... AtomicCounterBufferPName ....
    GL_ATOMIC_COUNTER_BUFFER_BINDING = $92C1;
 
    //... UniformBlockPName ....
    GL_UNIFORM_BLOCK_BINDING = $8A3F;
    GL_UNIFORM_BLOCK_DATA_SIZE = $8A40;
    GL_UNIFORM_BLOCK_NAME_LENGTH = $8A41;
    GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS = $8A42;
    GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES = $8A43;
    GL_UNIFORM_BLOCK_REFERENCED_BY_VERTEX_SHADER = $8A44;
    GL_UNIFORM_BLOCK_REFERENCED_BY_FRAGMENT_SHADER = $8A46;
 
    //... UniformPName ....
    GL_UNIFORM_TYPE = $8A37;
    GL_UNIFORM_SIZE = $8A38;
    GL_UNIFORM_NAME_LENGTH = $8A39;
    GL_UNIFORM_BLOCK_INDEX = $8A3A;
    GL_UNIFORM_OFFSET = $8A3B;
    GL_UNIFORM_ARRAY_STRIDE = $8A3C;
    GL_UNIFORM_MATRIX_STRIDE = $8A3D;
    GL_UNIFORM_IS_ROW_MAJOR = $8A3E;
 
    //... VertexProvokingMode ....
    GL_FIRST_VERTEX_CONVENTION = $8E4D;
    GL_LAST_VERTEX_CONVENTION = $8E4E;
 
    //... PatchParameterName ....
    GL_PATCH_VERTICES = $8E72;
 
    //... ObjectIdentifier ....
    GL_BUFFER = $82E0;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_SHADER = $82E1;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_PROGRAM = $82E2;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_QUERY = $82E3;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_PROGRAM_PIPELINE = $82E4;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_SAMPLER = $82E6;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
 
    //... TransformFeedbackPName ....
    GL_TRANSFORM_FEEDBACK_PAUSED = $8E23;
    GL_TRANSFORM_FEEDBACK_ACTIVE = $8E24;
 
    //... SyncParameterName ....
    GL_OBJECT_TYPE = $9112;
    GL_SYNC_STATUS = $9114;
    GL_SYNC_CONDITION = $9113;
    GL_SYNC_FLAGS = $9115;
 
    //... ShaderParameterName ....
    GL_SHADER_TYPE = $8B4F;
    GL_DELETE_STATUS = $8B80;
    GL_COMPILE_STATUS = $8B81;
    GL_INFO_LOG_LENGTH = $8B84;
    GL_SHADER_SOURCE_LENGTH = $8B88;
 
    //... QueryObjectParameterName ....
    GL_QUERY_RESULT_AVAILABLE = $8867;
    GL_QUERY_RESULT = $8866;
 
    //... QueryParameterName ....
    GL_CURRENT_QUERY = $8865;
 
    //... PipelineParameterName ....
    GL_ACTIVE_PROGRAM = $8259;
 
    //... ProgramInterface ....
    GL_UNIFORM = $92E1;
    GL_UNIFORM_BLOCK = $92E2;
    GL_PROGRAM_INPUT = $92E3;
    GL_PROGRAM_OUTPUT = $92E4;
    GL_TRANSFORM_FEEDBACK_VARYING = $92F4;
    GL_BUFFER_VARIABLE = $92E5;
    GL_SHADER_STORAGE_BLOCK = $92E6;
 
    //... VertexAttribType ....
    GL_FIXED = $140C;
    GL_HALF_FLOAT = $140B;
    GL_INT_2_10_10_10_REV = $8D9F;
    GL_UNSIGNED_INT_2_10_10_10_REV = $8368;
    GL_UNSIGNED_INT_10F_11F_11F_REV = $8C3B;
 
    //... AttributeType ....
    GL_FLOAT_VEC2 = $8B50;
    GL_FLOAT_VEC3 = $8B51;
    GL_FLOAT_VEC4 = $8B52;
    GL_INT_VEC2 = $8B53;
    GL_INT_VEC3 = $8B54;
    GL_INT_VEC4 = $8B55;
    GL_UNSIGNED_INT_VEC2 = $8DC6;
    GL_UNSIGNED_INT_VEC3 = $8DC7;
    GL_UNSIGNED_INT_VEC4 = $8DC8;
    GL_BOOL = $8B56;
    GL_BOOL_VEC2 = $8B57;
    GL_BOOL_VEC3 = $8B58;
    GL_BOOL_VEC4 = $8B59;
    GL_FLOAT_MAT2 = $8B5A;
    GL_FLOAT_MAT3 = $8B5B;
    GL_FLOAT_MAT4 = $8B5C;
    GL_FLOAT_MAT2x3 = $8B65;
    GL_FLOAT_MAT2x4 = $8B66;
    GL_FLOAT_MAT3x2 = $8B67;
    GL_FLOAT_MAT3x4 = $8B68;
    GL_FLOAT_MAT4x2 = $8B69;
    GL_FLOAT_MAT4x3 = $8B6A;
    GL_SAMPLER_2D = $8B5E;
    GL_SAMPLER_3D = $8B5F;
    GL_SAMPLER_CUBE = $8B60;
    GL_SAMPLER_2D_SHADOW = $8B62;
    GL_SAMPLER_CUBE_MAP_ARRAY = $900C;
    GL_SAMPLER_2D_ARRAY_SHADOW = $8DC4;
    GL_SAMPLER_2D_MULTISAMPLE = $9108;
    GL_SAMPLER_2D_MULTISAMPLE_ARRAY = $910B;
    GL_SAMPLER_CUBE_SHADOW = $8DC5;
    GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW = $900D;
    GL_SAMPLER_BUFFER = $8DC2;
    GL_INT_SAMPLER_2D = $8DCA;
    GL_INT_SAMPLER_3D = $8DCB;
    GL_INT_SAMPLER_CUBE = $8DCC;
    GL_INT_SAMPLER_2D_ARRAY = $8DCF;
    GL_INT_SAMPLER_CUBE_MAP_ARRAY = $900E;
    GL_INT_SAMPLER_2D_MULTISAMPLE = $9109;
    GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = $910C;
    GL_INT_SAMPLER_BUFFER = $8DD0;
    GL_UNSIGNED_INT_SAMPLER_2D = $8DD2;
    GL_UNSIGNED_INT_SAMPLER_3D = $8DD3;
    GL_UNSIGNED_INT_SAMPLER_CUBE = $8DD4;
    GL_UNSIGNED_INT_SAMPLER_2D_ARRAY = $8DD7;
    GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY = $900F;
    GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE = $910A;
    GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY = $910D;
    GL_UNSIGNED_INT_SAMPLER_BUFFER = $8DD8;
    GL_IMAGE_2D = $904D;
    GL_IMAGE_3D = $904E;
    GL_IMAGE_CUBE = $9050;
    GL_IMAGE_BUFFER = $9051;
    GL_IMAGE_2D_ARRAY = $9053;
    GL_IMAGE_CUBE_MAP_ARRAY = $9054;
    GL_INT_IMAGE_2D = $9058;
    GL_INT_IMAGE_3D = $9059;
    GL_INT_IMAGE_CUBE = $905B;
    GL_INT_IMAGE_BUFFER = $905C;
    GL_INT_IMAGE_2D_ARRAY = $905E;
    GL_INT_IMAGE_CUBE_MAP_ARRAY = $905F;
    GL_UNSIGNED_INT_IMAGE_2D = $9063;
    GL_UNSIGNED_INT_IMAGE_3D = $9064;
    GL_UNSIGNED_INT_IMAGE_CUBE = $9066;
    GL_UNSIGNED_INT_IMAGE_BUFFER = $9067;
    GL_UNSIGNED_INT_IMAGE_2D_ARRAY = $9069;
    GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY = $906A;
 
    //... UniformType ....
    GL_SAMPLER_2D_ARRAY = $8DC1;
 
    //... InternalFormatPName ....
    GL_NUM_SAMPLE_COUNTS = $9380;
    GL_IMAGE_FORMAT_COMPATIBILITY_TYPE = $90C7;
    GL_TEXTURE_COMPRESSED = $86A1;
 
    //... FramebufferAttachmentParameterName ....
    GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE = $8212;
    GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE = $8213;
    GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE = $8214;
    GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE = $8215;
    GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE = $8216;
    GL_FRAMEBUFFER_ATTACHMENT_STENCIL_SIZE = $8217;
    GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE = $8211;
    GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING = $8210;
    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = $8CD1;
    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = $8CD0;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = $8CD2;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = $8CD3;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER = $8CD4;
    GL_FRAMEBUFFER_ATTACHMENT_LAYERED = $8DA7;
 
    //... ProgramInterfacePName ....
    GL_ACTIVE_RESOURCES = $92F5;
    GL_MAX_NAME_LENGTH = $92F6;
    GL_MAX_NUM_ACTIVE_VARIABLES = $92F7;
 
    //... PrecisionType ....
    GL_LOW_FLOAT = $8DF0;
    GL_MEDIUM_FLOAT = $8DF1;
    GL_HIGH_FLOAT = $8DF2;
    GL_LOW_INT = $8DF3;
    GL_MEDIUM_INT = $8DF4;
    GL_HIGH_INT = $8DF5;
 
    //... ProgramPropertyARB ....
    GL_LINK_STATUS = $8B82;
    GL_VALIDATE_STATUS = $8B83;
    GL_ATTACHED_SHADERS = $8B85;
    GL_ACTIVE_ATOMIC_COUNTER_BUFFERS = $92D9;
    GL_ACTIVE_ATTRIBUTES = $8B89;
    GL_ACTIVE_ATTRIBUTE_MAX_LENGTH = $8B8A;
    GL_ACTIVE_UNIFORMS = $8B86;
    GL_ACTIVE_UNIFORM_BLOCKS = $8A36;
    GL_ACTIVE_UNIFORM_BLOCK_MAX_NAME_LENGTH = $8A35;
    GL_ACTIVE_UNIFORM_MAX_LENGTH = $8B87;
    GL_COMPUTE_WORK_GROUP_SIZE = $8267;
    GL_PROGRAM_BINARY_LENGTH = $8741;
    GL_TRANSFORM_FEEDBACK_BUFFER_MODE = $8C7F;
    GL_TRANSFORM_FEEDBACK_VARYINGS = $8C83;
    GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH = $8C76;
    GL_GEOMETRY_VERTICES_OUT = $8916;
    GL_GEOMETRY_INPUT_TYPE = $8917;
    GL_GEOMETRY_OUTPUT_TYPE = $8918;
 
    //... GlslTypeToken ....
    GL_UNSIGNED_INT_ATOMIC_COUNTER = $92DB;
 
    //... TransformFeedbackBufferMode ....
    GL_INTERLEAVED_ATTRIBS = $8C8C;
    GL_SEPARATE_ATTRIBS = $8C8D;
 
    //... ProgramResourceProperty ....
    GL_ACTIVE_VARIABLES = $9305;
    GL_BUFFER_BINDING = $9302;
    GL_NUM_ACTIVE_VARIABLES = $9304;
    GL_ARRAY_SIZE = $92FB;
    GL_ARRAY_STRIDE = $92FE;
    GL_BLOCK_INDEX = $92FD;
    GL_IS_ROW_MAJOR = $9300;
    GL_MATRIX_STRIDE = $92FF;
    GL_ATOMIC_COUNTER_BUFFER_INDEX = $9301;
    GL_BUFFER_DATA_SIZE = $9303;
    GL_IS_PER_PATCH = $92E7;
    GL_LOCATION = $930E;
    GL_NAME_LENGTH = $92F9;
    GL_OFFSET = $92FC;
    GL_REFERENCED_BY_VERTEX_SHADER = $9306;
    GL_REFERENCED_BY_TESS_CONTROL_SHADER = $9307;
    GL_REFERENCED_BY_TESS_EVALUATION_SHADER = $9308;
    GL_REFERENCED_BY_GEOMETRY_SHADER = $9309;
    GL_REFERENCED_BY_FRAGMENT_SHADER = $930A;
    GL_REFERENCED_BY_COMPUTE_SHADER = $930B;
    GL_TOP_LEVEL_ARRAY_SIZE = $930C;
    GL_TOP_LEVEL_ARRAY_STRIDE = $930D;
    GL_TYPE = $92FA;
 
    //... TextureCompareMode ....
    GL_COMPARE_REF_TO_TEXTURE = $884E;
 
    //... Other ....
    GL_CURRENT_BIT = $00000001;
    GL_POINT_BIT = $00000002;
    GL_LINE_BIT = $00000004;
    GL_POLYGON_BIT = $00000008;
    GL_POLYGON_STIPPLE_BIT = $00000010;
    GL_PIXEL_MODE_BIT = $00000020;
    GL_LIGHTING_BIT = $00000040;
    GL_FOG_BIT = $00000080;
    GL_ACCUM_BUFFER_BIT = $00000200;
    GL_VIEWPORT_BIT = $00000800;
    GL_TRANSFORM_BIT = $00001000;
    GL_ENABLE_BIT = $00002000;
    GL_HINT_BIT = $00008000;
    GL_EVAL_BIT = $00010000;
    GL_LIST_BIT = $00020000;
    GL_TEXTURE_BIT = $00040000;
    GL_SCISSOR_BIT = $00080000;
    GL_MULTISAMPLE_BIT = $20000000;
    GL_MULTISAMPLE_BIT_ARB = $20000000;
    GL_MULTISAMPLE_BIT_EXT = $20000000;
    GL_MULTISAMPLE_BIT_3DFX = $20000000;
    GL_ALL_ATTRIB_BITS = $FFFFFFFF;  //->Guaranteed to mark all attribute groups at once
    GL_DYNAMIC_STORAGE_BIT = $0100;
    GL_DYNAMIC_STORAGE_BIT_EXT = $0100;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_CLIENT_STORAGE_BIT = $0200;
    GL_CLIENT_STORAGE_BIT_EXT = $0200;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_SPARSE_STORAGE_BIT_ARB = $0400;
    GL_LGPU_SEPARATE_STORAGE_BIT_NVX = $0800;
    GL_PER_GPU_STORAGE_BIT_NV = $0800;
    GL_EXTERNAL_STORAGE_BIT_NVX = $2000;
    GL_COVERAGE_BUFFER_BIT_NV = $00008000;   // api(gles2) //extension: GL_NV_coverage_sample  //->Collides with AttribMask bit GL_HINT_BIT. OK since this token is for OpenGL ES 2, which doesn't have attribute groups.
    GL_CLIENT_PIXEL_STORE_BIT = $00000001;
    GL_CLIENT_VERTEX_ARRAY_BIT = $00000002;
    GL_CLIENT_ALL_ATTRIB_BITS = $FFFFFFFF;
    GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT = $00000001;
    GL_CONTEXT_FLAG_DEBUG_BIT_KHR = $00000002;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT_ARB = $00000004;
    GL_CONTEXT_FLAG_NO_ERROR_BIT = $00000008;
    GL_CONTEXT_FLAG_NO_ERROR_BIT_KHR = $00000008;   // api(gl|glcore|gles2) //extension: GL_KHR_no_error
    GL_CONTEXT_FLAG_PROTECTED_CONTENT_BIT_EXT = $00000010;   // api(gles2) //extension: GL_EXT_protected_textures
    GL_CONTEXT_CORE_PROFILE_BIT = $00000001;
    GL_CONTEXT_COMPATIBILITY_PROFILE_BIT = $00000002;
    GL_MAP_READ_BIT_EXT = $0001;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
    GL_MAP_WRITE_BIT_EXT = $0002;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
    GL_MAP_INVALIDATE_RANGE_BIT_EXT = $0004;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
    GL_MAP_INVALIDATE_BUFFER_BIT_EXT = $0008;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
    GL_MAP_FLUSH_EXPLICIT_BIT_EXT = $0010;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
    GL_MAP_UNSYNCHRONIZED_BIT_EXT = $0020;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
    GL_MAP_PERSISTENT_BIT = $0040;
    GL_MAP_PERSISTENT_BIT_EXT = $0040;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_MAP_COHERENT_BIT = $0080;
    GL_MAP_COHERENT_BIT_EXT = $0080;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT_EXT = $00000001;
    GL_ELEMENT_ARRAY_BARRIER_BIT_EXT = $00000002;
    GL_UNIFORM_BARRIER_BIT_EXT = $00000004;
    GL_TEXTURE_FETCH_BARRIER_BIT_EXT = $00000008;
    GL_SHADER_GLOBAL_ACCESS_BARRIER_BIT_NV = $00000010;
    GL_SHADER_IMAGE_ACCESS_BARRIER_BIT_EXT = $00000020;
    GL_COMMAND_BARRIER_BIT_EXT = $00000040;
    GL_PIXEL_BUFFER_BARRIER_BIT_EXT = $00000080;
    GL_TEXTURE_UPDATE_BARRIER_BIT_EXT = $00000100;
    GL_BUFFER_UPDATE_BARRIER_BIT_EXT = $00000200;
    GL_FRAMEBUFFER_BARRIER_BIT_EXT = $00000400;
    GL_TRANSFORM_FEEDBACK_BARRIER_BIT_EXT = $00000800;
    GL_ATOMIC_COUNTER_BARRIER_BIT_EXT = $00001000;
    GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT = $00004000;
    GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT_EXT = $00004000;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_QUERY_BUFFER_BARRIER_BIT = $00008000;
    GL_ALL_BARRIER_BITS_EXT = $FFFFFFFF;
    GL_QUERY_DEPTH_PASS_EVENT_BIT_AMD = $00000001;
    GL_QUERY_DEPTH_FAIL_EVENT_BIT_AMD = $00000002;
    GL_QUERY_STENCIL_FAIL_EVENT_BIT_AMD = $00000004;
    GL_QUERY_DEPTH_BOUNDS_FAIL_EVENT_BIT_AMD = $00000008;
    GL_QUERY_ALL_EVENT_BITS_AMD = $FFFFFFFF;
    GL_SYNC_FLUSH_COMMANDS_BIT_APPLE = $00000001;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_VERTEX_SHADER_BIT_EXT = $00000001;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
    GL_FRAGMENT_SHADER_BIT_EXT = $00000002;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
    GL_GEOMETRY_SHADER_BIT_EXT = $00000004;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_GEOMETRY_SHADER_BIT_OES = $00000004;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_TESS_CONTROL_SHADER_BIT_EXT = $00000008;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_CONTROL_SHADER_BIT_OES = $00000008;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_EVALUATION_SHADER_BIT_EXT = $00000010;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_EVALUATION_SHADER_BIT_OES = $00000010;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MESH_SHADER_BIT_NV = $00000040;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_TASK_SHADER_BIT_NV = $00000080;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_ALL_SHADER_BITS_EXT = $FFFFFFFF;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
    GL_SUBGROUP_FEATURE_BASIC_BIT_KHR = $00000001;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_VOTE_BIT_KHR = $00000002;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_ARITHMETIC_BIT_KHR = $00000004;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_BALLOT_BIT_KHR = $00000008;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_SHUFFLE_BIT_KHR = $00000010;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_SHUFFLE_RELATIVE_BIT_KHR = $00000020;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_CLUSTERED_BIT_KHR = $00000040;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_QUAD_BIT_KHR = $00000080;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_FEATURE_PARTITIONED_BIT_NV = $00000100;   // api(gl|glcore|gles2) //extension: GL_NV_shader_subgroup_partitioned
    GL_TEXTURE_STORAGE_SPARSE_BIT_AMD = $00000001;
    GL_RED_BIT_ATI = $00000001;
    GL_GREEN_BIT_ATI = $00000002;
    GL_BLUE_BIT_ATI = $00000004;
    GL_2X_BIT_ATI = $00000001;
    GL_4X_BIT_ATI = $00000002;
    GL_8X_BIT_ATI = $00000004;
    GL_HALF_BIT_ATI = $00000008;
    GL_QUARTER_BIT_ATI = $00000010;
    GL_EIGHTH_BIT_ATI = $00000020;
    GL_SATURATE_BIT_ATI = $00000040;
    GL_COMP_BIT_ATI = $00000002;
    GL_NEGATE_BIT_ATI = $00000004;
    GL_BIAS_BIT_ATI = $00000008;
    GL_TRACE_OPERATIONS_BIT_MESA = $0001;
    GL_TRACE_PRIMITIVES_BIT_MESA = $0002;
    GL_TRACE_ARRAYS_BIT_MESA = $0004;
    GL_TRACE_TEXTURES_BIT_MESA = $0008;
    GL_TRACE_PIXELS_BIT_MESA = $0010;
    GL_TRACE_ERRORS_BIT_MESA = $0020;
    GL_TRACE_ALL_BITS_MESA = $FFFF;
    GL_BOLD_BIT_NV = $01;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ITALIC_BIT_NV = $02;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_WIDTH_BIT_NV = $01;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_HEIGHT_BIT_NV = $02;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_HORIZONTAL_BEARING_X_BIT_NV = $04;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_HORIZONTAL_BEARING_Y_BIT_NV = $08;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_HORIZONTAL_BEARING_ADVANCE_BIT_NV = $10;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_VERTICAL_BEARING_X_BIT_NV = $20;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_VERTICAL_BEARING_Y_BIT_NV = $40;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_VERTICAL_BEARING_ADVANCE_BIT_NV = $80;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_GLYPH_HAS_KERNING_BIT_NV = $100;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_X_MIN_BOUNDS_BIT_NV = $00010000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_Y_MIN_BOUNDS_BIT_NV = $00020000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_X_MAX_BOUNDS_BIT_NV = $00040000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_Y_MAX_BOUNDS_BIT_NV = $00080000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_UNITS_PER_EM_BIT_NV = $00100000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_ASCENDER_BIT_NV = $00200000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_DESCENDER_BIT_NV = $00400000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_HEIGHT_BIT_NV = $00800000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_MAX_ADVANCE_WIDTH_BIT_NV = $01000000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_MAX_ADVANCE_HEIGHT_BIT_NV = $02000000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_UNDERLINE_POSITION_BIT_NV = $04000000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_UNDERLINE_THICKNESS_BIT_NV = $08000000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_HAS_KERNING_BIT_NV = $10000000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_NUM_GLYPH_INDICES_BIT_NV = $20000000;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PERFQUERY_SINGLE_CONTEXT_INTEL = $00000000;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_GLOBAL_CONTEXT_INTEL = $00000001;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_VERTEX23_BIT_PGI = $00000004;
    GL_VERTEX4_BIT_PGI = $00000008;
    GL_COLOR3_BIT_PGI = $00010000;
    GL_COLOR4_BIT_PGI = $00020000;
    GL_EDGEFLAG_BIT_PGI = $00040000;
    GL_INDEX_BIT_PGI = $00080000;
    GL_MAT_AMBIENT_BIT_PGI = $00100000;
    GL_MAT_AMBIENT_AND_DIFFUSE_BIT_PGI = $00200000;
    GL_MAT_DIFFUSE_BIT_PGI = $00400000;
    GL_MAT_EMISSION_BIT_PGI = $00800000;
    GL_MAT_COLOR_INDEXES_BIT_PGI = $01000000;
    GL_MAT_SHININESS_BIT_PGI = $02000000;
    GL_MAT_SPECULAR_BIT_PGI = $04000000;
    GL_NORMAL_BIT_PGI = $08000000;
    GL_TEXCOORD1_BIT_PGI = $10000000;
    GL_TEXCOORD2_BIT_PGI = $20000000;
    GL_TEXCOORD3_BIT_PGI = $40000000;
    GL_TEXCOORD4_BIT_PGI = $80000000;
    GL_COLOR_BUFFER_BIT0_QCOM = $00000001;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT1_QCOM = $00000002;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT2_QCOM = $00000004;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT3_QCOM = $00000008;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT4_QCOM = $00000010;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT5_QCOM = $00000020;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT6_QCOM = $00000040;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_COLOR_BUFFER_BIT7_QCOM = $00000080;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT0_QCOM = $00000100;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT1_QCOM = $00000200;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT2_QCOM = $00000400;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT3_QCOM = $00000800;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT4_QCOM = $00001000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT5_QCOM = $00002000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT6_QCOM = $00004000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_DEPTH_BUFFER_BIT7_QCOM = $00008000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT0_QCOM = $00010000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT1_QCOM = $00020000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT2_QCOM = $00040000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT3_QCOM = $00080000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT4_QCOM = $00100000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT5_QCOM = $00200000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT6_QCOM = $00400000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_STENCIL_BUFFER_BIT7_QCOM = $00800000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT0_QCOM = $01000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT1_QCOM = $02000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT2_QCOM = $04000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT3_QCOM = $08000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT4_QCOM = $10000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT5_QCOM = $20000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT6_QCOM = $40000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_MULTISAMPLE_BUFFER_BIT7_QCOM = $80000000;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
    GL_FOVEATION_ENABLE_BIT_QCOM = $00000001;   // api(gles2) //extension: GL_QCOM_framebuffer_foveated
    GL_FOVEATION_SCALED_BIN_METHOD_BIT_QCOM = $00000002;   // api(gles2) //extension: GL_QCOM_framebuffer_foveated
    GL_FOVEATION_SUBSAMPLED_LAYOUT_METHOD_BIT_QCOM = $00000004;   // api(gles2) //extension: GL_QCOM_texture_foveated_subsampled_layout
    GL_TEXTURE_DEFORMATION_BIT_SGIX = $00000001;
    GL_GEOMETRY_DEFORMATION_BIT_SGIX = $00000002;
    GL_TERMINATE_SEQUENCE_COMMAND_NV = $0000;
    GL_NOP_COMMAND_NV = $0001;
    GL_DRAW_ELEMENTS_COMMAND_NV = $0002;
    GL_DRAW_ARRAYS_COMMAND_NV = $0003;
    GL_DRAW_ELEMENTS_STRIP_COMMAND_NV = $0004;
    GL_DRAW_ARRAYS_STRIP_COMMAND_NV = $0005;
    GL_DRAW_ELEMENTS_INSTANCED_COMMAND_NV = $0006;
    GL_DRAW_ARRAYS_INSTANCED_COMMAND_NV = $0007;
    GL_ELEMENT_ADDRESS_COMMAND_NV = $0008;
    GL_ATTRIBUTE_ADDRESS_COMMAND_NV = $0009;
    GL_UNIFORM_ADDRESS_COMMAND_NV = $000A;
    GL_BLEND_COLOR_COMMAND_NV = $000B;
    GL_STENCIL_REF_COMMAND_NV = $000C;
    GL_LINE_WIDTH_COMMAND_NV = $000D;
    GL_POLYGON_OFFSET_COMMAND_NV = $000E;
    GL_ALPHA_REF_COMMAND_NV = $000F;
    GL_VIEWPORT_COMMAND_NV = $0010;
    GL_SCISSOR_COMMAND_NV = $0011;
    GL_FRONT_FACE_COMMAND_NV = $0012;
    GL_LAYOUT_DEFAULT_INTEL = 0;
    GL_LAYOUT_LINEAR_INTEL = 1;
    GL_LAYOUT_LINEAR_CPU_CACHED_INTEL = 2;
    GL_CLOSE_PATH_NV = $00;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MOVE_TO_NV = $02;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_MOVE_TO_NV = $03;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_LINE_TO_NV = $04;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_LINE_TO_NV = $05;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_HORIZONTAL_LINE_TO_NV = $06;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_HORIZONTAL_LINE_TO_NV = $07;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_VERTICAL_LINE_TO_NV = $08;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_VERTICAL_LINE_TO_NV = $09;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_QUADRATIC_CURVE_TO_NV = $0A;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_QUADRATIC_CURVE_TO_NV = $0B;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_CUBIC_CURVE_TO_NV = $0C;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_CUBIC_CURVE_TO_NV = $0D;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SMOOTH_QUADRATIC_CURVE_TO_NV = $0E;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_SMOOTH_QUADRATIC_CURVE_TO_NV = $0F;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SMOOTH_CUBIC_CURVE_TO_NV = $10;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_SMOOTH_CUBIC_CURVE_TO_NV = $11;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SMALL_CCW_ARC_TO_NV = $12;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_SMALL_CCW_ARC_TO_NV = $13;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SMALL_CW_ARC_TO_NV = $14;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_SMALL_CW_ARC_TO_NV = $15;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_LARGE_CCW_ARC_TO_NV = $16;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_LARGE_CCW_ARC_TO_NV = $17;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_LARGE_CW_ARC_TO_NV = $18;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_LARGE_CW_ARC_TO_NV = $19;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_CONIC_CURVE_TO_NV = $1A;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_CONIC_CURVE_TO_NV = $1B;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SHARED_EDGE_NV = $C0;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering_shared_edge
    GL_ROUNDED_RECT_NV = $E8;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_ROUNDED_RECT_NV = $E9;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ROUNDED_RECT2_NV = $EA;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_ROUNDED_RECT2_NV = $EB;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ROUNDED_RECT4_NV = $EC;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_ROUNDED_RECT4_NV = $ED;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ROUNDED_RECT8_NV = $EE;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_ROUNDED_RECT8_NV = $EF;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RESTART_PATH_NV = $F0;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_DUP_FIRST_CUBIC_CURVE_TO_NV = $F2;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_DUP_LAST_CUBIC_CURVE_TO_NV = $F4;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RECT_NV = $F6;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_RECT_NV = $F7;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_CIRCULAR_CCW_ARC_TO_NV = $F8;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_CIRCULAR_CW_ARC_TO_NV = $FA;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_CIRCULAR_TANGENT_ARC_TO_NV = $FC;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ARC_TO_NV = $FE;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_RELATIVE_ARC_TO_NV = $FF;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_NEXT_BUFFER_NV = -2;
    GL_SKIP_COMPONENTS4_NV = -3;
    GL_SKIP_COMPONENTS3_NV = -4;
    GL_SKIP_COMPONENTS2_NV = -5;
    GL_SKIP_COMPONENTS1_NV = -6;
    GL_RESTART_SUN = $0001;
    GL_REPLACE_MIDDLE_SUN = $0002;
    GL_REPLACE_OLDEST_SUN = $0003;
    GL_NONE_OES = 0;
    GL_INVALID_INDEX = $FFFFFFFF;  //->Tagged as uint
    GL_ALL_PIXELS_AMD = $FFFFFFFF;
    GL_TIMEOUT_IGNORED = $FFFFFFFFFFFFFFFF;  //->Tagged as uint64
    GL_TIMEOUT_IGNORED_APPLE = $FFFFFFFFFFFFFFFF;   // api(gles1|gles2) //extension: GL_APPLE_sync  //->Tagged as uint64
    GL_VERSION_ES_CL_1_0 = 1;  //->Not an API enum. API definition macro for ES 1.0/1.1 headers
    GL_VERSION_ES_CM_1_1 = 1;  //->Not an API enum. API definition macro for ES 1.0/1.1 headers
    GL_VERSION_ES_CL_1_1 = 1;  //->Not an API enum. API definition macro for ES 1.0/1.1 headers
    GL_UUID_SIZE_EXT = 16;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_LUID_SIZE_EXT = 8;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_QUADS_EXT = $0007;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_QUADS_OES = $0007;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_QUAD_STRIP = $0008;
    GL_POLYGON = $0009;
    GL_LINES_ADJACENCY_ARB = $000A;
    GL_LINES_ADJACENCY_EXT = $000A;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_LINES_ADJACENCY_OES = $000A;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_LINE_STRIP_ADJACENCY_ARB = $000B;
    GL_LINE_STRIP_ADJACENCY_EXT = $000B;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_LINE_STRIP_ADJACENCY_OES = $000B;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_TRIANGLES_ADJACENCY_ARB = $000C;
    GL_TRIANGLES_ADJACENCY_EXT = $000C;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_TRIANGLES_ADJACENCY_OES = $000C;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_TRIANGLE_STRIP_ADJACENCY_ARB = $000D;
    GL_TRIANGLE_STRIP_ADJACENCY_EXT = $000D;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_TRIANGLE_STRIP_ADJACENCY_OES = $000D;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_PATCHES_EXT = $000E;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_PATCHES_OES = $000E;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_ACCUM = $0100;
    GL_LOAD = $0101;
    GL_RETURN = $0102;
    GL_MULT = $0103;
    GL_ADD = $0104;
    GL_SRC_ALPHA_SATURATE_EXT = $0308;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_FRONT_LEFT = $0400;
    GL_FRONT_RIGHT = $0401;
    GL_BACK_LEFT = $0402;
    GL_BACK_RIGHT = $0403;
    GL_LEFT = $0406;
    GL_RIGHT = $0407;
    GL_AUX0 = $0409;
    GL_AUX1 = $040A;
    GL_AUX2 = $040B;
    GL_AUX3 = $040C;
    GL_STACK_OVERFLOW_KHR = $0503;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_STACK_UNDERFLOW_KHR = $0504;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_INVALID_FRAMEBUFFER_OPERATION_EXT = $0506;
    GL_INVALID_FRAMEBUFFER_OPERATION_OES = $0506;
    GL_CONTEXT_LOST = $0507;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_CONTEXT_LOST_KHR = $0507;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_2D = $0600;
    GL_3D = $0601;
    GL_3D_COLOR = $0602;
    GL_3D_COLOR_TEXTURE = $0603;
    GL_4D_COLOR_TEXTURE = $0604;
    GL_PASS_THROUGH_TOKEN = $0700;
    GL_POINT_TOKEN = $0701;
    GL_LINE_TOKEN = $0702;
    GL_POLYGON_TOKEN = $0703;
    GL_BITMAP_TOKEN = $0704;
    GL_DRAW_PIXEL_TOKEN = $0705;
    GL_COPY_PIXEL_TOKEN = $0706;
    GL_LINE_RESET_TOKEN = $0707;
    GL_EXP = $0800;
    GL_EXP2 = $0801;
    GL_COEFF = $0A00;
    GL_ORDER = $0A01;
    GL_DOMAIN = $0A02;
    GL_CURRENT_COLOR = $0B00;
    GL_CURRENT_INDEX = $0B01;
    GL_CURRENT_NORMAL = $0B02;
    GL_CURRENT_TEXTURE_COORDS = $0B03;
    GL_CURRENT_RASTER_COLOR = $0B04;
    GL_CURRENT_RASTER_INDEX = $0B05;
    GL_CURRENT_RASTER_TEXTURE_COORDS = $0B06;
    GL_CURRENT_RASTER_POSITION = $0B07;
    GL_CURRENT_RASTER_POSITION_VALID = $0B08;
    GL_CURRENT_RASTER_DISTANCE = $0B09;
    GL_POINT_SMOOTH = $0B10;
    GL_POINT_SIZE = $0B11;
    GL_POINT_SIZE_RANGE = $0B12;
    GL_SMOOTH_POINT_SIZE_RANGE = $0B12;
    GL_POINT_SIZE_GRANULARITY = $0B13;
    GL_SMOOTH_POINT_SIZE_GRANULARITY = $0B13;
    GL_LINE_SMOOTH = $0B20;
    GL_LINE_WIDTH_RANGE = $0B22;
    GL_SMOOTH_LINE_WIDTH_RANGE = $0B22;
    GL_LINE_WIDTH_GRANULARITY = $0B23;
    GL_SMOOTH_LINE_WIDTH_GRANULARITY = $0B23;
    GL_LINE_STIPPLE = $0B24;
    GL_LINE_STIPPLE_PATTERN = $0B25;
    GL_LINE_STIPPLE_REPEAT = $0B26;
    GL_LIST_MODE = $0B30;
    GL_MAX_LIST_NESTING = $0B31;
    GL_LIST_BASE = $0B32;
    GL_LIST_INDEX = $0B33;
    GL_POLYGON_MODE = $0B40;
    GL_POLYGON_MODE_NV = $0B40;   // api(gles2) //extension: GL_NV_polygon_mode
    GL_POLYGON_SMOOTH = $0B41;
    GL_POLYGON_STIPPLE = $0B42;
    GL_EDGE_FLAG = $0B43;
    GL_LIGHTING = $0B50;
    GL_LIGHT_MODEL_LOCAL_VIEWER = $0B51;
    GL_LIGHT_MODEL_TWO_SIDE = $0B52;
    GL_LIGHT_MODEL_AMBIENT = $0B53;
    GL_SHADE_MODEL = $0B54;
    GL_COLOR_MATERIAL_FACE = $0B55;
    GL_COLOR_MATERIAL_PARAMETER = $0B56;
    GL_COLOR_MATERIAL = $0B57;
    GL_FOG = $0B60;
    GL_FOG_INDEX = $0B61;
    GL_FOG_DENSITY = $0B62;
    GL_FOG_START = $0B63;
    GL_FOG_END = $0B64;
    GL_FOG_MODE = $0B65;
    GL_FOG_COLOR = $0B66;
    GL_ACCUM_CLEAR_VALUE = $0B80;
    GL_MATRIX_MODE = $0BA0;
    GL_NORMALIZE = $0BA1;
    GL_MODELVIEW_STACK_DEPTH = $0BA3;
    GL_MODELVIEW0_STACK_DEPTH_EXT = $0BA3;
    GL_PATH_MODELVIEW_STACK_DEPTH_NV = $0BA3;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PROJECTION_STACK_DEPTH = $0BA4;
    GL_PATH_PROJECTION_STACK_DEPTH_NV = $0BA4;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TEXTURE_STACK_DEPTH = $0BA5;
    GL_MODELVIEW_MATRIX = $0BA6;
    GL_MODELVIEW0_MATRIX_EXT = $0BA6;
    GL_PATH_MODELVIEW_MATRIX_NV = $0BA6;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PROJECTION_MATRIX = $0BA7;
    GL_PATH_PROJECTION_MATRIX_NV = $0BA7;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TEXTURE_MATRIX = $0BA8;
    GL_ATTRIB_STACK_DEPTH = $0BB0;
    GL_CLIENT_ATTRIB_STACK_DEPTH = $0BB1;
    GL_ALPHA_TEST = $0BC0;
    GL_ALPHA_TEST_QCOM = $0BC0;   // api(gles2) //extension: GL_QCOM_alpha_test
    GL_ALPHA_TEST_FUNC = $0BC1;
    GL_ALPHA_TEST_FUNC_QCOM = $0BC1;   // api(gles2) //extension: GL_QCOM_alpha_test
    GL_ALPHA_TEST_REF = $0BC2;
    GL_ALPHA_TEST_REF_QCOM = $0BC2;   // api(gles2) //extension: GL_QCOM_alpha_test
    GL_BLEND_DST = $0BE0;
    GL_BLEND_SRC = $0BE1;
    GL_LOGIC_OP_MODE = $0BF0;
    GL_INDEX_LOGIC_OP = $0BF1;
    GL_LOGIC_OP = $0BF1;
    GL_COLOR_LOGIC_OP = $0BF2;
    GL_AUX_BUFFERS = $0C00;
    GL_DRAW_BUFFER = $0C01;
    GL_DRAW_BUFFER_EXT = $0C01;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
    GL_READ_BUFFER_EXT = $0C02;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
    GL_READ_BUFFER_NV = $0C02;   // api(gles2) //extension: GL_NV_read_buffer
    GL_INDEX_CLEAR_VALUE = $0C20;
    GL_INDEX_WRITEMASK = $0C21;
    GL_INDEX_MODE = $0C30;
    GL_RGBA_MODE = $0C31;
    GL_DOUBLEBUFFER = $0C32;
    GL_STEREO = $0C33;
    GL_RENDER_MODE = $0C40;
    GL_PERSPECTIVE_CORRECTION_HINT = $0C50;
    GL_POINT_SMOOTH_HINT = $0C51;
    GL_LINE_SMOOTH_HINT = $0C52;
    GL_POLYGON_SMOOTH_HINT = $0C53;
    GL_FOG_HINT = $0C54;
    GL_TEXTURE_GEN_S = $0C60;
    GL_TEXTURE_GEN_T = $0C61;
    GL_TEXTURE_GEN_R = $0C62;
    GL_TEXTURE_GEN_Q = $0C63;
    GL_PIXEL_MAP_I_TO_I = $0C70;
    GL_PIXEL_MAP_S_TO_S = $0C71;
    GL_PIXEL_MAP_I_TO_R = $0C72;
    GL_PIXEL_MAP_I_TO_G = $0C73;
    GL_PIXEL_MAP_I_TO_B = $0C74;
    GL_PIXEL_MAP_I_TO_A = $0C75;
    GL_PIXEL_MAP_R_TO_R = $0C76;
    GL_PIXEL_MAP_G_TO_G = $0C77;
    GL_PIXEL_MAP_B_TO_B = $0C78;
    GL_PIXEL_MAP_A_TO_A = $0C79;
    GL_PIXEL_MAP_I_TO_I_SIZE = $0CB0;
    GL_PIXEL_MAP_S_TO_S_SIZE = $0CB1;
    GL_PIXEL_MAP_I_TO_R_SIZE = $0CB2;
    GL_PIXEL_MAP_I_TO_G_SIZE = $0CB3;
    GL_PIXEL_MAP_I_TO_B_SIZE = $0CB4;
    GL_PIXEL_MAP_I_TO_A_SIZE = $0CB5;
    GL_PIXEL_MAP_R_TO_R_SIZE = $0CB6;
    GL_PIXEL_MAP_G_TO_G_SIZE = $0CB7;
    GL_PIXEL_MAP_B_TO_B_SIZE = $0CB8;
    GL_PIXEL_MAP_A_TO_A_SIZE = $0CB9;
    GL_UNPACK_SWAP_BYTES = $0CF0;
    GL_UNPACK_LSB_FIRST = $0CF1;
    GL_UNPACK_ROW_LENGTH_EXT = $0CF2;   // api(gles2) //extension: GL_EXT_unpack_subimage
    GL_UNPACK_SKIP_ROWS_EXT = $0CF3;   // api(gles2) //extension: GL_EXT_unpack_subimage
    GL_UNPACK_SKIP_PIXELS_EXT = $0CF4;   // api(gles2) //extension: GL_EXT_unpack_subimage
    GL_PACK_SWAP_BYTES = $0D00;
    GL_PACK_LSB_FIRST = $0D01;
    GL_MAP_COLOR = $0D10;
    GL_MAP_STENCIL = $0D11;
    GL_INDEX_SHIFT = $0D12;
    GL_INDEX_OFFSET = $0D13;
    GL_RED_SCALE = $0D14;
    GL_RED_BIAS = $0D15;
    GL_ZOOM_X = $0D16;
    GL_ZOOM_Y = $0D17;
    GL_GREEN_SCALE = $0D18;
    GL_GREEN_BIAS = $0D19;
    GL_BLUE_SCALE = $0D1A;
    GL_BLUE_BIAS = $0D1B;
    GL_ALPHA_SCALE = $0D1C;
    GL_ALPHA_BIAS = $0D1D;
    GL_DEPTH_SCALE = $0D1E;
    GL_DEPTH_BIAS = $0D1F;
    GL_MAX_EVAL_ORDER = $0D30;
    GL_MAX_LIGHTS = $0D31;
    GL_MAX_CLIP_PLANES = $0D32;
    GL_MAX_CLIP_PLANES_IMG = $0D32;
    GL_MAX_CLIP_DISTANCES = $0D32;
    GL_MAX_CLIP_DISTANCES_EXT = $0D32;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_MAX_CLIP_DISTANCES_APPLE = $0D32;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_MAX_PIXEL_MAP_TABLE = $0D34;
    GL_MAX_ATTRIB_STACK_DEPTH = $0D35;
    GL_MAX_MODELVIEW_STACK_DEPTH = $0D36;
    GL_PATH_MAX_MODELVIEW_STACK_DEPTH_NV = $0D36;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MAX_NAME_STACK_DEPTH = $0D37;
    GL_MAX_PROJECTION_STACK_DEPTH = $0D38;
    GL_PATH_MAX_PROJECTION_STACK_DEPTH_NV = $0D38;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MAX_TEXTURE_STACK_DEPTH = $0D39;
    GL_MAX_CLIENT_ATTRIB_STACK_DEPTH = $0D3B;
    GL_INDEX_BITS = $0D51;
    GL_ACCUM_RED_BITS = $0D58;
    GL_ACCUM_GREEN_BITS = $0D59;
    GL_ACCUM_BLUE_BITS = $0D5A;
    GL_ACCUM_ALPHA_BITS = $0D5B;
    GL_NAME_STACK_DEPTH = $0D70;
    GL_AUTO_NORMAL = $0D80;
    GL_MAP1_COLOR_4 = $0D90;
    GL_MAP1_INDEX = $0D91;
    GL_MAP1_NORMAL = $0D92;
    GL_MAP1_TEXTURE_COORD_1 = $0D93;
    GL_MAP1_TEXTURE_COORD_2 = $0D94;
    GL_MAP1_TEXTURE_COORD_3 = $0D95;
    GL_MAP1_TEXTURE_COORD_4 = $0D96;
    GL_MAP1_VERTEX_3 = $0D97;
    GL_MAP1_VERTEX_4 = $0D98;
    GL_MAP2_COLOR_4 = $0DB0;
    GL_MAP2_INDEX = $0DB1;
    GL_MAP2_NORMAL = $0DB2;
    GL_MAP2_TEXTURE_COORD_1 = $0DB3;
    GL_MAP2_TEXTURE_COORD_2 = $0DB4;
    GL_MAP2_TEXTURE_COORD_3 = $0DB5;
    GL_MAP2_TEXTURE_COORD_4 = $0DB6;
    GL_MAP2_VERTEX_3 = $0DB7;
    GL_MAP2_VERTEX_4 = $0DB8;
    GL_MAP1_GRID_DOMAIN = $0DD0;
    GL_MAP1_GRID_SEGMENTS = $0DD1;
    GL_MAP2_GRID_DOMAIN = $0DD2;
    GL_MAP2_GRID_SEGMENTS = $0DD3;
    GL_TEXTURE_1D = $0DE0;
    GL_FEEDBACK_BUFFER_POINTER = $0DF0;
    GL_FEEDBACK_BUFFER_SIZE = $0DF1;
    GL_FEEDBACK_BUFFER_TYPE = $0DF2;
    GL_SELECTION_BUFFER_POINTER = $0DF3;
    GL_SELECTION_BUFFER_SIZE = $0DF4;
    GL_TEXTURE_COMPONENTS = $1003;
    GL_TEXTURE_BORDER_COLOR_EXT = $1004;   // api(gles2) //extension: GL_EXT_texture_border_clamp
    GL_TEXTURE_BORDER_COLOR_NV = $1004;   // api(gles2) //extension: GL_NV_texture_border_clamp
    GL_TEXTURE_BORDER_COLOR_OES = $1004;   // api(gles2) //extension: GL_OES_texture_border_clamp
    GL_TEXTURE_BORDER = $1005;
    GL_TEXTURE_TARGET = $1006;
    GL_AMBIENT = $1200;
    GL_DIFFUSE = $1201;
    GL_SPECULAR = $1202;
    GL_POSITION = $1203;
    GL_SPOT_DIRECTION = $1204;
    GL_SPOT_EXPONENT = $1205;
    GL_SPOT_CUTOFF = $1206;
    GL_CONSTANT_ATTENUATION = $1207;
    GL_LINEAR_ATTENUATION = $1208;
    GL_QUADRATIC_ATTENUATION = $1209;
    GL_COMPILE = $1300;
    GL_COMPILE_AND_EXECUTE = $1301;
    GL_2_BYTES = $1407;
    GL_2_BYTES_NV = $1407;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_3_BYTES = $1408;
    GL_3_BYTES_NV = $1408;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_4_BYTES = $1409;
    GL_4_BYTES_NV = $1409;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_DOUBLE = $140A;
    GL_DOUBLE_EXT = $140A;
    GL_HALF_FLOAT_ARB = $140B;
    GL_HALF_FLOAT_NV = $140B;
    GL_HALF_APPLE = $140B;
    GL_FIXED_OES = $140C;
    GL_INT64_ARB = $140E;
    GL_INT64_NV = $140E;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT64_ARB = $140F;
    GL_UNSIGNED_INT64_NV = $140F;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_CLEAR = $1500;
    GL_AND = $1501;
    GL_AND_REVERSE = $1502;
    GL_COPY = $1503;
    GL_AND_INVERTED = $1504;
    GL_NOOP = $1505;
    GL_XOR = $1506;
    GL_XOR_NV = $1506;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_OR = $1507;
    GL_NOR = $1508;
    GL_EQUIV = $1509;
    GL_OR_REVERSE = $150B;
    GL_COPY_INVERTED = $150C;
    GL_OR_INVERTED = $150D;
    GL_NAND = $150E;
    GL_SET = $150F;
    GL_EMISSION = $1600;
    GL_SHININESS = $1601;
    GL_AMBIENT_AND_DIFFUSE = $1602;
    GL_COLOR_INDEXES = $1603;
    GL_MODELVIEW = $1700;
    GL_MODELVIEW0_ARB = $1700;
    GL_MODELVIEW0_EXT = $1700;
    GL_PATH_MODELVIEW_NV = $1700;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PROJECTION = $1701;
    GL_PATH_PROJECTION_NV = $1701;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_COLOR_EXT = $1800;   // api(gles1|gles2) //extension: GL_EXT_discard_framebuffer
    GL_DEPTH_EXT = $1801;   // api(gles1|gles2) //extension: GL_EXT_discard_framebuffer
    GL_STENCIL_EXT = $1802;   // api(gles1|gles2) //extension: GL_EXT_discard_framebuffer
    GL_COLOR_INDEX = $1900;
    GL_STENCIL_INDEX_OES = $1901;   // api(gles2) //extension: GL_OES_texture_stencil8
    GL_RED_EXT = $1903;   // api(gles2) //extension: GL_EXT_texture_rg
    GL_RED_NV = $1903;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_GREEN_NV = $1904;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_BLUE_NV = $1905;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_BITMAP = $1A00;
    GL_POINT = $1B00;
    GL_POINT_NV = $1B00;   // api(gles2) //extension: GL_NV_polygon_mode
    GL_LINE = $1B01;
    GL_LINE_NV = $1B01;   // api(gles2) //extension: GL_NV_polygon_mode
    GL_FILL = $1B02;
    GL_FILL_NV = $1B02;   // api(gles2) //extension: GL_NV_polygon_mode
    GL_RENDER = $1C00;
    GL_FEEDBACK = $1C01;
    GL_SELECT = $1C02;
    GL_FLAT = $1D00;
    GL_SMOOTH = $1D01;
    GL_S = $2000;
    GL_T = $2001;
    GL_R = $2002;
    GL_Q = $2003;
    GL_MODULATE = $2100;
    GL_DECAL = $2101;
    GL_TEXTURE_ENV_MODE = $2200;
    GL_TEXTURE_ENV_COLOR = $2201;
    GL_TEXTURE_ENV = $2300;
    GL_EYE_LINEAR = $2400;
    GL_EYE_LINEAR_NV = $2400;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_OBJECT_LINEAR = $2401;
    GL_OBJECT_LINEAR_NV = $2401;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SPHERE_MAP = $2402;
    GL_TEXTURE_GEN_MODE = $2500;
    GL_TEXTURE_GEN_MODE_OES = $2500;
    GL_OBJECT_PLANE = $2501;
    GL_EYE_PLANE = $2502;
    GL_CLAMP = $2900;
    GL_POLYGON_OFFSET_POINT = $2A01;
    GL_POLYGON_OFFSET_POINT_NV = $2A01;   // api(gles2) //extension: GL_NV_polygon_mode
    GL_POLYGON_OFFSET_LINE = $2A02;
    GL_POLYGON_OFFSET_LINE_NV = $2A02;   // api(gles2) //extension: GL_NV_polygon_mode
    GL_R3_G3_B2 = $2A10;
    GL_V2F = $2A20;
    GL_V3F = $2A21;
    GL_C4UB_V2F = $2A22;
    GL_C4UB_V3F = $2A23;
    GL_C3F_V3F = $2A24;
    GL_N3F_V3F = $2A25;
    GL_C4F_N3F_V3F = $2A26;
    GL_T2F_V3F = $2A27;
    GL_T4F_V4F = $2A28;
    GL_T2F_C4UB_V3F = $2A29;
    GL_T2F_C3F_V3F = $2A2A;
    GL_T2F_N3F_V3F = $2A2B;
    GL_T2F_C4F_N3F_V3F = $2A2C;
    GL_T4F_C4F_N3F_V4F = $2A2D;
    GL_CLIP_PLANE0 = $3000;
    GL_CLIP_PLANE0_IMG = $3000;
    GL_CLIP_DISTANCE0 = $3000;
    GL_CLIP_DISTANCE0_EXT = $3000;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE0_APPLE = $3000;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_PLANE1 = $3001;
    GL_CLIP_PLANE1_IMG = $3001;
    GL_CLIP_DISTANCE1 = $3001;
    GL_CLIP_DISTANCE1_EXT = $3001;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE1_APPLE = $3001;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_PLANE2 = $3002;
    GL_CLIP_PLANE2_IMG = $3002;
    GL_CLIP_DISTANCE2 = $3002;
    GL_CLIP_DISTANCE2_EXT = $3002;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE2_APPLE = $3002;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_PLANE3 = $3003;
    GL_CLIP_PLANE3_IMG = $3003;
    GL_CLIP_DISTANCE3 = $3003;
    GL_CLIP_DISTANCE3_EXT = $3003;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE3_APPLE = $3003;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_PLANE4 = $3004;
    GL_CLIP_PLANE4_IMG = $3004;
    GL_CLIP_DISTANCE4 = $3004;
    GL_CLIP_DISTANCE4_EXT = $3004;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE4_APPLE = $3004;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_PLANE5 = $3005;
    GL_CLIP_PLANE5_IMG = $3005;
    GL_CLIP_DISTANCE5 = $3005;
    GL_CLIP_DISTANCE5_EXT = $3005;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE5_APPLE = $3005;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_DISTANCE6 = $3006;
    GL_CLIP_DISTANCE6_EXT = $3006;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE6_APPLE = $3006;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_CLIP_DISTANCE7 = $3007;
    GL_CLIP_DISTANCE7_EXT = $3007;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CLIP_DISTANCE7_APPLE = $3007;   // api(gles2) //extension: GL_APPLE_clip_distance
    GL_LIGHT0 = $4000;
    GL_LIGHT1 = $4001;
    GL_LIGHT2 = $4002;
    GL_LIGHT3 = $4003;
    GL_LIGHT4 = $4004;
    GL_LIGHT5 = $4005;
    GL_LIGHT6 = $4006;
    GL_LIGHT7 = $4007;
    GL_ABGR_EXT = $8000;
    GL_CONSTANT_COLOR_EXT = $8001;
    GL_ONE_MINUS_CONSTANT_COLOR_EXT = $8002;
    GL_CONSTANT_ALPHA_EXT = $8003;
    GL_ONE_MINUS_CONSTANT_ALPHA_EXT = $8004;
    GL_BLEND_COLOR_EXT = $8005;
    GL_FUNC_ADD_EXT = $8006;   // api(gl|gles1|gles2) //extension: GL_EXT_blend_minmax
    GL_FUNC_ADD_OES = $8006;
    GL_MIN_EXT = $8007;   // api(gl|gles1|gles2) //extension: GL_EXT_blend_minmax
    GL_MAX_EXT = $8008;   // api(gl|gles1|gles2) //extension: GL_EXT_blend_minmax
    GL_BLEND_EQUATION = $8009;
    GL_BLEND_EQUATION_EXT = $8009;   // api(gl|gles1|gles2) //extension: GL_EXT_blend_minmax
    GL_BLEND_EQUATION_OES = $8009;
    GL_BLEND_EQUATION_RGB_EXT = $8009;
    GL_BLEND_EQUATION_RGB_OES = $8009;
    GL_FUNC_SUBTRACT_EXT = $800A;
    GL_FUNC_SUBTRACT_OES = $800A;
    GL_FUNC_REVERSE_SUBTRACT_EXT = $800B;
    GL_FUNC_REVERSE_SUBTRACT_OES = $800B;
    GL_CMYK_EXT = $800C;
    GL_CMYKA_EXT = $800D;
    GL_PACK_CMYK_HINT_EXT = $800E;
    GL_UNPACK_CMYK_HINT_EXT = $800F;
    GL_CONVOLUTION_1D = $8010;
    GL_CONVOLUTION_1D_EXT = $8010;
    GL_CONVOLUTION_2D = $8011;
    GL_CONVOLUTION_2D_EXT = $8011;
    GL_SEPARABLE_2D = $8012;
    GL_SEPARABLE_2D_EXT = $8012;
    GL_CONVOLUTION_BORDER_MODE = $8013;
    GL_CONVOLUTION_BORDER_MODE_EXT = $8013;
    GL_CONVOLUTION_FILTER_SCALE = $8014;
    GL_CONVOLUTION_FILTER_SCALE_EXT = $8014;
    GL_CONVOLUTION_FILTER_BIAS = $8015;
    GL_CONVOLUTION_FILTER_BIAS_EXT = $8015;
    GL_REDUCE = $8016;
    GL_REDUCE_EXT = $8016;
    GL_CONVOLUTION_FORMAT = $8017;
    GL_CONVOLUTION_FORMAT_EXT = $8017;
    GL_CONVOLUTION_WIDTH = $8018;
    GL_CONVOLUTION_WIDTH_EXT = $8018;
    GL_CONVOLUTION_HEIGHT = $8019;
    GL_CONVOLUTION_HEIGHT_EXT = $8019;
    GL_MAX_CONVOLUTION_WIDTH = $801A;
    GL_MAX_CONVOLUTION_WIDTH_EXT = $801A;
    GL_MAX_CONVOLUTION_HEIGHT = $801B;
    GL_MAX_CONVOLUTION_HEIGHT_EXT = $801B;
    GL_POST_CONVOLUTION_RED_SCALE = $801C;
    GL_POST_CONVOLUTION_RED_SCALE_EXT = $801C;
    GL_POST_CONVOLUTION_GREEN_SCALE = $801D;
    GL_POST_CONVOLUTION_GREEN_SCALE_EXT = $801D;
    GL_POST_CONVOLUTION_BLUE_SCALE = $801E;
    GL_POST_CONVOLUTION_BLUE_SCALE_EXT = $801E;
    GL_POST_CONVOLUTION_ALPHA_SCALE = $801F;
    GL_POST_CONVOLUTION_ALPHA_SCALE_EXT = $801F;
    GL_POST_CONVOLUTION_RED_BIAS = $8020;
    GL_POST_CONVOLUTION_RED_BIAS_EXT = $8020;
    GL_POST_CONVOLUTION_GREEN_BIAS = $8021;
    GL_POST_CONVOLUTION_GREEN_BIAS_EXT = $8021;
    GL_POST_CONVOLUTION_BLUE_BIAS = $8022;
    GL_POST_CONVOLUTION_BLUE_BIAS_EXT = $8022;
    GL_POST_CONVOLUTION_ALPHA_BIAS = $8023;
    GL_POST_CONVOLUTION_ALPHA_BIAS_EXT = $8023;
    GL_HISTOGRAM = $8024;
    GL_HISTOGRAM_EXT = $8024;
    GL_PROXY_HISTOGRAM = $8025;
    GL_PROXY_HISTOGRAM_EXT = $8025;
    GL_HISTOGRAM_WIDTH = $8026;
    GL_HISTOGRAM_WIDTH_EXT = $8026;
    GL_HISTOGRAM_FORMAT = $8027;
    GL_HISTOGRAM_FORMAT_EXT = $8027;
    GL_HISTOGRAM_RED_SIZE = $8028;
    GL_HISTOGRAM_RED_SIZE_EXT = $8028;
    GL_HISTOGRAM_GREEN_SIZE = $8029;
    GL_HISTOGRAM_GREEN_SIZE_EXT = $8029;
    GL_HISTOGRAM_BLUE_SIZE = $802A;
    GL_HISTOGRAM_BLUE_SIZE_EXT = $802A;
    GL_HISTOGRAM_ALPHA_SIZE = $802B;
    GL_HISTOGRAM_ALPHA_SIZE_EXT = $802B;
    GL_HISTOGRAM_LUMINANCE_SIZE = $802C;
    GL_HISTOGRAM_LUMINANCE_SIZE_EXT = $802C;
    GL_HISTOGRAM_SINK = $802D;
    GL_HISTOGRAM_SINK_EXT = $802D;
    GL_MINMAX = $802E;
    GL_MINMAX_EXT = $802E;
    GL_MINMAX_FORMAT = $802F;
    GL_MINMAX_FORMAT_EXT = $802F;
    GL_MINMAX_SINK = $8030;
    GL_MINMAX_SINK_EXT = $8030;
    GL_TABLE_TOO_LARGE_EXT = $8031;
    GL_TABLE_TOO_LARGE = $8031;
    GL_UNSIGNED_BYTE_3_3_2 = $8032;
    GL_UNSIGNED_BYTE_3_3_2_EXT = $8032;
    GL_UNSIGNED_SHORT_4_4_4_4_EXT = $8033;
    GL_UNSIGNED_SHORT_5_5_5_1_EXT = $8034;
    GL_UNSIGNED_INT_8_8_8_8 = $8035;
    GL_UNSIGNED_INT_8_8_8_8_EXT = $8035;
    GL_UNSIGNED_INT_10_10_10_2 = $8036;
    GL_UNSIGNED_INT_10_10_10_2_EXT = $8036;
    GL_POLYGON_OFFSET_EXT = $8037;
    GL_POLYGON_OFFSET_FACTOR_EXT = $8038;
    GL_POLYGON_OFFSET_BIAS_EXT = $8039;
    GL_RESCALE_NORMAL = $803A;
    GL_RESCALE_NORMAL_EXT = $803A;
    GL_ALPHA4 = $803B;
    GL_ALPHA4_EXT = $803B;
    GL_ALPHA8 = $803C;
    GL_ALPHA8_EXT = $803C;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_ALPHA8_OES = $803C;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_ALPHA12 = $803D;
    GL_ALPHA12_EXT = $803D;
    GL_ALPHA16 = $803E;
    GL_ALPHA16_EXT = $803E;
    GL_LUMINANCE4 = $803F;
    GL_LUMINANCE4_EXT = $803F;
    GL_LUMINANCE8 = $8040;
    GL_LUMINANCE8_EXT = $8040;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_LUMINANCE8_OES = $8040;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_LUMINANCE12 = $8041;
    GL_LUMINANCE12_EXT = $8041;
    GL_LUMINANCE16 = $8042;
    GL_LUMINANCE16_EXT = $8042;
    GL_LUMINANCE4_ALPHA4 = $8043;
    GL_LUMINANCE4_ALPHA4_EXT = $8043;
    GL_LUMINANCE4_ALPHA4_OES = $8043;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_LUMINANCE6_ALPHA2 = $8044;
    GL_LUMINANCE6_ALPHA2_EXT = $8044;
    GL_LUMINANCE8_ALPHA8 = $8045;
    GL_LUMINANCE8_ALPHA8_EXT = $8045;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_LUMINANCE8_ALPHA8_OES = $8045;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_LUMINANCE12_ALPHA4 = $8046;
    GL_LUMINANCE12_ALPHA4_EXT = $8046;
    GL_LUMINANCE12_ALPHA12 = $8047;
    GL_LUMINANCE12_ALPHA12_EXT = $8047;
    GL_LUMINANCE16_ALPHA16 = $8048;
    GL_LUMINANCE16_ALPHA16_EXT = $8048;
    GL_INTENSITY = $8049;
    GL_INTENSITY_EXT = $8049;
    GL_INTENSITY4 = $804A;
    GL_INTENSITY4_EXT = $804A;
    GL_INTENSITY8 = $804B;
    GL_INTENSITY8_EXT = $804B;
    GL_INTENSITY12 = $804C;
    GL_INTENSITY12_EXT = $804C;
    GL_INTENSITY16 = $804D;
    GL_INTENSITY16_EXT = $804D;
    GL_RGB2_EXT = $804E;
    GL_RGB4 = $804F;
    GL_RGB4_EXT = $804F;
    GL_RGB5 = $8050;
    GL_RGB5_EXT = $8050;
    GL_RGB8_EXT = $8051;
    GL_RGB8_OES = $8051;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_RGB10 = $8052;
    GL_RGB10_EXT = $8052;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_RGB12 = $8053;
    GL_RGB12_EXT = $8053;
    GL_RGB16 = $8054;
    GL_RGB16_EXT = $8054;   // api(gles2) //extension: GL_EXT_texture_norm16
    GL_RGBA2 = $8055;
    GL_RGBA2_EXT = $8055;
    GL_RGBA4_EXT = $8056;
    GL_RGBA4_OES = $8056;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_RGB5_A1_EXT = $8057;
    GL_RGB5_A1_OES = $8057;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_RGBA8_EXT = $8058;
    GL_RGBA8_OES = $8058;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_RGB10_A2_EXT = $8059;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_RGBA12 = $805A;
    GL_RGBA12_EXT = $805A;
    GL_RGBA16 = $805B;
    GL_RGBA16_EXT = $805B;   // api(gles2) //extension: GL_EXT_texture_norm16
    GL_TEXTURE_RED_SIZE_EXT = $805C;
    GL_TEXTURE_GREEN_SIZE_EXT = $805D;
    GL_TEXTURE_BLUE_SIZE_EXT = $805E;
    GL_TEXTURE_ALPHA_SIZE_EXT = $805F;
    GL_TEXTURE_LUMINANCE_SIZE = $8060;
    GL_TEXTURE_LUMINANCE_SIZE_EXT = $8060;
    GL_TEXTURE_INTENSITY_SIZE = $8061;
    GL_TEXTURE_INTENSITY_SIZE_EXT = $8061;
    GL_REPLACE_EXT = $8062;
    GL_PROXY_TEXTURE_1D = $8063;
    GL_PROXY_TEXTURE_1D_EXT = $8063;
    GL_PROXY_TEXTURE_2D = $8064;
    GL_PROXY_TEXTURE_2D_EXT = $8064;
    GL_TEXTURE_TOO_LARGE_EXT = $8065;
    GL_TEXTURE_PRIORITY = $8066;
    GL_TEXTURE_PRIORITY_EXT = $8066;
    GL_TEXTURE_RESIDENT = $8067;
    GL_TEXTURE_RESIDENT_EXT = $8067;
    GL_TEXTURE_1D_BINDING_EXT = $8068;
    GL_TEXTURE_BINDING_1D = $8068;
    GL_TEXTURE_2D_BINDING_EXT = $8069;
    GL_TEXTURE_3D_BINDING_EXT = $806A;
    GL_TEXTURE_3D_BINDING_OES = $806A;
    GL_TEXTURE_BINDING_3D_OES = $806A;   // api(gles2) //extension: GL_OES_texture_3D
    GL_PACK_SKIP_IMAGES = $806B;
    GL_PACK_SKIP_IMAGES_EXT = $806B;
    GL_PACK_IMAGE_HEIGHT = $806C;
    GL_PACK_IMAGE_HEIGHT_EXT = $806C;
    GL_UNPACK_SKIP_IMAGES_EXT = $806D;
    GL_UNPACK_IMAGE_HEIGHT_EXT = $806E;
    GL_TEXTURE_3D_EXT = $806F;
    GL_TEXTURE_3D_OES = $806F;   // api(gles2) //extension: GL_OES_texture_3D
    GL_PROXY_TEXTURE_3D = $8070;
    GL_PROXY_TEXTURE_3D_EXT = $8070;
    GL_TEXTURE_DEPTH = $8071;
    GL_TEXTURE_DEPTH_EXT = $8071;
    GL_TEXTURE_WRAP_R_EXT = $8072;
    GL_TEXTURE_WRAP_R_OES = $8072;   // api(gles2) //extension: GL_OES_texture_3D
    GL_MAX_3D_TEXTURE_SIZE_EXT = $8073;
    GL_MAX_3D_TEXTURE_SIZE_OES = $8073;   // api(gles2) //extension: GL_OES_texture_3D
    GL_VERTEX_ARRAY_EXT = $8074;
    GL_VERTEX_ARRAY_KHR = $8074;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_NORMAL_ARRAY = $8075;
    GL_NORMAL_ARRAY_EXT = $8075;
    GL_COLOR_ARRAY = $8076;
    GL_COLOR_ARRAY_EXT = $8076;
    GL_INDEX_ARRAY = $8077;
    GL_INDEX_ARRAY_EXT = $8077;
    GL_TEXTURE_COORD_ARRAY = $8078;
    GL_TEXTURE_COORD_ARRAY_EXT = $8078;
    GL_EDGE_FLAG_ARRAY = $8079;
    GL_EDGE_FLAG_ARRAY_EXT = $8079;
    GL_VERTEX_ARRAY_SIZE = $807A;
    GL_VERTEX_ARRAY_SIZE_EXT = $807A;
    GL_VERTEX_ARRAY_TYPE = $807B;
    GL_VERTEX_ARRAY_TYPE_EXT = $807B;
    GL_VERTEX_ARRAY_STRIDE = $807C;
    GL_VERTEX_ARRAY_STRIDE_EXT = $807C;
    GL_VERTEX_ARRAY_COUNT_EXT = $807D;
    GL_NORMAL_ARRAY_TYPE = $807E;
    GL_NORMAL_ARRAY_TYPE_EXT = $807E;
    GL_NORMAL_ARRAY_STRIDE = $807F;
    GL_NORMAL_ARRAY_STRIDE_EXT = $807F;
    GL_NORMAL_ARRAY_COUNT_EXT = $8080;
    GL_COLOR_ARRAY_SIZE = $8081;
    GL_COLOR_ARRAY_SIZE_EXT = $8081;
    GL_COLOR_ARRAY_TYPE = $8082;
    GL_COLOR_ARRAY_TYPE_EXT = $8082;
    GL_COLOR_ARRAY_STRIDE = $8083;
    GL_COLOR_ARRAY_STRIDE_EXT = $8083;
    GL_COLOR_ARRAY_COUNT_EXT = $8084;
    GL_INDEX_ARRAY_TYPE = $8085;
    GL_INDEX_ARRAY_TYPE_EXT = $8085;
    GL_INDEX_ARRAY_STRIDE = $8086;
    GL_INDEX_ARRAY_STRIDE_EXT = $8086;
    GL_INDEX_ARRAY_COUNT_EXT = $8087;
    GL_TEXTURE_COORD_ARRAY_SIZE = $8088;
    GL_TEXTURE_COORD_ARRAY_SIZE_EXT = $8088;
    GL_TEXTURE_COORD_ARRAY_TYPE = $8089;
    GL_TEXTURE_COORD_ARRAY_TYPE_EXT = $8089;
    GL_TEXTURE_COORD_ARRAY_STRIDE = $808A;
    GL_TEXTURE_COORD_ARRAY_STRIDE_EXT = $808A;
    GL_TEXTURE_COORD_ARRAY_COUNT_EXT = $808B;
    GL_EDGE_FLAG_ARRAY_STRIDE = $808C;
    GL_EDGE_FLAG_ARRAY_STRIDE_EXT = $808C;
    GL_EDGE_FLAG_ARRAY_COUNT_EXT = $808D;
    GL_VERTEX_ARRAY_POINTER = $808E;
    GL_VERTEX_ARRAY_POINTER_EXT = $808E;
    GL_NORMAL_ARRAY_POINTER = $808F;
    GL_NORMAL_ARRAY_POINTER_EXT = $808F;
    GL_COLOR_ARRAY_POINTER = $8090;
    GL_COLOR_ARRAY_POINTER_EXT = $8090;
    GL_INDEX_ARRAY_POINTER = $8091;
    GL_INDEX_ARRAY_POINTER_EXT = $8091;
    GL_TEXTURE_COORD_ARRAY_POINTER = $8092;
    GL_TEXTURE_COORD_ARRAY_POINTER_EXT = $8092;
    GL_EDGE_FLAG_ARRAY_POINTER = $8093;
    GL_EDGE_FLAG_ARRAY_POINTER_EXT = $8093;
    GL_INTERLACE_SGIX = $8094;
    GL_DETAIL_TEXTURE_2D_SGIS = $8095;
    GL_DETAIL_TEXTURE_2D_BINDING_SGIS = $8096;
    GL_LINEAR_DETAIL_SGIS = $8097;
    GL_LINEAR_DETAIL_ALPHA_SGIS = $8098;
    GL_LINEAR_DETAIL_COLOR_SGIS = $8099;
    GL_DETAIL_TEXTURE_LEVEL_SGIS = $809A;
    GL_DETAIL_TEXTURE_MODE_SGIS = $809B;
    GL_DETAIL_TEXTURE_FUNC_POINTS_SGIS = $809C;
    GL_MULTISAMPLE = $809D;
    GL_MULTISAMPLE_ARB = $809D;
    GL_MULTISAMPLE_EXT = $809D;   // api(gles2) //extension: GL_EXT_multisampled_compatibility
    GL_MULTISAMPLE_SGIS = $809D;
    GL_SAMPLE_ALPHA_TO_COVERAGE_ARB = $809E;
    GL_SAMPLE_ALPHA_TO_MASK_EXT = $809E;
    GL_SAMPLE_ALPHA_TO_MASK_SGIS = $809E;
    GL_SAMPLE_ALPHA_TO_ONE = $809F;
    GL_SAMPLE_ALPHA_TO_ONE_ARB = $809F;
    GL_SAMPLE_ALPHA_TO_ONE_EXT = $809F;   // api(gles2) //extension: GL_EXT_multisampled_compatibility
    GL_SAMPLE_ALPHA_TO_ONE_SGIS = $809F;
    GL_SAMPLE_COVERAGE_ARB = $80A0;
    GL_SAMPLE_MASK_EXT = $80A0;
    GL_SAMPLE_MASK_SGIS = $80A0;
    GL_1PASS_EXT = $80A1;
    GL_1PASS_SGIS = $80A1;
    GL_2PASS_0_EXT = $80A2;
    GL_2PASS_0_SGIS = $80A2;
    GL_2PASS_1_EXT = $80A3;
    GL_2PASS_1_SGIS = $80A3;
    GL_4PASS_0_EXT = $80A4;
    GL_4PASS_0_SGIS = $80A4;
    GL_4PASS_1_EXT = $80A5;
    GL_4PASS_1_SGIS = $80A5;
    GL_4PASS_2_EXT = $80A6;
    GL_4PASS_2_SGIS = $80A6;
    GL_4PASS_3_EXT = $80A7;
    GL_4PASS_3_SGIS = $80A7;
    GL_SAMPLE_BUFFERS_ARB = $80A8;
    GL_SAMPLE_BUFFERS_EXT = $80A8;
    GL_SAMPLE_BUFFERS_SGIS = $80A8;
    GL_SAMPLES_ARB = $80A9;
    GL_SAMPLES_EXT = $80A9;
    GL_SAMPLES_SGIS = $80A9;
    GL_SAMPLE_COVERAGE_VALUE_ARB = $80AA;
    GL_SAMPLE_MASK_VALUE_EXT = $80AA;
    GL_SAMPLE_MASK_VALUE_SGIS = $80AA;
    GL_SAMPLE_COVERAGE_INVERT_ARB = $80AB;
    GL_SAMPLE_MASK_INVERT_EXT = $80AB;
    GL_SAMPLE_MASK_INVERT_SGIS = $80AB;
    GL_SAMPLE_PATTERN_EXT = $80AC;
    GL_SAMPLE_PATTERN_SGIS = $80AC;
    GL_LINEAR_SHARPEN_SGIS = $80AD;
    GL_LINEAR_SHARPEN_ALPHA_SGIS = $80AE;
    GL_LINEAR_SHARPEN_COLOR_SGIS = $80AF;
    GL_SHARPEN_TEXTURE_FUNC_POINTS_SGIS = $80B0;
    GL_COLOR_MATRIX = $80B1;
    GL_COLOR_MATRIX_SGI = $80B1;
    GL_COLOR_MATRIX_STACK_DEPTH = $80B2;
    GL_COLOR_MATRIX_STACK_DEPTH_SGI = $80B2;
    GL_MAX_COLOR_MATRIX_STACK_DEPTH = $80B3;
    GL_MAX_COLOR_MATRIX_STACK_DEPTH_SGI = $80B3;
    GL_POST_COLOR_MATRIX_RED_SCALE = $80B4;
    GL_POST_COLOR_MATRIX_RED_SCALE_SGI = $80B4;
    GL_POST_COLOR_MATRIX_GREEN_SCALE = $80B5;
    GL_POST_COLOR_MATRIX_GREEN_SCALE_SGI = $80B5;
    GL_POST_COLOR_MATRIX_BLUE_SCALE = $80B6;
    GL_POST_COLOR_MATRIX_BLUE_SCALE_SGI = $80B6;
    GL_POST_COLOR_MATRIX_ALPHA_SCALE = $80B7;
    GL_POST_COLOR_MATRIX_ALPHA_SCALE_SGI = $80B7;
    GL_POST_COLOR_MATRIX_RED_BIAS = $80B8;
    GL_POST_COLOR_MATRIX_RED_BIAS_SGI = $80B8;
    GL_POST_COLOR_MATRIX_GREEN_BIAS = $80B9;
    GL_POST_COLOR_MATRIX_GREEN_BIAS_SGI = $80B9;
    GL_POST_COLOR_MATRIX_BLUE_BIAS = $80BA;
    GL_POST_COLOR_MATRIX_BLUE_BIAS_SGI = $80BA;
    GL_POST_COLOR_MATRIX_ALPHA_BIAS = $80BB;
    GL_POST_COLOR_MATRIX_ALPHA_BIAS_SGI = $80BB;
    GL_TEXTURE_COLOR_TABLE_SGI = $80BC;
    GL_PROXY_TEXTURE_COLOR_TABLE_SGI = $80BD;
    GL_TEXTURE_ENV_BIAS_SGIX = $80BE;
    GL_SHADOW_AMBIENT_SGIX = $80BF;
    GL_TEXTURE_COMPARE_FAIL_VALUE_ARB = $80BF;
    GL_BLEND_DST_RGB_EXT = $80C8;
    GL_BLEND_DST_RGB_OES = $80C8;
    GL_BLEND_SRC_RGB_EXT = $80C9;
    GL_BLEND_SRC_RGB_OES = $80C9;
    GL_BLEND_DST_ALPHA_EXT = $80CA;
    GL_BLEND_DST_ALPHA_OES = $80CA;
    GL_BLEND_SRC_ALPHA_EXT = $80CB;
    GL_BLEND_SRC_ALPHA_OES = $80CB;
    GL_422_EXT = $80CC;
    GL_422_REV_EXT = $80CD;
    GL_422_AVERAGE_EXT = $80CE;
    GL_422_REV_AVERAGE_EXT = $80CF;
    GL_COLOR_TABLE = $80D0;
    GL_COLOR_TABLE_SGI = $80D0;
    GL_POST_CONVOLUTION_COLOR_TABLE = $80D1;
    GL_POST_CONVOLUTION_COLOR_TABLE_SGI = $80D1;
    GL_POST_COLOR_MATRIX_COLOR_TABLE = $80D2;
    GL_POST_COLOR_MATRIX_COLOR_TABLE_SGI = $80D2;
    GL_PROXY_COLOR_TABLE = $80D3;
    GL_PROXY_COLOR_TABLE_SGI = $80D3;
    GL_PROXY_POST_CONVOLUTION_COLOR_TABLE = $80D4;
    GL_PROXY_POST_CONVOLUTION_COLOR_TABLE_SGI = $80D4;
    GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE = $80D5;
    GL_PROXY_POST_COLOR_MATRIX_COLOR_TABLE_SGI = $80D5;
    GL_COLOR_TABLE_SCALE = $80D6;
    GL_COLOR_TABLE_SCALE_SGI = $80D6;
    GL_COLOR_TABLE_BIAS = $80D7;
    GL_COLOR_TABLE_BIAS_SGI = $80D7;
    GL_COLOR_TABLE_FORMAT = $80D8;
    GL_COLOR_TABLE_FORMAT_SGI = $80D8;
    GL_COLOR_TABLE_WIDTH = $80D9;
    GL_COLOR_TABLE_WIDTH_SGI = $80D9;
    GL_COLOR_TABLE_RED_SIZE = $80DA;
    GL_COLOR_TABLE_RED_SIZE_SGI = $80DA;
    GL_COLOR_TABLE_GREEN_SIZE = $80DB;
    GL_COLOR_TABLE_GREEN_SIZE_SGI = $80DB;
    GL_COLOR_TABLE_BLUE_SIZE = $80DC;
    GL_COLOR_TABLE_BLUE_SIZE_SGI = $80DC;
    GL_COLOR_TABLE_ALPHA_SIZE = $80DD;
    GL_COLOR_TABLE_ALPHA_SIZE_SGI = $80DD;
    GL_COLOR_TABLE_LUMINANCE_SIZE = $80DE;
    GL_COLOR_TABLE_LUMINANCE_SIZE_SGI = $80DE;
    GL_COLOR_TABLE_INTENSITY_SIZE = $80DF;
    GL_COLOR_TABLE_INTENSITY_SIZE_SGI = $80DF;
    GL_BGR = $80E0;
    GL_BGR_EXT = $80E0;
    GL_BGRA = $80E1;
    GL_BGRA_EXT = $80E1;   // api(gles1|gles2) //extension: GL_APPLE_texture_format_BGRA8888
    GL_BGRA_IMG = $80E1;   // api(gles1|gles2) //extension: GL_IMG_read_format
    GL_COLOR_INDEX1_EXT = $80E2;
    GL_COLOR_INDEX2_EXT = $80E3;
    GL_COLOR_INDEX4_EXT = $80E4;
    GL_COLOR_INDEX8_EXT = $80E5;
    GL_COLOR_INDEX12_EXT = $80E6;
    GL_COLOR_INDEX16_EXT = $80E7;
    GL_MAX_ELEMENTS_VERTICES_EXT = $80E8;
    GL_MAX_ELEMENTS_INDICES_EXT = $80E9;
    GL_PHONG_WIN = $80EA;
    GL_PHONG_HINT_WIN = $80EB;
    GL_FOG_SPECULAR_TEXTURE_WIN = $80EC;
    GL_TEXTURE_INDEX_SIZE_EXT = $80ED;
    GL_PARAMETER_BUFFER = $80EE;
    GL_PARAMETER_BUFFER_ARB = $80EE;
    GL_PARAMETER_BUFFER_BINDING = $80EF;
    GL_PARAMETER_BUFFER_BINDING_ARB = $80EF;
    GL_CLIP_VOLUME_CLIPPING_HINT_EXT = $80F0;
    GL_DUAL_ALPHA4_SGIS = $8110;
    GL_DUAL_ALPHA8_SGIS = $8111;
    GL_DUAL_ALPHA12_SGIS = $8112;
    GL_DUAL_ALPHA16_SGIS = $8113;
    GL_DUAL_LUMINANCE4_SGIS = $8114;
    GL_DUAL_LUMINANCE8_SGIS = $8115;
    GL_DUAL_LUMINANCE12_SGIS = $8116;
    GL_DUAL_LUMINANCE16_SGIS = $8117;
    GL_DUAL_INTENSITY4_SGIS = $8118;
    GL_DUAL_INTENSITY8_SGIS = $8119;
    GL_DUAL_INTENSITY12_SGIS = $811A;
    GL_DUAL_INTENSITY16_SGIS = $811B;
    GL_DUAL_LUMINANCE_ALPHA4_SGIS = $811C;
    GL_DUAL_LUMINANCE_ALPHA8_SGIS = $811D;
    GL_QUAD_ALPHA4_SGIS = $811E;
    GL_QUAD_ALPHA8_SGIS = $811F;
    GL_QUAD_LUMINANCE4_SGIS = $8120;
    GL_QUAD_LUMINANCE8_SGIS = $8121;
    GL_QUAD_INTENSITY4_SGIS = $8122;
    GL_QUAD_INTENSITY8_SGIS = $8123;
    GL_DUAL_TEXTURE_SELECT_SGIS = $8124;
    GL_QUAD_TEXTURE_SELECT_SGIS = $8125;
    GL_POINT_SIZE_MIN = $8126;
    GL_POINT_SIZE_MIN_ARB = $8126;
    GL_POINT_SIZE_MIN_EXT = $8126;
    GL_POINT_SIZE_MIN_SGIS = $8126;
    GL_POINT_SIZE_MAX = $8127;
    GL_POINT_SIZE_MAX_ARB = $8127;
    GL_POINT_SIZE_MAX_EXT = $8127;
    GL_POINT_SIZE_MAX_SGIS = $8127;
    GL_POINT_FADE_THRESHOLD_SIZE = $8128;
    GL_POINT_FADE_THRESHOLD_SIZE_ARB = $8128;
    GL_POINT_FADE_THRESHOLD_SIZE_EXT = $8128;
    GL_POINT_FADE_THRESHOLD_SIZE_SGIS = $8128;
    GL_DISTANCE_ATTENUATION_EXT = $8129;
    GL_DISTANCE_ATTENUATION_SGIS = $8129;
    GL_POINT_DISTANCE_ATTENUATION = $8129;
    GL_POINT_DISTANCE_ATTENUATION_ARB = $8129;
    GL_FOG_FUNC_SGIS = $812A;
    GL_FOG_FUNC_POINTS_SGIS = $812B;
    GL_MAX_FOG_FUNC_POINTS_SGIS = $812C;
    GL_CLAMP_TO_BORDER_ARB = $812D;
    GL_CLAMP_TO_BORDER_EXT = $812D;   // api(gles2) //extension: GL_EXT_texture_border_clamp
    GL_CLAMP_TO_BORDER_NV = $812D;   // api(gles2) //extension: GL_NV_texture_border_clamp
    GL_CLAMP_TO_BORDER_SGIS = $812D;
    GL_CLAMP_TO_BORDER_OES = $812D;   // api(gles2) //extension: GL_OES_texture_border_clamp
    GL_TEXTURE_MULTI_BUFFER_HINT_SGIX = $812E;
    GL_CLAMP_TO_EDGE_SGIS = $812F;
    GL_PACK_SKIP_VOLUMES_SGIS = $8130;
    GL_PACK_IMAGE_DEPTH_SGIS = $8131;
    GL_UNPACK_SKIP_VOLUMES_SGIS = $8132;
    GL_UNPACK_IMAGE_DEPTH_SGIS = $8133;
    GL_TEXTURE_4D_SGIS = $8134;
    GL_PROXY_TEXTURE_4D_SGIS = $8135;
    GL_TEXTURE_4DSIZE_SGIS = $8136;
    GL_TEXTURE_WRAP_Q_SGIS = $8137;
    GL_MAX_4D_TEXTURE_SIZE_SGIS = $8138;
    GL_PIXEL_TEX_GEN_SGIX = $8139;
    GL_TEXTURE_MIN_LOD_SGIS = $813A;
    GL_TEXTURE_MAX_LOD_SGIS = $813B;
    GL_TEXTURE_BASE_LEVEL_SGIS = $813C;
    GL_TEXTURE_MAX_LEVEL_APPLE = $813D;   // api(gles1|gles2) //extension: GL_APPLE_texture_max_level
    GL_TEXTURE_MAX_LEVEL_SGIS = $813D;
    GL_PIXEL_TILE_BEST_ALIGNMENT_SGIX = $813E;
    GL_PIXEL_TILE_CACHE_INCREMENT_SGIX = $813F;
    GL_PIXEL_TILE_WIDTH_SGIX = $8140;
    GL_PIXEL_TILE_HEIGHT_SGIX = $8141;
    GL_PIXEL_TILE_GRID_WIDTH_SGIX = $8142;
    GL_PIXEL_TILE_GRID_HEIGHT_SGIX = $8143;
    GL_PIXEL_TILE_GRID_DEPTH_SGIX = $8144;
    GL_PIXEL_TILE_CACHE_SIZE_SGIX = $8145;
    GL_FILTER4_SGIS = $8146;
    GL_TEXTURE_FILTER4_SIZE_SGIS = $8147;
    GL_SPRITE_SGIX = $8148;
    GL_SPRITE_MODE_SGIX = $8149;
    GL_SPRITE_AXIS_SGIX = $814A;
    GL_SPRITE_TRANSLATION_SGIX = $814B;
    GL_SPRITE_AXIAL_SGIX = $814C;
    GL_SPRITE_OBJECT_ALIGNED_SGIX = $814D;
    GL_SPRITE_EYE_ALIGNED_SGIX = $814E;
    GL_TEXTURE_4D_BINDING_SGIS = $814F;
    GL_IGNORE_BORDER_HP = $8150;
    GL_CONSTANT_BORDER = $8151;
    GL_CONSTANT_BORDER_HP = $8151;
    GL_REPLICATE_BORDER = $8153;
    GL_REPLICATE_BORDER_HP = $8153;
    GL_CONVOLUTION_BORDER_COLOR = $8154;
    GL_CONVOLUTION_BORDER_COLOR_HP = $8154;
    GL_IMAGE_SCALE_X_HP = $8155;
    GL_IMAGE_SCALE_Y_HP = $8156;
    GL_IMAGE_TRANSLATE_X_HP = $8157;
    GL_IMAGE_TRANSLATE_Y_HP = $8158;
    GL_IMAGE_ROTATE_ANGLE_HP = $8159;
    GL_IMAGE_ROTATE_ORIGIN_X_HP = $815A;
    GL_IMAGE_ROTATE_ORIGIN_Y_HP = $815B;
    GL_IMAGE_MAG_FILTER_HP = $815C;
    GL_IMAGE_MIN_FILTER_HP = $815D;
    GL_IMAGE_CUBIC_WEIGHT_HP = $815E;
    GL_CUBIC_HP = $815F;
    GL_AVERAGE_HP = $8160;
    GL_IMAGE_TRANSFORM_2D_HP = $8161;
    GL_POST_IMAGE_TRANSFORM_COLOR_TABLE_HP = $8162;
    GL_PROXY_POST_IMAGE_TRANSFORM_COLOR_TABLE_HP = $8163;
    GL_OCCLUSION_TEST_HP = $8165;
    GL_OCCLUSION_TEST_RESULT_HP = $8166;
    GL_TEXTURE_LIGHTING_MODE_HP = $8167;
    GL_TEXTURE_POST_SPECULAR_HP = $8168;
    GL_TEXTURE_PRE_SPECULAR_HP = $8169;
    GL_LINEAR_CLIPMAP_LINEAR_SGIX = $8170;
    GL_TEXTURE_CLIPMAP_CENTER_SGIX = $8171;
    GL_TEXTURE_CLIPMAP_FRAME_SGIX = $8172;
    GL_TEXTURE_CLIPMAP_OFFSET_SGIX = $8173;
    GL_TEXTURE_CLIPMAP_VIRTUAL_DEPTH_SGIX = $8174;
    GL_TEXTURE_CLIPMAP_LOD_OFFSET_SGIX = $8175;
    GL_TEXTURE_CLIPMAP_DEPTH_SGIX = $8176;
    GL_MAX_CLIPMAP_DEPTH_SGIX = $8177;
    GL_MAX_CLIPMAP_VIRTUAL_DEPTH_SGIX = $8178;
    GL_POST_TEXTURE_FILTER_BIAS_SGIX = $8179;
    GL_POST_TEXTURE_FILTER_SCALE_SGIX = $817A;
    GL_POST_TEXTURE_FILTER_BIAS_RANGE_SGIX = $817B;
    GL_POST_TEXTURE_FILTER_SCALE_RANGE_SGIX = $817C;
    GL_REFERENCE_PLANE_SGIX = $817D;
    GL_REFERENCE_PLANE_EQUATION_SGIX = $817E;
    GL_IR_INSTRUMENT1_SGIX = $817F;
    GL_INSTRUMENT_BUFFER_POINTER_SGIX = $8180;
    GL_INSTRUMENT_MEASUREMENTS_SGIX = $8181;
    GL_LIST_PRIORITY_SGIX = $8182;
    GL_CALLIGRAPHIC_FRAGMENT_SGIX = $8183;
    GL_PIXEL_TEX_GEN_Q_CEILING_SGIX = $8184;
    GL_PIXEL_TEX_GEN_Q_ROUND_SGIX = $8185;
    GL_PIXEL_TEX_GEN_Q_FLOOR_SGIX = $8186;
    GL_PIXEL_TEX_GEN_ALPHA_REPLACE_SGIX = $8187;
    GL_PIXEL_TEX_GEN_ALPHA_NO_REPLACE_SGIX = $8188;
    GL_PIXEL_TEX_GEN_ALPHA_LS_SGIX = $8189;
    GL_PIXEL_TEX_GEN_ALPHA_MS_SGIX = $818A;
    GL_FRAMEZOOM_SGIX = $818B;
    GL_FRAMEZOOM_FACTOR_SGIX = $818C;
    GL_MAX_FRAMEZOOM_FACTOR_SGIX = $818D;
    GL_TEXTURE_LOD_BIAS_S_SGIX = $818E;
    GL_TEXTURE_LOD_BIAS_T_SGIX = $818F;
    GL_TEXTURE_LOD_BIAS_R_SGIX = $8190;
    GL_GENERATE_MIPMAP = $8191;
    GL_GENERATE_MIPMAP_SGIS = $8191;
    GL_GENERATE_MIPMAP_HINT_SGIS = $8192;
    GL_GEOMETRY_DEFORMATION_SGIX = $8194;
    GL_TEXTURE_DEFORMATION_SGIX = $8195;
    GL_DEFORMATIONS_MASK_SGIX = $8196;
    GL_MAX_DEFORMATION_ORDER_SGIX = $8197;
    GL_FOG_OFFSET_SGIX = $8198;
    GL_FOG_OFFSET_VALUE_SGIX = $8199;
    GL_TEXTURE_COMPARE_SGIX = $819A;
    GL_TEXTURE_COMPARE_OPERATOR_SGIX = $819B;
    GL_TEXTURE_LEQUAL_R_SGIX = $819C;
    GL_TEXTURE_GEQUAL_R_SGIX = $819D;
    GL_DEPTH_COMPONENT16_ARB = $81A5;
    GL_DEPTH_COMPONENT16_OES = $81A5;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_DEPTH_COMPONENT16_SGIX = $81A5;
    GL_DEPTH_COMPONENT24 = $81A6;
    GL_DEPTH_COMPONENT24_ARB = $81A6;
    GL_DEPTH_COMPONENT24_OES = $81A6;   // api(gles1|gles2|glsc2) //extension: GL_OES_depth24
    GL_DEPTH_COMPONENT24_SGIX = $81A6;
    GL_DEPTH_COMPONENT32 = $81A7;
    GL_DEPTH_COMPONENT32_ARB = $81A7;
    GL_DEPTH_COMPONENT32_OES = $81A7;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_DEPTH_COMPONENT32_SGIX = $81A7;
    GL_ARRAY_ELEMENT_LOCK_FIRST_EXT = $81A8;
    GL_ARRAY_ELEMENT_LOCK_COUNT_EXT = $81A9;
    GL_CULL_VERTEX_EXT = $81AA;
    GL_CULL_VERTEX_EYE_POSITION_EXT = $81AB;
    GL_CULL_VERTEX_OBJECT_POSITION_EXT = $81AC;
    GL_IUI_V2F_EXT = $81AD;
    GL_IUI_V3F_EXT = $81AE;
    GL_IUI_N3F_V2F_EXT = $81AF;
    GL_IUI_N3F_V3F_EXT = $81B0;
    GL_T2F_IUI_V2F_EXT = $81B1;
    GL_T2F_IUI_V3F_EXT = $81B2;
    GL_T2F_IUI_N3F_V2F_EXT = $81B3;
    GL_T2F_IUI_N3F_V3F_EXT = $81B4;
    GL_INDEX_TEST_EXT = $81B5;
    GL_INDEX_TEST_FUNC_EXT = $81B6;
    GL_INDEX_TEST_REF_EXT = $81B7;
    GL_INDEX_MATERIAL_EXT = $81B8;
    GL_INDEX_MATERIAL_PARAMETER_EXT = $81B9;
    GL_INDEX_MATERIAL_FACE_EXT = $81BA;
    GL_YCRCB_422_SGIX = $81BB;
    GL_YCRCB_444_SGIX = $81BC;
    GL_WRAP_BORDER_SUN = $81D4;
    GL_UNPACK_CONSTANT_DATA_SUNX = $81D5;
    GL_TEXTURE_CONSTANT_DATA_SUNX = $81D6;
    GL_TRIANGLE_LIST_SUN = $81D7;
    GL_REPLACEMENT_CODE_SUN = $81D8;
    GL_GLOBAL_ALPHA_SUN = $81D9;
    GL_GLOBAL_ALPHA_FACTOR_SUN = $81DA;
    GL_TEXTURE_COLOR_WRITEMASK_SGIS = $81EF;
    GL_EYE_DISTANCE_TO_POINT_SGIS = $81F0;
    GL_OBJECT_DISTANCE_TO_POINT_SGIS = $81F1;
    GL_EYE_DISTANCE_TO_LINE_SGIS = $81F2;
    GL_OBJECT_DISTANCE_TO_LINE_SGIS = $81F3;
    GL_EYE_POINT_SGIS = $81F4;
    GL_OBJECT_POINT_SGIS = $81F5;
    GL_EYE_LINE_SGIS = $81F6;
    GL_OBJECT_LINE_SGIS = $81F7;
    GL_LIGHT_MODEL_COLOR_CONTROL = $81F8;
    GL_LIGHT_MODEL_COLOR_CONTROL_EXT = $81F8;
    GL_SINGLE_COLOR = $81F9;
    GL_SINGLE_COLOR_EXT = $81F9;
    GL_SEPARATE_SPECULAR_COLOR = $81FA;
    GL_SEPARATE_SPECULAR_COLOR_EXT = $81FA;
    GL_SHARED_TEXTURE_PALETTE_EXT = $81FB;
    GL_TEXT_FRAGMENT_SHADER_ATI = $8200;
    GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING_EXT = $8210;   // api(gles1|gles2) //extension: GL_EXT_sRGB
    GL_FRAMEBUFFER_ATTACHMENT_COMPONENT_TYPE_EXT = $8211;   // api(gles2) //extension: GL_EXT_color_buffer_half_float
    GL_FRAMEBUFFER_DEFAULT = $8218;
    GL_FRAMEBUFFER_UNDEFINED_OES = $8219;   // api(gles1|gles2) //extension: GL_OES_surfaceless_context
    GL_BUFFER_IMMUTABLE_STORAGE = $821F;
    GL_BUFFER_IMMUTABLE_STORAGE_EXT = $821F;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_BUFFER_STORAGE_FLAGS = $8220;
    GL_BUFFER_STORAGE_FLAGS_EXT = $8220;   // api(gles2) //extension: GL_EXT_buffer_storage
    GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED = $8221;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_PRIMITIVE_RESTART_FOR_PATCHES_SUPPORTED_OES = $8221;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_INDEX = $8222;
    GL_COMPRESSED_RED = $8225;
    GL_COMPRESSED_RG = $8226;
    GL_RG_EXT = $8227;   // api(gles2) //extension: GL_EXT_texture_rg
    GL_R8_EXT = $8229;   // api(gles2) //extension: GL_EXT_texture_rg
    GL_R16 = $822A;
    GL_R16_EXT = $822A;   // api(gles2) //extension: GL_EXT_texture_norm16
    GL_RG8_EXT = $822B;   // api(gles2) //extension: GL_EXT_texture_rg
    GL_RG16 = $822C;
    GL_RG16_EXT = $822C;   // api(gles2) //extension: GL_EXT_texture_norm16
    GL_R16F_EXT = $822D;   // api(gles2) //extension: GL_EXT_color_buffer_half_float
    GL_R32F_EXT = $822E;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_RG16F_EXT = $822F;   // api(gles2) //extension: GL_EXT_color_buffer_half_float
    GL_RG32F_EXT = $8230;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_SYNC_CL_EVENT_ARB = $8240;
    GL_SYNC_CL_EVENT_COMPLETE_ARB = $8241;
    GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB = $8242;
    GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR = $8242;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH = $8243;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_ARB = $8243;
    GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH_KHR = $8243;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_CALLBACK_FUNCTION_ARB = $8244;
    GL_DEBUG_CALLBACK_FUNCTION_KHR = $8244;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_CALLBACK_USER_PARAM_ARB = $8245;
    GL_DEBUG_CALLBACK_USER_PARAM_KHR = $8245;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_API_ARB = $8246;
    GL_DEBUG_SOURCE_API_KHR = $8246;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB = $8247;
    GL_DEBUG_SOURCE_WINDOW_SYSTEM_KHR = $8247;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_SHADER_COMPILER_ARB = $8248;
    GL_DEBUG_SOURCE_SHADER_COMPILER_KHR = $8248;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_THIRD_PARTY_ARB = $8249;
    GL_DEBUG_SOURCE_THIRD_PARTY_KHR = $8249;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_APPLICATION_ARB = $824A;
    GL_DEBUG_SOURCE_APPLICATION_KHR = $824A;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SOURCE_OTHER_ARB = $824B;
    GL_DEBUG_SOURCE_OTHER_KHR = $824B;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_ERROR_ARB = $824C;
    GL_DEBUG_TYPE_ERROR_KHR = $824C;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB = $824D;
    GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR = $824D;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB = $824E;
    GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR = $824E;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_PORTABILITY_ARB = $824F;
    GL_DEBUG_TYPE_PORTABILITY_KHR = $824F;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_PERFORMANCE_ARB = $8250;
    GL_DEBUG_TYPE_PERFORMANCE_KHR = $8250;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_OTHER_ARB = $8251;
    GL_DEBUG_TYPE_OTHER_KHR = $8251;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_LOSE_CONTEXT_ON_RESET = $8252;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_LOSE_CONTEXT_ON_RESET_ARB = $8252;
    GL_LOSE_CONTEXT_ON_RESET_EXT = $8252;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_LOSE_CONTEXT_ON_RESET_KHR = $8252;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_GUILTY_CONTEXT_RESET_ARB = $8253;
    GL_GUILTY_CONTEXT_RESET_EXT = $8253;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_GUILTY_CONTEXT_RESET_KHR = $8253;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_INNOCENT_CONTEXT_RESET_ARB = $8254;
    GL_INNOCENT_CONTEXT_RESET_EXT = $8254;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_INNOCENT_CONTEXT_RESET_KHR = $8254;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_UNKNOWN_CONTEXT_RESET_ARB = $8255;
    GL_UNKNOWN_CONTEXT_RESET_EXT = $8255;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_UNKNOWN_CONTEXT_RESET_KHR = $8255;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_RESET_NOTIFICATION_STRATEGY = $8256;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_RESET_NOTIFICATION_STRATEGY_ARB = $8256;
    GL_RESET_NOTIFICATION_STRATEGY_EXT = $8256;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_RESET_NOTIFICATION_STRATEGY_KHR = $8256;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_PROGRAM_SEPARABLE_EXT = $8258;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
    GL_ACTIVE_PROGRAM_EXT = $8259;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects  //->For the OpenGL ES version of EXT_separate_shader_objects
    GL_PROGRAM_PIPELINE_BINDING_EXT = $825A;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
    GL_MAX_VIEWPORTS = $825B;
    GL_MAX_VIEWPORTS_NV = $825B;   // api(gles2) //extension: GL_NV_viewport_array
    GL_MAX_VIEWPORTS_OES = $825B;   // api(gles2) //extension: GL_OES_viewport_array
    GL_VIEWPORT_SUBPIXEL_BITS = $825C;
    GL_VIEWPORT_SUBPIXEL_BITS_EXT = $825C;
    GL_VIEWPORT_SUBPIXEL_BITS_NV = $825C;   // api(gles2) //extension: GL_NV_viewport_array
    GL_VIEWPORT_SUBPIXEL_BITS_OES = $825C;   // api(gles2) //extension: GL_OES_viewport_array
    GL_VIEWPORT_BOUNDS_RANGE = $825D;
    GL_VIEWPORT_BOUNDS_RANGE_EXT = $825D;
    GL_VIEWPORT_BOUNDS_RANGE_NV = $825D;   // api(gles2) //extension: GL_NV_viewport_array
    GL_VIEWPORT_BOUNDS_RANGE_OES = $825D;   // api(gles2) //extension: GL_OES_viewport_array
    GL_LAYER_PROVOKING_VERTEX_EXT = $825E;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_LAYER_PROVOKING_VERTEX_OES = $825E;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_VIEWPORT_INDEX_PROVOKING_VERTEX = $825F;
    GL_VIEWPORT_INDEX_PROVOKING_VERTEX_EXT = $825F;
    GL_VIEWPORT_INDEX_PROVOKING_VERTEX_NV = $825F;   // api(gles2) //extension: GL_NV_viewport_array
    GL_VIEWPORT_INDEX_PROVOKING_VERTEX_OES = $825F;   // api(gles2) //extension: GL_OES_viewport_array
    GL_UNDEFINED_VERTEX = $8260;
    GL_UNDEFINED_VERTEX_EXT = $8260;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_UNDEFINED_VERTEX_OES = $8260;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_NO_RESET_NOTIFICATION = $8261;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_NO_RESET_NOTIFICATION_ARB = $8261;
    GL_NO_RESET_NOTIFICATION_EXT = $8261;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_NO_RESET_NOTIFICATION_KHR = $8261;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_MAX_COMPUTE_SHARED_MEMORY_SIZE = $8262;
    GL_DEBUG_TYPE_MARKER_KHR = $8268;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_PUSH_GROUP_KHR = $8269;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_TYPE_POP_GROUP_KHR = $826A;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_NOTIFICATION_KHR = $826B;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_DEBUG_GROUP_STACK_DEPTH_KHR = $826C;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_GROUP_STACK_DEPTH_KHR = $826D;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_INTERNALFORMAT_SUPPORTED = $826F;
    GL_INTERNALFORMAT_PREFERRED = $8270;
    GL_INTERNALFORMAT_RED_SIZE = $8271;
    GL_INTERNALFORMAT_GREEN_SIZE = $8272;
    GL_INTERNALFORMAT_BLUE_SIZE = $8273;
    GL_INTERNALFORMAT_ALPHA_SIZE = $8274;
    GL_INTERNALFORMAT_DEPTH_SIZE = $8275;
    GL_INTERNALFORMAT_STENCIL_SIZE = $8276;
    GL_INTERNALFORMAT_SHARED_SIZE = $8277;
    GL_INTERNALFORMAT_RED_TYPE = $8278;
    GL_INTERNALFORMAT_GREEN_TYPE = $8279;
    GL_INTERNALFORMAT_BLUE_TYPE = $827A;
    GL_INTERNALFORMAT_ALPHA_TYPE = $827B;
    GL_INTERNALFORMAT_DEPTH_TYPE = $827C;
    GL_INTERNALFORMAT_STENCIL_TYPE = $827D;
    GL_MAX_WIDTH = $827E;
    GL_MAX_HEIGHT = $827F;
    GL_MAX_DEPTH = $8280;
    GL_MAX_LAYERS = $8281;
    GL_MAX_COMBINED_DIMENSIONS = $8282;
    GL_COLOR_COMPONENTS = $8283;
    GL_DEPTH_COMPONENTS = $8284;
    GL_STENCIL_COMPONENTS = $8285;
    GL_COLOR_RENDERABLE = $8286;
    GL_DEPTH_RENDERABLE = $8287;
    GL_STENCIL_RENDERABLE = $8288;
    GL_FRAMEBUFFER_RENDERABLE = $8289;
    GL_FRAMEBUFFER_RENDERABLE_LAYERED = $828A;
    GL_FRAMEBUFFER_BLEND = $828B;
    GL_READ_PIXELS = $828C;
    GL_READ_PIXELS_FORMAT = $828D;
    GL_READ_PIXELS_TYPE = $828E;
    GL_TEXTURE_IMAGE_FORMAT = $828F;
    GL_TEXTURE_IMAGE_TYPE = $8290;
    GL_GET_TEXTURE_IMAGE_FORMAT = $8291;
    GL_GET_TEXTURE_IMAGE_TYPE = $8292;
    GL_MIPMAP = $8293;
    GL_MANUAL_GENERATE_MIPMAP = $8294;
    GL_AUTO_GENERATE_MIPMAP = $8295;  //->Should be deprecated
    GL_COLOR_ENCODING = $8296;
    GL_SRGB_READ = $8297;
    GL_SRGB_WRITE = $8298;
    GL_SRGB_DECODE_ARB = $8299;
    GL_FILTER = $829A;
    GL_VERTEX_TEXTURE = $829B;
    GL_TESS_CONTROL_TEXTURE = $829C;
    GL_TESS_EVALUATION_TEXTURE = $829D;
    GL_GEOMETRY_TEXTURE = $829E;
    GL_FRAGMENT_TEXTURE = $829F;
    GL_COMPUTE_TEXTURE = $82A0;
    GL_TEXTURE_SHADOW = $82A1;
    GL_TEXTURE_GATHER = $82A2;
    GL_TEXTURE_GATHER_SHADOW = $82A3;
    GL_SHADER_IMAGE_LOAD = $82A4;
    GL_SHADER_IMAGE_STORE = $82A5;
    GL_SHADER_IMAGE_ATOMIC = $82A6;
    GL_IMAGE_TEXEL_SIZE = $82A7;
    GL_IMAGE_COMPATIBILITY_CLASS = $82A8;
    GL_IMAGE_PIXEL_FORMAT = $82A9;
    GL_IMAGE_PIXEL_TYPE = $82AA;
    GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_TEST = $82AC;
    GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_TEST = $82AD;
    GL_SIMULTANEOUS_TEXTURE_AND_DEPTH_WRITE = $82AE;
    GL_SIMULTANEOUS_TEXTURE_AND_STENCIL_WRITE = $82AF;
    GL_TEXTURE_COMPRESSED_BLOCK_WIDTH = $82B1;
    GL_TEXTURE_COMPRESSED_BLOCK_HEIGHT = $82B2;
    GL_TEXTURE_COMPRESSED_BLOCK_SIZE = $82B3;
    GL_CLEAR_BUFFER = $82B4;
    GL_TEXTURE_VIEW = $82B5;
    GL_VIEW_COMPATIBILITY_CLASS = $82B6;
    GL_FULL_SUPPORT = $82B7;
    GL_CAVEAT_SUPPORT = $82B8;
    GL_IMAGE_CLASS_4_X_32 = $82B9;
    GL_IMAGE_CLASS_2_X_32 = $82BA;
    GL_IMAGE_CLASS_1_X_32 = $82BB;
    GL_IMAGE_CLASS_4_X_16 = $82BC;
    GL_IMAGE_CLASS_2_X_16 = $82BD;
    GL_IMAGE_CLASS_1_X_16 = $82BE;
    GL_IMAGE_CLASS_4_X_8 = $82BF;
    GL_IMAGE_CLASS_2_X_8 = $82C0;
    GL_IMAGE_CLASS_1_X_8 = $82C1;
    GL_IMAGE_CLASS_11_11_10 = $82C2;
    GL_IMAGE_CLASS_10_10_10_2 = $82C3;
    GL_VIEW_CLASS_128_BITS = $82C4;
    GL_VIEW_CLASS_96_BITS = $82C5;
    GL_VIEW_CLASS_64_BITS = $82C6;
    GL_VIEW_CLASS_48_BITS = $82C7;
    GL_VIEW_CLASS_32_BITS = $82C8;
    GL_VIEW_CLASS_24_BITS = $82C9;
    GL_VIEW_CLASS_16_BITS = $82CA;
    GL_VIEW_CLASS_8_BITS = $82CB;
    GL_VIEW_CLASS_S3TC_DXT1_RGB = $82CC;
    GL_VIEW_CLASS_S3TC_DXT1_RGBA = $82CD;
    GL_VIEW_CLASS_S3TC_DXT3_RGBA = $82CE;
    GL_VIEW_CLASS_S3TC_DXT5_RGBA = $82CF;
    GL_VIEW_CLASS_RGTC1_RED = $82D0;
    GL_VIEW_CLASS_RGTC2_RG = $82D1;
    GL_VIEW_CLASS_BPTC_UNORM = $82D2;
    GL_VIEW_CLASS_BPTC_FLOAT = $82D3;
    GL_TEXTURE_VIEW_MIN_LEVEL = $82DB;
    GL_TEXTURE_VIEW_MIN_LEVEL_EXT = $82DB;   // api(gles2) //extension: GL_EXT_texture_view
    GL_TEXTURE_VIEW_MIN_LEVEL_OES = $82DB;   // api(gles2) //extension: GL_OES_texture_view
    GL_TEXTURE_VIEW_NUM_LEVELS = $82DC;
    GL_TEXTURE_VIEW_NUM_LEVELS_EXT = $82DC;   // api(gles2) //extension: GL_EXT_texture_view
    GL_TEXTURE_VIEW_NUM_LEVELS_OES = $82DC;   // api(gles2) //extension: GL_OES_texture_view
    GL_TEXTURE_VIEW_MIN_LAYER = $82DD;
    GL_TEXTURE_VIEW_MIN_LAYER_EXT = $82DD;   // api(gles2) //extension: GL_EXT_texture_view
    GL_TEXTURE_VIEW_MIN_LAYER_OES = $82DD;   // api(gles2) //extension: GL_OES_texture_view
    GL_TEXTURE_VIEW_NUM_LAYERS = $82DE;
    GL_TEXTURE_VIEW_NUM_LAYERS_EXT = $82DE;   // api(gles2) //extension: GL_EXT_texture_view
    GL_TEXTURE_VIEW_NUM_LAYERS_OES = $82DE;   // api(gles2) //extension: GL_OES_texture_view
    GL_TEXTURE_IMMUTABLE_LEVELS = $82DF;   // api(gles2) //extension: GL_EXT_texture_view
    GL_BUFFER_KHR = $82E0;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_SHADER_KHR = $82E1;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_PROGRAM_KHR = $82E2;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_QUERY_KHR = $82E3;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_PROGRAM_PIPELINE_KHR = $82E4;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_VERTEX_ATTRIB_STRIDE = $82E5;
    GL_SAMPLER_KHR = $82E6;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DISPLAY_LIST = $82E7;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_LABEL_LENGTH_KHR = $82E8;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_NUM_SHADING_LANGUAGE_VERSIONS = $82E9;
    GL_QUERY_TARGET = $82EA;
    GL_TRANSFORM_FEEDBACK_OVERFLOW = $82EC;
    GL_TRANSFORM_FEEDBACK_OVERFLOW_ARB = $82EC;
    GL_TRANSFORM_FEEDBACK_STREAM_OVERFLOW = $82ED;
    GL_TRANSFORM_FEEDBACK_STREAM_OVERFLOW_ARB = $82ED;
    GL_VERTICES_SUBMITTED = $82EE;
    GL_VERTICES_SUBMITTED_ARB = $82EE;
    GL_PRIMITIVES_SUBMITTED = $82EF;
    GL_PRIMITIVES_SUBMITTED_ARB = $82EF;
    GL_VERTEX_SHADER_INVOCATIONS = $82F0;
    GL_VERTEX_SHADER_INVOCATIONS_ARB = $82F0;
    GL_TESS_CONTROL_SHADER_PATCHES = $82F1;
    GL_TESS_CONTROL_SHADER_PATCHES_ARB = $82F1;
    GL_TESS_EVALUATION_SHADER_INVOCATIONS = $82F2;
    GL_TESS_EVALUATION_SHADER_INVOCATIONS_ARB = $82F2;
    GL_GEOMETRY_SHADER_PRIMITIVES_EMITTED = $82F3;
    GL_GEOMETRY_SHADER_PRIMITIVES_EMITTED_ARB = $82F3;
    GL_FRAGMENT_SHADER_INVOCATIONS = $82F4;
    GL_FRAGMENT_SHADER_INVOCATIONS_ARB = $82F4;
    GL_COMPUTE_SHADER_INVOCATIONS = $82F5;
    GL_COMPUTE_SHADER_INVOCATIONS_ARB = $82F5;
    GL_CLIPPING_INPUT_PRIMITIVES = $82F6;
    GL_CLIPPING_INPUT_PRIMITIVES_ARB = $82F6;
    GL_CLIPPING_OUTPUT_PRIMITIVES = $82F7;
    GL_CLIPPING_OUTPUT_PRIMITIVES_ARB = $82F7;
    GL_SPARSE_BUFFER_PAGE_SIZE_ARB = $82F8;
    GL_MAX_CULL_DISTANCES = $82F9;
    GL_MAX_CULL_DISTANCES_EXT = $82F9;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_MAX_COMBINED_CLIP_AND_CULL_DISTANCES = $82FA;
    GL_MAX_COMBINED_CLIP_AND_CULL_DISTANCES_EXT = $82FA;   // api(gles2) //extension: GL_EXT_clip_cull_distance
    GL_CONTEXT_RELEASE_BEHAVIOR = $82FB;   // api(gl|glcore|gles2) //extension: GL_KHR_context_flush_control
    GL_CONTEXT_RELEASE_BEHAVIOR_KHR = $82FB;   // api(gl|glcore|gles2) //extension: GL_KHR_context_flush_control
    GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH = $82FC;   // api(gl|glcore|gles2) //extension: GL_KHR_context_flush_control
    GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH_KHR = $82FC;   // api(gl|glcore|gles2) //extension: GL_KHR_context_flush_control
    GL_ROBUST_GPU_TIMEOUT_MS_KHR = $82FD;  //->Reserved for future
    GL_DEPTH_PASS_INSTRUMENT_SGIX = $8310;
    GL_DEPTH_PASS_INSTRUMENT_COUNTERS_SGIX = $8311;
    GL_DEPTH_PASS_INSTRUMENT_MAX_SGIX = $8312;
    GL_FRAGMENTS_INSTRUMENT_SGIX = $8313;
    GL_FRAGMENTS_INSTRUMENT_COUNTERS_SGIX = $8314;
    GL_FRAGMENTS_INSTRUMENT_MAX_SGIX = $8315;
    GL_CONVOLUTION_HINT_SGIX = $8316;
    GL_YCRCB_SGIX = $8318;
    GL_YCRCBA_SGIX = $8319;
    GL_UNPACK_COMPRESSED_SIZE_SGIX = $831A;
    GL_PACK_MAX_COMPRESSED_SIZE_SGIX = $831B;
    GL_PACK_COMPRESSED_SIZE_SGIX = $831C;
    GL_SLIM8U_SGIX = $831D;
    GL_SLIM10U_SGIX = $831E;
    GL_SLIM12S_SGIX = $831F;
    GL_ALPHA_MIN_SGIX = $8320;
    GL_ALPHA_MAX_SGIX = $8321;
    GL_SCALEBIAS_HINT_SGIX = $8322;
    GL_ASYNC_MARKER_SGIX = $8329;
    GL_PIXEL_TEX_GEN_MODE_SGIX = $832B;
    GL_ASYNC_HISTOGRAM_SGIX = $832C;
    GL_MAX_ASYNC_HISTOGRAM_SGIX = $832D;
    GL_PIXEL_TRANSFORM_2D_EXT = $8330;
    GL_PIXEL_MAG_FILTER_EXT = $8331;
    GL_PIXEL_MIN_FILTER_EXT = $8332;
    GL_PIXEL_CUBIC_WEIGHT_EXT = $8333;
    GL_CUBIC_EXT = $8334;
    GL_AVERAGE_EXT = $8335;
    GL_PIXEL_TRANSFORM_2D_STACK_DEPTH_EXT = $8336;
    GL_MAX_PIXEL_TRANSFORM_2D_STACK_DEPTH_EXT = $8337;
    GL_PIXEL_TRANSFORM_2D_MATRIX_EXT = $8338;
    GL_FRAGMENT_MATERIAL_EXT = $8349;
    GL_FRAGMENT_NORMAL_EXT = $834A;
    GL_FRAGMENT_COLOR_EXT = $834C;
    GL_ATTENUATION_EXT = $834D;
    GL_SHADOW_ATTENUATION_EXT = $834E;
    GL_TEXTURE_APPLICATION_MODE_EXT = $834F;
    GL_TEXTURE_LIGHT_EXT = $8350;
    GL_TEXTURE_MATERIAL_FACE_EXT = $8351;
    GL_TEXTURE_MATERIAL_PARAMETER_EXT = $8352;
    GL_PIXEL_TEXTURE_SGIS = $8353;
    GL_PIXEL_FRAGMENT_RGB_SOURCE_SGIS = $8354;
    GL_PIXEL_FRAGMENT_ALPHA_SOURCE_SGIS = $8355;
    GL_PIXEL_GROUP_COLOR_SGIS = $8356;
    GL_LINE_QUALITY_HINT_SGIX = $835B;
    GL_ASYNC_TEX_IMAGE_SGIX = $835C;
    GL_ASYNC_DRAW_PIXELS_SGIX = $835D;
    GL_ASYNC_READ_PIXELS_SGIX = $835E;
    GL_MAX_ASYNC_TEX_IMAGE_SGIX = $835F;
    GL_MAX_ASYNC_DRAW_PIXELS_SGIX = $8360;
    GL_MAX_ASYNC_READ_PIXELS_SGIX = $8361;
    GL_UNSIGNED_BYTE_2_3_3_REV = $8362;
    GL_UNSIGNED_BYTE_2_3_3_REV_EXT = $8362;
    GL_UNSIGNED_SHORT_5_6_5 = $8363;
    GL_UNSIGNED_SHORT_5_6_5_EXT = $8363;
    GL_UNSIGNED_SHORT_5_6_5_REV = $8364;
    GL_UNSIGNED_SHORT_5_6_5_REV_EXT = $8364;
    GL_UNSIGNED_SHORT_4_4_4_4_REV = $8365;
    GL_UNSIGNED_SHORT_4_4_4_4_REV_EXT = $8365;   // api(gles1|gles2) //extension: GL_EXT_read_format_bgra
    GL_UNSIGNED_SHORT_4_4_4_4_REV_IMG = $8365;   // api(gles1|gles2) //extension: GL_IMG_read_format
    GL_UNSIGNED_SHORT_1_5_5_5_REV = $8366;
    GL_UNSIGNED_SHORT_1_5_5_5_REV_EXT = $8366;   // api(gles1|gles2) //extension: GL_EXT_read_format_bgra
    GL_UNSIGNED_INT_8_8_8_8_REV = $8367;
    GL_UNSIGNED_INT_8_8_8_8_REV_EXT = $8367;
    GL_UNSIGNED_INT_2_10_10_10_REV_EXT = $8368;   // api(gles2) //extension: GL_EXT_texture_type_2_10_10_10_REV
    GL_TEXTURE_MAX_CLAMP_S_SGIX = $8369;
    GL_TEXTURE_MAX_CLAMP_T_SGIX = $836A;
    GL_TEXTURE_MAX_CLAMP_R_SGIX = $836B;
    GL_MIRRORED_REPEAT_ARB = $8370;
    GL_MIRRORED_REPEAT_IBM = $8370;
    GL_MIRRORED_REPEAT_OES = $8370;
    GL_RGB_S3TC = $83A0;
    GL_RGB4_S3TC = $83A1;
    GL_RGBA_S3TC = $83A2;
    GL_RGBA4_S3TC = $83A3;
    GL_RGBA_DXT5_S3TC = $83A4;
    GL_RGBA4_DXT5_S3TC = $83A5;
    GL_VERTEX_PRECLIP_SGIX = $83EE;
    GL_VERTEX_PRECLIP_HINT_SGIX = $83EF;
    GL_COMPRESSED_RGB_S3TC_DXT1_EXT = $83F0;   // api(gles1|gles2) //extension: GL_EXT_texture_compression_dxt1
    GL_COMPRESSED_RGBA_S3TC_DXT1_EXT = $83F1;   // api(gles1|gles2) //extension: GL_EXT_texture_compression_dxt1
    GL_COMPRESSED_RGBA_S3TC_DXT3_ANGLE = $83F2;   // api(gles2) //extension: GL_ANGLE_texture_compression_dxt3
    GL_COMPRESSED_RGBA_S3TC_DXT3_EXT = $83F2;   // api(gl|glcore|gles2|glsc2) //extension: GL_EXT_texture_compression_s3tc
    GL_COMPRESSED_RGBA_S3TC_DXT5_ANGLE = $83F3;   // api(gles2) //extension: GL_ANGLE_texture_compression_dxt5
    GL_COMPRESSED_RGBA_S3TC_DXT5_EXT = $83F3;   // api(gl|glcore|gles2|glsc2) //extension: GL_EXT_texture_compression_s3tc
    GL_PARALLEL_ARRAYS_INTEL = $83F4;
    GL_VERTEX_ARRAY_PARALLEL_POINTERS_INTEL = $83F5;
    GL_NORMAL_ARRAY_PARALLEL_POINTERS_INTEL = $83F6;
    GL_COLOR_ARRAY_PARALLEL_POINTERS_INTEL = $83F7;
    GL_TEXTURE_COORD_ARRAY_PARALLEL_POINTERS_INTEL = $83F8;
    GL_PERFQUERY_DONOT_FLUSH_INTEL = $83F9;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_FLUSH_INTEL = $83FA;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_WAIT_INTEL = $83FB;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_BLACKHOLE_RENDER_INTEL = $83FC;   // api(gl|glcore|gles2) //extension: GL_INTEL_blackhole_render
    GL_CONSERVATIVE_RASTERIZATION_INTEL = $83FE;   // api(gl|glcore|gles2) //extension: GL_INTEL_conservative_rasterization
    GL_TEXTURE_MEMORY_LAYOUT_INTEL = $83FF;
    GL_FRAGMENT_LIGHTING_SGIX = $8400;
    GL_FRAGMENT_COLOR_MATERIAL_SGIX = $8401;
    GL_FRAGMENT_COLOR_MATERIAL_FACE_SGIX = $8402;
    GL_FRAGMENT_COLOR_MATERIAL_PARAMETER_SGIX = $8403;
    GL_MAX_FRAGMENT_LIGHTS_SGIX = $8404;
    GL_MAX_ACTIVE_LIGHTS_SGIX = $8405;
    GL_CURRENT_RASTER_NORMAL_SGIX = $8406;
    GL_LIGHT_ENV_MODE_SGIX = $8407;
    GL_FRAGMENT_LIGHT_MODEL_LOCAL_VIEWER_SGIX = $8408;
    GL_FRAGMENT_LIGHT_MODEL_TWO_SIDE_SGIX = $8409;
    GL_FRAGMENT_LIGHT_MODEL_AMBIENT_SGIX = $840A;
    GL_FRAGMENT_LIGHT_MODEL_NORMAL_INTERPOLATION_SGIX = $840B;
    GL_FRAGMENT_LIGHT0_SGIX = $840C;
    GL_FRAGMENT_LIGHT1_SGIX = $840D;
    GL_FRAGMENT_LIGHT2_SGIX = $840E;
    GL_FRAGMENT_LIGHT3_SGIX = $840F;
    GL_FRAGMENT_LIGHT4_SGIX = $8410;
    GL_FRAGMENT_LIGHT5_SGIX = $8411;
    GL_FRAGMENT_LIGHT6_SGIX = $8412;
    GL_FRAGMENT_LIGHT7_SGIX = $8413;
    GL_PACK_RESAMPLE_SGIX = $842E;  //->Formerly 0x842C in SGI specfile
    GL_UNPACK_RESAMPLE_SGIX = $842F;  //->Formerly 0x842D in SGI specfile
    GL_RESAMPLE_DECIMATE_SGIX = $8430;  //->Formerly 0x8430 in SGI specfile
    GL_RESAMPLE_REPLICATE_SGIX = $8433;  //->Formerly 0x842E in SGI specfile
    GL_RESAMPLE_ZERO_FILL_SGIX = $8434;  //->Formerly 0x842F in SGI specfile
    GL_TANGENT_ARRAY_EXT = $8439;
    GL_BINORMAL_ARRAY_EXT = $843A;
    GL_CURRENT_TANGENT_EXT = $843B;
    GL_CURRENT_BINORMAL_EXT = $843C;
    GL_TANGENT_ARRAY_TYPE_EXT = $843E;
    GL_TANGENT_ARRAY_STRIDE_EXT = $843F;
    GL_BINORMAL_ARRAY_TYPE_EXT = $8440;
    GL_BINORMAL_ARRAY_STRIDE_EXT = $8441;
    GL_TANGENT_ARRAY_POINTER_EXT = $8442;
    GL_BINORMAL_ARRAY_POINTER_EXT = $8443;
    GL_MAP1_TANGENT_EXT = $8444;
    GL_MAP2_TANGENT_EXT = $8445;
    GL_MAP1_BINORMAL_EXT = $8446;
    GL_MAP2_BINORMAL_EXT = $8447;
    GL_NEAREST_CLIPMAP_NEAREST_SGIX = $844D;
    GL_NEAREST_CLIPMAP_LINEAR_SGIX = $844E;
    GL_LINEAR_CLIPMAP_NEAREST_SGIX = $844F;
    GL_FOG_COORDINATE_SOURCE = $8450;
    GL_FOG_COORDINATE_SOURCE_EXT = $8450;
    GL_FOG_COORD_SRC = $8450;
    GL_FOG_COORDINATE = $8451;
    GL_FOG_COORD = $8451;
    GL_FOG_COORDINATE_EXT = $8451;
    GL_FRAGMENT_DEPTH = $8452;
    GL_FRAGMENT_DEPTH_EXT = $8452;
    GL_CURRENT_FOG_COORDINATE = $8453;
    GL_CURRENT_FOG_COORD = $8453;
    GL_CURRENT_FOG_COORDINATE_EXT = $8453;
    GL_FOG_COORDINATE_ARRAY_TYPE = $8454;
    GL_FOG_COORDINATE_ARRAY_TYPE_EXT = $8454;
    GL_FOG_COORD_ARRAY_TYPE = $8454;
    GL_FOG_COORDINATE_ARRAY_STRIDE = $8455;
    GL_FOG_COORDINATE_ARRAY_STRIDE_EXT = $8455;
    GL_FOG_COORD_ARRAY_STRIDE = $8455;
    GL_FOG_COORDINATE_ARRAY_POINTER = $8456;
    GL_FOG_COORDINATE_ARRAY_POINTER_EXT = $8456;
    GL_FOG_COORD_ARRAY_POINTER = $8456;
    GL_FOG_COORDINATE_ARRAY = $8457;
    GL_FOG_COORDINATE_ARRAY_EXT = $8457;
    GL_FOG_COORD_ARRAY = $8457;
    GL_COLOR_SUM = $8458;
    GL_COLOR_SUM_ARB = $8458;
    GL_COLOR_SUM_EXT = $8458;
    GL_CURRENT_SECONDARY_COLOR = $8459;
    GL_CURRENT_SECONDARY_COLOR_EXT = $8459;
    GL_SECONDARY_COLOR_ARRAY_SIZE = $845A;
    GL_SECONDARY_COLOR_ARRAY_SIZE_EXT = $845A;
    GL_SECONDARY_COLOR_ARRAY_TYPE = $845B;
    GL_SECONDARY_COLOR_ARRAY_TYPE_EXT = $845B;
    GL_SECONDARY_COLOR_ARRAY_STRIDE = $845C;
    GL_SECONDARY_COLOR_ARRAY_STRIDE_EXT = $845C;
    GL_SECONDARY_COLOR_ARRAY_POINTER = $845D;
    GL_SECONDARY_COLOR_ARRAY_POINTER_EXT = $845D;
    GL_SECONDARY_COLOR_ARRAY = $845E;
    GL_SECONDARY_COLOR_ARRAY_EXT = $845E;
    GL_CURRENT_RASTER_SECONDARY_COLOR = $845F;
    GL_SCREEN_COORDINATES_REND = $8490;
    GL_INVERTED_SCREEN_W_REND = $8491;
    GL_TEXTURE0_ARB = $84C0;
    GL_TEXTURE1_ARB = $84C1;
    GL_TEXTURE2_ARB = $84C2;
    GL_TEXTURE3_ARB = $84C3;
    GL_TEXTURE4_ARB = $84C4;
    GL_TEXTURE5_ARB = $84C5;
    GL_TEXTURE6_ARB = $84C6;
    GL_TEXTURE7_ARB = $84C7;
    GL_TEXTURE8_ARB = $84C8;
    GL_TEXTURE9_ARB = $84C9;
    GL_TEXTURE10_ARB = $84CA;
    GL_TEXTURE11_ARB = $84CB;
    GL_TEXTURE12_ARB = $84CC;
    GL_TEXTURE13_ARB = $84CD;
    GL_TEXTURE14_ARB = $84CE;
    GL_TEXTURE15_ARB = $84CF;
    GL_TEXTURE16_ARB = $84D0;
    GL_TEXTURE17_ARB = $84D1;
    GL_TEXTURE18_ARB = $84D2;
    GL_TEXTURE19_ARB = $84D3;
    GL_TEXTURE20_ARB = $84D4;
    GL_TEXTURE21_ARB = $84D5;
    GL_TEXTURE22_ARB = $84D6;
    GL_TEXTURE23_ARB = $84D7;
    GL_TEXTURE24_ARB = $84D8;
    GL_TEXTURE25_ARB = $84D9;
    GL_TEXTURE26_ARB = $84DA;
    GL_TEXTURE27_ARB = $84DB;
    GL_TEXTURE28_ARB = $84DC;
    GL_TEXTURE29_ARB = $84DD;
    GL_TEXTURE30_ARB = $84DE;
    GL_TEXTURE31_ARB = $84DF;
    GL_ACTIVE_TEXTURE_ARB = $84E0;
    GL_CLIENT_ACTIVE_TEXTURE = $84E1;
    GL_CLIENT_ACTIVE_TEXTURE_ARB = $84E1;
    GL_MAX_TEXTURE_UNITS = $84E2;
    GL_MAX_TEXTURE_UNITS_ARB = $84E2;
    GL_TRANSPOSE_MODELVIEW_MATRIX = $84E3;
    GL_TRANSPOSE_MODELVIEW_MATRIX_ARB = $84E3;
    GL_PATH_TRANSPOSE_MODELVIEW_MATRIX_NV = $84E3;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSPOSE_PROJECTION_MATRIX = $84E4;
    GL_TRANSPOSE_PROJECTION_MATRIX_ARB = $84E4;
    GL_PATH_TRANSPOSE_PROJECTION_MATRIX_NV = $84E4;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSPOSE_TEXTURE_MATRIX = $84E5;
    GL_TRANSPOSE_TEXTURE_MATRIX_ARB = $84E5;
    GL_TRANSPOSE_COLOR_MATRIX = $84E6;
    GL_TRANSPOSE_COLOR_MATRIX_ARB = $84E6;
    GL_SUBTRACT = $84E7;
    GL_SUBTRACT_ARB = $84E7;
    GL_MAX_RENDERBUFFER_SIZE_EXT = $84E8;
    GL_MAX_RENDERBUFFER_SIZE_OES = $84E8;
    GL_COMPRESSED_ALPHA = $84E9;
    GL_COMPRESSED_ALPHA_ARB = $84E9;
    GL_COMPRESSED_LUMINANCE = $84EA;
    GL_COMPRESSED_LUMINANCE_ARB = $84EA;
    GL_COMPRESSED_LUMINANCE_ALPHA = $84EB;
    GL_COMPRESSED_LUMINANCE_ALPHA_ARB = $84EB;
    GL_COMPRESSED_INTENSITY = $84EC;
    GL_COMPRESSED_INTENSITY_ARB = $84EC;
    GL_COMPRESSED_RGB = $84ED;
    GL_COMPRESSED_RGB_ARB = $84ED;
    GL_COMPRESSED_RGBA = $84EE;
    GL_COMPRESSED_RGBA_ARB = $84EE;
    GL_TEXTURE_COMPRESSION_HINT = $84EF;
    GL_TEXTURE_COMPRESSION_HINT_ARB = $84EF;
    GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_CONTROL_SHADER = $84F0;
    GL_UNIFORM_BLOCK_REFERENCED_BY_TESS_EVALUATION_SHADER = $84F1;
    GL_ALL_COMPLETED_NV = $84F2;   // api(gl|gles1|gles2) //extension: GL_NV_fence
    GL_FENCE_STATUS_NV = $84F3;   // api(gl|gles1|gles2) //extension: GL_NV_fence
    GL_FENCE_CONDITION_NV = $84F4;   // api(gl|gles1|gles2) //extension: GL_NV_fence
    GL_TEXTURE_RECTANGLE = $84F5;
    GL_TEXTURE_RECTANGLE_ARB = $84F5;
    GL_TEXTURE_RECTANGLE_NV = $84F5;
    GL_TEXTURE_BINDING_RECTANGLE = $84F6;
    GL_TEXTURE_BINDING_RECTANGLE_ARB = $84F6;
    GL_TEXTURE_BINDING_RECTANGLE_NV = $84F6;
    GL_PROXY_TEXTURE_RECTANGLE = $84F7;
    GL_PROXY_TEXTURE_RECTANGLE_ARB = $84F7;
    GL_PROXY_TEXTURE_RECTANGLE_NV = $84F7;
    GL_MAX_RECTANGLE_TEXTURE_SIZE = $84F8;
    GL_MAX_RECTANGLE_TEXTURE_SIZE_ARB = $84F8;
    GL_MAX_RECTANGLE_TEXTURE_SIZE_NV = $84F8;
    GL_DEPTH_STENCIL_EXT = $84F9;
    GL_DEPTH_STENCIL_NV = $84F9;
    GL_DEPTH_STENCIL_OES = $84F9;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_UNSIGNED_INT_24_8 = $84FA;
    GL_UNSIGNED_INT_24_8_EXT = $84FA;
    GL_UNSIGNED_INT_24_8_NV = $84FA;
    GL_UNSIGNED_INT_24_8_OES = $84FA;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_MAX_TEXTURE_LOD_BIAS_EXT = $84FD;
    GL_TEXTURE_MAX_ANISOTROPY = $84FE;
    GL_TEXTURE_MAX_ANISOTROPY_EXT = $84FE;   // api(gl|gles1|gles2) //extension: GL_EXT_texture_filter_anisotropic
    GL_MAX_TEXTURE_MAX_ANISOTROPY = $84FF;
    GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT = $84FF;   // api(gl|gles1|gles2) //extension: GL_EXT_texture_filter_anisotropic
    GL_TEXTURE_FILTER_CONTROL = $8500;
    GL_TEXTURE_FILTER_CONTROL_EXT = $8500;
    GL_TEXTURE_LOD_BIAS = $8501;
    GL_TEXTURE_LOD_BIAS_EXT = $8501;
    GL_MODELVIEW1_STACK_DEPTH_EXT = $8502;
    GL_COMBINE4_NV = $8503;
    GL_MAX_SHININESS_NV = $8504;
    GL_MAX_SPOT_EXPONENT_NV = $8505;
    GL_MODELVIEW1_MATRIX_EXT = $8506;
    GL_INCR_WRAP_EXT = $8507;
    GL_INCR_WRAP_OES = $8507;
    GL_DECR_WRAP_EXT = $8508;
    GL_DECR_WRAP_OES = $8508;
    GL_VERTEX_WEIGHTING_EXT = $8509;
    GL_MODELVIEW1_ARB = $850A;
    GL_MODELVIEW1_EXT = $850A;
    GL_CURRENT_VERTEX_WEIGHT_EXT = $850B;
    GL_VERTEX_WEIGHT_ARRAY_EXT = $850C;
    GL_VERTEX_WEIGHT_ARRAY_SIZE_EXT = $850D;
    GL_VERTEX_WEIGHT_ARRAY_TYPE_EXT = $850E;
    GL_VERTEX_WEIGHT_ARRAY_STRIDE_EXT = $850F;
    GL_VERTEX_WEIGHT_ARRAY_POINTER_EXT = $8510;
    GL_NORMAL_MAP = $8511;
    GL_NORMAL_MAP_ARB = $8511;
    GL_NORMAL_MAP_EXT = $8511;
    GL_NORMAL_MAP_NV = $8511;
    GL_NORMAL_MAP_OES = $8511;
    GL_REFLECTION_MAP = $8512;
    GL_REFLECTION_MAP_ARB = $8512;
    GL_REFLECTION_MAP_EXT = $8512;
    GL_REFLECTION_MAP_NV = $8512;
    GL_REFLECTION_MAP_OES = $8512;
    GL_TEXTURE_CUBE_MAP_ARB = $8513;
    GL_TEXTURE_CUBE_MAP_EXT = $8513;
    GL_TEXTURE_CUBE_MAP_OES = $8513;
    GL_TEXTURE_BINDING_CUBE_MAP_ARB = $8514;
    GL_TEXTURE_BINDING_CUBE_MAP_EXT = $8514;
    GL_TEXTURE_BINDING_CUBE_MAP_OES = $8514;
    GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB = $8515;
    GL_TEXTURE_CUBE_MAP_POSITIVE_X_EXT = $8515;
    GL_TEXTURE_CUBE_MAP_POSITIVE_X_OES = $8515;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB = $8516;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X_EXT = $8516;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_X_OES = $8516;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB = $8517;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y_EXT = $8517;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Y_OES = $8517;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB = $8518;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_EXT = $8518;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_OES = $8518;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB = $8519;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z_EXT = $8519;
    GL_TEXTURE_CUBE_MAP_POSITIVE_Z_OES = $8519;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB = $851A;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_EXT = $851A;
    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_OES = $851A;
    GL_PROXY_TEXTURE_CUBE_MAP = $851B;
    GL_PROXY_TEXTURE_CUBE_MAP_ARB = $851B;
    GL_PROXY_TEXTURE_CUBE_MAP_EXT = $851B;
    GL_MAX_CUBE_MAP_TEXTURE_SIZE_ARB = $851C;
    GL_MAX_CUBE_MAP_TEXTURE_SIZE_EXT = $851C;
    GL_MAX_CUBE_MAP_TEXTURE_SIZE_OES = $851C;
    GL_VERTEX_ARRAY_RANGE_APPLE = $851D;
    GL_VERTEX_ARRAY_RANGE_NV = $851D;
    GL_VERTEX_ARRAY_RANGE_LENGTH_APPLE = $851E;
    GL_VERTEX_ARRAY_RANGE_LENGTH_NV = $851E;
    GL_VERTEX_ARRAY_RANGE_VALID_NV = $851F;
    GL_VERTEX_ARRAY_STORAGE_HINT_APPLE = $851F;
    GL_MAX_VERTEX_ARRAY_RANGE_ELEMENT_NV = $8520;
    GL_VERTEX_ARRAY_RANGE_POINTER_APPLE = $8521;
    GL_VERTEX_ARRAY_RANGE_POINTER_NV = $8521;
    GL_REGISTER_COMBINERS_NV = $8522;
    GL_VARIABLE_A_NV = $8523;
    GL_VARIABLE_B_NV = $8524;
    GL_VARIABLE_C_NV = $8525;
    GL_VARIABLE_D_NV = $8526;
    GL_VARIABLE_E_NV = $8527;
    GL_VARIABLE_F_NV = $8528;
    GL_VARIABLE_G_NV = $8529;
    GL_CONSTANT_COLOR0_NV = $852A;
    GL_CONSTANT_COLOR1_NV = $852B;
    GL_PRIMARY_COLOR_NV = $852C;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SECONDARY_COLOR_NV = $852D;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SPARE0_NV = $852E;
    GL_SPARE1_NV = $852F;
    GL_DISCARD_NV = $8530;
    GL_E_TIMES_F_NV = $8531;
    GL_SPARE0_PLUS_SECONDARY_COLOR_NV = $8532;
    GL_VERTEX_ARRAY_RANGE_WITHOUT_FLUSH_NV = $8533;
    GL_MULTISAMPLE_FILTER_HINT_NV = $8534;
    GL_PER_STAGE_CONSTANTS_NV = $8535;
    GL_UNSIGNED_IDENTITY_NV = $8536;
    GL_UNSIGNED_INVERT_NV = $8537;
    GL_EXPAND_NORMAL_NV = $8538;
    GL_EXPAND_NEGATE_NV = $8539;
    GL_HALF_BIAS_NORMAL_NV = $853A;
    GL_HALF_BIAS_NEGATE_NV = $853B;
    GL_SIGNED_IDENTITY_NV = $853C;
    GL_SIGNED_NEGATE_NV = $853D;
    GL_SCALE_BY_TWO_NV = $853E;
    GL_SCALE_BY_FOUR_NV = $853F;
    GL_SCALE_BY_ONE_HALF_NV = $8540;
    GL_BIAS_BY_NEGATIVE_ONE_HALF_NV = $8541;
    GL_COMBINER_INPUT_NV = $8542;
    GL_COMBINER_MAPPING_NV = $8543;
    GL_COMBINER_COMPONENT_USAGE_NV = $8544;
    GL_COMBINER_AB_DOT_PRODUCT_NV = $8545;
    GL_COMBINER_CD_DOT_PRODUCT_NV = $8546;
    GL_COMBINER_MUX_SUM_NV = $8547;
    GL_COMBINER_SCALE_NV = $8548;
    GL_COMBINER_BIAS_NV = $8549;
    GL_COMBINER_AB_OUTPUT_NV = $854A;
    GL_COMBINER_CD_OUTPUT_NV = $854B;
    GL_COMBINER_SUM_OUTPUT_NV = $854C;
    GL_MAX_GENERAL_COMBINERS_NV = $854D;
    GL_NUM_GENERAL_COMBINERS_NV = $854E;
    GL_COLOR_SUM_CLAMP_NV = $854F;
    GL_COMBINER0_NV = $8550;
    GL_COMBINER1_NV = $8551;
    GL_COMBINER2_NV = $8552;
    GL_COMBINER3_NV = $8553;
    GL_COMBINER4_NV = $8554;
    GL_COMBINER5_NV = $8555;
    GL_COMBINER6_NV = $8556;
    GL_COMBINER7_NV = $8557;
    GL_PRIMITIVE_RESTART_NV = $8558;
    GL_PRIMITIVE_RESTART_INDEX_NV = $8559;
    GL_FOG_DISTANCE_MODE_NV = $855A;
    GL_EYE_RADIAL_NV = $855B;
    GL_EYE_PLANE_ABSOLUTE_NV = $855C;
    GL_EMBOSS_LIGHT_NV = $855D;
    GL_EMBOSS_CONSTANT_NV = $855E;
    GL_EMBOSS_MAP_NV = $855F;
    GL_RED_MIN_CLAMP_INGR = $8560;
    GL_GREEN_MIN_CLAMP_INGR = $8561;
    GL_BLUE_MIN_CLAMP_INGR = $8562;
    GL_ALPHA_MIN_CLAMP_INGR = $8563;
    GL_RED_MAX_CLAMP_INGR = $8564;
    GL_GREEN_MAX_CLAMP_INGR = $8565;
    GL_BLUE_MAX_CLAMP_INGR = $8566;
    GL_ALPHA_MAX_CLAMP_INGR = $8567;
    GL_INTERLACE_READ_INGR = $8568;
    GL_COMBINE = $8570;
    GL_COMBINE_ARB = $8570;
    GL_COMBINE_EXT = $8570;
    GL_COMBINE_RGB = $8571;
    GL_COMBINE_RGB_ARB = $8571;
    GL_COMBINE_RGB_EXT = $8571;
    GL_COMBINE_ALPHA = $8572;
    GL_COMBINE_ALPHA_ARB = $8572;
    GL_COMBINE_ALPHA_EXT = $8572;
    GL_RGB_SCALE = $8573;
    GL_RGB_SCALE_ARB = $8573;
    GL_RGB_SCALE_EXT = $8573;
    GL_ADD_SIGNED = $8574;
    GL_ADD_SIGNED_ARB = $8574;
    GL_ADD_SIGNED_EXT = $8574;
    GL_INTERPOLATE = $8575;
    GL_INTERPOLATE_ARB = $8575;
    GL_INTERPOLATE_EXT = $8575;
    GL_CONSTANT = $8576;
    GL_CONSTANT_ARB = $8576;
    GL_CONSTANT_EXT = $8576;
    GL_CONSTANT_NV = $8576;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PRIMARY_COLOR = $8577;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PRIMARY_COLOR_ARB = $8577;
    GL_PRIMARY_COLOR_EXT = $8577;
    GL_PREVIOUS = $8578;
    GL_PREVIOUS_ARB = $8578;
    GL_PREVIOUS_EXT = $8578;
    GL_SOURCE0_RGB = $8580;
    GL_SOURCE0_RGB_ARB = $8580;
    GL_SOURCE0_RGB_EXT = $8580;
    GL_SRC0_RGB = $8580;
    GL_SOURCE1_RGB = $8581;
    GL_SOURCE1_RGB_ARB = $8581;
    GL_SOURCE1_RGB_EXT = $8581;
    GL_SRC1_RGB = $8581;
    GL_SOURCE2_RGB = $8582;
    GL_SOURCE2_RGB_ARB = $8582;
    GL_SOURCE2_RGB_EXT = $8582;
    GL_SRC2_RGB = $8582;
    GL_SOURCE3_RGB_NV = $8583;
    GL_SOURCE0_ALPHA = $8588;
    GL_SOURCE0_ALPHA_ARB = $8588;
    GL_SOURCE0_ALPHA_EXT = $8588;
    GL_SRC0_ALPHA = $8588;
    GL_SOURCE1_ALPHA = $8589;
    GL_SOURCE1_ALPHA_ARB = $8589;
    GL_SOURCE1_ALPHA_EXT = $8589;
    GL_SRC1_ALPHA = $8589;
    GL_SRC1_ALPHA_EXT = $8589;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_SOURCE2_ALPHA = $858A;
    GL_SOURCE2_ALPHA_ARB = $858A;
    GL_SOURCE2_ALPHA_EXT = $858A;
    GL_SRC2_ALPHA = $858A;
    GL_SOURCE3_ALPHA_NV = $858B;
    GL_OPERAND0_RGB = $8590;
    GL_OPERAND0_RGB_ARB = $8590;
    GL_OPERAND0_RGB_EXT = $8590;
    GL_OPERAND1_RGB = $8591;
    GL_OPERAND1_RGB_ARB = $8591;
    GL_OPERAND1_RGB_EXT = $8591;
    GL_OPERAND2_RGB = $8592;
    GL_OPERAND2_RGB_ARB = $8592;
    GL_OPERAND2_RGB_EXT = $8592;
    GL_OPERAND3_RGB_NV = $8593;
    GL_OPERAND0_ALPHA = $8598;
    GL_OPERAND0_ALPHA_ARB = $8598;
    GL_OPERAND0_ALPHA_EXT = $8598;
    GL_OPERAND1_ALPHA = $8599;
    GL_OPERAND1_ALPHA_ARB = $8599;
    GL_OPERAND1_ALPHA_EXT = $8599;
    GL_OPERAND2_ALPHA = $859A;
    GL_OPERAND2_ALPHA_ARB = $859A;
    GL_OPERAND2_ALPHA_EXT = $859A;
    GL_OPERAND3_ALPHA_NV = $859B;
    GL_PACK_SUBSAMPLE_RATE_SGIX = $85A0;
    GL_UNPACK_SUBSAMPLE_RATE_SGIX = $85A1;
    GL_PIXEL_SUBSAMPLE_4444_SGIX = $85A2;
    GL_PIXEL_SUBSAMPLE_2424_SGIX = $85A3;
    GL_PIXEL_SUBSAMPLE_4242_SGIX = $85A4;
    GL_PERTURB_EXT = $85AE;
    GL_TEXTURE_NORMAL_EXT = $85AF;
    GL_LIGHT_MODEL_SPECULAR_VECTOR_APPLE = $85B0;
    GL_TRANSFORM_HINT_APPLE = $85B1;
    GL_UNPACK_CLIENT_STORAGE_APPLE = $85B2;
    GL_BUFFER_OBJECT_APPLE = $85B3;
    GL_STORAGE_CLIENT_APPLE = $85B4;
    GL_VERTEX_ARRAY_BINDING_APPLE = $85B5;
    GL_VERTEX_ARRAY_BINDING_OES = $85B5;   // api(gles1|gles2) //extension: GL_OES_vertex_array_object
    GL_TEXTURE_RANGE_LENGTH_APPLE = $85B7;
    GL_TEXTURE_RANGE_POINTER_APPLE = $85B8;
    GL_YCBCR_422_APPLE = $85B9;
    GL_UNSIGNED_SHORT_8_8_APPLE = $85BA;   // api(gl|glcore|gles2) //extension: GL_APPLE_rgb_422
    GL_UNSIGNED_SHORT_8_8_MESA = $85BA;
    GL_UNSIGNED_SHORT_8_8_REV_APPLE = $85BB;   // api(gl|glcore|gles2) //extension: GL_APPLE_rgb_422
    GL_UNSIGNED_SHORT_8_8_REV_MESA = $85BB;
    GL_TEXTURE_STORAGE_HINT_APPLE = $85BC;
    GL_STORAGE_PRIVATE_APPLE = $85BD;
    GL_STORAGE_CACHED_APPLE = $85BE;
    GL_STORAGE_SHARED_APPLE = $85BF;
    GL_REPLACEMENT_CODE_ARRAY_SUN = $85C0;
    GL_REPLACEMENT_CODE_ARRAY_TYPE_SUN = $85C1;
    GL_REPLACEMENT_CODE_ARRAY_STRIDE_SUN = $85C2;
    GL_REPLACEMENT_CODE_ARRAY_POINTER_SUN = $85C3;
    GL_R1UI_V3F_SUN = $85C4;
    GL_R1UI_C4UB_V3F_SUN = $85C5;
    GL_R1UI_C3F_V3F_SUN = $85C6;
    GL_R1UI_N3F_V3F_SUN = $85C7;
    GL_R1UI_C4F_N3F_V3F_SUN = $85C8;
    GL_R1UI_T2F_V3F_SUN = $85C9;
    GL_R1UI_T2F_N3F_V3F_SUN = $85CA;
    GL_R1UI_T2F_C4F_N3F_V3F_SUN = $85CB;
    GL_SLICE_ACCUM_SUN = $85CC;
    GL_QUAD_MESH_SUN = $8614;
    GL_TRIANGLE_MESH_SUN = $8615;
    GL_VERTEX_PROGRAM_ARB = $8620;
    GL_VERTEX_PROGRAM_NV = $8620;
    GL_VERTEX_STATE_PROGRAM_NV = $8621;
    GL_VERTEX_ATTRIB_ARRAY_ENABLED_ARB = $8622;
    GL_ATTRIB_ARRAY_SIZE_NV = $8623;
    GL_VERTEX_ATTRIB_ARRAY_SIZE_ARB = $8623;
    GL_ATTRIB_ARRAY_STRIDE_NV = $8624;
    GL_VERTEX_ATTRIB_ARRAY_STRIDE_ARB = $8624;
    GL_ATTRIB_ARRAY_TYPE_NV = $8625;
    GL_VERTEX_ATTRIB_ARRAY_TYPE_ARB = $8625;
    GL_CURRENT_ATTRIB_NV = $8626;
    GL_CURRENT_VERTEX_ATTRIB_ARB = $8626;
    GL_PROGRAM_LENGTH_ARB = $8627;
    GL_PROGRAM_LENGTH_NV = $8627;
    GL_PROGRAM_STRING_ARB = $8628;
    GL_PROGRAM_STRING_NV = $8628;
    GL_MODELVIEW_PROJECTION_NV = $8629;
    GL_IDENTITY_NV = $862A;
    GL_INVERSE_NV = $862B;
    GL_TRANSPOSE_NV = $862C;
    GL_INVERSE_TRANSPOSE_NV = $862D;
    GL_MAX_PROGRAM_MATRIX_STACK_DEPTH_ARB = $862E;
    GL_MAX_TRACK_MATRIX_STACK_DEPTH_NV = $862E;
    GL_MAX_PROGRAM_MATRICES_ARB = $862F;
    GL_MAX_TRACK_MATRICES_NV = $862F;
    GL_MATRIX0_NV = $8630;
    GL_MATRIX1_NV = $8631;
    GL_MATRIX2_NV = $8632;
    GL_MATRIX3_NV = $8633;
    GL_MATRIX4_NV = $8634;
    GL_MATRIX5_NV = $8635;
    GL_MATRIX6_NV = $8636;
    GL_MATRIX7_NV = $8637;
    GL_CURRENT_MATRIX_STACK_DEPTH_ARB = $8640;
    GL_CURRENT_MATRIX_STACK_DEPTH_NV = $8640;
    GL_CURRENT_MATRIX_ARB = $8641;
    GL_CURRENT_MATRIX_NV = $8641;
    GL_VERTEX_PROGRAM_POINT_SIZE = $8642;
    GL_VERTEX_PROGRAM_POINT_SIZE_ARB = $8642;
    GL_VERTEX_PROGRAM_POINT_SIZE_NV = $8642;
    GL_PROGRAM_POINT_SIZE = $8642;
    GL_PROGRAM_POINT_SIZE_ARB = $8642;
    GL_PROGRAM_POINT_SIZE_EXT = $8642;
    GL_VERTEX_PROGRAM_TWO_SIDE = $8643;
    GL_VERTEX_PROGRAM_TWO_SIDE_ARB = $8643;
    GL_VERTEX_PROGRAM_TWO_SIDE_NV = $8643;
    GL_PROGRAM_PARAMETER_NV = $8644;
    GL_ATTRIB_ARRAY_POINTER_NV = $8645;
    GL_VERTEX_ATTRIB_ARRAY_POINTER_ARB = $8645;
    GL_PROGRAM_TARGET_NV = $8646;
    GL_PROGRAM_RESIDENT_NV = $8647;
    GL_TRACK_MATRIX_NV = $8648;
    GL_TRACK_MATRIX_TRANSFORM_NV = $8649;
    GL_VERTEX_PROGRAM_BINDING_NV = $864A;
    GL_PROGRAM_ERROR_POSITION_ARB = $864B;
    GL_PROGRAM_ERROR_POSITION_NV = $864B;
    GL_OFFSET_TEXTURE_RECTANGLE_NV = $864C;
    GL_OFFSET_TEXTURE_RECTANGLE_SCALE_NV = $864D;
    GL_DOT_PRODUCT_TEXTURE_RECTANGLE_NV = $864E;
    GL_DEPTH_CLAMP = $864F;
    GL_DEPTH_CLAMP_NV = $864F;
    GL_DEPTH_CLAMP_EXT = $864F;   // api(gles2) //extension: GL_EXT_depth_clamp
    GL_VERTEX_ATTRIB_ARRAY0_NV = $8650;
    GL_VERTEX_ATTRIB_ARRAY1_NV = $8651;
    GL_VERTEX_ATTRIB_ARRAY2_NV = $8652;
    GL_VERTEX_ATTRIB_ARRAY3_NV = $8653;
    GL_VERTEX_ATTRIB_ARRAY4_NV = $8654;
    GL_VERTEX_ATTRIB_ARRAY5_NV = $8655;
    GL_VERTEX_ATTRIB_ARRAY6_NV = $8656;
    GL_VERTEX_ATTRIB_ARRAY7_NV = $8657;
    GL_VERTEX_ATTRIB_ARRAY8_NV = $8658;
    GL_VERTEX_ATTRIB_ARRAY9_NV = $8659;
    GL_VERTEX_ATTRIB_ARRAY10_NV = $865A;
    GL_VERTEX_ATTRIB_ARRAY11_NV = $865B;
    GL_VERTEX_ATTRIB_ARRAY12_NV = $865C;
    GL_VERTEX_ATTRIB_ARRAY13_NV = $865D;
    GL_VERTEX_ATTRIB_ARRAY14_NV = $865E;
    GL_VERTEX_ATTRIB_ARRAY15_NV = $865F;
    GL_MAP1_VERTEX_ATTRIB0_4_NV = $8660;
    GL_MAP1_VERTEX_ATTRIB1_4_NV = $8661;
    GL_MAP1_VERTEX_ATTRIB2_4_NV = $8662;
    GL_MAP1_VERTEX_ATTRIB3_4_NV = $8663;
    GL_MAP1_VERTEX_ATTRIB4_4_NV = $8664;
    GL_MAP1_VERTEX_ATTRIB5_4_NV = $8665;
    GL_MAP1_VERTEX_ATTRIB6_4_NV = $8666;
    GL_MAP1_VERTEX_ATTRIB7_4_NV = $8667;
    GL_MAP1_VERTEX_ATTRIB8_4_NV = $8668;
    GL_MAP1_VERTEX_ATTRIB9_4_NV = $8669;
    GL_MAP1_VERTEX_ATTRIB10_4_NV = $866A;
    GL_MAP1_VERTEX_ATTRIB11_4_NV = $866B;
    GL_MAP1_VERTEX_ATTRIB12_4_NV = $866C;
    GL_MAP1_VERTEX_ATTRIB13_4_NV = $866D;
    GL_MAP1_VERTEX_ATTRIB14_4_NV = $866E;
    GL_MAP1_VERTEX_ATTRIB15_4_NV = $866F;
    GL_MAP2_VERTEX_ATTRIB0_4_NV = $8670;
    GL_MAP2_VERTEX_ATTRIB1_4_NV = $8671;
    GL_MAP2_VERTEX_ATTRIB2_4_NV = $8672;
    GL_MAP2_VERTEX_ATTRIB3_4_NV = $8673;
    GL_MAP2_VERTEX_ATTRIB4_4_NV = $8674;
    GL_MAP2_VERTEX_ATTRIB5_4_NV = $8675;
    GL_MAP2_VERTEX_ATTRIB6_4_NV = $8676;
    GL_MAP2_VERTEX_ATTRIB7_4_NV = $8677;
    GL_PROGRAM_BINDING_ARB = $8677;  //->NOT an alias. Accidental reuse of GL_MAP2_VERTEX_ATTRIB7_4_NV
    GL_MAP2_VERTEX_ATTRIB8_4_NV = $8678;
    GL_MAP2_VERTEX_ATTRIB9_4_NV = $8679;
    GL_MAP2_VERTEX_ATTRIB10_4_NV = $867A;
    GL_MAP2_VERTEX_ATTRIB11_4_NV = $867B;
    GL_MAP2_VERTEX_ATTRIB12_4_NV = $867C;
    GL_MAP2_VERTEX_ATTRIB13_4_NV = $867D;
    GL_MAP2_VERTEX_ATTRIB14_4_NV = $867E;
    GL_MAP2_VERTEX_ATTRIB15_4_NV = $867F;
    GL_TEXTURE_COMPRESSED_IMAGE_SIZE = $86A0;
    GL_TEXTURE_COMPRESSED_IMAGE_SIZE_ARB = $86A0;
    GL_TEXTURE_COMPRESSED_ARB = $86A1;
    GL_NUM_COMPRESSED_TEXTURE_FORMATS_ARB = $86A2;
    GL_COMPRESSED_TEXTURE_FORMATS_ARB = $86A3;
    GL_MAX_VERTEX_UNITS_ARB = $86A4;
    GL_MAX_VERTEX_UNITS_OES = $86A4;
    GL_ACTIVE_VERTEX_UNITS_ARB = $86A5;
    GL_WEIGHT_SUM_UNITY_ARB = $86A6;
    GL_VERTEX_BLEND_ARB = $86A7;
    GL_CURRENT_WEIGHT_ARB = $86A8;
    GL_WEIGHT_ARRAY_TYPE_ARB = $86A9;
    GL_WEIGHT_ARRAY_TYPE_OES = $86A9;
    GL_WEIGHT_ARRAY_STRIDE_ARB = $86AA;
    GL_WEIGHT_ARRAY_STRIDE_OES = $86AA;
    GL_WEIGHT_ARRAY_SIZE_ARB = $86AB;
    GL_WEIGHT_ARRAY_SIZE_OES = $86AB;
    GL_WEIGHT_ARRAY_POINTER_ARB = $86AC;
    GL_WEIGHT_ARRAY_POINTER_OES = $86AC;
    GL_WEIGHT_ARRAY_ARB = $86AD;
    GL_WEIGHT_ARRAY_OES = $86AD;
    GL_DOT3_RGB = $86AE;
    GL_DOT3_RGB_ARB = $86AE;
    GL_DOT3_RGBA = $86AF;
    GL_DOT3_RGBA_ARB = $86AF;
    GL_DOT3_RGBA_IMG = $86AF;
    GL_COMPRESSED_RGB_FXT1_3DFX = $86B0;
    GL_COMPRESSED_RGBA_FXT1_3DFX = $86B1;
    GL_MULTISAMPLE_3DFX = $86B2;
    GL_SAMPLE_BUFFERS_3DFX = $86B3;
    GL_SAMPLES_3DFX = $86B4;
    GL_EVAL_2D_NV = $86C0;
    GL_EVAL_TRIANGULAR_2D_NV = $86C1;
    GL_MAP_TESSELLATION_NV = $86C2;
    GL_MAP_ATTRIB_U_ORDER_NV = $86C3;
    GL_MAP_ATTRIB_V_ORDER_NV = $86C4;
    GL_EVAL_FRACTIONAL_TESSELLATION_NV = $86C5;
    GL_EVAL_VERTEX_ATTRIB0_NV = $86C6;
    GL_EVAL_VERTEX_ATTRIB1_NV = $86C7;
    GL_EVAL_VERTEX_ATTRIB2_NV = $86C8;
    GL_EVAL_VERTEX_ATTRIB3_NV = $86C9;
    GL_EVAL_VERTEX_ATTRIB4_NV = $86CA;
    GL_EVAL_VERTEX_ATTRIB5_NV = $86CB;
    GL_EVAL_VERTEX_ATTRIB6_NV = $86CC;
    GL_EVAL_VERTEX_ATTRIB7_NV = $86CD;
    GL_EVAL_VERTEX_ATTRIB8_NV = $86CE;
    GL_EVAL_VERTEX_ATTRIB9_NV = $86CF;
    GL_EVAL_VERTEX_ATTRIB10_NV = $86D0;
    GL_EVAL_VERTEX_ATTRIB11_NV = $86D1;
    GL_EVAL_VERTEX_ATTRIB12_NV = $86D2;
    GL_EVAL_VERTEX_ATTRIB13_NV = $86D3;
    GL_EVAL_VERTEX_ATTRIB14_NV = $86D4;
    GL_EVAL_VERTEX_ATTRIB15_NV = $86D5;
    GL_MAX_MAP_TESSELLATION_NV = $86D6;
    GL_MAX_RATIONAL_EVAL_ORDER_NV = $86D7;
    GL_MAX_PROGRAM_PATCH_ATTRIBS_NV = $86D8;
    GL_RGBA_UNSIGNED_DOT_PRODUCT_MAPPING_NV = $86D9;
    GL_UNSIGNED_INT_S8_S8_8_8_NV = $86DA;
    GL_UNSIGNED_INT_8_8_S8_S8_REV_NV = $86DB;
    GL_DSDT_MAG_INTENSITY_NV = $86DC;
    GL_SHADER_CONSISTENT_NV = $86DD;
    GL_TEXTURE_SHADER_NV = $86DE;
    GL_SHADER_OPERATION_NV = $86DF;
    GL_CULL_MODES_NV = $86E0;
    GL_OFFSET_TEXTURE_MATRIX_NV = $86E1;
    GL_OFFSET_TEXTURE_2D_MATRIX_NV = $86E1;
    GL_OFFSET_TEXTURE_SCALE_NV = $86E2;
    GL_OFFSET_TEXTURE_2D_SCALE_NV = $86E2;
    GL_OFFSET_TEXTURE_BIAS_NV = $86E3;
    GL_OFFSET_TEXTURE_2D_BIAS_NV = $86E3;
    GL_PREVIOUS_TEXTURE_INPUT_NV = $86E4;
    GL_CONST_EYE_NV = $86E5;
    GL_PASS_THROUGH_NV = $86E6;
    GL_CULL_FRAGMENT_NV = $86E7;
    GL_OFFSET_TEXTURE_2D_NV = $86E8;
    GL_DEPENDENT_AR_TEXTURE_2D_NV = $86E9;
    GL_DEPENDENT_GB_TEXTURE_2D_NV = $86EA;
    GL_SURFACE_STATE_NV = $86EB;
    GL_DOT_PRODUCT_NV = $86EC;
    GL_DOT_PRODUCT_DEPTH_REPLACE_NV = $86ED;
    GL_DOT_PRODUCT_TEXTURE_2D_NV = $86EE;
    GL_DOT_PRODUCT_TEXTURE_3D_NV = $86EF;
    GL_DOT_PRODUCT_TEXTURE_CUBE_MAP_NV = $86F0;
    GL_DOT_PRODUCT_DIFFUSE_CUBE_MAP_NV = $86F1;
    GL_DOT_PRODUCT_REFLECT_CUBE_MAP_NV = $86F2;
    GL_DOT_PRODUCT_CONST_EYE_REFLECT_CUBE_MAP_NV = $86F3;
    GL_HILO_NV = $86F4;
    GL_DSDT_NV = $86F5;
    GL_DSDT_MAG_NV = $86F6;
    GL_DSDT_MAG_VIB_NV = $86F7;
    GL_HILO16_NV = $86F8;
    GL_SIGNED_HILO_NV = $86F9;
    GL_SIGNED_HILO16_NV = $86FA;
    GL_SIGNED_RGBA_NV = $86FB;
    GL_SIGNED_RGBA8_NV = $86FC;
    GL_SURFACE_REGISTERED_NV = $86FD;
    GL_SIGNED_RGB_NV = $86FE;
    GL_SIGNED_RGB8_NV = $86FF;
    GL_SURFACE_MAPPED_NV = $8700;
    GL_SIGNED_LUMINANCE_NV = $8701;
    GL_SIGNED_LUMINANCE8_NV = $8702;
    GL_SIGNED_LUMINANCE_ALPHA_NV = $8703;
    GL_SIGNED_LUMINANCE8_ALPHA8_NV = $8704;
    GL_SIGNED_ALPHA_NV = $8705;
    GL_SIGNED_ALPHA8_NV = $8706;
    GL_SIGNED_INTENSITY_NV = $8707;
    GL_SIGNED_INTENSITY8_NV = $8708;
    GL_DSDT8_NV = $8709;
    GL_DSDT8_MAG8_NV = $870A;
    GL_DSDT8_MAG8_INTENSITY8_NV = $870B;
    GL_SIGNED_RGB_UNSIGNED_ALPHA_NV = $870C;
    GL_SIGNED_RGB8_UNSIGNED_ALPHA8_NV = $870D;
    GL_HI_SCALE_NV = $870E;
    GL_LO_SCALE_NV = $870F;
    GL_DS_SCALE_NV = $8710;
    GL_DT_SCALE_NV = $8711;
    GL_MAGNITUDE_SCALE_NV = $8712;
    GL_VIBRANCE_SCALE_NV = $8713;
    GL_HI_BIAS_NV = $8714;
    GL_LO_BIAS_NV = $8715;
    GL_DS_BIAS_NV = $8716;
    GL_DT_BIAS_NV = $8717;
    GL_MAGNITUDE_BIAS_NV = $8718;
    GL_VIBRANCE_BIAS_NV = $8719;
    GL_TEXTURE_BORDER_VALUES_NV = $871A;
    GL_TEXTURE_HI_SIZE_NV = $871B;
    GL_TEXTURE_LO_SIZE_NV = $871C;
    GL_TEXTURE_DS_SIZE_NV = $871D;
    GL_TEXTURE_DT_SIZE_NV = $871E;
    GL_TEXTURE_MAG_SIZE_NV = $871F;
    GL_MODELVIEW2_ARB = $8722;
    GL_MODELVIEW3_ARB = $8723;
    GL_MODELVIEW4_ARB = $8724;
    GL_MODELVIEW5_ARB = $8725;
    GL_MODELVIEW6_ARB = $8726;
    GL_MODELVIEW7_ARB = $8727;
    GL_MODELVIEW8_ARB = $8728;
    GL_MODELVIEW9_ARB = $8729;
    GL_MODELVIEW10_ARB = $872A;
    GL_MODELVIEW11_ARB = $872B;
    GL_MODELVIEW12_ARB = $872C;
    GL_MODELVIEW13_ARB = $872D;
    GL_MODELVIEW14_ARB = $872E;
    GL_MODELVIEW15_ARB = $872F;
    GL_MODELVIEW16_ARB = $8730;
    GL_MODELVIEW17_ARB = $8731;
    GL_MODELVIEW18_ARB = $8732;
    GL_MODELVIEW19_ARB = $8733;
    GL_MODELVIEW20_ARB = $8734;
    GL_MODELVIEW21_ARB = $8735;
    GL_MODELVIEW22_ARB = $8736;
    GL_MODELVIEW23_ARB = $8737;
    GL_MODELVIEW24_ARB = $8738;
    GL_MODELVIEW25_ARB = $8739;
    GL_MODELVIEW26_ARB = $873A;
    GL_MODELVIEW27_ARB = $873B;
    GL_MODELVIEW28_ARB = $873C;
    GL_MODELVIEW29_ARB = $873D;
    GL_MODELVIEW30_ARB = $873E;
    GL_MODELVIEW31_ARB = $873F;
    GL_DOT3_RGB_EXT = $8740;
    GL_Z400_BINARY_AMD = $8740;   // api(gles2) //extension: GL_AMD_program_binary_Z400  //->NOT an alias. Accidental reuse of GL_DOT3_RGB_EXT
    GL_DOT3_RGBA_EXT = $8741;
    GL_PROGRAM_BINARY_LENGTH_OES = $8741;   // api(gles2) //extension: GL_OES_get_program_binary  //->NOT an alias. Accidental reuse of GL_DOT3_RGBA_EXT
    GL_MIRROR_CLAMP_ATI = $8742;
    GL_MIRROR_CLAMP_EXT = $8742;
    GL_MIRROR_CLAMP_TO_EDGE = $8743;
    GL_MIRROR_CLAMP_TO_EDGE_ATI = $8743;
    GL_MIRROR_CLAMP_TO_EDGE_EXT = $8743;   // api(gles2) //extension: GL_EXT_texture_mirror_clamp_to_edge
    GL_MODULATE_ADD_ATI = $8744;
    GL_MODULATE_SIGNED_ADD_ATI = $8745;
    GL_MODULATE_SUBTRACT_ATI = $8746;
    GL_SET_AMD = $874A;
    GL_REPLACE_VALUE_AMD = $874B;
    GL_STENCIL_OP_VALUE_AMD = $874C;
    GL_STENCIL_BACK_OP_VALUE_AMD = $874D;
    GL_VERTEX_ATTRIB_ARRAY_LONG = $874E;
    GL_OCCLUSION_QUERY_EVENT_MASK_AMD = $874F;
    GL_DEPTH_STENCIL_MESA = $8750;
    GL_UNSIGNED_INT_24_8_MESA = $8751;
    GL_UNSIGNED_INT_8_24_REV_MESA = $8752;
    GL_UNSIGNED_SHORT_15_1_MESA = $8753;
    GL_UNSIGNED_SHORT_1_15_REV_MESA = $8754;
    GL_TRACE_MASK_MESA = $8755;
    GL_TRACE_NAME_MESA = $8756;
    GL_YCBCR_MESA = $8757;
    GL_PACK_INVERT_MESA = $8758;
    GL_DEBUG_OBJECT_MESA = $8759;  //->NOT an alias. Accidental reuse of GL_TEXTURE_1D_STACK_MESAX
    GL_TEXTURE_1D_STACK_MESAX = $8759;
    GL_DEBUG_PRINT_MESA = $875A;  //->NOT an alias. Accidental reuse of GL_TEXTURE_2D_STACK_MESAX
    GL_TEXTURE_2D_STACK_MESAX = $875A;
    GL_DEBUG_ASSERT_MESA = $875B;  //->NOT an alias. Accidental reuse of GL_PROXY_TEXTURE_1D_STACK_MESAX
    GL_PROXY_TEXTURE_1D_STACK_MESAX = $875B;
    GL_PROXY_TEXTURE_2D_STACK_MESAX = $875C;
    GL_TEXTURE_1D_STACK_BINDING_MESAX = $875D;
    GL_TEXTURE_2D_STACK_BINDING_MESAX = $875E;
    GL_PROGRAM_BINARY_FORMAT_MESA = $875F;   // api(gl|gles2) //extension: GL_MESA_program_binary_formats
    GL_STATIC_ATI = $8760;
    GL_DYNAMIC_ATI = $8761;
    GL_PRESERVE_ATI = $8762;
    GL_DISCARD_ATI = $8763;
    GL_BUFFER_SIZE_ARB = $8764;
    GL_OBJECT_BUFFER_SIZE_ATI = $8764;
    GL_BUFFER_USAGE_ARB = $8765;
    GL_OBJECT_BUFFER_USAGE_ATI = $8765;
    GL_ARRAY_OBJECT_BUFFER_ATI = $8766;
    GL_ARRAY_OBJECT_OFFSET_ATI = $8767;
    GL_ELEMENT_ARRAY_ATI = $8768;
    GL_ELEMENT_ARRAY_TYPE_ATI = $8769;
    GL_ELEMENT_ARRAY_POINTER_ATI = $876A;
    GL_MAX_VERTEX_STREAMS_ATI = $876B;
    GL_VERTEX_STREAM0_ATI = $876C;
    GL_VERTEX_STREAM1_ATI = $876D;
    GL_VERTEX_STREAM2_ATI = $876E;
    GL_VERTEX_STREAM3_ATI = $876F;
    GL_VERTEX_STREAM4_ATI = $8770;
    GL_VERTEX_STREAM5_ATI = $8771;
    GL_VERTEX_STREAM6_ATI = $8772;
    GL_VERTEX_STREAM7_ATI = $8773;
    GL_VERTEX_SOURCE_ATI = $8774;
    GL_BUMP_ROT_MATRIX_ATI = $8775;
    GL_BUMP_ROT_MATRIX_SIZE_ATI = $8776;
    GL_BUMP_NUM_TEX_UNITS_ATI = $8777;
    GL_BUMP_TEX_UNITS_ATI = $8778;
    GL_DUDV_ATI = $8779;
    GL_DU8DV8_ATI = $877A;
    GL_BUMP_ENVMAP_ATI = $877B;
    GL_BUMP_TARGET_ATI = $877C;
    GL_VERTEX_SHADER_EXT = $8780;
    GL_VERTEX_SHADER_BINDING_EXT = $8781;
    GL_OP_INDEX_EXT = $8782;
    GL_OP_NEGATE_EXT = $8783;
    GL_OP_DOT3_EXT = $8784;
    GL_OP_DOT4_EXT = $8785;
    GL_OP_MUL_EXT = $8786;
    GL_OP_ADD_EXT = $8787;
    GL_OP_MADD_EXT = $8788;
    GL_OP_FRAC_EXT = $8789;
    GL_OP_MAX_EXT = $878A;
    GL_OP_MIN_EXT = $878B;
    GL_OP_SET_GE_EXT = $878C;
    GL_OP_SET_LT_EXT = $878D;
    GL_OP_CLAMP_EXT = $878E;
    GL_OP_FLOOR_EXT = $878F;
    GL_OP_ROUND_EXT = $8790;
    GL_OP_EXP_BASE_2_EXT = $8791;
    GL_OP_LOG_BASE_2_EXT = $8792;
    GL_OP_POWER_EXT = $8793;
    GL_OP_RECIP_EXT = $8794;
    GL_OP_RECIP_SQRT_EXT = $8795;
    GL_OP_SUB_EXT = $8796;
    GL_OP_CROSS_PRODUCT_EXT = $8797;
    GL_OP_MULTIPLY_MATRIX_EXT = $8798;
    GL_OP_MOV_EXT = $8799;
    GL_OUTPUT_VERTEX_EXT = $879A;
    GL_OUTPUT_COLOR0_EXT = $879B;
    GL_OUTPUT_COLOR1_EXT = $879C;
    GL_OUTPUT_TEXTURE_COORD0_EXT = $879D;
    GL_OUTPUT_TEXTURE_COORD1_EXT = $879E;
    GL_OUTPUT_TEXTURE_COORD2_EXT = $879F;
    GL_OUTPUT_TEXTURE_COORD3_EXT = $87A0;
    GL_OUTPUT_TEXTURE_COORD4_EXT = $87A1;
    GL_OUTPUT_TEXTURE_COORD5_EXT = $87A2;
    GL_OUTPUT_TEXTURE_COORD6_EXT = $87A3;
    GL_OUTPUT_TEXTURE_COORD7_EXT = $87A4;
    GL_OUTPUT_TEXTURE_COORD8_EXT = $87A5;
    GL_OUTPUT_TEXTURE_COORD9_EXT = $87A6;
    GL_OUTPUT_TEXTURE_COORD10_EXT = $87A7;
    GL_OUTPUT_TEXTURE_COORD11_EXT = $87A8;
    GL_OUTPUT_TEXTURE_COORD12_EXT = $87A9;
    GL_OUTPUT_TEXTURE_COORD13_EXT = $87AA;
    GL_OUTPUT_TEXTURE_COORD14_EXT = $87AB;
    GL_OUTPUT_TEXTURE_COORD15_EXT = $87AC;
    GL_OUTPUT_TEXTURE_COORD16_EXT = $87AD;
    GL_OUTPUT_TEXTURE_COORD17_EXT = $87AE;
    GL_OUTPUT_TEXTURE_COORD18_EXT = $87AF;
    GL_OUTPUT_TEXTURE_COORD19_EXT = $87B0;
    GL_OUTPUT_TEXTURE_COORD20_EXT = $87B1;
    GL_OUTPUT_TEXTURE_COORD21_EXT = $87B2;
    GL_OUTPUT_TEXTURE_COORD22_EXT = $87B3;
    GL_OUTPUT_TEXTURE_COORD23_EXT = $87B4;
    GL_OUTPUT_TEXTURE_COORD24_EXT = $87B5;
    GL_OUTPUT_TEXTURE_COORD25_EXT = $87B6;
    GL_OUTPUT_TEXTURE_COORD26_EXT = $87B7;
    GL_OUTPUT_TEXTURE_COORD27_EXT = $87B8;
    GL_OUTPUT_TEXTURE_COORD28_EXT = $87B9;
    GL_OUTPUT_TEXTURE_COORD29_EXT = $87BA;
    GL_OUTPUT_TEXTURE_COORD30_EXT = $87BB;
    GL_OUTPUT_TEXTURE_COORD31_EXT = $87BC;
    GL_OUTPUT_FOG_EXT = $87BD;
    GL_SCALAR_EXT = $87BE;
    GL_VECTOR_EXT = $87BF;
    GL_MATRIX_EXT = $87C0;
    GL_VARIANT_EXT = $87C1;
    GL_INVARIANT_EXT = $87C2;
    GL_LOCAL_CONSTANT_EXT = $87C3;
    GL_LOCAL_EXT = $87C4;
    GL_MAX_VERTEX_SHADER_INSTRUCTIONS_EXT = $87C5;
    GL_MAX_VERTEX_SHADER_VARIANTS_EXT = $87C6;
    GL_MAX_VERTEX_SHADER_INVARIANTS_EXT = $87C7;
    GL_MAX_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = $87C8;
    GL_MAX_VERTEX_SHADER_LOCALS_EXT = $87C9;
    GL_MAX_OPTIMIZED_VERTEX_SHADER_INSTRUCTIONS_EXT = $87CA;
    GL_MAX_OPTIMIZED_VERTEX_SHADER_VARIANTS_EXT = $87CB;
    GL_MAX_OPTIMIZED_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = $87CC;
    GL_MAX_OPTIMIZED_VERTEX_SHADER_INVARIANTS_EXT = $87CD;
    GL_MAX_OPTIMIZED_VERTEX_SHADER_LOCALS_EXT = $87CE;
    GL_VERTEX_SHADER_INSTRUCTIONS_EXT = $87CF;
    GL_VERTEX_SHADER_VARIANTS_EXT = $87D0;
    GL_VERTEX_SHADER_INVARIANTS_EXT = $87D1;
    GL_VERTEX_SHADER_LOCAL_CONSTANTS_EXT = $87D2;
    GL_VERTEX_SHADER_LOCALS_EXT = $87D3;
    GL_VERTEX_SHADER_OPTIMIZED_EXT = $87D4;
    GL_X_EXT = $87D5;
    GL_Y_EXT = $87D6;
    GL_Z_EXT = $87D7;
    GL_W_EXT = $87D8;
    GL_NEGATIVE_X_EXT = $87D9;
    GL_NEGATIVE_Y_EXT = $87DA;
    GL_NEGATIVE_Z_EXT = $87DB;
    GL_NEGATIVE_W_EXT = $87DC;
    GL_ZERO_EXT = $87DD;
    GL_ONE_EXT = $87DE;
    GL_NEGATIVE_ONE_EXT = $87DF;
    GL_NORMALIZED_RANGE_EXT = $87E0;
    GL_FULL_RANGE_EXT = $87E1;
    GL_CURRENT_VERTEX_EXT = $87E2;
    GL_MVP_MATRIX_EXT = $87E3;
    GL_VARIANT_VALUE_EXT = $87E4;
    GL_VARIANT_DATATYPE_EXT = $87E5;
    GL_VARIANT_ARRAY_STRIDE_EXT = $87E6;
    GL_VARIANT_ARRAY_TYPE_EXT = $87E7;
    GL_VARIANT_ARRAY_EXT = $87E8;
    GL_VARIANT_ARRAY_POINTER_EXT = $87E9;
    GL_INVARIANT_VALUE_EXT = $87EA;
    GL_INVARIANT_DATATYPE_EXT = $87EB;
    GL_LOCAL_CONSTANT_VALUE_EXT = $87EC;
    GL_LOCAL_CONSTANT_DATATYPE_EXT = $87ED;
    GL_ATC_RGBA_INTERPOLATED_ALPHA_AMD = $87EE;   // api(gles1|gles2) //extension: GL_AMD_compressed_ATC_texture
    GL_PN_TRIANGLES_ATI = $87F0;
    GL_MAX_PN_TRIANGLES_TESSELATION_LEVEL_ATI = $87F1;
    GL_PN_TRIANGLES_POINT_MODE_ATI = $87F2;
    GL_PN_TRIANGLES_NORMAL_MODE_ATI = $87F3;
    GL_PN_TRIANGLES_TESSELATION_LEVEL_ATI = $87F4;
    GL_PN_TRIANGLES_POINT_MODE_LINEAR_ATI = $87F5;
    GL_PN_TRIANGLES_POINT_MODE_CUBIC_ATI = $87F6;
    GL_PN_TRIANGLES_NORMAL_MODE_LINEAR_ATI = $87F7;
    GL_PN_TRIANGLES_NORMAL_MODE_QUADRATIC_ATI = $87F8;
    GL_3DC_X_AMD = $87F9;   // api(gles1|gles2) //extension: GL_AMD_compressed_3DC_texture
    GL_3DC_XY_AMD = $87FA;   // api(gles1|gles2) //extension: GL_AMD_compressed_3DC_texture
    GL_VBO_FREE_MEMORY_ATI = $87FB;
    GL_TEXTURE_FREE_MEMORY_ATI = $87FC;
    GL_RENDERBUFFER_FREE_MEMORY_ATI = $87FD;
    GL_NUM_PROGRAM_BINARY_FORMATS_OES = $87FE;   // api(gles2) //extension: GL_OES_get_program_binary
    GL_PROGRAM_BINARY_FORMATS_OES = $87FF;   // api(gles2) //extension: GL_OES_get_program_binary
    GL_STENCIL_BACK_FUNC_ATI = $8800;
    GL_STENCIL_BACK_FAIL_ATI = $8801;
    GL_STENCIL_BACK_PASS_DEPTH_FAIL_ATI = $8802;
    GL_STENCIL_BACK_PASS_DEPTH_PASS_ATI = $8803;
    GL_FRAGMENT_PROGRAM_ARB = $8804;
    GL_PROGRAM_ALU_INSTRUCTIONS_ARB = $8805;
    GL_PROGRAM_TEX_INSTRUCTIONS_ARB = $8806;
    GL_PROGRAM_TEX_INDIRECTIONS_ARB = $8807;
    GL_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB = $8808;
    GL_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB = $8809;
    GL_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB = $880A;
    GL_MAX_PROGRAM_ALU_INSTRUCTIONS_ARB = $880B;
    GL_MAX_PROGRAM_TEX_INSTRUCTIONS_ARB = $880C;
    GL_MAX_PROGRAM_TEX_INDIRECTIONS_ARB = $880D;
    GL_MAX_PROGRAM_NATIVE_ALU_INSTRUCTIONS_ARB = $880E;
    GL_MAX_PROGRAM_NATIVE_TEX_INSTRUCTIONS_ARB = $880F;
    GL_MAX_PROGRAM_NATIVE_TEX_INDIRECTIONS_ARB = $8810;
    GL_RGBA32F_ARB = $8814;
    GL_RGBA32F_EXT = $8814;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_RGBA_FLOAT32_APPLE = $8814;
    GL_RGBA_FLOAT32_ATI = $8814;
    GL_RGB32F_ARB = $8815;
    GL_RGB32F_EXT = $8815;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_RGB_FLOAT32_APPLE = $8815;
    GL_RGB_FLOAT32_ATI = $8815;
    GL_ALPHA32F_ARB = $8816;
    GL_ALPHA32F_EXT = $8816;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_ALPHA_FLOAT32_APPLE = $8816;
    GL_ALPHA_FLOAT32_ATI = $8816;
    GL_INTENSITY32F_ARB = $8817;
    GL_INTENSITY_FLOAT32_APPLE = $8817;
    GL_INTENSITY_FLOAT32_ATI = $8817;
    GL_LUMINANCE32F_ARB = $8818;
    GL_LUMINANCE32F_EXT = $8818;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_LUMINANCE_FLOAT32_APPLE = $8818;
    GL_LUMINANCE_FLOAT32_ATI = $8818;
    GL_LUMINANCE_ALPHA32F_ARB = $8819;
    GL_LUMINANCE_ALPHA32F_EXT = $8819;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_LUMINANCE_ALPHA_FLOAT32_APPLE = $8819;
    GL_LUMINANCE_ALPHA_FLOAT32_ATI = $8819;
    GL_RGBA16F_ARB = $881A;
    GL_RGBA16F_EXT = $881A;   // api(gles2) //extension: GL_EXT_color_buffer_half_float
    GL_RGBA_FLOAT16_APPLE = $881A;
    GL_RGBA_FLOAT16_ATI = $881A;
    GL_RGB16F_ARB = $881B;
    GL_RGB16F_EXT = $881B;   // api(gles2) //extension: GL_EXT_color_buffer_half_float
    GL_RGB_FLOAT16_APPLE = $881B;
    GL_RGB_FLOAT16_ATI = $881B;
    GL_ALPHA16F_ARB = $881C;
    GL_ALPHA16F_EXT = $881C;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_ALPHA_FLOAT16_APPLE = $881C;
    GL_ALPHA_FLOAT16_ATI = $881C;
    GL_INTENSITY16F_ARB = $881D;
    GL_INTENSITY_FLOAT16_APPLE = $881D;
    GL_INTENSITY_FLOAT16_ATI = $881D;
    GL_LUMINANCE16F_ARB = $881E;
    GL_LUMINANCE16F_EXT = $881E;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_LUMINANCE_FLOAT16_APPLE = $881E;
    GL_LUMINANCE_FLOAT16_ATI = $881E;
    GL_LUMINANCE_ALPHA16F_ARB = $881F;
    GL_LUMINANCE_ALPHA16F_EXT = $881F;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_LUMINANCE_ALPHA_FLOAT16_APPLE = $881F;
    GL_LUMINANCE_ALPHA_FLOAT16_ATI = $881F;
    GL_RGBA_FLOAT_MODE_ARB = $8820;
    GL_RGBA_FLOAT_MODE_ATI = $8820;
    GL_WRITEONLY_RENDERING_QCOM = $8823;   // api(gles1|gles2) //extension: GL_QCOM_writeonly_rendering
    GL_MAX_DRAW_BUFFERS_ARB = $8824;
    GL_MAX_DRAW_BUFFERS_ATI = $8824;
    GL_MAX_DRAW_BUFFERS_EXT = $8824;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_MAX_DRAW_BUFFERS_NV = $8824;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER0 = $8825;
    GL_DRAW_BUFFER0_ARB = $8825;
    GL_DRAW_BUFFER0_ATI = $8825;
    GL_DRAW_BUFFER0_EXT = $8825;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER0_NV = $8825;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER1 = $8826;
    GL_DRAW_BUFFER1_ARB = $8826;
    GL_DRAW_BUFFER1_ATI = $8826;
    GL_DRAW_BUFFER1_EXT = $8826;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER1_NV = $8826;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER2 = $8827;
    GL_DRAW_BUFFER2_ARB = $8827;
    GL_DRAW_BUFFER2_ATI = $8827;
    GL_DRAW_BUFFER2_EXT = $8827;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER2_NV = $8827;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER3 = $8828;
    GL_DRAW_BUFFER3_ARB = $8828;
    GL_DRAW_BUFFER3_ATI = $8828;
    GL_DRAW_BUFFER3_EXT = $8828;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER3_NV = $8828;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER4 = $8829;
    GL_DRAW_BUFFER4_ARB = $8829;
    GL_DRAW_BUFFER4_ATI = $8829;
    GL_DRAW_BUFFER4_EXT = $8829;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER4_NV = $8829;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER5 = $882A;
    GL_DRAW_BUFFER5_ARB = $882A;
    GL_DRAW_BUFFER5_ATI = $882A;
    GL_DRAW_BUFFER5_EXT = $882A;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER5_NV = $882A;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER6 = $882B;
    GL_DRAW_BUFFER6_ARB = $882B;
    GL_DRAW_BUFFER6_ATI = $882B;
    GL_DRAW_BUFFER6_EXT = $882B;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER6_NV = $882B;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER7 = $882C;
    GL_DRAW_BUFFER7_ARB = $882C;
    GL_DRAW_BUFFER7_ATI = $882C;
    GL_DRAW_BUFFER7_EXT = $882C;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER7_NV = $882C;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER8 = $882D;
    GL_DRAW_BUFFER8_ARB = $882D;
    GL_DRAW_BUFFER8_ATI = $882D;
    GL_DRAW_BUFFER8_EXT = $882D;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER8_NV = $882D;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER9 = $882E;
    GL_DRAW_BUFFER9_ARB = $882E;
    GL_DRAW_BUFFER9_ATI = $882E;
    GL_DRAW_BUFFER9_EXT = $882E;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER9_NV = $882E;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER10 = $882F;
    GL_DRAW_BUFFER10_ARB = $882F;
    GL_DRAW_BUFFER10_ATI = $882F;
    GL_DRAW_BUFFER10_EXT = $882F;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER10_NV = $882F;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER11 = $8830;
    GL_DRAW_BUFFER11_ARB = $8830;
    GL_DRAW_BUFFER11_ATI = $8830;
    GL_DRAW_BUFFER11_EXT = $8830;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER11_NV = $8830;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER12 = $8831;
    GL_DRAW_BUFFER12_ARB = $8831;
    GL_DRAW_BUFFER12_ATI = $8831;
    GL_DRAW_BUFFER12_EXT = $8831;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER12_NV = $8831;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER13 = $8832;
    GL_DRAW_BUFFER13_ARB = $8832;
    GL_DRAW_BUFFER13_ATI = $8832;
    GL_DRAW_BUFFER13_EXT = $8832;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER13_NV = $8832;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER14 = $8833;
    GL_DRAW_BUFFER14_ARB = $8833;
    GL_DRAW_BUFFER14_ATI = $8833;
    GL_DRAW_BUFFER14_EXT = $8833;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER14_NV = $8833;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DRAW_BUFFER15 = $8834;
    GL_DRAW_BUFFER15_ARB = $8834;
    GL_DRAW_BUFFER15_ATI = $8834;
    GL_DRAW_BUFFER15_EXT = $8834;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_DRAW_BUFFER15_NV = $8834;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_CLEAR_UNCLAMPED_VALUE_ATI = $8835;
    GL_COMPRESSED_LUMINANCE_ALPHA_3DC_ATI = $8837;  //->Defined by Mesa but not ATI
    GL_BLEND_EQUATION_ALPHA_EXT = $883D;
    GL_BLEND_EQUATION_ALPHA_OES = $883D;
    GL_SUBSAMPLE_DISTANCE_AMD = $883F;
    GL_MATRIX_PALETTE_ARB = $8840;
    GL_MATRIX_PALETTE_OES = $8840;
    GL_MAX_MATRIX_PALETTE_STACK_DEPTH_ARB = $8841;
    GL_MAX_PALETTE_MATRICES_ARB = $8842;
    GL_MAX_PALETTE_MATRICES_OES = $8842;
    GL_CURRENT_PALETTE_MATRIX_ARB = $8843;
    GL_CURRENT_PALETTE_MATRIX_OES = $8843;
    GL_MATRIX_INDEX_ARRAY_ARB = $8844;
    GL_MATRIX_INDEX_ARRAY_OES = $8844;
    GL_CURRENT_MATRIX_INDEX_ARB = $8845;
    GL_MATRIX_INDEX_ARRAY_SIZE_ARB = $8846;
    GL_MATRIX_INDEX_ARRAY_SIZE_OES = $8846;
    GL_MATRIX_INDEX_ARRAY_TYPE_ARB = $8847;
    GL_MATRIX_INDEX_ARRAY_TYPE_OES = $8847;
    GL_MATRIX_INDEX_ARRAY_STRIDE_ARB = $8848;
    GL_MATRIX_INDEX_ARRAY_STRIDE_OES = $8848;
    GL_MATRIX_INDEX_ARRAY_POINTER_ARB = $8849;
    GL_MATRIX_INDEX_ARRAY_POINTER_OES = $8849;
    GL_TEXTURE_DEPTH_SIZE = $884A;
    GL_TEXTURE_DEPTH_SIZE_ARB = $884A;
    GL_DEPTH_TEXTURE_MODE = $884B;
    GL_DEPTH_TEXTURE_MODE_ARB = $884B;
    GL_TEXTURE_COMPARE_MODE_ARB = $884C;
    GL_TEXTURE_COMPARE_MODE_EXT = $884C;   // api(gles2) //extension: GL_EXT_shadow_samplers
    GL_TEXTURE_COMPARE_FUNC_ARB = $884D;
    GL_TEXTURE_COMPARE_FUNC_EXT = $884D;   // api(gles2) //extension: GL_EXT_shadow_samplers
    GL_COMPARE_R_TO_TEXTURE = $884E;
    GL_COMPARE_R_TO_TEXTURE_ARB = $884E;
    GL_COMPARE_REF_DEPTH_TO_TEXTURE_EXT = $884E;
    GL_COMPARE_REF_TO_TEXTURE_EXT = $884E;   // api(gles2) //extension: GL_EXT_shadow_samplers
    GL_TEXTURE_CUBE_MAP_SEAMLESS = $884F;
    GL_OFFSET_PROJECTIVE_TEXTURE_2D_NV = $8850;
    GL_OFFSET_PROJECTIVE_TEXTURE_2D_SCALE_NV = $8851;
    GL_OFFSET_PROJECTIVE_TEXTURE_RECTANGLE_NV = $8852;
    GL_OFFSET_PROJECTIVE_TEXTURE_RECTANGLE_SCALE_NV = $8853;
    GL_OFFSET_HILO_TEXTURE_2D_NV = $8854;
    GL_OFFSET_HILO_TEXTURE_RECTANGLE_NV = $8855;
    GL_OFFSET_HILO_PROJECTIVE_TEXTURE_2D_NV = $8856;
    GL_OFFSET_HILO_PROJECTIVE_TEXTURE_RECTANGLE_NV = $8857;
    GL_DEPENDENT_HILO_TEXTURE_2D_NV = $8858;
    GL_DEPENDENT_RGB_TEXTURE_3D_NV = $8859;
    GL_DEPENDENT_RGB_TEXTURE_CUBE_MAP_NV = $885A;
    GL_DOT_PRODUCT_PASS_THROUGH_NV = $885B;
    GL_DOT_PRODUCT_TEXTURE_1D_NV = $885C;
    GL_DOT_PRODUCT_AFFINE_DEPTH_REPLACE_NV = $885D;
    GL_HILO8_NV = $885E;
    GL_SIGNED_HILO8_NV = $885F;
    GL_FORCE_BLUE_TO_ONE_NV = $8860;
    GL_POINT_SPRITE = $8861;
    GL_POINT_SPRITE_ARB = $8861;
    GL_POINT_SPRITE_NV = $8861;
    GL_POINT_SPRITE_OES = $8861;
    GL_COORD_REPLACE = $8862;
    GL_COORD_REPLACE_ARB = $8862;
    GL_COORD_REPLACE_NV = $8862;
    GL_COORD_REPLACE_OES = $8862;
    GL_POINT_SPRITE_R_MODE_NV = $8863;
    GL_PIXEL_COUNTER_BITS_NV = $8864;
    GL_QUERY_COUNTER_BITS = $8864;
    GL_QUERY_COUNTER_BITS_ARB = $8864;
    GL_QUERY_COUNTER_BITS_EXT = $8864;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_CURRENT_OCCLUSION_QUERY_ID_NV = $8865;
    GL_CURRENT_QUERY_ARB = $8865;
    GL_CURRENT_QUERY_EXT = $8865;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_PIXEL_COUNT_NV = $8866;
    GL_QUERY_RESULT_ARB = $8866;
    GL_QUERY_RESULT_EXT = $8866;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_PIXEL_COUNT_AVAILABLE_NV = $8867;
    GL_QUERY_RESULT_AVAILABLE_ARB = $8867;
    GL_QUERY_RESULT_AVAILABLE_EXT = $8867;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_MAX_FRAGMENT_PROGRAM_LOCAL_PARAMETERS_NV = $8868;
    GL_MAX_VERTEX_ATTRIBS_ARB = $8869;
    GL_VERTEX_ATTRIB_ARRAY_NORMALIZED_ARB = $886A;
    GL_MAX_TESS_CONTROL_INPUT_COMPONENTS = $886C;
    GL_MAX_TESS_CONTROL_INPUT_COMPONENTS_EXT = $886C;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_INPUT_COMPONENTS_OES = $886C;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS = $886D;
    GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS_EXT = $886D;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_INPUT_COMPONENTS_OES = $886D;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_DEPTH_STENCIL_TO_RGBA_NV = $886E;
    GL_DEPTH_STENCIL_TO_BGRA_NV = $886F;
    GL_FRAGMENT_PROGRAM_NV = $8870;
    GL_MAX_TEXTURE_COORDS = $8871;
    GL_MAX_TEXTURE_COORDS_ARB = $8871;
    GL_MAX_TEXTURE_COORDS_NV = $8871;
    GL_MAX_TEXTURE_IMAGE_UNITS_ARB = $8872;
    GL_MAX_TEXTURE_IMAGE_UNITS_NV = $8872;
    GL_FRAGMENT_PROGRAM_BINDING_NV = $8873;
    GL_PROGRAM_ERROR_STRING_ARB = $8874;
    GL_PROGRAM_ERROR_STRING_NV = $8874;
    GL_PROGRAM_FORMAT_ASCII_ARB = $8875;
    GL_PROGRAM_FORMAT_ARB = $8876;
    GL_WRITE_PIXEL_DATA_RANGE_NV = $8878;
    GL_READ_PIXEL_DATA_RANGE_NV = $8879;
    GL_WRITE_PIXEL_DATA_RANGE_LENGTH_NV = $887A;
    GL_READ_PIXEL_DATA_RANGE_LENGTH_NV = $887B;
    GL_WRITE_PIXEL_DATA_RANGE_POINTER_NV = $887C;
    GL_READ_PIXEL_DATA_RANGE_POINTER_NV = $887D;
    GL_GEOMETRY_SHADER_INVOCATIONS = $887F;
    GL_GEOMETRY_SHADER_INVOCATIONS_EXT = $887F;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_GEOMETRY_SHADER_INVOCATIONS_OES = $887F;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_FLOAT_R_NV = $8880;
    GL_FLOAT_RG_NV = $8881;
    GL_FLOAT_RGB_NV = $8882;
    GL_FLOAT_RGBA_NV = $8883;
    GL_FLOAT_R16_NV = $8884;
    GL_FLOAT_R32_NV = $8885;
    GL_FLOAT_RG16_NV = $8886;
    GL_FLOAT_RG32_NV = $8887;
    GL_FLOAT_RGB16_NV = $8888;
    GL_FLOAT_RGB32_NV = $8889;
    GL_FLOAT_RGBA16_NV = $888A;
    GL_FLOAT_RGBA32_NV = $888B;
    GL_TEXTURE_FLOAT_COMPONENTS_NV = $888C;
    GL_FLOAT_CLEAR_COLOR_VALUE_NV = $888D;
    GL_FLOAT_RGBA_MODE_NV = $888E;
    GL_TEXTURE_UNSIGNED_REMAP_MODE_NV = $888F;
    GL_DEPTH_BOUNDS_TEST_EXT = $8890;
    GL_DEPTH_BOUNDS_EXT = $8891;
    GL_ARRAY_BUFFER_ARB = $8892;
    GL_ELEMENT_ARRAY_BUFFER_ARB = $8893;
    GL_ARRAY_BUFFER_BINDING_ARB = $8894;
    GL_ELEMENT_ARRAY_BUFFER_BINDING_ARB = $8895;
    GL_VERTEX_ARRAY_BUFFER_BINDING = $8896;
    GL_VERTEX_ARRAY_BUFFER_BINDING_ARB = $8896;
    GL_NORMAL_ARRAY_BUFFER_BINDING = $8897;
    GL_NORMAL_ARRAY_BUFFER_BINDING_ARB = $8897;
    GL_COLOR_ARRAY_BUFFER_BINDING = $8898;
    GL_COLOR_ARRAY_BUFFER_BINDING_ARB = $8898;
    GL_INDEX_ARRAY_BUFFER_BINDING = $8899;
    GL_INDEX_ARRAY_BUFFER_BINDING_ARB = $8899;
    GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING = $889A;
    GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING_ARB = $889A;
    GL_EDGE_FLAG_ARRAY_BUFFER_BINDING = $889B;
    GL_EDGE_FLAG_ARRAY_BUFFER_BINDING_ARB = $889B;
    GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING = $889C;
    GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING_ARB = $889C;
    GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING_ARB = $889D;
    GL_FOG_COORDINATE_ARRAY_BUFFER_BINDING = $889D;
    GL_FOG_COORD_ARRAY_BUFFER_BINDING = $889D;
    GL_WEIGHT_ARRAY_BUFFER_BINDING = $889E;
    GL_WEIGHT_ARRAY_BUFFER_BINDING_ARB = $889E;
    GL_WEIGHT_ARRAY_BUFFER_BINDING_OES = $889E;
    GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ARB = $889F;
    GL_PROGRAM_INSTRUCTIONS_ARB = $88A0;
    GL_MAX_PROGRAM_INSTRUCTIONS_ARB = $88A1;
    GL_PROGRAM_NATIVE_INSTRUCTIONS_ARB = $88A2;
    GL_MAX_PROGRAM_NATIVE_INSTRUCTIONS_ARB = $88A3;
    GL_PROGRAM_TEMPORARIES_ARB = $88A4;
    GL_MAX_PROGRAM_TEMPORARIES_ARB = $88A5;
    GL_PROGRAM_NATIVE_TEMPORARIES_ARB = $88A6;
    GL_MAX_PROGRAM_NATIVE_TEMPORARIES_ARB = $88A7;
    GL_PROGRAM_PARAMETERS_ARB = $88A8;
    GL_MAX_PROGRAM_PARAMETERS_ARB = $88A9;
    GL_PROGRAM_NATIVE_PARAMETERS_ARB = $88AA;
    GL_MAX_PROGRAM_NATIVE_PARAMETERS_ARB = $88AB;
    GL_PROGRAM_ATTRIBS_ARB = $88AC;
    GL_MAX_PROGRAM_ATTRIBS_ARB = $88AD;
    GL_PROGRAM_NATIVE_ATTRIBS_ARB = $88AE;
    GL_MAX_PROGRAM_NATIVE_ATTRIBS_ARB = $88AF;
    GL_PROGRAM_ADDRESS_REGISTERS_ARB = $88B0;
    GL_MAX_PROGRAM_ADDRESS_REGISTERS_ARB = $88B1;
    GL_PROGRAM_NATIVE_ADDRESS_REGISTERS_ARB = $88B2;
    GL_MAX_PROGRAM_NATIVE_ADDRESS_REGISTERS_ARB = $88B3;
    GL_MAX_PROGRAM_LOCAL_PARAMETERS_ARB = $88B4;
    GL_MAX_PROGRAM_ENV_PARAMETERS_ARB = $88B5;
    GL_PROGRAM_UNDER_NATIVE_LIMITS_ARB = $88B6;
    GL_TRANSPOSE_CURRENT_MATRIX_ARB = $88B7;
    GL_READ_ONLY_ARB = $88B8;
    GL_WRITE_ONLY_ARB = $88B9;
    GL_WRITE_ONLY_OES = $88B9;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
    GL_READ_WRITE_ARB = $88BA;
    GL_BUFFER_ACCESS = $88BB;
    GL_BUFFER_ACCESS_ARB = $88BB;
    GL_BUFFER_ACCESS_OES = $88BB;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
    GL_BUFFER_MAPPED_ARB = $88BC;
    GL_BUFFER_MAPPED_OES = $88BC;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
    GL_BUFFER_MAP_POINTER_ARB = $88BD;
    GL_BUFFER_MAP_POINTER_OES = $88BD;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
    GL_WRITE_DISCARD_NV = $88BE;
    GL_TIME_ELAPSED = $88BF;
    GL_TIME_ELAPSED_EXT = $88BF;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_MATRIX0_ARB = $88C0;
    GL_MATRIX1_ARB = $88C1;
    GL_MATRIX2_ARB = $88C2;
    GL_MATRIX3_ARB = $88C3;
    GL_MATRIX4_ARB = $88C4;
    GL_MATRIX5_ARB = $88C5;
    GL_MATRIX6_ARB = $88C6;
    GL_MATRIX7_ARB = $88C7;
    GL_MATRIX8_ARB = $88C8;
    GL_MATRIX9_ARB = $88C9;
    GL_MATRIX10_ARB = $88CA;
    GL_MATRIX11_ARB = $88CB;
    GL_MATRIX12_ARB = $88CC;
    GL_MATRIX13_ARB = $88CD;
    GL_MATRIX14_ARB = $88CE;
    GL_MATRIX15_ARB = $88CF;
    GL_MATRIX16_ARB = $88D0;
    GL_MATRIX17_ARB = $88D1;
    GL_MATRIX18_ARB = $88D2;
    GL_MATRIX19_ARB = $88D3;
    GL_MATRIX20_ARB = $88D4;
    GL_MATRIX21_ARB = $88D5;
    GL_MATRIX22_ARB = $88D6;
    GL_MATRIX23_ARB = $88D7;
    GL_MATRIX24_ARB = $88D8;
    GL_MATRIX25_ARB = $88D9;
    GL_MATRIX26_ARB = $88DA;
    GL_MATRIX27_ARB = $88DB;
    GL_MATRIX28_ARB = $88DC;
    GL_MATRIX29_ARB = $88DD;
    GL_MATRIX30_ARB = $88DE;
    GL_MATRIX31_ARB = $88DF;
    GL_STREAM_DRAW_ARB = $88E0;
    GL_STREAM_READ_ARB = $88E1;
    GL_STREAM_COPY_ARB = $88E2;
    GL_STATIC_DRAW_ARB = $88E4;
    GL_STATIC_READ_ARB = $88E5;
    GL_STATIC_COPY_ARB = $88E6;
    GL_DYNAMIC_DRAW_ARB = $88E8;
    GL_DYNAMIC_READ_ARB = $88E9;
    GL_DYNAMIC_COPY_ARB = $88EA;
    GL_PIXEL_PACK_BUFFER_ARB = $88EB;
    GL_PIXEL_PACK_BUFFER_EXT = $88EB;
    GL_PIXEL_PACK_BUFFER_NV = $88EB;   // api(gles2) //extension: GL_NV_pixel_buffer_object
    GL_PIXEL_UNPACK_BUFFER_ARB = $88EC;
    GL_PIXEL_UNPACK_BUFFER_EXT = $88EC;
    GL_PIXEL_UNPACK_BUFFER_NV = $88EC;   // api(gles2) //extension: GL_NV_pixel_buffer_object
    GL_PIXEL_PACK_BUFFER_BINDING_ARB = $88ED;
    GL_PIXEL_PACK_BUFFER_BINDING_EXT = $88ED;
    GL_PIXEL_PACK_BUFFER_BINDING_NV = $88ED;   // api(gles2) //extension: GL_NV_pixel_buffer_object
    GL_ETC1_SRGB8_NV = $88EE;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_PIXEL_UNPACK_BUFFER_BINDING_ARB = $88EF;
    GL_PIXEL_UNPACK_BUFFER_BINDING_EXT = $88EF;
    GL_PIXEL_UNPACK_BUFFER_BINDING_NV = $88EF;   // api(gles2) //extension: GL_NV_pixel_buffer_object
    GL_DEPTH24_STENCIL8_EXT = $88F0;
    GL_DEPTH24_STENCIL8_OES = $88F0;   // api(gles2) //extension: GL_ANGLE_depth_texture
    GL_TEXTURE_STENCIL_SIZE = $88F1;
    GL_TEXTURE_STENCIL_SIZE_EXT = $88F1;
    GL_STENCIL_TAG_BITS_EXT = $88F2;
    GL_STENCIL_CLEAR_TAG_VALUE_EXT = $88F3;
    GL_MAX_PROGRAM_EXEC_INSTRUCTIONS_NV = $88F4;
    GL_MAX_PROGRAM_CALL_DEPTH_NV = $88F5;
    GL_MAX_PROGRAM_IF_DEPTH_NV = $88F6;
    GL_MAX_PROGRAM_LOOP_DEPTH_NV = $88F7;
    GL_MAX_PROGRAM_LOOP_COUNT_NV = $88F8;
    GL_SRC1_COLOR = $88F9;
    GL_SRC1_COLOR_EXT = $88F9;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_ONE_MINUS_SRC1_COLOR = $88FA;
    GL_ONE_MINUS_SRC1_COLOR_EXT = $88FA;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_ONE_MINUS_SRC1_ALPHA = $88FB;
    GL_ONE_MINUS_SRC1_ALPHA_EXT = $88FB;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_MAX_DUAL_SOURCE_DRAW_BUFFERS = $88FC;
    GL_MAX_DUAL_SOURCE_DRAW_BUFFERS_EXT = $88FC;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_VERTEX_ATTRIB_ARRAY_INTEGER_EXT = $88FD;
    GL_VERTEX_ATTRIB_ARRAY_INTEGER_NV = $88FD;
    GL_VERTEX_ATTRIB_ARRAY_DIVISOR_ANGLE = $88FE;   // api(gles2) //extension: GL_ANGLE_instanced_arrays
    GL_VERTEX_ATTRIB_ARRAY_DIVISOR_ARB = $88FE;
    GL_VERTEX_ATTRIB_ARRAY_DIVISOR_EXT = $88FE;   // api(gles2) //extension: GL_EXT_instanced_arrays
    GL_VERTEX_ATTRIB_ARRAY_DIVISOR_NV = $88FE;   // api(gles2) //extension: GL_NV_instanced_arrays
    GL_MAX_ARRAY_TEXTURE_LAYERS_EXT = $88FF;
    GL_MIN_PROGRAM_TEXEL_OFFSET_EXT = $8904;
    GL_MIN_PROGRAM_TEXEL_OFFSET_NV = $8904;
    GL_MAX_PROGRAM_TEXEL_OFFSET_EXT = $8905;
    GL_MAX_PROGRAM_TEXEL_OFFSET_NV = $8905;
    GL_PROGRAM_ATTRIB_COMPONENTS_NV = $8906;
    GL_PROGRAM_RESULT_COMPONENTS_NV = $8907;
    GL_MAX_PROGRAM_ATTRIB_COMPONENTS_NV = $8908;
    GL_MAX_PROGRAM_RESULT_COMPONENTS_NV = $8909;
    GL_STENCIL_TEST_TWO_SIDE_EXT = $8910;
    GL_ACTIVE_STENCIL_FACE_EXT = $8911;
    GL_MIRROR_CLAMP_TO_BORDER_EXT = $8912;
    GL_SAMPLES_PASSED = $8914;
    GL_SAMPLES_PASSED_ARB = $8914;
    GL_GEOMETRY_LINKED_VERTICES_OUT_EXT = $8916;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_GEOMETRY_LINKED_VERTICES_OUT_OES = $8916;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_GEOMETRY_LINKED_INPUT_TYPE_EXT = $8917;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_GEOMETRY_LINKED_INPUT_TYPE_OES = $8917;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_GEOMETRY_LINKED_OUTPUT_TYPE_EXT = $8918;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_GEOMETRY_LINKED_OUTPUT_TYPE_OES = $8918;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_CLAMP_VERTEX_COLOR = $891A;
    GL_CLAMP_VERTEX_COLOR_ARB = $891A;
    GL_CLAMP_FRAGMENT_COLOR = $891B;
    GL_CLAMP_FRAGMENT_COLOR_ARB = $891B;
    GL_CLAMP_READ_COLOR = $891C;
    GL_CLAMP_READ_COLOR_ARB = $891C;
    GL_FIXED_ONLY = $891D;
    GL_FIXED_ONLY_ARB = $891D;
    GL_TESS_CONTROL_PROGRAM_NV = $891E;
    GL_TESS_EVALUATION_PROGRAM_NV = $891F;
    GL_FRAGMENT_SHADER_ATI = $8920;
    GL_REG_0_ATI = $8921;
    GL_REG_1_ATI = $8922;
    GL_REG_2_ATI = $8923;
    GL_REG_3_ATI = $8924;
    GL_REG_4_ATI = $8925;
    GL_REG_5_ATI = $8926;
    GL_REG_6_ATI = $8927;
    GL_REG_7_ATI = $8928;
    GL_REG_8_ATI = $8929;
    GL_REG_9_ATI = $892A;
    GL_REG_10_ATI = $892B;
    GL_REG_11_ATI = $892C;
    GL_REG_12_ATI = $892D;
    GL_REG_13_ATI = $892E;
    GL_REG_14_ATI = $892F;
    GL_REG_15_ATI = $8930;
    GL_REG_16_ATI = $8931;
    GL_REG_17_ATI = $8932;
    GL_REG_18_ATI = $8933;
    GL_REG_19_ATI = $8934;
    GL_REG_20_ATI = $8935;
    GL_REG_21_ATI = $8936;
    GL_REG_22_ATI = $8937;
    GL_REG_23_ATI = $8938;
    GL_REG_24_ATI = $8939;
    GL_REG_25_ATI = $893A;
    GL_REG_26_ATI = $893B;
    GL_REG_27_ATI = $893C;
    GL_REG_28_ATI = $893D;
    GL_REG_29_ATI = $893E;
    GL_REG_30_ATI = $893F;
    GL_REG_31_ATI = $8940;
    GL_CON_0_ATI = $8941;
    GL_CON_1_ATI = $8942;
    GL_CON_2_ATI = $8943;
    GL_CON_3_ATI = $8944;
    GL_CON_4_ATI = $8945;
    GL_CON_5_ATI = $8946;
    GL_CON_6_ATI = $8947;
    GL_CON_7_ATI = $8948;
    GL_CON_8_ATI = $8949;
    GL_CON_9_ATI = $894A;
    GL_CON_10_ATI = $894B;
    GL_CON_11_ATI = $894C;
    GL_CON_12_ATI = $894D;
    GL_CON_13_ATI = $894E;
    GL_CON_14_ATI = $894F;
    GL_CON_15_ATI = $8950;
    GL_CON_16_ATI = $8951;
    GL_CON_17_ATI = $8952;
    GL_CON_18_ATI = $8953;
    GL_CON_19_ATI = $8954;
    GL_CON_20_ATI = $8955;
    GL_CON_21_ATI = $8956;
    GL_CON_22_ATI = $8957;
    GL_CON_23_ATI = $8958;
    GL_CON_24_ATI = $8959;
    GL_CON_25_ATI = $895A;
    GL_CON_26_ATI = $895B;
    GL_CON_27_ATI = $895C;
    GL_CON_28_ATI = $895D;
    GL_CON_29_ATI = $895E;
    GL_CON_30_ATI = $895F;
    GL_CON_31_ATI = $8960;
    GL_MOV_ATI = $8961;
    GL_ADD_ATI = $8963;
    GL_MUL_ATI = $8964;
    GL_SUB_ATI = $8965;
    GL_DOT3_ATI = $8966;
    GL_DOT4_ATI = $8967;
    GL_MAD_ATI = $8968;
    GL_LERP_ATI = $8969;
    GL_CND_ATI = $896A;
    GL_CND0_ATI = $896B;
    GL_DOT2_ADD_ATI = $896C;
    GL_SECONDARY_INTERPOLATOR_ATI = $896D;
    GL_NUM_FRAGMENT_REGISTERS_ATI = $896E;
    GL_NUM_FRAGMENT_CONSTANTS_ATI = $896F;
    GL_NUM_PASSES_ATI = $8970;
    GL_NUM_INSTRUCTIONS_PER_PASS_ATI = $8971;
    GL_NUM_INSTRUCTIONS_TOTAL_ATI = $8972;
    GL_NUM_INPUT_INTERPOLATOR_COMPONENTS_ATI = $8973;
    GL_NUM_LOOPBACK_COMPONENTS_ATI = $8974;
    GL_COLOR_ALPHA_PAIRING_ATI = $8975;
    GL_SWIZZLE_STR_ATI = $8976;
    GL_SWIZZLE_STQ_ATI = $8977;
    GL_SWIZZLE_STR_DR_ATI = $8978;
    GL_SWIZZLE_STQ_DQ_ATI = $8979;
    GL_SWIZZLE_STRQ_ATI = $897A;
    GL_SWIZZLE_STRQ_DQ_ATI = $897B;
    GL_INTERLACE_OML = $8980;
    GL_INTERLACE_READ_OML = $8981;
    GL_FORMAT_SUBSAMPLE_24_24_OML = $8982;
    GL_FORMAT_SUBSAMPLE_244_244_OML = $8983;
    GL_PACK_RESAMPLE_OML = $8984;
    GL_UNPACK_RESAMPLE_OML = $8985;
    GL_RESAMPLE_REPLICATE_OML = $8986;
    GL_RESAMPLE_ZERO_FILL_OML = $8987;
    GL_RESAMPLE_AVERAGE_OML = $8988;
    GL_RESAMPLE_DECIMATE_OML = $8989;
    GL_POINT_SIZE_ARRAY_TYPE_OES = $898A;
    GL_POINT_SIZE_ARRAY_STRIDE_OES = $898B;
    GL_POINT_SIZE_ARRAY_POINTER_OES = $898C;
    GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES = $898D;
    GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES = $898E;
    GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES = $898F;
    GL_VERTEX_ATTRIB_MAP1_APPLE = $8A00;
    GL_VERTEX_ATTRIB_MAP2_APPLE = $8A01;
    GL_VERTEX_ATTRIB_MAP1_SIZE_APPLE = $8A02;
    GL_VERTEX_ATTRIB_MAP1_COEFF_APPLE = $8A03;
    GL_VERTEX_ATTRIB_MAP1_ORDER_APPLE = $8A04;
    GL_VERTEX_ATTRIB_MAP1_DOMAIN_APPLE = $8A05;
    GL_VERTEX_ATTRIB_MAP2_SIZE_APPLE = $8A06;
    GL_VERTEX_ATTRIB_MAP2_COEFF_APPLE = $8A07;
    GL_VERTEX_ATTRIB_MAP2_ORDER_APPLE = $8A08;
    GL_VERTEX_ATTRIB_MAP2_DOMAIN_APPLE = $8A09;
    GL_DRAW_PIXELS_APPLE = $8A0A;
    GL_FENCE_APPLE = $8A0B;
    GL_ELEMENT_ARRAY_APPLE = $8A0C;
    GL_ELEMENT_ARRAY_TYPE_APPLE = $8A0D;
    GL_ELEMENT_ARRAY_POINTER_APPLE = $8A0E;
    GL_COLOR_FLOAT_APPLE = $8A0F;
    GL_BUFFER_SERIALIZED_MODIFY_APPLE = $8A12;
    GL_BUFFER_FLUSHING_UNMAP_APPLE = $8A13;
    GL_AUX_DEPTH_STENCIL_APPLE = $8A14;
    GL_PACK_ROW_BYTES_APPLE = $8A15;
    GL_UNPACK_ROW_BYTES_APPLE = $8A16;
    GL_RELEASED_APPLE = $8A19;
    GL_VOLATILE_APPLE = $8A1A;
    GL_RETAINED_APPLE = $8A1B;
    GL_UNDEFINED_APPLE = $8A1C;
    GL_PURGEABLE_APPLE = $8A1D;
    GL_RGB_422_APPLE = $8A1F;   // api(gl|glcore|gles2) //extension: GL_APPLE_rgb_422
    GL_MAX_GEOMETRY_UNIFORM_BLOCKS_EXT = $8A2C;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_UNIFORM_BLOCKS_OES = $8A2C;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS_EXT = $8A32;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS_OES = $8A32;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_UNIFORM_BLOCK_REFERENCED_BY_GEOMETRY_SHADER = $8A45;
    GL_TEXTURE_SRGB_DECODE_EXT = $8A48;   // api(gl|glcore|gles2) //extension: GL_EXT_texture_sRGB_decode
    GL_DECODE_EXT = $8A49;   // api(gl|glcore|gles2) //extension: GL_EXT_texture_sRGB_decode
    GL_SKIP_DECODE_EXT = $8A4A;   // api(gl|glcore|gles2) //extension: GL_EXT_texture_sRGB_decode
    GL_PROGRAM_PIPELINE_OBJECT_EXT = $8A4F;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
    GL_RGB_RAW_422_APPLE = $8A51;   // api(gl|glcore|gles2) //extension: GL_APPLE_rgb_422
    GL_FRAGMENT_SHADER_DISCARDS_SAMPLES_EXT = $8A52;   // api(gl|glcore|gles2) //extension: GL_EXT_shader_framebuffer_fetch
    GL_SYNC_OBJECT_APPLE = $8A53;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_COMPRESSED_SRGB_PVRTC_2BPPV1_EXT = $8A54;   // api(gles2) //extension: GL_EXT_pvrtc_sRGB
    GL_COMPRESSED_SRGB_PVRTC_4BPPV1_EXT = $8A55;   // api(gles2) //extension: GL_EXT_pvrtc_sRGB
    GL_COMPRESSED_SRGB_ALPHA_PVRTC_2BPPV1_EXT = $8A56;   // api(gles2) //extension: GL_EXT_pvrtc_sRGB
    GL_COMPRESSED_SRGB_ALPHA_PVRTC_4BPPV1_EXT = $8A57;   // api(gles2) //extension: GL_EXT_pvrtc_sRGB
    GL_FRAGMENT_SHADER_ARB = $8B30;
    GL_VERTEX_SHADER_ARB = $8B31;
    GL_PROGRAM_OBJECT_ARB = $8B40;
    GL_PROGRAM_OBJECT_EXT = $8B40;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
    GL_SHADER_OBJECT_ARB = $8B48;
    GL_SHADER_OBJECT_EXT = $8B48;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
    GL_MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB = $8B49;
    GL_MAX_VERTEX_UNIFORM_COMPONENTS_ARB = $8B4A;
    GL_MAX_VARYING_FLOATS = $8B4B;
    GL_MAX_VARYING_COMPONENTS_EXT = $8B4B;
    GL_MAX_VARYING_FLOATS_ARB = $8B4B;
    GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS_ARB = $8B4C;
    GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS_ARB = $8B4D;
    GL_OBJECT_TYPE_ARB = $8B4E;
    GL_OBJECT_SUBTYPE_ARB = $8B4F;
    GL_FLOAT_VEC2_ARB = $8B50;
    GL_FLOAT_VEC3_ARB = $8B51;
    GL_FLOAT_VEC4_ARB = $8B52;
    GL_INT_VEC2_ARB = $8B53;
    GL_INT_VEC3_ARB = $8B54;
    GL_INT_VEC4_ARB = $8B55;
    GL_BOOL_ARB = $8B56;
    GL_BOOL_VEC2_ARB = $8B57;
    GL_BOOL_VEC3_ARB = $8B58;
    GL_BOOL_VEC4_ARB = $8B59;
    GL_FLOAT_MAT2_ARB = $8B5A;
    GL_FLOAT_MAT3_ARB = $8B5B;
    GL_FLOAT_MAT4_ARB = $8B5C;
    GL_SAMPLER_1D = $8B5D;
    GL_SAMPLER_1D_ARB = $8B5D;
    GL_SAMPLER_2D_ARB = $8B5E;
    GL_SAMPLER_3D_ARB = $8B5F;
    GL_SAMPLER_3D_OES = $8B5F;   // api(gles2) //extension: GL_OES_texture_3D
    GL_SAMPLER_CUBE_ARB = $8B60;
    GL_SAMPLER_1D_SHADOW = $8B61;
    GL_SAMPLER_1D_SHADOW_ARB = $8B61;
    GL_SAMPLER_2D_SHADOW_ARB = $8B62;
    GL_SAMPLER_2D_SHADOW_EXT = $8B62;   // api(gles2) //extension: GL_EXT_shadow_samplers
    GL_SAMPLER_2D_RECT = $8B63;
    GL_SAMPLER_2D_RECT_ARB = $8B63;
    GL_SAMPLER_2D_RECT_SHADOW = $8B64;
    GL_SAMPLER_2D_RECT_SHADOW_ARB = $8B64;
    GL_FLOAT_MAT2x3_NV = $8B65;   // api(gles2) //extension: GL_NV_non_square_matrices
    GL_FLOAT_MAT2x4_NV = $8B66;   // api(gles2) //extension: GL_NV_non_square_matrices
    GL_FLOAT_MAT3x2_NV = $8B67;   // api(gles2) //extension: GL_NV_non_square_matrices
    GL_FLOAT_MAT3x4_NV = $8B68;   // api(gles2) //extension: GL_NV_non_square_matrices
    GL_FLOAT_MAT4x2_NV = $8B69;   // api(gles2) //extension: GL_NV_non_square_matrices
    GL_FLOAT_MAT4x3_NV = $8B6A;   // api(gles2) //extension: GL_NV_non_square_matrices
    GL_OBJECT_DELETE_STATUS_ARB = $8B80;
    GL_OBJECT_COMPILE_STATUS_ARB = $8B81;
    GL_OBJECT_LINK_STATUS_ARB = $8B82;
    GL_OBJECT_VALIDATE_STATUS_ARB = $8B83;
    GL_OBJECT_INFO_LOG_LENGTH_ARB = $8B84;
    GL_OBJECT_ATTACHED_OBJECTS_ARB = $8B85;
    GL_OBJECT_ACTIVE_UNIFORMS_ARB = $8B86;
    GL_OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB = $8B87;
    GL_OBJECT_SHADER_SOURCE_LENGTH_ARB = $8B88;
    GL_OBJECT_ACTIVE_ATTRIBUTES_ARB = $8B89;
    GL_OBJECT_ACTIVE_ATTRIBUTE_MAX_LENGTH_ARB = $8B8A;
    GL_FRAGMENT_SHADER_DERIVATIVE_HINT_ARB = $8B8B;
    GL_FRAGMENT_SHADER_DERIVATIVE_HINT_OES = $8B8B;   // api(gles2|glsc2) //extension: GL_OES_standard_derivatives
    GL_SHADING_LANGUAGE_VERSION_ARB = $8B8C;
    GL_PALETTE4_RGB8_OES = $8B90;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE4_RGBA8_OES = $8B91;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE4_R5_G6_B5_OES = $8B92;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE4_RGBA4_OES = $8B93;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE4_RGB5_A1_OES = $8B94;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE8_RGB8_OES = $8B95;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE8_RGBA8_OES = $8B96;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE8_R5_G6_B5_OES = $8B97;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE8_RGBA4_OES = $8B98;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_PALETTE8_RGB5_A1_OES = $8B99;   // api(gl|gles1|gles2) //extension: GL_OES_compressed_paletted_texture
    GL_IMPLEMENTATION_COLOR_READ_TYPE_OES = $8B9A;
    GL_IMPLEMENTATION_COLOR_READ_FORMAT_OES = $8B9B;
    GL_POINT_SIZE_ARRAY_OES = $8B9C;
    GL_TEXTURE_CROP_RECT_OES = $8B9D;
    GL_MATRIX_INDEX_ARRAY_BUFFER_BINDING_OES = $8B9E;
    GL_POINT_SIZE_ARRAY_BUFFER_BINDING_OES = $8B9F;
    GL_FRAGMENT_PROGRAM_POSITION_MESA = $8BB0;
    GL_FRAGMENT_PROGRAM_CALLBACK_MESA = $8BB1;
    GL_FRAGMENT_PROGRAM_CALLBACK_FUNC_MESA = $8BB2;
    GL_FRAGMENT_PROGRAM_CALLBACK_DATA_MESA = $8BB3;
    GL_VERTEX_PROGRAM_POSITION_MESA = $8BB4;
    GL_VERTEX_PROGRAM_CALLBACK_MESA = $8BB5;
    GL_VERTEX_PROGRAM_CALLBACK_FUNC_MESA = $8BB6;
    GL_VERTEX_PROGRAM_CALLBACK_DATA_MESA = $8BB7;
    GL_TILE_RASTER_ORDER_FIXED_MESA = $8BB8;
    GL_TILE_RASTER_ORDER_INCREASING_X_MESA = $8BB9;
    GL_TILE_RASTER_ORDER_INCREASING_Y_MESA = $8BBA;
    GL_FRAMEBUFFER_FLIP_Y_MESA = $8BBB;   // api(gl|glcore|gles2) //extension: GL_MESA_framebuffer_flip_y
    GL_FRAMEBUFFER_FLIP_X_MESA = $8BBC;   // api(gl|glcore|gles2) //extension: GL_MESA_framebuffer_flip_x
    GL_FRAMEBUFFER_SWAP_XY_MESA = $8BBD;   // api(gl|glcore|gles2) //extension: GL_MESA_framebuffer_swap_xy
    GL_COUNTER_TYPE_AMD = $8BC0;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_COUNTER_RANGE_AMD = $8BC1;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_UNSIGNED_INT64_AMD = $8BC2;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_PERCENTAGE_AMD = $8BC3;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_PERFMON_RESULT_AVAILABLE_AMD = $8BC4;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_PERFMON_RESULT_SIZE_AMD = $8BC5;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_PERFMON_RESULT_AMD = $8BC6;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
    GL_TEXTURE_WIDTH_QCOM = $8BD2;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_HEIGHT_QCOM = $8BD3;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_DEPTH_QCOM = $8BD4;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_INTERNAL_FORMAT_QCOM = $8BD5;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_FORMAT_QCOM = $8BD6;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_TYPE_QCOM = $8BD7;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_IMAGE_VALID_QCOM = $8BD8;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_NUM_LEVELS_QCOM = $8BD9;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_TARGET_QCOM = $8BDA;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_TEXTURE_OBJECT_VALID_QCOM = $8BDB;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_STATE_RESTORE = $8BDC;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
    GL_SAMPLER_EXTERNAL_2D_Y2Y_EXT = $8BE7;   // api(gles2) //extension: GL_EXT_YUV_target
    GL_TEXTURE_PROTECTED_EXT = $8BFA;   // api(gles2) //extension: GL_EXT_protected_textures
    GL_TEXTURE_FOVEATED_FEATURE_BITS_QCOM = $8BFB;   // api(gles2) //extension: GL_QCOM_texture_foveated
    GL_TEXTURE_FOVEATED_MIN_PIXEL_DENSITY_QCOM = $8BFC;   // api(gles2) //extension: GL_QCOM_texture_foveated
    GL_TEXTURE_FOVEATED_FEATURE_QUERY_QCOM = $8BFD;   // api(gles2) //extension: GL_QCOM_texture_foveated
    GL_TEXTURE_FOVEATED_NUM_FOCAL_POINTS_QUERY_QCOM = $8BFE;   // api(gles2) //extension: GL_QCOM_texture_foveated
    GL_FRAMEBUFFER_INCOMPLETE_FOVEATION_QCOM = $8BFF;   // api(gles2) //extension: GL_QCOM_texture_foveated
    GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG = $8C00;   // api(gles1|gles2) //extension: GL_IMG_texture_compression_pvrtc
    GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG = $8C01;   // api(gles1|gles2) //extension: GL_IMG_texture_compression_pvrtc
    GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG = $8C02;   // api(gles1|gles2) //extension: GL_IMG_texture_compression_pvrtc
    GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG = $8C03;   // api(gles1|gles2) //extension: GL_IMG_texture_compression_pvrtc
    GL_MODULATE_COLOR_IMG = $8C04;
    GL_RECIP_ADD_SIGNED_ALPHA_IMG = $8C05;
    GL_TEXTURE_ALPHA_MODULATE_IMG = $8C06;
    GL_FACTOR_ALPHA_MODULATE_IMG = $8C07;
    GL_FRAGMENT_ALPHA_MODULATE_IMG = $8C08;
    GL_ADD_BLEND_IMG = $8C09;
    GL_SGX_BINARY_IMG = $8C0A;   // api(gles2) //extension: GL_IMG_shader_binary
    GL_TEXTURE_RED_TYPE = $8C10;
    GL_TEXTURE_RED_TYPE_ARB = $8C10;
    GL_TEXTURE_GREEN_TYPE = $8C11;
    GL_TEXTURE_GREEN_TYPE_ARB = $8C11;
    GL_TEXTURE_BLUE_TYPE = $8C12;
    GL_TEXTURE_BLUE_TYPE_ARB = $8C12;
    GL_TEXTURE_ALPHA_TYPE = $8C13;
    GL_TEXTURE_ALPHA_TYPE_ARB = $8C13;
    GL_TEXTURE_LUMINANCE_TYPE = $8C14;
    GL_TEXTURE_LUMINANCE_TYPE_ARB = $8C14;
    GL_TEXTURE_INTENSITY_TYPE = $8C15;
    GL_TEXTURE_INTENSITY_TYPE_ARB = $8C15;
    GL_TEXTURE_DEPTH_TYPE = $8C16;
    GL_TEXTURE_DEPTH_TYPE_ARB = $8C16;
    GL_UNSIGNED_NORMALIZED = $8C17;
    GL_UNSIGNED_NORMALIZED_ARB = $8C17;
    GL_UNSIGNED_NORMALIZED_EXT = $8C17;   // api(gles2) //extension: GL_EXT_color_buffer_half_float
    GL_TEXTURE_1D_ARRAY = $8C18;
    GL_TEXTURE_1D_ARRAY_EXT = $8C18;
    GL_PROXY_TEXTURE_1D_ARRAY = $8C19;
    GL_PROXY_TEXTURE_1D_ARRAY_EXT = $8C19;
    GL_TEXTURE_2D_ARRAY_EXT = $8C1A;
    GL_PROXY_TEXTURE_2D_ARRAY = $8C1B;
    GL_PROXY_TEXTURE_2D_ARRAY_EXT = $8C1B;
    GL_TEXTURE_BINDING_1D_ARRAY = $8C1C;
    GL_TEXTURE_BINDING_1D_ARRAY_EXT = $8C1C;
    GL_TEXTURE_BINDING_2D_ARRAY_EXT = $8C1D;
    GL_GEOMETRY_PROGRAM_NV = $8C26;
    GL_MAX_PROGRAM_OUTPUT_VERTICES_NV = $8C27;
    GL_MAX_PROGRAM_TOTAL_OUTPUT_COMPONENTS_NV = $8C28;
    GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_ARB = $8C29;
    GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_EXT = $8C29;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS_OES = $8C29;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_TEXTURE_BUFFER_ARB = $8C2A;
    GL_TEXTURE_BUFFER_EXT = $8C2A;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BUFFER_OES = $8C2A;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_TEXTURE_BUFFER_BINDING = $8C2A;  //->Equivalent to GL_TEXTURE_BUFFER_ARB query, but named more consistently
    GL_TEXTURE_BUFFER_BINDING_EXT = $8C2A;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BUFFER_BINDING_OES = $8C2A;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_MAX_TEXTURE_BUFFER_SIZE_ARB = $8C2B;
    GL_MAX_TEXTURE_BUFFER_SIZE_EXT = $8C2B;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_MAX_TEXTURE_BUFFER_SIZE_OES = $8C2B;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_TEXTURE_BINDING_BUFFER_ARB = $8C2C;
    GL_TEXTURE_BINDING_BUFFER_EXT = $8C2C;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BINDING_BUFFER_OES = $8C2C;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_TEXTURE_BUFFER_DATA_STORE_BINDING = $8C2D;
    GL_TEXTURE_BUFFER_DATA_STORE_BINDING_ARB = $8C2D;
    GL_TEXTURE_BUFFER_DATA_STORE_BINDING_EXT = $8C2D;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BUFFER_DATA_STORE_BINDING_OES = $8C2D;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_TEXTURE_BUFFER_FORMAT_ARB = $8C2E;
    GL_TEXTURE_BUFFER_FORMAT_EXT = $8C2E;
    GL_ANY_SAMPLES_PASSED_EXT = $8C2F;   // api(gles2) //extension: GL_EXT_occlusion_query_boolean
    GL_SAMPLE_SHADING_ARB = $8C36;
    GL_SAMPLE_SHADING_OES = $8C36;   // api(gles2) //extension: GL_OES_sample_shading
    GL_MIN_SAMPLE_SHADING_VALUE = $8C37;
    GL_MIN_SAMPLE_SHADING_VALUE_ARB = $8C37;
    GL_MIN_SAMPLE_SHADING_VALUE_OES = $8C37;   // api(gles2) //extension: GL_OES_sample_shading
    GL_R11F_G11F_B10F_APPLE = $8C3A;   // api(gles2) //extension: GL_APPLE_texture_packed_float
    GL_R11F_G11F_B10F_EXT = $8C3A;
    GL_UNSIGNED_INT_10F_11F_11F_REV_APPLE = $8C3B;   // api(gles2) //extension: GL_APPLE_texture_packed_float
    GL_UNSIGNED_INT_10F_11F_11F_REV_EXT = $8C3B;
    GL_RGBA_SIGNED_COMPONENTS_EXT = $8C3C;
    GL_RGB9_E5_APPLE = $8C3D;   // api(gles2) //extension: GL_APPLE_texture_packed_float
    GL_RGB9_E5_EXT = $8C3D;
    GL_UNSIGNED_INT_5_9_9_9_REV = $8C3E;
    GL_UNSIGNED_INT_5_9_9_9_REV_APPLE = $8C3E;   // api(gles2) //extension: GL_APPLE_texture_packed_float
    GL_UNSIGNED_INT_5_9_9_9_REV_EXT = $8C3E;
    GL_TEXTURE_SHARED_SIZE = $8C3F;
    GL_TEXTURE_SHARED_SIZE_EXT = $8C3F;
    GL_SRGB_EXT = $8C40;   // api(gles1|gles2) //extension: GL_EXT_sRGB
    GL_SRGB8_EXT = $8C41;
    GL_SRGB8_NV = $8C41;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_SRGB_ALPHA = $8C42;
    GL_SRGB_ALPHA_EXT = $8C42;   // api(gles1|gles2) //extension: GL_EXT_sRGB
    GL_SRGB8_ALPHA8_EXT = $8C43;   // api(gles1|gles2) //extension: GL_EXT_sRGB
    GL_SLUMINANCE_ALPHA = $8C44;
    GL_SLUMINANCE_ALPHA_EXT = $8C44;
    GL_SLUMINANCE_ALPHA_NV = $8C44;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_SLUMINANCE8_ALPHA8 = $8C45;
    GL_SLUMINANCE8_ALPHA8_EXT = $8C45;
    GL_SLUMINANCE8_ALPHA8_NV = $8C45;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_SLUMINANCE = $8C46;
    GL_SLUMINANCE_EXT = $8C46;
    GL_SLUMINANCE_NV = $8C46;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_SLUMINANCE8 = $8C47;
    GL_SLUMINANCE8_EXT = $8C47;
    GL_SLUMINANCE8_NV = $8C47;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_COMPRESSED_SRGB = $8C48;
    GL_COMPRESSED_SRGB_EXT = $8C48;
    GL_COMPRESSED_SRGB_ALPHA = $8C49;
    GL_COMPRESSED_SRGB_ALPHA_EXT = $8C49;
    GL_COMPRESSED_SLUMINANCE = $8C4A;
    GL_COMPRESSED_SLUMINANCE_EXT = $8C4A;
    GL_COMPRESSED_SLUMINANCE_ALPHA = $8C4B;
    GL_COMPRESSED_SLUMINANCE_ALPHA_EXT = $8C4B;
    GL_COMPRESSED_SRGB_S3TC_DXT1_EXT = $8C4C;   // api(gles2) //extension: GL_EXT_texture_compression_s3tc_srgb
    GL_COMPRESSED_SRGB_S3TC_DXT1_NV = $8C4C;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT = $8C4D;   // api(gles2) //extension: GL_EXT_texture_compression_s3tc_srgb
    GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_NV = $8C4D;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT = $8C4E;   // api(gles2) //extension: GL_EXT_texture_compression_s3tc_srgb
    GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_NV = $8C4E;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT = $8C4F;   // api(gles2) //extension: GL_EXT_texture_compression_s3tc_srgb
    GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_NV = $8C4F;   // api(gles2) //extension: GL_NV_sRGB_formats
    GL_COMPRESSED_LUMINANCE_LATC1_EXT = $8C70;
    GL_COMPRESSED_SIGNED_LUMINANCE_LATC1_EXT = $8C71;
    GL_COMPRESSED_LUMINANCE_ALPHA_LATC2_EXT = $8C72;
    GL_COMPRESSED_SIGNED_LUMINANCE_ALPHA_LATC2_EXT = $8C73;
    GL_TESS_CONTROL_PROGRAM_PARAMETER_BUFFER_NV = $8C74;
    GL_TESS_EVALUATION_PROGRAM_PARAMETER_BUFFER_NV = $8C75;
    GL_TRANSFORM_FEEDBACK_VARYING_MAX_LENGTH_EXT = $8C76;
    GL_BACK_PRIMARY_COLOR_NV = $8C77;
    GL_BACK_SECONDARY_COLOR_NV = $8C78;
    GL_TEXTURE_COORD_NV = $8C79;
    GL_CLIP_DISTANCE_NV = $8C7A;
    GL_VERTEX_ID_NV = $8C7B;
    GL_PRIMITIVE_ID_NV = $8C7C;
    GL_GENERIC_ATTRIB_NV = $8C7D;
    GL_TRANSFORM_FEEDBACK_ATTRIBS_NV = $8C7E;
    GL_TRANSFORM_FEEDBACK_BUFFER_MODE_EXT = $8C7F;
    GL_TRANSFORM_FEEDBACK_BUFFER_MODE_NV = $8C7F;
    GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = $8C80;
    GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS_EXT = $8C80;
    GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS_NV = $8C80;
    GL_ACTIVE_VARYINGS_NV = $8C81;
    GL_ACTIVE_VARYING_MAX_LENGTH_NV = $8C82;
    GL_TRANSFORM_FEEDBACK_VARYINGS_EXT = $8C83;
    GL_TRANSFORM_FEEDBACK_VARYINGS_NV = $8C83;
    GL_TRANSFORM_FEEDBACK_BUFFER_START_EXT = $8C84;
    GL_TRANSFORM_FEEDBACK_BUFFER_START_NV = $8C84;
    GL_TRANSFORM_FEEDBACK_BUFFER_SIZE_EXT = $8C85;
    GL_TRANSFORM_FEEDBACK_BUFFER_SIZE_NV = $8C85;
    GL_TRANSFORM_FEEDBACK_RECORD_NV = $8C86;
    GL_PRIMITIVES_GENERATED_EXT = $8C87;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_PRIMITIVES_GENERATED_NV = $8C87;
    GL_PRIMITIVES_GENERATED_OES = $8C87;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_EXT = $8C88;
    GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN_NV = $8C88;
    GL_RASTERIZER_DISCARD_EXT = $8C89;
    GL_RASTERIZER_DISCARD_NV = $8C89;
    GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = $8C8A;
    GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS_EXT = $8C8A;
    GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS_NV = $8C8A;
    GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = $8C8B;
    GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS_EXT = $8C8B;
    GL_MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS_NV = $8C8B;
    GL_INTERLEAVED_ATTRIBS_EXT = $8C8C;
    GL_INTERLEAVED_ATTRIBS_NV = $8C8C;
    GL_SEPARATE_ATTRIBS_EXT = $8C8D;
    GL_SEPARATE_ATTRIBS_NV = $8C8D;
    GL_TRANSFORM_FEEDBACK_BUFFER_EXT = $8C8E;
    GL_TRANSFORM_FEEDBACK_BUFFER_NV = $8C8E;
    GL_TRANSFORM_FEEDBACK_BUFFER_BINDING_EXT = $8C8F;
    GL_TRANSFORM_FEEDBACK_BUFFER_BINDING_NV = $8C8F;
    GL_MOTION_ESTIMATION_SEARCH_BLOCK_X_QCOM = $8C90;   // api(gles2) //extension: GL_QCOM_motion_estimation
    GL_MOTION_ESTIMATION_SEARCH_BLOCK_Y_QCOM = $8C91;   // api(gles2) //extension: GL_QCOM_motion_estimation
    GL_ATC_RGB_AMD = $8C92;   // api(gles1|gles2) //extension: GL_AMD_compressed_ATC_texture
    GL_ATC_RGBA_EXPLICIT_ALPHA_AMD = $8C93;   // api(gles1|gles2) //extension: GL_AMD_compressed_ATC_texture
    GL_POINT_SPRITE_COORD_ORIGIN = $8CA0;
    GL_LOWER_LEFT = $8CA1;
    GL_LOWER_LEFT_EXT = $8CA1;   // api(gles2) //extension: GL_EXT_clip_control
    GL_UPPER_LEFT = $8CA2;
    GL_UPPER_LEFT_EXT = $8CA2;   // api(gles2) //extension: GL_EXT_clip_control
    GL_DRAW_FRAMEBUFFER_BINDING_ANGLE = $8CA6;   // api(gles2) //extension: GL_ANGLE_framebuffer_blit
    GL_DRAW_FRAMEBUFFER_BINDING_APPLE = $8CA6;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_DRAW_FRAMEBUFFER_BINDING_EXT = $8CA6;
    GL_DRAW_FRAMEBUFFER_BINDING_NV = $8CA6;   // api(gles2) //extension: GL_NV_framebuffer_blit
    GL_FRAMEBUFFER_BINDING = $8CA6;
    GL_FRAMEBUFFER_BINDING_ANGLE = $8CA6;
    GL_FRAMEBUFFER_BINDING_EXT = $8CA6;
    GL_FRAMEBUFFER_BINDING_OES = $8CA6;
    GL_RENDERBUFFER_BINDING_ANGLE = $8CA7;
    GL_RENDERBUFFER_BINDING_EXT = $8CA7;
    GL_RENDERBUFFER_BINDING_OES = $8CA7;
    GL_READ_FRAMEBUFFER_ANGLE = $8CA8;   // api(gles2) //extension: GL_ANGLE_framebuffer_blit
    GL_READ_FRAMEBUFFER_APPLE = $8CA8;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_READ_FRAMEBUFFER_EXT = $8CA8;
    GL_READ_FRAMEBUFFER_NV = $8CA8;   // api(gles2) //extension: GL_NV_framebuffer_blit
    GL_DRAW_FRAMEBUFFER_ANGLE = $8CA9;   // api(gles2) //extension: GL_ANGLE_framebuffer_blit
    GL_DRAW_FRAMEBUFFER_APPLE = $8CA9;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_DRAW_FRAMEBUFFER_EXT = $8CA9;
    GL_DRAW_FRAMEBUFFER_NV = $8CA9;   // api(gles2) //extension: GL_NV_framebuffer_blit
    GL_READ_FRAMEBUFFER_BINDING_ANGLE = $8CAA;   // api(gles2) //extension: GL_ANGLE_framebuffer_blit
    GL_READ_FRAMEBUFFER_BINDING_APPLE = $8CAA;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_READ_FRAMEBUFFER_BINDING_EXT = $8CAA;
    GL_READ_FRAMEBUFFER_BINDING_NV = $8CAA;   // api(gles2) //extension: GL_NV_framebuffer_blit
    GL_RENDERBUFFER_COVERAGE_SAMPLES_NV = $8CAB;
    GL_RENDERBUFFER_SAMPLES_ANGLE = $8CAB;   // api(gles2) //extension: GL_ANGLE_framebuffer_multisample
    GL_RENDERBUFFER_SAMPLES_APPLE = $8CAB;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_RENDERBUFFER_SAMPLES_EXT = $8CAB;   // api(gles1|gles2) //extension: GL_EXT_multisampled_render_to_texture
    GL_RENDERBUFFER_SAMPLES_NV = $8CAB;   // api(gles2) //extension: GL_NV_framebuffer_multisample
    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_EXT = $8CD0;
    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_OES = $8CD0;
    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_EXT = $8CD1;
    GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_OES = $8CD1;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_EXT = $8CD2;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL_OES = $8CD2;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_EXT = $8CD3;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE_OES = $8CD3;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_3D_ZOFFSET_EXT = $8CD4;
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_3D_ZOFFSET_OES = $8CD4;   // api(gles2) //extension: GL_OES_texture_3D
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_LAYER_EXT = $8CD4;
    GL_FRAMEBUFFER_COMPLETE_EXT = $8CD5;
    GL_FRAMEBUFFER_COMPLETE_OES = $8CD5;
    GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT = $8CD6;
    GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_OES = $8CD6;
    GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT = $8CD7;
    GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_OES = $8CD7;
    GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS = $8CD9;
    GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT = $8CD9;
    GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_OES = $8CD9;
    GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT = $8CDA;
    GL_FRAMEBUFFER_INCOMPLETE_FORMATS_OES = $8CDA;
    GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER = $8CDB;
    GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT = $8CDB;
    GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_OES = $8CDB;
    GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER = $8CDC;
    GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT = $8CDC;
    GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_OES = $8CDC;
    GL_FRAMEBUFFER_UNSUPPORTED_EXT = $8CDD;
    GL_FRAMEBUFFER_UNSUPPORTED_OES = $8CDD;
    GL_MAX_COLOR_ATTACHMENTS = $8CDF;
    GL_MAX_COLOR_ATTACHMENTS_EXT = $8CDF;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_MAX_COLOR_ATTACHMENTS_NV = $8CDF;   // api(gles2) //extension: GL_NV_fbo_color_attachments
    GL_COLOR_ATTACHMENT0_EXT = $8CE0;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT0_NV = $8CE0;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT0_OES = $8CE0;
    GL_COLOR_ATTACHMENT1_EXT = $8CE1;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT1_NV = $8CE1;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT2_EXT = $8CE2;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT2_NV = $8CE2;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT3_EXT = $8CE3;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT3_NV = $8CE3;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT4_EXT = $8CE4;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT4_NV = $8CE4;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT5_EXT = $8CE5;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT5_NV = $8CE5;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT6_EXT = $8CE6;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT6_NV = $8CE6;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT7_EXT = $8CE7;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT7_NV = $8CE7;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT8_EXT = $8CE8;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT8_NV = $8CE8;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT9_EXT = $8CE9;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT9_NV = $8CE9;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT10_EXT = $8CEA;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT10_NV = $8CEA;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT11_EXT = $8CEB;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT11_NV = $8CEB;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT12_EXT = $8CEC;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT12_NV = $8CEC;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT13_EXT = $8CED;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT13_NV = $8CED;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT14_EXT = $8CEE;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT14_NV = $8CEE;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_COLOR_ATTACHMENT15_EXT = $8CEF;   // api(gles2) //extension: GL_EXT_draw_buffers
    GL_COLOR_ATTACHMENT15_NV = $8CEF;   // api(gles2) //extension: GL_NV_draw_buffers
    GL_DEPTH_ATTACHMENT_EXT = $8D00;
    GL_DEPTH_ATTACHMENT_OES = $8D00;
    GL_STENCIL_ATTACHMENT_EXT = $8D20;
    GL_STENCIL_ATTACHMENT_OES = $8D20;
    GL_FRAMEBUFFER_EXT = $8D40;
    GL_FRAMEBUFFER_OES = $8D40;
    GL_RENDERBUFFER_EXT = $8D41;
    GL_RENDERBUFFER_OES = $8D41;
    GL_RENDERBUFFER_WIDTH_EXT = $8D42;
    GL_RENDERBUFFER_WIDTH_OES = $8D42;
    GL_RENDERBUFFER_HEIGHT_EXT = $8D43;
    GL_RENDERBUFFER_HEIGHT_OES = $8D43;
    GL_RENDERBUFFER_INTERNAL_FORMAT_EXT = $8D44;
    GL_RENDERBUFFER_INTERNAL_FORMAT_OES = $8D44;
    GL_STENCIL_INDEX1 = $8D46;
    GL_STENCIL_INDEX1_EXT = $8D46;
    GL_STENCIL_INDEX1_OES = $8D46;   // api(gles1|gles2) //extension: GL_OES_stencil1
    GL_STENCIL_INDEX4 = $8D47;
    GL_STENCIL_INDEX4_EXT = $8D47;
    GL_STENCIL_INDEX4_OES = $8D47;   // api(gles1|gles2) //extension: GL_OES_stencil4
    GL_STENCIL_INDEX8_EXT = $8D48;
    GL_STENCIL_INDEX8_OES = $8D48;   // api(gles2) //extension: GL_OES_texture_stencil8
    GL_STENCIL_INDEX16 = $8D49;
    GL_STENCIL_INDEX16_EXT = $8D49;
    GL_RENDERBUFFER_RED_SIZE_EXT = $8D50;
    GL_RENDERBUFFER_RED_SIZE_OES = $8D50;
    GL_RENDERBUFFER_GREEN_SIZE_EXT = $8D51;
    GL_RENDERBUFFER_GREEN_SIZE_OES = $8D51;
    GL_RENDERBUFFER_BLUE_SIZE_EXT = $8D52;
    GL_RENDERBUFFER_BLUE_SIZE_OES = $8D52;
    GL_RENDERBUFFER_ALPHA_SIZE_EXT = $8D53;
    GL_RENDERBUFFER_ALPHA_SIZE_OES = $8D53;
    GL_RENDERBUFFER_DEPTH_SIZE_EXT = $8D54;
    GL_RENDERBUFFER_DEPTH_SIZE_OES = $8D54;
    GL_RENDERBUFFER_STENCIL_SIZE_EXT = $8D55;
    GL_RENDERBUFFER_STENCIL_SIZE_OES = $8D55;
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_ANGLE = $8D56;   // api(gles2) //extension: GL_ANGLE_framebuffer_multisample
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_APPLE = $8D56;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_EXT = $8D56;   // api(gles1|gles2) //extension: GL_EXT_multisampled_render_to_texture
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_NV = $8D56;   // api(gles2) //extension: GL_NV_framebuffer_multisample
    GL_MAX_SAMPLES = $8D57;
    GL_MAX_SAMPLES_ANGLE = $8D57;   // api(gles2) //extension: GL_ANGLE_framebuffer_multisample
    GL_MAX_SAMPLES_APPLE = $8D57;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
    GL_MAX_SAMPLES_EXT = $8D57;   // api(gles1|gles2) //extension: GL_EXT_multisampled_render_to_texture
    GL_MAX_SAMPLES_NV = $8D57;   // api(gles2) //extension: GL_NV_framebuffer_multisample
    GL_TEXTURE_GEN_STR_OES = $8D60;
    GL_HALF_FLOAT_OES = $8D61;   // api(gles2) //extension: GL_OES_texture_half_float
    GL_RGB565_OES = $8D62;   // api(gles1|gles2) //extension: GL_OES_required_internalformat
    GL_RGB565 = $8D62;
    GL_ETC1_RGB8_OES = $8D64;   // api(gles1|gles2) //extension: GL_OES_compressed_ETC1_RGB8_texture
    GL_TEXTURE_EXTERNAL_OES = $8D65;   // api(gles2) //extension: GL_EXT_YUV_target
    GL_SAMPLER_EXTERNAL_OES = $8D66;   // api(gles1|gles2) //extension: GL_OES_EGL_image_external
    GL_TEXTURE_BINDING_EXTERNAL_OES = $8D67;   // api(gles2) //extension: GL_EXT_YUV_target
    GL_REQUIRED_TEXTURE_IMAGE_UNITS_OES = $8D68;   // api(gles2) //extension: GL_EXT_YUV_target
    GL_ANY_SAMPLES_PASSED_CONSERVATIVE_EXT = $8D6A;   // api(gles2) //extension: GL_EXT_occlusion_query_boolean
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_SAMPLES_EXT = $8D6C;   // api(gles1|gles2) //extension: GL_EXT_multisampled_render_to_texture
    GL_RGBA32UI_EXT = $8D70;
    GL_RGB32UI_EXT = $8D71;
    GL_ALPHA32UI_EXT = $8D72;
    GL_INTENSITY32UI_EXT = $8D73;
    GL_LUMINANCE32UI_EXT = $8D74;
    GL_LUMINANCE_ALPHA32UI_EXT = $8D75;
    GL_RGBA16UI_EXT = $8D76;
    GL_RGB16UI_EXT = $8D77;
    GL_ALPHA16UI_EXT = $8D78;
    GL_INTENSITY16UI_EXT = $8D79;
    GL_LUMINANCE16UI_EXT = $8D7A;
    GL_LUMINANCE_ALPHA16UI_EXT = $8D7B;
    GL_RGBA8UI_EXT = $8D7C;
    GL_RGB8UI_EXT = $8D7D;
    GL_ALPHA8UI_EXT = $8D7E;
    GL_INTENSITY8UI_EXT = $8D7F;
    GL_LUMINANCE8UI_EXT = $8D80;
    GL_LUMINANCE_ALPHA8UI_EXT = $8D81;
    GL_RGBA32I_EXT = $8D82;
    GL_RGB32I_EXT = $8D83;
    GL_ALPHA32I_EXT = $8D84;
    GL_INTENSITY32I_EXT = $8D85;
    GL_LUMINANCE32I_EXT = $8D86;
    GL_LUMINANCE_ALPHA32I_EXT = $8D87;
    GL_RGBA16I_EXT = $8D88;
    GL_RGB16I_EXT = $8D89;
    GL_ALPHA16I_EXT = $8D8A;
    GL_INTENSITY16I_EXT = $8D8B;
    GL_LUMINANCE16I_EXT = $8D8C;
    GL_LUMINANCE_ALPHA16I_EXT = $8D8D;
    GL_RGBA8I_EXT = $8D8E;
    GL_RGB8I_EXT = $8D8F;
    GL_ALPHA8I_EXT = $8D90;
    GL_INTENSITY8I_EXT = $8D91;
    GL_LUMINANCE8I_EXT = $8D92;
    GL_LUMINANCE_ALPHA8I_EXT = $8D93;
    GL_RED_INTEGER_EXT = $8D94;
    GL_GREEN_INTEGER = $8D95;
    GL_GREEN_INTEGER_EXT = $8D95;
    GL_BLUE_INTEGER = $8D96;
    GL_BLUE_INTEGER_EXT = $8D96;
    GL_ALPHA_INTEGER = $8D97;
    GL_ALPHA_INTEGER_EXT = $8D97;
    GL_RGB_INTEGER_EXT = $8D98;
    GL_RGBA_INTEGER_EXT = $8D99;
    GL_BGR_INTEGER = $8D9A;
    GL_BGR_INTEGER_EXT = $8D9A;
    GL_BGRA_INTEGER = $8D9B;
    GL_BGRA_INTEGER_EXT = $8D9B;
    GL_LUMINANCE_INTEGER_EXT = $8D9C;
    GL_LUMINANCE_ALPHA_INTEGER_EXT = $8D9D;
    GL_RGBA_INTEGER_MODE_EXT = $8D9E;
    GL_MAX_PROGRAM_PARAMETER_BUFFER_BINDINGS_NV = $8DA0;
    GL_MAX_PROGRAM_PARAMETER_BUFFER_SIZE_NV = $8DA1;
    GL_VERTEX_PROGRAM_PARAMETER_BUFFER_NV = $8DA2;
    GL_GEOMETRY_PROGRAM_PARAMETER_BUFFER_NV = $8DA3;
    GL_FRAGMENT_PROGRAM_PARAMETER_BUFFER_NV = $8DA4;
    GL_MAX_PROGRAM_GENERIC_ATTRIBS_NV = $8DA5;
    GL_MAX_PROGRAM_GENERIC_RESULTS_NV = $8DA6;
    GL_FRAMEBUFFER_ATTACHMENT_LAYERED_ARB = $8DA7;
    GL_FRAMEBUFFER_ATTACHMENT_LAYERED_EXT = $8DA7;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_FRAMEBUFFER_ATTACHMENT_LAYERED_OES = $8DA7;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_ARB = $8DA8;
    GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_EXT = $8DA8;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS_OES = $8DA8;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_FRAMEBUFFER_INCOMPLETE_LAYER_COUNT_ARB = $8DA9;
    GL_FRAMEBUFFER_INCOMPLETE_LAYER_COUNT_EXT = $8DA9;
    GL_LAYER_NV = $8DAA;
    GL_DEPTH_COMPONENT32F_NV = $8DAB;
    GL_DEPTH32F_STENCIL8_NV = $8DAC;
    GL_FLOAT_32_UNSIGNED_INT_24_8_REV = $8DAD;
    GL_FLOAT_32_UNSIGNED_INT_24_8_REV_NV = $8DAD;
    GL_SHADER_INCLUDE_ARB = $8DAE;
    GL_DEPTH_BUFFER_FLOAT_MODE_NV = $8DAF;
    GL_FRAMEBUFFER_SRGB = $8DB9;
    GL_FRAMEBUFFER_SRGB_EXT = $8DB9;   // api(gles2) //extension: GL_EXT_sRGB_write_control
    GL_FRAMEBUFFER_SRGB_CAPABLE_EXT = $8DBA;
    GL_COMPRESSED_RED_RGTC1 = $8DBB;
    GL_COMPRESSED_RED_RGTC1_EXT = $8DBB;   // api(gl|gles2) //extension: GL_EXT_texture_compression_rgtc
    GL_COMPRESSED_SIGNED_RED_RGTC1 = $8DBC;
    GL_COMPRESSED_SIGNED_RED_RGTC1_EXT = $8DBC;   // api(gl|gles2) //extension: GL_EXT_texture_compression_rgtc
    GL_COMPRESSED_RED_GREEN_RGTC2_EXT = $8DBD;   // api(gl|gles2) //extension: GL_EXT_texture_compression_rgtc
    GL_COMPRESSED_RG_RGTC2 = $8DBD;
    GL_COMPRESSED_SIGNED_RED_GREEN_RGTC2_EXT = $8DBE;   // api(gl|gles2) //extension: GL_EXT_texture_compression_rgtc
    GL_COMPRESSED_SIGNED_RG_RGTC2 = $8DBE;
    GL_SAMPLER_1D_ARRAY = $8DC0;
    GL_SAMPLER_1D_ARRAY_EXT = $8DC0;
    GL_SAMPLER_2D_ARRAY_EXT = $8DC1;
    GL_SAMPLER_BUFFER_EXT = $8DC2;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_SAMPLER_BUFFER_OES = $8DC2;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_SAMPLER_1D_ARRAY_SHADOW = $8DC3;
    GL_SAMPLER_1D_ARRAY_SHADOW_EXT = $8DC3;
    GL_SAMPLER_2D_ARRAY_SHADOW_EXT = $8DC4;
    GL_SAMPLER_2D_ARRAY_SHADOW_NV = $8DC4;   // api(gles2) //extension: GL_NV_shadow_samplers_array
    GL_SAMPLER_CUBE_SHADOW_EXT = $8DC5;
    GL_SAMPLER_CUBE_SHADOW_NV = $8DC5;   // api(gles2) //extension: GL_NV_shadow_samplers_cube
    GL_UNSIGNED_INT_VEC2_EXT = $8DC6;
    GL_UNSIGNED_INT_VEC3_EXT = $8DC7;
    GL_UNSIGNED_INT_VEC4_EXT = $8DC8;
    GL_INT_SAMPLER_1D = $8DC9;
    GL_INT_SAMPLER_1D_EXT = $8DC9;
    GL_INT_SAMPLER_2D_EXT = $8DCA;
    GL_INT_SAMPLER_3D_EXT = $8DCB;
    GL_INT_SAMPLER_CUBE_EXT = $8DCC;
    GL_INT_SAMPLER_2D_RECT = $8DCD;
    GL_INT_SAMPLER_2D_RECT_EXT = $8DCD;
    GL_INT_SAMPLER_1D_ARRAY = $8DCE;
    GL_INT_SAMPLER_1D_ARRAY_EXT = $8DCE;
    GL_INT_SAMPLER_2D_ARRAY_EXT = $8DCF;
    GL_INT_SAMPLER_BUFFER_EXT = $8DD0;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_INT_SAMPLER_BUFFER_OES = $8DD0;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_UNSIGNED_INT_SAMPLER_1D = $8DD1;
    GL_UNSIGNED_INT_SAMPLER_1D_EXT = $8DD1;
    GL_UNSIGNED_INT_SAMPLER_2D_EXT = $8DD2;
    GL_UNSIGNED_INT_SAMPLER_3D_EXT = $8DD3;
    GL_UNSIGNED_INT_SAMPLER_CUBE_EXT = $8DD4;
    GL_UNSIGNED_INT_SAMPLER_2D_RECT = $8DD5;
    GL_UNSIGNED_INT_SAMPLER_2D_RECT_EXT = $8DD5;
    GL_UNSIGNED_INT_SAMPLER_1D_ARRAY = $8DD6;
    GL_UNSIGNED_INT_SAMPLER_1D_ARRAY_EXT = $8DD6;
    GL_UNSIGNED_INT_SAMPLER_2D_ARRAY_EXT = $8DD7;
    GL_UNSIGNED_INT_SAMPLER_BUFFER_EXT = $8DD8;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_UNSIGNED_INT_SAMPLER_BUFFER_OES = $8DD8;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_GEOMETRY_SHADER_ARB = $8DD9;
    GL_GEOMETRY_SHADER_EXT = $8DD9;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_GEOMETRY_SHADER_OES = $8DD9;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_GEOMETRY_VERTICES_OUT_ARB = $8DDA;
    GL_GEOMETRY_VERTICES_OUT_EXT = $8DDA;
    GL_GEOMETRY_INPUT_TYPE_ARB = $8DDB;
    GL_GEOMETRY_INPUT_TYPE_EXT = $8DDB;
    GL_GEOMETRY_OUTPUT_TYPE_ARB = $8DDC;
    GL_GEOMETRY_OUTPUT_TYPE_EXT = $8DDC;
    GL_MAX_GEOMETRY_VARYING_COMPONENTS_ARB = $8DDD;
    GL_MAX_GEOMETRY_VARYING_COMPONENTS_EXT = $8DDD;
    GL_MAX_VERTEX_VARYING_COMPONENTS_ARB = $8DDE;
    GL_MAX_VERTEX_VARYING_COMPONENTS_EXT = $8DDE;
    GL_MAX_GEOMETRY_UNIFORM_COMPONENTS_ARB = $8DDF;
    GL_MAX_GEOMETRY_UNIFORM_COMPONENTS_EXT = $8DDF;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_UNIFORM_COMPONENTS_OES = $8DDF;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_GEOMETRY_OUTPUT_VERTICES = $8DE0;
    GL_MAX_GEOMETRY_OUTPUT_VERTICES_ARB = $8DE0;
    GL_MAX_GEOMETRY_OUTPUT_VERTICES_EXT = $8DE0;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_OUTPUT_VERTICES_OES = $8DE0;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = $8DE1;
    GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_ARB = $8DE1;
    GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_EXT = $8DE1;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS_OES = $8DE1;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_VERTEX_BINDABLE_UNIFORMS_EXT = $8DE2;
    GL_MAX_FRAGMENT_BINDABLE_UNIFORMS_EXT = $8DE3;
    GL_MAX_GEOMETRY_BINDABLE_UNIFORMS_EXT = $8DE4;
    GL_ACTIVE_SUBROUTINES = $8DE5;
    GL_ACTIVE_SUBROUTINE_UNIFORMS = $8DE6;
    GL_MAX_SUBROUTINES = $8DE7;
    GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS = $8DE8;
    GL_NAMED_STRING_LENGTH_ARB = $8DE9;
    GL_NAMED_STRING_TYPE_ARB = $8DEA;
    GL_MAX_BINDABLE_UNIFORM_SIZE_EXT = $8DED;
    GL_UNIFORM_BUFFER_EXT = $8DEE;
    GL_UNIFORM_BUFFER_BINDING_EXT = $8DEF;
    GL_UNSIGNED_INT_10_10_10_2_OES = $8DF6;   // api(gles2) //extension: GL_OES_vertex_type_10_10_10_2
    GL_INT_10_10_10_2_OES = $8DF7;   // api(gles2) //extension: GL_OES_vertex_type_10_10_10_2
    GL_SHADER_BINARY_FORMATS = $8DF8;
    GL_RENDERBUFFER_COLOR_SAMPLES_NV = $8E10;
    GL_MAX_MULTISAMPLE_COVERAGE_MODES_NV = $8E11;
    GL_MULTISAMPLE_COVERAGE_MODES_NV = $8E12;
    GL_QUERY_WAIT = $8E13;
    GL_QUERY_WAIT_NV = $8E13;   // api(gl|glcore|gles2) //extension: GL_NV_conditional_render
    GL_QUERY_NO_WAIT = $8E14;
    GL_QUERY_NO_WAIT_NV = $8E14;   // api(gl|glcore|gles2) //extension: GL_NV_conditional_render
    GL_QUERY_BY_REGION_WAIT = $8E15;
    GL_QUERY_BY_REGION_WAIT_NV = $8E15;   // api(gl|glcore|gles2) //extension: GL_NV_conditional_render
    GL_QUERY_BY_REGION_NO_WAIT = $8E16;
    GL_QUERY_BY_REGION_NO_WAIT_NV = $8E16;   // api(gl|glcore|gles2) //extension: GL_NV_conditional_render
    GL_QUERY_WAIT_INVERTED = $8E17;
    GL_QUERY_NO_WAIT_INVERTED = $8E18;
    GL_QUERY_BY_REGION_WAIT_INVERTED = $8E19;
    GL_QUERY_BY_REGION_NO_WAIT_INVERTED = $8E1A;
    GL_POLYGON_OFFSET_CLAMP = $8E1B;
    GL_POLYGON_OFFSET_CLAMP_EXT = $8E1B;   // api(gl|glcore|gles2) //extension: GL_EXT_polygon_offset_clamp
    GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = $8E1E;
    GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS_EXT = $8E1E;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS_OES = $8E1E;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = $8E1F;
    GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS_EXT = $8E1F;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS_OES = $8E1F;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_COLOR_SAMPLES_NV = $8E20;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_TRANSFORM_FEEDBACK_NV = $8E22;
    GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED = $8E23;
    GL_TRANSFORM_FEEDBACK_BUFFER_PAUSED_NV = $8E23;
    GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE = $8E24;
    GL_TRANSFORM_FEEDBACK_BUFFER_ACTIVE_NV = $8E24;
    GL_TRANSFORM_FEEDBACK_BINDING = $8E25;
    GL_TRANSFORM_FEEDBACK_BINDING_NV = $8E25;
    GL_FRAME_NV = $8E26;
    GL_FIELDS_NV = $8E27;
    GL_CURRENT_TIME_NV = $8E28;
    GL_TIMESTAMP = $8E28;
    GL_TIMESTAMP_EXT = $8E28;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_NUM_FILL_STREAMS_NV = $8E29;
    GL_PRESENT_TIME_NV = $8E2A;
    GL_PRESENT_DURATION_NV = $8E2B;
    GL_DEPTH_COMPONENT16_NONLINEAR_NV = $8E2C;   // api(gles2) //extension: GL_NV_depth_nonlinear
    GL_PROGRAM_MATRIX_EXT = $8E2D;
    GL_TRANSPOSE_PROGRAM_MATRIX_EXT = $8E2E;
    GL_PROGRAM_MATRIX_STACK_DEPTH_EXT = $8E2F;
    GL_TEXTURE_SWIZZLE_R_EXT = $8E42;
    GL_TEXTURE_SWIZZLE_G_EXT = $8E43;
    GL_TEXTURE_SWIZZLE_B_EXT = $8E44;
    GL_TEXTURE_SWIZZLE_A_EXT = $8E45;
    GL_TEXTURE_SWIZZLE_RGBA = $8E46;
    GL_TEXTURE_SWIZZLE_RGBA_EXT = $8E46;
    GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS = $8E47;
    GL_ACTIVE_SUBROUTINE_MAX_LENGTH = $8E48;
    GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH = $8E49;
    GL_NUM_COMPATIBLE_SUBROUTINES = $8E4A;
    GL_COMPATIBLE_SUBROUTINES = $8E4B;
    GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION = $8E4C;
    GL_QUADS_FOLLOW_PROVOKING_VERTEX_CONVENTION_EXT = $8E4C;
    GL_FIRST_VERTEX_CONVENTION_EXT = $8E4D;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_FIRST_VERTEX_CONVENTION_OES = $8E4D;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_LAST_VERTEX_CONVENTION_EXT = $8E4E;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_LAST_VERTEX_CONVENTION_OES = $8E4E;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_PROVOKING_VERTEX = $8E4F;
    GL_PROVOKING_VERTEX_EXT = $8E4F;
    GL_SAMPLE_POSITION_NV = $8E50;
    GL_SAMPLE_LOCATION_ARB = $8E50;
    GL_SAMPLE_LOCATION_NV = $8E50;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_SAMPLE_MASK_NV = $8E51;
    GL_SAMPLE_MASK_VALUE = $8E52;
    GL_SAMPLE_MASK_VALUE_NV = $8E52;
    GL_TEXTURE_BINDING_RENDERBUFFER_NV = $8E53;
    GL_TEXTURE_RENDERBUFFER_DATA_STORE_BINDING_NV = $8E54;
    GL_TEXTURE_RENDERBUFFER_NV = $8E55;
    GL_SAMPLER_RENDERBUFFER_NV = $8E56;
    GL_INT_SAMPLER_RENDERBUFFER_NV = $8E57;
    GL_UNSIGNED_INT_SAMPLER_RENDERBUFFER_NV = $8E58;
    GL_MAX_SAMPLE_MASK_WORDS_NV = $8E59;
    GL_MAX_GEOMETRY_PROGRAM_INVOCATIONS_NV = $8E5A;
    GL_MAX_GEOMETRY_SHADER_INVOCATIONS = $8E5A;
    GL_MAX_GEOMETRY_SHADER_INVOCATIONS_EXT = $8E5A;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_SHADER_INVOCATIONS_OES = $8E5A;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MIN_FRAGMENT_INTERPOLATION_OFFSET = $8E5B;
    GL_MIN_FRAGMENT_INTERPOLATION_OFFSET_OES = $8E5B;   // api(gles2) //extension: GL_OES_shader_multisample_interpolation
    GL_MIN_FRAGMENT_INTERPOLATION_OFFSET_NV = $8E5B;
    GL_MAX_FRAGMENT_INTERPOLATION_OFFSET = $8E5C;
    GL_MAX_FRAGMENT_INTERPOLATION_OFFSET_OES = $8E5C;   // api(gles2) //extension: GL_OES_shader_multisample_interpolation
    GL_MAX_FRAGMENT_INTERPOLATION_OFFSET_NV = $8E5C;
    GL_FRAGMENT_INTERPOLATION_OFFSET_BITS = $8E5D;
    GL_FRAGMENT_INTERPOLATION_OFFSET_BITS_OES = $8E5D;   // api(gles2) //extension: GL_OES_shader_multisample_interpolation
    GL_FRAGMENT_PROGRAM_INTERPOLATION_OFFSET_BITS_NV = $8E5D;
    GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET = $8E5E;
    GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET_ARB = $8E5E;
    GL_MIN_PROGRAM_TEXTURE_GATHER_OFFSET_NV = $8E5E;
    GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET = $8E5F;
    GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET_ARB = $8E5F;
    GL_MAX_PROGRAM_TEXTURE_GATHER_OFFSET_NV = $8E5F;
    GL_MAX_MESH_UNIFORM_BLOCKS_NV = $8E60;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_TEXTURE_IMAGE_UNITS_NV = $8E61;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_IMAGE_UNIFORMS_NV = $8E62;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_UNIFORM_COMPONENTS_NV = $8E63;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_ATOMIC_COUNTER_BUFFERS_NV = $8E64;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_ATOMIC_COUNTERS_NV = $8E65;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_SHADER_STORAGE_BLOCKS_NV = $8E66;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_COMBINED_MESH_UNIFORM_COMPONENTS_NV = $8E67;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_UNIFORM_BLOCKS_NV = $8E68;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_TEXTURE_IMAGE_UNITS_NV = $8E69;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_IMAGE_UNIFORMS_NV = $8E6A;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_UNIFORM_COMPONENTS_NV = $8E6B;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_ATOMIC_COUNTER_BUFFERS_NV = $8E6C;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_ATOMIC_COUNTERS_NV = $8E6D;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_SHADER_STORAGE_BLOCKS_NV = $8E6E;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_COMBINED_TASK_UNIFORM_COMPONENTS_NV = $8E6F;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TRANSFORM_FEEDBACK_BUFFERS = $8E70;
    GL_MAX_VERTEX_STREAMS = $8E71;
    GL_PATCH_VERTICES_EXT = $8E72;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_PATCH_VERTICES_OES = $8E72;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_PATCH_DEFAULT_INNER_LEVEL = $8E73;
    GL_PATCH_DEFAULT_INNER_LEVEL_EXT = $8E73;
    GL_PATCH_DEFAULT_OUTER_LEVEL = $8E74;
    GL_PATCH_DEFAULT_OUTER_LEVEL_EXT = $8E74;
    GL_TESS_CONTROL_OUTPUT_VERTICES = $8E75;
    GL_TESS_CONTROL_OUTPUT_VERTICES_EXT = $8E75;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_CONTROL_OUTPUT_VERTICES_OES = $8E75;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_GEN_MODE = $8E76;
    GL_TESS_GEN_MODE_EXT = $8E76;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_GEN_MODE_OES = $8E76;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_GEN_SPACING = $8E77;
    GL_TESS_GEN_SPACING_EXT = $8E77;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_GEN_SPACING_OES = $8E77;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_GEN_VERTEX_ORDER = $8E78;
    GL_TESS_GEN_VERTEX_ORDER_EXT = $8E78;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_GEN_VERTEX_ORDER_OES = $8E78;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_GEN_POINT_MODE = $8E79;
    GL_TESS_GEN_POINT_MODE_EXT = $8E79;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_GEN_POINT_MODE_OES = $8E79;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_ISOLINES = $8E7A;
    GL_ISOLINES_EXT = $8E7A;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_ISOLINES_OES = $8E7A;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_FRACTIONAL_ODD = $8E7B;
    GL_FRACTIONAL_ODD_EXT = $8E7B;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_FRACTIONAL_ODD_OES = $8E7B;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_FRACTIONAL_EVEN = $8E7C;
    GL_FRACTIONAL_EVEN_EXT = $8E7C;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_FRACTIONAL_EVEN_OES = $8E7C;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_PATCH_VERTICES = $8E7D;
    GL_MAX_PATCH_VERTICES_EXT = $8E7D;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_PATCH_VERTICES_OES = $8E7D;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_GEN_LEVEL = $8E7E;
    GL_MAX_TESS_GEN_LEVEL_EXT = $8E7E;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_GEN_LEVEL_OES = $8E7E;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS = $8E7F;
    GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS_EXT = $8E7F;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_UNIFORM_COMPONENTS_OES = $8E7F;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = $8E80;
    GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS_EXT = $8E80;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_UNIFORM_COMPONENTS_OES = $8E80;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = $8E81;
    GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS_EXT = $8E81;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS_OES = $8E81;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = $8E82;
    GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS_EXT = $8E82;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS_OES = $8E82;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS = $8E83;
    GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS_EXT = $8E83;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_OUTPUT_COMPONENTS_OES = $8E83;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_PATCH_COMPONENTS = $8E84;
    GL_MAX_TESS_PATCH_COMPONENTS_EXT = $8E84;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_PATCH_COMPONENTS_OES = $8E84;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = $8E85;
    GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS_EXT = $8E85;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS_OES = $8E85;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = $8E86;
    GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS_EXT = $8E86;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_OUTPUT_COMPONENTS_OES = $8E86;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_EVALUATION_SHADER_EXT = $8E87;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_EVALUATION_SHADER_OES = $8E87;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_TESS_CONTROL_SHADER_EXT = $8E88;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_TESS_CONTROL_SHADER_OES = $8E88;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS = $8E89;
    GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS_EXT = $8E89;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_UNIFORM_BLOCKS_OES = $8E89;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS = $8E8A;
    GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS_EXT = $8E8A;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_UNIFORM_BLOCKS_OES = $8E8A;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_COMPRESSED_RGBA_BPTC_UNORM = $8E8C;
    GL_COMPRESSED_RGBA_BPTC_UNORM_ARB = $8E8C;
    GL_COMPRESSED_RGBA_BPTC_UNORM_EXT = $8E8C;   // api(gles2) //extension: GL_EXT_texture_compression_bptc
    GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM = $8E8D;
    GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM_ARB = $8E8D;
    GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM_EXT = $8E8D;   // api(gles2) //extension: GL_EXT_texture_compression_bptc
    GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT = $8E8E;
    GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_ARB = $8E8E;
    GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT_EXT = $8E8E;   // api(gles2) //extension: GL_EXT_texture_compression_bptc
    GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT = $8E8F;
    GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_ARB = $8E8F;
    GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_EXT = $8E8F;   // api(gles2) //extension: GL_EXT_texture_compression_bptc
    GL_COVERAGE_COMPONENT_NV = $8ED0;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_COMPONENT4_NV = $8ED1;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_ATTACHMENT_NV = $8ED2;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_BUFFERS_NV = $8ED3;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_SAMPLES_NV = $8ED4;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_ALL_FRAGMENTS_NV = $8ED5;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_EDGE_FRAGMENTS_NV = $8ED6;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_COVERAGE_AUTOMATIC_NV = $8ED7;   // api(gles2) //extension: GL_NV_coverage_sample
    GL_INCLUSIVE_EXT = $8F10;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
    GL_EXCLUSIVE_EXT = $8F11;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
    GL_WINDOW_RECTANGLE_EXT = $8F12;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
    GL_WINDOW_RECTANGLE_MODE_EXT = $8F13;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
    GL_MAX_WINDOW_RECTANGLES_EXT = $8F14;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
    GL_NUM_WINDOW_RECTANGLES_EXT = $8F15;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
    GL_BUFFER_GPU_ADDRESS_NV = $8F1D;
    GL_VERTEX_ATTRIB_ARRAY_UNIFIED_NV = $8F1E;
    GL_ELEMENT_ARRAY_UNIFIED_NV = $8F1F;
    GL_VERTEX_ATTRIB_ARRAY_ADDRESS_NV = $8F20;
    GL_VERTEX_ARRAY_ADDRESS_NV = $8F21;
    GL_NORMAL_ARRAY_ADDRESS_NV = $8F22;
    GL_COLOR_ARRAY_ADDRESS_NV = $8F23;
    GL_INDEX_ARRAY_ADDRESS_NV = $8F24;
    GL_TEXTURE_COORD_ARRAY_ADDRESS_NV = $8F25;
    GL_EDGE_FLAG_ARRAY_ADDRESS_NV = $8F26;
    GL_SECONDARY_COLOR_ARRAY_ADDRESS_NV = $8F27;
    GL_FOG_COORD_ARRAY_ADDRESS_NV = $8F28;
    GL_ELEMENT_ARRAY_ADDRESS_NV = $8F29;
    GL_VERTEX_ATTRIB_ARRAY_LENGTH_NV = $8F2A;
    GL_VERTEX_ARRAY_LENGTH_NV = $8F2B;
    GL_NORMAL_ARRAY_LENGTH_NV = $8F2C;
    GL_COLOR_ARRAY_LENGTH_NV = $8F2D;
    GL_INDEX_ARRAY_LENGTH_NV = $8F2E;
    GL_TEXTURE_COORD_ARRAY_LENGTH_NV = $8F2F;
    GL_EDGE_FLAG_ARRAY_LENGTH_NV = $8F30;
    GL_SECONDARY_COLOR_ARRAY_LENGTH_NV = $8F31;
    GL_FOG_COORD_ARRAY_LENGTH_NV = $8F32;
    GL_ELEMENT_ARRAY_LENGTH_NV = $8F33;
    GL_GPU_ADDRESS_NV = $8F34;
    GL_MAX_SHADER_BUFFER_ADDRESS_NV = $8F35;
    GL_COPY_READ_BUFFER_NV = $8F36;   // api(gles2) //extension: GL_NV_copy_buffer
    GL_COPY_READ_BUFFER_BINDING = $8F36;
    GL_COPY_WRITE_BUFFER_NV = $8F37;   // api(gles2) //extension: GL_NV_copy_buffer
    GL_COPY_WRITE_BUFFER_BINDING = $8F37;
    GL_MAX_IMAGE_UNITS = $8F38;
    GL_MAX_IMAGE_UNITS_EXT = $8F38;
    GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS = $8F39;
    GL_MAX_COMBINED_IMAGE_UNITS_AND_FRAGMENT_OUTPUTS_EXT = $8F39;
    GL_MAX_COMBINED_SHADER_OUTPUT_RESOURCES = $8F39;
    GL_IMAGE_BINDING_NAME = $8F3A;
    GL_IMAGE_BINDING_NAME_EXT = $8F3A;
    GL_IMAGE_BINDING_LEVEL = $8F3B;
    GL_IMAGE_BINDING_LEVEL_EXT = $8F3B;
    GL_IMAGE_BINDING_LAYERED = $8F3C;
    GL_IMAGE_BINDING_LAYERED_EXT = $8F3C;
    GL_IMAGE_BINDING_LAYER = $8F3D;
    GL_IMAGE_BINDING_LAYER_EXT = $8F3D;
    GL_IMAGE_BINDING_ACCESS = $8F3E;
    GL_IMAGE_BINDING_ACCESS_EXT = $8F3E;
    GL_DRAW_INDIRECT_UNIFIED_NV = $8F40;
    GL_DRAW_INDIRECT_ADDRESS_NV = $8F41;
    GL_DRAW_INDIRECT_LENGTH_NV = $8F42;
    GL_DRAW_INDIRECT_BUFFER_BINDING = $8F43;
    GL_MAX_PROGRAM_SUBROUTINE_PARAMETERS_NV = $8F44;
    GL_MAX_PROGRAM_SUBROUTINE_NUM_NV = $8F45;
    GL_DOUBLE_MAT2 = $8F46;
    GL_DOUBLE_MAT2_EXT = $8F46;
    GL_DOUBLE_MAT3 = $8F47;
    GL_DOUBLE_MAT3_EXT = $8F47;
    GL_DOUBLE_MAT4 = $8F48;
    GL_DOUBLE_MAT4_EXT = $8F48;
    GL_DOUBLE_MAT2x3 = $8F49;
    GL_DOUBLE_MAT2x3_EXT = $8F49;
    GL_DOUBLE_MAT2x4 = $8F4A;
    GL_DOUBLE_MAT2x4_EXT = $8F4A;
    GL_DOUBLE_MAT3x2 = $8F4B;
    GL_DOUBLE_MAT3x2_EXT = $8F4B;
    GL_DOUBLE_MAT3x4 = $8F4C;
    GL_DOUBLE_MAT3x4_EXT = $8F4C;
    GL_DOUBLE_MAT4x2 = $8F4D;
    GL_DOUBLE_MAT4x2_EXT = $8F4D;
    GL_DOUBLE_MAT4x3 = $8F4E;
    GL_DOUBLE_MAT4x3_EXT = $8F4E;
    GL_VERTEX_BINDING_BUFFER = $8F4F;
    GL_MALI_SHADER_BINARY_ARM = $8F60;   // api(gles2) //extension: GL_ARM_mali_shader_binary
    GL_MALI_PROGRAM_BINARY_ARM = $8F61;   // api(gles2) //extension: GL_ARM_mali_program_binary
    GL_MAX_SHADER_PIXEL_LOCAL_STORAGE_FAST_SIZE_EXT = $8F63;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage
    GL_SHADER_PIXEL_LOCAL_STORAGE_EXT = $8F64;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage
    GL_FETCH_PER_SAMPLE_ARM = $8F65;   // api(gles2) //extension: GL_ARM_shader_framebuffer_fetch
    GL_FRAGMENT_SHADER_FRAMEBUFFER_FETCH_MRT_ARM = $8F66;   // api(gles2) //extension: GL_ARM_shader_framebuffer_fetch
    GL_MAX_SHADER_PIXEL_LOCAL_STORAGE_SIZE_EXT = $8F67;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage
    GL_TEXTURE_ASTC_DECODE_PRECISION_EXT = $8F69;   // api(gles2) //extension: GL_EXT_texture_compression_astc_decode_mode
    GL_TEXTURE_UNNORMALIZED_COORDINATES_ARM = $8F6A;   // api(gles2) //extension: GL_ARM_texture_unnormalized_coordinates
    GL_RED_SNORM = $8F90;
    GL_RG_SNORM = $8F91;
    GL_RGB_SNORM = $8F92;
    GL_RGBA_SNORM = $8F93;
    GL_R16_SNORM = $8F98;
    GL_R16_SNORM_EXT = $8F98;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_RG16_SNORM = $8F99;
    GL_RG16_SNORM_EXT = $8F99;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_RGB16_SNORM = $8F9A;
    GL_RGB16_SNORM_EXT = $8F9A;   // api(gles2) //extension: GL_EXT_texture_norm16
    GL_RGBA16_SNORM = $8F9B;
    GL_RGBA16_SNORM_EXT = $8F9B;   // api(gles2) //extension: GL_EXT_render_snorm
    GL_SIGNED_NORMALIZED = $8F9C;
    GL_PRIMITIVE_RESTART = $8F9D;
    GL_PRIMITIVE_RESTART_INDEX = $8F9E;
    GL_MAX_PROGRAM_TEXTURE_GATHER_COMPONENTS_ARB = $8F9F;
    GL_PERFMON_GLOBAL_MODE_QCOM = $8FA0;   // api(gles1|gles2) //extension: GL_QCOM_perfmon_global_mode
    GL_MAX_SHADER_SUBSAMPLED_IMAGE_UNITS_QCOM = $8FA1;   // api(gles2) //extension: GL_QCOM_texture_foveated_subsampled_layout
    GL_BINNING_CONTROL_HINT_QCOM = $8FB0;   // api(gles2) //extension: GL_QCOM_binning_control
    GL_CPU_OPTIMIZED_QCOM = $8FB1;   // api(gles2) //extension: GL_QCOM_binning_control
    GL_GPU_OPTIMIZED_QCOM = $8FB2;   // api(gles2) //extension: GL_QCOM_binning_control
    GL_RENDER_DIRECT_TO_FRAMEBUFFER_QCOM = $8FB3;   // api(gles2) //extension: GL_QCOM_binning_control
    GL_GPU_DISJOINT_EXT = $8FBB;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
    GL_SR8_EXT = $8FBD;   // api(gles2|gl|glcore) //extension: GL_EXT_texture_sRGB_R8
    GL_SRG8_EXT = $8FBE;   // api(gles2) //extension: GL_EXT_texture_sRGB_RG8
    GL_TEXTURE_FORMAT_SRGB_OVERRIDE_EXT = $8FBF;   // api(gles2) //extension: GL_EXT_texture_format_sRGB_override
    GL_SHADER_BINARY_VIV = $8FC4;   // api(gles2) //extension: GL_VIV_shader_binary
    GL_INT8_NV = $8FE0;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT8_VEC2_NV = $8FE1;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT8_VEC3_NV = $8FE2;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT8_VEC4_NV = $8FE3;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT16_NV = $8FE4;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT16_VEC2_NV = $8FE5;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT16_VEC3_NV = $8FE6;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT16_VEC4_NV = $8FE7;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT64_VEC2_ARB = $8FE9;
    GL_INT64_VEC2_NV = $8FE9;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT64_VEC3_ARB = $8FEA;
    GL_INT64_VEC3_NV = $8FEA;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_INT64_VEC4_ARB = $8FEB;
    GL_INT64_VEC4_NV = $8FEB;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT8_NV = $8FEC;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT8_VEC2_NV = $8FED;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT8_VEC3_NV = $8FEE;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT8_VEC4_NV = $8FEF;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT16_NV = $8FF0;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT16_VEC2_NV = $8FF1;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT16_VEC3_NV = $8FF2;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT16_VEC4_NV = $8FF3;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT64_VEC2_ARB = $8FF5;
    GL_UNSIGNED_INT64_VEC2_NV = $8FF5;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT64_VEC3_ARB = $8FF6;
    GL_UNSIGNED_INT64_VEC3_NV = $8FF6;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_UNSIGNED_INT64_VEC4_ARB = $8FF7;
    GL_UNSIGNED_INT64_VEC4_NV = $8FF7;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_FLOAT16_NV = $8FF8;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_FLOAT16_VEC2_NV = $8FF9;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_FLOAT16_VEC3_NV = $8FFA;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_FLOAT16_VEC4_NV = $8FFB;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
    GL_DOUBLE_VEC2 = $8FFC;
    GL_DOUBLE_VEC2_EXT = $8FFC;
    GL_DOUBLE_VEC3 = $8FFD;
    GL_DOUBLE_VEC3_EXT = $8FFD;
    GL_DOUBLE_VEC4 = $8FFE;
    GL_DOUBLE_VEC4_EXT = $8FFE;
    GL_SAMPLER_BUFFER_AMD = $9001;
    GL_INT_SAMPLER_BUFFER_AMD = $9002;
    GL_UNSIGNED_INT_SAMPLER_BUFFER_AMD = $9003;
    GL_TESSELLATION_MODE_AMD = $9004;
    GL_TESSELLATION_FACTOR_AMD = $9005;
    GL_DISCRETE_AMD = $9006;
    GL_CONTINUOUS_AMD = $9007;
    GL_TEXTURE_CUBE_MAP_ARRAY_ARB = $9009;
    GL_TEXTURE_CUBE_MAP_ARRAY_EXT = $9009;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_TEXTURE_CUBE_MAP_ARRAY_OES = $9009;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_TEXTURE_BINDING_CUBE_MAP_ARRAY = $900A;
    GL_TEXTURE_BINDING_CUBE_MAP_ARRAY_ARB = $900A;
    GL_TEXTURE_BINDING_CUBE_MAP_ARRAY_EXT = $900A;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_TEXTURE_BINDING_CUBE_MAP_ARRAY_OES = $900A;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_PROXY_TEXTURE_CUBE_MAP_ARRAY = $900B;
    GL_PROXY_TEXTURE_CUBE_MAP_ARRAY_ARB = $900B;
    GL_SAMPLER_CUBE_MAP_ARRAY_ARB = $900C;
    GL_SAMPLER_CUBE_MAP_ARRAY_EXT = $900C;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_SAMPLER_CUBE_MAP_ARRAY_OES = $900C;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW_ARB = $900D;
    GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW_EXT = $900D;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW_OES = $900D;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_INT_SAMPLER_CUBE_MAP_ARRAY_ARB = $900E;
    GL_INT_SAMPLER_CUBE_MAP_ARRAY_EXT = $900E;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_INT_SAMPLER_CUBE_MAP_ARRAY_OES = $900E;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY_ARB = $900F;
    GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY_EXT = $900F;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_UNSIGNED_INT_SAMPLER_CUBE_MAP_ARRAY_OES = $900F;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_ALPHA_SNORM = $9010;
    GL_LUMINANCE_SNORM = $9011;
    GL_LUMINANCE_ALPHA_SNORM = $9012;
    GL_INTENSITY_SNORM = $9013;
    GL_ALPHA8_SNORM = $9014;
    GL_LUMINANCE8_SNORM = $9015;
    GL_LUMINANCE8_ALPHA8_SNORM = $9016;
    GL_INTENSITY8_SNORM = $9017;
    GL_ALPHA16_SNORM = $9018;
    GL_LUMINANCE16_SNORM = $9019;
    GL_LUMINANCE16_ALPHA16_SNORM = $901A;
    GL_INTENSITY16_SNORM = $901B;
    GL_FACTOR_MIN_AMD = $901C;   // api(gl|glcore|gles2) //extension: GL_NV_blend_minmax_factor
    GL_FACTOR_MAX_AMD = $901D;   // api(gl|glcore|gles2) //extension: GL_NV_blend_minmax_factor
    GL_DEPTH_CLAMP_NEAR_AMD = $901E;
    GL_DEPTH_CLAMP_FAR_AMD = $901F;
    GL_VIDEO_BUFFER_NV = $9020;
    GL_VIDEO_BUFFER_BINDING_NV = $9021;
    GL_FIELD_UPPER_NV = $9022;
    GL_FIELD_LOWER_NV = $9023;
    GL_NUM_VIDEO_CAPTURE_STREAMS_NV = $9024;
    GL_NEXT_VIDEO_CAPTURE_BUFFER_STATUS_NV = $9025;
    GL_VIDEO_CAPTURE_TO_422_SUPPORTED_NV = $9026;
    GL_LAST_VIDEO_CAPTURE_STATUS_NV = $9027;
    GL_VIDEO_BUFFER_PITCH_NV = $9028;
    GL_VIDEO_COLOR_CONVERSION_MATRIX_NV = $9029;
    GL_VIDEO_COLOR_CONVERSION_MAX_NV = $902A;
    GL_VIDEO_COLOR_CONVERSION_MIN_NV = $902B;
    GL_VIDEO_COLOR_CONVERSION_OFFSET_NV = $902C;
    GL_VIDEO_BUFFER_INTERNAL_FORMAT_NV = $902D;
    GL_PARTIAL_SUCCESS_NV = $902E;
    GL_SUCCESS_NV = $902F;
    GL_FAILURE_NV = $9030;
    GL_YCBYCR8_422_NV = $9031;
    GL_YCBAYCR8A_4224_NV = $9032;
    GL_Z6Y10Z6CB10Z6Y10Z6CR10_422_NV = $9033;
    GL_Z6Y10Z6CB10Z6A10Z6Y10Z6CR10Z6A10_4224_NV = $9034;
    GL_Z4Y12Z4CB12Z4Y12Z4CR12_422_NV = $9035;
    GL_Z4Y12Z4CB12Z4A12Z4Y12Z4CR12Z4A12_4224_NV = $9036;
    GL_Z4Y12Z4CB12Z4CR12_444_NV = $9037;
    GL_VIDEO_CAPTURE_FRAME_WIDTH_NV = $9038;
    GL_VIDEO_CAPTURE_FRAME_HEIGHT_NV = $9039;
    GL_VIDEO_CAPTURE_FIELD_UPPER_HEIGHT_NV = $903A;
    GL_VIDEO_CAPTURE_FIELD_LOWER_HEIGHT_NV = $903B;
    GL_VIDEO_CAPTURE_SURFACE_ORIGIN_NV = $903C;
    GL_TEXTURE_COVERAGE_SAMPLES_NV = $9045;
    GL_TEXTURE_COLOR_SAMPLES_NV = $9046;
    GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX = $9047;
    GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX = $9048;
    GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX = $9049;
    GL_GPU_MEMORY_INFO_EVICTION_COUNT_NVX = $904A;
    GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX = $904B;
    GL_IMAGE_1D = $904C;
    GL_IMAGE_1D_EXT = $904C;
    GL_IMAGE_2D_EXT = $904D;
    GL_IMAGE_3D_EXT = $904E;
    GL_IMAGE_2D_RECT = $904F;
    GL_IMAGE_2D_RECT_EXT = $904F;
    GL_IMAGE_CUBE_EXT = $9050;
    GL_IMAGE_BUFFER_EXT = $9051;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_IMAGE_BUFFER_OES = $9051;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_IMAGE_1D_ARRAY = $9052;
    GL_IMAGE_1D_ARRAY_EXT = $9052;
    GL_IMAGE_2D_ARRAY_EXT = $9053;
    GL_IMAGE_CUBE_MAP_ARRAY_EXT = $9054;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_IMAGE_CUBE_MAP_ARRAY_OES = $9054;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_IMAGE_2D_MULTISAMPLE = $9055;
    GL_IMAGE_2D_MULTISAMPLE_EXT = $9055;
    GL_IMAGE_2D_MULTISAMPLE_ARRAY = $9056;
    GL_IMAGE_2D_MULTISAMPLE_ARRAY_EXT = $9056;
    GL_INT_IMAGE_1D = $9057;
    GL_INT_IMAGE_1D_EXT = $9057;
    GL_INT_IMAGE_2D_EXT = $9058;
    GL_INT_IMAGE_3D_EXT = $9059;
    GL_INT_IMAGE_2D_RECT = $905A;
    GL_INT_IMAGE_2D_RECT_EXT = $905A;
    GL_INT_IMAGE_CUBE_EXT = $905B;
    GL_INT_IMAGE_BUFFER_EXT = $905C;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_INT_IMAGE_BUFFER_OES = $905C;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_INT_IMAGE_1D_ARRAY = $905D;
    GL_INT_IMAGE_1D_ARRAY_EXT = $905D;
    GL_INT_IMAGE_2D_ARRAY_EXT = $905E;
    GL_INT_IMAGE_CUBE_MAP_ARRAY_EXT = $905F;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_INT_IMAGE_CUBE_MAP_ARRAY_OES = $905F;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_INT_IMAGE_2D_MULTISAMPLE = $9060;
    GL_INT_IMAGE_2D_MULTISAMPLE_EXT = $9060;
    GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY = $9061;
    GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY_EXT = $9061;
    GL_UNSIGNED_INT_IMAGE_1D = $9062;
    GL_UNSIGNED_INT_IMAGE_1D_EXT = $9062;
    GL_UNSIGNED_INT_IMAGE_2D_EXT = $9063;
    GL_UNSIGNED_INT_IMAGE_3D_EXT = $9064;
    GL_UNSIGNED_INT_IMAGE_2D_RECT = $9065;
    GL_UNSIGNED_INT_IMAGE_2D_RECT_EXT = $9065;
    GL_UNSIGNED_INT_IMAGE_CUBE_EXT = $9066;
    GL_UNSIGNED_INT_IMAGE_BUFFER_EXT = $9067;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_UNSIGNED_INT_IMAGE_BUFFER_OES = $9067;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_UNSIGNED_INT_IMAGE_1D_ARRAY = $9068;
    GL_UNSIGNED_INT_IMAGE_1D_ARRAY_EXT = $9068;
    GL_UNSIGNED_INT_IMAGE_2D_ARRAY_EXT = $9069;
    GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY_EXT = $906A;   // api(gles2) //extension: GL_EXT_texture_cube_map_array
    GL_UNSIGNED_INT_IMAGE_CUBE_MAP_ARRAY_OES = $906A;   // api(gles2) //extension: GL_OES_texture_cube_map_array
    GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE = $906B;
    GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_EXT = $906B;
    GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY = $906C;
    GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY_EXT = $906C;
    GL_MAX_IMAGE_SAMPLES = $906D;
    GL_MAX_IMAGE_SAMPLES_EXT = $906D;
    GL_IMAGE_BINDING_FORMAT = $906E;
    GL_IMAGE_BINDING_FORMAT_EXT = $906E;
    GL_PATH_FORMAT_SVG_NV = $9070;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_FORMAT_PS_NV = $9071;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_STANDARD_FONT_NAME_NV = $9072;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SYSTEM_FONT_NAME_NV = $9073;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FILE_NAME_NV = $9074;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STROKE_WIDTH_NV = $9075;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_END_CAPS_NV = $9076;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_INITIAL_END_CAP_NV = $9077;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_TERMINAL_END_CAP_NV = $9078;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_JOIN_STYLE_NV = $9079;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_MITER_LIMIT_NV = $907A;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_DASH_CAPS_NV = $907B;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_INITIAL_DASH_CAP_NV = $907C;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_TERMINAL_DASH_CAP_NV = $907D;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_DASH_OFFSET_NV = $907E;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_CLIENT_LENGTH_NV = $907F;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_FILL_MODE_NV = $9080;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_FILL_MASK_NV = $9081;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_FILL_COVER_MODE_NV = $9082;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STROKE_COVER_MODE_NV = $9083;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STROKE_MASK_NV = $9084;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_COUNT_UP_NV = $9088;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_COUNT_DOWN_NV = $9089;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_OBJECT_BOUNDING_BOX_NV = $908A;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_CONVEX_HULL_NV = $908B;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_BOUNDING_BOX_NV = $908D;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSLATE_X_NV = $908E;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSLATE_Y_NV = $908F;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSLATE_2D_NV = $9090;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSLATE_3D_NV = $9091;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_AFFINE_2D_NV = $9092;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_AFFINE_3D_NV = $9094;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSPOSE_AFFINE_2D_NV = $9096;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRANSPOSE_AFFINE_3D_NV = $9098;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_UTF8_NV = $909A;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_UTF16_NV = $909B;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_BOUNDING_BOX_OF_BOUNDING_BOXES_NV = $909C;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_COMMAND_COUNT_NV = $909D;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_COORD_COUNT_NV = $909E;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_DASH_ARRAY_COUNT_NV = $909F;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_COMPUTED_LENGTH_NV = $90A0;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_FILL_BOUNDING_BOX_NV = $90A1;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STROKE_BOUNDING_BOX_NV = $90A2;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SQUARE_NV = $90A3;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ROUND_NV = $90A4;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_TRIANGULAR_NV = $90A5;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_BEVEL_NV = $90A6;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MITER_REVERT_NV = $90A7;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MITER_TRUNCATE_NV = $90A8;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SKIP_MISSING_GLYPH_NV = $90A9;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_USE_MISSING_GLYPH_NV = $90AA;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_ERROR_POSITION_NV = $90AB;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_FOG_GEN_MODE_NV = $90AC;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ACCUM_ADJACENT_PAIRS_NV = $90AD;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_ADJACENT_PAIRS_NV = $90AE;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FIRST_TO_REST_NV = $90AF;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_GEN_MODE_NV = $90B0;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_GEN_COEFF_NV = $90B1;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_GEN_COLOR_FORMAT_NV = $90B2;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_GEN_COMPONENTS_NV = $90B3;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_DASH_OFFSET_RESET_NV = $90B4;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MOVE_TO_RESETS_NV = $90B5;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_MOVE_TO_CONTINUES_NV = $90B6;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STENCIL_FUNC_NV = $90B7;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STENCIL_REF_NV = $90B8;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STENCIL_VALUE_MASK_NV = $90B9;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_SCALED_RESOLVE_FASTEST_EXT = $90BA;
    GL_SCALED_RESOLVE_NICEST_EXT = $90BB;
    GL_MIN_MAP_BUFFER_ALIGNMENT = $90BC;
    GL_PATH_STENCIL_DEPTH_OFFSET_FACTOR_NV = $90BD;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_STENCIL_DEPTH_OFFSET_UNITS_NV = $90BE;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_PATH_COVER_DEPTH_FUNC_NV = $90BF;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_IMAGE_FORMAT_COMPATIBILITY_BY_SIZE = $90C8;
    GL_IMAGE_FORMAT_COMPATIBILITY_BY_CLASS = $90C9;
    GL_MAX_VERTEX_IMAGE_UNIFORMS = $90CA;
    GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS = $90CB;
    GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS_EXT = $90CB;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_IMAGE_UNIFORMS_OES = $90CB;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS = $90CC;
    GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS_EXT = $90CC;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_IMAGE_UNIFORMS_OES = $90CC;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_GEOMETRY_IMAGE_UNIFORMS = $90CD;
    GL_MAX_GEOMETRY_IMAGE_UNIFORMS_EXT = $90CD;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_IMAGE_UNIFORMS_OES = $90CD;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_FRAGMENT_IMAGE_UNIFORMS = $90CE;
    GL_MAX_COMBINED_IMAGE_UNIFORMS = $90CF;
    GL_MAX_DEEP_3D_TEXTURE_WIDTH_HEIGHT_NV = $90D0;
    GL_MAX_DEEP_3D_TEXTURE_DEPTH_NV = $90D1;
    GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS_EXT = $90D7;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS_OES = $90D7;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS_EXT = $90D8;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS_OES = $90D8;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS_EXT = $90D9;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS_OES = $90D9;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_SHADER_STORAGE_BLOCK_SIZE = $90DE;
    GL_SYNC_X11_FENCE_EXT = $90E1;
    GL_MAX_COMPUTE_FIXED_GROUP_INVOCATIONS_ARB = $90EB;
    GL_UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER = $90EC;
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER = $90ED;
    GL_COLOR_ATTACHMENT_EXT = $90F0;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
    GL_MULTIVIEW_EXT = $90F1;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
    GL_MAX_MULTIVIEW_BUFFERS_EXT = $90F2;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
    GL_CONTEXT_ROBUST_ACCESS = $90F3;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_CONTEXT_ROBUST_ACCESS_EXT = $90F3;   // api(gles1|gles2) //extension: GL_EXT_robustness
    GL_CONTEXT_ROBUST_ACCESS_KHR = $90F3;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
    GL_COMPUTE_PROGRAM_NV = $90FB;
    GL_COMPUTE_PROGRAM_PARAMETER_BUFFER_NV = $90FC;
    GL_PROXY_TEXTURE_2D_MULTISAMPLE = $9101;
    GL_TEXTURE_2D_MULTISAMPLE_ARRAY_OES = $9102;   // api(gles2) //extension: GL_OES_texture_storage_multisample_2d_array
    GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY = $9103;
    GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY_OES = $9105;   // api(gles2) //extension: GL_OES_texture_storage_multisample_2d_array
    GL_TEXTURE_SAMPLES = $9106;
    GL_TEXTURE_FIXED_SAMPLE_LOCATIONS = $9107;
    GL_SAMPLER_2D_MULTISAMPLE_ARRAY_OES = $910B;   // api(gles2) //extension: GL_OES_texture_storage_multisample_2d_array
    GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY_OES = $910C;   // api(gles2) //extension: GL_OES_texture_storage_multisample_2d_array
    GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY_OES = $910D;   // api(gles2) //extension: GL_OES_texture_storage_multisample_2d_array
    GL_MAX_SERVER_WAIT_TIMEOUT_APPLE = $9111;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_OBJECT_TYPE_APPLE = $9112;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_SYNC_CONDITION_APPLE = $9113;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_SYNC_STATUS_APPLE = $9114;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_SYNC_FLAGS_APPLE = $9115;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_SYNC_FENCE = $9116;
    GL_SYNC_FENCE_APPLE = $9116;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_SYNC_GPU_COMMANDS_COMPLETE_APPLE = $9117;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_UNSIGNALED = $9118;
    GL_UNSIGNALED_APPLE = $9118;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_SIGNALED = $9119;
    GL_SIGNALED_APPLE = $9119;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_ALREADY_SIGNALED_APPLE = $911A;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_TIMEOUT_EXPIRED_APPLE = $911B;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_CONDITION_SATISFIED_APPLE = $911C;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_WAIT_FAILED_APPLE = $911D;   // api(gles1|gles2) //extension: GL_APPLE_sync
    GL_MAX_GEOMETRY_INPUT_COMPONENTS_EXT = $9123;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_INPUT_COMPONENTS_OES = $9123;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_GEOMETRY_OUTPUT_COMPONENTS_EXT = $9124;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_OUTPUT_COMPONENTS_OES = $9124;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_CONTEXT_PROFILE_MASK = $9126;
    GL_UNPACK_COMPRESSED_BLOCK_WIDTH = $9127;
    GL_UNPACK_COMPRESSED_BLOCK_HEIGHT = $9128;
    GL_UNPACK_COMPRESSED_BLOCK_DEPTH = $9129;
    GL_UNPACK_COMPRESSED_BLOCK_SIZE = $912A;
    GL_PACK_COMPRESSED_BLOCK_WIDTH = $912B;
    GL_PACK_COMPRESSED_BLOCK_HEIGHT = $912C;
    GL_PACK_COMPRESSED_BLOCK_DEPTH = $912D;
    GL_PACK_COMPRESSED_BLOCK_SIZE = $912E;
    GL_TEXTURE_IMMUTABLE_FORMAT = $912F;
    GL_TEXTURE_IMMUTABLE_FORMAT_EXT = $912F;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
    GL_SGX_PROGRAM_BINARY_IMG = $9130;   // api(gles2) //extension: GL_IMG_program_binary
    GL_RENDERBUFFER_SAMPLES_IMG = $9133;   // api(gles1|gles2) //extension: GL_IMG_multisampled_render_to_texture
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_IMG = $9134;   // api(gles1|gles2) //extension: GL_IMG_multisampled_render_to_texture
    GL_MAX_SAMPLES_IMG = $9135;   // api(gles1|gles2) //extension: GL_IMG_multisampled_render_to_texture
    GL_TEXTURE_SAMPLES_IMG = $9136;   // api(gles1|gles2) //extension: GL_IMG_multisampled_render_to_texture
    GL_COMPRESSED_RGBA_PVRTC_2BPPV2_IMG = $9137;   // api(gles2) //extension: GL_IMG_texture_compression_pvrtc2
    GL_COMPRESSED_RGBA_PVRTC_4BPPV2_IMG = $9138;   // api(gles2) //extension: GL_IMG_texture_compression_pvrtc2
    GL_CUBIC_IMG = $9139;   // api(gles2) //extension: GL_IMG_texture_filter_cubic
    GL_CUBIC_MIPMAP_NEAREST_IMG = $913A;   // api(gles2) //extension: GL_IMG_texture_filter_cubic
    GL_CUBIC_MIPMAP_LINEAR_IMG = $913B;   // api(gles2) //extension: GL_IMG_texture_filter_cubic
    GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_AND_DOWNSAMPLE_IMG = $913C;   // api(gles2) //extension: GL_IMG_framebuffer_downsample
    GL_NUM_DOWNSAMPLE_SCALES_IMG = $913D;   // api(gles2) //extension: GL_IMG_framebuffer_downsample
    GL_DOWNSAMPLE_SCALES_IMG = $913E;   // api(gles2) //extension: GL_IMG_framebuffer_downsample
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_SCALE_IMG = $913F;   // api(gles2) //extension: GL_IMG_framebuffer_downsample
    GL_MAX_DEBUG_MESSAGE_LENGTH = $9143;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_DEBUG_MESSAGE_LENGTH_AMD = $9143;
    GL_MAX_DEBUG_MESSAGE_LENGTH_ARB = $9143;
    GL_MAX_DEBUG_MESSAGE_LENGTH_KHR = $9143;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_DEBUG_LOGGED_MESSAGES = $9144;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_MAX_DEBUG_LOGGED_MESSAGES_AMD = $9144;
    GL_MAX_DEBUG_LOGGED_MESSAGES_ARB = $9144;
    GL_MAX_DEBUG_LOGGED_MESSAGES_KHR = $9144;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_LOGGED_MESSAGES = $9145;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_LOGGED_MESSAGES_AMD = $9145;
    GL_DEBUG_LOGGED_MESSAGES_ARB = $9145;
    GL_DEBUG_LOGGED_MESSAGES_KHR = $9145;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_HIGH_AMD = $9146;
    GL_DEBUG_SEVERITY_HIGH_ARB = $9146;
    GL_DEBUG_SEVERITY_HIGH_KHR = $9146;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_MEDIUM_AMD = $9147;
    GL_DEBUG_SEVERITY_MEDIUM_ARB = $9147;
    GL_DEBUG_SEVERITY_MEDIUM_KHR = $9147;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_SEVERITY_LOW_AMD = $9148;
    GL_DEBUG_SEVERITY_LOW_ARB = $9148;
    GL_DEBUG_SEVERITY_LOW_KHR = $9148;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_DEBUG_CATEGORY_API_ERROR_AMD = $9149;
    GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD = $914A;
    GL_DEBUG_CATEGORY_DEPRECATION_AMD = $914B;
    GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD = $914C;
    GL_DEBUG_CATEGORY_PERFORMANCE_AMD = $914D;
    GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD = $914E;
    GL_DEBUG_CATEGORY_APPLICATION_AMD = $914F;
    GL_DEBUG_CATEGORY_OTHER_AMD = $9150;
    GL_BUFFER_OBJECT_EXT = $9151;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
    GL_DATA_BUFFER_AMD = $9151;
    GL_PERFORMANCE_MONITOR_AMD = $9152;
    GL_QUERY_OBJECT_AMD = $9153;
    GL_QUERY_OBJECT_EXT = $9153;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
    GL_VERTEX_ARRAY_OBJECT_AMD = $9154;
    GL_VERTEX_ARRAY_OBJECT_EXT = $9154;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
    GL_SAMPLER_OBJECT_AMD = $9155;
    GL_EXTERNAL_VIRTUAL_MEMORY_BUFFER_AMD = $9160;
    GL_QUERY_BUFFER = $9192;
    GL_QUERY_BUFFER_AMD = $9192;
    GL_QUERY_BUFFER_BINDING = $9193;
    GL_QUERY_BUFFER_BINDING_AMD = $9193;
    GL_QUERY_RESULT_NO_WAIT = $9194;
    GL_QUERY_RESULT_NO_WAIT_AMD = $9194;
    GL_VIRTUAL_PAGE_SIZE_X_ARB = $9195;
    GL_VIRTUAL_PAGE_SIZE_X_EXT = $9195;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_VIRTUAL_PAGE_SIZE_X_AMD = $9195;
    GL_VIRTUAL_PAGE_SIZE_Y_ARB = $9196;
    GL_VIRTUAL_PAGE_SIZE_Y_EXT = $9196;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_VIRTUAL_PAGE_SIZE_Y_AMD = $9196;
    GL_VIRTUAL_PAGE_SIZE_Z_ARB = $9197;
    GL_VIRTUAL_PAGE_SIZE_Z_EXT = $9197;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_VIRTUAL_PAGE_SIZE_Z_AMD = $9197;
    GL_MAX_SPARSE_TEXTURE_SIZE_ARB = $9198;
    GL_MAX_SPARSE_TEXTURE_SIZE_EXT = $9198;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_MAX_SPARSE_TEXTURE_SIZE_AMD = $9198;
    GL_MAX_SPARSE_3D_TEXTURE_SIZE_ARB = $9199;
    GL_MAX_SPARSE_3D_TEXTURE_SIZE_EXT = $9199;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_MAX_SPARSE_3D_TEXTURE_SIZE_AMD = $9199;
    GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS = $919A;
    GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS_ARB = $919A;
    GL_MAX_SPARSE_ARRAY_TEXTURE_LAYERS_EXT = $919A;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_MIN_SPARSE_LEVEL_AMD = $919B;
    GL_MIN_LOD_WARNING_AMD = $919C;
    GL_TEXTURE_BUFFER_OFFSET = $919D;
    GL_TEXTURE_BUFFER_OFFSET_EXT = $919D;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BUFFER_OFFSET_OES = $919D;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_TEXTURE_BUFFER_SIZE = $919E;
    GL_TEXTURE_BUFFER_SIZE_EXT = $919E;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BUFFER_SIZE_OES = $919E;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT_EXT = $919F;   // api(gles2) //extension: GL_EXT_texture_buffer
    GL_TEXTURE_BUFFER_OFFSET_ALIGNMENT_OES = $919F;   // api(gles2) //extension: GL_OES_texture_buffer
    GL_STREAM_RASTERIZATION_AMD = $91A0;
    GL_VERTEX_ELEMENT_SWIZZLE_AMD = $91A4;
    GL_VERTEX_ID_SWIZZLE_AMD = $91A5;
    GL_TEXTURE_SPARSE_ARB = $91A6;
    GL_TEXTURE_SPARSE_EXT = $91A6;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_VIRTUAL_PAGE_SIZE_INDEX_ARB = $91A7;
    GL_VIRTUAL_PAGE_SIZE_INDEX_EXT = $91A7;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_NUM_VIRTUAL_PAGE_SIZES_ARB = $91A8;
    GL_NUM_VIRTUAL_PAGE_SIZES_EXT = $91A8;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_SPARSE_TEXTURE_FULL_ARRAY_CUBE_MIPMAPS_ARB = $91A9;
    GL_SPARSE_TEXTURE_FULL_ARRAY_CUBE_MIPMAPS_EXT = $91A9;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_NUM_SPARSE_LEVELS_ARB = $91AA;
    GL_NUM_SPARSE_LEVELS_EXT = $91AA;   // api(gles2) //extension: GL_EXT_sparse_texture
    GL_PIXELS_PER_SAMPLE_PATTERN_X_AMD = $91AE;
    GL_PIXELS_PER_SAMPLE_PATTERN_Y_AMD = $91AF;
    GL_MAX_SHADER_COMPILER_THREADS_KHR = $91B0;   // api(gl|glcore|gles2) //extension: GL_KHR_parallel_shader_compile
    GL_MAX_SHADER_COMPILER_THREADS_ARB = $91B0;
    GL_COMPLETION_STATUS_KHR = $91B1;   // api(gl|glcore|gles2) //extension: GL_KHR_parallel_shader_compile
    GL_COMPLETION_STATUS_ARB = $91B1;
    GL_RENDERBUFFER_STORAGE_SAMPLES_AMD = $91B2;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
    GL_MAX_COLOR_FRAMEBUFFER_SAMPLES_AMD = $91B3;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
    GL_MAX_COLOR_FRAMEBUFFER_STORAGE_SAMPLES_AMD = $91B4;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
    GL_MAX_DEPTH_STENCIL_FRAMEBUFFER_SAMPLES_AMD = $91B5;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
    GL_NUM_SUPPORTED_MULTISAMPLE_MODES_AMD = $91B6;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
    GL_SUPPORTED_MULTISAMPLE_MODES_AMD = $91B7;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
    GL_MAX_COMPUTE_IMAGE_UNIFORMS = $91BD;
    GL_MAX_COMPUTE_FIXED_GROUP_SIZE_ARB = $91BF;
    GL_FLOAT16_MAT2_AMD = $91C5;
    GL_FLOAT16_MAT3_AMD = $91C6;
    GL_FLOAT16_MAT4_AMD = $91C7;
    GL_FLOAT16_MAT2x3_AMD = $91C8;
    GL_FLOAT16_MAT2x4_AMD = $91C9;
    GL_FLOAT16_MAT3x2_AMD = $91CA;
    GL_FLOAT16_MAT3x4_AMD = $91CB;
    GL_FLOAT16_MAT4x2_AMD = $91CC;
    GL_FLOAT16_MAT4x3_AMD = $91CD;
    GL_UNPACK_FLIP_Y_WEBGL = $9240;
    GL_UNPACK_PREMULTIPLY_ALPHA_WEBGL = $9241;
    GL_CONTEXT_LOST_WEBGL = $9242;
    GL_UNPACK_COLORSPACE_CONVERSION_WEBGL = $9243;
    GL_BROWSER_DEFAULT_WEBGL = $9244;
    GL_SHADER_BINARY_DMP = $9250;   // api(gles2) //extension: GL_DMP_shader_binary
    GL_SMAPHS30_PROGRAM_BINARY_DMP = $9251;   // api(gles2) //extension: GL_DMP_program_binary
    GL_SMAPHS_PROGRAM_BINARY_DMP = $9252;   // api(gles2) //extension: GL_DMP_program_binary
    GL_DMP_PROGRAM_BINARY_DMP = $9253;   // api(gles2) //extension: GL_DMP_program_binary
    GL_GCCSO_SHADER_BINARY_FJ = $9260;   // api(gles2) //extension: GL_FJ_shader_binary_GCCSO
    GL_COMPRESSED_R11_EAC_OES = $9270;
    GL_COMPRESSED_SIGNED_R11_EAC_OES = $9271;
    GL_COMPRESSED_RG11_EAC_OES = $9272;
    GL_COMPRESSED_SIGNED_RG11_EAC_OES = $9273;
    GL_COMPRESSED_RGB8_ETC2_OES = $9274;
    GL_COMPRESSED_SRGB8_ETC2_OES = $9275;
    GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2_OES = $9276;
    GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2_OES = $9277;
    GL_COMPRESSED_RGBA8_ETC2_EAC_OES = $9278;
    GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC_OES = $9279;
    GL_BLEND_PREMULTIPLIED_SRC_NV = $9280;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_BLEND_OVERLAP_NV = $9281;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_UNCORRELATED_NV = $9282;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DISJOINT_NV = $9283;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_CONJOINT_NV = $9284;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_BLEND_ADVANCED_COHERENT_KHR = $9285;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced_coherent
    GL_BLEND_ADVANCED_COHERENT_NV = $9285;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced_coherent
    GL_SRC_NV = $9286;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DST_NV = $9287;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_SRC_OVER_NV = $9288;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DST_OVER_NV = $9289;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_SRC_IN_NV = $928A;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DST_IN_NV = $928B;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_SRC_OUT_NV = $928C;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DST_OUT_NV = $928D;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_SRC_ATOP_NV = $928E;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DST_ATOP_NV = $928F;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_PLUS_NV = $9291;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_PLUS_DARKER_NV = $9292;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_MULTIPLY = $9294;
    GL_MULTIPLY_KHR = $9294;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_MULTIPLY_NV = $9294;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_SCREEN = $9295;
    GL_SCREEN_KHR = $9295;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_SCREEN_NV = $9295;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_OVERLAY = $9296;
    GL_OVERLAY_KHR = $9296;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_OVERLAY_NV = $9296;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DARKEN = $9297;
    GL_DARKEN_KHR = $9297;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_DARKEN_NV = $9297;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_LIGHTEN = $9298;
    GL_LIGHTEN_KHR = $9298;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_LIGHTEN_NV = $9298;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_COLORDODGE = $9299;
    GL_COLORDODGE_KHR = $9299;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_COLORDODGE_NV = $9299;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_COLORBURN = $929A;
    GL_COLORBURN_KHR = $929A;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_COLORBURN_NV = $929A;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_HARDLIGHT = $929B;
    GL_HARDLIGHT_KHR = $929B;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_HARDLIGHT_NV = $929B;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_SOFTLIGHT = $929C;
    GL_SOFTLIGHT_KHR = $929C;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_SOFTLIGHT_NV = $929C;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_DIFFERENCE = $929E;
    GL_DIFFERENCE_KHR = $929E;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_DIFFERENCE_NV = $929E;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_MINUS_NV = $929F;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_EXCLUSION = $92A0;
    GL_EXCLUSION_KHR = $92A0;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_EXCLUSION_NV = $92A0;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_CONTRAST_NV = $92A1;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_INVERT_RGB_NV = $92A3;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_LINEARDODGE_NV = $92A4;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_LINEARBURN_NV = $92A5;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_VIVIDLIGHT_NV = $92A6;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_LINEARLIGHT_NV = $92A7;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_PINLIGHT_NV = $92A8;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_HARDMIX_NV = $92A9;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_HSL_HUE = $92AD;
    GL_HSL_HUE_KHR = $92AD;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_HSL_HUE_NV = $92AD;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_HSL_SATURATION = $92AE;
    GL_HSL_SATURATION_KHR = $92AE;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_HSL_SATURATION_NV = $92AE;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_HSL_COLOR = $92AF;
    GL_HSL_COLOR_KHR = $92AF;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_HSL_COLOR_NV = $92AF;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_HSL_LUMINOSITY = $92B0;
    GL_HSL_LUMINOSITY_KHR = $92B0;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
    GL_HSL_LUMINOSITY_NV = $92B0;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_PLUS_CLAMPED_NV = $92B1;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_PLUS_CLAMPED_ALPHA_NV = $92B2;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_MINUS_CLAMPED_NV = $92B3;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_INVERT_OVG_NV = $92B4;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
    GL_MAX_LGPU_GPUS_NVX = $92BA;
    GL_MULTICAST_GPUS_NV = $92BA;
    GL_PURGED_CONTEXT_RESET_NV = $92BB;
    GL_PRIMITIVE_BOUNDING_BOX_ARB = $92BE;
    GL_PRIMITIVE_BOUNDING_BOX = $92BE;
    GL_PRIMITIVE_BOUNDING_BOX_EXT = $92BE;   // api(gles2) //extension: GL_EXT_primitive_bounding_box
    GL_PRIMITIVE_BOUNDING_BOX_OES = $92BE;   // api(gles2) //extension: GL_OES_primitive_bounding_box
    GL_ALPHA_TO_COVERAGE_DITHER_MODE_NV = $92BF;
    GL_ATOMIC_COUNTER_BUFFER_START = $92C2;
    GL_ATOMIC_COUNTER_BUFFER_SIZE = $92C3;
    GL_ATOMIC_COUNTER_BUFFER_DATA_SIZE = $92C4;
    GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTERS = $92C5;
    GL_ATOMIC_COUNTER_BUFFER_ACTIVE_ATOMIC_COUNTER_INDICES = $92C6;
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_VERTEX_SHADER = $92C7;
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_CONTROL_SHADER = $92C8;
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TESS_EVALUATION_SHADER = $92C9;
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_GEOMETRY_SHADER = $92CA;
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_FRAGMENT_SHADER = $92CB;
    GL_MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = $92CC;
    GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = $92CD;
    GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS_EXT = $92CD;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS_OES = $92CD;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = $92CE;
    GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS_EXT = $92CE;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS_OES = $92CE;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = $92CF;
    GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS_EXT = $92CF;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS_OES = $92CF;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = $92D0;
    GL_MAX_COMBINED_ATOMIC_COUNTER_BUFFERS = $92D1;
    GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS_EXT = $92D3;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS_OES = $92D3;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS_EXT = $92D4;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS_OES = $92D4;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_MAX_GEOMETRY_ATOMIC_COUNTERS_EXT = $92D5;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_GEOMETRY_ATOMIC_COUNTERS_OES = $92D5;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_ATOMIC_COUNTER_BUFFER_SIZE = $92D8;
    GL_UNIFORM_ATOMIC_COUNTER_BUFFER_INDEX = $92DA;
    GL_MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = $92DC;
    GL_FRAGMENT_COVERAGE_TO_COLOR_NV = $92DD;   // api(gl|glcore|gles2) //extension: GL_NV_fragment_coverage_to_color
    GL_FRAGMENT_COVERAGE_COLOR_NV = $92DE;   // api(gl|glcore|gles2) //extension: GL_NV_fragment_coverage_to_color
    GL_MESH_OUTPUT_PER_VERTEX_GRANULARITY_NV = $92DF;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_DEBUG_OUTPUT_KHR = $92E0;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
    GL_IS_PER_PATCH_EXT = $92E7;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_IS_PER_PATCH_OES = $92E7;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_VERTEX_SUBROUTINE = $92E8;
    GL_TESS_CONTROL_SUBROUTINE = $92E9;
    GL_TESS_EVALUATION_SUBROUTINE = $92EA;
    GL_GEOMETRY_SUBROUTINE = $92EB;
    GL_FRAGMENT_SUBROUTINE = $92EC;
    GL_COMPUTE_SUBROUTINE = $92ED;
    GL_VERTEX_SUBROUTINE_UNIFORM = $92EE;
    GL_TESS_CONTROL_SUBROUTINE_UNIFORM = $92EF;
    GL_TESS_EVALUATION_SUBROUTINE_UNIFORM = $92F0;
    GL_GEOMETRY_SUBROUTINE_UNIFORM = $92F1;
    GL_FRAGMENT_SUBROUTINE_UNIFORM = $92F2;
    GL_COMPUTE_SUBROUTINE_UNIFORM = $92F3;
    GL_MAX_NUM_COMPATIBLE_SUBROUTINES = $92F8;
    GL_REFERENCED_BY_TESS_CONTROL_SHADER_EXT = $9307;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_REFERENCED_BY_TESS_CONTROL_SHADER_OES = $9307;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_REFERENCED_BY_TESS_EVALUATION_SHADER_EXT = $9308;   // api(gles2) //extension: GL_EXT_tessellation_shader
    GL_REFERENCED_BY_TESS_EVALUATION_SHADER_OES = $9308;   // api(gles2) //extension: GL_OES_tessellation_shader
    GL_REFERENCED_BY_GEOMETRY_SHADER_EXT = $9309;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_REFERENCED_BY_GEOMETRY_SHADER_OES = $9309;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_LOCATION_INDEX = $930F;
    GL_LOCATION_INDEX_EXT = $930F;   // api(gles2) //extension: GL_EXT_blend_func_extended
    GL_FRAMEBUFFER_DEFAULT_LAYERS_EXT = $9312;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_FRAMEBUFFER_DEFAULT_LAYERS_OES = $9312;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_MAX_FRAMEBUFFER_LAYERS_EXT = $9317;   // api(gles2) //extension: GL_EXT_geometry_shader
    GL_MAX_FRAMEBUFFER_LAYERS_OES = $9317;   // api(gles2) //extension: GL_OES_geometry_shader
    GL_RASTER_MULTISAMPLE_EXT = $9327;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
    GL_RASTER_SAMPLES_EXT = $9328;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
    GL_MAX_RASTER_SAMPLES_EXT = $9329;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
    GL_RASTER_FIXED_SAMPLE_LOCATIONS_EXT = $932A;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
    GL_MULTISAMPLE_RASTERIZATION_ALLOWED_EXT = $932B;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
    GL_EFFECTIVE_RASTER_SAMPLES_EXT = $932C;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
    GL_DEPTH_SAMPLES_NV = $932D;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_STENCIL_SAMPLES_NV = $932E;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_MIXED_DEPTH_SAMPLES_SUPPORTED_NV = $932F;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_MIXED_STENCIL_SAMPLES_SUPPORTED_NV = $9330;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_COVERAGE_MODULATION_TABLE_NV = $9331;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_COVERAGE_MODULATION_NV = $9332;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_COVERAGE_MODULATION_TABLE_SIZE_NV = $9333;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
    GL_WARP_SIZE_NV = $9339;
    GL_WARPS_PER_SM_NV = $933A;
    GL_SM_COUNT_NV = $933B;
    GL_FILL_RECTANGLE_NV = $933C;   // api(gl|glcore|gles2) //extension: GL_NV_fill_rectangle
    GL_SAMPLE_LOCATION_SUBPIXEL_BITS_ARB = $933D;
    GL_SAMPLE_LOCATION_SUBPIXEL_BITS_NV = $933D;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_SAMPLE_LOCATION_PIXEL_GRID_WIDTH_ARB = $933E;
    GL_SAMPLE_LOCATION_PIXEL_GRID_WIDTH_NV = $933E;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_SAMPLE_LOCATION_PIXEL_GRID_HEIGHT_ARB = $933F;
    GL_SAMPLE_LOCATION_PIXEL_GRID_HEIGHT_NV = $933F;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_PROGRAMMABLE_SAMPLE_LOCATION_TABLE_SIZE_ARB = $9340;
    GL_PROGRAMMABLE_SAMPLE_LOCATION_TABLE_SIZE_NV = $9340;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_PROGRAMMABLE_SAMPLE_LOCATION_ARB = $9341;
    GL_PROGRAMMABLE_SAMPLE_LOCATION_NV = $9341;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_FRAMEBUFFER_PROGRAMMABLE_SAMPLE_LOCATIONS_ARB = $9342;
    GL_FRAMEBUFFER_PROGRAMMABLE_SAMPLE_LOCATIONS_NV = $9342;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_FRAMEBUFFER_SAMPLE_LOCATION_PIXEL_GRID_ARB = $9343;
    GL_FRAMEBUFFER_SAMPLE_LOCATION_PIXEL_GRID_NV = $9343;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
    GL_MAX_COMPUTE_VARIABLE_GROUP_INVOCATIONS_ARB = $9344;
    GL_MAX_COMPUTE_VARIABLE_GROUP_SIZE_ARB = $9345;
    GL_CONSERVATIVE_RASTERIZATION_NV = $9346;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster
    GL_SUBPIXEL_PRECISION_BIAS_X_BITS_NV = $9347;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster
    GL_SUBPIXEL_PRECISION_BIAS_Y_BITS_NV = $9348;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster
    GL_MAX_SUBPIXEL_PRECISION_BIAS_BITS_NV = $9349;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster
    GL_LOCATION_COMPONENT = $934A;
    GL_TRANSFORM_FEEDBACK_BUFFER_INDEX = $934B;
    GL_TRANSFORM_FEEDBACK_BUFFER_STRIDE = $934C;
    GL_ALPHA_TO_COVERAGE_DITHER_DEFAULT_NV = $934D;
    GL_ALPHA_TO_COVERAGE_DITHER_ENABLE_NV = $934E;
    GL_ALPHA_TO_COVERAGE_DITHER_DISABLE_NV = $934F;
    GL_VIEWPORT_SWIZZLE_POSITIVE_X_NV = $9350;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_NEGATIVE_X_NV = $9351;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_POSITIVE_Y_NV = $9352;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_NEGATIVE_Y_NV = $9353;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_POSITIVE_Z_NV = $9354;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_NEGATIVE_Z_NV = $9355;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_POSITIVE_W_NV = $9356;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_NEGATIVE_W_NV = $9357;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_X_NV = $9358;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_Y_NV = $9359;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_Z_NV = $935A;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_VIEWPORT_SWIZZLE_W_NV = $935B;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
    GL_CLIP_ORIGIN = $935C;
    GL_CLIP_ORIGIN_EXT = $935C;   // api(gles2) //extension: GL_EXT_clip_control
    GL_CLIP_DEPTH_MODE = $935D;
    GL_CLIP_DEPTH_MODE_EXT = $935D;   // api(gles2) //extension: GL_EXT_clip_control
    GL_NEGATIVE_ONE_TO_ONE = $935E;
    GL_NEGATIVE_ONE_TO_ONE_EXT = $935E;   // api(gles2) //extension: GL_EXT_clip_control
    GL_ZERO_TO_ONE = $935F;
    GL_ZERO_TO_ONE_EXT = $935F;   // api(gles2) //extension: GL_EXT_clip_control
    GL_CLEAR_TEXTURE = $9365;
    GL_TEXTURE_REDUCTION_MODE_ARB = $9366;
    GL_TEXTURE_REDUCTION_MODE_EXT = $9366;   // api(gl|glcore|gles2) //extension: GL_EXT_texture_filter_minmax
    GL_WEIGHTED_AVERAGE_ARB = $9367;
    GL_WEIGHTED_AVERAGE_EXT = $9367;   // api(gl|glcore|gles2) //extension: GL_EXT_texture_filter_minmax
    GL_FONT_GLYPHS_AVAILABLE_NV = $9368;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_TARGET_UNAVAILABLE_NV = $9369;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_UNAVAILABLE_NV = $936A;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FONT_UNINTELLIGIBLE_NV = $936B;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_STANDARD_FONT_FORMAT_NV = $936C;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_FRAGMENT_INPUT_NV = $936D;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
    GL_UNIFORM_BUFFER_UNIFIED_NV = $936E;
    GL_UNIFORM_BUFFER_ADDRESS_NV = $936F;
    GL_UNIFORM_BUFFER_LENGTH_NV = $9370;
    GL_MULTISAMPLES_NV = $9371;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
    GL_SUPERSAMPLE_SCALE_X_NV = $9372;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
    GL_SUPERSAMPLE_SCALE_Y_NV = $9373;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
    GL_CONFORMANT_NV = $9374;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
    GL_CONSERVATIVE_RASTER_DILATE_NV = $9379;
    GL_CONSERVATIVE_RASTER_DILATE_RANGE_NV = $937A;
    GL_CONSERVATIVE_RASTER_DILATE_GRANULARITY_NV = $937B;
    GL_VIEWPORT_POSITION_W_SCALE_NV = $937C;   // api(gl|glcore|gles2) //extension: GL_NV_clip_space_w_scaling
    GL_VIEWPORT_POSITION_W_SCALE_X_COEFF_NV = $937D;   // api(gl|glcore|gles2) //extension: GL_NV_clip_space_w_scaling
    GL_VIEWPORT_POSITION_W_SCALE_Y_COEFF_NV = $937E;   // api(gl|glcore|gles2) //extension: GL_NV_clip_space_w_scaling
    GL_REPRESENTATIVE_FRAGMENT_TEST_NV = $937F;   // api(gl|glcore|gles2) //extension: GL_NV_representative_fragment_test
    GL_MULTISAMPLE_LINE_WIDTH_RANGE_ARB = $9381;
    GL_MULTISAMPLE_LINE_WIDTH_RANGE = $9381;
    GL_MULTISAMPLE_LINE_WIDTH_GRANULARITY_ARB = $9382;
    GL_MULTISAMPLE_LINE_WIDTH_GRANULARITY = $9382;
    GL_VIEW_CLASS_EAC_R11 = $9383;
    GL_VIEW_CLASS_EAC_RG11 = $9384;
    GL_VIEW_CLASS_ETC2_RGB = $9385;
    GL_VIEW_CLASS_ETC2_RGBA = $9386;
    GL_VIEW_CLASS_ETC2_EAC_RGBA = $9387;
    GL_VIEW_CLASS_ASTC_4x4_RGBA = $9388;
    GL_VIEW_CLASS_ASTC_5x4_RGBA = $9389;
    GL_VIEW_CLASS_ASTC_5x5_RGBA = $938A;
    GL_VIEW_CLASS_ASTC_6x5_RGBA = $938B;
    GL_VIEW_CLASS_ASTC_6x6_RGBA = $938C;
    GL_VIEW_CLASS_ASTC_8x5_RGBA = $938D;
    GL_VIEW_CLASS_ASTC_8x6_RGBA = $938E;
    GL_VIEW_CLASS_ASTC_8x8_RGBA = $938F;
    GL_VIEW_CLASS_ASTC_10x5_RGBA = $9390;
    GL_VIEW_CLASS_ASTC_10x6_RGBA = $9391;
    GL_VIEW_CLASS_ASTC_10x8_RGBA = $9392;
    GL_VIEW_CLASS_ASTC_10x10_RGBA = $9393;
    GL_VIEW_CLASS_ASTC_12x10_RGBA = $9394;
    GL_VIEW_CLASS_ASTC_12x12_RGBA = $9395;
    GL_TRANSLATED_SHADER_SOURCE_LENGTH_ANGLE = $93A0;   // api(gles2) //extension: GL_ANGLE_translated_shader_source
    GL_BGRA8_EXT = $93A1;   // api(gles1|gles2) //extension: GL_APPLE_texture_format_BGRA8888
    GL_TEXTURE_USAGE_ANGLE = $93A2;   // api(gles2) //extension: GL_ANGLE_texture_usage
    GL_FRAMEBUFFER_ATTACHMENT_ANGLE = $93A3;   // api(gles2) //extension: GL_ANGLE_texture_usage
    GL_PACK_REVERSE_ROW_ORDER_ANGLE = $93A4;   // api(gles2) //extension: GL_ANGLE_pack_reverse_row_order
    GL_PROGRAM_BINARY_ANGLE = $93A6;   // api(gles2) //extension: GL_ANGLE_program_binary
    GL_COMPRESSED_RGBA_ASTC_4x4_KHR = $93B0;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_5x4_KHR = $93B1;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_5x5_KHR = $93B2;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_6x5_KHR = $93B3;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_6x6_KHR = $93B4;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_8x5_KHR = $93B5;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_8x6_KHR = $93B6;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_8x8_KHR = $93B7;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_10x5_KHR = $93B8;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_10x6_KHR = $93B9;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_10x8_KHR = $93BA;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_10x10_KHR = $93BB;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_12x10_KHR = $93BC;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_12x12_KHR = $93BD;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_RGBA_ASTC_3x3x3_OES = $93C0;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_4x3x3_OES = $93C1;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_4x4x3_OES = $93C2;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_4x4x4_OES = $93C3;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_5x4x4_OES = $93C4;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_5x5x4_OES = $93C5;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_5x5x5_OES = $93C6;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_6x5x5_OES = $93C7;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_6x6x5_OES = $93C8;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_RGBA_ASTC_6x6x6_OES = $93C9;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR = $93D0;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR = $93D1;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR = $93D2;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR = $93D3;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR = $93D4;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR = $93D5;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR = $93D6;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR = $93D7;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR = $93D8;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR = $93D9;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR = $93DA;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR = $93DB;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR = $93DC;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR = $93DD;   // api(gl|glcore|gles2) //extension: GL_KHR_texture_compression_astc_hdr
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_3x3x3_OES = $93E0;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x3x3_OES = $93E1;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4x3_OES = $93E2;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4x4_OES = $93E3;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4x4_OES = $93E4;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5x4_OES = $93E5;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5x5_OES = $93E6;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5x5_OES = $93E7;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6x5_OES = $93E8;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6x6_OES = $93E9;   // api(gles2) //extension: GL_OES_texture_compression_astc
    GL_COMPRESSED_SRGB_ALPHA_PVRTC_2BPPV2_IMG = $93F0;   // api(gles2) //extension: GL_EXT_pvrtc_sRGB
    GL_COMPRESSED_SRGB_ALPHA_PVRTC_4BPPV2_IMG = $93F1;   // api(gles2) //extension: GL_EXT_pvrtc_sRGB
    GL_PERFQUERY_COUNTER_EVENT_INTEL = $94F0;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DURATION_NORM_INTEL = $94F1;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DURATION_RAW_INTEL = $94F2;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_THROUGHPUT_INTEL = $94F3;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_RAW_INTEL = $94F4;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_TIMESTAMP_INTEL = $94F5;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DATA_UINT32_INTEL = $94F8;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DATA_UINT64_INTEL = $94F9;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DATA_FLOAT_INTEL = $94FA;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DATA_DOUBLE_INTEL = $94FB;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DATA_BOOL32_INTEL = $94FC;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_QUERY_NAME_LENGTH_MAX_INTEL = $94FD;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_NAME_LENGTH_MAX_INTEL = $94FE;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_COUNTER_DESC_LENGTH_MAX_INTEL = $94FF;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_PERFQUERY_GPA_EXTENDED_COUNTERS_INTEL = $9500;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
    GL_LAYOUT_DEPTH_READ_ONLY_STENCIL_ATTACHMENT_EXT = $9530;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_DEPTH_ATTACHMENT_STENCIL_READ_ONLY_EXT = $9531;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_SUBGROUP_SIZE_KHR = $9532;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_SUPPORTED_STAGES_KHR = $9533;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_SUPPORTED_FEATURES_KHR = $9534;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_SUBGROUP_QUAD_ALL_STAGES_KHR = $9535;   // api(gl|glcore|gles2) //extension: GL_KHR_shader_subgroup
    GL_MAX_MESH_TOTAL_MEMORY_SIZE_NV = $9536;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_TOTAL_MEMORY_SIZE_NV = $9537;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_OUTPUT_VERTICES_NV = $9538;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_OUTPUT_PRIMITIVES_NV = $9539;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_OUTPUT_COUNT_NV = $953A;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_WORK_GROUP_SIZE_NV = $953B;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_WORK_GROUP_SIZE_NV = $953C;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_DRAW_MESH_TASKS_COUNT_NV = $953D;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MESH_WORK_GROUP_SIZE_NV = $953E;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_TASK_WORK_GROUP_SIZE_NV = $953F;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_QUERY_RESOURCE_TYPE_VIDMEM_ALLOC_NV = $9540;
    GL_QUERY_RESOURCE_MEMTYPE_VIDMEM_NV = $9542;
    GL_MESH_OUTPUT_PER_PRIMITIVE_GRANULARITY_NV = $9543;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_QUERY_RESOURCE_SYS_RESERVED_NV = $9544;
    GL_QUERY_RESOURCE_TEXTURE_NV = $9545;
    GL_QUERY_RESOURCE_RENDERBUFFER_NV = $9546;
    GL_QUERY_RESOURCE_BUFFEROBJECT_NV = $9547;
    GL_PER_GPU_STORAGE_NV = $9548;
    GL_MULTICAST_PROGRAMMABLE_SAMPLE_LOCATION_NV = $9549;
    GL_UPLOAD_GPU_MASK_NVX = $954A;
    GL_CONSERVATIVE_RASTER_MODE_NV = $954D;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster_pre_snap_triangles
    GL_CONSERVATIVE_RASTER_MODE_POST_SNAP_NV = $954E;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster_pre_snap_triangles
    GL_CONSERVATIVE_RASTER_MODE_PRE_SNAP_TRIANGLES_NV = $954F;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster_pre_snap_triangles
    GL_CONSERVATIVE_RASTER_MODE_PRE_SNAP_NV = $9550;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster_pre_snap
    GL_SHADER_BINARY_FORMAT_SPIR_V = $9551;
    GL_SHADER_BINARY_FORMAT_SPIR_V_ARB = $9551;
    GL_SPIR_V_BINARY = $9552;
    GL_SPIR_V_BINARY_ARB = $9552;
    GL_SPIR_V_EXTENSIONS = $9553;
    GL_NUM_SPIR_V_EXTENSIONS = $9554;
    GL_SCISSOR_TEST_EXCLUSIVE_NV = $9555;   // api(gl|glcore|gles2) //extension: GL_NV_scissor_exclusive
    GL_SCISSOR_BOX_EXCLUSIVE_NV = $9556;   // api(gl|glcore|gles2) //extension: GL_NV_scissor_exclusive
    GL_MAX_MESH_VIEWS_NV = $9557;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_RENDER_GPU_MASK_NV = $9558;
    GL_MESH_SHADER_NV = $9559;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_TASK_SHADER_NV = $955A;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_SHADING_RATE_IMAGE_BINDING_NV = $955B;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_IMAGE_TEXEL_WIDTH_NV = $955C;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_IMAGE_TEXEL_HEIGHT_NV = $955D;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_IMAGE_PALETTE_SIZE_NV = $955E;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_MAX_COARSE_FRAGMENT_SAMPLES_NV = $955F;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_IMAGE_NV = $9563;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_NO_INVOCATIONS_NV = $9564;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_PIXEL_NV = $9565;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_1X2_PIXELS_NV = $9566;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_2X1_PIXELS_NV = $9567;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_2X2_PIXELS_NV = $9568;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_2X4_PIXELS_NV = $9569;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_4X2_PIXELS_NV = $956A;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_1_INVOCATION_PER_4X4_PIXELS_NV = $956B;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_2_INVOCATIONS_PER_PIXEL_NV = $956C;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_4_INVOCATIONS_PER_PIXEL_NV = $956D;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_8_INVOCATIONS_PER_PIXEL_NV = $956E;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_16_INVOCATIONS_PER_PIXEL_NV = $956F;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_MESH_VERTICES_OUT_NV = $9579;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MESH_PRIMITIVES_OUT_NV = $957A;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MESH_OUTPUT_TYPE_NV = $957B;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MESH_SUBROUTINE_NV = $957C;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_TASK_SUBROUTINE_NV = $957D;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MESH_SUBROUTINE_UNIFORM_NV = $957E;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_TASK_SUBROUTINE_UNIFORM_NV = $957F;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_TEXTURE_TILING_EXT = $9580;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_DEDICATED_MEMORY_OBJECT_EXT = $9581;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_NUM_TILING_TYPES_EXT = $9582;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_TILING_TYPES_EXT = $9583;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_OPTIMAL_TILING_EXT = $9584;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_LINEAR_TILING_EXT = $9585;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_HANDLE_TYPE_OPAQUE_FD_EXT = $9586;   // api(gl|gles2) //extension: GL_EXT_memory_object_fd
    GL_HANDLE_TYPE_OPAQUE_WIN32_EXT = $9587;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_HANDLE_TYPE_OPAQUE_WIN32_KMT_EXT = $9588;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_HANDLE_TYPE_D3D12_TILEPOOL_EXT = $9589;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_HANDLE_TYPE_D3D12_RESOURCE_EXT = $958A;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_HANDLE_TYPE_D3D11_IMAGE_EXT = $958B;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_HANDLE_TYPE_D3D11_IMAGE_KMT_EXT = $958C;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_LAYOUT_GENERAL_EXT = $958D;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_COLOR_ATTACHMENT_EXT = $958E;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_DEPTH_STENCIL_ATTACHMENT_EXT = $958F;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_DEPTH_STENCIL_READ_ONLY_EXT = $9590;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_SHADER_READ_ONLY_EXT = $9591;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_TRANSFER_SRC_EXT = $9592;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_LAYOUT_TRANSFER_DST_EXT = $9593;   // api(gl|gles2) //extension: GL_EXT_semaphore
    GL_HANDLE_TYPE_D3D12_FENCE_EXT = $9594;   // api(gl|gles2) //extension: GL_EXT_semaphore_win32
    GL_D3D12_FENCE_VALUE_EXT = $9595;   // api(gl|gles2) //extension: GL_EXT_semaphore_win32
    GL_TIMELINE_SEMAPHORE_VALUE_NV = $9595;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
    GL_NUM_DEVICE_UUIDS_EXT = $9596;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_DEVICE_UUID_EXT = $9597;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_DRIVER_UUID_EXT = $9598;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_DEVICE_LUID_EXT = $9599;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_DEVICE_NODE_MASK_EXT = $959A;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
    GL_PROTECTED_MEMORY_OBJECT_EXT = $959B;   // api(gl|gles2) //extension: GL_EXT_memory_object
    GL_UNIFORM_BLOCK_REFERENCED_BY_MESH_SHADER_NV = $959C;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_UNIFORM_BLOCK_REFERENCED_BY_TASK_SHADER_NV = $959D;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_MESH_SHADER_NV = $959E;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_ATOMIC_COUNTER_BUFFER_REFERENCED_BY_TASK_SHADER_NV = $959F;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_REFERENCED_BY_MESH_SHADER_NV = $95A0;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_REFERENCED_BY_TASK_SHADER_NV = $95A1;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_MESH_WORK_GROUP_INVOCATIONS_NV = $95A2;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_MAX_TASK_WORK_GROUP_INVOCATIONS_NV = $95A3;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
    GL_ATTACHED_MEMORY_OBJECT_NV = $95A4;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_ATTACHED_MEMORY_OFFSET_NV = $95A5;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_MEMORY_ATTACHABLE_ALIGNMENT_NV = $95A6;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_MEMORY_ATTACHABLE_SIZE_NV = $95A7;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_MEMORY_ATTACHABLE_NV = $95A8;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_DETACHED_MEMORY_INCARNATION_NV = $95A9;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_DETACHED_TEXTURES_NV = $95AA;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_DETACHED_BUFFERS_NV = $95AB;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_MAX_DETACHED_TEXTURES_NV = $95AC;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_MAX_DETACHED_BUFFERS_NV = $95AD;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
    GL_SHADING_RATE_SAMPLE_ORDER_DEFAULT_NV = $95AE;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_SAMPLE_ORDER_PIXEL_MAJOR_NV = $95AF;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SHADING_RATE_SAMPLE_ORDER_SAMPLE_MAJOR_NV = $95B0;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
    GL_SEMAPHORE_TYPE_NV = $95B3;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
    GL_SEMAPHORE_TYPE_BINARY_NV = $95B4;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
    GL_SEMAPHORE_TYPE_TIMELINE_NV = $95B5;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
    GL_MAX_TIMELINE_SEMAPHORE_VALUE_DIFFERENCE_NV = $95B6;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_NUM_VIEWS_OVR = $9630;   // api(gl|glcore|gles2) //extension: GL_OVR_multiview
    GL_MAX_VIEWS_OVR = $9631;   // api(gl|glcore|gles2) //extension: GL_OVR_multiview
    GL_FRAMEBUFFER_ATTACHMENT_TEXTURE_BASE_VIEW_INDEX_OVR = $9632;   // api(gl|glcore|gles2) //extension: GL_OVR_multiview
    GL_FRAMEBUFFER_INCOMPLETE_VIEW_TARGETS_OVR = $9633;   // api(gl|glcore|gles2) //extension: GL_OVR_multiview
    GL_GS_SHADER_BINARY_MTK = $9640;
    GL_GS_PROGRAM_BINARY_MTK = $9641;
    GL_MAX_SHADER_COMBINED_LOCAL_STORAGE_FAST_SIZE_EXT = $9650;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage2
    GL_MAX_SHADER_COMBINED_LOCAL_STORAGE_SIZE_EXT = $9651;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage2
    GL_FRAMEBUFFER_INCOMPLETE_INSUFFICIENT_SHADER_COMBINED_LOCAL_STORAGE_EXT = $9652;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage2
    GL_FRAMEBUFFER_FETCH_NONCOHERENT_QCOM = $96A2;   // api(gles2) //extension: GL_QCOM_shader_framebuffer_fetch_noncoherent
    GL_VALIDATE_SHADER_BINARY_QCOM = $96A3;
    GL_SHADING_RATE_QCOM = $96A4;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_PRESERVE_ASPECT_RATIO_QCOM = $96A5;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_1X1_PIXELS_QCOM = $96A6;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_1X2_PIXELS_QCOM = $96A7;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_2X1_PIXELS_QCOM = $96A8;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_2X2_PIXELS_QCOM = $96A9;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_1X4_PIXELS_QCOM = $96AA;
    GL_SHADING_RATE_4X1_PIXELS_QCOM = $96AB;
    GL_SHADING_RATE_4X2_PIXELS_QCOM = $96AC;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_SHADING_RATE_2X4_PIXELS_QCOM = $96AD;
    GL_SHADING_RATE_4X4_PIXELS_QCOM = $96AE;   // api(gles2) //extension: GL_QCOM_shading_rate
    GL_RASTER_POSITION_UNCLIPPED_IBM = $19262;
    GL_CULL_VERTEX_IBM = 103050;
    GL_ALL_STATIC_DATA_IBM = 103060;
    GL_STATIC_VERTEX_ARRAY_IBM = 103061;
    GL_VERTEX_ARRAY_LIST_IBM = 103070;
    GL_NORMAL_ARRAY_LIST_IBM = 103071;
    GL_COLOR_ARRAY_LIST_IBM = 103072;
    GL_INDEX_ARRAY_LIST_IBM = 103073;
    GL_TEXTURE_COORD_ARRAY_LIST_IBM = 103074;
    GL_EDGE_FLAG_ARRAY_LIST_IBM = 103075;
    GL_FOG_COORDINATE_ARRAY_LIST_IBM = 103076;
    GL_SECONDARY_COLOR_ARRAY_LIST_IBM = 103077;
    GL_VERTEX_ARRAY_LIST_STRIDE_IBM = 103080;
    GL_NORMAL_ARRAY_LIST_STRIDE_IBM = 103081;
    GL_COLOR_ARRAY_LIST_STRIDE_IBM = 103082;
    GL_INDEX_ARRAY_LIST_STRIDE_IBM = 103083;
    GL_TEXTURE_COORD_ARRAY_LIST_STRIDE_IBM = 103084;
    GL_EDGE_FLAG_ARRAY_LIST_STRIDE_IBM = 103085;
    GL_FOG_COORDINATE_ARRAY_LIST_STRIDE_IBM = 103086;
    GL_SECONDARY_COLOR_ARRAY_LIST_STRIDE_IBM = 103087;
    GL_PREFER_DOUBLEBUFFER_HINT_PGI = $1A1F8;
    GL_CONSERVE_MEMORY_HINT_PGI = $1A1FD;
    GL_RECLAIM_MEMORY_HINT_PGI = $1A1FE;
    GL_NATIVE_GRAPHICS_HANDLE_PGI = $1A202;
    GL_NATIVE_GRAPHICS_BEGIN_HINT_PGI = $1A203;
    GL_NATIVE_GRAPHICS_END_HINT_PGI = $1A204;
    GL_ALWAYS_FAST_HINT_PGI = $1A20C;
    GL_ALWAYS_SOFT_HINT_PGI = $1A20D;
    GL_ALLOW_DRAW_OBJ_HINT_PGI = $1A20E;
    GL_ALLOW_DRAW_WIN_HINT_PGI = $1A20F;
    GL_ALLOW_DRAW_FRG_HINT_PGI = $1A210;
    GL_ALLOW_DRAW_MEM_HINT_PGI = $1A211;
    GL_STRICT_DEPTHFUNC_HINT_PGI = $1A216;
    GL_STRICT_LIGHTING_HINT_PGI = $1A217;
    GL_STRICT_SCISSOR_HINT_PGI = $1A218;
    GL_FULL_STIPPLE_HINT_PGI = $1A219;
    GL_CLIP_NEAR_HINT_PGI = $1A220;
    GL_CLIP_FAR_HINT_PGI = $1A221;
    GL_WIDE_LINE_HINT_PGI = $1A222;
    GL_BACK_NORMALS_HINT_PGI = $1A223;
    GL_VERTEX_DATA_HINT_PGI = $1A22A;
    GL_VERTEX_CONSISTENT_HINT_PGI = $1A22B;
    GL_MATERIAL_SIDE_HINT_PGI = $1A22C;
    GL_MAX_VERTEX_HINT_PGI = $1A22D;
 
//--- EGL ----
 
    //... EGL_VERSION_1_0 ....
    EGL_ALPHA_SIZE = $3021;
    EGL_BAD_ACCESS = $3002;
    EGL_BAD_ALLOC = $3003;
    EGL_BAD_ATTRIBUTE = $3004;
    EGL_BAD_CONFIG = $3005;
    EGL_BAD_CONTEXT = $3006;
    EGL_BAD_CURRENT_SURFACE = $3007;
    EGL_BAD_DISPLAY = $3008;
    EGL_BAD_MATCH = $3009;
    EGL_BAD_NATIVE_PIXMAP = $300A;
    EGL_BAD_NATIVE_WINDOW = $300B;
    EGL_BAD_PARAMETER = $300C;
    EGL_BAD_SURFACE = $300D;
    EGL_BLUE_SIZE = $3022;
    EGL_BUFFER_SIZE = $3020;
    EGL_CONFIG_CAVEAT = $3027;
    EGL_CONFIG_ID = $3028;
    EGL_CORE_NATIVE_ENGINE = $305B;
    EGL_DEPTH_SIZE = $3025;
    EGL_DONT_CARE : EGLint = -1;
    EGL_DRAW = $3059;
    EGL_EXTENSIONS = $3055;
    EGL_FALSE = 0;
    EGL_GREEN_SIZE = $3023;
    EGL_HEIGHT = $3056;
    EGL_LARGEST_PBUFFER = $3058;
    EGL_LEVEL = $3029;
    EGL_MAX_PBUFFER_HEIGHT = $302A;
    EGL_MAX_PBUFFER_PIXELS = $302B;
    EGL_MAX_PBUFFER_WIDTH = $302C;
    EGL_NATIVE_RENDERABLE = $302D;
    EGL_NATIVE_VISUAL_ID = $302E;
    EGL_NATIVE_VISUAL_TYPE = $302F;
    EGL_NONE = $3038;  //->Attribute list terminator
    EGL_NON_CONFORMANT_CONFIG = $3051;
    EGL_NOT_INITIALIZED = $3001;
    EGL_NO_CONTEXT : EGLContext = nil;
    EGL_NO_DISPLAY : EGLDisplay = nil;
    EGL_NO_SURFACE : EGLSurface = nil;
    EGL_PBUFFER_BIT = $0001;
    EGL_PIXMAP_BIT = $0002;
    EGL_READ = $305A;
    EGL_RED_SIZE = $3024;
    EGL_SAMPLES = $3031;
    EGL_SAMPLE_BUFFERS = $3032;
    EGL_SLOW_CONFIG = $3050;
    EGL_STENCIL_SIZE = $3026;
    EGL_SUCCESS = $3000;
    EGL_SURFACE_TYPE = $3033;
    EGL_TRANSPARENT_BLUE_VALUE = $3035;
    EGL_TRANSPARENT_GREEN_VALUE = $3036;
    EGL_TRANSPARENT_RED_VALUE = $3037;
    EGL_TRANSPARENT_RGB = $3052;
    EGL_TRANSPARENT_TYPE = $3034;
    EGL_TRUE = 1;
    EGL_VENDOR = $3053;
    EGL_VERSION = $3054;
    EGL_WIDTH = $3057;
    EGL_WINDOW_BIT = $0004;
 
    //... EGL_VERSION_1_1 ....
    EGL_BACK_BUFFER = $3084;
    EGL_BIND_TO_TEXTURE_RGB = $3039;
    EGL_BIND_TO_TEXTURE_RGBA = $303A;
    EGL_CONTEXT_LOST = $300E;
    EGL_MIN_SWAP_INTERVAL = $303B;
    EGL_MAX_SWAP_INTERVAL = $303C;
    EGL_MIPMAP_TEXTURE = $3082;
    EGL_MIPMAP_LEVEL = $3083;
    EGL_NO_TEXTURE = $305C;
    EGL_TEXTURE_2D = $305F;
    EGL_TEXTURE_FORMAT = $3080;
    EGL_TEXTURE_RGB = $305D;
    EGL_TEXTURE_RGBA = $305E;
    EGL_TEXTURE_TARGET = $3081;
 
    //... EGL_VERSION_1_2 ....
    EGL_ALPHA_FORMAT = $3088;
    EGL_ALPHA_FORMAT_NONPRE = $308B;
    EGL_ALPHA_FORMAT_PRE = $308C;
    EGL_ALPHA_MASK_SIZE = $303E;
    EGL_BUFFER_PRESERVED = $3094;
    EGL_BUFFER_DESTROYED = $3095;
    EGL_CLIENT_APIS = $308D;
    EGL_COLORSPACE = $3087;
    EGL_COLORSPACE_sRGB = $3089;
    EGL_COLORSPACE_LINEAR = $308A;
    EGL_COLOR_BUFFER_TYPE = $303F;
    EGL_CONTEXT_CLIENT_TYPE = $3097;
    EGL_DISPLAY_SCALING = 10000;
    EGL_HORIZONTAL_RESOLUTION = $3090;
    EGL_LUMINANCE_BUFFER = $308F;
    EGL_LUMINANCE_SIZE = $303D;
    EGL_OPENGL_ES_BIT = $0001;
    EGL_OPENVG_BIT = $0002;
    EGL_OPENGL_ES_API = $30A0;
    EGL_OPENVG_API = $30A1;
    EGL_OPENVG_IMAGE = $3096;
    EGL_PIXEL_ASPECT_RATIO = $3092;
    EGL_RENDERABLE_TYPE = $3040;
    EGL_RENDER_BUFFER = $3086;
    EGL_RGB_BUFFER = $308E;
    EGL_SINGLE_BUFFER = $3085;
    EGL_SWAP_BEHAVIOR = $3093;
    EGL_UNKNOWN : EGLint = -1;
    EGL_VERTICAL_RESOLUTION = $3091;
 
    //... EGL_VERSION_1_3 ....
    EGL_CONFORMANT = $3042;
    EGL_CONTEXT_CLIENT_VERSION = $3098;
    EGL_MATCH_NATIVE_PIXMAP = $3041;
    EGL_OPENGL_ES2_BIT = $0004;
    EGL_VG_ALPHA_FORMAT = $3088;
    EGL_VG_ALPHA_FORMAT_NONPRE = $308B;
    EGL_VG_ALPHA_FORMAT_PRE = $308C;
    EGL_VG_ALPHA_FORMAT_PRE_BIT = $0040;
    EGL_VG_COLORSPACE = $3087;
    EGL_VG_COLORSPACE_sRGB = $3089;
    EGL_VG_COLORSPACE_LINEAR = $308A;
    EGL_VG_COLORSPACE_LINEAR_BIT = $0020;
 
    //... EGL_VERSION_1_4 ....
    EGL_DEFAULT_DISPLAY : EGLNativeDisplayType = 0;
    EGL_MULTISAMPLE_RESOLVE_BOX_BIT = $0200;
    EGL_MULTISAMPLE_RESOLVE = $3099;
    EGL_MULTISAMPLE_RESOLVE_DEFAULT = $309A;
    EGL_MULTISAMPLE_RESOLVE_BOX = $309B;
    EGL_OPENGL_API = $30A2;
    EGL_OPENGL_BIT = $0008;
    EGL_SWAP_BEHAVIOR_PRESERVED_BIT = $0400;
 
    //... EGL_VERSION_1_5 ....
    EGL_CONTEXT_MAJOR_VERSION = $3098;
    EGL_CONTEXT_MINOR_VERSION = $30FB;
    EGL_CONTEXT_OPENGL_PROFILE_MASK = $30FD;
    EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY = $31BD;
    EGL_NO_RESET_NOTIFICATION = $31BE;
    EGL_LOSE_CONTEXT_ON_RESET = $31BF;
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT = $00000001;
    EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT = $00000002;
    EGL_CONTEXT_OPENGL_DEBUG = $31B0;
    EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE = $31B1;
    EGL_CONTEXT_OPENGL_ROBUST_ACCESS = $31B2;
    EGL_OPENGL_ES3_BIT = $00000040;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CL_EVENT_HANDLE = $309C;
    EGL_SYNC_CL_EVENT = $30FE;
    EGL_SYNC_CL_EVENT_COMPLETE = $30FF;
    EGL_SYNC_PRIOR_COMMANDS_COMPLETE = $30F0;
    EGL_SYNC_TYPE = $30F7;
    EGL_SYNC_STATUS = $30F1;
    EGL_SYNC_CONDITION = $30F8;
    EGL_SIGNALED = $30F2;
    EGL_UNSIGNALED = $30F3;
    EGL_SYNC_FLUSH_COMMANDS_BIT = $0001;
    EGL_FOREVER = $FFFFFFFFFFFFFFFF;
    EGL_TIMEOUT_EXPIRED = $30F5;
    EGL_CONDITION_SATISFIED = $30F6;
    EGL_NO_SYNC : EGLSync = nil;
    EGL_SYNC_FENCE = $30F9;
    EGL_GL_COLORSPACE = $309D;   // api(egl) //extension: EGL_EXT_image_gl_colorspace
    EGL_GL_COLORSPACE_SRGB = $3089;
    EGL_GL_COLORSPACE_LINEAR = $308A;
    EGL_GL_RENDERBUFFER = $30B9;
    EGL_GL_TEXTURE_2D = $30B1;
    EGL_GL_TEXTURE_LEVEL = $30BC;
    EGL_GL_TEXTURE_3D = $30B2;
    EGL_GL_TEXTURE_ZOFFSET = $30BD;
    EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X = $30B3;
    EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X = $30B4;
    EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y = $30B5;
    EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y = $30B6;
    EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z = $30B7;
    EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z = $30B8;
    EGL_IMAGE_PRESERVED = $30D2;
    EGL_NO_IMAGE : EGLImage = nil;
 
    //... Other ....
    EGL_PBUFFER_IMAGE_BIT_TAO = $0008;  //->Unreleased TAO extension
    EGL_PBUFFER_PALETTE_IMAGE_BIT_TAO = $0010;  //->Unreleased TAO extension
    EGL_VG_COLORSPACE_LINEAR_BIT_KHR = $0020;   // api(egl) //extension: EGL_KHR_config_attribs
    EGL_VG_ALPHA_FORMAT_PRE_BIT_KHR = $0040;   // api(egl) //extension: EGL_KHR_config_attribs
    EGL_LOCK_SURFACE_BIT_KHR = $0080;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_OPTIMAL_FORMAT_BIT_KHR = $0100;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_STREAM_BIT_KHR = $0800;   // api(egl) //extension: EGL_KHR_stream_producer_eglsurface
    EGL_MUTABLE_RENDER_BUFFER_BIT_KHR = $1000;   // api(egl) //extension: EGL_KHR_mutable_render_buffer
    EGL_INTEROP_BIT_KHR = $0010;  //->EGL_KHR_interop
    EGL_OPENMAX_IL_BIT_KHR = $0020;  //->EGL_KHR_interop
    EGL_OPENGL_ES3_BIT_KHR = $00000040;   // api(egl) //extension: EGL_KHR_create_context
    EGL_READ_SURFACE_BIT_KHR = $0001;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_WRITE_SURFACE_BIT_KHR = $0002;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_NATIVE_BUFFER_USAGE_PROTECTED_BIT_ANDROID = $00000001;   // api(egl) //extension: EGL_ANDROID_create_native_client_buffer
    EGL_NATIVE_BUFFER_USAGE_RENDERBUFFER_BIT_ANDROID = $00000002;   // api(egl) //extension: EGL_ANDROID_create_native_client_buffer
    EGL_NATIVE_BUFFER_USAGE_TEXTURE_BIT_ANDROID = $00000004;   // api(egl) //extension: EGL_ANDROID_create_native_client_buffer
    EGL_SYNC_FLUSH_COMMANDS_BIT_KHR = $0001;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_SYNC_FLUSH_COMMANDS_BIT_NV = $0001;   // api(egl) //extension: EGL_NV_sync
    EGL_DRM_BUFFER_USE_SCANOUT_MESA = $00000001;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_DRM_BUFFER_USE_SHARE_MESA = $00000002;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_DRM_BUFFER_USE_CURSOR_MESA = $00000004;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR = $00000001;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE_BIT_KHR = $00000002;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_OPENGL_ROBUST_ACCESS_BIT_KHR = $00000004;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT_KHR = $00000001;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_OPENGL_COMPATIBILITY_PROFILE_BIT_KHR = $00000002;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_RELEASE_BEHAVIOR_NONE_KHR = 0;   // api(egl) //extension: EGL_KHR_context_flush_control
    EGL_CONTEXT_RELEASE_BEHAVIOR_KHR = $2097;   // api(egl) //extension: EGL_KHR_context_flush_control
    EGL_CONTEXT_RELEASE_BEHAVIOR_FLUSH_KHR = $2098;   // api(egl) //extension: EGL_KHR_context_flush_control
    EGL_NO_NATIVE_FENCE_FD_ANDROID = -1;   // api(egl) //extension: EGL_ANDROID_native_fence_sync
    EGL_DEPTH_ENCODING_NONE_NV = 0;   // api(egl) //extension: EGL_NV_depth_nonlinear
    EGL_NO_DEVICE_EXT : EGLDeviceEXT = nil;   // api(egl) //extension: EGL_EXT_device_base
    EGL_NO_IMAGE_KHR : EGLImageKHR = nil;   // api(egl) //extension: EGL_KHR_image
    EGL_NO_FILE_DESCRIPTOR_KHR : EGLNativeFileDescriptorKHR = -1;   // api(egl) //extension: EGL_KHR_stream_cross_process_fd
    EGL_NO_OUTPUT_LAYER_EXT : EGLOutputLayerEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
    EGL_NO_OUTPUT_PORT_EXT : EGLOutputPortEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
    EGL_NO_STREAM_KHR : EGLStreamKHR = nil;   // api(egl) //extension: EGL_KHR_stream
    EGL_NO_SYNC_KHR : EGLSyncKHR = nil;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_NO_SYNC_NV : EGLSyncNV = nil;   // api(egl) //extension: EGL_NV_sync
    EGL_NO_CONFIG_KHR : EGLConfig = nil;   // api(egl) //extension: EGL_KHR_no_config_context
    EGL_FOREVER_KHR = $FFFFFFFFFFFFFFFF;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_FOREVER_NV = $FFFFFFFFFFFFFFFF;   // api(egl) //extension: EGL_NV_sync
    EGL_CONFORMANT_KHR = $3042;   // api(egl) //extension: EGL_KHR_config_attribs
    EGL_MATCH_FORMAT_KHR = $3043;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_Y_INVERTED_NOK = $307F;   // api(egl) //extension: EGL_NOK_texture_from_pixmap
    EGL_GL_COLORSPACE_SRGB_KHR = $3089;   // api(egl) //extension: EGL_KHR_gl_colorspace
    EGL_GL_COLORSPACE_LINEAR_KHR = $308A;   // api(egl) //extension: EGL_KHR_gl_colorspace
    EGL_CONTEXT_MAJOR_VERSION_KHR = $3098;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CL_EVENT_HANDLE_KHR = $309C;   // api(egl) //extension: EGL_KHR_cl_event
    EGL_GL_COLORSPACE_KHR = $309D;   // api(egl) //extension: EGL_KHR_gl_colorspace
    EGL_NATIVE_PIXMAP_KHR = $30B0;   // api(egl) //extension: EGL_KHR_image
    EGL_GL_TEXTURE_2D_KHR = $30B1;   // api(egl) //extension: EGL_KHR_gl_texture_2D_image
    EGL_GL_TEXTURE_3D_KHR = $30B2;   // api(egl) //extension: EGL_KHR_gl_texture_3D_image
    EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_X_KHR = $30B3;   // api(egl) //extension: EGL_KHR_gl_texture_cubemap_image
    EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_X_KHR = $30B4;   // api(egl) //extension: EGL_KHR_gl_texture_cubemap_image
    EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Y_KHR = $30B5;   // api(egl) //extension: EGL_KHR_gl_texture_cubemap_image
    EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_KHR = $30B6;   // api(egl) //extension: EGL_KHR_gl_texture_cubemap_image
    EGL_GL_TEXTURE_CUBE_MAP_POSITIVE_Z_KHR = $30B7;   // api(egl) //extension: EGL_KHR_gl_texture_cubemap_image
    EGL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_KHR = $30B8;   // api(egl) //extension: EGL_KHR_gl_texture_cubemap_image
    EGL_GL_RENDERBUFFER_KHR = $30B9;   // api(egl) //extension: EGL_KHR_gl_renderbuffer_image
    EGL_VG_PARENT_IMAGE_KHR = $30BA;   // api(egl) //extension: EGL_KHR_vg_parent_image
    EGL_GL_TEXTURE_LEVEL_KHR = $30BC;   // api(egl) //extension: EGL_KHR_gl_texture_2D_image
    EGL_GL_TEXTURE_ZOFFSET_KHR = $30BD;   // api(egl) //extension: EGL_KHR_gl_texture_3D_image
    EGL_POST_SUB_BUFFER_SUPPORTED_NV = $30BE;   // api(egl) //extension: EGL_NV_post_sub_buffer
    EGL_CONTEXT_OPENGL_ROBUST_ACCESS_EXT = $30BF;   // api(egl) //extension: EGL_EXT_create_context_robustness
    EGL_FORMAT_RGB_565_EXACT_KHR = $30C0;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_FORMAT_RGB_565_KHR = $30C1;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_FORMAT_RGBA_8888_EXACT_KHR = $30C2;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_FORMAT_RGBA_8888_KHR = $30C3;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_MAP_PRESERVE_PIXELS_KHR = $30C4;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_LOCK_USAGE_HINT_KHR = $30C5;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_POINTER_KHR = $30C6;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_PITCH_KHR = $30C7;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_ORIGIN_KHR = $30C8;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_PIXEL_RED_OFFSET_KHR = $30C9;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_PIXEL_GREEN_OFFSET_KHR = $30CA;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_PIXEL_BLUE_OFFSET_KHR = $30CB;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_PIXEL_ALPHA_OFFSET_KHR = $30CC;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_BITMAP_PIXEL_LUMINANCE_OFFSET_KHR = $30CD;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_LOWER_LEFT_KHR = $30CE;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_UPPER_LEFT_KHR = $30CF;   // api(egl) //extension: EGL_KHR_lock_surface
    EGL_IMAGE_PRESERVED_KHR = $30D2;   // api(egl) //extension: EGL_KHR_image_base
    EGL_SHARED_IMAGE_NOK = $30DA;  //->Unreleased extension
    EGL_COVERAGE_BUFFERS_NV = $30E0;   // api(egl) //extension: EGL_NV_coverage_sample
    EGL_COVERAGE_SAMPLES_NV = $30E1;   // api(egl) //extension: EGL_NV_coverage_sample
    EGL_DEPTH_ENCODING_NV = $30E2;   // api(egl) //extension: EGL_NV_depth_nonlinear
    EGL_DEPTH_ENCODING_NONLINEAR_NV = $30E3;   // api(egl) //extension: EGL_NV_depth_nonlinear
    EGL_SYNC_PRIOR_COMMANDS_COMPLETE_NV = $30E6;   // api(egl) //extension: EGL_NV_sync
    EGL_SYNC_STATUS_NV = $30E7;   // api(egl) //extension: EGL_NV_sync
    EGL_SIGNALED_NV = $30E8;   // api(egl) //extension: EGL_NV_sync
    EGL_UNSIGNALED_NV = $30E9;   // api(egl) //extension: EGL_NV_sync
    EGL_ALREADY_SIGNALED_NV = $30EA;   // api(egl) //extension: EGL_NV_sync
    EGL_TIMEOUT_EXPIRED_NV = $30EB;   // api(egl) //extension: EGL_NV_sync
    EGL_CONDITION_SATISFIED_NV = $30EC;   // api(egl) //extension: EGL_NV_sync
    EGL_SYNC_TYPE_NV = $30ED;   // api(egl) //extension: EGL_NV_sync
    EGL_SYNC_CONDITION_NV = $30EE;   // api(egl) //extension: EGL_NV_sync
    EGL_SYNC_FENCE_NV = $30EF;   // api(egl) //extension: EGL_NV_sync
    EGL_SYNC_PRIOR_COMMANDS_COMPLETE_KHR = $30F0;   // api(egl) //extension: EGL_KHR_fence_sync
    EGL_SYNC_STATUS_KHR = $30F1;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_SIGNALED_KHR = $30F2;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_UNSIGNALED_KHR = $30F3;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_TIMEOUT_EXPIRED_KHR = $30F5;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_CONDITION_SATISFIED_KHR = $30F6;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_SYNC_TYPE_KHR = $30F7;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_SYNC_CONDITION_KHR = $30F8;   // api(egl) //extension: EGL_KHR_fence_sync
    EGL_SYNC_FENCE_KHR = $30F9;   // api(egl) //extension: EGL_KHR_fence_sync
    EGL_SYNC_REUSABLE_KHR = $30FA;   // api(egl) //extension: EGL_KHR_reusable_sync
    EGL_CONTEXT_MINOR_VERSION_KHR = $30FB;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_FLAGS_KHR = $30FC;   // api(egl) //extension: EGL_KHR_create_context
    EGL_CONTEXT_OPENGL_PROFILE_MASK_KHR = $30FD;   // api(egl) //extension: EGL_KHR_create_context
    EGL_SYNC_CL_EVENT_KHR = $30FE;   // api(egl) //extension: EGL_KHR_cl_event
    EGL_SYNC_CL_EVENT_COMPLETE_KHR = $30FF;   // api(egl) //extension: EGL_KHR_cl_event
    EGL_CONTEXT_PRIORITY_LEVEL_IMG = $3100;   // api(egl) //extension: EGL_IMG_context_priority
    EGL_CONTEXT_PRIORITY_HIGH_IMG = $3101;   // api(egl) //extension: EGL_IMG_context_priority
    EGL_CONTEXT_PRIORITY_MEDIUM_IMG = $3102;   // api(egl) //extension: EGL_IMG_context_priority
    EGL_CONTEXT_PRIORITY_LOW_IMG = $3103;   // api(egl) //extension: EGL_IMG_context_priority
    EGL_NATIVE_BUFFER_MULTIPLANE_SEPARATE_IMG = $3105;   // api(egl) //extension: EGL_IMG_image_plane_attribs
    EGL_NATIVE_BUFFER_PLANE_OFFSET_IMG = $3106;   // api(egl) //extension: EGL_IMG_image_plane_attribs
    EGL_BITMAP_PIXEL_SIZE_KHR = $3110;   // api(egl) //extension: EGL_KHR_lock_surface2
    EGL_NEW_IMAGE_QCOM = $3120;
    EGL_IMAGE_FORMAT_QCOM = $3121;
    EGL_FORMAT_RGBA_8888_QCOM = $3122;
    EGL_FORMAT_RGB_565_QCOM = $3123;
    EGL_FORMAT_YUYV_QCOM = $3124;
    EGL_FORMAT_UYVY_QCOM = $3125;
    EGL_FORMAT_YV12_QCOM = $3126;
    EGL_FORMAT_NV21_QCOM = $3127;
    EGL_FORMAT_NV12_TILED_QCOM = $3128;
    EGL_FORMAT_BGRA_8888_QCOM = $3129;
    EGL_FORMAT_BGRX_8888_QCOM = $312A;
    EGL_FORMAT_RGBX_8888_QCOM = $312F;
    EGL_COVERAGE_SAMPLE_RESOLVE_NV = $3131;   // api(egl) //extension: EGL_NV_coverage_sample_resolve
    EGL_COVERAGE_SAMPLE_RESOLVE_DEFAULT_NV = $3132;   // api(egl) //extension: EGL_NV_coverage_sample_resolve
    EGL_COVERAGE_SAMPLE_RESOLVE_NONE_NV = $3133;   // api(egl) //extension: EGL_NV_coverage_sample_resolve
    EGL_MULTIVIEW_VIEW_COUNT_EXT = $3134;   // api(egl) //extension: EGL_EXT_multiview_window
    EGL_AUTO_STEREO_NV = $3136;   // api(egl) //extension: EGL_NV_3dvision_surface
    EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_EXT = $3138;   // api(egl) //extension: EGL_EXT_create_context_robustness
    EGL_BUFFER_AGE_KHR = $313D;   // api(egl) //extension: EGL_KHR_partial_update
    EGL_BUFFER_AGE_EXT = $313D;   // api(egl) //extension: EGL_EXT_buffer_age
    EGL_PLATFORM_DEVICE_EXT = $313F;   // api(egl) //extension: EGL_EXT_platform_device
    EGL_NATIVE_BUFFER_ANDROID = $3140;   // api(egl) //extension: EGL_ANDROID_image_native_buffer
    EGL_PLATFORM_ANDROID_KHR = $3141;   // api(egl) //extension: EGL_KHR_platform_android
    EGL_RECORDABLE_ANDROID = $3142;   // api(egl) //extension: EGL_ANDROID_recordable
    EGL_NATIVE_BUFFER_USAGE_ANDROID = $3143;   // api(egl) //extension: EGL_ANDROID_create_native_client_buffer
    EGL_SYNC_NATIVE_FENCE_ANDROID = $3144;   // api(egl) //extension: EGL_ANDROID_native_fence_sync
    EGL_SYNC_NATIVE_FENCE_FD_ANDROID = $3145;   // api(egl) //extension: EGL_ANDROID_native_fence_sync
    EGL_SYNC_NATIVE_FENCE_SIGNALED_ANDROID = $3146;   // api(egl) //extension: EGL_ANDROID_native_fence_sync
    EGL_FRAMEBUFFER_TARGET_ANDROID = $3147;   // api(egl) //extension: EGL_ANDROID_framebuffer_target
    EGL_FRONT_BUFFER_AUTO_REFRESH_ANDROID = $314C;   // api(egl) //extension: EGL_ANDROID_front_buffer_auto_refresh
    EGL_GL_COLORSPACE_DEFAULT_EXT = $314D;   // api(egl) //extension: EGL_EXT_image_gl_colorspace
    EGL_CONTEXT_OPENGL_NO_ERROR_KHR = $31B3;   // api(egl) //extension: EGL_KHR_create_context_no_error
    EGL_CONTEXT_OPENGL_RESET_NOTIFICATION_STRATEGY_KHR = $31BD;   // api(egl) //extension: EGL_KHR_create_context
    EGL_NO_RESET_NOTIFICATION_KHR = $31BE;   // api(egl) //extension: EGL_KHR_create_context
    EGL_NO_RESET_NOTIFICATION_EXT = $31BE;   // api(egl) //extension: EGL_EXT_create_context_robustness
    EGL_LOSE_CONTEXT_ON_RESET_KHR = $31BF;   // api(egl) //extension: EGL_KHR_create_context
    EGL_LOSE_CONTEXT_ON_RESET_EXT = $31BF;   // api(egl) //extension: EGL_EXT_create_context_robustness
    EGL_FORMAT_R8_QCOM = $31C0;
    EGL_FORMAT_RG88_QCOM = $31C1;
    EGL_FORMAT_NV12_QCOM = $31C2;
    EGL_FORMAT_SRGBX_8888_QCOM = $31C3;
    EGL_FORMAT_SRGBA_8888_QCOM = $31C4;
    EGL_FORMAT_YVYU_QCOM = $31C5;
    EGL_FORMAT_VYUY_QCOM = $31C6;
    EGL_FORMAT_IYUV_QCOM = $31C7;
    EGL_FORMAT_RGB_888_QCOM = $31C8;
    EGL_FORMAT_RGBA_5551_QCOM = $31C9;
    EGL_FORMAT_RGBA_4444_QCOM = $31CA;
    EGL_FORMAT_R_16_FLOAT_QCOM = $31CB;
    EGL_FORMAT_RG_1616_FLOAT_QCOM = $31CC;
    EGL_FORMAT_RGBA_16_FLOAT_QCOM = $31CD;
    EGL_FORMAT_RGBA_1010102_QCOM = $31CE;
    EGL_FORMAT_FLAG_QCOM = $31CF;
    EGL_DRM_BUFFER_FORMAT_MESA = $31D0;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_DRM_BUFFER_USE_MESA = $31D1;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_DRM_BUFFER_FORMAT_ARGB32_MESA = $31D2;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_DRM_BUFFER_MESA = $31D3;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_DRM_BUFFER_STRIDE_MESA = $31D4;   // api(egl) //extension: EGL_MESA_drm_image
    EGL_PLATFORM_X11_KHR = $31D5;   // api(egl) //extension: EGL_KHR_platform_x11
    EGL_PLATFORM_X11_EXT = $31D5;   // api(egl) //extension: EGL_EXT_platform_x11
    EGL_PLATFORM_X11_SCREEN_KHR = $31D6;   // api(egl) //extension: EGL_KHR_platform_x11
    EGL_PLATFORM_X11_SCREEN_EXT = $31D6;   // api(egl) //extension: EGL_EXT_platform_x11
    EGL_PLATFORM_GBM_KHR = $31D7;   // api(egl) //extension: EGL_KHR_platform_gbm
    EGL_PLATFORM_GBM_MESA = $31D7;   // api(egl) //extension: EGL_MESA_platform_gbm
    EGL_PLATFORM_WAYLAND_KHR = $31D8;   // api(egl) //extension: EGL_KHR_platform_wayland
    EGL_PLATFORM_WAYLAND_EXT = $31D8;   // api(egl) //extension: EGL_EXT_platform_wayland
    EGL_PLATFORM_SURFACELESS_MESA = $31DD;   // api(egl) //extension: EGL_MESA_platform_surfaceless
    EGL_WAYLAND_BUFFER_WL = $31D5;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_WAYLAND_PLANE_WL = $31D6;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_TEXTURE_Y_U_V_WL = $31D7;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_TEXTURE_Y_UV_WL = $31D8;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_TEXTURE_Y_XUXV_WL = $31D9;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_TEXTURE_EXTERNAL_WL = $31DA;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_WAYLAND_Y_INVERTED_WL = $31DB;   // api(egl) //extension: EGL_WL_bind_wayland_display
    EGL_STREAM_FIFO_LENGTH_KHR = $31FC;   // api(egl) //extension: EGL_KHR_stream_fifo
    EGL_STREAM_TIME_NOW_KHR = $31FD;   // api(egl) //extension: EGL_KHR_stream_fifo
    EGL_STREAM_TIME_CONSUMER_KHR = $31FE;   // api(egl) //extension: EGL_KHR_stream_fifo
    EGL_STREAM_TIME_PRODUCER_KHR = $31FF;   // api(egl) //extension: EGL_KHR_stream_fifo
    EGL_D3D_TEXTURE_2D_SHARE_HANDLE_ANGLE = $3200;   // api(egl) //extension: EGL_ANGLE_d3d_share_handle_client_buffer
    EGL_FIXED_SIZE_ANGLE = $3201;   // api(egl) //extension: EGL_ANGLE_window_fixed_size
    EGL_CONSUMER_LATENCY_USEC_KHR = $3210;   // api(egl) //extension: EGL_KHR_stream
    EGL_PRODUCER_FRAME_KHR = $3212;   // api(egl) //extension: EGL_KHR_stream
    EGL_CONSUMER_FRAME_KHR = $3213;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_KHR = $3214;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_CREATED_KHR = $3215;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_CONNECTING_KHR = $3216;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_EMPTY_KHR = $3217;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_NEW_FRAME_AVAILABLE_KHR = $3218;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_OLD_FRAME_AVAILABLE_KHR = $3219;   // api(egl) //extension: EGL_KHR_stream
    EGL_STREAM_STATE_DISCONNECTED_KHR = $321A;   // api(egl) //extension: EGL_KHR_stream
    EGL_BAD_STREAM_KHR = $321B;   // api(egl) //extension: EGL_KHR_stream
    EGL_BAD_STATE_KHR = $321C;   // api(egl) //extension: EGL_KHR_stream
    EGL_BUFFER_COUNT_NV = $321D;  //->From EGL_NV_stream_producer_eglsurface, which has no known specification and was replaced by a KHR extension
    EGL_CONSUMER_ACQUIRE_TIMEOUT_USEC_KHR = $321E;   // api(egl) //extension: EGL_KHR_stream_consumer_gltexture
    EGL_SYNC_NEW_FRAME_NV = $321F;   // api(egl) //extension: EGL_NV_stream_sync
    EGL_BAD_DEVICE_EXT = $322B;   // api(egl) //extension: EGL_EXT_device_base
    EGL_DEVICE_EXT = $322C;   // api(egl) //extension: EGL_EXT_device_base
    EGL_BAD_OUTPUT_LAYER_EXT = $322D;   // api(egl) //extension: EGL_EXT_output_base
    EGL_BAD_OUTPUT_PORT_EXT = $322E;   // api(egl) //extension: EGL_EXT_output_base
    EGL_SWAP_INTERVAL_EXT = $322F;   // api(egl) //extension: EGL_EXT_output_base
    EGL_TRIPLE_BUFFER_NV = $3230;   // api(egl) //extension: EGL_NV_triple_buffer
    EGL_QUADRUPLE_BUFFER_NV = $3231;   // api(egl) //extension: EGL_NV_quadruple_buffer
    EGL_DRM_DEVICE_FILE_EXT = $3233;   // api(egl) //extension: EGL_EXT_device_drm
    EGL_DRM_CRTC_EXT = $3234;   // api(egl) //extension: EGL_EXT_output_drm
    EGL_DRM_PLANE_EXT = $3235;   // api(egl) //extension: EGL_EXT_output_drm
    EGL_DRM_CONNECTOR_EXT = $3236;   // api(egl) //extension: EGL_EXT_output_drm
    EGL_OPENWF_DEVICE_ID_EXT = $3237;   // api(egl) //extension: EGL_EXT_device_openwf
    EGL_OPENWF_PIPELINE_ID_EXT = $3238;   // api(egl) //extension: EGL_EXT_output_openwf
    EGL_OPENWF_PORT_ID_EXT = $3239;   // api(egl) //extension: EGL_EXT_output_openwf
    EGL_CUDA_DEVICE_NV = $323A;   // api(egl) //extension: EGL_NV_device_cuda
    EGL_CUDA_EVENT_HANDLE_NV = $323B;   // api(egl) //extension: EGL_NV_cuda_event
    EGL_SYNC_CUDA_EVENT_NV = $323C;   // api(egl) //extension: EGL_NV_cuda_event
    EGL_SYNC_CUDA_EVENT_COMPLETE_NV = $323D;   // api(egl) //extension: EGL_NV_cuda_event
    EGL_STREAM_CROSS_PARTITION_NV = $323F;   // api(egl) //extension: EGL_NV_stream_cross_partition
    EGL_STREAM_STATE_INITIALIZING_NV = $3240;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_TYPE_NV = $3241;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_PROTOCOL_NV = $3242;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_ENDPOINT_NV = $3243;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_LOCAL_NV = $3244;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_CROSS_PROCESS_NV = $3245;   // api(egl) //extension: EGL_NV_stream_cross_process
    EGL_STREAM_PROTOCOL_FD_NV = $3246;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_PRODUCER_NV = $3247;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_CONSUMER_NV = $3248;   // api(egl) //extension: EGL_NV_stream_remote
    EGL_STREAM_PROTOCOL_SOCKET_NV = $324B;   // api(egl) //extension: EGL_NV_stream_socket
    EGL_SOCKET_HANDLE_NV = $324C;   // api(egl) //extension: EGL_NV_stream_socket
    EGL_SOCKET_TYPE_NV = $324D;   // api(egl) //extension: EGL_NV_stream_socket
    EGL_SOCKET_TYPE_UNIX_NV = $324E;   // api(egl) //extension: EGL_NV_stream_socket_unix
    EGL_SOCKET_TYPE_INET_NV = $324F;   // api(egl) //extension: EGL_NV_stream_socket_inet
    EGL_MAX_STREAM_METADATA_BLOCKS_NV = $3250;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_MAX_STREAM_METADATA_BLOCK_SIZE_NV = $3251;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_MAX_STREAM_METADATA_TOTAL_SIZE_NV = $3252;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_PRODUCER_METADATA_NV = $3253;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_CONSUMER_METADATA_NV = $3254;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA0_SIZE_NV = $3255;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA1_SIZE_NV = $3256;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA2_SIZE_NV = $3257;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA3_SIZE_NV = $3258;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA0_TYPE_NV = $3259;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA1_TYPE_NV = $325A;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA2_TYPE_NV = $325B;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_METADATA3_TYPE_NV = $325C;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_LINUX_DMA_BUF_EXT = $3270;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_LINUX_DRM_FOURCC_EXT = $3271;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE0_FD_EXT = $3272;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE0_OFFSET_EXT = $3273;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE0_PITCH_EXT = $3274;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE1_FD_EXT = $3275;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE1_OFFSET_EXT = $3276;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE1_PITCH_EXT = $3277;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE2_FD_EXT = $3278;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE2_OFFSET_EXT = $3279;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DMA_BUF_PLANE2_PITCH_EXT = $327A;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_COLOR_SPACE_HINT_EXT = $327B;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_SAMPLE_RANGE_HINT_EXT = $327C;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_CHROMA_HORIZONTAL_SITING_HINT_EXT = $327D;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_CHROMA_VERTICAL_SITING_HINT_EXT = $327E;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_ITU_REC601_EXT = $327F;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_ITU_REC709_EXT = $3280;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_ITU_REC2020_EXT = $3281;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_FULL_RANGE_EXT = $3282;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_NARROW_RANGE_EXT = $3283;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_CHROMA_SITING_0_EXT = $3284;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_YUV_CHROMA_SITING_0_5_EXT = $3285;   // api(egl) //extension: EGL_EXT_image_dma_buf_import
    EGL_DISCARD_SAMPLES_ARM = $3286;   // api(egl) //extension: EGL_ARM_pixmap_multisample_discard
    EGL_COLOR_COMPONENT_TYPE_UNSIGNED_INTEGER_ARM = $3287;   // api(egl) //extension: EGL_ARM_image_format
    EGL_COLOR_COMPONENT_TYPE_INTEGER_ARM = $3288;   // api(egl) //extension: EGL_ARM_image_format
    EGL_SYNC_PRIOR_COMMANDS_IMPLICIT_EXTERNAL_ARM = $328A;   // api(egl) //extension: EGL_ARM_implicit_external_sync
    EGL_NATIVE_BUFFER_TIZEN = $32A0;   // api(egl) //extension: EGL_TIZEN_image_native_buffer
    EGL_NATIVE_SURFACE_TIZEN = $32A1;   // api(egl) //extension: EGL_TIZEN_image_native_surface
    EGL_IMAGE_NUM_PLANES_QCOM = $32B0;
    EGL_IMAGE_PLANE_PITCH_0_QCOM = $32B1;
    EGL_IMAGE_PLANE_PITCH_1_QCOM = $32B2;
    EGL_IMAGE_PLANE_PITCH_2_QCOM = $32B3;
    EGL_IMAGE_PLANE_DEPTH_0_QCOM = $32B4;
    EGL_IMAGE_PLANE_DEPTH_1_QCOM = $32B5;
    EGL_IMAGE_PLANE_DEPTH_2_QCOM = $32B6;
    EGL_IMAGE_PLANE_WIDTH_0_QCOM = $32B7;
    EGL_IMAGE_PLANE_WIDTH_1_QCOM = $32B8;
    EGL_IMAGE_PLANE_WIDTH_2_QCOM = $32B9;
    EGL_IMAGE_PLANE_HEIGHT_0_QCOM = $32BA;
    EGL_IMAGE_PLANE_HEIGHT_1_QCOM = $32BB;
    EGL_IMAGE_PLANE_HEIGHT_2_QCOM = $32BC;
    EGL_IMAGE_PLANE_POINTER_0_QCOM = $32BD;
    EGL_IMAGE_PLANE_POINTER_1_QCOM = $32BE;
    EGL_IMAGE_PLANE_POINTER_2_QCOM = $32BF;
    EGL_PROTECTED_CONTENT_EXT = $32C0;   // api(egl) //extension: EGL_EXT_protected_content
    EGL_GPU_PERF_HINT_QCOM = $32D0;
    EGL_HINT_PERSISTENT_QCOM = $32D1;
    EGL_YUV_BUFFER_EXT = $3300;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_EXT = $3301;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_YUV_EXT = $3302;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_YVU_EXT = $3303;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_YUYV_EXT = $3304;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_UYVY_EXT = $3305;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_YVYU_EXT = $3306;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_VYUY_EXT = $3307;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_ORDER_AYUV_EXT = $3308;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_CSC_STANDARD_EXT = $330A;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_CSC_STANDARD_601_EXT = $330B;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_CSC_STANDARD_709_EXT = $330C;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_CSC_STANDARD_2020_EXT = $330D;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_NUMBER_OF_PLANES_EXT = $3311;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_SUBSAMPLE_EXT = $3312;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_SUBSAMPLE_4_2_0_EXT = $3313;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_SUBSAMPLE_4_2_2_EXT = $3314;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_SUBSAMPLE_4_4_4_EXT = $3315;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_DEPTH_RANGE_EXT = $3317;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_DEPTH_RANGE_LIMITED_EXT = $3318;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_DEPTH_RANGE_FULL_EXT = $3319;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_PLANE_BPP_EXT = $331A;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_PLANE_BPP_0_EXT = $331B;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_PLANE_BPP_8_EXT = $331C;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_YUV_PLANE_BPP_10_EXT = $331D;   // api(egl) //extension: EGL_EXT_yuv_surface
    EGL_PENDING_METADATA_NV = $3328;   // api(egl) //extension: EGL_NV_stream_metadata
    EGL_PENDING_FRAME_NV = $3329;   // api(egl) //extension: EGL_NV_stream_fifo_next
    EGL_STREAM_TIME_PENDING_NV = $332A;   // api(egl) //extension: EGL_NV_stream_fifo_next
    EGL_YUV_PLANE0_TEXTURE_UNIT_NV = $332C;   // api(egl) //extension: EGL_NV_stream_consumer_gltexture_yuv
    EGL_YUV_PLANE1_TEXTURE_UNIT_NV = $332D;   // api(egl) //extension: EGL_NV_stream_consumer_gltexture_yuv
    EGL_YUV_PLANE2_TEXTURE_UNIT_NV = $332E;   // api(egl) //extension: EGL_NV_stream_consumer_gltexture_yuv
    EGL_SUPPORT_RESET_NV = $3334;   // api(egl) //extension: EGL_NV_stream_reset
    EGL_SUPPORT_REUSE_NV = $3335;   // api(egl) //extension: EGL_NV_stream_reset
    EGL_STREAM_FIFO_SYNCHRONOUS_NV = $3336;   // api(egl) //extension: EGL_NV_stream_fifo_synchronous
    EGL_PRODUCER_MAX_FRAME_HINT_NV = $3337;   // api(egl) //extension: EGL_NV_stream_frame_limits
    EGL_CONSUMER_MAX_FRAME_HINT_NV = $3338;   // api(egl) //extension: EGL_NV_stream_frame_limits
    EGL_COLOR_COMPONENT_TYPE_EXT = $3339;   // api(egl) //extension: EGL_EXT_pixel_format_float
    EGL_COLOR_COMPONENT_TYPE_FIXED_EXT = $333A;   // api(egl) //extension: EGL_EXT_pixel_format_float
    EGL_COLOR_COMPONENT_TYPE_FLOAT_EXT = $333B;   // api(egl) //extension: EGL_EXT_pixel_format_float
    EGL_DRM_MASTER_FD_EXT = $333C;   // api(egl) //extension: EGL_EXT_device_drm
    EGL_GL_COLORSPACE_BT2020_LINEAR_EXT = $333F;   // api(egl) //extension: EGL_EXT_gl_colorspace_bt2020_linear
    EGL_GL_COLORSPACE_BT2020_PQ_EXT = $3340;   // api(egl) //extension: EGL_EXT_gl_colorspace_bt2020_pq
    EGL_SMPTE2086_DISPLAY_PRIMARY_RX_EXT = $3341;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_DISPLAY_PRIMARY_RY_EXT = $3342;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_DISPLAY_PRIMARY_GX_EXT = $3343;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_DISPLAY_PRIMARY_GY_EXT = $3344;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_DISPLAY_PRIMARY_BX_EXT = $3345;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_DISPLAY_PRIMARY_BY_EXT = $3346;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_WHITE_POINT_X_EXT = $3347;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_WHITE_POINT_Y_EXT = $3348;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_MAX_LUMINANCE_EXT = $3349;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_SMPTE2086_MIN_LUMINANCE_EXT = $334A;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_METADATA_SCALING_EXT = 50000;   // api(egl) //extension: EGL_EXT_surface_SMPTE2086_metadata
    EGL_GENERATE_RESET_ON_VIDEO_MEMORY_PURGE_NV = $334C;   // api(egl) //extension: EGL_NV_robustness_video_memory_purge
    EGL_STREAM_CROSS_OBJECT_NV = $334D;   // api(egl) //extension: EGL_NV_stream_cross_object
    EGL_STREAM_CROSS_DISPLAY_NV = $334E;   // api(egl) //extension: EGL_NV_stream_cross_display
    EGL_STREAM_CROSS_SYSTEM_NV = $334F;   // api(egl) //extension: EGL_NV_stream_cross_system
    EGL_GL_COLORSPACE_SCRGB_LINEAR_EXT = $3350;   // api(egl) //extension: EGL_EXT_gl_colorspace_scrgb_linear
    EGL_GL_COLORSPACE_SCRGB_EXT = $3351;   // api(egl) //extension: EGL_EXT_gl_colorspace_scrgb
    EGL_TRACK_REFERENCES_KHR = $3352;   // api(egl) //extension: EGL_KHR_display_reference
    EGL_CONTEXT_PRIORITY_REALTIME_NV = $3357;   // api(egl) //extension: EGL_NV_context_priority_realtime
    EGL_RENDERER_EXT = $335F;   // api(egl) //extension: EGL_EXT_device_query_name
    EGL_CTA861_3_MAX_CONTENT_LIGHT_LEVEL_EXT = $3360;   // api(egl) //extension: EGL_EXT_surface_CTA861_3_metadata
    EGL_CTA861_3_MAX_FRAME_AVERAGE_LEVEL_EXT = $3361;   // api(egl) //extension: EGL_EXT_surface_CTA861_3_metadata
    EGL_GL_COLORSPACE_DISPLAY_P3_LINEAR_EXT = $3362;   // api(egl) //extension: EGL_EXT_gl_colorspace_display_p3_linear
    EGL_GL_COLORSPACE_DISPLAY_P3_EXT = $3363;   // api(egl) //extension: EGL_EXT_gl_colorspace_display_p3
    EGL_SYNC_CLIENT_EXT = $3364;   // api(egl) //extension: EGL_EXT_client_sync
    EGL_SYNC_CLIENT_SIGNAL_EXT = $3365;   // api(egl) //extension: EGL_EXT_client_sync
    EGL_STREAM_FRAME_ORIGIN_X_NV = $3366;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_STREAM_FRAME_ORIGIN_Y_NV = $3367;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_STREAM_FRAME_MAJOR_AXIS_NV = $3368;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_CONSUMER_AUTO_ORIENTATION_NV = $3369;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_PRODUCER_AUTO_ORIENTATION_NV = $336A;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_LEFT_NV = $336B;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_RIGHT_NV = $336C;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_TOP_NV = $336D;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_BOTTOM_NV = $336E;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_X_AXIS_NV = $336F;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_Y_AXIS_NV = $3370;   // api(egl) //extension: EGL_NV_stream_origin
    EGL_STREAM_DMA_NV = $3371;   // api(egl) //extension: EGL_NV_stream_dma
    EGL_STREAM_DMA_SERVER_NV = $3372;   // api(egl) //extension: EGL_NV_stream_dma
    EGL_STREAM_CONSUMER_IMAGE_NV = $3373;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
    EGL_STREAM_IMAGE_ADD_NV = $3374;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
    EGL_STREAM_IMAGE_REMOVE_NV = $3375;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
    EGL_STREAM_IMAGE_AVAILABLE_NV = $3376;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
    EGL_D3D9_DEVICE_ANGLE = $33A0;   // api(egl) //extension: EGL_ANGLE_device_d3d
    EGL_D3D11_DEVICE_ANGLE = $33A1;   // api(egl) //extension: EGL_ANGLE_device_d3d
    EGL_OBJECT_THREAD_KHR = $33B0;   // api(egl) //extension: EGL_KHR_debug
    EGL_OBJECT_DISPLAY_KHR = $33B1;   // api(egl) //extension: EGL_KHR_debug
    EGL_OBJECT_CONTEXT_KHR = $33B2;   // api(egl) //extension: EGL_KHR_debug
    EGL_OBJECT_SURFACE_KHR = $33B3;   // api(egl) //extension: EGL_KHR_debug
    EGL_OBJECT_IMAGE_KHR = $33B4;   // api(egl) //extension: EGL_KHR_debug
    EGL_OBJECT_SYNC_KHR = $33B5;   // api(egl) //extension: EGL_KHR_debug
    EGL_OBJECT_STREAM_KHR = $33B6;   // api(egl) //extension: EGL_KHR_debug
    EGL_DEBUG_CALLBACK_KHR = $33B8;   // api(egl) //extension: EGL_KHR_debug
    EGL_DEBUG_MSG_CRITICAL_KHR = $33B9;   // api(egl) //extension: EGL_KHR_debug
    EGL_DEBUG_MSG_ERROR_KHR = $33BA;   // api(egl) //extension: EGL_KHR_debug
    EGL_DEBUG_MSG_WARN_KHR = $33BB;   // api(egl) //extension: EGL_KHR_debug
    EGL_DEBUG_MSG_INFO_KHR = $33BC;   // api(egl) //extension: EGL_KHR_debug
    EGL_FORMAT_FLAG_UBWC_QCOM = $33E0;
    EGL_FORMAT_FLAG_MACROTILE_QCOM = $33E1;
    EGL_FORMAT_ASTC_4X4_QCOM = $33E2;
    EGL_FORMAT_ASTC_5X4_QCOM = $33E3;
    EGL_FORMAT_ASTC_5X5_QCOM = $33E4;
    EGL_FORMAT_ASTC_6X5_QCOM = $33E5;
    EGL_FORMAT_ASTC_6X6_QCOM = $33E6;
    EGL_FORMAT_ASTC_8X5_QCOM = $33E7;
    EGL_FORMAT_ASTC_8X6_QCOM = $33E8;
    EGL_FORMAT_ASTC_8X8_QCOM = $33E9;
    EGL_FORMAT_ASTC_10X5_QCOM = $33EA;
    EGL_FORMAT_ASTC_10X6_QCOM = $33EB;
    EGL_FORMAT_ASTC_10X8_QCOM = $33EC;
    EGL_FORMAT_ASTC_10X10_QCOM = $33ED;
    EGL_FORMAT_ASTC_12X10_QCOM = $33EE;
    EGL_FORMAT_ASTC_12X12_QCOM = $33EF;
    EGL_FORMAT_ASTC_4X4_SRGB_QCOM = $3400;
    EGL_FORMAT_ASTC_5X4_SRGB_QCOM = $3401;
    EGL_FORMAT_ASTC_5X5_SRGB_QCOM = $3402;
    EGL_FORMAT_ASTC_6X5_SRGB_QCOM = $3403;
    EGL_FORMAT_ASTC_6X6_SRGB_QCOM = $3404;
    EGL_FORMAT_ASTC_8X5_SRGB_QCOM = $3405;
    EGL_FORMAT_ASTC_8X6_SRGB_QCOM = $3406;
    EGL_FORMAT_ASTC_8X8_SRGB_QCOM = $3407;
    EGL_FORMAT_ASTC_10X5_SRGB_QCOM = $3408;
    EGL_FORMAT_ASTC_10X6_SRGB_QCOM = $3409;
    EGL_FORMAT_ASTC_10X8_SRGB_QCOM = $340A;
    EGL_FORMAT_ASTC_10X10_SRGB_QCOM = $340B;
    EGL_FORMAT_ASTC_12X10_SRGB_QCOM = $340C;
    EGL_FORMAT_ASTC_12X12_SRGB_QCOM = $340D;
    EGL_FORMAT_TP10_QCOM = $340E;
    EGL_FORMAT_NV12_Y_QCOM = $340F;
    EGL_FORMAT_NV12_UV_QCOM = $3410;
    EGL_FORMAT_NV21_VU_QCOM = $3411;
    EGL_FORMAT_NV12_4R_QCOM = $3412;
    EGL_FORMAT_NV12_4R_Y_QCOM = $3413;
    EGL_FORMAT_NV12_4R_UV_QCOM = $3414;
    EGL_FORMAT_P010_QCOM = $3415;
    EGL_FORMAT_P010_Y_QCOM = $3416;
    EGL_FORMAT_P010_UV_QCOM = $3417;
    EGL_FORMAT_TP10_Y_QCOM = $3418;
    EGL_FORMAT_TP10_UV_QCOM = $3419;
    EGL_GENERIC_TOKEN_1_QCOM = $3420;
    EGL_GENERIC_TOKEN_2_QCOM = $3421;
    EGL_GENERIC_TOKEN_3_QCOM = $3422;
    EGL_TIMESTAMP_PENDING_ANDROID : EGLnsecsANDROID = -2;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_TIMESTAMP_INVALID_ANDROID : EGLnsecsANDROID = -1;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_TIMESTAMPS_ANDROID = $3430;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_COMPOSITE_DEADLINE_ANDROID = $3431;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_COMPOSITE_INTERVAL_ANDROID = $3432;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_COMPOSITE_TO_PRESENT_LATENCY_ANDROID = $3433;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_REQUESTED_PRESENT_TIME_ANDROID = $3434;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_RENDERING_COMPLETE_TIME_ANDROID = $3435;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_COMPOSITION_LATCH_TIME_ANDROID = $3436;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_FIRST_COMPOSITION_START_TIME_ANDROID = $3437;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_LAST_COMPOSITION_START_TIME_ANDROID = $3438;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_FIRST_COMPOSITION_GPU_FINISHED_TIME_ANDROID = $3439;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_DISPLAY_PRESENT_TIME_ANDROID = $343A;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_DEQUEUE_READY_TIME_ANDROID = $343B;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_READS_DONE_TIME_ANDROID = $343C;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
    EGL_DMA_BUF_PLANE3_FD_EXT = $3440;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE3_OFFSET_EXT = $3441;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE3_PITCH_EXT = $3442;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE0_MODIFIER_LO_EXT = $3443;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE0_MODIFIER_HI_EXT = $3444;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE1_MODIFIER_LO_EXT = $3445;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE1_MODIFIER_HI_EXT = $3446;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE2_MODIFIER_LO_EXT = $3447;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE2_MODIFIER_HI_EXT = $3448;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE3_MODIFIER_LO_EXT = $3449;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_DMA_BUF_PLANE3_MODIFIER_HI_EXT = $344A;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
    EGL_PRIMARY_COMPOSITOR_CONTEXT_EXT = $3460;   // api(egl) //extension: EGL_EXT_compositor
    EGL_EXTERNAL_REF_ID_EXT = $3461;   // api(egl) //extension: EGL_EXT_compositor
    EGL_COMPOSITOR_DROP_NEWEST_FRAME_EXT = $3462;   // api(egl) //extension: EGL_EXT_compositor
    EGL_COMPOSITOR_KEEP_NEWEST_FRAME_EXT = $3463;   // api(egl) //extension: EGL_EXT_compositor
    EGL_FRONT_BUFFER_EXT = $3464;   // api(egl) //extension: EGL_EXT_bind_to_front
    EGL_IMPORT_SYNC_TYPE_EXT = $3470;   // api(egl) //extension: EGL_EXT_image_implicit_sync_control
    EGL_IMPORT_IMPLICIT_SYNC_EXT = $3471;   // api(egl) //extension: EGL_EXT_image_implicit_sync_control
    EGL_IMPORT_EXPLICIT_SYNC_EXT = $3472;   // api(egl) //extension: EGL_EXT_image_implicit_sync_control
    EGL_GL_COLORSPACE_DISPLAY_P3_PASSTHROUGH_EXT = $3490;   // api(egl) //extension: EGL_EXT_gl_colorspace_display_p3_passthrough
    EGL_COLOR_FORMAT_HI = $8F70;   // api(egl) //extension: EGL_HI_colorformats
    EGL_COLOR_RGB_HI = $8F71;   // api(egl) //extension: EGL_HI_colorformats
    EGL_COLOR_RGBA_HI = $8F72;   // api(egl) //extension: EGL_HI_colorformats
    EGL_COLOR_ARGB_HI = $8F73;   // api(egl) //extension: EGL_HI_colorformats
    EGL_CLIENT_PIXMAP_POINTER_HI = $8F74;   // api(egl) //extension: EGL_HI_clientpixmap
 
 
//============= OpenGL Extensions =========================== Total: 302
 var
 
  GL_AMD_compressed_3DC_texture: Boolean;   // api(gles1|gles2)
  GL_AMD_compressed_ATC_texture: Boolean;   // api(gles1|gles2)
  GL_AMD_framebuffer_multisample_advanced: Boolean;   // api(gl|glcore|gles2)
  GL_AMD_performance_monitor: Boolean;   // api(gl|glcore|gles2)
  GL_AMD_program_binary_Z400: Boolean;   // api(gles2)
  GL_ANDROID_extension_pack_es31a: Boolean;   // api(gles2)
  GL_ANGLE_depth_texture: Boolean;   // api(gles2)
  GL_ANGLE_framebuffer_blit: Boolean;   // api(gles2)
  GL_ANGLE_framebuffer_multisample: Boolean;   // api(gles2)
  GL_ANGLE_instanced_arrays: Boolean;   // api(gles2)
  GL_ANGLE_pack_reverse_row_order: Boolean;   // api(gles2)
  GL_ANGLE_program_binary: Boolean;   // api(gles2)
  GL_ANGLE_texture_compression_dxt3: Boolean;   // api(gles2)
  GL_ANGLE_texture_compression_dxt5: Boolean;   // api(gles2)
  GL_ANGLE_texture_usage: Boolean;   // api(gles2)
  GL_ANGLE_translated_shader_source: Boolean;   // api(gles2)
  GL_APPLE_clip_distance: Boolean;   // api(gles2)
  GL_APPLE_color_buffer_packed_float: Boolean;   // api(gles2)
  GL_APPLE_copy_texture_levels: Boolean;   // api(gles1|gles2)
  GL_APPLE_framebuffer_multisample: Boolean;   // api(gles1|gles2)
  GL_APPLE_rgb_422: Boolean;   // api(gl|glcore|gles2)
  GL_APPLE_sync: Boolean;   // api(gles1|gles2)
  GL_APPLE_texture_format_BGRA8888: Boolean;   // api(gles1|gles2)
  GL_APPLE_texture_max_level: Boolean;   // api(gles1|gles2)
  GL_APPLE_texture_packed_float: Boolean;   // api(gles2)
  GL_ARM_mali_program_binary: Boolean;   // api(gles2)
  GL_ARM_mali_shader_binary: Boolean;   // api(gles2)
  GL_ARM_rgba8: Boolean;   // api(gles1|gles2)
  GL_ARM_shader_framebuffer_fetch: Boolean;   // api(gles2)
  GL_ARM_shader_framebuffer_fetch_depth_stencil: Boolean;   // api(gles2)
  GL_ARM_texture_unnormalized_coordinates: Boolean;   // api(gles2)
  GL_DMP_program_binary: Boolean;   // api(gles2)
  GL_DMP_shader_binary: Boolean;   // api(gles2)
  GL_EXT_EGL_image_array: Boolean;   // api(gles2)
  GL_EXT_EGL_image_storage: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_YUV_target: Boolean;   // api(gles2)
  GL_EXT_base_instance: Boolean;   // api(gles2)
  GL_EXT_blend_func_extended: Boolean;   // api(gles2)
  GL_EXT_blend_minmax: Boolean;   // api(gl|gles1|gles2)
  GL_EXT_buffer_storage: Boolean;   // api(gles2)
  GL_EXT_clear_texture: Boolean;   // api(gles2)
  GL_EXT_clip_control: Boolean;   // api(gles2)
  GL_EXT_clip_cull_distance: Boolean;   // api(gles2)
  GL_EXT_color_buffer_float: Boolean;   // api(gles2)
  GL_EXT_color_buffer_half_float: Boolean;   // api(gles2)
  GL_EXT_conservative_depth: Boolean;   // api(gles2)
  GL_EXT_copy_image: Boolean;   // api(gles2)
  GL_EXT_debug_label: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_debug_marker: Boolean;   // api(gl|glcore|gles1|gles2)
  GL_EXT_depth_clamp: Boolean;   // api(gles2)
  GL_EXT_discard_framebuffer: Boolean;   // api(gles1|gles2)
  GL_EXT_disjoint_timer_query: Boolean;   // api(gles2)
  GL_EXT_draw_buffers: Boolean;   // api(gles2)
  GL_EXT_draw_buffers_indexed: Boolean;   // api(gles2)
  GL_EXT_draw_elements_base_vertex: Boolean;   // api(gles2)
  GL_EXT_draw_instanced: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_draw_transform_feedback: Boolean;   // api(gles2)
  GL_EXT_external_buffer: Boolean;   // api(gl|gles2)
  GL_EXT_float_blend: Boolean;   // api(gles2)
  GL_EXT_geometry_point_size: Boolean;   // api(gles2)
  GL_EXT_geometry_shader: Boolean;   // api(gles2)
  GL_EXT_gpu_shader5: Boolean;   // api(gles2)
  GL_EXT_instanced_arrays: Boolean;   // api(gles2)
  GL_EXT_map_buffer_range: Boolean;   // api(gles1|gles2)
  GL_EXT_memory_object: Boolean;   // api(gl|gles2)
  GL_EXT_memory_object_fd: Boolean;   // api(gl|gles2)
  GL_EXT_memory_object_win32: Boolean;   // api(gl|gles2)
  GL_EXT_multi_draw_arrays: Boolean;   // api(gl|gles1|gles2)
  GL_EXT_multi_draw_indirect: Boolean;   // api(gles2)
  GL_EXT_multisampled_compatibility: Boolean;   // api(gles2)
  GL_EXT_multisampled_render_to_texture: Boolean;   // api(gles1|gles2)
  GL_EXT_multisampled_render_to_texture2: Boolean;   // api(gles2)
  GL_EXT_multiview_draw_buffers: Boolean;   // api(gles2)
  GL_EXT_multiview_tessellation_geometry_shader: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_multiview_texture_multisample: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_multiview_timer_query: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_occlusion_query_boolean: Boolean;   // api(gles2)
  GL_EXT_polygon_offset_clamp: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_post_depth_coverage: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_primitive_bounding_box: Boolean;   // api(gles2)
  GL_EXT_protected_textures: Boolean;   // api(gles2)
  GL_EXT_pvrtc_sRGB: Boolean;   // api(gles2)
  GL_EXT_raster_multisample: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_read_format_bgra: Boolean;   // api(gles1|gles2)
  GL_EXT_render_snorm: Boolean;   // api(gles2)
  GL_EXT_robustness: Boolean;   // api(gles1|gles2)
  GL_EXT_semaphore: Boolean;   // api(gl|gles2)
  GL_EXT_semaphore_fd: Boolean;   // api(gl|gles2)
  GL_EXT_semaphore_win32: Boolean;   // api(gl|gles2)
  GL_EXT_sRGB: Boolean;   // api(gles1|gles2)
  GL_EXT_sRGB_write_control: Boolean;   // api(gles2)
  GL_EXT_separate_shader_objects: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_shader_framebuffer_fetch: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_shader_framebuffer_fetch_non_coherent: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_shader_group_vote: Boolean;   // api(gles2)
  GL_EXT_shader_implicit_conversions: Boolean;   // api(gles2)
  GL_EXT_shader_integer_mix: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_shader_io_blocks: Boolean;   // api(gles2)
  GL_EXT_shader_non_constant_global_initializers: Boolean;   // api(gles2)
  GL_EXT_shader_pixel_local_storage: Boolean;   // api(gles2)
  GL_EXT_shader_pixel_local_storage2: Boolean;   // api(gles2)
  GL_EXT_shader_texture_lod: Boolean;   // api(gles2)
  GL_EXT_shadow_samplers: Boolean;   // api(gles2)
  GL_EXT_sparse_texture: Boolean;   // api(gles2)
  GL_EXT_sparse_texture2: Boolean;   // api(gl|gles2)
  GL_EXT_tessellation_point_size: Boolean;   // api(gles2)
  GL_EXT_tessellation_shader: Boolean;   // api(gles2)
  GL_EXT_texture_border_clamp: Boolean;   // api(gles2)
  GL_EXT_texture_buffer: Boolean;   // api(gles2)
  GL_EXT_texture_compression_astc_decode_mode: Boolean;   // api(gles2)
  GL_EXT_texture_compression_bptc: Boolean;   // api(gles2)
  GL_EXT_texture_compression_dxt1: Boolean;   // api(gles1|gles2)
  GL_EXT_texture_compression_rgtc: Boolean;   // api(gl|gles2)
  GL_EXT_texture_compression_s3tc: Boolean;   // api(gl|glcore|gles2|glsc2)
  GL_EXT_texture_compression_s3tc_srgb: Boolean;   // api(gles2)
  GL_EXT_texture_cube_map_array: Boolean;   // api(gles2)
  GL_EXT_texture_filter_anisotropic: Boolean;   // api(gl|gles1|gles2)
  GL_EXT_texture_filter_minmax: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_texture_format_BGRA8888: Boolean;   // api(gles1|gles2)
  GL_EXT_texture_format_sRGB_override: Boolean;   // api(gles2)
  GL_EXT_texture_mirror_clamp_to_edge: Boolean;   // api(gles2)
  GL_EXT_texture_norm16: Boolean;   // api(gles2)
  GL_EXT_texture_query_lod: Boolean;   // api(gles2)
  GL_EXT_texture_rg: Boolean;   // api(gles2)
  GL_EXT_texture_sRGB_R8: Boolean;   // api(gles2|gl|glcore)
  GL_EXT_texture_sRGB_RG8: Boolean;   // api(gles2)
  GL_EXT_texture_sRGB_decode: Boolean;   // api(gl|glcore|gles2)
  GL_EXT_texture_storage: Boolean;   // api(gles1|gles2)
  GL_EXT_texture_type_2_10_10_10_REV: Boolean;   // api(gles2)
  GL_EXT_texture_view: Boolean;   // api(gles2)
  GL_NV_timeline_semaphore: Boolean;   // api(gl|gles2)
  GL_EXT_unpack_subimage: Boolean;   // api(gles2)
  GL_EXT_win32_keyed_mutex: Boolean;   // api(gl|gles2)
  GL_EXT_window_rectangles: Boolean;   // api(gl|glcore|gles2)
  GL_FJ_shader_binary_GCCSO: Boolean;   // api(gles2)
  GL_IMG_bindless_texture: Boolean;   // api(gles2)
  GL_IMG_framebuffer_downsample: Boolean;   // api(gles2)
  GL_IMG_multisampled_render_to_texture: Boolean;   // api(gles1|gles2)
  GL_IMG_program_binary: Boolean;   // api(gles2)
  GL_IMG_read_format: Boolean;   // api(gles1|gles2)
  GL_IMG_shader_binary: Boolean;   // api(gles2)
  GL_IMG_texture_compression_pvrtc: Boolean;   // api(gles1|gles2)
  GL_IMG_texture_compression_pvrtc2: Boolean;   // api(gles2)
  GL_IMG_texture_filter_cubic: Boolean;   // api(gles2)
  GL_INTEL_conservative_rasterization: Boolean;   // api(gl|glcore|gles2)
  GL_INTEL_framebuffer_CMAA: Boolean;   // api(gl|glcore|gles2)
  GL_INTEL_blackhole_render: Boolean;   // api(gl|glcore|gles2)
  GL_INTEL_performance_query: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_blend_equation_advanced: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_blend_equation_advanced_coherent: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_context_flush_control: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_debug: Boolean;   // api(gl|glcore|gles1|gles2)
  GL_KHR_no_error: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_robust_buffer_access_behavior: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_robustness: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_shader_subgroup: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_texture_compression_astc_hdr: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_texture_compression_astc_ldr: Boolean;   // api(gl|glcore|gles2) //->API is identical to GL_KHR_texture_compression_astc_hdr extension
  GL_KHR_texture_compression_astc_sliced_3d: Boolean;   // api(gl|glcore|gles2)
  GL_KHR_parallel_shader_compile: Boolean;   // api(gl|glcore|gles2)
  GL_MESA_framebuffer_flip_x: Boolean;   // api(gl|glcore|gles2)
  GL_MESA_framebuffer_flip_y: Boolean;   // api(gl|glcore|gles2)
  GL_MESA_framebuffer_swap_xy: Boolean;   // api(gl|glcore|gles2)
  GL_MESA_program_binary_formats: Boolean;   // api(gl|gles2)
  GL_MESA_shader_integer_functions: Boolean;   // api(gl|gles2)
  GL_NVX_blend_equation_advanced_multi_draw_buffers: Boolean;   // api(gl|gles2)
  GL_NV_bindless_texture: Boolean;   // api(gl|glcore|gles2)
  GL_NV_blend_equation_advanced: Boolean;   // api(gl|glcore|gles2)
  GL_NV_blend_equation_advanced_coherent: Boolean;   // api(gl|glcore|gles2)
  GL_NV_blend_minmax_factor: Boolean;   // api(gl|glcore|gles2)
  GL_NV_clip_space_w_scaling: Boolean;   // api(gl|glcore|gles2)
  GL_NV_compute_shader_derivatives: Boolean;   // api(gl|glcore|gles2)
  GL_NV_conditional_render: Boolean;   // api(gl|glcore|gles2)
  GL_NV_conservative_raster: Boolean;   // api(gl|glcore|gles2)
  GL_NV_conservative_raster_pre_snap: Boolean;   // api(gl|glcore|gles2)
  GL_NV_conservative_raster_pre_snap_triangles: Boolean;   // api(gl|glcore|gles2)
  GL_NV_copy_buffer: Boolean;   // api(gles2)
  GL_NV_coverage_sample: Boolean;   // api(gles2)
  GL_NV_depth_nonlinear: Boolean;   // api(gles2)
  GL_NV_draw_buffers: Boolean;   // api(gles2)
  GL_NV_draw_instanced: Boolean;   // api(gles2)
  GL_NV_draw_vulkan_image: Boolean;   // api(gl|glcore|gles2)
  GL_NV_explicit_attrib_location: Boolean;   // api(gles2)
  GL_NV_fbo_color_attachments: Boolean;   // api(gles2)
  GL_NV_fence: Boolean;   // api(gl|gles1|gles2)
  GL_NV_fill_rectangle: Boolean;   // api(gl|glcore|gles2)
  GL_NV_fragment_coverage_to_color: Boolean;   // api(gl|glcore|gles2)
  GL_NV_fragment_shader_barycentric: Boolean;   // api(gl|glcore|gles2)
  GL_NV_fragment_shader_interlock: Boolean;   // api(gl|glcore|gles2)
  GL_NV_framebuffer_blit: Boolean;   // api(gles2)
  GL_NV_framebuffer_mixed_samples: Boolean;   // api(gl|glcore|gles2)
  GL_NV_framebuffer_multisample: Boolean;   // api(gles2)
  GL_NV_generate_mipmap_sRGB: Boolean;   // api(gles2)
  GL_NV_geometry_shader_passthrough: Boolean;   // api(gl|glcore|gles2)
  GL_NV_gpu_shader5: Boolean;   // api(gl|glcore|gles2)
  GL_NV_image_formats: Boolean;   // api(gles2)
  GL_NV_instanced_arrays: Boolean;   // api(gles2)
  GL_NV_internalformat_sample_query: Boolean;   // api(gl|glcore|gles2)
  GL_NV_memory_attachment: Boolean;   // api(gl|glcore|gles2)
  GL_NV_memory_object_sparse: Boolean;   // api(gl|glcore|gles2)
  GL_NV_mesh_shader: Boolean;   // api(gl|glcore|gles2)
  GL_NV_non_square_matrices: Boolean;   // api(gles2)
  GL_NV_path_rendering: Boolean;   // api(gl|glcore|gles2)
  GL_NV_path_rendering_shared_edge: Boolean;   // api(gl|glcore|gles2)
  GL_NV_pixel_buffer_object: Boolean;   // api(gles2)
  GL_NV_polygon_mode: Boolean;   // api(gles2)
  GL_NV_read_buffer: Boolean;   // api(gles2)
  GL_NV_read_buffer_front: Boolean;   // api(gles2)
  GL_NV_read_depth: Boolean;   // api(gles2)
  GL_NV_read_depth_stencil: Boolean;   // api(gles2)
  GL_NV_read_stencil: Boolean;   // api(gles2)
  GL_NV_representative_fragment_test: Boolean;   // api(gl|glcore|gles2)
  GL_NV_sRGB_formats: Boolean;   // api(gles2)
  GL_NV_sample_locations: Boolean;   // api(gl|glcore|gles2)
  GL_NV_sample_mask_override_coverage: Boolean;   // api(gl|glcore|gles2)
  GL_NV_scissor_exclusive: Boolean;   // api(gl|glcore|gles2)
  GL_NV_shader_atomic_fp16_vector: Boolean;   // api(gl|glcore|gles2)
  GL_NV_shader_noperspective_interpolation: Boolean;   // api(gles2)
  GL_NV_shader_subgroup_partitioned: Boolean;   // api(gl|glcore|gles2)
  GL_NV_shader_texture_footprint: Boolean;   // api(gl|glcore|gles2)
  GL_NV_shading_rate_image: Boolean;   // api(gl|glcore|gles2)
  GL_NV_shadow_samplers_array: Boolean;   // api(gles2)
  GL_NV_shadow_samplers_cube: Boolean;   // api(gles2)
  GL_NV_stereo_view_rendering: Boolean;   // api(gl|glcore|gles2)
  GL_NV_texture_border_clamp: Boolean;   // api(gles2)
  GL_NV_texture_compression_s3tc_update: Boolean;   // api(gles2)
  GL_NV_texture_npot_2D_mipmap: Boolean;   // api(gles2)
  GL_NV_viewport_array: Boolean;   // api(gles2)
  GL_NV_viewport_array2: Boolean;   // api(gl|glcore|gles2)
  GL_NV_viewport_swizzle: Boolean;   // api(gl|glcore|gles2)
  GL_OES_EGL_image: Boolean;   // api(gles1|gles2)
  GL_OES_EGL_image_external: Boolean;   // api(gles1|gles2)
  GL_OES_EGL_image_external_essl3: Boolean;   // api(gles2)
  GL_OES_compressed_ETC1_RGB8_sub_texture: Boolean;   // api(gles1|gles2)
  GL_OES_compressed_ETC1_RGB8_texture: Boolean;   // api(gles1|gles2)
  GL_OES_compressed_paletted_texture: Boolean;   // api(gl|gles1|gles2)
  GL_OES_copy_image: Boolean;   // api(gles2)
  GL_OES_depth24: Boolean;   // api(gles1|gles2|glsc2)
  GL_OES_depth32: Boolean;   // api(gles1|gles2|glsc2)
  GL_OES_depth_texture: Boolean;   // api(gles2)
  GL_OES_draw_buffers_indexed: Boolean;   // api(gles2)
  GL_OES_draw_elements_base_vertex: Boolean;   // api(gles2)
  GL_OES_element_index_uint: Boolean;   // api(gles1|gles2)
  GL_OES_fbo_render_mipmap: Boolean;   // api(gles1|gles2)
  GL_OES_fragment_precision_high: Boolean;   // api(gles2)
  GL_OES_geometry_point_size: Boolean;   // api(gles2)
  GL_OES_geometry_shader: Boolean;   // api(gles2)
  GL_OES_get_program_binary: Boolean;   // api(gles2)
  GL_OES_gpu_shader5: Boolean;   // api(gles2)
  GL_OES_mapbuffer: Boolean;   // api(gles1|gles2)
  GL_OES_packed_depth_stencil: Boolean;   // api(gles1|gles2)
  GL_OES_primitive_bounding_box: Boolean;   // api(gles2)
  GL_OES_required_internalformat: Boolean;   // api(gles1|gles2)
  GL_OES_rgb8_rgba8: Boolean;   // api(gles1|gles2|glsc2)
  GL_OES_sample_shading: Boolean;   // api(gles2)
  GL_OES_sample_variables: Boolean;   // api(gles2)
  GL_OES_shader_image_atomic: Boolean;   // api(gles2)
  GL_OES_shader_io_blocks: Boolean;   // api(gles2)
  GL_OES_shader_multisample_interpolation: Boolean;   // api(gles2)
  GL_OES_standard_derivatives: Boolean;   // api(gles2|glsc2)
  GL_OES_stencil1: Boolean;   // api(gles1|gles2)
  GL_OES_stencil4: Boolean;   // api(gles1|gles2)
  GL_OES_surfaceless_context: Boolean;   // api(gles1|gles2)
  GL_OES_tessellation_point_size: Boolean;   // api(gles2)
  GL_OES_tessellation_shader: Boolean;   // api(gles2)
  GL_OES_texture_3D: Boolean;   // api(gles2)
  GL_OES_texture_border_clamp: Boolean;   // api(gles2)
  GL_OES_texture_buffer: Boolean;   // api(gles2)
  GL_OES_texture_compression_astc: Boolean;   // api(gles2) //->API is identical to GL_KHR_texture_compression_astc_hdr extension
  GL_OES_texture_cube_map_array: Boolean;   // api(gles2)
  GL_OES_texture_float: Boolean;   // api(gles2)
  GL_OES_texture_float_linear: Boolean;   // api(gles2)
  GL_OES_texture_half_float: Boolean;   // api(gles2)
  GL_OES_texture_half_float_linear: Boolean;   // api(gles2)
  GL_OES_texture_npot: Boolean;   // api(gles1|gles2)
  GL_OES_texture_stencil8: Boolean;   // api(gles2)
  GL_OES_texture_storage_multisample_2d_array: Boolean;   // api(gles2)
  GL_OES_texture_view: Boolean;   // api(gles2)
  GL_OES_vertex_array_object: Boolean;   // api(gles1|gles2)
  GL_OES_vertex_half_float: Boolean;   // api(gles2)
  GL_OES_vertex_type_10_10_10_2: Boolean;   // api(gles2)
  GL_OES_viewport_array: Boolean;   // api(gles2)
  GL_OVR_multiview: Boolean;   // api(gl|glcore|gles2)
  GL_OVR_multiview2: Boolean;   // api(gl|glcore|gles2)
  GL_OVR_multiview_multisampled_render_to_texture: Boolean;   // api(gles2)
  GL_QCOM_alpha_test: Boolean;   // api(gles2)
  GL_QCOM_binning_control: Boolean;   // api(gles2)
  GL_QCOM_driver_control: Boolean;   // api(gles1|gles2)
  GL_QCOM_extended_get: Boolean;   // api(gles1|gles2)
  GL_QCOM_extended_get2: Boolean;   // api(gles1|gles2)
  GL_QCOM_framebuffer_foveated: Boolean;   // api(gles2)
  GL_QCOM_motion_estimation: Boolean;   // api(gles2)
  GL_QCOM_texture_foveated: Boolean;   // api(gles2)
  GL_QCOM_texture_foveated_subsampled_layout: Boolean;   // api(gles2)
  GL_QCOM_perfmon_global_mode: Boolean;   // api(gles1|gles2)
  GL_QCOM_shader_framebuffer_fetch_noncoherent: Boolean;   // api(gles2)
  GL_QCOM_shader_framebuffer_fetch_rate: Boolean;   // api(gles2)
  GL_QCOM_shading_rate: Boolean;   // api(gles2)
  GL_QCOM_tiled_rendering: Boolean;   // api(gles1|gles2)
  GL_QCOM_writeonly_rendering: Boolean;   // api(gles1|gles2)
  GL_QCOM_YUV_texture_gather: Boolean;   // api(gles2)
  GL_VIV_shader_binary: Boolean;   // api(gles2)
  GL_EXT_texture_shadow_lod: Boolean;   // api(gl|glcore|gles2)
 
  //---- EGL Extensions ---------- Total: 150
  EGL_ANDROID_blob_cache: Boolean;   // api(egl)
  EGL_ANDROID_create_native_client_buffer: Boolean;   // api(egl)
  EGL_ANDROID_framebuffer_target: Boolean;   // api(egl)
  EGL_ANDROID_get_native_client_buffer: Boolean;   // api(egl)
  EGL_ANDROID_front_buffer_auto_refresh: Boolean;   // api(egl)
  EGL_ANDROID_image_native_buffer: Boolean;   // api(egl)
  EGL_ANDROID_native_fence_sync: Boolean;   // api(egl)
  EGL_ANDROID_presentation_time: Boolean;   // api(egl)
  EGL_ANDROID_get_frame_timestamps: Boolean;   // api(egl)
  EGL_ANDROID_recordable: Boolean;   // api(egl)
  EGL_ANDROID_GLES_layers: Boolean;   // api(egl)
  EGL_ANGLE_d3d_share_handle_client_buffer: Boolean;   // api(egl)
  EGL_ANGLE_device_d3d: Boolean;   // api(egl)
  EGL_ANGLE_query_surface_pointer: Boolean;   // api(egl)
  EGL_ANGLE_surface_d3d_texture_2d_share_handle: Boolean;   // api(egl)
  EGL_ANGLE_window_fixed_size: Boolean;   // api(egl)
  EGL_ARM_implicit_external_sync: Boolean;   // api(egl)
  EGL_ARM_pixmap_multisample_discard: Boolean;   // api(egl)
  EGL_EXT_buffer_age: Boolean;   // api(egl)
  EGL_EXT_client_extensions: Boolean;   // api(egl)
  EGL_EXT_client_sync: Boolean;   // api(egl)
  EGL_EXT_create_context_robustness: Boolean;   // api(egl)
  EGL_EXT_device_base: Boolean;   // api(egl)
  EGL_EXT_device_drm: Boolean;   // api(egl)
  EGL_EXT_device_enumeration: Boolean;   // api(egl)
  EGL_EXT_device_openwf: Boolean;   // api(egl)
  EGL_EXT_device_query: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_bt2020_linear: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_bt2020_pq: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_scrgb: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_scrgb_linear: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_display_p3_linear: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_display_p3: Boolean;   // api(egl)
  EGL_EXT_gl_colorspace_display_p3_passthrough: Boolean;   // api(egl)
  EGL_EXT_image_dma_buf_import: Boolean;   // api(egl)
  EGL_EXT_image_dma_buf_import_modifiers: Boolean;   // api(egl)
  EGL_EXT_image_gl_colorspace: Boolean;   // api(egl)
  EGL_EXT_multiview_window: Boolean;   // api(egl)
  EGL_EXT_output_base: Boolean;   // api(egl)
  EGL_EXT_output_drm: Boolean;   // api(egl)
  EGL_EXT_output_openwf: Boolean;   // api(egl)
  EGL_EXT_pixel_format_float: Boolean;   // api(egl)
  EGL_EXT_platform_base: Boolean;   // api(egl)
  EGL_EXT_platform_device: Boolean;   // api(egl)
  EGL_EXT_platform_wayland: Boolean;   // api(egl)
  EGL_EXT_platform_x11: Boolean;   // api(egl)
  EGL_EXT_protected_content: Boolean;   // api(egl)
  EGL_EXT_protected_surface: Boolean;   // api(egl)
  EGL_EXT_stream_consumer_egloutput: Boolean;   // api(egl)
  EGL_EXT_surface_SMPTE2086_metadata: Boolean;   // api(egl)
  EGL_EXT_swap_buffers_with_damage: Boolean;   // api(egl)
  EGL_EXT_sync_reuse: Boolean;   // api(egl)
  EGL_EXT_yuv_surface: Boolean;   // api(egl)
  EGL_HI_clientpixmap: Boolean;   // api(egl)
  EGL_HI_colorformats: Boolean;   // api(egl)
  EGL_IMG_context_priority: Boolean;   // api(egl)
  EGL_IMG_image_plane_attribs: Boolean;   // api(egl)
  EGL_KHR_cl_event: Boolean;   // api(egl)
  EGL_KHR_cl_event2: Boolean;   // api(egl)
  EGL_KHR_config_attribs: Boolean;   // api(egl)
  EGL_KHR_client_get_all_proc_addresses: Boolean;   // api(egl) //->Alias of EGL_KHR_get_all_proc_addresses
  EGL_KHR_context_flush_control: Boolean;   // api(egl)
  EGL_KHR_create_context: Boolean;   // api(egl)
  EGL_KHR_create_context_no_error: Boolean;   // api(egl)
  EGL_KHR_debug: Boolean;   // api(egl)
  EGL_KHR_display_reference: Boolean;   // api(egl)
  EGL_KHR_fence_sync: Boolean;   // api(egl)
  EGL_KHR_get_all_proc_addresses: Boolean;   // api(egl)
  EGL_KHR_gl_colorspace: Boolean;   // api(egl)
  EGL_KHR_gl_renderbuffer_image: Boolean;   // api(egl)
  EGL_KHR_gl_texture_2D_image: Boolean;   // api(egl)
  EGL_KHR_gl_texture_3D_image: Boolean;   // api(egl)
  EGL_KHR_gl_texture_cubemap_image: Boolean;   // api(egl)
  EGL_KHR_image: Boolean;   // api(egl)
  EGL_KHR_image_base: Boolean;   // api(egl)
  EGL_KHR_image_pixmap: Boolean;   // api(egl)
  EGL_KHR_lock_surface: Boolean;   // api(egl)
  EGL_KHR_lock_surface2: Boolean;   // api(egl)
  EGL_KHR_lock_surface3: Boolean;   // api(egl)
  EGL_KHR_mutable_render_buffer: Boolean;   // api(egl)
  EGL_KHR_no_config_context: Boolean;   // api(egl)
  EGL_KHR_partial_update: Boolean;   // api(egl)
  EGL_KHR_platform_android: Boolean;   // api(egl)
  EGL_KHR_platform_gbm: Boolean;   // api(egl)
  EGL_KHR_platform_wayland: Boolean;   // api(egl)
  EGL_KHR_platform_x11: Boolean;   // api(egl)
  EGL_KHR_reusable_sync: Boolean;   // api(egl)
  EGL_KHR_stream: Boolean;   // api(egl)
  EGL_KHR_stream_attrib: Boolean;   // api(egl)
  EGL_KHR_stream_consumer_gltexture: Boolean;   // api(egl)
  EGL_KHR_stream_cross_process_fd: Boolean;   // api(egl)
  EGL_KHR_stream_fifo: Boolean;   // api(egl)
  EGL_KHR_stream_producer_aldatalocator: Boolean;   // api(egl)
  EGL_KHR_stream_producer_eglsurface: Boolean;   // api(egl)
  EGL_KHR_surfaceless_context: Boolean;   // api(egl) //->Just relaxes an error condition
  EGL_KHR_swap_buffers_with_damage: Boolean;   // api(egl)
  EGL_KHR_vg_parent_image: Boolean;   // api(egl)
  EGL_KHR_wait_sync: Boolean;   // api(egl)
  EGL_MESA_drm_image: Boolean;   // api(egl)
  EGL_MESA_image_dma_buf_export: Boolean;   // api(egl)
  EGL_MESA_platform_gbm: Boolean;   // api(egl)
  EGL_MESA_platform_surfaceless: Boolean;   // api(egl)
  EGL_MESA_query_driver: Boolean;   // api(egl)
  EGL_NOK_swap_region: Boolean;   // api(egl)
  EGL_NOK_swap_region2: Boolean;   // api(egl)
  EGL_NOK_texture_from_pixmap: Boolean;   // api(egl)
  EGL_NV_3dvision_surface: Boolean;   // api(egl)
  EGL_NV_coverage_sample: Boolean;   // api(egl)
  EGL_NV_context_priority_realtime: Boolean;   // api(egl)
  EGL_NV_coverage_sample_resolve: Boolean;   // api(egl)
  EGL_NV_cuda_event: Boolean;   // api(egl)
  EGL_NV_depth_nonlinear: Boolean;   // api(egl)
  EGL_NV_device_cuda: Boolean;   // api(egl)
  EGL_NV_native_query: Boolean;   // api(egl)
  EGL_NV_post_convert_rounding: Boolean;   // api(egl)
  EGL_NV_post_sub_buffer: Boolean;   // api(egl)
  EGL_NV_quadruple_buffer: Boolean;   // api(egl)
  EGL_NV_robustness_video_memory_purge: Boolean;   // api(egl)
  EGL_NV_stream_consumer_gltexture_yuv: Boolean;   // api(egl)
  EGL_NV_stream_cross_object: Boolean;   // api(egl)
  EGL_NV_stream_cross_display: Boolean;   // api(egl)
  EGL_NV_stream_cross_partition: Boolean;   // api(egl)
  EGL_NV_stream_cross_process: Boolean;   // api(egl)
  EGL_NV_stream_cross_system: Boolean;   // api(egl)
  EGL_NV_stream_dma: Boolean;   // api(egl)
  EGL_NV_stream_consumer_eglimage: Boolean;   // api(egl)
  EGL_NV_stream_fifo_next: Boolean;   // api(egl)
  EGL_NV_stream_fifo_synchronous: Boolean;   // api(egl)
  EGL_NV_stream_flush: Boolean;   // api(egl)
  EGL_NV_stream_frame_limits: Boolean;   // api(egl)
  EGL_NV_stream_metadata: Boolean;   // api(egl)
  EGL_NV_stream_reset: Boolean;   // api(egl)
  EGL_NV_stream_remote: Boolean;   // api(egl)
  EGL_NV_stream_socket: Boolean;   // api(egl)
  EGL_NV_stream_socket_inet: Boolean;   // api(egl)
  EGL_NV_stream_socket_unix: Boolean;   // api(egl)
  EGL_NV_stream_sync: Boolean;   // api(egl)
  EGL_NV_sync: Boolean;   // api(egl)
  EGL_NV_system_time: Boolean;   // api(egl)
  EGL_NV_triple_buffer: Boolean;   // api(egl)
  EGL_TIZEN_image_native_buffer: Boolean;   // api(egl)
  EGL_TIZEN_image_native_surface: Boolean;   // api(egl)
  EGL_EXT_compositor: Boolean;   // api(egl)
  EGL_EXT_surface_CTA861_3_metadata: Boolean;   // api(egl)
  EGL_EXT_image_implicit_sync_control: Boolean;   // api(egl)
  EGL_EXT_bind_to_front: Boolean;   // api(egl)
  EGL_NV_stream_origin: Boolean;   // api(egl)
  EGL_WL_bind_wayland_display: Boolean;   // api(egl)
  EGL_WL_create_wayland_buffer_from_image: Boolean;   // api(egl)
  EGL_ARM_image_format: Boolean;   // api(egl)
  EGL_EXT_device_query_name: Boolean;   // api(egl)
 
 
 
//===================== Commands Types ===========================================
 
type
 
 
//------- GL_ES_VERSION_2_0----------------
  TglActiveTexture = procedure(texture: GLenum); extdecl;
  TglAttachShader = procedure(program_: GLuint; shader: GLuint); extdecl;
  TglBindAttribLocation = procedure(program_: GLuint; index_: GLuint; name_: PGLchar); extdecl;
  TglBindBuffer = procedure(target: GLenum; buffer: GLuint); extdecl;
  TglBindFramebuffer = procedure(target: GLenum; framebuffer: GLuint); extdecl;
  TglBindRenderbuffer = procedure(target: GLenum; renderbuffer: GLuint); extdecl;
  TglBindTexture = procedure(target: GLenum; texture: GLuint); extdecl;
  TglBlendColor = procedure(red: GLfloat; green: GLfloat; blue: GLfloat; alpha: GLfloat); extdecl;
  TglBlendEquation = procedure(mode: GLenum); extdecl;
  TglBlendEquationSeparate = procedure(modeRGB: GLenum; modeAlpha: GLenum); extdecl;
  TglBlendFunc = procedure(sfactor: GLenum; dfactor: GLenum); extdecl;
  TglBlendFuncSeparate = procedure(sfactorRGB: GLenum; dfactorRGB: GLenum; sfactorAlpha: GLenum; dfactorAlpha: GLenum); extdecl;
  TglBufferData = procedure(target: GLenum; size: GLsizeiptr; data: Pvoid; usage: GLenum); extdecl;
  TglBufferSubData = procedure(target: GLenum; offset: GLintptr; size: GLsizeiptr; data: Pvoid); extdecl;
  TglCheckFramebufferStatus = function(target: GLenum): GLenum; extdecl;
  TglClear = procedure(mask: GLbitfield); extdecl;
  TglClearColor = procedure(red: GLfloat; green: GLfloat; blue: GLfloat; alpha: GLfloat); extdecl;
  TglClearDepthf = procedure(d: GLfloat); extdecl;
  TglClearStencil = procedure(s: GLint); extdecl;
  TglColorMask = procedure(red: GLboolean; green: GLboolean; blue: GLboolean; alpha: GLboolean); extdecl;
  TglCompileShader = procedure(shader: GLuint); extdecl;
  TglCompressedTexImage2D = procedure(target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; border: GLint; imageSize: GLsizei; data: Pvoid); extdecl;
  TglCompressedTexSubImage2D = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; width: GLsizei; height: GLsizei; format: GLenum; imageSize: GLsizei; data: Pvoid); extdecl;
  TglCopyTexImage2D = procedure(target: GLenum; level: GLint; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei; border: GLint); extdecl;
  TglCopyTexSubImage2D = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglCreateProgram = function: GLuint; extdecl;
  TglCreateShader = function(type_: GLenum): GLuint; extdecl;
  TglCullFace = procedure(mode: GLenum); extdecl;
  TglDeleteBuffers = procedure(n: GLsizei; buffers: PGLuint); extdecl;
  TglDeleteFramebuffers = procedure(n: GLsizei; framebuffers: PGLuint); extdecl;
  TglDeleteProgram = procedure(program_: GLuint); extdecl;
  TglDeleteRenderbuffers = procedure(n: GLsizei; renderbuffers: PGLuint); extdecl;
  TglDeleteShader = procedure(shader: GLuint); extdecl;
  TglDeleteTextures = procedure(n: GLsizei; textures: PGLuint); extdecl;
  TglDepthFunc = procedure(func: GLenum); extdecl;
  TglDepthMask = procedure(flag: GLboolean); extdecl;
  TglDepthRangef = procedure(n: GLfloat; f: GLfloat); extdecl;
  TglDetachShader = procedure(program_: GLuint; shader: GLuint); extdecl;
  TglDisable = procedure(cap: GLenum); extdecl;
  TglDisableVertexAttribArray = procedure(index_: GLuint); extdecl;
  TglDrawArrays = procedure(mode: GLenum; first: GLint; count: GLsizei); extdecl;
  TglDrawElements = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid); extdecl;
  TglEnable = procedure(cap: GLenum); extdecl;
  TglEnableVertexAttribArray = procedure(index_: GLuint); extdecl;
  TglFinish = procedure; extdecl;
  TglFlush = procedure; extdecl;
  TglFramebufferRenderbuffer = procedure(target: GLenum; attachment: GLenum; renderbuffertarget: GLenum; renderbuffer: GLuint); extdecl;
  TglFramebufferTexture2D = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint); extdecl;
  TglFrontFace = procedure(mode: GLenum); extdecl;
  TglGenBuffers = procedure(n: GLsizei; buffers: PGLuint); extdecl;
  TglGenerateMipmap = procedure(target: GLenum); extdecl;
  TglGenFramebuffers = procedure(n: GLsizei; framebuffers: PGLuint); extdecl;
  TglGenRenderbuffers = procedure(n: GLsizei; renderbuffers: PGLuint); extdecl;
  TglGenTextures = procedure(n: GLsizei; textures: PGLuint); extdecl;
  TglGetActiveAttrib = procedure(program_: GLuint; index_: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLint; type_: PGLenum; name_: PGLchar); extdecl;
  TglGetActiveUniform = procedure(program_: GLuint; index_: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLint; type_: PGLenum; name_: PGLchar); extdecl;
  TglGetAttachedShaders = procedure(program_: GLuint; maxCount: GLsizei; count: PGLsizei; shaders: PGLuint); extdecl;
  TglGetAttribLocation = function(program_: GLuint; name_: PGLchar): GLint; extdecl;
  TglGetBooleanv = procedure(pname: GLenum; data: PGLboolean); extdecl;
  TglGetBufferParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetError = function: GLenum; extdecl;
  TglGetFloatv = procedure(pname: GLenum; data: PGLfloat); extdecl;
  TglGetFramebufferAttachmentParameteriv = procedure(target: GLenum; attachment: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetIntegerv = procedure(pname: GLenum; data: PGLint); extdecl;
  TglGetProgramiv = procedure(program_: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetProgramInfoLog = procedure(program_: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PGLchar); extdecl;
  TglGetRenderbufferParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetShaderiv = procedure(shader: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetShaderInfoLog = procedure(shader: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PGLchar); extdecl;
  TglGetShaderPrecisionFormat = procedure(shadertype: GLenum; precisiontype: GLenum; range: PGLint; precision: PGLint); extdecl;
  TglGetShaderSource = procedure(shader: GLuint; bufSize: GLsizei; length: PGLsizei; source: PGLchar); extdecl;
  TglGetString = function(name_: GLenum): PGLubyte; extdecl;
  TglGetTexParameterfv = procedure(target: GLenum; pname: GLenum; params: PGLfloat); extdecl;
  TglGetTexParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetUniformfv = procedure(program_: GLuint; location: GLint; params: PGLfloat); extdecl;
  TglGetUniformiv = procedure(program_: GLuint; location: GLint; params: PGLint); extdecl;
  TglGetUniformLocation = function(program_: GLuint; name_: PGLchar): GLint; extdecl;
  TglGetVertexAttribfv = procedure(index_: GLuint; pname: GLenum; params: PGLfloat); extdecl;
  TglGetVertexAttribiv = procedure(index_: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetVertexAttribPointerv = procedure(index_: GLuint; pname: GLenum; pointer: PPvoid); extdecl;
  TglHint = procedure(target: GLenum; mode: GLenum); extdecl;
  TglIsBuffer = function(buffer: GLuint): GLboolean; extdecl;
  TglIsEnabled = function(cap: GLenum): GLboolean; extdecl;
  TglIsFramebuffer = function(framebuffer: GLuint): GLboolean; extdecl;
  TglIsProgram = function(program_: GLuint): GLboolean; extdecl;
  TglIsRenderbuffer = function(renderbuffer: GLuint): GLboolean; extdecl;
  TglIsShader = function(shader: GLuint): GLboolean; extdecl;
  TglIsTexture = function(texture: GLuint): GLboolean; extdecl;
  TglLineWidth = procedure(width: GLfloat); extdecl;
  TglLinkProgram = procedure(program_: GLuint); extdecl;
  TglPixelStorei = procedure(pname: GLenum; param: GLint); extdecl;
  TglPolygonOffset = procedure(factor: GLfloat; units: GLfloat); extdecl;
  TglReadPixels = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglReleaseShaderCompiler = procedure; extdecl;
  TglRenderbufferStorage = procedure(target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglSampleCoverage = procedure(value: GLfloat; invert: GLboolean); extdecl;
  TglScissor = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglShaderBinary = procedure(count: GLsizei; shaders: PGLuint; binaryFormat: GLenum; binary: Pvoid; length: GLsizei); extdecl;
  TglShaderSource = procedure(shader: GLuint; count: GLsizei; string_: PPGLchar; length: PGLint); extdecl;
  TglStencilFunc = procedure(func: GLenum; ref: GLint; mask: GLuint); extdecl;
  TglStencilFuncSeparate = procedure(face: GLenum; func: GLenum; ref: GLint; mask: GLuint); extdecl;
  TglStencilMask = procedure(mask: GLuint); extdecl;
  TglStencilMaskSeparate = procedure(face: GLenum; mask: GLuint); extdecl;
  TglStencilOp = procedure(fail: GLenum; zfail: GLenum; zpass: GLenum); extdecl;
  TglStencilOpSeparate = procedure(face: GLenum; sfail: GLenum; dpfail: GLenum; dppass: GLenum); extdecl;
  TglTexImage2D = procedure(target: GLenum; level: GLint; internalformat: GLint; width: GLsizei; height: GLsizei; border: GLint; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglTexParameterf = procedure(target: GLenum; pname: GLenum; param: GLfloat); extdecl;
  TglTexParameterfv = procedure(target: GLenum; pname: GLenum; params: PGLfloat); extdecl;
  TglTexParameteri = procedure(target: GLenum; pname: GLenum; param: GLint); extdecl;
  TglTexParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglTexSubImage2D = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglUniform1f = procedure(location: GLint; v0: GLfloat); extdecl;
  TglUniform1fv = procedure(location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglUniform1i = procedure(location: GLint; v0: GLint); extdecl;
  TglUniform1iv = procedure(location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglUniform2f = procedure(location: GLint; v0: GLfloat; v1: GLfloat); extdecl;
  TglUniform2fv = procedure(location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglUniform2i = procedure(location: GLint; v0: GLint; v1: GLint); extdecl;
  TglUniform2iv = procedure(location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglUniform3f = procedure(location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat); extdecl;
  TglUniform3fv = procedure(location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglUniform3i = procedure(location: GLint; v0: GLint; v1: GLint; v2: GLint); extdecl;
  TglUniform3iv = procedure(location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglUniform4f = procedure(location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat; v3: GLfloat); extdecl;
  TglUniform4fv = procedure(location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglUniform4i = procedure(location: GLint; v0: GLint; v1: GLint; v2: GLint; v3: GLint); extdecl;
  TglUniform4iv = procedure(location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglUniformMatrix2fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix3fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix4fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUseProgram = procedure(program_: GLuint); extdecl;
  TglValidateProgram = procedure(program_: GLuint); extdecl;
  TglVertexAttrib1f = procedure(index_: GLuint; x: GLfloat); extdecl;
  TglVertexAttrib1fv = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglVertexAttrib2f = procedure(index_: GLuint; x: GLfloat; y: GLfloat); extdecl;
  TglVertexAttrib2fv = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglVertexAttrib3f = procedure(index_: GLuint; x: GLfloat; y: GLfloat; z: GLfloat); extdecl;
  TglVertexAttrib3fv = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglVertexAttrib4f = procedure(index_: GLuint; x: GLfloat; y: GLfloat; z: GLfloat; w: GLfloat); extdecl;
  TglVertexAttrib4fv = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglVertexAttribPointer = procedure(index_: GLuint; size: GLint; type_: GLenum; normalized: GLboolean; stride: GLsizei; pointer: Pvoid); extdecl;
  TglViewport = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
 
//------- GL_ES_VERSION_3_0----------------
  TglReadBuffer = procedure(src: GLenum); extdecl;
  TglDrawRangeElements = procedure(mode: GLenum; start: GLuint; end_: GLuint; count: GLsizei; type_: GLenum; indices: Pvoid); extdecl;
  TglTexImage3D = procedure(target: GLenum; level: GLint; internalformat: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglTexSubImage3D = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglCopyTexSubImage3D = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglCompressedTexImage3D = procedure(target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; imageSize: GLsizei; data: Pvoid); extdecl;
  TglCompressedTexSubImage3D = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; imageSize: GLsizei; data: Pvoid); extdecl;
  TglGenQueries = procedure(n: GLsizei; ids: PGLuint); extdecl;
  TglDeleteQueries = procedure(n: GLsizei; ids: PGLuint); extdecl;
  TglIsQuery = function(id: GLuint): GLboolean; extdecl;
  TglBeginQuery = procedure(target: GLenum; id: GLuint); extdecl;
  TglEndQuery = procedure(target: GLenum); extdecl;
  TglGetQueryiv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetQueryObjectuiv = procedure(id: GLuint; pname: GLenum; params: PGLuint); extdecl;
  TglUnmapBuffer = function(target: GLenum): GLboolean; extdecl;
  TglGetBufferPointerv = procedure(target: GLenum; pname: GLenum; params: PPvoid); extdecl;
  TglDrawBuffers = procedure(n: GLsizei; bufs: PGLenum); extdecl;
  TglUniformMatrix2x3fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix3x2fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix2x4fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix4x2fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix3x4fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix4x3fv = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglBlitFramebuffer = procedure(srcX0: GLint; srcY0: GLint; srcX1: GLint; srcY1: GLint; dstX0: GLint; dstY0: GLint; dstX1: GLint; dstY1: GLint; mask: GLbitfield; filter: GLenum); extdecl;
  TglRenderbufferStorageMultisample = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglFramebufferTextureLayer = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint; layer: GLint); extdecl;
  TglMapBufferRange = function(target: GLenum; offset: GLintptr; length: GLsizeiptr; access: GLbitfield): Pvoid; extdecl;
  TglFlushMappedBufferRange = procedure(target: GLenum; offset: GLintptr; length: GLsizeiptr); extdecl;
  TglBindVertexArray = procedure(array_: GLuint); extdecl;
  TglDeleteVertexArrays = procedure(n: GLsizei; arrays: PGLuint); extdecl;
  TglGenVertexArrays = procedure(n: GLsizei; arrays: PGLuint); extdecl;
  TglIsVertexArray = function(array_: GLuint): GLboolean; extdecl;
  TglGetIntegeri_v = procedure(target: GLenum; index_: GLuint; data: PGLint); extdecl;
  TglBeginTransformFeedback = procedure(primitiveMode: GLenum); extdecl;
  TglEndTransformFeedback = procedure; extdecl;
  TglBindBufferRange = procedure(target: GLenum; index_: GLuint; buffer: GLuint; offset: GLintptr; size: GLsizeiptr); extdecl;
  TglBindBufferBase = procedure(target: GLenum; index_: GLuint; buffer: GLuint); extdecl;
  TglTransformFeedbackVaryings = procedure(program_: GLuint; count: GLsizei; varyings: PPGLchar; bufferMode: GLenum); extdecl;
  TglGetTransformFeedbackVarying = procedure(program_: GLuint; index_: GLuint; bufSize: GLsizei; length: PGLsizei; size: PGLsizei; type_: PGLenum; name_: PGLchar); extdecl;
  TglVertexAttribIPointer = procedure(index_: GLuint; size: GLint; type_: GLenum; stride: GLsizei; pointer: Pvoid); extdecl;
  TglGetVertexAttribIiv = procedure(index_: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetVertexAttribIuiv = procedure(index_: GLuint; pname: GLenum; params: PGLuint); extdecl;
  TglVertexAttribI4i = procedure(index_: GLuint; x: GLint; y: GLint; z: GLint; w: GLint); extdecl;
  TglVertexAttribI4ui = procedure(index_: GLuint; x: GLuint; y: GLuint; z: GLuint; w: GLuint); extdecl;
  TglVertexAttribI4iv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglVertexAttribI4uiv = procedure(index_: GLuint; v: PGLuint); extdecl;
  TglGetUniformuiv = procedure(program_: GLuint; location: GLint; params: PGLuint); extdecl;
  TglGetFragDataLocation = function(program_: GLuint; name_: PGLchar): GLint; extdecl;
  TglUniform1ui = procedure(location: GLint; v0: GLuint); extdecl;
  TglUniform2ui = procedure(location: GLint; v0: GLuint; v1: GLuint); extdecl;
  TglUniform3ui = procedure(location: GLint; v0: GLuint; v1: GLuint; v2: GLuint); extdecl;
  TglUniform4ui = procedure(location: GLint; v0: GLuint; v1: GLuint; v2: GLuint; v3: GLuint); extdecl;
  TglUniform1uiv = procedure(location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglUniform2uiv = procedure(location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglUniform3uiv = procedure(location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglUniform4uiv = procedure(location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglClearBufferiv = procedure(buffer: GLenum; drawbuffer: GLint; value: PGLint); extdecl;
  TglClearBufferuiv = procedure(buffer: GLenum; drawbuffer: GLint; value: PGLuint); extdecl;
  TglClearBufferfv = procedure(buffer: GLenum; drawbuffer: GLint; value: PGLfloat); extdecl;
  TglClearBufferfi = procedure(buffer: GLenum; drawbuffer: GLint; depth: GLfloat; stencil: GLint); extdecl;
  TglGetStringi = function(name_: GLenum; index_: GLuint): PGLubyte; extdecl;
  TglCopyBufferSubData = procedure(readTarget: GLenum; writeTarget: GLenum; readOffset: GLintptr; writeOffset: GLintptr; size: GLsizeiptr); extdecl;
  TglGetUniformIndices = procedure(program_: GLuint; uniformCount: GLsizei; uniformNames: PPGLchar; uniformIndices: PGLuint); extdecl;
  TglGetActiveUniformsiv = procedure(program_: GLuint; uniformCount: GLsizei; uniformIndices: PGLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetUniformBlockIndex = function(program_: GLuint; uniformBlockName: PGLchar): GLuint; extdecl;
  TglGetActiveUniformBlockiv = procedure(program_: GLuint; uniformBlockIndex: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetActiveUniformBlockName = procedure(program_: GLuint; uniformBlockIndex: GLuint; bufSize: GLsizei; length: PGLsizei; uniformBlockName: PGLchar); extdecl;
  TglUniformBlockBinding = procedure(program_: GLuint; uniformBlockIndex: GLuint; uniformBlockBinding: GLuint); extdecl;
  TglDrawArraysInstanced = procedure(mode: GLenum; first: GLint; count: GLsizei; instancecount: GLsizei); extdecl;
  TglDrawElementsInstanced = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei); extdecl;
  TglFenceSync = function(condition: GLenum; flags: GLbitfield): GLsync; extdecl;
  TglIsSync = function(sync: GLsync): GLboolean; extdecl;
  TglDeleteSync = procedure(sync: GLsync); extdecl;
  TglClientWaitSync = function(sync: GLsync; flags: GLbitfield; timeout: GLuint64): GLenum; extdecl;
  TglWaitSync = procedure(sync: GLsync; flags: GLbitfield; timeout: GLuint64); extdecl;
  TglGetInteger64v = procedure(pname: GLenum; data: PGLint64); extdecl;
  TglGetSynciv = procedure(sync: GLsync; pname: GLenum; count: GLsizei; length: PGLsizei; values: PGLint); extdecl;
  TglGetInteger64i_v = procedure(target: GLenum; index_: GLuint; data: PGLint64); extdecl;
  TglGetBufferParameteri64v = procedure(target: GLenum; pname: GLenum; params: PGLint64); extdecl;
  TglGenSamplers = procedure(count: GLsizei; samplers: PGLuint); extdecl;
  TglDeleteSamplers = procedure(count: GLsizei; samplers: PGLuint); extdecl;
  TglIsSampler = function(sampler: GLuint): GLboolean; extdecl;
  TglBindSampler = procedure(unit_: GLuint; sampler: GLuint); extdecl;
  TglSamplerParameteri = procedure(sampler: GLuint; pname: GLenum; param: GLint); extdecl;
  TglSamplerParameteriv = procedure(sampler: GLuint; pname: GLenum; param: PGLint); extdecl;
  TglSamplerParameterf = procedure(sampler: GLuint; pname: GLenum; param: GLfloat); extdecl;
  TglSamplerParameterfv = procedure(sampler: GLuint; pname: GLenum; param: PGLfloat); extdecl;
  TglGetSamplerParameteriv = procedure(sampler: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetSamplerParameterfv = procedure(sampler: GLuint; pname: GLenum; params: PGLfloat); extdecl;
  TglVertexAttribDivisor = procedure(index_: GLuint; divisor: GLuint); extdecl;
  TglBindTransformFeedback = procedure(target: GLenum; id: GLuint); extdecl;
  TglDeleteTransformFeedbacks = procedure(n: GLsizei; ids: PGLuint); extdecl;
  TglGenTransformFeedbacks = procedure(n: GLsizei; ids: PGLuint); extdecl;
  TglIsTransformFeedback = function(id: GLuint): GLboolean; extdecl;
  TglPauseTransformFeedback = procedure; extdecl;
  TglResumeTransformFeedback = procedure; extdecl;
  TglGetProgramBinary = procedure(program_: GLuint; bufSize: GLsizei; length: PGLsizei; binaryFormat: PGLenum; binary: Pvoid); extdecl;
  TglProgramBinary = procedure(program_: GLuint; binaryFormat: GLenum; binary: Pvoid; length: GLsizei); extdecl;
  TglProgramParameteri = procedure(program_: GLuint; pname: GLenum; value: GLint); extdecl;
  TglInvalidateFramebuffer = procedure(target: GLenum; numAttachments: GLsizei; attachments: PGLenum); extdecl;
  TglInvalidateSubFramebuffer = procedure(target: GLenum; numAttachments: GLsizei; attachments: PGLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglTexStorage2D = procedure(target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglTexStorage3D = procedure(target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei); extdecl;
  TglGetInternalformativ = procedure(target: GLenum; internalformat: GLenum; pname: GLenum; count: GLsizei; params: PGLint); extdecl;
 
//------- GL_ES_VERSION_3_1----------------
  TglDispatchCompute = procedure(num_groups_x: GLuint; num_groups_y: GLuint; num_groups_z: GLuint); extdecl;
  TglDispatchComputeIndirect = procedure(indirect: GLintptr); extdecl;
  TglDrawArraysIndirect = procedure(mode: GLenum; indirect: Pvoid); extdecl;
  TglDrawElementsIndirect = procedure(mode: GLenum; type_: GLenum; indirect: Pvoid); extdecl;
  TglFramebufferParameteri = procedure(target: GLenum; pname: GLenum; param: GLint); extdecl;
  TglGetFramebufferParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetProgramInterfaceiv = procedure(program_: GLuint; programInterface: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetProgramResourceIndex = function(program_: GLuint; programInterface: GLenum; name_: PGLchar): GLuint; extdecl;
  TglGetProgramResourceName = procedure(program_: GLuint; programInterface: GLenum; index_: GLuint; bufSize: GLsizei; length: PGLsizei; name_: PGLchar); extdecl;
  TglGetProgramResourceiv = procedure(program_: GLuint; programInterface: GLenum; index_: GLuint; propCount: GLsizei; props: PGLenum; count: GLsizei; length: PGLsizei; params: PGLint); extdecl;
  TglGetProgramResourceLocation = function(program_: GLuint; programInterface: GLenum; name_: PGLchar): GLint; extdecl;
  TglUseProgramStages = procedure(pipeline: GLuint; stages: GLbitfield; program_: GLuint); extdecl;
  TglActiveShaderProgram = procedure(pipeline: GLuint; program_: GLuint); extdecl;
  TglCreateShaderProgramv = function(type_: GLenum; count: GLsizei; strings: PPGLchar): GLuint; extdecl;
  TglBindProgramPipeline = procedure(pipeline: GLuint); extdecl;
  TglDeleteProgramPipelines = procedure(n: GLsizei; pipelines: PGLuint); extdecl;
  TglGenProgramPipelines = procedure(n: GLsizei; pipelines: PGLuint); extdecl;
  TglIsProgramPipeline = function(pipeline: GLuint): GLboolean; extdecl;
  TglGetProgramPipelineiv = procedure(pipeline: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglProgramUniform1i = procedure(program_: GLuint; location: GLint; v0: GLint); extdecl;
  TglProgramUniform2i = procedure(program_: GLuint; location: GLint; v0: GLint; v1: GLint); extdecl;
  TglProgramUniform3i = procedure(program_: GLuint; location: GLint; v0: GLint; v1: GLint; v2: GLint); extdecl;
  TglProgramUniform4i = procedure(program_: GLuint; location: GLint; v0: GLint; v1: GLint; v2: GLint; v3: GLint); extdecl;
  TglProgramUniform1ui = procedure(program_: GLuint; location: GLint; v0: GLuint); extdecl;
  TglProgramUniform2ui = procedure(program_: GLuint; location: GLint; v0: GLuint; v1: GLuint); extdecl;
  TglProgramUniform3ui = procedure(program_: GLuint; location: GLint; v0: GLuint; v1: GLuint; v2: GLuint); extdecl;
  TglProgramUniform4ui = procedure(program_: GLuint; location: GLint; v0: GLuint; v1: GLuint; v2: GLuint; v3: GLuint); extdecl;
  TglProgramUniform1f = procedure(program_: GLuint; location: GLint; v0: GLfloat); extdecl;
  TglProgramUniform2f = procedure(program_: GLuint; location: GLint; v0: GLfloat; v1: GLfloat); extdecl;
  TglProgramUniform3f = procedure(program_: GLuint; location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat); extdecl;
  TglProgramUniform4f = procedure(program_: GLuint; location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat; v3: GLfloat); extdecl;
  TglProgramUniform1iv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform2iv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform3iv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform4iv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform1uiv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform2uiv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform3uiv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform4uiv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform1fv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform2fv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform3fv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform4fv = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniformMatrix2fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix3fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix4fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix2x3fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix3x2fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix2x4fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix4x2fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix3x4fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix4x3fv = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglValidateProgramPipeline = procedure(pipeline: GLuint); extdecl;
  TglGetProgramPipelineInfoLog = procedure(pipeline: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PGLchar); extdecl;
  TglBindImageTexture = procedure(unit_: GLuint; texture: GLuint; level: GLint; layered: GLboolean; layer: GLint; access: GLenum; format: GLenum); extdecl;
  TglGetBooleani_v = procedure(target: GLenum; index_: GLuint; data: PGLboolean); extdecl;
  TglMemoryBarrier = procedure(barriers: GLbitfield); extdecl;
  TglMemoryBarrierByRegion = procedure(barriers: GLbitfield); extdecl;
  TglTexStorage2DMultisample = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei; fixedsamplelocations: GLboolean); extdecl;
  TglGetMultisamplefv = procedure(pname: GLenum; index_: GLuint; val: PGLfloat); extdecl;
  TglSampleMaski = procedure(maskNumber: GLuint; mask: GLbitfield); extdecl;
  TglGetTexLevelParameteriv = procedure(target: GLenum; level: GLint; pname: GLenum; params: PGLint); extdecl;
  TglGetTexLevelParameterfv = procedure(target: GLenum; level: GLint; pname: GLenum; params: PGLfloat); extdecl;
  TglBindVertexBuffer = procedure(bindingindex: GLuint; buffer: GLuint; offset: GLintptr; stride: GLsizei); extdecl;
  TglVertexAttribFormat = procedure(attribindex: GLuint; size: GLint; type_: GLenum; normalized: GLboolean; relativeoffset: GLuint); extdecl;
  TglVertexAttribIFormat = procedure(attribindex: GLuint; size: GLint; type_: GLenum; relativeoffset: GLuint); extdecl;
  TglVertexAttribBinding = procedure(attribindex: GLuint; bindingindex: GLuint); extdecl;
  TglVertexBindingDivisor = procedure(bindingindex: GLuint; divisor: GLuint); extdecl;
 
//------- GL_ES_VERSION_3_2----------------
  TglBlendBarrier = procedure; extdecl;
  TglCopyImageSubData = procedure(srcName: GLuint; srcTarget: GLenum; srcLevel: GLint; srcX: GLint; srcY: GLint; srcZ: GLint; dstName: GLuint; dstTarget: GLenum; dstLevel: GLint; dstX: GLint; dstY: GLint; dstZ: GLint; srcWidth: GLsizei; srcHeight: GLsizei; srcDepth: GLsizei); extdecl;
  TglDebugMessageControl = procedure(source: GLenum; type_: GLenum; severity: GLenum; count: GLsizei; ids: PGLuint; enabled: GLboolean); extdecl;
  TglDebugMessageInsert = procedure(source: GLenum; type_: GLenum; id: GLuint; severity: GLenum; length: GLsizei; buf: PGLchar); extdecl;
  TglDebugMessageCallback = procedure(callback: GLDEBUGPROC; userParam: Pvoid); extdecl;
  TglGetDebugMessageLog = function(count: GLuint; bufSize: GLsizei; sources: PGLenum; types: PGLenum; ids: PGLuint; severities: PGLenum; lengths: PGLsizei; messageLog: PGLchar): GLuint; extdecl;
  TglPushDebugGroup = procedure(source: GLenum; id: GLuint; length: GLsizei; message_: PGLchar); extdecl;
  TglPopDebugGroup = procedure; extdecl;
  TglObjectLabel = procedure(identifier: GLenum; name_: GLuint; length: GLsizei; label_: PGLchar); extdecl;
  TglGetObjectLabel = procedure(identifier: GLenum; name_: GLuint; bufSize: GLsizei; length: PGLsizei; label_: PGLchar); extdecl;
  TglObjectPtrLabel = procedure(ptr: Pvoid; length: GLsizei; label_: PGLchar); extdecl;
  TglGetObjectPtrLabel = procedure(ptr: Pvoid; bufSize: GLsizei; length: PGLsizei; label_: PGLchar); extdecl;
  TglGetPointerv = procedure(pname: GLenum; params: PPvoid); extdecl;
  TglEnablei = procedure(target: GLenum; index_: GLuint); extdecl;
  TglDisablei = procedure(target: GLenum; index_: GLuint); extdecl;
  TglBlendEquationi = procedure(buf: GLuint; mode: GLenum); extdecl;
  TglBlendEquationSeparatei = procedure(buf: GLuint; modeRGB: GLenum; modeAlpha: GLenum); extdecl;
  TglBlendFunci = procedure(buf: GLuint; src: GLenum; dst: GLenum); extdecl;
  TglBlendFuncSeparatei = procedure(buf: GLuint; srcRGB: GLenum; dstRGB: GLenum; srcAlpha: GLenum; dstAlpha: GLenum); extdecl;
  TglColorMaski = procedure(index_: GLuint; r: GLboolean; g: GLboolean; b: GLboolean; a: GLboolean); extdecl;
  TglIsEnabledi = function(target: GLenum; index_: GLuint): GLboolean; extdecl;
  TglDrawElementsBaseVertex = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; basevertex: GLint); extdecl;
  TglDrawRangeElementsBaseVertex = procedure(mode: GLenum; start: GLuint; end_: GLuint; count: GLsizei; type_: GLenum; indices: Pvoid; basevertex: GLint); extdecl;
  TglDrawElementsInstancedBaseVertex = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; basevertex: GLint); extdecl;
  TglFramebufferTexture = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint); extdecl;
  TglPrimitiveBoundingBox = procedure(minX: GLfloat; minY: GLfloat; minZ: GLfloat; minW: GLfloat; maxX: GLfloat; maxY: GLfloat; maxZ: GLfloat; maxW: GLfloat); extdecl;
  TglGetGraphicsResetStatus = function: GLenum; extdecl;
  TglReadnPixels = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; bufSize: GLsizei; data: Pvoid); extdecl;
  TglGetnUniformfv = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLfloat); extdecl;
  TglGetnUniformiv = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLint); extdecl;
  TglGetnUniformuiv = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLuint); extdecl;
  TglMinSampleShading = procedure(value: GLfloat); extdecl;
  TglPatchParameteri = procedure(pname: GLenum; value: GLint); extdecl;
  TglTexParameterIiv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglTexParameterIuiv = procedure(target: GLenum; pname: GLenum; params: PGLuint); extdecl;
  TglGetTexParameterIiv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetTexParameterIuiv = procedure(target: GLenum; pname: GLenum; params: PGLuint); extdecl;
  TglSamplerParameterIiv = procedure(sampler: GLuint; pname: GLenum; param: PGLint); extdecl;
  TglSamplerParameterIuiv = procedure(sampler: GLuint; pname: GLenum; param: PGLuint); extdecl;
  TglGetSamplerParameterIiv = procedure(sampler: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetSamplerParameterIuiv = procedure(sampler: GLuint; pname: GLenum; params: PGLuint); extdecl;
  TglTexBuffer = procedure(target: GLenum; internalformat: GLenum; buffer: GLuint); extdecl;
  TglTexBufferRange = procedure(target: GLenum; internalformat: GLenum; buffer: GLuint; offset: GLintptr; size: GLsizeiptr); extdecl;
  TglTexStorage3DMultisample = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; fixedsamplelocations: GLboolean); extdecl;
 
//------- Other ----------------
  TglActiveProgramEXT = procedure(program_: GLuint); extdecl;
  TglActiveShaderProgramEXT = procedure(pipeline: GLuint; program_: GLuint); extdecl;
  TglAlphaFuncQCOM = procedure(func: GLenum; ref: GLclampf); extdecl;
  TglApplyFramebufferAttachmentCMAAINTEL = procedure; extdecl;
  TglAcquireKeyedMutexWin32EXT = function(memory: GLuint; key: GLuint64; timeout: GLuint): GLboolean; extdecl;
  TglArrayElement = procedure(i: GLint); extdecl;
  TglBeginConditionalRender = procedure(id: GLuint; mode: GLenum); extdecl;
  TglBeginConditionalRenderNV = procedure(id: GLuint; mode: GLenum); extdecl;
  TglBeginPerfMonitorAMD = procedure(monitor: GLuint); extdecl;
  TglBeginPerfQueryINTEL = procedure(queryHandle: GLuint); extdecl;
  TglBeginQueryEXT = procedure(target: GLenum; id: GLuint); extdecl;
  TglBindBufferOffsetEXT = procedure(target: GLenum; index_: GLuint; buffer: GLuint; offset: GLintptr); extdecl;
  TglBindFragDataLocation = procedure(program_: GLuint; color: GLuint; name_: PGLchar); extdecl;
  TglBindFragDataLocationEXT = procedure(program_: GLuint; color: GLuint; name_: PGLchar); extdecl;
  TglBindFragDataLocationIndexed = procedure(program_: GLuint; colorNumber: GLuint; index_: GLuint; name_: PGLchar); extdecl;
  TglBindFragDataLocationIndexedEXT = procedure(program_: GLuint; colorNumber: GLuint; index_: GLuint; name_: PGLchar); extdecl;
  TglBindProgramARB = procedure(target: GLenum; program_: GLuint); extdecl;
  TglBindProgramPipelineEXT = procedure(pipeline: GLuint); extdecl;
  TglBindShadingRateImageNV = procedure(texture: GLuint); extdecl;
  TglBindVertexArrayOES = procedure(array_: GLuint); extdecl;
  TglBlendBarrierKHR = procedure; extdecl;
  TglBlendBarrierNV = procedure; extdecl;
  TglBlendEquationEXT = procedure(mode: GLenum); extdecl;
  TglBlendEquationSeparateiEXT = procedure(buf: GLuint; modeRGB: GLenum; modeAlpha: GLenum); extdecl;
  TglBlendEquationSeparateiOES = procedure(buf: GLuint; modeRGB: GLenum; modeAlpha: GLenum); extdecl;
  TglBlendEquationiEXT = procedure(buf: GLuint; mode: GLenum); extdecl;
  TglBlendEquationiOES = procedure(buf: GLuint; mode: GLenum); extdecl;
  TglBlendFuncSeparateiEXT = procedure(buf: GLuint; srcRGB: GLenum; dstRGB: GLenum; srcAlpha: GLenum; dstAlpha: GLenum); extdecl;
  TglBlendFuncSeparateiOES = procedure(buf: GLuint; srcRGB: GLenum; dstRGB: GLenum; srcAlpha: GLenum; dstAlpha: GLenum); extdecl;
  TglBlendFunciEXT = procedure(buf: GLuint; src: GLenum; dst: GLenum); extdecl;
  TglBlendFunciOES = procedure(buf: GLuint; src: GLenum; dst: GLenum); extdecl;
  TglBlendParameteriNV = procedure(pname: GLenum; value: GLint); extdecl;
  TglBlitFramebufferANGLE = procedure(srcX0: GLint; srcY0: GLint; srcX1: GLint; srcY1: GLint; dstX0: GLint; dstY0: GLint; dstX1: GLint; dstY1: GLint; mask: GLbitfield; filter: GLenum); extdecl;
  TglBlitFramebufferNV = procedure(srcX0: GLint; srcY0: GLint; srcX1: GLint; srcY1: GLint; dstX0: GLint; dstY0: GLint; dstX1: GLint; dstY1: GLint; mask: GLbitfield; filter: GLenum); extdecl;
  TglBufferAttachMemoryNV = procedure(target: GLenum; memory: GLuint; offset: GLuint64); extdecl;
  TglBufferPageCommitmentMemNV = procedure(target: GLenum; offset: GLintptr; size: GLsizeiptr; memory: GLuint; memOffset: GLuint64; commit: GLboolean); extdecl;
  TglBufferStorage = procedure(target: GLenum; size: GLsizeiptr; data: Pvoid; flags: GLbitfield); extdecl;
  TglBufferStorageEXT = procedure(target: GLenum; size: GLsizeiptr; data: Pvoid; flags: GLbitfield); extdecl;
  TglBufferStorageExternalEXT = procedure(target: GLenum; offset: GLintptr; size: GLsizeiptr; clientBuffer: GLeglClientBufferEXT; flags: GLbitfield); extdecl;
  TglBufferStorageMemEXT = procedure(target: GLenum; size: GLsizeiptr; memory: GLuint; offset: GLuint64); extdecl;
  TglClampColor = procedure(target: GLenum; clamp: GLenum); extdecl;
  TglClearPixelLocalStorageuiEXT = procedure(offset: GLsizei; n: GLsizei; values: PGLuint); extdecl;
  TglClearTexImage = procedure(texture: GLuint; level: GLint; format: GLenum; type_: GLenum; data: Pvoid); extdecl;
  TglClearTexImageEXT = procedure(texture: GLuint; level: GLint; format: GLenum; type_: GLenum; data: Pvoid); extdecl;
  TglClearTexSubImage = procedure(texture: GLuint; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; type_: GLenum; data: Pvoid); extdecl;
  TglClearTexSubImageEXT = procedure(texture: GLuint; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; type_: GLenum; data: Pvoid); extdecl;
  TglClientActiveTexture = procedure(texture: GLenum); extdecl;
  TglClientWaitSyncAPPLE = function(sync: GLsync; flags: GLbitfield; timeout: GLuint64): GLenum; extdecl;
  TglClipControl = procedure(origin: GLenum; depth: GLenum); extdecl;
  TglClipControlEXT = procedure(origin: GLenum; depth: GLenum); extdecl;
  TglColorMaskiEXT = procedure(index_: GLuint; r: GLboolean; g: GLboolean; b: GLboolean; a: GLboolean); extdecl;
  TglColorMaskiOES = procedure(index_: GLuint; r: GLboolean; g: GLboolean; b: GLboolean; a: GLboolean); extdecl;
  TglColorSubTable = procedure(target: GLenum; start: GLsizei; count: GLsizei; format: GLenum; type_: GLenum; data: Pvoid); extdecl;
  TglColorTable = procedure(target: GLenum; internalformat: GLenum; width: GLsizei; format: GLenum; type_: GLenum; table: Pvoid); extdecl;
  TglColorTableParameterfv = procedure(target: GLenum; pname: GLenum; params: PGLfloat); extdecl;
  TglColorTableParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglCompressedTexImage1D = procedure(target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; border: GLint; imageSize: GLsizei; data: Pvoid); extdecl;
  TglCompressedTexImage3DOES = procedure(target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; imageSize: GLsizei; data: Pvoid); extdecl;
  TglCompressedTexSubImage1D = procedure(target: GLenum; level: GLint; xoffset: GLint; width: GLsizei; format: GLenum; imageSize: GLsizei; data: Pvoid); extdecl;
  TglCompressedTexSubImage3DOES = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; imageSize: GLsizei; data: Pvoid); extdecl;
  TglConservativeRasterParameteriNV = procedure(pname: GLenum; param: GLint); extdecl;
  TglConvolutionFilter1D = procedure(target: GLenum; internalformat: GLenum; width: GLsizei; format: GLenum; type_: GLenum; image: Pvoid); extdecl;
  TglConvolutionFilter2D = procedure(target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; image: Pvoid); extdecl;
  TglConvolutionParameterf = procedure(target: GLenum; pname: GLenum; params: GLfloat); extdecl;
  TglConvolutionParameterfv = procedure(target: GLenum; pname: GLenum; params: PGLfloat); extdecl;
  TglConvolutionParameteri = procedure(target: GLenum; pname: GLenum; params: GLint); extdecl;
  TglConvolutionParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglCopyBufferSubDataNV = procedure(readTarget: GLenum; writeTarget: GLenum; readOffset: GLintptr; writeOffset: GLintptr; size: GLsizeiptr); extdecl;
  TglCopyColorSubTable = procedure(target: GLenum; start: GLsizei; x: GLint; y: GLint; width: GLsizei); extdecl;
  TglCopyColorTable = procedure(target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei); extdecl;
  TglCopyConvolutionFilter1D = procedure(target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei); extdecl;
  TglCopyConvolutionFilter2D = procedure(target: GLenum; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglCopyImageSubDataEXT = procedure(srcName: GLuint; srcTarget: GLenum; srcLevel: GLint; srcX: GLint; srcY: GLint; srcZ: GLint; dstName: GLuint; dstTarget: GLenum; dstLevel: GLint; dstX: GLint; dstY: GLint; dstZ: GLint; srcWidth: GLsizei; srcHeight: GLsizei; srcDepth: GLsizei); extdecl;
  TglCopyImageSubDataOES = procedure(srcName: GLuint; srcTarget: GLenum; srcLevel: GLint; srcX: GLint; srcY: GLint; srcZ: GLint; dstName: GLuint; dstTarget: GLenum; dstLevel: GLint; dstX: GLint; dstY: GLint; dstZ: GLint; srcWidth: GLsizei; srcHeight: GLsizei; srcDepth: GLsizei); extdecl;
  TglCopyPathNV = procedure(resultPath: GLuint; srcPath: GLuint); extdecl;
  TglCopyTexImage1D = procedure(target: GLenum; level: GLint; internalformat: GLenum; x: GLint; y: GLint; width: GLsizei; border: GLint); extdecl;
  TglCopyTexSubImage1D = procedure(target: GLenum; level: GLint; xoffset: GLint; x: GLint; y: GLint; width: GLsizei); extdecl;
  TglCopyTexSubImage3DOES = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglCopyTextureLevelsAPPLE = procedure(destinationTexture: GLuint; sourceTexture: GLuint; sourceBaseLevel: GLint; sourceLevelCount: GLsizei); extdecl;
  TglCoverFillPathInstancedNV = procedure(numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; coverMode: GLenum; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglCoverFillPathNV = procedure(path: GLuint; coverMode: GLenum); extdecl;
  TglCoverStrokePathInstancedNV = procedure(numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; coverMode: GLenum; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglCoverStrokePathNV = procedure(path: GLuint; coverMode: GLenum); extdecl;
  TglCoverageMaskNV = procedure(mask: GLboolean); extdecl;
  TglCoverageModulationNV = procedure(components: GLenum); extdecl;
  TglCoverageModulationTableNV = procedure(n: GLsizei; v: PGLfloat); extdecl;
  TglCoverageOperationNV = procedure(operation: GLenum); extdecl;
  TglCreateMemoryObjectsEXT = procedure(n: GLsizei; memoryObjects: PGLuint); extdecl;
  TglCreatePerfQueryINTEL = procedure(queryId: GLuint; queryHandle: PGLuint); extdecl;
  TglCreateSemaphoresNV = procedure(n: GLsizei; semaphores: PGLuint); extdecl;
  TglCreateShaderProgramEXT = function(type_: GLenum; string_: PGLchar): GLuint; extdecl;
  TglCreateShaderProgramvEXT = function(type_: GLenum; count: GLsizei; strings: PPGLchar): GLuint; extdecl;
  TglDebugMessageCallbackKHR = procedure(callback: GLDEBUGPROCKHR; userParam: Pvoid); extdecl;
  TglDebugMessageControlKHR = procedure(source: GLenum; type_: GLenum; severity: GLenum; count: GLsizei; ids: PGLuint; enabled: GLboolean); extdecl;
  TglDebugMessageInsertKHR = procedure(source: GLenum; type_: GLenum; id: GLuint; severity: GLenum; length: GLsizei; buf: PGLchar); extdecl;
  TglDeleteFencesNV = procedure(n: GLsizei; fences: PGLuint); extdecl;
  TglDeleteMemoryObjectsEXT = procedure(n: GLsizei; memoryObjects: PGLuint); extdecl;
  TglDeletePathsNV = procedure(path: GLuint; range: GLsizei); extdecl;
  TglDeletePerfMonitorsAMD = procedure(n: GLsizei; monitors: PGLuint); extdecl;
  TglDeletePerfQueryINTEL = procedure(queryHandle: GLuint); extdecl;
  TglDeleteProgramPipelinesEXT = procedure(n: GLsizei; pipelines: PGLuint); extdecl;
  TglDeleteProgramsARB = procedure(n: GLsizei; programs: PGLuint); extdecl;
  TglDeleteQueriesEXT = procedure(n: GLsizei; ids: PGLuint); extdecl;
  TglDeleteSemaphoresEXT = procedure(n: GLsizei; semaphores: PGLuint); extdecl;
  TglDeleteSyncAPPLE = procedure(sync: GLsync); extdecl;
  TglDeleteVertexArraysOES = procedure(n: GLsizei; arrays: PGLuint); extdecl;
  TglDepthRangeArrayfvNV = procedure(first: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglDepthRangeArrayfvOES = procedure(first: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglDepthRangeIndexedfNV = procedure(index_: GLuint; n: GLfloat; f: GLfloat); extdecl;
  TglDepthRangeIndexedfOES = procedure(index_: GLuint; n: GLfloat; f: GLfloat); extdecl;
  TglDisableDriverControlQCOM = procedure(driverControl: GLuint); extdecl;
  TglDisableiEXT = procedure(target: GLenum; index_: GLuint); extdecl;
  TglDisableiNV = procedure(target: GLenum; index_: GLuint); extdecl;
  TglDisableiOES = procedure(target: GLenum; index_: GLuint); extdecl;
  TglDiscardFramebufferEXT = procedure(target: GLenum; numAttachments: GLsizei; attachments: PGLenum); extdecl;
  TglDrawArraysInstancedANGLE = procedure(mode: GLenum; first: GLint; count: GLsizei; primcount: GLsizei); extdecl;
  TglDrawArraysInstancedBaseInstance = procedure(mode: GLenum; first: GLint; count: GLsizei; instancecount: GLsizei; baseinstance: GLuint); extdecl;
  TglDrawArraysInstancedBaseInstanceEXT = procedure(mode: GLenum; first: GLint; count: GLsizei; instancecount: GLsizei; baseinstance: GLuint); extdecl;
  TglDrawArraysInstancedEXT = procedure(mode: GLenum; start: GLint; count: GLsizei; primcount: GLsizei); extdecl;
  TglDrawArraysInstancedNV = procedure(mode: GLenum; first: GLint; count: GLsizei; primcount: GLsizei); extdecl;
  TglDrawBuffersEXT = procedure(n: GLsizei; bufs: PGLenum); extdecl;
  TglDrawBuffersIndexedEXT = procedure(n: GLint; location: PGLenum; indices: PGLint); extdecl;
  TglDrawBuffersNV = procedure(n: GLsizei; bufs: PGLenum); extdecl;
  TglDrawElementsBaseVertexEXT = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; basevertex: GLint); extdecl;
  TglDrawElementsBaseVertexOES = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; basevertex: GLint); extdecl;
  TglDrawElementsInstancedANGLE = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; primcount: GLsizei); extdecl;
  TglDrawElementsInstancedBaseInstance = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; baseinstance: GLuint); extdecl;
  TglDrawElementsInstancedBaseInstanceEXT = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; baseinstance: GLuint); extdecl;
  TglDrawElementsInstancedBaseVertexBaseInstance = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; basevertex: GLint; baseinstance: GLuint); extdecl;
  TglDrawElementsInstancedBaseVertexBaseInstanceEXT = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; basevertex: GLint; baseinstance: GLuint); extdecl;
  TglDrawElementsInstancedBaseVertexEXT = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; basevertex: GLint); extdecl;
  TglDrawElementsInstancedBaseVertexOES = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; instancecount: GLsizei; basevertex: GLint); extdecl;
  TglDrawElementsInstancedEXT = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; primcount: GLsizei); extdecl;
  TglDrawElementsInstancedNV = procedure(mode: GLenum; count: GLsizei; type_: GLenum; indices: Pvoid; primcount: GLsizei); extdecl;
  TglDrawMeshTasksNV = procedure(first: GLuint; count: GLuint); extdecl;
  TglDrawMeshTasksIndirectNV = procedure(indirect: GLintptr); extdecl;
  TglDrawRangeElementsBaseVertexEXT = procedure(mode: GLenum; start: GLuint; end_: GLuint; count: GLsizei; type_: GLenum; indices: Pvoid; basevertex: GLint); extdecl;
  TglDrawRangeElementsBaseVertexOES = procedure(mode: GLenum; start: GLuint; end_: GLuint; count: GLsizei; type_: GLenum; indices: Pvoid; basevertex: GLint); extdecl;
  TglDrawTransformFeedback = procedure(mode: GLenum; id: GLuint); extdecl;
  TglDrawTransformFeedbackEXT = procedure(mode: GLenum; id: GLuint); extdecl;
  TglDrawTransformFeedbackInstanced = procedure(mode: GLenum; id: GLuint; instancecount: GLsizei); extdecl;
  TglDrawTransformFeedbackInstancedEXT = procedure(mode: GLenum; id: GLuint; instancecount: GLsizei); extdecl;
  TglEGLImageTargetRenderbufferStorageOES = procedure(target: GLenum; image: GLeglImageOES); extdecl;
  TglEGLImageTargetTexStorageEXT = procedure(target: GLenum; image: GLeglImageOES; attrib_list: PGLint); extdecl;
  TglEGLImageTargetTexture2DOES = procedure(target: GLenum; image: GLeglImageOES); extdecl;
  TglEGLImageTargetTextureStorageEXT = procedure(texture: GLuint; image: GLeglImageOES; attrib_list: PGLint); extdecl;
  TglEnableDriverControlQCOM = procedure(driverControl: GLuint); extdecl;
  TglEnableiEXT = procedure(target: GLenum; index_: GLuint); extdecl;
  TglEnableiNV = procedure(target: GLenum; index_: GLuint); extdecl;
  TglEnableiOES = procedure(target: GLenum; index_: GLuint); extdecl;
  TglEndConditionalRender = procedure; extdecl;
  TglEndConditionalRenderNV = procedure; extdecl;
  TglEndPerfMonitorAMD = procedure(monitor: GLuint); extdecl;
  TglEndPerfQueryINTEL = procedure(queryHandle: GLuint); extdecl;
  TglEndQueryEXT = procedure(target: GLenum); extdecl;
  TglEndTilingQCOM = procedure(preserveMask: GLbitfield); extdecl;
  TglExtGetBufferPointervQCOM = procedure(target: GLenum; params: PPvoid); extdecl;
  TglExtGetBuffersQCOM = procedure(buffers: PGLuint; maxBuffers: GLint; numBuffers: PGLint); extdecl;
  TglExtGetFramebuffersQCOM = procedure(framebuffers: PGLuint; maxFramebuffers: GLint; numFramebuffers: PGLint); extdecl;
  TglExtGetProgramBinarySourceQCOM = procedure(program_: GLuint; shadertype: GLenum; source: PGLchar; length: PGLint); extdecl;
  TglExtGetProgramsQCOM = procedure(programs: PGLuint; maxPrograms: GLint; numPrograms: PGLint); extdecl;
  TglExtGetRenderbuffersQCOM = procedure(renderbuffers: PGLuint; maxRenderbuffers: GLint; numRenderbuffers: PGLint); extdecl;
  TglExtGetShadersQCOM = procedure(shaders: PGLuint; maxShaders: GLint; numShaders: PGLint); extdecl;
  TglExtGetTexLevelParameterivQCOM = procedure(texture: GLuint; face: GLenum; level: GLint; pname: GLenum; params: PGLint); extdecl;
  TglExtGetTexSubImageQCOM = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; type_: GLenum; texels: Pvoid); extdecl;
  TglExtGetTexturesQCOM = procedure(textures: PGLuint; maxTextures: GLint; numTextures: PGLint); extdecl;
  TglExtIsProgramBinaryQCOM = function(program_: GLuint): GLboolean; extdecl;
  TglExtTexObjectStateOverrideiQCOM = procedure(target: GLenum; pname: GLenum; param: GLint); extdecl;
  TglFenceSyncAPPLE = function(condition: GLenum; flags: GLbitfield): GLsync; extdecl;
  TglFinishFenceNV = procedure(fence: GLuint); extdecl;
  TglFlushMappedBufferRangeEXT = procedure(target: GLenum; offset: GLintptr; length: GLsizeiptr); extdecl;
  TglFogCoordPointer = procedure(type_: GLenum; stride: GLsizei; pointer: Pvoid); extdecl;
  TglFogCoordd = procedure(coord: GLdouble); extdecl;
  TglFogCoorddv = procedure(coord: PGLdouble); extdecl;
  TglFogCoordf = procedure(coord: GLfloat); extdecl;
  TglFogCoordfv = procedure(coord: PGLfloat); extdecl;
  TglFragmentCoverageColorNV = procedure(color: GLuint); extdecl;
  TglFramebufferFetchBarrierEXT = procedure; extdecl;
  TglFramebufferFetchBarrierQCOM = procedure; extdecl;
  TglFramebufferFoveationConfigQCOM = procedure(framebuffer: GLuint; numLayers: GLuint; focalPointsPerLayer: GLuint; requestedFeatures: GLuint; providedFeatures: PGLuint); extdecl;
  TglFramebufferFoveationParametersQCOM = procedure(framebuffer: GLuint; layer: GLuint; focalPoint: GLuint; focalX: GLfloat; focalY: GLfloat; gainX: GLfloat; gainY: GLfloat; foveaArea: GLfloat); extdecl;
  TglFramebufferPixelLocalStorageSizeEXT = procedure(target: GLuint; size: GLsizei); extdecl;
  TglFramebufferSampleLocationsfvNV = procedure(target: GLenum; start: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglFramebufferTexture1D = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint); extdecl;
  TglFramebufferTexture2DDownsampleIMG = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint; xscale: GLint; yscale: GLint); extdecl;
  TglFramebufferTexture2DMultisampleEXT = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint; samples: GLsizei); extdecl;
  TglFramebufferTexture2DMultisampleIMG = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint; samples: GLsizei); extdecl;
  TglFramebufferTexture3D = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint; zoffset: GLint); extdecl;
  TglFramebufferTexture3DOES = procedure(target: GLenum; attachment: GLenum; textarget: GLenum; texture: GLuint; level: GLint; zoffset: GLint); extdecl;
  TglFramebufferTextureEXT = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint); extdecl;
  TglFramebufferTextureFaceARB = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint; face: GLenum); extdecl;
  TglFramebufferTextureLayerDownsampleIMG = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint; layer: GLint; xscale: GLint; yscale: GLint); extdecl;
  TglFramebufferTextureMultisampleMultiviewOVR = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint; samples: GLsizei; baseViewIndex: GLint; numViews: GLsizei); extdecl;
  TglFramebufferTextureMultiviewOVR = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint; baseViewIndex: GLint; numViews: GLsizei); extdecl;
  TglFramebufferTextureOES = procedure(target: GLenum; attachment: GLenum; texture: GLuint; level: GLint); extdecl;
  TglGenFencesNV = procedure(n: GLsizei; fences: PGLuint); extdecl;
  TglGenPathsNV = function(range: GLsizei): GLuint; extdecl;
  TglGenPerfMonitorsAMD = procedure(n: GLsizei; monitors: PGLuint); extdecl;
  TglGenProgramPipelinesEXT = procedure(n: GLsizei; pipelines: PGLuint); extdecl;
  TglGenProgramsARB = procedure(n: GLsizei; programs: PGLuint); extdecl;
  TglGenQueriesEXT = procedure(n: GLsizei; ids: PGLuint); extdecl;
  TglGenSemaphoresEXT = procedure(n: GLsizei; semaphores: PGLuint); extdecl;
  TglGenVertexArraysOES = procedure(n: GLsizei; arrays: PGLuint); extdecl;
  TglGetBufferPointervOES = procedure(target: GLenum; pname: GLenum; params: PPvoid); extdecl;
  TglGetBufferSubData = procedure(target: GLenum; offset: GLintptr; size: GLsizeiptr; data: Pvoid); extdecl;
  TglGetColorTable = procedure(target: GLenum; format: GLenum; type_: GLenum; table: Pvoid); extdecl;
  TglGetColorTableParameterfv = procedure(target: GLenum; pname: GLenum; params: PGLfloat); extdecl;
  TglGetColorTableParameteriv = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetCompressedTexImage = procedure(target: GLenum; level: GLint; img: Pvoid); extdecl;
  TglGetCoverageModulationTableNV = procedure(bufSize: GLsizei; v: PGLfloat); extdecl;
  TglGetDebugMessageLogKHR = function(count: GLuint; bufSize: GLsizei; sources: PGLenum; types: PGLenum; ids: PGLuint; severities: PGLenum; lengths: PGLsizei; messageLog: PGLchar): GLuint; extdecl;
  TglGetDoublei_v = procedure(target: GLenum; index_: GLuint; data: PGLdouble); extdecl;
  TglGetDriverControlStringQCOM = procedure(driverControl: GLuint; bufSize: GLsizei; length: PGLsizei; driverControlString: PGLchar); extdecl;
  TglGetDriverControlsQCOM = procedure(num: PGLint; size: GLsizei; driverControls: PGLuint); extdecl;
  TglGetFenceivNV = procedure(fence: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetFirstPerfQueryIdINTEL = procedure(queryId: PGLuint); extdecl;
  TglGetFloati_v = procedure(target: GLenum; index_: GLuint; data: PGLfloat); extdecl;
  TglGetFloati_vNV = procedure(target: GLenum; index_: GLuint; data: PGLfloat); extdecl;
  TglGetFloati_vOES = procedure(target: GLenum; index_: GLuint; data: PGLfloat); extdecl;
  TglGetFragDataIndex = function(program_: GLuint; name_: PGLchar): GLint; extdecl;
  TglGetFragDataIndexEXT = function(program_: GLuint; name_: PGLchar): GLint; extdecl;
  TglGetFramebufferPixelLocalStorageSizeEXT = function(target: GLuint): GLsizei; extdecl;
  TglGetGraphicsResetStatusEXT = function: GLenum; extdecl;
  TglGetGraphicsResetStatusKHR = function: GLenum; extdecl;
  TglGetImageHandleNV = function(texture: GLuint; level: GLint; layered: GLboolean; layer: GLint; format: GLenum): GLuint64; extdecl;
  TglGetInteger64vAPPLE = procedure(pname: GLenum; params: PGLint64); extdecl;
  TglGetInteger64vEXT = procedure(pname: GLenum; data: PGLint64); extdecl;
  TglGetIntegeri_vEXT = procedure(target: GLenum; index_: GLuint; data: PGLint); extdecl;
  TglGetInternalformatSampleivNV = procedure(target: GLenum; internalformat: GLenum; samples: GLsizei; pname: GLenum; count: GLsizei; params: PGLint); extdecl;
  TglGetMemoryObjectDetachedResourcesuivNV = procedure(memory: GLuint; pname: GLenum; first: GLint; count: GLsizei; params: PGLuint); extdecl;
  TglGetMemoryObjectParameterivEXT = procedure(memoryObject: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetNextPerfQueryIdINTEL = procedure(queryId: GLuint; nextQueryId: PGLuint); extdecl;
  TglGetObjectLabelEXT = procedure(type_: GLenum; object_: GLuint; bufSize: GLsizei; length: PGLsizei; label_: PGLchar); extdecl;
  TglGetObjectLabelKHR = procedure(identifier: GLenum; name_: GLuint; bufSize: GLsizei; length: PGLsizei; label_: PGLchar); extdecl;
  TglGetObjectPtrLabelKHR = procedure(ptr: Pvoid; bufSize: GLsizei; length: PGLsizei; label_: PGLchar); extdecl;
  TglGetPathColorGenfvNV = procedure(color: GLenum; pname: GLenum; value: PGLfloat); extdecl;
  TglGetPathColorGenivNV = procedure(color: GLenum; pname: GLenum; value: PGLint); extdecl;
  TglGetPathCommandsNV = procedure(path: GLuint; commands: PGLubyte); extdecl;
  TglGetPathCoordsNV = procedure(path: GLuint; coords: PGLfloat); extdecl;
  TglGetPathDashArrayNV = procedure(path: GLuint; dashArray: PGLfloat); extdecl;
  TglGetPathLengthNV = function(path: GLuint; startSegment: GLsizei; numSegments: GLsizei): GLfloat; extdecl;
  TglGetPathMetricRangeNV = procedure(metricQueryMask: GLbitfield; firstPathName: GLuint; numPaths: GLsizei; stride: GLsizei; metrics: PGLfloat); extdecl;
  TglGetPathMetricsNV = procedure(metricQueryMask: GLbitfield; numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; stride: GLsizei; metrics: PGLfloat); extdecl;
  TglGetPathParameterfvNV = procedure(path: GLuint; pname: GLenum; value: PGLfloat); extdecl;
  TglGetPathParameterivNV = procedure(path: GLuint; pname: GLenum; value: PGLint); extdecl;
  TglGetPathSpacingNV = procedure(pathListMode: GLenum; numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; advanceScale: GLfloat; kerningScale: GLfloat; transformType: GLenum; returnedSpacing: PGLfloat); extdecl;
  TglGetPathTexGenfvNV = procedure(texCoordSet: GLenum; pname: GLenum; value: PGLfloat); extdecl;
  TglGetPathTexGenivNV = procedure(texCoordSet: GLenum; pname: GLenum; value: PGLint); extdecl;
  TglGetPerfCounterInfoINTEL = procedure(queryId: GLuint; counterId: GLuint; counterNameLength: GLuint; counterName: PGLchar; counterDescLength: GLuint; counterDesc: PGLchar; counterOffset: PGLuint; counterDataSize: PGLuint; counterTypeEnum: PGLuint; counterDataTypeEnum: PGLuint; rawCounterMaxValue: PGLuint64); extdecl;
  TglGetPerfMonitorCounterDataAMD = procedure(monitor: GLuint; pname: GLenum; dataSize: GLsizei; data: PGLuint; bytesWritten: PGLint); extdecl;
  TglGetPerfMonitorCounterInfoAMD = procedure(group: GLuint; counter: GLuint; pname: GLenum; data: Pvoid); extdecl;
  TglGetPerfMonitorCounterStringAMD = procedure(group: GLuint; counter: GLuint; bufSize: GLsizei; length: PGLsizei; counterString: PGLchar); extdecl;
  TglGetPerfMonitorCountersAMD = procedure(group: GLuint; numCounters: PGLint; maxActiveCounters: PGLint; counterSize: GLsizei; counters: PGLuint); extdecl;
  TglGetPerfMonitorGroupStringAMD = procedure(group: GLuint; bufSize: GLsizei; length: PGLsizei; groupString: PGLchar); extdecl;
  TglGetPerfMonitorGroupsAMD = procedure(numGroups: PGLint; groupsSize: GLsizei; groups: PGLuint); extdecl;
  TglGetPerfQueryDataINTEL = procedure(queryHandle: GLuint; flags: GLuint; dataSize: GLsizei; data: Pvoid; bytesWritten: PGLuint); extdecl;
  TglGetPerfQueryIdByNameINTEL = procedure(queryName: PGLchar; queryId: PGLuint); extdecl;
  TglGetPerfQueryInfoINTEL = procedure(queryId: GLuint; queryNameLength: GLuint; queryName: PGLchar; dataSize: PGLuint; noCounters: PGLuint; noInstances: PGLuint; capsMask: PGLuint); extdecl;
  TglGetPointervKHR = procedure(pname: GLenum; params: PPvoid); extdecl;
  TglGetProgramBinaryOES = procedure(program_: GLuint; bufSize: GLsizei; length: PGLsizei; binaryFormat: PGLenum; binary: Pvoid); extdecl;
  TglGetProgramPipelineInfoLogEXT = procedure(pipeline: GLuint; bufSize: GLsizei; length: PGLsizei; infoLog: PGLchar); extdecl;
  TglGetProgramPipelineivEXT = procedure(pipeline: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetProgramResourceLocationIndexEXT = function(program_: GLuint; programInterface: GLenum; name_: PGLchar): GLint; extdecl;
  TglGetProgramResourcefvNV = procedure(program_: GLuint; programInterface: GLenum; index_: GLuint; propCount: GLsizei; props: PGLenum; count: GLsizei; length: PGLsizei; params: PGLfloat); extdecl;
  TglGetQueryObjecti64v = procedure(id: GLuint; pname: GLenum; params: PGLint64); extdecl;
  TglGetQueryObjecti64vEXT = procedure(id: GLuint; pname: GLenum; params: PGLint64); extdecl;
  TglGetQueryObjectiv = procedure(id: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetQueryObjectivEXT = procedure(id: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetQueryObjectui64v = procedure(id: GLuint; pname: GLenum; params: PGLuint64); extdecl;
  TglGetQueryObjectui64vEXT = procedure(id: GLuint; pname: GLenum; params: PGLuint64); extdecl;
  TglGetQueryObjectuivEXT = procedure(id: GLuint; pname: GLenum; params: PGLuint); extdecl;
  TglGetQueryivEXT = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetSamplerParameterIivEXT = procedure(sampler: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetSamplerParameterIivOES = procedure(sampler: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetSamplerParameterIuivEXT = procedure(sampler: GLuint; pname: GLenum; params: PGLuint); extdecl;
  TglGetSamplerParameterIuivOES = procedure(sampler: GLuint; pname: GLenum; params: PGLuint); extdecl;
  TglGetSemaphoreParameterivNV = procedure(semaphore: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglGetSemaphoreParameterui64vEXT = procedure(semaphore: GLuint; pname: GLenum; params: PGLuint64); extdecl;
  TglGetShadingRateImagePaletteNV = procedure(viewport: GLuint; entry: GLuint; rate: PGLenum); extdecl;
  TglGetShadingRateSampleLocationivNV = procedure(rate: GLenum; samples: GLuint; index_: GLuint; location: PGLint); extdecl;
  TglGetSyncivAPPLE = procedure(sync: GLsync; pname: GLenum; count: GLsizei; length: PGLsizei; values: PGLint); extdecl;
  TglGetTexParameterIivEXT = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetTexParameterIivOES = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglGetTexParameterIuivEXT = procedure(target: GLenum; pname: GLenum; params: PGLuint); extdecl;
  TglGetTexParameterIuivOES = procedure(target: GLenum; pname: GLenum; params: PGLuint); extdecl;
  TglGetTextureHandleARB = function(texture: GLuint): GLuint64; extdecl;
  TglGetTextureHandleIMG = function(texture: GLuint): GLuint64; extdecl;
  TglGetTextureHandleNV = function(texture: GLuint): GLuint64; extdecl;
  TglGetTextureSamplerHandleARB = function(texture: GLuint; sampler: GLuint): GLuint64; extdecl;
  TglGetTextureSamplerHandleIMG = function(texture: GLuint; sampler: GLuint): GLuint64; extdecl;
  TglGetTextureSamplerHandleNV = function(texture: GLuint; sampler: GLuint): GLuint64; extdecl;
  TglGetTranslatedShaderSourceANGLE = procedure(shader: GLuint; bufSize: GLsizei; length: PGLsizei; source: PGLchar); extdecl;
  TglGetUniformi64vNV = procedure(program_: GLuint; location: GLint; params: PGLint64EXT); extdecl;
  TglGetUnsignedBytevEXT = procedure(pname: GLenum; data: PGLubyte); extdecl;
  TglGetUnsignedBytei_vEXT = procedure(target: GLenum; index_: GLuint; data: PGLubyte); extdecl;
  TglGetVertexAttribLdv = procedure(index_: GLuint; pname: GLenum; params: PGLdouble); extdecl;
  TglGetVertexAttribdv = procedure(index_: GLuint; pname: GLenum; params: PGLdouble); extdecl;
  TglGetnUniformfvEXT = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLfloat); extdecl;
  TglGetnUniformfvKHR = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLfloat); extdecl;
  TglGetnUniformivEXT = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLint); extdecl;
  TglGetnUniformivKHR = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLint); extdecl;
  TglGetnUniformuivKHR = procedure(program_: GLuint; location: GLint; bufSize: GLsizei; params: PGLuint); extdecl;
  TglHistogram = procedure(target: GLenum; width: GLsizei; internalformat: GLenum; sink: GLboolean); extdecl;
  TglImportMemoryFdEXT = procedure(memory: GLuint; size: GLuint64; handleType: GLenum; fd: GLint); extdecl;
  TglImportMemoryWin32HandleEXT = procedure(memory: GLuint; size: GLuint64; handleType: GLenum; handle: Pvoid); extdecl;
  TglImportMemoryWin32NameEXT = procedure(memory: GLuint; size: GLuint64; handleType: GLenum; name_: Pvoid); extdecl;
  TglImportSemaphoreFdEXT = procedure(semaphore: GLuint; handleType: GLenum; fd: GLint); extdecl;
  TglImportSemaphoreWin32HandleEXT = procedure(semaphore: GLuint; handleType: GLenum; handle: Pvoid); extdecl;
  TglImportSemaphoreWin32NameEXT = procedure(semaphore: GLuint; handleType: GLenum; name_: Pvoid); extdecl;
  TglInsertEventMarkerEXT = procedure(length: GLsizei; marker: PGLchar); extdecl;
  TglInterpolatePathsNV = procedure(resultPath: GLuint; pathA: GLuint; pathB: GLuint; weight: GLfloat); extdecl;
  TglIsEnablediEXT = function(target: GLenum; index_: GLuint): GLboolean; extdecl;
  TglIsEnablediNV = function(target: GLenum; index_: GLuint): GLboolean; extdecl;
  TglIsEnablediOES = function(target: GLenum; index_: GLuint): GLboolean; extdecl;
  TglIsFenceNV = function(fence: GLuint): GLboolean; extdecl;
  TglIsImageHandleResidentNV = function(handle: GLuint64): GLboolean; extdecl;
  TglIsMemoryObjectEXT = function(memoryObject: GLuint): GLboolean; extdecl;
  TglIsPathNV = function(path: GLuint): GLboolean; extdecl;
  TglIsPointInFillPathNV = function(path: GLuint; mask: GLuint; x: GLfloat; y: GLfloat): GLboolean; extdecl;
  TglIsPointInStrokePathNV = function(path: GLuint; x: GLfloat; y: GLfloat): GLboolean; extdecl;
  TglIsProgramARB = function(program_: GLuint): GLboolean; extdecl;
  TglIsProgramPipelineEXT = function(pipeline: GLuint): GLboolean; extdecl;
  TglIsQueryEXT = function(id: GLuint): GLboolean; extdecl;
  TglIsSemaphoreEXT = function(semaphore: GLuint): GLboolean; extdecl;
  TglIsSyncAPPLE = function(sync: GLsync): GLboolean; extdecl;
  TglIsTextureHandleResidentNV = function(handle: GLuint64): GLboolean; extdecl;
  TglIsVertexArrayOES = function(array_: GLuint): GLboolean; extdecl;
  TglLabelObjectEXT = procedure(type_: GLenum; object_: GLuint; length: GLsizei; label_: PGLchar); extdecl;
  TglLoadTransposeMatrixd = procedure(m: PGLdouble); extdecl;
  TglLoadTransposeMatrixf = procedure(m: PGLfloat); extdecl;
  TglMakeImageHandleNonResidentNV = procedure(handle: GLuint64); extdecl;
  TglMakeImageHandleResidentNV = procedure(handle: GLuint64; access: GLenum); extdecl;
  TglMakeTextureHandleNonResidentNV = procedure(handle: GLuint64); extdecl;
  TglMakeTextureHandleResidentNV = procedure(handle: GLuint64); extdecl;
  TglMapBuffer = function(target: GLenum; access: GLenum): Pvoid; extdecl;
  TglMapBufferOES = function(target: GLenum; access: GLenum): Pvoid; extdecl;
  TglMapBufferRangeEXT = function(target: GLenum; offset: GLintptr; length: GLsizeiptr; access: GLbitfield): Pvoid; extdecl;
  TglMatrixFrustumEXT = procedure(mode: GLenum; left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble; zNear: GLdouble; zFar: GLdouble); extdecl;
  TglMatrixLoad3x2fNV = procedure(matrixMode: GLenum; m: PGLfloat); extdecl;
  TglMatrixLoad3x3fNV = procedure(matrixMode: GLenum; m: PGLfloat); extdecl;
  TglMatrixLoadIdentityEXT = procedure(mode: GLenum); extdecl;
  TglMatrixLoadTranspose3x3fNV = procedure(matrixMode: GLenum; m: PGLfloat); extdecl;
  TglMatrixLoadTransposedEXT = procedure(mode: GLenum; m: PGLdouble); extdecl;
  TglMatrixLoadTransposefEXT = procedure(mode: GLenum; m: PGLfloat); extdecl;
  TglMatrixLoaddEXT = procedure(mode: GLenum; m: PGLdouble); extdecl;
  TglMatrixLoadfEXT = procedure(mode: GLenum; m: PGLfloat); extdecl;
  TglMatrixMult3x2fNV = procedure(matrixMode: GLenum; m: PGLfloat); extdecl;
  TglMatrixMult3x3fNV = procedure(matrixMode: GLenum; m: PGLfloat); extdecl;
  TglMatrixMultTranspose3x3fNV = procedure(matrixMode: GLenum; m: PGLfloat); extdecl;
  TglMatrixMultTransposedEXT = procedure(mode: GLenum; m: PGLdouble); extdecl;
  TglMatrixMultTransposefEXT = procedure(mode: GLenum; m: PGLfloat); extdecl;
  TglMatrixMultdEXT = procedure(mode: GLenum; m: PGLdouble); extdecl;
  TglMatrixMultfEXT = procedure(mode: GLenum; m: PGLfloat); extdecl;
  TglMatrixOrthoEXT = procedure(mode: GLenum; left: GLdouble; right: GLdouble; bottom: GLdouble; top: GLdouble; zNear: GLdouble; zFar: GLdouble); extdecl;
  TglMatrixPopEXT = procedure(mode: GLenum); extdecl;
  TglMatrixPushEXT = procedure(mode: GLenum); extdecl;
  TglMatrixRotatedEXT = procedure(mode: GLenum; angle: GLdouble; x: GLdouble; y: GLdouble; z: GLdouble); extdecl;
  TglMatrixRotatefEXT = procedure(mode: GLenum; angle: GLfloat; x: GLfloat; y: GLfloat; z: GLfloat); extdecl;
  TglMatrixScaledEXT = procedure(mode: GLenum; x: GLdouble; y: GLdouble; z: GLdouble); extdecl;
  TglMatrixScalefEXT = procedure(mode: GLenum; x: GLfloat; y: GLfloat; z: GLfloat); extdecl;
  TglMatrixTranslatedEXT = procedure(mode: GLenum; x: GLdouble; y: GLdouble; z: GLdouble); extdecl;
  TglMatrixTranslatefEXT = procedure(mode: GLenum; x: GLfloat; y: GLfloat; z: GLfloat); extdecl;
  TglMaxShaderCompilerThreadsKHR = procedure(count: GLuint); extdecl;
  TglMemoryObjectParameterivEXT = procedure(memoryObject: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglMinSampleShadingOES = procedure(value: GLfloat); extdecl;
  TglMinmax = procedure(target: GLenum; internalformat: GLenum; sink: GLboolean); extdecl;
  TglMultTransposeMatrixd = procedure(m: PGLdouble); extdecl;
  TglMultTransposeMatrixf = procedure(m: PGLfloat); extdecl;
  TglMultiDrawArrays = procedure(mode: GLenum; first: PGLint; count: PGLsizei; drawcount: GLsizei); extdecl;
  TglMultiDrawArraysEXT = procedure(mode: GLenum; first: PGLint; count: PGLsizei; primcount: GLsizei); extdecl;
  TglMultiDrawArraysIndirect = procedure(mode: GLenum; indirect: Pvoid; drawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawArraysIndirectCount = procedure(mode: GLenum; indirect: Pvoid; drawcount: GLintptr; maxdrawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawArraysIndirectEXT = procedure(mode: GLenum; indirect: Pvoid; drawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawElements = procedure(mode: GLenum; count: PGLsizei; type_: GLenum; indices: PPvoid; drawcount: GLsizei); extdecl;
  TglMultiDrawElementsBaseVertex = procedure(mode: GLenum; count: PGLsizei; type_: GLenum; indices: PPvoid; drawcount: GLsizei; basevertex: PGLint); extdecl;
  TglMultiDrawElementsBaseVertexEXT = procedure(mode: GLenum; count: PGLsizei; type_: GLenum; indices: PPvoid; drawcount: GLsizei; basevertex: PGLint); extdecl;
  TglMultiDrawElementsEXT = procedure(mode: GLenum; count: PGLsizei; type_: GLenum; indices: PPvoid; primcount: GLsizei); extdecl;
  TglMultiDrawElementsIndirect = procedure(mode: GLenum; type_: GLenum; indirect: Pvoid; drawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawElementsIndirectCount = procedure(mode: GLenum; type_: GLenum; indirect: Pvoid; drawcount: GLintptr; maxdrawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawElementsIndirectEXT = procedure(mode: GLenum; type_: GLenum; indirect: Pvoid; drawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawMeshTasksIndirectNV = procedure(indirect: GLintptr; drawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiDrawMeshTasksIndirectCountNV = procedure(indirect: GLintptr; drawcount: GLintptr; maxdrawcount: GLsizei; stride: GLsizei); extdecl;
  TglMultiTexCoord1d = procedure(target: GLenum; s: GLdouble); extdecl;
  TglMultiTexCoord1dv = procedure(target: GLenum; v: PGLdouble); extdecl;
  TglMultiTexCoord1f = procedure(target: GLenum; s: GLfloat); extdecl;
  TglMultiTexCoord1fv = procedure(target: GLenum; v: PGLfloat); extdecl;
  TglMultiTexCoord1i = procedure(target: GLenum; s: GLint); extdecl;
  TglMultiTexCoord1iv = procedure(target: GLenum; v: PGLint); extdecl;
  TglMultiTexCoord1s = procedure(target: GLenum; s: GLshort); extdecl;
  TglMultiTexCoord1sv = procedure(target: GLenum; v: PGLshort); extdecl;
  TglMultiTexCoord2d = procedure(target: GLenum; s: GLdouble; t: GLdouble); extdecl;
  TglMultiTexCoord2dv = procedure(target: GLenum; v: PGLdouble); extdecl;
  TglMultiTexCoord2f = procedure(target: GLenum; s: GLfloat; t: GLfloat); extdecl;
  TglMultiTexCoord2fv = procedure(target: GLenum; v: PGLfloat); extdecl;
  TglMultiTexCoord2i = procedure(target: GLenum; s: GLint; t: GLint); extdecl;
  TglMultiTexCoord2iv = procedure(target: GLenum; v: PGLint); extdecl;
  TglMultiTexCoord2s = procedure(target: GLenum; s: GLshort; t: GLshort); extdecl;
  TglMultiTexCoord2sv = procedure(target: GLenum; v: PGLshort); extdecl;
  TglMultiTexCoord3d = procedure(target: GLenum; s: GLdouble; t: GLdouble; r: GLdouble); extdecl;
  TglMultiTexCoord3dv = procedure(target: GLenum; v: PGLdouble); extdecl;
  TglMultiTexCoord3f = procedure(target: GLenum; s: GLfloat; t: GLfloat; r: GLfloat); extdecl;
  TglMultiTexCoord3fv = procedure(target: GLenum; v: PGLfloat); extdecl;
  TglMultiTexCoord3i = procedure(target: GLenum; s: GLint; t: GLint; r: GLint); extdecl;
  TglMultiTexCoord3iv = procedure(target: GLenum; v: PGLint); extdecl;
  TglMultiTexCoord3s = procedure(target: GLenum; s: GLshort; t: GLshort; r: GLshort); extdecl;
  TglMultiTexCoord3sv = procedure(target: GLenum; v: PGLshort); extdecl;
  TglMultiTexCoord4d = procedure(target: GLenum; s: GLdouble; t: GLdouble; r: GLdouble; q: GLdouble); extdecl;
  TglMultiTexCoord4dv = procedure(target: GLenum; v: PGLdouble); extdecl;
  TglMultiTexCoord4f = procedure(target: GLenum; s: GLfloat; t: GLfloat; r: GLfloat; q: GLfloat); extdecl;
  TglMultiTexCoord4fv = procedure(target: GLenum; v: PGLfloat); extdecl;
  TglMultiTexCoord4i = procedure(target: GLenum; s: GLint; t: GLint; r: GLint; q: GLint); extdecl;
  TglMultiTexCoord4iv = procedure(target: GLenum; v: PGLint); extdecl;
  TglMultiTexCoord4s = procedure(target: GLenum; s: GLshort; t: GLshort; r: GLshort; q: GLshort); extdecl;
  TglMultiTexCoord4sv = procedure(target: GLenum; v: PGLshort); extdecl;
  TglNamedBufferAttachMemoryNV = procedure(buffer: GLuint; memory: GLuint; offset: GLuint64); extdecl;
  TglNamedBufferPageCommitmentMemNV = procedure(buffer: GLuint; offset: GLintptr; size: GLsizeiptr; memory: GLuint; memOffset: GLuint64; commit: GLboolean); extdecl;
  TglNamedBufferStorage = procedure(buffer: GLuint; size: GLsizeiptr; data: Pvoid; flags: GLbitfield); extdecl;
  TglNamedBufferStorageExternalEXT = procedure(buffer: GLuint; offset: GLintptr; size: GLsizeiptr; clientBuffer: GLeglClientBufferEXT; flags: GLbitfield); extdecl;
  TglNamedBufferStorageMemEXT = procedure(buffer: GLuint; size: GLsizeiptr; memory: GLuint; offset: GLuint64); extdecl;
  TglNamedBufferSubData = procedure(buffer: GLuint; offset: GLintptr; size: GLsizeiptr; data: Pvoid); extdecl;
  TglNamedFramebufferSampleLocationsfvNV = procedure(framebuffer: GLuint; start: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglNamedRenderbufferStorageMultisampleAdvancedAMD = procedure(renderbuffer: GLuint; samples: GLsizei; storageSamples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglObjectLabelKHR = procedure(identifier: GLenum; name_: GLuint; length: GLsizei; label_: PGLchar); extdecl;
  TglObjectPtrLabelKHR = procedure(ptr: Pvoid; length: GLsizei; label_: PGLchar); extdecl;
  TglPatchParameteriEXT = procedure(pname: GLenum; value: GLint); extdecl;
  TglPatchParameteriOES = procedure(pname: GLenum; value: GLint); extdecl;
  TglPathColorGenNV = procedure(color: GLenum; genMode: GLenum; colorFormat: GLenum; coeffs: PGLfloat); extdecl;
  TglPathCommandsNV = procedure(path: GLuint; numCommands: GLsizei; commands: PGLubyte; numCoords: GLsizei; coordType: GLenum; coords: Pvoid); extdecl;
  TglPathCoordsNV = procedure(path: GLuint; numCoords: GLsizei; coordType: GLenum; coords: Pvoid); extdecl;
  TglPathCoverDepthFuncNV = procedure(func: GLenum); extdecl;
  TglPathDashArrayNV = procedure(path: GLuint; dashCount: GLsizei; dashArray: PGLfloat); extdecl;
  TglPathFogGenNV = procedure(genMode: GLenum); extdecl;
  TglPathGlyphIndexArrayNV = function(firstPathName: GLuint; fontTarget: GLenum; fontName: Pvoid; fontStyle: GLbitfield; firstGlyphIndex: GLuint; numGlyphs: GLsizei; pathParameterTemplate: GLuint; emScale: GLfloat): GLenum; extdecl;
  TglPathGlyphIndexRangeNV = function(fontTarget: GLenum; fontName: Pvoid; fontStyle: GLbitfield; pathParameterTemplate: GLuint; emScale: GLfloat; baseAndCount: PGLuint): GLenum; extdecl;
  TglPathGlyphRangeNV = procedure(firstPathName: GLuint; fontTarget: GLenum; fontName: Pvoid; fontStyle: GLbitfield; firstGlyph: GLuint; numGlyphs: GLsizei; handleMissingGlyphs: GLenum; pathParameterTemplate: GLuint; emScale: GLfloat); extdecl;
  TglPathGlyphsNV = procedure(firstPathName: GLuint; fontTarget: GLenum; fontName: Pvoid; fontStyle: GLbitfield; numGlyphs: GLsizei; type_: GLenum; charcodes: Pvoid; handleMissingGlyphs: GLenum; pathParameterTemplate: GLuint; emScale: GLfloat); extdecl;
  TglPathMemoryGlyphIndexArrayNV = function(firstPathName: GLuint; fontTarget: GLenum; fontSize: GLsizeiptr; fontData: Pvoid; faceIndex: GLsizei; firstGlyphIndex: GLuint; numGlyphs: GLsizei; pathParameterTemplate: GLuint; emScale: GLfloat): GLenum; extdecl;
  TglPathParameterfNV = procedure(path: GLuint; pname: GLenum; value: GLfloat); extdecl;
  TglPathParameterfvNV = procedure(path: GLuint; pname: GLenum; value: PGLfloat); extdecl;
  TglPathParameteriNV = procedure(path: GLuint; pname: GLenum; value: GLint); extdecl;
  TglPathParameterivNV = procedure(path: GLuint; pname: GLenum; value: PGLint); extdecl;
  TglPathStencilDepthOffsetNV = procedure(factor: GLfloat; units: GLfloat); extdecl;
  TglPathStencilFuncNV = procedure(func: GLenum; ref: GLint; mask: GLuint); extdecl;
  TglPathStringNV = procedure(path: GLuint; format: GLenum; length: GLsizei; pathString: Pvoid); extdecl;
  TglPathSubCommandsNV = procedure(path: GLuint; commandStart: GLsizei; commandsToDelete: GLsizei; numCommands: GLsizei; commands: PGLubyte; numCoords: GLsizei; coordType: GLenum; coords: Pvoid); extdecl;
  TglPathSubCoordsNV = procedure(path: GLuint; coordStart: GLsizei; numCoords: GLsizei; coordType: GLenum; coords: Pvoid); extdecl;
  TglPathTexGenNV = procedure(texCoordSet: GLenum; genMode: GLenum; components: GLint; coeffs: PGLfloat); extdecl;
  TglPointAlongPathNV = function(path: GLuint; startSegment: GLsizei; numSegments: GLsizei; distance: GLfloat; x: PGLfloat; y: PGLfloat; tangentX: PGLfloat; tangentY: PGLfloat): GLboolean; extdecl;
  TglPointParameterf = procedure(pname: GLenum; param: GLfloat); extdecl;
  TglPointParameterfv = procedure(pname: GLenum; params: PGLfloat); extdecl;
  TglPointParameteri = procedure(pname: GLenum; param: GLint); extdecl;
  TglPointParameteriv = procedure(pname: GLenum; params: PGLint); extdecl;
  TglPolygonMode = procedure(face: GLenum; mode: GLenum); extdecl;
  TglPolygonModeNV = procedure(face: GLenum; mode: GLenum); extdecl;
  TglPolygonOffsetClamp = procedure(factor: GLfloat; units: GLfloat; clamp: GLfloat); extdecl;
  TglPolygonOffsetClampEXT = procedure(factor: GLfloat; units: GLfloat; clamp: GLfloat); extdecl;
  TglPopDebugGroupKHR = procedure; extdecl;
  TglPopGroupMarkerEXT = procedure; extdecl;
  TglPrimitiveBoundingBoxEXT = procedure(minX: GLfloat; minY: GLfloat; minZ: GLfloat; minW: GLfloat; maxX: GLfloat; maxY: GLfloat; maxZ: GLfloat; maxW: GLfloat); extdecl;
  TglPrimitiveBoundingBoxOES = procedure(minX: GLfloat; minY: GLfloat; minZ: GLfloat; minW: GLfloat; maxX: GLfloat; maxY: GLfloat; maxZ: GLfloat; maxW: GLfloat); extdecl;
  TglPrioritizeTextures = procedure(n: GLsizei; textures: PGLuint; priorities: PGLfloat); extdecl;
  TglProgramBinaryOES = procedure(program_: GLuint; binaryFormat: GLenum; binary: Pvoid; length: GLint); extdecl;
  TglProgramParameteriEXT = procedure(program_: GLuint; pname: GLenum; value: GLint); extdecl;
  TglProgramPathFragmentInputGenNV = procedure(program_: GLuint; location: GLint; genMode: GLenum; components: GLint; coeffs: PGLfloat); extdecl;
  TglProgramUniform1fEXT = procedure(program_: GLuint; location: GLint; v0: GLfloat); extdecl;
  TglProgramUniform1fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform1i64NV = procedure(program_: GLuint; location: GLint; x: GLint64EXT); extdecl;
  TglProgramUniform1i64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglProgramUniform1iEXT = procedure(program_: GLuint; location: GLint; v0: GLint); extdecl;
  TglProgramUniform1ivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform1ui64NV = procedure(program_: GLuint; location: GLint; x: GLuint64EXT); extdecl;
  TglProgramUniform1ui64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglProgramUniform1uiEXT = procedure(program_: GLuint; location: GLint; v0: GLuint); extdecl;
  TglProgramUniform1uivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform2fEXT = procedure(program_: GLuint; location: GLint; v0: GLfloat; v1: GLfloat); extdecl;
  TglProgramUniform2fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform2i64NV = procedure(program_: GLuint; location: GLint; x: GLint64EXT; y: GLint64EXT); extdecl;
  TglProgramUniform2i64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglProgramUniform2iEXT = procedure(program_: GLuint; location: GLint; v0: GLint; v1: GLint); extdecl;
  TglProgramUniform2ivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform2ui64NV = procedure(program_: GLuint; location: GLint; x: GLuint64EXT; y: GLuint64EXT); extdecl;
  TglProgramUniform2ui64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglProgramUniform2uiEXT = procedure(program_: GLuint; location: GLint; v0: GLuint; v1: GLuint); extdecl;
  TglProgramUniform2uivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform3fEXT = procedure(program_: GLuint; location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat); extdecl;
  TglProgramUniform3fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform3i64NV = procedure(program_: GLuint; location: GLint; x: GLint64EXT; y: GLint64EXT; z: GLint64EXT); extdecl;
  TglProgramUniform3i64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglProgramUniform3iEXT = procedure(program_: GLuint; location: GLint; v0: GLint; v1: GLint; v2: GLint); extdecl;
  TglProgramUniform3ivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform3ui64NV = procedure(program_: GLuint; location: GLint; x: GLuint64EXT; y: GLuint64EXT; z: GLuint64EXT); extdecl;
  TglProgramUniform3ui64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglProgramUniform3uiEXT = procedure(program_: GLuint; location: GLint; v0: GLuint; v1: GLuint; v2: GLuint); extdecl;
  TglProgramUniform3uivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniform4fEXT = procedure(program_: GLuint; location: GLint; v0: GLfloat; v1: GLfloat; v2: GLfloat; v3: GLfloat); extdecl;
  TglProgramUniform4fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLfloat); extdecl;
  TglProgramUniform4i64NV = procedure(program_: GLuint; location: GLint; x: GLint64EXT; y: GLint64EXT; z: GLint64EXT; w: GLint64EXT); extdecl;
  TglProgramUniform4i64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglProgramUniform4iEXT = procedure(program_: GLuint; location: GLint; v0: GLint; v1: GLint; v2: GLint; v3: GLint); extdecl;
  TglProgramUniform4ivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLint); extdecl;
  TglProgramUniform4ui64NV = procedure(program_: GLuint; location: GLint; x: GLuint64EXT; y: GLuint64EXT; z: GLuint64EXT; w: GLuint64EXT); extdecl;
  TglProgramUniform4ui64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglProgramUniform4uiEXT = procedure(program_: GLuint; location: GLint; v0: GLuint; v1: GLuint; v2: GLuint; v3: GLuint); extdecl;
  TglProgramUniform4uivEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; value: PGLuint); extdecl;
  TglProgramUniformHandleui64ARB = procedure(program_: GLuint; location: GLint; value: GLuint64); extdecl;
  TglProgramUniformHandleui64IMG = procedure(program_: GLuint; location: GLint; value: GLuint64); extdecl;
  TglProgramUniformHandleui64NV = procedure(program_: GLuint; location: GLint; value: GLuint64); extdecl;
  TglProgramUniformHandleui64vARB = procedure(program_: GLuint; location: GLint; count: GLsizei; values: PGLuint64); extdecl;
  TglProgramUniformHandleui64vIMG = procedure(program_: GLuint; location: GLint; count: GLsizei; values: PGLuint64); extdecl;
  TglProgramUniformHandleui64vNV = procedure(program_: GLuint; location: GLint; count: GLsizei; values: PGLuint64); extdecl;
  TglProgramUniformMatrix2fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix2x3fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix2x4fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix3fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix3x2fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix3x4fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix4fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix4x2fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProgramUniformMatrix4x3fvEXT = procedure(program_: GLuint; location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglProvokingVertex = procedure(mode: GLenum); extdecl;
  TglPushDebugGroupKHR = procedure(source: GLenum; id: GLuint; length: GLsizei; message_: PGLchar); extdecl;
  TglPushGroupMarkerEXT = procedure(length: GLsizei; marker: PGLchar); extdecl;
  TglQueryCounter = procedure(id: GLuint; target: GLenum); extdecl;
  TglQueryCounterEXT = procedure(id: GLuint; target: GLenum); extdecl;
  TglRasterSamplesEXT = procedure(samples: GLuint; fixedsamplelocations: GLboolean); extdecl;
  TglReadBufferIndexedEXT = procedure(src: GLenum; index_: GLint); extdecl;
  TglReadBufferNV = procedure(mode: GLenum); extdecl;
  TglReadnPixelsEXT = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; bufSize: GLsizei; data: Pvoid); extdecl;
  TglReadnPixelsKHR = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; bufSize: GLsizei; data: Pvoid); extdecl;
  TglReleaseKeyedMutexWin32EXT = function(memory: GLuint; key: GLuint64): GLboolean; extdecl;
  TglRenderbufferStorageMultisampleANGLE = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglRenderbufferStorageMultisampleAPPLE = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglRenderbufferStorageMultisampleAdvancedAMD = procedure(target: GLenum; samples: GLsizei; storageSamples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglRenderbufferStorageMultisampleEXT = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglRenderbufferStorageMultisampleIMG = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglRenderbufferStorageMultisampleNV = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglResetHistogram = procedure(target: GLenum); extdecl;
  TglResetMemoryObjectParameterNV = procedure(memory: GLuint; pname: GLenum); extdecl;
  TglResetMinmax = procedure(target: GLenum); extdecl;
  TglResolveDepthValuesNV = procedure; extdecl;
  TglResolveMultisampleFramebufferAPPLE = procedure; extdecl;
  TglSampleMaskEXT = procedure(value: GLclampf; invert: GLboolean); extdecl;
  TglSamplePatternEXT = procedure(pattern: GLenum); extdecl;
  TglSamplerParameterIivEXT = procedure(sampler: GLuint; pname: GLenum; param: PGLint); extdecl;
  TglSamplerParameterIivOES = procedure(sampler: GLuint; pname: GLenum; param: PGLint); extdecl;
  TglSamplerParameterIuivEXT = procedure(sampler: GLuint; pname: GLenum; param: PGLuint); extdecl;
  TglSamplerParameterIuivOES = procedure(sampler: GLuint; pname: GLenum; param: PGLuint); extdecl;
  TglScissorArrayv = procedure(first: GLuint; count: GLsizei; v: PGLint); extdecl;
  TglScissorArrayvNV = procedure(first: GLuint; count: GLsizei; v: PGLint); extdecl;
  TglScissorArrayvOES = procedure(first: GLuint; count: GLsizei; v: PGLint); extdecl;
  TglScissorExclusiveArrayvNV = procedure(first: GLuint; count: GLsizei; v: PGLint); extdecl;
  TglScissorExclusiveNV = procedure(x: GLint; y: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglScissorIndexed = procedure(index_: GLuint; left: GLint; bottom: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglScissorIndexedNV = procedure(index_: GLuint; left: GLint; bottom: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglScissorIndexedOES = procedure(index_: GLuint; left: GLint; bottom: GLint; width: GLsizei; height: GLsizei); extdecl;
  TglScissorIndexedv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglScissorIndexedvNV = procedure(index_: GLuint; v: PGLint); extdecl;
  TglScissorIndexedvOES = procedure(index_: GLuint; v: PGLint); extdecl;
  TglSecondaryColor3b = procedure(red: GLbyte; green: GLbyte; blue: GLbyte); extdecl;
  TglSecondaryColor3bv = procedure(v: PGLbyte); extdecl;
  TglSecondaryColor3d = procedure(red: GLdouble; green: GLdouble; blue: GLdouble); extdecl;
  TglSecondaryColor3dv = procedure(v: PGLdouble); extdecl;
  TglSecondaryColor3f = procedure(red: GLfloat; green: GLfloat; blue: GLfloat); extdecl;
  TglSecondaryColor3fv = procedure(v: PGLfloat); extdecl;
  TglSecondaryColor3i = procedure(red: GLint; green: GLint; blue: GLint); extdecl;
  TglSecondaryColor3iv = procedure(v: PGLint); extdecl;
  TglSecondaryColor3s = procedure(red: GLshort; green: GLshort; blue: GLshort); extdecl;
  TglSecondaryColor3sv = procedure(v: PGLshort); extdecl;
  TglSecondaryColor3ub = procedure(red: GLubyte; green: GLubyte; blue: GLubyte); extdecl;
  TglSecondaryColor3ubv = procedure(v: PGLubyte); extdecl;
  TglSecondaryColor3ui = procedure(red: GLuint; green: GLuint; blue: GLuint); extdecl;
  TglSecondaryColor3uiv = procedure(v: PGLuint); extdecl;
  TglSecondaryColor3us = procedure(red: GLushort; green: GLushort; blue: GLushort); extdecl;
  TglSecondaryColor3usv = procedure(v: PGLushort); extdecl;
  TglSecondaryColorPointer = procedure(size: GLint; type_: GLenum; stride: GLsizei; pointer: Pvoid); extdecl;
  TglSelectPerfMonitorCountersAMD = procedure(monitor: GLuint; enable: GLboolean; group: GLuint; numCounters: GLint; counterList: PGLuint); extdecl;
  TglSemaphoreParameterivNV = procedure(semaphore: GLuint; pname: GLenum; params: PGLint); extdecl;
  TglSemaphoreParameterui64vEXT = procedure(semaphore: GLuint; pname: GLenum; params: PGLuint64); extdecl;
  TglSeparableFilter2D = procedure(target: GLenum; internalformat: GLenum; width: GLsizei; height: GLsizei; format: GLenum; type_: GLenum; row: Pvoid; column: Pvoid); extdecl;
  TglSetFenceNV = procedure(fence: GLuint; condition: GLenum); extdecl;
  TglShadingRateImageBarrierNV = procedure(synchronize: GLboolean); extdecl;
  TglShadingRateQCOM = procedure(rate: GLenum); extdecl;
  TglShadingRateImagePaletteNV = procedure(viewport: GLuint; first: GLuint; count: GLsizei; rates: PGLenum); extdecl;
  TglShadingRateSampleOrderNV = procedure(order: GLenum); extdecl;
  TglShadingRateSampleOrderCustomNV = procedure(rate: GLenum; samples: GLuint; locations: PGLint); extdecl;
  TglSignalSemaphoreEXT = procedure(semaphore: GLuint; numBufferBarriers: GLuint; buffers: PGLuint; numTextureBarriers: GLuint; textures: PGLuint; dstLayouts: PGLenum); extdecl;
  TglSpecializeShader = procedure(shader: GLuint; pEntryPoint: PGLchar; numSpecializationConstants: GLuint; pConstantIndex: PGLuint; pConstantValue: PGLuint); extdecl;
  TglStartTilingQCOM = procedure(x: GLuint; y: GLuint; width: GLuint; height: GLuint; preserveMask: GLbitfield); extdecl;
  TglStencilFillPathInstancedNV = procedure(numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; fillMode: GLenum; mask: GLuint; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglStencilFillPathNV = procedure(path: GLuint; fillMode: GLenum; mask: GLuint); extdecl;
  TglStencilStrokePathInstancedNV = procedure(numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; reference: GLint; mask: GLuint; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglStencilStrokePathNV = procedure(path: GLuint; reference: GLint; mask: GLuint); extdecl;
  TglStencilThenCoverFillPathInstancedNV = procedure(numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; fillMode: GLenum; mask: GLuint; coverMode: GLenum; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglStencilThenCoverFillPathNV = procedure(path: GLuint; fillMode: GLenum; mask: GLuint; coverMode: GLenum); extdecl;
  TglStencilThenCoverStrokePathInstancedNV = procedure(numPaths: GLsizei; pathNameType: GLenum; paths: Pvoid; pathBase: GLuint; reference: GLint; mask: GLuint; coverMode: GLenum; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglStencilThenCoverStrokePathNV = procedure(path: GLuint; reference: GLint; mask: GLuint; coverMode: GLenum); extdecl;
  TglSubpixelPrecisionBiasNV = procedure(xbits: GLuint; ybits: GLuint); extdecl;
  TglTestFenceNV = function(fence: GLuint): GLboolean; extdecl;
  TglTexAttachMemoryNV = procedure(target: GLenum; memory: GLuint; offset: GLuint64); extdecl;
  TglTexBufferEXT = procedure(target: GLenum; internalformat: GLenum; buffer: GLuint); extdecl;
  TglTexBufferOES = procedure(target: GLenum; internalformat: GLenum; buffer: GLuint); extdecl;
  TglTexBufferRangeEXT = procedure(target: GLenum; internalformat: GLenum; buffer: GLuint; offset: GLintptr; size: GLsizeiptr); extdecl;
  TglTexBufferRangeOES = procedure(target: GLenum; internalformat: GLenum; buffer: GLuint; offset: GLintptr; size: GLsizeiptr); extdecl;
  TglTexEstimateMotionQCOM = procedure(ref: GLuint; target: GLuint; output: GLuint); extdecl;
  TglTexEstimateMotionRegionsQCOM = procedure(ref: GLuint; target: GLuint; output: GLuint; mask: GLuint); extdecl;
  TglTexImage3DOES = procedure(target: GLenum; level: GLint; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; border: GLint; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglTexPageCommitmentARB = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; commit: GLboolean); extdecl;
  TglTexPageCommitmentEXT = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; commit: GLboolean); extdecl;
  TglTexPageCommitmentMemNV = procedure(target: GLenum; layer: GLint; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; memory: GLuint; offset: GLuint64; commit: GLboolean); extdecl;
  TglTexParameterIivEXT = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglTexParameterIivOES = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
  TglTexParameterIuivEXT = procedure(target: GLenum; pname: GLenum; params: PGLuint); extdecl;
  TglTexParameterIuivOES = procedure(target: GLenum; pname: GLenum; params: PGLuint); extdecl;
  TglTexStorage1D = procedure(target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei); extdecl;
  TglTexStorage1DEXT = procedure(target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei); extdecl;
  TglTexStorage2DEXT = procedure(target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglTexStorage3DEXT = procedure(target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei); extdecl;
  TglTexStorage3DMultisampleOES = procedure(target: GLenum; samples: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; fixedsamplelocations: GLboolean); extdecl;
  TglTexStorageMem1DEXT = procedure(target: GLenum; levels: GLsizei; internalFormat: GLenum; width: GLsizei; memory: GLuint; offset: GLuint64); extdecl;
  TglTexStorageMem2DEXT = procedure(target: GLenum; levels: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; memory: GLuint; offset: GLuint64); extdecl;
  TglTexStorageMem2DMultisampleEXT = procedure(target: GLenum; samples: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; fixedSampleLocations: GLboolean; memory: GLuint; offset: GLuint64); extdecl;
  TglTexStorageMem3DEXT = procedure(target: GLenum; levels: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; memory: GLuint; offset: GLuint64); extdecl;
  TglTexStorageMem3DMultisampleEXT = procedure(target: GLenum; samples: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; fixedSampleLocations: GLboolean; memory: GLuint; offset: GLuint64); extdecl;
  TglTexSubImage1D = procedure(target: GLenum; level: GLint; xoffset: GLint; width: GLsizei; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglTexSubImage3DOES = procedure(target: GLenum; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; format: GLenum; type_: GLenum; pixels: Pvoid); extdecl;
  TglTextureAttachMemoryNV = procedure(texture: GLuint; memory: GLuint; offset: GLuint64); extdecl;
  TglTextureFoveationParametersQCOM = procedure(texture: GLuint; layer: GLuint; focalPoint: GLuint; focalX: GLfloat; focalY: GLfloat; gainX: GLfloat; gainY: GLfloat; foveaArea: GLfloat); extdecl;
  TglTexturePageCommitmentMemNV = procedure(texture: GLuint; layer: GLint; level: GLint; xoffset: GLint; yoffset: GLint; zoffset: GLint; width: GLsizei; height: GLsizei; depth: GLsizei; memory: GLuint; offset: GLuint64; commit: GLboolean); extdecl;
  TglTextureStorage1DEXT = procedure(texture: GLuint; target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei); extdecl;
  TglTextureStorage2DEXT = procedure(texture: GLuint; target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei); extdecl;
  TglTextureStorage3DEXT = procedure(texture: GLuint; target: GLenum; levels: GLsizei; internalformat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei); extdecl;
  TglTextureStorageMem1DEXT = procedure(texture: GLuint; levels: GLsizei; internalFormat: GLenum; width: GLsizei; memory: GLuint; offset: GLuint64); extdecl;
  TglTextureStorageMem2DEXT = procedure(texture: GLuint; levels: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; memory: GLuint; offset: GLuint64); extdecl;
  TglTextureStorageMem2DMultisampleEXT = procedure(texture: GLuint; samples: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; fixedSampleLocations: GLboolean; memory: GLuint; offset: GLuint64); extdecl;
  TglTextureStorageMem3DEXT = procedure(texture: GLuint; levels: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; memory: GLuint; offset: GLuint64); extdecl;
  TglTextureStorageMem3DMultisampleEXT = procedure(texture: GLuint; samples: GLsizei; internalFormat: GLenum; width: GLsizei; height: GLsizei; depth: GLsizei; fixedSampleLocations: GLboolean; memory: GLuint; offset: GLuint64); extdecl;
  TglTextureView = procedure(texture: GLuint; target: GLenum; origtexture: GLuint; internalformat: GLenum; minlevel: GLuint; numlevels: GLuint; minlayer: GLuint; numlayers: GLuint); extdecl;
  TglTextureViewEXT = procedure(texture: GLuint; target: GLenum; origtexture: GLuint; internalformat: GLenum; minlevel: GLuint; numlevels: GLuint; minlayer: GLuint; numlayers: GLuint); extdecl;
  TglTextureViewOES = procedure(texture: GLuint; target: GLenum; origtexture: GLuint; internalformat: GLenum; minlevel: GLuint; numlevels: GLuint; minlayer: GLuint; numlayers: GLuint); extdecl;
  TglTransformPathNV = procedure(resultPath: GLuint; srcPath: GLuint; transformType: GLenum; transformValues: PGLfloat); extdecl;
  TglUniform1i64NV = procedure(location: GLint; x: GLint64EXT); extdecl;
  TglUniform1i64vNV = procedure(location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglUniform1ui64NV = procedure(location: GLint; x: GLuint64EXT); extdecl;
  TglUniform1ui64vNV = procedure(location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglUniform2i64NV = procedure(location: GLint; x: GLint64EXT; y: GLint64EXT); extdecl;
  TglUniform2i64vNV = procedure(location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglUniform2ui64NV = procedure(location: GLint; x: GLuint64EXT; y: GLuint64EXT); extdecl;
  TglUniform2ui64vNV = procedure(location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglUniform3i64NV = procedure(location: GLint; x: GLint64EXT; y: GLint64EXT; z: GLint64EXT); extdecl;
  TglUniform3i64vNV = procedure(location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglUniform3ui64NV = procedure(location: GLint; x: GLuint64EXT; y: GLuint64EXT; z: GLuint64EXT); extdecl;
  TglUniform3ui64vNV = procedure(location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglUniform4i64NV = procedure(location: GLint; x: GLint64EXT; y: GLint64EXT; z: GLint64EXT; w: GLint64EXT); extdecl;
  TglUniform4i64vNV = procedure(location: GLint; count: GLsizei; value: PGLint64EXT); extdecl;
  TglUniform4ui64NV = procedure(location: GLint; x: GLuint64EXT; y: GLuint64EXT; z: GLuint64EXT; w: GLuint64EXT); extdecl;
  TglUniform4ui64vNV = procedure(location: GLint; count: GLsizei; value: PGLuint64EXT); extdecl;
  TglUniformHandleui64ARB = procedure(location: GLint; value: GLuint64); extdecl;
  TglUniformHandleui64IMG = procedure(location: GLint; value: GLuint64); extdecl;
  TglUniformHandleui64NV = procedure(location: GLint; value: GLuint64); extdecl;
  TglUniformHandleui64vARB = procedure(location: GLint; count: GLsizei; value: PGLuint64); extdecl;
  TglUniformHandleui64vIMG = procedure(location: GLint; count: GLsizei; value: PGLuint64); extdecl;
  TglUniformHandleui64vNV = procedure(location: GLint; count: GLsizei; value: PGLuint64); extdecl;
  TglUniformMatrix2x3fvNV = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix2x4fvNV = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix3x2fvNV = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix3x4fvNV = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix4x2fvNV = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUniformMatrix4x3fvNV = procedure(location: GLint; count: GLsizei; transpose: GLboolean; value: PGLfloat); extdecl;
  TglUnmapBufferOES = function(target: GLenum): GLboolean; extdecl;
  TglUseProgramStagesEXT = procedure(pipeline: GLuint; stages: GLbitfield; program_: GLuint); extdecl;
  TglUseShaderProgramEXT = procedure(type_: GLenum; program_: GLuint); extdecl;
  TglValidateProgramPipelineEXT = procedure(pipeline: GLuint); extdecl;
  TglVertexAttrib1d = procedure(index_: GLuint; x: GLdouble); extdecl;
  TglVertexAttrib1dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttrib1s = procedure(index_: GLuint; x: GLshort); extdecl;
  TglVertexAttrib1sv = procedure(index_: GLuint; v: PGLshort); extdecl;
  TglVertexAttrib2d = procedure(index_: GLuint; x: GLdouble; y: GLdouble); extdecl;
  TglVertexAttrib2dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttrib2s = procedure(index_: GLuint; x: GLshort; y: GLshort); extdecl;
  TglVertexAttrib2sv = procedure(index_: GLuint; v: PGLshort); extdecl;
  TglVertexAttrib3d = procedure(index_: GLuint; x: GLdouble; y: GLdouble; z: GLdouble); extdecl;
  TglVertexAttrib3dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttrib3s = procedure(index_: GLuint; x: GLshort; y: GLshort; z: GLshort); extdecl;
  TglVertexAttrib3sv = procedure(index_: GLuint; v: PGLshort); extdecl;
  TglVertexAttrib4Nbv = procedure(index_: GLuint; v: PGLbyte); extdecl;
  TglVertexAttrib4Niv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglVertexAttrib4Nsv = procedure(index_: GLuint; v: PGLshort); extdecl;
  TglVertexAttrib4Nub = procedure(index_: GLuint; x: GLubyte; y: GLubyte; z: GLubyte; w: GLubyte); extdecl;
  TglVertexAttrib4Nubv = procedure(index_: GLuint; v: PGLubyte); extdecl;
  TglVertexAttrib4Nuiv = procedure(index_: GLuint; v: PGLuint); extdecl;
  TglVertexAttrib4Nusv = procedure(index_: GLuint; v: PGLushort); extdecl;
  TglVertexAttrib4bv = procedure(index_: GLuint; v: PGLbyte); extdecl;
  TglVertexAttrib4d = procedure(index_: GLuint; x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble); extdecl;
  TglVertexAttrib4dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttrib4iv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglVertexAttrib4s = procedure(index_: GLuint; x: GLshort; y: GLshort; z: GLshort; w: GLshort); extdecl;
  TglVertexAttrib4sv = procedure(index_: GLuint; v: PGLshort); extdecl;
  TglVertexAttrib4ubv = procedure(index_: GLuint; v: PGLubyte); extdecl;
  TglVertexAttrib4uiv = procedure(index_: GLuint; v: PGLuint); extdecl;
  TglVertexAttrib4usv = procedure(index_: GLuint; v: PGLushort); extdecl;
  TglVertexAttribDivisorANGLE = procedure(index_: GLuint; divisor: GLuint); extdecl;
  TglVertexAttribDivisorEXT = procedure(index_: GLuint; divisor: GLuint); extdecl;
  TglVertexAttribDivisorNV = procedure(index_: GLuint; divisor: GLuint); extdecl;
  TglVertexAttribI1i = procedure(index_: GLuint; x: GLint); extdecl;
  TglVertexAttribI1iv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglVertexAttribI1ui = procedure(index_: GLuint; x: GLuint); extdecl;
  TglVertexAttribI1uiv = procedure(index_: GLuint; v: PGLuint); extdecl;
  TglVertexAttribI2i = procedure(index_: GLuint; x: GLint; y: GLint); extdecl;
  TglVertexAttribI2iv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglVertexAttribI2ui = procedure(index_: GLuint; x: GLuint; y: GLuint); extdecl;
  TglVertexAttribI2uiv = procedure(index_: GLuint; v: PGLuint); extdecl;
  TglVertexAttribI3i = procedure(index_: GLuint; x: GLint; y: GLint; z: GLint); extdecl;
  TglVertexAttribI3iv = procedure(index_: GLuint; v: PGLint); extdecl;
  TglVertexAttribI3ui = procedure(index_: GLuint; x: GLuint; y: GLuint; z: GLuint); extdecl;
  TglVertexAttribI3uiv = procedure(index_: GLuint; v: PGLuint); extdecl;
  TglVertexAttribI4bv = procedure(index_: GLuint; v: PGLbyte); extdecl;
  TglVertexAttribI4sv = procedure(index_: GLuint; v: PGLshort); extdecl;
  TglVertexAttribI4ubv = procedure(index_: GLuint; v: PGLubyte); extdecl;
  TglVertexAttribI4usv = procedure(index_: GLuint; v: PGLushort); extdecl;
  TglVertexAttribL1d = procedure(index_: GLuint; x: GLdouble); extdecl;
  TglVertexAttribL1dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttribL2d = procedure(index_: GLuint; x: GLdouble; y: GLdouble); extdecl;
  TglVertexAttribL2dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttribL3d = procedure(index_: GLuint; x: GLdouble; y: GLdouble; z: GLdouble); extdecl;
  TglVertexAttribL3dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttribL4d = procedure(index_: GLuint; x: GLdouble; y: GLdouble; z: GLdouble; w: GLdouble); extdecl;
  TglVertexAttribL4dv = procedure(index_: GLuint; v: PGLdouble); extdecl;
  TglVertexAttribLPointer = procedure(index_: GLuint; size: GLint; type_: GLenum; stride: GLsizei; pointer: Pvoid); extdecl;
  TglViewportArrayv = procedure(first: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglViewportArrayvNV = procedure(first: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglViewportArrayvOES = procedure(first: GLuint; count: GLsizei; v: PGLfloat); extdecl;
  TglViewportIndexedf = procedure(index_: GLuint; x: GLfloat; y: GLfloat; w: GLfloat; h: GLfloat); extdecl;
  TglViewportIndexedfOES = procedure(index_: GLuint; x: GLfloat; y: GLfloat; w: GLfloat; h: GLfloat); extdecl;
  TglViewportIndexedfNV = procedure(index_: GLuint; x: GLfloat; y: GLfloat; w: GLfloat; h: GLfloat); extdecl;
  TglViewportIndexedfv = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglViewportIndexedfvOES = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglViewportIndexedfvNV = procedure(index_: GLuint; v: PGLfloat); extdecl;
  TglViewportPositionWScaleNV = procedure(index_: GLuint; xcoeff: GLfloat; ycoeff: GLfloat); extdecl;
  TglViewportSwizzleNV = procedure(index_: GLuint; swizzlex: GLenum; swizzley: GLenum; swizzlez: GLenum; swizzlew: GLenum); extdecl;
  TglWaitSemaphoreEXT = procedure(semaphore: GLuint; numBufferBarriers: GLuint; buffers: PGLuint; numTextureBarriers: GLuint; textures: PGLuint; srcLayouts: PGLenum); extdecl;
  TglWaitSyncAPPLE = procedure(sync: GLsync; flags: GLbitfield; timeout: GLuint64); extdecl;
  TglWeightPathsNV = procedure(resultPath: GLuint; numPaths: GLsizei; paths: PGLuint; weights: PGLfloat); extdecl;
  TglWindowPos2d = procedure(x: GLdouble; y: GLdouble); extdecl;
  TglWindowPos2dv = procedure(v: PGLdouble); extdecl;
  TglWindowPos2f = procedure(x: GLfloat; y: GLfloat); extdecl;
  TglWindowPos2fv = procedure(v: PGLfloat); extdecl;
  TglWindowPos2i = procedure(x: GLint; y: GLint); extdecl;
  TglWindowPos2iv = procedure(v: PGLint); extdecl;
  TglWindowPos2s = procedure(x: GLshort; y: GLshort); extdecl;
  TglWindowPos2sv = procedure(v: PGLshort); extdecl;
  TglWindowPos3d = procedure(x: GLdouble; y: GLdouble; z: GLdouble); extdecl;
  TglWindowPos3dv = procedure(v: PGLdouble); extdecl;
  TglWindowPos3f = procedure(x: GLfloat; y: GLfloat; z: GLfloat); extdecl;
  TglWindowPos3fv = procedure(v: PGLfloat); extdecl;
  TglWindowPos3i = procedure(x: GLint; y: GLint; z: GLint); extdecl;
  TglWindowPos3iv = procedure(v: PGLint); extdecl;
  TglWindowPos3s = procedure(x: GLshort; y: GLshort; z: GLshort); extdecl;
  TglWindowPos3sv = procedure(v: PGLshort); extdecl;
  TglWindowRectanglesEXT = procedure(mode: GLenum; count: GLsizei; box: PGLint); extdecl;
  TglDrawVkImageNV = procedure(vkImage: GLuint64; sampler: GLuint; x0: GLfloat; y0: GLfloat; x1: GLfloat; y1: GLfloat; z: GLfloat; s0: GLfloat; t0: GLfloat; s1: GLfloat; t1: GLfloat); extdecl;
  TglGetVkProcAddrNV = function(name_: PGLchar): GLVULKANPROCNV; extdecl;
  TglWaitVkSemaphoreNV = procedure(vkSemaphore: GLuint64); extdecl;
  TglSignalVkSemaphoreNV = procedure(vkSemaphore: GLuint64); extdecl;
  TglSignalVkFenceNV = procedure(vkFence: GLuint64); extdecl;
  TglFramebufferParameteriMESA = procedure(target: GLenum; pname: GLenum; param: GLint); extdecl;
  TglGetFramebufferParameterivMESA = procedure(target: GLenum; pname: GLenum; params: PGLint); extdecl;
 
//---- EGL ---------
 
//------- EGL_VERSION_1_0----------------
  TeglChooseConfig = function(dpy: EGLDisplay; attrib_list: PEGLint; configs: PEGLConfig; config_size: EGLint; num_config: PEGLint): EGLBoolean; extdecl;
  TeglCopyBuffers = function(dpy: EGLDisplay; surface: EGLSurface; target: EGLNativePixmapType): EGLBoolean; extdecl;
  TeglCreateContext = function(dpy: EGLDisplay; config: EGLConfig; share_context: EGLContext; attrib_list: PEGLint): EGLContext; extdecl;
  TeglCreatePbufferSurface = function(dpy: EGLDisplay; config: EGLConfig; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglCreatePixmapSurface = function(dpy: EGLDisplay; config: EGLConfig; pixmap: EGLNativePixmapType; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglCreateWindowSurface = function(dpy: EGLDisplay; config: EGLConfig; win: EGLNativeWindowType; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglDestroyContext = function(dpy: EGLDisplay; ctx: EGLContext): EGLBoolean; extdecl;
  TeglDestroySurface = function(dpy: EGLDisplay; surface: EGLSurface): EGLBoolean; extdecl;
  TeglGetConfigAttrib = function(dpy: EGLDisplay; config: EGLConfig; attribute: EGLint; value: PEGLint): EGLBoolean; extdecl;
  TeglGetConfigs = function(dpy: EGLDisplay; configs: PEGLConfig; config_size: EGLint; num_config: PEGLint): EGLBoolean; extdecl;
  TeglGetCurrentDisplay = function: EGLDisplay; extdecl;
  TeglGetCurrentSurface = function(readdraw: EGLint): EGLSurface; extdecl;
  TeglGetDisplay = function(display_id: EGLNativeDisplayType): EGLDisplay; extdecl;
  TeglGetError = function: EGLint; extdecl;
  TeglGetProcAddress = function(procname: Pchar): __eglMustCastToProperFunctionPointerType; extdecl;
  TeglInitialize = function(dpy: EGLDisplay; major: PEGLint; minor: PEGLint): EGLBoolean; extdecl;
  TeglMakeCurrent = function(dpy: EGLDisplay; draw: EGLSurface; read_: EGLSurface; ctx: EGLContext): EGLBoolean; extdecl;
  TeglQueryContext = function(dpy: EGLDisplay; ctx: EGLContext; attribute: EGLint; value: PEGLint): EGLBoolean; extdecl;
  TeglQueryString = function(dpy: EGLDisplay; name_: EGLint): Pchar; extdecl;
  TeglQuerySurface = function(dpy: EGLDisplay; surface: EGLSurface; attribute: EGLint; value: PEGLint): EGLBoolean; extdecl;
  TeglSwapBuffers = function(dpy: EGLDisplay; surface: EGLSurface): EGLBoolean; extdecl;
  TeglTerminate = function(dpy: EGLDisplay): EGLBoolean; extdecl;
  TeglWaitGL = function: EGLBoolean; extdecl;
  TeglWaitNative = function(engine: EGLint): EGLBoolean; extdecl;
 
//------- EGL_VERSION_1_1----------------
  TeglBindTexImage = function(dpy: EGLDisplay; surface: EGLSurface; buffer: EGLint): EGLBoolean; extdecl;
  TeglReleaseTexImage = function(dpy: EGLDisplay; surface: EGLSurface; buffer: EGLint): EGLBoolean; extdecl;
  TeglSurfaceAttrib = function(dpy: EGLDisplay; surface: EGLSurface; attribute: EGLint; value: EGLint): EGLBoolean; extdecl;
  TeglSwapInterval = function(dpy: EGLDisplay; interval: EGLint): EGLBoolean; extdecl;
 
//------- EGL_VERSION_1_2----------------
  TeglBindAPI = function(api: EGLenum): EGLBoolean; extdecl;
  TeglQueryAPI = function: EGLenum; extdecl;
  TeglCreatePbufferFromClientBuffer = function(dpy: EGLDisplay; buftype: EGLenum; buffer: EGLClientBuffer; config: EGLConfig; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglReleaseThread = function: EGLBoolean; extdecl;
  TeglWaitClient = function: EGLBoolean; extdecl;
 
//------- EGL_VERSION_1_3----------------
 
//------- EGL_VERSION_1_4----------------
  TeglGetCurrentContext = function: EGLContext; extdecl;
 
//------- EGL_VERSION_1_5----------------
  TeglCreateSync = function(dpy: EGLDisplay; type_: EGLenum; attrib_list: PEGLAttrib): EGLSync; extdecl;
  TeglDestroySync = function(dpy: EGLDisplay; sync: EGLSync): EGLBoolean; extdecl;
  TeglClientWaitSync = function(dpy: EGLDisplay; sync: EGLSync; flags: EGLint; timeout: EGLTime): EGLint; extdecl;
  TeglGetSyncAttrib = function(dpy: EGLDisplay; sync: EGLSync; attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglCreateImage = function(dpy: EGLDisplay; ctx: EGLContext; target: EGLenum; buffer: EGLClientBuffer; attrib_list: PEGLAttrib): EGLImage; extdecl;
  TeglDestroyImage = function(dpy: EGLDisplay; image: EGLImage): EGLBoolean; extdecl;
  TeglGetPlatformDisplay = function(platform_: EGLenum; native_display: Pvoid; attrib_list: PEGLAttrib): EGLDisplay; extdecl;
  TeglCreatePlatformWindowSurface = function(dpy: EGLDisplay; config: EGLConfig; native_window: Pvoid; attrib_list: PEGLAttrib): EGLSurface; extdecl;
  TeglCreatePlatformPixmapSurface = function(dpy: EGLDisplay; config: EGLConfig; native_pixmap: Pvoid; attrib_list: PEGLAttrib): EGLSurface; extdecl;
  TeglWaitSync = function(dpy: EGLDisplay; sync: EGLSync; flags: EGLint): EGLBoolean; extdecl;
 
//------- Other ----------------
  TeglClientSignalSyncEXT = function(dpy: EGLDisplay; sync: EGLSync; attrib_list: PEGLAttrib): EGLBoolean; extdecl;
  TeglClientWaitSyncKHR = function(dpy: EGLDisplay; sync: EGLSyncKHR; flags: EGLint; timeout: EGLTimeKHR): EGLint; extdecl;
  TeglClientWaitSyncNV = function(sync: EGLSyncNV; flags: EGLint; timeout: EGLTimeNV): EGLint; extdecl;
  TeglCreateDRMImageMESA = function(dpy: EGLDisplay; attrib_list: PEGLint): EGLImageKHR; extdecl;
  TeglCreateFenceSyncNV = function(dpy: EGLDisplay; condition: EGLenum; attrib_list: PEGLint): EGLSyncNV; extdecl;
  TeglCreateImageKHR = function(dpy: EGLDisplay; ctx: EGLContext; target: EGLenum; buffer: EGLClientBuffer; attrib_list: PEGLint): EGLImageKHR; extdecl;
  TeglCreateNativeClientBufferANDROID = function(attrib_list: PEGLint): EGLClientBuffer; extdecl;
  TeglCreatePixmapSurfaceHI = function(dpy: EGLDisplay; config: EGLConfig; pixmap: PstructEGLClientPixmapHI): EGLSurface; extdecl;
  TeglCreatePlatformPixmapSurfaceEXT = function(dpy: EGLDisplay; config: EGLConfig; native_pixmap: Pvoid; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglCreatePlatformWindowSurfaceEXT = function(dpy: EGLDisplay; config: EGLConfig; native_window: Pvoid; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglCreateStreamFromFileDescriptorKHR = function(dpy: EGLDisplay; file_descriptor: EGLNativeFileDescriptorKHR): EGLStreamKHR; extdecl;
  TeglCreateStreamKHR = function(dpy: EGLDisplay; attrib_list: PEGLint): EGLStreamKHR; extdecl;
  TeglCreateStreamAttribKHR = function(dpy: EGLDisplay; attrib_list: PEGLAttrib): EGLStreamKHR; extdecl;
  TeglCreateStreamProducerSurfaceKHR = function(dpy: EGLDisplay; config: EGLConfig; stream: EGLStreamKHR; attrib_list: PEGLint): EGLSurface; extdecl;
  TeglCreateStreamSyncNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; type_: EGLenum; attrib_list: PEGLint): EGLSyncKHR; extdecl;
  TeglCreateSyncKHR = function(dpy: EGLDisplay; type_: EGLenum; attrib_list: PEGLint): EGLSyncKHR; extdecl;
  TeglCreateSync64KHR = function(dpy: EGLDisplay; type_: EGLenum; attrib_list: PEGLAttribKHR): EGLSyncKHR; extdecl;
  TeglDebugMessageControlKHR = function(callback: EGLDEBUGPROCKHR; attrib_list: PEGLAttrib): EGLint; extdecl;
  TeglDestroyImageKHR = function(dpy: EGLDisplay; image: EGLImageKHR): EGLBoolean; extdecl;
  TeglDestroyStreamKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLBoolean; extdecl;
  TeglDestroySyncKHR = function(dpy: EGLDisplay; sync: EGLSyncKHR): EGLBoolean; extdecl;
  TeglDestroySyncNV = function(sync: EGLSyncNV): EGLBoolean; extdecl;
  TeglDupNativeFenceFDANDROID = function(dpy: EGLDisplay; sync: EGLSyncKHR): EGLint; extdecl;
  TeglExportDMABUFImageMESA = function(dpy: EGLDisplay; image: EGLImageKHR; fds: PGLint; strides: PEGLint; offsets: PEGLint): EGLBoolean; extdecl;
  TeglExportDMABUFImageQueryMESA = function(dpy: EGLDisplay; image: EGLImageKHR; fourcc: PGLint; num_planes: PGLint; modifiers: PEGLuint64KHR): EGLBoolean; extdecl;
  TeglExportDRMImageMESA = function(dpy: EGLDisplay; image: EGLImageKHR; name_: PEGLint; handle: PEGLint; stride: PEGLint): EGLBoolean; extdecl;
  TeglFenceNV = function(sync: EGLSyncNV): EGLBoolean; extdecl;
  TeglGetDisplayDriverConfig = function(dpy: EGLDisplay): Pchar; extdecl;
  TeglGetDisplayDriverName = function(dpy: EGLDisplay): Pchar; extdecl;
  TeglGetNativeClientBufferANDROID = function(buffer: PstructAHardwareBuffer): EGLClientBuffer; extdecl;
  TeglGetOutputLayersEXT = function(dpy: EGLDisplay; attrib_list: PEGLAttrib; layers: PEGLOutputLayerEXT; max_layers: EGLint; num_layers: PEGLint): EGLBoolean; extdecl;
  TeglGetOutputPortsEXT = function(dpy: EGLDisplay; attrib_list: PEGLAttrib; ports: PEGLOutputPortEXT; max_ports: EGLint; num_ports: PEGLint): EGLBoolean; extdecl;
  TeglGetPlatformDisplayEXT = function(platform_: EGLenum; native_display: Pvoid; attrib_list: PEGLint): EGLDisplay; extdecl;
  TeglGetStreamFileDescriptorKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLNativeFileDescriptorKHR; extdecl;
  TeglGetSyncAttribKHR = function(dpy: EGLDisplay; sync: EGLSyncKHR; attribute: EGLint; value: PEGLint): EGLBoolean; extdecl;
  TeglGetSyncAttribNV = function(sync: EGLSyncNV; attribute: EGLint; value: PEGLint): EGLBoolean; extdecl;
  TeglGetSystemTimeFrequencyNV = function: EGLuint64NV; extdecl;
  TeglGetSystemTimeNV = function: EGLuint64NV; extdecl;
  TeglLabelObjectKHR = function(display: EGLDisplay; objectType: EGLenum; object_: EGLObjectKHR; label_: EGLLabelKHR): EGLint; extdecl;
  TeglLockSurfaceKHR = function(dpy: EGLDisplay; surface: EGLSurface; attrib_list: PEGLint): EGLBoolean; extdecl;
  TeglOutputLayerAttribEXT = function(dpy: EGLDisplay; layer: EGLOutputLayerEXT; attribute: EGLint; value: EGLAttrib): EGLBoolean; extdecl;
  TeglOutputPortAttribEXT = function(dpy: EGLDisplay; port: EGLOutputPortEXT; attribute: EGLint; value: EGLAttrib): EGLBoolean; extdecl;
  TeglPostSubBufferNV = function(dpy: EGLDisplay; surface: EGLSurface; x: EGLint; y: EGLint; width: EGLint; height: EGLint): EGLBoolean; extdecl;
  TeglPresentationTimeANDROID = function(dpy: EGLDisplay; surface: EGLSurface; time: EGLnsecsANDROID): EGLBoolean; extdecl;
  TeglGetCompositorTimingSupportedANDROID = function(dpy: EGLDisplay; surface: EGLSurface; name_: EGLint): EGLBoolean; extdecl;
  TeglGetCompositorTimingANDROID = function(dpy: EGLDisplay; surface: EGLSurface; numTimestamps: EGLint; names: PEGLint; values: PEGLnsecsANDROID): EGLBoolean; extdecl;
  TeglGetNextFrameIdANDROID = function(dpy: EGLDisplay; surface: EGLSurface; frameId: PEGLuint64KHR): EGLBoolean; extdecl;
  TeglGetFrameTimestampSupportedANDROID = function(dpy: EGLDisplay; surface: EGLSurface; timestamp: EGLint): EGLBoolean; extdecl;
  TeglGetFrameTimestampsANDROID = function(dpy: EGLDisplay; surface: EGLSurface; frameId: EGLuint64KHR; numTimestamps: EGLint; timestamps: PEGLint; values: PEGLnsecsANDROID): EGLBoolean; extdecl;
  TeglQueryDebugKHR = function(attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryDeviceAttribEXT = function(device: EGLDeviceEXT; attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryDeviceStringEXT = function(device: EGLDeviceEXT; name_: EGLint): Pchar; extdecl;
  TeglQueryDevicesEXT = function(max_devices: EGLint; devices: PEGLDeviceEXT; num_devices: PEGLint): EGLBoolean; extdecl;
  TeglQueryDisplayAttribEXT = function(dpy: EGLDisplay; attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryDisplayAttribKHR = function(dpy: EGLDisplay; name_: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryDisplayAttribNV = function(dpy: EGLDisplay; attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryDmaBufFormatsEXT = function(dpy: EGLDisplay; max_formats: EGLint; formats: PEGLint; num_formats: PEGLint): EGLBoolean; extdecl;
  TeglQueryDmaBufModifiersEXT = function(dpy: EGLDisplay; format: EGLint; max_modifiers: EGLint; modifiers: PEGLuint64KHR; external_only: PEGLBoolean; num_modifiers: PEGLint): EGLBoolean; extdecl;
  TeglQueryNativeDisplayNV = function(dpy: EGLDisplay; display_id: PEGLNativeDisplayType): EGLBoolean; extdecl;
  TeglQueryNativePixmapNV = function(dpy: EGLDisplay; surf: EGLSurface; pixmap: PEGLNativePixmapType): EGLBoolean; extdecl;
  TeglQueryNativeWindowNV = function(dpy: EGLDisplay; surf: EGLSurface; window: PEGLNativeWindowType): EGLBoolean; extdecl;
  TeglQueryOutputLayerAttribEXT = function(dpy: EGLDisplay; layer: EGLOutputLayerEXT; attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryOutputLayerStringEXT = function(dpy: EGLDisplay; layer: EGLOutputLayerEXT; name_: EGLint): Pchar; extdecl;
  TeglQueryOutputPortAttribEXT = function(dpy: EGLDisplay; port: EGLOutputPortEXT; attribute: EGLint; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryOutputPortStringEXT = function(dpy: EGLDisplay; port: EGLOutputPortEXT; name_: EGLint): Pchar; extdecl;
  TeglQueryStreamKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attribute: EGLenum; value: PEGLint): EGLBoolean; extdecl;
  TeglQueryStreamAttribKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attribute: EGLenum; value: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryStreamMetadataNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; name_: EGLenum; n: EGLint; offset: EGLint; size: EGLint; data: Pvoid): EGLBoolean; extdecl;
  TeglQueryStreamTimeKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attribute: EGLenum; value: PEGLTimeKHR): EGLBoolean; extdecl;
  TeglQueryStreamu64KHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attribute: EGLenum; value: PEGLuint64KHR): EGLBoolean; extdecl;
  TeglQuerySurface64KHR = function(dpy: EGLDisplay; surface: EGLSurface; attribute: EGLint; value: PEGLAttribKHR): EGLBoolean; extdecl;
  TeglQuerySurfacePointerANGLE = function(dpy: EGLDisplay; surface: EGLSurface; attribute: EGLint; value: PPvoid): EGLBoolean; extdecl;
  TeglResetStreamNV = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLBoolean; extdecl;
  TeglSetBlobCacheFuncsANDROID = procedure(dpy: EGLDisplay; set_: EGLSetBlobFuncANDROID; get: EGLGetBlobFuncANDROID); extdecl;
  TeglSetDamageRegionKHR = function(dpy: EGLDisplay; surface: EGLSurface; rects: PEGLint; n_rects: EGLint): EGLBoolean; extdecl;
  TeglSetStreamAttribKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attribute: EGLenum; value: EGLAttrib): EGLBoolean; extdecl;
  TeglSetStreamMetadataNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; n: EGLint; offset: EGLint; size: EGLint; data: Pvoid): EGLBoolean; extdecl;
  TeglSignalSyncKHR = function(dpy: EGLDisplay; sync: EGLSyncKHR; mode: EGLenum): EGLBoolean; extdecl;
  TeglSignalSyncNV = function(sync: EGLSyncNV; mode: EGLenum): EGLBoolean; extdecl;
  TeglStreamAttribKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attribute: EGLenum; value: EGLint): EGLBoolean; extdecl;
  TeglStreamConsumerAcquireKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLBoolean; extdecl;
  TeglStreamConsumerAcquireAttribKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attrib_list: PEGLAttrib): EGLBoolean; extdecl;
  TeglStreamConsumerGLTextureExternalKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLBoolean; extdecl;
  TeglStreamConsumerGLTextureExternalAttribsNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; attrib_list: PEGLAttrib): EGLBoolean; extdecl;
  TeglStreamConsumerOutputEXT = function(dpy: EGLDisplay; stream: EGLStreamKHR; layer: EGLOutputLayerEXT): EGLBoolean; extdecl;
  TeglStreamConsumerReleaseKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLBoolean; extdecl;
  TeglStreamConsumerReleaseAttribKHR = function(dpy: EGLDisplay; stream: EGLStreamKHR; attrib_list: PEGLAttrib): EGLBoolean; extdecl;
  TeglStreamFlushNV = function(dpy: EGLDisplay; stream: EGLStreamKHR): EGLBoolean; extdecl;
  TeglSwapBuffersWithDamageEXT = function(dpy: EGLDisplay; surface: EGLSurface; rects: PEGLint; n_rects: EGLint): EGLBoolean; extdecl;
  TeglSwapBuffersWithDamageKHR = function(dpy: EGLDisplay; surface: EGLSurface; rects: PEGLint; n_rects: EGLint): EGLBoolean; extdecl;
  TeglSwapBuffersRegionNOK = function(dpy: EGLDisplay; surface: EGLSurface; numRects: EGLint; rects: PEGLint): EGLBoolean; extdecl;
  TeglSwapBuffersRegion2NOK = function(dpy: EGLDisplay; surface: EGLSurface; numRects: EGLint; rects: PEGLint): EGLBoolean; extdecl;
  TeglUnlockSurfaceKHR = function(dpy: EGLDisplay; surface: EGLSurface): EGLBoolean; extdecl;
  TeglUnsignalSyncEXT = function(dpy: EGLDisplay; sync: EGLSync; attrib_list: PEGLAttrib): EGLBoolean; extdecl;
  TeglWaitSyncKHR = function(dpy: EGLDisplay; sync: EGLSyncKHR; flags: EGLint): EGLint; extdecl;
  TeglCompositorSetContextListEXT = function(external_ref_ids: PEGLint; num_entries: EGLint): EGLBoolean; extdecl;
  TeglCompositorSetContextAttributesEXT = function(external_ref_id: EGLint; context_attributes: PEGLint; num_entries: EGLint): EGLBoolean; extdecl;
  TeglCompositorSetWindowListEXT = function(external_ref_id: EGLint; external_win_ids: PEGLint; num_entries: EGLint): EGLBoolean; extdecl;
  TeglCompositorSetWindowAttributesEXT = function(external_win_id: EGLint; window_attributes: PEGLint; num_entries: EGLint): EGLBoolean; extdecl;
  TeglCompositorBindTexWindowEXT = function(external_win_id: EGLint): EGLBoolean; extdecl;
  TeglCompositorSetSizeEXT = function(external_win_id: EGLint; width: EGLint; height: EGLint): EGLBoolean; extdecl;
  TeglCompositorSwapPolicyEXT = function(external_win_id: EGLint; policy: EGLint): EGLBoolean; extdecl;
  TeglBindWaylandDisplayWL = function(dpy: EGLDisplay; display: Pstructwl_display): EGLBoolean; extdecl;
  TeglUnbindWaylandDisplayWL = function(dpy: EGLDisplay; display: Pstructwl_display): EGLBoolean; extdecl;
  TeglQueryWaylandBufferWL = function(dpy: EGLDisplay; buffer: Pstructwl_resource; attribute: EGLint; value: PEGLint): EGLBoolean; extdecl;
  TeglCreateWaylandBufferFromImageWL = function(dpy: EGLDisplay; image: EGLImageKHR): Pstructwl_buffer; extdecl;
  TeglStreamImageConsumerConnectNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; num_modifiers: EGLint; modifiers: PEGLuint64KHR; attrib_list: PEGLAttrib): EGLBoolean; extdecl;
  TeglQueryStreamConsumerEventNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; timeout: EGLTime; event: PEGLenum; aux: PEGLAttrib): EGLint; extdecl;
  TeglStreamAcquireImageNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; pImage: PEGLImage; sync: EGLSync): EGLBoolean; extdecl;
  TeglStreamReleaseImageNV = function(dpy: EGLDisplay; stream: EGLStreamKHR; image: EGLImage; sync: EGLSync): EGLBoolean; extdecl;
 
 
//===================== Commands Variables =======================================
 
var
 
 
//------- GL_ES_VERSION_2_0----------------
  glActiveTexture: TglActiveTexture = nil; //alias: glActiveTextureARB
  glAttachShader: TglAttachShader = nil; //alias: glAttachObjectARB
  glBindAttribLocation: TglBindAttribLocation = nil; //alias: glBindAttribLocationARB
  glBindBuffer: TglBindBuffer = nil; //alias: glBindBufferARB
  glBindFramebuffer: TglBindFramebuffer = nil;
  glBindRenderbuffer: TglBindRenderbuffer = nil;
  glBindTexture: TglBindTexture = nil; //alias: glBindTextureEXT
  glBlendColor: TglBlendColor = nil; //alias: glBlendColorEXT
  glBlendEquation: TglBlendEquation = nil; //alias: glBlendEquationEXT
  glBlendEquationSeparate: TglBlendEquationSeparate = nil; //alias: glBlendEquationSeparateEXT
  glBlendFunc: TglBlendFunc = nil;
  glBlendFuncSeparate: TglBlendFuncSeparate = nil; //alias: glBlendFuncSeparateEXT
  glBufferData: TglBufferData = nil; //alias: glBufferDataARB
  glBufferSubData: TglBufferSubData = nil; //alias: glBufferSubDataARB
  glCheckFramebufferStatus: TglCheckFramebufferStatus = nil; //alias: glCheckFramebufferStatusEXT
  glClear: TglClear = nil;
  glClearColor: TglClearColor = nil;
  glClearDepthf: TglClearDepthf = nil; //alias: glClearDepthfOES
  glClearStencil: TglClearStencil = nil;
  glColorMask: TglColorMask = nil;
  glCompileShader: TglCompileShader = nil; //alias: glCompileShaderARB
  glCompressedTexImage2D: TglCompressedTexImage2D = nil; //alias: glCompressedTexImage2DARB
  glCompressedTexSubImage2D: TglCompressedTexSubImage2D = nil; //alias: glCompressedTexSubImage2DARB
  glCopyTexImage2D: TglCopyTexImage2D = nil; //alias: glCopyTexImage2DEXT
  glCopyTexSubImage2D: TglCopyTexSubImage2D = nil; //alias: glCopyTexSubImage2DEXT
  glCreateProgram: TglCreateProgram = nil; //alias: glCreateProgramObjectARB
  glCreateShader: TglCreateShader = nil; //alias: glCreateShaderObjectARB
  glCullFace: TglCullFace = nil;
  glDeleteBuffers: TglDeleteBuffers = nil; //alias: glDeleteBuffersARB
  glDeleteFramebuffers: TglDeleteFramebuffers = nil; //alias: glDeleteFramebuffersEXT
  glDeleteProgram: TglDeleteProgram = nil;
  glDeleteRenderbuffers: TglDeleteRenderbuffers = nil; //alias: glDeleteRenderbuffersEXT
  glDeleteShader: TglDeleteShader = nil;
  glDeleteTextures: TglDeleteTextures = nil;
  glDepthFunc: TglDepthFunc = nil;
  glDepthMask: TglDepthMask = nil;
  glDepthRangef: TglDepthRangef = nil; //alias: glDepthRangefOES
  glDetachShader: TglDetachShader = nil; //alias: glDetachObjectARB
  glDisable: TglDisable = nil;
  glDisableVertexAttribArray: TglDisableVertexAttribArray = nil; //alias: glDisableVertexAttribArrayARB
  glDrawArrays: TglDrawArrays = nil; //alias: glDrawArraysEXT
  glDrawElements: TglDrawElements = nil;
  glEnable: TglEnable = nil;
  glEnableVertexAttribArray: TglEnableVertexAttribArray = nil; //alias: glEnableVertexAttribArrayARB
  glFinish: TglFinish = nil;
  glFlush: TglFlush = nil;
  glFramebufferRenderbuffer: TglFramebufferRenderbuffer = nil; //alias: glFramebufferRenderbufferEXT
  glFramebufferTexture2D: TglFramebufferTexture2D = nil; //alias: glFramebufferTexture2DEXT
  glFrontFace: TglFrontFace = nil;
  glGenBuffers: TglGenBuffers = nil; //alias: glGenBuffersARB
  glGenerateMipmap: TglGenerateMipmap = nil; //alias: glGenerateMipmapEXT
  glGenFramebuffers: TglGenFramebuffers = nil; //alias: glGenFramebuffersEXT
  glGenRenderbuffers: TglGenRenderbuffers = nil; //alias: glGenRenderbuffersEXT
  glGenTextures: TglGenTextures = nil;
  glGetActiveAttrib: TglGetActiveAttrib = nil; //alias: glGetActiveAttribARB
  glGetActiveUniform: TglGetActiveUniform = nil; //alias: glGetActiveUniformARB
  glGetAttachedShaders: TglGetAttachedShaders = nil;
  glGetAttribLocation: TglGetAttribLocation = nil; //alias: glGetAttribLocationARB
  glGetBooleanv: TglGetBooleanv = nil;
  glGetBufferParameteriv: TglGetBufferParameteriv = nil; //alias: glGetBufferParameterivARB
  glGetError: TglGetError = nil;
  glGetFloatv: TglGetFloatv = nil;
  glGetFramebufferAttachmentParameteriv: TglGetFramebufferAttachmentParameteriv = nil; //alias: glGetFramebufferAttachmentParameterivEXT
  glGetIntegerv: TglGetIntegerv = nil;
  glGetProgramiv: TglGetProgramiv = nil;
  glGetProgramInfoLog: TglGetProgramInfoLog = nil;
  glGetRenderbufferParameteriv: TglGetRenderbufferParameteriv = nil; //alias: glGetRenderbufferParameterivEXT
  glGetShaderiv: TglGetShaderiv = nil;
  glGetShaderInfoLog: TglGetShaderInfoLog = nil;
  glGetShaderPrecisionFormat: TglGetShaderPrecisionFormat = nil;
  glGetShaderSource: TglGetShaderSource = nil; //alias: glGetShaderSourceARB
  glGetString: TglGetString = nil;
  glGetTexParameterfv: TglGetTexParameterfv = nil;
  glGetTexParameteriv: TglGetTexParameteriv = nil;
  glGetUniformfv: TglGetUniformfv = nil; //alias: glGetUniformfvARB
  glGetUniformiv: TglGetUniformiv = nil; //alias: glGetUniformivARB
  glGetUniformLocation: TglGetUniformLocation = nil; //alias: glGetUniformLocationARB
  glGetVertexAttribfv: TglGetVertexAttribfv = nil; //alias: glGetVertexAttribfvARB
  glGetVertexAttribiv: TglGetVertexAttribiv = nil; //alias: glGetVertexAttribivARB
  glGetVertexAttribPointerv: TglGetVertexAttribPointerv = nil; //alias: glGetVertexAttribPointervARB
  glHint: TglHint = nil;
  glIsBuffer: TglIsBuffer = nil; //alias: glIsBufferARB
  glIsEnabled: TglIsEnabled = nil;
  glIsFramebuffer: TglIsFramebuffer = nil; //alias: glIsFramebufferEXT
  glIsProgram: TglIsProgram = nil;
  glIsRenderbuffer: TglIsRenderbuffer = nil; //alias: glIsRenderbufferEXT
  glIsShader: TglIsShader = nil;
  glIsTexture: TglIsTexture = nil;
  glLineWidth: TglLineWidth = nil;
  glLinkProgram: TglLinkProgram = nil; //alias: glLinkProgramARB
  glPixelStorei: TglPixelStorei = nil;
  glPolygonOffset: TglPolygonOffset = nil;
  glReadPixels: TglReadPixels = nil;
  glReleaseShaderCompiler: TglReleaseShaderCompiler = nil;
  glRenderbufferStorage: TglRenderbufferStorage = nil; //alias: glRenderbufferStorageEXT
  glSampleCoverage: TglSampleCoverage = nil; //alias: glSampleCoverageARB
  glScissor: TglScissor = nil;
  glShaderBinary: TglShaderBinary = nil;
  glShaderSource: TglShaderSource = nil; //alias: glShaderSourceARB
  glStencilFunc: TglStencilFunc = nil;
  glStencilFuncSeparate: TglStencilFuncSeparate = nil;
  glStencilMask: TglStencilMask = nil;
  glStencilMaskSeparate: TglStencilMaskSeparate = nil;
  glStencilOp: TglStencilOp = nil;
  glStencilOpSeparate: TglStencilOpSeparate = nil; //alias: glStencilOpSeparateATI
  glTexImage2D: TglTexImage2D = nil;
  glTexParameterf: TglTexParameterf = nil;
  glTexParameterfv: TglTexParameterfv = nil;
  glTexParameteri: TglTexParameteri = nil;
  glTexParameteriv: TglTexParameteriv = nil;
  glTexSubImage2D: TglTexSubImage2D = nil; //alias: glTexSubImage2DEXT
  glUniform1f: TglUniform1f = nil; //alias: glUniform1fARB
  glUniform1fv: TglUniform1fv = nil; //alias: glUniform1fvARB
  glUniform1i: TglUniform1i = nil; //alias: glUniform1iARB
  glUniform1iv: TglUniform1iv = nil; //alias: glUniform1ivARB
  glUniform2f: TglUniform2f = nil; //alias: glUniform2fARB
  glUniform2fv: TglUniform2fv = nil; //alias: glUniform2fvARB
  glUniform2i: TglUniform2i = nil; //alias: glUniform2iARB
  glUniform2iv: TglUniform2iv = nil; //alias: glUniform2ivARB
  glUniform3f: TglUniform3f = nil; //alias: glUniform3fARB
  glUniform3fv: TglUniform3fv = nil; //alias: glUniform3fvARB
  glUniform3i: TglUniform3i = nil; //alias: glUniform3iARB
  glUniform3iv: TglUniform3iv = nil; //alias: glUniform3ivARB
  glUniform4f: TglUniform4f = nil; //alias: glUniform4fARB
  glUniform4fv: TglUniform4fv = nil; //alias: glUniform4fvARB
  glUniform4i: TglUniform4i = nil; //alias: glUniform4iARB
  glUniform4iv: TglUniform4iv = nil; //alias: glUniform4ivARB
  glUniformMatrix2fv: TglUniformMatrix2fv = nil; //alias: glUniformMatrix2fvARB
  glUniformMatrix3fv: TglUniformMatrix3fv = nil; //alias: glUniformMatrix3fvARB
  glUniformMatrix4fv: TglUniformMatrix4fv = nil; //alias: glUniformMatrix4fvARB
  glUseProgram: TglUseProgram = nil; //alias: glUseProgramObjectARB
  glValidateProgram: TglValidateProgram = nil; //alias: glValidateProgramARB
  glVertexAttrib1f: TglVertexAttrib1f = nil; //alias: glVertexAttrib1fARB
  glVertexAttrib1fv: TglVertexAttrib1fv = nil; //alias: glVertexAttrib1fvARB
  glVertexAttrib2f: TglVertexAttrib2f = nil; //alias: glVertexAttrib2fARB
  glVertexAttrib2fv: TglVertexAttrib2fv = nil; //alias: glVertexAttrib2fvARB
  glVertexAttrib3f: TglVertexAttrib3f = nil; //alias: glVertexAttrib3fARB
  glVertexAttrib3fv: TglVertexAttrib3fv = nil; //alias: glVertexAttrib3fvARB
  glVertexAttrib4f: TglVertexAttrib4f = nil; //alias: glVertexAttrib4fARB
  glVertexAttrib4fv: TglVertexAttrib4fv = nil; //alias: glVertexAttrib4fvARB
  glVertexAttribPointer: TglVertexAttribPointer = nil; //alias: glVertexAttribPointerARB
  glViewport: TglViewport = nil;
 
//------- GL_ES_VERSION_3_0----------------
  glReadBuffer: TglReadBuffer = nil;
  glDrawRangeElements: TglDrawRangeElements = nil; //alias: glDrawRangeElementsEXT
  glTexImage3D: TglTexImage3D = nil; //alias: glTexImage3DEXT
  glTexSubImage3D: TglTexSubImage3D = nil; //alias: glTexSubImage3DEXT
  glCopyTexSubImage3D: TglCopyTexSubImage3D = nil; //alias: glCopyTexSubImage3DEXT
  glCompressedTexImage3D: TglCompressedTexImage3D = nil; //alias: glCompressedTexImage3DARB
  glCompressedTexSubImage3D: TglCompressedTexSubImage3D = nil; //alias: glCompressedTexSubImage3DARB
  glGenQueries: TglGenQueries = nil; //alias: glGenQueriesARB
  glDeleteQueries: TglDeleteQueries = nil; //alias: glDeleteQueriesARB
  glIsQuery: TglIsQuery = nil; //alias: glIsQueryARB
  glBeginQuery: TglBeginQuery = nil; //alias: glBeginQueryARB
  glEndQuery: TglEndQuery = nil; //alias: glEndQueryARB
  glGetQueryiv: TglGetQueryiv = nil; //alias: glGetQueryivARB
  glGetQueryObjectuiv: TglGetQueryObjectuiv = nil; //alias: glGetQueryObjectuivARB
  glUnmapBuffer: TglUnmapBuffer = nil; //alias: glUnmapBufferARB
  glGetBufferPointerv: TglGetBufferPointerv = nil; //alias: glGetBufferPointervARB
  glDrawBuffers: TglDrawBuffers = nil; //alias: glDrawBuffersARB
  glUniformMatrix2x3fv: TglUniformMatrix2x3fv = nil; //alias: glUniformMatrix2x3fvNV
  glUniformMatrix3x2fv: TglUniformMatrix3x2fv = nil; //alias: glUniformMatrix3x2fvNV
  glUniformMatrix2x4fv: TglUniformMatrix2x4fv = nil; //alias: glUniformMatrix2x4fvNV
  glUniformMatrix4x2fv: TglUniformMatrix4x2fv = nil; //alias: glUniformMatrix4x2fvNV
  glUniformMatrix3x4fv: TglUniformMatrix3x4fv = nil; //alias: glUniformMatrix3x4fvNV
  glUniformMatrix4x3fv: TglUniformMatrix4x3fv = nil; //alias: glUniformMatrix4x3fvNV
  glBlitFramebuffer: TglBlitFramebuffer = nil; //alias: glBlitFramebufferEXT
  glRenderbufferStorageMultisample: TglRenderbufferStorageMultisample = nil; //alias: glRenderbufferStorageMultisampleEXT
  glFramebufferTextureLayer: TglFramebufferTextureLayer = nil; //alias: glFramebufferTextureLayerARB
  glMapBufferRange: TglMapBufferRange = nil; //alias: glMapBufferRangeEXT
  glFlushMappedBufferRange: TglFlushMappedBufferRange = nil; //alias: glFlushMappedBufferRangeAPPLE
  glBindVertexArray: TglBindVertexArray = nil; //alias: glBindVertexArrayOES
  glDeleteVertexArrays: TglDeleteVertexArrays = nil; //alias: glDeleteVertexArraysAPPLE
  glGenVertexArrays: TglGenVertexArrays = nil; //alias: glGenVertexArraysAPPLE
  glIsVertexArray: TglIsVertexArray = nil; //alias: glIsVertexArrayAPPLE
  glGetIntegeri_v: TglGetIntegeri_v = nil; //alias: glGetIntegerIndexedvEXT
  glBeginTransformFeedback: TglBeginTransformFeedback = nil; //alias: glBeginTransformFeedbackEXT
  glEndTransformFeedback: TglEndTransformFeedback = nil; //alias: glEndTransformFeedbackEXT
  glBindBufferRange: TglBindBufferRange = nil; //alias: glBindBufferRangeEXT
  glBindBufferBase: TglBindBufferBase = nil; //alias: glBindBufferBaseEXT
  glTransformFeedbackVaryings: TglTransformFeedbackVaryings = nil; //alias: glTransformFeedbackVaryingsEXT
  glGetTransformFeedbackVarying: TglGetTransformFeedbackVarying = nil; //alias: glGetTransformFeedbackVaryingEXT
  glVertexAttribIPointer: TglVertexAttribIPointer = nil; //alias: glVertexAttribIPointerEXT
  glGetVertexAttribIiv: TglGetVertexAttribIiv = nil; //alias: glGetVertexAttribIivEXT
  glGetVertexAttribIuiv: TglGetVertexAttribIuiv = nil; //alias: glGetVertexAttribIuivEXT
  glVertexAttribI4i: TglVertexAttribI4i = nil; //alias: glVertexAttribI4iEXT
  glVertexAttribI4ui: TglVertexAttribI4ui = nil; //alias: glVertexAttribI4uiEXT
  glVertexAttribI4iv: TglVertexAttribI4iv = nil; //alias: glVertexAttribI4ivEXT
  glVertexAttribI4uiv: TglVertexAttribI4uiv = nil; //alias: glVertexAttribI4uivEXT
  glGetUniformuiv: TglGetUniformuiv = nil; //alias: glGetUniformuivEXT
  glGetFragDataLocation: TglGetFragDataLocation = nil; //alias: glGetFragDataLocationEXT
  glUniform1ui: TglUniform1ui = nil; //alias: glUniform1uiEXT
  glUniform2ui: TglUniform2ui = nil; //alias: glUniform2uiEXT
  glUniform3ui: TglUniform3ui = nil; //alias: glUniform3uiEXT
  glUniform4ui: TglUniform4ui = nil; //alias: glUniform4uiEXT
  glUniform1uiv: TglUniform1uiv = nil; //alias: glUniform1uivEXT
  glUniform2uiv: TglUniform2uiv = nil; //alias: glUniform2uivEXT
  glUniform3uiv: TglUniform3uiv = nil; //alias: glUniform3uivEXT
  glUniform4uiv: TglUniform4uiv = nil; //alias: glUniform4uivEXT
  glClearBufferiv: TglClearBufferiv = nil;
  glClearBufferuiv: TglClearBufferuiv = nil;
  glClearBufferfv: TglClearBufferfv = nil;
  glClearBufferfi: TglClearBufferfi = nil;
  glGetStringi: TglGetStringi = nil;
  glCopyBufferSubData: TglCopyBufferSubData = nil; //alias: glCopyBufferSubDataNV
  glGetUniformIndices: TglGetUniformIndices = nil;
  glGetActiveUniformsiv: TglGetActiveUniformsiv = nil;
  glGetUniformBlockIndex: TglGetUniformBlockIndex = nil;
  glGetActiveUniformBlockiv: TglGetActiveUniformBlockiv = nil;
  glGetActiveUniformBlockName: TglGetActiveUniformBlockName = nil;
  glUniformBlockBinding: TglUniformBlockBinding = nil;
  glDrawArraysInstanced: TglDrawArraysInstanced = nil; //alias: glDrawArraysInstancedANGLE
  glDrawElementsInstanced: TglDrawElementsInstanced = nil; //alias: glDrawElementsInstancedANGLE
  glFenceSync: TglFenceSync = nil; //alias: glFenceSyncAPPLE
  glIsSync: TglIsSync = nil; //alias: glIsSyncAPPLE
  glDeleteSync: TglDeleteSync = nil; //alias: glDeleteSyncAPPLE
  glClientWaitSync: TglClientWaitSync = nil; //alias: glClientWaitSyncAPPLE
  glWaitSync: TglWaitSync = nil; //alias: glWaitSyncAPPLE
  glGetInteger64v: TglGetInteger64v = nil; //alias: glGetInteger64vAPPLE
  glGetSynciv: TglGetSynciv = nil; //alias: glGetSyncivAPPLE
  glGetInteger64i_v: TglGetInteger64i_v = nil;
  glGetBufferParameteri64v: TglGetBufferParameteri64v = nil;
  glGenSamplers: TglGenSamplers = nil;
  glDeleteSamplers: TglDeleteSamplers = nil;
  glIsSampler: TglIsSampler = nil;
  glBindSampler: TglBindSampler = nil;
  glSamplerParameteri: TglSamplerParameteri = nil;
  glSamplerParameteriv: TglSamplerParameteriv = nil;
  glSamplerParameterf: TglSamplerParameterf = nil;
  glSamplerParameterfv: TglSamplerParameterfv = nil;
  glGetSamplerParameteriv: TglGetSamplerParameteriv = nil;
  glGetSamplerParameterfv: TglGetSamplerParameterfv = nil;
  glVertexAttribDivisor: TglVertexAttribDivisor = nil; //alias: glVertexAttribDivisorANGLE
  glBindTransformFeedback: TglBindTransformFeedback = nil;
  glDeleteTransformFeedbacks: TglDeleteTransformFeedbacks = nil; //alias: glDeleteTransformFeedbacksNV
  glGenTransformFeedbacks: TglGenTransformFeedbacks = nil; //alias: glGenTransformFeedbacksNV
  glIsTransformFeedback: TglIsTransformFeedback = nil; //alias: glIsTransformFeedbackNV
  glPauseTransformFeedback: TglPauseTransformFeedback = nil; //alias: glPauseTransformFeedbackNV
  glResumeTransformFeedback: TglResumeTransformFeedback = nil; //alias: glResumeTransformFeedbackNV
  glGetProgramBinary: TglGetProgramBinary = nil; //alias: glGetProgramBinaryOES
  glProgramBinary: TglProgramBinary = nil; //alias: glProgramBinaryOES
  glProgramParameteri: TglProgramParameteri = nil; //alias: glProgramParameteriARB
  glInvalidateFramebuffer: TglInvalidateFramebuffer = nil;
  glInvalidateSubFramebuffer: TglInvalidateSubFramebuffer = nil;
  glTexStorage2D: TglTexStorage2D = nil; //alias: glTexStorage2DEXT
  glTexStorage3D: TglTexStorage3D = nil; //alias: glTexStorage3DEXT
  glGetInternalformativ: TglGetInternalformativ = nil;
 
//------- GL_ES_VERSION_3_1----------------
  glDispatchCompute: TglDispatchCompute = nil;
  glDispatchComputeIndirect: TglDispatchComputeIndirect = nil;
  glDrawArraysIndirect: TglDrawArraysIndirect = nil;
  glDrawElementsIndirect: TglDrawElementsIndirect = nil;
  glFramebufferParameteri: TglFramebufferParameteri = nil;
  glGetFramebufferParameteriv: TglGetFramebufferParameteriv = nil;
  glGetProgramInterfaceiv: TglGetProgramInterfaceiv = nil;
  glGetProgramResourceIndex: TglGetProgramResourceIndex = nil;
  glGetProgramResourceName: TglGetProgramResourceName = nil;
  glGetProgramResourceiv: TglGetProgramResourceiv = nil;
  glGetProgramResourceLocation: TglGetProgramResourceLocation = nil;
  glUseProgramStages: TglUseProgramStages = nil;
  glActiveShaderProgram: TglActiveShaderProgram = nil;
  glCreateShaderProgramv: TglCreateShaderProgramv = nil;
  glBindProgramPipeline: TglBindProgramPipeline = nil;
  glDeleteProgramPipelines: TglDeleteProgramPipelines = nil;
  glGenProgramPipelines: TglGenProgramPipelines = nil;
  glIsProgramPipeline: TglIsProgramPipeline = nil;
  glGetProgramPipelineiv: TglGetProgramPipelineiv = nil;
  glProgramUniform1i: TglProgramUniform1i = nil; //alias: glProgramUniform1iEXT
  glProgramUniform2i: TglProgramUniform2i = nil; //alias: glProgramUniform2iEXT
  glProgramUniform3i: TglProgramUniform3i = nil; //alias: glProgramUniform3iEXT
  glProgramUniform4i: TglProgramUniform4i = nil; //alias: glProgramUniform4iEXT
  glProgramUniform1ui: TglProgramUniform1ui = nil; //alias: glProgramUniform1uiEXT
  glProgramUniform2ui: TglProgramUniform2ui = nil; //alias: glProgramUniform2uiEXT
  glProgramUniform3ui: TglProgramUniform3ui = nil; //alias: glProgramUniform3uiEXT
  glProgramUniform4ui: TglProgramUniform4ui = nil; //alias: glProgramUniform4uiEXT
  glProgramUniform1f: TglProgramUniform1f = nil; //alias: glProgramUniform1fEXT
  glProgramUniform2f: TglProgramUniform2f = nil; //alias: glProgramUniform2fEXT
  glProgramUniform3f: TglProgramUniform3f = nil; //alias: glProgramUniform3fEXT
  glProgramUniform4f: TglProgramUniform4f = nil; //alias: glProgramUniform4fEXT
  glProgramUniform1iv: TglProgramUniform1iv = nil; //alias: glProgramUniform1ivEXT
  glProgramUniform2iv: TglProgramUniform2iv = nil; //alias: glProgramUniform2ivEXT
  glProgramUniform3iv: TglProgramUniform3iv = nil; //alias: glProgramUniform3ivEXT
  glProgramUniform4iv: TglProgramUniform4iv = nil; //alias: glProgramUniform4ivEXT
  glProgramUniform1uiv: TglProgramUniform1uiv = nil; //alias: glProgramUniform1uivEXT
  glProgramUniform2uiv: TglProgramUniform2uiv = nil; //alias: glProgramUniform2uivEXT
  glProgramUniform3uiv: TglProgramUniform3uiv = nil; //alias: glProgramUniform3uivEXT
  glProgramUniform4uiv: TglProgramUniform4uiv = nil; //alias: glProgramUniform4uivEXT
  glProgramUniform1fv: TglProgramUniform1fv = nil; //alias: glProgramUniform1fvEXT
  glProgramUniform2fv: TglProgramUniform2fv = nil; //alias: glProgramUniform2fvEXT
  glProgramUniform3fv: TglProgramUniform3fv = nil; //alias: glProgramUniform3fvEXT
  glProgramUniform4fv: TglProgramUniform4fv = nil; //alias: glProgramUniform4fvEXT
  glProgramUniformMatrix2fv: TglProgramUniformMatrix2fv = nil; //alias: glProgramUniformMatrix2fvEXT
  glProgramUniformMatrix3fv: TglProgramUniformMatrix3fv = nil; //alias: glProgramUniformMatrix3fvEXT
  glProgramUniformMatrix4fv: TglProgramUniformMatrix4fv = nil; //alias: glProgramUniformMatrix4fvEXT
  glProgramUniformMatrix2x3fv: TglProgramUniformMatrix2x3fv = nil; //alias: glProgramUniformMatrix2x3fvEXT
  glProgramUniformMatrix3x2fv: TglProgramUniformMatrix3x2fv = nil; //alias: glProgramUniformMatrix3x2fvEXT
  glProgramUniformMatrix2x4fv: TglProgramUniformMatrix2x4fv = nil; //alias: glProgramUniformMatrix2x4fvEXT
  glProgramUniformMatrix4x2fv: TglProgramUniformMatrix4x2fv = nil; //alias: glProgramUniformMatrix4x2fvEXT
  glProgramUniformMatrix3x4fv: TglProgramUniformMatrix3x4fv = nil; //alias: glProgramUniformMatrix3x4fvEXT
  glProgramUniformMatrix4x3fv: TglProgramUniformMatrix4x3fv = nil; //alias: glProgramUniformMatrix4x3fvEXT
  glValidateProgramPipeline: TglValidateProgramPipeline = nil;
  glGetProgramPipelineInfoLog: TglGetProgramPipelineInfoLog = nil;
  glBindImageTexture: TglBindImageTexture = nil;
  glGetBooleani_v: TglGetBooleani_v = nil; //alias: glGetBooleanIndexedvEXT
  glMemoryBarrier: TglMemoryBarrier = nil; //alias: glMemoryBarrierEXT
  glMemoryBarrierByRegion: TglMemoryBarrierByRegion = nil;
  glTexStorage2DMultisample: TglTexStorage2DMultisample = nil;
  glGetMultisamplefv: TglGetMultisamplefv = nil; //alias: glGetMultisamplefvNV
  glSampleMaski: TglSampleMaski = nil;
  glGetTexLevelParameteriv: TglGetTexLevelParameteriv = nil;
  glGetTexLevelParameterfv: TglGetTexLevelParameterfv = nil;
  glBindVertexBuffer: TglBindVertexBuffer = nil;
  glVertexAttribFormat: TglVertexAttribFormat = nil;
  glVertexAttribIFormat: TglVertexAttribIFormat = nil;
  glVertexAttribBinding: TglVertexAttribBinding = nil;
  glVertexBindingDivisor: TglVertexBindingDivisor = nil;
 
//------- GL_ES_VERSION_3_2----------------
  glBlendBarrier: TglBlendBarrier = nil; //alias: glBlendBarrierKHR
  glCopyImageSubData: TglCopyImageSubData = nil; //alias: glCopyImageSubDataEXT
  glDebugMessageControl: TglDebugMessageControl = nil; //alias: glDebugMessageControlARB
  glDebugMessageInsert: TglDebugMessageInsert = nil; //alias: glDebugMessageInsertARB
  glDebugMessageCallback: TglDebugMessageCallback = nil; //alias: glDebugMessageCallbackARB
  glGetDebugMessageLog: TglGetDebugMessageLog = nil; //alias: glGetDebugMessageLogARB
  glPushDebugGroup: TglPushDebugGroup = nil; //alias: glPushDebugGroupKHR
  glPopDebugGroup: TglPopDebugGroup = nil; //alias: glPopDebugGroupKHR
  glObjectLabel: TglObjectLabel = nil; //alias: glObjectLabelKHR
  glGetObjectLabel: TglGetObjectLabel = nil; //alias: glGetObjectLabelKHR
  glObjectPtrLabel: TglObjectPtrLabel = nil; //alias: glObjectPtrLabelKHR
  glGetObjectPtrLabel: TglGetObjectPtrLabel = nil; //alias: glGetObjectPtrLabelKHR
  glGetPointerv: TglGetPointerv = nil; //alias: glGetPointervEXT
  glEnablei: TglEnablei = nil; //alias: glEnableIndexedEXT
  glDisablei: TglDisablei = nil; //alias: glDisableIndexedEXT
  glBlendEquationi: TglBlendEquationi = nil; //alias: glBlendEquationIndexedAMD
  glBlendEquationSeparatei: TglBlendEquationSeparatei = nil; //alias: glBlendEquationSeparateIndexedAMD
  glBlendFunci: TglBlendFunci = nil; //alias: glBlendFuncIndexedAMD
  glBlendFuncSeparatei: TglBlendFuncSeparatei = nil; //alias: glBlendFuncSeparateIndexedAMD
  glColorMaski: TglColorMaski = nil; //alias: glColorMaskIndexedEXT
  glIsEnabledi: TglIsEnabledi = nil; //alias: glIsEnabledIndexedEXT
  glDrawElementsBaseVertex: TglDrawElementsBaseVertex = nil; //alias: glDrawElementsBaseVertexEXT
  glDrawRangeElementsBaseVertex: TglDrawRangeElementsBaseVertex = nil; //alias: glDrawRangeElementsBaseVertexEXT
  glDrawElementsInstancedBaseVertex: TglDrawElementsInstancedBaseVertex = nil; //alias: glDrawElementsInstancedBaseVertexEXT
  glFramebufferTexture: TglFramebufferTexture = nil; //alias: glFramebufferTextureARB
  glPrimitiveBoundingBox: TglPrimitiveBoundingBox = nil; //alias: glPrimitiveBoundingBoxARB
  glGetGraphicsResetStatus: TglGetGraphicsResetStatus = nil; //alias: glGetGraphicsResetStatusEXT
  glReadnPixels: TglReadnPixels = nil; //alias: glReadnPixelsARB
  glGetnUniformfv: TglGetnUniformfv = nil; //alias: glGetnUniformfvEXT
  glGetnUniformiv: TglGetnUniformiv = nil; //alias: glGetnUniformivEXT
  glGetnUniformuiv: TglGetnUniformuiv = nil; //alias: glGetnUniformuivKHR
  glMinSampleShading: TglMinSampleShading = nil; //alias: glMinSampleShadingARB
  glPatchParameteri: TglPatchParameteri = nil; //alias: glPatchParameteriEXT
  glTexParameterIiv: TglTexParameterIiv = nil; //alias: glTexParameterIivEXT
  glTexParameterIuiv: TglTexParameterIuiv = nil; //alias: glTexParameterIuivEXT
  glGetTexParameterIiv: TglGetTexParameterIiv = nil; //alias: glGetTexParameterIivEXT
  glGetTexParameterIuiv: TglGetTexParameterIuiv = nil; //alias: glGetTexParameterIuivEXT
  glSamplerParameterIiv: TglSamplerParameterIiv = nil; //alias: glSamplerParameterIivEXT
  glSamplerParameterIuiv: TglSamplerParameterIuiv = nil; //alias: glSamplerParameterIuivEXT
  glGetSamplerParameterIiv: TglGetSamplerParameterIiv = nil; //alias: glGetSamplerParameterIivEXT
  glGetSamplerParameterIuiv: TglGetSamplerParameterIuiv = nil; //alias: glGetSamplerParameterIuivEXT
  glTexBuffer: TglTexBuffer = nil; //alias: glTexBufferARB
  glTexBufferRange: TglTexBufferRange = nil; //alias: glTexBufferRangeEXT
  glTexStorage3DMultisample: TglTexStorage3DMultisample = nil; //alias: glTexStorage3DMultisampleOES
 
//------- Other ----------------
  glActiveProgramEXT: TglActiveProgramEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glActiveShaderProgramEXT: TglActiveShaderProgramEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glAlphaFuncQCOM: TglAlphaFuncQCOM = nil;   // api(gles2) //extension: GL_QCOM_alpha_test
  glApplyFramebufferAttachmentCMAAINTEL: TglApplyFramebufferAttachmentCMAAINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_framebuffer_CMAA
  glAcquireKeyedMutexWin32EXT: TglAcquireKeyedMutexWin32EXT = nil;   // api(gl|gles2) //extension: GL_EXT_win32_keyed_mutex
  glArrayElement: TglArrayElement = nil; //alias: glArrayElementEXT
  glBeginConditionalRender: TglBeginConditionalRender = nil; //alias: glBeginConditionalRenderNV
  glBeginConditionalRenderNV: TglBeginConditionalRenderNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_conditional_render
  glBeginPerfMonitorAMD: TglBeginPerfMonitorAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glBeginPerfQueryINTEL: TglBeginPerfQueryINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glBeginQueryEXT: TglBeginQueryEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glBindBufferOffsetEXT: TglBindBufferOffsetEXT = nil; //alias: glBindBufferOffsetNV
  glBindFragDataLocation: TglBindFragDataLocation = nil; //alias: glBindFragDataLocationEXT
  glBindFragDataLocationEXT: TglBindFragDataLocationEXT = nil;   // api(gles2) //extension: GL_EXT_blend_func_extended
  glBindFragDataLocationIndexed: TglBindFragDataLocationIndexed = nil; //alias: glBindFragDataLocationIndexedEXT
  glBindFragDataLocationIndexedEXT: TglBindFragDataLocationIndexedEXT = nil;   // api(gles2) //extension: GL_EXT_blend_func_extended
  glBindProgramARB: TglBindProgramARB = nil; //alias: glBindProgramNV
  glBindProgramPipelineEXT: TglBindProgramPipelineEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glBindShadingRateImageNV: TglBindShadingRateImageNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glBindVertexArrayOES: TglBindVertexArrayOES = nil;   // api(gles1|gles2) //extension: GL_OES_vertex_array_object
  glBlendBarrierKHR: TglBlendBarrierKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_blend_equation_advanced
  glBlendBarrierNV: TglBlendBarrierNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
  glBlendEquationEXT: TglBlendEquationEXT = nil;   // api(gl|gles1|gles2) //extension: GL_EXT_blend_minmax
  glBlendEquationSeparateiEXT: TglBlendEquationSeparateiEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glBlendEquationSeparateiOES: TglBlendEquationSeparateiOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glBlendEquationiEXT: TglBlendEquationiEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glBlendEquationiOES: TglBlendEquationiOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glBlendFuncSeparateiEXT: TglBlendFuncSeparateiEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glBlendFuncSeparateiOES: TglBlendFuncSeparateiOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glBlendFunciEXT: TglBlendFunciEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glBlendFunciOES: TglBlendFunciOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glBlendParameteriNV: TglBlendParameteriNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_blend_equation_advanced
  glBlitFramebufferANGLE: TglBlitFramebufferANGLE = nil;   // api(gles2) //extension: GL_ANGLE_framebuffer_blit
  glBlitFramebufferNV: TglBlitFramebufferNV = nil;   // api(gles2) //extension: GL_NV_framebuffer_blit
  glBufferAttachMemoryNV: TglBufferAttachMemoryNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
  glBufferPageCommitmentMemNV: TglBufferPageCommitmentMemNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_object_sparse
  glBufferStorage: TglBufferStorage = nil; //alias: glBufferStorageEXT
  glBufferStorageEXT: TglBufferStorageEXT = nil;   // api(gles2) //extension: GL_EXT_buffer_storage
  glBufferStorageExternalEXT: TglBufferStorageExternalEXT = nil;   // api(gl|gles2) //extension: GL_EXT_external_buffer
  glBufferStorageMemEXT: TglBufferStorageMemEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glClampColor: TglClampColor = nil; //alias: glClampColorARB
  glClearPixelLocalStorageuiEXT: TglClearPixelLocalStorageuiEXT = nil;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage2
  glClearTexImage: TglClearTexImage = nil; //alias: glClearTexImageEXT
  glClearTexImageEXT: TglClearTexImageEXT = nil;   // api(gles2) //extension: GL_EXT_clear_texture
  glClearTexSubImage: TglClearTexSubImage = nil; //alias: glClearTexSubImageEXT
  glClearTexSubImageEXT: TglClearTexSubImageEXT = nil;   // api(gles2) //extension: GL_EXT_clear_texture
  glClientActiveTexture: TglClientActiveTexture = nil; //alias: glClientActiveTextureARB
  glClientWaitSyncAPPLE: TglClientWaitSyncAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glClipControl: TglClipControl = nil; //alias: glClipControlEXT
  glClipControlEXT: TglClipControlEXT = nil;   // api(gles2) //extension: GL_EXT_clip_control
  glColorMaskiEXT: TglColorMaskiEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glColorMaskiOES: TglColorMaskiOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glColorSubTable: TglColorSubTable = nil; //alias: glColorSubTableEXT
  glColorTable: TglColorTable = nil; //alias: glColorTableEXT
  glColorTableParameterfv: TglColorTableParameterfv = nil; //alias: glColorTableParameterfvSGI
  glColorTableParameteriv: TglColorTableParameteriv = nil; //alias: glColorTableParameterivSGI
  glCompressedTexImage1D: TglCompressedTexImage1D = nil; //alias: glCompressedTexImage1DARB
  glCompressedTexImage3DOES: TglCompressedTexImage3DOES = nil;   // api(gles2) //extension: GL_OES_texture_3D
  glCompressedTexSubImage1D: TglCompressedTexSubImage1D = nil; //alias: glCompressedTexSubImage1DARB
  glCompressedTexSubImage3DOES: TglCompressedTexSubImage3DOES = nil;   // api(gles2) //extension: GL_OES_texture_3D
  glConservativeRasterParameteriNV: TglConservativeRasterParameteriNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster_pre_snap_triangles
  glConvolutionFilter1D: TglConvolutionFilter1D = nil; //alias: glConvolutionFilter1DEXT
  glConvolutionFilter2D: TglConvolutionFilter2D = nil; //alias: glConvolutionFilter2DEXT
  glConvolutionParameterf: TglConvolutionParameterf = nil; //alias: glConvolutionParameterfEXT
  glConvolutionParameterfv: TglConvolutionParameterfv = nil; //alias: glConvolutionParameterfvEXT
  glConvolutionParameteri: TglConvolutionParameteri = nil; //alias: glConvolutionParameteriEXT
  glConvolutionParameteriv: TglConvolutionParameteriv = nil; //alias: glConvolutionParameterivEXT
  glCopyBufferSubDataNV: TglCopyBufferSubDataNV = nil;   // api(gles2) //extension: GL_NV_copy_buffer
  glCopyColorSubTable: TglCopyColorSubTable = nil; //alias: glCopyColorSubTableEXT
  glCopyColorTable: TglCopyColorTable = nil; //alias: glCopyColorTableSGI
  glCopyConvolutionFilter1D: TglCopyConvolutionFilter1D = nil; //alias: glCopyConvolutionFilter1DEXT
  glCopyConvolutionFilter2D: TglCopyConvolutionFilter2D = nil; //alias: glCopyConvolutionFilter2DEXT
  glCopyImageSubDataEXT: TglCopyImageSubDataEXT = nil;   // api(gles2) //extension: GL_EXT_copy_image
  glCopyImageSubDataOES: TglCopyImageSubDataOES = nil;   // api(gles2) //extension: GL_OES_copy_image
  glCopyPathNV: TglCopyPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glCopyTexImage1D: TglCopyTexImage1D = nil; //alias: glCopyTexImage1DEXT
  glCopyTexSubImage1D: TglCopyTexSubImage1D = nil; //alias: glCopyTexSubImage1DEXT
  glCopyTexSubImage3DOES: TglCopyTexSubImage3DOES = nil;   // api(gles2) //extension: GL_OES_texture_3D
  glCopyTextureLevelsAPPLE: TglCopyTextureLevelsAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_copy_texture_levels
  glCoverFillPathInstancedNV: TglCoverFillPathInstancedNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glCoverFillPathNV: TglCoverFillPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glCoverStrokePathInstancedNV: TglCoverStrokePathInstancedNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glCoverStrokePathNV: TglCoverStrokePathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glCoverageMaskNV: TglCoverageMaskNV = nil;   // api(gles2) //extension: GL_NV_coverage_sample
  glCoverageModulationNV: TglCoverageModulationNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
  glCoverageModulationTableNV: TglCoverageModulationTableNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
  glCoverageOperationNV: TglCoverageOperationNV = nil;   // api(gles2) //extension: GL_NV_coverage_sample
  glCreateMemoryObjectsEXT: TglCreateMemoryObjectsEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glCreatePerfQueryINTEL: TglCreatePerfQueryINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glCreateSemaphoresNV: TglCreateSemaphoresNV = nil;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
  glCreateShaderProgramEXT: TglCreateShaderProgramEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glCreateShaderProgramvEXT: TglCreateShaderProgramvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glDebugMessageCallbackKHR: TglDebugMessageCallbackKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glDebugMessageControlKHR: TglDebugMessageControlKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glDebugMessageInsertKHR: TglDebugMessageInsertKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glDeleteFencesNV: TglDeleteFencesNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glDeleteMemoryObjectsEXT: TglDeleteMemoryObjectsEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glDeletePathsNV: TglDeletePathsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glDeletePerfMonitorsAMD: TglDeletePerfMonitorsAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glDeletePerfQueryINTEL: TglDeletePerfQueryINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glDeleteProgramPipelinesEXT: TglDeleteProgramPipelinesEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glDeleteProgramsARB: TglDeleteProgramsARB = nil; //alias: glDeleteProgramsNV
  glDeleteQueriesEXT: TglDeleteQueriesEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glDeleteSemaphoresEXT: TglDeleteSemaphoresEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glDeleteSyncAPPLE: TglDeleteSyncAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glDeleteVertexArraysOES: TglDeleteVertexArraysOES = nil;   // api(gles1|gles2) //extension: GL_OES_vertex_array_object
  glDepthRangeArrayfvNV: TglDepthRangeArrayfvNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glDepthRangeArrayfvOES: TglDepthRangeArrayfvOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glDepthRangeIndexedfNV: TglDepthRangeIndexedfNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glDepthRangeIndexedfOES: TglDepthRangeIndexedfOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glDisableDriverControlQCOM: TglDisableDriverControlQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_driver_control
  glDisableiEXT: TglDisableiEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glDisableiNV: TglDisableiNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glDisableiOES: TglDisableiOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glDiscardFramebufferEXT: TglDiscardFramebufferEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_discard_framebuffer
  glDrawArraysInstancedANGLE: TglDrawArraysInstancedANGLE = nil;   // api(gles2) //extension: GL_ANGLE_instanced_arrays
  glDrawArraysInstancedBaseInstance: TglDrawArraysInstancedBaseInstance = nil; //alias: glDrawArraysInstancedBaseInstanceEXT
  glDrawArraysInstancedBaseInstanceEXT: TglDrawArraysInstancedBaseInstanceEXT = nil;   // api(gles2) //extension: GL_EXT_base_instance
  glDrawArraysInstancedEXT: TglDrawArraysInstancedEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_draw_instanced
  glDrawArraysInstancedNV: TglDrawArraysInstancedNV = nil;   // api(gles2) //extension: GL_NV_draw_instanced
  glDrawBuffersEXT: TglDrawBuffersEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers
  glDrawBuffersIndexedEXT: TglDrawBuffersIndexedEXT = nil;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
  glDrawBuffersNV: TglDrawBuffersNV = nil;   // api(gles2) //extension: GL_NV_draw_buffers
  glDrawElementsBaseVertexEXT: TglDrawElementsBaseVertexEXT = nil;   // api(gles2) //extension: GL_EXT_draw_elements_base_vertex
  glDrawElementsBaseVertexOES: TglDrawElementsBaseVertexOES = nil;   // api(gles2) //extension: GL_OES_draw_elements_base_vertex
  glDrawElementsInstancedANGLE: TglDrawElementsInstancedANGLE = nil;   // api(gles2) //extension: GL_ANGLE_instanced_arrays
  glDrawElementsInstancedBaseInstance: TglDrawElementsInstancedBaseInstance = nil; //alias: glDrawElementsInstancedBaseInstanceEXT
  glDrawElementsInstancedBaseInstanceEXT: TglDrawElementsInstancedBaseInstanceEXT = nil;   // api(gles2) //extension: GL_EXT_base_instance
  glDrawElementsInstancedBaseVertexBaseInstance: TglDrawElementsInstancedBaseVertexBaseInstance = nil; //alias: glDrawElementsInstancedBaseVertexBaseInstanceEXT
  glDrawElementsInstancedBaseVertexBaseInstanceEXT: TglDrawElementsInstancedBaseVertexBaseInstanceEXT = nil;   // api(gles2) //extension: GL_EXT_base_instance
  glDrawElementsInstancedBaseVertexEXT: TglDrawElementsInstancedBaseVertexEXT = nil;   // api(gles2) //extension: GL_EXT_draw_elements_base_vertex
  glDrawElementsInstancedBaseVertexOES: TglDrawElementsInstancedBaseVertexOES = nil;   // api(gles2) //extension: GL_OES_draw_elements_base_vertex
  glDrawElementsInstancedEXT: TglDrawElementsInstancedEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_draw_instanced
  glDrawElementsInstancedNV: TglDrawElementsInstancedNV = nil;   // api(gles2) //extension: GL_NV_draw_instanced
  glDrawMeshTasksNV: TglDrawMeshTasksNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
  glDrawMeshTasksIndirectNV: TglDrawMeshTasksIndirectNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
  glDrawRangeElementsBaseVertexEXT: TglDrawRangeElementsBaseVertexEXT = nil;   // api(gles2) //extension: GL_EXT_draw_elements_base_vertex
  glDrawRangeElementsBaseVertexOES: TglDrawRangeElementsBaseVertexOES = nil;   // api(gles2) //extension: GL_OES_draw_elements_base_vertex
  glDrawTransformFeedback: TglDrawTransformFeedback = nil; //alias: glDrawTransformFeedbackEXT
  glDrawTransformFeedbackEXT: TglDrawTransformFeedbackEXT = nil;   // api(gles2) //extension: GL_EXT_draw_transform_feedback
  glDrawTransformFeedbackInstanced: TglDrawTransformFeedbackInstanced = nil; //alias: glDrawTransformFeedbackInstancedEXT
  glDrawTransformFeedbackInstancedEXT: TglDrawTransformFeedbackInstancedEXT = nil;   // api(gles2) //extension: GL_EXT_draw_transform_feedback
  glEGLImageTargetRenderbufferStorageOES: TglEGLImageTargetRenderbufferStorageOES = nil;   // api(gles1|gles2) //extension: GL_OES_EGL_image
  glEGLImageTargetTexStorageEXT: TglEGLImageTargetTexStorageEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_EGL_image_storage
  glEGLImageTargetTexture2DOES: TglEGLImageTargetTexture2DOES = nil;   // api(gles1|gles2) //extension: GL_OES_EGL_image
  glEGLImageTargetTextureStorageEXT: TglEGLImageTargetTextureStorageEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_EGL_image_storage
  glEnableDriverControlQCOM: TglEnableDriverControlQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_driver_control
  glEnableiEXT: TglEnableiEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glEnableiNV: TglEnableiNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glEnableiOES: TglEnableiOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glEndConditionalRender: TglEndConditionalRender = nil; //alias: glEndConditionalRenderNV
  glEndConditionalRenderNV: TglEndConditionalRenderNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_conditional_render
  glEndPerfMonitorAMD: TglEndPerfMonitorAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glEndPerfQueryINTEL: TglEndPerfQueryINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glEndQueryEXT: TglEndQueryEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glEndTilingQCOM: TglEndTilingQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
  glExtGetBufferPointervQCOM: TglExtGetBufferPointervQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtGetBuffersQCOM: TglExtGetBuffersQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtGetFramebuffersQCOM: TglExtGetFramebuffersQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtGetProgramBinarySourceQCOM: TglExtGetProgramBinarySourceQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get2
  glExtGetProgramsQCOM: TglExtGetProgramsQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get2
  glExtGetRenderbuffersQCOM: TglExtGetRenderbuffersQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtGetShadersQCOM: TglExtGetShadersQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get2
  glExtGetTexLevelParameterivQCOM: TglExtGetTexLevelParameterivQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtGetTexSubImageQCOM: TglExtGetTexSubImageQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtGetTexturesQCOM: TglExtGetTexturesQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glExtIsProgramBinaryQCOM: TglExtIsProgramBinaryQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get2
  glExtTexObjectStateOverrideiQCOM: TglExtTexObjectStateOverrideiQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_extended_get
  glFenceSyncAPPLE: TglFenceSyncAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glFinishFenceNV: TglFinishFenceNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glFlushMappedBufferRangeEXT: TglFlushMappedBufferRangeEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
  glFogCoordPointer: TglFogCoordPointer = nil; //alias: glFogCoordPointerEXT
  glFogCoordd: TglFogCoordd = nil; //alias: glFogCoorddEXT
  glFogCoorddv: TglFogCoorddv = nil; //alias: glFogCoorddvEXT
  glFogCoordf: TglFogCoordf = nil; //alias: glFogCoordfEXT
  glFogCoordfv: TglFogCoordfv = nil; //alias: glFogCoordfvEXT
  glFragmentCoverageColorNV: TglFragmentCoverageColorNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_fragment_coverage_to_color
  glFramebufferFetchBarrierEXT: TglFramebufferFetchBarrierEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_shader_framebuffer_fetch_non_coherent
  glFramebufferFetchBarrierQCOM: TglFramebufferFetchBarrierQCOM = nil;   // api(gles2) //extension: GL_QCOM_shader_framebuffer_fetch_noncoherent
  glFramebufferFoveationConfigQCOM: TglFramebufferFoveationConfigQCOM = nil;   // api(gles2) //extension: GL_QCOM_framebuffer_foveated
  glFramebufferFoveationParametersQCOM: TglFramebufferFoveationParametersQCOM = nil;   // api(gles2) //extension: GL_QCOM_framebuffer_foveated
  glFramebufferPixelLocalStorageSizeEXT: TglFramebufferPixelLocalStorageSizeEXT = nil;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage2
  glFramebufferSampleLocationsfvNV: TglFramebufferSampleLocationsfvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
  glFramebufferTexture1D: TglFramebufferTexture1D = nil; //alias: glFramebufferTexture1DEXT
  glFramebufferTexture2DDownsampleIMG: TglFramebufferTexture2DDownsampleIMG = nil;   // api(gles2) //extension: GL_IMG_framebuffer_downsample
  glFramebufferTexture2DMultisampleEXT: TglFramebufferTexture2DMultisampleEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_multisampled_render_to_texture
  glFramebufferTexture2DMultisampleIMG: TglFramebufferTexture2DMultisampleIMG = nil;   // api(gles1|gles2) //extension: GL_IMG_multisampled_render_to_texture
  glFramebufferTexture3D: TglFramebufferTexture3D = nil; //alias: glFramebufferTexture3DEXT
  glFramebufferTexture3DOES: TglFramebufferTexture3DOES = nil;   // api(gles2) //extension: GL_OES_texture_3D
  glFramebufferTextureEXT: TglFramebufferTextureEXT = nil;   // api(gles2) //extension: GL_EXT_geometry_shader
  glFramebufferTextureFaceARB: TglFramebufferTextureFaceARB = nil; //alias: glFramebufferTextureFaceEXT
  glFramebufferTextureLayerDownsampleIMG: TglFramebufferTextureLayerDownsampleIMG = nil;   // api(gles2) //extension: GL_IMG_framebuffer_downsample
  glFramebufferTextureMultisampleMultiviewOVR: TglFramebufferTextureMultisampleMultiviewOVR = nil;   // api(gles2) //extension: GL_OVR_multiview_multisampled_render_to_texture
  glFramebufferTextureMultiviewOVR: TglFramebufferTextureMultiviewOVR = nil;   // api(gl|glcore|gles2) //extension: GL_OVR_multiview
  glFramebufferTextureOES: TglFramebufferTextureOES = nil;   // api(gles2) //extension: GL_OES_geometry_shader
  glGenFencesNV: TglGenFencesNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glGenPathsNV: TglGenPathsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGenPerfMonitorsAMD: TglGenPerfMonitorsAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGenProgramPipelinesEXT: TglGenProgramPipelinesEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glGenProgramsARB: TglGenProgramsARB = nil; //alias: glGenProgramsNV
  glGenQueriesEXT: TglGenQueriesEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGenSemaphoresEXT: TglGenSemaphoresEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glGenVertexArraysOES: TglGenVertexArraysOES = nil;   // api(gles1|gles2) //extension: GL_OES_vertex_array_object
  glGetBufferPointervOES: TglGetBufferPointervOES = nil;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
  glGetBufferSubData: TglGetBufferSubData = nil; //alias: glGetBufferSubDataARB
  glGetColorTable: TglGetColorTable = nil; //alias: glGetColorTableEXT
  glGetColorTableParameterfv: TglGetColorTableParameterfv = nil; //alias: glGetColorTableParameterfvEXT
  glGetColorTableParameteriv: TglGetColorTableParameteriv = nil; //alias: glGetColorTableParameterivEXT
  glGetCompressedTexImage: TglGetCompressedTexImage = nil; //alias: glGetCompressedTexImageARB
  glGetCoverageModulationTableNV: TglGetCoverageModulationTableNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_framebuffer_mixed_samples
  glGetDebugMessageLogKHR: TglGetDebugMessageLogKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glGetDoublei_v: TglGetDoublei_v = nil; //alias: glGetDoubleIndexedvEXT
  glGetDriverControlStringQCOM: TglGetDriverControlStringQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_driver_control
  glGetDriverControlsQCOM: TglGetDriverControlsQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_driver_control
  glGetFenceivNV: TglGetFenceivNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glGetFirstPerfQueryIdINTEL: TglGetFirstPerfQueryIdINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glGetFloati_v: TglGetFloati_v = nil; //alias: glGetFloatIndexedvEXT
  glGetFloati_vNV: TglGetFloati_vNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glGetFloati_vOES: TglGetFloati_vOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glGetFragDataIndex: TglGetFragDataIndex = nil; //alias: glGetFragDataIndexEXT
  glGetFragDataIndexEXT: TglGetFragDataIndexEXT = nil;   // api(gles2) //extension: GL_EXT_blend_func_extended
  glGetFramebufferPixelLocalStorageSizeEXT: TglGetFramebufferPixelLocalStorageSizeEXT = nil;   // api(gles2) //extension: GL_EXT_shader_pixel_local_storage2
  glGetGraphicsResetStatusEXT: TglGetGraphicsResetStatusEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_robustness
  glGetGraphicsResetStatusKHR: TglGetGraphicsResetStatusKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
  glGetImageHandleNV: TglGetImageHandleNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glGetInteger64vAPPLE: TglGetInteger64vAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glGetInteger64vEXT: TglGetInteger64vEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGetIntegeri_vEXT: TglGetIntegeri_vEXT = nil;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
  glGetInternalformatSampleivNV: TglGetInternalformatSampleivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_internalformat_sample_query
  glGetMemoryObjectDetachedResourcesuivNV: TglGetMemoryObjectDetachedResourcesuivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
  glGetMemoryObjectParameterivEXT: TglGetMemoryObjectParameterivEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glGetNextPerfQueryIdINTEL: TglGetNextPerfQueryIdINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glGetObjectLabelEXT: TglGetObjectLabelEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
  glGetObjectLabelKHR: TglGetObjectLabelKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glGetObjectPtrLabelKHR: TglGetObjectPtrLabelKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glGetPathColorGenfvNV: TglGetPathColorGenfvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathColorGenivNV: TglGetPathColorGenivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathCommandsNV: TglGetPathCommandsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathCoordsNV: TglGetPathCoordsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathDashArrayNV: TglGetPathDashArrayNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathLengthNV: TglGetPathLengthNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathMetricRangeNV: TglGetPathMetricRangeNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathMetricsNV: TglGetPathMetricsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathParameterfvNV: TglGetPathParameterfvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathParameterivNV: TglGetPathParameterivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathSpacingNV: TglGetPathSpacingNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathTexGenfvNV: TglGetPathTexGenfvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPathTexGenivNV: TglGetPathTexGenivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetPerfCounterInfoINTEL: TglGetPerfCounterInfoINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glGetPerfMonitorCounterDataAMD: TglGetPerfMonitorCounterDataAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGetPerfMonitorCounterInfoAMD: TglGetPerfMonitorCounterInfoAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGetPerfMonitorCounterStringAMD: TglGetPerfMonitorCounterStringAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGetPerfMonitorCountersAMD: TglGetPerfMonitorCountersAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGetPerfMonitorGroupStringAMD: TglGetPerfMonitorGroupStringAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGetPerfMonitorGroupsAMD: TglGetPerfMonitorGroupsAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glGetPerfQueryDataINTEL: TglGetPerfQueryDataINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glGetPerfQueryIdByNameINTEL: TglGetPerfQueryIdByNameINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glGetPerfQueryInfoINTEL: TglGetPerfQueryInfoINTEL = nil;   // api(gl|glcore|gles2) //extension: GL_INTEL_performance_query
  glGetPointervKHR: TglGetPointervKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glGetProgramBinaryOES: TglGetProgramBinaryOES = nil;   // api(gles2) //extension: GL_OES_get_program_binary
  glGetProgramPipelineInfoLogEXT: TglGetProgramPipelineInfoLogEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glGetProgramPipelineivEXT: TglGetProgramPipelineivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glGetProgramResourceLocationIndexEXT: TglGetProgramResourceLocationIndexEXT = nil;   // api(gles2) //extension: GL_EXT_blend_func_extended
  glGetProgramResourcefvNV: TglGetProgramResourcefvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glGetQueryObjecti64v: TglGetQueryObjecti64v = nil; //alias: glGetQueryObjecti64vEXT
  glGetQueryObjecti64vEXT: TglGetQueryObjecti64vEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGetQueryObjectiv: TglGetQueryObjectiv = nil; //alias: glGetQueryObjectivARB
  glGetQueryObjectivEXT: TglGetQueryObjectivEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGetQueryObjectui64v: TglGetQueryObjectui64v = nil; //alias: glGetQueryObjectui64vEXT
  glGetQueryObjectui64vEXT: TglGetQueryObjectui64vEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGetQueryObjectuivEXT: TglGetQueryObjectuivEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGetQueryivEXT: TglGetQueryivEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glGetSamplerParameterIivEXT: TglGetSamplerParameterIivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glGetSamplerParameterIivOES: TglGetSamplerParameterIivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glGetSamplerParameterIuivEXT: TglGetSamplerParameterIuivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glGetSamplerParameterIuivOES: TglGetSamplerParameterIuivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glGetSemaphoreParameterivNV: TglGetSemaphoreParameterivNV = nil;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
  glGetSemaphoreParameterui64vEXT: TglGetSemaphoreParameterui64vEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glGetShadingRateImagePaletteNV: TglGetShadingRateImagePaletteNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glGetShadingRateSampleLocationivNV: TglGetShadingRateSampleLocationivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glGetSyncivAPPLE: TglGetSyncivAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glGetTexParameterIivEXT: TglGetTexParameterIivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glGetTexParameterIivOES: TglGetTexParameterIivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glGetTexParameterIuivEXT: TglGetTexParameterIuivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glGetTexParameterIuivOES: TglGetTexParameterIuivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glGetTextureHandleARB: TglGetTextureHandleARB = nil; //alias: glGetTextureHandleIMG
  glGetTextureHandleIMG: TglGetTextureHandleIMG = nil;   // api(gles2) //extension: GL_IMG_bindless_texture
  glGetTextureHandleNV: TglGetTextureHandleNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glGetTextureSamplerHandleARB: TglGetTextureSamplerHandleARB = nil; //alias: glGetTextureSamplerHandleIMG
  glGetTextureSamplerHandleIMG: TglGetTextureSamplerHandleIMG = nil;   // api(gles2) //extension: GL_IMG_bindless_texture
  glGetTextureSamplerHandleNV: TglGetTextureSamplerHandleNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glGetTranslatedShaderSourceANGLE: TglGetTranslatedShaderSourceANGLE = nil;   // api(gles2) //extension: GL_ANGLE_translated_shader_source
  glGetUniformi64vNV: TglGetUniformi64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glGetUnsignedBytevEXT: TglGetUnsignedBytevEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glGetUnsignedBytei_vEXT: TglGetUnsignedBytei_vEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glGetVertexAttribLdv: TglGetVertexAttribLdv = nil; //alias: glGetVertexAttribLdvEXT
  glGetVertexAttribdv: TglGetVertexAttribdv = nil; //alias: glGetVertexAttribdvARB
  glGetnUniformfvEXT: TglGetnUniformfvEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_robustness
  glGetnUniformfvKHR: TglGetnUniformfvKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
  glGetnUniformivEXT: TglGetnUniformivEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_robustness
  glGetnUniformivKHR: TglGetnUniformivKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
  glGetnUniformuivKHR: TglGetnUniformuivKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
  glHistogram: TglHistogram = nil; //alias: glHistogramEXT
  glImportMemoryFdEXT: TglImportMemoryFdEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object_fd
  glImportMemoryWin32HandleEXT: TglImportMemoryWin32HandleEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
  glImportMemoryWin32NameEXT: TglImportMemoryWin32NameEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object_win32
  glImportSemaphoreFdEXT: TglImportSemaphoreFdEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore_fd
  glImportSemaphoreWin32HandleEXT: TglImportSemaphoreWin32HandleEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore_win32
  glImportSemaphoreWin32NameEXT: TglImportSemaphoreWin32NameEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore_win32
  glInsertEventMarkerEXT: TglInsertEventMarkerEXT = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_EXT_debug_marker
  glInterpolatePathsNV: TglInterpolatePathsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glIsEnablediEXT: TglIsEnablediEXT = nil;   // api(gles2) //extension: GL_EXT_draw_buffers_indexed
  glIsEnablediNV: TglIsEnablediNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glIsEnablediOES: TglIsEnablediOES = nil;   // api(gles2) //extension: GL_OES_draw_buffers_indexed
  glIsFenceNV: TglIsFenceNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glIsImageHandleResidentNV: TglIsImageHandleResidentNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glIsMemoryObjectEXT: TglIsMemoryObjectEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glIsPathNV: TglIsPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glIsPointInFillPathNV: TglIsPointInFillPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glIsPointInStrokePathNV: TglIsPointInStrokePathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glIsProgramARB: TglIsProgramARB = nil; //alias: glIsProgramNV
  glIsProgramPipelineEXT: TglIsProgramPipelineEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glIsQueryEXT: TglIsQueryEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glIsSemaphoreEXT: TglIsSemaphoreEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glIsSyncAPPLE: TglIsSyncAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glIsTextureHandleResidentNV: TglIsTextureHandleResidentNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glIsVertexArrayOES: TglIsVertexArrayOES = nil;   // api(gles1|gles2) //extension: GL_OES_vertex_array_object
  glLabelObjectEXT: TglLabelObjectEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_debug_label
  glLoadTransposeMatrixd: TglLoadTransposeMatrixd = nil; //alias: glLoadTransposeMatrixdARB
  glLoadTransposeMatrixf: TglLoadTransposeMatrixf = nil; //alias: glLoadTransposeMatrixfARB
  glMakeImageHandleNonResidentNV: TglMakeImageHandleNonResidentNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glMakeImageHandleResidentNV: TglMakeImageHandleResidentNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glMakeTextureHandleNonResidentNV: TglMakeTextureHandleNonResidentNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glMakeTextureHandleResidentNV: TglMakeTextureHandleResidentNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glMapBuffer: TglMapBuffer = nil; //alias: glMapBufferARB
  glMapBufferOES: TglMapBufferOES = nil;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
  glMapBufferRangeEXT: TglMapBufferRangeEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_map_buffer_range
  glMatrixFrustumEXT: TglMatrixFrustumEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoad3x2fNV: TglMatrixLoad3x2fNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoad3x3fNV: TglMatrixLoad3x3fNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoadIdentityEXT: TglMatrixLoadIdentityEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoadTranspose3x3fNV: TglMatrixLoadTranspose3x3fNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoadTransposedEXT: TglMatrixLoadTransposedEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoadTransposefEXT: TglMatrixLoadTransposefEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoaddEXT: TglMatrixLoaddEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixLoadfEXT: TglMatrixLoadfEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMult3x2fNV: TglMatrixMult3x2fNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMult3x3fNV: TglMatrixMult3x3fNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMultTranspose3x3fNV: TglMatrixMultTranspose3x3fNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMultTransposedEXT: TglMatrixMultTransposedEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMultTransposefEXT: TglMatrixMultTransposefEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMultdEXT: TglMatrixMultdEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixMultfEXT: TglMatrixMultfEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixOrthoEXT: TglMatrixOrthoEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixPopEXT: TglMatrixPopEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixPushEXT: TglMatrixPushEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixRotatedEXT: TglMatrixRotatedEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixRotatefEXT: TglMatrixRotatefEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixScaledEXT: TglMatrixScaledEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixScalefEXT: TglMatrixScalefEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixTranslatedEXT: TglMatrixTranslatedEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMatrixTranslatefEXT: TglMatrixTranslatefEXT = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glMaxShaderCompilerThreadsKHR: TglMaxShaderCompilerThreadsKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_parallel_shader_compile //alias: glMaxShaderCompilerThreadsARB
  glMemoryObjectParameterivEXT: TglMemoryObjectParameterivEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glMinSampleShadingOES: TglMinSampleShadingOES = nil;   // api(gles2) //extension: GL_OES_sample_shading
  glMinmax: TglMinmax = nil; //alias: glMinmaxEXT
  glMultTransposeMatrixd: TglMultTransposeMatrixd = nil; //alias: glMultTransposeMatrixdARB
  glMultTransposeMatrixf: TglMultTransposeMatrixf = nil; //alias: glMultTransposeMatrixfARB
  glMultiDrawArrays: TglMultiDrawArrays = nil; //alias: glMultiDrawArraysEXT
  glMultiDrawArraysEXT: TglMultiDrawArraysEXT = nil;   // api(gl|gles1|gles2) //extension: GL_EXT_multi_draw_arrays
  glMultiDrawArraysIndirect: TglMultiDrawArraysIndirect = nil; //alias: glMultiDrawArraysIndirectAMD
  glMultiDrawArraysIndirectCount: TglMultiDrawArraysIndirectCount = nil; //alias: glMultiDrawArraysIndirectCountARB
  glMultiDrawArraysIndirectEXT: TglMultiDrawArraysIndirectEXT = nil;   // api(gles2) //extension: GL_EXT_multi_draw_indirect
  glMultiDrawElements: TglMultiDrawElements = nil; //alias: glMultiDrawElementsEXT
  glMultiDrawElementsBaseVertex: TglMultiDrawElementsBaseVertex = nil; //alias: glMultiDrawElementsBaseVertexEXT
  glMultiDrawElementsBaseVertexEXT: TglMultiDrawElementsBaseVertexEXT = nil;   // api(gles2) //extension: GL_EXT_draw_elements_base_vertex
  glMultiDrawElementsEXT: TglMultiDrawElementsEXT = nil;   // api(gl|gles1|gles2) //extension: GL_EXT_multi_draw_arrays
  glMultiDrawElementsIndirect: TglMultiDrawElementsIndirect = nil; //alias: glMultiDrawElementsIndirectAMD
  glMultiDrawElementsIndirectCount: TglMultiDrawElementsIndirectCount = nil; //alias: glMultiDrawElementsIndirectCountARB
  glMultiDrawElementsIndirectEXT: TglMultiDrawElementsIndirectEXT = nil;   // api(gles2) //extension: GL_EXT_multi_draw_indirect
  glMultiDrawMeshTasksIndirectNV: TglMultiDrawMeshTasksIndirectNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
  glMultiDrawMeshTasksIndirectCountNV: TglMultiDrawMeshTasksIndirectCountNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_mesh_shader
  glMultiTexCoord1d: TglMultiTexCoord1d = nil; //alias: glMultiTexCoord1dARB
  glMultiTexCoord1dv: TglMultiTexCoord1dv = nil; //alias: glMultiTexCoord1dvARB
  glMultiTexCoord1f: TglMultiTexCoord1f = nil; //alias: glMultiTexCoord1fARB
  glMultiTexCoord1fv: TglMultiTexCoord1fv = nil; //alias: glMultiTexCoord1fvARB
  glMultiTexCoord1i: TglMultiTexCoord1i = nil; //alias: glMultiTexCoord1iARB
  glMultiTexCoord1iv: TglMultiTexCoord1iv = nil; //alias: glMultiTexCoord1ivARB
  glMultiTexCoord1s: TglMultiTexCoord1s = nil; //alias: glMultiTexCoord1sARB
  glMultiTexCoord1sv: TglMultiTexCoord1sv = nil; //alias: glMultiTexCoord1svARB
  glMultiTexCoord2d: TglMultiTexCoord2d = nil; //alias: glMultiTexCoord2dARB
  glMultiTexCoord2dv: TglMultiTexCoord2dv = nil; //alias: glMultiTexCoord2dvARB
  glMultiTexCoord2f: TglMultiTexCoord2f = nil; //alias: glMultiTexCoord2fARB
  glMultiTexCoord2fv: TglMultiTexCoord2fv = nil; //alias: glMultiTexCoord2fvARB
  glMultiTexCoord2i: TglMultiTexCoord2i = nil; //alias: glMultiTexCoord2iARB
  glMultiTexCoord2iv: TglMultiTexCoord2iv = nil; //alias: glMultiTexCoord2ivARB
  glMultiTexCoord2s: TglMultiTexCoord2s = nil; //alias: glMultiTexCoord2sARB
  glMultiTexCoord2sv: TglMultiTexCoord2sv = nil; //alias: glMultiTexCoord2svARB
  glMultiTexCoord3d: TglMultiTexCoord3d = nil; //alias: glMultiTexCoord3dARB
  glMultiTexCoord3dv: TglMultiTexCoord3dv = nil; //alias: glMultiTexCoord3dvARB
  glMultiTexCoord3f: TglMultiTexCoord3f = nil; //alias: glMultiTexCoord3fARB
  glMultiTexCoord3fv: TglMultiTexCoord3fv = nil; //alias: glMultiTexCoord3fvARB
  glMultiTexCoord3i: TglMultiTexCoord3i = nil; //alias: glMultiTexCoord3iARB
  glMultiTexCoord3iv: TglMultiTexCoord3iv = nil; //alias: glMultiTexCoord3ivARB
  glMultiTexCoord3s: TglMultiTexCoord3s = nil; //alias: glMultiTexCoord3sARB
  glMultiTexCoord3sv: TglMultiTexCoord3sv = nil; //alias: glMultiTexCoord3svARB
  glMultiTexCoord4d: TglMultiTexCoord4d = nil; //alias: glMultiTexCoord4dARB
  glMultiTexCoord4dv: TglMultiTexCoord4dv = nil; //alias: glMultiTexCoord4dvARB
  glMultiTexCoord4f: TglMultiTexCoord4f = nil; //alias: glMultiTexCoord4fARB
  glMultiTexCoord4fv: TglMultiTexCoord4fv = nil; //alias: glMultiTexCoord4fvARB
  glMultiTexCoord4i: TglMultiTexCoord4i = nil; //alias: glMultiTexCoord4iARB
  glMultiTexCoord4iv: TglMultiTexCoord4iv = nil; //alias: glMultiTexCoord4ivARB
  glMultiTexCoord4s: TglMultiTexCoord4s = nil; //alias: glMultiTexCoord4sARB
  glMultiTexCoord4sv: TglMultiTexCoord4sv = nil; //alias: glMultiTexCoord4svARB
  glNamedBufferAttachMemoryNV: TglNamedBufferAttachMemoryNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
  glNamedBufferPageCommitmentMemNV: TglNamedBufferPageCommitmentMemNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_object_sparse
  glNamedBufferStorage: TglNamedBufferStorage = nil; //alias: glNamedBufferStorageEXT
  glNamedBufferStorageExternalEXT: TglNamedBufferStorageExternalEXT = nil;   // api(gl|gles2) //extension: GL_EXT_external_buffer
  glNamedBufferStorageMemEXT: TglNamedBufferStorageMemEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glNamedBufferSubData: TglNamedBufferSubData = nil; //alias: glNamedBufferSubDataEXT
  glNamedFramebufferSampleLocationsfvNV: TglNamedFramebufferSampleLocationsfvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
  glNamedRenderbufferStorageMultisampleAdvancedAMD: TglNamedRenderbufferStorageMultisampleAdvancedAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
  glObjectLabelKHR: TglObjectLabelKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glObjectPtrLabelKHR: TglObjectPtrLabelKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glPatchParameteriEXT: TglPatchParameteriEXT = nil;   // api(gles2) //extension: GL_EXT_tessellation_shader
  glPatchParameteriOES: TglPatchParameteriOES = nil;   // api(gles2) //extension: GL_OES_tessellation_shader
  glPathColorGenNV: TglPathColorGenNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathCommandsNV: TglPathCommandsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathCoordsNV: TglPathCoordsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathCoverDepthFuncNV: TglPathCoverDepthFuncNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathDashArrayNV: TglPathDashArrayNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathFogGenNV: TglPathFogGenNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathGlyphIndexArrayNV: TglPathGlyphIndexArrayNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathGlyphIndexRangeNV: TglPathGlyphIndexRangeNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathGlyphRangeNV: TglPathGlyphRangeNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathGlyphsNV: TglPathGlyphsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathMemoryGlyphIndexArrayNV: TglPathMemoryGlyphIndexArrayNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathParameterfNV: TglPathParameterfNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathParameterfvNV: TglPathParameterfvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathParameteriNV: TglPathParameteriNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathParameterivNV: TglPathParameterivNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathStencilDepthOffsetNV: TglPathStencilDepthOffsetNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathStencilFuncNV: TglPathStencilFuncNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathStringNV: TglPathStringNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathSubCommandsNV: TglPathSubCommandsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathSubCoordsNV: TglPathSubCoordsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPathTexGenNV: TglPathTexGenNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPointAlongPathNV: TglPointAlongPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glPointParameterf: TglPointParameterf = nil; //alias: glPointParameterfARB
  glPointParameterfv: TglPointParameterfv = nil; //alias: glPointParameterfvARB
  glPointParameteri: TglPointParameteri = nil; //alias: glPointParameteriNV
  glPointParameteriv: TglPointParameteriv = nil; //alias: glPointParameterivNV
  glPolygonMode: TglPolygonMode = nil; //alias: glPolygonModeNV
  glPolygonModeNV: TglPolygonModeNV = nil;   // api(gles2) //extension: GL_NV_polygon_mode
  glPolygonOffsetClamp: TglPolygonOffsetClamp = nil; //alias: glPolygonOffsetClampEXT
  glPolygonOffsetClampEXT: TglPolygonOffsetClampEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_polygon_offset_clamp
  glPopDebugGroupKHR: TglPopDebugGroupKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glPopGroupMarkerEXT: TglPopGroupMarkerEXT = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_EXT_debug_marker
  glPrimitiveBoundingBoxEXT: TglPrimitiveBoundingBoxEXT = nil;   // api(gles2) //extension: GL_EXT_primitive_bounding_box
  glPrimitiveBoundingBoxOES: TglPrimitiveBoundingBoxOES = nil;   // api(gles2) //extension: GL_OES_primitive_bounding_box
  glPrioritizeTextures: TglPrioritizeTextures = nil; //alias: glPrioritizeTexturesEXT
  glProgramBinaryOES: TglProgramBinaryOES = nil;   // api(gles2) //extension: GL_OES_get_program_binary
  glProgramParameteriEXT: TglProgramParameteriEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramPathFragmentInputGenNV: TglProgramPathFragmentInputGenNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glProgramUniform1fEXT: TglProgramUniform1fEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform1fvEXT: TglProgramUniform1fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform1i64NV: TglProgramUniform1i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform1i64vNV: TglProgramUniform1i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform1iEXT: TglProgramUniform1iEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform1ivEXT: TglProgramUniform1ivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform1ui64NV: TglProgramUniform1ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform1ui64vNV: TglProgramUniform1ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform1uiEXT: TglProgramUniform1uiEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform1uivEXT: TglProgramUniform1uivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform2fEXT: TglProgramUniform2fEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform2fvEXT: TglProgramUniform2fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform2i64NV: TglProgramUniform2i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform2i64vNV: TglProgramUniform2i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform2iEXT: TglProgramUniform2iEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform2ivEXT: TglProgramUniform2ivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform2ui64NV: TglProgramUniform2ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform2ui64vNV: TglProgramUniform2ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform2uiEXT: TglProgramUniform2uiEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform2uivEXT: TglProgramUniform2uivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform3fEXT: TglProgramUniform3fEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform3fvEXT: TglProgramUniform3fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform3i64NV: TglProgramUniform3i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform3i64vNV: TglProgramUniform3i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform3iEXT: TglProgramUniform3iEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform3ivEXT: TglProgramUniform3ivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform3ui64NV: TglProgramUniform3ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform3ui64vNV: TglProgramUniform3ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform3uiEXT: TglProgramUniform3uiEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform3uivEXT: TglProgramUniform3uivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform4fEXT: TglProgramUniform4fEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform4fvEXT: TglProgramUniform4fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform4i64NV: TglProgramUniform4i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform4i64vNV: TglProgramUniform4i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform4iEXT: TglProgramUniform4iEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform4ivEXT: TglProgramUniform4ivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform4ui64NV: TglProgramUniform4ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform4ui64vNV: TglProgramUniform4ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glProgramUniform4uiEXT: TglProgramUniform4uiEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniform4uivEXT: TglProgramUniform4uivEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformHandleui64ARB: TglProgramUniformHandleui64ARB = nil; //alias: glProgramUniformHandleui64IMG
  glProgramUniformHandleui64IMG: TglProgramUniformHandleui64IMG = nil;   // api(gles2) //extension: GL_IMG_bindless_texture
  glProgramUniformHandleui64NV: TglProgramUniformHandleui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glProgramUniformHandleui64vARB: TglProgramUniformHandleui64vARB = nil; //alias: glProgramUniformHandleui64vIMG
  glProgramUniformHandleui64vIMG: TglProgramUniformHandleui64vIMG = nil;   // api(gles2) //extension: GL_IMG_bindless_texture
  glProgramUniformHandleui64vNV: TglProgramUniformHandleui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glProgramUniformMatrix2fvEXT: TglProgramUniformMatrix2fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix2x3fvEXT: TglProgramUniformMatrix2x3fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix2x4fvEXT: TglProgramUniformMatrix2x4fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix3fvEXT: TglProgramUniformMatrix3fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix3x2fvEXT: TglProgramUniformMatrix3x2fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix3x4fvEXT: TglProgramUniformMatrix3x4fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix4fvEXT: TglProgramUniformMatrix4fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix4x2fvEXT: TglProgramUniformMatrix4x2fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProgramUniformMatrix4x3fvEXT: TglProgramUniformMatrix4x3fvEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glProvokingVertex: TglProvokingVertex = nil; //alias: glProvokingVertexEXT
  glPushDebugGroupKHR: TglPushDebugGroupKHR = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_KHR_debug
  glPushGroupMarkerEXT: TglPushGroupMarkerEXT = nil;   // api(gl|glcore|gles1|gles2) //extension: GL_EXT_debug_marker
  glQueryCounter: TglQueryCounter = nil; //alias: glQueryCounterEXT
  glQueryCounterEXT: TglQueryCounterEXT = nil;   // api(gles2) //extension: GL_EXT_disjoint_timer_query
  glRasterSamplesEXT: TglRasterSamplesEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_raster_multisample
  glReadBufferIndexedEXT: TglReadBufferIndexedEXT = nil;   // api(gles2) //extension: GL_EXT_multiview_draw_buffers
  glReadBufferNV: TglReadBufferNV = nil;   // api(gles2) //extension: GL_NV_read_buffer
  glReadnPixelsEXT: TglReadnPixelsEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_robustness
  glReadnPixelsKHR: TglReadnPixelsKHR = nil;   // api(gl|glcore|gles2) //extension: GL_KHR_robustness
  glReleaseKeyedMutexWin32EXT: TglReleaseKeyedMutexWin32EXT = nil;   // api(gl|gles2) //extension: GL_EXT_win32_keyed_mutex
  glRenderbufferStorageMultisampleANGLE: TglRenderbufferStorageMultisampleANGLE = nil;   // api(gles2) //extension: GL_ANGLE_framebuffer_multisample
  glRenderbufferStorageMultisampleAPPLE: TglRenderbufferStorageMultisampleAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
  glRenderbufferStorageMultisampleAdvancedAMD: TglRenderbufferStorageMultisampleAdvancedAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_framebuffer_multisample_advanced
  glRenderbufferStorageMultisampleEXT: TglRenderbufferStorageMultisampleEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_multisampled_render_to_texture
  glRenderbufferStorageMultisampleIMG: TglRenderbufferStorageMultisampleIMG = nil;   // api(gles1|gles2) //extension: GL_IMG_multisampled_render_to_texture
  glRenderbufferStorageMultisampleNV: TglRenderbufferStorageMultisampleNV = nil;   // api(gles2) //extension: GL_NV_framebuffer_multisample
  glResetHistogram: TglResetHistogram = nil; //alias: glResetHistogramEXT
  glResetMemoryObjectParameterNV: TglResetMemoryObjectParameterNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
  glResetMinmax: TglResetMinmax = nil; //alias: glResetMinmaxEXT
  glResolveDepthValuesNV: TglResolveDepthValuesNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_sample_locations
  glResolveMultisampleFramebufferAPPLE: TglResolveMultisampleFramebufferAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_framebuffer_multisample
  glSampleMaskEXT: TglSampleMaskEXT = nil; //alias: glSampleMaskSGIS
  glSamplePatternEXT: TglSamplePatternEXT = nil; //alias: glSamplePatternSGIS
  glSamplerParameterIivEXT: TglSamplerParameterIivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glSamplerParameterIivOES: TglSamplerParameterIivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glSamplerParameterIuivEXT: TglSamplerParameterIuivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glSamplerParameterIuivOES: TglSamplerParameterIuivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glScissorArrayv: TglScissorArrayv = nil; //alias: glScissorArrayvNV
  glScissorArrayvNV: TglScissorArrayvNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glScissorArrayvOES: TglScissorArrayvOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glScissorExclusiveArrayvNV: TglScissorExclusiveArrayvNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_scissor_exclusive
  glScissorExclusiveNV: TglScissorExclusiveNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_scissor_exclusive
  glScissorIndexed: TglScissorIndexed = nil; //alias: glScissorIndexedNV
  glScissorIndexedNV: TglScissorIndexedNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glScissorIndexedOES: TglScissorIndexedOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glScissorIndexedv: TglScissorIndexedv = nil; //alias: glScissorIndexedvNV
  glScissorIndexedvNV: TglScissorIndexedvNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glScissorIndexedvOES: TglScissorIndexedvOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glSecondaryColor3b: TglSecondaryColor3b = nil; //alias: glSecondaryColor3bEXT
  glSecondaryColor3bv: TglSecondaryColor3bv = nil; //alias: glSecondaryColor3bvEXT
  glSecondaryColor3d: TglSecondaryColor3d = nil; //alias: glSecondaryColor3dEXT
  glSecondaryColor3dv: TglSecondaryColor3dv = nil; //alias: glSecondaryColor3dvEXT
  glSecondaryColor3f: TglSecondaryColor3f = nil; //alias: glSecondaryColor3fEXT
  glSecondaryColor3fv: TglSecondaryColor3fv = nil; //alias: glSecondaryColor3fvEXT
  glSecondaryColor3i: TglSecondaryColor3i = nil; //alias: glSecondaryColor3iEXT
  glSecondaryColor3iv: TglSecondaryColor3iv = nil; //alias: glSecondaryColor3ivEXT
  glSecondaryColor3s: TglSecondaryColor3s = nil; //alias: glSecondaryColor3sEXT
  glSecondaryColor3sv: TglSecondaryColor3sv = nil; //alias: glSecondaryColor3svEXT
  glSecondaryColor3ub: TglSecondaryColor3ub = nil; //alias: glSecondaryColor3ubEXT
  glSecondaryColor3ubv: TglSecondaryColor3ubv = nil; //alias: glSecondaryColor3ubvEXT
  glSecondaryColor3ui: TglSecondaryColor3ui = nil; //alias: glSecondaryColor3uiEXT
  glSecondaryColor3uiv: TglSecondaryColor3uiv = nil; //alias: glSecondaryColor3uivEXT
  glSecondaryColor3us: TglSecondaryColor3us = nil; //alias: glSecondaryColor3usEXT
  glSecondaryColor3usv: TglSecondaryColor3usv = nil; //alias: glSecondaryColor3usvEXT
  glSecondaryColorPointer: TglSecondaryColorPointer = nil; //alias: glSecondaryColorPointerEXT
  glSelectPerfMonitorCountersAMD: TglSelectPerfMonitorCountersAMD = nil;   // api(gl|glcore|gles2) //extension: GL_AMD_performance_monitor
  glSemaphoreParameterivNV: TglSemaphoreParameterivNV = nil;   // api(gl|gles2) //extension: GL_NV_timeline_semaphore
  glSemaphoreParameterui64vEXT: TglSemaphoreParameterui64vEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glSeparableFilter2D: TglSeparableFilter2D = nil; //alias: glSeparableFilter2DEXT
  glSetFenceNV: TglSetFenceNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glShadingRateImageBarrierNV: TglShadingRateImageBarrierNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glShadingRateQCOM: TglShadingRateQCOM = nil;   // api(gles2) //extension: GL_QCOM_shading_rate
  glShadingRateImagePaletteNV: TglShadingRateImagePaletteNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glShadingRateSampleOrderNV: TglShadingRateSampleOrderNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glShadingRateSampleOrderCustomNV: TglShadingRateSampleOrderCustomNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_shading_rate_image
  glSignalSemaphoreEXT: TglSignalSemaphoreEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glSpecializeShader: TglSpecializeShader = nil; //alias: glSpecializeShaderARB
  glStartTilingQCOM: TglStartTilingQCOM = nil;   // api(gles1|gles2) //extension: GL_QCOM_tiled_rendering
  glStencilFillPathInstancedNV: TglStencilFillPathInstancedNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilFillPathNV: TglStencilFillPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilStrokePathInstancedNV: TglStencilStrokePathInstancedNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilStrokePathNV: TglStencilStrokePathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilThenCoverFillPathInstancedNV: TglStencilThenCoverFillPathInstancedNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilThenCoverFillPathNV: TglStencilThenCoverFillPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilThenCoverStrokePathInstancedNV: TglStencilThenCoverStrokePathInstancedNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glStencilThenCoverStrokePathNV: TglStencilThenCoverStrokePathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glSubpixelPrecisionBiasNV: TglSubpixelPrecisionBiasNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_conservative_raster
  glTestFenceNV: TglTestFenceNV = nil;   // api(gl|gles1|gles2) //extension: GL_NV_fence
  glTexAttachMemoryNV: TglTexAttachMemoryNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
  glTexBufferEXT: TglTexBufferEXT = nil;   // api(gles2) //extension: GL_EXT_texture_buffer
  glTexBufferOES: TglTexBufferOES = nil;   // api(gles2) //extension: GL_OES_texture_buffer
  glTexBufferRangeEXT: TglTexBufferRangeEXT = nil;   // api(gles2) //extension: GL_EXT_texture_buffer
  glTexBufferRangeOES: TglTexBufferRangeOES = nil;   // api(gles2) //extension: GL_OES_texture_buffer
  glTexEstimateMotionQCOM: TglTexEstimateMotionQCOM = nil;   // api(gles2) //extension: GL_QCOM_motion_estimation
  glTexEstimateMotionRegionsQCOM: TglTexEstimateMotionRegionsQCOM = nil;   // api(gles2) //extension: GL_QCOM_motion_estimation
  glTexImage3DOES: TglTexImage3DOES = nil;   // api(gles2) //extension: GL_OES_texture_3D
  glTexPageCommitmentARB: TglTexPageCommitmentARB = nil; //alias: glTexPageCommitmentEXT
  glTexPageCommitmentEXT: TglTexPageCommitmentEXT = nil;   // api(gles2) //extension: GL_EXT_sparse_texture
  glTexPageCommitmentMemNV: TglTexPageCommitmentMemNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_object_sparse
  glTexParameterIivEXT: TglTexParameterIivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glTexParameterIivOES: TglTexParameterIivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glTexParameterIuivEXT: TglTexParameterIuivEXT = nil;   // api(gles2) //extension: GL_EXT_texture_border_clamp
  glTexParameterIuivOES: TglTexParameterIuivOES = nil;   // api(gles2) //extension: GL_OES_texture_border_clamp
  glTexStorage1D: TglTexStorage1D = nil; //alias: glTexStorage1DEXT
  glTexStorage1DEXT: TglTexStorage1DEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
  glTexStorage2DEXT: TglTexStorage2DEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
  glTexStorage3DEXT: TglTexStorage3DEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
  glTexStorage3DMultisampleOES: TglTexStorage3DMultisampleOES = nil;   // api(gles2) //extension: GL_OES_texture_storage_multisample_2d_array
  glTexStorageMem1DEXT: TglTexStorageMem1DEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTexStorageMem2DEXT: TglTexStorageMem2DEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTexStorageMem2DMultisampleEXT: TglTexStorageMem2DMultisampleEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTexStorageMem3DEXT: TglTexStorageMem3DEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTexStorageMem3DMultisampleEXT: TglTexStorageMem3DMultisampleEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTexSubImage1D: TglTexSubImage1D = nil; //alias: glTexSubImage1DEXT
  glTexSubImage3DOES: TglTexSubImage3DOES = nil;   // api(gles2) //extension: GL_OES_texture_3D
  glTextureAttachMemoryNV: TglTextureAttachMemoryNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_attachment
  glTextureFoveationParametersQCOM: TglTextureFoveationParametersQCOM = nil;   // api(gles2) //extension: GL_QCOM_texture_foveated
  glTexturePageCommitmentMemNV: TglTexturePageCommitmentMemNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_memory_object_sparse
  glTextureStorage1DEXT: TglTextureStorage1DEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
  glTextureStorage2DEXT: TglTextureStorage2DEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
  glTextureStorage3DEXT: TglTextureStorage3DEXT = nil;   // api(gles1|gles2) //extension: GL_EXT_texture_storage
  glTextureStorageMem1DEXT: TglTextureStorageMem1DEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTextureStorageMem2DEXT: TglTextureStorageMem2DEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTextureStorageMem2DMultisampleEXT: TglTextureStorageMem2DMultisampleEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTextureStorageMem3DEXT: TglTextureStorageMem3DEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTextureStorageMem3DMultisampleEXT: TglTextureStorageMem3DMultisampleEXT = nil;   // api(gl|gles2) //extension: GL_EXT_memory_object
  glTextureView: TglTextureView = nil; //alias: glTextureViewEXT
  glTextureViewEXT: TglTextureViewEXT = nil;   // api(gles2) //extension: GL_EXT_texture_view
  glTextureViewOES: TglTextureViewOES = nil;   // api(gles2) //extension: GL_OES_texture_view
  glTransformPathNV: TglTransformPathNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glUniform1i64NV: TglUniform1i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform1i64vNV: TglUniform1i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform1ui64NV: TglUniform1ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform1ui64vNV: TglUniform1ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform2i64NV: TglUniform2i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform2i64vNV: TglUniform2i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform2ui64NV: TglUniform2ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform2ui64vNV: TglUniform2ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform3i64NV: TglUniform3i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform3i64vNV: TglUniform3i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform3ui64NV: TglUniform3ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform3ui64vNV: TglUniform3ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform4i64NV: TglUniform4i64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform4i64vNV: TglUniform4i64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform4ui64NV: TglUniform4ui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniform4ui64vNV: TglUniform4ui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_gpu_shader5
  glUniformHandleui64ARB: TglUniformHandleui64ARB = nil; //alias: glUniformHandleui64IMG
  glUniformHandleui64IMG: TglUniformHandleui64IMG = nil;   // api(gles2) //extension: GL_IMG_bindless_texture
  glUniformHandleui64NV: TglUniformHandleui64NV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glUniformHandleui64vARB: TglUniformHandleui64vARB = nil; //alias: glUniformHandleui64vIMG
  glUniformHandleui64vIMG: TglUniformHandleui64vIMG = nil;   // api(gles2) //extension: GL_IMG_bindless_texture
  glUniformHandleui64vNV: TglUniformHandleui64vNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_bindless_texture
  glUniformMatrix2x3fvNV: TglUniformMatrix2x3fvNV = nil;   // api(gles2) //extension: GL_NV_non_square_matrices
  glUniformMatrix2x4fvNV: TglUniformMatrix2x4fvNV = nil;   // api(gles2) //extension: GL_NV_non_square_matrices
  glUniformMatrix3x2fvNV: TglUniformMatrix3x2fvNV = nil;   // api(gles2) //extension: GL_NV_non_square_matrices
  glUniformMatrix3x4fvNV: TglUniformMatrix3x4fvNV = nil;   // api(gles2) //extension: GL_NV_non_square_matrices
  glUniformMatrix4x2fvNV: TglUniformMatrix4x2fvNV = nil;   // api(gles2) //extension: GL_NV_non_square_matrices
  glUniformMatrix4x3fvNV: TglUniformMatrix4x3fvNV = nil;   // api(gles2) //extension: GL_NV_non_square_matrices
  glUnmapBufferOES: TglUnmapBufferOES = nil;   // api(gles1|gles2) //extension: GL_OES_mapbuffer
  glUseProgramStagesEXT: TglUseProgramStagesEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glUseShaderProgramEXT: TglUseShaderProgramEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glValidateProgramPipelineEXT: TglValidateProgramPipelineEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_separate_shader_objects
  glVertexAttrib1d: TglVertexAttrib1d = nil; //alias: glVertexAttrib1dARB
  glVertexAttrib1dv: TglVertexAttrib1dv = nil; //alias: glVertexAttrib1dvARB
  glVertexAttrib1s: TglVertexAttrib1s = nil; //alias: glVertexAttrib1sARB
  glVertexAttrib1sv: TglVertexAttrib1sv = nil; //alias: glVertexAttrib1svARB
  glVertexAttrib2d: TglVertexAttrib2d = nil; //alias: glVertexAttrib2dARB
  glVertexAttrib2dv: TglVertexAttrib2dv = nil; //alias: glVertexAttrib2dvARB
  glVertexAttrib2s: TglVertexAttrib2s = nil; //alias: glVertexAttrib2sARB
  glVertexAttrib2sv: TglVertexAttrib2sv = nil; //alias: glVertexAttrib2svARB
  glVertexAttrib3d: TglVertexAttrib3d = nil; //alias: glVertexAttrib3dARB
  glVertexAttrib3dv: TglVertexAttrib3dv = nil; //alias: glVertexAttrib3dvARB
  glVertexAttrib3s: TglVertexAttrib3s = nil; //alias: glVertexAttrib3sARB
  glVertexAttrib3sv: TglVertexAttrib3sv = nil; //alias: glVertexAttrib3svARB
  glVertexAttrib4Nbv: TglVertexAttrib4Nbv = nil; //alias: glVertexAttrib4NbvARB
  glVertexAttrib4Niv: TglVertexAttrib4Niv = nil; //alias: glVertexAttrib4NivARB
  glVertexAttrib4Nsv: TglVertexAttrib4Nsv = nil; //alias: glVertexAttrib4NsvARB
  glVertexAttrib4Nub: TglVertexAttrib4Nub = nil; //alias: glVertexAttrib4NubARB
  glVertexAttrib4Nubv: TglVertexAttrib4Nubv = nil; //alias: glVertexAttrib4NubvARB
  glVertexAttrib4Nuiv: TglVertexAttrib4Nuiv = nil; //alias: glVertexAttrib4NuivARB
  glVertexAttrib4Nusv: TglVertexAttrib4Nusv = nil; //alias: glVertexAttrib4NusvARB
  glVertexAttrib4bv: TglVertexAttrib4bv = nil; //alias: glVertexAttrib4bvARB
  glVertexAttrib4d: TglVertexAttrib4d = nil; //alias: glVertexAttrib4dARB
  glVertexAttrib4dv: TglVertexAttrib4dv = nil; //alias: glVertexAttrib4dvARB
  glVertexAttrib4iv: TglVertexAttrib4iv = nil; //alias: glVertexAttrib4ivARB
  glVertexAttrib4s: TglVertexAttrib4s = nil; //alias: glVertexAttrib4sARB
  glVertexAttrib4sv: TglVertexAttrib4sv = nil; //alias: glVertexAttrib4svARB
  glVertexAttrib4ubv: TglVertexAttrib4ubv = nil; //alias: glVertexAttrib4ubvARB
  glVertexAttrib4uiv: TglVertexAttrib4uiv = nil; //alias: glVertexAttrib4uivARB
  glVertexAttrib4usv: TglVertexAttrib4usv = nil; //alias: glVertexAttrib4usvARB
  glVertexAttribDivisorANGLE: TglVertexAttribDivisorANGLE = nil;   // api(gles2) //extension: GL_ANGLE_instanced_arrays
  glVertexAttribDivisorEXT: TglVertexAttribDivisorEXT = nil;   // api(gles2) //extension: GL_EXT_instanced_arrays
  glVertexAttribDivisorNV: TglVertexAttribDivisorNV = nil;   // api(gles2) //extension: GL_NV_instanced_arrays
  glVertexAttribI1i: TglVertexAttribI1i = nil; //alias: glVertexAttribI1iEXT
  glVertexAttribI1iv: TglVertexAttribI1iv = nil; //alias: glVertexAttribI1ivEXT
  glVertexAttribI1ui: TglVertexAttribI1ui = nil; //alias: glVertexAttribI1uiEXT
  glVertexAttribI1uiv: TglVertexAttribI1uiv = nil; //alias: glVertexAttribI1uivEXT
  glVertexAttribI2i: TglVertexAttribI2i = nil; //alias: glVertexAttribI2iEXT
  glVertexAttribI2iv: TglVertexAttribI2iv = nil; //alias: glVertexAttribI2ivEXT
  glVertexAttribI2ui: TglVertexAttribI2ui = nil; //alias: glVertexAttribI2uiEXT
  glVertexAttribI2uiv: TglVertexAttribI2uiv = nil; //alias: glVertexAttribI2uivEXT
  glVertexAttribI3i: TglVertexAttribI3i = nil; //alias: glVertexAttribI3iEXT
  glVertexAttribI3iv: TglVertexAttribI3iv = nil; //alias: glVertexAttribI3ivEXT
  glVertexAttribI3ui: TglVertexAttribI3ui = nil; //alias: glVertexAttribI3uiEXT
  glVertexAttribI3uiv: TglVertexAttribI3uiv = nil; //alias: glVertexAttribI3uivEXT
  glVertexAttribI4bv: TglVertexAttribI4bv = nil; //alias: glVertexAttribI4bvEXT
  glVertexAttribI4sv: TglVertexAttribI4sv = nil; //alias: glVertexAttribI4svEXT
  glVertexAttribI4ubv: TglVertexAttribI4ubv = nil; //alias: glVertexAttribI4ubvEXT
  glVertexAttribI4usv: TglVertexAttribI4usv = nil; //alias: glVertexAttribI4usvEXT
  glVertexAttribL1d: TglVertexAttribL1d = nil; //alias: glVertexAttribL1dEXT
  glVertexAttribL1dv: TglVertexAttribL1dv = nil; //alias: glVertexAttribL1dvEXT
  glVertexAttribL2d: TglVertexAttribL2d = nil; //alias: glVertexAttribL2dEXT
  glVertexAttribL2dv: TglVertexAttribL2dv = nil; //alias: glVertexAttribL2dvEXT
  glVertexAttribL3d: TglVertexAttribL3d = nil; //alias: glVertexAttribL3dEXT
  glVertexAttribL3dv: TglVertexAttribL3dv = nil; //alias: glVertexAttribL3dvEXT
  glVertexAttribL4d: TglVertexAttribL4d = nil; //alias: glVertexAttribL4dEXT
  glVertexAttribL4dv: TglVertexAttribL4dv = nil; //alias: glVertexAttribL4dvEXT
  glVertexAttribLPointer: TglVertexAttribLPointer = nil; //alias: glVertexAttribLPointerEXT
  glViewportArrayv: TglViewportArrayv = nil; //alias: glViewportArrayvNV
  glViewportArrayvNV: TglViewportArrayvNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glViewportArrayvOES: TglViewportArrayvOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glViewportIndexedf: TglViewportIndexedf = nil; //alias: glViewportIndexedfOES
  glViewportIndexedfOES: TglViewportIndexedfOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glViewportIndexedfNV: TglViewportIndexedfNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glViewportIndexedfv: TglViewportIndexedfv = nil; //alias: glViewportIndexedfvOES
  glViewportIndexedfvOES: TglViewportIndexedfvOES = nil;   // api(gles2) //extension: GL_OES_viewport_array
  glViewportIndexedfvNV: TglViewportIndexedfvNV = nil;   // api(gles2) //extension: GL_NV_viewport_array
  glViewportPositionWScaleNV: TglViewportPositionWScaleNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_clip_space_w_scaling
  glViewportSwizzleNV: TglViewportSwizzleNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_viewport_swizzle
  glWaitSemaphoreEXT: TglWaitSemaphoreEXT = nil;   // api(gl|gles2) //extension: GL_EXT_semaphore
  glWaitSyncAPPLE: TglWaitSyncAPPLE = nil;   // api(gles1|gles2) //extension: GL_APPLE_sync
  glWeightPathsNV: TglWeightPathsNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_path_rendering
  glWindowPos2d: TglWindowPos2d = nil; //alias: glWindowPos2dARB
  glWindowPos2dv: TglWindowPos2dv = nil; //alias: glWindowPos2dvARB
  glWindowPos2f: TglWindowPos2f = nil; //alias: glWindowPos2fARB
  glWindowPos2fv: TglWindowPos2fv = nil; //alias: glWindowPos2fvARB
  glWindowPos2i: TglWindowPos2i = nil; //alias: glWindowPos2iARB
  glWindowPos2iv: TglWindowPos2iv = nil; //alias: glWindowPos2ivARB
  glWindowPos2s: TglWindowPos2s = nil; //alias: glWindowPos2sARB
  glWindowPos2sv: TglWindowPos2sv = nil; //alias: glWindowPos2svARB
  glWindowPos3d: TglWindowPos3d = nil; //alias: glWindowPos3dARB
  glWindowPos3dv: TglWindowPos3dv = nil; //alias: glWindowPos3dvARB
  glWindowPos3f: TglWindowPos3f = nil; //alias: glWindowPos3fARB
  glWindowPos3fv: TglWindowPos3fv = nil; //alias: glWindowPos3fvARB
  glWindowPos3i: TglWindowPos3i = nil; //alias: glWindowPos3iARB
  glWindowPos3iv: TglWindowPos3iv = nil; //alias: glWindowPos3ivARB
  glWindowPos3s: TglWindowPos3s = nil; //alias: glWindowPos3sARB
  glWindowPos3sv: TglWindowPos3sv = nil; //alias: glWindowPos3svARB
  glWindowRectanglesEXT: TglWindowRectanglesEXT = nil;   // api(gl|glcore|gles2) //extension: GL_EXT_window_rectangles
  glDrawVkImageNV: TglDrawVkImageNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_draw_vulkan_image
  glGetVkProcAddrNV: TglGetVkProcAddrNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_draw_vulkan_image
  glWaitVkSemaphoreNV: TglWaitVkSemaphoreNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_draw_vulkan_image
  glSignalVkSemaphoreNV: TglSignalVkSemaphoreNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_draw_vulkan_image
  glSignalVkFenceNV: TglSignalVkFenceNV = nil;   // api(gl|glcore|gles2) //extension: GL_NV_draw_vulkan_image
  glFramebufferParameteriMESA: TglFramebufferParameteriMESA = nil;   // api(gl|glcore|gles2) //extension: GL_MESA_framebuffer_flip_y
  glGetFramebufferParameterivMESA: TglGetFramebufferParameterivMESA = nil;   // api(gl|glcore|gles2) //extension: GL_MESA_framebuffer_flip_y
 
//----- EGL ------
 
//------- EGL_VERSION_1_0----------------
  eglChooseConfig: TeglChooseConfig = nil;
  eglCopyBuffers: TeglCopyBuffers = nil;
  eglCreateContext: TeglCreateContext = nil;
  eglCreatePbufferSurface: TeglCreatePbufferSurface = nil;
  eglCreatePixmapSurface: TeglCreatePixmapSurface = nil;
  eglCreateWindowSurface: TeglCreateWindowSurface = nil;
  eglDestroyContext: TeglDestroyContext = nil;
  eglDestroySurface: TeglDestroySurface = nil;
  eglGetConfigAttrib: TeglGetConfigAttrib = nil;
  eglGetConfigs: TeglGetConfigs = nil;
  eglGetCurrentDisplay: TeglGetCurrentDisplay = nil;
  eglGetCurrentSurface: TeglGetCurrentSurface = nil;
  eglGetDisplay: TeglGetDisplay = nil;
  eglGetError: TeglGetError = nil;
  eglGetProcAddress: TeglGetProcAddress = nil;
  eglInitialize: TeglInitialize = nil;
  eglMakeCurrent: TeglMakeCurrent = nil;
  eglQueryContext: TeglQueryContext = nil;
  eglQueryString: TeglQueryString = nil;
  eglQuerySurface: TeglQuerySurface = nil;
  eglSwapBuffers: TeglSwapBuffers = nil;
  eglTerminate: TeglTerminate = nil;
  eglWaitGL: TeglWaitGL = nil;
  eglWaitNative: TeglWaitNative = nil;
 
//------- EGL_VERSION_1_1----------------
  eglBindTexImage: TeglBindTexImage = nil;
  eglReleaseTexImage: TeglReleaseTexImage = nil;
  eglSurfaceAttrib: TeglSurfaceAttrib = nil;
  eglSwapInterval: TeglSwapInterval = nil;
 
//------- EGL_VERSION_1_2----------------
  eglBindAPI: TeglBindAPI = nil;
  eglQueryAPI: TeglQueryAPI = nil;
  eglCreatePbufferFromClientBuffer: TeglCreatePbufferFromClientBuffer = nil;
  eglReleaseThread: TeglReleaseThread = nil;
  eglWaitClient: TeglWaitClient = nil;
 
//------- EGL_VERSION_1_3----------------
 
//------- EGL_VERSION_1_4----------------
  eglGetCurrentContext: TeglGetCurrentContext = nil;
 
//------- EGL_VERSION_1_5----------------
  eglCreateSync: TeglCreateSync = nil; //alias: eglCreateSync64KHR
  eglDestroySync: TeglDestroySync = nil; //alias: eglDestroySyncKHR
  eglClientWaitSync: TeglClientWaitSync = nil; //alias: eglClientWaitSyncKHR
  eglGetSyncAttrib: TeglGetSyncAttrib = nil;
  eglCreateImage: TeglCreateImage = nil;
  eglDestroyImage: TeglDestroyImage = nil; //alias: eglDestroyImageKHR
  eglGetPlatformDisplay: TeglGetPlatformDisplay = nil;
  eglCreatePlatformWindowSurface: TeglCreatePlatformWindowSurface = nil;
  eglCreatePlatformPixmapSurface: TeglCreatePlatformPixmapSurface = nil;
  eglWaitSync: TeglWaitSync = nil;
 
//------- Other ----------------
  eglClientSignalSyncEXT: TeglClientSignalSyncEXT = nil;   // api(egl) //extension: EGL_EXT_client_sync
  eglClientWaitSyncKHR: TeglClientWaitSyncKHR = nil;   // api(egl) //extension: EGL_KHR_fence_sync
  eglClientWaitSyncNV: TeglClientWaitSyncNV = nil;   // api(egl) //extension: EGL_NV_sync
  eglCreateDRMImageMESA: TeglCreateDRMImageMESA = nil;   // api(egl) //extension: EGL_MESA_drm_image
  eglCreateFenceSyncNV: TeglCreateFenceSyncNV = nil;   // api(egl) //extension: EGL_NV_sync
  eglCreateImageKHR: TeglCreateImageKHR = nil;   // api(egl) //extension: EGL_KHR_image
  eglCreateNativeClientBufferANDROID: TeglCreateNativeClientBufferANDROID = nil;   // api(egl) //extension: EGL_ANDROID_create_native_client_buffer
  eglCreatePixmapSurfaceHI: TeglCreatePixmapSurfaceHI = nil;   // api(egl) //extension: EGL_HI_clientpixmap
  eglCreatePlatformPixmapSurfaceEXT: TeglCreatePlatformPixmapSurfaceEXT = nil;   // api(egl) //extension: EGL_EXT_platform_base
  eglCreatePlatformWindowSurfaceEXT: TeglCreatePlatformWindowSurfaceEXT = nil;   // api(egl) //extension: EGL_EXT_platform_base
  eglCreateStreamFromFileDescriptorKHR: TeglCreateStreamFromFileDescriptorKHR = nil;   // api(egl) //extension: EGL_KHR_stream_cross_process_fd
  eglCreateStreamKHR: TeglCreateStreamKHR = nil;   // api(egl) //extension: EGL_KHR_stream
  eglCreateStreamAttribKHR: TeglCreateStreamAttribKHR = nil;   // api(egl) //extension: EGL_KHR_stream_attrib
  eglCreateStreamProducerSurfaceKHR: TeglCreateStreamProducerSurfaceKHR = nil;   // api(egl) //extension: EGL_KHR_stream_producer_eglsurface
  eglCreateStreamSyncNV: TeglCreateStreamSyncNV = nil;   // api(egl) //extension: EGL_NV_stream_sync
  eglCreateSyncKHR: TeglCreateSyncKHR = nil;   // api(egl) //extension: EGL_KHR_fence_sync
  eglCreateSync64KHR: TeglCreateSync64KHR = nil;   // api(egl) //extension: EGL_KHR_cl_event2
  eglDebugMessageControlKHR: TeglDebugMessageControlKHR = nil;   // api(egl) //extension: EGL_KHR_debug
  eglDestroyImageKHR: TeglDestroyImageKHR = nil;   // api(egl) //extension: EGL_KHR_image
  eglDestroyStreamKHR: TeglDestroyStreamKHR = nil;   // api(egl) //extension: EGL_KHR_stream
  eglDestroySyncKHR: TeglDestroySyncKHR = nil;   // api(egl) //extension: EGL_KHR_fence_sync
  eglDestroySyncNV: TeglDestroySyncNV = nil;   // api(egl) //extension: EGL_NV_sync
  eglDupNativeFenceFDANDROID: TeglDupNativeFenceFDANDROID = nil;   // api(egl) //extension: EGL_ANDROID_native_fence_sync
  eglExportDMABUFImageMESA: TeglExportDMABUFImageMESA = nil;   // api(egl) //extension: EGL_MESA_image_dma_buf_export
  eglExportDMABUFImageQueryMESA: TeglExportDMABUFImageQueryMESA = nil;   // api(egl) //extension: EGL_MESA_image_dma_buf_export
  eglExportDRMImageMESA: TeglExportDRMImageMESA = nil;   // api(egl) //extension: EGL_MESA_drm_image
  eglFenceNV: TeglFenceNV = nil;   // api(egl) //extension: EGL_NV_sync
  eglGetDisplayDriverConfig: TeglGetDisplayDriverConfig = nil;   // api(egl) //extension: EGL_MESA_query_driver
  eglGetDisplayDriverName: TeglGetDisplayDriverName = nil;   // api(egl) //extension: EGL_MESA_query_driver
  eglGetNativeClientBufferANDROID: TeglGetNativeClientBufferANDROID = nil;   // api(egl) //extension: EGL_ANDROID_get_native_client_buffer
  eglGetOutputLayersEXT: TeglGetOutputLayersEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglGetOutputPortsEXT: TeglGetOutputPortsEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglGetPlatformDisplayEXT: TeglGetPlatformDisplayEXT = nil;   // api(egl) //extension: EGL_EXT_platform_base
  eglGetStreamFileDescriptorKHR: TeglGetStreamFileDescriptorKHR = nil;   // api(egl) //extension: EGL_KHR_stream_cross_process_fd
  eglGetSyncAttribKHR: TeglGetSyncAttribKHR = nil;   // api(egl) //extension: EGL_KHR_fence_sync
  eglGetSyncAttribNV: TeglGetSyncAttribNV = nil;   // api(egl) //extension: EGL_NV_sync
  eglGetSystemTimeFrequencyNV: TeglGetSystemTimeFrequencyNV = nil;   // api(egl) //extension: EGL_NV_system_time
  eglGetSystemTimeNV: TeglGetSystemTimeNV = nil;   // api(egl) //extension: EGL_NV_system_time
  eglLabelObjectKHR: TeglLabelObjectKHR = nil;   // api(egl) //extension: EGL_KHR_debug
  eglLockSurfaceKHR: TeglLockSurfaceKHR = nil;   // api(egl) //extension: EGL_KHR_lock_surface
  eglOutputLayerAttribEXT: TeglOutputLayerAttribEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglOutputPortAttribEXT: TeglOutputPortAttribEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglPostSubBufferNV: TeglPostSubBufferNV = nil;   // api(egl) //extension: EGL_NV_post_sub_buffer
  eglPresentationTimeANDROID: TeglPresentationTimeANDROID = nil;   // api(egl) //extension: EGL_ANDROID_presentation_time
  eglGetCompositorTimingSupportedANDROID: TeglGetCompositorTimingSupportedANDROID = nil;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
  eglGetCompositorTimingANDROID: TeglGetCompositorTimingANDROID = nil;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
  eglGetNextFrameIdANDROID: TeglGetNextFrameIdANDROID = nil;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
  eglGetFrameTimestampSupportedANDROID: TeglGetFrameTimestampSupportedANDROID = nil;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
  eglGetFrameTimestampsANDROID: TeglGetFrameTimestampsANDROID = nil;   // api(egl) //extension: EGL_ANDROID_get_frame_timestamps
  eglQueryDebugKHR: TeglQueryDebugKHR = nil;   // api(egl) //extension: EGL_KHR_debug
  eglQueryDeviceAttribEXT: TeglQueryDeviceAttribEXT = nil;   // api(egl) //extension: EGL_EXT_device_base
  eglQueryDeviceStringEXT: TeglQueryDeviceStringEXT = nil;   // api(egl) //extension: EGL_EXT_device_base
  eglQueryDevicesEXT: TeglQueryDevicesEXT = nil;   // api(egl) //extension: EGL_EXT_device_base
  eglQueryDisplayAttribEXT: TeglQueryDisplayAttribEXT = nil;   // api(egl) //extension: EGL_EXT_device_base
  eglQueryDisplayAttribKHR: TeglQueryDisplayAttribKHR = nil;   // api(egl) //extension: EGL_KHR_display_reference //alias: eglQueryDisplayAttribEXT
  eglQueryDisplayAttribNV: TeglQueryDisplayAttribNV = nil;   // api(egl) //extension: EGL_NV_stream_metadata
  eglQueryDmaBufFormatsEXT: TeglQueryDmaBufFormatsEXT = nil;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
  eglQueryDmaBufModifiersEXT: TeglQueryDmaBufModifiersEXT = nil;   // api(egl) //extension: EGL_EXT_image_dma_buf_import_modifiers
  eglQueryNativeDisplayNV: TeglQueryNativeDisplayNV = nil;   // api(egl) //extension: EGL_NV_native_query
  eglQueryNativePixmapNV: TeglQueryNativePixmapNV = nil;   // api(egl) //extension: EGL_NV_native_query
  eglQueryNativeWindowNV: TeglQueryNativeWindowNV = nil;   // api(egl) //extension: EGL_NV_native_query
  eglQueryOutputLayerAttribEXT: TeglQueryOutputLayerAttribEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglQueryOutputLayerStringEXT: TeglQueryOutputLayerStringEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglQueryOutputPortAttribEXT: TeglQueryOutputPortAttribEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglQueryOutputPortStringEXT: TeglQueryOutputPortStringEXT = nil;   // api(egl) //extension: EGL_EXT_output_base
  eglQueryStreamKHR: TeglQueryStreamKHR = nil;   // api(egl) //extension: EGL_KHR_stream
  eglQueryStreamAttribKHR: TeglQueryStreamAttribKHR = nil;   // api(egl) //extension: EGL_KHR_stream_attrib
  eglQueryStreamMetadataNV: TeglQueryStreamMetadataNV = nil;   // api(egl) //extension: EGL_NV_stream_metadata
  eglQueryStreamTimeKHR: TeglQueryStreamTimeKHR = nil;   // api(egl) //extension: EGL_KHR_stream_fifo
  eglQueryStreamu64KHR: TeglQueryStreamu64KHR = nil;   // api(egl) //extension: EGL_KHR_stream
  eglQuerySurface64KHR: TeglQuerySurface64KHR = nil;   // api(egl) //extension: EGL_KHR_lock_surface3
  eglQuerySurfacePointerANGLE: TeglQuerySurfacePointerANGLE = nil;   // api(egl) //extension: EGL_ANGLE_query_surface_pointer
  eglResetStreamNV: TeglResetStreamNV = nil;   // api(egl) //extension: EGL_NV_stream_reset
  eglSetBlobCacheFuncsANDROID: TeglSetBlobCacheFuncsANDROID = nil;   // api(egl) //extension: EGL_ANDROID_blob_cache
  eglSetDamageRegionKHR: TeglSetDamageRegionKHR = nil;   // api(egl) //extension: EGL_KHR_partial_update
  eglSetStreamAttribKHR: TeglSetStreamAttribKHR = nil;   // api(egl) //extension: EGL_KHR_stream_attrib
  eglSetStreamMetadataNV: TeglSetStreamMetadataNV = nil;   // api(egl) //extension: EGL_NV_stream_metadata
  eglSignalSyncKHR: TeglSignalSyncKHR = nil;   // api(egl) //extension: EGL_KHR_reusable_sync
  eglSignalSyncNV: TeglSignalSyncNV = nil;   // api(egl) //extension: EGL_NV_sync
  eglStreamAttribKHR: TeglStreamAttribKHR = nil;   // api(egl) //extension: EGL_KHR_stream
  eglStreamConsumerAcquireKHR: TeglStreamConsumerAcquireKHR = nil;   // api(egl) //extension: EGL_KHR_stream_consumer_gltexture
  eglStreamConsumerAcquireAttribKHR: TeglStreamConsumerAcquireAttribKHR = nil;   // api(egl) //extension: EGL_KHR_stream_attrib
  eglStreamConsumerGLTextureExternalKHR: TeglStreamConsumerGLTextureExternalKHR = nil;   // api(egl) //extension: EGL_KHR_stream_consumer_gltexture
  eglStreamConsumerGLTextureExternalAttribsNV: TeglStreamConsumerGLTextureExternalAttribsNV = nil;   // api(egl) //extension: EGL_NV_stream_consumer_gltexture_yuv
  eglStreamConsumerOutputEXT: TeglStreamConsumerOutputEXT = nil;   // api(egl) //extension: EGL_EXT_stream_consumer_egloutput
  eglStreamConsumerReleaseKHR: TeglStreamConsumerReleaseKHR = nil;   // api(egl) //extension: EGL_KHR_stream_consumer_gltexture
  eglStreamConsumerReleaseAttribKHR: TeglStreamConsumerReleaseAttribKHR = nil;   // api(egl) //extension: EGL_KHR_stream_attrib
  eglStreamFlushNV: TeglStreamFlushNV = nil;   // api(egl) //extension: EGL_NV_stream_flush
  eglSwapBuffersWithDamageEXT: TeglSwapBuffersWithDamageEXT = nil;   // api(egl) //extension: EGL_EXT_swap_buffers_with_damage
  eglSwapBuffersWithDamageKHR: TeglSwapBuffersWithDamageKHR = nil;   // api(egl) //extension: EGL_KHR_swap_buffers_with_damage
  eglSwapBuffersRegionNOK: TeglSwapBuffersRegionNOK = nil;   // api(egl) //extension: EGL_NOK_swap_region
  eglSwapBuffersRegion2NOK: TeglSwapBuffersRegion2NOK = nil;   // api(egl) //extension: EGL_NOK_swap_region2
  eglUnlockSurfaceKHR: TeglUnlockSurfaceKHR = nil;   // api(egl) //extension: EGL_KHR_lock_surface
  eglUnsignalSyncEXT: TeglUnsignalSyncEXT = nil;   // api(egl) //extension: EGL_EXT_sync_reuse
  eglWaitSyncKHR: TeglWaitSyncKHR = nil;   // api(egl) //extension: EGL_KHR_wait_sync
  eglCompositorSetContextListEXT: TeglCompositorSetContextListEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglCompositorSetContextAttributesEXT: TeglCompositorSetContextAttributesEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglCompositorSetWindowListEXT: TeglCompositorSetWindowListEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglCompositorSetWindowAttributesEXT: TeglCompositorSetWindowAttributesEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglCompositorBindTexWindowEXT: TeglCompositorBindTexWindowEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglCompositorSetSizeEXT: TeglCompositorSetSizeEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglCompositorSwapPolicyEXT: TeglCompositorSwapPolicyEXT = nil;   // api(egl) //extension: EGL_EXT_compositor
  eglBindWaylandDisplayWL: TeglBindWaylandDisplayWL = nil;   // api(egl) //extension: EGL_WL_bind_wayland_display
  eglUnbindWaylandDisplayWL: TeglUnbindWaylandDisplayWL = nil;   // api(egl) //extension: EGL_WL_bind_wayland_display
  eglQueryWaylandBufferWL: TeglQueryWaylandBufferWL = nil;   // api(egl) //extension: EGL_WL_bind_wayland_display
  eglCreateWaylandBufferFromImageWL: TeglCreateWaylandBufferFromImageWL = nil;   // api(egl) //extension: EGL_WL_create_wayland_buffer_from_image
  eglStreamImageConsumerConnectNV: TeglStreamImageConsumerConnectNV = nil;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
  eglQueryStreamConsumerEventNV: TeglQueryStreamConsumerEventNV = nil;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
  eglStreamAcquireImageNV: TeglStreamAcquireImageNV = nil;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
  eglStreamReleaseImageNV: TeglStreamReleaseImageNV = nil;   // api(egl) //extension: EGL_NV_stream_consumer_eglimage
 

function  OpenGLES_Initialize(const alibEGL:string=LIBNAME_EGL;
                                 const alibOpenGLES:string=LIBNAME_OPENGLES): Boolean;
// Initialize only <<BASE>> OpenGLES API.

function  OpenGLES_InitializeAdvance(aEGLDisplay:EGLDisplay): Boolean;
// Initialize <<FULL>> OpenGLES API.
// WARNING: Call this function only after use of <<BASE>> opengles API funtions:
//          wglCreateContext and eglMakeCurrent.
//          Only then OpenGL Driver will find <<FULL>> API functions for Current Context

procedure OpenGLES_Finalize;

function  OpenGLESLib_InitOK: Boolean;
function  OpenGLESLib_ExtensionsCount: Integer;
function  OpenGLESLib_ExtensionGet(Name: string): Boolean;
function  OpenGLESLib_VersionNum: Integer;
function  OpenGLESLib_VersionStr: String;
 
function  OpenEGLLib_InitOK: Boolean;
function  OpenEGLLib_ExtensionsCount: Integer;
function  OpenEGLLib_ExtensionGet(Name: string): Boolean;
function  OpenEGLlib_VersionNum: Integer;
function  OpenEGLLib_VersionStr: String;
 
 var
  VarAllGLExtensions : Array of string;
  VarAllEGLExtensions: Array of string;
 
implementation
 
const
  InvalidLibHandle = 0;
var
  VarOpenGLLibHandle  : TLibHandle = InvalidLibHandle;
  VarEGLLibHandle: TLibHandle = InvalidLibHandle;
  VarOpenGLLoaded: Boolean = False;
  VarEGLLoaded: Boolean = False;
  VarEGLDisplay:EGLDisplay = NIL;

procedure FillGLExtensions;
var
  ExtStr, EName: string;
  I: Integer;
begin
  SetLength(VarAllGLExtensions, 0);
  if glGetString=nil then Exit;

  ExtStr:=string(PChar(glGetString(GL_EXTENSIONS))) + ' ';
  EName:='';
  for I:=1 to Length(ExtStr) do begin
    if ExtStr[I]=' ' then begin
      if EName <> '' then begin
        SetLength(VarAllGLExtensions, Length(VarAllGLExtensions) + 1);
        VarAllGLExtensions[High(VarAllGLExtensions)]:=EName;
      end;
      EName:='';
    end else EName += ExtStr[I];
  end;
end;

procedure FillEGLExtensions;
var
  ExtStr, EName: string;
  I: Integer;
begin
  SetLength(VarAllEGLExtensions, 0);
  if eglQueryString=nil then Exit;

  ExtStr:=string(PChar(eglQueryString(VarEGLDisplay,EGL_EXTENSIONS))) + ' ';
  EName:='';
  for I:=1 to Length(ExtStr) do begin
    if ExtStr[I]=' ' then begin
      if EName <> '' then begin
        SetLength(VarAllEGLExtensions, Length(VarAllEGLExtensions) + 1);
        VarAllEGLExtensions[High(VarAllEGLExtensions)]:=EName;
      end;
      EName:='';
    end else EName += ExtStr[I];
  end;
end;

Function _GetProcedureAddress(const ProcName : AnsiString) : Pointer;
begin
     result:=nil;
 
     if eglGetProcAddress<>nil then
        result:=eglGetProcAddress(PChar(ProcName));
 
     if result=nil then
        result:=dynlibs.GetProcedureAddress(VarEGLLibHandle,ProcName);
end;
 
function _Internal_LoadGLLibrary(const alibEGL:string=LIBNAME_EGL;
                                 const alibOpenGLES:string=LIBNAME_OPENGLES): Boolean;
begin
  Result:=False;
  VarOpenGLLibHandle:=dynlibs.LoadLibrary(alibOpenGLES);
  if VarOpenGLLibHandle=InvalidLibHandle then Exit(False);
 
  VarEGLLibHandle:=dynlibs.LoadLibrary(alibEGL);
  if VarEGLLibHandle=InvalidLibHandle then Exit(False);
 
  eglGetProcAddress:=TeglGetProcAddress(dynlibs.GetProcAddress(VarEGLLibHandle, 'eglGetProcAddress'));
 
  Result:=True;
  VarOpenGLLoaded:=True;
  VarEGLLoaded:=True;
 
end;
 
Procedure _Internal_LoadGLProc;
begin
 
  Pointer(glActiveTexture):=_GetProcedureAddress('glActiveTexture');
  Pointer(glAttachShader):=_GetProcedureAddress('glAttachShader');
  Pointer(glBindAttribLocation):=_GetProcedureAddress('glBindAttribLocation');
  Pointer(glBindBuffer):=_GetProcedureAddress('glBindBuffer');
  Pointer(glBindFramebuffer):=_GetProcedureAddress('glBindFramebuffer');
  Pointer(glBindRenderbuffer):=_GetProcedureAddress('glBindRenderbuffer');
  Pointer(glBindTexture):=_GetProcedureAddress('glBindTexture');
  Pointer(glBlendColor):=_GetProcedureAddress('glBlendColor');
  Pointer(glBlendEquation):=_GetProcedureAddress('glBlendEquation');
  Pointer(glBlendEquationSeparate):=_GetProcedureAddress('glBlendEquationSeparate');
  Pointer(glBlendFunc):=_GetProcedureAddress('glBlendFunc');
  Pointer(glBlendFuncSeparate):=_GetProcedureAddress('glBlendFuncSeparate');
  Pointer(glBufferData):=_GetProcedureAddress('glBufferData');
  Pointer(glBufferSubData):=_GetProcedureAddress('glBufferSubData');
  Pointer(glCheckFramebufferStatus):=_GetProcedureAddress('glCheckFramebufferStatus');
  Pointer(glClear):=_GetProcedureAddress('glClear');
  Pointer(glClearColor):=_GetProcedureAddress('glClearColor');
  Pointer(glClearDepthf):=_GetProcedureAddress('glClearDepthf');
  Pointer(glClearStencil):=_GetProcedureAddress('glClearStencil');
  Pointer(glColorMask):=_GetProcedureAddress('glColorMask');
  Pointer(glCompileShader):=_GetProcedureAddress('glCompileShader');
  Pointer(glCompressedTexImage2D):=_GetProcedureAddress('glCompressedTexImage2D');
  Pointer(glCompressedTexSubImage2D):=_GetProcedureAddress('glCompressedTexSubImage2D');
  Pointer(glCopyTexImage2D):=_GetProcedureAddress('glCopyTexImage2D');
  Pointer(glCopyTexSubImage2D):=_GetProcedureAddress('glCopyTexSubImage2D');
  Pointer(glCreateProgram):=_GetProcedureAddress('glCreateProgram');
  Pointer(glCreateShader):=_GetProcedureAddress('glCreateShader');
  Pointer(glCullFace):=_GetProcedureAddress('glCullFace');
  Pointer(glDeleteBuffers):=_GetProcedureAddress('glDeleteBuffers');
  Pointer(glDeleteFramebuffers):=_GetProcedureAddress('glDeleteFramebuffers');
  Pointer(glDeleteProgram):=_GetProcedureAddress('glDeleteProgram');
  Pointer(glDeleteRenderbuffers):=_GetProcedureAddress('glDeleteRenderbuffers');
  Pointer(glDeleteShader):=_GetProcedureAddress('glDeleteShader');
  Pointer(glDeleteTextures):=_GetProcedureAddress('glDeleteTextures');
  Pointer(glDepthFunc):=_GetProcedureAddress('glDepthFunc');
  Pointer(glDepthMask):=_GetProcedureAddress('glDepthMask');
  Pointer(glDepthRangef):=_GetProcedureAddress('glDepthRangef');
  Pointer(glDetachShader):=_GetProcedureAddress('glDetachShader');
  Pointer(glDisable):=_GetProcedureAddress('glDisable');
  Pointer(glDisableVertexAttribArray):=_GetProcedureAddress('glDisableVertexAttribArray');
  Pointer(glDrawArrays):=_GetProcedureAddress('glDrawArrays');
  Pointer(glDrawElements):=_GetProcedureAddress('glDrawElements');
  Pointer(glEnable):=_GetProcedureAddress('glEnable');
  Pointer(glEnableVertexAttribArray):=_GetProcedureAddress('glEnableVertexAttribArray');
  Pointer(glFinish):=_GetProcedureAddress('glFinish');
  Pointer(glFlush):=_GetProcedureAddress('glFlush');
  Pointer(glFramebufferRenderbuffer):=_GetProcedureAddress('glFramebufferRenderbuffer');
  Pointer(glFramebufferTexture2D):=_GetProcedureAddress('glFramebufferTexture2D');
  Pointer(glFrontFace):=_GetProcedureAddress('glFrontFace');
  Pointer(glGenBuffers):=_GetProcedureAddress('glGenBuffers');
  Pointer(glGenerateMipmap):=_GetProcedureAddress('glGenerateMipmap');
  Pointer(glGenFramebuffers):=_GetProcedureAddress('glGenFramebuffers');
  Pointer(glGenRenderbuffers):=_GetProcedureAddress('glGenRenderbuffers');
  Pointer(glGenTextures):=_GetProcedureAddress('glGenTextures');
  Pointer(glGetActiveAttrib):=_GetProcedureAddress('glGetActiveAttrib');
  Pointer(glGetActiveUniform):=_GetProcedureAddress('glGetActiveUniform');
  Pointer(glGetAttachedShaders):=_GetProcedureAddress('glGetAttachedShaders');
  Pointer(glGetAttribLocation):=_GetProcedureAddress('glGetAttribLocation');
  Pointer(glGetBooleanv):=_GetProcedureAddress('glGetBooleanv');
  Pointer(glGetBufferParameteriv):=_GetProcedureAddress('glGetBufferParameteriv');
  Pointer(glGetError):=_GetProcedureAddress('glGetError');
  Pointer(glGetFloatv):=_GetProcedureAddress('glGetFloatv');
  Pointer(glGetFramebufferAttachmentParameteriv):=_GetProcedureAddress('glGetFramebufferAttachmentParameteriv');
  Pointer(glGetIntegerv):=_GetProcedureAddress('glGetIntegerv');
  Pointer(glGetProgramiv):=_GetProcedureAddress('glGetProgramiv');
  Pointer(glGetProgramInfoLog):=_GetProcedureAddress('glGetProgramInfoLog');
  Pointer(glGetRenderbufferParameteriv):=_GetProcedureAddress('glGetRenderbufferParameteriv');
  Pointer(glGetShaderiv):=_GetProcedureAddress('glGetShaderiv');
  Pointer(glGetShaderInfoLog):=_GetProcedureAddress('glGetShaderInfoLog');
  Pointer(glGetShaderPrecisionFormat):=_GetProcedureAddress('glGetShaderPrecisionFormat');
  Pointer(glGetShaderSource):=_GetProcedureAddress('glGetShaderSource');
  Pointer(glGetString):=_GetProcedureAddress('glGetString');
  Pointer(glGetTexParameterfv):=_GetProcedureAddress('glGetTexParameterfv');
  Pointer(glGetTexParameteriv):=_GetProcedureAddress('glGetTexParameteriv');
  Pointer(glGetUniformfv):=_GetProcedureAddress('glGetUniformfv');
  Pointer(glGetUniformiv):=_GetProcedureAddress('glGetUniformiv');
  Pointer(glGetUniformLocation):=_GetProcedureAddress('glGetUniformLocation');
  Pointer(glGetVertexAttribfv):=_GetProcedureAddress('glGetVertexAttribfv');
  Pointer(glGetVertexAttribiv):=_GetProcedureAddress('glGetVertexAttribiv');
  Pointer(glGetVertexAttribPointerv):=_GetProcedureAddress('glGetVertexAttribPointerv');
  Pointer(glHint):=_GetProcedureAddress('glHint');
  Pointer(glIsBuffer):=_GetProcedureAddress('glIsBuffer');
  Pointer(glIsEnabled):=_GetProcedureAddress('glIsEnabled');
  Pointer(glIsFramebuffer):=_GetProcedureAddress('glIsFramebuffer');
  Pointer(glIsProgram):=_GetProcedureAddress('glIsProgram');
  Pointer(glIsRenderbuffer):=_GetProcedureAddress('glIsRenderbuffer');
  Pointer(glIsShader):=_GetProcedureAddress('glIsShader');
  Pointer(glIsTexture):=_GetProcedureAddress('glIsTexture');
  Pointer(glLineWidth):=_GetProcedureAddress('glLineWidth');
  Pointer(glLinkProgram):=_GetProcedureAddress('glLinkProgram');
  Pointer(glPixelStorei):=_GetProcedureAddress('glPixelStorei');
  Pointer(glPolygonOffset):=_GetProcedureAddress('glPolygonOffset');
  Pointer(glReadPixels):=_GetProcedureAddress('glReadPixels');
  Pointer(glReleaseShaderCompiler):=_GetProcedureAddress('glReleaseShaderCompiler');
  Pointer(glRenderbufferStorage):=_GetProcedureAddress('glRenderbufferStorage');
  Pointer(glSampleCoverage):=_GetProcedureAddress('glSampleCoverage');
  Pointer(glScissor):=_GetProcedureAddress('glScissor');
  Pointer(glShaderBinary):=_GetProcedureAddress('glShaderBinary');
  Pointer(glShaderSource):=_GetProcedureAddress('glShaderSource');
  Pointer(glStencilFunc):=_GetProcedureAddress('glStencilFunc');
  Pointer(glStencilFuncSeparate):=_GetProcedureAddress('glStencilFuncSeparate');
  Pointer(glStencilMask):=_GetProcedureAddress('glStencilMask');
  Pointer(glStencilMaskSeparate):=_GetProcedureAddress('glStencilMaskSeparate');
  Pointer(glStencilOp):=_GetProcedureAddress('glStencilOp');
  Pointer(glStencilOpSeparate):=_GetProcedureAddress('glStencilOpSeparate');
  Pointer(glTexImage2D):=_GetProcedureAddress('glTexImage2D');
  Pointer(glTexParameterf):=_GetProcedureAddress('glTexParameterf');
  Pointer(glTexParameterfv):=_GetProcedureAddress('glTexParameterfv');
  Pointer(glTexParameteri):=_GetProcedureAddress('glTexParameteri');
  Pointer(glTexParameteriv):=_GetProcedureAddress('glTexParameteriv');
  Pointer(glTexSubImage2D):=_GetProcedureAddress('glTexSubImage2D');
  Pointer(glUniform1f):=_GetProcedureAddress('glUniform1f');
  Pointer(glUniform1fv):=_GetProcedureAddress('glUniform1fv');
  Pointer(glUniform1i):=_GetProcedureAddress('glUniform1i');
  Pointer(glUniform1iv):=_GetProcedureAddress('glUniform1iv');
  Pointer(glUniform2f):=_GetProcedureAddress('glUniform2f');
  Pointer(glUniform2fv):=_GetProcedureAddress('glUniform2fv');
  Pointer(glUniform2i):=_GetProcedureAddress('glUniform2i');
  Pointer(glUniform2iv):=_GetProcedureAddress('glUniform2iv');
  Pointer(glUniform3f):=_GetProcedureAddress('glUniform3f');
  Pointer(glUniform3fv):=_GetProcedureAddress('glUniform3fv');
  Pointer(glUniform3i):=_GetProcedureAddress('glUniform3i');
  Pointer(glUniform3iv):=_GetProcedureAddress('glUniform3iv');
  Pointer(glUniform4f):=_GetProcedureAddress('glUniform4f');
  Pointer(glUniform4fv):=_GetProcedureAddress('glUniform4fv');
  Pointer(glUniform4i):=_GetProcedureAddress('glUniform4i');
  Pointer(glUniform4iv):=_GetProcedureAddress('glUniform4iv');
  Pointer(glUniformMatrix2fv):=_GetProcedureAddress('glUniformMatrix2fv');
  Pointer(glUniformMatrix3fv):=_GetProcedureAddress('glUniformMatrix3fv');
  Pointer(glUniformMatrix4fv):=_GetProcedureAddress('glUniformMatrix4fv');
  Pointer(glUseProgram):=_GetProcedureAddress('glUseProgram');
  Pointer(glValidateProgram):=_GetProcedureAddress('glValidateProgram');
  Pointer(glVertexAttrib1f):=_GetProcedureAddress('glVertexAttrib1f');
  Pointer(glVertexAttrib1fv):=_GetProcedureAddress('glVertexAttrib1fv');
  Pointer(glVertexAttrib2f):=_GetProcedureAddress('glVertexAttrib2f');
  Pointer(glVertexAttrib2fv):=_GetProcedureAddress('glVertexAttrib2fv');
  Pointer(glVertexAttrib3f):=_GetProcedureAddress('glVertexAttrib3f');
  Pointer(glVertexAttrib3fv):=_GetProcedureAddress('glVertexAttrib3fv');
  Pointer(glVertexAttrib4f):=_GetProcedureAddress('glVertexAttrib4f');
  Pointer(glVertexAttrib4fv):=_GetProcedureAddress('glVertexAttrib4fv');
  Pointer(glVertexAttribPointer):=_GetProcedureAddress('glVertexAttribPointer');
  Pointer(glViewport):=_GetProcedureAddress('glViewport');
  Pointer(glReadBuffer):=_GetProcedureAddress('glReadBuffer');
  Pointer(glDrawRangeElements):=_GetProcedureAddress('glDrawRangeElements');
  Pointer(glTexImage3D):=_GetProcedureAddress('glTexImage3D');
  Pointer(glTexSubImage3D):=_GetProcedureAddress('glTexSubImage3D');
  Pointer(glCopyTexSubImage3D):=_GetProcedureAddress('glCopyTexSubImage3D');
  Pointer(glCompressedTexImage3D):=_GetProcedureAddress('glCompressedTexImage3D');
  Pointer(glCompressedTexSubImage3D):=_GetProcedureAddress('glCompressedTexSubImage3D');
  Pointer(glGenQueries):=_GetProcedureAddress('glGenQueries');
  Pointer(glDeleteQueries):=_GetProcedureAddress('glDeleteQueries');
  Pointer(glIsQuery):=_GetProcedureAddress('glIsQuery');
  Pointer(glBeginQuery):=_GetProcedureAddress('glBeginQuery');
  Pointer(glEndQuery):=_GetProcedureAddress('glEndQuery');
  Pointer(glGetQueryiv):=_GetProcedureAddress('glGetQueryiv');
  Pointer(glGetQueryObjectuiv):=_GetProcedureAddress('glGetQueryObjectuiv');
  Pointer(glUnmapBuffer):=_GetProcedureAddress('glUnmapBuffer');
  Pointer(glGetBufferPointerv):=_GetProcedureAddress('glGetBufferPointerv');
  Pointer(glDrawBuffers):=_GetProcedureAddress('glDrawBuffers');
  Pointer(glUniformMatrix2x3fv):=_GetProcedureAddress('glUniformMatrix2x3fv');
  Pointer(glUniformMatrix3x2fv):=_GetProcedureAddress('glUniformMatrix3x2fv');
  Pointer(glUniformMatrix2x4fv):=_GetProcedureAddress('glUniformMatrix2x4fv');
  Pointer(glUniformMatrix4x2fv):=_GetProcedureAddress('glUniformMatrix4x2fv');
  Pointer(glUniformMatrix3x4fv):=_GetProcedureAddress('glUniformMatrix3x4fv');
  Pointer(glUniformMatrix4x3fv):=_GetProcedureAddress('glUniformMatrix4x3fv');
  Pointer(glBlitFramebuffer):=_GetProcedureAddress('glBlitFramebuffer');
  Pointer(glRenderbufferStorageMultisample):=_GetProcedureAddress('glRenderbufferStorageMultisample');
  Pointer(glFramebufferTextureLayer):=_GetProcedureAddress('glFramebufferTextureLayer');
  Pointer(glMapBufferRange):=_GetProcedureAddress('glMapBufferRange');
  Pointer(glFlushMappedBufferRange):=_GetProcedureAddress('glFlushMappedBufferRange');
  Pointer(glBindVertexArray):=_GetProcedureAddress('glBindVertexArray');
  Pointer(glDeleteVertexArrays):=_GetProcedureAddress('glDeleteVertexArrays');
  Pointer(glGenVertexArrays):=_GetProcedureAddress('glGenVertexArrays');
  Pointer(glIsVertexArray):=_GetProcedureAddress('glIsVertexArray');
  Pointer(glGetIntegeri_v):=_GetProcedureAddress('glGetIntegeri_v');
  Pointer(glBeginTransformFeedback):=_GetProcedureAddress('glBeginTransformFeedback');
  Pointer(glEndTransformFeedback):=_GetProcedureAddress('glEndTransformFeedback');
  Pointer(glBindBufferRange):=_GetProcedureAddress('glBindBufferRange');
  Pointer(glBindBufferBase):=_GetProcedureAddress('glBindBufferBase');
  Pointer(glTransformFeedbackVaryings):=_GetProcedureAddress('glTransformFeedbackVaryings');
  Pointer(glGetTransformFeedbackVarying):=_GetProcedureAddress('glGetTransformFeedbackVarying');
  Pointer(glVertexAttribIPointer):=_GetProcedureAddress('glVertexAttribIPointer');
  Pointer(glGetVertexAttribIiv):=_GetProcedureAddress('glGetVertexAttribIiv');
  Pointer(glGetVertexAttribIuiv):=_GetProcedureAddress('glGetVertexAttribIuiv');
  Pointer(glVertexAttribI4i):=_GetProcedureAddress('glVertexAttribI4i');
  Pointer(glVertexAttribI4ui):=_GetProcedureAddress('glVertexAttribI4ui');
  Pointer(glVertexAttribI4iv):=_GetProcedureAddress('glVertexAttribI4iv');
  Pointer(glVertexAttribI4uiv):=_GetProcedureAddress('glVertexAttribI4uiv');
  Pointer(glGetUniformuiv):=_GetProcedureAddress('glGetUniformuiv');
  Pointer(glGetFragDataLocation):=_GetProcedureAddress('glGetFragDataLocation');
  Pointer(glUniform1ui):=_GetProcedureAddress('glUniform1ui');
  Pointer(glUniform2ui):=_GetProcedureAddress('glUniform2ui');
  Pointer(glUniform3ui):=_GetProcedureAddress('glUniform3ui');
  Pointer(glUniform4ui):=_GetProcedureAddress('glUniform4ui');
  Pointer(glUniform1uiv):=_GetProcedureAddress('glUniform1uiv');
  Pointer(glUniform2uiv):=_GetProcedureAddress('glUniform2uiv');
  Pointer(glUniform3uiv):=_GetProcedureAddress('glUniform3uiv');
  Pointer(glUniform4uiv):=_GetProcedureAddress('glUniform4uiv');
  Pointer(glClearBufferiv):=_GetProcedureAddress('glClearBufferiv');
  Pointer(glClearBufferuiv):=_GetProcedureAddress('glClearBufferuiv');
  Pointer(glClearBufferfv):=_GetProcedureAddress('glClearBufferfv');
  Pointer(glClearBufferfi):=_GetProcedureAddress('glClearBufferfi');
  Pointer(glGetStringi):=_GetProcedureAddress('glGetStringi');
  Pointer(glCopyBufferSubData):=_GetProcedureAddress('glCopyBufferSubData');
  Pointer(glGetUniformIndices):=_GetProcedureAddress('glGetUniformIndices');
  Pointer(glGetActiveUniformsiv):=_GetProcedureAddress('glGetActiveUniformsiv');
  Pointer(glGetUniformBlockIndex):=_GetProcedureAddress('glGetUniformBlockIndex');
  Pointer(glGetActiveUniformBlockiv):=_GetProcedureAddress('glGetActiveUniformBlockiv');
  Pointer(glGetActiveUniformBlockName):=_GetProcedureAddress('glGetActiveUniformBlockName');
  Pointer(glUniformBlockBinding):=_GetProcedureAddress('glUniformBlockBinding');
  Pointer(glDrawArraysInstanced):=_GetProcedureAddress('glDrawArraysInstanced');
  Pointer(glDrawElementsInstanced):=_GetProcedureAddress('glDrawElementsInstanced');
  Pointer(glFenceSync):=_GetProcedureAddress('glFenceSync');
  Pointer(glIsSync):=_GetProcedureAddress('glIsSync');
  Pointer(glDeleteSync):=_GetProcedureAddress('glDeleteSync');
  Pointer(glClientWaitSync):=_GetProcedureAddress('glClientWaitSync');
  Pointer(glWaitSync):=_GetProcedureAddress('glWaitSync');
  Pointer(glGetInteger64v):=_GetProcedureAddress('glGetInteger64v');
  Pointer(glGetSynciv):=_GetProcedureAddress('glGetSynciv');
  Pointer(glGetInteger64i_v):=_GetProcedureAddress('glGetInteger64i_v');
  Pointer(glGetBufferParameteri64v):=_GetProcedureAddress('glGetBufferParameteri64v');
  Pointer(glGenSamplers):=_GetProcedureAddress('glGenSamplers');
  Pointer(glDeleteSamplers):=_GetProcedureAddress('glDeleteSamplers');
  Pointer(glIsSampler):=_GetProcedureAddress('glIsSampler');
  Pointer(glBindSampler):=_GetProcedureAddress('glBindSampler');
  Pointer(glSamplerParameteri):=_GetProcedureAddress('glSamplerParameteri');
  Pointer(glSamplerParameteriv):=_GetProcedureAddress('glSamplerParameteriv');
  Pointer(glSamplerParameterf):=_GetProcedureAddress('glSamplerParameterf');
  Pointer(glSamplerParameterfv):=_GetProcedureAddress('glSamplerParameterfv');
  Pointer(glGetSamplerParameteriv):=_GetProcedureAddress('glGetSamplerParameteriv');
  Pointer(glGetSamplerParameterfv):=_GetProcedureAddress('glGetSamplerParameterfv');
  Pointer(glVertexAttribDivisor):=_GetProcedureAddress('glVertexAttribDivisor');
  Pointer(glBindTransformFeedback):=_GetProcedureAddress('glBindTransformFeedback');
  Pointer(glDeleteTransformFeedbacks):=_GetProcedureAddress('glDeleteTransformFeedbacks');
  Pointer(glGenTransformFeedbacks):=_GetProcedureAddress('glGenTransformFeedbacks');
  Pointer(glIsTransformFeedback):=_GetProcedureAddress('glIsTransformFeedback');
  Pointer(glPauseTransformFeedback):=_GetProcedureAddress('glPauseTransformFeedback');
  Pointer(glResumeTransformFeedback):=_GetProcedureAddress('glResumeTransformFeedback');
  Pointer(glGetProgramBinary):=_GetProcedureAddress('glGetProgramBinary');
  Pointer(glProgramBinary):=_GetProcedureAddress('glProgramBinary');
  Pointer(glProgramParameteri):=_GetProcedureAddress('glProgramParameteri');
  Pointer(glInvalidateFramebuffer):=_GetProcedureAddress('glInvalidateFramebuffer');
  Pointer(glInvalidateSubFramebuffer):=_GetProcedureAddress('glInvalidateSubFramebuffer');
  Pointer(glTexStorage2D):=_GetProcedureAddress('glTexStorage2D');
  Pointer(glTexStorage3D):=_GetProcedureAddress('glTexStorage3D');
  Pointer(glGetInternalformativ):=_GetProcedureAddress('glGetInternalformativ');
  Pointer(glDispatchCompute):=_GetProcedureAddress('glDispatchCompute');
  Pointer(glDispatchComputeIndirect):=_GetProcedureAddress('glDispatchComputeIndirect');
  Pointer(glDrawArraysIndirect):=_GetProcedureAddress('glDrawArraysIndirect');
  Pointer(glDrawElementsIndirect):=_GetProcedureAddress('glDrawElementsIndirect');
  Pointer(glFramebufferParameteri):=_GetProcedureAddress('glFramebufferParameteri');
  Pointer(glGetFramebufferParameteriv):=_GetProcedureAddress('glGetFramebufferParameteriv');
  Pointer(glGetProgramInterfaceiv):=_GetProcedureAddress('glGetProgramInterfaceiv');
  Pointer(glGetProgramResourceIndex):=_GetProcedureAddress('glGetProgramResourceIndex');
  Pointer(glGetProgramResourceName):=_GetProcedureAddress('glGetProgramResourceName');
  Pointer(glGetProgramResourceiv):=_GetProcedureAddress('glGetProgramResourceiv');
  Pointer(glGetProgramResourceLocation):=_GetProcedureAddress('glGetProgramResourceLocation');
  Pointer(glUseProgramStages):=_GetProcedureAddress('glUseProgramStages');
  Pointer(glActiveShaderProgram):=_GetProcedureAddress('glActiveShaderProgram');
  Pointer(glCreateShaderProgramv):=_GetProcedureAddress('glCreateShaderProgramv');
  Pointer(glBindProgramPipeline):=_GetProcedureAddress('glBindProgramPipeline');
  Pointer(glDeleteProgramPipelines):=_GetProcedureAddress('glDeleteProgramPipelines');
  Pointer(glGenProgramPipelines):=_GetProcedureAddress('glGenProgramPipelines');
  Pointer(glIsProgramPipeline):=_GetProcedureAddress('glIsProgramPipeline');
  Pointer(glGetProgramPipelineiv):=_GetProcedureAddress('glGetProgramPipelineiv');
  Pointer(glProgramUniform1i):=_GetProcedureAddress('glProgramUniform1i');
  Pointer(glProgramUniform2i):=_GetProcedureAddress('glProgramUniform2i');
  Pointer(glProgramUniform3i):=_GetProcedureAddress('glProgramUniform3i');
  Pointer(glProgramUniform4i):=_GetProcedureAddress('glProgramUniform4i');
  Pointer(glProgramUniform1ui):=_GetProcedureAddress('glProgramUniform1ui');
  Pointer(glProgramUniform2ui):=_GetProcedureAddress('glProgramUniform2ui');
  Pointer(glProgramUniform3ui):=_GetProcedureAddress('glProgramUniform3ui');
  Pointer(glProgramUniform4ui):=_GetProcedureAddress('glProgramUniform4ui');
  Pointer(glProgramUniform1f):=_GetProcedureAddress('glProgramUniform1f');
  Pointer(glProgramUniform2f):=_GetProcedureAddress('glProgramUniform2f');
  Pointer(glProgramUniform3f):=_GetProcedureAddress('glProgramUniform3f');
  Pointer(glProgramUniform4f):=_GetProcedureAddress('glProgramUniform4f');
  Pointer(glProgramUniform1iv):=_GetProcedureAddress('glProgramUniform1iv');
  Pointer(glProgramUniform2iv):=_GetProcedureAddress('glProgramUniform2iv');
  Pointer(glProgramUniform3iv):=_GetProcedureAddress('glProgramUniform3iv');
  Pointer(glProgramUniform4iv):=_GetProcedureAddress('glProgramUniform4iv');
  Pointer(glProgramUniform1uiv):=_GetProcedureAddress('glProgramUniform1uiv');
  Pointer(glProgramUniform2uiv):=_GetProcedureAddress('glProgramUniform2uiv');
  Pointer(glProgramUniform3uiv):=_GetProcedureAddress('glProgramUniform3uiv');
  Pointer(glProgramUniform4uiv):=_GetProcedureAddress('glProgramUniform4uiv');
  Pointer(glProgramUniform1fv):=_GetProcedureAddress('glProgramUniform1fv');
  Pointer(glProgramUniform2fv):=_GetProcedureAddress('glProgramUniform2fv');
  Pointer(glProgramUniform3fv):=_GetProcedureAddress('glProgramUniform3fv');
  Pointer(glProgramUniform4fv):=_GetProcedureAddress('glProgramUniform4fv');
  Pointer(glProgramUniformMatrix2fv):=_GetProcedureAddress('glProgramUniformMatrix2fv');
  Pointer(glProgramUniformMatrix3fv):=_GetProcedureAddress('glProgramUniformMatrix3fv');
  Pointer(glProgramUniformMatrix4fv):=_GetProcedureAddress('glProgramUniformMatrix4fv');
  Pointer(glProgramUniformMatrix2x3fv):=_GetProcedureAddress('glProgramUniformMatrix2x3fv');
  Pointer(glProgramUniformMatrix3x2fv):=_GetProcedureAddress('glProgramUniformMatrix3x2fv');
  Pointer(glProgramUniformMatrix2x4fv):=_GetProcedureAddress('glProgramUniformMatrix2x4fv');
  Pointer(glProgramUniformMatrix4x2fv):=_GetProcedureAddress('glProgramUniformMatrix4x2fv');
  Pointer(glProgramUniformMatrix3x4fv):=_GetProcedureAddress('glProgramUniformMatrix3x4fv');
  Pointer(glProgramUniformMatrix4x3fv):=_GetProcedureAddress('glProgramUniformMatrix4x3fv');
  Pointer(glValidateProgramPipeline):=_GetProcedureAddress('glValidateProgramPipeline');
  Pointer(glGetProgramPipelineInfoLog):=_GetProcedureAddress('glGetProgramPipelineInfoLog');
  Pointer(glBindImageTexture):=_GetProcedureAddress('glBindImageTexture');
  Pointer(glGetBooleani_v):=_GetProcedureAddress('glGetBooleani_v');
  Pointer(glMemoryBarrier):=_GetProcedureAddress('glMemoryBarrier');
  Pointer(glMemoryBarrierByRegion):=_GetProcedureAddress('glMemoryBarrierByRegion');
  Pointer(glTexStorage2DMultisample):=_GetProcedureAddress('glTexStorage2DMultisample');
  Pointer(glGetMultisamplefv):=_GetProcedureAddress('glGetMultisamplefv');
  Pointer(glSampleMaski):=_GetProcedureAddress('glSampleMaski');
  Pointer(glGetTexLevelParameteriv):=_GetProcedureAddress('glGetTexLevelParameteriv');
  Pointer(glGetTexLevelParameterfv):=_GetProcedureAddress('glGetTexLevelParameterfv');
  Pointer(glBindVertexBuffer):=_GetProcedureAddress('glBindVertexBuffer');
  Pointer(glVertexAttribFormat):=_GetProcedureAddress('glVertexAttribFormat');
  Pointer(glVertexAttribIFormat):=_GetProcedureAddress('glVertexAttribIFormat');
  Pointer(glVertexAttribBinding):=_GetProcedureAddress('glVertexAttribBinding');
  Pointer(glVertexBindingDivisor):=_GetProcedureAddress('glVertexBindingDivisor');
  Pointer(glBlendBarrier):=_GetProcedureAddress('glBlendBarrier');
  Pointer(glCopyImageSubData):=_GetProcedureAddress('glCopyImageSubData');
  Pointer(glDebugMessageControl):=_GetProcedureAddress('glDebugMessageControl');
  Pointer(glDebugMessageInsert):=_GetProcedureAddress('glDebugMessageInsert');
  Pointer(glDebugMessageCallback):=_GetProcedureAddress('glDebugMessageCallback');
  Pointer(glGetDebugMessageLog):=_GetProcedureAddress('glGetDebugMessageLog');
  Pointer(glPushDebugGroup):=_GetProcedureAddress('glPushDebugGroup');
  Pointer(glPopDebugGroup):=_GetProcedureAddress('glPopDebugGroup');
  Pointer(glObjectLabel):=_GetProcedureAddress('glObjectLabel');
  Pointer(glGetObjectLabel):=_GetProcedureAddress('glGetObjectLabel');
  Pointer(glObjectPtrLabel):=_GetProcedureAddress('glObjectPtrLabel');
  Pointer(glGetObjectPtrLabel):=_GetProcedureAddress('glGetObjectPtrLabel');
  Pointer(glGetPointerv):=_GetProcedureAddress('glGetPointerv');
  Pointer(glEnablei):=_GetProcedureAddress('glEnablei');
  Pointer(glDisablei):=_GetProcedureAddress('glDisablei');
  Pointer(glBlendEquationi):=_GetProcedureAddress('glBlendEquationi');
  Pointer(glBlendEquationSeparatei):=_GetProcedureAddress('glBlendEquationSeparatei');
  Pointer(glBlendFunci):=_GetProcedureAddress('glBlendFunci');
  Pointer(glBlendFuncSeparatei):=_GetProcedureAddress('glBlendFuncSeparatei');
  Pointer(glColorMaski):=_GetProcedureAddress('glColorMaski');
  Pointer(glIsEnabledi):=_GetProcedureAddress('glIsEnabledi');
  Pointer(glDrawElementsBaseVertex):=_GetProcedureAddress('glDrawElementsBaseVertex');
  Pointer(glDrawRangeElementsBaseVertex):=_GetProcedureAddress('glDrawRangeElementsBaseVertex');
  Pointer(glDrawElementsInstancedBaseVertex):=_GetProcedureAddress('glDrawElementsInstancedBaseVertex');
  Pointer(glFramebufferTexture):=_GetProcedureAddress('glFramebufferTexture');
  Pointer(glPrimitiveBoundingBox):=_GetProcedureAddress('glPrimitiveBoundingBox');
  Pointer(glGetGraphicsResetStatus):=_GetProcedureAddress('glGetGraphicsResetStatus');
  Pointer(glReadnPixels):=_GetProcedureAddress('glReadnPixels');
  Pointer(glGetnUniformfv):=_GetProcedureAddress('glGetnUniformfv');
  Pointer(glGetnUniformiv):=_GetProcedureAddress('glGetnUniformiv');
  Pointer(glGetnUniformuiv):=_GetProcedureAddress('glGetnUniformuiv');
  Pointer(glMinSampleShading):=_GetProcedureAddress('glMinSampleShading');
  Pointer(glPatchParameteri):=_GetProcedureAddress('glPatchParameteri');
  Pointer(glTexParameterIiv):=_GetProcedureAddress('glTexParameterIiv');
  Pointer(glTexParameterIuiv):=_GetProcedureAddress('glTexParameterIuiv');
  Pointer(glGetTexParameterIiv):=_GetProcedureAddress('glGetTexParameterIiv');
  Pointer(glGetTexParameterIuiv):=_GetProcedureAddress('glGetTexParameterIuiv');
  Pointer(glSamplerParameterIiv):=_GetProcedureAddress('glSamplerParameterIiv');
  Pointer(glSamplerParameterIuiv):=_GetProcedureAddress('glSamplerParameterIuiv');
  Pointer(glGetSamplerParameterIiv):=_GetProcedureAddress('glGetSamplerParameterIiv');
  Pointer(glGetSamplerParameterIuiv):=_GetProcedureAddress('glGetSamplerParameterIuiv');
  Pointer(glTexBuffer):=_GetProcedureAddress('glTexBuffer');
  Pointer(glTexBufferRange):=_GetProcedureAddress('glTexBufferRange');
  Pointer(glTexStorage3DMultisample):=_GetProcedureAddress('glTexStorage3DMultisample');
  Pointer(glActiveProgramEXT):=_GetProcedureAddress('glActiveProgramEXT');
  Pointer(glActiveShaderProgramEXT):=_GetProcedureAddress('glActiveShaderProgramEXT');
  Pointer(glAlphaFuncQCOM):=_GetProcedureAddress('glAlphaFuncQCOM');
  Pointer(glApplyFramebufferAttachmentCMAAINTEL):=_GetProcedureAddress('glApplyFramebufferAttachmentCMAAINTEL');
  Pointer(glAcquireKeyedMutexWin32EXT):=_GetProcedureAddress('glAcquireKeyedMutexWin32EXT');
  Pointer(glArrayElement):=_GetProcedureAddress('glArrayElement');
  Pointer(glBeginConditionalRender):=_GetProcedureAddress('glBeginConditionalRender');
  Pointer(glBeginConditionalRenderNV):=_GetProcedureAddress('glBeginConditionalRenderNV');
  Pointer(glBeginPerfMonitorAMD):=_GetProcedureAddress('glBeginPerfMonitorAMD');
  Pointer(glBeginPerfQueryINTEL):=_GetProcedureAddress('glBeginPerfQueryINTEL');
  Pointer(glBeginQueryEXT):=_GetProcedureAddress('glBeginQueryEXT');
  Pointer(glBindBufferOffsetEXT):=_GetProcedureAddress('glBindBufferOffsetEXT');
  Pointer(glBindFragDataLocation):=_GetProcedureAddress('glBindFragDataLocation');
  Pointer(glBindFragDataLocationEXT):=_GetProcedureAddress('glBindFragDataLocationEXT');
  Pointer(glBindFragDataLocationIndexed):=_GetProcedureAddress('glBindFragDataLocationIndexed');
  Pointer(glBindFragDataLocationIndexedEXT):=_GetProcedureAddress('glBindFragDataLocationIndexedEXT');
  Pointer(glBindProgramARB):=_GetProcedureAddress('glBindProgramARB');
  Pointer(glBindProgramPipelineEXT):=_GetProcedureAddress('glBindProgramPipelineEXT');
  Pointer(glBindShadingRateImageNV):=_GetProcedureAddress('glBindShadingRateImageNV');
  Pointer(glBindVertexArrayOES):=_GetProcedureAddress('glBindVertexArrayOES');
  Pointer(glBlendBarrierKHR):=_GetProcedureAddress('glBlendBarrierKHR');
  Pointer(glBlendBarrierNV):=_GetProcedureAddress('glBlendBarrierNV');
  Pointer(glBlendEquationEXT):=_GetProcedureAddress('glBlendEquationEXT');
  Pointer(glBlendEquationSeparateiEXT):=_GetProcedureAddress('glBlendEquationSeparateiEXT');
  Pointer(glBlendEquationSeparateiOES):=_GetProcedureAddress('glBlendEquationSeparateiOES');
  Pointer(glBlendEquationiEXT):=_GetProcedureAddress('glBlendEquationiEXT');
  Pointer(glBlendEquationiOES):=_GetProcedureAddress('glBlendEquationiOES');
  Pointer(glBlendFuncSeparateiEXT):=_GetProcedureAddress('glBlendFuncSeparateiEXT');
  Pointer(glBlendFuncSeparateiOES):=_GetProcedureAddress('glBlendFuncSeparateiOES');
  Pointer(glBlendFunciEXT):=_GetProcedureAddress('glBlendFunciEXT');
  Pointer(glBlendFunciOES):=_GetProcedureAddress('glBlendFunciOES');
  Pointer(glBlendParameteriNV):=_GetProcedureAddress('glBlendParameteriNV');
  Pointer(glBlitFramebufferANGLE):=_GetProcedureAddress('glBlitFramebufferANGLE');
  Pointer(glBlitFramebufferNV):=_GetProcedureAddress('glBlitFramebufferNV');
  Pointer(glBufferAttachMemoryNV):=_GetProcedureAddress('glBufferAttachMemoryNV');
  Pointer(glBufferPageCommitmentMemNV):=_GetProcedureAddress('glBufferPageCommitmentMemNV');
  Pointer(glBufferStorage):=_GetProcedureAddress('glBufferStorage');
  Pointer(glBufferStorageEXT):=_GetProcedureAddress('glBufferStorageEXT');
  Pointer(glBufferStorageExternalEXT):=_GetProcedureAddress('glBufferStorageExternalEXT');
  Pointer(glBufferStorageMemEXT):=_GetProcedureAddress('glBufferStorageMemEXT');
  Pointer(glClampColor):=_GetProcedureAddress('glClampColor');
  Pointer(glClearPixelLocalStorageuiEXT):=_GetProcedureAddress('glClearPixelLocalStorageuiEXT');
  Pointer(glClearTexImage):=_GetProcedureAddress('glClearTexImage');
  Pointer(glClearTexImageEXT):=_GetProcedureAddress('glClearTexImageEXT');
  Pointer(glClearTexSubImage):=_GetProcedureAddress('glClearTexSubImage');
  Pointer(glClearTexSubImageEXT):=_GetProcedureAddress('glClearTexSubImageEXT');
  Pointer(glClientActiveTexture):=_GetProcedureAddress('glClientActiveTexture');
  Pointer(glClientWaitSyncAPPLE):=_GetProcedureAddress('glClientWaitSyncAPPLE');
  Pointer(glClipControl):=_GetProcedureAddress('glClipControl');
  Pointer(glClipControlEXT):=_GetProcedureAddress('glClipControlEXT');
  Pointer(glColorMaskiEXT):=_GetProcedureAddress('glColorMaskiEXT');
  Pointer(glColorMaskiOES):=_GetProcedureAddress('glColorMaskiOES');
  Pointer(glColorSubTable):=_GetProcedureAddress('glColorSubTable');
  Pointer(glColorTable):=_GetProcedureAddress('glColorTable');
  Pointer(glColorTableParameterfv):=_GetProcedureAddress('glColorTableParameterfv');
  Pointer(glColorTableParameteriv):=_GetProcedureAddress('glColorTableParameteriv');
  Pointer(glCompressedTexImage1D):=_GetProcedureAddress('glCompressedTexImage1D');
  Pointer(glCompressedTexImage3DOES):=_GetProcedureAddress('glCompressedTexImage3DOES');
  Pointer(glCompressedTexSubImage1D):=_GetProcedureAddress('glCompressedTexSubImage1D');
  Pointer(glCompressedTexSubImage3DOES):=_GetProcedureAddress('glCompressedTexSubImage3DOES');
  Pointer(glConservativeRasterParameteriNV):=_GetProcedureAddress('glConservativeRasterParameteriNV');
  Pointer(glConvolutionFilter1D):=_GetProcedureAddress('glConvolutionFilter1D');
  Pointer(glConvolutionFilter2D):=_GetProcedureAddress('glConvolutionFilter2D');
  Pointer(glConvolutionParameterf):=_GetProcedureAddress('glConvolutionParameterf');
  Pointer(glConvolutionParameterfv):=_GetProcedureAddress('glConvolutionParameterfv');
  Pointer(glConvolutionParameteri):=_GetProcedureAddress('glConvolutionParameteri');
  Pointer(glConvolutionParameteriv):=_GetProcedureAddress('glConvolutionParameteriv');
  Pointer(glCopyBufferSubDataNV):=_GetProcedureAddress('glCopyBufferSubDataNV');
  Pointer(glCopyColorSubTable):=_GetProcedureAddress('glCopyColorSubTable');
  Pointer(glCopyColorTable):=_GetProcedureAddress('glCopyColorTable');
  Pointer(glCopyConvolutionFilter1D):=_GetProcedureAddress('glCopyConvolutionFilter1D');
  Pointer(glCopyConvolutionFilter2D):=_GetProcedureAddress('glCopyConvolutionFilter2D');
  Pointer(glCopyImageSubDataEXT):=_GetProcedureAddress('glCopyImageSubDataEXT');
  Pointer(glCopyImageSubDataOES):=_GetProcedureAddress('glCopyImageSubDataOES');
  Pointer(glCopyPathNV):=_GetProcedureAddress('glCopyPathNV');
  Pointer(glCopyTexImage1D):=_GetProcedureAddress('glCopyTexImage1D');
  Pointer(glCopyTexSubImage1D):=_GetProcedureAddress('glCopyTexSubImage1D');
  Pointer(glCopyTexSubImage3DOES):=_GetProcedureAddress('glCopyTexSubImage3DOES');
  Pointer(glCopyTextureLevelsAPPLE):=_GetProcedureAddress('glCopyTextureLevelsAPPLE');
  Pointer(glCoverFillPathInstancedNV):=_GetProcedureAddress('glCoverFillPathInstancedNV');
  Pointer(glCoverFillPathNV):=_GetProcedureAddress('glCoverFillPathNV');
  Pointer(glCoverStrokePathInstancedNV):=_GetProcedureAddress('glCoverStrokePathInstancedNV');
  Pointer(glCoverStrokePathNV):=_GetProcedureAddress('glCoverStrokePathNV');
  Pointer(glCoverageMaskNV):=_GetProcedureAddress('glCoverageMaskNV');
  Pointer(glCoverageModulationNV):=_GetProcedureAddress('glCoverageModulationNV');
  Pointer(glCoverageModulationTableNV):=_GetProcedureAddress('glCoverageModulationTableNV');
  Pointer(glCoverageOperationNV):=_GetProcedureAddress('glCoverageOperationNV');
  Pointer(glCreateMemoryObjectsEXT):=_GetProcedureAddress('glCreateMemoryObjectsEXT');
  Pointer(glCreatePerfQueryINTEL):=_GetProcedureAddress('glCreatePerfQueryINTEL');
  Pointer(glCreateSemaphoresNV):=_GetProcedureAddress('glCreateSemaphoresNV');
  Pointer(glCreateShaderProgramEXT):=_GetProcedureAddress('glCreateShaderProgramEXT');
  Pointer(glCreateShaderProgramvEXT):=_GetProcedureAddress('glCreateShaderProgramvEXT');
  Pointer(glDebugMessageCallbackKHR):=_GetProcedureAddress('glDebugMessageCallbackKHR');
  Pointer(glDebugMessageControlKHR):=_GetProcedureAddress('glDebugMessageControlKHR');
  Pointer(glDebugMessageInsertKHR):=_GetProcedureAddress('glDebugMessageInsertKHR');
  Pointer(glDeleteFencesNV):=_GetProcedureAddress('glDeleteFencesNV');
  Pointer(glDeleteMemoryObjectsEXT):=_GetProcedureAddress('glDeleteMemoryObjectsEXT');
  Pointer(glDeletePathsNV):=_GetProcedureAddress('glDeletePathsNV');
  Pointer(glDeletePerfMonitorsAMD):=_GetProcedureAddress('glDeletePerfMonitorsAMD');
  Pointer(glDeletePerfQueryINTEL):=_GetProcedureAddress('glDeletePerfQueryINTEL');
  Pointer(glDeleteProgramPipelinesEXT):=_GetProcedureAddress('glDeleteProgramPipelinesEXT');
  Pointer(glDeleteProgramsARB):=_GetProcedureAddress('glDeleteProgramsARB');
  Pointer(glDeleteQueriesEXT):=_GetProcedureAddress('glDeleteQueriesEXT');
  Pointer(glDeleteSemaphoresEXT):=_GetProcedureAddress('glDeleteSemaphoresEXT');
  Pointer(glDeleteSyncAPPLE):=_GetProcedureAddress('glDeleteSyncAPPLE');
  Pointer(glDeleteVertexArraysOES):=_GetProcedureAddress('glDeleteVertexArraysOES');
  Pointer(glDepthRangeArrayfvNV):=_GetProcedureAddress('glDepthRangeArrayfvNV');
  Pointer(glDepthRangeArrayfvOES):=_GetProcedureAddress('glDepthRangeArrayfvOES');
  Pointer(glDepthRangeIndexedfNV):=_GetProcedureAddress('glDepthRangeIndexedfNV');
  Pointer(glDepthRangeIndexedfOES):=_GetProcedureAddress('glDepthRangeIndexedfOES');
  Pointer(glDisableDriverControlQCOM):=_GetProcedureAddress('glDisableDriverControlQCOM');
  Pointer(glDisableiEXT):=_GetProcedureAddress('glDisableiEXT');
  Pointer(glDisableiNV):=_GetProcedureAddress('glDisableiNV');
  Pointer(glDisableiOES):=_GetProcedureAddress('glDisableiOES');
  Pointer(glDiscardFramebufferEXT):=_GetProcedureAddress('glDiscardFramebufferEXT');
  Pointer(glDrawArraysInstancedANGLE):=_GetProcedureAddress('glDrawArraysInstancedANGLE');
  Pointer(glDrawArraysInstancedBaseInstance):=_GetProcedureAddress('glDrawArraysInstancedBaseInstance');
  Pointer(glDrawArraysInstancedBaseInstanceEXT):=_GetProcedureAddress('glDrawArraysInstancedBaseInstanceEXT');
  Pointer(glDrawArraysInstancedEXT):=_GetProcedureAddress('glDrawArraysInstancedEXT');
  Pointer(glDrawArraysInstancedNV):=_GetProcedureAddress('glDrawArraysInstancedNV');
  Pointer(glDrawBuffersEXT):=_GetProcedureAddress('glDrawBuffersEXT');
  Pointer(glDrawBuffersIndexedEXT):=_GetProcedureAddress('glDrawBuffersIndexedEXT');
  Pointer(glDrawBuffersNV):=_GetProcedureAddress('glDrawBuffersNV');
  Pointer(glDrawElementsBaseVertexEXT):=_GetProcedureAddress('glDrawElementsBaseVertexEXT');
  Pointer(glDrawElementsBaseVertexOES):=_GetProcedureAddress('glDrawElementsBaseVertexOES');
  Pointer(glDrawElementsInstancedANGLE):=_GetProcedureAddress('glDrawElementsInstancedANGLE');
  Pointer(glDrawElementsInstancedBaseInstance):=_GetProcedureAddress('glDrawElementsInstancedBaseInstance');
  Pointer(glDrawElementsInstancedBaseInstanceEXT):=_GetProcedureAddress('glDrawElementsInstancedBaseInstanceEXT');
  Pointer(glDrawElementsInstancedBaseVertexBaseInstance):=_GetProcedureAddress('glDrawElementsInstancedBaseVertexBaseInstance');
  Pointer(glDrawElementsInstancedBaseVertexBaseInstanceEXT):=_GetProcedureAddress('glDrawElementsInstancedBaseVertexBaseInstanceEXT');
  Pointer(glDrawElementsInstancedBaseVertexEXT):=_GetProcedureAddress('glDrawElementsInstancedBaseVertexEXT');
  Pointer(glDrawElementsInstancedBaseVertexOES):=_GetProcedureAddress('glDrawElementsInstancedBaseVertexOES');
  Pointer(glDrawElementsInstancedEXT):=_GetProcedureAddress('glDrawElementsInstancedEXT');
  Pointer(glDrawElementsInstancedNV):=_GetProcedureAddress('glDrawElementsInstancedNV');
  Pointer(glDrawMeshTasksNV):=_GetProcedureAddress('glDrawMeshTasksNV');
  Pointer(glDrawMeshTasksIndirectNV):=_GetProcedureAddress('glDrawMeshTasksIndirectNV');
  Pointer(glDrawRangeElementsBaseVertexEXT):=_GetProcedureAddress('glDrawRangeElementsBaseVertexEXT');
  Pointer(glDrawRangeElementsBaseVertexOES):=_GetProcedureAddress('glDrawRangeElementsBaseVertexOES');
  Pointer(glDrawTransformFeedback):=_GetProcedureAddress('glDrawTransformFeedback');
  Pointer(glDrawTransformFeedbackEXT):=_GetProcedureAddress('glDrawTransformFeedbackEXT');
  Pointer(glDrawTransformFeedbackInstanced):=_GetProcedureAddress('glDrawTransformFeedbackInstanced');
  Pointer(glDrawTransformFeedbackInstancedEXT):=_GetProcedureAddress('glDrawTransformFeedbackInstancedEXT');
  Pointer(glEGLImageTargetRenderbufferStorageOES):=_GetProcedureAddress('glEGLImageTargetRenderbufferStorageOES');
  Pointer(glEGLImageTargetTexStorageEXT):=_GetProcedureAddress('glEGLImageTargetTexStorageEXT');
  Pointer(glEGLImageTargetTexture2DOES):=_GetProcedureAddress('glEGLImageTargetTexture2DOES');
  Pointer(glEGLImageTargetTextureStorageEXT):=_GetProcedureAddress('glEGLImageTargetTextureStorageEXT');
  Pointer(glEnableDriverControlQCOM):=_GetProcedureAddress('glEnableDriverControlQCOM');
  Pointer(glEnableiEXT):=_GetProcedureAddress('glEnableiEXT');
  Pointer(glEnableiNV):=_GetProcedureAddress('glEnableiNV');
  Pointer(glEnableiOES):=_GetProcedureAddress('glEnableiOES');
  Pointer(glEndConditionalRender):=_GetProcedureAddress('glEndConditionalRender');
  Pointer(glEndConditionalRenderNV):=_GetProcedureAddress('glEndConditionalRenderNV');
  Pointer(glEndPerfMonitorAMD):=_GetProcedureAddress('glEndPerfMonitorAMD');
  Pointer(glEndPerfQueryINTEL):=_GetProcedureAddress('glEndPerfQueryINTEL');
  Pointer(glEndQueryEXT):=_GetProcedureAddress('glEndQueryEXT');
  Pointer(glEndTilingQCOM):=_GetProcedureAddress('glEndTilingQCOM');
  Pointer(glExtGetBufferPointervQCOM):=_GetProcedureAddress('glExtGetBufferPointervQCOM');
  Pointer(glExtGetBuffersQCOM):=_GetProcedureAddress('glExtGetBuffersQCOM');
  Pointer(glExtGetFramebuffersQCOM):=_GetProcedureAddress('glExtGetFramebuffersQCOM');
  Pointer(glExtGetProgramBinarySourceQCOM):=_GetProcedureAddress('glExtGetProgramBinarySourceQCOM');
  Pointer(glExtGetProgramsQCOM):=_GetProcedureAddress('glExtGetProgramsQCOM');
  Pointer(glExtGetRenderbuffersQCOM):=_GetProcedureAddress('glExtGetRenderbuffersQCOM');
  Pointer(glExtGetShadersQCOM):=_GetProcedureAddress('glExtGetShadersQCOM');
  Pointer(glExtGetTexLevelParameterivQCOM):=_GetProcedureAddress('glExtGetTexLevelParameterivQCOM');
  Pointer(glExtGetTexSubImageQCOM):=_GetProcedureAddress('glExtGetTexSubImageQCOM');
  Pointer(glExtGetTexturesQCOM):=_GetProcedureAddress('glExtGetTexturesQCOM');
  Pointer(glExtIsProgramBinaryQCOM):=_GetProcedureAddress('glExtIsProgramBinaryQCOM');
  Pointer(glExtTexObjectStateOverrideiQCOM):=_GetProcedureAddress('glExtTexObjectStateOverrideiQCOM');
  Pointer(glFenceSyncAPPLE):=_GetProcedureAddress('glFenceSyncAPPLE');
  Pointer(glFinishFenceNV):=_GetProcedureAddress('glFinishFenceNV');
  Pointer(glFlushMappedBufferRangeEXT):=_GetProcedureAddress('glFlushMappedBufferRangeEXT');
  Pointer(glFogCoordPointer):=_GetProcedureAddress('glFogCoordPointer');
  Pointer(glFogCoordd):=_GetProcedureAddress('glFogCoordd');
  Pointer(glFogCoorddv):=_GetProcedureAddress('glFogCoorddv');
  Pointer(glFogCoordf):=_GetProcedureAddress('glFogCoordf');
  Pointer(glFogCoordfv):=_GetProcedureAddress('glFogCoordfv');
  Pointer(glFragmentCoverageColorNV):=_GetProcedureAddress('glFragmentCoverageColorNV');
  Pointer(glFramebufferFetchBarrierEXT):=_GetProcedureAddress('glFramebufferFetchBarrierEXT');
  Pointer(glFramebufferFetchBarrierQCOM):=_GetProcedureAddress('glFramebufferFetchBarrierQCOM');
  Pointer(glFramebufferFoveationConfigQCOM):=_GetProcedureAddress('glFramebufferFoveationConfigQCOM');
  Pointer(glFramebufferFoveationParametersQCOM):=_GetProcedureAddress('glFramebufferFoveationParametersQCOM');
  Pointer(glFramebufferPixelLocalStorageSizeEXT):=_GetProcedureAddress('glFramebufferPixelLocalStorageSizeEXT');
  Pointer(glFramebufferSampleLocationsfvNV):=_GetProcedureAddress('glFramebufferSampleLocationsfvNV');
  Pointer(glFramebufferTexture1D):=_GetProcedureAddress('glFramebufferTexture1D');
  Pointer(glFramebufferTexture2DDownsampleIMG):=_GetProcedureAddress('glFramebufferTexture2DDownsampleIMG');
  Pointer(glFramebufferTexture2DMultisampleEXT):=_GetProcedureAddress('glFramebufferTexture2DMultisampleEXT');
  Pointer(glFramebufferTexture2DMultisampleIMG):=_GetProcedureAddress('glFramebufferTexture2DMultisampleIMG');
  Pointer(glFramebufferTexture3D):=_GetProcedureAddress('glFramebufferTexture3D');
  Pointer(glFramebufferTexture3DOES):=_GetProcedureAddress('glFramebufferTexture3DOES');
  Pointer(glFramebufferTextureEXT):=_GetProcedureAddress('glFramebufferTextureEXT');
  Pointer(glFramebufferTextureFaceARB):=_GetProcedureAddress('glFramebufferTextureFaceARB');
  Pointer(glFramebufferTextureLayerDownsampleIMG):=_GetProcedureAddress('glFramebufferTextureLayerDownsampleIMG');
  Pointer(glFramebufferTextureMultisampleMultiviewOVR):=_GetProcedureAddress('glFramebufferTextureMultisampleMultiviewOVR');
  Pointer(glFramebufferTextureMultiviewOVR):=_GetProcedureAddress('glFramebufferTextureMultiviewOVR');
  Pointer(glFramebufferTextureOES):=_GetProcedureAddress('glFramebufferTextureOES');
  Pointer(glGenFencesNV):=_GetProcedureAddress('glGenFencesNV');
  Pointer(glGenPathsNV):=_GetProcedureAddress('glGenPathsNV');
  Pointer(glGenPerfMonitorsAMD):=_GetProcedureAddress('glGenPerfMonitorsAMD');
  Pointer(glGenProgramPipelinesEXT):=_GetProcedureAddress('glGenProgramPipelinesEXT');
  Pointer(glGenProgramsARB):=_GetProcedureAddress('glGenProgramsARB');
  Pointer(glGenQueriesEXT):=_GetProcedureAddress('glGenQueriesEXT');
  Pointer(glGenSemaphoresEXT):=_GetProcedureAddress('glGenSemaphoresEXT');
  Pointer(glGenVertexArraysOES):=_GetProcedureAddress('glGenVertexArraysOES');
  Pointer(glGetBufferPointervOES):=_GetProcedureAddress('glGetBufferPointervOES');
  Pointer(glGetBufferSubData):=_GetProcedureAddress('glGetBufferSubData');
  Pointer(glGetColorTable):=_GetProcedureAddress('glGetColorTable');
  Pointer(glGetColorTableParameterfv):=_GetProcedureAddress('glGetColorTableParameterfv');
  Pointer(glGetColorTableParameteriv):=_GetProcedureAddress('glGetColorTableParameteriv');
  Pointer(glGetCompressedTexImage):=_GetProcedureAddress('glGetCompressedTexImage');
  Pointer(glGetCoverageModulationTableNV):=_GetProcedureAddress('glGetCoverageModulationTableNV');
  Pointer(glGetDebugMessageLogKHR):=_GetProcedureAddress('glGetDebugMessageLogKHR');
  Pointer(glGetDoublei_v):=_GetProcedureAddress('glGetDoublei_v');
  Pointer(glGetDriverControlStringQCOM):=_GetProcedureAddress('glGetDriverControlStringQCOM');
  Pointer(glGetDriverControlsQCOM):=_GetProcedureAddress('glGetDriverControlsQCOM');
  Pointer(glGetFenceivNV):=_GetProcedureAddress('glGetFenceivNV');
  Pointer(glGetFirstPerfQueryIdINTEL):=_GetProcedureAddress('glGetFirstPerfQueryIdINTEL');
  Pointer(glGetFloati_v):=_GetProcedureAddress('glGetFloati_v');
  Pointer(glGetFloati_vNV):=_GetProcedureAddress('glGetFloati_vNV');
  Pointer(glGetFloati_vOES):=_GetProcedureAddress('glGetFloati_vOES');
  Pointer(glGetFragDataIndex):=_GetProcedureAddress('glGetFragDataIndex');
  Pointer(glGetFragDataIndexEXT):=_GetProcedureAddress('glGetFragDataIndexEXT');
  Pointer(glGetFramebufferPixelLocalStorageSizeEXT):=_GetProcedureAddress('glGetFramebufferPixelLocalStorageSizeEXT');
  Pointer(glGetGraphicsResetStatusEXT):=_GetProcedureAddress('glGetGraphicsResetStatusEXT');
  Pointer(glGetGraphicsResetStatusKHR):=_GetProcedureAddress('glGetGraphicsResetStatusKHR');
  Pointer(glGetImageHandleNV):=_GetProcedureAddress('glGetImageHandleNV');
  Pointer(glGetInteger64vAPPLE):=_GetProcedureAddress('glGetInteger64vAPPLE');
  Pointer(glGetInteger64vEXT):=_GetProcedureAddress('glGetInteger64vEXT');
  Pointer(glGetIntegeri_vEXT):=_GetProcedureAddress('glGetIntegeri_vEXT');
  Pointer(glGetInternalformatSampleivNV):=_GetProcedureAddress('glGetInternalformatSampleivNV');
  Pointer(glGetMemoryObjectDetachedResourcesuivNV):=_GetProcedureAddress('glGetMemoryObjectDetachedResourcesuivNV');
  Pointer(glGetMemoryObjectParameterivEXT):=_GetProcedureAddress('glGetMemoryObjectParameterivEXT');
  Pointer(glGetNextPerfQueryIdINTEL):=_GetProcedureAddress('glGetNextPerfQueryIdINTEL');
  Pointer(glGetObjectLabelEXT):=_GetProcedureAddress('glGetObjectLabelEXT');
  Pointer(glGetObjectLabelKHR):=_GetProcedureAddress('glGetObjectLabelKHR');
  Pointer(glGetObjectPtrLabelKHR):=_GetProcedureAddress('glGetObjectPtrLabelKHR');
  Pointer(glGetPathColorGenfvNV):=_GetProcedureAddress('glGetPathColorGenfvNV');
  Pointer(glGetPathColorGenivNV):=_GetProcedureAddress('glGetPathColorGenivNV');
  Pointer(glGetPathCommandsNV):=_GetProcedureAddress('glGetPathCommandsNV');
  Pointer(glGetPathCoordsNV):=_GetProcedureAddress('glGetPathCoordsNV');
  Pointer(glGetPathDashArrayNV):=_GetProcedureAddress('glGetPathDashArrayNV');
  Pointer(glGetPathLengthNV):=_GetProcedureAddress('glGetPathLengthNV');
  Pointer(glGetPathMetricRangeNV):=_GetProcedureAddress('glGetPathMetricRangeNV');
  Pointer(glGetPathMetricsNV):=_GetProcedureAddress('glGetPathMetricsNV');
  Pointer(glGetPathParameterfvNV):=_GetProcedureAddress('glGetPathParameterfvNV');
  Pointer(glGetPathParameterivNV):=_GetProcedureAddress('glGetPathParameterivNV');
  Pointer(glGetPathSpacingNV):=_GetProcedureAddress('glGetPathSpacingNV');
  Pointer(glGetPathTexGenfvNV):=_GetProcedureAddress('glGetPathTexGenfvNV');
  Pointer(glGetPathTexGenivNV):=_GetProcedureAddress('glGetPathTexGenivNV');
  Pointer(glGetPerfCounterInfoINTEL):=_GetProcedureAddress('glGetPerfCounterInfoINTEL');
  Pointer(glGetPerfMonitorCounterDataAMD):=_GetProcedureAddress('glGetPerfMonitorCounterDataAMD');
  Pointer(glGetPerfMonitorCounterInfoAMD):=_GetProcedureAddress('glGetPerfMonitorCounterInfoAMD');
  Pointer(glGetPerfMonitorCounterStringAMD):=_GetProcedureAddress('glGetPerfMonitorCounterStringAMD');
  Pointer(glGetPerfMonitorCountersAMD):=_GetProcedureAddress('glGetPerfMonitorCountersAMD');
  Pointer(glGetPerfMonitorGroupStringAMD):=_GetProcedureAddress('glGetPerfMonitorGroupStringAMD');
  Pointer(glGetPerfMonitorGroupsAMD):=_GetProcedureAddress('glGetPerfMonitorGroupsAMD');
  Pointer(glGetPerfQueryDataINTEL):=_GetProcedureAddress('glGetPerfQueryDataINTEL');
  Pointer(glGetPerfQueryIdByNameINTEL):=_GetProcedureAddress('glGetPerfQueryIdByNameINTEL');
  Pointer(glGetPerfQueryInfoINTEL):=_GetProcedureAddress('glGetPerfQueryInfoINTEL');
  Pointer(glGetPointervKHR):=_GetProcedureAddress('glGetPointervKHR');
  Pointer(glGetProgramBinaryOES):=_GetProcedureAddress('glGetProgramBinaryOES');
  Pointer(glGetProgramPipelineInfoLogEXT):=_GetProcedureAddress('glGetProgramPipelineInfoLogEXT');
  Pointer(glGetProgramPipelineivEXT):=_GetProcedureAddress('glGetProgramPipelineivEXT');
  Pointer(glGetProgramResourceLocationIndexEXT):=_GetProcedureAddress('glGetProgramResourceLocationIndexEXT');
  Pointer(glGetProgramResourcefvNV):=_GetProcedureAddress('glGetProgramResourcefvNV');
  Pointer(glGetQueryObjecti64v):=_GetProcedureAddress('glGetQueryObjecti64v');
  Pointer(glGetQueryObjecti64vEXT):=_GetProcedureAddress('glGetQueryObjecti64vEXT');
  Pointer(glGetQueryObjectiv):=_GetProcedureAddress('glGetQueryObjectiv');
  Pointer(glGetQueryObjectivEXT):=_GetProcedureAddress('glGetQueryObjectivEXT');
  Pointer(glGetQueryObjectui64v):=_GetProcedureAddress('glGetQueryObjectui64v');
  Pointer(glGetQueryObjectui64vEXT):=_GetProcedureAddress('glGetQueryObjectui64vEXT');
  Pointer(glGetQueryObjectuivEXT):=_GetProcedureAddress('glGetQueryObjectuivEXT');
  Pointer(glGetQueryivEXT):=_GetProcedureAddress('glGetQueryivEXT');
  Pointer(glGetSamplerParameterIivEXT):=_GetProcedureAddress('glGetSamplerParameterIivEXT');
  Pointer(glGetSamplerParameterIivOES):=_GetProcedureAddress('glGetSamplerParameterIivOES');
  Pointer(glGetSamplerParameterIuivEXT):=_GetProcedureAddress('glGetSamplerParameterIuivEXT');
  Pointer(glGetSamplerParameterIuivOES):=_GetProcedureAddress('glGetSamplerParameterIuivOES');
  Pointer(glGetSemaphoreParameterivNV):=_GetProcedureAddress('glGetSemaphoreParameterivNV');
  Pointer(glGetSemaphoreParameterui64vEXT):=_GetProcedureAddress('glGetSemaphoreParameterui64vEXT');
  Pointer(glGetShadingRateImagePaletteNV):=_GetProcedureAddress('glGetShadingRateImagePaletteNV');
  Pointer(glGetShadingRateSampleLocationivNV):=_GetProcedureAddress('glGetShadingRateSampleLocationivNV');
  Pointer(glGetSyncivAPPLE):=_GetProcedureAddress('glGetSyncivAPPLE');
  Pointer(glGetTexParameterIivEXT):=_GetProcedureAddress('glGetTexParameterIivEXT');
  Pointer(glGetTexParameterIivOES):=_GetProcedureAddress('glGetTexParameterIivOES');
  Pointer(glGetTexParameterIuivEXT):=_GetProcedureAddress('glGetTexParameterIuivEXT');
  Pointer(glGetTexParameterIuivOES):=_GetProcedureAddress('glGetTexParameterIuivOES');
  Pointer(glGetTextureHandleARB):=_GetProcedureAddress('glGetTextureHandleARB');
  Pointer(glGetTextureHandleIMG):=_GetProcedureAddress('glGetTextureHandleIMG');
  Pointer(glGetTextureHandleNV):=_GetProcedureAddress('glGetTextureHandleNV');
  Pointer(glGetTextureSamplerHandleARB):=_GetProcedureAddress('glGetTextureSamplerHandleARB');
  Pointer(glGetTextureSamplerHandleIMG):=_GetProcedureAddress('glGetTextureSamplerHandleIMG');
  Pointer(glGetTextureSamplerHandleNV):=_GetProcedureAddress('glGetTextureSamplerHandleNV');
  Pointer(glGetTranslatedShaderSourceANGLE):=_GetProcedureAddress('glGetTranslatedShaderSourceANGLE');
  Pointer(glGetUniformi64vNV):=_GetProcedureAddress('glGetUniformi64vNV');
  Pointer(glGetUnsignedBytevEXT):=_GetProcedureAddress('glGetUnsignedBytevEXT');
  Pointer(glGetUnsignedBytei_vEXT):=_GetProcedureAddress('glGetUnsignedBytei_vEXT');
  Pointer(glGetVertexAttribLdv):=_GetProcedureAddress('glGetVertexAttribLdv');
  Pointer(glGetVertexAttribdv):=_GetProcedureAddress('glGetVertexAttribdv');
  Pointer(glGetnUniformfvEXT):=_GetProcedureAddress('glGetnUniformfvEXT');
  Pointer(glGetnUniformfvKHR):=_GetProcedureAddress('glGetnUniformfvKHR');
  Pointer(glGetnUniformivEXT):=_GetProcedureAddress('glGetnUniformivEXT');
  Pointer(glGetnUniformivKHR):=_GetProcedureAddress('glGetnUniformivKHR');
  Pointer(glGetnUniformuivKHR):=_GetProcedureAddress('glGetnUniformuivKHR');
  Pointer(glHistogram):=_GetProcedureAddress('glHistogram');
  Pointer(glImportMemoryFdEXT):=_GetProcedureAddress('glImportMemoryFdEXT');
  Pointer(glImportMemoryWin32HandleEXT):=_GetProcedureAddress('glImportMemoryWin32HandleEXT');
  Pointer(glImportMemoryWin32NameEXT):=_GetProcedureAddress('glImportMemoryWin32NameEXT');
  Pointer(glImportSemaphoreFdEXT):=_GetProcedureAddress('glImportSemaphoreFdEXT');
  Pointer(glImportSemaphoreWin32HandleEXT):=_GetProcedureAddress('glImportSemaphoreWin32HandleEXT');
  Pointer(glImportSemaphoreWin32NameEXT):=_GetProcedureAddress('glImportSemaphoreWin32NameEXT');
  Pointer(glInsertEventMarkerEXT):=_GetProcedureAddress('glInsertEventMarkerEXT');
  Pointer(glInterpolatePathsNV):=_GetProcedureAddress('glInterpolatePathsNV');
  Pointer(glIsEnablediEXT):=_GetProcedureAddress('glIsEnablediEXT');
  Pointer(glIsEnablediNV):=_GetProcedureAddress('glIsEnablediNV');
  Pointer(glIsEnablediOES):=_GetProcedureAddress('glIsEnablediOES');
  Pointer(glIsFenceNV):=_GetProcedureAddress('glIsFenceNV');
  Pointer(glIsImageHandleResidentNV):=_GetProcedureAddress('glIsImageHandleResidentNV');
  Pointer(glIsMemoryObjectEXT):=_GetProcedureAddress('glIsMemoryObjectEXT');
  Pointer(glIsPathNV):=_GetProcedureAddress('glIsPathNV');
  Pointer(glIsPointInFillPathNV):=_GetProcedureAddress('glIsPointInFillPathNV');
  Pointer(glIsPointInStrokePathNV):=_GetProcedureAddress('glIsPointInStrokePathNV');
  Pointer(glIsProgramARB):=_GetProcedureAddress('glIsProgramARB');
  Pointer(glIsProgramPipelineEXT):=_GetProcedureAddress('glIsProgramPipelineEXT');
  Pointer(glIsQueryEXT):=_GetProcedureAddress('glIsQueryEXT');
  Pointer(glIsSemaphoreEXT):=_GetProcedureAddress('glIsSemaphoreEXT');
  Pointer(glIsSyncAPPLE):=_GetProcedureAddress('glIsSyncAPPLE');
  Pointer(glIsTextureHandleResidentNV):=_GetProcedureAddress('glIsTextureHandleResidentNV');
  Pointer(glIsVertexArrayOES):=_GetProcedureAddress('glIsVertexArrayOES');
  Pointer(glLabelObjectEXT):=_GetProcedureAddress('glLabelObjectEXT');
  Pointer(glLoadTransposeMatrixd):=_GetProcedureAddress('glLoadTransposeMatrixd');
  Pointer(glLoadTransposeMatrixf):=_GetProcedureAddress('glLoadTransposeMatrixf');
  Pointer(glMakeImageHandleNonResidentNV):=_GetProcedureAddress('glMakeImageHandleNonResidentNV');
  Pointer(glMakeImageHandleResidentNV):=_GetProcedureAddress('glMakeImageHandleResidentNV');
  Pointer(glMakeTextureHandleNonResidentNV):=_GetProcedureAddress('glMakeTextureHandleNonResidentNV');
  Pointer(glMakeTextureHandleResidentNV):=_GetProcedureAddress('glMakeTextureHandleResidentNV');
  Pointer(glMapBuffer):=_GetProcedureAddress('glMapBuffer');
  Pointer(glMapBufferOES):=_GetProcedureAddress('glMapBufferOES');
  Pointer(glMapBufferRangeEXT):=_GetProcedureAddress('glMapBufferRangeEXT');
  Pointer(glMatrixFrustumEXT):=_GetProcedureAddress('glMatrixFrustumEXT');
  Pointer(glMatrixLoad3x2fNV):=_GetProcedureAddress('glMatrixLoad3x2fNV');
  Pointer(glMatrixLoad3x3fNV):=_GetProcedureAddress('glMatrixLoad3x3fNV');
  Pointer(glMatrixLoadIdentityEXT):=_GetProcedureAddress('glMatrixLoadIdentityEXT');
  Pointer(glMatrixLoadTranspose3x3fNV):=_GetProcedureAddress('glMatrixLoadTranspose3x3fNV');
  Pointer(glMatrixLoadTransposedEXT):=_GetProcedureAddress('glMatrixLoadTransposedEXT');
  Pointer(glMatrixLoadTransposefEXT):=_GetProcedureAddress('glMatrixLoadTransposefEXT');
  Pointer(glMatrixLoaddEXT):=_GetProcedureAddress('glMatrixLoaddEXT');
  Pointer(glMatrixLoadfEXT):=_GetProcedureAddress('glMatrixLoadfEXT');
  Pointer(glMatrixMult3x2fNV):=_GetProcedureAddress('glMatrixMult3x2fNV');
  Pointer(glMatrixMult3x3fNV):=_GetProcedureAddress('glMatrixMult3x3fNV');
  Pointer(glMatrixMultTranspose3x3fNV):=_GetProcedureAddress('glMatrixMultTranspose3x3fNV');
  Pointer(glMatrixMultTransposedEXT):=_GetProcedureAddress('glMatrixMultTransposedEXT');
  Pointer(glMatrixMultTransposefEXT):=_GetProcedureAddress('glMatrixMultTransposefEXT');
  Pointer(glMatrixMultdEXT):=_GetProcedureAddress('glMatrixMultdEXT');
  Pointer(glMatrixMultfEXT):=_GetProcedureAddress('glMatrixMultfEXT');
  Pointer(glMatrixOrthoEXT):=_GetProcedureAddress('glMatrixOrthoEXT');
  Pointer(glMatrixPopEXT):=_GetProcedureAddress('glMatrixPopEXT');
  Pointer(glMatrixPushEXT):=_GetProcedureAddress('glMatrixPushEXT');
  Pointer(glMatrixRotatedEXT):=_GetProcedureAddress('glMatrixRotatedEXT');
  Pointer(glMatrixRotatefEXT):=_GetProcedureAddress('glMatrixRotatefEXT');
  Pointer(glMatrixScaledEXT):=_GetProcedureAddress('glMatrixScaledEXT');
  Pointer(glMatrixScalefEXT):=_GetProcedureAddress('glMatrixScalefEXT');
  Pointer(glMatrixTranslatedEXT):=_GetProcedureAddress('glMatrixTranslatedEXT');
  Pointer(glMatrixTranslatefEXT):=_GetProcedureAddress('glMatrixTranslatefEXT');
  Pointer(glMaxShaderCompilerThreadsKHR):=_GetProcedureAddress('glMaxShaderCompilerThreadsKHR');
  Pointer(glMemoryObjectParameterivEXT):=_GetProcedureAddress('glMemoryObjectParameterivEXT');
  Pointer(glMinSampleShadingOES):=_GetProcedureAddress('glMinSampleShadingOES');
  Pointer(glMinmax):=_GetProcedureAddress('glMinmax');
  Pointer(glMultTransposeMatrixd):=_GetProcedureAddress('glMultTransposeMatrixd');
  Pointer(glMultTransposeMatrixf):=_GetProcedureAddress('glMultTransposeMatrixf');
  Pointer(glMultiDrawArrays):=_GetProcedureAddress('glMultiDrawArrays');
  Pointer(glMultiDrawArraysEXT):=_GetProcedureAddress('glMultiDrawArraysEXT');
  Pointer(glMultiDrawArraysIndirect):=_GetProcedureAddress('glMultiDrawArraysIndirect');
  Pointer(glMultiDrawArraysIndirectCount):=_GetProcedureAddress('glMultiDrawArraysIndirectCount');
  Pointer(glMultiDrawArraysIndirectEXT):=_GetProcedureAddress('glMultiDrawArraysIndirectEXT');
  Pointer(glMultiDrawElements):=_GetProcedureAddress('glMultiDrawElements');
  Pointer(glMultiDrawElementsBaseVertex):=_GetProcedureAddress('glMultiDrawElementsBaseVertex');
  Pointer(glMultiDrawElementsBaseVertexEXT):=_GetProcedureAddress('glMultiDrawElementsBaseVertexEXT');
  Pointer(glMultiDrawElementsEXT):=_GetProcedureAddress('glMultiDrawElementsEXT');
  Pointer(glMultiDrawElementsIndirect):=_GetProcedureAddress('glMultiDrawElementsIndirect');
  Pointer(glMultiDrawElementsIndirectCount):=_GetProcedureAddress('glMultiDrawElementsIndirectCount');
  Pointer(glMultiDrawElementsIndirectEXT):=_GetProcedureAddress('glMultiDrawElementsIndirectEXT');
  Pointer(glMultiDrawMeshTasksIndirectNV):=_GetProcedureAddress('glMultiDrawMeshTasksIndirectNV');
  Pointer(glMultiDrawMeshTasksIndirectCountNV):=_GetProcedureAddress('glMultiDrawMeshTasksIndirectCountNV');
  Pointer(glMultiTexCoord1d):=_GetProcedureAddress('glMultiTexCoord1d');
  Pointer(glMultiTexCoord1dv):=_GetProcedureAddress('glMultiTexCoord1dv');
  Pointer(glMultiTexCoord1f):=_GetProcedureAddress('glMultiTexCoord1f');
  Pointer(glMultiTexCoord1fv):=_GetProcedureAddress('glMultiTexCoord1fv');
  Pointer(glMultiTexCoord1i):=_GetProcedureAddress('glMultiTexCoord1i');
  Pointer(glMultiTexCoord1iv):=_GetProcedureAddress('glMultiTexCoord1iv');
  Pointer(glMultiTexCoord1s):=_GetProcedureAddress('glMultiTexCoord1s');
  Pointer(glMultiTexCoord1sv):=_GetProcedureAddress('glMultiTexCoord1sv');
  Pointer(glMultiTexCoord2d):=_GetProcedureAddress('glMultiTexCoord2d');
  Pointer(glMultiTexCoord2dv):=_GetProcedureAddress('glMultiTexCoord2dv');
  Pointer(glMultiTexCoord2f):=_GetProcedureAddress('glMultiTexCoord2f');
  Pointer(glMultiTexCoord2fv):=_GetProcedureAddress('glMultiTexCoord2fv');
  Pointer(glMultiTexCoord2i):=_GetProcedureAddress('glMultiTexCoord2i');
  Pointer(glMultiTexCoord2iv):=_GetProcedureAddress('glMultiTexCoord2iv');
  Pointer(glMultiTexCoord2s):=_GetProcedureAddress('glMultiTexCoord2s');
  Pointer(glMultiTexCoord2sv):=_GetProcedureAddress('glMultiTexCoord2sv');
  Pointer(glMultiTexCoord3d):=_GetProcedureAddress('glMultiTexCoord3d');
  Pointer(glMultiTexCoord3dv):=_GetProcedureAddress('glMultiTexCoord3dv');
  Pointer(glMultiTexCoord3f):=_GetProcedureAddress('glMultiTexCoord3f');
  Pointer(glMultiTexCoord3fv):=_GetProcedureAddress('glMultiTexCoord3fv');
  Pointer(glMultiTexCoord3i):=_GetProcedureAddress('glMultiTexCoord3i');
  Pointer(glMultiTexCoord3iv):=_GetProcedureAddress('glMultiTexCoord3iv');
  Pointer(glMultiTexCoord3s):=_GetProcedureAddress('glMultiTexCoord3s');
  Pointer(glMultiTexCoord3sv):=_GetProcedureAddress('glMultiTexCoord3sv');
  Pointer(glMultiTexCoord4d):=_GetProcedureAddress('glMultiTexCoord4d');
  Pointer(glMultiTexCoord4dv):=_GetProcedureAddress('glMultiTexCoord4dv');
  Pointer(glMultiTexCoord4f):=_GetProcedureAddress('glMultiTexCoord4f');
  Pointer(glMultiTexCoord4fv):=_GetProcedureAddress('glMultiTexCoord4fv');
  Pointer(glMultiTexCoord4i):=_GetProcedureAddress('glMultiTexCoord4i');
  Pointer(glMultiTexCoord4iv):=_GetProcedureAddress('glMultiTexCoord4iv');
  Pointer(glMultiTexCoord4s):=_GetProcedureAddress('glMultiTexCoord4s');
  Pointer(glMultiTexCoord4sv):=_GetProcedureAddress('glMultiTexCoord4sv');
  Pointer(glNamedBufferAttachMemoryNV):=_GetProcedureAddress('glNamedBufferAttachMemoryNV');
  Pointer(glNamedBufferPageCommitmentMemNV):=_GetProcedureAddress('glNamedBufferPageCommitmentMemNV');
  Pointer(glNamedBufferStorage):=_GetProcedureAddress('glNamedBufferStorage');
  Pointer(glNamedBufferStorageExternalEXT):=_GetProcedureAddress('glNamedBufferStorageExternalEXT');
  Pointer(glNamedBufferStorageMemEXT):=_GetProcedureAddress('glNamedBufferStorageMemEXT');
  Pointer(glNamedBufferSubData):=_GetProcedureAddress('glNamedBufferSubData');
  Pointer(glNamedFramebufferSampleLocationsfvNV):=_GetProcedureAddress('glNamedFramebufferSampleLocationsfvNV');
  Pointer(glNamedRenderbufferStorageMultisampleAdvancedAMD):=_GetProcedureAddress('glNamedRenderbufferStorageMultisampleAdvancedAMD');
  Pointer(glObjectLabelKHR):=_GetProcedureAddress('glObjectLabelKHR');
  Pointer(glObjectPtrLabelKHR):=_GetProcedureAddress('glObjectPtrLabelKHR');
  Pointer(glPatchParameteriEXT):=_GetProcedureAddress('glPatchParameteriEXT');
  Pointer(glPatchParameteriOES):=_GetProcedureAddress('glPatchParameteriOES');
  Pointer(glPathColorGenNV):=_GetProcedureAddress('glPathColorGenNV');
  Pointer(glPathCommandsNV):=_GetProcedureAddress('glPathCommandsNV');
  Pointer(glPathCoordsNV):=_GetProcedureAddress('glPathCoordsNV');
  Pointer(glPathCoverDepthFuncNV):=_GetProcedureAddress('glPathCoverDepthFuncNV');
  Pointer(glPathDashArrayNV):=_GetProcedureAddress('glPathDashArrayNV');
  Pointer(glPathFogGenNV):=_GetProcedureAddress('glPathFogGenNV');
  Pointer(glPathGlyphIndexArrayNV):=_GetProcedureAddress('glPathGlyphIndexArrayNV');
  Pointer(glPathGlyphIndexRangeNV):=_GetProcedureAddress('glPathGlyphIndexRangeNV');
  Pointer(glPathGlyphRangeNV):=_GetProcedureAddress('glPathGlyphRangeNV');
  Pointer(glPathGlyphsNV):=_GetProcedureAddress('glPathGlyphsNV');
  Pointer(glPathMemoryGlyphIndexArrayNV):=_GetProcedureAddress('glPathMemoryGlyphIndexArrayNV');
  Pointer(glPathParameterfNV):=_GetProcedureAddress('glPathParameterfNV');
  Pointer(glPathParameterfvNV):=_GetProcedureAddress('glPathParameterfvNV');
  Pointer(glPathParameteriNV):=_GetProcedureAddress('glPathParameteriNV');
  Pointer(glPathParameterivNV):=_GetProcedureAddress('glPathParameterivNV');
  Pointer(glPathStencilDepthOffsetNV):=_GetProcedureAddress('glPathStencilDepthOffsetNV');
  Pointer(glPathStencilFuncNV):=_GetProcedureAddress('glPathStencilFuncNV');
  Pointer(glPathStringNV):=_GetProcedureAddress('glPathStringNV');
  Pointer(glPathSubCommandsNV):=_GetProcedureAddress('glPathSubCommandsNV');
  Pointer(glPathSubCoordsNV):=_GetProcedureAddress('glPathSubCoordsNV');
  Pointer(glPathTexGenNV):=_GetProcedureAddress('glPathTexGenNV');
  Pointer(glPointAlongPathNV):=_GetProcedureAddress('glPointAlongPathNV');
  Pointer(glPointParameterf):=_GetProcedureAddress('glPointParameterf');
  Pointer(glPointParameterfv):=_GetProcedureAddress('glPointParameterfv');
  Pointer(glPointParameteri):=_GetProcedureAddress('glPointParameteri');
  Pointer(glPointParameteriv):=_GetProcedureAddress('glPointParameteriv');
  Pointer(glPolygonMode):=_GetProcedureAddress('glPolygonMode');
  Pointer(glPolygonModeNV):=_GetProcedureAddress('glPolygonModeNV');
  Pointer(glPolygonOffsetClamp):=_GetProcedureAddress('glPolygonOffsetClamp');
  Pointer(glPolygonOffsetClampEXT):=_GetProcedureAddress('glPolygonOffsetClampEXT');
  Pointer(glPopDebugGroupKHR):=_GetProcedureAddress('glPopDebugGroupKHR');
  Pointer(glPopGroupMarkerEXT):=_GetProcedureAddress('glPopGroupMarkerEXT');
  Pointer(glPrimitiveBoundingBoxEXT):=_GetProcedureAddress('glPrimitiveBoundingBoxEXT');
  Pointer(glPrimitiveBoundingBoxOES):=_GetProcedureAddress('glPrimitiveBoundingBoxOES');
  Pointer(glPrioritizeTextures):=_GetProcedureAddress('glPrioritizeTextures');
  Pointer(glProgramBinaryOES):=_GetProcedureAddress('glProgramBinaryOES');
  Pointer(glProgramParameteriEXT):=_GetProcedureAddress('glProgramParameteriEXT');
  Pointer(glProgramPathFragmentInputGenNV):=_GetProcedureAddress('glProgramPathFragmentInputGenNV');
  Pointer(glProgramUniform1fEXT):=_GetProcedureAddress('glProgramUniform1fEXT');
  Pointer(glProgramUniform1fvEXT):=_GetProcedureAddress('glProgramUniform1fvEXT');
  Pointer(glProgramUniform1i64NV):=_GetProcedureAddress('glProgramUniform1i64NV');
  Pointer(glProgramUniform1i64vNV):=_GetProcedureAddress('glProgramUniform1i64vNV');
  Pointer(glProgramUniform1iEXT):=_GetProcedureAddress('glProgramUniform1iEXT');
  Pointer(glProgramUniform1ivEXT):=_GetProcedureAddress('glProgramUniform1ivEXT');
  Pointer(glProgramUniform1ui64NV):=_GetProcedureAddress('glProgramUniform1ui64NV');
  Pointer(glProgramUniform1ui64vNV):=_GetProcedureAddress('glProgramUniform1ui64vNV');
  Pointer(glProgramUniform1uiEXT):=_GetProcedureAddress('glProgramUniform1uiEXT');
  Pointer(glProgramUniform1uivEXT):=_GetProcedureAddress('glProgramUniform1uivEXT');
  Pointer(glProgramUniform2fEXT):=_GetProcedureAddress('glProgramUniform2fEXT');
  Pointer(glProgramUniform2fvEXT):=_GetProcedureAddress('glProgramUniform2fvEXT');
  Pointer(glProgramUniform2i64NV):=_GetProcedureAddress('glProgramUniform2i64NV');
  Pointer(glProgramUniform2i64vNV):=_GetProcedureAddress('glProgramUniform2i64vNV');
  Pointer(glProgramUniform2iEXT):=_GetProcedureAddress('glProgramUniform2iEXT');
  Pointer(glProgramUniform2ivEXT):=_GetProcedureAddress('glProgramUniform2ivEXT');
  Pointer(glProgramUniform2ui64NV):=_GetProcedureAddress('glProgramUniform2ui64NV');
  Pointer(glProgramUniform2ui64vNV):=_GetProcedureAddress('glProgramUniform2ui64vNV');
  Pointer(glProgramUniform2uiEXT):=_GetProcedureAddress('glProgramUniform2uiEXT');
  Pointer(glProgramUniform2uivEXT):=_GetProcedureAddress('glProgramUniform2uivEXT');
  Pointer(glProgramUniform3fEXT):=_GetProcedureAddress('glProgramUniform3fEXT');
  Pointer(glProgramUniform3fvEXT):=_GetProcedureAddress('glProgramUniform3fvEXT');
  Pointer(glProgramUniform3i64NV):=_GetProcedureAddress('glProgramUniform3i64NV');
  Pointer(glProgramUniform3i64vNV):=_GetProcedureAddress('glProgramUniform3i64vNV');
  Pointer(glProgramUniform3iEXT):=_GetProcedureAddress('glProgramUniform3iEXT');
  Pointer(glProgramUniform3ivEXT):=_GetProcedureAddress('glProgramUniform3ivEXT');
  Pointer(glProgramUniform3ui64NV):=_GetProcedureAddress('glProgramUniform3ui64NV');
  Pointer(glProgramUniform3ui64vNV):=_GetProcedureAddress('glProgramUniform3ui64vNV');
  Pointer(glProgramUniform3uiEXT):=_GetProcedureAddress('glProgramUniform3uiEXT');
  Pointer(glProgramUniform3uivEXT):=_GetProcedureAddress('glProgramUniform3uivEXT');
  Pointer(glProgramUniform4fEXT):=_GetProcedureAddress('glProgramUniform4fEXT');
  Pointer(glProgramUniform4fvEXT):=_GetProcedureAddress('glProgramUniform4fvEXT');
  Pointer(glProgramUniform4i64NV):=_GetProcedureAddress('glProgramUniform4i64NV');
  Pointer(glProgramUniform4i64vNV):=_GetProcedureAddress('glProgramUniform4i64vNV');
  Pointer(glProgramUniform4iEXT):=_GetProcedureAddress('glProgramUniform4iEXT');
  Pointer(glProgramUniform4ivEXT):=_GetProcedureAddress('glProgramUniform4ivEXT');
  Pointer(glProgramUniform4ui64NV):=_GetProcedureAddress('glProgramUniform4ui64NV');
  Pointer(glProgramUniform4ui64vNV):=_GetProcedureAddress('glProgramUniform4ui64vNV');
  Pointer(glProgramUniform4uiEXT):=_GetProcedureAddress('glProgramUniform4uiEXT');
  Pointer(glProgramUniform4uivEXT):=_GetProcedureAddress('glProgramUniform4uivEXT');
  Pointer(glProgramUniformHandleui64ARB):=_GetProcedureAddress('glProgramUniformHandleui64ARB');
  Pointer(glProgramUniformHandleui64IMG):=_GetProcedureAddress('glProgramUniformHandleui64IMG');
  Pointer(glProgramUniformHandleui64NV):=_GetProcedureAddress('glProgramUniformHandleui64NV');
  Pointer(glProgramUniformHandleui64vARB):=_GetProcedureAddress('glProgramUniformHandleui64vARB');
  Pointer(glProgramUniformHandleui64vIMG):=_GetProcedureAddress('glProgramUniformHandleui64vIMG');
  Pointer(glProgramUniformHandleui64vNV):=_GetProcedureAddress('glProgramUniformHandleui64vNV');
  Pointer(glProgramUniformMatrix2fvEXT):=_GetProcedureAddress('glProgramUniformMatrix2fvEXT');
  Pointer(glProgramUniformMatrix2x3fvEXT):=_GetProcedureAddress('glProgramUniformMatrix2x3fvEXT');
  Pointer(glProgramUniformMatrix2x4fvEXT):=_GetProcedureAddress('glProgramUniformMatrix2x4fvEXT');
  Pointer(glProgramUniformMatrix3fvEXT):=_GetProcedureAddress('glProgramUniformMatrix3fvEXT');
  Pointer(glProgramUniformMatrix3x2fvEXT):=_GetProcedureAddress('glProgramUniformMatrix3x2fvEXT');
  Pointer(glProgramUniformMatrix3x4fvEXT):=_GetProcedureAddress('glProgramUniformMatrix3x4fvEXT');
  Pointer(glProgramUniformMatrix4fvEXT):=_GetProcedureAddress('glProgramUniformMatrix4fvEXT');
  Pointer(glProgramUniformMatrix4x2fvEXT):=_GetProcedureAddress('glProgramUniformMatrix4x2fvEXT');
  Pointer(glProgramUniformMatrix4x3fvEXT):=_GetProcedureAddress('glProgramUniformMatrix4x3fvEXT');
  Pointer(glProvokingVertex):=_GetProcedureAddress('glProvokingVertex');
  Pointer(glPushDebugGroupKHR):=_GetProcedureAddress('glPushDebugGroupKHR');
  Pointer(glPushGroupMarkerEXT):=_GetProcedureAddress('glPushGroupMarkerEXT');
  Pointer(glQueryCounter):=_GetProcedureAddress('glQueryCounter');
  Pointer(glQueryCounterEXT):=_GetProcedureAddress('glQueryCounterEXT');
  Pointer(glRasterSamplesEXT):=_GetProcedureAddress('glRasterSamplesEXT');
  Pointer(glReadBufferIndexedEXT):=_GetProcedureAddress('glReadBufferIndexedEXT');
  Pointer(glReadBufferNV):=_GetProcedureAddress('glReadBufferNV');
  Pointer(glReadnPixelsEXT):=_GetProcedureAddress('glReadnPixelsEXT');
  Pointer(glReadnPixelsKHR):=_GetProcedureAddress('glReadnPixelsKHR');
  Pointer(glReleaseKeyedMutexWin32EXT):=_GetProcedureAddress('glReleaseKeyedMutexWin32EXT');
  Pointer(glRenderbufferStorageMultisampleANGLE):=_GetProcedureAddress('glRenderbufferStorageMultisampleANGLE');
  Pointer(glRenderbufferStorageMultisampleAPPLE):=_GetProcedureAddress('glRenderbufferStorageMultisampleAPPLE');
  Pointer(glRenderbufferStorageMultisampleAdvancedAMD):=_GetProcedureAddress('glRenderbufferStorageMultisampleAdvancedAMD');
  Pointer(glRenderbufferStorageMultisampleEXT):=_GetProcedureAddress('glRenderbufferStorageMultisampleEXT');
  Pointer(glRenderbufferStorageMultisampleIMG):=_GetProcedureAddress('glRenderbufferStorageMultisampleIMG');
  Pointer(glRenderbufferStorageMultisampleNV):=_GetProcedureAddress('glRenderbufferStorageMultisampleNV');
  Pointer(glResetHistogram):=_GetProcedureAddress('glResetHistogram');
  Pointer(glResetMemoryObjectParameterNV):=_GetProcedureAddress('glResetMemoryObjectParameterNV');
  Pointer(glResetMinmax):=_GetProcedureAddress('glResetMinmax');
  Pointer(glResolveDepthValuesNV):=_GetProcedureAddress('glResolveDepthValuesNV');
  Pointer(glResolveMultisampleFramebufferAPPLE):=_GetProcedureAddress('glResolveMultisampleFramebufferAPPLE');
  Pointer(glSampleMaskEXT):=_GetProcedureAddress('glSampleMaskEXT');
  Pointer(glSamplePatternEXT):=_GetProcedureAddress('glSamplePatternEXT');
  Pointer(glSamplerParameterIivEXT):=_GetProcedureAddress('glSamplerParameterIivEXT');
  Pointer(glSamplerParameterIivOES):=_GetProcedureAddress('glSamplerParameterIivOES');
  Pointer(glSamplerParameterIuivEXT):=_GetProcedureAddress('glSamplerParameterIuivEXT');
  Pointer(glSamplerParameterIuivOES):=_GetProcedureAddress('glSamplerParameterIuivOES');
  Pointer(glScissorArrayv):=_GetProcedureAddress('glScissorArrayv');
  Pointer(glScissorArrayvNV):=_GetProcedureAddress('glScissorArrayvNV');
  Pointer(glScissorArrayvOES):=_GetProcedureAddress('glScissorArrayvOES');
  Pointer(glScissorExclusiveArrayvNV):=_GetProcedureAddress('glScissorExclusiveArrayvNV');
  Pointer(glScissorExclusiveNV):=_GetProcedureAddress('glScissorExclusiveNV');
  Pointer(glScissorIndexed):=_GetProcedureAddress('glScissorIndexed');
  Pointer(glScissorIndexedNV):=_GetProcedureAddress('glScissorIndexedNV');
  Pointer(glScissorIndexedOES):=_GetProcedureAddress('glScissorIndexedOES');
  Pointer(glScissorIndexedv):=_GetProcedureAddress('glScissorIndexedv');
  Pointer(glScissorIndexedvNV):=_GetProcedureAddress('glScissorIndexedvNV');
  Pointer(glScissorIndexedvOES):=_GetProcedureAddress('glScissorIndexedvOES');
  Pointer(glSecondaryColor3b):=_GetProcedureAddress('glSecondaryColor3b');
  Pointer(glSecondaryColor3bv):=_GetProcedureAddress('glSecondaryColor3bv');
  Pointer(glSecondaryColor3d):=_GetProcedureAddress('glSecondaryColor3d');
  Pointer(glSecondaryColor3dv):=_GetProcedureAddress('glSecondaryColor3dv');
  Pointer(glSecondaryColor3f):=_GetProcedureAddress('glSecondaryColor3f');
  Pointer(glSecondaryColor3fv):=_GetProcedureAddress('glSecondaryColor3fv');
  Pointer(glSecondaryColor3i):=_GetProcedureAddress('glSecondaryColor3i');
  Pointer(glSecondaryColor3iv):=_GetProcedureAddress('glSecondaryColor3iv');
  Pointer(glSecondaryColor3s):=_GetProcedureAddress('glSecondaryColor3s');
  Pointer(glSecondaryColor3sv):=_GetProcedureAddress('glSecondaryColor3sv');
  Pointer(glSecondaryColor3ub):=_GetProcedureAddress('glSecondaryColor3ub');
  Pointer(glSecondaryColor3ubv):=_GetProcedureAddress('glSecondaryColor3ubv');
  Pointer(glSecondaryColor3ui):=_GetProcedureAddress('glSecondaryColor3ui');
  Pointer(glSecondaryColor3uiv):=_GetProcedureAddress('glSecondaryColor3uiv');
  Pointer(glSecondaryColor3us):=_GetProcedureAddress('glSecondaryColor3us');
  Pointer(glSecondaryColor3usv):=_GetProcedureAddress('glSecondaryColor3usv');
  Pointer(glSecondaryColorPointer):=_GetProcedureAddress('glSecondaryColorPointer');
  Pointer(glSelectPerfMonitorCountersAMD):=_GetProcedureAddress('glSelectPerfMonitorCountersAMD');
  Pointer(glSemaphoreParameterivNV):=_GetProcedureAddress('glSemaphoreParameterivNV');
  Pointer(glSemaphoreParameterui64vEXT):=_GetProcedureAddress('glSemaphoreParameterui64vEXT');
  Pointer(glSeparableFilter2D):=_GetProcedureAddress('glSeparableFilter2D');
  Pointer(glSetFenceNV):=_GetProcedureAddress('glSetFenceNV');
  Pointer(glShadingRateImageBarrierNV):=_GetProcedureAddress('glShadingRateImageBarrierNV');
  Pointer(glShadingRateQCOM):=_GetProcedureAddress('glShadingRateQCOM');
  Pointer(glShadingRateImagePaletteNV):=_GetProcedureAddress('glShadingRateImagePaletteNV');
  Pointer(glShadingRateSampleOrderNV):=_GetProcedureAddress('glShadingRateSampleOrderNV');
  Pointer(glShadingRateSampleOrderCustomNV):=_GetProcedureAddress('glShadingRateSampleOrderCustomNV');
  Pointer(glSignalSemaphoreEXT):=_GetProcedureAddress('glSignalSemaphoreEXT');
  Pointer(glSpecializeShader):=_GetProcedureAddress('glSpecializeShader');
  Pointer(glStartTilingQCOM):=_GetProcedureAddress('glStartTilingQCOM');
  Pointer(glStencilFillPathInstancedNV):=_GetProcedureAddress('glStencilFillPathInstancedNV');
  Pointer(glStencilFillPathNV):=_GetProcedureAddress('glStencilFillPathNV');
  Pointer(glStencilStrokePathInstancedNV):=_GetProcedureAddress('glStencilStrokePathInstancedNV');
  Pointer(glStencilStrokePathNV):=_GetProcedureAddress('glStencilStrokePathNV');
  Pointer(glStencilThenCoverFillPathInstancedNV):=_GetProcedureAddress('glStencilThenCoverFillPathInstancedNV');
  Pointer(glStencilThenCoverFillPathNV):=_GetProcedureAddress('glStencilThenCoverFillPathNV');
  Pointer(glStencilThenCoverStrokePathInstancedNV):=_GetProcedureAddress('glStencilThenCoverStrokePathInstancedNV');
  Pointer(glStencilThenCoverStrokePathNV):=_GetProcedureAddress('glStencilThenCoverStrokePathNV');
  Pointer(glSubpixelPrecisionBiasNV):=_GetProcedureAddress('glSubpixelPrecisionBiasNV');
  Pointer(glTestFenceNV):=_GetProcedureAddress('glTestFenceNV');
  Pointer(glTexAttachMemoryNV):=_GetProcedureAddress('glTexAttachMemoryNV');
  Pointer(glTexBufferEXT):=_GetProcedureAddress('glTexBufferEXT');
  Pointer(glTexBufferOES):=_GetProcedureAddress('glTexBufferOES');
  Pointer(glTexBufferRangeEXT):=_GetProcedureAddress('glTexBufferRangeEXT');
  Pointer(glTexBufferRangeOES):=_GetProcedureAddress('glTexBufferRangeOES');
  Pointer(glTexEstimateMotionQCOM):=_GetProcedureAddress('glTexEstimateMotionQCOM');
  Pointer(glTexEstimateMotionRegionsQCOM):=_GetProcedureAddress('glTexEstimateMotionRegionsQCOM');
  Pointer(glTexImage3DOES):=_GetProcedureAddress('glTexImage3DOES');
  Pointer(glTexPageCommitmentARB):=_GetProcedureAddress('glTexPageCommitmentARB');
  Pointer(glTexPageCommitmentEXT):=_GetProcedureAddress('glTexPageCommitmentEXT');
  Pointer(glTexPageCommitmentMemNV):=_GetProcedureAddress('glTexPageCommitmentMemNV');
  Pointer(glTexParameterIivEXT):=_GetProcedureAddress('glTexParameterIivEXT');
  Pointer(glTexParameterIivOES):=_GetProcedureAddress('glTexParameterIivOES');
  Pointer(glTexParameterIuivEXT):=_GetProcedureAddress('glTexParameterIuivEXT');
  Pointer(glTexParameterIuivOES):=_GetProcedureAddress('glTexParameterIuivOES');
  Pointer(glTexStorage1D):=_GetProcedureAddress('glTexStorage1D');
  Pointer(glTexStorage1DEXT):=_GetProcedureAddress('glTexStorage1DEXT');
  Pointer(glTexStorage2DEXT):=_GetProcedureAddress('glTexStorage2DEXT');
  Pointer(glTexStorage3DEXT):=_GetProcedureAddress('glTexStorage3DEXT');
  Pointer(glTexStorage3DMultisampleOES):=_GetProcedureAddress('glTexStorage3DMultisampleOES');
  Pointer(glTexStorageMem1DEXT):=_GetProcedureAddress('glTexStorageMem1DEXT');
  Pointer(glTexStorageMem2DEXT):=_GetProcedureAddress('glTexStorageMem2DEXT');
  Pointer(glTexStorageMem2DMultisampleEXT):=_GetProcedureAddress('glTexStorageMem2DMultisampleEXT');
  Pointer(glTexStorageMem3DEXT):=_GetProcedureAddress('glTexStorageMem3DEXT');
  Pointer(glTexStorageMem3DMultisampleEXT):=_GetProcedureAddress('glTexStorageMem3DMultisampleEXT');
  Pointer(glTexSubImage1D):=_GetProcedureAddress('glTexSubImage1D');
  Pointer(glTexSubImage3DOES):=_GetProcedureAddress('glTexSubImage3DOES');
  Pointer(glTextureAttachMemoryNV):=_GetProcedureAddress('glTextureAttachMemoryNV');
  Pointer(glTextureFoveationParametersQCOM):=_GetProcedureAddress('glTextureFoveationParametersQCOM');
  Pointer(glTexturePageCommitmentMemNV):=_GetProcedureAddress('glTexturePageCommitmentMemNV');
  Pointer(glTextureStorage1DEXT):=_GetProcedureAddress('glTextureStorage1DEXT');
  Pointer(glTextureStorage2DEXT):=_GetProcedureAddress('glTextureStorage2DEXT');
  Pointer(glTextureStorage3DEXT):=_GetProcedureAddress('glTextureStorage3DEXT');
  Pointer(glTextureStorageMem1DEXT):=_GetProcedureAddress('glTextureStorageMem1DEXT');
  Pointer(glTextureStorageMem2DEXT):=_GetProcedureAddress('glTextureStorageMem2DEXT');
  Pointer(glTextureStorageMem2DMultisampleEXT):=_GetProcedureAddress('glTextureStorageMem2DMultisampleEXT');
  Pointer(glTextureStorageMem3DEXT):=_GetProcedureAddress('glTextureStorageMem3DEXT');
  Pointer(glTextureStorageMem3DMultisampleEXT):=_GetProcedureAddress('glTextureStorageMem3DMultisampleEXT');
  Pointer(glTextureView):=_GetProcedureAddress('glTextureView');
  Pointer(glTextureViewEXT):=_GetProcedureAddress('glTextureViewEXT');
  Pointer(glTextureViewOES):=_GetProcedureAddress('glTextureViewOES');
  Pointer(glTransformPathNV):=_GetProcedureAddress('glTransformPathNV');
  Pointer(glUniform1i64NV):=_GetProcedureAddress('glUniform1i64NV');
  Pointer(glUniform1i64vNV):=_GetProcedureAddress('glUniform1i64vNV');
  Pointer(glUniform1ui64NV):=_GetProcedureAddress('glUniform1ui64NV');
  Pointer(glUniform1ui64vNV):=_GetProcedureAddress('glUniform1ui64vNV');
  Pointer(glUniform2i64NV):=_GetProcedureAddress('glUniform2i64NV');
  Pointer(glUniform2i64vNV):=_GetProcedureAddress('glUniform2i64vNV');
  Pointer(glUniform2ui64NV):=_GetProcedureAddress('glUniform2ui64NV');
  Pointer(glUniform2ui64vNV):=_GetProcedureAddress('glUniform2ui64vNV');
  Pointer(glUniform3i64NV):=_GetProcedureAddress('glUniform3i64NV');
  Pointer(glUniform3i64vNV):=_GetProcedureAddress('glUniform3i64vNV');
  Pointer(glUniform3ui64NV):=_GetProcedureAddress('glUniform3ui64NV');
  Pointer(glUniform3ui64vNV):=_GetProcedureAddress('glUniform3ui64vNV');
  Pointer(glUniform4i64NV):=_GetProcedureAddress('glUniform4i64NV');
  Pointer(glUniform4i64vNV):=_GetProcedureAddress('glUniform4i64vNV');
  Pointer(glUniform4ui64NV):=_GetProcedureAddress('glUniform4ui64NV');
  Pointer(glUniform4ui64vNV):=_GetProcedureAddress('glUniform4ui64vNV');
  Pointer(glUniformHandleui64ARB):=_GetProcedureAddress('glUniformHandleui64ARB');
  Pointer(glUniformHandleui64IMG):=_GetProcedureAddress('glUniformHandleui64IMG');
  Pointer(glUniformHandleui64NV):=_GetProcedureAddress('glUniformHandleui64NV');
  Pointer(glUniformHandleui64vARB):=_GetProcedureAddress('glUniformHandleui64vARB');
  Pointer(glUniformHandleui64vIMG):=_GetProcedureAddress('glUniformHandleui64vIMG');
  Pointer(glUniformHandleui64vNV):=_GetProcedureAddress('glUniformHandleui64vNV');
  Pointer(glUniformMatrix2x3fvNV):=_GetProcedureAddress('glUniformMatrix2x3fvNV');
  Pointer(glUniformMatrix2x4fvNV):=_GetProcedureAddress('glUniformMatrix2x4fvNV');
  Pointer(glUniformMatrix3x2fvNV):=_GetProcedureAddress('glUniformMatrix3x2fvNV');
  Pointer(glUniformMatrix3x4fvNV):=_GetProcedureAddress('glUniformMatrix3x4fvNV');
  Pointer(glUniformMatrix4x2fvNV):=_GetProcedureAddress('glUniformMatrix4x2fvNV');
  Pointer(glUniformMatrix4x3fvNV):=_GetProcedureAddress('glUniformMatrix4x3fvNV');
  Pointer(glUnmapBufferOES):=_GetProcedureAddress('glUnmapBufferOES');
  Pointer(glUseProgramStagesEXT):=_GetProcedureAddress('glUseProgramStagesEXT');
  Pointer(glUseShaderProgramEXT):=_GetProcedureAddress('glUseShaderProgramEXT');
  Pointer(glValidateProgramPipelineEXT):=_GetProcedureAddress('glValidateProgramPipelineEXT');
  Pointer(glVertexAttrib1d):=_GetProcedureAddress('glVertexAttrib1d');
  Pointer(glVertexAttrib1dv):=_GetProcedureAddress('glVertexAttrib1dv');
  Pointer(glVertexAttrib1s):=_GetProcedureAddress('glVertexAttrib1s');
  Pointer(glVertexAttrib1sv):=_GetProcedureAddress('glVertexAttrib1sv');
  Pointer(glVertexAttrib2d):=_GetProcedureAddress('glVertexAttrib2d');
  Pointer(glVertexAttrib2dv):=_GetProcedureAddress('glVertexAttrib2dv');
  Pointer(glVertexAttrib2s):=_GetProcedureAddress('glVertexAttrib2s');
  Pointer(glVertexAttrib2sv):=_GetProcedureAddress('glVertexAttrib2sv');
  Pointer(glVertexAttrib3d):=_GetProcedureAddress('glVertexAttrib3d');
  Pointer(glVertexAttrib3dv):=_GetProcedureAddress('glVertexAttrib3dv');
  Pointer(glVertexAttrib3s):=_GetProcedureAddress('glVertexAttrib3s');
  Pointer(glVertexAttrib3sv):=_GetProcedureAddress('glVertexAttrib3sv');
  Pointer(glVertexAttrib4Nbv):=_GetProcedureAddress('glVertexAttrib4Nbv');
  Pointer(glVertexAttrib4Niv):=_GetProcedureAddress('glVertexAttrib4Niv');
  Pointer(glVertexAttrib4Nsv):=_GetProcedureAddress('glVertexAttrib4Nsv');
  Pointer(glVertexAttrib4Nub):=_GetProcedureAddress('glVertexAttrib4Nub');
  Pointer(glVertexAttrib4Nubv):=_GetProcedureAddress('glVertexAttrib4Nubv');
  Pointer(glVertexAttrib4Nuiv):=_GetProcedureAddress('glVertexAttrib4Nuiv');
  Pointer(glVertexAttrib4Nusv):=_GetProcedureAddress('glVertexAttrib4Nusv');
  Pointer(glVertexAttrib4bv):=_GetProcedureAddress('glVertexAttrib4bv');
  Pointer(glVertexAttrib4d):=_GetProcedureAddress('glVertexAttrib4d');
  Pointer(glVertexAttrib4dv):=_GetProcedureAddress('glVertexAttrib4dv');
  Pointer(glVertexAttrib4iv):=_GetProcedureAddress('glVertexAttrib4iv');
  Pointer(glVertexAttrib4s):=_GetProcedureAddress('glVertexAttrib4s');
  Pointer(glVertexAttrib4sv):=_GetProcedureAddress('glVertexAttrib4sv');
  Pointer(glVertexAttrib4ubv):=_GetProcedureAddress('glVertexAttrib4ubv');
  Pointer(glVertexAttrib4uiv):=_GetProcedureAddress('glVertexAttrib4uiv');
  Pointer(glVertexAttrib4usv):=_GetProcedureAddress('glVertexAttrib4usv');
  Pointer(glVertexAttribDivisorANGLE):=_GetProcedureAddress('glVertexAttribDivisorANGLE');
  Pointer(glVertexAttribDivisorEXT):=_GetProcedureAddress('glVertexAttribDivisorEXT');
  Pointer(glVertexAttribDivisorNV):=_GetProcedureAddress('glVertexAttribDivisorNV');
  Pointer(glVertexAttribI1i):=_GetProcedureAddress('glVertexAttribI1i');
  Pointer(glVertexAttribI1iv):=_GetProcedureAddress('glVertexAttribI1iv');
  Pointer(glVertexAttribI1ui):=_GetProcedureAddress('glVertexAttribI1ui');
  Pointer(glVertexAttribI1uiv):=_GetProcedureAddress('glVertexAttribI1uiv');
  Pointer(glVertexAttribI2i):=_GetProcedureAddress('glVertexAttribI2i');
  Pointer(glVertexAttribI2iv):=_GetProcedureAddress('glVertexAttribI2iv');
  Pointer(glVertexAttribI2ui):=_GetProcedureAddress('glVertexAttribI2ui');
  Pointer(glVertexAttribI2uiv):=_GetProcedureAddress('glVertexAttribI2uiv');
  Pointer(glVertexAttribI3i):=_GetProcedureAddress('glVertexAttribI3i');
  Pointer(glVertexAttribI3iv):=_GetProcedureAddress('glVertexAttribI3iv');
  Pointer(glVertexAttribI3ui):=_GetProcedureAddress('glVertexAttribI3ui');
  Pointer(glVertexAttribI3uiv):=_GetProcedureAddress('glVertexAttribI3uiv');
  Pointer(glVertexAttribI4bv):=_GetProcedureAddress('glVertexAttribI4bv');
  Pointer(glVertexAttribI4sv):=_GetProcedureAddress('glVertexAttribI4sv');
  Pointer(glVertexAttribI4ubv):=_GetProcedureAddress('glVertexAttribI4ubv');
  Pointer(glVertexAttribI4usv):=_GetProcedureAddress('glVertexAttribI4usv');
  Pointer(glVertexAttribL1d):=_GetProcedureAddress('glVertexAttribL1d');
  Pointer(glVertexAttribL1dv):=_GetProcedureAddress('glVertexAttribL1dv');
  Pointer(glVertexAttribL2d):=_GetProcedureAddress('glVertexAttribL2d');
  Pointer(glVertexAttribL2dv):=_GetProcedureAddress('glVertexAttribL2dv');
  Pointer(glVertexAttribL3d):=_GetProcedureAddress('glVertexAttribL3d');
  Pointer(glVertexAttribL3dv):=_GetProcedureAddress('glVertexAttribL3dv');
  Pointer(glVertexAttribL4d):=_GetProcedureAddress('glVertexAttribL4d');
  Pointer(glVertexAttribL4dv):=_GetProcedureAddress('glVertexAttribL4dv');
  Pointer(glVertexAttribLPointer):=_GetProcedureAddress('glVertexAttribLPointer');
  Pointer(glViewportArrayv):=_GetProcedureAddress('glViewportArrayv');
  Pointer(glViewportArrayvNV):=_GetProcedureAddress('glViewportArrayvNV');
  Pointer(glViewportArrayvOES):=_GetProcedureAddress('glViewportArrayvOES');
  Pointer(glViewportIndexedf):=_GetProcedureAddress('glViewportIndexedf');
  Pointer(glViewportIndexedfOES):=_GetProcedureAddress('glViewportIndexedfOES');
  Pointer(glViewportIndexedfNV):=_GetProcedureAddress('glViewportIndexedfNV');
  Pointer(glViewportIndexedfv):=_GetProcedureAddress('glViewportIndexedfv');
  Pointer(glViewportIndexedfvOES):=_GetProcedureAddress('glViewportIndexedfvOES');
  Pointer(glViewportIndexedfvNV):=_GetProcedureAddress('glViewportIndexedfvNV');
  Pointer(glViewportPositionWScaleNV):=_GetProcedureAddress('glViewportPositionWScaleNV');
  Pointer(glViewportSwizzleNV):=_GetProcedureAddress('glViewportSwizzleNV');
  Pointer(glWaitSemaphoreEXT):=_GetProcedureAddress('glWaitSemaphoreEXT');
  Pointer(glWaitSyncAPPLE):=_GetProcedureAddress('glWaitSyncAPPLE');
  Pointer(glWeightPathsNV):=_GetProcedureAddress('glWeightPathsNV');
  Pointer(glWindowPos2d):=_GetProcedureAddress('glWindowPos2d');
  Pointer(glWindowPos2dv):=_GetProcedureAddress('glWindowPos2dv');
  Pointer(glWindowPos2f):=_GetProcedureAddress('glWindowPos2f');
  Pointer(glWindowPos2fv):=_GetProcedureAddress('glWindowPos2fv');
  Pointer(glWindowPos2i):=_GetProcedureAddress('glWindowPos2i');
  Pointer(glWindowPos2iv):=_GetProcedureAddress('glWindowPos2iv');
  Pointer(glWindowPos2s):=_GetProcedureAddress('glWindowPos2s');
  Pointer(glWindowPos2sv):=_GetProcedureAddress('glWindowPos2sv');
  Pointer(glWindowPos3d):=_GetProcedureAddress('glWindowPos3d');
  Pointer(glWindowPos3dv):=_GetProcedureAddress('glWindowPos3dv');
  Pointer(glWindowPos3f):=_GetProcedureAddress('glWindowPos3f');
  Pointer(glWindowPos3fv):=_GetProcedureAddress('glWindowPos3fv');
  Pointer(glWindowPos3i):=_GetProcedureAddress('glWindowPos3i');
  Pointer(glWindowPos3iv):=_GetProcedureAddress('glWindowPos3iv');
  Pointer(glWindowPos3s):=_GetProcedureAddress('glWindowPos3s');
  Pointer(glWindowPos3sv):=_GetProcedureAddress('glWindowPos3sv');
  Pointer(glWindowRectanglesEXT):=_GetProcedureAddress('glWindowRectanglesEXT');
  Pointer(glDrawVkImageNV):=_GetProcedureAddress('glDrawVkImageNV');
  Pointer(glGetVkProcAddrNV):=_GetProcedureAddress('glGetVkProcAddrNV');
  Pointer(glWaitVkSemaphoreNV):=_GetProcedureAddress('glWaitVkSemaphoreNV');
  Pointer(glSignalVkSemaphoreNV):=_GetProcedureAddress('glSignalVkSemaphoreNV');
  Pointer(glSignalVkFenceNV):=_GetProcedureAddress('glSignalVkFenceNV');
  Pointer(glFramebufferParameteriMESA):=_GetProcedureAddress('glFramebufferParameteriMESA');
  Pointer(glGetFramebufferParameterivMESA):=_GetProcedureAddress('glGetFramebufferParameterivMESA');
 
//--- EGL -----
  Pointer(eglChooseConfig):=_GetProcedureAddress('eglChooseConfig');
  Pointer(eglCopyBuffers):=_GetProcedureAddress('eglCopyBuffers');
  Pointer(eglCreateContext):=_GetProcedureAddress('eglCreateContext');
  Pointer(eglCreatePbufferSurface):=_GetProcedureAddress('eglCreatePbufferSurface');
  Pointer(eglCreatePixmapSurface):=_GetProcedureAddress('eglCreatePixmapSurface');
  Pointer(eglCreateWindowSurface):=_GetProcedureAddress('eglCreateWindowSurface');
  Pointer(eglDestroyContext):=_GetProcedureAddress('eglDestroyContext');
  Pointer(eglDestroySurface):=_GetProcedureAddress('eglDestroySurface');
  Pointer(eglGetConfigAttrib):=_GetProcedureAddress('eglGetConfigAttrib');
  Pointer(eglGetConfigs):=_GetProcedureAddress('eglGetConfigs');
  Pointer(eglGetCurrentDisplay):=_GetProcedureAddress('eglGetCurrentDisplay');
  Pointer(eglGetCurrentSurface):=_GetProcedureAddress('eglGetCurrentSurface');
  Pointer(eglGetDisplay):=_GetProcedureAddress('eglGetDisplay');
  Pointer(eglGetError):=_GetProcedureAddress('eglGetError');
  Pointer(eglInitialize):=_GetProcedureAddress('eglInitialize');
  Pointer(eglMakeCurrent):=_GetProcedureAddress('eglMakeCurrent');
  Pointer(eglQueryContext):=_GetProcedureAddress('eglQueryContext');
  Pointer(eglQueryString):=_GetProcedureAddress('eglQueryString');
  Pointer(eglQuerySurface):=_GetProcedureAddress('eglQuerySurface');
  Pointer(eglSwapBuffers):=_GetProcedureAddress('eglSwapBuffers');
  Pointer(eglTerminate):=_GetProcedureAddress('eglTerminate');
  Pointer(eglWaitGL):=_GetProcedureAddress('eglWaitGL');
  Pointer(eglWaitNative):=_GetProcedureAddress('eglWaitNative');
  Pointer(eglBindTexImage):=_GetProcedureAddress('eglBindTexImage');
  Pointer(eglReleaseTexImage):=_GetProcedureAddress('eglReleaseTexImage');
  Pointer(eglSurfaceAttrib):=_GetProcedureAddress('eglSurfaceAttrib');
  Pointer(eglSwapInterval):=_GetProcedureAddress('eglSwapInterval');
  Pointer(eglBindAPI):=_GetProcedureAddress('eglBindAPI');
  Pointer(eglQueryAPI):=_GetProcedureAddress('eglQueryAPI');
  Pointer(eglCreatePbufferFromClientBuffer):=_GetProcedureAddress('eglCreatePbufferFromClientBuffer');
  Pointer(eglReleaseThread):=_GetProcedureAddress('eglReleaseThread');
  Pointer(eglWaitClient):=_GetProcedureAddress('eglWaitClient');
  Pointer(eglGetCurrentContext):=_GetProcedureAddress('eglGetCurrentContext');
  Pointer(eglCreateSync):=_GetProcedureAddress('eglCreateSync');
  Pointer(eglDestroySync):=_GetProcedureAddress('eglDestroySync');
  Pointer(eglClientWaitSync):=_GetProcedureAddress('eglClientWaitSync');
  Pointer(eglGetSyncAttrib):=_GetProcedureAddress('eglGetSyncAttrib');
  Pointer(eglCreateImage):=_GetProcedureAddress('eglCreateImage');
  Pointer(eglDestroyImage):=_GetProcedureAddress('eglDestroyImage');
  Pointer(eglGetPlatformDisplay):=_GetProcedureAddress('eglGetPlatformDisplay');
  Pointer(eglCreatePlatformWindowSurface):=_GetProcedureAddress('eglCreatePlatformWindowSurface');
  Pointer(eglCreatePlatformPixmapSurface):=_GetProcedureAddress('eglCreatePlatformPixmapSurface');
  Pointer(eglWaitSync):=_GetProcedureAddress('eglWaitSync');
  Pointer(eglClientSignalSyncEXT):=_GetProcedureAddress('eglClientSignalSyncEXT');
  Pointer(eglClientWaitSyncKHR):=_GetProcedureAddress('eglClientWaitSyncKHR');
  Pointer(eglClientWaitSyncNV):=_GetProcedureAddress('eglClientWaitSyncNV');
  Pointer(eglCreateDRMImageMESA):=_GetProcedureAddress('eglCreateDRMImageMESA');
  Pointer(eglCreateFenceSyncNV):=_GetProcedureAddress('eglCreateFenceSyncNV');
  Pointer(eglCreateImageKHR):=_GetProcedureAddress('eglCreateImageKHR');
  Pointer(eglCreateNativeClientBufferANDROID):=_GetProcedureAddress('eglCreateNativeClientBufferANDROID');
  Pointer(eglCreatePixmapSurfaceHI):=_GetProcedureAddress('eglCreatePixmapSurfaceHI');
  Pointer(eglCreatePlatformPixmapSurfaceEXT):=_GetProcedureAddress('eglCreatePlatformPixmapSurfaceEXT');
  Pointer(eglCreatePlatformWindowSurfaceEXT):=_GetProcedureAddress('eglCreatePlatformWindowSurfaceEXT');
  Pointer(eglCreateStreamFromFileDescriptorKHR):=_GetProcedureAddress('eglCreateStreamFromFileDescriptorKHR');
  Pointer(eglCreateStreamKHR):=_GetProcedureAddress('eglCreateStreamKHR');
  Pointer(eglCreateStreamAttribKHR):=_GetProcedureAddress('eglCreateStreamAttribKHR');
  Pointer(eglCreateStreamProducerSurfaceKHR):=_GetProcedureAddress('eglCreateStreamProducerSurfaceKHR');
  Pointer(eglCreateStreamSyncNV):=_GetProcedureAddress('eglCreateStreamSyncNV');
  Pointer(eglCreateSyncKHR):=_GetProcedureAddress('eglCreateSyncKHR');
  Pointer(eglCreateSync64KHR):=_GetProcedureAddress('eglCreateSync64KHR');
  Pointer(eglDebugMessageControlKHR):=_GetProcedureAddress('eglDebugMessageControlKHR');
  Pointer(eglDestroyImageKHR):=_GetProcedureAddress('eglDestroyImageKHR');
  Pointer(eglDestroyStreamKHR):=_GetProcedureAddress('eglDestroyStreamKHR');
  Pointer(eglDestroySyncKHR):=_GetProcedureAddress('eglDestroySyncKHR');
  Pointer(eglDestroySyncNV):=_GetProcedureAddress('eglDestroySyncNV');
  Pointer(eglDupNativeFenceFDANDROID):=_GetProcedureAddress('eglDupNativeFenceFDANDROID');
  Pointer(eglExportDMABUFImageMESA):=_GetProcedureAddress('eglExportDMABUFImageMESA');
  Pointer(eglExportDMABUFImageQueryMESA):=_GetProcedureAddress('eglExportDMABUFImageQueryMESA');
  Pointer(eglExportDRMImageMESA):=_GetProcedureAddress('eglExportDRMImageMESA');
  Pointer(eglFenceNV):=_GetProcedureAddress('eglFenceNV');
  Pointer(eglGetDisplayDriverConfig):=_GetProcedureAddress('eglGetDisplayDriverConfig');
  Pointer(eglGetDisplayDriverName):=_GetProcedureAddress('eglGetDisplayDriverName');
  Pointer(eglGetNativeClientBufferANDROID):=_GetProcedureAddress('eglGetNativeClientBufferANDROID');
  Pointer(eglGetOutputLayersEXT):=_GetProcedureAddress('eglGetOutputLayersEXT');
  Pointer(eglGetOutputPortsEXT):=_GetProcedureAddress('eglGetOutputPortsEXT');
  Pointer(eglGetPlatformDisplayEXT):=_GetProcedureAddress('eglGetPlatformDisplayEXT');
  Pointer(eglGetStreamFileDescriptorKHR):=_GetProcedureAddress('eglGetStreamFileDescriptorKHR');
  Pointer(eglGetSyncAttribKHR):=_GetProcedureAddress('eglGetSyncAttribKHR');
  Pointer(eglGetSyncAttribNV):=_GetProcedureAddress('eglGetSyncAttribNV');
  Pointer(eglGetSystemTimeFrequencyNV):=_GetProcedureAddress('eglGetSystemTimeFrequencyNV');
  Pointer(eglGetSystemTimeNV):=_GetProcedureAddress('eglGetSystemTimeNV');
  Pointer(eglLabelObjectKHR):=_GetProcedureAddress('eglLabelObjectKHR');
  Pointer(eglLockSurfaceKHR):=_GetProcedureAddress('eglLockSurfaceKHR');
  Pointer(eglOutputLayerAttribEXT):=_GetProcedureAddress('eglOutputLayerAttribEXT');
  Pointer(eglOutputPortAttribEXT):=_GetProcedureAddress('eglOutputPortAttribEXT');
  Pointer(eglPostSubBufferNV):=_GetProcedureAddress('eglPostSubBufferNV');
  Pointer(eglPresentationTimeANDROID):=_GetProcedureAddress('eglPresentationTimeANDROID');
  Pointer(eglGetCompositorTimingSupportedANDROID):=_GetProcedureAddress('eglGetCompositorTimingSupportedANDROID');
  Pointer(eglGetCompositorTimingANDROID):=_GetProcedureAddress('eglGetCompositorTimingANDROID');
  Pointer(eglGetNextFrameIdANDROID):=_GetProcedureAddress('eglGetNextFrameIdANDROID');
  Pointer(eglGetFrameTimestampSupportedANDROID):=_GetProcedureAddress('eglGetFrameTimestampSupportedANDROID');
  Pointer(eglGetFrameTimestampsANDROID):=_GetProcedureAddress('eglGetFrameTimestampsANDROID');
  Pointer(eglQueryDebugKHR):=_GetProcedureAddress('eglQueryDebugKHR');
  Pointer(eglQueryDeviceAttribEXT):=_GetProcedureAddress('eglQueryDeviceAttribEXT');
  Pointer(eglQueryDeviceStringEXT):=_GetProcedureAddress('eglQueryDeviceStringEXT');
  Pointer(eglQueryDevicesEXT):=_GetProcedureAddress('eglQueryDevicesEXT');
  Pointer(eglQueryDisplayAttribEXT):=_GetProcedureAddress('eglQueryDisplayAttribEXT');
  Pointer(eglQueryDisplayAttribKHR):=_GetProcedureAddress('eglQueryDisplayAttribKHR');
  Pointer(eglQueryDisplayAttribNV):=_GetProcedureAddress('eglQueryDisplayAttribNV');
  Pointer(eglQueryDmaBufFormatsEXT):=_GetProcedureAddress('eglQueryDmaBufFormatsEXT');
  Pointer(eglQueryDmaBufModifiersEXT):=_GetProcedureAddress('eglQueryDmaBufModifiersEXT');
  Pointer(eglQueryNativeDisplayNV):=_GetProcedureAddress('eglQueryNativeDisplayNV');
  Pointer(eglQueryNativePixmapNV):=_GetProcedureAddress('eglQueryNativePixmapNV');
  Pointer(eglQueryNativeWindowNV):=_GetProcedureAddress('eglQueryNativeWindowNV');
  Pointer(eglQueryOutputLayerAttribEXT):=_GetProcedureAddress('eglQueryOutputLayerAttribEXT');
  Pointer(eglQueryOutputLayerStringEXT):=_GetProcedureAddress('eglQueryOutputLayerStringEXT');
  Pointer(eglQueryOutputPortAttribEXT):=_GetProcedureAddress('eglQueryOutputPortAttribEXT');
  Pointer(eglQueryOutputPortStringEXT):=_GetProcedureAddress('eglQueryOutputPortStringEXT');
  Pointer(eglQueryStreamKHR):=_GetProcedureAddress('eglQueryStreamKHR');
  Pointer(eglQueryStreamAttribKHR):=_GetProcedureAddress('eglQueryStreamAttribKHR');
  Pointer(eglQueryStreamMetadataNV):=_GetProcedureAddress('eglQueryStreamMetadataNV');
  Pointer(eglQueryStreamTimeKHR):=_GetProcedureAddress('eglQueryStreamTimeKHR');
  Pointer(eglQueryStreamu64KHR):=_GetProcedureAddress('eglQueryStreamu64KHR');
  Pointer(eglQuerySurface64KHR):=_GetProcedureAddress('eglQuerySurface64KHR');
  Pointer(eglQuerySurfacePointerANGLE):=_GetProcedureAddress('eglQuerySurfacePointerANGLE');
  Pointer(eglResetStreamNV):=_GetProcedureAddress('eglResetStreamNV');
  Pointer(eglSetBlobCacheFuncsANDROID):=_GetProcedureAddress('eglSetBlobCacheFuncsANDROID');
  Pointer(eglSetDamageRegionKHR):=_GetProcedureAddress('eglSetDamageRegionKHR');
  Pointer(eglSetStreamAttribKHR):=_GetProcedureAddress('eglSetStreamAttribKHR');
  Pointer(eglSetStreamMetadataNV):=_GetProcedureAddress('eglSetStreamMetadataNV');
  Pointer(eglSignalSyncKHR):=_GetProcedureAddress('eglSignalSyncKHR');
  Pointer(eglSignalSyncNV):=_GetProcedureAddress('eglSignalSyncNV');
  Pointer(eglStreamAttribKHR):=_GetProcedureAddress('eglStreamAttribKHR');
  Pointer(eglStreamConsumerAcquireKHR):=_GetProcedureAddress('eglStreamConsumerAcquireKHR');
  Pointer(eglStreamConsumerAcquireAttribKHR):=_GetProcedureAddress('eglStreamConsumerAcquireAttribKHR');
  Pointer(eglStreamConsumerGLTextureExternalKHR):=_GetProcedureAddress('eglStreamConsumerGLTextureExternalKHR');
  Pointer(eglStreamConsumerGLTextureExternalAttribsNV):=_GetProcedureAddress('eglStreamConsumerGLTextureExternalAttribsNV');
  Pointer(eglStreamConsumerOutputEXT):=_GetProcedureAddress('eglStreamConsumerOutputEXT');
  Pointer(eglStreamConsumerReleaseKHR):=_GetProcedureAddress('eglStreamConsumerReleaseKHR');
  Pointer(eglStreamConsumerReleaseAttribKHR):=_GetProcedureAddress('eglStreamConsumerReleaseAttribKHR');
  Pointer(eglStreamFlushNV):=_GetProcedureAddress('eglStreamFlushNV');
  Pointer(eglSwapBuffersWithDamageEXT):=_GetProcedureAddress('eglSwapBuffersWithDamageEXT');
  Pointer(eglSwapBuffersWithDamageKHR):=_GetProcedureAddress('eglSwapBuffersWithDamageKHR');
  Pointer(eglSwapBuffersRegionNOK):=_GetProcedureAddress('eglSwapBuffersRegionNOK');
  Pointer(eglSwapBuffersRegion2NOK):=_GetProcedureAddress('eglSwapBuffersRegion2NOK');
  Pointer(eglUnlockSurfaceKHR):=_GetProcedureAddress('eglUnlockSurfaceKHR');
  Pointer(eglUnsignalSyncEXT):=_GetProcedureAddress('eglUnsignalSyncEXT');
  Pointer(eglWaitSyncKHR):=_GetProcedureAddress('eglWaitSyncKHR');
  Pointer(eglCompositorSetContextListEXT):=_GetProcedureAddress('eglCompositorSetContextListEXT');
  Pointer(eglCompositorSetContextAttributesEXT):=_GetProcedureAddress('eglCompositorSetContextAttributesEXT');
  Pointer(eglCompositorSetWindowListEXT):=_GetProcedureAddress('eglCompositorSetWindowListEXT');
  Pointer(eglCompositorSetWindowAttributesEXT):=_GetProcedureAddress('eglCompositorSetWindowAttributesEXT');
  Pointer(eglCompositorBindTexWindowEXT):=_GetProcedureAddress('eglCompositorBindTexWindowEXT');
  Pointer(eglCompositorSetSizeEXT):=_GetProcedureAddress('eglCompositorSetSizeEXT');
  Pointer(eglCompositorSwapPolicyEXT):=_GetProcedureAddress('eglCompositorSwapPolicyEXT');
  Pointer(eglBindWaylandDisplayWL):=_GetProcedureAddress('eglBindWaylandDisplayWL');
  Pointer(eglUnbindWaylandDisplayWL):=_GetProcedureAddress('eglUnbindWaylandDisplayWL');
  Pointer(eglQueryWaylandBufferWL):=_GetProcedureAddress('eglQueryWaylandBufferWL');
  Pointer(eglCreateWaylandBufferFromImageWL):=_GetProcedureAddress('eglCreateWaylandBufferFromImageWL');
  Pointer(eglStreamImageConsumerConnectNV):=_GetProcedureAddress('eglStreamImageConsumerConnectNV');
  Pointer(eglQueryStreamConsumerEventNV):=_GetProcedureAddress('eglQueryStreamConsumerEventNV');
  Pointer(eglStreamAcquireImageNV):=_GetProcedureAddress('eglStreamAcquireImageNV');
  Pointer(eglStreamReleaseImageNV):=_GetProcedureAddress('eglStreamReleaseImageNV');
 
end;
 
Procedure _Internal_LoadGLAliases; //---- Fill Missing Aliases -----------------
begin
 
  if not Assigned(glBeginConditionalRenderNV) then
  begin
    if Assigned(glBeginConditionalRender) then Pointer(glBeginConditionalRenderNV):=Pointer(glBeginConditionalRender);
  end else
  begin
    if not Assigned(glBeginConditionalRender) then Pointer(glBeginConditionalRender):=Pointer(glBeginConditionalRenderNV);
  end;
  if not Assigned(glBindFragDataLocationEXT) then
  begin
    if Assigned(glBindFragDataLocation) then Pointer(glBindFragDataLocationEXT):=Pointer(glBindFragDataLocation);
  end else
  begin
    if not Assigned(glBindFragDataLocation) then Pointer(glBindFragDataLocation):=Pointer(glBindFragDataLocationEXT);
  end;
  if not Assigned(glBindFragDataLocationIndexedEXT) then
  begin
    if Assigned(glBindFragDataLocationIndexed) then Pointer(glBindFragDataLocationIndexedEXT):=Pointer(glBindFragDataLocationIndexed);
  end else
  begin
    if not Assigned(glBindFragDataLocationIndexed) then Pointer(glBindFragDataLocationIndexed):=Pointer(glBindFragDataLocationIndexedEXT);
  end;
  if not Assigned(glBindVertexArrayOES) then
  begin
    if Assigned(glBindVertexArray) then Pointer(glBindVertexArrayOES):=Pointer(glBindVertexArray);
  end else
  begin
    if not Assigned(glBindVertexArray) then Pointer(glBindVertexArray):=Pointer(glBindVertexArrayOES);
  end;
  if not Assigned(glBlendBarrierKHR) then
  begin
    if Assigned(glBlendBarrier) then Pointer(glBlendBarrierKHR):=Pointer(glBlendBarrier);
  end else
  begin
    if not Assigned(glBlendBarrier) then Pointer(glBlendBarrier):=Pointer(glBlendBarrierKHR);
  end;
  if not Assigned(glBlendBarrierNV) then
  begin
    if Assigned(glBlendBarrier) then Pointer(glBlendBarrierNV):=Pointer(glBlendBarrier);
  end else
  begin
    if not Assigned(glBlendBarrier) then Pointer(glBlendBarrier):=Pointer(glBlendBarrierNV);
  end;
  if not Assigned(glBlendEquationEXT) then
  begin
    if Assigned(glBlendEquation) then Pointer(glBlendEquationEXT):=Pointer(glBlendEquation);
  end else
  begin
    if not Assigned(glBlendEquation) then Pointer(glBlendEquation):=Pointer(glBlendEquationEXT);
  end;
  if not Assigned(glBlendEquationSeparateiEXT) then
  begin
    if Assigned(glBlendEquationSeparatei) then Pointer(glBlendEquationSeparateiEXT):=Pointer(glBlendEquationSeparatei);
  end else
  begin
    if not Assigned(glBlendEquationSeparatei) then Pointer(glBlendEquationSeparatei):=Pointer(glBlendEquationSeparateiEXT);
  end;
  if not Assigned(glBlendEquationSeparateiOES) then
  begin
    if Assigned(glBlendEquationSeparatei) then Pointer(glBlendEquationSeparateiOES):=Pointer(glBlendEquationSeparatei);
  end else
  begin
    if not Assigned(glBlendEquationSeparatei) then Pointer(glBlendEquationSeparatei):=Pointer(glBlendEquationSeparateiOES);
  end;
  if not Assigned(glBlendEquationiEXT) then
  begin
    if Assigned(glBlendEquationi) then Pointer(glBlendEquationiEXT):=Pointer(glBlendEquationi);
  end else
  begin
    if not Assigned(glBlendEquationi) then Pointer(glBlendEquationi):=Pointer(glBlendEquationiEXT);
  end;
  if not Assigned(glBlendEquationiOES) then
  begin
    if Assigned(glBlendEquationi) then Pointer(glBlendEquationiOES):=Pointer(glBlendEquationi);
  end else
  begin
    if not Assigned(glBlendEquationi) then Pointer(glBlendEquationi):=Pointer(glBlendEquationiOES);
  end;
  if not Assigned(glBlendFuncSeparateiEXT) then
  begin
    if Assigned(glBlendFuncSeparatei) then Pointer(glBlendFuncSeparateiEXT):=Pointer(glBlendFuncSeparatei);
  end else
  begin
    if not Assigned(glBlendFuncSeparatei) then Pointer(glBlendFuncSeparatei):=Pointer(glBlendFuncSeparateiEXT);
  end;
  if not Assigned(glBlendFuncSeparateiOES) then
  begin
    if Assigned(glBlendFuncSeparatei) then Pointer(glBlendFuncSeparateiOES):=Pointer(glBlendFuncSeparatei);
  end else
  begin
    if not Assigned(glBlendFuncSeparatei) then Pointer(glBlendFuncSeparatei):=Pointer(glBlendFuncSeparateiOES);
  end;
  if not Assigned(glBlendFunciEXT) then
  begin
    if Assigned(glBlendFunci) then Pointer(glBlendFunciEXT):=Pointer(glBlendFunci);
  end else
  begin
    if not Assigned(glBlendFunci) then Pointer(glBlendFunci):=Pointer(glBlendFunciEXT);
  end;
  if not Assigned(glBlendFunciOES) then
  begin
    if Assigned(glBlendFunci) then Pointer(glBlendFunciOES):=Pointer(glBlendFunci);
  end else
  begin
    if not Assigned(glBlendFunci) then Pointer(glBlendFunci):=Pointer(glBlendFunciOES);
  end;
  if not Assigned(glBlitFramebufferNV) then
  begin
    if Assigned(glBlitFramebuffer) then Pointer(glBlitFramebufferNV):=Pointer(glBlitFramebuffer);
  end else
  begin
    if not Assigned(glBlitFramebuffer) then Pointer(glBlitFramebuffer):=Pointer(glBlitFramebufferNV);
  end;
  if not Assigned(glBufferStorageEXT) then
  begin
    if Assigned(glBufferStorage) then Pointer(glBufferStorageEXT):=Pointer(glBufferStorage);
  end else
  begin
    if not Assigned(glBufferStorage) then Pointer(glBufferStorage):=Pointer(glBufferStorageEXT);
  end;
  if not Assigned(glClearTexImageEXT) then
  begin
    if Assigned(glClearTexImage) then Pointer(glClearTexImageEXT):=Pointer(glClearTexImage);
  end else
  begin
    if not Assigned(glClearTexImage) then Pointer(glClearTexImage):=Pointer(glClearTexImageEXT);
  end;
  if not Assigned(glClearTexSubImageEXT) then
  begin
    if Assigned(glClearTexSubImage) then Pointer(glClearTexSubImageEXT):=Pointer(glClearTexSubImage);
  end else
  begin
    if not Assigned(glClearTexSubImage) then Pointer(glClearTexSubImage):=Pointer(glClearTexSubImageEXT);
  end;
  if not Assigned(glClientWaitSyncAPPLE) then
  begin
    if Assigned(glClientWaitSync) then Pointer(glClientWaitSyncAPPLE):=Pointer(glClientWaitSync);
  end else
  begin
    if not Assigned(glClientWaitSync) then Pointer(glClientWaitSync):=Pointer(glClientWaitSyncAPPLE);
  end;
  if not Assigned(glClipControlEXT) then
  begin
    if Assigned(glClipControl) then Pointer(glClipControlEXT):=Pointer(glClipControl);
  end else
  begin
    if not Assigned(glClipControl) then Pointer(glClipControl):=Pointer(glClipControlEXT);
  end;
  if not Assigned(glColorMaskiEXT) then
  begin
    if Assigned(glColorMaski) then Pointer(glColorMaskiEXT):=Pointer(glColorMaski);
  end else
  begin
    if not Assigned(glColorMaski) then Pointer(glColorMaski):=Pointer(glColorMaskiEXT);
  end;
  if not Assigned(glColorMaskiOES) then
  begin
    if Assigned(glColorMaski) then Pointer(glColorMaskiOES):=Pointer(glColorMaski);
  end else
  begin
    if not Assigned(glColorMaski) then Pointer(glColorMaski):=Pointer(glColorMaskiOES);
  end;
  if not Assigned(glCopyBufferSubDataNV) then
  begin
    if Assigned(glCopyBufferSubData) then Pointer(glCopyBufferSubDataNV):=Pointer(glCopyBufferSubData);
  end else
  begin
    if not Assigned(glCopyBufferSubData) then Pointer(glCopyBufferSubData):=Pointer(glCopyBufferSubDataNV);
  end;
  if not Assigned(glCopyImageSubDataEXT) then
  begin
    if Assigned(glCopyImageSubData) then Pointer(glCopyImageSubDataEXT):=Pointer(glCopyImageSubData);
  end else
  begin
    if not Assigned(glCopyImageSubData) then Pointer(glCopyImageSubData):=Pointer(glCopyImageSubDataEXT);
  end;
  if not Assigned(glCopyImageSubDataOES) then
  begin
    if Assigned(glCopyImageSubData) then Pointer(glCopyImageSubDataOES):=Pointer(glCopyImageSubData);
  end else
  begin
    if not Assigned(glCopyImageSubData) then Pointer(glCopyImageSubData):=Pointer(glCopyImageSubDataOES);
  end;
  if not Assigned(glDebugMessageCallbackKHR) then
  begin
    if Assigned(glDebugMessageCallback) then Pointer(glDebugMessageCallbackKHR):=Pointer(glDebugMessageCallback);
  end else
  begin
    if not Assigned(glDebugMessageCallback) then Pointer(glDebugMessageCallback):=Pointer(glDebugMessageCallbackKHR);
  end;
  if not Assigned(glDebugMessageControlKHR) then
  begin
    if Assigned(glDebugMessageControl) then Pointer(glDebugMessageControlKHR):=Pointer(glDebugMessageControl);
  end else
  begin
    if not Assigned(glDebugMessageControl) then Pointer(glDebugMessageControl):=Pointer(glDebugMessageControlKHR);
  end;
  if not Assigned(glDebugMessageInsertKHR) then
  begin
    if Assigned(glDebugMessageInsert) then Pointer(glDebugMessageInsertKHR):=Pointer(glDebugMessageInsert);
  end else
  begin
    if not Assigned(glDebugMessageInsert) then Pointer(glDebugMessageInsert):=Pointer(glDebugMessageInsertKHR);
  end;
  if not Assigned(glDeleteSyncAPPLE) then
  begin
    if Assigned(glDeleteSync) then Pointer(glDeleteSyncAPPLE):=Pointer(glDeleteSync);
  end else
  begin
    if not Assigned(glDeleteSync) then Pointer(glDeleteSync):=Pointer(glDeleteSyncAPPLE);
  end;
  if not Assigned(glDeleteVertexArraysOES) then
  begin
    if Assigned(glDeleteVertexArrays) then Pointer(glDeleteVertexArraysOES):=Pointer(glDeleteVertexArrays);
  end else
  begin
    if not Assigned(glDeleteVertexArrays) then Pointer(glDeleteVertexArrays):=Pointer(glDeleteVertexArraysOES);
  end;
  if not Assigned(glDisableiEXT) then
  begin
    if Assigned(glDisablei) then Pointer(glDisableiEXT):=Pointer(glDisablei);
  end else
  begin
    if not Assigned(glDisablei) then Pointer(glDisablei):=Pointer(glDisableiEXT);
  end;
  if not Assigned(glDisableiNV) then
  begin
    if Assigned(glDisablei) then Pointer(glDisableiNV):=Pointer(glDisablei);
  end else
  begin
    if not Assigned(glDisablei) then Pointer(glDisablei):=Pointer(glDisableiNV);
  end;
  if not Assigned(glDisableiOES) then
  begin
    if Assigned(glDisablei) then Pointer(glDisableiOES):=Pointer(glDisablei);
  end else
  begin
    if not Assigned(glDisablei) then Pointer(glDisablei):=Pointer(glDisableiOES);
  end;
  if not Assigned(glDrawArraysInstancedANGLE) then
  begin
    if Assigned(glDrawArraysInstanced) then Pointer(glDrawArraysInstancedANGLE):=Pointer(glDrawArraysInstanced);
  end else
  begin
    if not Assigned(glDrawArraysInstanced) then Pointer(glDrawArraysInstanced):=Pointer(glDrawArraysInstancedANGLE);
  end;
  if not Assigned(glDrawArraysInstancedBaseInstanceEXT) then
  begin
    if Assigned(glDrawArraysInstancedBaseInstance) then Pointer(glDrawArraysInstancedBaseInstanceEXT):=Pointer(glDrawArraysInstancedBaseInstance);
  end else
  begin
    if not Assigned(glDrawArraysInstancedBaseInstance) then Pointer(glDrawArraysInstancedBaseInstance):=Pointer(glDrawArraysInstancedBaseInstanceEXT);
  end;
  if not Assigned(glDrawArraysInstancedEXT) then
  begin
    if Assigned(glDrawArraysInstanced) then Pointer(glDrawArraysInstancedEXT):=Pointer(glDrawArraysInstanced);
  end else
  begin
    if not Assigned(glDrawArraysInstanced) then Pointer(glDrawArraysInstanced):=Pointer(glDrawArraysInstancedEXT);
  end;
  if not Assigned(glDrawArraysInstancedNV) then
  begin
    if Assigned(glDrawArraysInstanced) then Pointer(glDrawArraysInstancedNV):=Pointer(glDrawArraysInstanced);
  end else
  begin
    if not Assigned(glDrawArraysInstanced) then Pointer(glDrawArraysInstanced):=Pointer(glDrawArraysInstancedNV);
  end;
  if not Assigned(glDrawBuffersEXT) then
  begin
    if Assigned(glDrawBuffers) then Pointer(glDrawBuffersEXT):=Pointer(glDrawBuffers);
  end else
  begin
    if not Assigned(glDrawBuffers) then Pointer(glDrawBuffers):=Pointer(glDrawBuffersEXT);
  end;
  if not Assigned(glDrawElementsBaseVertexEXT) then
  begin
    if Assigned(glDrawElementsBaseVertex) then Pointer(glDrawElementsBaseVertexEXT):=Pointer(glDrawElementsBaseVertex);
  end else
  begin
    if not Assigned(glDrawElementsBaseVertex) then Pointer(glDrawElementsBaseVertex):=Pointer(glDrawElementsBaseVertexEXT);
  end;
  if not Assigned(glDrawElementsBaseVertexOES) then
  begin
    if Assigned(glDrawElementsBaseVertex) then Pointer(glDrawElementsBaseVertexOES):=Pointer(glDrawElementsBaseVertex);
  end else
  begin
    if not Assigned(glDrawElementsBaseVertex) then Pointer(glDrawElementsBaseVertex):=Pointer(glDrawElementsBaseVertexOES);
  end;
  if not Assigned(glDrawElementsInstancedANGLE) then
  begin
    if Assigned(glDrawElementsInstanced) then Pointer(glDrawElementsInstancedANGLE):=Pointer(glDrawElementsInstanced);
  end else
  begin
    if not Assigned(glDrawElementsInstanced) then Pointer(glDrawElementsInstanced):=Pointer(glDrawElementsInstancedANGLE);
  end;
  if not Assigned(glDrawElementsInstancedBaseInstanceEXT) then
  begin
    if Assigned(glDrawElementsInstancedBaseInstance) then Pointer(glDrawElementsInstancedBaseInstanceEXT):=Pointer(glDrawElementsInstancedBaseInstance);
  end else
  begin
    if not Assigned(glDrawElementsInstancedBaseInstance) then Pointer(glDrawElementsInstancedBaseInstance):=Pointer(glDrawElementsInstancedBaseInstanceEXT);
  end;
  if not Assigned(glDrawElementsInstancedBaseVertexBaseInstanceEXT) then
  begin
    if Assigned(glDrawElementsInstancedBaseVertexBaseInstance) then Pointer(glDrawElementsInstancedBaseVertexBaseInstanceEXT):=Pointer(glDrawElementsInstancedBaseVertexBaseInstance);
  end else
  begin
    if not Assigned(glDrawElementsInstancedBaseVertexBaseInstance) then Pointer(glDrawElementsInstancedBaseVertexBaseInstance):=Pointer(glDrawElementsInstancedBaseVertexBaseInstanceEXT);
  end;
  if not Assigned(glDrawElementsInstancedBaseVertexEXT) then
  begin
    if Assigned(glDrawElementsInstancedBaseVertex) then Pointer(glDrawElementsInstancedBaseVertexEXT):=Pointer(glDrawElementsInstancedBaseVertex);
  end else
  begin
    if not Assigned(glDrawElementsInstancedBaseVertex) then Pointer(glDrawElementsInstancedBaseVertex):=Pointer(glDrawElementsInstancedBaseVertexEXT);
  end;
  if not Assigned(glDrawElementsInstancedBaseVertexOES) then
  begin
    if Assigned(glDrawElementsInstancedBaseVertex) then Pointer(glDrawElementsInstancedBaseVertexOES):=Pointer(glDrawElementsInstancedBaseVertex);
  end else
  begin
    if not Assigned(glDrawElementsInstancedBaseVertex) then Pointer(glDrawElementsInstancedBaseVertex):=Pointer(glDrawElementsInstancedBaseVertexOES);
  end;
  if not Assigned(glDrawElementsInstancedEXT) then
  begin
    if Assigned(glDrawElementsInstanced) then Pointer(glDrawElementsInstancedEXT):=Pointer(glDrawElementsInstanced);
  end else
  begin
    if not Assigned(glDrawElementsInstanced) then Pointer(glDrawElementsInstanced):=Pointer(glDrawElementsInstancedEXT);
  end;
  if not Assigned(glDrawElementsInstancedNV) then
  begin
    if Assigned(glDrawElementsInstanced) then Pointer(glDrawElementsInstancedNV):=Pointer(glDrawElementsInstanced);
  end else
  begin
    if not Assigned(glDrawElementsInstanced) then Pointer(glDrawElementsInstanced):=Pointer(glDrawElementsInstancedNV);
  end;
  if not Assigned(glDrawRangeElementsBaseVertexEXT) then
  begin
    if Assigned(glDrawRangeElementsBaseVertex) then Pointer(glDrawRangeElementsBaseVertexEXT):=Pointer(glDrawRangeElementsBaseVertex);
  end else
  begin
    if not Assigned(glDrawRangeElementsBaseVertex) then Pointer(glDrawRangeElementsBaseVertex):=Pointer(glDrawRangeElementsBaseVertexEXT);
  end;
  if not Assigned(glDrawRangeElementsBaseVertexOES) then
  begin
    if Assigned(glDrawRangeElementsBaseVertex) then Pointer(glDrawRangeElementsBaseVertexOES):=Pointer(glDrawRangeElementsBaseVertex);
  end else
  begin
    if not Assigned(glDrawRangeElementsBaseVertex) then Pointer(glDrawRangeElementsBaseVertex):=Pointer(glDrawRangeElementsBaseVertexOES);
  end;
  if not Assigned(glDrawTransformFeedbackEXT) then
  begin
    if Assigned(glDrawTransformFeedback) then Pointer(glDrawTransformFeedbackEXT):=Pointer(glDrawTransformFeedback);
  end else
  begin
    if not Assigned(glDrawTransformFeedback) then Pointer(glDrawTransformFeedback):=Pointer(glDrawTransformFeedbackEXT);
  end;
  if not Assigned(glDrawTransformFeedbackInstancedEXT) then
  begin
    if Assigned(glDrawTransformFeedbackInstanced) then Pointer(glDrawTransformFeedbackInstancedEXT):=Pointer(glDrawTransformFeedbackInstanced);
  end else
  begin
    if not Assigned(glDrawTransformFeedbackInstanced) then Pointer(glDrawTransformFeedbackInstanced):=Pointer(glDrawTransformFeedbackInstancedEXT);
  end;
  if not Assigned(glEnableiEXT) then
  begin
    if Assigned(glEnablei) then Pointer(glEnableiEXT):=Pointer(glEnablei);
  end else
  begin
    if not Assigned(glEnablei) then Pointer(glEnablei):=Pointer(glEnableiEXT);
  end;
  if not Assigned(glEnableiNV) then
  begin
    if Assigned(glEnablei) then Pointer(glEnableiNV):=Pointer(glEnablei);
  end else
  begin
    if not Assigned(glEnablei) then Pointer(glEnablei):=Pointer(glEnableiNV);
  end;
  if not Assigned(glEnableiOES) then
  begin
    if Assigned(glEnablei) then Pointer(glEnableiOES):=Pointer(glEnablei);
  end else
  begin
    if not Assigned(glEnablei) then Pointer(glEnablei):=Pointer(glEnableiOES);
  end;
  if not Assigned(glEndConditionalRenderNV) then
  begin
    if Assigned(glEndConditionalRender) then Pointer(glEndConditionalRenderNV):=Pointer(glEndConditionalRender);
  end else
  begin
    if not Assigned(glEndConditionalRender) then Pointer(glEndConditionalRender):=Pointer(glEndConditionalRenderNV);
  end;
  if not Assigned(glFenceSyncAPPLE) then
  begin
    if Assigned(glFenceSync) then Pointer(glFenceSyncAPPLE):=Pointer(glFenceSync);
  end else
  begin
    if not Assigned(glFenceSync) then Pointer(glFenceSync):=Pointer(glFenceSyncAPPLE);
  end;
  if not Assigned(glFlushMappedBufferRangeEXT) then
  begin
    if Assigned(glFlushMappedBufferRange) then Pointer(glFlushMappedBufferRangeEXT):=Pointer(glFlushMappedBufferRange);
  end else
  begin
    if not Assigned(glFlushMappedBufferRange) then Pointer(glFlushMappedBufferRange):=Pointer(glFlushMappedBufferRangeEXT);
  end;
  if not Assigned(glFramebufferTextureEXT) then
  begin
    if Assigned(glFramebufferTexture) then Pointer(glFramebufferTextureEXT):=Pointer(glFramebufferTexture);
  end else
  begin
    if not Assigned(glFramebufferTexture) then Pointer(glFramebufferTexture):=Pointer(glFramebufferTextureEXT);
  end;
  if not Assigned(glFramebufferTextureOES) then
  begin
    if Assigned(glFramebufferTexture) then Pointer(glFramebufferTextureOES):=Pointer(glFramebufferTexture);
  end else
  begin
    if not Assigned(glFramebufferTexture) then Pointer(glFramebufferTexture):=Pointer(glFramebufferTextureOES);
  end;
  if not Assigned(glGenVertexArraysOES) then
  begin
    if Assigned(glGenVertexArrays) then Pointer(glGenVertexArraysOES):=Pointer(glGenVertexArrays);
  end else
  begin
    if not Assigned(glGenVertexArrays) then Pointer(glGenVertexArrays):=Pointer(glGenVertexArraysOES);
  end;
  if not Assigned(glGetBufferPointervOES) then
  begin
    if Assigned(glGetBufferPointerv) then Pointer(glGetBufferPointervOES):=Pointer(glGetBufferPointerv);
  end else
  begin
    if not Assigned(glGetBufferPointerv) then Pointer(glGetBufferPointerv):=Pointer(glGetBufferPointervOES);
  end;
  if not Assigned(glGetDebugMessageLogKHR) then
  begin
    if Assigned(glGetDebugMessageLog) then Pointer(glGetDebugMessageLogKHR):=Pointer(glGetDebugMessageLog);
  end else
  begin
    if not Assigned(glGetDebugMessageLog) then Pointer(glGetDebugMessageLog):=Pointer(glGetDebugMessageLogKHR);
  end;
  if not Assigned(glGetFloati_vNV) then
  begin
    if Assigned(glGetFloati_v) then Pointer(glGetFloati_vNV):=Pointer(glGetFloati_v);
  end else
  begin
    if not Assigned(glGetFloati_v) then Pointer(glGetFloati_v):=Pointer(glGetFloati_vNV);
  end;
  if not Assigned(glGetFloati_vOES) then
  begin
    if Assigned(glGetFloati_v) then Pointer(glGetFloati_vOES):=Pointer(glGetFloati_v);
  end else
  begin
    if not Assigned(glGetFloati_v) then Pointer(glGetFloati_v):=Pointer(glGetFloati_vOES);
  end;
  if not Assigned(glGetFragDataIndexEXT) then
  begin
    if Assigned(glGetFragDataIndex) then Pointer(glGetFragDataIndexEXT):=Pointer(glGetFragDataIndex);
  end else
  begin
    if not Assigned(glGetFragDataIndex) then Pointer(glGetFragDataIndex):=Pointer(glGetFragDataIndexEXT);
  end;
  if not Assigned(glGetGraphicsResetStatusEXT) then
  begin
    if Assigned(glGetGraphicsResetStatus) then Pointer(glGetGraphicsResetStatusEXT):=Pointer(glGetGraphicsResetStatus);
  end else
  begin
    if not Assigned(glGetGraphicsResetStatus) then Pointer(glGetGraphicsResetStatus):=Pointer(glGetGraphicsResetStatusEXT);
  end;
  if not Assigned(glGetGraphicsResetStatusKHR) then
  begin
    if Assigned(glGetGraphicsResetStatus) then Pointer(glGetGraphicsResetStatusKHR):=Pointer(glGetGraphicsResetStatus);
  end else
  begin
    if not Assigned(glGetGraphicsResetStatus) then Pointer(glGetGraphicsResetStatus):=Pointer(glGetGraphicsResetStatusKHR);
  end;
  if not Assigned(glGetInteger64vAPPLE) then
  begin
    if Assigned(glGetInteger64v) then Pointer(glGetInteger64vAPPLE):=Pointer(glGetInteger64v);
  end else
  begin
    if not Assigned(glGetInteger64v) then Pointer(glGetInteger64v):=Pointer(glGetInteger64vAPPLE);
  end;
  if not Assigned(glGetInteger64vEXT) then
  begin
    if Assigned(glGetInteger64v) then Pointer(glGetInteger64vEXT):=Pointer(glGetInteger64v);
  end else
  begin
    if not Assigned(glGetInteger64v) then Pointer(glGetInteger64v):=Pointer(glGetInteger64vEXT);
  end;
  if not Assigned(glGetObjectLabelKHR) then
  begin
    if Assigned(glGetObjectLabel) then Pointer(glGetObjectLabelKHR):=Pointer(glGetObjectLabel);
  end else
  begin
    if not Assigned(glGetObjectLabel) then Pointer(glGetObjectLabel):=Pointer(glGetObjectLabelKHR);
  end;
  if not Assigned(glGetObjectPtrLabelKHR) then
  begin
    if Assigned(glGetObjectPtrLabel) then Pointer(glGetObjectPtrLabelKHR):=Pointer(glGetObjectPtrLabel);
  end else
  begin
    if not Assigned(glGetObjectPtrLabel) then Pointer(glGetObjectPtrLabel):=Pointer(glGetObjectPtrLabelKHR);
  end;
  if not Assigned(glGetPointervKHR) then
  begin
    if Assigned(glGetPointerv) then Pointer(glGetPointervKHR):=Pointer(glGetPointerv);
  end else
  begin
    if not Assigned(glGetPointerv) then Pointer(glGetPointerv):=Pointer(glGetPointervKHR);
  end;
  if not Assigned(glGetProgramBinaryOES) then
  begin
    if Assigned(glGetProgramBinary) then Pointer(glGetProgramBinaryOES):=Pointer(glGetProgramBinary);
  end else
  begin
    if not Assigned(glGetProgramBinary) then Pointer(glGetProgramBinary):=Pointer(glGetProgramBinaryOES);
  end;
  if not Assigned(glGetQueryObjecti64vEXT) then
  begin
    if Assigned(glGetQueryObjecti64v) then Pointer(glGetQueryObjecti64vEXT):=Pointer(glGetQueryObjecti64v);
  end else
  begin
    if not Assigned(glGetQueryObjecti64v) then Pointer(glGetQueryObjecti64v):=Pointer(glGetQueryObjecti64vEXT);
  end;
  if not Assigned(glGetQueryObjectivEXT) then
  begin
    if Assigned(glGetQueryObjectiv) then Pointer(glGetQueryObjectivEXT):=Pointer(glGetQueryObjectiv);
  end else
  begin
    if not Assigned(glGetQueryObjectiv) then Pointer(glGetQueryObjectiv):=Pointer(glGetQueryObjectivEXT);
  end;
  if not Assigned(glGetQueryObjectui64vEXT) then
  begin
    if Assigned(glGetQueryObjectui64v) then Pointer(glGetQueryObjectui64vEXT):=Pointer(glGetQueryObjectui64v);
  end else
  begin
    if not Assigned(glGetQueryObjectui64v) then Pointer(glGetQueryObjectui64v):=Pointer(glGetQueryObjectui64vEXT);
  end;
  if not Assigned(glGetSamplerParameterIivEXT) then
  begin
    if Assigned(glGetSamplerParameterIiv) then Pointer(glGetSamplerParameterIivEXT):=Pointer(glGetSamplerParameterIiv);
  end else
  begin
    if not Assigned(glGetSamplerParameterIiv) then Pointer(glGetSamplerParameterIiv):=Pointer(glGetSamplerParameterIivEXT);
  end;
  if not Assigned(glGetSamplerParameterIivOES) then
  begin
    if Assigned(glGetSamplerParameterIiv) then Pointer(glGetSamplerParameterIivOES):=Pointer(glGetSamplerParameterIiv);
  end else
  begin
    if not Assigned(glGetSamplerParameterIiv) then Pointer(glGetSamplerParameterIiv):=Pointer(glGetSamplerParameterIivOES);
  end;
  if not Assigned(glGetSamplerParameterIuivEXT) then
  begin
    if Assigned(glGetSamplerParameterIuiv) then Pointer(glGetSamplerParameterIuivEXT):=Pointer(glGetSamplerParameterIuiv);
  end else
  begin
    if not Assigned(glGetSamplerParameterIuiv) then Pointer(glGetSamplerParameterIuiv):=Pointer(glGetSamplerParameterIuivEXT);
  end;
  if not Assigned(glGetSamplerParameterIuivOES) then
  begin
    if Assigned(glGetSamplerParameterIuiv) then Pointer(glGetSamplerParameterIuivOES):=Pointer(glGetSamplerParameterIuiv);
  end else
  begin
    if not Assigned(glGetSamplerParameterIuiv) then Pointer(glGetSamplerParameterIuiv):=Pointer(glGetSamplerParameterIuivOES);
  end;
  if not Assigned(glGetSyncivAPPLE) then
  begin
    if Assigned(glGetSynciv) then Pointer(glGetSyncivAPPLE):=Pointer(glGetSynciv);
  end else
  begin
    if not Assigned(glGetSynciv) then Pointer(glGetSynciv):=Pointer(glGetSyncivAPPLE);
  end;
  if not Assigned(glGetTexParameterIivEXT) then
  begin
    if Assigned(glGetTexParameterIiv) then Pointer(glGetTexParameterIivEXT):=Pointer(glGetTexParameterIiv);
  end else
  begin
    if not Assigned(glGetTexParameterIiv) then Pointer(glGetTexParameterIiv):=Pointer(glGetTexParameterIivEXT);
  end;
  if not Assigned(glGetTexParameterIivOES) then
  begin
    if Assigned(glGetTexParameterIiv) then Pointer(glGetTexParameterIivOES):=Pointer(glGetTexParameterIiv);
  end else
  begin
    if not Assigned(glGetTexParameterIiv) then Pointer(glGetTexParameterIiv):=Pointer(glGetTexParameterIivOES);
  end;
  if not Assigned(glGetTexParameterIuivEXT) then
  begin
    if Assigned(glGetTexParameterIuiv) then Pointer(glGetTexParameterIuivEXT):=Pointer(glGetTexParameterIuiv);
  end else
  begin
    if not Assigned(glGetTexParameterIuiv) then Pointer(glGetTexParameterIuiv):=Pointer(glGetTexParameterIuivEXT);
  end;
  if not Assigned(glGetTexParameterIuivOES) then
  begin
    if Assigned(glGetTexParameterIuiv) then Pointer(glGetTexParameterIuivOES):=Pointer(glGetTexParameterIuiv);
  end else
  begin
    if not Assigned(glGetTexParameterIuiv) then Pointer(glGetTexParameterIuiv):=Pointer(glGetTexParameterIuivOES);
  end;
  if not Assigned(glGetTextureHandleIMG) then
  begin
    if Assigned(glGetTextureHandleARB) then Pointer(glGetTextureHandleIMG):=Pointer(glGetTextureHandleARB);
  end else
  begin
    if not Assigned(glGetTextureHandleARB) then Pointer(glGetTextureHandleARB):=Pointer(glGetTextureHandleIMG);
  end;
  if not Assigned(glGetTextureSamplerHandleIMG) then
  begin
    if Assigned(glGetTextureSamplerHandleARB) then Pointer(glGetTextureSamplerHandleIMG):=Pointer(glGetTextureSamplerHandleARB);
  end else
  begin
    if not Assigned(glGetTextureSamplerHandleARB) then Pointer(glGetTextureSamplerHandleARB):=Pointer(glGetTextureSamplerHandleIMG);
  end;
  if not Assigned(glGetnUniformfvEXT) then
  begin
    if Assigned(glGetnUniformfv) then Pointer(glGetnUniformfvEXT):=Pointer(glGetnUniformfv);
  end else
  begin
    if not Assigned(glGetnUniformfv) then Pointer(glGetnUniformfv):=Pointer(glGetnUniformfvEXT);
  end;
  if not Assigned(glGetnUniformfvKHR) then
  begin
    if Assigned(glGetnUniformfv) then Pointer(glGetnUniformfvKHR):=Pointer(glGetnUniformfv);
  end else
  begin
    if not Assigned(glGetnUniformfv) then Pointer(glGetnUniformfv):=Pointer(glGetnUniformfvKHR);
  end;
  if not Assigned(glGetnUniformivEXT) then
  begin
    if Assigned(glGetnUniformiv) then Pointer(glGetnUniformivEXT):=Pointer(glGetnUniformiv);
  end else
  begin
    if not Assigned(glGetnUniformiv) then Pointer(glGetnUniformiv):=Pointer(glGetnUniformivEXT);
  end;
  if not Assigned(glGetnUniformivKHR) then
  begin
    if Assigned(glGetnUniformiv) then Pointer(glGetnUniformivKHR):=Pointer(glGetnUniformiv);
  end else
  begin
    if not Assigned(glGetnUniformiv) then Pointer(glGetnUniformiv):=Pointer(glGetnUniformivKHR);
  end;
  if not Assigned(glGetnUniformuivKHR) then
  begin
    if Assigned(glGetnUniformuiv) then Pointer(glGetnUniformuivKHR):=Pointer(glGetnUniformuiv);
  end else
  begin
    if not Assigned(glGetnUniformuiv) then Pointer(glGetnUniformuiv):=Pointer(glGetnUniformuivKHR);
  end;
  if not Assigned(glIsEnablediEXT) then
  begin
    if Assigned(glIsEnabledi) then Pointer(glIsEnablediEXT):=Pointer(glIsEnabledi);
  end else
  begin
    if not Assigned(glIsEnabledi) then Pointer(glIsEnabledi):=Pointer(glIsEnablediEXT);
  end;
  if not Assigned(glIsEnablediNV) then
  begin
    if Assigned(glIsEnabledi) then Pointer(glIsEnablediNV):=Pointer(glIsEnabledi);
  end else
  begin
    if not Assigned(glIsEnabledi) then Pointer(glIsEnabledi):=Pointer(glIsEnablediNV);
  end;
  if not Assigned(glIsEnablediOES) then
  begin
    if Assigned(glIsEnabledi) then Pointer(glIsEnablediOES):=Pointer(glIsEnabledi);
  end else
  begin
    if not Assigned(glIsEnabledi) then Pointer(glIsEnabledi):=Pointer(glIsEnablediOES);
  end;
  if not Assigned(glIsSyncAPPLE) then
  begin
    if Assigned(glIsSync) then Pointer(glIsSyncAPPLE):=Pointer(glIsSync);
  end else
  begin
    if not Assigned(glIsSync) then Pointer(glIsSync):=Pointer(glIsSyncAPPLE);
  end;
  if not Assigned(glIsVertexArrayOES) then
  begin
    if Assigned(glIsVertexArray) then Pointer(glIsVertexArrayOES):=Pointer(glIsVertexArray);
  end else
  begin
    if not Assigned(glIsVertexArray) then Pointer(glIsVertexArray):=Pointer(glIsVertexArrayOES);
  end;
  if not Assigned(glMapBufferOES) then
  begin
    if Assigned(glMapBuffer) then Pointer(glMapBufferOES):=Pointer(glMapBuffer);
  end else
  begin
    if not Assigned(glMapBuffer) then Pointer(glMapBuffer):=Pointer(glMapBufferOES);
  end;
  if not Assigned(glMapBufferRangeEXT) then
  begin
    if Assigned(glMapBufferRange) then Pointer(glMapBufferRangeEXT):=Pointer(glMapBufferRange);
  end else
  begin
    if not Assigned(glMapBufferRange) then Pointer(glMapBufferRange):=Pointer(glMapBufferRangeEXT);
  end;
  if not Assigned(glMinSampleShadingOES) then
  begin
    if Assigned(glMinSampleShading) then Pointer(glMinSampleShadingOES):=Pointer(glMinSampleShading);
  end else
  begin
    if not Assigned(glMinSampleShading) then Pointer(glMinSampleShading):=Pointer(glMinSampleShadingOES);
  end;
  if not Assigned(glMultiDrawArraysEXT) then
  begin
    if Assigned(glMultiDrawArrays) then Pointer(glMultiDrawArraysEXT):=Pointer(glMultiDrawArrays);
  end else
  begin
    if not Assigned(glMultiDrawArrays) then Pointer(glMultiDrawArrays):=Pointer(glMultiDrawArraysEXT);
  end;
  if not Assigned(glMultiDrawArraysIndirectEXT) then
  begin
    if Assigned(glMultiDrawArraysIndirect) then Pointer(glMultiDrawArraysIndirectEXT):=Pointer(glMultiDrawArraysIndirect);
  end else
  begin
    if not Assigned(glMultiDrawArraysIndirect) then Pointer(glMultiDrawArraysIndirect):=Pointer(glMultiDrawArraysIndirectEXT);
  end;
  if not Assigned(glMultiDrawElementsBaseVertexEXT) then
  begin
    if Assigned(glMultiDrawElementsBaseVertex) then Pointer(glMultiDrawElementsBaseVertexEXT):=Pointer(glMultiDrawElementsBaseVertex);
  end else
  begin
    if not Assigned(glMultiDrawElementsBaseVertex) then Pointer(glMultiDrawElementsBaseVertex):=Pointer(glMultiDrawElementsBaseVertexEXT);
  end;
  if not Assigned(glMultiDrawElementsEXT) then
  begin
    if Assigned(glMultiDrawElements) then Pointer(glMultiDrawElementsEXT):=Pointer(glMultiDrawElements);
  end else
  begin
    if not Assigned(glMultiDrawElements) then Pointer(glMultiDrawElements):=Pointer(glMultiDrawElementsEXT);
  end;
  if not Assigned(glMultiDrawElementsIndirectEXT) then
  begin
    if Assigned(glMultiDrawElementsIndirect) then Pointer(glMultiDrawElementsIndirectEXT):=Pointer(glMultiDrawElementsIndirect);
  end else
  begin
    if not Assigned(glMultiDrawElementsIndirect) then Pointer(glMultiDrawElementsIndirect):=Pointer(glMultiDrawElementsIndirectEXT);
  end;
  if not Assigned(glObjectLabelKHR) then
  begin
    if Assigned(glObjectLabel) then Pointer(glObjectLabelKHR):=Pointer(glObjectLabel);
  end else
  begin
    if not Assigned(glObjectLabel) then Pointer(glObjectLabel):=Pointer(glObjectLabelKHR);
  end;
  if not Assigned(glObjectPtrLabelKHR) then
  begin
    if Assigned(glObjectPtrLabel) then Pointer(glObjectPtrLabelKHR):=Pointer(glObjectPtrLabel);
  end else
  begin
    if not Assigned(glObjectPtrLabel) then Pointer(glObjectPtrLabel):=Pointer(glObjectPtrLabelKHR);
  end;
  if not Assigned(glPatchParameteriEXT) then
  begin
    if Assigned(glPatchParameteri) then Pointer(glPatchParameteriEXT):=Pointer(glPatchParameteri);
  end else
  begin
    if not Assigned(glPatchParameteri) then Pointer(glPatchParameteri):=Pointer(glPatchParameteriEXT);
  end;
  if not Assigned(glPatchParameteriOES) then
  begin
    if Assigned(glPatchParameteri) then Pointer(glPatchParameteriOES):=Pointer(glPatchParameteri);
  end else
  begin
    if not Assigned(glPatchParameteri) then Pointer(glPatchParameteri):=Pointer(glPatchParameteriOES);
  end;
  if not Assigned(glPolygonModeNV) then
  begin
    if Assigned(glPolygonMode) then Pointer(glPolygonModeNV):=Pointer(glPolygonMode);
  end else
  begin
    if not Assigned(glPolygonMode) then Pointer(glPolygonMode):=Pointer(glPolygonModeNV);
  end;
  if not Assigned(glPolygonOffsetClampEXT) then
  begin
    if Assigned(glPolygonOffsetClamp) then Pointer(glPolygonOffsetClampEXT):=Pointer(glPolygonOffsetClamp);
  end else
  begin
    if not Assigned(glPolygonOffsetClamp) then Pointer(glPolygonOffsetClamp):=Pointer(glPolygonOffsetClampEXT);
  end;
  if not Assigned(glPopDebugGroupKHR) then
  begin
    if Assigned(glPopDebugGroup) then Pointer(glPopDebugGroupKHR):=Pointer(glPopDebugGroup);
  end else
  begin
    if not Assigned(glPopDebugGroup) then Pointer(glPopDebugGroup):=Pointer(glPopDebugGroupKHR);
  end;
  if not Assigned(glPrimitiveBoundingBoxEXT) then
  begin
    if Assigned(glPrimitiveBoundingBox) then Pointer(glPrimitiveBoundingBoxEXT):=Pointer(glPrimitiveBoundingBox);
  end else
  begin
    if not Assigned(glPrimitiveBoundingBox) then Pointer(glPrimitiveBoundingBox):=Pointer(glPrimitiveBoundingBoxEXT);
  end;
  if not Assigned(glPrimitiveBoundingBoxOES) then
  begin
    if Assigned(glPrimitiveBoundingBox) then Pointer(glPrimitiveBoundingBoxOES):=Pointer(glPrimitiveBoundingBox);
  end else
  begin
    if not Assigned(glPrimitiveBoundingBox) then Pointer(glPrimitiveBoundingBox):=Pointer(glPrimitiveBoundingBoxOES);
  end;
  if not Assigned(glProgramBinaryOES) then
  begin
    if Assigned(glProgramBinary) then Pointer(glProgramBinaryOES):=Pointer(glProgramBinary);
  end else
  begin
    if not Assigned(glProgramBinary) then Pointer(glProgramBinary):=Pointer(glProgramBinaryOES);
  end;
  if not Assigned(glProgramParameteriEXT) then
  begin
    if Assigned(glProgramParameteri) then Pointer(glProgramParameteriEXT):=Pointer(glProgramParameteri);
  end else
  begin
    if not Assigned(glProgramParameteri) then Pointer(glProgramParameteri):=Pointer(glProgramParameteriEXT);
  end;
  if not Assigned(glProgramUniform1fEXT) then
  begin
    if Assigned(glProgramUniform1f) then Pointer(glProgramUniform1fEXT):=Pointer(glProgramUniform1f);
  end else
  begin
    if not Assigned(glProgramUniform1f) then Pointer(glProgramUniform1f):=Pointer(glProgramUniform1fEXT);
  end;
  if not Assigned(glProgramUniform1fvEXT) then
  begin
    if Assigned(glProgramUniform1fv) then Pointer(glProgramUniform1fvEXT):=Pointer(glProgramUniform1fv);
  end else
  begin
    if not Assigned(glProgramUniform1fv) then Pointer(glProgramUniform1fv):=Pointer(glProgramUniform1fvEXT);
  end;
  if not Assigned(glProgramUniform1iEXT) then
  begin
    if Assigned(glProgramUniform1i) then Pointer(glProgramUniform1iEXT):=Pointer(glProgramUniform1i);
  end else
  begin
    if not Assigned(glProgramUniform1i) then Pointer(glProgramUniform1i):=Pointer(glProgramUniform1iEXT);
  end;
  if not Assigned(glProgramUniform1ivEXT) then
  begin
    if Assigned(glProgramUniform1iv) then Pointer(glProgramUniform1ivEXT):=Pointer(glProgramUniform1iv);
  end else
  begin
    if not Assigned(glProgramUniform1iv) then Pointer(glProgramUniform1iv):=Pointer(glProgramUniform1ivEXT);
  end;
  if not Assigned(glProgramUniform1uiEXT) then
  begin
    if Assigned(glProgramUniform1ui) then Pointer(glProgramUniform1uiEXT):=Pointer(glProgramUniform1ui);
  end else
  begin
    if not Assigned(glProgramUniform1ui) then Pointer(glProgramUniform1ui):=Pointer(glProgramUniform1uiEXT);
  end;
  if not Assigned(glProgramUniform1uivEXT) then
  begin
    if Assigned(glProgramUniform1uiv) then Pointer(glProgramUniform1uivEXT):=Pointer(glProgramUniform1uiv);
  end else
  begin
    if not Assigned(glProgramUniform1uiv) then Pointer(glProgramUniform1uiv):=Pointer(glProgramUniform1uivEXT);
  end;
  if not Assigned(glProgramUniform2fEXT) then
  begin
    if Assigned(glProgramUniform2f) then Pointer(glProgramUniform2fEXT):=Pointer(glProgramUniform2f);
  end else
  begin
    if not Assigned(glProgramUniform2f) then Pointer(glProgramUniform2f):=Pointer(glProgramUniform2fEXT);
  end;
  if not Assigned(glProgramUniform2fvEXT) then
  begin
    if Assigned(glProgramUniform2fv) then Pointer(glProgramUniform2fvEXT):=Pointer(glProgramUniform2fv);
  end else
  begin
    if not Assigned(glProgramUniform2fv) then Pointer(glProgramUniform2fv):=Pointer(glProgramUniform2fvEXT);
  end;
  if not Assigned(glProgramUniform2iEXT) then
  begin
    if Assigned(glProgramUniform2i) then Pointer(glProgramUniform2iEXT):=Pointer(glProgramUniform2i);
  end else
  begin
    if not Assigned(glProgramUniform2i) then Pointer(glProgramUniform2i):=Pointer(glProgramUniform2iEXT);
  end;
  if not Assigned(glProgramUniform2ivEXT) then
  begin
    if Assigned(glProgramUniform2iv) then Pointer(glProgramUniform2ivEXT):=Pointer(glProgramUniform2iv);
  end else
  begin
    if not Assigned(glProgramUniform2iv) then Pointer(glProgramUniform2iv):=Pointer(glProgramUniform2ivEXT);
  end;
  if not Assigned(glProgramUniform2uiEXT) then
  begin
    if Assigned(glProgramUniform2ui) then Pointer(glProgramUniform2uiEXT):=Pointer(glProgramUniform2ui);
  end else
  begin
    if not Assigned(glProgramUniform2ui) then Pointer(glProgramUniform2ui):=Pointer(glProgramUniform2uiEXT);
  end;
  if not Assigned(glProgramUniform2uivEXT) then
  begin
    if Assigned(glProgramUniform2uiv) then Pointer(glProgramUniform2uivEXT):=Pointer(glProgramUniform2uiv);
  end else
  begin
    if not Assigned(glProgramUniform2uiv) then Pointer(glProgramUniform2uiv):=Pointer(glProgramUniform2uivEXT);
  end;
  if not Assigned(glProgramUniform3fEXT) then
  begin
    if Assigned(glProgramUniform3f) then Pointer(glProgramUniform3fEXT):=Pointer(glProgramUniform3f);
  end else
  begin
    if not Assigned(glProgramUniform3f) then Pointer(glProgramUniform3f):=Pointer(glProgramUniform3fEXT);
  end;
  if not Assigned(glProgramUniform3fvEXT) then
  begin
    if Assigned(glProgramUniform3fv) then Pointer(glProgramUniform3fvEXT):=Pointer(glProgramUniform3fv);
  end else
  begin
    if not Assigned(glProgramUniform3fv) then Pointer(glProgramUniform3fv):=Pointer(glProgramUniform3fvEXT);
  end;
  if not Assigned(glProgramUniform3iEXT) then
  begin
    if Assigned(glProgramUniform3i) then Pointer(glProgramUniform3iEXT):=Pointer(glProgramUniform3i);
  end else
  begin
    if not Assigned(glProgramUniform3i) then Pointer(glProgramUniform3i):=Pointer(glProgramUniform3iEXT);
  end;
  if not Assigned(glProgramUniform3ivEXT) then
  begin
    if Assigned(glProgramUniform3iv) then Pointer(glProgramUniform3ivEXT):=Pointer(glProgramUniform3iv);
  end else
  begin
    if not Assigned(glProgramUniform3iv) then Pointer(glProgramUniform3iv):=Pointer(glProgramUniform3ivEXT);
  end;
  if not Assigned(glProgramUniform3uiEXT) then
  begin
    if Assigned(glProgramUniform3ui) then Pointer(glProgramUniform3uiEXT):=Pointer(glProgramUniform3ui);
  end else
  begin
    if not Assigned(glProgramUniform3ui) then Pointer(glProgramUniform3ui):=Pointer(glProgramUniform3uiEXT);
  end;
  if not Assigned(glProgramUniform3uivEXT) then
  begin
    if Assigned(glProgramUniform3uiv) then Pointer(glProgramUniform3uivEXT):=Pointer(glProgramUniform3uiv);
  end else
  begin
    if not Assigned(glProgramUniform3uiv) then Pointer(glProgramUniform3uiv):=Pointer(glProgramUniform3uivEXT);
  end;
  if not Assigned(glProgramUniform4fEXT) then
  begin
    if Assigned(glProgramUniform4f) then Pointer(glProgramUniform4fEXT):=Pointer(glProgramUniform4f);
  end else
  begin
    if not Assigned(glProgramUniform4f) then Pointer(glProgramUniform4f):=Pointer(glProgramUniform4fEXT);
  end;
  if not Assigned(glProgramUniform4fvEXT) then
  begin
    if Assigned(glProgramUniform4fv) then Pointer(glProgramUniform4fvEXT):=Pointer(glProgramUniform4fv);
  end else
  begin
    if not Assigned(glProgramUniform4fv) then Pointer(glProgramUniform4fv):=Pointer(glProgramUniform4fvEXT);
  end;
  if not Assigned(glProgramUniform4iEXT) then
  begin
    if Assigned(glProgramUniform4i) then Pointer(glProgramUniform4iEXT):=Pointer(glProgramUniform4i);
  end else
  begin
    if not Assigned(glProgramUniform4i) then Pointer(glProgramUniform4i):=Pointer(glProgramUniform4iEXT);
  end;
  if not Assigned(glProgramUniform4ivEXT) then
  begin
    if Assigned(glProgramUniform4iv) then Pointer(glProgramUniform4ivEXT):=Pointer(glProgramUniform4iv);
  end else
  begin
    if not Assigned(glProgramUniform4iv) then Pointer(glProgramUniform4iv):=Pointer(glProgramUniform4ivEXT);
  end;
  if not Assigned(glProgramUniform4uiEXT) then
  begin
    if Assigned(glProgramUniform4ui) then Pointer(glProgramUniform4uiEXT):=Pointer(glProgramUniform4ui);
  end else
  begin
    if not Assigned(glProgramUniform4ui) then Pointer(glProgramUniform4ui):=Pointer(glProgramUniform4uiEXT);
  end;
  if not Assigned(glProgramUniform4uivEXT) then
  begin
    if Assigned(glProgramUniform4uiv) then Pointer(glProgramUniform4uivEXT):=Pointer(glProgramUniform4uiv);
  end else
  begin
    if not Assigned(glProgramUniform4uiv) then Pointer(glProgramUniform4uiv):=Pointer(glProgramUniform4uivEXT);
  end;
  if not Assigned(glProgramUniformHandleui64IMG) then
  begin
    if Assigned(glProgramUniformHandleui64ARB) then Pointer(glProgramUniformHandleui64IMG):=Pointer(glProgramUniformHandleui64ARB);
  end else
  begin
    if not Assigned(glProgramUniformHandleui64ARB) then Pointer(glProgramUniformHandleui64ARB):=Pointer(glProgramUniformHandleui64IMG);
  end;
  if not Assigned(glProgramUniformHandleui64vIMG) then
  begin
    if Assigned(glProgramUniformHandleui64vARB) then Pointer(glProgramUniformHandleui64vIMG):=Pointer(glProgramUniformHandleui64vARB);
  end else
  begin
    if not Assigned(glProgramUniformHandleui64vARB) then Pointer(glProgramUniformHandleui64vARB):=Pointer(glProgramUniformHandleui64vIMG);
  end;
  if not Assigned(glProgramUniformMatrix2fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix2fv) then Pointer(glProgramUniformMatrix2fvEXT):=Pointer(glProgramUniformMatrix2fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix2fv) then Pointer(glProgramUniformMatrix2fv):=Pointer(glProgramUniformMatrix2fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix2x3fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix2x3fv) then Pointer(glProgramUniformMatrix2x3fvEXT):=Pointer(glProgramUniformMatrix2x3fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix2x3fv) then Pointer(glProgramUniformMatrix2x3fv):=Pointer(glProgramUniformMatrix2x3fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix2x4fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix2x4fv) then Pointer(glProgramUniformMatrix2x4fvEXT):=Pointer(glProgramUniformMatrix2x4fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix2x4fv) then Pointer(glProgramUniformMatrix2x4fv):=Pointer(glProgramUniformMatrix2x4fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix3fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix3fv) then Pointer(glProgramUniformMatrix3fvEXT):=Pointer(glProgramUniformMatrix3fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix3fv) then Pointer(glProgramUniformMatrix3fv):=Pointer(glProgramUniformMatrix3fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix3x2fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix3x2fv) then Pointer(glProgramUniformMatrix3x2fvEXT):=Pointer(glProgramUniformMatrix3x2fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix3x2fv) then Pointer(glProgramUniformMatrix3x2fv):=Pointer(glProgramUniformMatrix3x2fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix3x4fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix3x4fv) then Pointer(glProgramUniformMatrix3x4fvEXT):=Pointer(glProgramUniformMatrix3x4fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix3x4fv) then Pointer(glProgramUniformMatrix3x4fv):=Pointer(glProgramUniformMatrix3x4fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix4fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix4fv) then Pointer(glProgramUniformMatrix4fvEXT):=Pointer(glProgramUniformMatrix4fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix4fv) then Pointer(glProgramUniformMatrix4fv):=Pointer(glProgramUniformMatrix4fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix4x2fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix4x2fv) then Pointer(glProgramUniformMatrix4x2fvEXT):=Pointer(glProgramUniformMatrix4x2fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix4x2fv) then Pointer(glProgramUniformMatrix4x2fv):=Pointer(glProgramUniformMatrix4x2fvEXT);
  end;
  if not Assigned(glProgramUniformMatrix4x3fvEXT) then
  begin
    if Assigned(glProgramUniformMatrix4x3fv) then Pointer(glProgramUniformMatrix4x3fvEXT):=Pointer(glProgramUniformMatrix4x3fv);
  end else
  begin
    if not Assigned(glProgramUniformMatrix4x3fv) then Pointer(glProgramUniformMatrix4x3fv):=Pointer(glProgramUniformMatrix4x3fvEXT);
  end;
  if not Assigned(glPushDebugGroupKHR) then
  begin
    if Assigned(glPushDebugGroup) then Pointer(glPushDebugGroupKHR):=Pointer(glPushDebugGroup);
  end else
  begin
    if not Assigned(glPushDebugGroup) then Pointer(glPushDebugGroup):=Pointer(glPushDebugGroupKHR);
  end;
  if not Assigned(glQueryCounterEXT) then
  begin
    if Assigned(glQueryCounter) then Pointer(glQueryCounterEXT):=Pointer(glQueryCounter);
  end else
  begin
    if not Assigned(glQueryCounter) then Pointer(glQueryCounter):=Pointer(glQueryCounterEXT);
  end;
  if not Assigned(glReadnPixelsEXT) then
  begin
    if Assigned(glReadnPixels) then Pointer(glReadnPixelsEXT):=Pointer(glReadnPixels);
  end else
  begin
    if not Assigned(glReadnPixels) then Pointer(glReadnPixels):=Pointer(glReadnPixelsEXT);
  end;
  if not Assigned(glReadnPixelsKHR) then
  begin
    if Assigned(glReadnPixels) then Pointer(glReadnPixelsKHR):=Pointer(glReadnPixels);
  end else
  begin
    if not Assigned(glReadnPixels) then Pointer(glReadnPixels):=Pointer(glReadnPixelsKHR);
  end;
  if not Assigned(glRenderbufferStorageMultisampleEXT) then
  begin
    if Assigned(glRenderbufferStorageMultisample) then Pointer(glRenderbufferStorageMultisampleEXT):=Pointer(glRenderbufferStorageMultisample);
  end else
  begin
    if not Assigned(glRenderbufferStorageMultisample) then Pointer(glRenderbufferStorageMultisample):=Pointer(glRenderbufferStorageMultisampleEXT);
  end;
  if not Assigned(glRenderbufferStorageMultisampleNV) then
  begin
    if Assigned(glRenderbufferStorageMultisample) then Pointer(glRenderbufferStorageMultisampleNV):=Pointer(glRenderbufferStorageMultisample);
  end else
  begin
    if not Assigned(glRenderbufferStorageMultisample) then Pointer(glRenderbufferStorageMultisample):=Pointer(glRenderbufferStorageMultisampleNV);
  end;
  if not Assigned(glSamplerParameterIivEXT) then
  begin
    if Assigned(glSamplerParameterIiv) then Pointer(glSamplerParameterIivEXT):=Pointer(glSamplerParameterIiv);
  end else
  begin
    if not Assigned(glSamplerParameterIiv) then Pointer(glSamplerParameterIiv):=Pointer(glSamplerParameterIivEXT);
  end;
  if not Assigned(glSamplerParameterIivOES) then
  begin
    if Assigned(glSamplerParameterIiv) then Pointer(glSamplerParameterIivOES):=Pointer(glSamplerParameterIiv);
  end else
  begin
    if not Assigned(glSamplerParameterIiv) then Pointer(glSamplerParameterIiv):=Pointer(glSamplerParameterIivOES);
  end;
  if not Assigned(glSamplerParameterIuivEXT) then
  begin
    if Assigned(glSamplerParameterIuiv) then Pointer(glSamplerParameterIuivEXT):=Pointer(glSamplerParameterIuiv);
  end else
  begin
    if not Assigned(glSamplerParameterIuiv) then Pointer(glSamplerParameterIuiv):=Pointer(glSamplerParameterIuivEXT);
  end;
  if not Assigned(glSamplerParameterIuivOES) then
  begin
    if Assigned(glSamplerParameterIuiv) then Pointer(glSamplerParameterIuivOES):=Pointer(glSamplerParameterIuiv);
  end else
  begin
    if not Assigned(glSamplerParameterIuiv) then Pointer(glSamplerParameterIuiv):=Pointer(glSamplerParameterIuivOES);
  end;
  if not Assigned(glScissorArrayvNV) then
  begin
    if Assigned(glScissorArrayv) then Pointer(glScissorArrayvNV):=Pointer(glScissorArrayv);
  end else
  begin
    if not Assigned(glScissorArrayv) then Pointer(glScissorArrayv):=Pointer(glScissorArrayvNV);
  end;
  if not Assigned(glScissorArrayvOES) then
  begin
    if Assigned(glScissorArrayv) then Pointer(glScissorArrayvOES):=Pointer(glScissorArrayv);
  end else
  begin
    if not Assigned(glScissorArrayv) then Pointer(glScissorArrayv):=Pointer(glScissorArrayvOES);
  end;
  if not Assigned(glScissorIndexedNV) then
  begin
    if Assigned(glScissorIndexed) then Pointer(glScissorIndexedNV):=Pointer(glScissorIndexed);
  end else
  begin
    if not Assigned(glScissorIndexed) then Pointer(glScissorIndexed):=Pointer(glScissorIndexedNV);
  end;
  if not Assigned(glScissorIndexedOES) then
  begin
    if Assigned(glScissorIndexed) then Pointer(glScissorIndexedOES):=Pointer(glScissorIndexed);
  end else
  begin
    if not Assigned(glScissorIndexed) then Pointer(glScissorIndexed):=Pointer(glScissorIndexedOES);
  end;
  if not Assigned(glScissorIndexedvNV) then
  begin
    if Assigned(glScissorIndexedv) then Pointer(glScissorIndexedvNV):=Pointer(glScissorIndexedv);
  end else
  begin
    if not Assigned(glScissorIndexedv) then Pointer(glScissorIndexedv):=Pointer(glScissorIndexedvNV);
  end;
  if not Assigned(glScissorIndexedvOES) then
  begin
    if Assigned(glScissorIndexedv) then Pointer(glScissorIndexedvOES):=Pointer(glScissorIndexedv);
  end else
  begin
    if not Assigned(glScissorIndexedv) then Pointer(glScissorIndexedv):=Pointer(glScissorIndexedvOES);
  end;
  if not Assigned(glTexBufferEXT) then
  begin
    if Assigned(glTexBuffer) then Pointer(glTexBufferEXT):=Pointer(glTexBuffer);
  end else
  begin
    if not Assigned(glTexBuffer) then Pointer(glTexBuffer):=Pointer(glTexBufferEXT);
  end;
  if not Assigned(glTexBufferOES) then
  begin
    if Assigned(glTexBuffer) then Pointer(glTexBufferOES):=Pointer(glTexBuffer);
  end else
  begin
    if not Assigned(glTexBuffer) then Pointer(glTexBuffer):=Pointer(glTexBufferOES);
  end;
  if not Assigned(glTexBufferRangeEXT) then
  begin
    if Assigned(glTexBufferRange) then Pointer(glTexBufferRangeEXT):=Pointer(glTexBufferRange);
  end else
  begin
    if not Assigned(glTexBufferRange) then Pointer(glTexBufferRange):=Pointer(glTexBufferRangeEXT);
  end;
  if not Assigned(glTexBufferRangeOES) then
  begin
    if Assigned(glTexBufferRange) then Pointer(glTexBufferRangeOES):=Pointer(glTexBufferRange);
  end else
  begin
    if not Assigned(glTexBufferRange) then Pointer(glTexBufferRange):=Pointer(glTexBufferRangeOES);
  end;
  if not Assigned(glTexPageCommitmentEXT) then
  begin
    if Assigned(glTexPageCommitmentARB) then Pointer(glTexPageCommitmentEXT):=Pointer(glTexPageCommitmentARB);
  end else
  begin
    if not Assigned(glTexPageCommitmentARB) then Pointer(glTexPageCommitmentARB):=Pointer(glTexPageCommitmentEXT);
  end;
  if not Assigned(glTexParameterIivEXT) then
  begin
    if Assigned(glTexParameterIiv) then Pointer(glTexParameterIivEXT):=Pointer(glTexParameterIiv);
  end else
  begin
    if not Assigned(glTexParameterIiv) then Pointer(glTexParameterIiv):=Pointer(glTexParameterIivEXT);
  end;
  if not Assigned(glTexParameterIivOES) then
  begin
    if Assigned(glTexParameterIiv) then Pointer(glTexParameterIivOES):=Pointer(glTexParameterIiv);
  end else
  begin
    if not Assigned(glTexParameterIiv) then Pointer(glTexParameterIiv):=Pointer(glTexParameterIivOES);
  end;
  if not Assigned(glTexParameterIuivEXT) then
  begin
    if Assigned(glTexParameterIuiv) then Pointer(glTexParameterIuivEXT):=Pointer(glTexParameterIuiv);
  end else
  begin
    if not Assigned(glTexParameterIuiv) then Pointer(glTexParameterIuiv):=Pointer(glTexParameterIuivEXT);
  end;
  if not Assigned(glTexParameterIuivOES) then
  begin
    if Assigned(glTexParameterIuiv) then Pointer(glTexParameterIuivOES):=Pointer(glTexParameterIuiv);
  end else
  begin
    if not Assigned(glTexParameterIuiv) then Pointer(glTexParameterIuiv):=Pointer(glTexParameterIuivOES);
  end;
  if not Assigned(glTexStorage1DEXT) then
  begin
    if Assigned(glTexStorage1D) then Pointer(glTexStorage1DEXT):=Pointer(glTexStorage1D);
  end else
  begin
    if not Assigned(glTexStorage1D) then Pointer(glTexStorage1D):=Pointer(glTexStorage1DEXT);
  end;
  if not Assigned(glTexStorage2DEXT) then
  begin
    if Assigned(glTexStorage2D) then Pointer(glTexStorage2DEXT):=Pointer(glTexStorage2D);
  end else
  begin
    if not Assigned(glTexStorage2D) then Pointer(glTexStorage2D):=Pointer(glTexStorage2DEXT);
  end;
  if not Assigned(glTexStorage3DEXT) then
  begin
    if Assigned(glTexStorage3D) then Pointer(glTexStorage3DEXT):=Pointer(glTexStorage3D);
  end else
  begin
    if not Assigned(glTexStorage3D) then Pointer(glTexStorage3D):=Pointer(glTexStorage3DEXT);
  end;
  if not Assigned(glTexStorage3DMultisampleOES) then
  begin
    if Assigned(glTexStorage3DMultisample) then Pointer(glTexStorage3DMultisampleOES):=Pointer(glTexStorage3DMultisample);
  end else
  begin
    if not Assigned(glTexStorage3DMultisample) then Pointer(glTexStorage3DMultisample):=Pointer(glTexStorage3DMultisampleOES);
  end;
  if not Assigned(glTextureViewEXT) then
  begin
    if Assigned(glTextureView) then Pointer(glTextureViewEXT):=Pointer(glTextureView);
  end else
  begin
    if not Assigned(glTextureView) then Pointer(glTextureView):=Pointer(glTextureViewEXT);
  end;
  if not Assigned(glTextureViewOES) then
  begin
    if Assigned(glTextureView) then Pointer(glTextureViewOES):=Pointer(glTextureView);
  end else
  begin
    if not Assigned(glTextureView) then Pointer(glTextureView):=Pointer(glTextureViewOES);
  end;
  if not Assigned(glUniformHandleui64IMG) then
  begin
    if Assigned(glUniformHandleui64ARB) then Pointer(glUniformHandleui64IMG):=Pointer(glUniformHandleui64ARB);
  end else
  begin
    if not Assigned(glUniformHandleui64ARB) then Pointer(glUniformHandleui64ARB):=Pointer(glUniformHandleui64IMG);
  end;
  if not Assigned(glUniformHandleui64vIMG) then
  begin
    if Assigned(glUniformHandleui64vARB) then Pointer(glUniformHandleui64vIMG):=Pointer(glUniformHandleui64vARB);
  end else
  begin
    if not Assigned(glUniformHandleui64vARB) then Pointer(glUniformHandleui64vARB):=Pointer(glUniformHandleui64vIMG);
  end;
  if not Assigned(glUniformMatrix2x3fvNV) then
  begin
    if Assigned(glUniformMatrix2x3fv) then Pointer(glUniformMatrix2x3fvNV):=Pointer(glUniformMatrix2x3fv);
  end else
  begin
    if not Assigned(glUniformMatrix2x3fv) then Pointer(glUniformMatrix2x3fv):=Pointer(glUniformMatrix2x3fvNV);
  end;
  if not Assigned(glUniformMatrix2x4fvNV) then
  begin
    if Assigned(glUniformMatrix2x4fv) then Pointer(glUniformMatrix2x4fvNV):=Pointer(glUniformMatrix2x4fv);
  end else
  begin
    if not Assigned(glUniformMatrix2x4fv) then Pointer(glUniformMatrix2x4fv):=Pointer(glUniformMatrix2x4fvNV);
  end;
  if not Assigned(glUniformMatrix3x2fvNV) then
  begin
    if Assigned(glUniformMatrix3x2fv) then Pointer(glUniformMatrix3x2fvNV):=Pointer(glUniformMatrix3x2fv);
  end else
  begin
    if not Assigned(glUniformMatrix3x2fv) then Pointer(glUniformMatrix3x2fv):=Pointer(glUniformMatrix3x2fvNV);
  end;
  if not Assigned(glUniformMatrix3x4fvNV) then
  begin
    if Assigned(glUniformMatrix3x4fv) then Pointer(glUniformMatrix3x4fvNV):=Pointer(glUniformMatrix3x4fv);
  end else
  begin
    if not Assigned(glUniformMatrix3x4fv) then Pointer(glUniformMatrix3x4fv):=Pointer(glUniformMatrix3x4fvNV);
  end;
  if not Assigned(glUniformMatrix4x2fvNV) then
  begin
    if Assigned(glUniformMatrix4x2fv) then Pointer(glUniformMatrix4x2fvNV):=Pointer(glUniformMatrix4x2fv);
  end else
  begin
    if not Assigned(glUniformMatrix4x2fv) then Pointer(glUniformMatrix4x2fv):=Pointer(glUniformMatrix4x2fvNV);
  end;
  if not Assigned(glUniformMatrix4x3fvNV) then
  begin
    if Assigned(glUniformMatrix4x3fv) then Pointer(glUniformMatrix4x3fvNV):=Pointer(glUniformMatrix4x3fv);
  end else
  begin
    if not Assigned(glUniformMatrix4x3fv) then Pointer(glUniformMatrix4x3fv):=Pointer(glUniformMatrix4x3fvNV);
  end;
  if not Assigned(glUnmapBufferOES) then
  begin
    if Assigned(glUnmapBuffer) then Pointer(glUnmapBufferOES):=Pointer(glUnmapBuffer);
  end else
  begin
    if not Assigned(glUnmapBuffer) then Pointer(glUnmapBuffer):=Pointer(glUnmapBufferOES);
  end;
  if not Assigned(glVertexAttribDivisorANGLE) then
  begin
    if Assigned(glVertexAttribDivisor) then Pointer(glVertexAttribDivisorANGLE):=Pointer(glVertexAttribDivisor);
  end else
  begin
    if not Assigned(glVertexAttribDivisor) then Pointer(glVertexAttribDivisor):=Pointer(glVertexAttribDivisorANGLE);
  end;
  if not Assigned(glVertexAttribDivisorEXT) then
  begin
    if Assigned(glVertexAttribDivisor) then Pointer(glVertexAttribDivisorEXT):=Pointer(glVertexAttribDivisor);
  end else
  begin
    if not Assigned(glVertexAttribDivisor) then Pointer(glVertexAttribDivisor):=Pointer(glVertexAttribDivisorEXT);
  end;
  if not Assigned(glVertexAttribDivisorNV) then
  begin
    if Assigned(glVertexAttribDivisor) then Pointer(glVertexAttribDivisorNV):=Pointer(glVertexAttribDivisor);
  end else
  begin
    if not Assigned(glVertexAttribDivisor) then Pointer(glVertexAttribDivisor):=Pointer(glVertexAttribDivisorNV);
  end;
  if not Assigned(glViewportArrayvNV) then
  begin
    if Assigned(glViewportArrayv) then Pointer(glViewportArrayvNV):=Pointer(glViewportArrayv);
  end else
  begin
    if not Assigned(glViewportArrayv) then Pointer(glViewportArrayv):=Pointer(glViewportArrayvNV);
  end;
  if not Assigned(glViewportArrayvOES) then
  begin
    if Assigned(glViewportArrayv) then Pointer(glViewportArrayvOES):=Pointer(glViewportArrayv);
  end else
  begin
    if not Assigned(glViewportArrayv) then Pointer(glViewportArrayv):=Pointer(glViewportArrayvOES);
  end;
  if not Assigned(glViewportIndexedfOES) then
  begin
    if Assigned(glViewportIndexedf) then Pointer(glViewportIndexedfOES):=Pointer(glViewportIndexedf);
  end else
  begin
    if not Assigned(glViewportIndexedf) then Pointer(glViewportIndexedf):=Pointer(glViewportIndexedfOES);
  end;
  if not Assigned(glViewportIndexedfNV) then
  begin
    if Assigned(glViewportIndexedf) then Pointer(glViewportIndexedfNV):=Pointer(glViewportIndexedf);
  end else
  begin
    if not Assigned(glViewportIndexedf) then Pointer(glViewportIndexedf):=Pointer(glViewportIndexedfNV);
  end;
  if not Assigned(glViewportIndexedfvOES) then
  begin
    if Assigned(glViewportIndexedfv) then Pointer(glViewportIndexedfvOES):=Pointer(glViewportIndexedfv);
  end else
  begin
    if not Assigned(glViewportIndexedfv) then Pointer(glViewportIndexedfv):=Pointer(glViewportIndexedfvOES);
  end;
  if not Assigned(glViewportIndexedfvNV) then
  begin
    if Assigned(glViewportIndexedfv) then Pointer(glViewportIndexedfvNV):=Pointer(glViewportIndexedfv);
  end else
  begin
    if not Assigned(glViewportIndexedfv) then Pointer(glViewportIndexedfv):=Pointer(glViewportIndexedfvNV);
  end;
  if not Assigned(glWaitSyncAPPLE) then
  begin
    if Assigned(glWaitSync) then Pointer(glWaitSyncAPPLE):=Pointer(glWaitSync);
  end else
  begin
    if not Assigned(glWaitSync) then Pointer(glWaitSync):=Pointer(glWaitSyncAPPLE);
  end;
 
//--- EGL -----
  if not Assigned(eglClientWaitSyncKHR) then
  begin
    if Assigned(eglClientWaitSync) then Pointer(eglClientWaitSyncKHR):=Pointer(eglClientWaitSync);
  end else
  begin
    if not Assigned(eglClientWaitSync) then Pointer(eglClientWaitSync):=Pointer(eglClientWaitSyncKHR);
  end;
  if not Assigned(eglCreateSync64KHR) then
  begin
    if Assigned(eglCreateSync) then Pointer(eglCreateSync64KHR):=Pointer(eglCreateSync);
  end else
  begin
    if not Assigned(eglCreateSync) then Pointer(eglCreateSync):=Pointer(eglCreateSync64KHR);
  end;
  if not Assigned(eglDestroyImageKHR) then
  begin
    if Assigned(eglDestroyImage) then Pointer(eglDestroyImageKHR):=Pointer(eglDestroyImage);
  end else
  begin
    if not Assigned(eglDestroyImage) then Pointer(eglDestroyImage):=Pointer(eglDestroyImageKHR);
  end;
  if not Assigned(eglDestroySyncKHR) then
  begin
    if Assigned(eglDestroySync) then Pointer(eglDestroySyncKHR):=Pointer(eglDestroySync);
  end else
  begin
    if not Assigned(eglDestroySync) then Pointer(eglDestroySync):=Pointer(eglDestroySyncKHR);
  end;
  if not Assigned(eglQueryDisplayAttribEXT) then
  begin
    if Assigned(eglQueryDisplayAttribKHR) then Pointer(eglQueryDisplayAttribEXT):=Pointer(eglQueryDisplayAttribKHR);
  end else
  begin
    if not Assigned(eglQueryDisplayAttribKHR) then Pointer(eglQueryDisplayAttribKHR):=Pointer(eglQueryDisplayAttribEXT);
  end;
  if not Assigned(eglQueryDisplayAttribNV) then
  begin
    if Assigned(eglQueryDisplayAttribKHR) then Pointer(eglQueryDisplayAttribNV):=Pointer(eglQueryDisplayAttribKHR);
  end else
  begin
    if not Assigned(eglQueryDisplayAttribKHR) then Pointer(eglQueryDisplayAttribKHR):=Pointer(eglQueryDisplayAttribNV);
  end;
 
end;
 
Procedure _Internal_LoadGLExtensions; //---- Supported Extensions -------------
begin
  FillGLExtensions;
  FillEGLExtensions;
 
  GL_AMD_compressed_3DC_texture:=OpenGLESlib_ExtensionGet('GL_AMD_compressed_3DC_texture');
  GL_AMD_compressed_ATC_texture:=OpenGLESlib_ExtensionGet('GL_AMD_compressed_ATC_texture');
  GL_AMD_framebuffer_multisample_advanced:=OpenGLESlib_ExtensionGet('GL_AMD_framebuffer_multisample_advanced');
  GL_AMD_performance_monitor:=OpenGLESlib_ExtensionGet('GL_AMD_performance_monitor');
  GL_AMD_program_binary_Z400:=OpenGLESlib_ExtensionGet('GL_AMD_program_binary_Z400');
  GL_ANDROID_extension_pack_es31a:=OpenGLESlib_ExtensionGet('GL_ANDROID_extension_pack_es31a');
  GL_ANGLE_depth_texture:=OpenGLESlib_ExtensionGet('GL_ANGLE_depth_texture');
  GL_ANGLE_framebuffer_blit:=OpenGLESlib_ExtensionGet('GL_ANGLE_framebuffer_blit');
  GL_ANGLE_framebuffer_multisample:=OpenGLESlib_ExtensionGet('GL_ANGLE_framebuffer_multisample');
  GL_ANGLE_instanced_arrays:=OpenGLESlib_ExtensionGet('GL_ANGLE_instanced_arrays');
  GL_ANGLE_pack_reverse_row_order:=OpenGLESlib_ExtensionGet('GL_ANGLE_pack_reverse_row_order');
  GL_ANGLE_program_binary:=OpenGLESlib_ExtensionGet('GL_ANGLE_program_binary');
  GL_ANGLE_texture_compression_dxt3:=OpenGLESlib_ExtensionGet('GL_ANGLE_texture_compression_dxt3');
  GL_ANGLE_texture_compression_dxt5:=OpenGLESlib_ExtensionGet('GL_ANGLE_texture_compression_dxt5');
  GL_ANGLE_texture_usage:=OpenGLESlib_ExtensionGet('GL_ANGLE_texture_usage');
  GL_ANGLE_translated_shader_source:=OpenGLESlib_ExtensionGet('GL_ANGLE_translated_shader_source');
  GL_APPLE_clip_distance:=OpenGLESlib_ExtensionGet('GL_APPLE_clip_distance');
  GL_APPLE_color_buffer_packed_float:=OpenGLESlib_ExtensionGet('GL_APPLE_color_buffer_packed_float');
  GL_APPLE_copy_texture_levels:=OpenGLESlib_ExtensionGet('GL_APPLE_copy_texture_levels');
  GL_APPLE_framebuffer_multisample:=OpenGLESlib_ExtensionGet('GL_APPLE_framebuffer_multisample');
  GL_APPLE_rgb_422:=OpenGLESlib_ExtensionGet('GL_APPLE_rgb_422');
  GL_APPLE_sync:=OpenGLESlib_ExtensionGet('GL_APPLE_sync');
  GL_APPLE_texture_format_BGRA8888:=OpenGLESlib_ExtensionGet('GL_APPLE_texture_format_BGRA8888');
  GL_APPLE_texture_max_level:=OpenGLESlib_ExtensionGet('GL_APPLE_texture_max_level');
  GL_APPLE_texture_packed_float:=OpenGLESlib_ExtensionGet('GL_APPLE_texture_packed_float');
  GL_ARM_mali_program_binary:=OpenGLESlib_ExtensionGet('GL_ARM_mali_program_binary');
  GL_ARM_mali_shader_binary:=OpenGLESlib_ExtensionGet('GL_ARM_mali_shader_binary');
  GL_ARM_rgba8:=OpenGLESlib_ExtensionGet('GL_ARM_rgba8');
  GL_ARM_shader_framebuffer_fetch:=OpenGLESlib_ExtensionGet('GL_ARM_shader_framebuffer_fetch');
  GL_ARM_shader_framebuffer_fetch_depth_stencil:=OpenGLESlib_ExtensionGet('GL_ARM_shader_framebuffer_fetch_depth_stencil');
  GL_ARM_texture_unnormalized_coordinates:=OpenGLESlib_ExtensionGet('GL_ARM_texture_unnormalized_coordinates');
  GL_DMP_program_binary:=OpenGLESlib_ExtensionGet('GL_DMP_program_binary');
  GL_DMP_shader_binary:=OpenGLESlib_ExtensionGet('GL_DMP_shader_binary');
  GL_EXT_EGL_image_array:=OpenGLESlib_ExtensionGet('GL_EXT_EGL_image_array');
  GL_EXT_EGL_image_storage:=OpenGLESlib_ExtensionGet('GL_EXT_EGL_image_storage');
  GL_EXT_YUV_target:=OpenGLESlib_ExtensionGet('GL_EXT_YUV_target');
  GL_EXT_base_instance:=OpenGLESlib_ExtensionGet('GL_EXT_base_instance');
  GL_EXT_blend_func_extended:=OpenGLESlib_ExtensionGet('GL_EXT_blend_func_extended');
  GL_EXT_blend_minmax:=OpenGLESlib_ExtensionGet('GL_EXT_blend_minmax');
  GL_EXT_buffer_storage:=OpenGLESlib_ExtensionGet('GL_EXT_buffer_storage');
  GL_EXT_clear_texture:=OpenGLESlib_ExtensionGet('GL_EXT_clear_texture');
  GL_EXT_clip_control:=OpenGLESlib_ExtensionGet('GL_EXT_clip_control');
  GL_EXT_clip_cull_distance:=OpenGLESlib_ExtensionGet('GL_EXT_clip_cull_distance');
  GL_EXT_color_buffer_float:=OpenGLESlib_ExtensionGet('GL_EXT_color_buffer_float');
  GL_EXT_color_buffer_half_float:=OpenGLESlib_ExtensionGet('GL_EXT_color_buffer_half_float');
  GL_EXT_conservative_depth:=OpenGLESlib_ExtensionGet('GL_EXT_conservative_depth');
  GL_EXT_copy_image:=OpenGLESlib_ExtensionGet('GL_EXT_copy_image');
  GL_EXT_debug_label:=OpenGLESlib_ExtensionGet('GL_EXT_debug_label');
  GL_EXT_debug_marker:=OpenGLESlib_ExtensionGet('GL_EXT_debug_marker');
  GL_EXT_depth_clamp:=OpenGLESlib_ExtensionGet('GL_EXT_depth_clamp');
  GL_EXT_discard_framebuffer:=OpenGLESlib_ExtensionGet('GL_EXT_discard_framebuffer');
  GL_EXT_disjoint_timer_query:=OpenGLESlib_ExtensionGet('GL_EXT_disjoint_timer_query');
  GL_EXT_draw_buffers:=OpenGLESlib_ExtensionGet('GL_EXT_draw_buffers');
  GL_EXT_draw_buffers_indexed:=OpenGLESlib_ExtensionGet('GL_EXT_draw_buffers_indexed');
  GL_EXT_draw_elements_base_vertex:=OpenGLESlib_ExtensionGet('GL_EXT_draw_elements_base_vertex');
  GL_EXT_draw_instanced:=OpenGLESlib_ExtensionGet('GL_EXT_draw_instanced');
  GL_EXT_draw_transform_feedback:=OpenGLESlib_ExtensionGet('GL_EXT_draw_transform_feedback');
  GL_EXT_external_buffer:=OpenGLESlib_ExtensionGet('GL_EXT_external_buffer');
  GL_EXT_float_blend:=OpenGLESlib_ExtensionGet('GL_EXT_float_blend');
  GL_EXT_geometry_point_size:=OpenGLESlib_ExtensionGet('GL_EXT_geometry_point_size');
  GL_EXT_geometry_shader:=OpenGLESlib_ExtensionGet('GL_EXT_geometry_shader');
  GL_EXT_gpu_shader5:=OpenGLESlib_ExtensionGet('GL_EXT_gpu_shader5');
  GL_EXT_instanced_arrays:=OpenGLESlib_ExtensionGet('GL_EXT_instanced_arrays');
  GL_EXT_map_buffer_range:=OpenGLESlib_ExtensionGet('GL_EXT_map_buffer_range');
  GL_EXT_memory_object:=OpenGLESlib_ExtensionGet('GL_EXT_memory_object');
  GL_EXT_memory_object_fd:=OpenGLESlib_ExtensionGet('GL_EXT_memory_object_fd');
  GL_EXT_memory_object_win32:=OpenGLESlib_ExtensionGet('GL_EXT_memory_object_win32');
  GL_EXT_multi_draw_arrays:=OpenGLESlib_ExtensionGet('GL_EXT_multi_draw_arrays');
  GL_EXT_multi_draw_indirect:=OpenGLESlib_ExtensionGet('GL_EXT_multi_draw_indirect');
  GL_EXT_multisampled_compatibility:=OpenGLESlib_ExtensionGet('GL_EXT_multisampled_compatibility');
  GL_EXT_multisampled_render_to_texture:=OpenGLESlib_ExtensionGet('GL_EXT_multisampled_render_to_texture');
  GL_EXT_multisampled_render_to_texture2:=OpenGLESlib_ExtensionGet('GL_EXT_multisampled_render_to_texture2');
  GL_EXT_multiview_draw_buffers:=OpenGLESlib_ExtensionGet('GL_EXT_multiview_draw_buffers');
  GL_EXT_multiview_tessellation_geometry_shader:=OpenGLESlib_ExtensionGet('GL_EXT_multiview_tessellation_geometry_shader');
  GL_EXT_multiview_texture_multisample:=OpenGLESlib_ExtensionGet('GL_EXT_multiview_texture_multisample');
  GL_EXT_multiview_timer_query:=OpenGLESlib_ExtensionGet('GL_EXT_multiview_timer_query');
  GL_EXT_occlusion_query_boolean:=OpenGLESlib_ExtensionGet('GL_EXT_occlusion_query_boolean');
  GL_EXT_polygon_offset_clamp:=OpenGLESlib_ExtensionGet('GL_EXT_polygon_offset_clamp');
  GL_EXT_post_depth_coverage:=OpenGLESlib_ExtensionGet('GL_EXT_post_depth_coverage');
  GL_EXT_primitive_bounding_box:=OpenGLESlib_ExtensionGet('GL_EXT_primitive_bounding_box');
  GL_EXT_protected_textures:=OpenGLESlib_ExtensionGet('GL_EXT_protected_textures');
  GL_EXT_pvrtc_sRGB:=OpenGLESlib_ExtensionGet('GL_EXT_pvrtc_sRGB');
  GL_EXT_raster_multisample:=OpenGLESlib_ExtensionGet('GL_EXT_raster_multisample');
  GL_EXT_read_format_bgra:=OpenGLESlib_ExtensionGet('GL_EXT_read_format_bgra');
  GL_EXT_render_snorm:=OpenGLESlib_ExtensionGet('GL_EXT_render_snorm');
  GL_EXT_robustness:=OpenGLESlib_ExtensionGet('GL_EXT_robustness');
  GL_EXT_semaphore:=OpenGLESlib_ExtensionGet('GL_EXT_semaphore');
  GL_EXT_semaphore_fd:=OpenGLESlib_ExtensionGet('GL_EXT_semaphore_fd');
  GL_EXT_semaphore_win32:=OpenGLESlib_ExtensionGet('GL_EXT_semaphore_win32');
  GL_EXT_sRGB:=OpenGLESlib_ExtensionGet('GL_EXT_sRGB');
  GL_EXT_sRGB_write_control:=OpenGLESlib_ExtensionGet('GL_EXT_sRGB_write_control');
  GL_EXT_separate_shader_objects:=OpenGLESlib_ExtensionGet('GL_EXT_separate_shader_objects');
  GL_EXT_shader_framebuffer_fetch:=OpenGLESlib_ExtensionGet('GL_EXT_shader_framebuffer_fetch');
  GL_EXT_shader_framebuffer_fetch_non_coherent:=OpenGLESlib_ExtensionGet('GL_EXT_shader_framebuffer_fetch_non_coherent');
  GL_EXT_shader_group_vote:=OpenGLESlib_ExtensionGet('GL_EXT_shader_group_vote');
  GL_EXT_shader_implicit_conversions:=OpenGLESlib_ExtensionGet('GL_EXT_shader_implicit_conversions');
  GL_EXT_shader_integer_mix:=OpenGLESlib_ExtensionGet('GL_EXT_shader_integer_mix');
  GL_EXT_shader_io_blocks:=OpenGLESlib_ExtensionGet('GL_EXT_shader_io_blocks');
  GL_EXT_shader_non_constant_global_initializers:=OpenGLESlib_ExtensionGet('GL_EXT_shader_non_constant_global_initializers');
  GL_EXT_shader_pixel_local_storage:=OpenGLESlib_ExtensionGet('GL_EXT_shader_pixel_local_storage');
  GL_EXT_shader_pixel_local_storage2:=OpenGLESlib_ExtensionGet('GL_EXT_shader_pixel_local_storage2');
  GL_EXT_shader_texture_lod:=OpenGLESlib_ExtensionGet('GL_EXT_shader_texture_lod');
  GL_EXT_shadow_samplers:=OpenGLESlib_ExtensionGet('GL_EXT_shadow_samplers');
  GL_EXT_sparse_texture:=OpenGLESlib_ExtensionGet('GL_EXT_sparse_texture');
  GL_EXT_sparse_texture2:=OpenGLESlib_ExtensionGet('GL_EXT_sparse_texture2');
  GL_EXT_tessellation_point_size:=OpenGLESlib_ExtensionGet('GL_EXT_tessellation_point_size');
  GL_EXT_tessellation_shader:=OpenGLESlib_ExtensionGet('GL_EXT_tessellation_shader');
  GL_EXT_texture_border_clamp:=OpenGLESlib_ExtensionGet('GL_EXT_texture_border_clamp');
  GL_EXT_texture_buffer:=OpenGLESlib_ExtensionGet('GL_EXT_texture_buffer');
  GL_EXT_texture_compression_astc_decode_mode:=OpenGLESlib_ExtensionGet('GL_EXT_texture_compression_astc_decode_mode');
  GL_EXT_texture_compression_bptc:=OpenGLESlib_ExtensionGet('GL_EXT_texture_compression_bptc');
  GL_EXT_texture_compression_dxt1:=OpenGLESlib_ExtensionGet('GL_EXT_texture_compression_dxt1');
  GL_EXT_texture_compression_rgtc:=OpenGLESlib_ExtensionGet('GL_EXT_texture_compression_rgtc');
  GL_EXT_texture_compression_s3tc:=OpenGLESlib_ExtensionGet('GL_EXT_texture_compression_s3tc');
  GL_EXT_texture_compression_s3tc_srgb:=OpenGLESlib_ExtensionGet('GL_EXT_texture_compression_s3tc_srgb');
  GL_EXT_texture_cube_map_array:=OpenGLESlib_ExtensionGet('GL_EXT_texture_cube_map_array');
  GL_EXT_texture_filter_anisotropic:=OpenGLESlib_ExtensionGet('GL_EXT_texture_filter_anisotropic');
  GL_EXT_texture_filter_minmax:=OpenGLESlib_ExtensionGet('GL_EXT_texture_filter_minmax');
  GL_EXT_texture_format_BGRA8888:=OpenGLESlib_ExtensionGet('GL_EXT_texture_format_BGRA8888');
  GL_EXT_texture_format_sRGB_override:=OpenGLESlib_ExtensionGet('GL_EXT_texture_format_sRGB_override');
  GL_EXT_texture_mirror_clamp_to_edge:=OpenGLESlib_ExtensionGet('GL_EXT_texture_mirror_clamp_to_edge');
  GL_EXT_texture_norm16:=OpenGLESlib_ExtensionGet('GL_EXT_texture_norm16');
  GL_EXT_texture_query_lod:=OpenGLESlib_ExtensionGet('GL_EXT_texture_query_lod');
  GL_EXT_texture_rg:=OpenGLESlib_ExtensionGet('GL_EXT_texture_rg');
  GL_EXT_texture_sRGB_R8:=OpenGLESlib_ExtensionGet('GL_EXT_texture_sRGB_R8');
  GL_EXT_texture_sRGB_RG8:=OpenGLESlib_ExtensionGet('GL_EXT_texture_sRGB_RG8');
  GL_EXT_texture_sRGB_decode:=OpenGLESlib_ExtensionGet('GL_EXT_texture_sRGB_decode');
  GL_EXT_texture_storage:=OpenGLESlib_ExtensionGet('GL_EXT_texture_storage');
  GL_EXT_texture_type_2_10_10_10_REV:=OpenGLESlib_ExtensionGet('GL_EXT_texture_type_2_10_10_10_REV');
  GL_EXT_texture_view:=OpenGLESlib_ExtensionGet('GL_EXT_texture_view');
  GL_NV_timeline_semaphore:=OpenGLESlib_ExtensionGet('GL_NV_timeline_semaphore');
  GL_EXT_unpack_subimage:=OpenGLESlib_ExtensionGet('GL_EXT_unpack_subimage');
  GL_EXT_win32_keyed_mutex:=OpenGLESlib_ExtensionGet('GL_EXT_win32_keyed_mutex');
  GL_EXT_window_rectangles:=OpenGLESlib_ExtensionGet('GL_EXT_window_rectangles');
  GL_FJ_shader_binary_GCCSO:=OpenGLESlib_ExtensionGet('GL_FJ_shader_binary_GCCSO');
  GL_IMG_bindless_texture:=OpenGLESlib_ExtensionGet('GL_IMG_bindless_texture');
  GL_IMG_framebuffer_downsample:=OpenGLESlib_ExtensionGet('GL_IMG_framebuffer_downsample');
  GL_IMG_multisampled_render_to_texture:=OpenGLESlib_ExtensionGet('GL_IMG_multisampled_render_to_texture');
  GL_IMG_program_binary:=OpenGLESlib_ExtensionGet('GL_IMG_program_binary');
  GL_IMG_read_format:=OpenGLESlib_ExtensionGet('GL_IMG_read_format');
  GL_IMG_shader_binary:=OpenGLESlib_ExtensionGet('GL_IMG_shader_binary');
  GL_IMG_texture_compression_pvrtc:=OpenGLESlib_ExtensionGet('GL_IMG_texture_compression_pvrtc');
  GL_IMG_texture_compression_pvrtc2:=OpenGLESlib_ExtensionGet('GL_IMG_texture_compression_pvrtc2');
  GL_IMG_texture_filter_cubic:=OpenGLESlib_ExtensionGet('GL_IMG_texture_filter_cubic');
  GL_INTEL_conservative_rasterization:=OpenGLESlib_ExtensionGet('GL_INTEL_conservative_rasterization');
  GL_INTEL_framebuffer_CMAA:=OpenGLESlib_ExtensionGet('GL_INTEL_framebuffer_CMAA');
  GL_INTEL_blackhole_render:=OpenGLESlib_ExtensionGet('GL_INTEL_blackhole_render');
  GL_INTEL_performance_query:=OpenGLESlib_ExtensionGet('GL_INTEL_performance_query');
  GL_KHR_blend_equation_advanced:=OpenGLESlib_ExtensionGet('GL_KHR_blend_equation_advanced');
  GL_KHR_blend_equation_advanced_coherent:=OpenGLESlib_ExtensionGet('GL_KHR_blend_equation_advanced_coherent');
  GL_KHR_context_flush_control:=OpenGLESlib_ExtensionGet('GL_KHR_context_flush_control');
  GL_KHR_debug:=OpenGLESlib_ExtensionGet('GL_KHR_debug');
  GL_KHR_no_error:=OpenGLESlib_ExtensionGet('GL_KHR_no_error');
  GL_KHR_robust_buffer_access_behavior:=OpenGLESlib_ExtensionGet('GL_KHR_robust_buffer_access_behavior');
  GL_KHR_robustness:=OpenGLESlib_ExtensionGet('GL_KHR_robustness');
  GL_KHR_shader_subgroup:=OpenGLESlib_ExtensionGet('GL_KHR_shader_subgroup');
  GL_KHR_texture_compression_astc_hdr:=OpenGLESlib_ExtensionGet('GL_KHR_texture_compression_astc_hdr');
  GL_KHR_texture_compression_astc_ldr:=OpenGLESlib_ExtensionGet('GL_KHR_texture_compression_astc_ldr');
  GL_KHR_texture_compression_astc_sliced_3d:=OpenGLESlib_ExtensionGet('GL_KHR_texture_compression_astc_sliced_3d');
  GL_KHR_parallel_shader_compile:=OpenGLESlib_ExtensionGet('GL_KHR_parallel_shader_compile');
  GL_MESA_framebuffer_flip_x:=OpenGLESlib_ExtensionGet('GL_MESA_framebuffer_flip_x');
  GL_MESA_framebuffer_flip_y:=OpenGLESlib_ExtensionGet('GL_MESA_framebuffer_flip_y');
  GL_MESA_framebuffer_swap_xy:=OpenGLESlib_ExtensionGet('GL_MESA_framebuffer_swap_xy');
  GL_MESA_program_binary_formats:=OpenGLESlib_ExtensionGet('GL_MESA_program_binary_formats');
  GL_MESA_shader_integer_functions:=OpenGLESlib_ExtensionGet('GL_MESA_shader_integer_functions');
  GL_NVX_blend_equation_advanced_multi_draw_buffers:=OpenGLESlib_ExtensionGet('GL_NVX_blend_equation_advanced_multi_draw_buffers');
  GL_NV_bindless_texture:=OpenGLESlib_ExtensionGet('GL_NV_bindless_texture');
  GL_NV_blend_equation_advanced:=OpenGLESlib_ExtensionGet('GL_NV_blend_equation_advanced');
  GL_NV_blend_equation_advanced_coherent:=OpenGLESlib_ExtensionGet('GL_NV_blend_equation_advanced_coherent');
  GL_NV_blend_minmax_factor:=OpenGLESlib_ExtensionGet('GL_NV_blend_minmax_factor');
  GL_NV_clip_space_w_scaling:=OpenGLESlib_ExtensionGet('GL_NV_clip_space_w_scaling');
  GL_NV_compute_shader_derivatives:=OpenGLESlib_ExtensionGet('GL_NV_compute_shader_derivatives');
  GL_NV_conditional_render:=OpenGLESlib_ExtensionGet('GL_NV_conditional_render');
  GL_NV_conservative_raster:=OpenGLESlib_ExtensionGet('GL_NV_conservative_raster');
  GL_NV_conservative_raster_pre_snap:=OpenGLESlib_ExtensionGet('GL_NV_conservative_raster_pre_snap');
  GL_NV_conservative_raster_pre_snap_triangles:=OpenGLESlib_ExtensionGet('GL_NV_conservative_raster_pre_snap_triangles');
  GL_NV_copy_buffer:=OpenGLESlib_ExtensionGet('GL_NV_copy_buffer');
  GL_NV_coverage_sample:=OpenGLESlib_ExtensionGet('GL_NV_coverage_sample');
  GL_NV_depth_nonlinear:=OpenGLESlib_ExtensionGet('GL_NV_depth_nonlinear');
  GL_NV_draw_buffers:=OpenGLESlib_ExtensionGet('GL_NV_draw_buffers');
  GL_NV_draw_instanced:=OpenGLESlib_ExtensionGet('GL_NV_draw_instanced');
  GL_NV_draw_vulkan_image:=OpenGLESlib_ExtensionGet('GL_NV_draw_vulkan_image');
  GL_NV_explicit_attrib_location:=OpenGLESlib_ExtensionGet('GL_NV_explicit_attrib_location');
  GL_NV_fbo_color_attachments:=OpenGLESlib_ExtensionGet('GL_NV_fbo_color_attachments');
  GL_NV_fence:=OpenGLESlib_ExtensionGet('GL_NV_fence');
  GL_NV_fill_rectangle:=OpenGLESlib_ExtensionGet('GL_NV_fill_rectangle');
  GL_NV_fragment_coverage_to_color:=OpenGLESlib_ExtensionGet('GL_NV_fragment_coverage_to_color');
  GL_NV_fragment_shader_barycentric:=OpenGLESlib_ExtensionGet('GL_NV_fragment_shader_barycentric');
  GL_NV_fragment_shader_interlock:=OpenGLESlib_ExtensionGet('GL_NV_fragment_shader_interlock');
  GL_NV_framebuffer_blit:=OpenGLESlib_ExtensionGet('GL_NV_framebuffer_blit');
  GL_NV_framebuffer_mixed_samples:=OpenGLESlib_ExtensionGet('GL_NV_framebuffer_mixed_samples');
  GL_NV_framebuffer_multisample:=OpenGLESlib_ExtensionGet('GL_NV_framebuffer_multisample');
  GL_NV_generate_mipmap_sRGB:=OpenGLESlib_ExtensionGet('GL_NV_generate_mipmap_sRGB');
  GL_NV_geometry_shader_passthrough:=OpenGLESlib_ExtensionGet('GL_NV_geometry_shader_passthrough');
  GL_NV_gpu_shader5:=OpenGLESlib_ExtensionGet('GL_NV_gpu_shader5');
  GL_NV_image_formats:=OpenGLESlib_ExtensionGet('GL_NV_image_formats');
  GL_NV_instanced_arrays:=OpenGLESlib_ExtensionGet('GL_NV_instanced_arrays');
  GL_NV_internalformat_sample_query:=OpenGLESlib_ExtensionGet('GL_NV_internalformat_sample_query');
  GL_NV_memory_attachment:=OpenGLESlib_ExtensionGet('GL_NV_memory_attachment');
  GL_NV_memory_object_sparse:=OpenGLESlib_ExtensionGet('GL_NV_memory_object_sparse');
  GL_NV_mesh_shader:=OpenGLESlib_ExtensionGet('GL_NV_mesh_shader');
  GL_NV_non_square_matrices:=OpenGLESlib_ExtensionGet('GL_NV_non_square_matrices');
  GL_NV_path_rendering:=OpenGLESlib_ExtensionGet('GL_NV_path_rendering');
  GL_NV_path_rendering_shared_edge:=OpenGLESlib_ExtensionGet('GL_NV_path_rendering_shared_edge');
  GL_NV_pixel_buffer_object:=OpenGLESlib_ExtensionGet('GL_NV_pixel_buffer_object');
  GL_NV_polygon_mode:=OpenGLESlib_ExtensionGet('GL_NV_polygon_mode');
  GL_NV_read_buffer:=OpenGLESlib_ExtensionGet('GL_NV_read_buffer');
  GL_NV_read_buffer_front:=OpenGLESlib_ExtensionGet('GL_NV_read_buffer_front');
  GL_NV_read_depth:=OpenGLESlib_ExtensionGet('GL_NV_read_depth');
  GL_NV_read_depth_stencil:=OpenGLESlib_ExtensionGet('GL_NV_read_depth_stencil');
  GL_NV_read_stencil:=OpenGLESlib_ExtensionGet('GL_NV_read_stencil');
  GL_NV_representative_fragment_test:=OpenGLESlib_ExtensionGet('GL_NV_representative_fragment_test');
  GL_NV_sRGB_formats:=OpenGLESlib_ExtensionGet('GL_NV_sRGB_formats');
  GL_NV_sample_locations:=OpenGLESlib_ExtensionGet('GL_NV_sample_locations');
  GL_NV_sample_mask_override_coverage:=OpenGLESlib_ExtensionGet('GL_NV_sample_mask_override_coverage');
  GL_NV_scissor_exclusive:=OpenGLESlib_ExtensionGet('GL_NV_scissor_exclusive');
  GL_NV_shader_atomic_fp16_vector:=OpenGLESlib_ExtensionGet('GL_NV_shader_atomic_fp16_vector');
  GL_NV_shader_noperspective_interpolation:=OpenGLESlib_ExtensionGet('GL_NV_shader_noperspective_interpolation');
  GL_NV_shader_subgroup_partitioned:=OpenGLESlib_ExtensionGet('GL_NV_shader_subgroup_partitioned');
  GL_NV_shader_texture_footprint:=OpenGLESlib_ExtensionGet('GL_NV_shader_texture_footprint');
  GL_NV_shading_rate_image:=OpenGLESlib_ExtensionGet('GL_NV_shading_rate_image');
  GL_NV_shadow_samplers_array:=OpenGLESlib_ExtensionGet('GL_NV_shadow_samplers_array');
  GL_NV_shadow_samplers_cube:=OpenGLESlib_ExtensionGet('GL_NV_shadow_samplers_cube');
  GL_NV_stereo_view_rendering:=OpenGLESlib_ExtensionGet('GL_NV_stereo_view_rendering');
  GL_NV_texture_border_clamp:=OpenGLESlib_ExtensionGet('GL_NV_texture_border_clamp');
  GL_NV_texture_compression_s3tc_update:=OpenGLESlib_ExtensionGet('GL_NV_texture_compression_s3tc_update');
  GL_NV_texture_npot_2D_mipmap:=OpenGLESlib_ExtensionGet('GL_NV_texture_npot_2D_mipmap');
  GL_NV_viewport_array:=OpenGLESlib_ExtensionGet('GL_NV_viewport_array');
  GL_NV_viewport_array2:=OpenGLESlib_ExtensionGet('GL_NV_viewport_array2');
  GL_NV_viewport_swizzle:=OpenGLESlib_ExtensionGet('GL_NV_viewport_swizzle');
  GL_OES_EGL_image:=OpenGLESlib_ExtensionGet('GL_OES_EGL_image');
  GL_OES_EGL_image_external:=OpenGLESlib_ExtensionGet('GL_OES_EGL_image_external');
  GL_OES_EGL_image_external_essl3:=OpenGLESlib_ExtensionGet('GL_OES_EGL_image_external_essl3');
  GL_OES_compressed_ETC1_RGB8_sub_texture:=OpenGLESlib_ExtensionGet('GL_OES_compressed_ETC1_RGB8_sub_texture');
  GL_OES_compressed_ETC1_RGB8_texture:=OpenGLESlib_ExtensionGet('GL_OES_compressed_ETC1_RGB8_texture');
  GL_OES_compressed_paletted_texture:=OpenGLESlib_ExtensionGet('GL_OES_compressed_paletted_texture');
  GL_OES_copy_image:=OpenGLESlib_ExtensionGet('GL_OES_copy_image');
  GL_OES_depth24:=OpenGLESlib_ExtensionGet('GL_OES_depth24');
  GL_OES_depth32:=OpenGLESlib_ExtensionGet('GL_OES_depth32');
  GL_OES_depth_texture:=OpenGLESlib_ExtensionGet('GL_OES_depth_texture');
  GL_OES_draw_buffers_indexed:=OpenGLESlib_ExtensionGet('GL_OES_draw_buffers_indexed');
  GL_OES_draw_elements_base_vertex:=OpenGLESlib_ExtensionGet('GL_OES_draw_elements_base_vertex');
  GL_OES_element_index_uint:=OpenGLESlib_ExtensionGet('GL_OES_element_index_uint');
  GL_OES_fbo_render_mipmap:=OpenGLESlib_ExtensionGet('GL_OES_fbo_render_mipmap');
  GL_OES_fragment_precision_high:=OpenGLESlib_ExtensionGet('GL_OES_fragment_precision_high');
  GL_OES_geometry_point_size:=OpenGLESlib_ExtensionGet('GL_OES_geometry_point_size');
  GL_OES_geometry_shader:=OpenGLESlib_ExtensionGet('GL_OES_geometry_shader');
  GL_OES_get_program_binary:=OpenGLESlib_ExtensionGet('GL_OES_get_program_binary');
  GL_OES_gpu_shader5:=OpenGLESlib_ExtensionGet('GL_OES_gpu_shader5');
  GL_OES_mapbuffer:=OpenGLESlib_ExtensionGet('GL_OES_mapbuffer');
  GL_OES_packed_depth_stencil:=OpenGLESlib_ExtensionGet('GL_OES_packed_depth_stencil');
  GL_OES_primitive_bounding_box:=OpenGLESlib_ExtensionGet('GL_OES_primitive_bounding_box');
  GL_OES_required_internalformat:=OpenGLESlib_ExtensionGet('GL_OES_required_internalformat');
  GL_OES_rgb8_rgba8:=OpenGLESlib_ExtensionGet('GL_OES_rgb8_rgba8');
  GL_OES_sample_shading:=OpenGLESlib_ExtensionGet('GL_OES_sample_shading');
  GL_OES_sample_variables:=OpenGLESlib_ExtensionGet('GL_OES_sample_variables');
  GL_OES_shader_image_atomic:=OpenGLESlib_ExtensionGet('GL_OES_shader_image_atomic');
  GL_OES_shader_io_blocks:=OpenGLESlib_ExtensionGet('GL_OES_shader_io_blocks');
  GL_OES_shader_multisample_interpolation:=OpenGLESlib_ExtensionGet('GL_OES_shader_multisample_interpolation');
  GL_OES_standard_derivatives:=OpenGLESlib_ExtensionGet('GL_OES_standard_derivatives');
  GL_OES_stencil1:=OpenGLESlib_ExtensionGet('GL_OES_stencil1');
  GL_OES_stencil4:=OpenGLESlib_ExtensionGet('GL_OES_stencil4');
  GL_OES_surfaceless_context:=OpenGLESlib_ExtensionGet('GL_OES_surfaceless_context');
  GL_OES_tessellation_point_size:=OpenGLESlib_ExtensionGet('GL_OES_tessellation_point_size');
  GL_OES_tessellation_shader:=OpenGLESlib_ExtensionGet('GL_OES_tessellation_shader');
  GL_OES_texture_3D:=OpenGLESlib_ExtensionGet('GL_OES_texture_3D');
  GL_OES_texture_border_clamp:=OpenGLESlib_ExtensionGet('GL_OES_texture_border_clamp');
  GL_OES_texture_buffer:=OpenGLESlib_ExtensionGet('GL_OES_texture_buffer');
  GL_OES_texture_compression_astc:=OpenGLESlib_ExtensionGet('GL_OES_texture_compression_astc');
  GL_OES_texture_cube_map_array:=OpenGLESlib_ExtensionGet('GL_OES_texture_cube_map_array');
  GL_OES_texture_float:=OpenGLESlib_ExtensionGet('GL_OES_texture_float');
  GL_OES_texture_float_linear:=OpenGLESlib_ExtensionGet('GL_OES_texture_float_linear');
  GL_OES_texture_half_float:=OpenGLESlib_ExtensionGet('GL_OES_texture_half_float');
  GL_OES_texture_half_float_linear:=OpenGLESlib_ExtensionGet('GL_OES_texture_half_float_linear');
  GL_OES_texture_npot:=OpenGLESlib_ExtensionGet('GL_OES_texture_npot');
  GL_OES_texture_stencil8:=OpenGLESlib_ExtensionGet('GL_OES_texture_stencil8');
  GL_OES_texture_storage_multisample_2d_array:=OpenGLESlib_ExtensionGet('GL_OES_texture_storage_multisample_2d_array');
  GL_OES_texture_view:=OpenGLESlib_ExtensionGet('GL_OES_texture_view');
  GL_OES_vertex_array_object:=OpenGLESlib_ExtensionGet('GL_OES_vertex_array_object');
  GL_OES_vertex_half_float:=OpenGLESlib_ExtensionGet('GL_OES_vertex_half_float');
  GL_OES_vertex_type_10_10_10_2:=OpenGLESlib_ExtensionGet('GL_OES_vertex_type_10_10_10_2');
  GL_OES_viewport_array:=OpenGLESlib_ExtensionGet('GL_OES_viewport_array');
  GL_OVR_multiview:=OpenGLESlib_ExtensionGet('GL_OVR_multiview');
  GL_OVR_multiview2:=OpenGLESlib_ExtensionGet('GL_OVR_multiview2');
  GL_OVR_multiview_multisampled_render_to_texture:=OpenGLESlib_ExtensionGet('GL_OVR_multiview_multisampled_render_to_texture');
  GL_QCOM_alpha_test:=OpenGLESlib_ExtensionGet('GL_QCOM_alpha_test');
  GL_QCOM_binning_control:=OpenGLESlib_ExtensionGet('GL_QCOM_binning_control');
  GL_QCOM_driver_control:=OpenGLESlib_ExtensionGet('GL_QCOM_driver_control');
  GL_QCOM_extended_get:=OpenGLESlib_ExtensionGet('GL_QCOM_extended_get');
  GL_QCOM_extended_get2:=OpenGLESlib_ExtensionGet('GL_QCOM_extended_get2');
  GL_QCOM_framebuffer_foveated:=OpenGLESlib_ExtensionGet('GL_QCOM_framebuffer_foveated');
  GL_QCOM_motion_estimation:=OpenGLESlib_ExtensionGet('GL_QCOM_motion_estimation');
  GL_QCOM_texture_foveated:=OpenGLESlib_ExtensionGet('GL_QCOM_texture_foveated');
  GL_QCOM_texture_foveated_subsampled_layout:=OpenGLESlib_ExtensionGet('GL_QCOM_texture_foveated_subsampled_layout');
  GL_QCOM_perfmon_global_mode:=OpenGLESlib_ExtensionGet('GL_QCOM_perfmon_global_mode');
  GL_QCOM_shader_framebuffer_fetch_noncoherent:=OpenGLESlib_ExtensionGet('GL_QCOM_shader_framebuffer_fetch_noncoherent');
  GL_QCOM_shader_framebuffer_fetch_rate:=OpenGLESlib_ExtensionGet('GL_QCOM_shader_framebuffer_fetch_rate');
  GL_QCOM_shading_rate:=OpenGLESlib_ExtensionGet('GL_QCOM_shading_rate');
  GL_QCOM_tiled_rendering:=OpenGLESlib_ExtensionGet('GL_QCOM_tiled_rendering');
  GL_QCOM_writeonly_rendering:=OpenGLESlib_ExtensionGet('GL_QCOM_writeonly_rendering');
  GL_QCOM_YUV_texture_gather:=OpenGLESlib_ExtensionGet('GL_QCOM_YUV_texture_gather');
  GL_VIV_shader_binary:=OpenGLESlib_ExtensionGet('GL_VIV_shader_binary');
  GL_EXT_texture_shadow_lod:=OpenGLESlib_ExtensionGet('GL_EXT_texture_shadow_lod');
 
//---- EGL ----
  EGL_ANDROID_blob_cache:=OpenEGLlib_ExtensionGet('EGL_ANDROID_blob_cache');
  EGL_ANDROID_blob_cache:=OpenGLESlib_ExtensionGet('EGL_ANDROID_blob_cache');
  EGL_ANDROID_create_native_client_buffer:=OpenEGLlib_ExtensionGet('EGL_ANDROID_create_native_client_buffer');
  EGL_ANDROID_create_native_client_buffer:=OpenGLESlib_ExtensionGet('EGL_ANDROID_create_native_client_buffer');
  EGL_ANDROID_framebuffer_target:=OpenEGLlib_ExtensionGet('EGL_ANDROID_framebuffer_target');
  EGL_ANDROID_framebuffer_target:=OpenGLESlib_ExtensionGet('EGL_ANDROID_framebuffer_target');
  EGL_ANDROID_get_native_client_buffer:=OpenEGLlib_ExtensionGet('EGL_ANDROID_get_native_client_buffer');
  EGL_ANDROID_get_native_client_buffer:=OpenGLESlib_ExtensionGet('EGL_ANDROID_get_native_client_buffer');
  EGL_ANDROID_front_buffer_auto_refresh:=OpenEGLlib_ExtensionGet('EGL_ANDROID_front_buffer_auto_refresh');
  EGL_ANDROID_front_buffer_auto_refresh:=OpenGLESlib_ExtensionGet('EGL_ANDROID_front_buffer_auto_refresh');
  EGL_ANDROID_image_native_buffer:=OpenEGLlib_ExtensionGet('EGL_ANDROID_image_native_buffer');
  EGL_ANDROID_image_native_buffer:=OpenGLESlib_ExtensionGet('EGL_ANDROID_image_native_buffer');
  EGL_ANDROID_native_fence_sync:=OpenEGLlib_ExtensionGet('EGL_ANDROID_native_fence_sync');
  EGL_ANDROID_native_fence_sync:=OpenGLESlib_ExtensionGet('EGL_ANDROID_native_fence_sync');
  EGL_ANDROID_presentation_time:=OpenEGLlib_ExtensionGet('EGL_ANDROID_presentation_time');
  EGL_ANDROID_presentation_time:=OpenGLESlib_ExtensionGet('EGL_ANDROID_presentation_time');
  EGL_ANDROID_get_frame_timestamps:=OpenEGLlib_ExtensionGet('EGL_ANDROID_get_frame_timestamps');
  EGL_ANDROID_get_frame_timestamps:=OpenGLESlib_ExtensionGet('EGL_ANDROID_get_frame_timestamps');
  EGL_ANDROID_recordable:=OpenEGLlib_ExtensionGet('EGL_ANDROID_recordable');
  EGL_ANDROID_recordable:=OpenGLESlib_ExtensionGet('EGL_ANDROID_recordable');
  EGL_ANDROID_GLES_layers:=OpenEGLlib_ExtensionGet('EGL_ANDROID_GLES_layers');
  EGL_ANDROID_GLES_layers:=OpenGLESlib_ExtensionGet('EGL_ANDROID_GLES_layers');
  EGL_ANGLE_d3d_share_handle_client_buffer:=OpenEGLlib_ExtensionGet('EGL_ANGLE_d3d_share_handle_client_buffer');
  EGL_ANGLE_d3d_share_handle_client_buffer:=OpenGLESlib_ExtensionGet('EGL_ANGLE_d3d_share_handle_client_buffer');
  EGL_ANGLE_device_d3d:=OpenEGLlib_ExtensionGet('EGL_ANGLE_device_d3d');
  EGL_ANGLE_device_d3d:=OpenGLESlib_ExtensionGet('EGL_ANGLE_device_d3d');
  EGL_ANGLE_query_surface_pointer:=OpenEGLlib_ExtensionGet('EGL_ANGLE_query_surface_pointer');
  EGL_ANGLE_query_surface_pointer:=OpenGLESlib_ExtensionGet('EGL_ANGLE_query_surface_pointer');
  EGL_ANGLE_surface_d3d_texture_2d_share_handle:=OpenEGLlib_ExtensionGet('EGL_ANGLE_surface_d3d_texture_2d_share_handle');
  EGL_ANGLE_surface_d3d_texture_2d_share_handle:=OpenGLESlib_ExtensionGet('EGL_ANGLE_surface_d3d_texture_2d_share_handle');
  EGL_ANGLE_window_fixed_size:=OpenEGLlib_ExtensionGet('EGL_ANGLE_window_fixed_size');
  EGL_ANGLE_window_fixed_size:=OpenGLESlib_ExtensionGet('EGL_ANGLE_window_fixed_size');
  EGL_ARM_implicit_external_sync:=OpenEGLlib_ExtensionGet('EGL_ARM_implicit_external_sync');
  EGL_ARM_implicit_external_sync:=OpenGLESlib_ExtensionGet('EGL_ARM_implicit_external_sync');
  EGL_ARM_pixmap_multisample_discard:=OpenEGLlib_ExtensionGet('EGL_ARM_pixmap_multisample_discard');
  EGL_ARM_pixmap_multisample_discard:=OpenGLESlib_ExtensionGet('EGL_ARM_pixmap_multisample_discard');
  EGL_EXT_buffer_age:=OpenEGLlib_ExtensionGet('EGL_EXT_buffer_age');
  EGL_EXT_buffer_age:=OpenGLESlib_ExtensionGet('EGL_EXT_buffer_age');
  EGL_EXT_client_extensions:=OpenEGLlib_ExtensionGet('EGL_EXT_client_extensions');
  EGL_EXT_client_extensions:=OpenGLESlib_ExtensionGet('EGL_EXT_client_extensions');
  EGL_EXT_client_sync:=OpenEGLlib_ExtensionGet('EGL_EXT_client_sync');
  EGL_EXT_client_sync:=OpenGLESlib_ExtensionGet('EGL_EXT_client_sync');
  EGL_EXT_create_context_robustness:=OpenEGLlib_ExtensionGet('EGL_EXT_create_context_robustness');
  EGL_EXT_create_context_robustness:=OpenGLESlib_ExtensionGet('EGL_EXT_create_context_robustness');
  EGL_EXT_device_base:=OpenEGLlib_ExtensionGet('EGL_EXT_device_base');
  EGL_EXT_device_base:=OpenGLESlib_ExtensionGet('EGL_EXT_device_base');
  EGL_EXT_device_drm:=OpenEGLlib_ExtensionGet('EGL_EXT_device_drm');
  EGL_EXT_device_drm:=OpenGLESlib_ExtensionGet('EGL_EXT_device_drm');
  EGL_EXT_device_enumeration:=OpenEGLlib_ExtensionGet('EGL_EXT_device_enumeration');
  EGL_EXT_device_enumeration:=OpenGLESlib_ExtensionGet('EGL_EXT_device_enumeration');
  EGL_EXT_device_openwf:=OpenEGLlib_ExtensionGet('EGL_EXT_device_openwf');
  EGL_EXT_device_openwf:=OpenGLESlib_ExtensionGet('EGL_EXT_device_openwf');
  EGL_EXT_device_query:=OpenEGLlib_ExtensionGet('EGL_EXT_device_query');
  EGL_EXT_device_query:=OpenGLESlib_ExtensionGet('EGL_EXT_device_query');
  EGL_EXT_gl_colorspace_bt2020_linear:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_bt2020_linear');
  EGL_EXT_gl_colorspace_bt2020_linear:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_bt2020_linear');
  EGL_EXT_gl_colorspace_bt2020_pq:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_bt2020_pq');
  EGL_EXT_gl_colorspace_bt2020_pq:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_bt2020_pq');
  EGL_EXT_gl_colorspace_scrgb:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_scrgb');
  EGL_EXT_gl_colorspace_scrgb:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_scrgb');
  EGL_EXT_gl_colorspace_scrgb_linear:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_scrgb_linear');
  EGL_EXT_gl_colorspace_scrgb_linear:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_scrgb_linear');
  EGL_EXT_gl_colorspace_display_p3_linear:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_display_p3_linear');
  EGL_EXT_gl_colorspace_display_p3_linear:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_display_p3_linear');
  EGL_EXT_gl_colorspace_display_p3:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_display_p3');
  EGL_EXT_gl_colorspace_display_p3:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_display_p3');
  EGL_EXT_gl_colorspace_display_p3_passthrough:=OpenEGLlib_ExtensionGet('EGL_EXT_gl_colorspace_display_p3_passthrough');
  EGL_EXT_gl_colorspace_display_p3_passthrough:=OpenGLESlib_ExtensionGet('EGL_EXT_gl_colorspace_display_p3_passthrough');
  EGL_EXT_image_dma_buf_import:=OpenEGLlib_ExtensionGet('EGL_EXT_image_dma_buf_import');
  EGL_EXT_image_dma_buf_import:=OpenGLESlib_ExtensionGet('EGL_EXT_image_dma_buf_import');
  EGL_EXT_image_dma_buf_import_modifiers:=OpenEGLlib_ExtensionGet('EGL_EXT_image_dma_buf_import_modifiers');
  EGL_EXT_image_dma_buf_import_modifiers:=OpenGLESlib_ExtensionGet('EGL_EXT_image_dma_buf_import_modifiers');
  EGL_EXT_image_gl_colorspace:=OpenEGLlib_ExtensionGet('EGL_EXT_image_gl_colorspace');
  EGL_EXT_image_gl_colorspace:=OpenGLESlib_ExtensionGet('EGL_EXT_image_gl_colorspace');
  EGL_EXT_multiview_window:=OpenEGLlib_ExtensionGet('EGL_EXT_multiview_window');
  EGL_EXT_multiview_window:=OpenGLESlib_ExtensionGet('EGL_EXT_multiview_window');
  EGL_EXT_output_base:=OpenEGLlib_ExtensionGet('EGL_EXT_output_base');
  EGL_EXT_output_base:=OpenGLESlib_ExtensionGet('EGL_EXT_output_base');
  EGL_EXT_output_drm:=OpenEGLlib_ExtensionGet('EGL_EXT_output_drm');
  EGL_EXT_output_drm:=OpenGLESlib_ExtensionGet('EGL_EXT_output_drm');
  EGL_EXT_output_openwf:=OpenEGLlib_ExtensionGet('EGL_EXT_output_openwf');
  EGL_EXT_output_openwf:=OpenGLESlib_ExtensionGet('EGL_EXT_output_openwf');
  EGL_EXT_pixel_format_float:=OpenEGLlib_ExtensionGet('EGL_EXT_pixel_format_float');
  EGL_EXT_pixel_format_float:=OpenGLESlib_ExtensionGet('EGL_EXT_pixel_format_float');
  EGL_EXT_platform_base:=OpenEGLlib_ExtensionGet('EGL_EXT_platform_base');
  EGL_EXT_platform_base:=OpenGLESlib_ExtensionGet('EGL_EXT_platform_base');
  EGL_EXT_platform_device:=OpenEGLlib_ExtensionGet('EGL_EXT_platform_device');
  EGL_EXT_platform_device:=OpenGLESlib_ExtensionGet('EGL_EXT_platform_device');
  EGL_EXT_platform_wayland:=OpenEGLlib_ExtensionGet('EGL_EXT_platform_wayland');
  EGL_EXT_platform_wayland:=OpenGLESlib_ExtensionGet('EGL_EXT_platform_wayland');
  EGL_EXT_platform_x11:=OpenEGLlib_ExtensionGet('EGL_EXT_platform_x11');
  EGL_EXT_platform_x11:=OpenGLESlib_ExtensionGet('EGL_EXT_platform_x11');
  EGL_EXT_protected_content:=OpenEGLlib_ExtensionGet('EGL_EXT_protected_content');
  EGL_EXT_protected_content:=OpenGLESlib_ExtensionGet('EGL_EXT_protected_content');
  EGL_EXT_protected_surface:=OpenEGLlib_ExtensionGet('EGL_EXT_protected_surface');
  EGL_EXT_protected_surface:=OpenGLESlib_ExtensionGet('EGL_EXT_protected_surface');
  EGL_EXT_stream_consumer_egloutput:=OpenEGLlib_ExtensionGet('EGL_EXT_stream_consumer_egloutput');
  EGL_EXT_stream_consumer_egloutput:=OpenGLESlib_ExtensionGet('EGL_EXT_stream_consumer_egloutput');
  EGL_EXT_surface_SMPTE2086_metadata:=OpenEGLlib_ExtensionGet('EGL_EXT_surface_SMPTE2086_metadata');
  EGL_EXT_surface_SMPTE2086_metadata:=OpenGLESlib_ExtensionGet('EGL_EXT_surface_SMPTE2086_metadata');
  EGL_EXT_swap_buffers_with_damage:=OpenEGLlib_ExtensionGet('EGL_EXT_swap_buffers_with_damage');
  EGL_EXT_swap_buffers_with_damage:=OpenGLESlib_ExtensionGet('EGL_EXT_swap_buffers_with_damage');
  EGL_EXT_sync_reuse:=OpenEGLlib_ExtensionGet('EGL_EXT_sync_reuse');
  EGL_EXT_sync_reuse:=OpenGLESlib_ExtensionGet('EGL_EXT_sync_reuse');
  EGL_EXT_yuv_surface:=OpenEGLlib_ExtensionGet('EGL_EXT_yuv_surface');
  EGL_EXT_yuv_surface:=OpenGLESlib_ExtensionGet('EGL_EXT_yuv_surface');
  EGL_HI_clientpixmap:=OpenEGLlib_ExtensionGet('EGL_HI_clientpixmap');
  EGL_HI_clientpixmap:=OpenGLESlib_ExtensionGet('EGL_HI_clientpixmap');
  EGL_HI_colorformats:=OpenEGLlib_ExtensionGet('EGL_HI_colorformats');
  EGL_HI_colorformats:=OpenGLESlib_ExtensionGet('EGL_HI_colorformats');
  EGL_IMG_context_priority:=OpenEGLlib_ExtensionGet('EGL_IMG_context_priority');
  EGL_IMG_context_priority:=OpenGLESlib_ExtensionGet('EGL_IMG_context_priority');
  EGL_IMG_image_plane_attribs:=OpenEGLlib_ExtensionGet('EGL_IMG_image_plane_attribs');
  EGL_IMG_image_plane_attribs:=OpenGLESlib_ExtensionGet('EGL_IMG_image_plane_attribs');
  EGL_KHR_cl_event:=OpenEGLlib_ExtensionGet('EGL_KHR_cl_event');
  EGL_KHR_cl_event:=OpenGLESlib_ExtensionGet('EGL_KHR_cl_event');
  EGL_KHR_cl_event2:=OpenEGLlib_ExtensionGet('EGL_KHR_cl_event2');
  EGL_KHR_cl_event2:=OpenGLESlib_ExtensionGet('EGL_KHR_cl_event2');
  EGL_KHR_config_attribs:=OpenEGLlib_ExtensionGet('EGL_KHR_config_attribs');
  EGL_KHR_config_attribs:=OpenGLESlib_ExtensionGet('EGL_KHR_config_attribs');
  EGL_KHR_client_get_all_proc_addresses:=OpenEGLlib_ExtensionGet('EGL_KHR_client_get_all_proc_addresses');
  EGL_KHR_client_get_all_proc_addresses:=OpenGLESlib_ExtensionGet('EGL_KHR_client_get_all_proc_addresses');
  EGL_KHR_context_flush_control:=OpenEGLlib_ExtensionGet('EGL_KHR_context_flush_control');
  EGL_KHR_context_flush_control:=OpenGLESlib_ExtensionGet('EGL_KHR_context_flush_control');
  EGL_KHR_create_context:=OpenEGLlib_ExtensionGet('EGL_KHR_create_context');
  EGL_KHR_create_context:=OpenGLESlib_ExtensionGet('EGL_KHR_create_context');
  EGL_KHR_create_context_no_error:=OpenEGLlib_ExtensionGet('EGL_KHR_create_context_no_error');
  EGL_KHR_create_context_no_error:=OpenGLESlib_ExtensionGet('EGL_KHR_create_context_no_error');
  EGL_KHR_debug:=OpenEGLlib_ExtensionGet('EGL_KHR_debug');
  EGL_KHR_debug:=OpenGLESlib_ExtensionGet('EGL_KHR_debug');
  EGL_KHR_display_reference:=OpenEGLlib_ExtensionGet('EGL_KHR_display_reference');
  EGL_KHR_display_reference:=OpenGLESlib_ExtensionGet('EGL_KHR_display_reference');
  EGL_KHR_fence_sync:=OpenEGLlib_ExtensionGet('EGL_KHR_fence_sync');
  EGL_KHR_fence_sync:=OpenGLESlib_ExtensionGet('EGL_KHR_fence_sync');
  EGL_KHR_get_all_proc_addresses:=OpenEGLlib_ExtensionGet('EGL_KHR_get_all_proc_addresses');
  EGL_KHR_get_all_proc_addresses:=OpenGLESlib_ExtensionGet('EGL_KHR_get_all_proc_addresses');
  EGL_KHR_gl_colorspace:=OpenEGLlib_ExtensionGet('EGL_KHR_gl_colorspace');
  EGL_KHR_gl_colorspace:=OpenGLESlib_ExtensionGet('EGL_KHR_gl_colorspace');
  EGL_KHR_gl_renderbuffer_image:=OpenEGLlib_ExtensionGet('EGL_KHR_gl_renderbuffer_image');
  EGL_KHR_gl_renderbuffer_image:=OpenGLESlib_ExtensionGet('EGL_KHR_gl_renderbuffer_image');
  EGL_KHR_gl_texture_2D_image:=OpenEGLlib_ExtensionGet('EGL_KHR_gl_texture_2D_image');
  EGL_KHR_gl_texture_2D_image:=OpenGLESlib_ExtensionGet('EGL_KHR_gl_texture_2D_image');
  EGL_KHR_gl_texture_3D_image:=OpenEGLlib_ExtensionGet('EGL_KHR_gl_texture_3D_image');
  EGL_KHR_gl_texture_3D_image:=OpenGLESlib_ExtensionGet('EGL_KHR_gl_texture_3D_image');
  EGL_KHR_gl_texture_cubemap_image:=OpenEGLlib_ExtensionGet('EGL_KHR_gl_texture_cubemap_image');
  EGL_KHR_gl_texture_cubemap_image:=OpenGLESlib_ExtensionGet('EGL_KHR_gl_texture_cubemap_image');
  EGL_KHR_image:=OpenEGLlib_ExtensionGet('EGL_KHR_image');
  EGL_KHR_image:=OpenGLESlib_ExtensionGet('EGL_KHR_image');
  EGL_KHR_image_base:=OpenEGLlib_ExtensionGet('EGL_KHR_image_base');
  EGL_KHR_image_base:=OpenGLESlib_ExtensionGet('EGL_KHR_image_base');
  EGL_KHR_image_pixmap:=OpenEGLlib_ExtensionGet('EGL_KHR_image_pixmap');
  EGL_KHR_image_pixmap:=OpenGLESlib_ExtensionGet('EGL_KHR_image_pixmap');
  EGL_KHR_lock_surface:=OpenEGLlib_ExtensionGet('EGL_KHR_lock_surface');
  EGL_KHR_lock_surface:=OpenGLESlib_ExtensionGet('EGL_KHR_lock_surface');
  EGL_KHR_lock_surface2:=OpenEGLlib_ExtensionGet('EGL_KHR_lock_surface2');
  EGL_KHR_lock_surface2:=OpenGLESlib_ExtensionGet('EGL_KHR_lock_surface2');
  EGL_KHR_lock_surface3:=OpenEGLlib_ExtensionGet('EGL_KHR_lock_surface3');
  EGL_KHR_lock_surface3:=OpenGLESlib_ExtensionGet('EGL_KHR_lock_surface3');
  EGL_KHR_mutable_render_buffer:=OpenEGLlib_ExtensionGet('EGL_KHR_mutable_render_buffer');
  EGL_KHR_mutable_render_buffer:=OpenGLESlib_ExtensionGet('EGL_KHR_mutable_render_buffer');
  EGL_KHR_no_config_context:=OpenEGLlib_ExtensionGet('EGL_KHR_no_config_context');
  EGL_KHR_no_config_context:=OpenGLESlib_ExtensionGet('EGL_KHR_no_config_context');
  EGL_KHR_partial_update:=OpenEGLlib_ExtensionGet('EGL_KHR_partial_update');
  EGL_KHR_partial_update:=OpenGLESlib_ExtensionGet('EGL_KHR_partial_update');
  EGL_KHR_platform_android:=OpenEGLlib_ExtensionGet('EGL_KHR_platform_android');
  EGL_KHR_platform_android:=OpenGLESlib_ExtensionGet('EGL_KHR_platform_android');
  EGL_KHR_platform_gbm:=OpenEGLlib_ExtensionGet('EGL_KHR_platform_gbm');
  EGL_KHR_platform_gbm:=OpenGLESlib_ExtensionGet('EGL_KHR_platform_gbm');
  EGL_KHR_platform_wayland:=OpenEGLlib_ExtensionGet('EGL_KHR_platform_wayland');
  EGL_KHR_platform_wayland:=OpenGLESlib_ExtensionGet('EGL_KHR_platform_wayland');
  EGL_KHR_platform_x11:=OpenEGLlib_ExtensionGet('EGL_KHR_platform_x11');
  EGL_KHR_platform_x11:=OpenGLESlib_ExtensionGet('EGL_KHR_platform_x11');
  EGL_KHR_reusable_sync:=OpenEGLlib_ExtensionGet('EGL_KHR_reusable_sync');
  EGL_KHR_reusable_sync:=OpenGLESlib_ExtensionGet('EGL_KHR_reusable_sync');
  EGL_KHR_stream:=OpenEGLlib_ExtensionGet('EGL_KHR_stream');
  EGL_KHR_stream:=OpenGLESlib_ExtensionGet('EGL_KHR_stream');
  EGL_KHR_stream_attrib:=OpenEGLlib_ExtensionGet('EGL_KHR_stream_attrib');
  EGL_KHR_stream_attrib:=OpenGLESlib_ExtensionGet('EGL_KHR_stream_attrib');
  EGL_KHR_stream_consumer_gltexture:=OpenEGLlib_ExtensionGet('EGL_KHR_stream_consumer_gltexture');
  EGL_KHR_stream_consumer_gltexture:=OpenGLESlib_ExtensionGet('EGL_KHR_stream_consumer_gltexture');
  EGL_KHR_stream_cross_process_fd:=OpenEGLlib_ExtensionGet('EGL_KHR_stream_cross_process_fd');
  EGL_KHR_stream_cross_process_fd:=OpenGLESlib_ExtensionGet('EGL_KHR_stream_cross_process_fd');
  EGL_KHR_stream_fifo:=OpenEGLlib_ExtensionGet('EGL_KHR_stream_fifo');
  EGL_KHR_stream_fifo:=OpenGLESlib_ExtensionGet('EGL_KHR_stream_fifo');
  EGL_KHR_stream_producer_aldatalocator:=OpenEGLlib_ExtensionGet('EGL_KHR_stream_producer_aldatalocator');
  EGL_KHR_stream_producer_aldatalocator:=OpenGLESlib_ExtensionGet('EGL_KHR_stream_producer_aldatalocator');
  EGL_KHR_stream_producer_eglsurface:=OpenEGLlib_ExtensionGet('EGL_KHR_stream_producer_eglsurface');
  EGL_KHR_stream_producer_eglsurface:=OpenGLESlib_ExtensionGet('EGL_KHR_stream_producer_eglsurface');
  EGL_KHR_surfaceless_context:=OpenEGLlib_ExtensionGet('EGL_KHR_surfaceless_context');
  EGL_KHR_surfaceless_context:=OpenGLESlib_ExtensionGet('EGL_KHR_surfaceless_context');
  EGL_KHR_swap_buffers_with_damage:=OpenEGLlib_ExtensionGet('EGL_KHR_swap_buffers_with_damage');
  EGL_KHR_swap_buffers_with_damage:=OpenGLESlib_ExtensionGet('EGL_KHR_swap_buffers_with_damage');
  EGL_KHR_vg_parent_image:=OpenEGLlib_ExtensionGet('EGL_KHR_vg_parent_image');
  EGL_KHR_vg_parent_image:=OpenGLESlib_ExtensionGet('EGL_KHR_vg_parent_image');
  EGL_KHR_wait_sync:=OpenEGLlib_ExtensionGet('EGL_KHR_wait_sync');
  EGL_KHR_wait_sync:=OpenGLESlib_ExtensionGet('EGL_KHR_wait_sync');
  EGL_MESA_drm_image:=OpenEGLlib_ExtensionGet('EGL_MESA_drm_image');
  EGL_MESA_drm_image:=OpenGLESlib_ExtensionGet('EGL_MESA_drm_image');
  EGL_MESA_image_dma_buf_export:=OpenEGLlib_ExtensionGet('EGL_MESA_image_dma_buf_export');
  EGL_MESA_image_dma_buf_export:=OpenGLESlib_ExtensionGet('EGL_MESA_image_dma_buf_export');
  EGL_MESA_platform_gbm:=OpenEGLlib_ExtensionGet('EGL_MESA_platform_gbm');
  EGL_MESA_platform_gbm:=OpenGLESlib_ExtensionGet('EGL_MESA_platform_gbm');
  EGL_MESA_platform_surfaceless:=OpenEGLlib_ExtensionGet('EGL_MESA_platform_surfaceless');
  EGL_MESA_platform_surfaceless:=OpenGLESlib_ExtensionGet('EGL_MESA_platform_surfaceless');
  EGL_MESA_query_driver:=OpenEGLlib_ExtensionGet('EGL_MESA_query_driver');
  EGL_MESA_query_driver:=OpenGLESlib_ExtensionGet('EGL_MESA_query_driver');
  EGL_NOK_swap_region:=OpenEGLlib_ExtensionGet('EGL_NOK_swap_region');
  EGL_NOK_swap_region:=OpenGLESlib_ExtensionGet('EGL_NOK_swap_region');
  EGL_NOK_swap_region2:=OpenEGLlib_ExtensionGet('EGL_NOK_swap_region2');
  EGL_NOK_swap_region2:=OpenGLESlib_ExtensionGet('EGL_NOK_swap_region2');
  EGL_NOK_texture_from_pixmap:=OpenEGLlib_ExtensionGet('EGL_NOK_texture_from_pixmap');
  EGL_NOK_texture_from_pixmap:=OpenGLESlib_ExtensionGet('EGL_NOK_texture_from_pixmap');
  EGL_NV_3dvision_surface:=OpenEGLlib_ExtensionGet('EGL_NV_3dvision_surface');
  EGL_NV_3dvision_surface:=OpenGLESlib_ExtensionGet('EGL_NV_3dvision_surface');
  EGL_NV_coverage_sample:=OpenEGLlib_ExtensionGet('EGL_NV_coverage_sample');
  EGL_NV_coverage_sample:=OpenGLESlib_ExtensionGet('EGL_NV_coverage_sample');
  EGL_NV_context_priority_realtime:=OpenEGLlib_ExtensionGet('EGL_NV_context_priority_realtime');
  EGL_NV_context_priority_realtime:=OpenGLESlib_ExtensionGet('EGL_NV_context_priority_realtime');
  EGL_NV_coverage_sample_resolve:=OpenEGLlib_ExtensionGet('EGL_NV_coverage_sample_resolve');
  EGL_NV_coverage_sample_resolve:=OpenGLESlib_ExtensionGet('EGL_NV_coverage_sample_resolve');
  EGL_NV_cuda_event:=OpenEGLlib_ExtensionGet('EGL_NV_cuda_event');
  EGL_NV_cuda_event:=OpenGLESlib_ExtensionGet('EGL_NV_cuda_event');
  EGL_NV_depth_nonlinear:=OpenEGLlib_ExtensionGet('EGL_NV_depth_nonlinear');
  EGL_NV_depth_nonlinear:=OpenGLESlib_ExtensionGet('EGL_NV_depth_nonlinear');
  EGL_NV_device_cuda:=OpenEGLlib_ExtensionGet('EGL_NV_device_cuda');
  EGL_NV_device_cuda:=OpenGLESlib_ExtensionGet('EGL_NV_device_cuda');
  EGL_NV_native_query:=OpenEGLlib_ExtensionGet('EGL_NV_native_query');
  EGL_NV_native_query:=OpenGLESlib_ExtensionGet('EGL_NV_native_query');
  EGL_NV_post_convert_rounding:=OpenEGLlib_ExtensionGet('EGL_NV_post_convert_rounding');
  EGL_NV_post_convert_rounding:=OpenGLESlib_ExtensionGet('EGL_NV_post_convert_rounding');
  EGL_NV_post_sub_buffer:=OpenEGLlib_ExtensionGet('EGL_NV_post_sub_buffer');
  EGL_NV_post_sub_buffer:=OpenGLESlib_ExtensionGet('EGL_NV_post_sub_buffer');
  EGL_NV_quadruple_buffer:=OpenEGLlib_ExtensionGet('EGL_NV_quadruple_buffer');
  EGL_NV_quadruple_buffer:=OpenGLESlib_ExtensionGet('EGL_NV_quadruple_buffer');
  EGL_NV_robustness_video_memory_purge:=OpenEGLlib_ExtensionGet('EGL_NV_robustness_video_memory_purge');
  EGL_NV_robustness_video_memory_purge:=OpenGLESlib_ExtensionGet('EGL_NV_robustness_video_memory_purge');
  EGL_NV_stream_consumer_gltexture_yuv:=OpenEGLlib_ExtensionGet('EGL_NV_stream_consumer_gltexture_yuv');
  EGL_NV_stream_consumer_gltexture_yuv:=OpenGLESlib_ExtensionGet('EGL_NV_stream_consumer_gltexture_yuv');
  EGL_NV_stream_cross_object:=OpenEGLlib_ExtensionGet('EGL_NV_stream_cross_object');
  EGL_NV_stream_cross_object:=OpenGLESlib_ExtensionGet('EGL_NV_stream_cross_object');
  EGL_NV_stream_cross_display:=OpenEGLlib_ExtensionGet('EGL_NV_stream_cross_display');
  EGL_NV_stream_cross_display:=OpenGLESlib_ExtensionGet('EGL_NV_stream_cross_display');
  EGL_NV_stream_cross_partition:=OpenEGLlib_ExtensionGet('EGL_NV_stream_cross_partition');
  EGL_NV_stream_cross_partition:=OpenGLESlib_ExtensionGet('EGL_NV_stream_cross_partition');
  EGL_NV_stream_cross_process:=OpenEGLlib_ExtensionGet('EGL_NV_stream_cross_process');
  EGL_NV_stream_cross_process:=OpenGLESlib_ExtensionGet('EGL_NV_stream_cross_process');
  EGL_NV_stream_cross_system:=OpenEGLlib_ExtensionGet('EGL_NV_stream_cross_system');
  EGL_NV_stream_cross_system:=OpenGLESlib_ExtensionGet('EGL_NV_stream_cross_system');
  EGL_NV_stream_dma:=OpenEGLlib_ExtensionGet('EGL_NV_stream_dma');
  EGL_NV_stream_dma:=OpenGLESlib_ExtensionGet('EGL_NV_stream_dma');
  EGL_NV_stream_consumer_eglimage:=OpenEGLlib_ExtensionGet('EGL_NV_stream_consumer_eglimage');
  EGL_NV_stream_consumer_eglimage:=OpenGLESlib_ExtensionGet('EGL_NV_stream_consumer_eglimage');
  EGL_NV_stream_fifo_next:=OpenEGLlib_ExtensionGet('EGL_NV_stream_fifo_next');
  EGL_NV_stream_fifo_next:=OpenGLESlib_ExtensionGet('EGL_NV_stream_fifo_next');
  EGL_NV_stream_fifo_synchronous:=OpenEGLlib_ExtensionGet('EGL_NV_stream_fifo_synchronous');
  EGL_NV_stream_fifo_synchronous:=OpenGLESlib_ExtensionGet('EGL_NV_stream_fifo_synchronous');
  EGL_NV_stream_flush:=OpenEGLlib_ExtensionGet('EGL_NV_stream_flush');
  EGL_NV_stream_flush:=OpenGLESlib_ExtensionGet('EGL_NV_stream_flush');
  EGL_NV_stream_frame_limits:=OpenEGLlib_ExtensionGet('EGL_NV_stream_frame_limits');
  EGL_NV_stream_frame_limits:=OpenGLESlib_ExtensionGet('EGL_NV_stream_frame_limits');
  EGL_NV_stream_metadata:=OpenEGLlib_ExtensionGet('EGL_NV_stream_metadata');
  EGL_NV_stream_metadata:=OpenGLESlib_ExtensionGet('EGL_NV_stream_metadata');
  EGL_NV_stream_reset:=OpenEGLlib_ExtensionGet('EGL_NV_stream_reset');
  EGL_NV_stream_reset:=OpenGLESlib_ExtensionGet('EGL_NV_stream_reset');
  EGL_NV_stream_remote:=OpenEGLlib_ExtensionGet('EGL_NV_stream_remote');
  EGL_NV_stream_remote:=OpenGLESlib_ExtensionGet('EGL_NV_stream_remote');
  EGL_NV_stream_socket:=OpenEGLlib_ExtensionGet('EGL_NV_stream_socket');
  EGL_NV_stream_socket:=OpenGLESlib_ExtensionGet('EGL_NV_stream_socket');
  EGL_NV_stream_socket_inet:=OpenEGLlib_ExtensionGet('EGL_NV_stream_socket_inet');
  EGL_NV_stream_socket_inet:=OpenGLESlib_ExtensionGet('EGL_NV_stream_socket_inet');
  EGL_NV_stream_socket_unix:=OpenEGLlib_ExtensionGet('EGL_NV_stream_socket_unix');
  EGL_NV_stream_socket_unix:=OpenGLESlib_ExtensionGet('EGL_NV_stream_socket_unix');
  EGL_NV_stream_sync:=OpenEGLlib_ExtensionGet('EGL_NV_stream_sync');
  EGL_NV_stream_sync:=OpenGLESlib_ExtensionGet('EGL_NV_stream_sync');
  EGL_NV_sync:=OpenEGLlib_ExtensionGet('EGL_NV_sync');
  EGL_NV_sync:=OpenGLESlib_ExtensionGet('EGL_NV_sync');
  EGL_NV_system_time:=OpenEGLlib_ExtensionGet('EGL_NV_system_time');
  EGL_NV_system_time:=OpenGLESlib_ExtensionGet('EGL_NV_system_time');
  EGL_NV_triple_buffer:=OpenEGLlib_ExtensionGet('EGL_NV_triple_buffer');
  EGL_NV_triple_buffer:=OpenGLESlib_ExtensionGet('EGL_NV_triple_buffer');
  EGL_TIZEN_image_native_buffer:=OpenEGLlib_ExtensionGet('EGL_TIZEN_image_native_buffer');
  EGL_TIZEN_image_native_buffer:=OpenGLESlib_ExtensionGet('EGL_TIZEN_image_native_buffer');
  EGL_TIZEN_image_native_surface:=OpenEGLlib_ExtensionGet('EGL_TIZEN_image_native_surface');
  EGL_TIZEN_image_native_surface:=OpenGLESlib_ExtensionGet('EGL_TIZEN_image_native_surface');
  EGL_EXT_compositor:=OpenEGLlib_ExtensionGet('EGL_EXT_compositor');
  EGL_EXT_compositor:=OpenGLESlib_ExtensionGet('EGL_EXT_compositor');
  EGL_EXT_surface_CTA861_3_metadata:=OpenEGLlib_ExtensionGet('EGL_EXT_surface_CTA861_3_metadata');
  EGL_EXT_surface_CTA861_3_metadata:=OpenGLESlib_ExtensionGet('EGL_EXT_surface_CTA861_3_metadata');
  EGL_EXT_image_implicit_sync_control:=OpenEGLlib_ExtensionGet('EGL_EXT_image_implicit_sync_control');
  EGL_EXT_image_implicit_sync_control:=OpenGLESlib_ExtensionGet('EGL_EXT_image_implicit_sync_control');
  EGL_EXT_bind_to_front:=OpenEGLlib_ExtensionGet('EGL_EXT_bind_to_front');
  EGL_EXT_bind_to_front:=OpenGLESlib_ExtensionGet('EGL_EXT_bind_to_front');
  EGL_NV_stream_origin:=OpenEGLlib_ExtensionGet('EGL_NV_stream_origin');
  EGL_NV_stream_origin:=OpenGLESlib_ExtensionGet('EGL_NV_stream_origin');
  EGL_WL_bind_wayland_display:=OpenEGLlib_ExtensionGet('EGL_WL_bind_wayland_display');
  EGL_WL_bind_wayland_display:=OpenGLESlib_ExtensionGet('EGL_WL_bind_wayland_display');
  EGL_WL_create_wayland_buffer_from_image:=OpenEGLlib_ExtensionGet('EGL_WL_create_wayland_buffer_from_image');
  EGL_WL_create_wayland_buffer_from_image:=OpenGLESlib_ExtensionGet('EGL_WL_create_wayland_buffer_from_image');
  EGL_ARM_image_format:=OpenEGLlib_ExtensionGet('EGL_ARM_image_format');
  EGL_ARM_image_format:=OpenGLESlib_ExtensionGet('EGL_ARM_image_format');
  EGL_EXT_device_query_name:=OpenEGLlib_ExtensionGet('EGL_EXT_device_query_name');
  EGL_EXT_device_query_name:=OpenGLESlib_ExtensionGet('EGL_EXT_device_query_name');
 
end;
 
function OpenGLES_Initialize(const alibEGL:string=LIBNAME_EGL;
                                const alibOpenGLES:string=LIBNAME_OPENGLES): Boolean;
begin
  Result:=((OpenGLESLib_InitOK) and (OpenEGLLib_InitOK));
  if Result=false then
     Result:=_Internal_LoadGLLibrary(alibEGL,alibOpenGLES);

  if Result=false then Exit;

  _Internal_LoadGLProc;
  _Internal_LoadGLAliases;
  _Internal_LoadGLExtensions;
end;

function OpenGLES_InitializeAdvance(aEGLDisplay:EGLDisplay): Boolean;
begin
  VarEGLDisplay:=aEGLDisplay;
  Result:=OpenGLESLib_InitOK;
  if Result=false then
     Result:=_Internal_LoadGLLibrary;

  if Result=false then Exit;

  _Internal_LoadGLProc;
  _Internal_LoadGLAliases;
  _Internal_LoadGLExtensions;
end;

procedure OpenGLES_Finalize;
begin
  SetLength(VarAllGLExtensions , 0); 
  SetLength(VarAllEGLExtensions, 0); 

  if (VarOpenGLLibHandle <> InvalidLibHandle) then
    begin
      dynlibs.FreeLibrary(VarOpenGLLibHandle);
      VarOpenGLLibHandle := InvalidLibHandle;
      VarOpenGLLoaded:=False;
    end;
  if (VarEGLLibHandle <> InvalidLibHandle) then
    begin
      dynlibs.FreeLibrary(VarEGLLibHandle);
      VarEGLLibHandle := InvalidLibHandle;
      VarEGLLoaded:= False;
    end;
end;
 
function  OpenGLESlib_InitOK: Boolean;
begin
  Result:=(VarOpenGLLoaded=true);
end;

function  OpenGLESlib_ExtensionsCount: Integer;
begin
  Result:=High(VarAllGLExtensions);
end;
 
function OpenGLESlib_ExtensionGet(Name: string): Boolean;
var
  I: Integer;
begin
  Result:=False;
  for I:=0 to High(VarAllGLExtensions) do
    if VarAllGLExtensions[I]=Name then Exit(True);
end;
 
function OpenGLESlib_VersionStr: String;
 begin
   result:=IntToStr(OpenGLESLib_VersionNum);
 end;

function OpenGLESlib_VersionNum: Integer;
begin
  Result:=0;
 
  if assigned(glActiveTexture) and assigned(glBindFramebuffer) then
     Result:=200;
  if assigned(glReadBuffer) and assigned(glTexSubImage3D) then
     Result:=300;
  if assigned(glDispatchCompute) and assigned(glDrawElementsIndirect) then
     Result:=310;
  if assigned(glBlendBarrier) and assigned(glDebugMessageInsert) then
     Result:=320;
  if assigned(glBlendBarrier) and assigned(glDebugMessageInsert) then
     Result:=320;
 
end;

function  OpenEGLlib_InitOK: Boolean;
begin
  Result:=(VarEGLLoaded=true);
end;

function  OpenEGLlib_ExtensionsCount: Integer;
begin
  Result:=High(VarAllEGLExtensions);
end;
 
function OpenEGLlib_ExtensionGet(Name: string): Boolean;
var
  I: Integer;
begin
  Result:=False;
  for I:=0 to High(VarAllEGLExtensions) do
    if VarAllEGLExtensions[I]=Name then Exit(True);
end;
 
function OpenEGLlib_VersionStr: String;
 begin
   result:=IntToStr(OpenEGLlib_VersionNum);
 end;

function OpenEGLlib_VersionNum: Integer;
begin
  Result:=0;
 
  if assigned(eglChooseConfig) and assigned(eglWaitNative) then
     Result:=100;
  if assigned(eglBindTexImage) and assigned(eglSwapInterval) then
     Result:=110;
  if assigned(eglBindAPI) and assigned(eglWaitClient) then
     Result:=120;
  if assigned(eglGetCurrentContext) then
     Result:=140;
  if assigned(eglCreateSync) and assigned(eglGetPlatformDisplay) then
     Result:=150;
 
end;

Initialization

{$IFDEF CPU386}{$IFNDEF DARWIN}
  Set8087CW($133F);
{$ENDIF}{$ENDIF}
{$IFDEF CPUx86_64}
  SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide,exOverflow, exUnderflow, exPrecision]);
{$ENDIF}

Finalization
  OpenGLES_Finalize;
end.
