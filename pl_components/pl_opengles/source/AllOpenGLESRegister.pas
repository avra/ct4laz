{*************************************************************************
                PilotLogic Software House

  Package pl_OpenGLES
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

 ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * ***** END LICENSE BLOCK *****
 ************************************************************************}

unit AllOpenGLESRegister;

{$mode objfpc}{$H+}

interface

uses
  Classes,SysUtils,TypInfo,lresources,PropEdits,ComponentEditors, 
  ctopengles1panel,
  ctopengles2panel;

procedure Register;

implementation

{$R AllOpenGLESRegister.res}

//==========================================================

procedure Register;
begin
 RegisterComponents ('OpenGLES',[
                                 TOpenGLES1Panel,
                                 TOpenGLES2Panel
                                 ]);



end;

end.

