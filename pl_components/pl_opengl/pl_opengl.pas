{ This file was automatically created by Typhon IDE. Do not edit!
  This source is only used to compile and install the package.
 }

unit pl_opengl;

{$warn 5023 off : no warning about unused units}
interface

uses
  AllOpenGLRegister, ctGL, ctGL_rpt, ctGLU, OpenGL_Particles, OpenGL_Shaders, 
  OpenGL_TextSys, OpenGL_Textures, OpenGLCanvas, OpenGLPanel, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('AllOpenGLRegister', @AllOpenGLRegister.Register);
end;

initialization
  RegisterPackage('pl_opengl', @Register);
end.
