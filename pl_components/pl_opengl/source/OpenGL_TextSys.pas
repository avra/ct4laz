{**********************************************************************
                   PilotLogic Software House.

 Package pl_OpenGL
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

{
   This unit draws text using a fixed vector-font set.
   The size of a letter are 1.0x1.0 (no thickness in z direction).
   The size and the location on the display can be controlled by
   glScale and glTranslate (not glRasterPos)
}

unit OpenGL_TextSys;

interface

uses ctGL;


procedure glTextOutSys(str: string; centering: integer; useDisplayList: integer = 1); Overload;
procedure glTextOutSys(PosX, PosY, PosZ: double; ScaleX, ScaleY, ScaleZ: double; center: integer; str: string); Overload;

var
  varFontSysWidth  : double  = 100;   //Font geometry Coordinate ranges in the arrays are 0<=x<=100
  varFontSysHeight : double  = 100;   //Font geometry Coordinate ranges in the arrays are 0<=y<=100
  varFontSysList   : integer = 1400;  //Fixed OpenGL display list 1400 to 1655. If it conflicts with your program, modify varFontSysList variable


implementation


var
  (*   *) Chr032: array [0..1]  of integer = (32, -1);
  (* ! *) Chr033: array [0..19] of integer = (33, 0, 3, 50, 100, 75, 100, 50, 25, 0, 4, 50, 16, 62, 16, 62, 0, 50, 0, -1);
  (* " *) Chr034: array [0..11] of integer = (34, 2, 4, 37, 100, 37, 83, 62, 100, 62, 83, -1);
  (* # *) Chr035: array [0..19] of integer = (35, 2, 8, 12, 66, 87, 66, 12, 33, 87, 33, 37, 91, 37, 8, 62, 8, 62, 91, -1);
  (* $ *) Chr036: array [0..37] of integer = (36, 1, 12, 87, 75, 75, 83, 25, 83, 12, 75, 12, 58, 25, 50, 75, 50, 87, 41, 87, 25, 75, 16, 25, 16, 12, 25, 2, 4, 37, 91, 37, 8, 62, 8, 62, 91, -1);
  (* % *) Chr037: array [0..31] of integer = (37, 1, 2, 87, 100, 12, 0, 1, 5, 12, 100, 37, 100, 37, 75, 12, 75, 12, 100, 1, 5, 87, 0, 87, 25, 62, 25, 62, 0, 87, 0, -1);
  (* & *) Chr038: array [0..25] of integer = (38, 1, 11, 87, 33, 62, 0, 25, 0, 0, 16, 0, 41, 75, 83, 75, 91, 62, 100, 37, 100, 25, 83, 87, 0, -1);
  (* ' *) Chr039: array [0..9]  of integer = (39, 0, 3, 50, 83, 50, 100, 62, 100, -1);
  (* ( *) Chr040: array [0..15] of integer = (40, 1, 6, 62, 100, 37, 83, 25, 58, 25, 41, 37, 16, 62, 0, -1);
  (* ) *) Chr041: array [0..15] of integer = (41, 1, 6, 37, 100, 62, 83, 75, 58, 75, 41, 62, 16, 37, 0, -1);
  (* * *) Chr042: array [0..19] of integer = (42, 2, 8, 50, 100, 50, 0, 0, 50, 100, 50, 87, 91, 12, 8, 87, 8, 12, 91, -1);
  (* + *) Chr043: array [0..11] of integer = (43, 2, 4, 12, 50, 87, 50, 50, 75, 50, 25, -1);
  (* , *) Chr044: array [0..11] of integer = (44, 1, 4, 37, 25, 62, 25, 62, 8, 37, -8, -1);
  (* - *) Chr045: array [0..7]  of integer = (45, 1, 2, 12, 50, 87, 50, -1);
  (* . *) Chr046: array [0..11] of integer = (46, 0, 4, 37, 16, 62, 16, 62, 0, 37, 0, -1);
  (* / *) Chr047: array [0..7]  of integer = (47, 1, 2, 100, 100, 0, 0, -1);
  (* 0 *) Chr048: array [0..27] of integer = (48, 1, 9, 25, 100, 75, 100, 100, 83, 100, 16, 75, 0, 25, 0, 0, 16, 0, 83, 25, 100, 1, 2, 87, 91, 12, 8, -1);
  (* 1 *) Chr049: array [0..9]  of integer = (49, 1, 3, 25, 83, 50, 100, 50, 0, -1);
  (* 2 *) Chr050: array [0..19] of integer = (50, 1, 8, 12, 83, 37, 100, 75, 100, 100, 83, 100, 66, 12, 16, 12, 0, 100, 0, -1);
  (* 3 *) Chr051: array [0..25] of integer = (51, 1, 11, 12, 83, 37, 100, 75, 100, 100, 83, 100, 66, 75, 50, 100, 33, 100, 16, 75, 0, 25, 0, 0, 16, -1);
  (* 4 *) Chr052: array [0..15] of integer = (52, 1, 3, 37, 100, 12, 25, 87, 25, 1, 2, 62, 75, 62, 0, -1);
  (* 5 *) Chr053: array [0..23] of integer = (53, 1, 10, 87, 100, 12, 100, 12, 41, 37, 58, 62, 58, 87, 41, 87, 16, 62, 0, 37, 0, 12, 16, -1);
  (* 6 *) Chr054: array [0..27] of integer = (54, 1, 12, 87, 83, 62, 100, 25, 100, 0, 83, 0, 16, 25, 0, 75, 0, 100, 16, 100, 33, 75, 50, 25, 50, 0, 33, -1);
  (* 7 *) Chr055: array [0..13] of integer = (55, 1, 5, 12, 83, 12, 100, 87, 100, 50, 33, 50, 0, -1);
  (* 8 *) Chr056: array [0..39] of integer = (56, 1, 9, 100, 83, 75, 100, 25, 100, 0, 83, 0, 66, 25, 50, 75, 50, 100, 66, 100, 83, 1, 8, 25, 50, 0, 33, 0, 16, 25, 0, 75, 0, 100, 16, 100, 33, 75, 50, -1);
  (* 9 *) Chr057: array [0..27] of integer = (57, 1, 12, 0, 16, 25, 0, 75, 0, 100, 16, 100, 83, 75, 100, 25, 100, 0, 83, 0, 58, 25, 41, 75, 41, 100, 58, -1);
  (* : *) Chr058: array [0..21] of integer = (58, 0, 4, 37, 91, 62, 91, 62, 75, 37, 75, 0, 4, 37, 25, 62, 25, 62, 8, 37, 8, -1);
  (* ; *) Chr059: array [0..27] of integer = (59, 0, 4, 37, 91, 62, 91, 62, 75, 37, 75, 0, 4, 37, 25, 62, 25, 62, 8, 37, 8, 1, 2, 62, 8, 37, -8, -1);
  (* < *) Chr060: array [0..9]  of integer = (60, 1, 3, 87, 100, 12, 50, 87, 0, -1);
  (* = *) Chr061: array [0..11] of integer = (61, 2, 4, 12, 66, 87, 66, 12, 33, 87, 33, -1);
  (* > *) Chr062: array [0..9]  of integer = (62, 1, 3, 12, 0, 87, 50, 12, 100, -1);
  (* ? *) Chr063: array [0..29] of integer = (63, 1, 8, 12, 83, 37, 100, 75, 100, 100, 83, 100, 66, 75, 50, 50, 50, 50, 25, 0, 4, 50, 16, 62, 16, 62, 8, 50, 8, -1);
  (* @ *) Chr064: array [0..39] of integer = (64, 1, 18, 62, 50, 50, 58, 25, 58, 12, 41, 12, 25, 25, 16, 50, 16, 62, 41, 75, 25, 87, 66, 75, 91, 62, 100, 25, 100, 0, 75, 0, 16, 25, 0, 62, 0, 87, 16, -1);
  (* A *) Chr065: array [0..15] of integer = (65, 1, 3, 0, 0, 50, 100, 100, 0, 1, 2, 25, 50, 75, 50, -1);
  (* B *) Chr066: array [0..29] of integer = (66, 1, 10, 0, 0, 0, 100, 75, 100, 87, 91, 87, 58, 75, 50, 100, 33, 100, 8, 87, 0, 0, 0, 1, 2, 75, 50, 0, 50, -1);
  (* C *) Chr067: array [0..19] of integer = (67, 1, 8, 100, 83, 75, 100, 25, 100, 0, 83, 0, 16, 25, 0, 75, 0, 100, 16, -1);
  (* D *) Chr068: array [0..17] of integer = (68, 1, 7, 0, 100, 75, 100, 100, 83, 100, 16, 75, 0, 0, 0, 0, 100, -1);
  (* E *) Chr069: array [0..17] of integer = (69, 1, 4, 100, 100, 0, 100, 0, 0, 100, 0, 1, 2, 0, 50, 87, 50, -1);
  (* F *) Chr070: array [0..15] of integer = (70, 1, 3, 100, 100, 0, 100, 0, 0, 1, 2, 0, 50, 75, 50, -1);
  (* G *) Chr071: array [0..23] of integer = (71, 1, 10, 100, 83, 75, 100, 25, 100, 0, 83, 0, 16, 25, 0, 75, 0, 100, 16, 100, 41, 62, 41, -1);
  (* H *) Chr072: array [0..15] of integer = (72, 2, 6, 0, 100, 0, 0, 100, 100, 100, 0, 0, 50, 100, 50, -1);
  (* I *) Chr073: array [0..15] of integer = (73, 2, 6, 37, 100, 62, 100, 37, 0, 62, 0, 50, 0, 50, 100, -1);
  (* J *) Chr074: array [0..21] of integer = (74, 1, 2, 75, 100, 100, 100, 1, 6, 87, 100, 87, 16, 62, 0, 37, 0, 12, 16, 12, 33, -1);
  (* K *) Chr075: array [0..19] of integer = (75, 1, 2, 12, 100, 12, 0, 1, 2, 12, 33, 100, 100, 1, 2, 25, 41, 100, 0, -1);
  (* L *) Chr076: array [0..9]  of integer = (76, 1, 3, 0, 100, 0, 0, 100, 0, -1);
  (* M *) Chr077: array [0..13] of integer = (77, 1, 5, 0, 0, 0, 100, 50, 50, 100, 100, 100, 0, -1);
  (* N *) Chr078: array [0..11] of integer = (78, 1, 4, 0, 0, 0, 100, 100, 0, 100, 100, -1);
  (* O *) Chr079: array [0..21] of integer = (79, 1, 9, 0, 83, 25, 100, 75, 100, 100, 83, 100, 16, 75, 0, 25, 0, 0, 16, 0, 83, -1);
  (* P *) Chr080: array [0..17] of integer = (80, 1, 7, 0, 0, 0, 100, 75, 100, 100, 83, 100, 66, 75, 50, 0, 50, -1);
  (* Q *) Chr081: array [0..27] of integer = (81, 1, 9, 25, 0, 0, 16, 0, 83, 25, 100, 75, 100, 100, 83, 100, 16, 75, 0, 25, 0, 1, 2, 62, 25, 100, 0, -1);
  (* R *) Chr082: array [0..25] of integer = (82, 1, 7, 0, 0, 0, 100, 75, 100, 100, 83, 100, 66, 75, 50, 0, 50, 1, 3, 75, 50, 100, 33, 100, 0, -1);
  (* S *) Chr083: array [0..27] of integer = (83, 1, 12, 100, 83, 75, 100, 25, 100, 0, 83, 0, 66, 25, 50, 75, 50, 100, 33, 100, 16, 75, 0, 25, 0, 0, 16, -1);
  (* T *) Chr084: array [0..11] of integer = (84, 2, 4, 0, 100, 100, 100, 50, 100, 50, 0, -1);
  (* U *) Chr085: array [0..15] of integer = (85, 1, 6, 0, 100, 0, 16, 25, 0, 75, 0, 100, 16, 100, 100, -1);
  (* V *) Chr086: array [0..9]  of integer = (86, 1, 3, 0, 100, 50, 0, 100, 100, -1);
  (* W *) Chr087: array [0..13] of integer = (87, 1, 5, 0, 100, 25, 0, 50, 66, 75, 0, 100, 100, -1);
  (* X *) Chr088: array [0..11] of integer = (88, 2, 4, 0, 0, 100, 100, 100, 0, 0, 100, -1);
  (* Y *) Chr089: array [0..15] of integer = (89, 1, 3, 0, 100, 50, 50, 50, 0, 1, 2, 50, 50, 100, 100, -1);
  (* Z *) Chr090: array [0..11] of integer = (90, 1, 4, 0, 100, 100, 100, 0, 0, 100, 0, -1);
  (* [ *) Chr091: array [0..11] of integer = (91, 1, 4, 62, 100, 37, 100, 37, 0, 62, 0, -1);
  (* \ *) Chr092: array [0..7]  of integer = (92, 1, 2, 0, 100, 100, 0, -1);
  (* ] *) Chr093: array [0..11] of integer = (93, 1, 4, 37, 100, 62, 100, 62, 0, 37, 0, -1);
  (* ^ *) Chr094: array [0..9]  of integer = (94, 1, 3, 0, 66, 50, 91, 100, 66, -1);
  (* _ *) Chr095: array [0..7]  of integer = (95, 1, 2, 0, 8, 100, 8, -1);
  (* ` *) Chr096: array [0..9]  of integer = (96, 0, 3, 37, 100, 50, 100, 50, 83, -1);
  (* a *) Chr097: array [0..29] of integer = (97, 1, 5, 12, 50, 25, 58, 75, 58, 87, 50, 87, 0, 1, 7, 87, 33, 25, 33, 12, 25, 12, 8, 25, 0, 75, 0, 87, 8, -1);
  (* b *) Chr098: array [0..17] of integer = (98, 1, 7, 12, 100, 12, 0, 75, 0, 87, 8, 87, 50, 75, 58, 12, 58, -1);
  (* c *) Chr099: array [0..19] of integer = (99, 1, 8, 87, 50, 75, 58, 25, 58, 12, 50, 12, 8, 25, 0, 75, 0, 87, 8, -1);
  (* d *) Chr100: array [0..19] of integer = (100, 1, 8, 87, 100, 87, 0, 25, 0, 12, 8, 12, 50, 25, 58, 75, 58, 87, 50, -1);
  (* e *) Chr101: array [0..23] of integer = (101, 1, 10, 12, 33, 87, 33, 87, 50, 75, 58, 25, 58, 12, 50, 12, 8, 25, 0, 75, 0, 87, 8, -1);
  (* f *) Chr102: array [0..17] of integer = (102, 1, 4, 75, 100, 62, 100, 50, 91, 50, 0, 1, 2, 25, 58, 75, 58, -1);
  (* g *) Chr103: array [0..31] of integer = (103, 1, 5, 87, 58, 87, 0, 75, -8, 25, -8, 12, 0, 1, 8, 87, 50, 75, 58, 25, 58, 12, 50, 12, 33, 25, 25, 75, 25, 87, 33, -1);
  (* h *) Chr104: array [0..19] of integer = (104, 1, 2, 12, 0, 12, 100, 1, 5, 12, 50, 25, 58, 75, 58, 87, 50, 87, 0, -1);
  (* i *) Chr105: array [0..13] of integer = (105, 1, 2, 50, 75, 50, 66, 1, 2, 50, 58, 50, 0, -1);
  (* j *) Chr106: array [0..17] of integer = (106, 1, 2, 50, 75, 50, 66, 1, 4, 50, 58, 50, 0, 37, -8, 12, -8, -1);
  (* k *) Chr107: array [0..15] of integer = (107, 1, 2, 12, 100, 12, 0, 1, 3, 87, 0, 12, 33, 75, 58, -1);
  (* l *) Chr108: array [0..9]  of integer = (108, 1, 3, 37, 100, 50, 100, 50, 0, -1);
  (* m *) Chr109: array [0..21] of integer = (109, 1, 5, 12, 0, 12, 58, 75, 58, 87, 50, 87, 0, 1, 3, 37, 58, 50, 50, 50, 0, -1);
  (* n *) Chr110: array [0..13] of integer = (110, 1, 5, 12, 0, 12, 58, 75, 58, 87, 50, 87, 0, -1);
  (* o *) Chr111: array [0..21] of integer = (111, 1, 9, 25, 0, 12, 8, 12, 50, 25, 58, 75, 58, 87, 50, 87, 8, 75, 0, 25, 0, -1);
  (* p *) Chr112: array [0..23] of integer = (112, 1, 2, 12, 58, 12, -16, 1, 7, 12, 50, 25, 58, 75, 58, 87, 50, 87, 8, 75, 0, 12, 0, -1);
  (* q *) Chr113: array [0..23] of integer = (113, 1, 2, 87, 58, 87, -16, 1, 7, 87, 50, 75, 58, 25, 58, 12, 50, 12, 8, 25, 0, 87, 0, -1);
  (* r *) Chr114: array [0..15] of integer = (114, 1, 2, 25, 58, 25, 0, 1, 3, 25, 50, 62, 58, 87, 58, -1);
  (* s *) Chr115: array [0..23] of integer = (115, 1, 10, 87, 50, 75, 58, 25, 58, 12, 50, 12, 41, 87, 16, 87, 8, 75, 0, 25, 0, 12, 8, -1);
  (* t *) Chr116: array [0..17] of integer = (116, 1, 2, 25, 58, 75, 58, 1, 4, 37, 75, 37, 8, 50, 0, 75, 0, -1);
  (* u *) Chr117: array [0..19] of integer = (117, 1, 5, 12, 58, 12, 8, 25, 0, 62, 0, 87, 8, 1, 2, 87, 58, 87, 0, -1);
  (* v *) Chr118: array [0..9]  of integer = (118, 1, 3, 12, 58, 50, 0, 87, 58, -1);
  (* w *) Chr119: array [0..13] of integer = (119, 1, 5, 12, 58, 25, 0, 50, 41, 75, 0, 87, 58, -1);
  (* x *) Chr120: array [0..11] of integer = (120, 2, 4, 87, 0, 12, 58, 87, 58, 12, 0, -1);
  (* y *) Chr121: array [0..11] of integer = (121, 2, 4, 87, 58, 12, -25, 12, 58, 50, 16, -1);
  (* z *) Chr122: array [0..11] of integer = (122, 1, 4, 12, 58, 87, 58, 12, 0, 87, 0, -1);
  (* { *) Chr123: array [0..21] of integer = (123, 1, 6, 75, 100, 50, 100, 37, 91, 37, 8, 50, 0, 75, 0, 1, 2, 37, 50, 25, 50, -1);
  (* | *) Chr124: array [0..7]  of integer = (124, 1, 2, 50, 100, 50, 0, -1);
  (* } *) Chr125: array [0..21] of integer = (125, 1, 6, 25, 0, 50, 0, 62, 8, 62, 91, 50, 100, 25, 100, 1, 2, 62, 50, 75, 50, -1);
  (* ~ *) Chr126: array [0..7]  of integer = (126, 1, 2, 0, 91, 100, 91, -1);

  varFontSysData: array [0..255] of pinteger = (nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                @Chr032[0], @Chr033[0], @Chr034[0], @Chr035[0], @Chr036[0], @Chr037[0], @Chr038[0],
                                                @Chr039[0], @Chr040[0], @Chr041[0], @Chr042[0], @Chr043[0], @Chr044[0], @Chr045[0],
                                                @Chr046[0], @Chr047[0], @Chr048[0], @Chr049[0], @Chr050[0], @Chr051[0], @Chr052[0],
                                                @Chr053[0], @Chr054[0], @Chr055[0], @Chr056[0], @Chr057[0], @Chr058[0], @Chr059[0],
                                                @Chr060[0], @Chr061[0], @Chr062[0], @Chr063[0], @Chr064[0], @Chr065[0], @Chr066[0],
                                                @Chr067[0], @Chr068[0], @Chr069[0], @Chr070[0], @Chr071[0], @Chr072[0], @Chr073[0],
                                                @Chr074[0], @Chr075[0], @Chr076[0], @Chr077[0], @Chr078[0], @Chr079[0], @Chr080[0],
                                                @Chr081[0], @Chr082[0], @Chr083[0], @Chr084[0], @Chr085[0], @Chr086[0], @Chr087[0],
                                                @Chr088[0], @Chr089[0], @Chr090[0], @Chr091[0], @Chr092[0], @Chr093[0], @Chr094[0],
                                                @Chr095[0], @Chr096[0], @Chr097[0], @Chr098[0], @Chr099[0], @Chr100[0], @Chr101[0],
                                                @Chr102[0], @Chr103[0], @Chr104[0], @Chr105[0], @Chr106[0], @Chr107[0], @Chr108[0],
                                                @Chr109[0], @Chr110[0], @Chr111[0], @Chr112[0], @Chr113[0], @Chr114[0], @Chr115[0],
                                                @Chr116[0], @Chr117[0], @Chr118[0], @Chr119[0], @Chr120[0], @Chr121[0], @Chr122[0],
                                                @Chr123[0], @Chr124[0], @Chr125[0], @Chr126[0],
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,
                                                nil, nil, nil, nil, nil, nil, nil, nil, nil, nil);

procedure DrawSysFontPattern(ptn: pinteger);
var
  j: integer;
  ptr: pinteger;
begin
  if ptn <> nil then
  begin
    ptr := ptn;
    Inc(ptr);
    while ptr[0] <> -1 do
    begin
      case ptr[0] of
        0: glBegin(GL_POLYGON);
        1: glBegin(GL_LINE_STRIP);
        2: glBegin(GL_LINES);
      end;

      for j := 0 to Pred(ptr[1]) do
        glVertex2i(ptr[2 + j * 2], ptr[3 + j * 2]);

      glEnd;

      ptr := ptr + 2 + ptr[1] * 2;
    end;
  end;
  glTranslated(varFontSysWidth * 8 / 7, 0, 0);
end;

procedure FontSysMakeDisplayList inline;
var
  i: integer;
begin
  if glIsList(varFontSysList) <> Boolean(GL_TRUE) then
    for i := 0 to Pred(256) do
    begin
      glNewList(varFontSysList + i, GL_COMPILE);
      DrawSysFontPattern(varFontSysData[i]);
      glEndList;
    end;
end;

//====================================
     
procedure glTextOutSys(str: string; centering: integer; useDisplayList: integer);
var
  l: integer;
  i: integer;
begin
  l := Length(str);
  glPushMatrix;

  if centering <> 0 then
    glTranslated(-l / 2, -0.5, 0);

  glScaled(1 / (varFontSysWidth * 8 / 7), 1 / varFontSysHeight, 1);

  if useDisplayList <> 0 then
  begin
    FontSysMakeDisplayList;
    glPushAttrib(GL_LIST_BIT);
    glListBase(varFontSysList);
    glCallLists(l, GL_UNSIGNED_BYTE, @str[1]);
    glPopAttrib;
  end
  else
  begin
    i := 0;
    while str[i] <> #0 do
    begin
      DrawSysFontPattern(varFontSysData[Ord(str[i])]);
      Inc(i);
    end;
  end;
  glPopMatrix;
end;

procedure glTextOutSys(PosX, PosY, PosZ: double; ScaleX, ScaleY, ScaleZ: double; center: integer; str: string);
begin
  glPushMatrix;
  glTranslatef(PosX, PosY, PosZ);
  glScalef(ScaleX, ScaleY, ScaleZ);
  glTextOutSys(str, center);
  glPopMatrix;
end;

end.

