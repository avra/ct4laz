{**********************************************************************
                PilotLogic Software House.

 Package pl_OpenGL
 This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)
***********************************************************************}

unit ctGLU;

{$MODE OBJFPC}{$H+}
               
{$MACRO ON}
{$PACKRECORDS C}

{$IFDEF Windows}
  {$DEFINE extdecl := stdcall}
{$ELSE}
  {$DEFINE extdecl := cdecl}
{$ENDIF}

interface

uses
  SysUtils, dynlibs,
  ctgl;

Const
{$IFDEF Windows}
  GLU_Lib = 'glu32.dll';
{$ELSE}
{$IFDEF OS2}
  GLU_Lib = 'opengl.dll';
{$ELSE OS2}
{$ifdef darwin}
  GLU_LIB =  '/System/Library/Frameworks/OpenGL.framework/Libraries/libGLU.dylib';
{$else}
{$IFDEF haiku}
  GLU_LIB = 'libGLU.so';
{$ELSE}
  GLU_LIB = 'libGLU.so.1';
{$ENDIF}
{$ENDIF}
{$ENDIF OS2}
{$endif}

type
  TViewPortArray = array [0..3] of GLint;
  T16dArray = array [0..15] of GLdouble;
  T3dArray = array [0..2] of GLdouble;
  T4pArray = array [0..3] of Pointer;
  T4fArray = array [0..3] of GLfloat;

const
  GLU_EXT_object_space_tess = 1;
  GLU_EXT_nurbs_tessellator = 1;
  { Boolean  }
  GLU_FALSE = 0;
  GLU_TRUE = 1;
  { Version  }
  GLU_VERSION_1_1 = 1;
  GLU_VERSION_1_2 = 1;
  GLU_VERSION_1_3 = 1;
  { StringName  }
  GLU_VERSION = 100800;
  GLU_EXTENSIONS = 100801;
  { ErrorCode  }
  GLU_INVALID_ENUM = 100900;
  GLU_INVALID_VALUE = 100901;
  GLU_OUT_OF_MEMORY = 100902;
  GLU_INCOMPATIBLE_GL_VERSION = 100903;
  GLU_INVALID_OPERATION = 100904;
  { NurbsDisplay  }
  {      GLU_FILL  }
  GLU_OUTLINE_POLYGON = 100240;
  GLU_OUTLINE_PATCH = 100241;
  { NurbsCallback  }
  GLU_NURBS_ERROR = 100103;
  GLU_ERROR = 100103;
  GLU_NURBS_BEGIN = 100164;
  GLU_NURBS_BEGIN_EXT = 100164;
  GLU_NURBS_VERTEX = 100165;
  GLU_NURBS_VERTEX_EXT = 100165;
  GLU_NURBS_NORMAL = 100166;
  GLU_NURBS_NORMAL_EXT = 100166;
  GLU_NURBS_COLOR = 100167;
  GLU_NURBS_COLOR_EXT = 100167;
  GLU_NURBS_TEXTURE_COORD = 100168;
  GLU_NURBS_TEX_COORD_EXT = 100168;
  GLU_NURBS_END = 100169;
  GLU_NURBS_END_EXT = 100169;
  GLU_NURBS_BEGIN_DATA = 100170;
  GLU_NURBS_BEGIN_DATA_EXT = 100170;
  GLU_NURBS_VERTEX_DATA = 100171;
  GLU_NURBS_VERTEX_DATA_EXT = 100171;
  GLU_NURBS_NORMAL_DATA = 100172;
  GLU_NURBS_NORMAL_DATA_EXT = 100172;
  GLU_NURBS_COLOR_DATA = 100173;
  GLU_NURBS_COLOR_DATA_EXT = 100173;
  GLU_NURBS_TEXTURE_COORD_DATA = 100174;
  GLU_NURBS_TEX_COORD_DATA_EXT = 100174;
  GLU_NURBS_END_DATA = 100175;
  GLU_NURBS_END_DATA_EXT = 100175;
  { NurbsError  }
  GLU_NURBS_ERROR1 = 100251;
  GLU_NURBS_ERROR2 = 100252;
  GLU_NURBS_ERROR3 = 100253;
  GLU_NURBS_ERROR4 = 100254;
  GLU_NURBS_ERROR5 = 100255;
  GLU_NURBS_ERROR6 = 100256;
  GLU_NURBS_ERROR7 = 100257;
  GLU_NURBS_ERROR8 = 100258;
  GLU_NURBS_ERROR9 = 100259;
  GLU_NURBS_ERROR10 = 100260;
  GLU_NURBS_ERROR11 = 100261;
  GLU_NURBS_ERROR12 = 100262;
  GLU_NURBS_ERROR13 = 100263;
  GLU_NURBS_ERROR14 = 100264;
  GLU_NURBS_ERROR15 = 100265;
  GLU_NURBS_ERROR16 = 100266;
  GLU_NURBS_ERROR17 = 100267;
  GLU_NURBS_ERROR18 = 100268;
  GLU_NURBS_ERROR19 = 100269;
  GLU_NURBS_ERROR20 = 100270;
  GLU_NURBS_ERROR21 = 100271;
  GLU_NURBS_ERROR22 = 100272;
  GLU_NURBS_ERROR23 = 100273;
  GLU_NURBS_ERROR24 = 100274;
  GLU_NURBS_ERROR25 = 100275;
  GLU_NURBS_ERROR26 = 100276;
  GLU_NURBS_ERROR27 = 100277;
  GLU_NURBS_ERROR28 = 100278;
  GLU_NURBS_ERROR29 = 100279;
  GLU_NURBS_ERROR30 = 100280;
  GLU_NURBS_ERROR31 = 100281;
  GLU_NURBS_ERROR32 = 100282;
  GLU_NURBS_ERROR33 = 100283;
  GLU_NURBS_ERROR34 = 100284;
  GLU_NURBS_ERROR35 = 100285;
  GLU_NURBS_ERROR36 = 100286;
  GLU_NURBS_ERROR37 = 100287;
  { NurbsProperty  }
  GLU_AUTO_LOAD_MATRIX = 100200;
  GLU_CULLING = 100201;
  GLU_SAMPLING_TOLERANCE = 100203;
  GLU_DISPLAY_MODE = 100204;
  GLU_PARAMETRIC_TOLERANCE = 100202;
  GLU_SAMPLING_METHOD = 100205;
  GLU_U_STEP = 100206;
  GLU_V_STEP = 100207;
  GLU_NURBS_MODE = 100160;
  GLU_NURBS_MODE_EXT = 100160;
  GLU_NURBS_TESSELLATOR = 100161;
  GLU_NURBS_TESSELLATOR_EXT = 100161;
  GLU_NURBS_RENDERER = 100162;
  GLU_NURBS_RENDERER_EXT = 100162;
  { NurbsSampling  }
  GLU_OBJECT_PARAMETRIC_ERROR = 100208;
  GLU_OBJECT_PARAMETRIC_ERROR_EXT = 100208;
  GLU_OBJECT_PATH_LENGTH = 100209;
  GLU_OBJECT_PATH_LENGTH_EXT = 100209;
  GLU_PATH_LENGTH = 100215;
  GLU_PARAMETRIC_ERROR = 100216;
  GLU_DOMAIN_DISTANCE = 100217;
  { NurbsTrim  }
  GLU_MAP1_TRIM_2 = 100210;
  GLU_MAP1_TRIM_3 = 100211;
  { QuadricDrawStyle  }
  GLU_POINT = 100010;
  GLU_LINE = 100011;
  GLU_FILL = 100012;
  GLU_SILHOUETTE = 100013;
  { QuadricCallback  }
  {      GLU_ERROR  }
  { QuadricNormal  }
  GLU_SMOOTH = 100000;
  GLU_FLAT = 100001;
  GLU_NONE = 100002;
  { QuadricOrientation  }
  GLU_OUTSIDE = 100020;
  GLU_INSIDE = 100021;
  { TessCallback  }
  GLU_TESS_BEGIN = 100100;
  GLU_BEGIN = 100100;
  GLU_TESS_VERTEX = 100101;
  GLU_VERTEX = 100101;
  GLU_TESS_END = 100102;
  GLU_END = 100102;
  GLU_TESS_ERROR = 100103;
  GLU_TESS_EDGE_FLAG = 100104;
  GLU_EDGE_FLAG = 100104;
  GLU_TESS_COMBINE = 100105;
  GLU_TESS_BEGIN_DATA = 100106;
  GLU_TESS_VERTEX_DATA = 100107;
  GLU_TESS_END_DATA = 100108;
  GLU_TESS_ERROR_DATA = 100109;
  GLU_TESS_EDGE_FLAG_DATA = 100110;
  GLU_TESS_COMBINE_DATA = 100111;
  { TessContour  }
  GLU_CW = 100120;
  GLU_CCW = 100121;
  GLU_INTERIOR = 100122;
  GLU_EXTERIOR = 100123;
  GLU_UNKNOWN = 100124;
  { TessProperty  }
  GLU_TESS_WINDING_RULE = 100140;
  GLU_TESS_BOUNDARY_ONLY = 100141;
  GLU_TESS_TOLERANCE = 100142;
  { TessError  }
  GLU_TESS_ERROR1 = 100151;
  GLU_TESS_ERROR2 = 100152;
  GLU_TESS_ERROR3 = 100153;
  GLU_TESS_ERROR4 = 100154;
  GLU_TESS_ERROR5 = 100155;
  GLU_TESS_ERROR6 = 100156;
  GLU_TESS_ERROR7 = 100157;
  GLU_TESS_ERROR8 = 100158;
  GLU_TESS_MISSING_BEGIN_POLYGON = 100151;
  GLU_TESS_MISSING_BEGIN_CONTOUR = 100152;
  GLU_TESS_MISSING_END_POLYGON = 100153;
  GLU_TESS_MISSING_END_CONTOUR = 100154;
  GLU_TESS_COORD_TOO_LARGE = 100155;
  GLU_TESS_NEED_COMBINE_CALLBACK = 100156;
  { TessWinding  }
  GLU_TESS_WINDING_ODD = 100130;
  GLU_TESS_WINDING_NONZERO = 100131;
  GLU_TESS_WINDING_POSITIVE = 100132;
  GLU_TESS_WINDING_NEGATIVE = 100133;
  GLU_TESS_WINDING_ABS_GEQ_TWO = 100134;

const
  GLU_TESS_MAX_COORD = 1.0e150;

{*********************************************************** }

type
  GLUnurbs = record
  end;
  GLUquadric = record
  end;
  GLUtesselator = record
  end;

  PGLdouble = ^GLdouble;
  PGLfloat = ^GLfloat;
  PGLint = ^GLint;
  PGLubyte = PAnsiChar; //< this is only used for strings in GLU
  PGLUnurbs = ^GLUnurbs;
  PGLUquadric = ^GLUquadric;
  PGLUtesselator = ^GLUtesselator;


  // =========================================== ct9999
  // backwards compatibility:

  GLUnurbsObj = GLUnurbs;
  PGLUnurbsObj = PGLUnurbs;
  GLUquadricObj = GLUquadric;
  PGLUquadricObj = PGLUquadric;
  GLUtesselatorObj = GLUtesselator;
  PGLUtesselatorObj = PGLUtesselator;
  GLUtriangulatorObj = GLUtesselator;
  PGLUtriangulatorObj = PGLUtesselator;

  TGLUnurbs = GLUnurbs;
  TGLUquadric = GLUquadric;
  TGLUtesselator = GLUtesselator;

  TGLUnurbsObj = GLUnurbsObj;
  TGLUquadricObj = GLUquadricObj;
  TGLUtesselatorObj = GLUtesselatorObj;
  TGLUtriangulatorObj = GLUtriangulatorObj;

  // ============================================

  _GLUfuncptr = procedure; extdecl;
  TCallback = _GLUfuncptr;

type

  TGLUQuadricErrorProc = procedure(errorCode: GLenum); extdecl;
  TGLUTessBeginProc = procedure(AType: GLenum); extdecl;
  TGLUTessEdgeFlagProc = procedure(Flag: GLboolean); extdecl;
  TGLUTessVertexProc = procedure(VertexData: Pointer); extdecl;
  TGLUTessEndProc = procedure; extdecl;
  TGLUTessErrorProc = procedure(ErrNo: GLenum); extdecl;
  TGLUTessCombineProc = procedure(Coords: TGLArrayd3; VertexData: TGLArrayp4; Weight: TGLArrayf4; OutData: PPointer); extdecl;
  TGLUTessBeginDataProc = procedure(AType: GLenum; UserData: Pointer); extdecl;
  TGLUTessEdgeFlagDataProc = procedure(Flag: GLboolean; UserData: Pointer); extdecl;
  TGLUTessVertexDataProc = procedure(VertexData: Pointer; UserData: Pointer); extdecl;
  TGLUTessEndDataProc = procedure(UserData: Pointer); extdecl;
  TGLUTessErrorDataProc = procedure(ErrNo: GLenum; UserData: Pointer); extdecl;
  TGLUTessCombineDataProc = procedure(Coords: TGLArrayd3; VertexData: TGLArrayp4; Weight: TGLArrayf4; OutData: PPointer; UserData: Pointer); extdecl;
  TGLUNurbsErrorProc = procedure(ErrorCode: GLEnum); extdecl;
  TgluErrorString = function(errCode: GLEnum): PAnsiChar; extdecl;
  TgluGetString = function(name: GLEnum): PAnsiChar; extdecl;
  TgluOrtho2D = procedure(left, right, bottom, top: GLdouble); extdecl;
  TgluPerspective = procedure(fovy, aspect, zNear, zFar: GLdouble); extdecl;
  TgluPickMatrix = procedure(x, y, width, height: GLdouble; const viewport: TVector4i); extdecl;
  TgluLookAt = procedure(eyex, eyey, eyez, centerx, centery, centerz, upx, upy, upz: GLdouble); extdecl;
  TgluProject = function(objx, objy, objz: GLdouble; const modelMatrix: TGLMatrixd4; const projMatrix: TGLMatrixd4; const viewport: TVector4i; winx, winy, winz: PGLdouble): GLint; extdecl;
  TgluUnProject = function(winx, winy, winz: GLdouble; const modelMatrix: TGLMatrixd4; const projMatrix: TGLMatrixd4; const viewport: TVector4i; objx, objy, objz: PGLdouble): GLint; extdecl;
  TgluScaleImage = function(format: GLEnum; widthin, heightin: GLint; typein: GLEnum; datain: Pointer; widthout, heightout: GLint; typeout: GLEnum; const dataout: Pointer): GLint; extdecl;
  TgluBuild1DMipmaps = function(target: GLEnum; components, width: GLint; format, atype: GLEnum; const data: Pointer): GLint; extdecl;
  TgluBuild2DMipmaps = function(target: GLEnum; components, width, height: GLint; format, atype: GLEnum; const Data: Pointer): GLint; extdecl;
  TgluNewQuadric = function: PGLUquadric; extdecl;
  TgluDeleteQuadric = procedure(state: PGLUquadric); extdecl;
  TgluQuadricNormals = procedure(quadObject: PGLUquadric; normals: GLEnum); extdecl;
  TgluQuadricTexture = procedure(quadObject: PGLUquadric; textureCoords: GLboolean); extdecl;
  TgluQuadricOrientation = procedure(quadObject: PGLUquadric; orientation: GLEnum); extdecl;
  TgluQuadricDrawStyle = procedure(quadObject: PGLUquadric; drawStyle: GLEnum); extdecl;
  TgluCylinder = procedure(quadObject: PGLUquadric; baseRadius, topRadius, height: GLdouble; slices, stacks: GLint); extdecl;
  TgluDisk = procedure(quadObject: PGLUquadric; innerRadius, outerRadius: GLdouble; slices, loops: GLint); extdecl;
  TgluPartialDisk = procedure(quadObject: PGLUquadric; innerRadius, outerRadius: GLdouble; slices, loops: GLint; startAngle, sweepAngle: GLdouble); extdecl;
  TgluSphere = procedure(quadObject: PGLUquadric; radius: GLdouble; slices, stacks: GLint); extdecl;
  TgluQuadricCallback = procedure(quadObject: PGLUquadric; which: GLEnum; fn: TGLUQuadricErrorProc); extdecl;
  TgluNewTess = function: PGLUtesselator; extdecl;
  TgluDeleteTess = procedure(tess: PGLUtesselator); extdecl;
  TgluTessBeginPolygon = procedure(tess: PGLUtesselator; polygon_data: Pointer); extdecl;
  TgluTessBeginContour = procedure(tess: PGLUtesselator); extdecl;
  TgluTessVertex = procedure(tess: PGLUtesselator; const coords: TGLArrayd3; data: Pointer); extdecl;
  TgluTessEndContour = procedure(tess: PGLUtesselator); extdecl;
  TgluTessEndPolygon = procedure(tess: PGLUtesselator); extdecl;
  TgluTessProperty = procedure(tess: PGLUtesselator; which: GLEnum; value: GLdouble); extdecl;
  TgluTessNormal = procedure(tess: PGLUtesselator; x, y, z: GLdouble); extdecl;
  TgluTessCallback = procedure(tess: PGLUtesselator; which: GLEnum; fn: Pointer); extdecl;
  TgluGetTessProperty = procedure(tess: PGLUtesselator; which: GLEnum; value: PGLdouble); extdecl;
  TgluNewNurbsRenderer = function: PGLUnurbs; extdecl;
  TgluDeleteNurbsRenderer = procedure(nobj: PGLUnurbs); extdecl;
  TgluBeginSurface = procedure(nobj: PGLUnurbs); extdecl;
  TgluBeginCurve = procedure(nobj: PGLUnurbs); extdecl;
  TgluEndCurve = procedure(nobj: PGLUnurbs); extdecl;
  TgluEndSurface = procedure(nobj: PGLUnurbs); extdecl;
  TgluBeginTrim = procedure(nobj: PGLUnurbs); extdecl;
  TgluEndTrim = procedure(nobj: PGLUnurbs); extdecl;
  TgluPwlCurve = procedure(nobj: PGLUnurbs; count: GLint; points: PGLfloat; stride: GLint; atype: GLEnum); extdecl;
  TgluNurbsCurve = procedure(nobj: PGLUnurbs; nknots: GLint; knot: PGLfloat; stride: GLint; ctlarray: PGLfloat; order: GLint; atype: GLEnum); extdecl;
  TgluNurbsSurface = procedure(nobj: PGLUnurbs; sknot_count: GLint; sknot: PGLfloat; tknot_count: GLint; tknot: PGLfloat; s_stride, t_stride: GLint; ctlarray: PGLfloat; sorder, torder: GLint; atype: GLEnum); extdecl;
  TgluLoadSamplingMatrices = procedure(nobj: PGLUnurbs; const modelMatrix, projMatrix: TGLMatrixf4; const viewport: TVector4i); extdecl;
  TgluNurbsProperty = procedure(nobj: PGLUnurbs; aproperty: GLEnum; value: GLfloat); extdecl;
  TgluGetNurbsProperty = procedure(nobj: PGLUnurbs; aproperty: GLEnum; value: PGLfloat); extdecl;
  TgluNurbsCallback = procedure(nobj: PGLUnurbs; which: GLEnum; fn: TGLUNurbsErrorProc); extdecl;
  TgluBeginPolygon = procedure(tess: PGLUtesselator); extdecl;
  TgluNextContour = procedure(tess: PGLUtesselator; atype: GLEnum); extdecl;
  TgluEndPolygon = procedure(tess: PGLUtesselator); extdecl;
  TgluBuild1DMipmapLevels = function(target:GLenum; internalFormat:GLint; width:GLsizei; format:GLenum; _type:GLenum;
                                      level:GLint; base:GLint; max:GLint; data:pointer):GLint;extdecl;
  TgluBuild2DMipmapLevels = function(target:GLenum; internalFormat:GLint; width:GLsizei; height:GLsizei; format:GLenum;
                                     _type:GLenum; level:GLint; base:GLint; max:GLint; data:pointer):GLint;extdecl;
  TgluBuild3DMipmapLevels = function(target:GLenum; internalFormat:GLint; width:GLsizei; height:GLsizei; depth:GLsizei;
                                     format:GLenum; _type:GLenum; level:GLint; base:GLint; max:GLint;
                                     data:pointer):GLint;extdecl;
  TgluBuild3DMipmaps = function(target:GLenum; internalFormat:GLint; width:GLsizei; height:GLsizei; depth:GLsizei;
                                format:GLenum; _type:GLenum; data:pointer):GLint;extdecl;
  TgluCheckExtension = function(extName:PGLubyte; extString:PGLubyte):GLboolean;extdecl;
  TgluNurbsCallbackData = procedure(nurb:PGLUnurbs; userData:PGLvoid);extdecl; 
  TgluNurbsCallbackDataEXT = procedure(nurb:PGLUnurbs; userData:PGLvoid);extdecl; 
  TgluUnProject4 = function(winX:GLdouble; winY:GLdouble; winZ:GLdouble; clipW:GLdouble; model:PGLdouble;
                            proj:PGLdouble; view:PGLint; nearVal:GLdouble; farVal:GLdouble; objX:PGLdouble;
                            objY:PGLdouble; objZ:PGLdouble; objW:PGLdouble):GLint;extdecl;


var

    gluErrorString : TgluErrorString = NIL;
    gluGetString   : TgluGetString = NIL;
    gluOrtho2D  : TgluOrtho2D = NIL;
    gluPerspective  : TgluPerspective = NIL;
    gluPickMatrix : TgluPickMatrix =NIL;
    gluLookAt : TgluLookAt = NIL;
    gluProject : TgluProject = NIL;
    gluUnProject : TgluUnProject = NIL;
    gluBuild1DMipmaps   : TgluBuild1DMipmaps = NIL;
    gluBuild2DMipmaps : TgluBuild2DMipmaps =NIL;
    gluNewQuadric  : TgluNewQuadric = NIL;
    gluDeleteQuadric : TgluDeleteQuadric = NIL;
    gluQuadricNormals  : TgluQuadricNormals = NIL;
    gluQuadricTexture  : TgluQuadricTexture =NIL;
    gluQuadricOrientation  : TgluQuadricOrientation = NIL;
    gluQuadricDrawStyle  : TgluQuadricDrawStyle =NIL;
    gluCylinder  : TgluCylinder =NIL;
    gluDisk : TgluDisk = NIL;
    gluPartialDisk : TgluPartialDisk = NIL;
    gluSphere : TgluSphere = NIL;
    gluQuadricCallback : TgluQuadricCallback =NIL;
    gluNewTess : TgluNewTess = NIL;
    gluDeleteTess : TgluDeleteTess = NIL;
    gluTessBeginPolygon : TgluTessBeginPolygon = NIL;
    gluTessBeginContour : TgluTessBeginContour =NIL;
    gluTessVertex : TgluTessVertex = NIL;
    gluTessEndContour : TgluTessEndContour =NIL;
    gluTessEndPolygon : TgluTessEndPolygon = NIL;
    gluTessProperty : TgluTessProperty = NIL;
    gluTessNormal : TgluTessNormal = NIL;
    gluTessCallback : TgluTessCallback = NIL;
    gluGetTessProperty : TgluGetTessProperty = NIL;
    gluNewNurbsRenderer : TgluNewNurbsRenderer = NIL;
    gluDeleteNurbsRenderer : TgluDeleteNurbsRenderer = NIL;
    gluBeginSurface : TgluBeginSurface = NIL;
    gluBeginCurve : TgluBeginCurve = NIL;
    gluEndCurve : TgluEndCurve = NIL;
    gluEndSurface : TgluEndSurface = NIL;
    gluBeginTrim: TgluBeginTrim = NIL;
    gluEndTrim: TgluEndTrim = NIL;
    gluPwlCurve : TgluPwlCurve = NIL;
    gluNurbsCurve : TgluNurbsCurve = NIL;
    gluNurbsSurface : TgluNurbsSurface = NIL;
    gluLoadSamplingMatrices : TgluLoadSamplingMatrices = NIL;
    gluNurbsProperty : TgluNurbsProperty = NIL;
    gluGetNurbsProperty : TgluGetNurbsProperty = NIL;
    gluNurbsCallback : TgluNurbsCallback = NIL;
    gluBeginPolygon: TgluBeginPolygon = NIL;
    gluNextContour : TgluNextContour = NIL;
    gluEndPolygon : TgluEndPolygon =NIL;
    gluBuild1DMipmapLevels : TgluBuild1DMipmapLevels = NIL;
    gluBuild2DMipmapLevels : TgluBuild2DMipmapLevels = NIL;
    gluBuild3DMipmapLevels : TgluBuild3DMipmapLevels = NIL;
    gluBuild3DMipmaps : TgluBuild3DMipmaps = NIL;
    gluCheckExtension  : TgluCheckExtension = NIL;
    gluNurbsCallbackData : TgluNurbsCallbackData = NIL;
    gluNurbsCallbackDataEXT : TgluNurbsCallbackDataEXT =NIL;
    gluUnProject4 : TgluUnProject4 = NIL;
    gluQuadricErrorProc : TgluQuadricErrorProc = NIL;
    gluTessBeginProc : TgluTessBeginProc = NIL;
    gluTessEdgeFlagProc : TgluTessEdgeFlagProc = NIL;
    gluTessVertexProc : TgluTessVertexProc = NIL;
    gluTessEndProc  : TgluTessEndProc = NIL;
    gluTessErrorProc : TgluTessErrorProc = NIL;
    gluTessCombineProc : TgluTessCombineProc =NIL;
    gluTessBeginDataProc : TgluTessBeginDataProc = NIL;
    gluTessEdgeFlagDataProc : TgluTessEdgeFlagDataProc = NIL;
    gluTessVertexDataProc : TgluTessVertexDataProc = NIL;
    gluTessEndDataProc : TgluTessEndDataProc = NIL;
    gluTessErrorDataProc : TgluTessErrorDataProc = NIL;
    gluTessCombineDataProc : TgluTessCombineDataProc = NIL;
    gluNurbsErrorProc : TgluNurbsErrorProc = NIL;
    gluScaleImage:TgluScaleImage = NIL;



function  OpenGLULib_Initialize: Boolean;
procedure OpenGLULib_Finalize;
function  OpenGLULib_InitOK: Boolean;

implementation

const
  InvalidLibHandle = 0;
var
  VarOpenGLULibHandle: TLibHandle = InvalidLibHandle;
  VarOpenGLULoaded: Boolean = False; 

function  OpenGLULib_InitOK: Boolean;
begin
  Result:=VarOpenGLULoaded;
end;


procedure OpenGLULib_Finalize;
begin
  if (VarOpenGLULibHandle <> 0) then FreeLibrary(VarOpenGLULibHandle);

    gluErrorString := NIL;
    gluGetString  := NIL;
    gluOrtho2D := NIL;
    gluPerspective := NIL;
    gluPickMatrix := NIL;
    gluLookAt := NIL;
    gluProject := NIL;
    gluUnProject:= NIL;
    gluBuild1DMipmaps  := NIL;
    gluBuild2DMipmaps := NIL;
    gluNewQuadric  := NIL;
    gluDeleteQuadric := NIL;
    gluQuadricNormals := NIL;
    gluQuadricTexture  := NIL;
    gluQuadricOrientation  := NIL;
    gluQuadricDrawStyle := NIL;
    gluCylinder := NIL;
    gluDisk := NIL;
    gluPartialDisk := NIL;
    gluSphere := NIL;
    gluQuadricCallback := NIL;
    gluNewTess := NIL;
    gluDeleteTess := NIL;
    gluTessBeginPolygon := NIL;
    gluTessBeginContour := NIL;
    gluTessVertex := NIL;
    gluTessEndContour := NIL;
    gluTessEndPolygon := NIL;
    gluTessProperty := NIL;
    gluTessNormal := NIL;
    gluTessCallback := NIL;
    gluGetTessProperty := NIL;
    gluNewNurbsRenderer := NIL;
    gluDeleteNurbsRenderer := NIL;
    gluBeginSurface := NIL;
    gluBeginCurve := NIL;
    gluEndCurve := NIL;
    gluEndSurface := NIL;
    gluBeginTrim := NIL;
    gluEndTrim := NIL;
    gluPwlCurve := NIL;
    gluNurbsCurve := NIL;
    gluNurbsSurface := NIL;
    gluLoadSamplingMatrices := NIL;
    gluNurbsProperty := NIL;
    gluGetNurbsProperty := NIL;
    gluNurbsCallback := NIL;
    gluBeginPolygon := NIL;
    gluNextContour := NIL;
    gluEndPolygon := NIL;
    gluBuild1DMipmapLevels := NIL;
    gluBuild2DMipmapLevels := NIL;
    gluBuild3DMipmapLevels := NIL;
    gluBuild3DMipmaps := NIL;
    gluCheckExtension  := NIL;
    gluNurbsCallbackData := NIL;
    gluNurbsCallbackDataEXT := NIL;
    gluUnProject4 := NIL;
    gluQuadricErrorProc := NIL;
    gluTessBeginProc := NIL;
    gluTessEdgeFlagProc := NIL;
    gluTessVertexProc := NIL;
    gluTessEndProc  := NIL;
    gluTessErrorProc := NIL;
    gluTessCombineProc := NIL;
    gluTessBeginDataProc := NIL;
    gluTessEdgeFlagDataProc := NIL;
    gluTessVertexDataProc := NIL;
    gluTessEndDataProc := NIL;
    gluTessErrorDataProc := NIL;
    gluTessCombineDataProc := NIL;
    gluNurbsErrorProc := NIL;
    gluScaleImage := NIL;
end;


function OpenGLULib_Initialize: Boolean;
begin
  Result:=False;
  OpenGLULib_Finalize;
  VarOpenGLULibHandle := LoadLibrary(GLU_LIB);
  if VarOpenGLULibHandle = 0 then Exit(false);

    Pointer(gluErrorString) := GetProcedureAddress(VarOpenGLULibHandle, 'gluErrorString');
    Pointer(gluGetString) := GetProcedureAddress(VarOpenGLULibHandle, 'gluGetString');
    Pointer(gluOrtho2D) := GetProcedureAddress(VarOpenGLULibHandle, 'gluOrtho2D');
    Pointer(gluPerspective) := GetProcedureAddress(VarOpenGLULibHandle, 'gluPerspective');
    Pointer(gluPickMatrix) := GetProcedureAddress(VarOpenGLULibHandle, 'gluPickMatrix');
    Pointer(gluLookAt) := GetProcedureAddress(VarOpenGLULibHandle, 'gluLookAt');
    Pointer(gluProject) := GetProcedureAddress(VarOpenGLULibHandle, 'gluProject');
    Pointer(gluUnProject):= GetProcedureAddress(VarOpenGLULibHandle, 'gluUnProject');
    Pointer(gluBuild1DMipmaps ) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBuild1DMipmaps');
    Pointer(gluBuild2DMipmaps) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBuild2DMipmaps');
    Pointer(gluNewQuadric ) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNewQuadric');
    Pointer(gluDeleteQuadric) := GetProcedureAddress(VarOpenGLULibHandle, 'gluDeleteQuadric');
    Pointer(gluQuadricNormals) := GetProcedureAddress(VarOpenGLULibHandle, 'gluQuadricNormals');
    Pointer(gluQuadricTexture ) := GetProcedureAddress(VarOpenGLULibHandle, 'gluQuadricTexture');
    Pointer(gluQuadricOrientation ) := GetProcedureAddress(VarOpenGLULibHandle, 'gluQuadricOrientation');
    Pointer(gluQuadricDrawStyle) := GetProcedureAddress(VarOpenGLULibHandle, 'gluQuadricDrawStyle');
    Pointer(gluCylinder) := GetProcedureAddress(VarOpenGLULibHandle, 'gluCylinder');
    Pointer(gluDisk) := GetProcedureAddress(VarOpenGLULibHandle, 'gluDisk');
    Pointer(gluPartialDisk) := GetProcedureAddress(VarOpenGLULibHandle, 'gluPartialDisk');
    Pointer(gluSphere) := GetProcedureAddress(VarOpenGLULibHandle, 'gluSphere');
    Pointer(gluQuadricCallback) := GetProcedureAddress(VarOpenGLULibHandle, 'gluQuadricCallback');
    Pointer(gluNewTess) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNewTess');
    Pointer(gluDeleteTess) := GetProcedureAddress(VarOpenGLULibHandle, 'gluDeleteTess');
    Pointer(gluTessBeginPolygon) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessBeginPolygon');
    Pointer(gluTessBeginContour) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessBeginContour');
    Pointer(gluTessVertex) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessVertex');
    Pointer(gluTessEndContour) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessEndContour');
    Pointer(gluTessEndPolygon) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessEndPolygon');
    Pointer(gluTessProperty) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessProperty');
    Pointer(gluTessNormal) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessNormal');
    Pointer(gluTessCallback) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessCallback');
    Pointer(gluGetTessProperty) := GetProcedureAddress(VarOpenGLULibHandle, 'gluGetTessProperty');
    Pointer(gluNewNurbsRenderer) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNewNurbsRenderer');
    Pointer(gluDeleteNurbsRenderer) := GetProcedureAddress(VarOpenGLULibHandle, 'gluDeleteNurbsRenderer');
    Pointer(gluBeginSurface) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBeginSurface');
    Pointer(gluBeginCurve) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBeginCurve');
    Pointer(gluEndCurve) := GetProcedureAddress(VarOpenGLULibHandle, 'gluEndCurve');
    Pointer(gluEndSurface) := GetProcedureAddress(VarOpenGLULibHandle, 'gluEndSurface');
    Pointer(gluBeginTrim) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBeginTrim');
    Pointer(gluEndTrim) := GetProcedureAddress(VarOpenGLULibHandle, 'gluEndTrim');
    Pointer(gluPwlCurve) := GetProcedureAddress(VarOpenGLULibHandle, 'gluPwlCurve');
    Pointer(gluNurbsCurve) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsCurve');
    Pointer(gluNurbsSurface) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsSurface');
    Pointer(gluLoadSamplingMatrices) := GetProcedureAddress(VarOpenGLULibHandle, 'gluLoadSamplingMatrices');
    Pointer(gluNurbsProperty) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsProperty');
    Pointer(gluGetNurbsProperty) := GetProcedureAddress(VarOpenGLULibHandle, 'gluGetNurbsProperty');
    Pointer(gluNurbsCallback) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsCallback');
    Pointer(gluBeginPolygon) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBeginPolygon');
    Pointer(gluNextContour) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNextContour');
    Pointer(gluEndPolygon) := GetProcedureAddress(VarOpenGLULibHandle, 'gluEndPolygon');
    Pointer(gluBuild1DMipmapLevels) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBuild1DMipmapLevels');
    Pointer(gluBuild2DMipmapLevels) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBuild2DMipmapLevels');
    Pointer(gluBuild3DMipmapLevels) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBuild3DMipmapLevels');
    Pointer(gluBuild3DMipmaps) := GetProcedureAddress(VarOpenGLULibHandle, 'gluBuild3DMipmaps');
    Pointer(gluCheckExtension ) := GetProcedureAddress(VarOpenGLULibHandle, 'gluCheckExtension');
    Pointer(gluNurbsCallbackData) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsCallbackData');
    Pointer(gluNurbsCallbackDataEXT) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsCallbackDataEXT');
    Pointer(gluUnProject4) := GetProcedureAddress(VarOpenGLULibHandle, 'gluUnProject4');
    Pointer(gluQuadricErrorProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluQuadricErrorProc');
    Pointer(gluTessBeginProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessBeginProc');
    Pointer(gluTessEdgeFlagProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessEdgeFlagProc');
    Pointer(gluTessVertexProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessVertexProc');
    Pointer(gluTessEndProc ) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessEndProc');
    Pointer(gluTessErrorProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessErrorProc');
    Pointer(gluTessCombineProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessCombineProc');
    Pointer(gluTessBeginDataProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessBeginDataProc');
    Pointer(gluTessEdgeFlagDataProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessEdgeFlagDataProc');
    Pointer(gluTessVertexDataProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessVertexDataProc');
    Pointer(gluTessEndDataProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessEndDataProc');
    Pointer(gluTessErrorDataProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessErrorDataProc');
    Pointer(gluTessCombineDataProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluTessCombineDataProc');
    Pointer(gluNurbsErrorProc) := GetProcedureAddress(VarOpenGLULibHandle, 'gluNurbsErrorProc');
    Pointer(gluScaleImage) := GetProcedureAddress(VarOpenGLULibHandle, 'gluScaleImage');


  Result:=True;
  VarOpenGLULoaded:=True;
end;


initialization
  OpenGLULib_Initialize;

finalization
  OpenGLULib_Finalize;
end.
