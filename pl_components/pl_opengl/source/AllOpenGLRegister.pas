{*************************************************************************
                PilotLogic Software House

  Package pl_OpenGL
  This file is part of CodeTyphon Studio (https://www.pilotlogic.com/)

 ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * ***** END LICENSE BLOCK *****
 ************************************************************************}

unit AllOpenGLRegister;

{$mode objfpc}{$H+}

interface

uses
  Classes,SysUtils,TypInfo,lresources,PropEdits,ComponentEditors,
  {$IFDEF MSWINDOWS}
   ctOpenGL2DCanvas,
  {$ENDIF}
  OpenGLPanel,
  OpenGLCanvas;

procedure Register;

implementation

{$R AllOpenGLRegister.res}

//==========================================================

procedure Register;
begin
 RegisterComponents ('OpenGL',[
                               {$IFDEF MSWINDOWS}
                                TOpenGL2DCanvas,
                                {$ENDIF}
                                TOpenGLCanvas,
                                TOpenGLPanel
                                 ]);



end;

end.

